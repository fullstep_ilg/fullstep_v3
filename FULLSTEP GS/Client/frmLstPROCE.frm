VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPROCE 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar proceso"
   ClientHeight    =   8595
   ClientLeft      =   150
   ClientTop       =   2235
   ClientWidth     =   8100
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPROCE.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   8100
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      Height          =   405
      Left            =   6675
      TabIndex        =   203
      Top             =   8100
      Width           =   1350
   End
   Begin TabDlg.SSTab sstabProce 
      Height          =   7980
      Left            =   60
      TabIndex        =   0
      Top             =   15
      Width           =   7950
      _ExtentX        =   14023
      _ExtentY        =   14076
      _Version        =   393216
      Style           =   1
      Tabs            =   7
      TabsPerRow      =   7
      TabHeight       =   520
      BackColor       =   8421376
      TabCaption(0)   =   "DFiltro de datos generales"
      TabPicture(0)   =   "frmLstPROCE.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraProve"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraGrupo"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraMat"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "fraArt"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "fraGenerales"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Filtro de fechas, presupuestos y personas relacionadas"
      TabPicture(1)   =   "frmLstPROCE.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(1)=   "fraResp"
      Tab(1).Control(2)=   "fraUsuAper"
      Tab(1).Control(3)=   "fraPresupuestos"
      Tab(1).Control(4)=   "fraFechas"
      Tab(1).ControlCount=   5
      TabCaption(2)   =   "Opciones"
      TabPicture(2)   =   "frmLstPROCE.frx":0182
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Line6"
      Tab(2).Control(1)=   "Line5"
      Tab(2).Control(2)=   "Line4"
      Tab(2).Control(3)=   "Line3"
      Tab(2).Control(4)=   "Line2"
      Tab(2).Control(5)=   "Line1"
      Tab(2).Control(6)=   "lblMonedas"
      Tab(2).Control(7)=   "lblOrden"
      Tab(2).Control(8)=   "sdbcMonDenom"
      Tab(2).Control(9)=   "sdbcMonCodigo"
      Tab(2).Control(10)=   "sdbcOrdenar"
      Tab(2).Control(11)=   "picItem"
      Tab(2).Control(12)=   "Picture1"
      Tab(2).Control(13)=   "picIncluir"
      Tab(2).Control(14)=   "picGrup"
      Tab(2).ControlCount=   15
      TabCaption(3)   =   "Opciones"
      TabPicture(3)   =   "frmLstPROCE.frx":019E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraSelProve2"
      Tab(3).Control(1)=   "fraSelProve1"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Opciones"
      TabPicture(4)   =   "frmLstPROCE.frx":01BA
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame5"
      Tab(4).Control(1)=   "Frame4"
      Tab(4).ControlCount=   2
      TabCaption(5)   =   "Opciones"
      TabPicture(5)   =   "frmLstPROCE.frx":01D6
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "fraOfeRecGrupos"
      Tab(5).Control(1)=   "Frame8"
      Tab(5).Control(2)=   "Frame7"
      Tab(5).Control(3)=   "Frame6"
      Tab(5).ControlCount=   4
      TabCaption(6)   =   "Opciones"
      TabPicture(6)   =   "frmLstPROCE.frx":01F2
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "fraGruposCerrados"
      Tab(6).Control(1)=   "Frame13"
      Tab(6).Control(2)=   "Frame12"
      Tab(6).Control(3)=   "Frame11"
      Tab(6).ControlCount=   4
      Begin VB.Frame Frame11 
         Caption         =   "Opciones"
         Height          =   3000
         Left            =   -74835
         TabIndex        =   202
         Top             =   390
         Width           =   7560
         Begin VB.OptionButton opCompPorProve 
            Caption         =   "DResultados por proveedor"
            Height          =   300
            Left            =   1005
            TabIndex        =   95
            Top             =   600
            Value           =   -1  'True
            Width           =   4110
         End
         Begin VB.OptionButton opCompPorItem 
            Caption         =   "DResultados por item"
            Height          =   285
            Left            =   1005
            TabIndex        =   96
            Top             =   1300
            Width           =   3810
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcNivDet 
            Height          =   285
            Left            =   3345
            TabIndex        =   206
            Top             =   2010
            Width           =   1665
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            AutoRestore     =   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   3
            Col.Count       =   2
            Row(0).Col(0)   =   "Proceso"
            Row(1).Col(0)   =   "Grupo"
            Row(2).Col(0)   =   "Item"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3889
            Columns(0).Caption=   "ORDENAR"
            Columns(0).Name =   "ORDENAR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "NUM"
            Columns(1).Name =   "NUM"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2937
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblNivDet 
            Caption         =   "Mostrar detalle a nivel de:"
            Height          =   330
            Left            =   1005
            TabIndex        =   207
            Top             =   2025
            Width           =   2595
         End
      End
      Begin VB.Frame Frame12 
         Caption         =   "Orden"
         Height          =   885
         Left            =   -74835
         TabIndex        =   201
         Top             =   3750
         Width           =   7560
         Begin VB.OptionButton opCompOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   300
            Left            =   4395
            TabIndex        =   104
            Top             =   360
            Width           =   1605
         End
         Begin VB.OptionButton opCompOrdCod 
            Caption         =   "C�digo"
            Height          =   300
            Left            =   1005
            TabIndex        =   103
            Top             =   360
            Value           =   -1  'True
            Width           =   1920
         End
      End
      Begin VB.Frame Frame13 
         Caption         =   "Mostrar items en procesos parcialmente cerrados"
         Height          =   855
         Left            =   -74835
         TabIndex        =   200
         Top             =   2760
         Visible         =   0   'False
         Width           =   7560
         Begin VB.OptionButton optItAbiertosAdj 
            Caption         =   "Items abiertos"
            Height          =   255
            Left            =   1005
            TabIndex        =   100
            Top             =   360
            Width           =   2000
         End
         Begin VB.OptionButton optItCerradosAdj 
            Caption         =   "Items cerrados"
            Height          =   255
            Left            =   3000
            TabIndex        =   101
            Top             =   360
            Width           =   2000
         End
         Begin VB.OptionButton optTodosItAdj 
            Caption         =   "Todos los items"
            Height          =   255
            Left            =   5000
            TabIndex        =   102
            Top             =   360
            Value           =   -1  'True
            Width           =   2220
         End
      End
      Begin VB.Frame fraGruposCerrados 
         Caption         =   "Mostrar grupos en procesos parcialmente cerrados"
         Height          =   885
         Left            =   -74835
         TabIndex        =   199
         Top             =   1770
         Visible         =   0   'False
         Width           =   7560
         Begin VB.OptionButton optGrAbiertosAdj 
            Caption         =   "Grupos abiertos"
            Height          =   255
            Left            =   990
            TabIndex        =   97
            Top             =   360
            Width           =   2000
         End
         Begin VB.OptionButton optGrCerradosAdj 
            Caption         =   "Grupos cerrados"
            Height          =   255
            Left            =   3000
            TabIndex        =   98
            Top             =   360
            Width           =   2000
         End
         Begin VB.OptionButton optGrTodosAdj 
            Caption         =   "Todos los grupos"
            Height          =   255
            Left            =   5000
            TabIndex        =   99
            Top             =   360
            Value           =   -1  'True
            Width           =   2265
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Opciones"
         Height          =   810
         Left            =   -74850
         TabIndex        =   198
         Top             =   405
         Width           =   7560
         Begin VB.CheckBox chkOfeRecSin 
            Caption         =   "Listado de proveedores SIN ofertas"
            Height          =   195
            Left            =   930
            TabIndex        =   80
            Top             =   360
            Width           =   6405
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Ofertas"
         Height          =   2000
         Left            =   -74850
         TabIndex        =   197
         Top             =   1230
         Width           =   7560
         Begin VB.OptionButton optOfeRecItem 
            Caption         =   "DListado por items"
            Height          =   195
            Left            =   480
            TabIndex        =   82
            Top             =   1560
            Width           =   2865
         End
         Begin VB.OptionButton optOfeRecProve 
            Caption         =   "DListado por proveedores"
            Height          =   195
            Left            =   510
            TabIndex        =   81
            Top             =   600
            Value           =   -1  'True
            Width           =   2880
         End
         Begin VB.CheckBox chkOfeRecUltProve 
            Caption         =   "DIncluir solamente la �ltima oferta"
            Height          =   195
            Left            =   3465
            TabIndex        =   83
            Top             =   270
            Width           =   3975
         End
         Begin VB.CheckBox chkOfeRecItems 
            Caption         =   "Incluir precios"
            Height          =   195
            Left            =   3465
            TabIndex        =   84
            Top             =   585
            Width           =   4020
         End
         Begin VB.CheckBox chkOfeRecUltItem 
            Caption         =   "Incluir solamente la �ltima oferta"
            Enabled         =   0   'False
            Height          =   195
            Left            =   3465
            TabIndex        =   87
            Top             =   1590
            Width           =   3885
         End
         Begin VB.CheckBox chkOfeRecAdjuntos 
            Caption         =   "Incluir archivos adjuntos"
            Height          =   195
            Left            =   3465
            TabIndex        =   85
            Top             =   900
            Width           =   4020
         End
         Begin VB.CheckBox chkOfeRecAtributos 
            Caption         =   "Incluir atributos"
            Height          =   195
            Left            =   3465
            TabIndex        =   86
            Top             =   1215
            Width           =   4020
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "Mostrar �tems en procesos parcialmente cerrados"
         Height          =   800
         Left            =   -74850
         TabIndex        =   196
         Top             =   4125
         Width           =   7575
         Begin VB.OptionButton optItemAbiertos 
            Caption         =   "Items abiertos"
            Height          =   255
            Left            =   840
            TabIndex        =   92
            Top             =   350
            Width           =   2190
         End
         Begin VB.OptionButton optItemCerrados 
            Caption         =   "Items cerrados"
            Height          =   255
            Left            =   3120
            TabIndex        =   93
            Top             =   350
            Width           =   2160
         End
         Begin VB.OptionButton optTodosItems 
            Caption         =   "Todos los �tems"
            Height          =   255
            Left            =   5280
            TabIndex        =   94
            Top             =   350
            Value           =   -1  'True
            Width           =   2250
         End
      End
      Begin VB.Frame fraOfeRecGrupos 
         Caption         =   "Mostrar grupos en procesos parcialmente cerrados"
         Height          =   800
         Left            =   -74850
         TabIndex        =   195
         Top             =   3330
         Width           =   7575
         Begin VB.OptionButton opOfeRecTodosGrupos 
            Caption         =   "Todos los grupos"
            Height          =   255
            Left            =   5280
            TabIndex        =   91
            Top             =   350
            Value           =   -1  'True
            Width           =   2205
         End
         Begin VB.OptionButton opOfeRecGCerrados 
            Caption         =   "Grupos cerrados"
            Height          =   255
            Left            =   3120
            TabIndex        =   89
            Top             =   350
            Width           =   2160
         End
         Begin VB.OptionButton opOfeRecGAbiertos 
            Caption         =   "Grupos abiertos"
            Height          =   255
            Left            =   840
            TabIndex        =   88
            Top             =   350
            Width           =   2190
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Opciones"
         Height          =   2835
         Left            =   -74850
         TabIndex        =   189
         Top             =   525
         Width           =   7710
         Begin VB.CheckBox chkComAnulacion 
            Caption         =   "DIncluir aviso de anulaci�n"
            Height          =   300
            Left            =   3945
            TabIndex        =   77
            Top             =   2235
            Width           =   3720
         End
         Begin VB.TextBox txtComHasta 
            Height          =   285
            Left            =   5865
            TabIndex        =   69
            Top             =   480
            Width           =   1125
         End
         Begin VB.CommandButton cmdComCalFecHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7005
            Picture         =   "frmLstPROCE.frx":020E
            Style           =   1  'Graphical
            TabIndex        =   191
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   480
            Width           =   315
         End
         Begin VB.TextBox txtComDesde 
            Height          =   285
            Left            =   3210
            TabIndex        =   68
            Top             =   480
            Width           =   1140
         End
         Begin VB.CommandButton cmdComCalFecDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4380
            Picture         =   "frmLstPROCE.frx":0798
            Style           =   1  'Graphical
            TabIndex        =   190
            TabStop         =   0   'False
            Top             =   480
            Width           =   315
         End
         Begin VB.CheckBox chkComPubWeb 
            Caption         =   "DIncluir Publicaciones en el Web"
            Height          =   300
            Left            =   3945
            TabIndex        =   75
            Top             =   1455
            Value           =   1  'Checked
            Width           =   3705
         End
         Begin VB.CheckBox chkComSinPet 
            Caption         =   "DIncluir Proveedores sin Peticiones"
            Height          =   300
            Left            =   3945
            TabIndex        =   76
            Top             =   1845
            Width           =   3720
         End
         Begin VB.CheckBox chkComObj 
            Caption         =   "DIncluir Comunicaciones de Objetivos"
            Height          =   300
            Left            =   135
            TabIndex        =   72
            Top             =   1845
            Value           =   1  'Checked
            Width           =   3705
         End
         Begin VB.CheckBox chkComAdj 
            Caption         =   "DIncluir Comunicaciones de Adjudicaci�n"
            Height          =   300
            Left            =   135
            TabIndex        =   71
            Top             =   1455
            Value           =   1  'Checked
            Width           =   3720
         End
         Begin VB.CheckBox chkComExc 
            Caption         =   "DIncluir Comunicaciones de Exclusi�n"
            Height          =   300
            Left            =   3945
            TabIndex        =   74
            Top             =   1075
            Value           =   1  'Checked
            Width           =   3705
         End
         Begin VB.CheckBox chkComPet 
            Caption         =   "DIncluir Peticiones de Oferta"
            Height          =   300
            Left            =   135
            TabIndex        =   70
            Top             =   1075
            Value           =   1  'Checked
            Width           =   3720
         End
         Begin VB.CheckBox chkComAvi 
            Caption         =   "DIncluir aviso de despublicaci�n"
            Height          =   300
            Left            =   135
            TabIndex        =   73
            Top             =   2235
            Value           =   1  'Checked
            Width           =   3660
         End
         Begin VB.Label Label10 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   5055
            TabIndex        =   194
            Top             =   540
            Width           =   690
         End
         Begin VB.Label Label8 
            Caption         =   "Desde:"
            Height          =   225
            Left            =   2415
            TabIndex        =   193
            Top             =   525
            Width           =   690
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha de comunicaci�n:"
            Height          =   255
            Left            =   165
            TabIndex        =   192
            Top             =   525
            Width           =   2130
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Orden"
         Height          =   930
         Left            =   -74850
         TabIndex        =   188
         Top             =   3525
         Width           =   7695
         Begin VB.OptionButton opComOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   285
            Left            =   4020
            TabIndex        =   79
            Top             =   435
            Width           =   2400
         End
         Begin VB.OptionButton opComOrdCod 
            Caption         =   "C�digo"
            Height          =   285
            Left            =   390
            TabIndex        =   78
            Top             =   435
            Value           =   -1  'True
            Width           =   2505
         End
      End
      Begin VB.Frame fraSelProve1 
         Caption         =   "Opciones"
         Height          =   2265
         Left            =   -74775
         TabIndex        =   187
         Top             =   405
         Width           =   7560
         Begin VB.CheckBox chkSelProveGrupos 
            Caption         =   "Incluir asignaci�n de grupos"
            Height          =   195
            Left            =   540
            TabIndex        =   204
            Top             =   1665
            Width           =   5070
         End
         Begin VB.CheckBox chkSelProvePot 
            Caption         =   "DIncluir proveedores potenciales"
            Height          =   195
            Left            =   3720
            TabIndex        =   64
            Top             =   480
            Width           =   3780
         End
         Begin VB.CheckBox chkSelProveCal 
            Caption         =   "DIncluir calificaciones"
            Height          =   195
            Left            =   540
            TabIndex        =   62
            Top             =   480
            Width           =   3180
         End
         Begin VB.CheckBox chkSelProveEqp 
            Caption         =   "DIncluir equipos"
            Height          =   195
            Left            =   540
            TabIndex        =   63
            Top             =   1110
            Width           =   3120
         End
         Begin VB.CheckBox chkSelProveComp 
            Caption         =   "DIncluir compradores"
            Height          =   195
            Left            =   3735
            TabIndex        =   65
            Top             =   1110
            Width           =   3765
         End
      End
      Begin VB.Frame fraSelProve2 
         Caption         =   "Orden"
         Height          =   1515
         Left            =   -74775
         TabIndex        =   186
         Top             =   2865
         Width           =   7560
         Begin VB.OptionButton optSelProveOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   3705
            TabIndex        =   67
            Top             =   630
            Width           =   2880
         End
         Begin VB.OptionButton optSelProveOrdCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   720
            TabIndex        =   66
            Top             =   630
            Value           =   -1  'True
            Width           =   1905
         End
      End
      Begin VB.PictureBox picGrup 
         BorderStyle     =   0  'None
         Height          =   590
         Left            =   -74895
         ScaleHeight     =   585
         ScaleWidth      =   7725
         TabIndex        =   184
         Top             =   2850
         Width           =   7725
         Begin VB.OptionButton optGrTodos 
            Caption         =   "Todos los grupos"
            Height          =   255
            Left            =   5280
            TabIndex        =   55
            Top             =   240
            Value           =   -1  'True
            Width           =   2340
         End
         Begin VB.OptionButton optGrCerrados 
            Caption         =   "Grupos cerrados"
            Height          =   255
            Left            =   3120
            TabIndex        =   54
            Top             =   240
            Width           =   2070
         End
         Begin VB.OptionButton optGrAbiertos 
            Caption         =   "Grupos abiertos"
            Height          =   255
            Left            =   840
            TabIndex        =   53
            Top             =   240
            Width           =   2280
         End
         Begin VB.Label lblGrupos 
            Caption         =   "Mostrar grupos en procesos parcialmente cerrados:"
            Enabled         =   0   'False
            Height          =   300
            Left            =   90
            TabIndex        =   185
            Top             =   0
            Width           =   6750
         End
      End
      Begin VB.PictureBox picIncluir 
         BorderStyle     =   0  'None
         Height          =   2205
         Left            =   -71355
         ScaleHeight     =   2205
         ScaleWidth      =   4260
         TabIndex        =   180
         Top             =   480
         Width           =   4260
         Begin VB.CheckBox chkIncPresup 
            Caption         =   "Incluir Distribuc. Pres."
            Height          =   195
            Left            =   15
            TabIndex        =   52
            Top             =   2010
            Width           =   3375
         End
         Begin VB.CheckBox chkIncUO 
            Caption         =   "Incluir Distribuci�n UO"
            Height          =   195
            Left            =   15
            TabIndex        =   51
            Top             =   1650
            Width           =   3255
         End
         Begin VB.CheckBox chkIncPers 
            Caption         =   "Incluir personas"
            Height          =   195
            Left            =   15
            TabIndex        =   50
            Top             =   1290
            Width           =   3255
         End
         Begin VB.CheckBox chkIncEsp 
            Caption         =   "Incluir especificaciones"
            Height          =   255
            Left            =   15
            TabIndex        =   49
            Top             =   930
            Width           =   3135
         End
         Begin VB.CheckBox chkIncAtrib 
            Caption         =   "Incluir atributos"
            Height          =   255
            Left            =   15
            TabIndex        =   47
            Top             =   435
            Width           =   1950
         End
         Begin VB.CheckBox chkDescr 
            Caption         =   "Incluir descripciones"
            Enabled         =   0   'False
            Height          =   255
            Left            =   300
            TabIndex        =   48
            Top             =   660
            Width           =   2040
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcNiv 
            Height          =   285
            Left            =   1125
            TabIndex        =   46
            Top             =   120
            Width           =   1665
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            AutoRestore     =   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   3
            Col.Count       =   2
            Row(0).Col(0)   =   "Proceso"
            Row(1).Col(0)   =   "Grupo"
            Row(2).Col(0)   =   "Item"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3889
            Columns(0).Caption=   "ORDENAR"
            Columns(0).Name =   "ORDENAR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "NUM"
            Columns(1).Name =   "NUM"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2937
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            Enabled         =   0   'False
         End
         Begin VB.Label lblNiv 
            Caption         =   "A nivel de:"
            Height          =   255
            Left            =   15
            TabIndex        =   181
            Top             =   120
            Width           =   1455
         End
      End
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   2205
         Left            =   -74640
         ScaleHeight     =   2205
         ScaleWidth      =   2895
         TabIndex        =   179
         Top             =   480
         Width           =   2895
         Begin VB.OptionButton optCiclo 
            Caption         =   "Listado de ciclo de vida"
            Height          =   195
            Left            =   240
            TabIndex        =   43
            Top             =   360
            Width           =   2580
         End
         Begin VB.OptionButton optResumido 
            Caption         =   "Listado resumido"
            Height          =   195
            Left            =   240
            TabIndex        =   44
            Top             =   960
            Value           =   -1  'True
            Width           =   2580
         End
         Begin VB.OptionButton optDetallado 
            Caption         =   "Listado detallado"
            Height          =   195
            Left            =   240
            TabIndex        =   45
            Top             =   1560
            Width           =   2535
         End
      End
      Begin VB.PictureBox picItem 
         BorderStyle     =   0  'None
         Height          =   590
         Left            =   -74880
         ScaleHeight     =   585
         ScaleWidth      =   7725
         TabIndex        =   177
         Top             =   3600
         Width           =   7725
         Begin VB.OptionButton optItAbiertos 
            Caption         =   "�tems abiertos"
            Height          =   255
            Left            =   840
            TabIndex        =   56
            Top             =   240
            Width           =   2235
         End
         Begin VB.OptionButton optItCerrados 
            Caption         =   "�tems cerrados"
            Height          =   255
            Left            =   3120
            TabIndex        =   57
            Top             =   240
            Width           =   2175
         End
         Begin VB.OptionButton optItTodos 
            Caption         =   "Todos los �tems"
            Height          =   255
            Left            =   5280
            TabIndex        =   58
            Top             =   240
            Value           =   -1  'True
            Width           =   2340
         End
         Begin VB.Label lblItem 
            Caption         =   "Mostrar �tems en procesos parci�lmente cerrados"
            Height          =   255
            Left            =   120
            TabIndex        =   178
            Top             =   0
            Width           =   6930
         End
      End
      Begin VB.Frame fraGenerales 
         Height          =   3570
         Left            =   45
         TabIndex        =   161
         Top             =   360
         Width           =   7815
         Begin VB.CommandButton cmdBorrarSolic 
            Height          =   285
            Left            =   3960
            Picture         =   "frmLstPROCE.frx":0D22
            Style           =   1  'Graphical
            TabIndex        =   163
            TabStop         =   0   'False
            Top             =   2775
            Width           =   315
         End
         Begin VB.CommandButton cmdBuscarSolic 
            Height          =   285
            Left            =   4320
            Picture         =   "frmLstPROCE.frx":0DC7
            Style           =   1  'Graphical
            TabIndex        =   162
            TabStop         =   0   'False
            Top             =   2775
            Width           =   315
         End
         Begin VB.CheckBox chkSubasta 
            Caption         =   "Recibe ofertas en modo subasta"
            Height          =   195
            Left            =   4080
            TabIndex        =   11
            Top             =   2040
            Width           =   3690
         End
         Begin VB.TextBox txtReferencia 
            Height          =   285
            Left            =   1860
            MaxLength       =   20
            TabIndex        =   10
            Top             =   2000
            Width           =   2025
         End
         Begin VB.CheckBox chkAdjReu 
            Caption         =   "De adjudicaci�n mediante reuni�n"
            Height          =   195
            Left            =   4680
            TabIndex        =   15
            Top             =   3180
            Value           =   1  'Checked
            Width           =   3015
         End
         Begin VB.CheckBox chkAdjDir 
            Caption         =   "De adjudicaci�n directa"
            Height          =   195
            Left            =   1860
            TabIndex        =   14
            Top             =   3180
            Value           =   1  'Checked
            Width           =   2490
         End
         Begin VB.TextBox txtPresHasta 
            Height          =   285
            Left            =   4905
            MaxLength       =   100
            TabIndex        =   9
            Top             =   1640
            Width           =   2025
         End
         Begin VB.TextBox txtPresDesde 
            Height          =   285
            Left            =   1860
            MaxLength       =   20
            TabIndex        =   8
            Top             =   1640
            Width           =   2025
         End
         Begin VB.TextBox txtDen 
            Height          =   285
            Left            =   1860
            MaxLength       =   100
            TabIndex        =   4
            Top             =   605
            Width           =   5070
         End
         Begin VB.TextBox txtCod 
            Height          =   285
            Left            =   6000
            MaxLength       =   20
            TabIndex        =   3
            Top             =   255
            Width           =   930
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1Proce_4Cod 
            Height          =   285
            Left            =   3915
            TabIndex        =   2
            Top             =   255
            Width           =   870
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1535
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
            Height          =   285
            Left            =   1860
            TabIndex        =   1
            Top             =   260
            Width           =   930
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1640
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
            Height          =   285
            Left            =   1860
            TabIndex        =   6
            Top             =   1295
            Width           =   945
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1667
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMonDen 
            Height          =   285
            Left            =   2790
            TabIndex        =   7
            Top             =   1290
            Width           =   4140
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   7302
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcDestCodProce 
            Height          =   285
            Left            =   1860
            TabIndex        =   12
            Top             =   2400
            Width           =   1275
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   7
            Columns(0).Width=   1931
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   8017
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "Direcci�n"
            Columns(2).Name =   "DIR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   2143
            Columns(3).Caption=   "Poblaci�n"
            Columns(3).Name =   "POB"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   1217
            Columns(4).Caption=   "CP"
            Columns(4).Name =   "CP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   1296
            Columns(5).Caption=   "Pais"
            Columns(5).Name =   "PAIS"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   1429
            Columns(6).Caption=   "Provincia"
            Columns(6).Name =   "PROVI"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            _ExtentX        =   2249
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcDestDenProce 
            Height          =   285
            Left            =   3135
            TabIndex        =   13
            Top             =   2400
            Width           =   3795
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   7
            Columns(0).Width=   7673
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2143
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   2487
            Columns(2).Caption=   "Direcci�n"
            Columns(2).Name =   "DIR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   1958
            Columns(3).Caption=   "Poblaci�n"
            Columns(3).Name =   "POB"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   1164
            Columns(4).Caption=   "CP"
            Columns(4).Name =   "CP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   1032
            Columns(5).Caption=   "Pais"
            Columns(5).Name =   "PAIS"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   1349
            Columns(6).Caption=   "Provincia"
            Columns(6).Name =   "PROVI"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            _ExtentX        =   6694
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEst 
            Height          =   285
            Left            =   1860
            TabIndex        =   5
            Top             =   945
            Width           =   5070
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   8361
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   8943
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblIdSolic 
            Height          =   255
            Left            =   4920
            TabIndex        =   176
            Top             =   2520
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.Label lblSolicCompras 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1860
            TabIndex        =   175
            Top             =   2775
            Width           =   2025
         End
         Begin VB.Label lblSolicitud 
            Caption         =   "DSolicitud:"
            Height          =   255
            Left            =   120
            TabIndex        =   174
            Top             =   2805
            Width           =   1335
         End
         Begin VB.Label lblReferencia 
            Caption         =   "DReferencia:"
            Height          =   225
            Left            =   120
            TabIndex        =   173
            Top             =   2060
            Width           =   1590
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Commodity:"
            Height          =   225
            Left            =   2880
            TabIndex        =   172
            Top             =   290
            Width           =   930
         End
         Begin VB.Label lblMon 
            Caption         =   "Moneda:"
            Height          =   240
            Left            =   120
            TabIndex        =   171
            Top             =   1355
            Width           =   1515
         End
         Begin VB.Label lblPresHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   4095
            TabIndex        =   170
            Top             =   1695
            Width           =   780
         End
         Begin VB.Label lblPresDesde 
            Caption         =   "Presupuesto:"
            Height          =   225
            Left            =   120
            TabIndex        =   169
            Top             =   1700
            Width           =   1590
         End
         Begin VB.Label lblAnyo 
            Caption         =   "A�o:"
            ForeColor       =   &H00000000&
            Height          =   165
            Left            =   120
            TabIndex        =   168
            Top             =   320
            Width           =   1590
         End
         Begin VB.Label lblDen 
            Caption         =   "Den.:"
            Height          =   225
            Left            =   120
            TabIndex        =   167
            Top             =   675
            Width           =   1590
         End
         Begin VB.Label lblCod 
            Caption         =   "C�d.Proceso:"
            Height          =   225
            Left            =   4935
            TabIndex        =   166
            Top             =   290
            Width           =   1050
         End
         Begin VB.Label lblEst 
            Caption         =   "Estado:"
            Height          =   225
            Left            =   120
            TabIndex        =   165
            Top             =   1040
            Width           =   1590
         End
         Begin VB.Label lblDest 
            Caption         =   "Destino"
            Height          =   255
            Left            =   120
            TabIndex        =   164
            Top             =   2415
            Width           =   1605
         End
      End
      Begin VB.Frame fraFechas 
         Caption         =   "Fechas"
         Height          =   2100
         Left            =   -74955
         TabIndex        =   144
         Top             =   360
         Width           =   7815
         Begin VB.TextBox txtFecAdjDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1845
            TabIndex        =   211
            Top             =   1680
            Width           =   1125
         End
         Begin VB.CommandButton cmdCalFEcAdjDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3000
            Picture         =   "frmLstPROCE.frx":0E54
            Style           =   1  'Graphical
            TabIndex        =   210
            TabStop         =   0   'False
            Top             =   1680
            Width           =   315
         End
         Begin VB.CommandButton cmdCalFecAdjHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6240
            Picture         =   "frmLstPROCE.frx":13DE
            Style           =   1  'Graphical
            TabIndex        =   209
            TabStop         =   0   'False
            Top             =   1665
            Width           =   315
         End
         Begin VB.TextBox txtFecAdjHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5100
            TabIndex        =   208
            Top             =   1665
            Width           =   1110
         End
         Begin VB.TextBox txtFecUltReuHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5100
            TabIndex        =   37
            Top             =   1320
            Width           =   1110
         End
         Begin VB.CommandButton cmdcalFecUltReuHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6240
            Picture         =   "frmLstPROCE.frx":1968
            Style           =   1  'Graphical
            TabIndex        =   152
            TabStop         =   0   'False
            Top             =   1320
            Width           =   315
         End
         Begin VB.TextBox txtFecUltReuDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1860
            TabIndex        =   36
            Top             =   1320
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecUltReuDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3000
            Picture         =   "frmLstPROCE.frx":1EF2
            Style           =   1  'Graphical
            TabIndex        =   151
            TabStop         =   0   'False
            Top             =   1335
            Width           =   315
         End
         Begin VB.TextBox txtFecApeHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5100
            TabIndex        =   31
            Top             =   240
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecApeHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6240
            Picture         =   "frmLstPROCE.frx":247C
            Style           =   1  'Graphical
            TabIndex        =   150
            TabStop         =   0   'False
            Top             =   255
            Width           =   315
         End
         Begin VB.TextBox txtFecApeDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1860
            TabIndex        =   30
            Top             =   240
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecApeDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3000
            Picture         =   "frmLstPROCE.frx":2A06
            Style           =   1  'Graphical
            TabIndex        =   149
            TabStop         =   0   'False
            Top             =   255
            Width           =   315
         End
         Begin VB.TextBox txtFecNeceHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5100
            TabIndex        =   33
            Top             =   600
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalfecNecHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6240
            Picture         =   "frmLstPROCE.frx":2F90
            Style           =   1  'Graphical
            TabIndex        =   148
            TabStop         =   0   'False
            Top             =   615
            Width           =   315
         End
         Begin VB.TextBox txtFecNeceDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1860
            TabIndex        =   32
            Top             =   600
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecNecDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3000
            Picture         =   "frmLstPROCE.frx":351A
            Style           =   1  'Graphical
            TabIndex        =   147
            TabStop         =   0   'False
            Top             =   615
            Width           =   315
         End
         Begin VB.TextBox txtFecPresHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5100
            TabIndex        =   35
            Top             =   960
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecPresHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6240
            Picture         =   "frmLstPROCE.frx":3AA4
            Style           =   1  'Graphical
            TabIndex        =   146
            TabStop         =   0   'False
            Top             =   960
            Width           =   315
         End
         Begin VB.TextBox txtFecPresDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1860
            TabIndex        =   34
            Top             =   960
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecPresDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3000
            Picture         =   "frmLstPROCE.frx":402E
            Style           =   1  'Graphical
            TabIndex        =   145
            TabStop         =   0   'False
            Top             =   975
            Width           =   315
         End
         Begin VB.Label lblFecAdjDesde 
            Caption         =   "Adjudicaci�n:"
            Height          =   225
            Left            =   120
            TabIndex        =   213
            Top             =   1695
            Width           =   1650
         End
         Begin VB.Label lblFecAdjHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   4245
            TabIndex        =   212
            Top             =   1680
            Width           =   840
         End
         Begin VB.Label lblFecUltReuHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   4245
            TabIndex        =   160
            Top             =   1380
            Width           =   840
         End
         Begin VB.Label lblFecUltReuDesde 
            BackStyle       =   0  'Transparent
            Caption         =   "Ultima reuni�n:"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   120
            TabIndex        =   159
            Top             =   1365
            Width           =   1800
         End
         Begin VB.Label lblFecPresHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   4245
            TabIndex        =   158
            Top             =   1020
            Width           =   840
         End
         Begin VB.Label lblFecNeceHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   4245
            TabIndex        =   157
            Top             =   660
            Width           =   840
         End
         Begin VB.Label lblFecApeHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   4245
            TabIndex        =   156
            Top             =   300
            Width           =   840
         End
         Begin VB.Label lblFecPresDesde 
            BackStyle       =   0  'Transparent
            Caption         =   "Presentaci�n:"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   120
            TabIndex        =   155
            Top             =   1005
            Width           =   1800
         End
         Begin VB.Label lblFecNeceDesde 
            BackStyle       =   0  'Transparent
            Caption         =   "Necesidad:"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   120
            TabIndex        =   154
            Top             =   660
            Width           =   1800
         End
         Begin VB.Label lblFecApeDesde 
            BackStyle       =   0  'Transparent
            Caption         =   "Apertura:"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   120
            TabIndex        =   153
            Top             =   300
            Width           =   1800
         End
      End
      Begin VB.Frame fraArt 
         Caption         =   "Art�culo"
         Height          =   705
         Left            =   45
         TabIndex        =   141
         Top             =   6330
         Width           =   7815
         Begin VB.TextBox txtDenArticulo 
            Height          =   285
            Left            =   4020
            MaxLength       =   100
            TabIndex        =   27
            Top             =   270
            Width           =   3705
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCodArticulo 
            Height          =   285
            Left            =   1860
            TabIndex        =   26
            Top             =   270
            Width           =   1650
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2381
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7170
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2910
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin VB.Label lblDenArticulo 
            Caption         =   "Den.:"
            Height          =   225
            Left            =   3570
            TabIndex        =   143
            Top             =   330
            Width           =   420
         End
         Begin VB.Label lblCodArticulo 
            Caption         =   "C�digo:"
            Height          =   225
            Left            =   120
            TabIndex        =   142
            Top             =   300
            Width           =   1095
         End
      End
      Begin VB.Frame fraMat 
         Caption         =   "Material"
         Height          =   1740
         Left            =   45
         TabIndex        =   136
         Top             =   4590
         Width           =   7815
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7410
            Picture         =   "frmLstPROCE.frx":45B8
            Style           =   1  'Graphical
            TabIndex        =   137
            TabStop         =   0   'False
            Top             =   180
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
            Height          =   285
            Left            =   1860
            TabIndex        =   20
            Top             =   625
            Width           =   1260
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2222
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
            Height          =   285
            Left            =   1860
            TabIndex        =   22
            Top             =   965
            Width           =   1260
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2222
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
            Height          =   285
            Left            =   1860
            TabIndex        =   24
            Top             =   1305
            Width           =   1260
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2222
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
            Height          =   285
            Left            =   3135
            TabIndex        =   21
            Top             =   635
            Width           =   3765
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4498
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1164
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
            Height          =   285
            Left            =   3135
            TabIndex        =   23
            Top             =   970
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4498
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1164
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
            Height          =   285
            Left            =   3135
            TabIndex        =   25
            Top             =   1305
            Width           =   3765
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4498
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1164
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   1860
            TabIndex        =   18
            Top             =   270
            Width           =   1260
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2222
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
            Height          =   285
            Left            =   3150
            TabIndex        =   19
            Top             =   270
            Width           =   3765
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4498
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1164
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblGMN1Mat 
            Caption         =   "Familia1:"
            Height          =   225
            Left            =   135
            TabIndex        =   205
            Top             =   345
            Width           =   1335
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "Grupo:"
            Height          =   225
            Left            =   135
            TabIndex        =   140
            Top             =   1365
            Width           =   1065
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "Familia:"
            Height          =   225
            Left            =   120
            TabIndex        =   139
            Top             =   675
            Width           =   1095
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "Subfamilia:"
            Height          =   225
            Left            =   120
            TabIndex        =   138
            Top             =   1005
            Width           =   1110
         End
      End
      Begin VB.Frame fraGrupo 
         Caption         =   "Grupo"
         Height          =   630
         Left            =   45
         TabIndex        =   133
         Top             =   3945
         Width           =   7815
         Begin VB.TextBox txtDenGrupo 
            Height          =   285
            Left            =   4020
            MaxLength       =   100
            TabIndex        =   17
            Top             =   210
            Width           =   3705
         End
         Begin VB.TextBox txtCodGrupo 
            Height          =   285
            Left            =   1860
            MaxLength       =   100
            TabIndex        =   16
            Top             =   210
            Width           =   1575
         End
         Begin VB.Label lblCodGr 
            Caption         =   "C�digo:"
            Height          =   225
            Left            =   120
            TabIndex        =   135
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label lblDenGr 
            Caption         =   "Den.:"
            Height          =   225
            Left            =   3570
            TabIndex        =   134
            Top             =   270
            Width           =   420
         End
      End
      Begin VB.Frame fraPresupuestos 
         Caption         =   "Conceptos presupuestarios"
         Height          =   1800
         Left            =   -74955
         TabIndex        =   116
         Top             =   2475
         Width           =   7815
         Begin VB.CommandButton cmdBorrarPresup1 
            Height          =   285
            Left            =   6945
            Picture         =   "frmLstPROCE.frx":4624
            Style           =   1  'Graphical
            TabIndex        =   124
            TabStop         =   0   'False
            Top             =   195
            Width           =   315
         End
         Begin VB.CommandButton cmdSelPresup1 
            Height          =   285
            Left            =   7305
            Picture         =   "frmLstPROCE.frx":46C9
            Style           =   1  'Graphical
            TabIndex        =   123
            TabStop         =   0   'False
            Top             =   195
            Width           =   315
         End
         Begin VB.CommandButton cmdBorrarPresup2 
            Height          =   285
            Left            =   6945
            Picture         =   "frmLstPROCE.frx":4735
            Style           =   1  'Graphical
            TabIndex        =   122
            TabStop         =   0   'False
            Top             =   585
            Width           =   315
         End
         Begin VB.CommandButton cmdSelPresup2 
            Height          =   285
            Left            =   7305
            Picture         =   "frmLstPROCE.frx":47DA
            Style           =   1  'Graphical
            TabIndex        =   121
            TabStop         =   0   'False
            Top             =   585
            Width           =   315
         End
         Begin VB.CommandButton cmdBorrarPresup3 
            Height          =   285
            Left            =   6945
            Picture         =   "frmLstPROCE.frx":4846
            Style           =   1  'Graphical
            TabIndex        =   120
            TabStop         =   0   'False
            Top             =   990
            Width           =   315
         End
         Begin VB.CommandButton cmdSelPresup3 
            Height          =   285
            Left            =   7305
            Picture         =   "frmLstPROCE.frx":48EB
            Style           =   1  'Graphical
            TabIndex        =   119
            TabStop         =   0   'False
            Top             =   990
            Width           =   315
         End
         Begin VB.CommandButton cmdBorrarPresup4 
            Height          =   285
            Left            =   6945
            Picture         =   "frmLstPROCE.frx":4957
            Style           =   1  'Graphical
            TabIndex        =   118
            TabStop         =   0   'False
            Top             =   1395
            Width           =   315
         End
         Begin VB.CommandButton cmdSelPresup4 
            Height          =   285
            Left            =   7305
            Picture         =   "frmLstPROCE.frx":49FC
            Style           =   1  'Graphical
            TabIndex        =   117
            TabStop         =   0   'False
            Top             =   1395
            Width           =   315
         End
         Begin VB.Label lblPresup 
            Caption         =   "Presupuesto tipo1:"
            Height          =   225
            Index           =   0
            Left            =   120
            TabIndex        =   132
            Top             =   255
            Width           =   1740
         End
         Begin VB.Label lblPresup1 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1860
            TabIndex        =   131
            Top             =   195
            Width           =   4995
         End
         Begin VB.Label lblPresup 
            Caption         =   "Presupuesto tipo2:"
            Height          =   225
            Index           =   1
            Left            =   120
            TabIndex        =   130
            Top             =   645
            Width           =   1740
         End
         Begin VB.Label lblPresup2 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1860
            TabIndex        =   129
            Top             =   585
            Width           =   4995
         End
         Begin VB.Label lblPresup 
            Caption         =   "Presupuesto tipo3:"
            Height          =   225
            Index           =   2
            Left            =   120
            TabIndex        =   128
            Top             =   1050
            Width           =   1740
         End
         Begin VB.Label lblPresup3 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1860
            TabIndex        =   127
            Top             =   990
            Width           =   4995
         End
         Begin VB.Label lblPresup 
            Caption         =   "Presupuesto tipo4:"
            Height          =   225
            Index           =   3
            Left            =   120
            TabIndex        =   126
            Top             =   1455
            Width           =   1740
         End
         Begin VB.Label lblPresup4 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1860
            TabIndex        =   125
            Top             =   1395
            Width           =   4995
         End
      End
      Begin VB.Frame fraUsuAper 
         Caption         =   "Datos del usuario que abre el proceso"
         Height          =   900
         Left            =   -74955
         TabIndex        =   112
         Top             =   4320
         Width           =   7815
         Begin VB.CommandButton cmdBorrarUsuAper 
            Height          =   285
            Left            =   6945
            Picture         =   "frmLstPROCE.frx":4A68
            Style           =   1  'Graphical
            TabIndex        =   114
            TabStop         =   0   'False
            Top             =   375
            Width           =   315
         End
         Begin VB.CommandButton cmdSelUsuAper 
            Height          =   285
            Left            =   7305
            Picture         =   "frmLstPROCE.frx":4B0D
            Style           =   1  'Graphical
            TabIndex        =   113
            TabStop         =   0   'False
            Top             =   375
            Width           =   315
         End
         Begin VB.Label lblUsuAper 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1860
            TabIndex        =   115
            Top             =   375
            Width           =   4995
         End
      End
      Begin VB.Frame fraResp 
         Caption         =   "Datos del responsable del proceso"
         Height          =   900
         Left            =   -74955
         TabIndex        =   108
         Top             =   5280
         Width           =   7815
         Begin VB.CommandButton cmdBorrarResp 
            Height          =   285
            Left            =   6945
            Picture         =   "frmLstPROCE.frx":4B79
            Style           =   1  'Graphical
            TabIndex        =   110
            TabStop         =   0   'False
            Top             =   375
            Width           =   315
         End
         Begin VB.CommandButton cmdSelResp 
            Height          =   285
            Left            =   7305
            Picture         =   "frmLstPROCE.frx":4C1E
            Style           =   1  'Graphical
            TabIndex        =   109
            TabStop         =   0   'False
            Top             =   375
            Width           =   315
         End
         Begin VB.Label lblResp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1860
            TabIndex        =   111
            Top             =   375
            Width           =   4995
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Comprador"
         Height          =   1635
         Left            =   -74955
         TabIndex        =   105
         Top             =   6225
         Width           =   7815
         Begin VB.OptionButton optComp 
            Caption         =   "Asignado"
            Height          =   195
            Left            =   240
            TabIndex        =   38
            Top             =   315
            Width           =   1815
         End
         Begin VB.OptionButton optResp 
            Caption         =   "Responsable"
            Height          =   195
            Left            =   2100
            TabIndex        =   39
            Top             =   315
            Width           =   1635
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
            Height          =   285
            Left            =   1875
            TabIndex        =   40
            Top             =   705
            Width           =   4995
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1640
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   8811
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCompDen 
            Height          =   285
            Left            =   1875
            TabIndex        =   41
            Top             =   1185
            Width           =   4995
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5424
            Columns(0).Caption=   "Apellido y nombre"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1296
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   8811
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label Label6 
            BackStyle       =   0  'Transparent
            Caption         =   "Equipo:"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   135
            TabIndex        =   107
            Top             =   720
            Width           =   1800
         End
         Begin VB.Label Label7 
            BackStyle       =   0  'Transparent
            Caption         =   "Comprador:"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   135
            TabIndex        =   106
            Top             =   1200
            Width           =   1800
         End
      End
      Begin VB.Frame fraProve 
         Caption         =   "Filtro por proveedor"
         Height          =   780
         Left            =   45
         TabIndex        =   42
         Top             =   7080
         Width           =   7815
         Begin VB.CommandButton cmdSelProveBuscar 
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7425
            Picture         =   "frmLstPROCE.frx":4C8A
            Style           =   1  'Graphical
            TabIndex        =   90
            TabStop         =   0   'False
            Top             =   285
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   3690
            TabIndex        =   29
            Top             =   285
            Width           =   3705
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6535
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   1860
            TabIndex        =   28
            Top             =   285
            Width           =   1815
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3201
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcOrdenar 
         Height          =   285
         Left            =   -74160
         TabIndex        =   59
         Top             =   4680
         Width           =   2040
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         AutoRestore     =   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3889
         Columns(0).Caption=   "ORDENAR"
         Columns(0).Name =   "ORDENAR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "NUM"
         Columns(1).Name =   "NUM"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3598
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMonCodigo 
         Height          =   285
         Left            =   -70695
         TabIndex        =   60
         Top             =   4680
         Width           =   1035
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1905
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "Equivalencia"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMonDenom 
         Height          =   285
         Left            =   -69630
         TabIndex        =   61
         Top             =   4680
         Width           =   2385
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1905
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "Equivalencia"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   4207
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblOrden 
         Caption         =   "Orden:"
         Height          =   255
         Left            =   -74160
         TabIndex        =   183
         Top             =   4320
         Width           =   1935
      End
      Begin VB.Label lblMonedas 
         Caption         =   "Mostrar las cantidades en:"
         Height          =   255
         Left            =   -70680
         TabIndex        =   182
         Top             =   4320
         Width           =   1770
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   -75000
         X2              =   -66960
         Y1              =   4200
         Y2              =   4200
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   -71160
         X2              =   -71160
         Y1              =   5160
         Y2              =   4200
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   -75000
         X2              =   -66960
         Y1              =   3480
         Y2              =   3480
      End
      Begin VB.Line Line4 
         X1              =   -74880
         X2              =   -67200
         Y1              =   2880
         Y2              =   2880
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   -71550
         X2              =   -71550
         Y1              =   2760
         Y2              =   480
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   -75000
         X2              =   -66960
         Y1              =   2760
         Y2              =   2760
      End
   End
End
Attribute VB_Name = "frmLstPROCE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public oProceEncontrados As CProcesos
Public g_oProceSeleccionado As CProceso
Public g_oProveSeleccionado As CProveedor
Private m_oProceso As CProceso
Private oFos As FileSystemObject
Private RepPath As String
Private sSeleccion As String
Private sText() As String
Private iAnio As Integer

Private udtBusquedaProc As TipoBusquedaProcesos
Public DesdeEst As TipoEstadoProceso
Public HastaEst As TipoEstadoProceso
    
' Variables de restricciones
Public bRMat As Boolean
Public bRAsig As Boolean
Public bRCompResponsable As Boolean
Public bREqpAsig As Boolean
Public bRUsuAper As Boolean
Public bRUsuUON As Boolean
Public bRUsuDep As Boolean
Public bRDest As Boolean
Public m_bProveAsigEqp As Boolean   ' ver solo lo asignado al equipo del usuario
Public m_bProveAsigComp As Boolean  ' ver solo lo asignado al comprador del usuario
Public bRUO As Boolean ' distribuci�n en Unidades organizativas a las que pertenece
Public bSoloAdjDir As Boolean       'Permitir solo procesos adj. directa
Public bSoloAdjReu As Boolean
Private m_bMostrarSolicitud As Boolean
Public g_bEsInvitado As Boolean
Private bRestProvMatComp As Boolean

'Recordset desconectados para los ttx
Public g_adoresDG As Ador.Recordset
Public g_adoresArt As Ador.Recordset
Public g_adoresEsp As Ador.Recordset
Public g_adoresPre4 As Ador.Recordset
Public g_adoresPre3 As Ador.Recordset
Public g_adoresPre2 As Ador.Recordset
Public g_adoresPre1 As Ador.Recordset
Public g_adoresPer As Ador.Recordset
Public g_adoresOrg As Ador.Recordset
Public g_adoresAtrib As Ador.Recordset
Public g_adoresReu As Ador.Recordset
Public g_adoresOfe As Ador.Recordset
Public g_adoresPro As Ador.Recordset
Public g_adoresAho As Ador.Recordset
Public g_adoresComu As Ador.Recordset
Public g_adoresAdj As Ador.Recordset
Public g_adoresEsc As Ador.Recordset
Public g_sOrigen As String


'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4

Private oEqps As CEquipos
Private oEqpSeleccionado As CEquipo

Private oComps As CCompradores
Private oCompSeleccionado As CComprador

Private oProves As CProveedores

'Variables para Art�culos
Private oArticulos As CArticulos
Private oArticuloSeleccionado As CArticulo

Private bHayEscalados As Boolean

' Variables para el manejo de combos
Private bRespetarCombo As Boolean
Public bRespetarComboGMN2 As Boolean
Public bRespetarComboGMN3 As Boolean
Public bRespetarComboGMN4 As Boolean
Public bRespetarComboArt As Boolean
Private bRespetarComboEst As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN1RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Private GMN2RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean
Private GMN3RespetarCombo As Boolean
Private GMN4CargarComboDesde As Boolean
Private GMN4RespetarCombo As Boolean

Private bCargarComboDesde As Boolean

Private m_bDestRespetarCombo As Boolean
Private bCargarComboDesdeEqp As Boolean
Private bCargarComboDesdeComp As Boolean
Private bRespetarComboProve As Boolean
Private bPermProcMultiMaterial As Boolean

Private oMonedas As CMonedas
Private m_oDestinos As CDestinos

'Variable para saber el origen
Public sOrigen As String

Private dPresDesde As Double
Private dPresHasta As Double

'Presupuestos:
Private m_vPresup1_1 As Variant
Private m_vPresup1_2 As Variant
Private m_vPresup1_3 As Variant
Private m_vPresup1_4 As Variant

Private m_vPresup2_1 As Variant
Private m_vPresup2_2 As Variant
Private m_vPresup2_3 As Variant
Private m_vPresup2_4 As Variant

Private m_vPresup3_1 As Variant
Private m_vPresup3_2 As Variant
Private m_vPresup3_3 As Variant
Private m_vPresup3_4 As Variant

Private m_vPresup4_1 As Variant
Private m_vPresup4_2 As Variant
Private m_vPresup4_3 As Variant
Private m_vPresup4_4 As Variant


Private m_sUON1Pres1 As Variant
Private m_sUON2Pres1 As Variant
Private m_sUON3Pres1 As Variant

Private m_sUON1Pres2 As Variant
Private m_sUON2Pres2 As Variant
Private m_sUON3Pres2 As Variant

Private m_sUON1Pres3 As Variant
Private m_sUON2Pres3 As Variant
Private m_sUON3Pres3 As Variant

Private m_sUON1Pres4 As Variant
Private m_sUON2Pres4 As Variant
Private m_sUON3Pres4 As Variant

Private m_sCodResp As Variant
Private m_sCodPerAper As Variant

Private m_sUON1Aper As Variant
Private m_sUON2Aper As Variant
Private m_sUON3Aper As Variant

Private m_sUON1Resp As Variant
Private m_sUON2Resp As Variant
Private m_sUON3Resp As Variant

Public m_sDepAper As Variant
Public m_sDepResp As Variant


'Multilenguaje
Private sIdiEst(1 To 10) As String
Private sIdiMoneda As String
Private sIdiMaterial As String
Private sIdiDetalle As String
Private sIdiResponsable As String
Private sIdiFecha As String
Private sIdiPresupuesto As String
Private m_sPresGlobalDesde As String
Private m_sPresGlobalHasta As String
Private m_sFechaAperDesde As String
Private m_sFechaAperHasta As String
Private m_sFechaNecDesde As String
Private m_sFechaNecHasta As String
Private m_sFechaPresDesde As String
Private m_sFechaPresHasta As String
Private m_sFechaUltReuDesde As String
Private m_sFechaUltReuHasta As String
Private m_sFechaAdjDesde As String
Private m_sFechaAdjHasta As String
Private m_stxtDetalle As String
Private m_sUsuApertura As String
' Variables de idioma
Private sEstados(1 To 12) As String
Private sOrdenar(1 To 9) As String
Private sEspera(1 To 3) As String
Private sOpc As String
Private sTituloI As String
Private sTit As String
Private sTitulo(1 To 10) As String
Private sFilPro As String
Private sMON As String
Private sMat As String
Private sFec As String
Private m_sIdiMon1 As String
Private m_sIdiMon2 As String
Private m_sIdiMon3 As String
Private m_sIdiSubasta As String
Private m_stxtDir As String
Private m_stxtReu As String
Private m_stxtEqpRes As String
Private m_stxtEqp As String
Private m_stxtCompAs As String
Private m_stxtCompRes As String
Private m_stxtDenE As String
Private m_stxtProveSel As String
Private m_stxtProvePot As String
Private m_stxtFDesde As String
Private m_stxtFHasta As String
Private m_stxtProveSin As String
Private m_stxtUltOfe As String
Private m_stxtAsignado As String
Private m_stxtAnio As String
Private m_stxtCod As String
Private m_stxtDen As String
Private m_stxtEstado As String
Private m_stxtResp As String
Private m_stxtPag As String
Private m_stxtDe As String
Private m_stxtfec As String
Private m_stxtNece As String
Private m_stxtAper As String
Private m_stxtPresen As String
Private m_stxtLimitOfer As String
Private m_stxtMon As String
Private m_stxtCambio As String
Private m_stxtPres As String
Private m_stxtPresAnu As String
Private m_stxtitems As String
Private m_stxtDest As String
Private m_stxtUnd As String
Private m_stxtCant As String
Private m_stxtPrec As String
Private m_stxtPago As String
Private m_stxtPagoP As String
Private m_stxtIni As String
Private m_stxtFin As String
Private m_stxtEspec As String
Private m_stxtPerson As String
Private m_stxtNom As String
Private m_stxtCar As String
Private m_stxtRol As String
Private m_stxtTfno As String
Private m_stxtFax As String
Private m_stxtEmail As String
Private m_stxtObj As String
Private m_stxtSeleccion As String
Private m_stxtCal As String
Private m_stxtComp As String
Private m_stxtLimi As String
Private m_stxtProve As String
Private m_stxtPubWeb As String
Private m_stxtCAdj As String
Private m_stxtCarta As String
Private m_stxtCObj As String
Private m_stxtCExc As String
Private m_stxtCAvi As String
Private m_stxtComunic As String
Private m_stxtContac As String
Private m_stxtCPet As String
Private m_stxtFecCom As String
Private m_stxtFecDec As String
Private m_stxtTipo As String
Private m_stxtWeb As String
Private m_stxtHomo As String
Private m_stxtFecVal As String
Private m_stxtFecRec As String
Private m_stxtEquiv As String
Private m_stxtObs As String
Private m_stxtNumOfe As String
Private m_stxtFecValidez As String
Private m_stxtCantMax As String
Private m_stxtOfer As String
Private m_stxtProce As String
Private m_stxtFecAdj As String
Private m_stxtImp As String
Private m_stxtAho As String
Private m_stxtItem As String
Private m_stxtNOf As String
Private m_stxtPrecAdj As String
Private m_stxtCantAdj As String
Private m_stxtImpAdj As String
Private m_stxtPAdj As String
Private m_stxtPAho As String
Private m_stxtNombreAdjunto As String
Private m_stxtComentarioAdjunto As String
Private m_stxtOfertaAnterior As String
Private m_stxtFecValSProv As String
Private m_stxtPresup As String  'Presup anuales de tipo:
Private m_stxtProveAct As String
Private m_stxtCerr As String
Private m_stxtPersonas As String
Private m_stxtProveAsig As String
Private m_stxtCodCom As String
Private m_stxtCodProv As String
Private m_stxtOfertas As String
Private m_stxtReuniones As String
Private m_stxtHora As String
Private m_stxtEstProc As String
Private m_stxtEstProc2 As String
Private m_stxtArticulos As String
Private m_stxtEspecificaciones As String
Private m_stxtAdjudicaciones As String
Private m_stxtAdjudicado As String
Private m_stxtConsumido As String
Private m_stxtAhorrado As String
Private m_stxtComunica As String
Private m_stxtProveAdj As String
Private m_stxtNomAr As String
Private m_stxtProveedor As String
Private m_stxtListado As String
Private m_stxtTipoAdj As String
Private m_stxtSubasta As String
Private m_stxtPorcen As String
Private m_stxtDistribuc As String
Private m_stxtDatos As String
Private m_stxtGrupo As String
Private m_stxtEquipo As String
Private m_stxtPresentacion As String
Private m_stxtAdjudicacion As String
Private m_stxtValidacion As String
Private m_stxtProvAsign As String
Private m_stxtComp2 As String
Private m_stxtNivP As String
Private m_stxtNivG As String
Private m_stxtNivI As String
Private m_stxtPedido As String
Private m_stxtProceso As String
Private m_iNivel As Integer
Private m_stxtDistrOrg As String
Private m_stxtDistrPre As String
Private m_stxtUnds As String
Private m_stxtPrecio1 As String
Private m_stxtPrecio2 As String
Private m_stxtPrecio3 As String
Private m_stxtUsarPrec As String
Private m_stxtValor As String
Private m_stxtSi As String
Private m_stxtNo As String
Private m_stxtArchivo As String
Private m_stxtComent As String
Private m_stxtProvAct As String
Private m_stxtSolicit As String
Private m_stxtSolic As String
Private m_stxtComent1 As String
Private m_stxtComent2 As String
Private m_stxtComent3 As String
Private m_stxtBajMinPuja As String
Private m_stxtInicioSubasta As String
Private m_stxtCierreSubasta As String
Private m_stxtMoneda As String
Private m_stxtAdj As String
Private m_stxtProveAdjudicado As String
Private m_stxtPresEsc As String
Private m_stxtCantIni As String
Private m_stxtCantFin As String
Private m_stxtPresUniEsc As String
Private m_stxtComentPrec As String
Private m_stxtRango As String
Private m_stxtCostesDesc As String
Private m_stxtAlPrecio As String
Private m_stxtAlTotal As String
Private m_stxtCAnul As String
Private m_stxtAnyoImput As String

'Listado Detallado
Private m_stxtTexto As String
Private m_stxtNum As String
Private m_stxtDuda As String
Private m_stxtInterno As String
Private m_stxtExterno As String
Private m_stxtLibre As String
Private m_stxtSelec As String
Private m_stxtPreferBajo As String
Private m_stxtPreferAlto As String
Private m_stxtObligatorioS As String
Private m_stxtObligatorioN As String
Private m_stxtTotalOfer As String
Private m_stxtTotalGrupo As String
Private m_stxtTotalItem As String
Private m_stxtUnitario As String
Private m_stxtAplicarS As String
Private m_stxtAplicarN As String
Private m_stxtAtributos As String
Private m_stxtListadoDetallado As String
Private m_stxtAdjuntos As String
Private m_stxtPresUni As String
Private m_stxtPresItem As String  'Presupuestos asignados
Private m_stxtAmb As String
Private m_stxtDestinos As String
Private m_stxtPagos As String
Private m_stxtProveActs As String
Private m_stxtSolicitud As String
Private sIdiOrigenGS As String
Private sIdiOrigenPORTAL As String
Private sIdiUSU As String
Private sIdiContacto As String
Private m_stxtGruAsig As String

Private OrdenListado As TipoOrdenacionProcesos

Private dequivalencia As Double
Private sMoneda As String
Private m_sNomIt As String '= m_stxtProce

Private sGMN1Cod As String
Private sGMN2Cod As String
Private sGMN3Cod As String
Private sGMN4Cod As String

Private lblMaterialesProce As String


'edu T94
Private m_AtributosEspecificacion As String
Private m_Denominacion As String
Private m_Valor As String

Private m_AtributosEspecificacionItem As String
Private m_DenominacionItem As String
Private m_ValorItem As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub chkIncAtrib_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If chkIncAtrib Then
    chkDescr.Enabled = True
Else
    chkDescr.Enabled = False
    chkDescr.Value = vbUnchecked
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "chkIncAtrib_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub cmdBorrarPresup1_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Me.lblPresup1.caption = ""

m_vPresup1_1 = Null
m_vPresup1_2 = Null
m_vPresup1_3 = Null
m_vPresup1_4 = Null
m_sUON1Pres1 = Null
m_sUON2Pres1 = Null
m_sUON3Pres1 = Null
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdBorrarPresup1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBorrarPresup2_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_vPresup2_1 = Null
m_vPresup2_2 = Null
m_vPresup2_3 = Null
m_vPresup2_4 = Null
m_sUON1Pres2 = Null
m_sUON2Pres2 = Null
m_sUON3Pres2 = Null

Me.lblPresup2.caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdBorrarPresup2_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrarPresup3_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Me.lblPresup3.caption = ""
        m_vPresup3_1 = Null
        m_vPresup3_2 = Null
        m_vPresup3_3 = Null
        m_vPresup3_4 = Null
        m_sUON1Pres3 = Null
        m_sUON2Pres3 = Null
        m_sUON3Pres3 = Null
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdBorrarPresup3_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBorrarPresup4_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Me.lblPresup4.caption = ""
m_vPresup4_1 = Null
m_vPresup4_2 = Null
m_vPresup4_3 = Null
m_vPresup4_4 = Null
m_sUON1Pres4 = Null
m_sUON2Pres4 = Null
m_sUON3Pres4 = Null
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdBorrarPresup4_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBorrarResp_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_sCodResp = Null
m_sUON1Resp = Null
m_sUON2Resp = Null
m_sUON3Resp = Null
m_sDepResp = Null
Me.lblResp.caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdBorrarResp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrarSolic_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblSolicCompras.caption = ""
    lblIdSolic.caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdBorrarSolic_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrarUsuAper_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_sCodPerAper = Null
m_sUON1Aper = Null
m_sUON2Aper = Null
m_sUON3Aper = Null
m_sDepAper = Null
Me.lblUsuAper.caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdBorrarUsuAper_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscarSolic_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSolicitudBuscar.g_sOrigen = "frmLstPROCE" & sOrigen
    frmSolicitudBuscar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdBuscarSolic_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFEcAdjDesde_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecAdjDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalFEcAdjDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecAdjHasta_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecAdjHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalFecAdjHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecNecDesde_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecNeceDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalFecNecDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeDesde_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecApeDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalFecApeDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeHasta_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecApeHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalFecApeHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalfecNecHasta_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecNeceHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalfecNecHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecPresDesde_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecPresDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalFecPresDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecPresHasta_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecPresHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalFecPresHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecUltReuDesde_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecUltReuDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdCalFecUltReuDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdcalFecUltReuHasta_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtFecUltReuHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdcalFecUltReuHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdComCalFecDesde_Click()
  
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtComDesde
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdComCalFecDesde_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdComCalFecHasta_Click()
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirFormCalendar Me, txtComHasta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdComCalFecHasta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

''' <summary>Obtiene el listado seleccionado en el filtro</summary>
''' <remarks>Llamada desde: Click del boton obtener; Tiempo m�ximo:0,1</remarks>
Private Sub cmdObtener_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
  
    If Not IsNumeric(txtCod.Text) And txtCod.Text <> "" Then
        oMensajes.NoValida m_stxtCod
        If Me.Visible Then txtCod.SetFocus
        Exit Sub
    End If
    
    If txtCod.Text <> "" Then
        If txtCod.Text > giMaxProceCod Then
            txtCod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido m_stxtCod
            Exit Sub
        End If
    End If
    
    If Not crs_Connected Then Exit Sub
    
    cmdObtener.Enabled = False
    
    If sdbcEqpDen.Text = "" And sdbcCompDen.Text = "" Then
        optComp.Value = False
        optResp.Value = False
    End If
    If bRAsig Then
        optComp = True
    ElseIf bRCompResponsable Then
        optResp = True
    End If
    
    dPresDesde = 0
    dPresHasta = 0
    If txtPresDesde.Text <> "" And IsNumeric(txtPresDesde.Text) Then dPresDesde = txtPresDesde.Text
    If txtPresHasta.Text <> "" And IsNumeric(txtPresHasta.Text) Then dPresHasta = txtPresHasta.Text
        
    Select Case sOrigen
        Case "A2B1", "frmPROCE"
            If Not optCiclo.Value Then
                If Not optDetallado.Value Then
                    If Not optResumido.Value Then
                        oMensajes.FaltanDatos sOpc
                    Else
                        ObtenerListadoAperturaResumido
                    End If
                Else
                    If Trim(txtCod.Text) <> "" Then
                         Select Case sdbcNiv.Text
                            Case m_stxtProceso
                                m_iNivel = 1
                                ObtenerListadoDetallado
                            Case m_stxtGrupo
                                m_iNivel = 2
                                ObtenerListadoDetallado
                            Case m_stxtItem
                                m_iNivel = 3
                                ObtenerListadoDetallado
                        End Select
                    Else
                        optResumido.Value = True
                        ObtenerListadoAperturaResumido
                    End If
                End If
            Else
                ObtenerListadoCicloVida
            End If
            
        Case "A2B2", "frmSELPROVE"
            ObtenerListadoSelProve

        Case "A2B3", "frmOFEPet"
            ObtenerListadoOFEPet

        Case "A2B4C2", "frmOFERec"
            ObtenerListadoOfeRec
        
        Case "A2B5"
            ObtenerListadoAdjudicacion

        Case "A6B3C1", "frmOFEHistWeb"
            ObtenerListadoOfeRecWeb
    End Select
    
    cmdObtener.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdObtener_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdSelMat_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELMAT.sOrigen = "frmLstPROCE" & sOrigen
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.bRCompResponsable = bRCompResponsable
    frmSELMAT.bRUsuAper = bRUsuAper
    frmSELMAT.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdSelMat_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdSelPresup1_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELPresAnuUON.sOrigen = "frmLstPROCE" & sOrigen
    frmSELPresAnuUON.g_iTipoPres = 1
    frmSELPresAnuUON.Show 1
            
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdSelPresup1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSelPresup2_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELPresAnuUON.sOrigen = "frmLstPROCE" & sOrigen
    frmSELPresAnuUON.g_iTipoPres = 2
    frmSELPresAnuUON.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdSelPresup2_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
            
End Sub

Private Sub cmdSelPresup3_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELPresUO.sOrigen = "frmLstPROCE" & sOrigen
    frmSELPresUO.g_iTipoPres = 3
    Screen.MousePointer = vbNormal
    frmSELPresUO.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdSelPresup3_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSelPresup4_Click()
            
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELPresUO.sOrigen = "frmLstPROCE" & sOrigen
    frmSELPresUO.g_iTipoPres = 4
    frmSELPresUO.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdSelPresup4_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSelProveBuscar_Click()
Dim bNuevoProce As Boolean
Dim oProces As CProcesos

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If basOptimizacion.gTipoDeUsuario <> Administrador Then
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
        Set frmPROVEBuscar.g_oProcesoSeleccionado = g_oProceSeleccionado
    End If
    
    If sOrigen = "frmSELPROVE" Then
        frmPROVEBuscar.bRMat = False
    Else
        frmPROVEBuscar.bRMat = bRMat Or bRestProvMatComp
    End If
    frmPROVEBuscar.sOrigen = "frmLstPROCE" & sOrigen
    
    frmPROVEBuscar.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdSelProveBuscar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSelResp_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
frmPROCEBusqPer.sOrigen = "frmLstPROCE" & sOrigen
frmPROCEBusqPer.giTipo = 2
frmPROCEBusqPer.bRUO = bRUsuUON
frmPROCEBusqPer.bRDep = bRUsuDep
frmPROCEBusqPer.Show 1

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdSelResp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSelUsuAper_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
frmPROCEBusqPer.sOrigen = "frmLstPROCE" & sOrigen
frmPROCEBusqPer.giTipo = 1
frmPROCEBusqPer.bRUO = bRUsuUON
frmPROCEBusqPer.bRDep = bRUsuDep
frmPROCEBusqPer.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "cmdSelUsuAper_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If


    Select Case sOrigen
        
        Case "frmPROCE", "A2B1"
                
            sdbcEst.Enabled = True
            DesdeEst = sinitems
            HastaEst = Cerrado

        Case "frmSELPROVE", "A2B2"
            
            DesdeEst = validado
            HastaEst = Cerrado
            sdbcEst.Enabled = True
        
        Case "frmOFEPet", "A2B3"
            
            DesdeEst = conasignacionvalida
            HastaEst = Cerrado
            sdbcEst.Enabled = True
        
        Case "frmOFERec", "A2B4C2"
            
            DesdeEst = conasignacionvalida
            HastaEst = Cerrado
            sdbcEst.Enabled = True
            
        Case "A2B5"
            
            DesdeEst = ParcialmenteCerrado
            HastaEst = Cerrado
            sdbcEst.Enabled = True
        
            
        Case "frmOFEHistWeb", "A6B3C1"
            
            DesdeEst = conofertas
            HastaEst = Cerrado
            sdbcEst.Enabled = True
    
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    m_bActivado = True
    If g_bEsInvitado Then
        sdbcAnyo.Enabled = False
        sdbcGMN1_4Cod.Enabled = False
        txtCod.Locked = True
        bRMat = False
        bRAsig = False
        bRCompResponsable = False
        bREqpAsig = False
        bRUsuAper = False
        bRUsuUON = False
        bRUsuDep = False
        bRUO = False
        m_bProveAsigEqp = False
        m_bProveAsigComp = False
        bSoloAdjDir = False
        bSoloAdjReu = False
    Else
        sdbcAnyo.Enabled = True
        sdbcGMN1_4Cod.Enabled = True
        txtCod.Locked = False
        ConfigurarSeguridad (sOrigen)
    End If
    
    PonerFieldSeparator Me
    
    ConfigurarSolicitud
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas

    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
        
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2 + 425
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
'    m_bProveAsigComp = False
'    m_bProveAsigEqp = False
    
    CargarRecursos
    
    

    sstabProce.TabVisible(2) = False
    sstabProce.TabVisible(3) = False
    sstabProce.TabVisible(4) = False
    sstabProce.TabVisible(5) = False
    sstabProce.TabVisible(6) = False
    

    If gParametrosGenerales.gbSubasta Then
        chkSubasta.Visible = True
        
    ElseIf Not gParametrosGenerales.gbSubasta Then
        chkSubasta.Visible = False
        chkSubasta.Value = vbUnchecked
    End If
    
    'Si no se usan los presupuestos no se ven sus controles
    If Not gParametrosGenerales.gbUsarPres1 And Not gParametrosGenerales.gbUsarPres2 And Not gParametrosGenerales.gbUsarPres3 And Not gParametrosGenerales.gbUsarPres4 Then
        fraPresupuestos.Visible = False
        fraUsuAper.Top = fraPresupuestos.Top
        fraResp.Top = fraUsuAper.Top + fraUsuAper.Height + 75
        Frame3.Top = fraResp.Top + fraResp.Height + 75
    Else
        Dim dblfraPesH As Double
        dblfraPesH = fraPresupuestos.Height / 4
        fraPresupuestos.Visible = True
        If Not gParametrosGenerales.gbUsarPres1 Then
            lblPresup(0).Visible = False
            lblPresup1.Visible = False
            cmdBorrarPresup1.Visible = False
            cmdSelPresup1.Visible = False
            lblPresup(3).Top = lblPresup(2).Top
            lblPresup4.Top = lblPresup3.Top
            cmdBorrarPresup4.Top = cmdBorrarPresup3.Top
            cmdSelPresup4.Top = cmdSelPresup3.Top
            lblPresup(2).Top = lblPresup(1).Top
            lblPresup3.Top = lblPresup2.Top
            cmdBorrarPresup3.Top = cmdBorrarPresup2.Top
            cmdSelPresup3.Top = cmdSelPresup2.Top
            lblPresup(1).Top = lblPresup(0).Top
            lblPresup2.Top = lblPresup1.Top
            cmdBorrarPresup2.Top = cmdBorrarPresup1.Top
            cmdSelPresup2.Top = cmdSelPresup1.Top
            fraPresupuestos.Height = fraPresupuestos.Height - dblfraPesH
        End If
        If Not gParametrosGenerales.gbUsarPres2 Then
            lblPresup(1).Visible = False
            lblPresup2.Visible = False
            cmdBorrarPresup2.Visible = False
            cmdSelPresup2.Visible = False
            lblPresup(3).Top = lblPresup(2).Top
            lblPresup4.Top = lblPresup3.Top
            cmdBorrarPresup4.Top = cmdBorrarPresup3.Top
            cmdSelPresup4.Top = cmdSelPresup3.Top
            lblPresup(2).Top = lblPresup(1).Top
            lblPresup3.Top = lblPresup2.Top
            cmdBorrarPresup3.Top = cmdBorrarPresup2.Top
            cmdSelPresup3.Top = cmdSelPresup2.Top
            fraPresupuestos.Height = fraPresupuestos.Height - dblfraPesH
        End If
        If Not gParametrosGenerales.gbUsarPres3 Then
            lblPresup(2).Visible = False
            lblPresup3.Visible = False
            cmdBorrarPresup3.Visible = False
            cmdSelPresup3.Visible = False
            lblPresup(3).Top = lblPresup(2).Top
            lblPresup4.Top = lblPresup3.Top
            cmdBorrarPresup4.Top = cmdBorrarPresup3.Top
            cmdSelPresup4.Top = cmdSelPresup3.Top
            fraPresupuestos.Height = fraPresupuestos.Height - dblfraPesH
        End If
        If Not gParametrosGenerales.gbUsarPres4 Then
            lblPresup(2).Visible = False
            lblPresup3.Visible = False
            cmdBorrarPresup3.Visible = False
            cmdSelPresup3.Visible = False
            fraPresupuestos.Height = fraPresupuestos.Height - dblfraPesH
        End If
        If Not gParametrosGenerales.gbUsarPres1 Or Not gParametrosGenerales.gbUsarPres2 Or Not gParametrosGenerales.gbUsarPres3 Or Not gParametrosGenerales.gbUsarPres4 Then
            fraPresupuestos.Height = fraPresupuestos.Height + 75
            fraUsuAper.Top = fraPresupuestos.Top + fraPresupuestos.Height + 75
            fraResp.Top = fraUsuAper.Top + fraUsuAper.Height + 75
            Frame3.Top = fraResp.Top + fraResp.Height + 75
        End If
        
    End If
    
    If Not bPermProcMultiMaterial Then
        lblGMN1Mat.Visible = False
        sdbcGMN1_4Cod.Visible = False
        sdbcGMN1_4Den.Visible = False
        lblGMN2_4.Top = lblGMN1_4.Top
        sdbcGMN2_4Cod.Top = sdbcGMN1_4Cod.Top
        sdbcGMN2_4Den.Top = sdbcGMN2_4Cod.Top
        lblGMN3_4.Top = lblGMN2_4.Top + lblGMN2_4.Height + 75
        sdbcGMN3_4Cod.Top = sdbcGMN2_4Cod.Top + sdbcGMN2_4Cod.Height + 50
        sdbcGMN3_4Den.Top = sdbcGMN3_4Cod.Top
        lblGMN4_4.Top = lblGMN3_4.Top + lblGMN3_4.Height + 75
        sdbcGMN4_4Cod.Top = sdbcGMN3_4Cod.Top + sdbcGMN3_4Cod.Height + 50
        sdbcGMN4_4Den.Top = sdbcGMN4_4Cod.Top
        fraMAT.Height = 1375
        fraArt.Top = fraMAT.Top + fraMAT.Height + 50
        fraProve.Top = fraArt.Top + fraArt.Height + 50
        sstabProce.Height = 7930
    Else
        sdbcGMN1_4Cod.Visible = True
        sdbcGMN1_4Den.Visible = True
        lblGMN1_4.Visible = True
        sdbcGMN2_4Cod.Top = sdbcGMN1_4Cod.Top + sdbcGMN1_4Cod.Height + 50
        sdbcGMN2_4Den.Top = sdbcGMN2_4Cod.Top
        lblGMN3_4.Top = lblGMN2_4.Top + lblGMN2_4.Height + 50
        sdbcGMN3_4Cod.Top = sdbcGMN2_4Cod.Top + sdbcGMN2_4Cod.Height + 50
        sdbcGMN3_4Den.Top = sdbcGMN3_4Cod.Top
        lblGMN4_4.Top = lblGMN3_4.Top + lblGMN3_4.Height + 50
        sdbcGMN4_4Cod.Top = sdbcGMN3_4Cod.Top + sdbcGMN3_4Cod.Height + 50
        sdbcGMN4_4Den.Top = sdbcGMN4_4Cod.Top
        fraMAT.Height = 1710
        fraArt.Top = fraMAT.Top + fraMAT.Height + 50
        fraProve.Top = fraArt.Top + fraArt.Height + 50
    End If
    
    'Comprueba si se visualizar�n o no las solicitudes de compras
    If FSEPConf Then
        lblSolicCompras.Visible = False
        lblSolicitud.Visible = False
        cmdBorrarSolic.Visible = False
        cmdBuscarSolic.Visible = False
    Else
        If basParametros.gParametrosGenerales.gbSolicitudesCompras = False Then
            lblSolicCompras.Visible = False
            lblSolicitud.Visible = False
            cmdBorrarSolic.Visible = False
            cmdBuscarSolic.Visible = False
        End If
    End If
    'Si no es el usuario administrador
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        'Si no tiene permisos de consulta de las solicitudes no podr� visualizarlas
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APESolicAsignar)) Is Nothing) Then
            lblSolicCompras.Visible = False
            lblSolicitud.Visible = False
            cmdBorrarSolic.Visible = False
            cmdBuscarSolic.Visible = False
        End If
    End If
    

   If bRAsig Or bRCompResponsable Then
        sdbcEqpDen.AddItem oUsuarioSummit.comprador.DenEqp & Chr(m_lSeparador) & basOptimizacion.gCodEqpUsuario
        
        Set oEqps = oFSGSRaiz.Generar_CEquipos
        
        oEqps.Add basOptimizacion.gCodEqpUsuario, oUsuarioSummit.comprador.DenEqp
        Set oEqpSeleccionado = oEqps.Item(1)
        
        Set oEqpSeleccionado.Compradores = oFSGSRaiz.generar_CCompradores
        oEqpSeleccionado.Compradores.Add oEqpSeleccionado.Cod, oUsuarioSummit.comprador.DenEqp, basOptimizacion.gCodCompradorUsuario, oUsuarioSummit.comprador.nombre, oUsuarioSummit.comprador.Apel, "", "", ""
        
        Set oComps = oEqpSeleccionado.Compradores
        Set oCompSeleccionado = oComps.Item(1)
        sdbcCompDen.AddItem oUsuarioSummit.comprador.Apel & ", " & oUsuarioSummit.comprador.nombre & Chr(m_lSeparador) & basOptimizacion.gCodCompradorUsuario
        

    
    End If

        
    lblReferencia.caption = gParametrosGenerales.gsDenSolicitudCompra & ":"
    
    
    Select Case sOrigen
    
        
        Case "frmSELPROVE", "A2B2"
        
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 2
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
        
        Case "frmOFEPet", "A2B3"
             
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
            
        Case "frmOFERec", "A2B4C2", "frmOFEHistWeb", "A6B3C1"
            
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
                                                
        Case "A2B5"
            
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
        
        Case Else
            
            sdbcEst.RemoveAll
            sdbcEst.AddItem sIdiEst(1) & Chr(m_lSeparador) & 1
            sdbcEst.AddItem sIdiEst(2) & Chr(m_lSeparador) & 2
            sdbcEst.AddItem sIdiEst(3) & Chr(m_lSeparador) & 3
            sdbcEst.AddItem sIdiEst(4) & Chr(m_lSeparador) & 4
            sdbcEst.AddItem sIdiEst(5) & Chr(m_lSeparador) & 5
            sdbcEst.AddItem sIdiEst(6) & Chr(m_lSeparador) & 6
            sdbcEst.AddItem sIdiEst(7) & Chr(m_lSeparador) & 7
            sdbcEst.AddItem sIdiEst(8) & Chr(m_lSeparador) & 8
            sdbcEst.AddItem sIdiEst(9) & Chr(m_lSeparador) & 9
            sdbcEst.AddItem sIdiEst(10) & Chr(m_lSeparador) & 10
    
    End Select
    
    CargarAnyos
    
    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN1Mat.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"
    
    Me.lblPresup(0).caption = gParametrosGenerales.gsPlurPres1
    Me.lblPresup(1).caption = gParametrosGenerales.gsPlurPres2
    Me.lblPresup(2).caption = gParametrosGenerales.gsPlurPres3
    Me.lblPresup(3).caption = gParametrosGenerales.gsPlurPres4
    
    Select Case sOrigen
        
       Case "A2B1", "frmPROCE"   ' Datos de apertura
            Me.caption = sTituloI & " - " & sTitulo(1) & " (" & sOpc & ")"
            sstabProce.TabVisible(2) = True
            CargarComboOrden
            If sOrigen = "frmPROCE" Then
                If Not frmPROCE.g_oProcesoSeleccionado Is Nothing Then
                    sdbcMonCodigo.Text = frmPROCE.g_oProcesoSeleccionado.MonCod
                    sdbcMonCodigo_Validate False
                End If
            Else
                sdbcMonCodigo.Text = basPublic.gParametrosInstalacion.gsMoneda
                sdbcMonCodigo_Validate False
            End If
            DoEvents
        
        Case "A2B2", "frmSELPROVE" ' Proveedores seleccionados
            Me.caption = sTituloI & " - " & sTitulo(2) & " (" & sOpc & ")"
            sstabProce.TabVisible(3) = True
            fraProve.caption = sFilPro
            If gParametrosGenerales.gbProveGrupos Then
                fraSelProve1.Height = 2265
                chkSelProveGrupos.Visible = True
            Else
                fraSelProve1.Height = 1725
                chkSelProveGrupos.Visible = False
            End If
            
        Case "A2B3", "frmOFEPet"      ' Comunicaciones con proveedores
            Me.caption = sTituloI & " - " & sTitulo(3) & " (" & sOpc & ")"
            sstabProce.TabVisible(4) = True
            If basParametros.gParametrosGenerales.giINSTWEB = SinWeb Then
                chkComPubWeb.Value = vbUnchecked
                chkComPubWeb.Visible = False
            End If
       
         Case "A2B4C2", "frmOFERec" ' Ofertas por proceso
            Me.caption = sTituloI & " - " & sTitulo(4) & " (" & sOpc & ")"
            sstabProce.TabVisible(5) = True
            chkOfeRecUltProve.Value = vbChecked
            chkOfeRecItems.Value = vbChecked
            If sOrigen = "frmOFERec" Then
                If Not g_oProceSeleccionado Is Nothing And Not g_oProveSeleccionado Is Nothing Then
                    bRespetarComboProve = True
                    sdbcProveCod.Text = g_oProveSeleccionado.Cod
                    sdbcProveDen.Text = g_oProveSeleccionado.Den
                    bRespetarComboProve = False
                End If
            End If
            
         Case "A2B5" ' Comparativas y adjudicaciones
            Me.caption = sTitulo(5) & " (" & sOpc & ")"
            'si se ense�an s�lo procesos cerrados no se muestran los frames de �tems y grupos abiertos/cerrados
            Frame11.Height = 3000
            opCompPorProve.Top = 600
            opCompPorItem.Top = 1300
            sdbcNivDet.Value = m_stxtProceso
            Frame12.Top = 3700
            Frame12.Height = 1000
            sstabProce.TabVisible(6) = True
            If bSoloAdjDir Then
                chkAdjDir.Value = vbChecked
                chkAdjReu.Value = vbUnchecked
                chkAdjDir.Visible = False
                chkAdjReu.Visible = False
            ElseIf bSoloAdjReu Then
                chkAdjReu.Value = vbChecked
                chkAdjDir.Value = vbUnchecked
                chkAdjReu.Visible = False
                chkAdjDir.Visible = False
            End If
        Case "A6B3C1", "frmOFEHistWeb" ' Ofertas recibidas
            Me.caption = sTitulo(6) & " (" & sOpc & ")"
            sstabProce.TabVisible(5) = True
            chkOfeRecUltProve.Value = vbChecked
            chkOfeRecItems.Value = vbChecked
            chkOfeRecSin.Enabled = False
            chkOfeRecUltItem.Enabled = False
            optOfeRecItem.Enabled = False
            chkOfeRecAdjuntos.Enabled = False
            chkOfeRecAtributos.Enabled = False

         Case Else
            
             chkAdjReu.Enabled = True
             chkAdjDir.Enabled = True
            
     End Select

    'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And bRMat Then
        CargarGMN1Automaticamente
    End If
    InicializarVariables
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub InicializarVariables()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_sCodPerAper = Null
m_sUON1Aper = Null
m_sUON2Aper = Null
m_sUON3Aper = Null
m_sDepAper = Null
m_sCodResp = Null
m_sUON1Resp = Null
m_sUON2Resp = Null
m_sUON3Resp = Null
m_sDepResp = Null

m_vPresup1_1 = Null
m_vPresup1_2 = Null
m_vPresup1_3 = Null
m_vPresup1_4 = Null
m_vPresup2_1 = Null
m_vPresup2_2 = Null
m_vPresup2_3 = Null
m_vPresup2_4 = Null
m_vPresup3_1 = Null
m_vPresup3_2 = Null
m_vPresup3_3 = Null
m_vPresup3_4 = Null
m_vPresup4_1 = Null
m_vPresup4_2 = Null
m_vPresup4_3 = Null
m_vPresup4_4 = Null
m_sUON1Pres1 = Null
m_sUON2Pres1 = Null
m_sUON3Pres1 = Null
m_sUON1Pres2 = Null
m_sUON2Pres2 = Null
m_sUON3Pres2 = Null
m_sUON1Pres3 = Null
m_sUON2Pres3 = Null
m_sUON3Pres3 = Null
m_sUON1Pres4 = Null
m_sUON2Pres4 = Null
m_sUON3Pres4 = Null
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "InicializarVariables", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcCodArticulo_Change()
         
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboArt Then
    
        bRespetarComboArt = True
        txtDenArticulo.Text = ""
        bRespetarComboArt = False
        
        If sdbcCodArticulo.Value = "" Then
            bCargarComboDesde = False
        Else
            bCargarComboDesde = True
        End If
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCodArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcCodArticulo_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCodArticulo.Value = "..." Then
        sdbcCodArticulo.Text = ""
        Exit Sub
    End If
    
    bRespetarComboArt = True
    sdbcCodArticulo.Text = sdbcCodArticulo.Columns(0).Text
    txtDenArticulo.Text = sdbcCodArticulo.Columns(1).Text
    bRespetarComboArt = False
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCodArticulo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcCodArticulo_DropDown()

Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim sGMN1Cod As String
Dim sGMN2Cod As String
Dim sGMN3Cod As String
Dim sGMN4Cod As String
       
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCodArticulo.RemoveAll
    sdbcCodArticulo.Columns(0).Value = ""
    sdbcCodArticulo.Columns(1).Value = ""
    Set oArticulos = Nothing
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
               
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
    Else
        sGMN1Cod = ""
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
    Else
        sGMN2Cod = ""
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
    Else
        sGMN3Cod = ""
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
    Else
        sGMN4Cod = ""
    End If
       
        If bRMat Then
            If oGMN4Seleccionado Is Nothing And sdbcCodArticulo.Text = "" Then Exit Sub
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, False, Trim(sdbcCodArticulo), , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
        Else
            If oGMN4Seleccionado Is Nothing Then
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, , False, Trim(sdbcCodArticulo.Text)
            Else
                oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcCodArticulo), , , , , , False
               Set oArticulos = oGMN4Seleccionado.ARTICULOS
            End If
        End If
              
        Codigos = oArticulos.DevolverLosCodigos
           
        For i = 0 To UBound(Codigos.Cod) - 1
           
            sdbcCodArticulo.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)

        Next
        
        If Not oArticulos.EOF Then
            sdbcCodArticulo.AddItem "..."
        End If
        sdbcCodArticulo.SelStart = 0
        sdbcCodArticulo.SelLength = Len(sdbcCodArticulo.Text)
        sdbcCodArticulo.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCodArticulo_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
      
End Sub

Private Sub sdbcCodArticulo_InitColumnProps()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCodArticulo.DataFieldList = "Column 0"
    sdbcCodArticulo.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCodArticulo_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcCodArticulo_Validate(Cancel As Boolean)
    
    Dim oArts As CArticulos
    Dim bExiste As Boolean
Dim sGMN1Cod As String
Dim sGMN2Cod As String
Dim sGMN3Cod As String
Dim sGMN4Cod As String
       
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCodArticulo.Text = "" Then Exit Sub
        
    
    sdbcCodArticulo.RemoveAll
    Set oArts = Nothing
    Set oArts = oFSGSRaiz.Generar_CArticulos
           
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
    Else
        sGMN1Cod = ""
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
    Else
        sGMN2Cod = ""
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
    Else
        sGMN3Cod = ""
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
    Else
        sGMN4Cod = ""
    End If
    
    
    ''' Solo continuamos si existe el grupo
    If bRMat Then
        oArts.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, False, Trim(sdbcCodArticulo), , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        If oGMN4Seleccionado Is Nothing Then
            oArts.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, sGMN1Cod, sGMN2Cod, sGMN3Cod, , False, Trim(sdbcCodArticulo.Text)
        Else
            oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcCodArticulo), , , , , , False
            Set oArts = oGMN4Seleccionado.ARTICULOS
        End If
    End If
    
    bExiste = Not (oArts.Count = 0)
        
    
    If Not bExiste Then
        oMensajes.NoValido 21
        sdbcCodArticulo.Text = ""
        txtDenArticulo = ""
    Else
        sdbcCodArticulo.Columns(0).Text = sdbcCodArticulo.Text
    End If
    
    Set oArts = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCodArticulo_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Sub sdbcDestDenProce_Validate(Cancel As Boolean)

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestDenProce.Text = "" Then Exit Sub
    
    
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el destino
    If bRDest Then
         m_oDestinos.CargarTodosLosDestinosUON oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , UCase(CStr(sdbcDestDenProce.Value)), True
    Else
         m_oDestinos.CargarTodosLosDestinos , UCase(CStr(sdbcDestDenProce.Value)), True, , , , , , , , True
    End If
    
    If m_oDestinos.Count = 0 Then
        sdbcDestCodProce.Value = ""
        Screen.MousePointer = vbNormal
    Else
        m_bDestRespetarCombo = True
        sdbcDestCodProce.Value = m_oDestinos.Item(1).Cod

        sdbcDestCodProce.Columns(0).Value = sdbcDestCodProce.Value
        sdbcDestCodProce.Columns(1).Value = sdbcDestDenProce.Value
        sdbcDestDenProce.Columns(1).Value = sdbcDestCodProce.Value
        sdbcDestDenProce.Columns(0).Value = sdbcDestDenProce.Value

        m_bDestRespetarCombo = False
    End If

    Screen.MousePointer = vbNormal

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestDenProce_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEst_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboEst Then
      
        bRespetarComboEst = False
        sdbcEst.Text = ""
        bRespetarComboEst = True
           
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEst_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub



Private Sub sdbcEst_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEst.Value = "..." Or Trim(sdbcEst.Value) = "" Then
        sdbcEst.Text = ""
        Exit Sub
    End If
    
    bRespetarComboEst = True
    sdbcEst.Text = sdbcEst.Columns(0).Text
    bRespetarComboEst = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEst_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcEst_InitColumnProps()
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcEst.DataFieldList = "Column 0"
    sdbcEst.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEst_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub



Private Sub sdbcEst_Validate(Cancel As Boolean)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEst.Text <> "" Then
        sdbcEst.Text = sdbcEst.Columns(0).Text
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEst_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub



Private Sub sdbcGMN1_4Den_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN1RespetarCombo Then

        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Den_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1_4Den_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN1_4Den.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Den_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Den.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Den_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1_4Den_DropDown()
 Dim udtCodigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1_4Den.RemoveAll

    ''If oGMN1Seleccionado Is Nothing Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    Screen.MousePointer = vbNormal
    
    udtCodigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
    Set oIMAsig = Nothing
    Set oGruposMN1 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Den_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Den_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN1Proce_4Cod_Change()
               
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticuloSeleccionado = Nothing
    
    If Not GMN1RespetarCombo Then
        GMN1RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN1_4Den.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1Proce_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN1Proce_4Cod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN1Proce_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1Proce_4Cod.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    
    
    GMN1RespetarCombo = False
    If Not bPermProcMultiMaterial Then
        GMN1Seleccionado
        sdbcGMN1Proce_4Cod.Text = sdbcGMN1Proce_4Cod.Columns(0).Text
    End If
    
    GMN1CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1Proce_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN1Proce_4Cod_DropDown()
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1Proce_4Cod.RemoveAll
        
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
 
    Screen.MousePointer = vbHourglass
    

    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
      
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1Proce_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1Proce_4Cod), , , False
    End If
    
    bExiste = Not (oGruposMN1.Count = 0)
    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1Proce_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not oGruposMN1.EOF Then
        sdbcGMN1Proce_4Cod.AddItem "..."
    End If

    sdbcGMN1Proce_4Cod.SelStart = 0
    sdbcGMN1Proce_4Cod.SelLength = Len(sdbcGMN1Proce_4Cod.Text)
    sdbcGMN1Proce_4Cod.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1Proce_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub

Private Sub sdbcGMN1Proce_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1Proce_4Cod, Text
End Sub

Public Sub sdbcGMN1Proce_4Cod_Validate(Cancel As Boolean)
    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1Proce_4Cod.Value = "" Then Exit Sub

    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        If IsNumeric(sdbcAnyo.Value) Then
            Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1Proce_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1Proce_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, True)
        End If
    Else
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1Proce_4Cod.Text, , True, , False
    End If
    
    Screen.MousePointer = vbNormal
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        sdbcGMN1Proce_4Cod.Text = ""
        sdbcGMN1Proce_4Cod.Text = ""
    Else
        sdbcGMN1Proce_4Cod.Columns(0).Value = sdbcGMN1Proce_4Cod.Text
        If Not bPermProcMultiMaterial Then
            sdbcGMN1_4Cod.Text = sdbcGMN1Proce_4Cod.Text
            GMN1RespetarCombo = False
            GMN1Seleccionado
            GMN1CargarComboDesde = False
        End If

        
    End If
    
    Set oGMN1s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1Proce_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
 
End Sub

Private Sub sdbcGMN2_4Cod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN2_4Cod.DataFieldList = "Column 0"
    sdbcGMN2_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)
    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    ''' Solo continuamos si existe el grupo
    If Not oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, , Trim(sdbcGMN2_4Den.Text), True, , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2s.Item(1).Cod
        
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
                    
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Den_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN3_4Cod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN3_4Cod.DataFieldList = "Column 0"
    sdbcGMN3_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False)
        Else
            
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
   Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3s.Item(1).Cod
        
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
                    
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Den_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN4_4Den.Text = "" Then Exit Sub
     
     Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
           
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
                    
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Den_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcGMN4_4Cod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN4_4Cod.DataFieldList = "Column 0"
    sdbcGMN4_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcMonCod_Change()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not bRespetarCombo Then
        
        bRespetarCombo = False
        sdbcMonDen.Text = ""
        bRespetarCombo = True
        
        bCargarComboDesde = True
       
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcMonCod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcMonCod.Value = "..." Or Trim(sdbcMonCod.Value) = "" Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDen.Text = sdbcMonCod.Columns(1).Text
    sdbcMonCod.Text = sdbcMonCod.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcMonCod_DropDown()

Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim sDesde As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bCargarComboDesde Then
        sDesde = sdbcMonCod.Value
    Else
        sDesde = ""
    End If
    
    oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, sDesde, , , True
        
    Codigos = oMonedas.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcMonCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMonCod.DataFieldList = "Column 0"
    sdbcMonCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcMonCod_PositionList(ByVal Text As String)
PositionList sdbcMonCod, Text
End Sub

Private Sub sdbcMonCod_Validate(Cancel As Boolean)


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Trim(sdbcMonCod.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcMonCod.Value)) = UCase(Trim(sdbcMonCod.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonDen = sdbcMonCod.Columns(1).Value
        bRespetarCombo = False
       
        Exit Sub
    End If
    
    
    oMonedas.CargarTodasLasMonedasDesde 1, sdbcMonCod.Text, , , True, , True
    
    If oMonedas.Item(1) Is Nothing Then
        oMensajes.NoValida sIdiMoneda
        sdbcMonCod = ""
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Cod) <> UCase(sdbcMonCod.Value) Then
            oMensajes.NoValida sIdiMoneda
            sdbcMonCod = ""
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonDen = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sdbcMonCod.Columns(0).Value = sdbcMonCod.Text
            sdbcMonCod.Columns(1).Value = sdbcMonDen.Text
            bRespetarCombo = False
           
        End If
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonden_Change()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcMonCod.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
       
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonden_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcMonDen_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcMonDen.Value = "..." Or Trim(sdbcMonDen.Value) = "" Then
        sdbcMonDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDen.Text = sdbcMonDen.Columns(0).Text
    sdbcMonCod.Text = sdbcMonDen.Columns(1).Text
    bRespetarCombo = False
   
    bCargarComboDesde = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcMonDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim sDesde As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If bCargarComboDesde Then
    sDesde = sdbcMonDen.Value
Else
    sDesde = ""
End If

    
    oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , sDesde, True, True
        
    Codigos = oMonedas.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcMonDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMonDen.DataFieldList = "Column 0"
    sdbcMonDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcMonDen_PositionList(ByVal Text As String)
PositionList sdbcMonDen, Text
End Sub

Private Sub sdbcMonDen_Validate(Cancel As Boolean)


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Trim(sdbcMonDen.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcMonDen.Value)) = UCase(Trim(sdbcMonDen.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonCod.Text = sdbcMonDen.Columns(1).Value
        bRespetarCombo = False
        Exit Sub
    End If
    
    
    oMonedas.CargarTodasLasMonedasDesde 1, , sdbcMonDen.Text, True, True, , True
    
    If oMonedas.Item(1) Is Nothing Then
        oMensajes.NoValida sIdiMoneda
        sdbcMonDen.Text = ""
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) <> UCase(sdbcMonDen.Value) Then
            oMensajes.NoValida sIdiMoneda
            sdbcMonDen.Text = ""
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonCod.Text = oMonedas.Item(1).Cod
            sdbcMonDen.Columns(0).Value = sdbcMonDen.Text
            sdbcMonDen.Columns(1).Value = sdbcMonCod.Text
            bRespetarCombo = False
           
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN1_4Cod_Change()
           
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticuloSeleccionado = Nothing
    
    If Not GMN1RespetarCombo Then
        GMN1RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN1_4Den.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    GMN1RespetarCombo = False
     
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo

Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1_4Cod.RemoveAll
        
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
 
    Screen.MousePointer = vbHourglass
    
    If GMN1CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
    
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub
Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
Dim oGMN1s As CGruposMatNivel1
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    sGMN1Cod = sdbcGMN1_4Cod.Text
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
    Else
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sGMN1Cod = sdbcGMN1_4Cod.Text
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        
        GMN1RespetarCombo = False
        GMN1Seleccionado
        GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN1_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub
Private Sub sdbcGMN2_4Cod_Change()
       
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        
        GMN2CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Public Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)
Dim oGMN2s As CGruposMatNivel2
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    
    If Not oGMN1Seleccionado Is Nothing Then
        
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN2_4Cod.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN2 = Nothing
        
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        End If
    Else
       
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh

    Set oIMAsig = Nothing
    Set oGruposMN2 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN2RespetarCombo Then

        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Den_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Den.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Den_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass

    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN2 = Nothing
        
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , False)
        End If
    
    Else
        
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN2 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Den_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub
Private Sub sdbcGMN2_4Den_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN2_4Den.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Den_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN2_4Den_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN3_4Cod_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Cod_CloseUp()
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN3_4Cod.RemoveAll

    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN3 = Nothing
        
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        End If
    Else
       
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN3 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub
Private Sub sdbcGMN3_4Cod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN3_4Cod.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Cod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Public Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)
Dim oGMN3s As CGruposMatNivel3
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
    Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGMN3_4den_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN3RespetarCombo Then

        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4den_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Den_Click()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN3_4Den.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Den_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Den.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Den_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN3_4Den.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN3 = Nothing
        
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        End If
    
    Else
             
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    Screen.MousePointer = vbNormal

    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN3 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Den_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN3_4Den_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN4RespetarCombo Then
    
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcGMN4_4Cod_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN4_4Cod.RemoveAll

    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN4 = Nothing
        
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        End If
    
    Else
       
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN4_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN4_4Cod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcGMN4_4Cod.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Cod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Public Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)
Dim oGMN4s As CGruposMatNivel4
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Set oIMAsig = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub sdbcGMN4_4Den_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not GMN4RespetarCombo Then

        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Den_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Den_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()
Dim Codigos As TipoDatosCombo

Dim oIMAsig As IMaterialAsignado
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN4_4Den.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN4 = Nothing
        
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False, bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        End If
    Else
             
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    Screen.MousePointer = vbNormal
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Set oIMAsig = Nothing
    Set oGruposMN4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Den_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN4_4Den_InitColumnProps()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcGMN4_4Den_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub


Private Function Cargar() As TipoBusquedaProcesos

Dim udtBusqueda As TipoBusquedaProcesos

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEst <> "" Then
            Select Case sdbcEst.Columns("COD").Value
        
                Case 1
                    'Pendiente de validar Apertura
                    DesdeEst = sinitems
                    HastaEst = ConItemsSinValidar
                
                Case 2
                    'Pendiente de asignar proveedores
                    DesdeEst = validado
                    HastaEst = Conproveasignados
                    
                Case 3
                    'Pendiente de enviar peticiones
                    DesdeEst = conasignacionvalida
                    HastaEst = conasignacionvalida
                    
                Case 4
                    ' en rec ofertas
                    If sOrigen = "frmOFEHistWeb" Then
                        DesdeEst = conofertas
                        HastaEst = conofertas
                    Else
                        DesdeEst = conpeticiones
                        HastaEst = conofertas
                    End If
                                   
                Case 5
                    'pendiente de comunicar objetivos
                    DesdeEst = ConObjetivosSinNotificar
                    HastaEst = ConObjetivosSinNotificarYPreadjudicado
                    
                Case 6
                    'Pendiente de adjudicar
                    DesdeEst = ConObjetivosSinNotificarYPreadjudicado
                    HastaEst = PreadjYConObjNotificados
                Case 7
                    'Parcialmente cerrado
                    DesdeEst = ParcialmenteCerrado
                    HastaEst = ParcialmenteCerrado
                Case 8
                    'Pendiente de notificar adjudicaciones
                    DesdeEst = conadjudicaciones
                    HastaEst = conadjudicaciones
                Case 9
                    'Adjudicado y notificado
                    DesdeEst = ConAdjudicacionesNotificadas
                    HastaEst = ConAdjudicacionesNotificadas
                Case 10
                    'Anulado
                    DesdeEst = Cerrado
                    HastaEst = Cerrado
            
            End Select
    
    Else
        If sdbcAnyo.Text <> "" And txtCod.Text <> "" And sdbcGMN1Proce_4Cod.Text <> "" Then
            DesdeEst = sinitems
            HastaEst = Cerrado
        Else
        
            Select Case sOrigen
            
                
                Case "frmSELPROVE", "A2B2"
                            
                    DesdeEst = validado
                    HastaEst = ConAdjudicacionesNotificadas
                       
                Case "frmOFEPet", "A2B3"
                
                    DesdeEst = conasignacionvalida
                    HastaEst = ConAdjudicacionesNotificadas
                     
                Case "frmOFERec", "A2B4C2"
                    
                    DesdeEst = conasignacionvalida
                    HastaEst = ConAdjudicacionesNotificadas
                    
                Case "frmOFEHistWeb", "A6B3C1"
                           
                    DesdeEst = conofertas
                    HastaEst = ConAdjudicacionesNotificadas
                
                Case "A2B5"
                           
                    DesdeEst = ParcialmenteCerrado
                    HastaEst = ConAdjudicacionesNotificadas
                
                Case Else
                    
                    DesdeEst = sinitems
                    HastaEst = Cerrado
                                
            End Select
        End If
    End If
    
    If sdbcGMN1_4Cod.Text <> "" And sGMN1Cod = "" Then
        sdbcGMN1_4Cod_Validate False
    End If
    
    If sdbcAnyo.Text = "" Then
        iAnio = 0
    Else
        iAnio = val(sdbcAnyo.Text)
    End If
    
    With udtBusqueda
        .DesdeFechaApertura = txtFecApeDesde
        .HastaFechaApertura = txtFecApeHasta
        .DesdeFechaNecesidad = txtFecNeceDesde
        .HastaFechaNecesidad = txtFecNeceHasta
        .DesdeFechaPresentacion = txtFecPresDesde
        .HastaFechaPresentacion = txtFecPresHasta
        .DesdeFechaUltReu = txtFecUltReuDesde
        .HastaFechaUltReu = txtFecUltReuHasta
        .DesdeFechaAdjudicacion = txtFecAdjDesde
        .HastaFechaAdjudicacion = txtFecAdjHasta
        .sGMN1_Proce = sdbcGMN1Proce_4Cod.Text
        .GMN1 = sdbcGMN1_4Cod.Text
        .GMN2 = sdbcGMN2_4Cod.Text
        .GMN3 = sdbcGMN3_4Cod.Text
        .GMN4 = sdbcGMN4_4Cod.Text
        .Cod = val(txtCod)
        
        .CarIniDen = txtDen.Text
        .mon = sdbcMonCod.Text
        .usarindice = False
        .coincidenciatotal = IIf(txtCod <> "" And txtDen = "", True, False)
        .PresGlobalDesde = dPresDesde
        .PresGlobalHasta = dPresHasta
        .Eqp = basOptimizacion.gCodEqpUsuario
        .Comp = basOptimizacion.gCodCompradorUsuario
        .CodUsu = basOptimizacion.gvarCodUsuario
        .bRMat = bRMat
        .bRAsig = bRAsig
        .bRCompResponsable = bRCompResponsable
        .bREqpAsig = bREqpAsig
        .bProveAsigEqp = m_bProveAsigEqp
        .bProveAsigComp = m_bProveAsigComp
        .bRUsuAper = bRUsuAper
        .bRUsuUON = bRUsuUON
        .bRUsuDep = bRUsuDep
        .UON1 = basOptimizacion.gUON1Usuario
        .UON2 = basOptimizacion.gUON2Usuario
        .UON3 = basOptimizacion.gUON3Usuario
        .Dep = basOptimizacion.gCodDepUsuario
        .DeAdjDirecta = (chkAdjDir.Value = vbChecked)
        .DeAdjReu = (chkAdjReu.Value = vbChecked)
        .Referencia = txtReferencia.Text
        .CodArt = sdbcCodArticulo.Text
        .DenArt = txtDenArticulo.Text
        .bSoloModoSubasta = (chkSubasta.Value = vbChecked)
        .Solicit = lblIdSolic.caption
        .sCodGrupo = Me.txtCodGrupo.Text
        .sDenGrupo = Me.txtDenGrupo.Text
        .vPresup1_1 = m_vPresup1_1
        .vPresup1_2 = m_vPresup1_2
        .vPresup1_3 = m_vPresup1_3
        .vPresup1_4 = m_vPresup1_4
        .vPresup2_1 = m_vPresup2_1
        .vPresup2_2 = m_vPresup2_2
        .vPresup2_3 = m_vPresup2_3
        .vPresup2_4 = m_vPresup2_4
        .vPresup3_1 = m_vPresup3_1
        .vPresup3_2 = m_vPresup3_2
        .vPresup3_3 = m_vPresup3_3
        .vPresup3_4 = m_vPresup3_4
        .vPresup4_1 = m_vPresup4_1
        .vPresup4_2 = m_vPresup4_2
        .vPresup4_3 = m_vPresup4_3
        .vPresup4_4 = m_vPresup4_4
        .sUON1Pres1 = m_sUON1Pres1
        .sUON2Pres1 = m_sUON2Pres1
        .sUON3Pres1 = m_sUON3Pres1
        .sUON1Pres2 = m_sUON1Pres2
        .sUON2Pres2 = m_sUON2Pres2
        .sUON3Pres2 = m_sUON3Pres2
        .sUON1Pres3 = m_sUON1Pres3
        .sUON2Pres3 = m_sUON2Pres3
        .sUON3Pres3 = m_sUON3Pres3
        .sUON1Pres4 = m_sUON1Pres4
        .sUON2Pres4 = m_sUON2Pres4
        .sUON3Pres4 = m_sUON3Pres4
        .sCodResp = m_sCodResp
        .sCodPerAper = m_sCodPerAper
        .sUON1Aper = m_sUON1Aper
        .sUON2Aper = m_sUON2Aper
        .sUON3Aper = m_sUON3Aper
        .sUON1Resp = m_sUON1Resp
        .sUON2Resp = m_sUON2Resp
        .sUON3Resp = m_sUON3Resp
        .sDepAper = m_sDepAper
        .sDepResp = m_sDepResp
        '.iPedido = Pedido
        .sCodDest = Me.sdbcDestCodProce.Columns("COD").Value
        If Me.sdbcEqpDen.Text <> "" Then
            .sCodEqp = Me.sdbcEqpDen.Columns("COD").Value
        End If
        If Me.sdbcCompDen.Text <> "" Then
            .sCodComp = Me.sdbcCompDen.Columns("COD").Value
        End If
        
        .bCompAdj = optComp.Value
        
        .scodProve = Me.sdbcProveCod.Text
    
    End With

Cargar = udtBusqueda


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "Cargar", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Public Sub PonerMatSeleccionadoEnCombos()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
      
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        
        bRespetarComboGMN2 = True
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        bRespetarComboGMN2 = False
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
      
        bRespetarComboGMN3 = True
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        bRespetarComboGMN3 = False
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
       
        bRespetarComboGMN4 = True
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        bRespetarComboGMN4 = False
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "PonerMatSeleccionadoEnCombos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub



Private Sub sdbcNivDet_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcNivDet.RemoveAll
    sdbcNivDet.AddItem m_stxtProceso & Chr(m_lSeparador) & 1
    sdbcNivDet.MoveNext
    sdbcNivDet.AddItem m_stxtGrupo
    sdbcNivDet.MoveNext
    sdbcNivDet.AddItem m_stxtItem
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcNivDet_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcNivDet_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcNivDet.Columns(0).caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcNivDet_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtCod_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsNumeric(txtCod) And txtCod <> "" Then
        oMensajes.NoValida "C�digo"
        Cancel = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub txtDenArticulo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboArt Then
        sdbcCodArticulo.Text = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtDenArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub txtFecApeDesde_Validate(Cancel As Boolean)
    
    If txtFecApeDesde <> "" Then
        If Not IsDate(txtFecApeDesde) Then
            oMensajes.NoValida " " & sIdiFecha & " "
            Cancel = True
        End If
    End If
End Sub

Private Sub txtFecApeHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecApeHasta <> "" Then
        If Not IsDate(txtFecApeHasta) Then
            oMensajes.NoValida " " & sIdiFecha & " "
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtFecApeHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecNeceDesde_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecNeceDesde <> "" Then
        If Not IsDate(txtFecNeceDesde) Then
            oMensajes.NoValida " " & sIdiFecha & " "
           Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtFecNeceDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecNeceHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecNeceHasta <> "" Then
        If Not IsDate(txtFecNeceHasta) Then
            oMensajes.NoValida " " & sIdiFecha & " "
           Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtFecNeceHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecPresDesde_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecPresDesde <> "" Then
        If Not IsDate(txtFecPresDesde) Then
            oMensajes.NoValida " " & sIdiFecha & " "
           Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtFecPresDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecPresHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecPresHasta <> "" Then
        If Not IsDate(txtFecPresHasta) Then
            oMensajes.NoValida " " & sIdiFecha & " "
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtFecPresHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub txtFecUltReuDesde_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If txtFecUltReuDesde <> "" Then
        If Not IsDate(txtFecUltReuDesde) Then
            oMensajes.NoValida " " & sIdiFecha & " "

            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtFecUltReuDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecUltReuHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecUltReuHasta <> "" Then
        If Not IsDate(txtFecUltReuHasta) Then
            oMensajes.NoValida " " & sIdiFecha & " "
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtFecUltReuHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtPresDesde_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtPresDesde.Text <> "" Then
        If Not IsNumeric(txtPresDesde) Then
            oMensajes.NoValido sIdiPresupuesto
            Cancel = True
            If Me.Visible Then txtPresDesde.SetFocus
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtPresDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub txtPresHasta_Validate(Cancel As Boolean)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtPresHasta.Text <> "" Then
        If Not IsNumeric(txtPresHasta) Then
            oMensajes.NoValido sIdiPresupuesto
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtPresHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub GMN1Seleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
 
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    If bPermProcMultiMaterial Then
        oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    Else
        oGMN1Seleccionado.Cod = sdbcGMN1Proce_4Cod
    End If
   
    sdbcGMN2_4Cod = ""
    sdbcGMN2_4Den = ""
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
    sdbcGMN4_4Cod = ""
    sdbcGMN4_4Den = ""
    sdbcCodArticulo.Text = ""
    txtDenArticulo.Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "GMN1Seleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub GMN2Seleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod.Text
    If Not bPermProcMultiMaterial Then
        oGMN2Seleccionado.GMN1Cod = sdbcGMN1Proce_4Cod.Text
    Else
        oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    End If
    sdbcGMN3_4Cod = ""
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "GMN2Seleccionado", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
   
End Sub
Private Sub GMN3Seleccionado()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN3Seleccionado = Nothing
    
    If sdbcGMN3_4Cod.Text = "" Or sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    If Not bPermProcMultiMaterial Then
        oGMN3Seleccionado.GMN1Cod = sdbcGMN1Proce_4Cod.Text
    Else
        oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    End If
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod

    sdbcGMN4_4Cod = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "GMN3Seleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub GMN4Seleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN4Seleccionado = Nothing
    
    If sdbcGMN4_4Cod.Text = "" Or sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    If bPermProcMultiMaterial Then
        oGMN4Seleccionado.GMN1Cod = sdbcGMN1Proce_4Cod.Text
    Else
        oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    End If
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
    sdbcCodArticulo = ""
    txtDenArticulo = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "GMN4Seleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
    sdbcAnyo.AllowInput = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "CargarAnyos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPROCE, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        m_sPresGlobalDesde = Ador(0).Value  '2
        Ador.MoveNext
        m_sPresGlobalHasta = Ador(0).Value  '3
        Ador.MoveNext
        fraGenerales.caption = Ador(0).Value    '4
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value & ":" '5
        Ador.MoveNext
        lblCodArticulo.caption = Ador(0).Value  '6
        Me.lblCodGr.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblDenGr.caption = Ador(0).Value '7
        lblDenArticulo.caption = Ador(0).Value
        Ador.MoveNext
        lblEst.caption = Ador(0).Value  '8
        Ador.MoveNext
        lblMon.caption = Ador(0).Value  '9
        Ador.MoveNext
        lblPresDesde.caption = Ador(0).Value    '10
        Ador.MoveNext
        lblPresHasta.caption = Ador(0).Value    '11
        lblFecApeHasta.caption = Ador(0).Value
        lblFecNeceHasta.caption = Ador(0).Value
        lblFecPresHasta.caption = Ador(0).Value
        lblFecUltReuHasta.caption = Ador(0).Value
        lblFecAdjHasta.caption = Ador(0).Value
        Ador.MoveNext
        lblSolicitud.caption = Ador(0).Value    '12
        Ador.MoveNext
        chkAdjDir.caption = Ador(0).Value    '13
        Ador.MoveNext
        chkAdjReu.caption = Ador(0).Value    '14
        Ador.MoveNext
        chkSubasta.caption = Ador(0).Value    '15
        Ador.MoveNext
        fraMAT.caption = Ador(0).Value    '16
        Ador.MoveNext
        fraFechas.caption = Ador(0).Value    '17
        Ador.MoveNext
        lblFecApeDesde.caption = Ador(0).Value    '18
        Ador.MoveNext
        lblFecNeceDesde.caption = Ador(0).Value    '19
        Ador.MoveNext
        lblFecPresDesde.caption = Ador(0).Value '20
        Ador.MoveNext
        lblFecUltReuDesde.caption = Ador(0).Value    '21
        Ador.MoveNext
        m_sFechaAperDesde = Ador(0).Value '22
        Ador.MoveNext
        m_sFechaAperHasta = Ador(0).Value    '23
        Ador.MoveNext
        m_sFechaNecDesde = Ador(0).Value    '24
        Ador.MoveNext
        m_sFechaNecHasta = Ador(0).Value    '25
        Ador.MoveNext
        m_sFechaPresDesde = Ador(0).Value    '26
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value    '27 'C�d.
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        sdbcMonCod.Columns(0).caption = Ador(0).Value
        sdbcMonDen.Columns(1).caption = Ador(0).Value
        sdbcCodArticulo.Columns(0).caption = Ador(0).Value
        Me.sdbcDestCodProce.Columns("COD").caption = Ador(0).Value
        Me.sdbcDestDenProce.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbcEst.Columns(0).caption = Ador(0).Value  '28 Denominaci�n
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbcMonCod.Columns(1).caption = Ador(0).Value
        sdbcMonDen.Columns(0).caption = Ador(0).Value
        sdbcCodArticulo.Columns(1).caption = Ador(0).Value
        Me.sdbcDestCodProce.Columns("DEN").caption = Ador(0).Value
        Me.sdbcDestDenProce.Columns("DEN").caption = Ador(0).Value
        m_Denominacion = Ador(0).Value
        m_DenominacionItem = Ador(0).Value
        Ador.MoveNext
        m_sFechaPresHasta = Ador(0).Value   '29
        Ador.MoveNext
        m_sFechaUltReuDesde = Ador(0).Value '30
        Ador.MoveNext
        sIdiEst(1) = Ador(0).Value '31
        Ador.MoveNext
        sIdiEst(2) = Ador(0).Value '32
        Ador.MoveNext
        sIdiEst(3) = Ador(0).Value '33
        Ador.MoveNext
        sIdiEst(4) = Ador(0).Value '34
        Ador.MoveNext
        sIdiEst(5) = Ador(0).Value '35
        Ador.MoveNext
        sIdiEst(6) = Ador(0).Value '36
        Ador.MoveNext
        sIdiEst(7) = Ador(0).Value '37
        Ador.MoveNext
        sIdiEst(8) = Ador(0).Value '38
        Ador.MoveNext
        sIdiEst(9) = Ador(0).Value '39
        Ador.MoveNext
        sIdiEst(10) = Ador(0).Value '40
        Ador.MoveNext
        sIdiMoneda = Ador(0).Value '41
        Ador.MoveNext
        sIdiMaterial = Ador(0).Value '42
        Ador.MoveNext
        sIdiDetalle = Ador(0).Value '43
        Ador.MoveNext
        sIdiResponsable = Ador(0).Value '44
        Ador.MoveNext
        sIdiFecha = Ador(0).Value '45
        Ador.MoveNext
        sIdiPresupuesto = Ador(0).Value '46
        Ador.MoveNext
        fraArt.caption = Ador(0).Value '47
        Ador.MoveNext
        m_sFechaUltReuHasta = Ador(0).Value '48
        Ador.MoveNext
        lblDest.caption = Ador(0).Value & ":" '49
        Ador.MoveNext
        m_sUsuApertura = Ador(0).Value '50
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        sstabProce.TabCaption(0) = Ador(0).Value '56
        Ador.MoveNext
        sstabProce.TabCaption(1) = Ador(0).Value    '57
        Ador.MoveNext
        Me.fraGrupo.caption = Ador(0).Value    '58
        Ador.MoveNext
        Me.fraUsuAper.caption = Ador(0).Value    '59
        Ador.MoveNext
        Me.fraResp.caption = Ador(0).Value '60
        Ador.MoveNext
        Me.fraPresupuestos.caption = Ador(0).Value    '61
        Ador.MoveNext
        Ador.MoveNext
        sEstados(1) = Ador(0).Value      '63 Aper
        Ador.MoveNext
        sEstados(2) = Ador(0).Value 'Proves
        Ador.MoveNext
        sEstados(3) = Ador(0).Value 'petic
        Ador.MoveNext
        sEstados(4) = Ador(0).Value 'REcp ofer
        Ador.MoveNext
        sEstados(5) = Ador(0).Value 'Comun obj
        Ador.MoveNext
        sEstados(6) = Ador(0).Value 'Pend adj
        Ador.MoveNext
        sEstados(7) = Ador(0).Value 'Pend notifadj
        Ador.MoveNext
        sEstados(8) = Ador(0).Value '70 Adj y notif
        Ador.MoveNext
        sEstados(9) = Ador(0).Value 'Anulado
        Ador.MoveNext
        sEstados(10) = Ador(0).Value 'Cerrado
        Ador.MoveNext
        sEstados(11) = Ador(0).Value 'Abierto 73
        Ador.MoveNext
        sOrdenar(1) = Ador(0).Value '74 C�digo
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        optSelProveOrdCod.caption = Ador(0).Value
        opComOrdCod.caption = Ador(0).Value
        opCompOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        sOrdenar(2) = Ador(0).Value '75 Denominaci�n
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        sdbcEqpDen.Columns(0).caption = Ador(0).Value
        sdbcCompDen.Columns(0).caption = Ador(0).Value
        sdbcMonCod.Columns(1).caption = Ador(0).Value
        sdbcMonCodigo.Columns(1).caption = Ador(0).Value
        sdbcMonDen.Columns(0).caption = Ador(0).Value
        sdbcMonDenom.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        optSelProveOrdDen.caption = Ador(0).Value
        opComOrdDen.caption = Ador(0).Value
        opCompOrdDen.caption = Ador(0).Value
        m_stxtDen = Ador(0).Value
        sdbcEst.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sOrdenar(3) = Ador(0).Value '76 Material
        Ador.MoveNext
        sOrdenar(4) = Ador(0).Value '77
        Ador.MoveNext
        sOrdenar(5) = Ador(0).Value
        Ador.MoveNext
        sOrdenar(6) = Ador(0).Value
        Ador.MoveNext
        m_stxtEstado = Ador(0).Value '80
        sOrdenar(7) = Ador(0).Value
        Ador.MoveNext
        sOrdenar(8) = Ador(0).Value
        Ador.MoveNext
        sOpc = Ador(0).Value '82 Opciones
        For i = 2 To 6
        sstabProce.TabCaption(i) = Ador(0).Value
        Next
        fraSelProve1.caption = Ador(0).Value
        Frame4.caption = Ador(0).Value
        Frame6.caption = Ador(0).Value
        Frame11.caption = Ador(0).Value
        Ador.MoveNext
        sTituloI = Ador(0).Value  '83
        Ador.MoveNext
        sTitulo(1) = Ador(0).Value
        Ador.MoveNext
        sTitulo(2) = Ador(0).Value
        Ador.MoveNext
        sTitulo(3) = Ador(0).Value
        Ador.MoveNext
        sTitulo(4) = Ador(0).Value '87
        Ador.MoveNext
        sTitulo(5) = Ador(0).Value
        Ador.MoveNext
        sTitulo(6) = Ador(0).Value
        Ador.MoveNext
        sFilPro = Ador(0).Value '90
        Ador.MoveNext
        sMON = Ador(0).Value  '91 Moneda
        Ador.MoveNext
        sMat = Ador(0).Value
        Ador.MoveNext
        sFec = Ador(0).Value
        Ador.MoveNext
        sstabProce.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value & ":"
        m_stxtAnio = Ador(0).Value
        Ador.MoveNext
        chkAdjDir.caption = Ador(0).Value '97
        m_stxtDir = Ador(0).Value
        Ador.MoveNext
        chkAdjReu.caption = Ador(0).Value
        m_stxtReu = Ador(0).Value
        Ador.MoveNext
        fraMAT.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        optComp.caption = Ador(0).Value '102
        m_stxtAsignado = Ador(0).Value
        Ador.MoveNext
        optResp.caption = Ador(0).Value
        m_stxtResp = Ador(0).Value
        Ador.MoveNext
        fraProve.caption = Ador(0).Value
        Ador.MoveNext
        sdbcEqpDen.Columns(1).caption = Ador(0).Value
        sdbcCompDen.Columns(1).caption = Ador(0).Value
        sdbcMonCod.Columns(0).caption = Ador(0).Value
        Me.sdbcMonCodigo.Columns(0).caption = Ador(0).Value
        sdbcMonDen.Columns(1).caption = Ador(0).Value
        Me.sdbcMonDenom.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        m_stxtCod = Ador(0).Value
        Ador.MoveNext
        lblMonedas.caption = Ador(0).Value
        Ador.MoveNext
        fraSelProve2.caption = Ador(0).Value
        Frame5.caption = Ador(0).Value
        Frame12.caption = Ador(0).Value
        Ador.MoveNext
        optResumido.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        chkIncPresup.caption = Ador(0).Value '110
        Ador.MoveNext
        chkIncUO.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        chkIncPers.caption = Ador(0).Value
        Ador.MoveNext
        chkIncEsp.caption = Ador(0).Value
        Ador.MoveNext
        chkSelProveCal.caption = Ador(0).Value '115
        Ador.MoveNext
        chkSelProvePot.caption = Ador(0).Value
        Ador.MoveNext
        chkSelProveEqp.caption = Ador(0).Value
        Ador.MoveNext
        chkSelProveComp.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        Label8.caption = Ador(0).Value '120
        Ador.MoveNext
        label10.caption = Ador(0).Value
        Ador.MoveNext
        chkComPet.caption = Ador(0).Value
        Ador.MoveNext
        chkComObj.caption = Ador(0).Value
        Ador.MoveNext
        chkComAdj.caption = Ador(0).Value
        Ador.MoveNext
        chkComExc.caption = Ador(0).Value '125
        Ador.MoveNext
        chkComSinPet.caption = Ador(0).Value
        Ador.MoveNext
        chkComPubWeb.caption = Ador(0).Value
        Ador.MoveNext
        Frame7.caption = Ador(0).Value
        Ador.MoveNext
        chkOfeRecSin.caption = Ador(0).Value
        Ador.MoveNext
        optOfeRecProve.caption = Ador(0).Value '130
        Ador.MoveNext
        optOfeRecItem.caption = Ador(0).Value
        Ador.MoveNext
        chkOfeRecUltProve.caption = Ador(0).Value
        chkOfeRecUltItem.caption = Ador(0).Value
        Ador.MoveNext
        chkOfeRecItems.caption = Ador(0).Value
        Ador.MoveNext
        Frame8.caption = Ador(0).Value
        Frame13.caption = Ador(0).Value
        lblItem.caption = Ador(0).Value
        Ador.MoveNext
        optItAbiertos.caption = Ador(0).Value '135
        optItemAbiertos.caption = Ador(0).Value
        optItAbiertosAdj.caption = Ador(0).Value
        Ador.MoveNext
        optItCerrados.caption = Ador(0).Value
        optItemCerrados.caption = Ador(0).Value
        optItCerradosAdj.caption = Ador(0).Value
        Ador.MoveNext
        optItTodos.caption = Ador(0).Value
        optTodosItems.caption = Ador(0).Value
        optTodosItAdj.caption = Ador(0).Value
        Ador.MoveNext
        opCompPorProve.caption = Ador(0).Value
        Ador.MoveNext
        opCompPorItem.caption = Ador(0).Value
        Ador.MoveNext
        sEspera(1) = Ador(0).Value '140
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        Ador.MoveNext
        m_sIdiMon1 = Ador(0).Value '143
        Ador.MoveNext
        m_sIdiMon2 = Ador(0).Value
        Ador.MoveNext
        m_stxtEqpRes = Ador(0).Value
        Ador.MoveNext
        m_stxtEqp = Ador(0).Value
        Ador.MoveNext
        m_stxtCompAs = Ador(0).Value
        Ador.MoveNext
        m_stxtCompRes = Ador(0).Value '148
        Ador.MoveNext
        m_stxtDenE = Ador(0).Value
        Ador.MoveNext
        m_stxtProveSel = Ador(0).Value
        Ador.MoveNext
        m_stxtProvePot = Ador(0).Value
        Ador.MoveNext
        m_stxtFDesde = Ador(0).Value
        Ador.MoveNext
        m_stxtFHasta = Ador(0).Value
        Ador.MoveNext
        m_stxtProveSin = Ador(0).Value
        Ador.MoveNext
        m_sIdiMon3 = Ador(0).Value '155
        Ador.MoveNext
        m_stxtUltOfe = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        'Idiomas del rpt
        Ador.MoveNext
        m_stxtPag = Ador(0).Value '158
        Ador.MoveNext
        m_stxtDe = Ador(0).Value
        Ador.MoveNext
        m_stxtfec = Ador(0).Value
        Ador.MoveNext
        m_stxtNece = Ador(0).Value
        Ador.MoveNext
        m_stxtAper = Ador(0).Value
        Ador.MoveNext
        m_stxtPresen = Ador(0).Value
        Ador.MoveNext
        m_stxtMon = Ador(0).Value
        Ador.MoveNext
        m_stxtCambio = Ador(0).Value '165
        Ador.MoveNext
        m_stxtPres = Ador(0).Value
        Ador.MoveNext
        m_stxtitems = Ador(0).Value
        Ador.MoveNext
        m_stxtDest = Ador(0).Value
        Ador.MoveNext
        m_stxtUnd = Ador(0).Value
        Ador.MoveNext
        m_stxtCant = Ador(0).Value '170
        Ador.MoveNext
        m_stxtPrec = Ador(0).Value
        Ador.MoveNext
        m_stxtPago = Ador(0).Value
        Ador.MoveNext
        m_stxtIni = Ador(0).Value
        Ador.MoveNext
        m_stxtFin = Ador(0).Value
        Ador.MoveNext
        m_stxtEspec = Ador(0).Value
        Ador.MoveNext
        m_stxtPerson = Ador(0).Value
        Ador.MoveNext
        m_stxtNom = Ador(0).Value
        Ador.MoveNext
        m_stxtCar = Ador(0).Value
        Ador.MoveNext
        m_stxtRol = Ador(0).Value
        Ador.MoveNext
        m_stxtTfno = Ador(0).Value '180
        Ador.MoveNext
        m_stxtFax = Ador(0).Value
        Ador.MoveNext
        m_stxtEmail = Ador(0).Value
        Ador.MoveNext
        m_stxtObj = Ador(0).Value
        Ador.MoveNext
        m_stxtSeleccion = Ador(0).Value
        Ador.MoveNext
        m_stxtCal = Ador(0).Value
        Ador.MoveNext
        m_stxtComp = Ador(0).Value
        Ador.MoveNext
        m_stxtLimi = Ador(0).Value
        Ador.MoveNext
        m_stxtProve = Ador(0).Value
        Ador.MoveNext
        m_stxtPubWeb = Ador(0).Value
        Ador.MoveNext
        m_stxtFecCom = Ador(0).Value '190
        Ador.MoveNext
        m_stxtFecDec = Ador(0).Value
        Ador.MoveNext
        m_stxtTipo = Ador(0).Value
        Ador.MoveNext
        m_stxtCPet = Ador(0).Value
        Ador.MoveNext
        m_stxtCObj = Ador(0).Value
        Ador.MoveNext
        m_stxtCAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtCExc = Ador(0).Value
        Ador.MoveNext
        m_stxtContac = Ador(0).Value
        Ador.MoveNext
        m_stxtComunic = Ador(0).Value
        Ador.MoveNext
        m_stxtWeb = Ador(0).Value
        Ador.MoveNext
        m_stxtCarta = Ador(0).Value '200
        Ador.MoveNext
        m_stxtHomo = Ador(0).Value
        Ador.MoveNext
        m_stxtFecVal = Ador(0).Value
        Ador.MoveNext
        m_stxtFecRec = Ador(0).Value
        Ador.MoveNext
        m_stxtFecValidez = Ador(0).Value
        Ador.MoveNext
        m_stxtEquiv = Ador(0).Value & ":" '205
        Me.sdbcMonCodigo.Columns(2).caption = Ador(0).Value
        Me.sdbcMonDenom.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        m_stxtObs = Ador(0).Value
        Ador.MoveNext
        m_stxtNumOfe = Ador(0).Value
        Ador.MoveNext
        m_stxtOfer = Ador(0).Value
        Ador.MoveNext
        m_stxtCantMax = Ador(0).Value
        Ador.MoveNext
        m_stxtProce = Ador(0).Value '210
        Ador.MoveNext
        m_stxtFecAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtImp = Ador(0).Value
        Ador.MoveNext
        m_stxtAho = Ador(0).Value
        Ador.MoveNext
        m_stxtItem = Ador(0).Value
        m_sNomIt = Ador(0).Value
        Ador.MoveNext
        m_stxtNOf = Ador(0).Value
        Ador.MoveNext
        m_stxtPrecAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtCantAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtPAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtImpAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtPAho = Ador(0).Value '220
        Ador.MoveNext
        m_stxtLimitOfer = Ador(0).Value
        Ador.MoveNext
        m_stxtNombreAdjunto = Ador(0).Value
        Ador.MoveNext
        m_stxtComentarioAdjunto = Ador(0).Value
        Ador.MoveNext
        chkOfeRecAdjuntos.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiSubasta = Ador(0).Value
        Ador.MoveNext
        chkSubasta.caption = Ador(0).Value
        m_stxtSubasta = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sEstados(12) = Ador(0).Value
        Ador.MoveNext
        lblMonedas.caption = Ador(0).Value
        Ador.MoveNext
        m_stxtOfertaAnterior = Ador(0).Value '230
        Ador.MoveNext
        optCiclo.caption = Ador(0).Value
        m_stxtListado = Ador(0).Value
        Ador.MoveNext
        optDetallado.caption = Ador(0).Value
        Ador.MoveNext
        lblNiv.caption = Ador(0).Value
        Ador.MoveNext
        optGrAbiertos.caption = Ador(0).Value
        optGrAbiertosAdj.caption = Ador(0).Value
        opOfeRecGAbiertos.caption = Ador(0).Value
        Ador.MoveNext
        optGrCerrados.caption = Ador(0).Value
        optGrCerradosAdj.caption = Ador(0).Value
        opOfeRecGCerrados.caption = Ador(0).Value
        Ador.MoveNext
        optGrTodos.caption = Ador(0).Value
        optGrTodosAdj.caption = Ador(0).Value
        opOfeRecTodosGrupos.caption = Ador(0).Value
        Ador.MoveNext
        chkIncAtrib.caption = Ador(0).Value
        chkOfeRecAtributos.caption = Ador(0).Value
        Ador.MoveNext
        lblOrden.caption = Ador(0).Value
        Ador.MoveNext
        m_stxtEstProc = Ador(0).Value
        Ador.MoveNext
        m_stxtEstProc2 = Ador(0).Value '240
        Ador.MoveNext
        m_stxtFecValSProv = Ador(0).Value
        Ador.MoveNext
        m_stxtPresItem = Ador(0).Value
        Ador.MoveNext
        m_stxtProveAct = Ador(0).Value
        Ador.MoveNext
        m_stxtCerr = Ador(0).Value
        Ador.MoveNext
        m_stxtPersonas = Ador(0).Value
        Ador.MoveNext
        m_stxtPresAnu = Ador(0).Value
        Ador.MoveNext
        m_stxtProveAsig = Ador(0).Value
        Ador.MoveNext
        m_stxtCodCom = Ador(0).Value
        Ador.MoveNext
        m_stxtCodProv = Ador(0).Value
        Ador.MoveNext
        m_stxtOfertas = Ador(0).Value '250
        Ador.MoveNext
        m_stxtReuniones = Ador(0).Value
        Ador.MoveNext
        m_stxtComunica = Ador(0).Value
        Ador.MoveNext
        m_stxtEspecificaciones = Ador(0).Value
        Ador.MoveNext
        m_stxtArticulos = Ador(0).Value
        Ador.MoveNext
        m_stxtAdjudicaciones = Ador(0).Value
        Ador.MoveNext
        m_stxtProveAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtConsumido = Ador(0).Value
        Ador.MoveNext
        m_stxtAdjudicado = Ador(0).Value
        Ador.MoveNext
        m_stxtAhorrado = Ador(0).Value
        Ador.MoveNext
        m_stxtNomAr = Ador(0).Value '260
        Ador.MoveNext
        m_stxtProveedor = Ador(0).Value
        Ador.MoveNext
        lblGrupos = Ador(0).Value
        fraGruposCerrados.caption = Mid(Ador(0).Value, Len(Ador(0).Value - 1))
        fraOfeRecGrupos.caption = Ador(0).Value
        Ador.MoveNext
        m_stxtHora = Ador(0).Value
        Ador.MoveNext
        m_stxtTipoAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtDistribuc = Ador(0).Value
        Ador.MoveNext
        m_stxtDatos = Ador(0).Value
        Ador.MoveNext
        m_stxtGrupo = Ador(0).Value
        Ador.MoveNext
        m_stxtEquipo = Ador(0).Value
        Label6.caption = Ador(0).Value & ":"
        Ador.MoveNext
        m_stxtValidacion = Ador(0).Value
        Ador.MoveNext
        m_stxtPresentacion = Ador(0).Value '270
        Ador.MoveNext
        m_stxtAdjudicacion = Ador(0).Value
        Ador.MoveNext
        m_stxtProvAsign = Ador(0).Value
        Ador.MoveNext
        m_stxtComp2 = Ador(0).Value
        Me.Frame3.caption = Ador(0).Value
        Label7.caption = Ador(0).Value & ":"
        Ador.MoveNext
        m_stxtPorcen = Ador(0).Value
        Ador.MoveNext
        m_stxtNivP = Ador(0).Value
        Ador.MoveNext
        m_stxtNivG = Ador(0).Value
        Ador.MoveNext
        m_stxtNivI = Ador(0).Value
        Ador.MoveNext
        m_stxtProceso = Ador(0).Value
        Ador.MoveNext
        m_stxtTexto = Ador(0).Value
        Ador.MoveNext
        m_stxtNum = Ador(0).Value '280
        Ador.MoveNext
        m_stxtDuda = Ador(0).Value
        Ador.MoveNext
        m_stxtInterno = Ador(0).Value
        Ador.MoveNext
        m_stxtExterno = Ador(0).Value
        Ador.MoveNext
        m_stxtLibre = Ador(0).Value
        Ador.MoveNext
        m_stxtSelec = Ador(0).Value
        Ador.MoveNext
        m_stxtPreferBajo = Ador(0).Value
        Ador.MoveNext
        m_stxtPreferAlto = Ador(0).Value
        Ador.MoveNext
        m_stxtObligatorioS = Ador(0).Value
        Ador.MoveNext
        m_stxtObligatorioN = Ador(0).Value
        Ador.MoveNext
        m_stxtTotalOfer = Ador(0).Value '290
        Ador.MoveNext
        m_stxtTotalGrupo = Ador(0).Value
        Ador.MoveNext
        m_stxtTotalItem = Ador(0).Value
        Ador.MoveNext
        m_stxtUnitario = Ador(0).Value
        Ador.MoveNext
        m_stxtAplicarS = Ador(0).Value
        Ador.MoveNext
        m_stxtAplicarN = Ador(0).Value
        Ador.MoveNext
        m_stxtAtributos = Ador(0).Value
        Ador.MoveNext
        m_stxtListadoDetallado = Ador(0).Value
        Ador.MoveNext
        m_stxtDistrOrg = Ador(0).Value
        Ador.MoveNext
        m_stxtDistrPre = Ador(0).Value
        Ador.MoveNext
        m_stxtUnds = Ador(0).Value '300
        Ador.MoveNext
        m_stxtPrecio1 = Ador(0).Value
        Ador.MoveNext
        m_stxtPrecio2 = Ador(0).Value
        Ador.MoveNext
        m_stxtPrecio3 = Ador(0).Value
        Ador.MoveNext
        m_stxtUsarPrec = Ador(0).Value
        Ador.MoveNext
        m_stxtValor = Ador(0).Value
        Ador.MoveNext
        m_stxtSi = Ador(0).Value
        Ador.MoveNext
        m_stxtNo = Ador(0).Value
        Ador.MoveNext
        m_stxtAdjuntos = Ador(0).Value
        Ador.MoveNext
        m_stxtArchivo = Ador(0).Value
        Ador.MoveNext
        m_stxtComent = Ador(0).Value '310
        Ador.MoveNext
        m_stxtProvAct = Ador(0).Value
        Ador.MoveNext
        m_stxtPresUni = Ador(0).Value
        Ador.MoveNext
        m_stxtAmb = Ador(0).Value
        Ador.MoveNext
        m_stxtDestinos = Ador(0).Value
        Ador.MoveNext
        m_stxtPagos = Ador(0).Value
        Ador.MoveNext
        m_stxtProveActs = Ador(0).Value
        Ador.MoveNext
        m_stxtSolicitud = Ador(0).Value
        Ador.MoveNext
        m_stxtSolicit = Ador(0).Value
        Ador.MoveNext
        m_stxtComent1 = Ador(0).Value & "1"
        m_stxtComent2 = Ador(0).Value & "2"
        m_stxtComent3 = Ador(0).Value & "3"
        m_stxtComentPrec = Ador(0).Value
        Ador.MoveNext
        m_stxtBajMinPuja = Ador(0).Value '320
        Ador.MoveNext
        m_stxtPresup = Ador(0).Value
        Ador.MoveNext
        m_stxtInicioSubasta = Ador(0).Value
        Ador.MoveNext
        m_stxtCierreSubasta = Ador(0).Value
        Ador.MoveNext
        chkDescr.caption = Ador(0).Value
        Ador.MoveNext
        sIdiOrigenGS = Ador(0).Value
        Ador.MoveNext
        sIdiOrigenPORTAL = Ador(0).Value
        Ador.MoveNext
        sIdiUSU = Ador(0).Value
        Ador.MoveNext
        sIdiContacto = Ador(0).Value
        Ador.MoveNext
        m_stxtCAvi = Ador(0).Value
        Ador.MoveNext
        m_stxtPagoP = Ador(0).Value '330
        Ador.MoveNext
        m_stxtSolic = Ador(0).Value
        Ador.MoveNext
        Me.chkComAvi.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcDestCodProce.Columns("DIR").caption = Ador(0).Value
        Me.sdbcDestDenProce.Columns("DIR").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcDestCodProce.Columns("POB").caption = Ador(0).Value
        Me.sdbcDestDenProce.Columns("POB").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcDestCodProce.Columns("CP").caption = Ador(0).Value
        Me.sdbcDestDenProce.Columns("CP").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcDestCodProce.Columns("PAIS").caption = Ador(0).Value
        Me.sdbcDestDenProce.Columns("PAIS").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcDestCodProce.Columns("PROVI").caption = Ador(0).Value '337
        Me.sdbcDestDenProce.Columns("PROVI").caption = Ador(0).Value
        Ador.MoveNext
        lblCod.caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value
        Ador.MoveNext
        chkSelProveGrupos.caption = Ador(0).Value '340
        Ador.MoveNext
        m_stxtGruAsig = Ador(0).Value '341
        Ador.MoveNext
        lblMaterialesProce = Ador(0).Value 'Materiales del proceso   '342
        Ador.MoveNext
        m_AtributosEspecificacion = Ador(0).Value '343 jpa Tarea 629
        m_AtributosEspecificacionItem = Ador(0).Value '343 jpa Tarea 629
        Ador.MoveNext
        m_Valor = Ador(0).Value '344 jpa Tarea 629
        m_ValorItem = Ador(0).Value '344 jpa Tarea 629
        Ador.MoveNext
        m_stxtPedido = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        lblFecAdjDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblNivDet.caption = Ador(0).Value
        Ador.MoveNext
        m_sFechaAdjDesde = Ador(0).Value
        Ador.MoveNext
        m_sFechaAdjHasta = Ador(0).Value '355
        Ador.MoveNext
        m_stxtMoneda = Ador(0).Value
        Ador.MoveNext
        m_stxtAdj = Ador(0).Value
        Ador.MoveNext
        m_stxtProveAdjudicado = Ador(0).Value
        Ador.MoveNext
        sOrdenar(9) = Ador(0).Value 'Fecha de adjudicacion
        Ador.MoveNext
        sTitulo(7) = Ador(0).Value  '360 Listado de adjudicaciones por proveedor
        Ador.MoveNext
        sTitulo(8) = Ador(0).Value
        Ador.MoveNext
        sTitulo(9) = Ador(0).Value
        Ador.MoveNext
        sTitulo(10) = Ador(0).Value
        Ador.MoveNext
        m_stxtDetalle = Ador(0).Value
        Ador.MoveNext
        m_stxtRango = Ador(0).Value
        Ador.MoveNext
        m_stxtCostesDesc = Ador(0).Value '367 Costes / Descuentos
        Ador.MoveNext
        m_stxtAlPrecio = Ador(0).Value '368 al precio unitario
        Ador.MoveNext
        m_stxtAlTotal = Ador(0).Value '369 al total del �tem
        Ador.MoveNext
        chkComAnulacion.caption = Ador(0).Value
        Ador.MoveNext
        m_stxtCAnul = Ador(0).Value
        Ador.MoveNext
        m_stxtAnyoImput = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "CargarRecursos", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


Public Sub PonerSolicitudSeleccionada(ByVal oSolic As CInstancia)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblSolicCompras.caption = oSolic.Id & " " & oSolic.DescrBreve
    lblIdSolic.caption = oSolic.Id
    
    Set oSolic = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "PonerSolicitudSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub MostrarPresSeleccionado12(ByVal iTipo As Integer)
Dim sLblUO As String
Dim sPresup1 As String
Dim sPresup2 As String
Dim sPresup3 As String
Dim sPresup4 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If iTipo = 1 Then
        m_vPresup1_1 = Null
        m_vPresup1_2 = Null
        m_vPresup1_3 = Null
        m_vPresup1_4 = Null
    Else
        m_vPresup2_1 = Null
        m_vPresup2_2 = Null
        m_vPresup2_3 = Null
        m_vPresup2_4 = Null
    End If
    
    If iTipo = 1 Then
        m_sUON1Pres1 = frmSELPresAnuUON.g_sUON1
        m_sUON2Pres1 = frmSELPresAnuUON.g_sUON2
        m_sUON3Pres1 = frmSELPresAnuUON.g_sUON3
    Else
        m_sUON1Pres2 = frmSELPresAnuUON.g_sUON1
        m_sUON2Pres2 = frmSELPresAnuUON.g_sUON2
        m_sUON3Pres2 = frmSELPresAnuUON.g_sUON3
    End If
    
    
    sLblUO = ""
    If iTipo = 1 Then
        If m_sUON1Pres1 <> "" Then
            sLblUO = "(" & m_sUON1Pres1
            If m_sUON2Pres1 <> "" Then
                sLblUO = sLblUO & " - " & m_sUON2Pres1
                If m_sUON3Pres1 <> "" Then
                    sLblUO = sLblUO & " - " & m_sUON3Pres1 & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
    Else
        If m_sUON1Pres2 <> "" Then
            sLblUO = "(" & m_sUON1Pres2
            If m_sUON2Pres2 <> "" Then
                sLblUO = sLblUO & " - " & m_sUON2Pres2
                If m_sUON3Pres2 <> "" Then
                    sLblUO = sLblUO & " - " & m_sUON3Pres2 & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
    End If
    
    
    sPresup1 = frmSELPresAnuUON.g_sPRES1
    sPresup2 = frmSELPresAnuUON.g_sPRES2
    sPresup3 = frmSELPresAnuUON.g_sPRES3
    sPresup4 = frmSELPresAnuUON.g_sPRES4

    If iTipo = 1 Then
        If sPresup4 <> "" Then
            Me.lblPresup1.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup3 <> "" Then
            Me.lblPresup1.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup2 <> "" Then
            Me.lblPresup1.caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup1 <> "" Then
            Me.lblPresup1.caption = sLblUO & sPresup1 & " " & frmSELPresAnuUON.g_sDenPres
        End If
    Else
        If sPresup4 <> "" Then
            Me.lblPresup2.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup3 <> "" Then
            Me.lblPresup2.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup2 <> "" Then
            Me.lblPresup2.caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresAnuUON.g_sDenPres
        ElseIf sPresup1 <> "" Then
            Me.lblPresup2.caption = sLblUO & sPresup1 & " " & frmSELPresAnuUON.g_sDenPres
        End If
    End If
    
    If iTipo = 1 Then
        m_vPresup1_1 = frmSELPresAnuUON.g_vIdPRES1
        m_vPresup1_2 = frmSELPresAnuUON.g_vIdPRES2
        m_vPresup1_3 = frmSELPresAnuUON.g_vIdPRES3
        m_vPresup1_4 = frmSELPresAnuUON.g_vIdPRES4
    Else
        m_vPresup2_1 = frmSELPresAnuUON.g_vIdPRES1
        m_vPresup2_2 = frmSELPresAnuUON.g_vIdPRES2
        m_vPresup2_3 = frmSELPresAnuUON.g_vIdPRES3
        m_vPresup2_4 = frmSELPresAnuUON.g_vIdPRES4
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "MostrarPresSeleccionado12", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


Public Sub MostrarPresSeleccionado34(ByVal iTipo As Integer)
    Dim sLblUO As String
    Dim sPresup1 As String
    Dim sPresup2 As String
    Dim sPresup3 As String
    Dim sPresup4 As String
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If iTipo = 3 Then
        m_vPresup3_1 = Null
        m_vPresup3_2 = Null
        m_vPresup3_3 = Null
        m_vPresup3_4 = Null
        m_sUON1Pres3 = frmSELPresUO.g_sUON1
        m_sUON2Pres3 = frmSELPresUO.g_sUON2
        m_sUON3Pres3 = frmSELPresUO.g_sUON3
    Else
        m_vPresup4_1 = Null
        m_vPresup4_2 = Null
        m_vPresup4_3 = Null
        m_vPresup4_4 = Null
        m_sUON1Pres4 = frmSELPresUO.g_sUON1
        m_sUON2Pres4 = frmSELPresUO.g_sUON2
        m_sUON3Pres4 = frmSELPresUO.g_sUON3
    End If
    
    
    sLblUO = ""
    If iTipo = 3 Then
        If m_sUON1Pres3 <> "" Then
            sLblUO = "(" & m_sUON1Pres3
            If m_sUON2Pres3 <> "" Then
                sLblUO = sLblUO & " - " & m_sUON2Pres3
                If m_sUON3Pres3 <> "" Then
                    sLblUO = sLblUO & " - " & m_sUON3Pres3 & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
    Else
        If m_sUON1Pres4 <> "" Then
            sLblUO = "(" & m_sUON1Pres4
            If m_sUON2Pres4 <> "" Then
                sLblUO = sLblUO & " - " & m_sUON2Pres4
                If m_sUON3Pres4 <> "" Then
                    sLblUO = sLblUO & " - " & m_sUON3Pres4 & ") "
                Else
                    sLblUO = sLblUO & ") "
                End If
            Else
                sLblUO = sLblUO & ") "
            End If
        End If
    End If
    
    sPresup1 = frmSELPresUO.g_sPRES1
    sPresup2 = frmSELPresUO.g_sPRES2
    sPresup3 = frmSELPresUO.g_sPRES3
    sPresup4 = frmSELPresUO.g_sPRES4

    If iTipo = 3 Then
        If sPresup4 <> "" Then
            lblPresup3.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup3 <> "" Then
            lblPresup3.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup2 <> "" Then
            lblPresup3.caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup1 <> "" Then
            lblPresup3.caption = sLblUO & sPresup1 & " " & frmSELPresUO.g_sDenPres
        End If
    Else
    
        If sPresup4 <> "" Then
            lblPresup4.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup3 <> "" Then
            lblPresup4.caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup2 <> "" Then
            lblPresup4.caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresUO.g_sDenPres
        ElseIf sPresup1 <> "" Then
            lblPresup4.caption = sLblUO & sPresup1 & " " & frmSELPresUO.g_sDenPres
        End If
    End If
    
    If iTipo = 3 Then
        m_vPresup3_1 = frmSELPresUO.g_vIdPRES1
        m_vPresup3_2 = frmSELPresUO.g_vIdPRES2
        m_vPresup3_3 = frmSELPresUO.g_vIdPRES3
        m_vPresup3_4 = frmSELPresUO.g_vIdPRES4
    Else
        m_vPresup4_1 = frmSELPresUO.g_vIdPRES1
        m_vPresup4_2 = frmSELPresUO.g_vIdPRES2
        m_vPresup4_3 = frmSELPresUO.g_vIdPRES3
        m_vPresup4_4 = frmSELPresUO.g_vIdPRES4
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "MostrarPresSeleccionado34", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Public Sub PersonaSeleccionada(iTipo As Integer, sUON1 As Variant, sUON2 As Variant, sUON3 As Variant, sDep As Variant, sPer As Variant)
Dim sUON As String
Dim iNivel As Integer
Dim oUons As Object

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If IsNull(sUON3) Then
    If IsNull(sUON2) Then
        If Not IsNull(sUON1) Then
            sUON = "(" & sUON1
            iNivel = 1
            If IsNull(sPer) And IsNull(sDep) Then
                Set oUons = oFSGSRaiz.Generar_CUnidadesOrgNivel1
                sUON = sUON & " - " & oUons.DevolverDenominacion(CStr(sUON1)) & ")"
                Set oUons = Nothing
            Else
                sUON = sUON & ")"
            End If
            
        Else
            If iTipo = 1 Then
                cmdBorrarUsuAper_Click
            Else
                cmdBorrarResp_Click
            End If
        End If
    Else
        iNivel = 2
        sUON = "(" & sUON1 & " - " & sUON2
        If IsNull(sPer) And IsNull(sDep) Then
            Set oUons = oFSGSRaiz.Generar_CUnidadesOrgNivel2
            sUON = sUON & " - " & oUons.DevolverDenominacion(CStr(sUON1), CStr(sUON2)) & ")"
            Set oUons = Nothing
        Else
            sUON = sUON & ")"
        End If
    End If
Else
    iNivel = 3
    sUON = "(" & sUON1 & " - " & sUON2 & " - " & sUON3
    If IsNull(sPer) And IsNull(sDep) Then
        Set oUons = oFSGSRaiz.Generar_CUnidadesOrgNivel3
        sUON = sUON & " - " & oUons.DevolverDenominacion(CStr(sUON1), CStr(sUON2), CStr(sUON3)) & ")"
        Set oUons = Nothing
    Else
        sUON = sUON & ")"
    End If
End If

If Not IsNull(sDep) Then
    If sUON <> "" Then
        sUON = sUON & " " & sDep
    Else
        sUON = sDep
    End If
    If Not IsNull(sPer) Then
        Dim oPersonas As CPersonas
        Dim oPer As CPersona
        Set oPersonas = oFSGSRaiz.Generar_CPersonas
        oPersonas.CargarTodasLasPersonas CStr(sPer), , , True, 4
        Set oPer = oPersonas.Item(1)
        sUON = sUON & " - " & oPer.nombre & " " & oPer.Apellidos
    Else
        Dim oDeps As CDepartamentos
        Set oDeps = oFSGSRaiz.Generar_CDepartamentos
        oDeps.CargarTodosLosDepartamentos CStr(sDep), , True
        If oDeps.Count > 0 Then sUON = sUON & " - " & oDeps.Item(1).Den
        Set oDeps = Nothing
    End If
End If

If iTipo = 1 Then
    Me.lblUsuAper.caption = sUON
    m_sCodPerAper = sPer
    m_sUON1Aper = sUON1
    m_sUON2Aper = sUON2
    m_sUON3Aper = sUON3
    m_sDepAper = sDep
Else
    Me.lblResp.caption = sUON
    m_sCodResp = sPer
    m_sUON1Resp = sUON1
    m_sUON2Resp = sUON2
    m_sUON3Resp = sUON3
    m_sDepResp = sDep
End If
    


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "PersonaSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestCodProce_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestDenProce.Value = ""
        sdbcDestCodProce.RemoveAll
        m_bDestRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestCodProce_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCodProce_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestCodProce.DroppedDown Then
        sdbcDestCodProce.Value = ""
        sdbcDestDenProce.Value = ""
        sdbcDestCodProce.RemoveAll
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestCodProce_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCodProce_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCodProce.Value = "..." Then
        sdbcDestCodProce.Value = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestDenProce.Value = sdbcDestCodProce.Columns(1).Value
    sdbcDestCodProce.Value = sdbcDestCodProce.Columns(0).Value
    m_bDestRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestCodProce_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcDestCodProce_DropDown()
Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestCodProce.RemoveAll
    
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , basOptimizacion.gPYMEUsuario)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest)
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCodProce.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestCodProce.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestCodProce.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestCodProce.Rows = 0 Then
        sdbcDestCodProce.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestCodProce_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestCodProce_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestCodProce.DataFieldList = "Column 0"
    sdbcDestCodProce.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestCodProce_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCodProce_PositionList(ByVal Text As String)
PositionList sdbcDestCodProce, Text
End Sub


Private Sub sdbcDestCodProce_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
  If sdbcDestCodProce.Value = "" Then Exit Sub

    If sdbcDestCodProce.Value = sdbcDestCodProce.Columns(0).Value Then
        m_bDestRespetarCombo = True
        sdbcDestDenProce.Value = sdbcDestCodProce.Columns(1).Value
        m_bDestRespetarCombo = False
        Exit Sub
    End If

    If sdbcDestCodProce.Value = sdbcDestDenProce.Columns(1).Value Then
        m_bDestRespetarCombo = True
        sdbcDestDenProce.Value = sdbcDestDenProce.Columns(0).Value
        m_bDestRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el destino
    If bRDest Then
         m_oDestinos.CargarTodosLosDestinosUON oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , UCase(CStr(sdbcDestCodProce.Value)), , True
    Else
         m_oDestinos.CargarTodosLosDestinos UCase(CStr(sdbcDestCodProce.Value)), , True, , , , , , , , True
    End If
    
    If m_oDestinos.Count = 0 Then
        sdbcDestCodProce.Value = ""
        Screen.MousePointer = vbNormal
    Else
        m_bDestRespetarCombo = True
        sdbcDestDenProce.Value = m_oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den

        sdbcDestCodProce.Columns(0).Value = sdbcDestCodProce.Value
        sdbcDestCodProce.Columns(1).Value = sdbcDestDenProce.Value

        m_bDestRespetarCombo = False
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestCodProce_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestDenProce_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestCodProce.Value = ""
        sdbcDestCodProce.RemoveAll
        m_bDestRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestDenProce_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDenProce_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestDenProce.DroppedDown Then
        sdbcDestDenProce.Value = ""
        sdbcDestCodProce.Value = ""
        sdbcDestCodProce.RemoveAll
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestDenProce_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDenProce_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestDenProce.Value = "..." Then
        sdbcDestDenProce.Value = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestCodProce.Value = sdbcDestDenProce.Columns(1).Value
    sdbcDestCodProce.Columns("COD").Value = sdbcDestDenProce.Columns(1).Value
    sdbcDestDenProce.Value = sdbcDestDenProce.Columns(0).Value
    'sdbcDestCodProce_Validate False
    m_bDestRespetarCombo = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestDenProce_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestDenProce_DropDown()
Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestDenProce.RemoveAll
    
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , True, basOptimizacion.gPYMEUsuario)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest, , , , , True)
    End If
    
    If ADORs Is Nothing Then
        sdbcDestDenProce.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestDenProce.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestDenProce.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestDenProce.Rows = 0 Then
        sdbcDestDenProce.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestDenProce_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestDenProce_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestDenProce.DataFieldList = "Column 0"
    sdbcDestDenProce.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcDestDenProce_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDenProce_PositionList(ByVal Text As String)
PositionList sdbcDestDenProce, Text
End Sub


Private Sub sdbcEqpDen_Change()
            
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
       If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCompDen = ""
        sdbcEqpDen.RemoveAll
        bRespetarCombo = False
        
        bCargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
         
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEqpDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
          
End Sub

Private Sub sdbcEqpDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpDen = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEqpDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcEqpDen_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpDen.Value = "" Then Exit Sub
    
    sdbcCompDen = ""
    sdbcCompDen.RemoveAll
    optComp.Enabled = True
    optResp.Enabled = True
        
    bRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    bRespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpDen.Columns(1).Text)
    Screen.MousePointer = vbNormal
    
    bCargarComboDesdeEqp = False
    
'    If gParametrosGenerales.gbOblProveEqp Then
'        sdbcProveCod.Text = ""
'        sdbcProveDen.Text = ""
'    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEqpDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bRAsig Or bRCompResponsable Then
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEqpDen.RemoveAll
    
    If bCargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            oEqps.CargarTodosLosEquipos , , False, True, False, basOptimizacion.gPYMEUsuario, NullToStr(oUsuarioSummit.Persona.codEqp)
        Else
            oEqps.CargarTodosLosEquipos , , False, True, False
        End If
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEqpDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEqpDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEqpDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)
PositionList sdbcEqpDen, Text
End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    If bRAsig Or bRCompResponsable Then
        If UCase(oUsuarioSummit.comprador.DenEqp) = UCase(sdbcEqpDen.Text) Then
            oEquipos.Add basOptimizacion.gCodEqpUsuario, oUsuarioSummit.comprador.DenEqp
        End If
    Else
        oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    End If
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
        sdbcCompDen = ""
        sdbcCompDen.RemoveAll
    Else
        bRespetarCombo = True
        Set oEqpSeleccionado = oEquipos.Item(1)

        sdbcEqpDen.Columns(1).Text = oEqpSeleccionado.Cod
        sdbcEqpDen.Columns(0).Text = oEqpSeleccionado.Den
        
        bRespetarCombo = False
        bCargarComboDesdeEqp = False
   
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcEqpDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcCompDen_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCompDen.RemoveAll
        bRespetarCombo = False
        
        bCargarComboDesdeComp = True
        Set oCompSeleccionado = Nothing
               
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCompDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcCompDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcCompDen.DroppedDown Then
        sdbcCompDen = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCompDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcCompDen_CloseUp()
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCompDen.Value = "..." Then
        sdbcCompDen.Text = ""
        Exit Sub
    End If
    
    If sdbcCompDen.Value = "" Then Exit Sub
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    bRespetarCombo = True
    sdbcCompDen.Text = sdbcCompDen.Columns(0).Text
    bRespetarCombo = False
    
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    
    Screen.MousePointer = vbHourglass
    Set oCompSeleccionado = oComps.Item(sCod & sdbcCompDen.Columns(1).Text)
    Screen.MousePointer = vbNormal
      
    bCargarComboDesdeComp = False
    
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCompDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
       
End Sub

Private Sub sdbcCompDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Dim EqpCod As String
    
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    If (optComp.Value = True And bRAsig) Or (optResp.Value = True And bRCompResponsable) Then
        Set oEqpSeleccionado.Compradores = oFSGSRaiz.generar_CCompradores
        oEqpSeleccionado.Compradores.Add oEqpSeleccionado.Cod, oUsuarioSummit.comprador.DenEqp, basOptimizacion.gCodCompradorUsuario, oUsuarioSummit.comprador.nombre, oUsuarioSummit.comprador.Apel, "", "", ""
        
        Set oComps = oEqpSeleccionado.Compradores
        Set oCompSeleccionado = oComps.Item(1)
        sdbcCompDen.AddItem oUsuarioSummit.comprador.Apel & ", " & oUsuarioSummit.comprador.nombre & Chr(m_lSeparador) & basOptimizacion.gCodCompradorUsuario
        Exit Sub
    End If

        
    Screen.MousePointer = vbHourglass
    Set oComps = Nothing
    Set oComps = oFSGSRaiz.generar_CCompradores
    
    
    
    If bCargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(Apellidos(sdbcEqpDen.Text)), True, False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, True, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    Set oEqpSeleccionado.Compradores = Nothing
    Screen.MousePointer = vbNormal
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        
        If bREqpAsig Then
            
            EqpCod = basOptimizacion.gCodEqpUsuario
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
            Next
            
        Else
            
            EqpCod = sdbcEqpDen.Columns("COD").Value
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
            Next
        
        End If
        
    
    Else
        
        For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
    
    End If
    
    If bCargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompDen.AddItem "..."
    End If

    sdbcCompDen.SelStart = 0
    sdbcCompDen.SelLength = Len(sdbcCompDen.Text)
    sdbcCompDen.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCompDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcCompDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCompDen.DataFieldList = "Column 0"
    sdbcCompDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCompDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcCompDen_PositionList(ByVal Text As String)
PositionList sdbcCompDen, Text
End Sub
Private Sub sdbcCompDen_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oEqpSeleccionado Is Nothing Then
        sdbcCompDen.Text = ""
        Exit Sub
    End If
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    Set oEqpSeleccionado.Compradores = oCompradores
    
    If sdbcCompDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    
    Screen.MousePointer = vbHourglass
    
    
    If bRAsig Or bRCompResponsable Then
        If UCase(oUsuarioSummit.comprador.Apel & ", " & oUsuarioSummit.comprador.nombre) = UCase(sdbcCompDen.Text) Then
            oEqpSeleccionado.Compradores.Add oEqpSeleccionado.Cod, oUsuarioSummit.comprador.DenEqp, basOptimizacion.gCodCompradorUsuario, oUsuarioSummit.comprador.nombre, oUsuarioSummit.comprador.Apel, "", "", ""
        End If
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , nombre(sdbcCompDen.Text), Apellidos(sdbcCompDen.Text), True, False, False, False
    End If
    Set oCompradores = oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompDen.Text = ""
    Else
        bRespetarCombo = True
        Set oCompSeleccionado = oCompradores.Item(1)
        sdbcCompDen.Columns(1).Text = oCompSeleccionado.Cod
        sdbcCompDen.Columns(0).Text = oCompSeleccionado.DenEqp
        bRespetarCombo = False
        bCargarComboDesdeComp = False
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcCompDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcProveCod_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveDen.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveCod_Click()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
        bCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub
''' <summary>
''' Validacion del codigo del proveedor
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProveCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIasig As IAsignaciones
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If basOptimizacion.gTipoDeUsuario <> Administrador Then
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
    End If
    Set oProves = oFSGSRaiz.generar_CProveedores
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") And Not g_oProceSeleccionado Is Nothing Then
        ' Ofertas por proceso
        Set oIasig = g_oProceSeleccionado

        If (m_bProveAsigComp) Then
            Set oProves = oIasig.DevolverProveedoresDesde(Trim(sdbcProveCod.Text), , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If (m_bProveAsigEqp) Then
                Set oProves = oIasig.DevolverProveedoresDesde(Trim(sdbcProveCod.Text), , True, , basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(Trim(sdbcProveCod.Text), , True)
            End If
        End If
        Set oIasig = Nothing
        
    Else
        
        If bRestProvMatComp Then
            oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False, bRestProvMatComp, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod
        Else
            oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
        End If
    End If
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        bRespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
            
        bRespetarComboProve = False
    End If
    bCargarComboDesde = True
    Set oProves = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveCod.Text = ""
        bRespetarComboProve = False
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveDen_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
        bCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub sdbcProveDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIasig As IAsignaciones
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveDen.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If basOptimizacion.gTipoDeUsuario <> Administrador Then
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
    End If
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") And Not g_oProceSeleccionado Is Nothing Then
        ' Ofertas por proceso
        Set oIasig = g_oProceSeleccionado

        If (m_bProveAsigComp) Then
            Set oProves = oIasig.DevolverProveedoresDesde(, Trim(sdbcProveDen.Text), True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If (m_bProveAsigEqp) Then
                Set oProves = oIasig.DevolverProveedoresDesde(, Trim(sdbcProveDen.Text), True, , basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(, Trim(sdbcProveDen.Text), True)
            End If
        End If
        Set oIasig = Nothing
    Else

        If bRestProvMatComp Then
            oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveDen.Text), True, , False, bRestProvMatComp, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod
        Else
            oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveDen.Text), True, , False
        End If
    End If
    
    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProveDen.Text = ""
    Else
        bRespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        sdbcProveDen.Text = oProves.Item(1).Den
        sdbcProveDen.Columns(1).Text = sdbcProveCod.Text
        sdbcProveDen.Columns(0).Text = sdbcProveDen.Text
        
        bRespetarComboProve = False
    End If
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
    Set oProves = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcProveDen_CloseUp()

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    bRespetarComboProve = False
    bCargarComboDesde = False
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
            
End Sub

''' <summary>
''' Cargar el combo ordenado por denominaci�n.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
Private Sub sdbcProveDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oIasig As IAsignaciones
    Dim sDen As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
       
    If gParametrosGenerales.gbOblProveEqp Then
        If sdbcEqpDen.Text <> "" Then
            sEqp = Trim(sdbcEqpDen.Columns(0).Value)
        Else
            sEqp = ""
        End If
    Else
        sEqp = ""
    End If
       
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If basOptimizacion.gTipoDeUsuario <> Administrador Then
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
    End If
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") And Not g_oProceSeleccionado Is Nothing Then
        ' Ofertas por proceso
        If bCargarComboDesde Then
            sDen = Trim(sdbcProveDen.Text)
        Else
            sDen = ""
        End If
        Set oIasig = g_oProceSeleccionado

        If (m_bProveAsigComp) Then
            Set oProves = oIasig.DevolverProveedoresDesde(, sDen, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If (m_bProveAsigEqp) Then
                Set oProves = oIasig.DevolverProveedoresDesde(, sDen, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(, sDen, , OrdAsigPorDenProve)
            End If
        End If
        Set oIasig = Nothing
    Else
        If bRestProvMatComp Then
            If bCargarComboDesde Then
                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen.Text), , , , , , , True
            Else
                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , True
            End If
        Else
            If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
                If bCargarComboDesde Then
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProveDen.Text), , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
                Else
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
                End If
            Else
            
                If bCargarComboDesde Then
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProveDen.Text), , , , , , , , , , , True
                Else
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True
                End If
                
            End If
        End If
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    bRespetarComboProve = False
    bCargarComboDesde = False
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub
Private Sub sdbcProveCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oIasig As IAsignaciones
    Dim sCod As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If gParametrosGenerales.gbOblProveEqp Then
        If sdbcEqpDen.Text <> "" Then
            sEqp = Trim(sdbcEqpDen.Columns("cod").Value)
        Else
            sEqp = ""
        End If
    Else
        sEqp = ""
    End If
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") Then
        If Trim(sdbcAnyo.Text) <> "" And Trim(txtCod.Text) <> "" And Trim(sdbcGMN1_4Cod.Text) <> "" Then
            If Not g_oProceSeleccionado Is Nothing Then
                If Trim(sdbcAnyo.Text) <> g_oProceSeleccionado.Anyo Or Trim(txtCod.Text) <> g_oProceSeleccionado.Cod Or Trim(sdbcGMN1_4Cod.Text) <> g_oProceSeleccionado.GMN1Cod Then
                    bNuevoProce = True
                End If
            Else
                bNuevoProce = True
            End If
            
            If bNuevoProce = True Then
                Set g_oProceSeleccionado = Nothing
                Set g_oProceSeleccionado = oFSGSRaiz.Generar_CProceso
                g_oProceSeleccionado.Anyo = Trim(sdbcAnyo.Text)
                g_oProceSeleccionado.GMN1Cod = Trim(sdbcGMN1_4Cod.Text)
                g_oProceSeleccionado.Cod = Trim(Trim(txtCod.Text))
                If basOptimizacion.gTipoDeUsuario <> Administrador Then
                    Set oProces = oFSGSRaiz.generar_CProcesos
                    oProces.CargarTodosLosProcesosDesde 1, 1, 20, TipoOrdenacionProcesos.OrdPorCod, g_oProceSeleccionado.Anyo, g_oProceSeleccionado.GMN1Cod, , , , g_oProceSeleccionado.Cod, , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, bRUsuUON, bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
                    If oProces.Count = 0 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.PermisoDenegadoProceso
                        sdbcProveCod.Text = ""
                        Set g_oProceSeleccionado = Nothing
                        Set oProces = Nothing
                        Exit Sub
                    End If
                    Set oProces = Nothing
                End If
            End If
        Else
            Set g_oProceSeleccionado = Nothing
        End If
    End If
    
    If (sOrigen = "frmOFERec" Or sOrigen = "A2B4C2") And Not g_oProceSeleccionado Is Nothing Then
        ' Ofertas por proceso
        If bCargarComboDesde Then
            sCod = Trim(sdbcProveCod.Text)
        Else
            sCod = ""
        End If
        
        Set oIasig = g_oProceSeleccionado

        If (m_bProveAsigComp) Then
            Set oProves = oIasig.DevolverProveedoresDesde(sCod, , , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If (m_bProveAsigEqp) Then
                Set oProves = oIasig.DevolverProveedoresDesde(sCod, , , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(sCod, , , OrdAsigPorCodProve)
            End If
        End If
        Set oIasig = Nothing
    Else
        If bRestProvMatComp Then
            If bCargarComboDesde Then
                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(sdbcProveCod.Text)
            Else
                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            End If
        Else
            If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
                If bCargarComboDesde Then
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProveCod.Text), , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
                Else
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
                End If
            Else
            
                If bCargarComboDesde Then
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProveCod.Text)
                Else
                    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp
                End If
                
            End If
        End If
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcProveCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub ConfigurarSeguridad(sOrigen As String)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
bRMat = False
bRAsig = False
bRCompResponsable = False
bREqpAsig = False
bRUsuAper = False
bRUsuUON = False
bRUsuDep = False
bRUO = False
m_bProveAsigEqp = False
m_bProveAsigComp = False
bSoloAdjDir = False
bSoloAdjReu = False

Select Case sOrigen
    
    
    Case "A2B1", "frmPROCE"
    ' Datos de apertura

        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestMatComp)) Is Nothing) Then
                bRMat = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestAsignado)) Is Nothing) Then
                bRAsig = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestResponsable)) Is Nothing) Then
                bRCompResponsable = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestEqpAsignado)) Is Nothing) Then
                bREqpAsig = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUsuAper)) Is Nothing) Then
                bRUsuAper = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUsuUON)) Is Nothing) Then
                bRUsuUON = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUsuDep)) Is Nothing) Then
                bRUsuDep = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUOPers)) Is Nothing) Then
                bRUO = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestProvMatComp)) Is Nothing) Then
                bRestProvMatComp = True
            End If
       End If
       
    Case "A2B2", "frmSELPROVE"
    ' Proveedores seleccionados
        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestMatComp)) Is Nothing) Then
                bRMat = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestComprador)) Is Nothing) Then
                bRAsig = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestResponsable)) Is Nothing) Then
                bRCompResponsable = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestEquipo)) Is Nothing) Then
                bREqpAsig = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestUsuAper)) Is Nothing) Then
                bRUsuAper = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestUsuUON)) Is Nothing) Then
                bRUsuUON = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestUsuDep)) Is Nothing) Then
                bRUsuDep = True
            End If
            If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestAsigEqp)) Is Nothing) Then
                m_bProveAsigEqp = True
            End If
            If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestAsigComp)) Is Nothing) Then
                m_bProveAsigComp = True
            End If
'            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestMatComp)) Is Nothing) Then
'                bRestProvMatComp = True
'            End If
        End If
    Case "A2B3", "frmOFEPet"
    ' Comunicaci�n con proveedores
        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestMatComp)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRMat = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestComprador)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRAsig = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestResponsable)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRCompResponsable = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestEquipo)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bREqpAsig = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuAper)) Is Nothing) Then
                bRUsuAper = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuUON)) Is Nothing) Then
                bRUsuUON = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuDep)) Is Nothing) Then
                bRUsuDep = True
            End If
            If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestAsigEqp)) Is Nothing) Then
                m_bProveAsigEqp = True
            End If
            If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestComprador)) Is Nothing) Then
                m_bProveAsigComp = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestMatComp)) Is Nothing) Then
                bRestProvMatComp = True
            End If
        End If
    
    Case "A2B4C2", "frmOFERec"
    ' Ofertas recibidas
        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestMatComprador)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRMat = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestComprador)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRAsig = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestResponsable)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRCompResponsable = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestEquipo)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bREqpAsig = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestUsuAper)) Is Nothing) Then
                bRUsuAper = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestUsuUON)) Is Nothing) Then
                bRUsuUON = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestUsuDep)) Is Nothing) Then
                bRUsuDep = True
            End If
            If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestOfeEqp)) Is Nothing) Then
                m_bProveAsigEqp = True
            End If
            If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestOfeComp)) Is Nothing) Then
                m_bProveAsigComp = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestProvMatComp)) Is Nothing) Then
                bRestProvMatComp = True
            End If
        End If

    Case "A2B5", "frmADJ"
        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPPermSoloAdjDir)) Is Nothing) Then
                bSoloAdjDir = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPPermSoloenReunion)) Is Nothing) Then
                bSoloAdjReu = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestMat)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRMat = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestAsignado)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRAsig = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestResponsable)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bRCompResponsable = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestEqpAsig)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
                bREqpAsig = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestUsuAper)) Is Nothing) Then
                bRUsuAper = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestUsuUON)) Is Nothing) Then
                bRUsuUON = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestUsuDep)) Is Nothing) Then
                bRUsuDep = True
            End If
            If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestOfeEqp)) Is Nothing) Then
                m_bProveAsigEqp = True
            End If
            If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestOfeComp)) Is Nothing) Then
                m_bProveAsigComp = True
            End If
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPRestProvMatComp)) Is Nothing) Then
                bRestProvMatComp = True
            End If
        End If
    End Select
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
            If gParametrosGenerales.gbMultiMaterial Then
                bPermProcMultiMaterial = True
            Else
                bPermProcMultiMaterial = False
            End If
        End If
    Else
        If gParametrosGenerales.gbMultiMaterial Then
            bPermProcMultiMaterial = True
        Else
            bPermProcMultiMaterial = False
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub ConfigurarSolicitud()
    'Comprueba si se visualizar�n o no las solicitudes de compras
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bMostrarSolicitud = True
    If FSEPConf Then
        m_bMostrarSolicitud = False
    Else
        If basParametros.gParametrosGenerales.gbSolicitudesCompras = False Then
            m_bMostrarSolicitud = False
        End If
        
        'Si no es el usuario administrador
        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            'Si no tiene permisos de consulta de las solicitudes no podr� visualizarlas
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APESolicAsignar)) Is Nothing) Then
                m_bMostrarSolicitud = False
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ConfigurarSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub PonerMatSeleccionado()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
      
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        
        bRespetarComboGMN2 = True
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        bRespetarComboGMN2 = False
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
      
        bRespetarComboGMN3 = True
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        bRespetarComboGMN3 = False
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
       
        bRespetarComboGMN4 = True
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        bRespetarComboGMN4 = False
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "PonerMatSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Public Sub CargarProveBusqueda()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveCod_Validate False
    Set frmPROVEBuscar.oProveEncontrados = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "CargarProveBusqueda", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub CargarComboOrden()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcOrdenar.AddItem sOrdenar(1) & Chr(m_lSeparador) & 1
    sdbcOrdenar.AddItem sOrdenar(2) & Chr(m_lSeparador) & 2
    sdbcOrdenar.AddItem sOrdenar(7) & Chr(m_lSeparador) & 3
    sdbcOrdenar.AddItem m_stxtResp & Chr(m_lSeparador) & 4

    sdbcOrdenar.Columns("NUM").Value = 1
    OrdenListado = TipoOrdenacionProcesos.OrdPorCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "CargarComboOrden", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcMonCodigo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcMonDenom.Text = ""
        bRespetarCombo = False
        
        dequivalencia = 0
        sMoneda = ""
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCodigo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonCodigo_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcMonCodigo.Value = "..." Or Trim(sdbcMonCodigo.Value) = "" Then
        sdbcMonCodigo.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDenom.Text = sdbcMonCodigo.Columns(1).Text
    sdbcMonCodigo.Text = sdbcMonCodigo.Columns(0).Text
    dequivalencia = sdbcMonCodigo.Columns(2).Value
    sMoneda = sdbcMonCodigo.Columns(1).Text

    bRespetarCombo = False
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCodigo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonCodigo_DropDown()

Dim sDesde As String
Dim oMon As CMoneda

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMonCodigo.RemoveAll

    If bCargarComboDesde Then
        sDesde = sdbcMonCodigo.Value
    Else
        sDesde = ""
    End If

    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, sDesde, , , True

    For Each oMon In oMonedas
       
        sdbcMonCodigo.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
   
    Next
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCodigo_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonCodigo_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMonCodigo.DataFieldList = "Column 0"
    sdbcMonCodigo.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCodigo_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonCodigo_PositionList(ByVal Text As String)
PositionList sdbcMonCodigo, Text
End Sub

Private Sub sdbcMonCodigo_Validate(Cancel As Boolean)


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Trim(sdbcMonCodigo.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcMonCodigo.Value)) = UCase(Trim(sdbcMonCodigo.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonDenom = sdbcMonCodigo.Columns(1).Value
        bRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedasDesde 1, sdbcMonCodigo.Text, , , True, , True
    
    If oMonedas.Item(1) Is Nothing Then
        'Para que no salte un error en el load si la moneda de instalacion no existe
        If sdbcMonCodigo.Text <> basPublic.gParametrosInstalacion.gsMoneda Then
            oMensajes.NoValido sMON
        End If
        sdbcMonCodigo = ""
        dequivalencia = 0
        sMoneda = ""
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Cod) <> UCase(sdbcMonCodigo.Value) Then
            If sdbcMonCodigo.Text <> basPublic.gParametrosInstalacion.gsMoneda Then
                oMensajes.NoValido sMON
            End If
            sdbcMonCodigo = ""
            dequivalencia = 0
            sMoneda = ""
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonDenom = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            dequivalencia = oMonedas.Item(1).Equiv
            sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            
            sdbcMonCodigo.Columns(0).Text = sdbcMonCodigo.Text
            sdbcMonCodigo.Columns(1).Text = sdbcMonDenom.Text
            sdbcMonCodigo.Columns(0).Value = oMonedas.Item(1).Cod
            sdbcMonCodigo.Columns(1).Value = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sdbcMonCodigo.Columns(2).Value = oMonedas.Item(1).Equiv
        
            bRespetarCombo = False
        End If
    End If
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonCodigo_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonDenom_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcMonCodigo.Text = ""
        bRespetarCombo = False
        dequivalencia = 0
        sMoneda = ""
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDenom_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonDenom_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcMonDenom.Value = "....." Or Trim(sdbcMonDenom.Value) = "" Then
        sdbcMonDenom.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDenom.Text = sdbcMonDenom.Columns(0).Text
    sdbcMonCodigo.Text = sdbcMonDenom.Columns(1).Text
    dequivalencia = sdbcMonDenom.Columns(2).Value
    sMoneda = sdbcMonDenom.Columns(0).Text

    bRespetarCombo = False
    'txtCambio = oMonedas.Item(sdbcMoncodigo).Equiv
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDenom_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonDenom_DropDown()

Dim sDesde As String
Dim oMon As CMoneda

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMonDenom.RemoveAll
    
If bCargarComboDesde Then
    sDesde = sdbcMonDenom.Value
Else
    sDesde = ""
End If

    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , sDesde, True, True
    
    For Each oMon In oMonedas
   
        sdbcMonDenom.AddItem oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Cod & Chr(m_lSeparador) & oMon.Equiv
   
    Next
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDenom_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonDenom_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMonDenom.DataFieldList = "Column 0"
    sdbcMonDenom.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDenom_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMonDenom_PositionList(ByVal Text As String)
PositionList sdbcMonDenom, Text
End Sub

Private Sub sdbcMonDenom_Validate(Cancel As Boolean)


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Trim(sdbcMonDenom.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcMonDenom.Value)) = UCase(Trim(sdbcMonDenom.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonCodigo.Text = sdbcMonDenom.Columns(1).Value
        bRespetarCombo = False
        'txtCambio = oMonedas.Item(sdbcMoncodigo).Equiv
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedasDesde 1, , sdbcMonDenom.Text, True, True, , True
    
    If oMonedas.Item(1) Is Nothing Then
        'Para que no salte un error en el load si la moneda de instalacion no existe
        If sdbcMonCodigo.Text <> basPublic.gParametrosInstalacion.gsMoneda Then
            oMensajes.NoValido sMON
        End If
        sdbcMonDenom = ""
        dequivalencia = 0
        sMoneda = ""
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) <> UCase(sdbcMonDenom.Value) Then
            'Para que no salte un error en el load si la moneda de instalacion no existe
            If sdbcMonCodigo.Text <> basPublic.gParametrosInstalacion.gsMoneda Then
                oMensajes.NoValido sMON
            End If
            sdbcMonDenom = ""
            dequivalencia = 0
            sMoneda = ""
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonCodigo.Text = oMonedas.Item(1).Cod
            dequivalencia = oMonedas.Item(1).Equiv
            sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            
            sdbcMonDenom.Columns(1).Text = sdbcMonCodigo.Text
            sdbcMonDenom.Columns(0).Text = sdbcMonDenom.Text
            sdbcMonCodigo.Columns(0).Value = oMonedas.Item(1).Cod
            sdbcMonCodigo.Columns(1).Value = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sdbcMonCodigo.Columns(2).Value = oMonedas.Item(1).Equiv

            bRespetarCombo = False
        End If
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcMonDenom_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub optCiclo_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcOrdenar.Enabled = False
    lblNiv.Enabled = False
    sdbcNiv.Value = ""
    sdbcNiv.Enabled = False
    chkIncAtrib.Value = vbUnchecked
    chkIncAtrib.Enabled = False
    
    chkDescr.Enabled = False
    chkDescr.Value = vbUnchecked
    
    chkIncEsp.Value = vbUnchecked
    chkIncEsp.Enabled = False
    chkIncPers.Value = vbUnchecked
    chkIncPers.Enabled = False
    chkIncUO.Value = vbUnchecked
    chkIncUO.Enabled = False
    chkIncPresup.Value = vbUnchecked
    chkIncPresup.Enabled = False
    lblGrupos.Enabled = False
    optGrAbiertos.Value = False
    optGrAbiertos.Enabled = False
    optGrCerrados.Value = False
    optGrCerrados.Enabled = False
    optGrTodos.Value = False
    optGrTodos.Enabled = False
    lblItem.Enabled = False
    optItAbiertos.Value = False
    optItAbiertos.Enabled = False
    optItCerrados.Value = False
    optItCerrados.Enabled = False
    optItTodos.Value = False
    optItTodos.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "optCiclo_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub optResumido_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcOrdenar.Enabled = True
    lblNiv.Enabled = False
    sdbcNiv.Value = ""
    sdbcNiv.Enabled = False
    chkIncAtrib.Value = vbUnchecked
    chkIncAtrib.Enabled = False
    
    chkDescr.Enabled = False
    chkDescr.Value = vbUnchecked
    
    chkIncEsp.Value = vbUnchecked
    chkIncEsp.Enabled = False
    chkIncPers.Value = vbUnchecked
    chkIncPers.Enabled = False
    chkIncUO.Value = vbUnchecked
    chkIncUO.Enabled = False
    chkIncPresup.Value = vbUnchecked
    chkIncPresup.Enabled = False
    lblGrupos.Enabled = False
    optGrAbiertos.Value = False
    optGrAbiertos.Enabled = False
    optGrCerrados.Value = False
    optGrCerrados.Enabled = False
    optGrTodos.Value = False
    optGrTodos.Enabled = False
    lblItem.Enabled = False
    optItAbiertos.Value = False
    optItAbiertos.Enabled = False
    optItCerrados.Value = False
    optItCerrados.Enabled = False
    optItTodos.Value = False
    optItTodos.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "optResumido_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub optDetallado_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If optDetallado.Value = True Then
        sdbcOrdenar.Enabled = False
        picIncluir.Enabled = True
        lblNiv.Enabled = True
        chkIncAtrib.Enabled = True
        'jpa Tarea 629
        'chkDescr.Enabled = True
        chkDescr.Enabled = False
        chkDescr.Value = vbUnchecked
        'fin jpa Tarea 629
        
        chkIncEsp.Enabled = True
        chkIncPers.Enabled = True
        chkIncUO.Enabled = True
        chkIncPresup.Enabled = True
        sdbcNiv.Enabled = True
        sdbcNiv.Value = m_stxtProceso
        If sdbcNiv.Value = m_stxtGrupo Then
            picGrup.Enabled = True
            optGrAbiertos.Enabled = True
            optGrCerrados.Enabled = True
            optGrTodos.Enabled = True
        Else
            picGrup.Enabled = False
        End If
        If sdbcNiv.Value = m_stxtItem Then
            lblItem.Enabled = True
            picItem.Enabled = True
            optItAbiertos.Enabled = True
            optItCerrados.Enabled = True
            optItTodos.Enabled = True
        Else
            lblItem.Enabled = False
            picItem.Enabled = False
        End If
    Else
        picIncluir.Enabled = False
        picGrup.Enabled = False
        picItem.Enabled = False
        sdbcOrdenar.Enabled = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "optDetallado_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcNiv_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case sdbcNiv.Value
    
        Case m_stxtProceso
            
            lblGrupos.Enabled = False
            optGrAbiertos.Enabled = False
            optGrAbiertos.Value = False
            optGrCerrados.Enabled = False
            optGrCerrados.Value = False
            optGrTodos.Enabled = False
            optGrTodos.Value = False
            lblItem.Enabled = False
            optItAbiertos.Enabled = False
            optItAbiertos.Value = False
            optItCerrados.Enabled = False
            optItCerrados.Value = False
            optItTodos.Enabled = False
            optItTodos.Value = False
        
        Case m_stxtGrupo
        
            picGrup.Enabled = True
            lblGrupos.Enabled = True
            optGrAbiertos.Enabled = True
            optGrCerrados.Enabled = True
            optGrTodos.Enabled = True
            optGrTodos.Value = True
            
            lblItem.Enabled = False
            optItAbiertos.Enabled = False
            optItAbiertos.Value = False
            optItCerrados.Enabled = False
            optItCerrados.Value = False
            optItTodos.Enabled = False
            optItTodos.Value = False
            
        Case m_stxtItem:
            
            picItem.Enabled = True
            lblItem.Enabled = True
            optItAbiertos.Enabled = True
            optItCerrados.Enabled = True
            optItTodos.Enabled = True
            optItTodos.Value = True
            
            lblGrupos.Enabled = False
            optGrAbiertos.Enabled = False
            optGrAbiertos.Value = False
            optGrCerrados.Enabled = False
            optGrCerrados.Value = False
            optGrTodos.Enabled = False
            optGrTodos.Value = False
            
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcNiv_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcNiv_DropDown()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcNiv.RemoveAll
    sdbcNiv.AddItem m_stxtProceso & Chr(m_lSeparador) & 1
    sdbcNiv.MoveNext
    sdbcNiv.AddItem m_stxtGrupo
    sdbcNiv.MoveNext
    sdbcNiv.AddItem m_stxtItem
   
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcNiv_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcNiv_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcNiv.Columns(0).caption = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcNiv_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcOrdenar_click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 Select Case sdbcOrdenar.Columns("NUM").Value
    Case 1 '"C�digo"
        OrdenListado = TipoOrdenacionProcesos.OrdPorCod
    Case 2 '"Denominaci�n"
        OrdenListado = TipoOrdenacionProcesos.OrdPorDen
    Case 3 '"Estado"
        OrdenListado = TipoOrdenacionProcesos.OrdPorEstado
    Case 4 '"Responsable"
        OrdenListado = TipoOrdenacionProcesos.OrdPorResponsable
    Case Else
        OrdenListado = TipoOrdenacionProcesos.OrdPorCod
 End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sdbcOrdenar_click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub txtComDesde_Validate(Cancel As Boolean)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtComDesde.Text <> "" Then
        If Not IsDate(txtComDesde.Text) Then
            oMensajes.NoValido sFec
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtComDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtComHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtComHasta.Text <> "" Then
        If Not IsDate(txtComHasta.Text) Then
            oMensajes.NoValido sFec
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "txtComHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkOfeRecSin_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If chkOfeRecSin.Value = vbChecked Then
    chkOfeRecItems.Enabled = False
    chkOfeRecUltProve.Enabled = False
    chkOfeRecAdjuntos.Enabled = False
    chkOfeRecAtributos.Enabled = False
    chkOfeRecUltItem.Enabled = False
    optOfeRecProve.Enabled = False
    optOfeRecItem.Enabled = False
Else
    optOfeRecProve.Enabled = True
    optOfeRecItem.Enabled = True
    If optOfeRecProve.Value Then
        chkOfeRecItems.Enabled = True
        chkOfeRecUltProve.Enabled = True
        chkOfeRecAdjuntos.Enabled = True
        chkOfeRecAtributos.Enabled = True
    Else
        chkOfeRecUltItem.Enabled = True
    End If
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "chkOfeRecSin_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub optOfeRecItem_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If optOfeRecItem Then
        chkOfeRecUltProve.Enabled = False
        chkOfeRecUltProve.Value = False
        chkOfeRecItems.Enabled = False
        chkOfeRecItems.Value = False
        chkOfeRecAdjuntos.Enabled = False
        chkOfeRecAdjuntos.Value = False
        chkOfeRecAtributos.Enabled = False
        chkOfeRecAtributos.Value = False
        chkOfeRecUltItem.Enabled = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "optOfeRecItem_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub optOfeRecProve_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If optOfeRecProve Then
        chkOfeRecUltProve.Enabled = True
        chkOfeRecItems.Enabled = True
        chkOfeRecAdjuntos.Enabled = True
        chkOfeRecAtributos.Enabled = True
        chkOfeRecUltItem.Enabled = False
        chkOfeRecUltItem.Value = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "optOfeRecProve_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub optGrAbiertosAdj_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If optGrCerrados.Value = False Then
        Frame13.Enabled = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "optGrAbiertosAdj_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub optGrCerradosAdj_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Frame13.Enabled = False
    optTodosItAdj.Value = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "optGrCerradosAdj_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub optGrTodosAdj_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If optGrCerrados.Value = False Then
        Frame13.Enabled = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "optGrTodosAdj_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Function GenerarTextoSeleccion() As String
Dim sSeleccion As String
Dim sEstMat As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sSeleccion = ""
        
    If val(iAnio) <> 0 Then
        sSeleccion = m_stxtAnio & ": " & Format(val(iAnio), "0000") & "; "
    End If

    With udtBusquedaProc
        If .sGMN1_Proce <> "" Then
            sSeleccion = sSeleccion & lblGMN1_4.caption & .sGMN1_Proce & "; "
        End If
        If .Cod <> 0 Then
            sSeleccion = sSeleccion & m_stxtCod & ": " & CStr(.Cod) & "; "
        ElseIf .CarIniDen <> "" Then
            sSeleccion = sSeleccion & m_stxtDenE & ": '" & .CarIniDen & "'" & "; "
        End If
        
        If sdbcEst.Text <> "" Then sSeleccion = sSeleccion & m_stxtEstado & ": " & sIdiEst(sdbcEst.Columns("COD").Value) & "; "
        
        If .mon <> "" Then sSeleccion = sSeleccion & lblMon.caption & " " & .mon & "; "
        If .PresGlobalDesde <> 0 Then sSeleccion = sSeleccion & m_sPresGlobalDesde & " " & CStr(.PresGlobalDesde) & "; "
        If .PresGlobalHasta <> 0 Then sSeleccion = sSeleccion & m_sPresGlobalHasta & " " & CStr(.PresGlobalHasta) & "; "
        If .Referencia <> "" Then sSeleccion = sSeleccion & lblReferencia.caption & " " & CStr(.Referencia) & "; "
        If .bSoloModoSubasta Then sSeleccion = sSeleccion & m_sIdiSubasta & "; "
        If .sCodDest <> "" Then sSeleccion = sSeleccion & lblDest.caption & " " & CStr(.sCodDest) & "; "
        If Not IsNull(.Solicit) And Not IsEmpty(.Solicit) And .Solicit <> "" Then sSeleccion = sSeleccion & lblSolicitud.caption & " " & CStr(.Solicit) & "; "
        If .DeAdjDirecta And Not .DeAdjReu Then sSeleccion = sSeleccion & chkAdjDir.caption & "; "
        If Not .DeAdjDirecta And .DeAdjReu Then sSeleccion = sSeleccion & chkAdjReu.caption & "; "
        If .sCodGrupo <> "" Then
            sSeleccion = sSeleccion & fraGrupo.caption & " " & CStr(.sCodGrupo) & "; "
        ElseIf .sDenGrupo <> "" Then
            sSeleccion = sSeleccion & fraGrupo.caption & " " & CStr(.sDenGrupo) & "; "
        End If

        If .GMN4 <> "" Then
            sEstMat = .GMN1 & " - " & .GMN2 & " - " & .GMN3 & " - " & .GMN4
            sSeleccion = sSeleccion & sMat & ": " & sEstMat & "; "
        ElseIf .GMN3 <> "" Then
            sEstMat = .GMN1 & " - " & .GMN2 & " - " & .GMN3
            sSeleccion = sSeleccion & sMat & ": " & sEstMat & "; "
        ElseIf .GMN2 <> "" Then
            sEstMat = .GMN1 & " - " & .GMN2
            sSeleccion = sSeleccion & sMat & ": " & sEstMat & "; "
        ElseIf .GMN1 <> "" Then
            sEstMat = .GMN1
            sSeleccion = sSeleccion & sMat & ": " & sEstMat & "; "
        End If
        
        If .CodArt <> "" Then
            sSeleccion = sSeleccion & fraArt.caption & ": " & .CodArt & "; "
        ElseIf .DenArt <> "" Then
            sSeleccion = sSeleccion & fraArt.caption & ": " & .DenArt & "; "
        End If
        If .scodProve <> "" Then
            sSeleccion = sSeleccion & m_stxtProveSel & " " & Trim(.scodProve) & "; "
        End If
        If IsDate(.DesdeFechaApertura) And IsDate(.HastaFechaApertura) Then
            sSeleccion = sSeleccion & sOrdenar(4) & ": " & .DesdeFechaApertura & " - " & .HastaFechaApertura & "; "
        ElseIf IsDate(.DesdeFechaApertura) Then
            sSeleccion = sSeleccion & m_sFechaAperDesde & " " & .DesdeFechaApertura & "; "
        ElseIf IsDate(.HastaFechaApertura) Then
            sSeleccion = sSeleccion & m_sFechaAperHasta & " " & .HastaFechaApertura & "; "
        End If
        If IsDate(.DesdeFechaNecesidad) And IsDate(.HastaFechaNecesidad) Then
            sSeleccion = sSeleccion & sOrdenar(5) & ": " & .DesdeFechaNecesidad & " - " & .HastaFechaNecesidad & "; "
        ElseIf IsDate(.DesdeFechaNecesidad) Then
            sSeleccion = sSeleccion & m_sFechaNecDesde & " " & .DesdeFechaNecesidad & "; "
        ElseIf IsDate(.HastaFechaNecesidad) Then
            sSeleccion = sSeleccion & m_sFechaNecHasta & " " & .HastaFechaNecesidad & "; "
        End If
        If IsDate(.DesdeFechaPresentacion) And IsDate(.HastaFechaPresentacion) Then
            sSeleccion = sSeleccion & sOrdenar(6) & ": " & .DesdeFechaPresentacion & " - " & .HastaFechaPresentacion & "; "
        ElseIf IsDate(.DesdeFechaNecesidad) Then
            sSeleccion = sSeleccion & m_sFechaPresDesde & " " & .DesdeFechaPresentacion & "; "
        ElseIf IsDate(.HastaFechaNecesidad) Then
            sSeleccion = sSeleccion & m_sFechaPresHasta & " " & .HastaFechaPresentacion & "; "
        End If
        If IsDate(.DesdeFechaUltReu) And IsDate(.HastaFechaUltReu) Then
            sSeleccion = sSeleccion & lblFecUltReuDesde.caption & .DesdeFechaUltReu & " - " & .HastaFechaUltReu & "; "
        ElseIf IsDate(.DesdeFechaUltReu) Then
            sSeleccion = sSeleccion & m_sFechaUltReuDesde & " " & .DesdeFechaUltReu & "; "
        ElseIf IsDate(.HastaFechaUltReu) Then
            sSeleccion = sSeleccion & m_sFechaUltReuHasta & " " & .HastaFechaUltReu & "; "
        End If
        If IsDate(.DesdeFechaAdjudicacion) And IsDate(.HastaFechaAdjudicacion) Then
            sSeleccion = sSeleccion & sOrdenar(9) & ": " & .DesdeFechaAdjudicacion & " - " & .HastaFechaAdjudicacion & ";"
        ElseIf IsDate(.DesdeFechaAdjudicacion) Then
            sSeleccion = sSeleccion & m_sFechaAdjDesde & " " & .DesdeFechaAdjudicacion & "; "
        ElseIf IsDate(.HastaFechaAdjudicacion) Then
            sSeleccion = sSeleccion & m_sFechaAdjHasta & " " & .HastaFechaAdjudicacion & "; "
        End If
        If lblPresup1.caption <> "" Then
            sSeleccion = sSeleccion & gParametrosGenerales.gsPlurPres1 & ": " & lblPresup1.caption & "; "
        End If
        If lblPresup2.caption <> "" Then
            sSeleccion = sSeleccion & gParametrosGenerales.gsPlurPres2 & ": " & lblPresup2.caption & "; "
        End If
        If lblPresup3.caption <> "" Then
            sSeleccion = sSeleccion & gParametrosGenerales.gsPlurPres3 & ": " & lblPresup3.caption & "; "
        End If
        If lblPresup4.caption <> "" Then
            sSeleccion = sSeleccion & gParametrosGenerales.gsPlurPres4 & ": " & lblPresup4.caption & "; "
        End If
        If lblUsuAper.caption <> "" Then
            sSeleccion = sSeleccion & m_sUsuApertura & " " & lblUsuAper.caption & "; "
        End If
        If lblResp.caption <> "" Then
            sSeleccion = sSeleccion & sIdiResponsable & ": " & lblResp.caption & "; "
        End If
        If Not bREqpAsig And Not bRAsig And Not bRCompResponsable Then
            If .bCompAdj Then
                If .sCodComp <> "" Then
                    sSeleccion = sSeleccion & m_stxtCompAs & " " & Trim(sdbcEqpDen.Text) & " - " & Trim(sdbcCompDen.Text) & "; "
                ElseIf .sCodEqp <> "" Then
                    sSeleccion = sSeleccion & m_stxtEqp & " " & Trim(sdbcEqpDen.Text) & "; "
                End If
            Else
                If .sCodComp <> "" Then
                    sSeleccion = sSeleccion & m_stxtCompRes & " " & Trim(sdbcEqpDen.Text) & " - " & Trim(sdbcCompDen.Text) & "; "
                ElseIf .sCodEqp <> "" Then
                    sSeleccion = sSeleccion & m_stxtEqpRes & " " & Trim(sdbcEqpDen.Text) & "; "
                End If
            End If
        End If
        If sOrigen = "A2B5" Then
            Select Case sdbcNivDet.Text
                Case m_stxtProceso
                    sSeleccion = sSeleccion & m_stxtDetalle & " " & LCase(m_stxtProceso)
                Case m_stxtGrupo
                    sSeleccion = sSeleccion & m_stxtDetalle & " " & LCase(m_stxtGrupo)
                Case m_stxtItem
                    sSeleccion = sSeleccion & m_stxtDetalle & " " & LCase(m_stxtItem)
            End Select
        End If
    End With
    
    
    GenerarTextoSeleccion = sSeleccion
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "GenerarTextoSeleccion", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub ObtenerListadoAperturaResumido()
    Dim FormulaFields(1 To 2, 1 To 8) As String
    Dim sMonedaSel As String
    Dim i As Integer
    Dim j As Integer
    Dim oReport As CRAXDRT.Report
    Dim oCRProceso As CRProceso
    Dim pv As Preview

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCRProceso = GenerarCRProceso

    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If

    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPROCEAperResumido.rpt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing

    udtBusquedaProc = Cargar()


    Screen.MousePointer = vbHourglass


    sSeleccion = GenerarTextoSeleccion

    For i = 1 To 2 'Inicializo el array de formulas
        For j = 1 To UBound(FormulaFields, 2)
            FormulaFields(i, j) = ""
        Next j
    Next i

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)

    'Selecci�n que se lista, formula 1 - @SEL
    FormulaFields(2, 1) = "SEL"
    If sSeleccion = "" Then
        FormulaFields(1, 1) = ""
    Else
        FormulaFields(1, 1) = m_stxtSeleccion & " " & Trim(sSeleccion)
    End If

    'Paso los textos de los estados
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst1")).Text = """" & sEstados(1) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst2")).Text = """" & sEstados(2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst3")).Text = """" & sEstados(3) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst4")).Text = """" & sEstados(4) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst5")).Text = """" & sEstados(5) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst6")).Text = """" & sEstados(6) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst7")).Text = """" & sEstados(7) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst8")).Text = """" & sEstados(8) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst10")).Text = """" & sEstados(12) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst9")).Text = """" & sEstados(9) & """"

    sTit = sTituloI & " - " & sTitulo(1)
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sTit & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.ParameterFields(crs_ParameterIndex(oReport, "EQUIV")).SetCurrentValue dequivalencia, 7
    oReport.EnableParameterPrompting = False

    sMonedaSel = ""

    If optResumido.Value Then
        'Cabecera de columna form. de la 2 a la 8
        FormulaFields(2, 2) = "EtiqAnyo": FormulaFields(1, 2) = m_stxtAnio '@EtiqAnyo
        FormulaFields(2, 3) = "EtiqG1": FormulaFields(1, 3) = gParametrosGenerales.gsabr_GMN1
        FormulaFields(2, 4) = "EtiqCod": FormulaFields(1, 4) = m_stxtCod
        FormulaFields(2, 5) = "EtiqDesc": FormulaFields(1, 5) = m_stxtDen
        FormulaFields(2, 6) = "EtiqEst": FormulaFields(1, 6) = m_stxtEstado
        FormulaFields(2, 7) = "EtiqResp": FormulaFields(1, 7) = m_stxtResp
        FormulaFields(2, 8) = "RESUMEN": FormulaFields(1, 8) = "S"
        For i = 1 To UBound(FormulaFields, 2)
            If FormulaFields(2, i) <> "" Then
                oReport.FormulaFields(crs_FormulaIndex(oReport, FormulaFields(2, i))).Text = """" & FormulaFields(1, i) & """"
            End If
        Next i
        
        If Not oCRProceso.ListadoPROCEAperResumido(oGestorInformes, oReport, iAnio, DesdeEst, HastaEst, OrdenListado, udtBusquedaProc) Then
            oMensajes.ImposibleMostrarInforme
            Screen.MousePointer = vbNormal
            Exit Sub
        End If

    End If

    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Me.Hide

    frmESPERA.lblGeneral.caption = sEspera(1) & " " & sTit
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)

    Set pv = New Preview
    pv.Hide
    pv.caption = sTit
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport

    frmESPERA.Show

    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show

    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ObtenerListadoAperturaResumido", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


Private Sub Timer1_Timer()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "Timer1_Timer", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


''' <summary>
''' Obtiene el listado del proceso, cuando esta el check de detalles seleccionado
''' </summary>
''' <remarks>Llamada desde=cmdObtener_Click; Tiempo m�ximo Puede ser mayor que 2 seg dependiendo del proceso</remarks>

Private Sub ObtenerListadoDetallado()

Dim oReport As CRAXDRT.Report
Dim pv As Preview
Dim sMonedaSel As String
Dim sOpt As String
Dim SubListado As Object
Dim m_sEstado As String
Dim sOptPagArt As String
Dim sOptDestinoArt As String
Dim sOptFecArt As String
Dim sOptProvArt As String
Dim sCompradorCod As String
Dim sCompradorEqp As String
Dim optNivelIt As String
Dim iTipoGr As Integer
Dim iTipoIt As Integer
Dim sOptSolicitArt As String
Dim dEquivMon As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If crs_Connected = False Then
        Exit Sub
    End If
        
    
    udtBusquedaProc = Cargar
    
    If Not g_bEsInvitado Then
        Set oProceEncontrados = oFSGSRaiz.generar_CProcesos
        oProceEncontrados.BuscarTodosLosProcesos 1, sdbcAnyo.Text, sinitems, Cerrado, TipoOrdenacionProcesos.OrdPorCod, udtBusquedaProc
        If oProceEncontrados.Count = 0 Then
            oMensajes.PermisoDenegadoProceso
            Exit Sub
        End If
        Set oProceEncontrados = Nothing
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptListadoDetallado.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
               
    Screen.MousePointer = vbHourglass
          
    If optGrAbiertos.Value = vbUnchecked Then
        If optGrCerrados.Value = vbUnchecked Then
            If optGrTodos.Value = vbChecked Then
                iTipoGr = 2
            End If
        Else
            iTipoGr = 1
        End If
    Else
        iTipoGr = 0
    End If
            
    If optItAbiertos.Value = vbUnchecked Then
        If optItCerrados.Value = vbUnchecked Then
            If optItTodos.Value = vbChecked Then
                iTipoIt = 2
            End If
        Else
            iTipoIt = 1
        End If
    Else
        iTipoIt = 0
    End If
          
    Set m_oProceso = oFSGSRaiz.Generar_CProceso
    m_oProceso.Anyo = sdbcAnyo
    m_oProceso.GMN1Cod = sdbcGMN1Proce_4Cod.Text
    m_oProceso.Cod = txtCod.Text
    m_oProceso.CargarDatosGeneralesProceso
    
    Select Case m_oProceso.DefDestino
        Case EnProceso:
            sOptDestinoArt = "P"
        Case EnGrupo:
            sOptDestinoArt = "G"
        Case EnItem:
            sOptDestinoArt = "I"
    End Select
    
    Select Case m_oProceso.DefFechasSum
        Case EnProceso:
            sOptFecArt = "P"
        Case EnGrupo:
            sOptFecArt = "G"
        Case EnItem:
            sOptFecArt = "I"
    End Select
      
    Select Case m_oProceso.DefFormaPago
        Case EnProceso:
            sOptPagArt = "P"
        Case EnGrupo:
            sOptPagArt = "G"
        Case EnItem:
            sOptPagArt = "I"
    End Select
       
    Select Case m_oProceso.DefProveActual
        Case EnProceso:
            sOptProvArt = "P"
        Case EnGrupo:
            sOptProvArt = "G"
        Case EnItem:
            sOptProvArt = "I"
    End Select
    
    Select Case m_oProceso.DefSolicitud
        Case EnProceso:
            sOptSolicitArt = "P"
        Case EnGrupo:
            sOptSolicitArt = "G"
        Case EnItem:
            sOptSolicitArt = "I"
    End Select
    
    If sdbcMonCodigo.Text = "" Then
        oMonedas.CargarTodasLasMonedasDesde 1, m_oProceso.MonCod, , , True, , True
        sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        dequivalencia = oMonedas.Item(1).Equiv

    End If
    dEquivMon = dequivalencia
    dequivalencia = dequivalencia / m_oProceso.Cambio
    sText = DividirFrase(m_sIdiMon1)
    sMonedaSel = sText(1) & sMoneda & sText(2)
    sMonedaSel = sMonedaSel & "; " & sdbcMonCodigo.Columns(2).caption & ": " & dEquivMon
        
    sTit = sTituloI & " - " & sTitulo(1)
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & m_stxtListadoDetallado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.ParameterFields(crs_ParameterIndex(oReport, "Equiv")).SetCurrentValue dequivalencia, 7
    oReport.FormulaFields(crs_FormulaIndex(oReport, "Moneda")).Text = """" & sMonedaSel & """"
    
    'Indica al report si se va a mostrar o no las solicitudes
    If m_bMostrarSolicitud = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & "N" & """"
    End If
    
    '*** DATOS GENERALES ********************************************
    Set SubListado = oReport.OpenSubreport("rptDatosGenerales")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDatosGenerales")).Text = """" & m_stxtDatos & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProceso")).Text = """" & m_stxtProceso & """"
    Select Case m_oProceso.Estado
        Case 1:
            m_sEstado = sEstados(1)
        Case 2:
            m_sEstado = sEstados(1)
        Case 3:
            m_sEstado = sEstados(2)
        Case 4:
            m_sEstado = sEstados(2)
        Case 5:
            m_sEstado = sEstados(3)
        Case 6:
            m_sEstado = sEstados(4)
        Case 7:
            m_sEstado = sEstados(4)
        Case 8:
            m_sEstado = sEstados(5)
        Case 9:
            m_sEstado = sEstados(5)
        Case 10:
            m_sEstado = sEstados(6)
        Case 11:
            m_sEstado = sEstados(12)
        Case 12:
            m_sEstado = sEstados(7)
        Case 13:
            m_sEstado = sEstados(8)
        Case 20:
            m_sEstado = sEstados(9)
    End Select
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst")).Text = """" & m_stxtEstado & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "Estado")).Text = """" & m_sEstado & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDest")).Text = """" & m_stxtDest & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjReu")).Text = """" & m_stxtReu & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjDir")).Text = """" & m_stxtDir & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecAdj")).Text = """" & m_stxtAdjudicacion & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecPres")).Text = """" & m_stxtPresentacion & ":" & """"
    If m_oProceso.PermitirAdjDirecta = True Then
        sOpt = "S"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptTipo")).Text = """" & sOpt & """"
    Else
        sOpt = "N"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptTipo")).Text = """" & sOpt & """"
    End If
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecNec")).Text = """" & m_stxtNece & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecLim")).Text = """" & m_stxtLimitOfer & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptPagArt")).Text = """" & sOptPagArt & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtoptDestinoArt")).Text = """" & sOptDestinoArt & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptProvArt")).Text = """" & sOptProvArt & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptFecArt")).Text = """" & sOptFecArt & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtMon")).Text = """" & sMON & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCambio")).Text = """" & m_stxtCambio & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "Cambio")).Text = """" & m_oProceso.Cambio & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProve")).Text = """" & m_stxtProve & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFechaIn")).Text = """" & m_stxtIni & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFechaFin")).Text = """" & m_stxtFin & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPag")).Text = """" & m_stxtPago & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecApe")).Text = """" & m_stxtAper & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptSolicit")).Text = """" & sOptSolicitArt & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSolicit")).Text = """" & m_stxtSolicit & ":" & """"
    'Indica al report si se va a mostrar o no las solicitudes
    If m_bMostrarSolicitud = True Then
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "vMostrarSolicit")).Text = """" & "S" & """"
    Else
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "vMostrarSolicit")).Text = """" & "N" & """"
    End If
    
    If m_oProceso.responsable Is Nothing Then
        sCompradorCod = ""
        sCompradorEqp = ""
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CompCod")).Text = """" & sCompradorCod & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CompEqp")).Text = """" & sCompradorEqp & """"
    Else
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CompCod")).Text = """" & m_oProceso.responsable.Cod & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CompEqp")).Text = """" & m_oProceso.responsable.codEqp & """"
    End If
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCompResp")).Text = """" & m_stxtCompRes & """"
                      
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtInicioSubasta")).Text = """" & m_stxtInicioSubasta & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFinSubasta")).Text = """" & m_stxtCierreSubasta & """"
    
    If chkIncEsp.Value = vbChecked Then
        sOpt = "S"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEspTexto")).Text = """" & m_stxtEspec & """"
    Else
        sOpt = "N"
    End If
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "ESPEC")).Text = """" & sOpt & """"
    
    Set g_adoresDG = oGestorInformes.CicloVidaProcesoDatosGenerales(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, CBool(chkIncEsp.Value))
    
    If Not g_adoresDG Is Nothing Then
        SubListado.Database.SetDataSource g_adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    
    'jpa Tarea 629
    '*** DATOS GENERALES ATRIBUTOS DE ESPEFICICACION********************************************
    If chkIncEsp.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptAtributosEspecificacion.rpt")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributosEspecificacion")).Text = """" & m_AtributosEspecificacion & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDENOM")).Text = """" & m_Denominacion & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVALOR")).Text = """" & m_stxtValor & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_stxtSi & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_stxtNo & """"
        Set g_adoresDG = oGestorInformes.CicloVidaProcesoDatosGeneralesConAtriEspe(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)

        If Not g_adoresDG Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEspAtr")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresDG
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEspAtr")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEspAtr")).Text = """" & sOpt & """"
    End If
    'fin jpa Tarea 629
        
    '***MATERIALES
    Set SubListado = oReport.OpenSubreport("rptMateriales")
    
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "Materiales")).Text = """" & lblMaterialesProce & """"
        
    Set g_adoresDG = oGestorInformes.DevolverMaterial(m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod) ', oMat.GMN1Cod, oMat.GMN2Cod, oMat.GMN3Cod, oMat.Cod)
    If Not g_adoresDG Is Nothing Then
        SubListado.Database.SetDataSource g_adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set g_adoresDG = Nothing
    
    
    '*** GRUPOS-ITEMS ************************************************************************************
        
    If m_iNivel = 1 Then
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptArt")).Text = """" & sOpt & """"
    Else
        Set SubListado = oReport.OpenSubreport("rptArticulos")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtItems")).Text = """" & m_stxtitems & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGrupo")).Text = """" & m_stxtGrupo & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDest")).Text = """" & m_stxtDest & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGDest")).Text = """" & m_stxtDest & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPago")).Text = """" & m_stxtPago & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGPago")).Text = """" & m_stxtPago & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtIni")).Text = """" & m_stxtIni & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGIni")).Text = """" & m_stxtIni & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFin")).Text = """" & m_stxtFin & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGFin")).Text = """" & m_stxtFin & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProveAct")).Text = """" & m_stxtProve & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGProveAct")).Text = """" & m_stxtProve & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnd")).Text = """" & m_stxtUnd & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCant")).Text = """" & m_stxtCant & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec")).Text = """" & m_stxtPresUni & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & sOrdenar(8) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSolicitud")).Text = """" & m_stxtSolicit & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtBajMinPuja")).Text = """" & m_stxtBajMinPuja & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresEsc")).Text = """" & m_stxtPresEsc & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCantIni")).Text = """" & m_stxtCantIni & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCantFinal")).Text = """" & m_stxtCantFin & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresUni")).Text = """" & m_stxtPresUniEsc & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAnyoImput")).Text = """" & m_stxtAnyoImput & """"
        
        'Indica al subreport si se va a mostrar o no las solicitudes
        If m_bMostrarSolicitud = True Then
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "vMostrarSolicit")).Text = """" & "S" & """"
        Else
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "vMostrarSolicit")).Text = """" & "N" & """"
        End If
        
        If chkIncEsp.Value = vbChecked Then
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "ESPEC")).Text = """" & "S" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEspTexto")).Text = """" & m_stxtEspec & """"
            'jpa Tarea 629
            'Se incluye en el subreport la informacion de los atributos de especificacion de grupo o item
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributosEspecificacion")).Text = """" & m_AtributosEspecificacion & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDENOM")).Text = """" & m_Denominacion & ":" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVALOR")).Text = """" & m_stxtValor & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributosEspecificacionItem")).Text = """" & m_AtributosEspecificacionItem & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDENOMItem")).Text = """" & m_DenominacionItem & ":" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVALORItem")).Text = """" & m_ValorItem & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_stxtSi & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_stxtNo & """"
            'fin jpa Tarea 629
        Else
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "ESPEC")).Text = """" & "N" & """"
        End If
 
        Set g_adoresArt = oGestorInformes.CicloVidaProcesoGruposItems(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, m_iNivel, m_oProceso.Estado, iTipoGr, iTipoIt, CBool(chkIncEsp.Value), dequivalencia)
 
        If Not g_adoresArt Is Nothing Then
            If m_iNivel <= 2 Then
                optNivelIt = "N"
            Else
                optNivelIt = "S"
            End If
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptNivelIt")).Text = """" & optNivelIt & """"
            SubListado.Database.SetDataSource g_adoresArt
        End If
    End If
    
    
    '*** DESTINOS
    Set g_adoresDG = oGestorInformes.AperturaListadoDestinos(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, txtCod.Text, m_iNivel)
    If Not g_adoresDG Is Nothing Then
        Set SubListado = oReport.OpenSubreport("rptDestinos")
        SubListado.Database.SetDataSource g_adoresDG
        sOpt = "S"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptDest")).Text = """" & sOpt & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDestinos")).Text = """" & m_stxtDestinos & """"
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptDest")).Text = """" & sOpt & """"
    End If
    

    '*** FORMAS DE PAGO
    Set g_adoresDG = oGestorInformes.AperturaListadoFormasDePago(sdbcAnyo.Value, sdbcGMN1Proce_4Cod.Text, txtCod.Text, m_iNivel)
    If Not g_adoresDG Is Nothing Then
        Set SubListado = oReport.OpenSubreport("rptPagos")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPagos")).Text = """" & m_stxtPagos & """"
        sOpt = "S"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPag")).Text = """" & sOpt & """"
        SubListado.Database.SetDataSource g_adoresDG
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPag")).Text = """" & sOpt & """"
    End If

    
    '*** PROVEEDORES ACTUALES
    If m_oProceso.DefProveActual <> NoDefinido Then
        Set g_adoresDG = oGestorInformes.AperturaListadoProveedoresAct(sdbcAnyo.Value, sdbcGMN1Proce_4Cod.Text, txtCod.Text, m_iNivel)
        If Not g_adoresDG Is Nothing Then
            Set SubListado = oReport.OpenSubreport("rptProves")
            SubListado.Database.SetDataSource g_adoresDG
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProveAct")).Text = """" & sOpt & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProves")).Text = """" & m_stxtProveActs & """"
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProveAct")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProveAct")).Text = """" & sOpt & """"
    End If
    
    '*** SOLICITUDES DE COMPRAS
    If m_oProceso.DefSolicitud <> NoDefinido Then
        Set g_adoresDG = oGestorInformes.AperturaListadoSolicitud(sdbcAnyo.Value, sdbcGMN1Proce_4Cod.Text, txtCod.Text, m_iNivel)
        If Not g_adoresDG Is Nothing Then
            Set SubListado = oReport.OpenSubreport("rptSolicit")
            SubListado.Database.SetDataSource g_adoresDG
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptSolicitud")).Text = """" & sOpt & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSolicitudes")).Text = """" & m_stxtSolicitud & """"
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptSolicitud")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptSolicitud")).Text = """" & sOpt & """"
    End If
    
    '*** ATRIBUTOS
    If chkIncAtrib.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptAtributos")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributos")).Text = """" & m_stxtAtributos & """"
        
        Set g_adoresAtrib = oGestorInformes.CicloListaDetalleAtributos(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, m_iNivel, m_oProceso.Estado, iTipoGr)
        If Not g_adoresAtrib Is Nothing Then
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "MOSTRARDESCR")).Text = IIf(Me.chkDescr.Value = 1, 1, 0)
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTipo1")).Text = """" & m_stxtTexto & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTipo2")).Text = """" & m_stxtNum & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTipo3")).Text = """" & sFec & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTipo4")).Text = """" & m_stxtDuda & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtInterno")).Text = """" & m_stxtInterno & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtExterno")).Text = """" & m_stxtExterno & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtIntro")).Text = """" & m_stxtLibre & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSelec")).Text = """" & m_stxtSelec & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "Preferencia1")).Text = """" & m_stxtPreferBajo & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "Preferencia2")).Text = """" & m_stxtPreferAlto & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivel1")).Text = """" & m_stxtProceso & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivel2")).Text = """" & m_stxtGrupo & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivel3")).Text = """" & m_stxtItem & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "Oblig1")).Text = """" & m_stxtObligatorioS & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "Oblig2")).Text = """" & m_stxtObligatorioN & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTotal1")).Text = """" & m_stxtTotalOfer & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTotal2")).Text = """" & m_stxtTotalGrupo & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTotal3")).Text = """" & m_stxtTotalItem & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTotal4")).Text = """" & m_stxtUnitario & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAplicar1")).Text = """" & m_stxtAplicarS & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAplicar2")).Text = """" & m_stxtAplicarN & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAmb")).Text = """" & m_stxtAmb & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtribProce")).Text = """" & m_stxtNivP & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtribGrupo")).Text = """" & m_stxtNivG & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPedido")).Text = """" & m_stxtPedido & """"
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtrib")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresAtrib
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtrib")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtrib")).Text = """" & sOpt & """"
    End If
    
    
    '*** ESPECIFICACIONES
    If chkIncEsp.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptEspecificaciones")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEspecificaciones")).Text = """" & m_stxtEspecificaciones & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNombre")).Text = """" & m_stxtNomAr & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
        
        Set g_adoresEsp = oGestorInformes.CicloListaDetalleEspecificaciones(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, m_iNivel, m_oProceso.Estado, iTipoGr, iTipoIt)
        If Not g_adoresEsp Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEsp")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresEsp
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEsp")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEsp")).Text = """" & sOpt & """"
    End If
    
    
    '*** PERSONAS
    If m_oProceso.HayPersonas = True And chkIncPers.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptPersonas")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPersonas")).Text = """" & m_stxtPerson & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNom")).Text = """" & m_stxtNom & """" ' Hay que a�adir en la base de datos el nombre Articulos
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtRol")).Text = """" & m_stxtRol & """"
        Set g_adoresPer = oGestorInformes.CicloVidaProcesoPersonas(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)
        If Not g_adoresPer Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPer")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPer
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPer")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPer")).Text = """" & sOpt & """"
    End If
    
    
    '*** DISTRIBUCION UON
    If m_oProceso.DefDistribUON <> NoDefinido And chkIncUO.Value = vbChecked Then
        If m_iNivel = 3 And m_oProceso.DefDistribUON = EnProceso Then
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptOrg")).Text = """" & sOpt & """"
        Else
            Set SubListado = oReport.OpenSubreport("rptOrganizacion")
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOrganizacion")).Text = """" & m_stxtDistrOrg & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnds")).Text = """" & m_stxtUnds & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
            
            Set g_adoresOrg = oGestorInformes.CicloListaDetalleOrg(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, m_iNivel, m_oProceso.Estado, m_oProceso.DefDistribUON, iTipoGr, iTipoIt)
            If Not g_adoresOrg Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptOrg")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource g_adoresOrg
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptOrg")).Text = """" & sOpt & """"
            End If
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptOrg")).Text = """" & sOpt & """"
    End If
    
    
    '*** PRESANU1
    If m_oProceso.DefPresAnualTipo1 <> NoDefinido And chkIncPresup.Value = vbChecked Then
        If m_iNivel = 3 And m_oProceso.DefPresAnualTipo1 = EnProceso Then
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres1")).Text = """" & sOpt & """"
        Else
            Set SubListado = oReport.OpenSubreport("rptPres1")
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPresAnu & " " & gParametrosGenerales.gsSingPres1 & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPresItem & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDistribucion")).Text = """" & m_stxtUnds & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
            
            Set g_adoresPre1 = oGestorInformes.CicloListadoDetalladoPresupuestosN1(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, m_iNivel, m_oProceso.Estado, m_oProceso.DefPresAnualTipo1, iTipoGr, iTipoIt)
            If Not g_adoresPre1 Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres1")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource g_adoresPre1
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres1")).Text = """" & sOpt & """"
            End If
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres1")).Text = """" & sOpt & """"
    End If
    
    
    '*** PRESANU2
    If m_oProceso.DefPresAnualTipo2 <> NoDefinido And chkIncPresup.Value = vbChecked Then
        If m_iNivel = 3 And m_oProceso.DefPresAnualTipo2 = EnProceso Then
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres2")).Text = """" & sOpt & """"
        Else
            Set SubListado = oReport.OpenSubreport("rptPres2")
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPresAnu & " " & gParametrosGenerales.gsSingPres2 & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPresItem & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDistribucion")).Text = """" & m_stxtUnds & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
            
            Set g_adoresPre2 = oGestorInformes.CicloListadoDetalladoPresupuestosN2(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, m_iNivel, m_oProceso.Estado, m_oProceso.DefPresAnualTipo2, iTipoGr, iTipoIt)
            If Not g_adoresPre2 Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres2")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource g_adoresPre2
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres2")).Text = """" & sOpt & """"
            End If
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres2")).Text = """" & sOpt & """"
    End If
    
    
    '*** PRES1
    If m_oProceso.DefPresTipo1 <> NoDefinido And chkIncPresup.Value = vbChecked Then
        If m_iNivel = 3 And m_oProceso.DefPresTipo1 = EnProceso Then
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres3")).Text = """" & sOpt & """"
        Else
            Set SubListado = oReport.OpenSubreport("rptPres3")
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPresup & " " & gParametrosGenerales.gsSingPres3 & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPresItem & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDistribucion")).Text = """" & m_stxtUnds & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
            
            Set g_adoresPre3 = oGestorInformes.CicloListadoDetalladoPresupuestosN3(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, m_iNivel, m_oProceso.Estado, m_oProceso.DefPresTipo1, iTipoGr, iTipoIt)
            If Not g_adoresPre3 Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres3")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource g_adoresPre3
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres3")).Text = """" & sOpt & """"
            End If
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres3")).Text = """" & sOpt & """"
    End If
    
    
    '*** PRES2
    If m_oProceso.DefPresTipo2 <> NoDefinido And chkIncPresup.Value = vbChecked Then
        If m_iNivel = 3 And m_oProceso.DefPresTipo2 = EnProceso Then
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres4")).Text = """" & sOpt & """"
        Else
            Set SubListado = oReport.OpenSubreport("rptPres4")
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPresup & " " & gParametrosGenerales.gsSingPres4 & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPresItem & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDistribucion")).Text = """" & m_stxtUnds & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
            
            Set g_adoresPre4 = oGestorInformes.CicloListadoDetalladoPresupuestosN4(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, m_iNivel, m_oProceso.Estado, m_oProceso.DefPresTipo2, iTipoGr, iTipoIt)
            If Not g_adoresPre4 Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres4")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource g_adoresPre4
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres4")).Text = """" & sOpt & """"
            End If
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres4")).Text = """" & sOpt & """"
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sEspera(1) & " " & sTit
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    
    Set pv = New Preview
    If sOrigen = "frmPROCE" Then
        pv.g_sOrigen = sOrigen
    Else
        pv.g_sOrigen = "frmLstPROCE"
    End If
    pv.Hide
    pv.caption = sTit
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    Timer1.Enabled = True
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    
    Set oReport = Nothing
    Set SubListado = Nothing
    
    Unload Me
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ObtenerListadoDetallado", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub sstabProce_Click(PreviousTab As Integer)

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If (sstabProce.Tab = 2) Then
        
        If txtCod.Text <> "" And sdbcGMN1Proce_4Cod.Text <> "" Then
            sdbcOrdenar.Enabled = True
            sdbcOrdenar.Value = ""
            'Si hemos elegido un proceso concreto
            optCiclo.Enabled = True
            optDetallado.Enabled = True
        Else
            sdbcOrdenar.Enabled = True
            optCiclo.Enabled = False
            optDetallado.Enabled = False
            optResumido.Enabled = True
            optResumido.Value = True
        End If
        
        If Not optDetallado.Value Then
            picIncluir.Enabled = False
            lblNiv.Enabled = False
            chkIncAtrib.Enabled = False
            chkIncAtrib.Value = vbUnchecked
            
            chkDescr.Enabled = False
            chkDescr.Value = vbUnchecked
            
            chkIncEsp.Enabled = False
            chkIncEsp.Value = vbUnchecked
            chkIncPers.Enabled = False
            chkIncPers.Value = vbUnchecked
            chkIncUO.Enabled = False
            chkIncUO.Value = vbUnchecked
            chkIncPresup.Enabled = False
            chkIncPresup.Value = vbUnchecked
            lblGrupos.Enabled = False
            optGrAbiertos.Enabled = False
            optGrAbiertos.Value = False
            optGrCerrados.Enabled = False
            optGrCerrados.Value = False
            optGrTodos.Enabled = False
            optGrTodos.Value = False
            lblItem.Enabled = False
            optItAbiertos.Enabled = False
            optItAbiertos.Value = False
            optItCerrados.Enabled = False
            optItCerrados.Value = False
            optItTodos.Enabled = False
            optItTodos.Value = False
        End If
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "sstabProce_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub ObtenerListadoCicloVida()
Dim oReport As CRAXDRT.Report
Dim pv As Preview
Dim sMonedaSel As String
Dim sOpt As String
Dim SubListado As Object
Dim m_sEstado As String
Dim sOptDestinoArt As String
Dim sCompradorCod As String
Dim dEquivMon As Double
Dim dblEquiv As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If crs_Connected = False Then
        Exit Sub
    End If
    
    
    udtBusquedaProc = Cargar
    
    If Not g_bEsInvitado Then
        Set oProceEncontrados = oFSGSRaiz.generar_CProcesos
        oProceEncontrados.BuscarTodosLosProcesos 1, sdbcAnyo.Text, sinitems, Cerrado, TipoOrdenacionProcesos.OrdPorCod, udtBusquedaProc
        If oProceEncontrados.Count = 0 Then
            oMensajes.PermisoDenegadoProceso
            Exit Sub
        End If
        Set oProceEncontrados = Nothing
    End If
        
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptCicloProc.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
               
    Screen.MousePointer = vbHourglass
    Set m_oProceso = oFSGSRaiz.Generar_CProceso
    m_oProceso.Anyo = sdbcAnyo
    m_oProceso.GMN1Cod = sdbcGMN1Proce_4Cod.Text
    m_oProceso.Cod = txtCod.Text
    m_oProceso.CargarDatosGeneralesProceso
    
    Select Case m_oProceso.DefDestino
        Case EnProceso
            sOptDestinoArt = "P"
        Case EnGrupo
            sOptDestinoArt = "G"
        Case EnItem
            sOptDestinoArt = "I"
    End Select

    sTit = sTituloI & " - " & sTitulo(1)
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & m_stxtListado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    
    If sdbcMonCodigo.Text = "" Then
        oMonedas.CargarTodasLasMonedasDesde 1, m_oProceso.MonCod, , , True, , True
        sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        dequivalencia = oMonedas.Item(1).Equiv
    End If
    dEquivMon = dequivalencia
    dequivalencia = dequivalencia / m_oProceso.Cambio
    sText = DividirFrase(m_sIdiMon1)
    sMonedaSel = sText(1) & sMoneda & sText(2)
    sMonedaSel = sMonedaSel & "; " & sdbcMonCodigo.Columns(2).caption & ": " & dEquivMon
        
    oReport.FormulaFields(crs_FormulaIndex(oReport, "Moneda")).Text = """" & sMonedaSel & """"
    
    'Indica al report si se va a mostrar o no las solicitudes
    If m_bMostrarSolicitud = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & "N" & """"
    End If
    
    '*** DATOS GENERALES
    Set SubListado = oReport.OpenSubreport("rptDatosGenerales")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDatosGenerales")).Text = """" & m_stxtDatos & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCod")).Text = """" & m_stxtCod & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDen")).Text = """" & m_stxtDen & ":" & """"
    Select Case m_oProceso.Estado
        Case 1:
            m_sEstado = sEstados(1)
        Case 2:
            m_sEstado = sEstados(1)
        Case 3:
            m_sEstado = sEstados(2)
        Case 4:
            m_sEstado = sEstados(2)
        Case 5:
            m_sEstado = sEstados(3)
        Case 6:
            m_sEstado = sEstados(4)
        Case 7:
            m_sEstado = sEstados(4)
        Case 8:
            m_sEstado = sEstados(5)
        Case 9:
            m_sEstado = sEstados(5)
        Case 10:
            m_sEstado = sEstados(6)
        Case 11:
            m_sEstado = sEstados(12)
        Case 12:
            m_sEstado = sEstados(7)
        Case 13:
            m_sEstado = sEstados(8)
        Case 20:
            m_sEstado = sEstados(9)
    End Select
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst")).Text = """" & m_stxtEstado & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "Estado")).Text = """" & m_sEstado & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDest")).Text = """" & m_stxtDest & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjReu")).Text = """" & m_stxtReu & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjDir")).Text = """" & m_stxtDir & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecAdj")).Text = """" & m_stxtAdjudicacion & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecPres")).Text = """" & m_stxtPresentacion & ":" & """"
    If m_oProceso.PermitirAdjDirecta = True Then
        sOpt = "S"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptTipo")).Text = """" & sOpt & """"
    Else
        sOpt = "N"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptTipo")).Text = """" & sOpt & """"
    End If
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecNec")).Text = """" & m_stxtNece & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecLim")).Text = """" & m_stxtLimitOfer & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecApe")).Text = """" & m_stxtAper & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecVal")).Text = """" & m_stxtValidacion & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecValSProv")).Text = """" & m_stxtFecValSProv & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEstProc")).Text = """" & m_stxtEstProc & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEstProc2")).Text = """" & m_stxtEstProc2 & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEstProc2")).Text = """" & m_stxtEstProc2 & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtInicioSubasta")).Text = """" & m_stxtInicioSubasta & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFinSubasta")).Text = """" & m_stxtCierreSubasta & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEspTexto")).Text = """" & m_stxtEspec & """"

          
    Set g_adoresDG = oGestorInformes.CicloVidaProcesoDatosGenerales(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, True)
    If Not g_adoresDG Is Nothing Then
        SubListado.Database.SetDataSource g_adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
            
    'jpa Tarea 629 -------------------------------------------------------------
    'He a�adido mas lineas de detalle en el subreport
    'Substituire la antigua select GRUPOS-ITEMS por una union de GRUPOS-ITEMS-ATRIBUTOS DE ESPECIFICACION
    'fin jpa Tarea 629
    '*** GRUPOS-ITEMS
    Set SubListado = oReport.OpenSubreport("rptArticulos")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtItems")).Text = """" & m_stxtitems & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGrupo")).Text = """" & m_stxtGrupo & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDest")).Text = """" & m_stxtDest & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGDest")).Text = """" & m_stxtDest & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPago")).Text = """" & m_stxtPago & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGPago")).Text = """" & m_stxtPago & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtIni")).Text = """" & m_stxtIni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGIni")).Text = """" & m_stxtIni & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFin")).Text = """" & m_stxtFin & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGFin")).Text = """" & m_stxtFin & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProveAct")).Text = """" & m_stxtProve & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGProveAct")).Text = """" & m_stxtProve & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnd")).Text = """" & m_stxtUnd & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCant")).Text = """" & m_stxtCant & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec")).Text = """" & m_stxtPresUni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & sOrdenar(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSolicit")).Text = """" & m_stxtSolicit & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGSolicitud")).Text = """" & m_stxtSolicit & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtBajMinPuja")).Text = """" & m_stxtBajMinPuja & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEspTexto")).Text = """" & m_stxtEspec & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOBJ")).Text = """" & m_stxtObj & """"
    'jpa Tarea 629
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributosEspecificacion")).Text = """" & m_AtributosEspecificacion & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDENOM")).Text = """" & m_Denominacion & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVALOR")).Text = """" & m_stxtValor & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributosEspecificacionItem")).Text = """" & m_AtributosEspecificacionItem & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDENOMItem")).Text = """" & m_DenominacionItem & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVALORItem")).Text = """" & m_ValorItem & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_stxtSi & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_stxtNo & """"
    
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresEsc")).Text = """" & m_stxtPresEsc & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCantIni")).Text = """" & m_stxtCantIni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCantFinal")).Text = """" & m_stxtCantFin & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresUni")).Text = """" & m_stxtPresUniEsc & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAnyoImput")).Text = """" & m_stxtAnyoImput & """"
    
    'fin jpa Tarea 629
    'Indica al subreport si se va a mostrar o no las solicitudes
    If m_bMostrarSolicitud = True Then
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "vSolicit")).Text = """" & "S" & """"
    Else
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "vSolicit")).Text = """" & "N" & """"
    End If
    
    Set g_adoresArt = oGestorInformes.CicloVidaProcesoGruposItems(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, , , , , True, dequivalencia)
    If Not g_adoresArt Is Nothing Then
        sOpt = "S"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptArt")).Text = """" & sOpt & """"
        SubListado.Database.SetDataSource g_adoresArt
    
    Else
        sOpt = "N"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOptArt")).Text = """" & sOpt & """"
    End If
    'fin jpa Tarea 629
    
    '*** DESTINOS
    Set g_adoresDG = oGestorInformes.AperturaListadoDestinos(sdbcAnyo.Value, sdbcGMN1Proce_4Cod.Text, txtCod.Text, m_oProceso.DefDestino)
    If Not g_adoresDG Is Nothing Then
        Set SubListado = oReport.OpenSubreport("rptDestinos")
        SubListado.Database.SetDataSource g_adoresDG
        sOpt = "S"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptDest")).Text = """" & sOpt & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDestinos")).Text = """" & m_stxtDestinos & """"
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptDest")).Text = """" & sOpt & """"
    End If
    

    '*** FORMAS DE PAGO
    Set g_adoresDG = oGestorInformes.AperturaListadoFormasDePago(sdbcAnyo.Value, sdbcGMN1Proce_4Cod.Text, txtCod.Text, m_oProceso.DefFormaPago)
    If Not g_adoresDG Is Nothing Then
        Set SubListado = oReport.OpenSubreport("rptPagos")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPagos")).Text = """" & m_stxtPagos & """"
        sOpt = "S"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPag")).Text = """" & sOpt & """"
        SubListado.Database.SetDataSource g_adoresDG
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPag")).Text = """" & sOpt & """"
    End If

    
    '*** PROVEEDORES ACTUALES
    If m_oProceso.DefProveActual <> NoDefinido Then
        Set g_adoresDG = oGestorInformes.AperturaListadoProveedoresAct(sdbcAnyo.Value, sdbcGMN1Proce_4Cod.Text, txtCod.Text, m_oProceso.DefProveActual)
        If Not g_adoresDG Is Nothing Then
            Set SubListado = oReport.OpenSubreport("rptProves")
            SubListado.Database.SetDataSource g_adoresDG
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProveAct")).Text = """" & sOpt & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProves")).Text = """" & m_stxtProveActs & """"
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProveAct")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProveAct")).Text = """" & sOpt & """"
    End If
    
    '*** SOLICITUDES DE COMPRAS
    If m_bMostrarSolicitud = True Then
        If m_oProceso.DefSolicitud <> NoDefinido Then
            Set g_adoresDG = oGestorInformes.AperturaListadoSolicitud(sdbcAnyo.Value, sdbcGMN1Proce_4Cod.Text, txtCod.Text, m_oProceso.DefSolicitud)
            If Not g_adoresDG Is Nothing Then
                Set SubListado = oReport.OpenSubreport("rptSolicitudes")
                SubListado.Database.SetDataSource g_adoresDG
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & sOpt & """"
                SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSolicitudes")).Text = """" & m_stxtSolicitud & """"
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & sOpt & """"
            End If
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & sOpt & """"
        End If
    End If
    
    '*** ESPECIFICACIONES
    If m_oProceso.DefEspecificaciones Or m_oProceso.DefEspGrupos Or m_oProceso.DefEspItems Then
        Set SubListado = oReport.OpenSubreport("rptEspecificaciones")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEspecificaciones")).Text = """" & m_stxtEspecificaciones & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNombre")).Text = """" & m_stxtNomAr & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
       
        Set g_adoresEsp = oGestorInformes.CicloVidaProcesoEspecificaciones(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)
        If Not g_adoresEsp Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEsp")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresEsp
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEsp")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEsp")).Text = """" & sOpt & """"
    End If
    
    If sdbcMonCodigo.Text = "" Then
        dblEquiv = 1
    Else
        dblEquiv = dEquivMon
    End If

    '*** edu T94 Atributos de especificacion -----------------------------------------------------------------
    'm_oProceso.CargarAtributosEspecificacion
    'If Not m_oProceso.AtributosEspecificacion Is Nothing Then
    '    Set SubListado = oReport.OpenSubreport("rptAtributosEspecificacion.rpt")
    '    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributosEspecificacion")).Text = """" & m_AtributosEspecificacion & """"
    '    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDENOM")).Text = """" & m_Denominacion & ":" & """"
    '    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVALOR")).Text = """" & m_stxtValor & """"
    '    Dim RS As Recordset
    '    Set RS = m_oProceso.AtributosEspecificacion.Seleccionar(m_oProceso, AmbProceso)
    '    SubListado.Database.SetDataSource RS
    '    Set RS = Nothing
    'End If
    
    'jpa Tarea 629 ---------------------------------------------------------------------------------------------------------------------------------
    '*** PROCESO-ATRIBUTOSESPECIFICACION
    Set SubListado = oReport.OpenSubreport("rptAtributosEspecificacion.rpt")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributosEspecificacion")).Text = """" & m_AtributosEspecificacion & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDENOM")).Text = """" & m_Denominacion & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVALOR")).Text = """" & m_stxtValor & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_stxtSi & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_stxtNo & """"
    Set g_adoresArt = oGestorInformes.CicloVidaProcesoAtributosEspecificacion(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)
    
    If Not g_adoresArt Is Nothing Then
        sOpt = "S"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEspAtr")).Text = """" & sOpt & """"
        SubListado.Database.SetDataSource g_adoresArt
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptEspAtr")).Text = """" & sOpt & """"
    End If
    
    'fin jpa Tarea 629 ---------------------------------------------------------------------------------------------------------------------------------
    
    '*** ADJUDICACIONES
    If m_oProceso.Estado >= ConObjetivosSinNotificarYPreadjudicado Then
        Set SubListado = oReport.OpenSubreport("rptAdjudicaciones")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjud")).Text = """" & m_stxtAdjudicaciones & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProveedor")).Text = """" & m_stxtProve & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtImporte")).Text = """" & m_stxtImp & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCompradorCod")).Text = """" & sCompradorCod & """"
        Set g_adoresAdj = oGestorInformes.CicloVidaProcesoAdjudicaciones(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, dblEquiv)
        If Not g_adoresAdj Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdj")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresAdj
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdj")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdj")).Text = """" & sOpt & """"
    End If
    
    
    '*** PERSONAS
    If m_oProceso.HayPersonas = True Then
        Set SubListado = oReport.OpenSubreport("rptPersonas")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPersonas")).Text = """" & m_stxtPerson & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNom")).Text = """" & m_stxtNom & """" ' Hay que a�adir en la base de datos el nombre Articulos
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtRol")).Text = """" & m_stxtRol & """"
        Set g_adoresPer = oGestorInformes.CicloVidaProcesoPersonas(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)
        If Not g_adoresPer Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPer")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPer
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPer")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPer")).Text = """" & sOpt & """"
    End If
    
    
    '*** PRESANU1
    If m_oProceso.DefPresAnualTipo1 <> NoDefinido Then
        Set SubListado = oReport.OpenSubreport("rptPres1")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPresAnu & " " & gParametrosGenerales.gsSingPres1 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPresItem & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDistribucion")).Text = """" & m_stxtUnds & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
        
        Set g_adoresPre1 = oGestorInformes.CicloVidaProcesoPresupuestosN1(sdbcGMN1Proce_4Cod.Text, sdbcAnyo.Value, txtCod.Text, m_oProceso.DefPresAnualTipo1)
        If Not g_adoresPre1 Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres1")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPre1
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres1")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres1")).Text = """" & sOpt & """"
    End If
    
    
    '*** PRESANU2
    If m_oProceso.DefPresAnualTipo2 <> NoDefinido Then
        Set SubListado = oReport.OpenSubreport("rptPres2")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPresAnu & " " & gParametrosGenerales.gsSingPres2 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPresItem & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDistribucion")).Text = """" & m_stxtUnds & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
        
        Set g_adoresPre2 = oGestorInformes.CicloVidaProcesoPresupuestosN2(sdbcGMN1Proce_4Cod.Text, sdbcAnyo.Value, txtCod.Text, m_oProceso.DefPresAnualTipo2)
        If Not g_adoresPre2 Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres2")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPre2
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres2")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres2")).Text = """" & sOpt & """"
    End If
    
    
    '*** PRES1
    If m_oProceso.DefPresTipo1 <> NoDefinido Then
        Set SubListado = oReport.OpenSubreport("rptPres3")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPresup & " " & gParametrosGenerales.gsSingPres3 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPresItem & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDistribucion")).Text = """" & m_stxtUnds & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
        
        Set g_adoresPre3 = oGestorInformes.CicloVidaProcesoPresupuestosN3(sdbcGMN1Proce_4Cod.Text, sdbcAnyo.Value, txtCod.Text, m_oProceso.DefPresTipo1)
        If Not g_adoresPre3 Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres3")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPre3
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres3")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres3")).Text = """" & sOpt & """"
    End If
    
    
    '*** PRES2
    If m_oProceso.DefPresTipo2 <> NoDefinido Then
        Set SubListado = oReport.OpenSubreport("rptPres4")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPresup & " " & gParametrosGenerales.gsSingPres4 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPresItem & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDistribucion")).Text = """" & m_stxtUnds & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtPorcen & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivProce")).Text = """" & m_stxtNivP & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivGrupo")).Text = """" & m_stxtNivG & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNivItem")).Text = """" & m_stxtNivI & ":" & """"
        
        Set g_adoresPre4 = oGestorInformes.CicloVidaProcesoPresupuestosN4(sdbcGMN1Proce_4Cod.Text, sdbcAnyo.Value, txtCod.Text, m_oProceso.DefPresTipo2)
        If Not g_adoresPre4 Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres4")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPre4
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres4")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptPres4")).Text = """" & sOpt & """"
    End If
                        
                        
    '*** PROVEEDORES SELECCIONADOS
    If m_oProceso.Estado > validado Then
        Set SubListado = oReport.OpenSubreport("rptProvSel")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProveAsig")).Text = """" & m_stxtProvAsign & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCodProv")).Text = """" & m_stxtProveedor & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCodCom")).Text = """" & m_stxtComp2 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNom")).Text = """" & m_stxtNom & """"
        If Not m_oProceso.responsable Is Nothing Then
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComprador")).Text = """" & m_stxtCompRes & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CompradorCod")).Text = """" & m_oProceso.responsable.Cod & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CompradorEqp")).Text = """" & m_oProceso.responsable.codEqp & """"
        End If
      
        Set g_adoresPro = oGestorInformes.CicloVidaSeleccionProves(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)
        If Not g_adoresPro Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProvSel")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPro
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProvSel")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptProvSel")).Text = """" & sOpt & """"
    End If
    
        
    '*** OFERTAS
    If m_oProceso.Estado >= conofertas Then
        Set SubListado = oReport.OpenSubreport("rptOfertas")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOfertas")).Text = """" & m_stxtOfertas & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecha")).Text = """" & Left(m_stxtfec, Len(m_stxtfec) - 1) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNumOfer")).Text = """" & Left(m_stxtNumOfe, Len(m_stxtNumOfe) - 1) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProveedor")).Text = """" & m_stxtProve & """"
        Set g_adoresOfe = oGestorInformes.CicloVidaProcesoOfertas(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)
        If Not g_adoresOfe Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptOfer")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresOfe
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptOfer")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptOfer")).Text = """" & sOpt & """"
    End If
        
    
    '*** REUNIONES
    If m_oProceso.Estado >= validado And m_oProceso.PermitirAdjDirecta = False Then
        Set SubListado = oReport.OpenSubreport("rptReuniones")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtReuniones")).Text = """" & m_stxtReuniones & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecha")).Text = """" & Left(m_stxtfec, Len(m_stxtfec) - 1) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtHora")).Text = """" & m_stxtHora & """"
        Set g_adoresReu = oGestorInformes.CicloVidaProcesoReuniones(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)
        If Not g_adoresReu Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptReu")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresReu
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptReu")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptReu")).Text = """" & sOpt & """"
    End If
           
    
    '*** AHORROS
    If m_oProceso.Estado >= ParcialmenteCerrado Then
        Set SubListado = oReport.OpenSubreport("rptAhorro")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAhorro")).Text = """" & m_stxtAho & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtConsumido")).Text = """" & m_stxtConsumido & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjudicado")).Text = """" & m_stxtAdjudicado & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAhorrado")).Text = """" & m_stxtAhorrado & """"
        Set g_adoresAho = oGestorInformes.CicloVidaProcesoAhorro(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text, dblEquiv)
        If Not g_adoresAho Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAho")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresAho
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAho")).Text = """" & sOpt & """"
    End If
    
    
    '*** COMUNICACIONES
    If m_oProceso.Estado >= conpeticiones Then
        Set SubListado = oReport.OpenSubreport("rptComunicaciones")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComunicaciones")).Text = """" & sTitulo(3) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecha")).Text = """" & Left(m_stxtfec, Len(m_stxtfec) - 1) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTipo")).Text = """" & m_stxtTipo & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtContacto")).Text = """" & Left(m_stxtContac, Len(m_stxtContac) - 1) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProveedor")).Text = """" & m_stxtProveedor & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPetOfer")).Text = """" & m_stxtCPet & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComuObj")).Text = """" & m_stxtCObj & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjudica")).Text = """" & m_stxtCAdj & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtExclusion")).Text = """" & m_stxtCExc & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAviso")).Text = """" & m_stxtCAvi & """"
        
        Set g_adoresComu = oGestorInformes.CicloVidaComunicaciones(sdbcGMN1Proce_4Cod.Text, sdbcAnyo, txtCod.Text)
        If Not g_adoresComu Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptComun")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresComu
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptComun")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptComun")).Text = """" & sOpt & """"
    End If
    
    
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sEspera(1) & " " & sTit
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    
    Set pv = New Preview
    If sOrigen = "frmPROCE" Then
        pv.g_sOrigen = sOrigen
    Else
        pv.g_sOrigen = "frmLstPROCE"
    End If
    pv.Hide
    pv.caption = sTit
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    
    Set oReport = Nothing
    Set SubListado = Nothing
    
    Unload Me
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ObtenerListadoCicloVida", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub ObtenerListadoSelProve()
    ' Listado de Proveedores seleccionados por procesos
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim SubListado As CRAXDRT.Report
    Dim oCRProceso As CRProceso
    Dim sSeleccion1 As String
    Dim sAux As String
    Dim Adores As Ador.Recordset
    Dim OrdenRpt(1 To 2, 1 To 4) As String
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRProceso = GenerarCRProceso
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPROCEProveSel.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Set oReport = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    Screen.MousePointer = vbHourglass
        
    udtBusquedaProc = Cargar
    
    sSeleccion = GenerarTextoSeleccion
    
    sTit = sTituloI & " - " & sTitulo(2)
    If chkSelProvePot Then
       sSeleccion1 = m_stxtProvePot
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sTit & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL1")).Text = """" & sSeleccion1 & """"

    '***** Textos ****************************
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & m_stxtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComp")).Text = """" & m_stxtComp & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCal")).Text = """" & m_stxtCal & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = """" & sOrdenar(1) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenominacion")).Text = """" & sOrdenar(2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAsignado")).Text = """" & m_stxtAsignado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSi")).Text = """" & m_stxtSi & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNo")).Text = """" & m_stxtNo & """"
    
    
    '********** FORMULA FIELDS REPORT ****************************
    sAux = "N"
    If chkSelProveEqp.Value = vbChecked Then 'solo equipos
        sAux = "S"
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "EQUIPOS")).Text = """" & sAux & """"
    sAux = "N"
    If chkSelProveComp.Value = vbChecked Then
        sAux = "S"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "EQUIPOS")).Text = """" & sAux & """"
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "COMPRADORES")).Text = """" & sAux & """"
    
    sAux = "N"
    If chkSelProveCal.Value = vbChecked Then
        sAux = "S"
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "CALIFICACIONES")).Text = """" & sAux & """"
    
    sAux = "N"
    If chkSelProvePot.Value = vbChecked Then
        sAux = "S"
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "POTENCIALES")).Text = """" & sAux & """"
    
    sAux = "N"
    If chkSelProveGrupos.Value = vbChecked And gParametrosGenerales.gbProveGrupos Then
        sAux = "S"
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GRUPOS")).Text = """" & sAux & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "CAL1NOM")).Text = """" & gParametrosGenerales.gsDEN_CAL1 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "CAL2NOM")).Text = """" & gParametrosGenerales.gsDEN_CAL2 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "CAL3NOM")).Text = """" & gParametrosGenerales.gsDEN_CAL3 & """"
    
    OrdenRpt(2, 1) = "ORDEN_GRUPO1"
    OrdenRpt(2, 2) = "ORDEN_GRUPO_2"
    OrdenRpt(2, 3) = "ORDEN_GRUPO_3"
    OrdenRpt(2, 4) = "ORDEN_GRUPO_4"
    If optSelProveOrdCod.Value Then 'Por defecto ordenado por codigo
        OrdenRpt(1, 1) = "{@PROCESO}"
        OrdenRpt(1, 2) = "{SelProve_ttx.PROVECOD}"
        OrdenRpt(1, 3) = "{SelProve_ttx.EQPCOD}"
        OrdenRpt(1, 4) = "{SelProve_ttx.COMCOD}"
        
    Else
        OrdenRpt(1, 1) = "{SelProve_ttx.PROCEDEN}"
        OrdenRpt(1, 2) = "{SelProve_ttx.PROVEDEN}"
        OrdenRpt(1, 3) = "{SelProve_ttx.EQPDEN}"
        OrdenRpt(1, 4) = "{SelProve_ttx.COMAPE}"
    End If
    For i = 1 To UBound(OrdenRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, OrdenRpt(2, i))).Text = OrdenRpt(1, i)
    Next i

    Set Adores = oGestorInformes.ListadoPROCEProveSel(val(sdbcAnyo), DesdeEst, HastaEst, optSelProveOrdCod.Value, udtBusquedaProc, chkSelProvePot, gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
        oReport.Database.SetDataSource Adores
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    If chkSelProveGrupos.Value = vbChecked And gParametrosGenerales.gbProveGrupos Then
        Set SubListado = oReport.OpenSubreport("rptSelProveGrupos")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGruAsig")).Text = """" & m_stxtGruAsig & """"
        Set g_adoresAtrib = oGestorInformes.ListadoPROCEProveSelGrupos(val(sdbcAnyo), DesdeEst, HastaEst, optSelProveOrdCod.Value, udtBusquedaProc, gParametrosInstalacion.gIdioma)
        SubListado.Database.SetDataSource g_adoresAtrib
        Set SubListado = Nothing
    End If

        
    If oReport Is Nothing Then
           Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set pv = New Preview
    Me.Hide
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
        
    frmESPERA.lblGeneral.caption = sEspera(1) & " " & sTit
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.caption = sTit
    pv.crViewer.ViewReport
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oCRProceso = Nothing
    
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ObtenerListadoSelProve", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub ObtenerListadoOFEPet()
    Dim oReport As CRAXDRT.Report
    Dim SubListado As CRAXDRT.Report
    Dim oCRProceso As CRProceso
    Dim pv As Preview
    Dim sVer As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
  
    Set oCRProceso = GenerarCRProceso
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptOFEPet.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
        
    udtBusquedaProc = Cargar
        
    sSeleccion = GenerarTextoSeleccion

    sTit = sTituloI & " - " & sTitulo(3)
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & Trim(sSeleccion) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sTit & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & m_stxtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFec")).Text = """" & m_stxtfec & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNece")).Text = """" & m_stxtNece & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLimi")).Text = """" & m_stxtLimi & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPresen")).Text = """" & m_stxtPresen & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = """" & m_stxtProve & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPubWeb")).Text = """" & m_stxtPubWeb & """"

    ' SUBREPORT
    Set SubListado = oReport.OpenSubreport("PETIC_PROVE")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCAdj")).Text = """" & m_stxtCAdj & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCar")).Text = """" & m_stxtCar & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCarta")).Text = """" & m_stxtCarta & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCExc")).Text = """" & m_stxtCExc & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCObj")).Text = """" & m_stxtCObj & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComunic")).Text = """" & m_stxtComunic & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtContac")).Text = """" & m_stxtContac & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCPet")).Text = """" & m_stxtCPet & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEmail")).Text = """" & m_stxtEmail & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecCom")).Text = """" & m_stxtFecCom & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecDec")).Text = """" & m_stxtFecDec & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTfno")).Text = """" & m_stxtTfno & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTipo")).Text = """" & m_stxtTipo & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtWeb")).Text = """" & m_stxtWeb & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCAvi")).Text = """" & m_stxtCAvi & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCAnul")).Text = """" & m_stxtCAnul & """"
    Set SubListado = Nothing
    
    sVer = ""
    If chkComPet.Value = vbChecked Then
        sVer = "1"
    Else
        sVer = "0"
    End If
    If chkComObj.Value = vbChecked Then
        sVer = sVer & "1"
    Else
        sVer = sVer & "0"
    End If
    If chkComAdj.Value = vbChecked Then
        sVer = sVer & "1"
    Else
        sVer = sVer & "0"
    End If
    If chkComExc.Value = vbChecked Then
        sVer = sVer & "1"
    Else
        sVer = sVer & "0"
    End If
    If chkComAvi.Value = vbChecked Then
        sVer = sVer & "1"
    Else
        sVer = sVer & "0"
    End If
    If chkComAnulacion.Value = vbChecked Then
        sVer = sVer & "1"
    Else
        sVer = sVer & "0"
    End If
    
    If Not oCRProceso.ListadoOFEPet(oGestorInformes, oReport, val(sdbcAnyo), DesdeEst, HastaEst, OrdenListado, udtBusquedaProc, txtComDesde.Text, txtComHasta.Text, sVer, chkComSinPet, chkComPubWeb) Then
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set pv = New Preview
    Me.Hide
    pv.Hide
    pv.caption = sTit
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.lblGeneral.caption = sEspera(1) & " " & sTit
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    frmESPERA.Show
    frmESPERA.ZOrder 0
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ObtenerListadoOFEPet", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


Private Sub ObtenerListadoOfeRec()
    ' Listado de OFERTAS RECIBIDAS por procesos
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim oCRProceso As CRProceso
    Dim ListadoPorItems As Boolean
    Dim sProveSin As String
    Dim sMonedaSel As String
    Dim bAdjuntos As Boolean
    Dim bAtributos As Boolean
    Dim MostrarItemAbierto As Integer
    Dim MostrarGrupoAbierto As Integer
    Dim sAdjuntos As String
    Dim sAtributos As String
    Dim bMostrarReport As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCRProceso = GenerarCRProceso
    
    MostrarItemAbierto = 2
    MostrarGrupoAbierto = 2
    
    bHayEscalados = False
            
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    bAdjuntos = False
    bAtributos = False
    
    If chkOfeRecSin.Value = vbChecked Then
        ListadoPorItems = False
    Else
        If optOfeRecProve Then
            ListadoPorItems = False
            If chkOfeRecAdjuntos.Value = vbChecked Then
                bAdjuntos = True
            End If
            If chkOfeRecAtributos.Value = vbChecked Then
                bAtributos = True
            End If
        Else
            ListadoPorItems = True
        End If
    End If
    
    If ListadoPorItems Then
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPROCEOfeRecItem.rpt"
    Else
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPROCEOfeRecProv.rpt"
    End If
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    Screen.MousePointer = vbHourglass
    
    udtBusquedaProc = Cargar
    sSeleccion = GenerarTextoSeleccion

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    '*********** FORMULA FIELDS REPORT
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    If chkOfeRecUltProve.Value = vbChecked Or chkOfeRecUltItem.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL1")).Text = """" & m_stxtUltOfe & """"
    End If

    If chkOfeRecSin.Value = vbChecked Then
        sProveSin = "S"
       sTit = sTituloI & " - " & m_stxtProveSin
    Else
        sProveSin = "N"
        sTit = sTituloI & " - " & sTitulo(4)
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "PROVESINOFE")).Text = """" & sProveSin & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTit & """"
    
    sMonedaSel = ""
    If dequivalencia <> 0 Then
        sText = DividirFrase(m_sIdiMon1)
        sMonedaSel = sText(1) & sMoneda & sText(2)
    Else
         sMonedaSel = m_sIdiMon3
    End If
    
    If bAdjuntos Then
        sAdjuntos = "S"
    Else
        sAdjuntos = "N"
    End If
    
    If bAtributos Then
        sAtributos = "S"
    Else
        sAtributos = "N"
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & sMonedaSel & """"
    If Not ListadoPorItems Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "ADJUNTOS")).Text = """" & sAdjuntos & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "ATRIBUTOS")).Text = """" & sAtributos & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtORIGENGS")).Text = """" & sIdiOrigenGS & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtORIGENPORTAL")).Text = """" & sIdiOrigenPORTAL & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUSU")).Text = """" & sIdiUSU & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtContacto")).Text = """" & sIdiContacto & """"
    End If
    oReport.ParameterFields(crs_ParameterIndex(oReport, "EQUIV")).SetCurrentValue dequivalencia, 7
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & m_stxtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLimitOfer")).Text = """" & m_stxtLimitOfer & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & m_stxtGrupo & ":" & """"
    
    'Indica al report si se va a mostrar o no las solicitudes
    If m_bMostrarSolicitud = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "vMostrarSolicit")).Text = """" & "N" & """"
    End If
    
    If ListadoPorItems Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGDest")).Text = """" & m_stxtDest & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGFin")).Text = """" & m_stxtFin & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGIni")).Text = """" & m_stxtIni & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGPago")).Text = """" & m_stxtPago & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGProveAct")).Text = """" & m_stxtProvAct & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGSolicitud")).Text = """" & m_stxtSolicit & ":" & """"
        
    Else
        
        If chkOfeRecItems.Value = vbChecked Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "ARTICULOS")).Text = """S"""
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "ARTICULOS")).Text = """N"""
        End If
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = """" & m_stxtProve & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNumOfe")).Text = """" & m_stxtNumOfe & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEstado")).Text = """" & m_stxtEstado & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecRec")).Text = """" & m_stxtFecRec & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecValidez")).Text = """" & m_stxtFecValidez & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMon")).Text = """" & m_stxtMon & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEquiv")).Text = """" & m_stxtEquiv & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtObs")).Text = """" & m_stxtObs & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtItems")).Text = """" & m_stxtitems & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCant")).Text = """" & m_stxtCant & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDest")).Text = """" & m_stxtDest & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFin")).Text = """" & m_stxtFin & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtIni")).Text = """" & m_stxtIni & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPago")).Text = """" & m_stxtPagoP & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrec")).Text = """" & m_stxtPresUni & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOfe")).Text = """" & m_stxtOfertaAnterior & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUnd")).Text = """" & m_stxtUnd & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtHomo")).Text = """" & m_stxtHomo & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & m_stxtGrupo & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrec1")).Text = """" & m_stxtPrecio1 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrec2")).Text = """" & m_stxtPrecio2 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrec3")).Text = """" & m_stxtPrecio3 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrecio1")).Text = """" & m_stxtPrecio1 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrecio2")).Text = """" & m_stxtPrecio2 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrecio3")).Text = """" & m_stxtPrecio3 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUsarPrec")).Text = """" & m_stxtUsarPrec & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCantMax")).Text = """" & m_stxtCantMax & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProveAct")).Text = """" & m_stxtProvAct & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGDest")).Text = """" & m_stxtDest & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGFin")).Text = """" & m_stxtFin & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGIni")).Text = """" & m_stxtIni & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGPago")).Text = """" & m_stxtPago & ":" & """"
        
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComentPrecio")).Text = """" & m_stxtComentPrec & """"
        
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPGProveAct")).Text = """" & m_stxtProvAct & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSolicit")).Text = """" & m_stxtSolicit & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSolic")).Text = """" & m_stxtSolic & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComent1")).Text = """" & m_stxtComent1 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComent2")).Text = """" & m_stxtComent2 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComent3")).Text = """" & m_stxtComent3 & """"
        
    End If
    
    If optItemAbiertos.Value = True Then
        MostrarItemAbierto = 1
    End If
    
    If optItemCerrados.Value = True Then
        MostrarItemAbierto = 0
    End If
    
    If optTodosItems.Value = True Then
        MostrarItemAbierto = 2
    End If
    
    If opOfeRecGAbiertos.Value = True Then
        MostrarGrupoAbierto = 1
    End If
    
    If opOfeRecGCerrados.Value = True Then
        MostrarGrupoAbierto = 0
    End If
    
    If opOfeRecTodosGrupos.Value = True Then
        MostrarGrupoAbierto = 2
    End If
    
    If chkOfeRecSin.Value = vbChecked Then
        If g_oProceSeleccionado Is Nothing Then
            bMostrarReport = ListadoProceOfeRecProve(oReport, MostrarItemAbierto, MostrarGrupoAbierto)
        Else
            bMostrarReport = ListadoProceOfeRecProve(oReport, MostrarItemAbierto, MostrarGrupoAbierto, g_oProceSeleccionado.PubMatProve)
        End If
        If bMostrarReport = False Then
            oMensajes.ImposibleMostrarInforme
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    Else
        If optOfeRecProve.Value = True Then
            If g_oProceSeleccionado Is Nothing Then
                bMostrarReport = ListadoProceOfeRecProve(oReport, MostrarItemAbierto, MostrarGrupoAbierto)
            Else
                bMostrarReport = ListadoProceOfeRecProve(oReport, MostrarItemAbierto, MostrarGrupoAbierto, g_oProceSeleccionado.PubMatProve)
            End If
            If bMostrarReport = False Then
                oMensajes.ImposibleMostrarInforme
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            bMostrarReport = ListadoProceOfeRecItem(oReport, MostrarItemAbierto, MostrarGrupoAbierto)
            If bMostrarReport = False Then
                oMensajes.ImposibleMostrarInforme
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
    End If
     
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set pv = New Preview
    Me.Hide
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
        
    frmESPERA.lblGeneral.caption = sEspera(1) & " " & sTit
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.caption = sTit
    If sOrigen = "frmOFERec" Then
        pv.g_sOrigen = sOrigen
    Else
        pv.g_sOrigen = "frmLstOFERec"
    End If
    pv.g_sOrigen = sOrigen
    pv.crViewer.ViewReport
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oCRProceso = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ObtenerListadoOfeRec", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


''' <summary>
''' Obtiene el listado del ofertas por proveedor
''' </summary>
''' <param name="oReport">Report de CR.</param>
''' <param name="MostrarItemAbierto">Si solo se muestran items no cerrados, solo cerrados o todos</param>
''' <param name="MostrarGrupoAbierto">Si solo se muestran grupos no cerrados, solo cerrados o todos</param>
''' <param name="bPubMatProve">Si solo se publican a cada proveedor los articulos de sus materiales</param>
''' <remarks>Llamada desde=ObtenerListadoOfeRec; Tiempo m�ximo Puede ser mayor que 2 seg dependiendo del proceso</remarks>
'''Revisada EPB 23/08/2011
Private Function ListadoProceOfeRecProve(ByVal oReport As CRAXDRT.Report, Optional ByVal MostrarItemAbierto As Integer, Optional ByVal MostrarGrupoAbierto As Integer, Optional ByVal bPubMatProve As Boolean) As Boolean
Dim SubListado As CRAXDRT.Report
Dim sOpt As String
Dim m_ArRec As Ador.Recordset
Dim bMostrar As Boolean


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Function
    End If


    'REPORT PRINCIPAL
    
    Set g_adoresDG = oGestorInformes.ListadoOfertasRecPorProve(val(sdbcAnyo.Value), DesdeEst, HastaEst, udtBusquedaProc, IIf(chkOfeRecSin.Value = vbChecked, False, chkOfeRecUltProve.Value), chkOfeRecSin.Value, gParametrosInstalacion.gIdioma, MostrarItemAbierto, MostrarGrupoAbierto, bPubMatProve)
    If g_adoresDG Is Nothing Then
        ListadoProceOfeRecProve = False
        Exit Function
    
    Else
        If basParametros.gParametrosGenerales.gbAdminPublica = False Then
            If g_adoresDG.EOF Then
                ListadoProceOfeRecProve = False
                Exit Function
            Else
                oReport.Database.SetDataSource g_adoresDG
            End If
        Else
        
            'Definition
            Set m_ArRec = New Ador.Recordset
            m_ArRec.Fields.Append "ANYO", adInteger
            m_ArRec.Fields.Append "GMN1", adVarChar, 50
            m_ArRec.Fields.Append "PROCE", adInteger
            m_ArRec.Fields.Append "PROCEDEN", adVarChar, 255
            m_ArRec.Fields.Append "FECLIMOFE", adDate, , adFldIsNullable
            m_ArRec.Fields.Append "PROCEMON", adVarChar, 50
            m_ArRec.Fields.Append "CAMBIO", adDouble
            m_ArRec.Fields.Append "PROVE", adVarChar, 50
            m_ArRec.Fields.Append "PROVEDEN", adVarChar, 255
            m_ArRec.Fields.Append "OFE", adInteger
            m_ArRec.Fields.Append "OFEEST", adVarChar, 50, adFldIsNullable
            m_ArRec.Fields.Append "OFEESTDEN", adVarChar, 255, adFldIsNullable
            m_ArRec.Fields.Append "FECREC", adDate, , adFldIsNullable
            m_ArRec.Fields.Append "FECVAL", adDate, , adFldIsNullable
            m_ArRec.Fields.Append "OFEMON", adVarChar, 50, adFldIsNullable
            m_ArRec.Fields.Append "OFEMONDEN", adVarChar, 255, adFldIsNullable
            m_ArRec.Fields.Append "OFECAMBIO", adDouble
            m_ArRec.Fields.Append "OBS", adVariant, , adFldIsNullable
            m_ArRec.Fields.Append "OFECERRADO", adInteger
            m_ArRec.Fields.Append "PADJUN", adInteger
            m_ArRec.Fields.Append "GADJUN", adInteger
            m_ArRec.Fields.Append "IADJUN", adInteger
            m_ArRec.Fields.Append "GRUPOCOD", adVarChar, 50
            m_ArRec.Fields.Append "GRUPODEN", adVarChar, 255
            m_ArRec.Fields.Append "GRUPOCERRADO", adInteger, , adFldIsNullable
            m_ArRec.Fields.Append "GRUPOESCALADOS", adInteger, , adFldIsNullable
            m_ArRec.Fields.Append "ITEM", adInteger
            m_ArRec.Fields.Append "ART", adVarChar, 50, adFldIsNullable
            m_ArRec.Fields.Append "DESCR", adVarChar, 200, adFldIsNullable
            m_ArRec.Fields.Append "DEN", adVarChar, 200, adFldIsNullable
            m_ArRec.Fields.Append "DEFDEST", adInteger
            m_ArRec.Fields.Append "DEFPAG", adInteger
            m_ArRec.Fields.Append "DEFFECSUM", adInteger
            m_ArRec.Fields.Append "DEFPROVE", adInteger
            m_ArRec.Fields.Append "DEFPRECALTER", adInteger
            m_ArRec.Fields.Append "DEFSOLCANTMAX", adInteger
            m_ArRec.Fields.Append "DEST", adVarChar, 50, adFldIsNullable
            m_ArRec.Fields.Append "PAG", adVarChar, 50, adFldIsNullable
            m_ArRec.Fields.Append "FECINI", adDate, , adFldIsNullable
            m_ArRec.Fields.Append "FECFIN", adDate, , adFldIsNullable
            m_ArRec.Fields.Append "PROVEACT", adVarChar, 50, adFldIsNullable
            m_ArRec.Fields.Append "UNI", adVarChar, 50, adFldIsNullable
            m_ArRec.Fields.Append "CANT", adDouble, , adFldIsNullable
            m_ArRec.Fields.Append "PRECIO", adDouble, , adFldIsNullable
            m_ArRec.Fields.Append "PRECIO1", adDouble, , adFldIsNullable
            m_ArRec.Fields.Append "PRECIO2", adDouble, , adFldIsNullable
            m_ArRec.Fields.Append "PRECIO3", adDouble, , adFldIsNullable
            m_ArRec.Fields.Append "CANTMAX", adDouble, , adFldIsNullable
            m_ArRec.Fields.Append "USAR", adInteger, , adFldIsNullable
            m_ArRec.Fields.Append "HOM", adInteger, , adFldIsNullable
            m_ArRec.Fields.Append "EST", adInteger, , adFldIsNullable
            m_ArRec.Fields.Append "DEFSOLICIT", adInteger
            m_ArRec.Fields.Append "SOLICIT", adInteger, , adFldIsNullable
            m_ArRec.Fields.Append "ADMIN_PUB", adInteger
            m_ArRec.Fields.Append "SOBRE", adInteger, , adFldIsNullable
            m_ArRec.Fields.Append "ESTR_SOBRE", adInteger, , adFldIsNullable
            m_ArRec.Fields.Append "COMENT1", adVarChar, 2000, adFldIsNullable
            m_ArRec.Fields.Append "COMENT2", adVarChar, 2000, adFldIsNullable
            m_ArRec.Fields.Append "COMENT3", adVarChar, 2000, adFldIsNullable
            m_ArRec.Fields.Append "NUMDEC", adInteger
            m_ArRec.Fields.Append "PORTAL", adInteger
            m_ArRec.Fields.Append "USU", adVarChar, 50
            m_ArRec.Fields.Append "NOMUSU", adVarChar, 200
            
            
            m_ArRec.Open
        
            While Not g_adoresDG.EOF
                bMostrar = True
                If Not IsNull(g_adoresDG("SOBRE").Value) Then
                    'Comprueba que el estado del sobre est� abierto,sino no se mostrar� el grupo.
                    Select Case EncriptarAperturaSobre(CStr(g_adoresDG("ANYO").Value) & CStr(g_adoresDG("PROCE").Value) & CStr(g_adoresDG("SOBRE").Value), g_adoresDG("ESTR_SOBRE").Value, False)
                        Case "Abierto"
                            bMostrar = True
                        Case "Cerrado"
                            bMostrar = False
                        Case Else
                            bMostrar = False
                    End Select
                End If
                
                If bMostrar = True Then
                    m_ArRec.AddNew
                    m_ArRec("ANYO").Value = g_adoresDG("ANYO").Value
                    m_ArRec("GMN1").Value = g_adoresDG("GMN1").Value
                    m_ArRec("PROCE").Value = g_adoresDG("PROCE").Value
                    m_ArRec("PROCEDEN").Value = g_adoresDG("PROCEDEN").Value
                    m_ArRec("FECLIMOFE").Value = g_adoresDG("FECLIMOFE").Value
                    m_ArRec("PROCEMON").Value = g_adoresDG("PROCEMON").Value
                    m_ArRec("CAMBIO").Value = g_adoresDG("CAMBIO").Value
                    m_ArRec("PROVE").Value = g_adoresDG("PROVE").Value
                    m_ArRec("PROVEDEN").Value = g_adoresDG("PROVEDEN").Value
                    If IsNull(g_adoresDG("OFE").Value) Then
                        m_ArRec("OFE").Value = 0
                    Else
                        m_ArRec("OFE").Value = g_adoresDG("OFE").Value
                    End If
                    m_ArRec("OFEEST").Value = g_adoresDG("OFEEST").Value
                    m_ArRec("OFEESTDEN").Value = g_adoresDG("OFEESTDEN").Value
                    'If IsNull(g_adoresDG("FECREC").Value) Then
                    '    m_ArRec("FECREC").Value = ""
                    'Else
                        m_ArRec("FECREC").Value = g_adoresDG("FECREC").Value
                    'End If
                    m_ArRec("FECVAL").Value = g_adoresDG("FECVAL").Value
                    m_ArRec("OFEMON").Value = g_adoresDG("OFEMON").Value
                    m_ArRec("OFEMONDEN").Value = g_adoresDG("OFEMONDEN").Value
                    If IsNull(g_adoresDG("OFECAMBIO").Value) Then
                        m_ArRec("OFECAMBIO").Value = 1
                    Else
                        m_ArRec("OFECAMBIO").Value = g_adoresDG("OFECAMBIO").Value
                    End If
                    m_ArRec("OBS").Value = g_adoresDG("OBS").Value
                    If IsNull(g_adoresDG("OFECERRADO").Value) Then
                        m_ArRec("OFECERRADO").Value = 0
                    Else
                        m_ArRec("OFECERRADO").Value = g_adoresDG("OFECERRADO").Value
                    End If
                    m_ArRec("PADJUN").Value = g_adoresDG("PADJUN").Value
                    m_ArRec("GADJUN").Value = g_adoresDG("GADJUN").Value
                    m_ArRec("IADJUN").Value = g_adoresDG("IADJUN").Value
                    m_ArRec("GRUPOCOD").Value = g_adoresDG("GRUPOCOD").Value
                    m_ArRec("GRUPODEN").Value = g_adoresDG("GRUPODEN").Value
                    m_ArRec("GRUPOCERRADO").Value = g_adoresDG("GRUPOCERRADO").Value
                    m_ArRec("GRUPOESCALADOS").Value = g_adoresDG("GRUPOESCALADOS").Value
                    If IsNull(g_adoresDG("ITEM").Value) Then
                        m_ArRec("ITEM").Value = 0
                    Else
                        m_ArRec("ITEM").Value = g_adoresDG("ITEM").Value
                    End If
                    m_ArRec("ART").Value = g_adoresDG("ART").Value
                    m_ArRec("DESCR").Value = g_adoresDG("DESCR").Value
                    m_ArRec("DEN").Value = g_adoresDG("DESCR").Value
                    m_ArRec("DEFDEST").Value = g_adoresDG("DEFDEST").Value
                    m_ArRec("DEFPAG").Value = g_adoresDG("DEFPAG").Value
                    m_ArRec("DEFFECSUM").Value = g_adoresDG("DEFFECSUM").Value
                    m_ArRec("DEFPROVE").Value = g_adoresDG("DEFPROVE").Value
                    m_ArRec("DEFPRECALTER").Value = g_adoresDG("DEFPRECALTER").Value
                    m_ArRec("DEFSOLCANTMAX").Value = g_adoresDG("DEFSOLCANTMAX").Value
                    m_ArRec("DEST").Value = g_adoresDG("DEST").Value
                    m_ArRec("PAG").Value = g_adoresDG("PAG").Value
                    m_ArRec("FECINI").Value = g_adoresDG("FECINI").Value
                    m_ArRec("FECFIN").Value = g_adoresDG("FECFIN").Value
                    m_ArRec("PROVEACT").Value = g_adoresDG("PROVEACT").Value
                    m_ArRec("UNI").Value = g_adoresDG("UNI").Value
                    m_ArRec("CANT").Value = g_adoresDG("CANT").Value
                    m_ArRec("PRECIO").Value = g_adoresDG("PRECIO").Value
                    m_ArRec("PRECIO1").Value = g_adoresDG("PRECIO1").Value
                    m_ArRec("PRECIO2").Value = g_adoresDG("PRECIO2").Value
                    m_ArRec("PRECIO3").Value = g_adoresDG("PRECIO3").Value
                    m_ArRec("CANTMAX").Value = g_adoresDG("CANTMAX").Value
                    m_ArRec("USAR").Value = g_adoresDG("USAR").Value
                    m_ArRec("HOM").Value = g_adoresDG("HOM").Value
                    m_ArRec("EST").Value = g_adoresDG("EST").Value
                    m_ArRec("DEFSOLICIT").Value = g_adoresDG("DEFSOLICIT").Value
                    m_ArRec("SOLICIT").Value = g_adoresDG("SOLICIT").Value
                    m_ArRec("ADMIN_PUB").Value = g_adoresDG("ADMIN_PUB").Value
                    m_ArRec("SOBRE").Value = g_adoresDG("SOBRE").Value
                    m_ArRec("ESTR_SOBRE").Value = Null
                    m_ArRec("COMENT1").Value = g_adoresDG("COMENT1").Value
                    m_ArRec("COMENT2").Value = g_adoresDG("COMENT2").Value
                    m_ArRec("COMENT3").Value = g_adoresDG("COMENT3").Value
                    m_ArRec("NUMDEC").Value = NullToDbl0(g_adoresDG("NUMDEC").Value)
                    m_ArRec("PORTAL").Value = g_adoresDG("PORTAL").Value
                    m_ArRec("USU").Value = NullToStr(g_adoresDG("USU").Value)
                    If IsNull(g_adoresDG("NOMUSU").Value) Then
                        m_ArRec("NOMUSU").Value = NullToStr(g_adoresDG("USU").Value)
                    Else
                        m_ArRec("NOMUSU").Value = g_adoresDG("NOMUSU").Value
                    End If
                    
                End If
              
                g_adoresDG.MoveNext
            Wend
        
            If m_ArRec.RecordCount > 0 Then
                m_ArRec.Update
            ElseIf m_ArRec.RecordCount = 0 Then
                ListadoProceOfeRecProve = False
                Exit Function
            End If
            
            'Open report
            oReport.Database.SetDataSource m_ArRec
            Set m_ArRec = Nothing
        End If
    End If
    
    'ESCALADOS
    Set SubListado = oReport.OpenSubreport("rptPrecEsc")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtRango")).Text = """" & m_stxtRango & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresUni")).Text = """" & m_stxtPresUni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec")).Text = """" & m_stxtPrec & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCostesDesc")).Text = """" & m_stxtCostesDesc & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAlPrecio")).Text = """" & m_stxtAlPrecio & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAlTotal")).Text = """" & m_stxtAlTotal & """"
    Set g_adoresEsc = oGestorInformes.ListadoOfertasRecEscalados(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, val(txtCod.Text), txtDen.Text, udtBusquedaProc, IIf(chkOfeRecSin.Value = vbChecked, False, chkOfeRecUltProve.Value))
    SubListado.Database.SetDataSource g_adoresEsc
    Set SubListado = Nothing
        
    'ATRIBUTOS
    If chkOfeRecAtributos.Value = vbChecked Then
        'proceso
        Set SubListado = oReport.OpenSubreport("rptOFERecAtrib")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributos")).Text = """" & m_stxtAtributos & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtValor")).Text = """" & m_stxtValor & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_stxtSi & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_stxtNo & """"
        Set g_adoresAtrib = oGestorInformes.ListadoOfertasRecAtributos(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, val(txtCod.Text), 1, txtDen.Text, udtBusquedaProc, IIf(chkOfeRecSin.Value = vbChecked, False, chkOfeRecUltProve.Value), oUsuarioSummit.Cod)
        If Not g_adoresAtrib Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtribP")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresAtrib
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtribP")).Text = """" & sOpt & """"
        End If
        Set SubListado = Nothing
        'grupo
        Set SubListado = oReport.OpenSubreport("rptAtribGrupo")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributos")).Text = """" & m_stxtAtributos & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtValor")).Text = """" & m_stxtValor & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_stxtSi & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_stxtNo & """"
        Set g_adoresPre1 = oGestorInformes.ListadoOfertasRecAtributos(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, val(txtCod.Text), 2, txtDen.Text, udtBusquedaProc, IIf(chkOfeRecSin.Value = vbChecked, False, chkOfeRecUltProve.Value), oUsuarioSummit.Cod)
        If Not g_adoresPre1 Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtribG")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPre1
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtribG")).Text = """" & sOpt & """"
        End If
        Set SubListado = Nothing
        '�tem
        Set SubListado = oReport.OpenSubreport("rptAtribItem")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAtributos")).Text = """" & m_stxtAtributos & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtValor")).Text = """" & m_stxtValor & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_stxtSi & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_stxtNo & """"
        Set g_adoresPre2 = oGestorInformes.ListadoOfertasRecAtributos(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, val(txtCod.Text), 3, txtDen.Text, udtBusquedaProc, IIf(chkOfeRecSin.Value = vbChecked, False, chkOfeRecUltProve.Value), oUsuarioSummit.Cod)
        If Not g_adoresPre2 Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtribIt")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPre2
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAtribIt")).Text = """" & sOpt & """"
        End If
        Set SubListado = Nothing
    End If
    'ADJUNTOS
    If chkOfeRecAdjuntos.Value = vbChecked Then
        'proceso
        Set SubListado = oReport.OpenSubreport("rptAdjuntos")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjuntos")).Text = """" & m_stxtAdjuntos & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArchivo")).Text = """" & m_stxtArchivo & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComent")).Text = """" & m_stxtComent & """"
        Set g_adoresEsp = oGestorInformes.ListadoOfertasRecAdjuntos(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, val(txtCod.Text), 1, txtDen.Text, udtBusquedaProc, IIf(chkOfeRecSin.Value = vbChecked, False, chkOfeRecUltProve.Value))
        If Not g_adoresEsp Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdjunP")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresEsp
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdjunP")).Text = """" & sOpt & """"
        End If
        Set SubListado = Nothing
        'grupo
        Set SubListado = oReport.OpenSubreport("rptAdjunGrupo")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjuntos")).Text = """" & m_stxtAdjuntos & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArchivo")).Text = """" & m_stxtArchivo & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComent")).Text = """" & m_stxtComent & """"
        Set g_adoresPre3 = oGestorInformes.ListadoOfertasRecAdjuntos(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, val(txtCod.Text), 2, txtDen.Text, udtBusquedaProc, IIf(chkOfeRecSin.Value = vbChecked, False, chkOfeRecUltProve.Value))
        If Not g_adoresPre3 Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdjunG")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPre3
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdjunG")).Text = """" & sOpt & """"
        End If
        Set SubListado = Nothing
        '�tem
        Set SubListado = oReport.OpenSubreport("rptAdjunItem")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjuntos")).Text = """" & m_stxtAdjuntos & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArchivo")).Text = """" & m_stxtArchivo & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComent")).Text = """" & m_stxtComent & """"
        Set g_adoresPre4 = oGestorInformes.ListadoOfertasRecAdjuntos(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, val(txtCod.Text), 3, txtDen.Text, udtBusquedaProc, IIf(chkOfeRecSin.Value = vbChecked, False, chkOfeRecUltProve.Value))
        If Not g_adoresPre4 Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdjunIt")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource g_adoresPre4
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOptAdjunIt")).Text = """" & sOpt & """"
        End If
        Set SubListado = Nothing
    End If

    ListadoProceOfeRecProve = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ListadoProceOfeRecProve", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Obtiene el listado del ofertas por �tem
''' </summary>
''' <param name="oReport">Report de CR.</param>
''' <param name="MostrarItemAbierto">Si solo se muestran items no cerrados, solo cerrados o todos</param>
''' <param name="MostrarGrupoAbierto">Si solo se muestran grupos no cerrados, solo cerrados o todos</param>
''' <remarks>Llamada desde=ObtenerListadoOfeRec; Tiempo m�ximo Puede ser mayor que 2 seg dependiendo del proceso</remarks>
Private Function ListadoProceOfeRecItem(ByVal oReport As CRAXDRT.Report, Optional ByVal MostrarItemAbierto As Integer, Optional ByVal MostrarGrupoAbierto As Integer) As Boolean
    Dim Adores As ADODB.Recordset
    Dim SubListado As CRAXDRT.Report
    Dim oCRProceso As CRProceso

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Function
    End If
        
    Set oCRProceso = GenerarCRProceso
   
    ListadoProceOfeRecItem = oCRProceso.ListadoPROCEOfeRec(oGestorInformes, oReport, val(sdbcAnyo.Value), DesdeEst, HastaEst, udtBusquedaProc, chkOfeRecSin.Value, chkOfeRecUltItem.Value, optSelProveOrdCod.Value, MostrarGrupoAbierto)

    Set SubListado = oReport.OpenSubreport("rptOFERTAS")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecVal")).Text = """" & m_stxtFecVal & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtHomo")).Text = """" & m_stxtHomo & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOfe")).Text = """" & m_stxtOfertaAnterior & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtItems")).Text = """" & m_stxtitems & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCant")).Text = """" & m_stxtCant & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDest")).Text = """" & m_stxtDest & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFin")).Text = """" & m_stxtFin & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtIni")).Text = """" & m_stxtIni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPago")).Text = """" & m_stxtPago & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec")).Text = """" & m_stxtPres & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnd")).Text = """" & m_stxtUnd & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGrupo")).Text = """" & m_stxtGrupo & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec1")).Text = """" & m_stxtPrecio1 & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec2")).Text = """" & m_stxtPrecio2 & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec3")).Text = """" & m_stxtPrecio3 & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUsarPrec")).Text = """" & m_stxtUsarPrec & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrecio1")).Text = """" & m_stxtPrecio1 & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrecio2")).Text = """" & m_stxtPrecio2 & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrecio3")).Text = """" & m_stxtPrecio3 & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCantMax")).Text = """" & m_stxtCantMax & ":" & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSolicit")).Text = """" & m_stxtSolicit & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComent1")).Text = """" & m_stxtComent1 & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComent2")).Text = """" & m_stxtComent2 & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComent3")).Text = """" & m_stxtComent3 & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtObs")).Text = """" & m_stxtObs & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtORIGENGS")).Text = """" & sIdiOrigenGS & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtORIGENPORTAL")).Text = """" & sIdiOrigenPORTAL & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUSU")).Text = """" & sIdiUSU & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtContacto")).Text = """" & sIdiContacto & """"
    'literales relacionados con escalados
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtRango")).Text = """" & m_stxtRango & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresUni")).Text = """" & m_stxtPresUni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec")).Text = """" & m_stxtPrec & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCostesDesc")).Text = """" & m_stxtCostesDesc & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAlPrecio")).Text = """" & m_stxtAlPrecio & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAlTotal")).Text = """" & m_stxtAlTotal & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAnyoImput")).Text = """" & m_stxtAnyoImput & """"
    'Indica al report si se va a mostrar o no las solicitudes
    If m_bMostrarSolicitud = True Then
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "vMostrarSolicit")).Text = """" & "S" & """"
    Else
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "vMostrarSolicit")).Text = """" & "N" & """"
    End If
    
    Set Adores = oGestorInformes.ListadoOfertasRecPorItem(val(sdbcAnyo.Value), sdbcGMN1Proce_4Cod.Text, val(txtCod.Text), chkOfeRecUltItem.Value, sdbcProveCod.Value, MostrarItemAbierto, udtBusquedaProc.bSoloModoSubasta, oUsuarioSummit.Cod)
'    If Not Adores Is Nothing Then
'        If Not Adores.EOF Then
            SubListado.Database.SetDataSource Adores
'        End If
'    End If
    Set SubListado = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ListadoProceOfeRecItem", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub ObtenerListadoAdjudicacion()
    Dim oReport As Object
    Dim oCRProceso As CRProceso
    Dim pv As Preview
    Dim sMonedaSel As String
    Dim MostrarItemAbiertos As Integer
    Dim MostrarGrAbiertos As Integer
    Dim bListadoPorItem As Boolean
    Dim m_sNivelDet As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCRProceso = GenerarCRProceso
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    If opCompPorItem Then
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptADJPorItem.rpt"
    Else
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptADJPorProve.rpt"
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
            
            
    Screen.MousePointer = vbHourglass
          
    udtBusquedaProc = Cargar
    
    sSeleccion = GenerarTextoSeleccion
    
    If dequivalencia <> 0 Then
        sText = DividirFrase(m_sIdiMon1)
        sMonedaSel = sText(1) & sMoneda & sText(2)
    Else
        sMonedaSel = m_sIdiMon2
    End If
    
    'SE ABRE EL REPORT
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    If opCompPorItem.Value = True Then
        Select Case sdbcNivDet.Text
            Case m_stxtProceso
                sTit = sTitulo(8) 'Listado de adjudicacion por proceso
            Case m_stxtGrupo
                sTit = sTitulo(9) 'Listado de adjudicacion por grupo
            Case m_stxtItem
                sTit = sTitulo(10) 'Listado de adjudicacion por item
        End Select
    Else
        sTit = sTitulo(7) 'Listado de adjudicacion por proveedor
    End If

    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & m_stxtSeleccion & " " & Trim(sSeleccion) & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTit & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & sMonedaSel & """"
    oReport.ParameterFields(crs_ParameterIndex(oReport, "EQUIV")).SetCurrentValue dequivalencia, 7
    oReport.EnableParameterPrompting = False
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAdj")).Text = """" & m_stxtPAdj & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAho")).Text = """" & m_stxtPAho & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAho")).Text = """" & m_stxtAhorrado & """"
    
    If opCompPorItem.Value = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCant")).Text = """" & m_stxtCant & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCantAdj")).Text = """" & m_stxtCant & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtItem")).Text = """" & m_stxtItem & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = """" & m_stxtProveAdjudicado & """"
        bListadoPorItem = True
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCantAdj")).Text = """" & m_stxtCant & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtItem")).Text = """" & m_stxtItem & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = """" & m_stxtProveAdjudicado & """"
        bListadoPorItem = False
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecAdj")).Text = """" & m_stxtFecAdj & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtImp")).Text = """" & m_stxtAdj & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtImpAdj")).Text = """" & m_stxtImpAdj & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNOf")).Text = """" & m_stxtNOf & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrecAdj")).Text = """" & m_stxtPrec & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtImpGrupo")).Text = """" & m_stxtImp & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAhorroGrupo")).Text = """" & m_stxtAho & ":" & """"
    
    If opCompPorItem.Value = True Then
        Select Case sdbcNivDet.Text
            Case m_stxtProceso
                m_sNivelDet = "P"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = """" & m_stxtProce & """"
            Case m_stxtGrupo
                m_sNivelDet = "G"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = """" & m_stxtProce & "/" & m_stxtGrupo & """"
            Case m_stxtItem
                m_sNivelDet = "I"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = """" & m_stxtProce & "/" & m_stxtGrupo & "/" & m_stxtItem & """"
        End Select
    Else
        Select Case sdbcNivDet.Text
            Case m_stxtProceso
                m_sNivelDet = "P"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = """" & m_stxtProce & "/" & m_stxtProveAdjudicado & """"
            Case m_stxtGrupo
                m_sNivelDet = "G"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = """" & m_stxtProce & "/" & m_stxtProveAdjudicado & "/" & m_stxtGrupo & """"
            Case m_stxtItem
                m_sNivelDet = "I"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = """" & m_stxtProce & "/" & m_stxtProveAdjudicado & "/" & m_stxtGrupo & """"
        End Select
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & m_stxtGrupo & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMoneda")).Text = """" & m_stxtMoneda & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPorcenAdj")).Text = """" & m_stxtPAho & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNivelDet")).Text = """" & m_sNivelDet & """"
       
    
    If optGrCerradosAdj.Value = True Then
        MostrarGrAbiertos = 1
        MostrarItemAbiertos = 2
    Else
        If optGrAbiertosAdj.Value = True Then
            MostrarGrAbiertos = 0
        ElseIf optGrTodosAdj.Value = True Then
            MostrarGrAbiertos = 2
        End If
        
        'Items abiertos,cerrados o todos los items
        If optItAbiertosAdj.Value = True Then
            MostrarItemAbiertos = 0
        ElseIf optItCerradosAdj.Value = True Then
            MostrarItemAbiertos = 1
        ElseIf optTodosItAdj.Value = True Then
            MostrarItemAbiertos = 2
        End If
    End If
    
    
    If Not oCRProceso.ListadoPROCEAdj(oGestorInformes, oReport, val(sdbcAnyo.Text), DesdeEst, HastaEst, opCompOrdDen.Value, udtBusquedaProc, bListadoPorItem, MostrarItemAbiertos, MostrarGrAbiertos) Then
            oMensajes.ImposibleMostrarInforme
            Screen.MousePointer = vbNormal
            Exit Sub
    End If
    
 
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sEspera(3) & " " & sTit
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    
    Set pv = New Preview
    pv.Hide
    pv.caption = sTit
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ObtenerListadoAdjudicacion", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


Private Sub ObtenerListadoOfeRecWeb()
    ' Listado de OFERTAS RECIBIDAS a traves del WEB por procesos
    Dim pv As Preview
    Dim oReportWeb As Object
    Dim SubListado As CRAXDRT.Report
    Dim oCRProceso As CRProceso
    Dim MostrarItemAbierto As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCRProceso = GenerarCRProceso
        
    MostrarItemAbierto = 2
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPROCEOfeRecWeb.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Set oReportWeb = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)

    Screen.MousePointer = vbHourglass
    
    udtBusquedaProc = Cargar
    
    sSeleccion = GenerarTextoSeleccion
    
    sTit = sTitulo(6)
    If chkOfeRecItems.Value = vbChecked Then
        oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "ARTICULOS")).Text = """S"""
    Else
        oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "ARTICULOS")).Text = """N"""
    End If
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "SEL")).Text = """" & sSeleccion & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtOfe")).Text = """" & m_stxtOfertaAnterior & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtPag")).Text = """" & m_stxtPag & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtDe")).Text = """" & m_stxtDe & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtFecRec")).Text = """" & m_stxtFecRec & ":" & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtFecValidez")).Text = """" & m_stxtFecValidez & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtMon")).Text = """" & m_stxtMon & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtNumOfe")).Text = """" & m_stxtNumOfe & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtProve")).Text = """" & m_stxtProve & ":" & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "txtSeleccion")).Text = """" & m_stxtSeleccion & """"
    oReportWeb.FormulaFields(crs_FormulaIndex(oReportWeb, "TITULO")).Text = """" & sTit & """"
    
    Set SubListado = oReportWeb.OpenSubreport("rptPRECIOS")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOfer")).Text = """" & m_stxtOfer & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCantMax")).Text = """" & m_stxtCantMax & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtItems")).Text = """" & m_stxtitems & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCant")).Text = """" & m_stxtCant & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDest")).Text = """" & m_stxtDest & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFin")).Text = """" & m_stxtFin & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtIni")).Text = """" & m_stxtIni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPago")).Text = """" & m_stxtPagoP & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrec")).Text = """" & m_stxtPrec & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnd")).Text = """" & m_stxtUnd & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPres")).Text = """" & m_stxtPres & """"
    Set SubListado = Nothing
    
    If optItemAbiertos.Value = True Then
        MostrarItemAbierto = 1
    End If
    
    If optItemCerrados.Value = True Then
        MostrarItemAbierto = 0
    End If
    
    If optTodosItems.Value = True Then
        MostrarItemAbierto = 2
    End If

    oCRProceso.ListadoPROCEOfeRecWeb oReportWeb, val(sdbcAnyo), DesdeEst, HastaEst, udtBusquedaProc, chkOfeRecUltProve, optSelProveOrdCod, chkOfeRecItems, MostrarItemAbierto
         
    If oReportWeb Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set pv = New Preview
    Me.Hide
    pv.Hide
    Set pv.g_oReport = oReportWeb
    pv.crViewer.ReportSource = oReportWeb
        
    frmESPERA.lblGeneral.caption = sEspera(1) & " " & sTitulo(6)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.caption = sTitulo(6)
    pv.crViewer.ViewReport
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReportWeb = Nothing
    Set oCRProceso = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "ObtenerListadoOfeRecWeb", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
Private Sub CargarGMN1Automaticamente()
    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oIMAsig = oUsuarioSummit.comprador
    Set oGMN1s = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
    If Not oGMN1s.Count = 0 Then
        GMN1RespetarCombo = True
        sdbcGMN1Proce_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1Proce_4Cod.Columns(0).Text = sdbcGMN1Proce_4Cod.Text
        GMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    Set oGMN1s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmLstPROCE", "CargarGMN1Automaticamente", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
