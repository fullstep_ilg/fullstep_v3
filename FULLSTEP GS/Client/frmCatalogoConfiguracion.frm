VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCatalogoConfiguracion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "frmCatalogoConfiguracion"
   ClientHeight    =   7590
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13455
   Icon            =   "frmCatalogoConfiguracion.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7590
   ScaleWidth      =   13455
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab ssTabConfiguracionCatalogo 
      Height          =   7215
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   12726
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "DFlujos de Aprobacion"
      TabPicture(0)   =   "frmCatalogoConfiguracion.frx":0562
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblFlujosAprobacion"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblEmpresas"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblTipoPedido"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblCategoria"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "sdbcCategoria"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "sdbcFlujos"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "sdbcEmp"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "sdbcTipoPedido"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "sdbgFlujosAprobacion"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).ControlCount=   9
      TabCaption(1)   =   "DAprovisionadores"
      TabPicture(1)   =   "frmCatalogoConfiguracion.frx":057E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdSelAprovisionador"
      Tab(1).Control(1)=   "cmdBorrarAprovisionador"
      Tab(1).Control(2)=   "txtAprovisionador"
      Tab(1).Control(3)=   "tvwEstrCat"
      Tab(1).ControlCount=   4
      Begin VB.CommandButton cmdSelAprovisionador 
         Height          =   285
         Left            =   -69525
         Picture         =   "frmCatalogoConfiguracion.frx":059A
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   480
         Width           =   285
      End
      Begin VB.CommandButton cmdBorrarAprovisionador 
         Height          =   285
         Left            =   -69840
         Picture         =   "frmCatalogoConfiguracion.frx":0606
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   480
         Width           =   285
      End
      Begin VB.TextBox txtAprovisionador 
         BackColor       =   &H0080C0FF&
         Height          =   310
         Left            =   -74640
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   480
         Width           =   4695
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgFlujosAprobacion 
         Height          =   5730
         Left            =   120
         TabIndex        =   1
         Top             =   1320
         Width           =   12645
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   10
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoConfiguracion.frx":06AC
         stylesets(1).Name=   "StringTachado"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   -1  'True
         EndProperty
         stylesets(1).Picture=   "frmCatalogoConfiguracion.frx":06C8
         stylesets(2).Name=   "Bloqueado"
         stylesets(2).BackColor=   12632256
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmCatalogoConfiguracion.frx":06E4
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   10
         Columns(0).Width=   5927
         Columns(0).Caption=   "CATEGORIA"
         Columns(0).Name =   "CATEGORIA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   4419
         Columns(1).Caption=   "TIPO_PEDIDO"
         Columns(1).Name =   "TIPO_PEDIDO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   6112
         Columns(2).Caption=   "EMP"
         Columns(2).Name =   "EMP"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   4842
         Columns(3).Caption=   "SOLICITUD"
         Columns(3).Name =   "SOLICITUD"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "TIPO_PEDIDO_HIDDEN"
         Columns(4).Name =   "TIPO_PEDIDO_HIDDEN"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "EMP_HIDDEN"
         Columns(5).Name =   "EMP_HIDDEN"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "SOLICITUD_HIDDEN"
         Columns(6).Name =   "SOLICITUD_HIDDEN"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "TIPOSOLICITUD"
         Columns(7).Name =   "TIPOSOLICITUD"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "FORMULARIO"
         Columns(8).Name =   "FORMULARIO"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "WORKFLOW"
         Columns(9).Name =   "WORKFLOW"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         _ExtentX        =   22304
         _ExtentY        =   10107
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwEstrCat 
         Height          =   6135
         Left            =   -74640
         TabIndex        =   2
         Top             =   840
         Width           =   12315
         _ExtentX        =   21722
         _ExtentY        =   10821
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoPedido 
         Height          =   285
         Left            =   3240
         TabIndex        =   6
         Top             =   840
         Width           =   2805
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4948
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEmp 
         Height          =   285
         Left            =   6240
         TabIndex        =   7
         Top             =   840
         Width           =   2805
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4948
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcFlujos 
         Height          =   285
         Left            =   9240
         TabIndex        =   8
         Top             =   840
         Width           =   2805
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4948
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCategoria 
         Height          =   285
         Left            =   120
         TabIndex        =   12
         Top             =   840
         Width           =   2805
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "NIVEL"
         Columns(2).Name =   "NIVEL"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   4948
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblCategoria 
         Caption         =   "DCategoria"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label lblTipoPedido 
         Caption         =   "DTipos de Pedido"
         Height          =   255
         Left            =   3240
         TabIndex        =   11
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label lblEmpresas 
         Caption         =   "DEmpresas"
         Height          =   255
         Left            =   6240
         TabIndex        =   10
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label lblFlujosAprobacion 
         Caption         =   "DFlujo de Aprobacion"
         Height          =   255
         Left            =   9240
         TabIndex        =   9
         Top             =   600
         Width           =   2175
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":0700
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":0812
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":0924
            Key             =   "RAIZ"
            Object.Tag             =   "RAIZ"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":13EE
            Key             =   "CANDADO"
            Object.Tag             =   "CANDADO"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":1840
            Key             =   "Baja"
            Object.Tag             =   "Baja"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":1BE3
            Key             =   "Marcado"
            Object.Tag             =   "Marcado"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":1F80
            Key             =   "CANMarca"
            Object.Tag             =   "CANMarca"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":23D2
            Key             =   "CAT_INT"
            Object.Tag             =   "CAT_INT"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoConfiguracion.frx":29FC
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCatalogoConfiguracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
    PonerFieldSeparator Me
    
    CargarGridFlujosAprobacion
    CargarEmpresas
    CargarTipoPedidos
    CargarCategorias
    CargarSolicitudes
    
    GenerarEstructuraCat (False)
    CargarRecursos
End Sub

''' <summary>Carga Textos de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_CONFIGURACION, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.ssTabConfiguracionCatalogo.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        Me.sdbgFlujosAprobacion.Columns("CATEGORIA").caption = Ador(0).Value
        Me.lblCategoria.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgFlujosAprobacion.Columns("TIPO_PEDIDO").caption = Ador(0).Value
        Me.lblTipoPedido.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgFlujosAprobacion.Columns("EMP").caption = Ador(0).Value
        Me.lblEmpresas.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgFlujosAprobacion.Columns("SOLICITUD").caption = Ador(0).Value
        Me.lblFlujosAprobacion.caption = Ador(0).Value
        Ador.MoveNext
        Me.ssTabConfiguracionCatalogo.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        tvwEstrCat.Nodes(1).Text = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.Close
    End If
End Sub

''' <summary>Limpia el arbol de categorias quitando las categorias de un aprovisionador ya seleccionado</summary>
Private Sub cmdBorrarAprovisionador_Click()
    Me.txtAprovisionador.Text = ""
    Me.txtAprovisionador.Tag = ""
    'Quito la marca de las categorias que el aprovisionador tenia configuradas
    For Each nodx In tvwEstrCat.Nodes
        nodx.Image = "ACT"
    Next
End Sub

''' <summary>Abre la pantalla para seleccionar el aprovisionador y con esa persona se indicaran las categorias a las que puede aprovisionar</summary>
Private Sub cmdSelAprovisionador_Click()
    Dim oAprovisionador As cAprovisionador
    frmSOLSelPersona.g_sOrigen = "frmCatalogoConfiguracion"
    frmSOLSelPersona.Show vbModal
    If txtAprovisionador.Tag <> "" Then
        'Si se ha seleccionado el aprovisionador
            Dim rs As ADODB.Recordset
            Dim nodx As MSComctlLib.node
            Set oAprovisionador = oFSGSRaiz.Generar_CAprovisionador
            Dim oPersona As CPersona
            Set oPersona = oFSGSRaiz.Generar_CPersona
            oPersona.Cod = txtAprovisionador.Tag
            Set oAprovisionador.Persona = oPersona
            'Devolvemos las categorias para las que ese aprovisionador puede emitir un pedido
            Set rs = oAprovisionador.DevolverCategoriasDeAprovisionador
            If Not rs.EOF Then
                For Each nodx In tvwEstrCat.Nodes
                    'Marcamos en el arbol esas categorias para las que el aprovisionador puede emitir un pedido
                    rs.MoveFirst
                    While Not rs.EOF
                        Select Case Mid(nodx.Tag, 1, 4)
                            Case "CAT1"
                                If DevolverId(nodx) = rs("CATN") And rs("NIVEL") = 1 Then
                                    nodx.Image = "ACTASIG"
                                    nodx.EnsureVisible
                                    DoEvents
                                End If
                            Case "CAT2"
                                If DevolverId(nodx) = rs("CATN") And rs("NIVEL") = 2 Then
                                    nodx.Image = "ACTASIG"
                                    nodx.EnsureVisible
                                    DoEvents
                                End If
                            Case "CAT3"
                                If DevolverId(nodx) = rs("CATN") And rs("NIVEL") = 3 Then
                                    nodx.Image = "ACTASIG"
                                    nodx.EnsureVisible
                                    DoEvents
                                End If
                            Case "CAT4"
                                If DevolverId(nodx) = rs("CATN") And rs("NIVEL") = 4 Then
                                    nodx.Image = "ACTASIG"
                                    nodx.EnsureVisible
                                    DoEvents
                                End If
                            Case "CAT5"
                                If DevolverId(nodx) = rs("CATN") And rs("NIVEL") = 5 Then
                                    nodx.Image = "ACTASIG"
                                    nodx.EnsureVisible
                                    DoEvents
                                End If
                        End Select
                        rs.MoveNext
                    Wend
                Next
            End If
    End If
End Sub

''' <summary>Carga en la grid los flujos de aprobacion de todas las categorias del catalogo</summary>
Private Sub CargarGridFlujosAprobacion()
    Dim oFlujosAprobacion As cFlujosAprobacion
    Dim oFlujoAprobacion As cFlujoAprobacion
    Set oFlujosAprobacion = oFSGSRaiz.Generar_CFlujosAprobacion
    
    'Cargamos todos los flujos de aprobacion
    oFlujosAprobacion.CargarTodosFlujosAprobacion Me.sdbcTipoPedido.Value, Me.sdbcEmp.Value, Me.sdbcFlujos.Value, Me.sdbcCategoria.Value, Me.sdbcCategoria.Columns("NIVEL").Value
    
    'Cargamos la grid con los flujos
    sdbgFlujosAprobacion.RemoveAll
    For Each oFlujoAprobacion In oFlujosAprobacion
        sdbgFlujosAprobacion.AddItem oFlujoAprobacion.CodCompletoCategoria & "-" & oFlujoAprobacion.DenCategoria & Chr(m_lSeparador) & oFlujoAprobacion.TipoPedidoDen & Chr(m_lSeparador) & oFlujoAprobacion.Empden & Chr(m_lSeparador) & oFlujoAprobacion.SolicitudDen & Chr(m_lSeparador) & oFlujoAprobacion.TipoPedido & Chr(m_lSeparador) & oFlujoAprobacion.Emp & Chr(m_lSeparador) & oFlujoAprobacion.Solicitud & Chr(m_lSeparador) & oFlujoAprobacion.tiposolicitud & Chr(m_lSeparador) & oFlujoAprobacion.Formulario & Chr(m_lSeparador) & oFlujoAprobacion.Workflow
    Next
    
End Sub
''' <summary>Devuelve el id de la categoria</summary>
''' <param name="node">Nodo de la categoria de la que se devuelve el id</param>
Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion1 As Integer
Dim iPosicion2 As Integer

    If node Is Nothing Then Exit Function
    
    'posici�n del primer -%$,justo �ntes del ID
    iPosicion1 = InStr(6, node.Tag, "-%$")
    iPosicion1 = iPosicion1 + 3
    'posici�n del segundo -%$
    iPosicion2 = InStr(iPosicion1, node.Tag, "-%$")
    If iPosicion2 = 0 Then
        DevolverId = val(Right(node.Tag, Len(node.Tag) - (iPosicion1 - 1)))
    Else
        DevolverId = val(Mid(node.Tag, iPosicion1, iPosicion2 - iPosicion1))
    End If

End Function

''' <summary>
''' Genera la estructura de categorias en el Treview
''' </summary>
''' <param name="bOrdenadoPorDen">True si se quiere ordenar las categorias por la denominacion</param>
Public Sub GenerarEstructuraCat(ByVal bOrdenadoPorDen As Boolean)
Dim oACN1 As CCategoriaN1
Dim oACN2 As CCategoriaN2
Dim oACN3 As CCategoriaN3
Dim oACN4 As CCategoriaN4
Dim oACN5 As CCategoriaN5

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Dim nodx As MSComctlLib.node


    
On Error GoTo Error

    tvwEstrCat.Nodes.clear
    
    Set nodx = tvwEstrCat.Nodes.Add(, , "Raiz", "DJerarquia de Categorias", "RAIZ")
    nodx.Tag = "Raiz"
    nodx.Expanded = True

    Set oActsN1 = oFSGSRaiz.Generar_CCategoriasN1

    oActsN1.GenerarEstructuraCategorias False, bOrdenadoPorDen
    
    If Not oActsN1 Is Nothing Then
           
        For Each oACN1 In oActsN1
            'Categorias NIVEL 1
            scod1 = CStr(oACN1.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
            Set nodx = tvwEstrCat.Nodes.Add("Raiz", tvwChild, "CAT1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACT")
            nodx.Tag = "CAT1-" & oACN1.Cod & "-%$" & oACN1.Id
            
            nodx.Expanded = False
                  
            If Not oACN1.CategoriasN2 Is Nothing Then
                'Categorias NIVEL 2
                For Each oACN2 In oACN1.CategoriasN2

                    scod2 = CStr(oACN2.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
                    Set nodx = tvwEstrCat.Nodes.Add("CAT1" & scod1, tvwChild, "CAT2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACT")
                    nodx.Tag = "CAT2-" & oACN2.Cod & "-%$" & oACN2.Id
                    
                    nodx.Expanded = False
                    
                    If Not oACN2.CategoriasN3 Is Nothing Then
                        'Categorias NIVEL 3
                        For Each oACN3 In oACN2.CategoriasN3

                            scod3 = CStr(oACN3.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
                            Set nodx = tvwEstrCat.Nodes.Add("CAT2" & scod1 & scod2, tvwChild, "CAT3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACT")
                            nodx.Tag = "CAT3-" & oACN3.Cod & "-%$" & oACN3.Id
                            
                            nodx.Expanded = False
                            
                            If Not oACN3.CategoriasN4 Is Nothing Then
                                'Categorias NIVEL 4
                                For Each oACN4 In oACN3.CategoriasN4

                                    scod4 = CStr(oACN4.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
                                    Set nodx = tvwEstrCat.Nodes.Add("CAT3" & scod1 & scod2 & scod3, tvwChild, "CAT4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACT")
                                    nodx.Tag = "CAT4-" & oACN4.Cod & "-%$" & oACN4.Id
                                    
                                    nodx.Expanded = False
                                    
                                    If Not oACN4.CategoriasN5 Is Nothing Then
                                        'Categorias NIVEL 5
                                        For Each oACN5 In oACN4.CategoriasN5

                                            scod5 = CStr(oACN5.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
                                            Set nodx = tvwEstrCat.Nodes.Add("CAT4" & scod1 & scod2 & scod3 & scod4, tvwChild, "CAT5" & scod1 & scod2 & scod3 & scod4 & scod5, oACN5.Cod & " - " & oACN5.Den, "ACT")
                                            nodx.Tag = "CAT5-" & oACN5.Cod & "-%$" & oACN5.Id
                                            
                                            nodx.Expanded = False

                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
   
    End If
    
Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
    
End Sub

''' <summary>
''' Carga las solicitudes del tipo Solicitud de pedido para a�adir al combo de flujos de aprobacion
''' </summary>
Private Sub CargarSolicitudes()
    Dim oRes As ADODB.Recordset
    Dim oSolicitudes As CSolicitudes
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    Set oRes = oSolicitudes.DevolverSolicitudesDeTipo(tiposolicitud.SolicitudDePedidoCatalogo)
    sdbcFlujos.RemoveAll
    Me.sdbcFlujos.AddItem "" & Chr(m_lSeparador) & ""
    While Not oRes.EOF
        Me.sdbcFlujos.AddItem oRes("ID").Value & Chr(m_lSeparador) & oRes("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador)
        oRes.MoveNext
    Wend
End Sub

''' <summary>
''' Carga todos los tipos de pedido que esten asociados a la categoria
''' </summary>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,3seg.</remarks>
Private Sub CargarTipoPedidos()
    Dim oTiposPedido As CTiposPedido
    Dim oTipoPedido As CTipoPedido
    Set oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    
    'Cargo todos los tipos de pedidos
    oTiposPedido.CargarTiposPedidosPorFlujoDeAprobacion gParametrosInstalacion.gIdioma
    sdbcTipoPedido.RemoveAll
    Me.sdbcTipoPedido.AddItem "" & Chr(m_lSeparador) & ""
    For Each oTipoPedido In oTiposPedido
       Me.sdbcTipoPedido.AddItem oTipoPedido.Id & Chr(m_lSeparador) & oTipoPedido.Den
    Next
    
    Set oTiposPedido = Nothing
End Sub

''' <summary>
''' Carga las empresas para el combo
''' </summary>
Private Sub CargarEmpresas()
    Dim oRes As ADODB.Recordset
    Dim oEmpresas As CEmpresas
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    Set oRes = oEmpresas.DevolverEmpresasPorCategoria
    sdbcEmp.RemoveAll
    Me.sdbcEmp.AddItem "" & Chr(m_lSeparador) & ""
    While Not oRes.EOF
        Me.sdbcEmp.AddItem oRes("EMP_ID").Value & Chr(m_lSeparador) & oRes("DEN").Value
        oRes.MoveNext
    Wend
    Set oEmpresas = Nothing
    Set oRes = Nothing
End Sub
''' <summary>
''' Carga las categorias para el combo
''' </summary>
Private Sub CargarCategorias()
    Dim oRes As ADODB.Recordset
    Dim oFlujosAprobacion As cFlujosAprobacion
    Set oFlujosAprobacion = oFSGSRaiz.Generar_CFlujosAprobacion
    Set oRes = oFlujosAprobacion.CargarCategoriasFlujosAprobacion
    sdbcCategoria.RemoveAll
    sdbcCategoria.AddItem "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
    While Not oRes.EOF
        sdbcCategoria.AddItem oRes("CAT").Value & Chr(m_lSeparador) & oRes("COD_CAT_COMPLETO").Value & Chr(m_lSeparador) & oRes("NIVEL").Value
        oRes.MoveNext
    Wend
    Set oFlujosAprobacion = Nothing
    Set oRes = Nothing
End Sub

''' <summary>
''' Inicializa el combo de Tipos de Pedido
''' </summary>
Private Sub sdbcTipoPedido_InitColumnProps()
    sdbcTipoPedido.DataFieldList = "Column 0"
    sdbcTipoPedido.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Inicializa el combo de Flujos
''' </summary>
Private Sub sdbcFlujos_InitColumnProps()
    sdbcFlujos.DataFieldList = "Column 0"
    sdbcFlujos.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Inicializa el combo de Empresas
''' </summary>
Private Sub sdbcEmp_InitColumnProps()
    sdbcEmp.DataFieldList = "Column 0"
    sdbcEmp.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Inicializa el combo de Empresas
''' </summary>
Private Sub sdbcCategoria_InitColumnProps()
    sdbcCategoria.DataFieldList = "Column 0"
    sdbcCategoria.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcFlujos_CloseUp()
    CargarGridFlujosAprobacion
End Sub

Private Sub sdbcEmp_CloseUp()
    CargarGridFlujosAprobacion
End Sub

Private Sub sdbcTipoPedido_CloseUp()
    CargarGridFlujosAprobacion
End Sub

Private Sub sdbcCategoria_CloseUp()
    CargarGridFlujosAprobacion
End Sub

''''''''''''''''''''''''''''''''''''
'''EVENTOS GRID FLUJOS APROBACION'''
''''''''''''''''''''''''''''''''''''
''' <summary>Evento que se produce al cambiar de fila o de celda en el grid</summary>
Private Sub sdbgFlujosAprobacion_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If Me.sdbgFlujosAprobacion.Col <> -1 Then
        If sdbgFlujosAprobacion.Columns(Me.sdbgFlujosAprobacion.Col).Name = "SOLICITUD" Then
            sdbgFlujosAprobacion.Columns("SOLICITUD").Style = ssStyleEditButton
            sdbgFlujosAprobacion.Columns("SOLICITUD").Locked = True
        End If
    End If
End Sub
''' <summary>Evento que se produce al pulsar el boton de una celda</summary>
Private Sub sdbgFlujosAprobacion_BtnClick()
    If sdbgFlujosAprobacion.Columns(Me.sdbgFlujosAprobacion.Col).Name = "SOLICITUD" Then
        frmFlujos.m_bModifFlujo = True
        frmFlujos.m_lIdFlujo = sdbgFlujosAprobacion.Columns("WORKFLOW").Value
        frmFlujos.m_lIdFormulario = sdbgFlujosAprobacion.Columns("FORMULARIO").Value
        frmFlujos.m_sSolicitud = sdbgFlujosAprobacion.Columns("SOLICITUD").Value
        frmFlujos.g_lSolicitud = sdbgFlujosAprobacion.Columns("SOLICITUD_HIDDEN").Value
        frmFlujos.g_lSolicitudTipo = sdbgFlujosAprobacion.Columns("TIPOSOLICITUD").Value
        frmFlujos.lblDenFlujo.caption = ""
        frmFlujos.g_iSolicitudTipoTipo = sdbgFlujosAprobacion.Columns("SOLICITUD_HIDDEN").Value
        frmFlujos.Show vbModal
    End If
End Sub
''' <summary>Evento que se produce antes de eliminar una fila del grid y evita que salte el mensaje de eliminar propio del grid</summary>
Private Sub sdbgFlujosAprobacion_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub


