_________________________________________________________________________________________
		FULLSTEP GS
		AVISO DE RECHAZO DE SOLICITUD
	
 Esta notificaci�n es para indicarle que su solicitud de compra @ID "@DESCR_BREVE" con fecha 
 de @FECHA_ALTA ha sido rechazada por "@APROBADOR_TITULO".

 Comentarios del comprador:

 	@COMENTARIO	


 Un saludo

 
PD: A continuaci�n le presentamos la informaci�n relativa a su solicitud:
_________________________________________________________________________________________

Datos de la solicitud:
 
 Identificador:		@ID
 Descripci�n breve:		@DESCR_BREVE 
 Fecha de alta: 		@FECHA_ALTA
 Fecha de necesidad:	@FECHA_NECESIDAD
 Importe aproximado:	@IMPORTE (@MON_COD - @MON_DEN)
 Tipo de solicitud:		@TIPOCOD - @TIPODEN	

_______________________________________________________________________________________

Datos del comprador asignado:
 
 C�digo:			@COMPRADOR_COD
 Nombre:			@COMPRADOR_NOM @COMPRADOR_APE
 Tel�fono:		@COMPRADOR_TFNO
 Email:			@COMPRADOR_EMAIL
 Fax:			@COMPRADOR_FAX
 Unidad organizativa:	@COMPRADOR_UON
 Departamento:		@COMPRADOR_DEP_COD - @COMPRADOR_DEP_DEN
_______________________________________________________________________________________

Datos del aprobador :
 
 C�digo:			@APROBADOR_COD
 Nombre:			@APROBADOR_NOM @APROBADOR_APE
 Tel�fono:		@APROBADOR_TFNO
 Email:			@APROBADOR_EMAIL
 Fax:			@APROBADOR_FAX
 Unidad organizativa:	@APROBADOR_UON
 Departamento:		@APROBADOR_DEP_COD - @APROBADOR_DEP_DEN
_______________________________________________________________________________________

Datos del peticionario:

 C�digo:			@PET_COD
 Nombre:			@PET_NOM @PET_APE
 Tel�fono:		@PET_TFNO
 Email:			@PET_EMAIL
 Fax:			@PET_FAX
 Unidad organizativa:	@PET_UON
 Departamento:		@PET_DEP_COD - @PET_DEP_DEN
 
 
