VERSION 5.00
Begin VB.Form frmTIPOCom3 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tipo de comunicación "
   ClientHeight    =   1680
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   3660
   Icon            =   "frmTIPOCom3.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1680
   ScaleWidth      =   3660
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton optObj 
      BackColor       =   &H00808000&
      Caption         =   "Comunicación de objetivos"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   720
      TabIndex        =   4
      Top             =   240
      Value           =   -1  'True
      Width           =   2600
   End
   Begin VB.OptionButton optAviso 
      BackColor       =   &H00808000&
      Caption         =   "Aviso de despublicación"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   720
      TabIndex        =   3
      Top             =   720
      Width           =   2310
   End
   Begin VB.OptionButton optOfe 
      BackColor       =   &H00808000&
      Caption         =   "Petición de ofertas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   720
      TabIndex        =   2
      Top             =   240
      Width           =   2400
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1875
      TabIndex        =   1
      Top             =   1305
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   735
      TabIndex        =   0
      Top             =   1305
      Width           =   1005
   End
End
Attribute VB_Name = "frmTIPOCom3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmdAceptar_Click()
    
    
    If optOfe And Me.optOfe.Visible Then
        frmOFEPet.bOpcOfe = True
    ElseIf Me.optObj And Me.optObj.Visible Then
        frmOFEPet.bOpcObj = True
    ElseIf optAviso Then
        frmOFEPet.bOpcAviso = True
    End If
    Unload Me
End Sub
Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    

    CargarRecursos
    optObj.Value = True
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPOCOM3, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Caption = Ador(0).Value
        Ador.MoveNext
        optOfe.Caption = Ador(0).Value
        Ador.MoveNext
        optAviso.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optObj.Caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


