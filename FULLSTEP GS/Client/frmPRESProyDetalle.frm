VERSION 5.00
Begin VB.Form frmPRESProyDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle"
   ClientHeight    =   2535
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   Icon            =   "frmPRESProyDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2535
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   675
      ScaleHeight     =   420
      ScaleWidth      =   3465
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   2040
      Width           =   3465
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1650
         TabIndex        =   11
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   540
         TabIndex        =   10
         Top             =   60
         Width           =   1005
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   1305
      ScaleHeight     =   1935
      ScaleWidth      =   3285
      TabIndex        =   4
      Top             =   45
      Width           =   3285
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   0
         Top             =   210
         Width           =   1530
      End
      Begin VB.TextBox txtDen 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   1
         Top             =   660
         Width           =   3165
      End
      Begin VB.TextBox txtImp 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   2
         Top             =   1080
         Width           =   1530
      End
      Begin VB.TextBox txtObj 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   3
         Top             =   1500
         Width           =   720
      End
   End
   Begin VB.Label lblDen 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   195
      Left            =   120
      TabIndex        =   8
      Top             =   780
      Width           =   1245
   End
   Begin VB.Label lblCod 
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   360
      Width           =   1245
   End
   Begin VB.Label lblImp 
      BackStyle       =   0  'Transparent
      Caption         =   "Importe:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   135
      TabIndex        =   6
      Top             =   1200
      Width           =   1245
   End
   Begin VB.Label lblObj 
      BackStyle       =   0  'Transparent
      Caption         =   "Objetivo %:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1620
      Width           =   1245
   End
End
Attribute VB_Name = "frmPRESProyDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Multilenguaje
Private sIdiCodigo As String
Private sIdiDenominacion As String
Private sIdiImporte As String
Private sIdiObjetivo As String


Private Sub cmdAceptar_Click()

Dim oPRES1 As CPresProyNivel1
Dim oPRES2 As CPresProyNivel2
Dim oPRES3 As CPresProyNivel3
Dim oPRES4 As CPresProyNivel4
Dim oIBaseDatos As IBaseDatos
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

txtImp_LostFocus
txtObj_LostFocus

Select Case frmPresupuestos1.Accion
    
    Case accionessummit.ACCPresProyNivel1Anya
        
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdiCodigo
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdiDenominacion
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiImporte
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdiObjetivo
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            Set oPRES1 = oFSGSRaiz.generar_CPresProyNivel1
            oPRES1.Anyo = frmPresupuestos1.sdbcAnyo
            oPRES1.Cod = Trim(txtCod)
            oPRES1.Den = Trim(txtDen)
            oPRES1.importe = StrToDblOrNull(txtImp)
            oPRES1.Objetivo = StrToDblOrNull(txtObj)
            'A�adir UON(s)
            If frmPresupuestos1.m_sUON1 <> "" Then oPRES1.UON1 = frmPresupuestos1.m_sUON1
            If frmPresupuestos1.m_sUON2 <> "" Then oPRES1.UON2 = frmPresupuestos1.m_sUON2
            If frmPresupuestos1.m_sUON3 <> "" Then oPRES1.UON3 = frmPresupuestos1.m_sUON3
                        
            Set oIBaseDatos = oPRES1
            
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError = TESnoerror Then
                AnyadirPRES1AEstructura
                RegistrarAccion accionessummit.ACCPresProyNivel1Anya, "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                If Me.Visible Then txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            frmPresupuestos1.MostrarDatosBarraInf
            Set oPRES1 = Nothing
            Set oIBaseDatos = Nothing
           
            
    Case ACCPresProyNivel2Anya
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdiCodigo
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdiDenominacion
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiImporte
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiObjetivo
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperior Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            Set oPRES2 = oFSGSRaiz.generar_CPresProyNivel2
            oPRES2.Anyo = frmPresupuestos1.sdbcAnyo
            oPRES2.Cod = Trim(txtCod)
            oPRES2.CodPRES1 = frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem)
            oPRES2.Den = Trim(txtDen)
            oPRES2.importe = StrToDblOrNull(txtImp)
            oPRES2.Objetivo = StrToDblOrNull(txtObj)
            'A�adir UON(s)
            If frmPresupuestos1.m_sUON1 <> "" Then oPRES2.UON1 = frmPresupuestos1.m_sUON1
            If frmPresupuestos1.m_sUON2 <> "" Then oPRES2.UON2 = frmPresupuestos1.m_sUON2
            If frmPresupuestos1.m_sUON3 <> "" Then oPRES2.UON3 = frmPresupuestos1.m_sUON3
         
            Set oIBaseDatos = oPRES2
            
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError = TESnoerror Then
                AnyadirPRES2AEstructura
                RegistrarAccion ACCPresProyNivel2Anya, "CodPRES1:" & frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem) & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                If Me.Visible Then txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            frmPresupuestos1.MostrarDatosBarraInf
            Set oPRES2 = Nothing
            Set oIBaseDatos = Nothing
           
    Case accionessummit.ACCPresProyNivel3Anya
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdiCodigo
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdiDenominacion
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiImporte
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdiObjetivo
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperior Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            Set oPRES3 = oFSGSRaiz.generar_CPresProyNivel3
            oPRES3.Anyo = frmPresupuestos1.sdbcAnyo
            oPRES3.CodPRES1 = frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem.Parent)
            oPRES3.CodPRES2 = frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem)
            oPRES3.Cod = Trim(txtCod)
            oPRES3.Den = Trim(txtDen)
            oPRES3.importe = StrToDblOrNull(txtImp)
            oPRES3.Objetivo = StrToDblOrNull(txtObj)
            'A�adir UON(s)
            If frmPresupuestos1.m_sUON1 <> "" Then oPRES3.UON1 = frmPresupuestos1.m_sUON1
            If frmPresupuestos1.m_sUON2 <> "" Then oPRES3.UON2 = frmPresupuestos1.m_sUON2
            If frmPresupuestos1.m_sUON3 <> "" Then oPRES3.UON3 = frmPresupuestos1.m_sUON3

            Set oIBaseDatos = oPRES3
            
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError = TESnoerror Then
                AnyadirPRES3AEstructura
                RegistrarAccion ACCPresProyNivel3Anya, "CodPRES1:" & frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem.Parent) & "CodPRES2:" & frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem) & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                If Me.Visible Then txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            frmPresupuestos1.MostrarDatosBarraInf
            Set oPRES3 = Nothing
            Set oIBaseDatos = Nothing
           
    Case accionessummit.ACCPresProyNivel4Anya
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdiCodigo
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdiDenominacion
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiImporte
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdiObjetivo
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperior Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            Set oPRES4 = oFSGSRaiz.generar_CPresProyNivel4
            oPRES4.Anyo = frmPresupuestos1.sdbcAnyo
            oPRES4.CodPRES1 = frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem.Parent.Parent)
            oPRES4.CodPRES2 = frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem.Parent)
            oPRES4.CodPRES3 = frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem)
            oPRES4.Cod = Trim(txtCod)
            oPRES4.Den = Trim(txtDen)
            oPRES4.importe = StrToDblOrNull(txtImp)
            oPRES4.Objetivo = StrToDblOrNull(txtObj)
            'A�adir UON(s)
            If frmPresupuestos1.m_sUON1 <> "" Then oPRES4.UON1 = frmPresupuestos1.m_sUON1
            If frmPresupuestos1.m_sUON2 <> "" Then oPRES4.UON2 = frmPresupuestos1.m_sUON2
            If frmPresupuestos1.m_sUON3 <> "" Then oPRES4.UON3 = frmPresupuestos1.m_sUON3

            Set oIBaseDatos = oPRES4
            
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError = TESnoerror Then
                AnyadirPRES4AEstructura
                RegistrarAccion ACCPresProyNivel4Anya, "CodPRES1:" & frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem.Parent.Parent) & "CodPRES2:" & frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem.Parent) & "CodPRES3:" & frmPresupuestos1.DevolverCod(frmPresupuestos1.tvwestrPres.selectedItem) & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                If Me.Visible Then txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            frmPresupuestos1.MostrarDatosBarraInf
            Set oPRES4 = Nothing
            Set oIBaseDatos = Nothing
           
    
    Case ACCPresProyNivel1Mod
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdiCodigo
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdiDenominacion
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiImporte
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdiObjetivo
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperiorMod Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            frmPresupuestos1.oPres1Seleccionado.Den = Trim(txtDen)
            frmPresupuestos1.oPres1Seleccionado.importe = StrToDblOrNull(txtImp)
            frmPresupuestos1.oPres1Seleccionado.Objetivo = StrToDblOrNull(txtObj)
            
            teserror = frmPresupuestos1.oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError = TESnoerror Then
                ModificarPRESEnEstructura
                RegistrarAccion ACCPresProyNivel1Mod, "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
    Case ACCPresProyNivel2Mod
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdiCodigo
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdiDenominacion
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiImporte
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdiObjetivo
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperiorMod Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            frmPresupuestos1.oPres2Seleccionado.Den = Trim(txtDen)
            frmPresupuestos1.oPres2Seleccionado.importe = StrToDblOrNull(txtImp)
            frmPresupuestos1.oPres2Seleccionado.Objetivo = StrToDblOrNull(txtObj)
            
            teserror = frmPresupuestos1.oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError = TESnoerror Then
                ModificarPRESEnEstructura
                RegistrarAccion ACCPresProyNivel2Mod, "CodPRES1:" & frmPresupuestos1.oPres2Seleccionado.CodPRES1 & "Cod:" & frmPresupuestos1.oPres2Seleccionado.Cod
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
           
    Case ACCPresProyNivel3Mod
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdiCodigo
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdiDenominacion
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiImporte
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdiObjetivo
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperiorMod Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            frmPresupuestos1.oPres3Seleccionado.Den = Trim(txtDen)
            frmPresupuestos1.oPres3Seleccionado.importe = StrToDblOrNull(txtImp)
            frmPresupuestos1.oPres3Seleccionado.Objetivo = StrToDblOrNull(txtObj)
            
            teserror = frmPresupuestos1.oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError = TESnoerror Then
                ModificarPRESEnEstructura
                RegistrarAccion ACCPresProyNivel3Mod, "CodPRES1:" & frmPresupuestos1.oPres3Seleccionado.CodPRES1 & "CodPRES2:" & frmPresupuestos1.oPres3Seleccionado.CodPRES2 & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
    Case ACCPresProyNivel4MOd
        
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdiCodigo
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdiDenominacion
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdiImporte
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdiObjetivo
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
        
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperiorMod Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            frmPresupuestos1.oPres4Seleccionado.Den = Trim(txtDen)
            frmPresupuestos1.oPres4Seleccionado.importe = StrToDblOrNull(txtImp)
            frmPresupuestos1.oPres4Seleccionado.Objetivo = StrToDblOrNull(txtObj)
            Set oIBaseDatos = frmPresupuestos1.oPres4Seleccionado
            teserror = oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError = TESnoerror Then
                ModificarPRESEnEstructura
                RegistrarAccion ACCPresProyNivel4MOd, "CodPRES1:" & frmPresupuestos1.oPres4Seleccionado.CodPRES1 & "CodPRES2:" & frmPresupuestos1.oPres4Seleccionado.CodPRES2 & "CodPRES3:" & frmPresupuestos1.oPres4Seleccionado.CodPRES3 & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
    Screen.MousePointer = vbNormal
           
End Select

Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()

Select Case frmPresupuestos1.Accion

    Case ACCPresProyNivel1Eli, ACCPresProyNivel2Eli, ACCPresProyNivel3Eli, ACCPresProyNivel4Eli
            
        picDatos.Enabled = False
    
    Case ACCPresProyNivel1Mod, ACCPresProyNivel2Mod, ACCPresProyNivel3Mod, ACCPresProyNivel4MOd
            
        txtCod.Enabled = False
        If Me.Visible Then txtDen.SetFocus
        
    Case ACCPresProynivel1Det, ACCPresProynivel2Det, ACCPresProynivel3Det, ACCPresProynivel4Det
        
        picDatos.Enabled = False
        picEdit.Visible = False
        Height = Height - 400
        
    Case Else
        If txtCod.Enabled And Me.Visible Then txtCod.SetFocus

End Select
End Sub

Private Sub Form_Load()

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    CargarRecursos
Select Case frmPresupuestos1.Accion
            
    Case ACCPresProyNivel1Anya
                
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1
                
    Case ACCPresProyNivel2Anya
            
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2
    
    Case ACCPresProyNivel3Anya
            
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3
    
    Case ACCPresProyNivel4Anya
            
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4
            
End Select

End Sub

Private Sub Form_Unload(Cancel As Integer)

Select Case frmPresupuestos1.Accion
            
    Case ACCPresProyNivel1Mod, ACCPresProyNivel2Mod, ACCPresProyNivel3Mod, ACCPresProyNivel4MOd, ACCPresProyNivel1Eli, ACCPresProyNivel2Eli, ACCPresProyNivel3Eli, ACCPresProyNivel4Eli
    
            frmPresupuestos1.oIBaseDatos.CancelarEdicion
End Select

Set frmPresupuestos1.oIBaseDatos = Nothing
Set frmPresupuestos1.oPres1Seleccionado = Nothing
Set frmPresupuestos1.oPres2Seleccionado = Nothing
Set frmPresupuestos1.oPres3Seleccionado = Nothing
Set frmPresupuestos1.oPres4Seleccionado = Nothing

frmPresupuestos1.Accion = ACCPresProyCon

End Sub
Public Function AnyadirPRES1AEstructura()
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String

Set nodx = frmPresupuestos1.tvwestrPres.selectedItem
scod1 = Trim(txtCod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(Trim(txtCod)))
Set nodo = frmPresupuestos1.tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, Trim(txtCod) & " - " & Trim(txtDen), "PRES1")


'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
frmPresupuestos1.oPresupuestos.Add frmPresupuestos1.sdbcAnyo, Trim(txtCod), Trim(txtDen), StrToDblOrNull(txtImp), StrToDblOrNull(txtObj)

If frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2 Is Nothing Then
        Set frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2 = oFSGSRaiz.Generar_CPresProyectosNivel2
End If

nodo.Tag = "PRES1" & Trim(txtCod)
nodo.Selected = True
nodo.EnsureVisible
frmPresupuestos1.tvwEstrPres_NodeClick nodo
Set nodo = Nothing
Set nodx = Nothing

End Function
Public Function AnyadirPRES2AEstructura()
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim sCod2 As String

Set nodx = frmPresupuestos1.tvwestrPres.selectedItem
scod1 = frmPresupuestos1.DevolverCod(nodx)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
sCod2 = Trim(txtCod)
sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
Set nodo = frmPresupuestos1.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES2" & scod1 & sCod2, Trim(txtCod) & " - " & Trim(txtDen), "PRES2")
nodo.Tag = "PRES2" & Trim(txtCod)

'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2 Is Nothing Then
    Set frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2 = oFSGSRaiz.Generar_CPresProyectosNivel2
End If

frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Add frmPresupuestos1.sdbcAnyo, scod1, Trim(txtCod), Trim(txtDen), StrToDblOrNull(txtImp), StrToDblOrNull(txtObj)

If frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3 Is Nothing Then
    Set frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3 = oFSGSRaiz.Generar_CPresProyectosNivel3
End If

frmPresupuestos1.tvwEstrPres_NodeClick nodo

nodo.Selected = True
nodo.EnsureVisible
Set nodo = Nothing
Set nodx = Nothing

End Function
Public Function AnyadirPRES3AEstructura()
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim sCod2 As String
Dim sCod3 As String

Set nodx = frmPresupuestos1.tvwestrPres.selectedItem
scod1 = frmPresupuestos1.DevolverCod(nodx.Parent)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
sCod2 = frmPresupuestos1.DevolverCod(nodx)
sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
sCod3 = Trim(txtCod)
sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))

'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3 Is Nothing Then
    Set frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3 = oFSGSRaiz.Generar_CPresProyectosNivel3
End If

frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Add frmPresupuestos1.sdbcAnyo, scod1, sCod2, Trim(txtCod), Trim(txtDen), StrToDblOrNull(txtImp), StrToDblOrNull(txtObj)

If frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4 Is Nothing Then
    Set frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4 = oFSGSRaiz.Generar_CPresProyectosNivel4
End If

Set nodo = frmPresupuestos1.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES3" & scod1 & sCod2 & sCod3, Trim(txtCod) & " - " & Trim(txtDen), "PRES3")

nodo.Tag = "PRES3" & Trim(txtCod)
frmPresupuestos1.tvwEstrPres_NodeClick nodo
nodo.Selected = True
nodo.EnsureVisible
Set nodo = Nothing
Set nodx = Nothing

End Function
Public Function AnyadirPRES4AEstructura()
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim sCod2 As String
Dim sCod3 As String
Dim sCod4 As String

Set nodx = frmPresupuestos1.tvwestrPres.selectedItem
scod1 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
sCod2 = frmPresupuestos1.DevolverCod(nodx.Parent)
sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
sCod3 = frmPresupuestos1.DevolverCod(nodx)
sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
sCod4 = Trim(txtCod)
sCod4 = sCod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(sCod4))
'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4 Is Nothing Then
    Set frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4 = oFSGSRaiz.Generar_CPresProyectosNivel4
End If

frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4.Add frmPresupuestos1.sdbcAnyo, scod1, sCod2, sCod3, Trim(txtCod), Trim(txtDen), StrToDblOrNull(txtImp), StrToDblOrNull(txtObj)

Set nodo = frmPresupuestos1.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES4" & scod1 & sCod2 & sCod3 & sCod4, Trim(txtCod) & " - " & Trim(txtDen), "PRES4")
nodo.Tag = "PRES4" & Trim(txtCod)
frmPresupuestos1.tvwEstrPres_NodeClick nodo
nodo.Selected = True
nodo.EnsureVisible
Set nodo = Nothing
Set nodx = Nothing

End Function
Public Function ModificarPRESEnEstructura()

Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim sCod2 As String
Dim sCod3 As String
Dim sCod4 As String

Set nodx = frmPresupuestos1.tvwestrPres.selectedItem
nodx.Text = Trim(txtCod) & " - " & Trim(txtDen)

Select Case Left(nodx.Tag, 5)
    
    Case "PRES1"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
                    
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            frmPresupuestos1.oPresupuestos.Item(scod1).Den = Trim(txtDen)
            frmPresupuestos1.oPresupuestos.Item(scod1).importe = StrToDblOrNull(txtImp)
            frmPresupuestos1.oPresupuestos.Item(scod1).Objetivo = StrToDblOrNull(txtObj)
            
    Case "PRES2"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).Den = Trim(txtDen)
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).importe = StrToDblOrNull(txtImp)
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).Objetivo = StrToDblOrNull(txtObj)
            
    Case "PRES3"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx.Parent)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            sCod3 = frmPresupuestos1.DevolverCod(nodx)
            sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
            
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).Den = Trim(txtDen)
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).importe = StrToDblOrNull(txtImp)
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).Objetivo = StrToDblOrNull(txtObj)
            
    Case "PRES4"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            sCod3 = frmPresupuestos1.DevolverCod(nodx.Parent)
            sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
            sCod4 = frmPresupuestos1.DevolverCod(nodx)
            sCod4 = sCod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(sCod4))
            
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4.Item(scod1 & sCod2 & sCod3 & sCod4).Den = Trim(txtDen)
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4.Item(scod1 & sCod2 & sCod3 & sCod4).importe = StrToDblOrNull(txtImp)
            frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4.Item(scod1 & sCod2 & sCod3 & sCod4).Objetivo = StrToDblOrNull(txtObj)
            
End Select

Set nodx = Nothing

frmPresupuestos1.MostrarDatosBarraInf

End Function

Private Function EsimporteSuperior() As Boolean
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim sCod2 As String
Dim sCod3 As String
Dim sCod4 As String
Dim dImporte As Double

Set nodx = frmPresupuestos1.tvwestrPres.selectedItem

Select Case Left(nodx.Tag, 5)
    
    Case "PRES1"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
                    
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            If Trim(frmPresupuestos1.oPresupuestos.Item(scod1).importe) = "" Then
                EsimporteSuperior = True
                Exit Function
            Else
                dImporte = SumarImportesHijos(nodx)
                If dImporte > frmPresupuestos1.oPresupuestos.Item(scod1).importe Then
                    EsimporteSuperior = True
                Else
                    EsimporteSuperior = False
                End If
            End If
            
            
    Case "PRES2"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            
            If Trim(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).importe) = "" Then
                EsimporteSuperior = True
                Exit Function
            Else
                dImporte = SumarImportesHijos(nodx)
                If dImporte > frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).importe Then
                    EsimporteSuperior = True
                Else
                    EsimporteSuperior = False
                End If
            End If
            
    Case "PRES3"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx.Parent)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            sCod3 = frmPresupuestos1.DevolverCod(nodx)
            sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
            
            If Trim(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).importe) = "" Then
                EsimporteSuperior = True
                Exit Function
            Else
                dImporte = SumarImportesHijos(nodx)
                If dImporte > frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).importe Then
                    EsimporteSuperior = True
                Else
                    EsimporteSuperior = False
                End If
            End If
            
    Case "PRES4"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            sCod3 = frmPresupuestos1.DevolverCod(nodx.Parent)
            sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
            sCod4 = frmPresupuestos1.DevolverCod(nodx)
            sCod4 = sCod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(sCod4))
            
            If Trim(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4.Item(scod1 & sCod2 & sCod3 & sCod4).importe) = "" Then
                EsimporteSuperior = True
                Exit Function
            Else
                dImporte = SumarImportesHijos(nodx)
                If dImporte > frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4.Item(scod1 & sCod2 & sCod3 & sCod4).importe Then
                    EsimporteSuperior = True
                Else
                    EsimporteSuperior = False
                End If
            End If
            
End Select

Set nodx = Nothing

End Function
Private Function EsimporteSuperiorMod() As Boolean
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim sCod2 As String
Dim sCod3 As String
Dim sCod4 As String
Dim dImporte As Double

Set nodx = frmPresupuestos1.tvwestrPres.selectedItem

Select Case Left(nodx.Tag, 5)
    
    Case "PRES2"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            
            dImporte = NullToDbl0(SumarImportesHijos(nodx.Parent)) - NullToDbl0(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).importe)
            
            If dImporte > frmPresupuestos1.oPresupuestos.Item(scod1).importe Then
                EsimporteSuperiorMod = True
            Else
                EsimporteSuperiorMod = False
            End If
            
            
    Case "PRES3"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx.Parent)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            sCod3 = frmPresupuestos1.DevolverCod(nodx)
            sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
            
            ' La suma de los hijos - su propio valor anterior + el valor actual debe ser inferior al de su padre
            dImporte = NullToDbl0(SumarImportesHijos(nodx.Parent)) - NullToDbl0(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).importe) '+ NullToDbl0(StrToDblOrNull(txtImp))
            
            If dImporte > frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).importe Then
                EsimporteSuperiorMod = True
            Else
                EsimporteSuperiorMod = False
            End If
            
    Case "PRES4"
            
            scod1 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
            sCod2 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent)
            sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
            sCod3 = frmPresupuestos1.DevolverCod(nodx.Parent)
            sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
            sCod4 = frmPresupuestos1.DevolverCod(nodx)
            sCod4 = sCod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(sCod4))
            
            ' La suma de los hijos - su propio valor anterior + el valor actual debe ser inferior al de su padre
            dImporte = NullToDbl0(SumarImportesHijos(nodx.Parent)) - NullToDbl0(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4.Item(scod1 & sCod2 & sCod3 & sCod4).importe) '+ NullToDbl0(StrToDblOrNull(txtImp))
            
            If dImporte > frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).importe Then
                EsimporteSuperiorMod = True
            Else
                EsimporteSuperiorMod = False
            End If
End Select

Set nodx = Nothing

End Function

Private Function SumarImportesHijos(ByVal nodx As MSComctlLib.node) As Double
Dim scod1 As String
Dim sCod2 As String
Dim sCod3 As String
Dim sCod4 As String
Dim dImporte As Double
Dim i As Integer
Dim nodSiguiente As MSComctlLib.node


If nodx.Child Is Nothing Then
    dImporte = NullToDbl0(StrToDblOrNull(txtImp))
Else
    
    Select Case Left(nodx.Tag, 5)
        
        Case "PRES1"
                            
            Set nodSiguiente = nodx.Child
    
            While Not (nodSiguiente Is Nothing)
                
                scod1 = frmPresupuestos1.DevolverCod(nodx)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
                sCod2 = frmPresupuestos1.DevolverCod(nodSiguiente)
                sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
                
                dImporte = dImporte + CDbl(NullToDbl0(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).importe))
                Set nodSiguiente = nodSiguiente.Next
            
            Wend
            
            dImporte = dImporte + CDbl(NullToDbl0(txtImp))
        
        Case "PRES2"
                
            Set nodSiguiente = nodx.Child
    
            While Not (nodSiguiente Is Nothing)
                
                scod1 = frmPresupuestos1.DevolverCod(nodx.Parent)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
                sCod2 = frmPresupuestos1.DevolverCod(nodx)
                sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
                sCod3 = frmPresupuestos1.DevolverCod(nodSiguiente)
                sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
                dImporte = dImporte + NullToDbl0(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).importe)
                Set nodSiguiente = nodSiguiente.Next
            
            Wend
            
            dImporte = dImporte + CDbl(NullToDbl0(txtImp))
                
        Case "PRES3"
                
            Set nodSiguiente = nodx.Child
    
            While Not (nodSiguiente Is Nothing)
                
                scod1 = frmPresupuestos1.DevolverCod(nodx.Parent.Parent)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(scod1))
                sCod2 = frmPresupuestos1.DevolverCod(nodx.Parent)
                sCod2 = sCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sCod2))
                sCod3 = frmPresupuestos1.DevolverCod(nodx)
                sCod3 = sCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sCod3))
                sCod4 = frmPresupuestos1.DevolverCod(nodSiguiente)
                sCod4 = sCod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(sCod4))
                
                dImporte = dImporte + NullToDbl0(frmPresupuestos1.oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & sCod2).PresProyectosNivel3.Item(scod1 & sCod2 & sCod3).PresProyectosNivel4.Item(scod1 & sCod2 & sCod3 & sCod4).importe)
                
                Set nodSiguiente = nodSiguiente.Next
            
            Wend
        
            dImporte = dImporte + CDbl(NullToDbl0(txtImp))
                
    End Select
        
End If

Set nodx = Nothing
Set nodSiguiente = Nothing

SumarImportesHijos = dImporte

End Function

Private Sub txtImp_LostFocus()
    txtImp = Format(txtImp, "Standard")
End Sub

Private Sub txtObj_LostFocus()
    txtObj = Format(txtObj, "0.0#")
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESPROY_DETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    caption = Ador(0).value
    Ador.MoveNext
    lblCod.caption = Ador(0).value
    Ador.MoveNext
    lblDen.caption = Ador(0).value
    Ador.MoveNext
    lblImp.caption = Ador(0).value
    Ador.MoveNext
    lblObj.caption = Ador(0).value
    Ador.MoveNext
    
    cmdAceptar.caption = Ador(0).value
    Ador.MoveNext
    cmdCancelar.caption = Ador(0).value
    Ador.MoveNext
    sIdiCodigo = Ador(0).value
    Ador.MoveNext
    sIdiDenominacion = Ador(0).value
    Ador.MoveNext
    sIdiImporte = Ador(0).value
    Ador.MoveNext
    sIdiObjetivo = Ador(0).value
       
    Ador.Close
    
    End If

    Set Ador = Nothing

End Sub
