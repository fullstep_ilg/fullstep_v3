VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmADJAnya 
   BackColor       =   &H00808000&
   Caption         =   "DComunicaci�n del cierre de proceso"
   ClientHeight    =   6255
   ClientLeft      =   135
   ClientTop       =   2925
   ClientWidth     =   10440
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADJAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6255
   ScaleWidth      =   10440
   Begin VB.Frame fraContratos 
      BackColor       =   &H00808000&
      Caption         =   "DContratos"
      ForeColor       =   &H00FFFFFF&
      Height          =   1185
      Left            =   7470
      TabIndex        =   34
      Top             =   2235
      Width           =   2745
      Begin VB.CheckBox chkContratos 
         BackColor       =   &H00808000&
         Caption         =   "Adjuntar contratos"
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Left            =   390
         TabIndex        =   17
         Top             =   390
         Width           =   2085
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00808000&
      Caption         =   "DEspecificaciones"
      ForeColor       =   &H00FFFFFF&
      Height          =   1545
      Left            =   7470
      TabIndex        =   33
      Top             =   630
      Width           =   2745
      Begin VB.CheckBox chkEsp 
         BackColor       =   &H00808000&
         Caption         =   "DIncluir especificaciones de items"
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Left            =   390
         TabIndex        =   15
         Top             =   300
         Width           =   2085
      End
      Begin VB.CheckBox chkFich 
         BackColor       =   &H00808000&
         Caption         =   "DIncluir archivos adjuntos"
         ForeColor       =   &H00FFFFFF&
         Height          =   495
         Left            =   390
         TabIndex        =   16
         Top             =   855
         Width           =   2085
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Caption         =   "DPlantillas para proveedores NO adjudicados"
      ForeColor       =   &H00FFFFFF&
      Height          =   1185
      Left            =   240
      TabIndex        =   30
      Top             =   2235
      Width           =   7125
      Begin VB.CommandButton cmdDOT 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6735
         Picture         =   "frmADJAnya.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   330
         Width           =   315
      End
      Begin VB.TextBox txtDOT 
         Height          =   285
         Left            =   1440
         MaxLength       =   255
         TabIndex        =   11
         Top             =   330
         Width           =   5235
      End
      Begin VB.CommandButton cmdMailDot 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6735
         Picture         =   "frmADJAnya.frx":0D71
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   750
         Width           =   315
      End
      Begin VB.TextBox txtMailDot 
         Height          =   285
         Left            =   1440
         MaxLength       =   255
         TabIndex        =   13
         Top             =   720
         Width           =   5235
      End
      Begin VB.Label Label7 
         BackColor       =   &H00808000&
         Caption         =   "DPara carta:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   32
         Top             =   390
         Width           =   1290
      End
      Begin VB.Label Label2 
         BackColor       =   &H00808000&
         Caption         =   "DPara e-mail:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   31
         Top             =   750
         Width           =   1260
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Caption         =   "DPlantillas para proveedores adjudicados"
      ForeColor       =   &H00FFFFFF&
      Height          =   1545
      Left            =   240
      TabIndex        =   27
      Top             =   630
      Width           =   7125
      Begin VB.TextBox txtForm 
         Height          =   285
         Left            =   1440
         MaxLength       =   255
         TabIndex        =   9
         Top             =   1080
         Width           =   5235
      End
      Begin VB.CommandButton cmdForm 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6720
         Picture         =   "frmADJAnya.frx":0E30
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   1080
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.CommandButton cmdMailDotAdj 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6720
         Picture         =   "frmADJAnya.frx":0EEF
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   705
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.CommandButton cmdDOTAdj 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6720
         Picture         =   "frmADJAnya.frx":0FAE
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   330
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.TextBox txtMailDotAdj 
         Height          =   285
         Left            =   1440
         MaxLength       =   255
         TabIndex        =   7
         Top             =   705
         Width           =   5235
      End
      Begin VB.TextBox txtDOTAdj 
         Height          =   285
         Left            =   1440
         MaxLength       =   255
         TabIndex        =   5
         Top             =   330
         Width           =   5235
      End
      Begin VB.Label lblForm 
         BackColor       =   &H00808000&
         Caption         =   "Archivo de notificacion:"
         ForeColor       =   &H00FFFFFF&
         Height          =   420
         Left            =   135
         TabIndex        =   35
         Top             =   1035
         Width           =   1245
      End
      Begin VB.Label Label6 
         BackColor       =   &H00808000&
         Caption         =   "DPara e-mail:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   150
         TabIndex        =   29
         Top             =   750
         Width           =   1290
      End
      Begin VB.Label Label8 
         BackColor       =   &H00808000&
         Caption         =   "DPara carta:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   150
         TabIndex        =   28
         Top             =   375
         Width           =   1320
      End
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   10440
      TabIndex        =   19
      Top             =   5760
      Width           =   10440
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "D&Aceptar"
         Height          =   345
         Left            =   4110
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "D&Cancelar"
         Height          =   345
         Left            =   5280
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   8700
      Top             =   3180
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPet 
      Height          =   2175
      Left            =   90
      TabIndex        =   18
      Top             =   3555
      Width           =   13440
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   15
      stylesets.count =   2
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJAnya.frx":106D
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmADJAnya.frx":1089
      UseGroups       =   -1  'True
      BevelColorFrame =   64
      BevelColorShadow=   8421504
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      HeadStyleSet    =   "General"
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   3
      Groups(0).Width =   7646
      Groups(0).Caption=   "DProveedores"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   1958
      Groups(0).Columns(0).Caption=   "DAdjudicado"
      Groups(0).Columns(0).Name=   "ADJ"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).Style=   2
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   16777140
      Groups(0).Columns(1).Width=   2514
      Groups(0).Columns(1).Caption=   "DC�digo"
      Groups(0).Columns(1).Name=   "CODPROVE"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(2).Width=   3175
      Groups(0).Columns(2).Caption=   "DDenominaci�n"
      Groups(0).Columns(2).Name=   "DENPROVE"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(1).Width =   6668
      Groups(1).Caption=   "DContactos"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   2
      Groups(1).Columns(0).Width=   4207
      Groups(1).Columns(0).Visible=   0   'False
      Groups(1).Columns(0).Caption=   "DApellidos"
      Groups(1).Columns(0).Name=   "APE"
      Groups(1).Columns(0).DataField=   "Column 3"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(1).Width=   6668
      Groups(1).Columns(1).Caption=   "DNombre"
      Groups(1).Columns(1).Name=   "NOM"
      Groups(1).Columns(1).DataField=   "Column 4"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(2).Width =   3307
      Groups(2).Caption=   "DVia notificaci�n"
      Groups(2).HasHeadForeColor=   -1  'True
      Groups(2).HasHeadBackColor=   -1  'True
      Groups(2).HeadForeColor=   16777215
      Groups(2).HeadBackColor=   8421504
      Groups(2).Columns.Count=   10
      Groups(2).Columns(0).Width=   1561
      Groups(2).Columns(0).Caption=   "DMail"
      Groups(2).Columns(0).Name=   "MAIL"
      Groups(2).Columns(0).CaptionAlignment=   2
      Groups(2).Columns(0).DataField=   "Column 5"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Style=   2
      Groups(2).Columns(1).Width=   1746
      Groups(2).Columns(1).Caption=   "DCarta"
      Groups(2).Columns(1).Name=   "IMP"
      Groups(2).Columns(1).CaptionAlignment=   2
      Groups(2).Columns(1).DataField=   "Column 6"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(1).Style=   2
      Groups(2).Columns(2).Width=   661
      Groups(2).Columns(2).Visible=   0   'False
      Groups(2).Columns(2).Caption=   "EMAIL"
      Groups(2).Columns(2).Name=   "EMAIL"
      Groups(2).Columns(2).DataField=   "Column 7"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(3).Width=   1640
      Groups(2).Columns(3).Visible=   0   'False
      Groups(2).Columns(3).Caption=   "CODCON"
      Groups(2).Columns(3).Name=   "CODCON"
      Groups(2).Columns(3).DataField=   "Column 8"
      Groups(2).Columns(3).DataType=   8
      Groups(2).Columns(3).FieldLen=   256
      Groups(2).Columns(4).Width=   1058
      Groups(2).Columns(4).Visible=   0   'False
      Groups(2).Columns(4).Caption=   "TFNO"
      Groups(2).Columns(4).Name=   "TFNO"
      Groups(2).Columns(4).DataField=   "Column 9"
      Groups(2).Columns(4).DataType=   8
      Groups(2).Columns(4).FieldLen=   256
      Groups(2).Columns(5).Width=   1138
      Groups(2).Columns(5).Visible=   0   'False
      Groups(2).Columns(5).Caption=   "TFNO2"
      Groups(2).Columns(5).Name=   "TFNO2"
      Groups(2).Columns(5).DataField=   "Column 10"
      Groups(2).Columns(5).DataType=   8
      Groups(2).Columns(5).FieldLen=   256
      Groups(2).Columns(6).Width=   1323
      Groups(2).Columns(6).Visible=   0   'False
      Groups(2).Columns(6).Caption=   "Adj."
      Groups(2).Columns(6).Name=   "FAX"
      Groups(2).Columns(6).DataField=   "Column 11"
      Groups(2).Columns(6).DataType=   8
      Groups(2).Columns(6).FieldLen=   256
      Groups(2).Columns(6).Style=   2
      Groups(2).Columns(7).Width=   1640
      Groups(2).Columns(7).Visible=   0   'False
      Groups(2).Columns(7).Caption=   "TFNOMOVIL"
      Groups(2).Columns(7).Name=   "TFNO_MOVIL"
      Groups(2).Columns(7).DataField=   "Column 12"
      Groups(2).Columns(7).DataType=   8
      Groups(2).Columns(7).FieldLen=   256
      Groups(2).Columns(8).Width=   635
      Groups(2).Columns(8).Visible=   0   'False
      Groups(2).Columns(8).Caption=   "PORTALCON"
      Groups(2).Columns(8).Name=   "PORTALCON"
      Groups(2).Columns(8).DataField=   "Column 13"
      Groups(2).Columns(8).DataType=   8
      Groups(2).Columns(8).FieldLen=   256
      Groups(2).Columns(9).Width=   1852
      Groups(2).Columns(9).Visible=   0   'False
      Groups(2).Columns(9).Caption=   "TIPOEMAIL"
      Groups(2).Columns(9).Name=   "TIPOEMAIL"
      Groups(2).Columns(9).DataField=   "Column 14"
      Groups(2).Columns(9).DataType=   8
      Groups(2).Columns(9).FieldLen=   256
      _ExtentX        =   23707
      _ExtentY        =   3836
      _StockProps     =   79
      ForeColor       =   64
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveCod 
      Height          =   1665
      Left            =   900
      TabIndex        =   21
      Top             =   4050
      Width           =   5175
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJAnya.frx":10A5
      stylesets(0).AlignmentText=   0
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmADJAnya.frx":10C1
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3201
      Columns(0).Caption=   "DCod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6006
      Columns(1).Caption=   "DDenominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9128
      _ExtentY        =   2937
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveDen 
      Height          =   1575
      Left            =   1680
      TabIndex        =   22
      Top             =   4155
      Width           =   5055
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJAnya.frx":10DD
      stylesets(0).AlignmentText=   0
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmADJAnya.frx":10F9
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5477
      Columns(0).Caption=   "DDenominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2990
      Columns(1).Caption=   "DC�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8916
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConApe 
      Height          =   1515
      Left            =   1140
      TabIndex        =   20
      Top             =   4050
      Width           =   5715
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJAnya.frx":1115
      stylesets(0).AlignmentText=   0
      stylesets(1).Name=   "Tan"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmADJAnya.frx":1131
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   10
      Columns(0).Width=   3201
      Columns(0).Caption=   "DApellidos"
      Columns(0).Name =   "APE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1852
      Columns(1).Caption=   "DNombre"
      Columns(1).Name =   "NOM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3757
      Columns(2).Caption=   "DMail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "TFNO"
      Columns(4).Name =   "TFNO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "TFNO2"
      Columns(5).Name =   "TFNO2"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "FAX"
      Columns(6).Name =   "FAX"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TFNOMOVIL"
      Columns(7).Name =   "TFNO_MOVIL"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "PORT"
      Columns(8).Name =   "PORT"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "TIPOEMAIL"
      Columns(9).Name =   "TIPOEMAIL"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   10081
      _ExtentY        =   2672
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConNom 
      Height          =   1515
      Left            =   2520
      TabIndex        =   23
      Top             =   4170
      Width           =   5775
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJAnya.frx":114D
      stylesets(0).AlignmentText=   0
      stylesets(1).Name=   "Tan"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmADJAnya.frx":1169
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   10
      Columns(0).Width=   1852
      Columns(0).Caption=   "DNombre"
      Columns(0).Name =   "NOM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4022
      Columns(1).Caption=   "DApellidos"
      Columns(1).Name =   "APE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3942
      Columns(2).Caption=   "DMail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "TFNO"
      Columns(4).Name =   "TFNO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "TFNO2"
      Columns(5).Name =   "TFNO2"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "FAX"
      Columns(6).Name =   "FAX"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TFNOMOVIL"
      Columns(7).Name =   "TFNO_MOVIL"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "PORT"
      Columns(8).Name =   "PORT"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "TIPOEMAIL"
      Columns(9).Name =   "TIPOEMAIL"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   2672
      _StockProps     =   77
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00FFFFFF&
      Height          =   3450
      Left            =   90
      Top             =   60
      Width           =   10275
   End
   Begin VB.Label lblFecPresBox 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5122
      TabIndex        =   3
      Top             =   210
      Width           =   1290
   End
   Begin VB.Label lblFecPres 
      BackColor       =   &H00808000&
      Caption         =   "DPresentaci�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3600
      TabIndex        =   26
      Top             =   270
      Width           =   1410
   End
   Begin VB.Label lblFecNecBox 
      BackColor       =   &H00E1FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00000040&
      Height          =   285
      Left            =   1680
      TabIndex        =   2
      Top             =   210
      Width           =   1290
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "DNecesidad:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   420
      TabIndex        =   25
      Top             =   270
      Width           =   1245
   End
   Begin VB.Label Label3 
      BackColor       =   &H00808000&
      Caption         =   "DCierre:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   7665
      TabIndex        =   24
      Top             =   270
      Width           =   1005
   End
   Begin VB.Label LblFecCierreBox 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   8790
      TabIndex        =   4
      Top             =   210
      Width           =   1290
   End
End
Attribute VB_Name = "frmADJAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oFos As Scripting.FileSystemObject
Private oProves As CProveedores
Private oProve As CProveedor
Private oContactos As CContactos
Private oIasig As IAsignaciones  ' Para cargar las combos de proveedores asignables
Private m_oAsigs As CAsignaciones 'Para las asignaciones proves-grupos
Private bCargarComboDesdeDD As Boolean
'Guardaremos los nombres de los temporales aqui, para borrarlos al descargarse el formulario
Private sayFileNames() As String
' nombres de ficheros a visualizar en el frmMensaje
Private Nombres() As String
'Textos de la traducci�n
Private sCap(1 To 21) As String
Private appword  As Object
Private sOrdenCompra As String
Private sFormatoNumber As String
Private sNotifAdj  As String
Private m_iWordVer As Integer

Private m_sOrden As String
Private m_sNotif As String
Private m_sSi As String
Private m_sNo As String
Private oProvesAsignados As CProveedores

Private m_sOperacion As String
Private m_sTotalItem As String
Private m_sItem As String
Private m_sTotalGrupo As String
Private m_sTotalProce As String

Public g_bCancelarMail As Boolean
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String
Private Sub chkEsp_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkEsp.Value = vbUnchecked Then
        chkFich.Value = vbUnchecked
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "chkEsp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub chkFich_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If chkEsp.Value = vbUnchecked Then
        chkFich.Value = vbUnchecked
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "chkFich_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Comunicar
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim oPet As CPetOferta
    Dim oPets As CPetOfertas
    Dim oIPet As IPeticiones
    Dim teserror As TipoErrorSummit
    Dim oItem As CItem
    Dim sVersion As String
    Dim docword As Object
    Dim docOrdenWord As Object
    Dim sTempNombreWord  As String
    Dim rangeword As Object
    Dim Section As Object
    Dim bVisibleWord As Boolean
    Dim bSesionIniciada As Boolean   'Indica si se ha iniciado una sesion de correo
    Dim i As Integer
    Dim splantilla As String
    Dim sPlantillaMail As String
    Dim sPlantillaAdj As String
    Dim sPlantillaMailAdj As String
    Dim bWeb As Boolean
    Dim bEMail As Boolean
    Dim bImp As Boolean
    Dim sTemp As String
    Dim bAdjudicado As Boolean
    Dim bAdjuntarImpreso As Boolean
    Dim iNumPet As Integer
    Dim iNumPets As Integer
    Dim iTipoComunicacion As Integer
    Dim bSalvar As Boolean
    Dim errormail As TipoErrorSummit
    Dim sCuerpo As String
    Dim sMailSubject As String
    Dim ResulGrabaError As TipoErrorSummit
    Dim bSiguiente As Boolean
    Dim bEsHTML As Boolean
    Dim oOferta As COferta
    Dim oiasignaciones As IAsignaciones
    Dim oAsignaciones As CAsignaciones
    Dim oEquipo As CEquipo
    Dim oError As TipoErrorSummit
    Dim bComunicando As Boolean
    Dim oCEmailAdj As CEmailAdj
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo ERROR_Frm:
    
    'Comprobamos que las plantillas  de word son v�lidas
    If Trim(txtDOT.Text) = "" Then
        oMensajes.NoValido sCap(1)
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    Else
        If Right(txtDOT.Text, 3) <> "dot" Then
            oMensajes.NoValido sCap(1)
            If Me.Visible Then txtDOT.SetFocus
            Exit Sub
        End If
    End If
    If Not oFos.FileExists(txtDOT.Text) Then
        oMensajes.PlantillaNoEncontrada txtDOT.Text
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    End If
    
    If Trim(txtDOTAdj.Text) = "" Then
        oMensajes.NoValido sCap(1)
        If Me.Visible Then txtDOTAdj.SetFocus
        Exit Sub
    Else
        If Right(txtDOTAdj.Text, 3) <> "dot" Then
            oMensajes.NoValido sCap(1)
            If Me.Visible Then txtDOTAdj.SetFocus
            Exit Sub
        End If
    End If
    If Not oFos.FileExists(txtDOTAdj.Text) Then
        oMensajes.PlantillaNoEncontrada txtDOTAdj.Text
        If Me.Visible Then txtDOTAdj.SetFocus
        Exit Sub
    End If
    
    
    splantilla = txtDOT.Text
    sPlantillaAdj = txtDOTAdj.Text
    
    ' Plantillas para notificaciones via mail
    If gParametrosGenerales.giMail <> 0 Then
        If Trim(txtMailDot.Text) = "" Then
            oMensajes.NoValido sCap(1)
            If Me.Visible Then txtMailDot.SetFocus
            Exit Sub
        Else
            If LCase(Right(txtMailDot.Text, 3)) <> "htm" Then
                oMensajes.NoValido sCap(1)
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
            If Not oFos.FileExists(txtMailDot.Text) Then
                oMensajes.PlantillaNoEncontrada txtMailDot
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
            splantilla = Left(txtMailDot.Text, Len(txtMailDot.Text) - 4) & ".txt"
            If Not oFos.FileExists(splantilla) Then
                oMensajes.PlantillaNoEncontrada splantilla
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
        End If
        
        If Trim(txtMailDotAdj.Text) = "" Then
            oMensajes.NoValido sCap(1)
            If Me.Visible Then txtMailDot.SetFocus
            Exit Sub
        Else
            If LCase(Right(txtMailDotAdj.Text, 3)) <> "htm" Then
                oMensajes.NoValido sCap(1)
                If Me.Visible Then txtMailDotAdj.SetFocus
                Exit Sub
            End If
            If Not oFos.FileExists(txtMailDotAdj.Text) Then
                oMensajes.PlantillaNoEncontrada txtMailDotAdj.Text
                If Me.Visible Then txtMailDotAdj.SetFocus
                Exit Sub
            End If
            splantilla = Left(txtMailDotAdj.Text, Len(txtMailDotAdj.Text) - 4) & ".txt"
            If Not oFos.FileExists(splantilla) Then
                oMensajes.PlantillaNoEncontrada splantilla
                If Me.Visible Then txtMailDotAdj.SetFocus
                Exit Sub
            End If
        End If
        sPlantillaMail = txtMailDot.Text
        sPlantillaMailAdj = txtMailDotAdj.Text
    End If
    
    'Creamos las peticiones de ofertas
    Set oPets = Nothing
    Set oPets = oFSGSRaiz.generar_CPetOfertas
   
    'Generamos las peticiones correspondientes
    sdbgPet.MoveFirst
    iNumPet = 0
    For i = 0 To sdbgPet.Rows - 1
    
        If sdbgPet.Columns(1).Value <> "" And (sdbgPet.Columns(5).Value <> "0" Or sdbgPet.Columns(6).Value <> "0") Then
            If (sdbgPet.Columns(5).Value <> "0" And sdbgPet.Columns(3).Value <> "") Or sdbgPet.Columns(6).Value <> "0" Then
                bWeb = False
                bEMail = False
                bImp = False
                bEsHTML = (sdbgPet.Columns("TIPOEMAIL").Value = 1)
                
                If sdbgPet.Columns(5).Value <> "0" Then
                    bEMail = True
                Else
                    If sdbgPet.Columns(6).Value <> "0" Then
                        bImp = True
                    End If
                End If
                If sdbgPet.Columns(0).Value <> "0" Then
                    iTipoComunicacion = 2
                Else
                    iTipoComunicacion = 3
                End If
                'Se reusa AntesDeCierreParcial para guardar si es html � txt
                oPets.Add sdbgPet.Columns(1).Value, sdbgPet.Columns(2).Value, bWeb, bEMail, bImp, Date, Int(val(sdbgPet.Columns(8).Value)), sdbgPet.Columns(3).Value, sdbgPet.Columns(4).Value, sdbgPet.Columns(7).Value, iNumPet, sdbgPet.Columns("TFNO").Value, sdbgPet.Columns("TFNO2").Value, sdbgPet.Columns("FAX").Value, CDate(frmADJAnya.LblFecCierreBox), iTipoComunicacion, sdbgPet.Columns("TFNO_MOVIL").Value, bEsHTML
                iNumPet = iNumPet + 1
            End If
        End If
        sdbgPet.MoveNext
    Next
    
    If oPets.Count = 0 Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    frmESPERA.lblGeneral.caption = sCap(2)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sCap(3)
    
    ' Recorremos las peticiones para enviar un mail u obtener un impreso
    sTemp = DevolverPathFichTemp
    
    iNumPet = 0
    iNumPets = oPets.Count
    
    For Each oPet In oPets
        sCuerpo = ""
        Set docword = Nothing
    
        g_bCancelarMail = False
        
        ReDim Nombres(0)
        iNumPet = iNumPet + 1
        
        frmESPERA.lblContacto = sCap(4) & " " & oPet.nombre & " " & oPet.Apellidos
        frmESPERA.lblContacto.Refresh
        DoEvents
        frmESPERA.ProgressBar2.Value = val((iNumPet / iNumPets) * 100)
        frmESPERA.ProgressBar1.Max = 10
        frmESPERA.ProgressBar1.Value = 1
        frmESPERA.lblDetalle = sCap(5)
        frmESPERA.lblDetalle.Refresh
        
        ' DATOS PARA COMPRADORES
        
        Set oiasignaciones = frmOFEPet.oProcesoSeleccionado
        Set oAsignaciones = oiasignaciones.DevolverAsignaciones(, , , oPet.CodProve)
        If frmOFEPet.oProcesoSeleccionado.TError.NumError <> TESnoerror Then
            teserror = frmOFEPet.oProcesoSeleccionado.TError
            frmOFEPet.oProcesoSeleccionado.TError.NumError = TESnoerror
            GoTo ERROR_Frm
        End If
        Set oEquipo = Nothing
        Set oEquipo = oFSGSRaiz.generar_CEquipo
        oEquipo.Cod = oAsignaciones.Item(1).codEqp
        oEquipo.CargarTodosLosCompradoresDesde 1, oAsignaciones.Item(1).CodComp
        If oEquipo.TError.NumError <> TESnoerror Then
            teserror = oEquipo.TError
            oEquipo.TError.NumError = TESnoerror
            GoTo ERROR_Frm
        End If
        Set oOferta = frmOFEPet.oProcesoSeleccionado.DevolverUltimaOfertaProveedor(oPet.CodProve, False, True, , basPublic.gParametrosInstalacion.gIdioma)
        If frmOFEPet.oProcesoSeleccionado.TError.NumError <> TESnoerror Then
            teserror = frmOFEPet.oProcesoSeleccionado.TError
            frmOFEPet.oProcesoSeleccionado.TError.NumError = TESnoerror
            GoTo ERROR_Frm
        End If
        
        ' Proveedor Adjudicado / NO Adjudicado
        If frmOFEPet.oProvesAdj.Item(oPet.CodProve) Is Nothing Then
            bAdjudicado = False
        Else
            bAdjudicado = True
        End If
                
        DoEvents
        
                
        If oPet.viaEMail And oPet.Email <> "" Then
            ' Env�o de mensaje al proveedor VIA MAIL
                      
            If Not bSesionIniciada Or oIdsMail Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sCap(5)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set oIdsMail = IniciarSesionMail
                bSesionIniciada = True
            End If
            
            If appword Is Nothing Then
                frmESPERA.ProgressBar1.Value = 3
                frmESPERA.lblDetalle = sCap(6)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set appword = CreateObject("Word.Application")
                bSalvar = appword.Options.SavePropertiesPrompt
                appword.Options.SavePropertiesPrompt = False
                bVisibleWord = appword.Visible
                appword.WindowState = 2
                sVersion = appword.VERSION
                If InStr(1, sVersion, ".") > 1 Then
                    sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                    m_iWordVer = CInt(sVersion)
                Else
                    m_iWordVer = 9
                End If
            Else 'Para ver si se ha podido cerrar el word
                sVersion = appword.VERSION
            End If
                        
            frmESPERA.ProgressBar1.Value = 4
            DoEvents
            If Not bAdjudicado Then
                'PROVEEDOR EXCLUIDO
                frmESPERA.lblDetalle = sCap(7) & " " & sPlantillaMail & " ..."
                frmESPERA.lblDetalle.Refresh
                                                                
                Set oCEmailAdj = GenerarCEmailAdj(oFSGSRaiz, gParametrosGenerales, gParametrosInstalacion, oMensajes, oUsuarioSummit, basParametros.gLongitudesDeCodigos, basOptimizacion.gTipoDeUsuario)
                sCuerpo = oCEmailAdj.GenerarMensajeAdjudicado(oPet, oEquipo, oOferta, frmOFEPet.oProcesoSeleccionado, m_bDescargarFrm, m_bActivado, CInt(frmOFEPet.sdbcAnyo.Text), frmOFEPet.sdbcGMN1_4Cod.Text, _
                                                              CLng(frmOFEPet.sdbcProceCod.Text), frmOFEPet.sdbcProceDen.Text, LblFecCierreBox, Me.txtMailDot.Text)
                Set oCEmailAdj = Nothing
                                                                
                frmESPERA.ProgressBar1.Value = 5
                frmESPERA.lblDetalle = sCap(8)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                               
                sMailSubject = ComponerAsuntoEMail(gParametrosInstalacion.gsAdjNoMailSubject, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, _
                                                        frmOFEPet.oProcesoSeleccionado.Cod, frmOFEPet.oProcesoSeleccionado.Den)
                bComunicando = True
                errormail = ComponerMensaje(oPet.Email, sMailSubject, sCuerpo, , "frmADJAnya", oPet.AntesDeCierreParcial, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, _
                                            entidadNotificacion:=ProcesoCompra, _
                                            tipoNotificacion:=ProveNoAdjudicado, _
                                            Anyo:=frmOFEPet.oProcesoSeleccionado.Anyo, _
                                            GMN1:=frmOFEPet.oProcesoSeleccionado.GMN1Cod, _
                                            Proce:=frmOFEPet.oProcesoSeleccionado.Cod, _
                                            sToName:=oPet.nombre & " " & oPet.Apellidos)
                
                
                If errormail.NumError <> TESnoerror Then
                    oPet.MailCancel = True
                    GoTo ERROR_Frm
                ElseIf g_bCancelarMail = True Then
                    oPet.MailCancel = True
                End If
                                
            Else
                'PROVEEDOR ADJUDICADO
                
                frmESPERA.lblDetalle = sCap(7) & " " & sPlantillaMailAdj & " ..."
                frmESPERA.lblDetalle.Refresh
                                                
                Set oCEmailAdj = GenerarCEmailAdj(oFSGSRaiz, gParametrosGenerales, gParametrosInstalacion, oMensajes, oUsuarioSummit, basParametros.gLongitudesDeCodigos, basOptimizacion.gTipoDeUsuario)
                sCuerpo = oCEmailAdj.GenerarMensajeAdjudicado(oPet, oEquipo, oOferta, frmOFEPet.oProcesoSeleccionado, m_bDescargarFrm, m_bActivado, CInt(frmOFEPet.sdbcAnyo.Text), frmOFEPet.sdbcGMN1_4Cod.Text, _
                                                              CLng(frmOFEPet.sdbcProceCod.Text), frmOFEPet.sdbcProceDen.Text, LblFecCierreBox, txtMailDotAdj.Text)
                Set oCEmailAdj = Nothing
                                
                frmESPERA.ProgressBar1.Value = 5
                        
                If gParametrosInstalacion.gbAdjOrden Then
                    If frmOFEPet.bOrdenCompra Then
                        'ADJUNTAR ORDEN DE COMPRA
                        frmESPERA.lblDetalle = sCap(9)
                        frmESPERA.lblDetalle.Refresh
                        DoEvents
                    
                        Set docOrdenWord = OrdenCompra(oPet, True, oEquipo)
                        If Not docOrdenWord Is Nothing Then
                            sTemp = DevolverPathFichTemp
                            sTempNombreWord = DevolverNombreArchivo(sOrdenCompra, sTemp, "doc")
                            docOrdenWord.SaveAs sTemp & sTempNombreWord
                            docOrdenWord.Close False
                            sayFileNames(UBound(sayFileNames)) = sTemp & sTempNombreWord
                            ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                            Nombres(UBound(Nombres)) = sTempNombreWord
                            ReDim Preserve Nombres(UBound(Nombres) + 1)
                        End If
                    End If
                ElseIf gParametrosInstalacion.gbAdjNotif Then
                    'NO ADJUNTAR ORDEN DE COMPRA. ADJUNTAR NOTIFICACI�N DE ADJUDICACI�N
                    frmESPERA.lblDetalle = sCap(20)
                    frmESPERA.lblDetalle.Refresh
                    DoEvents
                
                    Set docOrdenWord = OrdenCompra(oPet, False, oEquipo)
                    If Not docOrdenWord Is Nothing Then
                        sTemp = DevolverPathFichTemp
                        sTempNombreWord = DevolverNombreArchivo(sNotifAdj, sTemp, "doc")
                        docOrdenWord.SaveAs sTemp & sTempNombreWord
                        docOrdenWord.Close False
                        sayFileNames(UBound(sayFileNames)) = sTemp & sTempNombreWord
                        ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                        Nombres(UBound(Nombres)) = sTempNombreWord
                        ReDim Preserve Nombres(UBound(Nombres) + 1)
                    End If
                                    
                End If
                
                If chkFich.Value = vbChecked Then
                    AdjuntarEspecificaciones sTemp, oPet.CodProve
                End If
                
                If chkContratos.Value = vbChecked Then
                    AdjuntarContratos
                End If
                frmESPERA.ProgressBar1.Value = 10
                frmESPERA.lblDetalle = sCap(8)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                     
                sMailSubject = ComponerAsuntoEMail(gParametrosInstalacion.gsAdjMailSubject, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, frmOFEPet.oProcesoSeleccionado.Den)
                bComunicando = True
                errormail = ComponerMensaje(oPet.Email, sMailSubject, sCuerpo, Nombres, "frmADJAnya", oPet.AntesDeCierreParcial, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, _
                                                entidadNotificacion:=ProcesoCompra, _
                                                tipoNotificacion:=AdjudicacionYOrdenCompra, _
                                                Anyo:=frmOFEPet.oProcesoSeleccionado.Anyo, _
                                                GMN1:=frmOFEPet.oProcesoSeleccionado.GMN1Cod, _
                                                Proce:=frmOFEPet.oProcesoSeleccionado.Cod, _
                                                sToName:=oPet.nombre & " " & oPet.Apellidos)
                If errormail.NumError <> TESnoerror Then
                    oPet.MailCancel = True
                    GoTo ERROR_Frm
                ElseIf g_bCancelarMail = True Then
                    oPet.MailCancel = True
                End If
            End If
            
        Else
                
                'Generamos un documento de notificaci�n impresa para cada contacto
                
                ' *************** PETICIONES VIA IMPRESO **********************
                
            If oPet.ViaImp Then
                    
                bAdjuntarImpreso = True
                
                oPet.MailCancel = False
                
                If appword Is Nothing Then
                    frmESPERA.ProgressBar1.Value = 4
                    frmESPERA.lblDetalle = sCap(6)
                    frmESPERA.lblDetalle.Refresh
                    DoEvents
                    Set appword = CreateObject("Word.Application")
                    bSalvar = appword.Options.SavePropertiesPrompt
                    appword.Options.SavePropertiesPrompt = False
                    bVisibleWord = appword.Visible
                    appword.WindowState = 2
                    sVersion = appword.VERSION
                    If InStr(1, sVersion, ".") > 1 Then
                        sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                        m_iWordVer = CInt(sVersion)
                    Else
                        m_iWordVer = 9
                    End If
                Else
                    sVersion = appword.VERSION
                End If
                    
                frmESPERA.ProgressBar1.Value = 6
                If Not bAdjudicado Then
                    frmESPERA.lblDetalle = sCap(7) & " " & splantilla & " ..."
                    frmESPERA.lblDetalle.Refresh
                    
                    Set docword = CartaNoAdjudicado(splantilla, oPet, oEquipo)
                    
                Else
                    frmESPERA.lblDetalle.Refresh
                    DoEvents
                    
                    ' PROVEEDOR ADJUDICADO
                    Set docword = CartaAdjOrden(sPlantillaAdj, oPet, oEquipo, oOferta)
                    
                    If gParametrosInstalacion.gbAdjOrden Then
                        If frmOFEPet.bOrdenCompra Then
                            frmESPERA.lblDetalle = sCap(7) & " " & sPlantillaAdj & " ..."
                            frmESPERA.lblDetalle.Refresh
                            Set docOrdenWord = OrdenCompra(oPet, True, oEquipo)
                            If Not docOrdenWord Is Nothing Then
                                ' Se pega este �ltimo documento a las cartas de adjudicaci�n
                                docOrdenWord.Range.Select
                                docOrdenWord.Range.Copy
                                docOrdenWord.Close False
                                Set rangeword = docword.Range
                                rangeword.Collapse 0 'wdCollapseEnd
                                Set Section = docword.Sections.Add(rangeword, 0)
                                docword.Sections(2).Range.Paste
                            End If
                        End If
                    ElseIf gParametrosInstalacion.gbAdjNotif Then
                        frmESPERA.lblDetalle = sCap(7) & " " & sPlantillaAdj & " ..."
                        frmESPERA.lblDetalle.Refresh
                        Set docOrdenWord = OrdenCompra(oPet, False, oEquipo)
                        If Not docOrdenWord Is Nothing Then
                            ' Se pega este �ltimo documento a las cartas de adjudicaci�n
                            docOrdenWord.Range.Select
                            docOrdenWord.Range.Copy
                            docOrdenWord.Close False
                            Set rangeword = docword.Range
                            rangeword.Collapse 0 'wdCollapseEnd
                            Set Section = docword.Sections.Add(rangeword, 0)
                            docword.Sections(2).Range.Paste
                        End If
                    End If
                    frmESPERA.ProgressBar1.Value = 10
                    frmESPERA.lblDetalle.Refresh
                    DoEvents
                        
                End If
            End If
        End If
        
Siguiente:
        bComunicando = False
        If oPet.MailCancel = False Then
            basSeguridad.RegistrarAccion accionessummit.ACCPetComAnya, "Anyo:" & CStr(frmOFEPet.sdbcAnyo.Value) & "GMN1:" & CStr(frmOFEPet.sdbcGMN1_4Cod.Value) & "Proce:" & Int(frmOFEPet.sdbcProceCod.Value) & "Prove:" & oPet.CodProve & "Con:" & oPet.Apellidos & " " & oPet.nombre
        End If
    Next
          
    bComunicando = False
    If Not appword Is Nothing Then appword.Options.SavePropertiesPrompt = bSalvar
        
    frmESPERA.ProgressBar1.Value = 2
    frmESPERA.lblDetalle = sCap(21)
    
    'Modificamos la fecha l�mite de ofertas del proceso
    Set oIPet = frmOFEPet.oProcesoSeleccionado
    
    ' Almacenamos las notificaciones en la BD
    teserror = oIPet.RealizarComunicacion(oPets)
    
    If teserror.NumError <> TESnoerror Then
        GoTo ERROR_Frm
    End If
    
    If bAdjuntarImpreso Then
        frmESPERA.ProgressBar1.Value = 5
        frmESPERA.lblDetalle = sCap(11)
        frmESPERA.lblDetalle.Refresh
        DoEvents
        If Not appword Is Nothing Then
            appword.WindowState = 1
            appword.Visible = True
        End If
        bVisibleWord = True
    End If
        
FinalControlado:
    If bSesionIniciada Then
        frmESPERA.ProgressBar1.Value = 10
        frmESPERA.lblDetalle = sCap(12)
        FinalizarSesionMail
    End If
    frmOFEPet.cmdRestaurar_Click
    
    Unload frmESPERA
    Set oProve = Nothing
    Set oProves = Nothing
    If bVisibleWord = False And Not appword Is Nothing Then
        ' cuando se realizan solo peticiones via web, queda una tarea
        ' de word abierta que no se destruye con appword = nothing
        appword.Quit
    End If
    Set appword = Nothing
    Set docword = Nothing
    Set rangeword = Nothing
    Set Section = Nothing
    Set oEquipo = Nothing
    Set oiasignaciones = Nothing
    Set oAsignaciones = Nothing
    
    Set oItem = Nothing
    Set oPets = Nothing
    Set oPet = Nothing
    Set oIPet = Nothing
    Set oItem = Nothing
    Set oContactos = Nothing
    Set oOferta = Nothing

    Screen.MousePointer = vbNormal
    
    Unload Me
       
    Exit Sub
                    
ERROR_Frm:
    oError.Arg1 = err.Number
    oError.Arg2 = err.Description

    If bComunicando And errormail.NumError <> TESnoerror Then
        'viene de un error en componermensaje con lo que el error ya se ha guardado en BD, muestro un mensaje y siguiente
        If Right(CStr(errormail.Arg2), 3) = "$$4" Then
            oMensajes.ErrorComunicacionMail_MalMail False, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, Left(CStr(errormail.Arg2), Len(CStr(errormail.Arg2)) - 3)
        ElseIf Right(CStr(errormail.Arg2), 3) = "$$5" Then
            oMensajes.ErrorComunicacionMail_MalMail True, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, Left(CStr(errormail.Arg2), Len(CStr(errormail.Arg2)) - 3), errormail.Arg1
        Else
            oMensajes.ErrorComunicacionMail errormail.Arg1, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre
        End If
        bSiguiente = True
    ElseIf teserror.NumError <> TESnoerror Then
        TratarError teserror
        bSiguiente = False
    Else
        If oError.Arg1 = 462 Then
            ResulGrabaError = basErrores.GrabarError("frmAdjAnya.cmdAceptar", CStr(oError.Arg1), "Descr:" & oError.Arg2)
            ' the remote server machine... " han cerrado el word"
            Set appword = Nothing
            DoEvents
            Set appword = CreateObject("Word.Application")
            bSalvar = appword.Options.SavePropertiesPrompt
            appword.Options.SavePropertiesPrompt = False
            bVisibleWord = appword.Visible
            appword.WindowState = 2
            sVersion = appword.VERSION
            If InStr(1, sVersion, ".") > 1 Then
                sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                m_iWordVer = CInt(sVersion)
            Else
                m_iWordVer = 9
            End If
            Resume Next
        Else
            If oPet Is Nothing Then
                ResulGrabaError = basErrores.GrabarError("frmAdjAnya.cmdAceptar", CStr(oError.Arg1), "Descr:" & oError.Arg2)
            Else
                ResulGrabaError = basErrores.GrabarError("frmAdjAnya.cmdAceptar", CStr(oError.Arg1), "Prove:" & oPet.CodProve & " Con:" & oPet.Apellidos & ", " & oPet.nombre & " Descr:" & oError.Arg2)
            End If
            ResulGrabaError.NumError = TESErrorCodigo
            TratarError ResulGrabaError
            bSiguiente = False
        End If
    End If
    
    If bSiguiente Then
        GoTo Siguiente
    Else
        GoTo FinalControlado
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
        
End Sub
Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdDot_Click()
    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(13) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOT.Text = cmmdDot.filename
        
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    'El error 32755 se produce al pulsar el bot�n de Cancelar
    If err.Number <> 0 And err.Number <> 32755 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "cmdDot_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdDOTAdj_Click()
    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(13) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOTAdj.Text = cmmdDot.filename
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    'El error 32755 se produce al pulsar el bot�n de Cancelar
    If err.Number <> 0 And err.Number <> 32755 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "cmdDOTAdj_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdForm_Click()
    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(13) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtForm.Text = cmmdDot.filename
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    'El error 32755 se produce al pulsar el bot�n de Cancelar
    If err.Number <> 0 And err.Number <> 32755 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "cmdForm_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Elegir la plantilla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdMailDot_Click()
    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(13) & " (*.htm)|*.htm"
    cmmdDot.DefaultExt = "htm"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtMailDot.Text = cmmdDot.filename
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    'El error 32755 se produce al pulsar el bot�n de Cancelar
    If err.Number <> 0 And err.Number <> 32755 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "cmdMailDot_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub



''' <summary>
''' Elegir la plantilla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdMailDotAdj_Click()
    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(13) & " (*.htm)|*.htm"
    cmmdDot.DefaultExt = "htm"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtMailDotAdj.Text = cmmdDot.filename

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    'El error 32755 se produce al pulsar el bot�n de Cancelar
    If err.Number <> 0 And err.Number <> 32755 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "cmdMailDotAdj_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADJAnya, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        sCap(1) = Ador(0).Value      '1 Plantilla
        Ador.MoveNext
        sCap(2) = Ador(0).Value      '2 Generando comunicaci�n de cierre...
        Ador.MoveNext
        sCap(3) = Ador(0).Value      '3 Creando comunicaciones...
        Ador.MoveNext
        sCap(4) = Ador(0).Value      '4 Contacto:
        Ador.MoveNext
        sCap(5) = Ador(0).Value      '5 Iniciando sesi�n de correo ...
        Ador.MoveNext
        sCap(6) = Ador(0).Value      '6 Iniciando Microsoft Word...
        Ador.MoveNext
        sCap(7) = Ador(0).Value      '7 A�adiendo plantilla
        Ador.MoveNext
        sCap(8) = Ador(0).Value      '8 Enviando mensaje ...
        Ador.MoveNext
        sCap(9) = Ador(0).Value      '9 Generando orden de compra ...
        Ador.MoveNext
        sCap(10) = Ador(0).Value     '10 Adjuntando especificaci�n:
        Ador.MoveNext
        sCap(11) = Ador(0).Value     '11 Visualizando impreso...
        Ador.MoveNext
        sCap(12) = Ador(0).Value     '12 Cerrando sesi�n de correo...
        Ador.MoveNext
        sCap(13) = Ador(0).Value     '13 Plantillas de documento
        Ador.MoveNext
        sCap(14) = Ador(0).Value     '14 Plantilla para la comunicaci�n de adjudicaci�n.
        Ador.MoveNext
        sCap(15) = Ador(0).Value     '15 Plantilla para la comunicaci�n de NO adjudicaci�n.
        Ador.MoveNext
        sCap(16) = Ador(0).Value     '16 Plantilla mail para la comunicaci�n de adjudicaci�n.
        Ador.MoveNext
        sCap(17) = Ador(0).Value     '17 Plantilla mail para la comunicaci�n de no adjudicaci�n.
        Ador.MoveNext
        sCap(18) = Ador(0).Value     '18 Direcci�n de correo electr�nico
        Ador.MoveNext
        sCap(19) = Ador(0).Value     '19 Plantilla para orden de compra.
        '---------------------------------------
        Ador.MoveNext
        chkEsp.caption = Ador(0).Value      '20
        Ador.MoveNext
        chkFich.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        Frame1.caption = Ador(0).Value
        Ador.MoveNext
        Frame2.caption = Ador(0).Value '25
        Ador.MoveNext
        Frame3.caption = Ador(0).Value
        Ador.MoveNext
        frmADJAnya.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Label6.caption = Ador(0).Value
        Ador.MoveNext
        Label3.caption = Ador(0).Value '30
        Ador.MoveNext
        Label7.caption = Ador(0).Value
        Label8.caption = Ador(0).Value
        Ador.MoveNext
        lblFecPres.caption = Ador(0).Value
        Ador.MoveNext
        sdbddProveCod.Columns(0).caption = Ador(0).Value  '33 C�digo
        sdbddProveDen.Columns(1).caption = Ador(0).Value
        sdbgPet.Groups(0).Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbddProveCod.Columns(1).caption = Ador(0).Value  '34 Denominaci�n
        sdbddProveDen.Columns(0).caption = Ador(0).Value
        sdbgPet.Groups(0).Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbddConApe.Columns(0).caption = Ador(0).Value  '35 Apellidos
        sdbddConNom.Columns(1).caption = Ador(0).Value
        sdbgPet.Groups(1).Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddConApe.Columns(1).caption = Ador(0).Value  '36 Nombre
        sdbddConNom.Columns(0).caption = Ador(0).Value
        sdbgPet.Groups(1).Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbddConApe.Columns(2).caption = Ador(0).Value  '37 Mail
        sdbddConNom.Columns(2).caption = Ador(0).Value
        sdbgPet.Groups(2).Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(0).caption = Ador(0).Value  '38 Proveedores
        Ador.MoveNext
        sdbgPet.Groups(1).caption = Ador(0).Value  '39 Contactos
        Ador.MoveNext
        sdbgPet.Groups(2).caption = Ador(0).Value  '40 Via notificaci�n
        Ador.MoveNext
        sdbgPet.Groups(0).Columns(0).caption = Ador(0).Value  '41 Adjudicado
        Ador.MoveNext
        sdbgPet.Groups(2).Columns(1).caption = Ador(0).Value  '42 Carta
        Ador.MoveNext
        sOrdenCompra = Ador(0).Value
        Ador.MoveNext
        fraContratos.caption = Ador(0).Value
        Ador.MoveNext
        chkContratos.caption = Ador(0).Value
        Ador.MoveNext
        sNotifAdj = Ador(0).Value
        
        Ador.MoveNext
        sCap(20) = Ador(0).Value  '47 Generando notificaci�n de adjudicaci�n�
        Ador.MoveNext
        sCap(21) = Ador(0).Value  '48 Almacenando notificaci�n de adjudicaci�n en base de datos
        Ador.MoveNext
        m_sOrden = Ador(0).Value
        Ador.MoveNext
        m_sNotif = Ador(0).Value
        Ador.MoveNext
        m_sSi = Ador(0).Value 'S�
        Ador.MoveNext
        m_sNo = Ador(0).Value 'No
        Ador.MoveNext
        m_sTotalItem = Ador(0).Value
        Ador.MoveNext
        m_sItem = Ador(0).Value
        Ador.MoveNext
        m_sTotalGrupo = Ador(0).Value
        Ador.MoveNext
        m_sTotalProce = Ador(0).Value
        Ador.MoveNext
        m_sOperacion = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
           
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
Dim oEmpresas As CEmpresas
Dim sCarpeta As String
Dim sMarcaPlantillas As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    m_bDescargarFrm = False
    Me.Width = 10560
    Me.Height = 6660
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    CargarRecursos
    
    PonerFieldSeparator Me
    
    'Inicializar el array de ficheros
    ReDim sayFileNames(0)
    ReDim Nombres(0)
    Set oFos = New Scripting.FileSystemObject
    
    Set oProve = oFSGSRaiz.generar_CProveedor

    If gParametrosGenerales.giMail = 0 Then
        txtMailDotAdj.Enabled = False
        txtMailDot.Enabled = False
        cmdMailDot.Enabled = False
        cmdMailDotAdj.Enabled = False
    End If
    
    sdbgPet.Columns(1).DropDownHwnd = sdbddProveCod.hWnd
    sdbgPet.Columns(2).DropDownHwnd = sdbddProveDen.hWnd
    sdbgPet.Columns(3).DropDownHwnd = sdbddConApe.hWnd
    sdbgPet.Columns(4).DropDownHwnd = sdbddConNom.hWnd
    
    sdbddConApe.AddItem ""
    sdbddConNom.AddItem ""
    sdbddProveCod.AddItem ""
    sdbddProveDen.AddItem ""
    
    Set oIasig = frmOFEPet.oProcesoSeleccionado
    
    Set oProvesAsignados = oFSGSRaiz.generar_CProveedores
    If frmOFEPet.bComunAsigComp Then
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    ElseIf frmOFEPet.bComunAsigEqp Then
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario)
    Else
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve)
    End If
    
    If gParametrosGenerales.gbProveGrupos Then
        Set m_oAsigs = oIasig.DevolverAsignaciones(OrdAsigPorCodProve, , , , , True)
    End If
    
    If frmOFEPet.oProcesoSeleccionado.PermitirAdjDirecta Then
        lblFecPres.Visible = False
        lblFecPresBox.Visible = False
    End If
    
    ' PLANTILLAS PARA CARTAS
    
    If gParametrosInstalacion.gsAdjCarta = "" Then
        If gParametrosGenerales.gsAdjCartaDot = "" Then
            oMensajes.NoValido sCap(14)
        Else
            gParametrosInstalacion.gsAdjCarta = gParametrosGenerales.gsAdjCartaDot
            g_GuardarParametrosIns = True
        End If
    End If
    If gParametrosInstalacion.gsAdjNoCarta = "" Then
        If gParametrosGenerales.gsAdjNoCartaDot = "" Then
            oMensajes.NoValido sCap(15)
        Else
            gParametrosInstalacion.gsAdjNoCarta = gParametrosGenerales.gsAdjNoCartaDot
            g_GuardarParametrosIns = True
        End If
    End If
    
    ' PLANTILLAS PARA MAIL
    If gParametrosGenerales.giMail <> 0 Then
        If gParametrosInstalacion.gsAdjMail = "" Then
            If gParametrosGenerales.gsAdjMailDot = "" Then
                oMensajes.NoValido sCap(16)
            Else
                gParametrosInstalacion.gsAdjMail = gParametrosGenerales.gsAdjMailDot
                g_GuardarParametrosIns = True
            End If
        End If
        If gParametrosInstalacion.gsAdjNoMail = "" Then
            If gParametrosGenerales.gsAdjNoMailDot = "" Then
                oMensajes.NoValido sCap(17)
            Else
                gParametrosInstalacion.gsAdjNoMail = gParametrosGenerales.gsAdjNoMailDot
                g_GuardarParametrosIns = True
            End If
        End If
    End If
    If InStr(gParametrosInstalacion.gsAdjNoCarta, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsAdjNoMail, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsAdjCarta, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsAdjMail, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        sCarpeta = oEmpresas.DevolverCarpetaPlantillaProce_o_Ped(frmOFEPet.sdbcAnyo.Text, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.sdbcProceCod.Text, 0)
    End If
    If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    If gParametrosInstalacion.gbAdjOrden Then
        If gParametrosInstalacion.gsAdj = "" Then
            If gParametrosGenerales.gsadjDot = "" Then
                oMensajes.NoValido sCap(19)
            Else
                gParametrosInstalacion.gsAdj = gParametrosGenerales.gsadjDot
                g_GuardarParametrosIns = True
            End If
        End If
        lblForm.caption = m_sOrden
        txtForm.Text = Replace(gParametrosInstalacion.gsAdj, sMarcaPlantillas, sCarpeta)
    ElseIf gParametrosInstalacion.gbAdjNotif Then
        If gParametrosInstalacion.gsAdjNotif = "" Then
            If gParametrosGenerales.gsADJNOTIFDOT = "" Then
                oMensajes.NoValido sCap(14)
            Else
                gParametrosInstalacion.gsAdjNotif = gParametrosGenerales.gsADJNOTIFDOT
                g_GuardarParametrosIns = True
            End If
        End If
        lblForm.caption = m_sNotif
        txtForm.Text = Replace(gParametrosInstalacion.gsAdjNotif, sMarcaPlantillas, sCarpeta)
    Else
        lblForm.Visible = False
        txtForm.Visible = False
        cmdForm.Visible = False
        Me.Height = 6405
        Frame1.Height = 1185
        Frame2.Top = 1920
        sdbgPet.Top = 3225
        Frame3.Height = 1185
        chkEsp.Top = 255
        chkFich.Top = 655
        fraContratos.Top = 1920
        Shape1.Height = 3135
    End If
    
    txtDOT.Text = Replace(gParametrosInstalacion.gsAdjNoCarta, sMarcaPlantillas, sCarpeta)
    txtMailDot.Text = Replace(gParametrosInstalacion.gsAdjNoMail, sMarcaPlantillas, sCarpeta)
    txtDOTAdj.Text = Replace(gParametrosInstalacion.gsAdjCarta, sMarcaPlantillas, sCarpeta)
    txtMailDotAdj.Text = Replace(gParametrosInstalacion.gsAdjMail, sMarcaPlantillas, sCarpeta)
        
    sFormatoNumber = "#,##0.######"
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Resize()

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Form_Unload(Cancel As Integer)

Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm:
    
    'Borramos los archivos temporales que hayamos creado
    Set oProvesAsignados = Nothing
    g_bCancelarMail = False
    
    i = 0
    While i < UBound(sayFileNames)
        
        If oFos.FileExists(sayFileNames(i)) Then
            oFos.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    Set oFos = Nothing
    
    Me.Visible = False
    Exit Sub

ERROR_Frm:
    basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' CLose up del combo del apellido de contacto
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddConApe_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbddConApe.Columns("ID").Value = sdbgPet.Columns("CODCON").Value Then
        Exit Sub
    End If
    
    If sdbddConApe.Columns(0).Value <> "" Then
        
        sdbgPet.Columns(3).Value = sdbddConApe.Columns(0).Value
        sdbgPet.Columns(4).Value = sdbddConApe.Columns(1).Value
        sdbgPet.Columns(7).Value = sdbddConApe.Columns(2).Value
        sdbgPet.Columns(8).Value = sdbddConApe.Columns(3).Value
        sdbgPet.Columns("TFNO").Value = sdbddConApe.Columns("TFNO").Value
        sdbgPet.Columns("TFNO2").Value = sdbddConApe.Columns("TFNO2").Value
        sdbgPet.Columns("FAX").Value = sdbddConApe.Columns("FAX").Value
        sdbgPet.Columns("TFNO_MOVIL").Value = sdbddConApe.Columns("TFNO_MOVIL").Value
        sdbgPet.Columns("PORTALCON").Value = sdbddConApe.Columns("PORT").Value
        sdbgPet.Columns("TIPOEMAIL").Value = sdbddConApe.Columns("TIPOEMAIL").Value
        
        If sdbddConApe.Columns(2).Value <> "" And gParametrosGenerales.giMail <> 0 Then
        ' contacto con e-mail
            sdbgPet.Columns(5).Value = 1
            sdbgPet.Columns(6).Value = 0
        Else
            sdbgPet.Columns(5).Value = 0
            sdbgPet.Columns(6).Value = 1
        End If
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddConApe_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbddConApe_DropDown()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgPet.Columns(1).Value = "" Then Exit Sub
    Screen.MousePointer = vbHourglass
    
    oProve.Cod = sdbgPet.Columns(1).Value
    
    If bCargarComboDesdeDD Then
        Set oContactos = oProve.CargarTodosLosContactos(sdbgPet.ActiveCell.Value, , , , True)
    Else
        Set oContactos = oProve.CargarTodosLosContactos(, , , , True)
    End If
    
        
    CargarGridConContactosApe
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddConApe_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddConApe_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddConApe.DataFieldList = "Column 0"
    sdbddConApe.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddConApe_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub CargarGridConProveCod()

    ''' * Objetivo: Cargar combo con la coleccion de proveedores
    
    Dim oProve As CProveedor
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddProveCod.RemoveAll
    
    For Each oProve In oProves
        sdbddProveCod.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den
    Next
    
    If sdbddProveCod.Rows = 0 Then
        sdbddProveCod.AddItem ""
    End If
    
    sdbddProveCod.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "CargarGridConProveCod", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub
Private Sub CargarGridConProveDen()

    ''' * Objetivo: Cargar combo con la coleccion de proveedores
    
    Dim oProve As CProveedor
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddProveDen.RemoveAll
    
    For Each oProve In oProves
        sdbddProveDen.AddItem oProve.Den & Chr(m_lSeparador) & oProve.Cod
    Next
    
    If sdbddProveDen.Rows = 0 Then
        sdbddProveDen.AddItem ""
    End If
    
    sdbddProveDen.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "CargarGridConProveDen", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' * Objetivo: Cargar combo con la coleccion de Contactos
''' </summary>
''' <remarks>Llamada desde: sdbddConApe_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConContactosApe()

    Dim oCon As CContacto
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddConApe.RemoveAll
    
    For Each oCon In oContactos
        sdbddConApe.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Tfno2 & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & IIf(oCon.Port, "1", "0") & Chr(m_lSeparador) & oCon.TipoEmail
    Next
    
    If sdbddConApe.Rows = 0 Then
        sdbddConApe.AddItem ""
    End If
    
    sdbddConApe.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "CargarGridConContactosApe", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub


Private Sub sdbddConApe_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbddConApe.Columns("PORT").Value = "1" Then
        For i = 0 To sdbddConApe.Cols - 1
            sdbddConApe.Columns(i).CellStyleSet "Tan"
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddConApe_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' CLose up del combo del nombre de contacto
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddConNom_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbddConNom.Columns(1).Value <> "" Then
        sdbgPet.Columns(4).Value = sdbddConNom.Columns(0).Value
        sdbgPet.Columns(3).Value = sdbddConNom.Columns(1).Value
        sdbgPet.Columns(7).Value = sdbddConNom.Columns(2).Value
        sdbgPet.Columns(8).Value = sdbddConNom.Columns(3).Value
        sdbgPet.Columns("TFNO").Value = sdbddConNom.Columns("TFNO").Value
        sdbgPet.Columns("TFNO2").Value = sdbddConNom.Columns("TFNO2").Value
        sdbgPet.Columns("FAX").Value = sdbddConNom.Columns("FAX").Value
        sdbgPet.Columns("TFNO_MOVIL").Value = sdbddConNom.Columns("TFNO_MOVIL").Value
        sdbgPet.Columns("PORTALCON").Value = sdbddConNom.Columns("PORT").Value
        sdbgPet.Columns("TIPOEMAIL").Value = sdbddConNom.Columns("TIPOEMAIL").Value
        
        If sdbddConNom.Columns(2).Value <> "" And gParametrosGenerales.giMail <> 0 Then
        ' contacto con e-mail
            sdbgPet.Columns(5).Value = 1
            sdbgPet.Columns(6).Value = 0
        Else
            sdbgPet.Columns(5).Value = 0
            sdbgPet.Columns(6).Value = 1
        End If
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddConNom_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbddConNom_DropDown()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgPet.Columns(1).Value = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    oProve.Cod = sdbgPet.Columns(1).Value
    
    If bCargarComboDesdeDD Then
        Set oContactos = oProve.CargarTodosLosContactos(, sdbgPet.ActiveCell.Value, , True)
    Else
        Set oContactos = oProve.CargarTodosLosContactos(, , , True)
    End If
    
        
    CargarGridConContactosNom
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddConNom_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddConNom_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddConNom.DataFieldList = "Column 0"
    sdbddConNom.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddConNom_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' * Objetivo: Cargar combo con la coleccion de Contactos
''' </summary>
''' <remarks>Llamada desde: sdbddConNom_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConContactosNom()
    
    Dim oCon As CContacto
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddConNom.RemoveAll
    
    For Each oCon In oContactos
        sdbddConNom.AddItem oCon.nombre & Chr(m_lSeparador) & oCon.Apellidos & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Tfno2 & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & IIf(oCon.Port, "1", "0") & Chr(m_lSeparador) & oCon.TipoEmail
    Next
    
    If sdbddConNom.Rows = 0 Then
        sdbddConNom.AddItem ""
    End If
    
    sdbddConNom.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "CargarGridConContactosNom", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub


Private Sub sdbddConNom_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbddConNom.Columns("PORT").Value = "1" Then
        For i = 0 To sdbddConNom.Cols - 1
            sdbddConNom.Columns(i).CellStyleSet "Tan"
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddConNom_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddProveCod_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbddProveCod.Columns(0).Value <> "" Then
        sdbgPet.Columns(2).Value = sdbddProveCod.Columns(1).Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbddProveCod_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oProvesAsignados Is Nothing Then Exit Sub
    If oProvesAsignados.Item(sdbddProveCod.Columns("COD").Value) Is Nothing Then Exit Sub
    If NullToStr(oProvesAsignados.Item(sdbddProveCod.Columns("COD").Value).CodPortal) <> "" And _
      gParametrosGenerales.giINSTWEB = ConPortal Then
        sdbddProveCod.Columns("COD").CellStyleSet "ProvPortal"
        sdbddProveCod.Columns("DEN").CellStyleSet "ProvPortal"
    Else
        sdbddProveCod.Columns("COD").CellStyleSet "Normal"
        sdbddProveCod.Columns("DEN").CellStyleSet "Normal"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveCod_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddProveDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbddProveDen.Columns(0).Value <> "" Then
        sdbgPet.Columns(1).Value = sdbddProveDen.Columns(1).Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbddProveDen_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddProveDen.DataFieldList = "Column 0"
    sdbddProveDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddProveDen_RowLoaded(ByVal Bookmark As Variant)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oProvesAsignados Is Nothing Then Exit Sub
    
    If oProvesAsignados.Item(sdbddProveDen.Columns("COD").Value) Is Nothing Then Exit Sub
    
    If NullToStr(oProvesAsignados.Item(sdbddProveDen.Columns("COD").Value).CodPortal) <> "" And _
      gParametrosGenerales.giINSTWEB = ConPortal Then
        sdbddProveDen.Columns("COD").CellStyleSet "ProvPortal"
        sdbddProveDen.Columns("DEN").CellStyleSet "ProvPortal"
    Else
        sdbddProveDen.Columns("COD").CellStyleSet "Normal"
        sdbddProveDen.Columns("DEN").CellStyleSet "Normal"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveDen_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbgPet_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DispPromptMsg = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbgPet_BeforeDelete", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgPet_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case sdbgPet.col
    Case 1, 2
        
        sdbgPet.Columns(3).Value = ""
        sdbgPet.Columns(4).Value = ""
        sdbgPet.Columns(5).Value = 0
        sdbgPet.Columns(6).Value = 0
        sdbgPet.Columns(7).Value = ""
        sdbgPet.Columns(8).Value = ""
        sdbgPet.Columns(9).Value = ""
        ' marta las a�ado
        sdbgPet.Columns(10).Value = ""
        sdbgPet.Columns(11).Value = ""
        sdbgPet.Columns(0).Value = 0
        sdbgPet.Columns("PORTALCON").Value = ""
        
    Case 5
        
        If sdbgPet.Columns(5).Value = "1" Or sdbgPet.Columns(5).Value = "-1" Then
            If sdbgPet.Columns("EMAIL").Value = "" Then
                oMensajes.NoValido sCap(18)
                sdbgPet.DataChanged = False
                Exit Sub
            Else
                sdbgPet.Columns(6).Value = 0
            End If
        End If
    
        
    Case 6
        
        If sdbgPet.Columns(6).Value = "1" Or sdbgPet.Columns(6).Value = "-1" Then
            sdbgPet.Columns(5).Value = 0
        End If
        
        
    End Select

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbgPet_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbgPet_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgPet.Columns(3).Locked = True
    sdbgPet.Columns(4).Locked = True
    
   
    If gParametrosGenerales.giMail = 0 Then
        sdbgPet.Columns("MAIL").Locked = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbgPet_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub
Private Sub sdbddProveCod_DropDown()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    If frmOFEPet.bComunAsigComp Then
    
        If bCargarComboDesdeDD Then
            Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        End If
    Else
    
        If frmOFEPet.bComunAsigEqp Then
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario)
            End If
        Else
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "")
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve)
            End If
        End If
    
    End If
        
    CargarGridConProveCod
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddProveCod_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddProveCod.DataFieldList = "Column 0"
    sdbddProveCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub
Private Sub sdbddProveCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbddProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveCod.Rows - 1
            bm = sdbddProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveCod_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub
Private Sub sdbddProveCod_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String
Dim oProves As CProveedores
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Text = "" Then
        RtnPassed = True
        
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddProveCod.Columns(0).Value = Text Then
            RtnPassed = True
            sCod = sdbgPet.Columns(1).Value
            sdbgPet.Columns(1).Value = sCod
            If frmOFEPet.oProvesAdj.Item(sCod) Is Nothing Then
                sdbgPet.Columns(0).Value = 0
            Else
                sdbgPet.Columns(0).Value = 1
            End If
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        
        If frmOFEPet.bComunAsigComp Then
            Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If frmOFEPet.bComunAsigEqp Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True)
            End If
        End If
        
        If Not oProves.Item(1) Is Nothing Then
            RtnPassed = True
            sCod = oProves.Item(1).Cod
            sdbgPet.Columns(1).Value = sCod
            sdbgPet.Columns(2).Value = oProves.Item(1).Den
            ' actualizar check box adjudicado de la grid
            If frmOFEPet.oProvesAdj.Item(sCod) Is Nothing Then
                sdbgPet.Columns(0).Value = 0
            Else
                sdbgPet.Columns(0).Value = 1
            End If
        Else
            sdbgPet.Columns(0).Value = 0
            sdbgPet.Columns(1).Value = ""
            sdbgPet.Columns(2).Value = ""
        End If
    
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveCod_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddProveDen_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    If frmOFEPet.bComunAsigComp Then
    
        If bCargarComboDesdeDD Then
            Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorDenProve, gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        End If
    Else
    
        If frmOFEPet.bComunAsigEqp Then
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorDenProve, gCodEqpUsuario)
            End If
        Else
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", "", OrdAsigPorDenProve)
            End If
        End If
    
    End If
        
    CargarGridConProveDen
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddProveDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbddProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveDen.Rows - 1
            bm = sdbddProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveDen_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub
Private Sub sdbddProveDen_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String
Dim oProves As CProveedores
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Text = "" Then
        RtnPassed = True
        
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddProveDen.Columns(0).Value = Text Then
            RtnPassed = True
            sCod = sdbddProveDen.Columns(1).Value
            sdbgPet.Columns(1).Value = sCod
            If frmOFEPet.oProvesAdj.Item(sCod) Is Nothing Then
                sdbgPet.Columns(0).Value = 0
            Else
                sdbgPet.Columns(0).Value = 1
            End If
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        
        If frmOFEPet.bComunAsigComp Then
            Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If frmOFEPet.bComunAsigEqp Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve)
            End If
        End If
        
        If Not oProves.Item(1) Is Nothing Then
            RtnPassed = True
            sCod = oProves.Item(1).Cod
            sdbgPet.Columns(1).Value = sCod
            ' actualizar check box adjudicado de la grid
            If frmOFEPet.oProvesAdj.Item(sCod) Is Nothing Then
                sdbgPet.Columns(0).Value = 0
            Else
                sdbgPet.Columns(0).Value = 1
            End If
            sdbgPet.Columns(2).Value = oProves.Item(1).Den
        Else
            sdbgPet.Columns(0).Value = 0
            sdbgPet.Columns(1).Value = ""
            sdbgPet.Columns(2).Value = ""
        End If
    
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbddProveDen_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    If Me.Height - Shape1.Height - picEdit.Height - 500 > 10 Then sdbgPet.Height = Me.Height - Shape1.Height - picEdit.Height - 500
    If Width >= 360 Then sdbgPet.Width = Width - 360
    sdbgPet.Groups(0).Width = sdbgPet.Width * 0.45
    sdbgPet.Groups(1).Width = sdbgPet.Width * 0.35
    sdbgPet.Groups(2).Width = sdbgPet.Width * 0.15
    
    cmdAceptar.Left = Me.Width / 2 - 1400
    cmdCancelar.Left = cmdAceptar.Left + 1200
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>Sacar un Word con la orden de compra</summary>
''' <param name="oPet">Objeto pedido</param>
''' <param name="appword">Objeto word</param>
''' <param name="bOrdenCompra">Adjuntar Orden de compra � notif de adjudicaci�n</param>
''' <returns>Word con la orden de compra</returns>
''' <remarks>Llamada desde: cmdAceptar_click; Tiempo m�ximo: 0,2</remarks>
''' <revision>LTG 02/12/2011</revision>

Private Function OrdenCompra(ByVal oPet As CPetOferta, ByVal bOrdenCompra As Boolean, ByVal oEquipo As CEquipo) As Object
    Dim splantilla As String
    Dim oProce As CProceso
    Dim oOferta As COferta
    Dim oProves As CProveedores
    Dim oOrdenCompra As COrdenCompra
    Set oOrdenCompra = New COrdenCompra
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
    
    If txtForm.Text = "" Then
        oMensajes.NoValido sCap(19)
        Set OrdenCompra = Nothing
        Exit Function
    End If
    splantilla = txtForm.Text
    
    If Right(splantilla, 3) <> "dot" Then
        oMensajes.NoValido sCap(1) & ": " & splantilla
        Set OrdenCompra = Nothing
        Exit Function
    End If
        
    If Not oFos.FileExists(splantilla) Then
        oMensajes.PlantillaNoEncontrada splantilla
        Set OrdenCompra = Nothing
        Exit Function
    End If
    
    Dim sNumProceso As String
    sNumProceso = frmOFEPet.sdbcAnyo.Text & "/" & frmOFEPet.sdbcGMN1_4Cod.Text & "/" & frmOFEPet.sdbcProceCod.Text
    Dim sdbcProceDen As String
    sdbcProceDen = frmOFEPet.sdbcProceDen.Text & " "
    
    Set oProce = frmOFEPet.oProcesoSeleccionado
            
    ' CARGAR DATOS GENERALES DE PROVEEDOR
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.Add oPet.CodProve, oPet.DenProve
    oProves.CargarDatosProveedor oPet.CodProve
    Dim scodProve As String
    scodProve = oPet.CodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oPet.CodProve))
        
    ' DATOS PARA COMPRADOR RESPONSABLE
    
    ' DATOS DE LA ULTIMA OFERTA
    Set oOferta = oProce.DevolverUltimaOfertaProveedor(oPet.CodProve, False, True, , basPublic.gParametrosInstalacion.gIdioma)
    
    Set OrdenCompra = oOrdenCompra.GenerarOrdenCompra(appword, sNumProceso, sdbcProceDen, frmOFEPet.oDestinos, frmOFEPet.oPagos, frmOFEPet.oUnidades, oPet, scodProve, m_oAsigs, bOrdenCompra, oEquipo, _
                        splantilla, oProce, oProves, oOferta, m_iWordVer, LblFecCierreBox, oUsuarioSummit.TimeZone, basPublic.gParametrosInstalacion.gIdioma, chkFich.Value, _
                        m_sSi, m_sNo, m_sOperacion, m_sTotalProce, sFormatoNumber, m_sTotalItem, m_sItem, m_sTotalGrupo, chkEsp.Value, m_bActivado, gParametrosGenerales, oFSGSRaiz)
    
    Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "OrdenCompra", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function



''' <summary>
''' Crear el word para un no adjudicado
''' </summary>
''' <param name="appword">objeto word con el q trabajamos</param>
''' <param name="splantilla">plantilla de la no adjudicaci�n</param>
''' <param name="oPet">prove no adjudicado</param>
''' <param name="oEquipo">Equipo del no adjudicado</param>
''' <returns>fichero generado</returns>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Function CartaNoAdjudicado(ByVal splantilla As String, ByVal oPet As CPetOferta, oEquipo As CEquipo) As Object
    Dim oProves As CProveedores
    ' personas asociadas
    Dim IPersonas As IPersonasAsig
    Dim oPersonas As CPersonas
    Dim oPer As CPersona
    Dim blankword As Object
    Dim rangeword As Object
    Dim docword As Object
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.Add oPet.CodProve, oPet.DenProve
    oProves.CargarDatosProveedor oPet.CodProve
    
    Set docword = appword.Documents.Add(splantilla)
    Set blankword = appword.Documents.Add(splantilla)

    If m_iWordVer >= 9 Then
        If appword.Visible Then docword.activewindow.WindowState = 2
        If appword.Visible Then blankword.activewindow.WindowState = 2
    Else
        appword.Visible = True
        appword.Windows(docword.Name).WindowState = 1
        appword.Windows(blankword.Name).WindowState = 1
    End If

    With docword
    ' DATOS GENERALES PROCESO
        If .Bookmarks.Exists("NUM_PROCESO") Then
            DatoAWord docword, "NUM_PROCESO", frmOFEPet.sdbcAnyo.Text & "/" & frmOFEPet.sdbcGMN1_4Cod.Text & "/" & frmOFEPet.sdbcProceCod.Text
        End If
        If .Bookmarks.Exists("DEN_PROCESO") Then
            DatoAWord docword, "DEN_PROCESO", frmOFEPet.sdbcProceDen.Text & " "
        End If
        If .Bookmarks.Exists("MAT1_PROCESO") Then
            DatoAWord docword, "MAT1_PROCESO", frmOFEPet.sdbcGMN1_4Cod.Text
        End If
        If .Bookmarks.Exists("FECHA_CIERRE") Then
            DatoAWord docword, "FECHA_CIERRE", LblFecCierreBox
        End If
        If .Bookmarks.Exists("SOLICITUD") Then
            DatoAWord docword, "SOLICITUD", NullToStr(frmOFEPet.oProcesoSeleccionado.Referencia)
        End If
        ' DATOS PROVEEDORES
        If .Bookmarks.Exists("COD_PROVE") Then
            DatoAWord docword, "COD_PROVE", oPet.CodProve
        End If
        If .Bookmarks.Exists("DEN_PROVE") Then
            DatoAWord docword, "DEN_PROVE", oPet.DenProve
        End If
        If .Bookmarks.Exists("DIR_PROVE") Then
            DatoAWord docword, "DIR_PROVE", NullToStr(oProves.Item(1).Direccion)
        End If
        If .Bookmarks.Exists("POBL_PROVE") Then
            DatoAWord docword, "POBL_PROVE", NullToStr(oProves.Item(1).Poblacion)
        End If
        If .Bookmarks.Exists("CP_PROVE") Then
            DatoAWord docword, "CP_PROVE", NullToStr(oProves.Item(1).cP)
        End If
        If .Bookmarks.Exists("PROV_PROVE") Then
            DatoAWord docword, "PROV_PROVE", NullToStr(oProves.Item(1).DenProvi)
        End If
        If .Bookmarks.Exists("PAIS_PROVE") Then
            DatoAWord docword, "PAIS_PROVE", NullToStr(oProves.Item(1).DenPais)
        End If
        If .Bookmarks.Exists("NIF_PROVE") Then
            DatoAWord docword, "NIF_PROVE", NullToStr(oProves.Item(1).NIF)
        End If
        ' DATOS PERSONA CONTACTO
        If .Bookmarks.Exists("NOM_CONTACTO") Then
            DatoAWord docword, "NOM_CONTACTO", NullToStr(oPet.nombre)
        End If
        If .Bookmarks.Exists("APE_CONTACTO") Then
            DatoAWord docword, "APE_CONTACTO", NullToStr(oPet.Apellidos)
        End If
        If .Bookmarks.Exists("TFNO_CONTACTO") Then
            DatoAWord docword, "TFNO_CONTACTO", NullToStr(oPet.Tfno)
        End If
        If .Bookmarks.Exists("FAX_CONTACTO") Then
            DatoAWord docword, "FAX_CONTACTO", NullToStr(oPet.Fax)
        End If
        If .Bookmarks.Exists("MAIL_CONTACTO") Then
            DatoAWord docword, "MAIL_CONTACTO", NullToStr(oPet.Email)
        End If
        If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
            DatoAWord docword, "TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil)
        End If
    
        ' DATOS COMPRADOR
        If .Bookmarks.Exists("APE_COMPRADOR") Then
            DatoAWord docword, "APE_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Apel)
        End If
        If .Bookmarks.Exists("NOM_COMPRADOR") Then
            DatoAWord docword, "NOM_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).nombre)
        End If
        If .Bookmarks.Exists("TFNO_COMPRADOR") Then
            DatoAWord docword, "TFNO_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Tfno)
        End If
        If .Bookmarks.Exists("FAX_COMPRADOR") Then
            DatoAWord docword, "FAX_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Fax)
        End If
        If .Bookmarks.Exists("MAIL_COMPRADOR") Then
            DatoAWord docword, "MAIL_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).mail)
        End If
        If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
            DatoAWord docword, "TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil)
        End If
        ' PERSONAS ASOCIADAS
        If .Bookmarks.Exists("IMPLICADOS") Then
            Set IPersonas = frmOFEPet.oProcesoSeleccionado
            Set oPersonas = IPersonas.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorApe, True)
            If oPersonas.Count = 0 Then
                .Bookmarks("IMPLICADOS").Range.Delete
            ElseIf .Bookmarks.Exists("IMPLICADO") Then
                .Bookmarks("IMPLICADO").Select
                Set rangeword = blankword.Bookmarks("IMPLICADO").Range
                rangeword.Copy
                .Application.Selection.Delete
                For Each oPer In oPersonas
                    .Application.Selection.Paste
                    If .Bookmarks.Exists("COD_IMPLICADO") Then
                        .Bookmarks("COD_IMPLICADO").Range.Text = NullToStr(oPer.Cod)
                    End If
                    If .Bookmarks.Exists("NOM_IMPLICADO") Then
                        .Bookmarks("NOM_IMPLICADO").Range.Text = NullToStr(oPer.nombre)
                    End If
                    If .Bookmarks.Exists("APE_IMPLICADO") Then
                        .Bookmarks("APE_IMPLICADO").Range.Text = NullToStr(oPer.Apellidos)
                    End If
                    If .Bookmarks.Exists("ROL_IMPLICADO") Then
                        .Bookmarks("ROL_IMPLICADO").Range.Text = NullToStr(oPer.Rol)
                    End If
                    If .Bookmarks.Exists("TLFNO_IMPLICADO") Then
                        .Bookmarks("TLFNO_IMPLICADO").Range.Text = NullToStr(oPer.Tfno)
                    End If
                    If .Bookmarks.Exists("FAX_IMPLICADO") Then
                        .Bookmarks("FAX_IMPLICADO").Range.Text = NullToStr(oPer.Fax)
                    End If
                    If .Bookmarks.Exists("MAIL_IMPLICADO") Then
                        .Bookmarks("MAIL_IMPLICADO").Range.Text = NullToStr(oPer.mail)
                    End If
                    If .Bookmarks.Exists("UO1_IMPLICADO") Then
                        .Bookmarks("UO1_IMPLICADO").Range.Text = NullToStr(oPer.UON1)
                    End If
                    If .Bookmarks.Exists("UO2_IMPLICADO") Then
                        .Bookmarks("UO2_IMPLICADO").Range.Text = NullToStr(oPer.UON2)
                    End If
                    If .Bookmarks.Exists("UO3_IMPLICADO") Then
                        .Bookmarks("UO3_IMPLICADO").Range.Text = NullToStr(oPer.UON3)
                    End If
                    If .Bookmarks.Exists("DPTO_IMPLICADO") Then
                        .Bookmarks("DPTO_IMPLICADO").Range.Text = NullToStr(oPer.CodDep)
                    End If
                    If .Bookmarks.Exists("CARGO_IMPLICADO") Then
                        .Bookmarks("CARGO_IMPLICADO").Range.Text = NullToStr(oPer.Cargo)
                    End If
                Next
            End If
        End If

    End With
    
    Set oProves = Nothing
    Set IPersonas = Nothing
    Set oPersonas = Nothing
    Set oPer = Nothing
    
    Set CartaNoAdjudicado = docword
      
    docword.Close
    Set docword = Nothing
    blankword.Close 'False
    Set blankword = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "CartaNoAdjudicado", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

''' <summary>
''' Crear el word para un adjudicado
''' </summary>
''' <param name="appword">objeto word con el q trabajamos</param>
''' <param name="splantilla">plantilla de la adjudicaci�n</param>
''' <param name="oPet">prove adjudicado</param>
''' <param name="oEquipo">Equipo del adjudicado</param>
''' <returns>fichero generado</returns>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Function CartaAdjOrden(ByVal splantilla As String, ByVal oPet As CPetOferta, oEquipo As CEquipo, ByVal oOferta As COferta) As Object
Dim oProves As CProveedores
' personas asociadas
Dim IPersonas As IPersonasAsig
Dim oPersonas As CPersonas
Dim oPer As CPersona
Dim blankword As Object
Dim rangeword As Object
Dim docword As Object
Dim dtFechaOferta As Date
Dim dtHoraOferta As Date
Dim tzInfo As TIME_ZONE_INFO

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oProves = oFSGSRaiz.generar_CProveedores

oProves.Add oPet.CodProve, oPet.DenProve
oProves.CargarDatosProveedor oPet.CodProve
' DATOS DE LA ULTIMA OFERTA

Set docword = appword.Documents.Add(splantilla)
Set blankword = appword.Documents.Add(splantilla)
'appword.Visible = True

If m_iWordVer >= 9 Then
    If appword.Visible Then docword.activewindow.WindowState = 2
    If appword.Visible Then blankword.activewindow.WindowState = 2
Else
    appword.Visible = True
    appword.Windows(docword.Name).WindowState = 1
    appword.Windows(blankword.Name).WindowState = 1
End If

With docword
' DATOS GENERALES PROCESO
    If .Bookmarks.Exists("NUM_PROCESO") Then
        DatoAWord docword, "NUM_PROCESO", frmOFEPet.sdbcAnyo.Text & "/" & frmOFEPet.sdbcGMN1_4Cod.Text & "/" & frmOFEPet.sdbcProceCod.Text
    End If
    If .Bookmarks.Exists("DEN_PROCESO") Then
        DatoAWord docword, "DEN_PROCESO", frmOFEPet.sdbcProceDen.Text & " "
    End If
    If .Bookmarks.Exists("MAT1_PROCESO") Then
        DatoAWord docword, "MAT1_PROCESO", frmOFEPet.sdbcGMN1_4Cod.Text
    End If
    If .Bookmarks.Exists("FECHA_CIERRE") Then
        DatoAWord docword, "FECHA_CIERRE", LblFecCierreBox
    End If
    If .Bookmarks.Exists("SOLICITUD") Then
        DatoAWord docword, "SOLICITUD", NullToStr(frmOFEPet.oProcesoSeleccionado.Referencia)
    End If
    ' DATOS PROVEEDORES
    If .Bookmarks.Exists("COD_PROVE") Then
        DatoAWord docword, "COD_PROVE", oPet.CodProve
    End If
    If .Bookmarks.Exists("DEN_PROVE") Then
        DatoAWord docword, "DEN_PROVE", oPet.DenProve
    End If
    If .Bookmarks.Exists("DIR_PROVE") Then
        DatoAWord docword, "DIR_PROVE", NullToStr(oProves.Item(1).Direccion)
    End If
    If .Bookmarks.Exists("POBL_PROVE") Then
        DatoAWord docword, "POBL_PROVE", NullToStr(oProves.Item(1).Poblacion)
    End If
    If .Bookmarks.Exists("CP_PROVE") Then
        DatoAWord docword, "CP_PROVE", NullToStr(oProves.Item(1).cP)
    End If
    If .Bookmarks.Exists("PROV_PROVE") Then
        DatoAWord docword, "PROV_PROVE", NullToStr(oProves.Item(1).DenProvi)
    End If
    If .Bookmarks.Exists("PAIS_PROVE") Then
        DatoAWord docword, "PAIS_PROVE", NullToStr(oProves.Item(1).DenPais)
    End If
    If .Bookmarks.Exists("NIF_PROVE") Then
        DatoAWord docword, "NIF_PROVE", NullToStr(oProves.Item(1).NIF)
    End If
    ' DATOS PERSONA CONTACTO
    If .Bookmarks.Exists("NOM_CONTACTO") Then
        DatoAWord docword, "NOM_CONTACTO", oPet.nombre
    End If
    If .Bookmarks.Exists("APE_CONTACTO") Then
        DatoAWord docword, "APE_CONTACTO", oPet.Apellidos
    End If
    If .Bookmarks.Exists("TFNO_CONTACTO") Then
        DatoAWord docword, "TFNO_CONTACTO", NullToStr(oPet.Tfno)
    End If
    If .Bookmarks.Exists("FAX_CONTACTO") Then
        DatoAWord docword, "FAX_CONTACTO", NullToStr(oPet.Fax)
    End If
    If .Bookmarks.Exists("MAIL_CONTACTO") Then
        DatoAWord docword, "MAIL_CONTACTO", NullToStr(oPet.Email)
    End If
    If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
        DatoAWord docword, "TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil)
    End If
    
    ' DATOS COMPRADOR
    If .Bookmarks.Exists("APE_COMPRADOR") Then
        DatoAWord docword, "APE_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Apel)
    End If
    If .Bookmarks.Exists("NOM_COMPRADOR") Then
        DatoAWord docword, "NOM_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).nombre)
    End If
    If .Bookmarks.Exists("TFNO_COMPRADOR") Then
        DatoAWord docword, "TFNO_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Tfno)
    End If
    If .Bookmarks.Exists("FAX_COMPRADOR") Then
        DatoAWord docword, "FAX_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Fax)
    End If
    If .Bookmarks.Exists("MAIL_COMPRADOR") Then
        DatoAWord docword, "MAIL_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).mail)
   End If
   If Not oOferta Is Nothing Then
        ' DATOS OFERTA
        If .Bookmarks.Exists("FEC_OFERTA") Then
            If frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
                'Pasar la fecha al TZ del usuario
                ConvertirUTCaTZ DateValue(oOferta.FechaRec), TimeValue(oOferta.FechaRec), oUsuarioSummit.TimeZone, dtFechaOferta, dtHoraOferta
                tzInfo = GetTimeZoneByKey(oUsuarioSummit.TimeZone)
                DatoAWord docword, "FEC_OFERTA", CStr(dtFechaOferta + dtHoraOferta) & " " & Mid(tzInfo.DisplayName, 1, 11)
            Else
                DatoAWord docword, "FEC_OFERTA", Format(oOferta.FechaRec, "Short date")
            End If
        End If
        If .Bookmarks.Exists("NUM_OFERTA") Then
            DatoAWord docword, "NUM_OFERTA", NullToStr(oOferta.Num)
        End If
        If .Bookmarks.Exists("OBS_OFERTA") Then
            DatoAWord docword, "OBS_OFERTA", NullToStr(oOferta.obs)
        End If
    End If
    If .Bookmarks.Exists("DETALLE") Then
        .Bookmarks("DETALLE").Range.Delete
        .Bookmarks("DETALLE").Delete
    End If
            
    If .Bookmarks.Exists("IMPLICADOS") Then
        Set IPersonas = frmOFEPet.oProcesoSeleccionado
        Set oPersonas = IPersonas.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorApe, True)
        If oPersonas.Count = 0 Then
            .Bookmarks("IMPLICADOS").Range.Delete
        ElseIf .Bookmarks.Exists("IMPLICADO") Then
            .Bookmarks("IMPLICADO").Select
            Set rangeword = blankword.Bookmarks("IMPLICADO").Range
            rangeword.Copy
            .Application.Selection.Delete
            For Each oPer In oPersonas
                .Application.Selection.Paste
                If .Bookmarks.Exists("COD_IMPLICADO") Then
                    .Bookmarks("COD_IMPLICADO").Range.Text = NullToStr(oPer.Cod)
                End If
                If .Bookmarks.Exists("NOM_IMPLICADO") Then
                    .Bookmarks("NOM_IMPLICADO").Range.Text = NullToStr(oPer.nombre)
                End If
                If .Bookmarks.Exists("APE_IMPLICADO") Then
                    .Bookmarks("APE_IMPLICADO").Range.Text = NullToStr(oPer.Apellidos)
                End If
                If .Bookmarks.Exists("ROL_IMPLICADO") Then
                    .Bookmarks("ROL_IMPLICADO").Range.Text = NullToStr(oPer.Rol)
                End If
                If .Bookmarks.Exists("TLFNO_IMPLICADO") Then
                    .Bookmarks("TLFNO_IMPLICADO").Range.Text = NullToStr(oPer.Tfno)
                End If
                If .Bookmarks.Exists("FAX_IMPLICADO") Then
                    .Bookmarks("FAX_IMPLICADO").Range.Text = NullToStr(oPer.Fax)
                End If
                If .Bookmarks.Exists("MAIL_IMPLICADO") Then
                    .Bookmarks("MAIL_IMPLICADO").Range.Text = NullToStr(oPer.mail)
                End If
                If .Bookmarks.Exists("UO1_IMPLICADO") Then
                    .Bookmarks("UO1_IMPLICADO").Range.Text = NullToStr(oPer.UON1)
                End If
                If .Bookmarks.Exists("UO2_IMPLICADO") Then
                    .Bookmarks("UO2_IMPLICADO").Range.Text = NullToStr(oPer.UON2)
                End If
                If .Bookmarks.Exists("UO3_IMPLICADO") Then
                    .Bookmarks("UO3_IMPLICADO").Range.Text = NullToStr(oPer.UON3)
                End If
                If .Bookmarks.Exists("DPTO_IMPLICADO") Then
                    .Bookmarks("DPTO_IMPLICADO").Range.Text = NullToStr(oPer.CodDep)
                End If
                If .Bookmarks.Exists("CARGO_IMPLICADO") Then
                    .Bookmarks("CARGO_IMPLICADO").Range.Text = NullToStr(oPer.Cargo)
                End If
            Next
        End If
    End If
    End With
    Set oProves = Nothing
    Set IPersonas = Nothing
    Set oPersonas = Nothing
    Set oPer = Nothing
    
    Set CartaAdjOrden = docword
    Set docword = Nothing
    blankword.Close
    Set blankword = Nothing
    Set rangeword = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "CartaAdjOrden", err, Erl, , m_bActivado)
        Exit Function
    End If
  
End Function

Private Sub sdbgPet_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value) Is Nothing Then
        If NullToStr(oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value).CodPortal) <> "" And _
          gParametrosGenerales.giINSTWEB = ConPortal Then
            sdbgPet.Columns("CODPROVE").CellStyleSet "ProvPortal"
            sdbgPet.Columns("DENPROVE").CellStyleSet "ProvPortal"
        Else
            sdbgPet.Columns("CODPROVE").CellStyleSet "Normal"
            sdbgPet.Columns("DENPROVE").CellStyleSet "Normal"
        End If
    End If
    
    If sdbgPet.Columns("PORTALCON").Value = "1" Then
        sdbgPet.Columns("APE").CellStyleSet "ProvPortal"
        sdbgPet.Columns("NOM").CellStyleSet "ProvPortal"
    Else
        sdbgPet.Columns("APE").CellStyleSet "Normal"
        sdbgPet.Columns("NOM").CellStyleSet "Normal"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "sdbgPet_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Adjuntar Especificaciones
''' </summary>
''' <param name="sTemp">ruta archivos</param>
''' <param name="scodProve">Proveedor</param>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Sub AdjuntarEspecificaciones(ByVal sTemp As String, ByVal CodProve As String)
    Dim oEsp As CEspecificacion
    Dim oIAdjudicaciones As IAdjudicaciones
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oGrupos As CGrupos
    Dim oGrupo As CGrupo

    'Primero adjuntamos las del proceso
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmOFEPet.oProcesoSeleccionado.especificaciones Is Nothing Then frmOFEPet.oProcesoSeleccionado.CargarTodasLasEspecificaciones
    For Each oEsp In frmOFEPet.oProcesoSeleccionado.especificaciones
        frmESPERA.ProgressBar1.Value = 6
        frmESPERA.lblDetalle = sCap(10) & " " & oEsp.nombre
        frmESPERA.lblDetalle.Refresh
        DoEvents
        EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspProceso
        
        sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
        ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
        Nombres(UBound(Nombres)) = oEsp.nombre
        ReDim Preserve Nombres(UBound(Nombres) + 1)
    Next
    'Ahora adjuntamos los FICHEROS de los items ADJUDICADOS AL PROVEEDOR
    Set oGrupos = oFSGSRaiz.Generar_CGrupos
    Set oIAdjudicaciones = frmOFEPet.oProcesoSeleccionado
    Set oAdjs = oIAdjudicaciones.DevolverAdjudicacionesDeProveedor(CodProve)
    If frmOFEPet.oProcesoSeleccionado.Items Is Nothing Then
        frmOFEPet.oProcesoSeleccionado.CargarTodosLosItems CriterioOrdenacion:=OrdItemPorOrden, SoloConfirmados:=True
    End If
    For Each oAdj In oAdjs
        With frmOFEPet.oProcesoSeleccionado.Items.Item(CStr(oAdj.Id))
            .CargarTodasLasEspecificaciones
            If oGrupos.Item(.grupoCod) Is Nothing Then
                oGrupos.Add frmOFEPet.oProcesoSeleccionado, .grupoCod, ""
            End If
            If .especificaciones.Count > 0 Then
                
                 For Each oEsp In .especificaciones
                        
                    frmESPERA.ProgressBar1.Value = 7
                    frmESPERA.lblDetalle = sCap(10) & " " & oEsp.nombre
                    frmESPERA.lblDetalle.Refresh
                    DoEvents
                    EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspItem

                    sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
                    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                    Nombres(UBound(Nombres)) = oEsp.nombre
                    ReDim Preserve Nombres(UBound(Nombres) + 1)
                Next
            End If
        End With
    Next
    For Each oGrupo In oGrupos
        With oGrupo
            .CargarTodasLasEspecificaciones
            If .especificaciones.Count > 0 Then
                 For Each oEsp In .especificaciones
                        
                    frmESPERA.ProgressBar1.Value = 8
                    frmESPERA.lblDetalle = sCap(10) & " " & oEsp.nombre
                    frmESPERA.lblDetalle.Refresh
                    DoEvents
                    EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspGrupo

                    sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
                    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                    Nombres(UBound(Nombres)) = oEsp.nombre
                    ReDim Preserve Nombres(UBound(Nombres) + 1)
                Next
            End If
        End With
    Next
    
    Set oEsp = Nothing
    Set oIAdjudicaciones = Nothing
    Set oAdjs = Nothing
    Set oAdj = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "AdjuntarEspecificaciones", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Adjuntar Contratos
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Sub AdjuntarContratos()
''COMENTADO PQ SE HAN ELIMINADO LAS CLASES ANTIGUAS DE CONTRATOS PERO ESTO DEBER�A ESTAR, ES LA INCIDENCIA Pruebas 31900.8 2013/208 (EPB 28/06/2013)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
'                'Adjuntamos los contratos si los hay
'                    Set oContratos = oFSGSRaiz.Generar_CContratos
'                    oContratos.BuscarContratos , , frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, oPet.CodProve
'                    For Each oContr In oContratos
'
'                        sFileName = sTemp & oContr.nombre & "." & oContr.Extension
'
'                        frmESPERA.ProgressBar1.Value = 9
'                        frmESPERA.lblDetalle = sCap(10) & " " & sFileName
'                        frmESPERA.lblDetalle.Refresh
'                        DoEvents
'
'                        ' Cargamos el contenido en la esp.
'                        teserror = oContr.ComenzarLecturaData
'                        If teserror.NumError <> TESnoerror Then
'                            basErrores.TratarError teserror
'
'                        Else
'
'                            DataFile = 1
'                            'Abrimos el fichero para escritura binaria
'                            Open sFileName For Binary Access Write As DataFile
'                            lInit = 0
'                            lSize = giChunkSize
'
'                            While lInit < oContr.dataSize
'                                bytChunk = oContr.ReadData(lInit, lSize)
'                                Put DataFile, , bytChunk
'                                lInit = lInit + lSize + 1
'                            Wend
'
'                            'Verificar escritura
'                            If LOF(DataFile) <> oContr.dataSize Then
'                                oMensajes.ErrorDeEscrituraEnDisco
'                            End If
'                            Close DataFile
'
'                            sayFileNames(UBound(sayFileNames)) = sFileName
'                            ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
'                            ' para adjuntar el fichero al mensaje
'                            Nombres(UBound(Nombres)) = oContr.nombre & "." & oContr.Extension
'                            ReDim Preserve Nombres(UBound(Nombres) + 1)
'                            DataFile = 0
'                        End If
'
'                    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAnya", "AdjuntarContratos", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub




