Attribute VB_Name = "basSubMain"
Option Explicit
Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Public Sub CargarRecursos_Errores()

Dim Ador As Ador.Recordset
Dim sTexto1 As String
Dim sTexto2 As String
Dim sTexto3 As String
Dim sTexto4 As String
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(C_ERRORES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sTexto1 = Ador(0).Value
        Ador.MoveNext
        sTexto2 = Ador(0).Value
        Ador.MoveNext
        sTexto3 = Ador(0).Value
        Ador.MoveNext
        sTexto4 = Ador(0).Value
        Ador.Close
        oFSGSRaiz.p_TextosErrores sTexto1, sTexto2, sTexto3, sTexto4
    End If


   Set Ador = Nothing


End Sub
Function RegistroIds()

    Dim idsMail As Object
    
    On Error GoTo Registrar
    
    Set idsMail = CreateObject("IDSMailInterface32.Server")
    
    Set idsMail = Nothing
    
    GoTo Salir
    
Registrar:

    Shell App.Path & "\IDSMAI32.EXE /REGSERVER", vbHide
    Resume Salir
    
Salir:

End Function

''' <summary>
''' Carga la variables globales: gInstancia, gServidor y gBaseDatos
''' </summary>
''' <remarks>Llamada desde: Main        frmLogin/cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Public Function CargarInstancia()
    Dim oFos As FileSystemObject
    Dim ostream As TextStream
    Dim sDesacControlError As String
    Set oFos = New FileSystemObject
    
    Set ostream = oFos.OpenTextFile(App.Path & "\FSGSClient.ini", ForReading, True)
    
    gServidor = ""
    gBaseDatos = ""
    If Not ostream Is Nothing Then
        If Not ostream.AtEndOfStream Then
            gInstancia = ostream.ReadLine
            If InStr(1, gInstancia, "INSTANCIA=") Then
                gInstancia = Right(gInstancia, Len(gInstancia) - 10)
                '10 es el n�mero de letras de 'INSTANCIA=' +1
                If Not ostream.AtEndOfStream Then
                    gServidor = ostream.ReadLine
                    If InStr(1, gServidor, "SERVIDOR=") Then
                        gServidor = Right(gServidor, Len(gServidor) - 9)
                        If Not ostream.AtEndOfStream Then
                            gBaseDatos = ostream.ReadLine
                            If InStr(1, gBaseDatos, "BASEDATOS=") Then
                                gBaseDatos = Right(gBaseDatos, Len(gBaseDatos) - 10)
                            End If
                            If Not ostream.AtEndOfStream Then
                                sDesacControlError = ostream.ReadLine
                                If InStr(1, sDesacControlError, "DESAC_CONTROL_ERROR=") Then
                                    sDesacControlError = Replace(sDesacControlError, "DESAC_CONTROL_ERROR=", "")
                                Else
                                    sDesacControlError = ""
                                End If
                            End If
                            giDesacControlError = val(sDesacControlError)
                        End If
                    End If
                End If
                
            Else
                err.Raise 10000, "FULLSTEP GS", "Instance name missing"
                End
            End If
        Else
            err.Raise 10000, "FULLSTEP GS", "Instance name missing"
            End
        End If
    End If
    ostream.Close
    Set ostream = Nothing
    
    
    Set oFos = Nothing
End Function

''' <summary>
''' Punto de entrada de la aplicaci�n. Lanza el programa si te logeas correcatmente.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Sub Main()
       
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim licfec As String, licloginenc As String, liccontraenc As String
    Dim liclogin As String, liccontra As String
    Dim res As Integer
    Dim BDver As String
    Dim GSver As String
    Dim sBDIdiomas As String
    
    ReDim basPublic.g_aFileNamesToDelete(0)
    
    CargarInstancia
    
   ' Antes de mostrar las primeras pantallas recogemos el idioma,
    ' si no existiera entrada se mueve en un principio INGLES y luego se machaca con el valor de par�metros generales
    gParametrosInstalacion.gIdioma = GetStringSetting("FULLSTEP GS", gInstancia, "Opciones", "Idioma")
    If gParametrosInstalacion.gIdioma = "" Then
       gParametrosInstalacion.gIdioma = "ENG"
    End If
         
    ''' Recogemos los valores de autorizaci�n del servidor de objetos
    Set fil = fso.GetFile(App.Path & "\FSOS.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    licfec = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    licloginenc = ts.ReadLine
    liccontraenc = ts.ReadLine
    ts.Close
    
    ''' Desencriptamos el usuario y la contrase�a del servidor de objetos
    
    ''' Desencriptaci�n login archivo LIC
    liclogin = EncriptarAES("", licloginenc, False, licfec, 2)
    
    ''' Desencriptaci�n contrase�a archivo LIC
    liccontra = EncriptarAES("", liccontraenc, False, licfec, 2)
    
    'Creamos el objeto raiz
    'Tarea 3412: Necesito conectar a la bd de la instalaci�n para ver cual es la bbdd de textos (la mdb) y tener mensajes de error en la carga
    'de gParametrosGenerales.giWinSecurity y para hacer la carga de gParametrosGenerales.giWinSecurity
    
    Set oFSGSRaiz = New CRaiz
    oFSGSRaiz.SetProgramando (giDesacControlError)
    
    'Tarea 3412: Necesito conectar a la bd de la instalaci�n para ver cual es la bbdd de textos (la mdb)
    'Habilitar el gestor de idiomas
    'Recupero de la BD el nombre de la BD Access
    sBDIdiomas = oFSGSRaiz.RecuperarBDIdiomas(gInstancia, gServidor, gBaseDatos)
    If sBDIdiomas = "1" Then
        'Si no hay conexion al servidor de BD: mensaje y end
        MsgBox "Cannot connect to database server.", vbCritical + vbOKOnly, "FULLSTEP"
        Unload frmEsperaAccion
        End
    End If
            
    Set oGestorIdiomas = New FSgsidiomas.CGestorIdiomas
    oGestorIdiomas.Inicializar (sBDIdiomas)
    
    'Tarea 3412: Necesito conectar a la bd de la instalaci�n para tener mensajes de error
    Set oMensajes = New FSGSLibrary.CMensajes
    Set oMensajes.oGestorIdiomas = oGestorIdiomas
    Set oMensajes.frmMessageBox = frmMessageBox
    oMensajes.IdiomaInstalacion = basPublic.gParametrosInstalacion.gIdioma
    
    frmEsperaAccion.WindowState = vbNormal
    frmEsperaAccion.Show
    DoEvents
    
    'Tarea 3412: Necesito conectar a la bd de la instalaci�n para saber gParametrosGenerales.giWinSecurity
    res = oFSGSRaiz.Autentificar(liclogin, liccontra, gInstancia, gServidor, gBaseDatos)
    
    Select Case res
    Case 1
        oMensajes.ImposibleConectarBD
        End
    Case 2
        oMensajes.ImposibleAutentificar
        End
    End Select
    
    'Nos conectamos a la BD
    res = oFSGSRaiz.Conectar(gInstancia, gServidor, gBaseDatos)
   
    Select Case res
        Case 1
            oMensajes.ImposibleConectarBD
            End
    End Select
   
    'Tarea 3412: Necesito conectar a la bd de la instalaci�n para saber si es posible ejecutar el GS, a tienes el Gs al d�a.
    BDver = oFSGSRaiz.ValidarBDVersion(App.Major, App.Minor)
    If BDver <> "" Then
        GSver = App.Major & "." & App.Minor & "." & App.Revision
        oMensajes.ImposibleEjecutarGS GSver, BDver
        End
    End If
 
    'Instancio el gestor de seguridad
    Set oGestorSeguridad = oFSGSRaiz.generar_cgestorseguridad
       
    'Tarea 3412: Necesito conectar a la bd de la instalaci�n para saber gParametrosGenerales.giWinSecurity
    ''' CARGA DE PARAMETROS GENERALES
    Set oGestorParametros = oFSGSRaiz.generar_CGestorParametros
    gParametrosGenerales = oGestorParametros.DevolverParametrosGenerales(gParametrosInstalacion.gIdioma)
    If gParametrosGenerales.giINSTWEB = ConPortal Then
        If basParametros.gParametrosGenerales.gsFSP_SRV = "" Or basParametros.gParametrosGenerales.gsFSP_BD = "" Or basParametros.gParametrosGenerales.gsFSP_CIA = "" Then
            oMensajes.InstalacionPortalIncompleta
            Unload frmEsperaAccion
            FinDeSesion
        End If
    End If
    
    garSimbolos = oGestorParametros.DevolverSettingsSymbols
    
    Dim Ador As Ador.Recordset
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(OTROS, basPublic.gParametrosInstalacion.gIdioma, 201)
    If Not Ador Is Nothing Then
        gSubjectMultiIdiomaSMTP = Ador(0).Value
    End If
    
    If gParametrosGenerales.giWinSecurity = TiposDeAutenticacion.Windows Or gParametrosGenerales.giWinSecurity = TiposDeAutenticacion.UsuariosLocales Then

        Dim oUsuario As CUsuario
        Dim iCont As Integer
        Dim sUsuario As String
        Dim sPwd As String

        Set oUsuario = oFSGSRaiz.generar_cusuario
        sUsuario = ShowUserName
        Set oUsuario = oGestorSeguridad.DevolverBaseUsuario(sUsuario)

        If oUsuario.Tipo = 0 Or IsNull(oUsuario.fecpwd) Then
            'EPB 20150313 si vengo de windows es windows quien controla cuantas veces puede intentarlo, no GS
            'If oUsuario.Bloq = 0 Then
                For iCont = 1 To 5
                    iCont = DesactivarConexion(iCont)
                Next iCont
                oMensajes.UsuarioNoAutorizado
                Unload frmEsperaAccion
                FinDeSesion
            'End If
            Exit Sub
        End If

        'Comprobacion de si ha expirado la contrase�a
        If Contrase�aExpirada(oUsuario.fecpwd) Then
            'INC Pruebas 31900.1/2010/139: hay que inicializar aqui el objeto oUsuarioSummit porque es el que se utiliza en el formulario de cambio de password.
            'Mas adelante cuando se valida la contrase�a se sobreescribe el objeto.
            oUsuario.Cod = sUsuario
            oUsuario.PWDDes = EncriptarAES(sUsuario, oUsuario.Pwd, False, oUsuario.fecpwd, 1, TIpoDeUsuario.Persona)
            Set oUsuarioSummit = oUsuario
            
            If Not MostrarFormUSUCamb(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, True, oUsuarioSummit, oMensajes, gParametrosGenerales) Then FinDeSesion
        End If

        sPwd = EncriptarAES(sUsuario, oUsuario.Pwd, False, oUsuario.fecpwd, 1, TIpoDeUsuario.Persona)

        LoginInvisible sUsuario & " " & sPwd
    Else
       frmLogin.Show 1
    End If
   
        
    If Not oUsuarioSummit Is Nothing Then
        'Si el usuario est� bloqueado
        If oUsuarioSummit.Bloq = 100 Then
            Unload frmEsperaAccion
            oMensajes.UsuarioBloqueado
            FinDeSesion
        End If
        
        ' Registro una nueva sesion
        Set oSesionSummit = oFSGSRaiz.generar_cSesion
        oSesionSummit.RegistrarNuevaSesion oUsuarioSummit.Cod
                       
        ''' CARGA DE PARAMETROS DE LA INSTALACION
        gParametrosInstalacion = oGestorParametros.DevolverParametrosInstalacion(oUsuarioSummit.Cod, oUsuarioSummit.idioma)
        oMensajes.IdiomaInstalacion = basPublic.gParametrosInstalacion.gIdioma
        CargarRecursos_Errores
        '''Lee otra vez los literales pero en el idioma del usuario
        gParametrosGenerales = oGestorParametros.DevolverLiteralesGenerales(gParametrosInstalacion.gIdioma)
        
        ''' CARGA DE PARAMETROS DE SM
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            Set g_oParametrosSM = oFSGSRaiz.Generar_CParametrosSM
            g_oParametrosSM.CargarTodosLosArboles
            If g_oParametrosSM.Count = 0 Then
                Set g_oParametrosSM = Nothing
            End If
        End If
        
        ''' CARGA DE LONGITUDES DE CODIGOS
        Set oDiccionarioSummit = oFSGSRaiz.Generar_CDiccionario
        gLongitudesDeCodigos = oDiccionarioSummit.DevolverLongitudesDeCodigos
        
        
        '''Carga del gestor de informes personalizados
        Set oGestorListadosPers = oFSGSRaiz.Generar_CGestorListadosPers
        
        '''Carga del gestor de informes personalizados externos (Microstrategy)
        Set oGestorLisPerExternos = oFSGSRaiz.generar_CGestorLisPerExternos
        
        ''SI EST� COMPILADO SOLO PARA ADMINISTRACI�N P�BLICA
        If ADMIN_OBLIGATORIO Then
            ConfigurarAdminPublicaObligatoria
        End If
        
        ''' CARGA DE PARAMETROS DE INTEGRACION
        gParametrosIntegracion = oGestorParametros.DevolverParametrosIntegracion
                
        ' Almaceno los valores de optimizacion
        basOptimizacion.gvarCodUsuario = oUsuarioSummit.Cod
        If oUsuarioSummit.EsAdmin Then
            basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador
        Else
            basOptimizacion.gTipoDeUsuario = oUsuarioSummit.Tipo
        End If
        If gParametrosGenerales.gbPymes Then
            basOptimizacion.gPYMEUsuario = oUsuarioSummit.pyme
        End If
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            basOptimizacion.gCodEqpUsuario = oUsuarioSummit.Persona.codEqp
            basOptimizacion.gCodCompradorUsuario = oUsuarioSummit.Persona.Cod
        End If
        
        basOptimizacion.gCodPersonaUsuario = oUsuarioSummit.Persona.Cod
        basOptimizacion.gCodDepUsuario = oUsuarioSummit.Persona.CodDep
        basOptimizacion.gUON1Usuario = oUsuarioSummit.Persona.UON1
        basOptimizacion.gUON2Usuario = oUsuarioSummit.Persona.UON2
        basOptimizacion.gUON3Usuario = oUsuarioSummit.Persona.UON3
        Set basOptimizacion.g_oEmpresaUsuario = oUsuarioSummit.DevolverEmpresaDelUsuario
        
        
        ''' CARGA DE PROPIEDADES DE WINDOWS DEL USARIO
        If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
            If oUsuarioSummit.CuentaDebeCambiarPwd Then
                MsgBox oMensajes.CargarTextoMensaje(554), vbCritical, "FULLSTEP" 'Debe cambiar la contrase�a en la primera conexion
                
                If Not MostrarFormUSUPWDCamb(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oUsuarioSummit, oMensajes, gParametrosGenerales) Then FinDeSesion
            End If
        End If
        
        ''' Conexi�n con el servidor de informes
        If Not FSGSInformes.Conectar(gInstancia, gServidor, gBaseDatos) Then
            oMensajes.ImposibleConectarAlServidorDeInformes
        End If
                        
        'Lanzo el MDI
        Unload frmEsperaAccion
        
        If gParametrosGenerales.giMail > 1 Then
            RegistroIds
        End If
        
        MDI.Show
        
   Else
        Unload frmEsperaAccion
        oMensajes.UsuarioNoAutorizado
        FinDeSesion
   End If
      
End Sub
  
''' <summary>
''' Libera la memoria ocupada por las variables globales
''' </summary>
''' <remarks>Llamada desde: pantallas q acaban con la ejecuci�n ; Tiempo m�ximo: 0,2</remarks>
Public Sub FinDeSesion()
Dim i As Integer
Dim FOSFile As Scripting.FileSystemObject
  
    Set oUsuarioSummit = Nothing
    Set oGestorSeguridad = Nothing
    Set oGestorParametros = Nothing
    Set oGestorInformes = Nothing
    Set oDiccionarioSummit = Nothing
    Set oSesionSummit = Nothing
    Set oGestorInformes = Nothing
    If Not oIdsMail Is Nothing Then
        oIdsMail.FuerzaFinalizeSmtpClient
    End If
    Set oIdsMail = Nothing
    Set oGestorListadosPers = Nothing
    Set g_oParametrosSM = Nothing
    Set oMensajes = Nothing
    
On Error GoTo Error:
    Set FOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(basPublic.g_aFileNamesToDelete)
        If FOSFile.FileExists(basPublic.g_aFileNamesToDelete(i)) Then
            FOSFile.DeleteFile basPublic.g_aFileNamesToDelete(i), True
        End If
        
        i = i + 1
    Wend
  
    Set FOSFile = Nothing

    End
      
Error:
    Resume Next

  End Sub

''' <summary>
''' Procedimiento que valida al usuario para que pueda entrar en el GS sin tener que introducir usuario y contrase�a
''' </summary>
''' <param name="Command">El codigo de usuario y contrase�a del usuario</param>
''' <remarks>
''' Llamada desde: FSGSServer/BasSubMain/Main
''' Tiempo maximo: 0 seg</remarks>
Private Sub LoginInvisible(sCommand As String)
    Dim i As Integer
    Dim oUsuario As CUsuario
    Dim sUsuCod As String
    Dim sUsuPWD As String
    
    i = InStr(1, sCommand, " ", vbTextCompare)
    
    If i = 0 Then
        sUsuPWD = ""
        sUsuCod = sCommand
    Else
        sUsuCod = Left(sCommand, i - 1)
        sUsuPWD = Right(sCommand, Len(sCommand) - InStr(1, sCommand, " ", vbTextCompare))
    End If
    
    Set oUsuario = oFSGSRaiz.generar_cusuario
    
    Set oUsuario = oGestorSeguridad.DevolverBaseUsuario(sUsuCod)
    
    If oUsuario.Tipo = 0 Or IsNull(oUsuario.fecpwd) Then
        Set oUsuarioSummit = Nothing
        Exit Sub
    End If
        
    ''' Validaci�n
    Set oUsuarioSummit = oGestorSeguridad.ValidarUsuario(sUsuCod, sUsuPWD)
    
End Sub

Private Function ShowUserName() As String

    Dim lpbuff As String
    Dim nSize As Long
    Dim res As Long

    nSize = 25&
    lpbuff = Space(20)
    res = GetUserName(lpbuff, nSize)
    ShowUserName = Left(lpbuff, nSize - 1)

End Function

Private Function Contrase�aExpirada(ByVal fecpwd As Date) As Boolean
    If gParametrosGenerales.giEDAD_MAX_PWD = 0 Then
        Contrase�aExpirada = False
    Else
        If Date > gParametrosGenerales.giEDAD_MAX_PWD + fecpwd Then
            Contrase�aExpirada = True
        Else
            Contrase�aExpirada = False
        End If
    End If
End Function

