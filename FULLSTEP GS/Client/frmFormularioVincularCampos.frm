VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFormularioVincularCampos 
   BackColor       =   &H00808000&
   Caption         =   "DVincular a otras solicitudes"
   ClientHeight    =   4485
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9060
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormularioVincularCampos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   9060
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "DCancelar"
      Height          =   315
      Left            =   4680
      TabIndex        =   2
      Top             =   4080
      Width           =   1500
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "DAceptar"
      Height          =   315
      Left            =   2880
      TabIndex        =   1
      Top             =   4080
      Width           =   1500
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddOrigen 
      Height          =   975
      Left            =   240
      TabIndex        =   0
      Top             =   3360
      Width           =   2295
      DataFieldList   =   "Column 1"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3757
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1720
      _StockProps     =   77
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgVincular 
      Height          =   3855
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   8865
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   17
      stylesets.count =   2
      stylesets(0).Name=   "Amarillo"
      stylesets(0).BackColor=   12648447
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormularioVincularCampos.frx":0CB2
      stylesets(1).Name=   "Azul"
      stylesets(1).BackColor=   16777160
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFormularioVincularCampos.frx":0CCE
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   17
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "IDATRIBUTO"
      Columns(1).Name =   "IDATRIBUTO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Name =   "ATRIBUTO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "IDDESTINO"
      Columns(3).Name =   "IDDESTINO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "DESTINO"
      Columns(4).Name =   "DESTINO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "IDORIGEN"
      Columns(5).Name =   "IDORIGEN"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "ORIGEN"
      Columns(6).Name =   "ORIGEN"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   3
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TIPO_CAMPO_GS"
      Columns(7).Name =   "TIPO_CAMPO_GS"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "TIPO"
      Columns(8).Name =   "TIPO"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "DVID"
      Columns(9).Name =   "DVID"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "SOLICITUD"
      Columns(10).Name=   "SOLICITUD"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "PADREDESTINO"
      Columns(11).Name=   "PADREDESTINO"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "PADREORIGEN"
      Columns(12).Name=   "PADREORIGEN"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "SUBTIPO"
      Columns(13).Name=   "SUBTIPO"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "ID_ATRIB"
      Columns(14).Name=   "ID_ATRIB"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "TABLA_EXTERNA"
      Columns(15).Name=   "TABLA_EXTERNA"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "INTRO"
      Columns(16).Name=   "INTRO"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      _ExtentX        =   15637
      _ExtentY        =   6800
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmFormularioVincularCampos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_vDesgloseVinculadoId As Variant
Public g_lCampoDesgloseVinculado As Long
Public g_lCampoDesgloseOrigen As Long
Public g_lSolicitudOrigen As Long
Public g_bModificar As Boolean


''' <summary>
''' Grabar los cambios realizados en la vinculacion
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim oVinculados As CVinculaciones
    Dim vbm As Variant
    Dim i As Integer
    Dim lOrigenEnBd As Long
    
    Dim aIdentificadoresCampoVin() As String
    Dim aIdentificadoresCampoOri() As String
        
    Set oVinculados = Nothing
    Set oVinculados = oFSGSRaiz.Generar_CVinculados
    Set oVinculados.Campos = oFSGSRaiz.Generar_CCamposVinculado
    
    'Tiene que introducir tipo y desglose en todas las lineas
    Me.sdbgVincular.MoveFirst
    lOrigenEnBd = sdbgVincular.Columns("PADREORIGEN").Value
    
    ReDim aIdentificadoresCampoVin(sdbgVincular.Rows - 1)
    ReDim aIdentificadoresCampoOri(sdbgVincular.Rows - 1)
       
    For i = 0 To sdbgVincular.Rows - 1
        vbm = sdbgVincular.AddItemBookmark(i)
            
        oVinculados.Campos.Add sdbgVincular.Columns("ID").CellValue(vbm), sdbgVincular.Columns("IDDESTINO").CellValue(vbm) _
            , IIf(sdbgVincular.Columns("ORIGEN").CellValue(vbm) = "", "", sdbgVincular.Columns("IDORIGEN").CellValue(vbm)) _
            , g_lSolicitudOrigen, g_lCampoDesgloseOrigen, g_lCampoDesgloseVinculado
            
        aIdentificadoresCampoVin(i) = sdbgVincular.Columns("IDDESTINO").CellValue(vbm)
        aIdentificadoresCampoOri(i) = IIf(sdbgVincular.Columns("ORIGEN").CellValue(vbm) = "", "", sdbgVincular.Columns("IDORIGEN").CellValue(vbm))
    Next i
    
    If oVinculados.Campos.Count > 0 Then
        Screen.MousePointer = vbHourglass
        udtTeserror = oVinculados.Campos.GrabarCamposVinculaciones(lOrigenEnBd <> g_lCampoDesgloseOrigen)
        If udtTeserror.NumError = TESnoerror Then
            RegistrarAccion ACCVincularDesglose, "Desgl Vinc:" & CStr(g_lCampoDesgloseVinculado) _
                & " Solic:" & CStr(g_lSolicitudOrigen) & " Desgl Origen:" & CStr(g_lCampoDesgloseOrigen)
            For i = 0 To UBound(aIdentificadoresCampoVin)
                RegistrarAccion ACCVincularCampos, "Campo Vinc:" & aIdentificadoresCampoVin(i) & " Campo Origen:" & aIdentificadoresCampoOri(i)
            Next
            
            Unload Me
        Else
            Screen.MousePointer = vbNormal
            TratarError udtTeserror
        End If
        Screen.MousePointer = vbNormal
    End If

    Set oVinculados = Nothing
    
End Sub
''' <summary>
''' Cerrar pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdCancelar_Click()
    Unload Me
End Sub
''' <summary>
''' Carga de la pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargaGridVinculaci�n
    
    sdbddOrigen.AddItem ""
    If g_bModificar Then
        sdbgVincular.Columns("ORIGEN").DropDownHwnd = Me.sdbddOrigen.hWnd
    Else
        sdbgVincular.Columns("ORIGEN").Locked = True
        sdbgVincular.Columns("ORIGEN").Style = ssStyleEdit
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
    End If
    
End Sub
''' <summary>
''' Carga de los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_VINCULARCAMPOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        sdbgVincular.Columns("DESTINO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgVincular.Columns("ORIGEN").caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
''' <summary>
''' Establecer los tama�os de los campos de pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Resize()
    Me.sdbgVincular.Width = Me.Width - 350
    Me.sdbgVincular.Height = Me.Height - 1200
    
    Me.sdbgVincular.Columns("ATRIBUTO").Width = (Me.sdbgVincular.Width / 5) - 200
    Me.sdbgVincular.Columns("DESTINO").Width = Me.sdbgVincular.Columns("ATRIBUTO").Width * 2
    Me.sdbgVincular.Columns("ORIGEN").Width = Me.sdbgVincular.Width - Me.sdbgVincular.Columns("ATRIBUTO").Width
    Me.sdbgVincular.Columns("ORIGEN").Width = Me.sdbgVincular.Columns("ORIGEN").Width - Me.sdbgVincular.Columns("DESTINO").Width - 600
    
    Me.sdbddOrigen.Columns("DEN").Width = Me.sdbgVincular.Columns("ORIGEN").Width - 380
    
    Me.cmdAceptar.Top = Me.Height - 915
    Me.cmdCancelar.Top = Me.cmdAceptar.Top
    
    Me.cmdAceptar.Left = (Me.Width / 2) - Me.cmdAceptar.Width - 195
    Me.cmdCancelar.Left = Me.cmdAceptar.Left + Me.cmdAceptar.Width + 300
End Sub
''' <summary>
''' Cargar las vinculaciones a nivel de campo de un desglose
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo:0,2</remarks>
Private Sub CargaGridVinculaci�n()
    Dim sCadena As String
    Dim Ador As ADODB.Recordset
    Dim oVinculados As CVinculaciones
    
    sdbgVincular.RemoveAll
    
    
    Set oVinculados = Nothing
    Set oVinculados = oFSGSRaiz.Generar_CVinculados

    Set Ador = oVinculados.CargarCamposVinculados(g_vDesgloseVinculadoId, g_lCampoDesgloseVinculado, g_lCampoDesgloseOrigen)
    
    While Not Ador.EOF
        sCadena = Ador.Fields("DVID").Value & Chr(m_lSeparador) & Ador.Fields("COD_ATRIB").Value & Chr(m_lSeparador) & Ador.Fields("DEN_ATRIB").Value
        sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("ID").Value & Chr(m_lSeparador) & Ador.Fields("DEN").Value
        sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("CAMPO_HIJO_ORIGEN").Value & Chr(m_lSeparador) & Ador.Fields("DENLV").Value
        sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("TIPO_CAMPO_GS").Value & Chr(m_lSeparador) & Ador.Fields("TIPO").Value
        sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("DVID").Value & Chr(m_lSeparador) & Ador.Fields("SOLICITUD").Value
        sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("CAMPO_VINCULADO").Value & Chr(m_lSeparador) & Ador.Fields("CAMPO_ORIGEN").Value
        sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("SUBTIPO").Value & Chr(m_lSeparador) & Ador.Fields("ID_ATRIB").Value
        sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("TABLA_EXTERNA").Value & Chr(m_lSeparador) & Ador.Fields("INTRO").Value
            
        sdbgVincular.AddItem sCadena
        
        Ador.MoveNext
    Wend
End Sub
''' <summary>
''' Seleccionar un campo para la columna campo origen
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddOrigen_CloseUp()
    sdbgVincular.Columns("IDORIGEN").Value = sdbddOrigen.Columns("ID").Value
    sdbgVincular.Columns("ORIGEN").Value = sdbddOrigen.Columns("DEN").Value
End Sub
''' <summary>
''' Cargar el combo para los campos para la columna campo origen
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddOrigen_DropDown()
    Dim oCampo As CFormItem
    Dim Ador As ADODB.Recordset
    Dim sCadena As String
    
    sdbddOrigen.RemoveAll
    
    If sdbddOrigen.Rows = 0 Then
        sdbddOrigen.AddItem ""
    End If
    
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    
    If sdbgVincular.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
        Set Ador = oCampo.CargarCamposDesgloseVincular(g_lCampoDesgloseOrigen, Me.sdbgVincular.Columns("TIPO_CAMPO_GS").Value, -1, -1, -1, -1)
    ElseIf sdbgVincular.Columns("TIPO").Value = TipoCampoPredefinido.Atributo Then
        Set Ador = oCampo.CargarCamposDesgloseVincular(g_lCampoDesgloseOrigen, -1, sdbgVincular.Columns("ID_ATRIB").Value, -1, -1, -1)
    ElseIf sdbgVincular.Columns("TIPO").Value = TipoCampoPredefinido.Normal Then
        Set Ador = oCampo.CargarCamposDesgloseVincular(g_lCampoDesgloseOrigen, -1, -1, sdbgVincular.Columns("SUBTIPO").Value, sdbgVincular.Columns("INTRO").Value, -1)
    Else 'externo
        Set Ador = oCampo.CargarCamposDesgloseVincular(g_lCampoDesgloseOrigen, -1, -1, -1, -1, sdbgVincular.Columns("TABLA_EXTERNA").Value)
    End If
    
    While Not Ador.EOF
        sCadena = Ador.Fields("ID").Value
        sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("DEN").Value
        
        sdbddOrigen.AddItem sCadena
        Ador.MoveNext
    Wend
    
    Ador.Close
    Set Ador = Nothing
    Set oCampo = Nothing
        
    sdbgVincular.ActiveCell.SelStart = 0
    sdbgVincular.ActiveCell.SelLength = Len(sdbgVincular.ActiveCell.Value)
    
End Sub
''' <summary>
''' Inicializar el combo para un campo para la columna campo origen
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddOrigen_InitColumnProps()
    sdbddOrigen.DataFieldList = "Column 1"
    sdbddOrigen.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>
''' Posicionar el combo para la columna campo origen
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddOrigen_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddOrigen.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddOrigen.Rows - 1
            bm = sdbddOrigen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOrigen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbddOrigen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
''' <summary>
''' Estilo que se le va a dar a la fila cuando se cargue.
''' </summary>
''' <returns>nada</returns>
''' <remarks>Llamada desde=propio formulario; Tiempo m�ximo=0seg.</remarks>
Private Sub sdbgVincular_RowLoaded(ByVal Bookmark As Variant)
    If sdbgVincular.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgVincular.Columns("DESTINO").CellStyleSet "Amarillo"
    ElseIf sdbgVincular.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Atributo Then
        sdbgVincular.Columns("DESTINO").CellStyleSet "Azul"
    Else
        sdbgVincular.Columns("DESTINO").CellStyleSet ""
    End If
    sdbgVincular.Columns("ATRIBUTO").CellStyleSet "Azul"
End Sub


