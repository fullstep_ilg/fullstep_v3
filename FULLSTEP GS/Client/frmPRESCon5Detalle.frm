VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPRESCon5Detalle 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   2220
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6120
   Icon            =   "frmPRESCon5Detalle.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   6120
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   1320
      ScaleHeight     =   420
      ScaleWidth      =   3225
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1680
      Width           =   3225
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   510
         TabIndex        =   3
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   1650
         TabIndex        =   4
         Top             =   60
         Width           =   1005
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   1350
      ScaleHeight     =   555
      ScaleWidth      =   3210
      TabIndex        =   0
      Top             =   120
      Width           =   3210
      Begin VB.TextBox txtCod 
         Height          =   285
         Left            =   60
         TabIndex        =   1
         Top             =   210
         Width           =   1530
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgDen 
      Height          =   735
      Left            =   1410
      TabIndex        =   2
      Top             =   720
      Width           =   4350
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2170
      Columns(0).Caption=   "IDI"
      Columns(0).Name =   "IDI"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   14671839
      Columns(1).Width=   5477
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_IDI"
      Columns(2).Name =   "COD_IDI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   7673
      _ExtentY        =   1296
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblDenominacion 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Denominaci�n:"
      ForeColor       =   &H8000000E&
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   720
      Width           =   1065
   End
   Begin VB.Label lblCodigo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo:"
      ForeColor       =   &H8000000E&
      Height          =   195
      Left            =   120
      TabIndex        =   6
      Top             =   345
      Width           =   540
   End
End
Attribute VB_Name = "frmPRESCon5Detalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public m_sAccion As String
Public m_sTitulo As String

'Codigos Presupuesto arbol
Public m_iNivel As Integer
Public m_sPres0 As String
Public m_sPres1 As String
Public m_sPres2 As String
Public m_sPres3 As String
Public m_sCod As String 'Con las 2 variables evitamos la lectura de la carga (Al modificar)
Public m_sDen As String
Public m_bNodoConHijos As Boolean

'Variables privadas
Private m_sAnyadir As String
Private m_sModificar As String
Private m_sDetalle As String
Private g_oIdiomas As CIdiomas
Private m_Text As String

''' <summary>
''' Guarda un nuevo nivel del arbol o guarda cambios en uno existente
''' </summary>
''' <remarks>Llamada desde Click del boton Aceptar; Tiempo m�ximo < 1 seg.</remarks>

Private Sub cmdAceptar_Click()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    Screen.MousePointer = vbHourglass
    Dim oPRES1 As CPresConcep5Nivel1
    Dim oPRES2 As CPresConcep5Nivel2
    Dim oPRES3 As CPresConcep5Nivel3
    Dim oPRES4 As CPresConcep5Nivel4
    
    Dim i As Integer
    Dim vbm As Variant
    Dim oIdioma As CIdioma
    Dim oDenominaciones As CMultiidiomas
    
    If Trim(txtCod.Text) = "" Then
        oMensajes.NoValido lblCodigo.caption
        If Me.Visible Then txtCod.SetFocus
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    sdbgDen.MoveFirst
    For i = 0 To sdbgDen.Rows - 1
        vbm = sdbgDen.AddItemBookmark(i)
            If sdbgDen.Columns("DEN").CellValue(vbm) = "" Then
                oMensajes.NoValido sdbgDen.Columns("IDI").CellValue(vbm)
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
    Next i
                                                
    Select Case m_sAccion
        Case "Anyadir":
            Select Case m_iNivel
                Case 1:

                    Set oPRES1 = oFSGSRaiz.Generar_CPresConcep5Nivel1
                    oPRES1.Pres0 = m_sPres0
                    oPRES1.Cod = Trim(txtCod.Text)
                    'oPRES1.Den = Trim(txtDen.Text)
                    Set oIBaseDatos = oPRES1
                    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                    For Each oIdioma In g_oIdiomas
                        For i = 0 To sdbgDen.Rows - 1
                            vbm = sdbgDen.AddItemBookmark(i)
                            If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                            
                                If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                                    m_Text = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                End If
                                    oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                Exit For
                                
                            End If
                        Next i
                    Next
                    Set oPRES1.Denominaciones = oDenominaciones
                    
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirPRESAEstructura
                    Else
                        TratarError teserror
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    
                    Set oPRES1 = Nothing
                    Unload Me
                    

                
                Case 2:
                    Set oPRES2 = oFSGSRaiz.Generar_CPresConcep5Nivel2
                    oPRES2.Pres0 = m_sPres0
                    oPRES2.Pres1 = m_sPres1
                    oPRES2.Cod = Trim(txtCod.Text)
                    'oPRES2.Den = Trim(txtDen.Text)
                    oPRES2.NodoConHermanos = m_bNodoConHijos

                    Set oIBaseDatos = oPRES2
                    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                    For Each oIdioma In g_oIdiomas
                        For i = 0 To sdbgDen.Rows - 1
                            vbm = sdbgDen.AddItemBookmark(i)
                            If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                                If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                                    m_Text = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                End If
                                oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                Exit For
                            End If
                        Next i
                    Next
                    Set oPRES2.Denominaciones = oDenominaciones
                    
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirPRESAEstructura
                    Else
                        TratarError teserror
                        Set oPRES2 = Nothing
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If

                    Unload Me


                    Set oPRES2 = Nothing
                                        
                Case 3:
                    Set oPRES3 = oFSGSRaiz.Generar_CPresConcep5Nivel3
                    oPRES3.Pres0 = m_sPres0
                    oPRES3.Pres1 = m_sPres1
                    oPRES3.Pres2 = m_sPres2
                    oPRES3.Cod = Trim(txtCod.Text)
                    'oPRES3.Den = Trim(txtDen.Text)
                    oPRES3.NodoConHermanos = m_bNodoConHijos
                    
                    Set oIBaseDatos = oPRES3
                    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                    
                    For Each oIdioma In g_oIdiomas
                        For i = 0 To sdbgDen.Rows - 1
                            vbm = sdbgDen.AddItemBookmark(i)
                            If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                            
                                If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                                    m_Text = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                End If
                                    oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                Exit For
                                
                            End If
                        Next i
                    Next
                    Set oPRES3.Denominaciones = oDenominaciones
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirPRESAEstructura
                    Else
                        TratarError teserror
                        Set oPRES3 = Nothing
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    Unload Me

                    Set oPRES3 = Nothing
                    
                Case 4:
                    Set oPRES4 = oFSGSRaiz.Generar_CPresConcep5Nivel4
                    oPRES4.Pres0 = m_sPres0
                    oPRES4.Pres1 = m_sPres1
                    oPRES4.Pres2 = m_sPres2
                    oPRES4.Pres3 = m_sPres3
                    oPRES4.Cod = Trim(txtCod.Text)
                    'oPRES4.Den = Trim(txtDen.Text)
                    oPRES4.NodoConHermanos = m_bNodoConHijos
                    
                    Set oIBaseDatos = oPRES4
                    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                    
                    For Each oIdioma In g_oIdiomas
                        For i = 0 To sdbgDen.Rows - 1
                            vbm = sdbgDen.AddItemBookmark(i)
                            If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                            
                                If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                                    m_Text = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                End If
                                    oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                Exit For
                                
                            End If
                        Next i
                    Next
                    Set oPRES4.Denominaciones = oDenominaciones
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirPRESAEstructura
                    Else
                        TratarError teserror
                        Set oPRES4 = Nothing
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                        
                        
                    Unload Me
                    Set oPRES4 = Nothing
                
            End Select
            
        Case "Modificar":
                Select Case m_iNivel
                    Case 1:
                        
                        Set oPRES1 = oFSGSRaiz.Generar_CPresConcep5Nivel1
                        oPRES1.Pres0 = m_sPres0
                        oPRES1.Cod = Trim(txtCod.Text)
                        'oPRES1.Den = Trim(txtDen.Text)
                        Set oIBaseDatos = oPRES1
                        
                        Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                            For Each oIdioma In g_oIdiomas
                                For i = 0 To sdbgDen.Rows - 1
                                    vbm = sdbgDen.AddItemBookmark(i)
                                    If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                                        If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                                            m_Text = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                        End If
                                        oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                        Exit For
                                    End If
                                Next i
                            Next
                        Set oPRES1.Denominaciones = oDenominaciones
                    
                        teserror = oIBaseDatos.FinalizarEdicionModificando
                        If teserror.NumError = TESnoerror Then
                            ModificarPRESEnEstructura
                            'RegistrarAccion ACCPresCon4Nivel3Mod, "CodPRES1:" & frmPresupuestos4.oPres3Seleccionado.CodPRES1 & "CodPRES2:" & frmPresupuestos4.oPres3Seleccionado.CodPRES2 & "Cod:" & Trim(txtCod)
                        Else
                            TratarError teserror
                            Set oPRES1 = Nothing
                            Set oIBaseDatos = Nothing
                            Exit Sub
                        End If
                        
                        Set oPRES1 = Nothing
                        Unload Me
                    
                    Case 2:
                    
                        Set oPRES2 = oFSGSRaiz.Generar_CPresConcep5Nivel2
                        oPRES2.Pres0 = m_sPres0
                        oPRES2.Pres1 = m_sPres1
                        oPRES2.Cod = Trim(txtCod.Text)
                        'oPRES2.Den = Trim(txtDen.Text)
                        Set oIBaseDatos = oPRES2
                        
                        Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                            For Each oIdioma In g_oIdiomas
                                For i = 0 To sdbgDen.Rows - 1
                                    vbm = sdbgDen.AddItemBookmark(i)
                                    If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                                        If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                                            m_Text = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                        End If
                                        oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                        Exit For
                                    End If
                                Next i
                            Next
                        Set oPRES2.Denominaciones = oDenominaciones
                        
                        teserror = oIBaseDatos.FinalizarEdicionModificando
                        If teserror.NumError = TESnoerror Then
                            ModificarPRESEnEstructura
                        Else
                            TratarError teserror
                            Set oPRES2 = Nothing
                            Set oIBaseDatos = Nothing
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                        
                        Set oPRES2 = Nothing
                        Unload Me
                    
                    Case 3:
                        Set oPRES3 = oFSGSRaiz.Generar_CPresConcep5Nivel3
                        oPRES3.Pres0 = m_sPres0
                        oPRES3.Pres1 = m_sPres1
                        oPRES3.Pres2 = m_sPres2
                        oPRES3.Cod = Trim(txtCod.Text)
                        'oPRES3.Den = Trim(txtDen.Text)
                        Set oIBaseDatos = oPRES3
                        
                        Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                            For Each oIdioma In g_oIdiomas
                                For i = 0 To sdbgDen.Rows - 1
                                    vbm = sdbgDen.AddItemBookmark(i)
                                    If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                                        If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                                            m_Text = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                        End If
                                        oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                        Exit For
                                    End If
                                Next i
                            Next
                        Set oPRES3.Denominaciones = oDenominaciones
                        
                        teserror = oIBaseDatos.FinalizarEdicionModificando
                        If teserror.NumError = TESnoerror Then
                            ModificarPRESEnEstructura
                        Else
                            TratarError teserror
                            Set oPRES3 = Nothing
                            Set oIBaseDatos = Nothing
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                        
                        Set oPRES3 = Nothing
                        Unload Me
                    
                    Case 4:
                        Set oPRES4 = oFSGSRaiz.Generar_CPresConcep5Nivel4
                        oPRES4.Pres0 = m_sPres0
                        oPRES4.Pres1 = m_sPres1
                        oPRES4.Pres2 = m_sPres2
                        oPRES4.Pres3 = m_sPres3
                        oPRES4.Cod = Trim(txtCod.Text)
                        'oPRES4.Den = Trim(txtDen.Text)
                        Set oIBaseDatos = oPRES4
                        
                        Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                            For Each oIdioma In g_oIdiomas
                                For i = 0 To sdbgDen.Rows - 1
                                    vbm = sdbgDen.AddItemBookmark(i)
                                    If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                                        If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                                            m_Text = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                        End If
                                        oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                                        Exit For
                                    End If
                                Next i
                            Next
                        Set oPRES4.Denominaciones = oDenominaciones
                        teserror = oIBaseDatos.FinalizarEdicionModificando
                        If teserror.NumError = TESnoerror Then
                            ModificarPRESEnEstructura
                        Else
                            TratarError teserror
                            Set oPRES4 = Nothing
                            Set oIBaseDatos = Nothing
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                        
                        Set oPRES4 = Nothing
                        Unload Me
                    
                 End Select

    End Select
    Set oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

''' <summary>
''' Evento que se produce al activarse el formulario
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Activate()
Select Case m_sAccion
        Case "Anyadir":
            If Me.Visible Then txtCod.SetFocus
            
        Case "Modificar":
            If Me.Visible Then sdbgDen.SetFocus
    End Select
End Sub

''' <summary>
''' Carga del formulario
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Load()
    CargarRecursos
    PonerFieldSeparator Me
    Select Case m_sAccion
        Case "Anyadir":
            Me.caption = m_sAnyadir & " " & m_sTitulo
            
        Case "Modificar":
            Me.caption = m_sModificar & " " & m_sTitulo
            txtCod.Text = m_sCod
            'txtDen.Text = m_sDen
            Me.picEdit.Visible = True
            txtCod.Enabled = False
        Case "Detalle":
            Me.caption = m_sDetalle & " " & m_sTitulo
            txtCod.Text = m_sCod
            'txtDen.Text = m_sDen
            Me.picDatos.Enabled = False
            Me.picEdit.Visible = False
            Me.sdbgDen.Enabled = False
            'Me.Height = Me.Height - 470
    End Select
    
    If m_iNivel = 1 Then
        txtCod.MaxLength = gLongitudesDeCodigos.giLongCodPres5Niv1
    ElseIf m_iNivel = 2 Then
        txtCod.MaxLength = gLongitudesDeCodigos.giLongCodPres5Niv2
    ElseIf m_iNivel = 3 Then
        txtCod.MaxLength = gLongitudesDeCodigos.giLongCodPres5Niv3
    Else
        txtCod.MaxLength = gLongitudesDeCodigos.giLongCodPres5Niv4
    End If
    
    CargarGridDenIdiomas
    sdbgDen.Height = (sdbgDen.Rows) * sdbgDen.RowHeight + 10
    
    Select Case m_sAccion
        Case "Anyadir", "Modificar"
            Me.picEdit.Top = sdbgDen.Top + sdbgDen.Height + 100
            Me.Height = sdbgDen.Height + Me.picDatos.Height + Me.picEdit.Height + 800
        Case "Detalle"
            Me.Height = sdbgDen.Height + Me.picDatos.Height + 800
    End Select
    
    
    
End Sub

''' <summary>
''' Carga de las grids de denominacion
''' </summary>
''' <remarks>Llamada desde FORM_Load; Tiempo m�ximo < 1 seg.</remarks>

Private Sub CargarGridDenIdiomas()
Dim oIdioma As CIdioma
Dim rs As ADODB.Recordset
Dim sCadena As String

    Select Case m_sAccion
        Case "Anyadir":
    
            'Carga las denominaciones del grupo de datos generales en todos los idiomas:
            For Each oIdioma In g_oIdiomas
                sdbgDen.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
            Next
            sdbgDen.MoveFirst
            
        Case "Modificar", "Detalle"
        
            If m_iNivel = 1 Then
                Dim oPRES1 As CPresConcep5Nivel1
                Set oPRES1 = oFSGSRaiz.Generar_CPresConcep5Nivel1
                oPRES1.Pres0 = m_sPres0
                oPRES1.Cod = m_sCod
                
                Set rs = oPRES1.DevolverDenominaciones
                
            ElseIf m_iNivel = 2 Then
                Dim oPRES2 As CPresConcep5Nivel2
                Set oPRES2 = oFSGSRaiz.Generar_CPresConcep5Nivel2
                oPRES2.Pres0 = m_sPres0
                oPRES2.Pres1 = m_sPres1
                oPRES2.Cod = m_sCod
                
                Set rs = oPRES2.DevolverDenominaciones
            
                txtCod.MaxLength = gLongitudesDeCodigos.giLongCodPres5Niv2
            ElseIf m_iNivel = 3 Then
                Dim oPRES3 As CPresConcep5Nivel3
                Set oPRES3 = oFSGSRaiz.Generar_CPresConcep5Nivel3
                oPRES3.Pres0 = m_sPres0
                oPRES3.Pres1 = m_sPres1
                oPRES3.Pres2 = m_sPres2
                oPRES3.Cod = m_sCod
                
                Set rs = oPRES3.DevolverDenominaciones
                txtCod.MaxLength = gLongitudesDeCodigos.giLongCodPres5Niv3
            Else
                Dim oPRES4 As CPresConcep5Nivel4
                Set oPRES4 = oFSGSRaiz.Generar_CPresConcep5Nivel4
                oPRES4.Pres0 = m_sPres0
                oPRES4.Pres1 = m_sPres1
                oPRES4.Pres2 = m_sPres2
                oPRES4.Pres3 = m_sPres3
                oPRES4.Cod = m_sCod
           
                Set rs = oPRES4.DevolverDenominaciones
                txtCod.MaxLength = gLongitudesDeCodigos.giLongCodPres5Niv4
            End If

            
            If rs Is Nothing Then
                Exit Sub
            End If
            
            For Each oIdioma In g_oIdiomas
                sCadena = ""
                While Not rs.EOF
                    If rs("IDIOMA").Value = oIdioma.Cod Then
                        sCadena = NullToStr(rs("DEN").Value)
                    End If
                    rs.MoveNext
                Wend
                rs.MoveFirst
                sdbgDen.AddItem oIdioma.Den & Chr(m_lSeparador) & sCadena & Chr(m_lSeparador) & oIdioma.Cod
            Next
            sdbgDen.MoveFirst
            rs.Close
            Set rs = Nothing
            
        End Select
        
End Sub


''' <summary>
''' Carga las cadenas de idiomas de la pantalla
''' </summary>
''' <remarks>Llamada desde Form_Load; Tiempo m�ximo < 0,1 seg.</remarks>

Private Sub CargarRecursos()
Dim rs As New ADODB.Recordset

    Set rs = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESCon5Detalle, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not rs Is Nothing Then

        lblCodigo.caption = rs(0).Value & ":" '1 C�digo
        rs.MoveNext
        lblDenominacion.caption = rs(0).Value & ":" '2 Denominaci�n
        rs.MoveNext
        cmdAceptar.caption = rs(0).Value '3 Aceptar
        rs.MoveNext
        cmdCancelar.caption = rs(0).Value ' 4 Cancenlar
        rs.MoveNext
        m_sAnyadir = rs(0).Value & ":"  '5 A�adir
        rs.MoveNext
        m_sModificar = rs(0).Value & ":" '6 Modificar
        rs.MoveNext
        m_sDetalle = rs(0).Value & ":"         '7 Detalle

    End If
        
    rs.Close
    
    Set g_oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
End Sub


    
''' <summary>
''' Descarga del formulario
''' </summary>
''' <param name="Cancel">Indica si se para la descarga del formulario</param>
''' <remarks>Llamada desde cmdaceptar_Click y cmdCancelar_click; Tiempo m�ximo < 1 seg.</remarks>

Private Sub Form_Unload(Cancel As Integer)
    m_sAccion = ""
    m_sTitulo = ""
    m_sPres0 = ""
    m_sPres1 = ""
    m_sPres2 = ""
    m_sPres3 = ""
    Set g_oIdiomas = Nothing
    m_Text = ""
End Sub

''' <summary>
''' A�ade un nuevo nodo en el arbol
''' </summary>
''' <remarks>Llamada desde Click del boton Aceptar; Tiempo m�ximo < 1 seg.</remarks>

Public Function AnyadirPRESAEstructura()
Dim nodx, nodo As MSComctlLib.node
'Dim nodo As MSComctlLib.node
Dim scod1, scod2, scod3, scod4 As String

Set nodx = frmPresupuestos5.tvwestrPres.selectedItem

Select Case Left(nodx.Tag, 5)
    Case "Raiz "
        scod1 = Trim(txtCod.Text) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(Trim(txtCod.Text)))
        'Set nodo = frmPresupuestos5.tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, Trim(txtCod.Text) & " - " & Trim(txtDen.Text), "PRES1")
        Set nodx = frmPresupuestos5.tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, Trim(txtCod.Text) & " - " & Trim(m_Text), "PRES1")
                 
        'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
        frmPresupuestos5.m_oPresupuestos.Add m_sPres0, Trim(txtCod.Text), Trim(m_Text)
        
        If frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2 Is Nothing Then
            Set frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2 = oFSGSRaiz.Generar_CPresConceptos5Nivel2
        End If

        nodx.Tag = "PRES1" & Trim(txtCod)
    
    Case "PRES1"
    
        Set nodx = frmPresupuestos5.tvwestrPres.selectedItem
        scod1 = frmPresupuestos5.DevolverCod(nodx)
        scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
        scod2 = Trim(txtCod.Text)
        scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
        Set nodo = frmPresupuestos5.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES2" & scod1 & scod2, Trim(txtCod) & " - " & Trim(m_Text), "PRES2")
        nodo.Tag = "PRES2" & Trim(txtCod.Text)
        
        'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
        If frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2 Is Nothing Then
            Set frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2 = oFSGSRaiz.Generar_CPresConceptos5Nivel2
        End If
        
        frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Add m_sPres0, scod1, Trim(txtCod.Text), Trim(m_Text)
        
        If frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3 Is Nothing Then
            Set frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3 = oFSGSRaiz.Generar_CPresConceptos5Nivel3
        End If
     
    Case "PRES2"
        Set nodx = frmPresupuestos5.tvwestrPres.selectedItem
        scod1 = frmPresupuestos5.DevolverCod(nodx.Parent)
        scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
        scod2 = frmPresupuestos5.DevolverCod(nodx)
        scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
        scod3 = Trim(txtCod)
        scod3 = scod3 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(scod3))
        
        'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
        If frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3 Is Nothing Then
            Set frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3 = oFSGSRaiz.Generar_CPresConceptos5Nivel3
        End If
        
        frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Add m_sPres0, scod1, scod2, Trim(txtCod), Trim(m_Text)
        
        If frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4 Is Nothing Then
            Set frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4 = oFSGSRaiz.Generar_CPresConceptos5Nivel4
        End If
        
        Set nodo = frmPresupuestos5.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES3" & scod1 & scod2 & scod3, Trim(txtCod) & " - " & Trim(m_Text), "PRES3")
        
        nodo.Tag = "PRES3" & Trim(txtCod)
        
    Case "PRES3"
        Set nodx = frmPresupuestos5.tvwestrPres.selectedItem
        scod1 = frmPresupuestos5.DevolverCod(nodx.Parent.Parent)
        scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
        scod2 = frmPresupuestos5.DevolverCod(nodx.Parent)
        scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
        scod3 = frmPresupuestos5.DevolverCod(nodx)
        scod3 = scod3 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(scod3))
        scod4 = Trim(txtCod.Text)
        scod4 = scod4 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(scod4))
        
        'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
        If frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4 Is Nothing Then
            Set frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4 = oFSGSRaiz.Generar_CPresConceptos5Nivel4
        End If
        
        frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4.Add m_sPres0, scod1, scod2, scod3, Trim(txtCod.Text), Trim(m_Text)
        
        Set nodo = frmPresupuestos5.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, Trim(txtCod.Text) & " - " & Trim(m_Text), "PRES4")
        nodo.Tag = "PRES4" & Trim(txtCod.Text)
    
End Select

nodx.Selected = True
nodx.EnsureVisible
'frmPresupuestos5.tvwEstrPres_NodeClick nodx
'Set nodo = Nothing
Set nodx = Nothing

End Function

''' <summary>
''' Modificacion de un nodo en el arbol
''' </summary>
''' <remarks>Llamada desde Click del boton Aceptar; Tiempo m�ximo < 1 seg.</remarks>

Public Function ModificarPRESEnEstructura()

Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Set nodx = frmPresupuestos5.tvwestrPres.selectedItem
nodx.Text = Trim(txtCod.Text) & " - " & Trim(m_Text)

Select Case Left(nodx.Tag, 5)
    
    Case "PRES1"
            
            scod1 = txtCod.Text
            scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
            frmPresupuestos5.m_oPresupuestos.Item(scod1).Den = Trim(m_Text)
    Case "PRES2"
            scod1 = Me.m_sPres1
            scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
            scod2 = txtCod.Text
            scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))

            frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).Den = Trim(m_Text)
            
    Case "PRES3"
            scod1 = m_sPres1
            scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
            scod2 = m_sPres2
            scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
            scod3 = txtCod.Text
            scod3 = scod3 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(scod3))

            frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).Den = Trim(m_Text)
            
    Case "PRES4"
            
            scod1 = m_sPres1
            scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
            scod2 = m_sPres2
            scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
            scod3 = m_sPres3
            scod3 = scod3 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(scod3))
            scod4 = txtCod.Text
            scod4 = scod4 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(scod4))
                  
            frmPresupuestos5.m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4.Item(scod1 & scod2 & scod3 & scod4).Den = Trim(m_Text)
            
End Select

Set nodx = Nothing

End Function

