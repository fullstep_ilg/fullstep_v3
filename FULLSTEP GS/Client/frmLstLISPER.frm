VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstLISPER 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de listados personalizados"
   ClientHeight    =   2070
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4875
   Icon            =   "frmLstLISPER.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2070
   ScaleWidth      =   4875
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   4875
      TabIndex        =   0
      Top             =   1695
      Width           =   4875
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   1
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1650
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   2910
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Orden"
      TabPicture(0)   =   "frmLstLISPER.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   1005
         Left            =   120
         TabIndex        =   3
         Top             =   420
         Width           =   4575
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominación"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2445
            TabIndex        =   5
            Top             =   450
            Width           =   1515
         End
         Begin VB.OptionButton opOrdRPT 
            Caption         =   "Archivo ""rpt"""
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   630
            TabIndex        =   4
            Top             =   450
            Value           =   -1  'True
            Width           =   1515
         End
      End
   End
End
Attribute VB_Name = "frmLstLISPER"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private txtRPT As String
Private txtPag As String
Private txtDe As String
Private txtDen As String
Private sTit As String


Private Sub cmdObtener_Click()
    Dim oReport As CRAXDRT.Report
    Dim oCRParametros As CRParametros
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim Textos() As String
    Dim i As Integer
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRParametros = GenerarCRParametros
    
    ReDim Textos(1 To 2, 1 To 2)
    Textos(1, 1) = txtRPT: Textos(2, 1) = "txtRPT"
    Textos(1, 2) = txtDen: Textos(2, 2) = "txtDen"
            
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\" & "rptLISPER.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sTit & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
    For i = 1 To UBound(Textos, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, Textos(2, i))).Text = """" & Textos(1, i) & """"
    Next i
    
    oCRParametros.ListadoLISPERMant oReport, opOrdDen
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTit
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Unload Me

    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Load()
    Me.Height = 2460
    Me.Width = 4965
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LST_LISPER, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        Me.opOrdRPT.caption = Ador(0).Value
        txtRPT = Ador(0).Value
        Ador.MoveNext
    
        Me.opOrdDen.caption = Ador(0).Value
        txtDen = Ador(0).Value
        Ador.MoveNext
        
        Me.caption = Ador(0).Value
        sTit = Ador(0).Value
        Ador.MoveNext
        
        cmdObtener.caption = Ador(0).Value
        
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub
