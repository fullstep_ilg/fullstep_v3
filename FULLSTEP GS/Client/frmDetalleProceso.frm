VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmDetalleProceso 
   BackColor       =   &H00808000&
   Caption         =   "DDetalle de proceso"
   ClientHeight    =   4740
   ClientLeft      =   2085
   ClientTop       =   5010
   ClientWidth     =   7485
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetalleProceso.frx":0000
   LinkTopic       =   "Form3"
   ScaleHeight     =   4740
   ScaleWidth      =   7485
   Begin VB.PictureBox picProce 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Left            =   0
      ScaleHeight     =   4335
      ScaleWidth      =   7515
      TabIndex        =   17
      Top             =   375
      Width           =   7515
      Begin VB.ListBox lstMat 
         BackColor       =   &H80000018&
         Height          =   645
         Left            =   1680
         TabIndex        =   50
         Top             =   120
         Width           =   4890
      End
      Begin ComctlLib.ListView ListView1 
         Height          =   60
         Left            =   5385
         TabIndex        =   49
         Top             =   525
         Width           =   60
         _ExtentX        =   106
         _ExtentY        =   106
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.PictureBox picAdj 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   200
         Left            =   4560
         ScaleHeight     =   195
         ScaleWidth      =   2700
         TabIndex        =   45
         Top             =   1920
         Width           =   2700
         Begin VB.CheckBox chkPermAdjDirecta 
            BackColor       =   &H00808000&
            Caption         =   "Permitir adjudicaci�n directa"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   0
            TabIndex        =   46
            Top             =   0
            Width           =   2655
         End
      End
      Begin VB.TextBox txtIni 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   42
         Top             =   2385
         Width           =   1110
      End
      Begin VB.TextBox txtFin 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   41
         Top             =   2385
         Width           =   1110
      End
      Begin VB.TextBox txtFecPres 
         Height          =   285
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   40
         Top             =   1365
         Width           =   1110
      End
      Begin VB.TextBox txtFecApe 
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   39
         Top             =   1410
         Width           =   1110
      End
      Begin VB.TextBox txtFecNec 
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   38
         Top             =   975
         Width           =   1110
      End
      Begin VB.TextBox txtFecLimitOfer 
         Height          =   285
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   37
         Top             =   945
         Width           =   1110
      End
      Begin VB.TextBox txtSol 
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   1845
         Width           =   2415
      End
      Begin VB.CommandButton cmdResponsable 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6600
         Picture         =   "frmDetalleProceso.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   555
         Width           =   315
      End
      Begin VB.Label lblSolicitud 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   48
         Top             =   4020
         Width           =   4890
      End
      Begin VB.Label lblSolicit 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DReferencia:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   47
         Top             =   4065
         Width           =   945
      End
      Begin VB.Label lblFecIni 
         BackStyle       =   0  'Transparent
         Caption         =   "DInicio suministro"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   90
         TabIndex        =   34
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label lblFecFin 
         BackStyle       =   0  'Transparent
         Caption         =   "DFin suministro"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   3105
         TabIndex        =   33
         Top             =   2430
         Width           =   1455
      End
      Begin VB.Label lblProve 
         BackStyle       =   0  'Transparent
         Caption         =   "DProveedor"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   90
         TabIndex        =   32
         Top             =   3645
         Width           =   1530
      End
      Begin VB.Label lblPag 
         BackStyle       =   0  'Transparent
         Caption         =   "DForma de pago"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   90
         TabIndex        =   31
         Top             =   3195
         Width           =   1455
      End
      Begin VB.Label lblDest 
         BackStyle       =   0  'Transparent
         Caption         =   "DDestino"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   90
         TabIndex        =   30
         Top             =   2790
         Width           =   1395
      End
      Begin VB.Label lblMaterial 
         BackColor       =   &H00808000&
         Caption         =   "DMaterial"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   90
         TabIndex        =   29
         Top             =   180
         Width           =   1485
      End
      Begin VB.Label lblMat 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   28
         Top             =   120
         Width           =   4890
      End
      Begin VB.Label lblFecPres 
         BackStyle       =   0  'Transparent
         Caption         =   "DPresentaci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   3105
         TabIndex        =   27
         Top             =   1380
         Width           =   1545
      End
      Begin VB.Label lblFecNec 
         BackStyle       =   0  'Transparent
         Caption         =   "DNecesidad"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   90
         TabIndex        =   26
         Top             =   1005
         Width           =   1275
      End
      Begin VB.Label lblFecAper 
         BackStyle       =   0  'Transparent
         Caption         =   "DApertura"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   90
         TabIndex        =   25
         Top             =   1425
         Width           =   1305
      End
      Begin VB.Label lblFecLimit 
         BackColor       =   &H00808000&
         Caption         =   "DL�mite de ofertas"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   3105
         TabIndex        =   24
         Top             =   960
         Width           =   1485
      End
      Begin VB.Label lblSolCompra 
         BackStyle       =   0  'Transparent
         Caption         =   "DSolicitud de compra"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   90
         TabIndex        =   23
         Top             =   1860
         Width           =   1560
      End
      Begin VB.Label lblDestino 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   22
         Top             =   2760
         Width           =   4890
      End
      Begin VB.Label lblFormaPago 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   21
         Top             =   3180
         Width           =   4890
      End
      Begin VB.Label lblProveedor 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   20
         Top             =   3600
         Width           =   4890
      End
      Begin VB.Label lblResponsable 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   19
         Top             =   555
         Width           =   4890
      End
      Begin VB.Label lblResp 
         BackColor       =   &H00808000&
         Caption         =   "DResponsable"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   90
         TabIndex        =   18
         Top             =   585
         Width           =   1485
      End
   End
   Begin VB.PictureBox picPer 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4035
      Left            =   0
      ScaleHeight     =   4035
      ScaleWidth      =   7395
      TabIndex        =   5
      Top             =   360
      Width           =   7395
      Begin SSDataWidgets_B.SSDBGrid sdbgPersonas 
         Height          =   3675
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   300
         Width           =   7245
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         Col.Count       =   11
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmDetalleProceso.frx":0DC2
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         BalloonHelp     =   0   'False
         MaxSelectedRows =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   79
         SplitterPos     =   2
         SplitterVisible =   -1  'True
         Columns.Count   =   11
         Columns(0).Width=   1191
         Columns(0).Caption=   "DRol"
         Columns(0).Name =   "REL"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1508
         Columns(1).Caption=   "DCodigo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   3
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   2302
         Columns(2).Caption=   "DApellidos"
         Columns(2).Name =   "APE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1349
         Columns(3).Caption=   "DNombre"
         Columns(3).Name =   "DEN"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   50
         Columns(4).Width=   2487
         Columns(4).Caption=   "DUnidad organizativa"
         Columns(4).Name =   "UON"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1799
         Columns(5).Caption=   "DDepartamento"
         Columns(5).Name =   "DEP"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1535
         Columns(6).Caption=   "DCargo"
         Columns(6).Name =   "CAR"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   1429
         Columns(7).Caption=   "DTelefono"
         Columns(7).Name =   "TFNO"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   1482
         Columns(8).Caption=   "DTelefono2"
         Columns(8).Name =   "TFNO2"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   1270
         Columns(9).Caption=   "DFax"
         Columns(9).Name =   "FAX"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   2990
         Columns(10).Caption=   "DMail"
         Columns(10).Name=   "MAIL"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         _ExtentX        =   12779
         _ExtentY        =   6482
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblPerAsig 
         BackColor       =   &H00808000&
         Caption         =   "DPersonas asignadas"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   135
         TabIndex        =   6
         Top             =   0
         Width           =   2235
      End
   End
   Begin VB.CommandButton cmdDatGen 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   0
      Picture         =   "frmDetalleProceso.frx":0DDE
      Style           =   1  'Graphical
      TabIndex        =   43
      ToolTipText     =   "Pulse para ver las personas asignadas al proceso"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdAtributos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1080
      Picture         =   "frmDetalleProceso.frx":1368
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Pulse para ver los atributos del proceso"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdEsp 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   720
      Picture         =   "frmDetalleProceso.frx":14D9
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Pulse para ver las especificaciones del  proceso"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdPer 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   360
      Picture         =   "frmDetalleProceso.frx":183A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Pulse para ver las personas asignadas al proceso"
      Top             =   0
      Width           =   375
   End
   Begin VB.PictureBox picAtributos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4035
      Left            =   0
      ScaleHeight     =   4035
      ScaleWidth      =   7395
      TabIndex        =   15
      Top             =   360
      Width           =   7395
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
         Height          =   3555
         Left            =   135
         TabIndex        =   44
         TabStop         =   0   'False
         Top             =   300
         Width           =   7215
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         stylesets.count =   2
         stylesets(0).Name=   "Head"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmDetalleProceso.frx":18A4
         stylesets(1).Name=   "Verde"
         stylesets(1).BackColor=   32896
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmDetalleProceso.frx":18C0
         UseGroups       =   -1  'True
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         SelectByCell    =   -1  'True
         CellNavigation  =   1
         MaxSelectedRows =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   26
         SplitterVisible =   -1  'True
         Groups(0).Width =   5106
         Groups(0).Caption=   "DESCR"
         Groups(0).HeadStyleSet=   "Head"
         Groups(0).Columns.Count=   3
         Groups(0).Columns(0).Width=   1349
         Groups(0).Columns(0).Caption=   "COD"
         Groups(0).Columns(0).Name=   "COD"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Locked=   -1  'True
         Groups(0).Columns(0).StyleSet=   "Verde"
         Groups(0).Columns(1).Width=   3757
         Groups(0).Columns(1).Caption=   "DEN"
         Groups(0).Columns(1).Name=   "DEN"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(1).Locked=   -1  'True
         Groups(0).Columns(1).Style=   1
         Groups(0).Columns(1).ButtonsAlways=   -1  'True
         Groups(0).Columns(1).StyleSet=   "Verde"
         Groups(0).Columns(2).Width=   1561
         Groups(0).Columns(2).Visible=   0   'False
         Groups(0).Columns(2).Caption=   "ID"
         Groups(0).Columns(2).Name=   "ID"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         _ExtentX        =   12726
         _ExtentY        =   6271
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAtributos 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DAtributos"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   135
         TabIndex        =   16
         Top             =   0
         Width           =   765
      End
   End
   Begin VB.PictureBox picEsp 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4035
      Left            =   0
      ScaleHeight     =   4035
      ScaleWidth      =   7395
      TabIndex        =   8
      Top             =   360
      Width           =   7395
      Begin VB.CommandButton cmdSalvarEspGen 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   6390
         Picture         =   "frmDetalleProceso.frx":18DC
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   3720
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdAbrirEspGen 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   6870
         Picture         =   "frmDetalleProceso.frx":1C1E
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   3720
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.TextBox txtProceEsp 
         BackColor       =   &H00FFFFFF&
         Height          =   1635
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Top             =   240
         Width           =   7155
      End
      Begin MSComctlLib.ListView lstvwEsp 
         Height          =   1395
         Left            =   135
         TabIndex        =   13
         Top             =   2220
         Width           =   7155
         _ExtentX        =   12621
         _ExtentY        =   2461
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Comentario"
            Object.Width           =   6588
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2867
         EndProperty
      End
      Begin MSComDlg.CommonDialog cmmdEsp 
         Left            =   2880
         Top             =   1935
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label lblArchivosAdj 
         BackColor       =   &H00808000&
         Caption         =   "DArchivos adjuntos"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   14
         Top             =   1980
         Width           =   2235
      End
      Begin VB.Label lblEspecificaciones 
         BackColor       =   &H00808000&
         Caption         =   "DEspecificaciones"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   9
         Top             =   0
         Width           =   2235
      End
   End
   Begin VB.Label lblDenEstado 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0E0FF&
      Caption         =   "(Den. Estado)"
      Height          =   225
      Left            =   6435
      TabIndex        =   1
      Top             =   60
      UseMnemonic     =   0   'False
      Width           =   1005
   End
   Begin VB.Label lblProceso 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "    Denominacion del proceso                                          "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   0
      Top             =   0
      Width           =   6105
   End
End
Attribute VB_Name = "frmDetalleProceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public m_oProcesoSeleccionado As CProceso
Public g_oProveedores As CProveedores
Public g_sFormatoNumber As String

'Variables privadas
Private m_oPersonasAsignadas As CPersonas
Private m_aSayFileNames() As String
Private m_sIdiElArchivo As String
Private m_sIdiTipoOrig As String
Private m_sIdiGuardar As String
Private m_sProceso As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_skb As String

Private m_sEstado(15) As String

Private m_bMostrarSolicit As Boolean

'form:control de errores
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean

Private m_sMsgError As String
Private Sub CargarRecursos()
    Dim Adores As Ador.Recordset



'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_DETALLE_PROCESO, basPublic.gParametrosInstalacion.gIdioma)

    If Not Adores Is Nothing Then
        frmDetalleProceso.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblDenEstado.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblMaterial.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblResp.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblFecNec.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblFecAper.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblFecLimit.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblFecPres.caption = Adores.Fields.Item(0).Value & " :"
        
        lblSolCompra.caption = gParametrosGenerales.gsDenSolicitudCompra & ":"
        
        Adores.MoveNext
        lblFecIni.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblFecFin.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblDest.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblPag.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblProve.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblPerAsig.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblAtributos.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblEspecificaciones.caption = Adores.Fields.Item(0).Value & " :"
        Adores.MoveNext
        lblArchivosAdj.caption = Adores.Fields.Item(0).Value & " :"
        With sdbgPersonas
            Adores.MoveNext
            .Columns.Item(0).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(1).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(2).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(3).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(4).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(5).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(6).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(7).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(8).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(9).caption = Adores.Fields.Item(0).Value
            Adores.MoveNext
            .Columns.Item(10).caption = Adores.Fields.Item(0).Value
        End With
        Adores.MoveNext
        sdbgAtributos.Groups.Item(0).caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sIdiElArchivo = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sIdiTipoOrig = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sIdiGuardar = Adores.Fields.Item(0).Value
        Adores.MoveNext
        
        'Estados del proceso
        Adores.MoveNext
        m_sEstado(1) = Adores.Fields.Item(0).Value '35
        Adores.MoveNext
        m_sEstado(2) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(3) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(4) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(5) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(6) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(7) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(8) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(9) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(10) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(11) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(12) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(13) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(14) = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sEstado(15) = Adores.Fields.Item(0).Value
        
        Adores.MoveNext
        m_sProceso = Adores.Fields.Item(0).Value
        
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(1).Text = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(3).Text = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(4).Text = Adores.Fields.Item(0).Value
        
        Adores.MoveNext
        m_sIdiTrue = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sIdiFalse = Adores.Fields.Item(0).Value
        
        Adores.MoveNext
        cmdAtributos.ToolTipText = Adores.Fields.Item(0).Value
        Adores.MoveNext
        cmdEsp.ToolTipText = Adores.Fields.Item(0).Value
        Adores.MoveNext
        cmdPer.ToolTipText = Adores.Fields.Item(0).Value
        Adores.MoveNext
        cmdDatGen.ToolTipText = Adores.Fields.Item(0).Value
        Adores.MoveNext
        chkPermAdjDirecta.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblSolicit.caption = Adores.Fields.Item(0).Value
        
        Adores.MoveNext
        cmdResponsable.ToolTipText = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(2).Text = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_skb = Adores.Fields.Item(0).Value
        
        Adores.Close
        
    End If
    
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub

Private Sub cmdAbrirEspGen_Click()
    Dim Item As MSComctlLib.listItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
        
    On Error GoTo Cancelar:
    
    Set Item = lstvwEsp.selectedItem
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    Else
        Set oEsp = m_oProcesoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & oEsp.nombre
        sFileTitle = Item.Text
    End If
    
    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing
    
    oEsp.ComenzarLecturaData (TipoEspecificacion.EspProceso)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oEsp = Nothing
        Exit Sub
    End If
    
    oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspProceso, oEsp.DataSize, sFileName
       
    'Lanzamos la aplicacion
    ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Set oEsp = Nothing
End Sub

Private Sub cmdAtributos_Click()
    Dim oAtributo As CAtributo
    Dim oOferta As COferta
    Dim sLinea As String
    Dim i As Integer
    Dim oProv As CProveedor
    
    'A�ade los grupos de proveedores a la grid
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAtributos.Groups.Count = 1 Then
         i = 1
         For Each oProv In g_oProveedores
             sdbgAtributos.Groups.Add i
             sdbgAtributos.Groups(i).caption = oProv.Cod & " - " & oProv.Den
             sdbgAtributos.Groups(i).Columns.Add 0
             sdbgAtributos.Groups(i).Columns(0).Name = oProv.Cod
             sdbgAtributos.Groups(i).Columns(0).Locked = True
             sdbgAtributos.Groups(i).HeadStyleSet = "Head"
             i = i + 1
        Next
    End If
    
    'Ahora rellena la grid
    sdbgAtributos.RemoveAll
    
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        For Each oAtributo In m_oProcesoSeleccionado.ATRIBUTOS
            sLinea = oAtributo.Cod & Chr(m_lSeparador) & oAtributo.Den & Chr(m_lSeparador) & oAtributo.idAtribProce
            For Each oProv In g_oProveedores
                Set oOferta = m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod)
                If Not oOferta Is Nothing Then
                    If Not oOferta.AtribProcOfertados Is Nothing Then
                        If Not oOferta.AtribProcOfertados.Item(CStr(oAtributo.idAtribProce)) Is Nothing Then
                            Select Case oAtributo.Tipo
                                Case TiposDeAtributos.TipoBoolean
                                    If oOferta.AtribProcOfertados.Item(CStr(oAtributo.idAtribProce)).valorBool = 1 Then
                                        sLinea = sLinea & Chr(m_lSeparador) & m_sIdiTrue
                                    Else
                                        sLinea = sLinea & Chr(m_lSeparador) & m_sIdiFalse
                                    End If
                                    
                                Case TiposDeAtributos.TipoFecha
                                    sLinea = sLinea & Chr(m_lSeparador) & oOferta.AtribProcOfertados.Item(CStr(oAtributo.idAtribProce)).valorFec
                                Case TiposDeAtributos.TipoNumerico
                                    sLinea = sLinea & Chr(m_lSeparador) & Format(oOferta.AtribProcOfertados.Item(CStr(oAtributo.idAtribProce)).valorNum, g_sFormatoNumber)
                                Case TiposDeAtributos.TipoString
                                    sLinea = sLinea & Chr(m_lSeparador) & EliminarTab(NullToStr(oOferta.AtribProcOfertados.Item(CStr(oAtributo.idAtribProce)).valorText))
                            End Select
                        Else
                            sLinea = sLinea & Chr(m_lSeparador) & ""
                        End If
                    Else
                        sLinea = sLinea & Chr(m_lSeparador) & ""
                    End If
                Else
                    sLinea = sLinea & Chr(m_lSeparador) & ""
                End If
                Set oOferta = Nothing
            Next
            
            'A�ade la l�nea a la grid
            sdbgAtributos.AddItem sLinea
        Next
    End If
    'Muestra el splitter
    sdbgAtributos.SplitterVisible = True
    sdbgAtributos.SplitterPos = 1
    
    'Muestra el picture de atributos y oculta los dem�s
    picAtributos.Visible = True
    picEsp.Visible = False
    picProce.Visible = False
    picPer.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "cmdAtributos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdDatGen_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picEsp.Visible = False
    picAtributos.Visible = False
    picProce.Visible = True
    picPer.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "cmdDatGen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdEsp_Click()
    Dim oEsp As CEspecificacion
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado.especificaciones Is Nothing Then
        m_oProcesoSeleccionado.CargarTodasLasEspecificaciones , True
    End If
    
    'Especificaci�n del proceso:
    txtProceEsp.Text = NullToStr(m_oProcesoSeleccionado.esp)
    
    'Limpia la lista
    lstvwEsp.ListItems.clear
    'A�ade las especificaciones a la lista
    For Each oEsp In m_oProcesoSeleccionado.especificaciones
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.indice), oEsp.nombre
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).Tag = CStr(oEsp.indice)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).ListSubItems.Add , "ESP" & CStr(oEsp.indice), NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).ListSubItems.Add , , oEsp.Fecha
    Next
    
    'Muestra el picture de especificaciones y oculta los dem�s
    picEsp.Visible = True
    picAtributos.Visible = False
    picProce.Visible = False
    picPer.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "cmdEsp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdPer_Click()
    Dim oIPersonasAsig As IPersonasAsig
    Dim oPersona As CPersona
    Dim sUON As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oPersonasAsignadas Is Nothing Then
        Set oIPersonasAsig = m_oProcesoSeleccionado
        Set m_oPersonasAsignadas = oIPersonasAsig.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorCod, True)
        Set oIPersonasAsig = Nothing
    End If
    
    sdbgPersonas.RemoveAll
    For Each oPersona In m_oPersonasAsignadas
        If Not IsNull(oPersona.UON3) Then
            sUON = oPersona.UON1 & " - " & oPersona.UON2 & " - " & oPersona.UON3
        ElseIf Not IsNull(oPersona.UON2) Then
            sUON = oPersona.UON1 & " - " & oPersona.UON2
        ElseIf Not IsNull(oPersona.UON1) Then
            sUON = oPersona.UON1
        Else
            sUON = " "
        End If
        
        sdbgPersonas.AddItem oPersona.Rol & Chr(m_lSeparador) & oPersona.Cod & Chr(m_lSeparador) & oPersona.Apellidos & Chr(m_lSeparador) & oPersona.nombre & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & oPersona.CodDep & Chr(m_lSeparador) & oPersona.Cargo & Chr(m_lSeparador) & oPersona.Tfno & Chr(m_lSeparador) & oPersona.Tfno2 & Chr(m_lSeparador) & oPersona.Fax & Chr(m_lSeparador) & oPersona.mail
    Next
    
    'Muestra el picture de las personas y oculta los dem�s
    picPer.Visible = True
    picAtributos.Visible = False
    picEsp.Visible = False
    picProce.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "cmdPer_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdResponsable_Click()
    'Muestra la pantalla de detalle de proceso:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado.responsable Is Nothing Then Exit Sub
    If m_oProcesoSeleccionado.responsable.Cod = "" Then Exit Sub
    MostrarFormDetPersona oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, MDI.ScaleWidth, MDI.ScaleHeight, basParametros.gLongitudesDeCodigos.giLongCodPER, _
                          m_oProcesoSeleccionado.responsable.Cod, "frmDetalleProceso", FSEPConf
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "cmdResponsable_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdSalvarEspGen_Click()
    Dim Item As MSComctlLib.listItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
       
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo ERROR_Frm:
    

    Set Item = lstvwEsp.selectedItem

    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        Set oEsp = m_oProcesoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        cmmdEsp.DialogTitle = m_sIdiGuardar
        cmmdEsp.CancelError = True
        cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
        cmmdEsp.filename = oEsp.nombre
        cmmdEsp.ShowSave

        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdiElArchivo
            Exit Sub
        End If

        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProceso)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Exit Sub
        End If

        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspProceso, oEsp.DataSize, sFileName
    End If

ERROR_Frm:
    On Error Resume Next

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "cmdSalvarEspGen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      GoTo ERROR_Frm
      Exit Sub
   End If
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If Not m_bActivado Then
        m_bActivado = True
    End If
    

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    
    ReDim m_aSayFileNames(0)
    
    'si no se han definido especificaciones a nivel de proceso se ocultan y se dimesionan
    'los botones
    If m_oProcesoSeleccionado.DefEspecificaciones = False Then
        cmdEsp.Visible = False
        lblProceso.Left = cmdAtributos.Left
        lblProceso.Width = lblProceso.Width + cmdEsp.Width
        cmdAtributos.Left = cmdEsp.Left
        lblDenEstado.Left = lblDenEstado.Left - cmdEsp.Width
    End If
    
    'Si hay mas de un material en el proceso se muestra la lista de materiales
    If m_oProcesoSeleccionado.MaterialProce.Count = 1 Then
        lblMat.Visible = True
        lstMat.Visible = False
        Me.Height = 5250
        lblResp.Top = lblMat.Top + lblMat.Height + 150
    Else
        lblMat.Visible = False
        lstMat.Visible = True
        Me.Height = 5500
        lblResp.Top = lstMat.Top + lstMat.Height + 150
    End If
   
    lblResponsable.Top = lblResp.Top
    cmdResponsable.Top = lblResponsable.Top
    lblFecNec.Top = lblResp.Top + lblResp.Height + 150
    txtFecNec.Top = lblFecNec.Top
    lblFecLimit.Top = txtFecNec.Top
    txtFecLimitOfer.Top = lblFecLimit.Top
    lblFecAper.Top = lblFecNec.Top + lblFecNec.Height + 150
    txtFecApe.Top = lblFecAper.Top
    lblFecPres.Top = txtFecApe.Top
    txtFecPres.Top = lblFecPres.Top
    lblSolCompra.Top = lblFecAper.Top + lblFecAper.Height + 150
    txtSol.Top = lblSolCompra.Top
    picAdj.Top = txtFecPres.Top + txtFecPres.Height + 210
    lblFecIni.Top = lblSolCompra.Top + lblSolCompra.Height + 255
    txtIni.Top = lblFecIni.Top
    lblFecFin.Top = lblFecIni.Top
    txtFin.Top = lblFecFin.Top
    lblDest.Top = lblFecIni.Top + lblFecIni.Height + 150
    lblDestino.Top = lblDest.Top
    lblPag.Top = lblDest.Top + lblDest.Height + 150
    lblFormaPago.Top = lblPag.Top
    lblProve.Top = lblPag.Top + lblPag.Height + 150
    lblProveedor.Top = lblProve.Top
    lblSolicit.Top = lblProve.Top + lblProve.Height + 150
    lblSolicitud.Top = lblSolicit.Top
    
    'si no se han definido atributos a nivel de proceso se ocultan y se dimesionan
    'los botones
    If m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        cmdAtributos.Visible = False
        lblProceso.Left = cmdAtributos.Left
        lblProceso.Width = lblProceso.Width + cmdAtributos.Width
        lblDenEstado.Left = lblDenEstado.Left - cmdAtributos.Width
    Else
        If m_oProcesoSeleccionado.ATRIBUTOS.Count = 0 Then
            cmdAtributos.Visible = False
            lblProceso.Left = cmdAtributos.Left
            lblProceso.Width = lblProceso.Width + cmdAtributos.Width
            lblDenEstado.Left = lblDenEstado.Left - cmdAtributos.Width
        End If
    End If
    
    'Comprueba si se visualizar�n o no las solicitudes de compras
    m_bMostrarSolicit = True
    If FSEPConf Then
        m_bMostrarSolicit = False
    Else
        If basParametros.gParametrosGenerales.gbSolicitudesCompras = False Then
            m_bMostrarSolicit = False
        End If
        
        'Si no tiene permisos de consulta de las solicitudes no podr� visualizarlas
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APESolicAsignar)) Is Nothing) Then
            m_bMostrarSolicit = False
        End If
    End If
    
    'Rellena el picture de los datos generales y lo muestra
    RellenarDatosGenerales
    cmdDatGen_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub RellenarDatosGenerales()
'**************************************************************************************
'*** Descripci�n: Este procedimiento rellena los datos generales del proceso        ***
'***              seleccionado en la pantalla de comparativa y adjudicaciones. Se   ***
'***              rellenan los campos correspondientes a la pantalla de datos       ***
'***              generales del proceso, que es la que se ve cuando se carga el     ***
'***              form.                                                             ***
'***                                                                                ***
'*** Par�metros : ------                                                            ***
'***                                                                                ***
'*** Valor que devuelve: ------                                                     ***
'**************************************************************************************
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblProceso.caption = Space(4) & m_oProcesoSeleccionado.Anyo & "/" & m_oProcesoSeleccionado.GMN1Cod & "/" & m_oProcesoSeleccionado.Cod & " - " & m_oProcesoSeleccionado.Den
    Select Case m_oProcesoSeleccionado.Estado
        Case TipoEstadoProceso.sinitems            'Sin validar y sin items
            lblDenEstado.caption = "(" & m_sEstado(1) & ")"
        Case TipoEstadoProceso.ConItemsSinValidar  'Con items y sin validar
            lblDenEstado.caption = m_sEstado(15)
        Case TipoEstadoProceso.validado            'Validado y sin provedores asignados
           lblDenEstado.caption = m_sEstado(14)
        Case TipoEstadoProceso.Conproveasignados   'Validado y con proveedores asignados pero sin validar la asignacion
            lblDenEstado.caption = "(" & m_sEstado(2) & ")"
        Case TipoEstadoProceso.conasignacionvalida 'Con asignacion de proveedores validada
            lblDenEstado.caption = "(" & m_sEstado(3) & ")"
        Case TipoEstadoProceso.conpeticiones
            lblDenEstado.caption = "(" & m_sEstado(4) & ")"
        Case TipoEstadoProceso.conofertas
            lblDenEstado.caption = m_sEstado(13)
        Case TipoEstadoProceso.ConObjetivosSinNotificar
            lblDenEstado.caption = "(" & m_sEstado(5) & ")"
        Case TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
            lblDenEstado.caption = m_sEstado(12)
        Case TipoEstadoProceso.PreadjYConObjNotificados
            lblDenEstado.caption = "(" & m_sEstado(6) & ")"
        Case TipoEstadoProceso.ParcialmenteCerrado
           lblDenEstado.caption = "(" & m_sEstado(11) & ")"
        Case TipoEstadoProceso.conadjudicaciones
            lblDenEstado.caption = "(" & m_sEstado(8) & ")"
        Case TipoEstadoProceso.ConAdjudicacionesNotificadas
            lblDenEstado.caption = "(" & m_sEstado(9) & ")"
        Case TipoEstadoProceso.Cerrado
            lblDenEstado.caption = "(" & m_sEstado(10) & ")"
    End Select
    If m_oProcesoSeleccionado.MaterialProce.Count = 1 Then
        lblMat.caption = m_oProcesoSeleccionado.MaterialProce.Item(1).GMN1Cod & " - " & m_oProcesoSeleccionado.MaterialProce.Item(1).GMN2Cod & " - " & m_oProcesoSeleccionado.MaterialProce.Item(1).GMN3Cod & " - " & m_oProcesoSeleccionado.MaterialProce.Item(1).Cod & " " & m_oProcesoSeleccionado.MaterialProce.Item(1).Den
    Else
        Dim oMat As CGrupoMatNivel4
        For Each oMat In m_oProcesoSeleccionado.MaterialProce
           lstMat.AddItem oMat.GMN1Cod & "-" & oMat.GMN2Cod & "-" & oMat.GMN3Cod & "-" & oMat.Cod & "-" & oMat.Den
        Next
    End If
    
    If Not m_oProcesoSeleccionado.responsable Is Nothing Then
        lblResponsable.caption = NullToStr(m_oProcesoSeleccionado.responsable.Cod)
        If m_oProcesoSeleccionado.responsable.nombre <> "" Or m_oProcesoSeleccionado.responsable.Apel <> "" Then
            lblResponsable.caption = lblResponsable.caption & " - " & NullToStr(m_oProcesoSeleccionado.responsable.nombre) & " " & NullToStr(m_oProcesoSeleccionado.responsable.Apel)
        End If
        If NullToStr(m_oProcesoSeleccionado.responsable.Cod) = "" Then
            cmdResponsable.Enabled = False
        Else
            cmdResponsable.Enabled = True
        End If
    End If
    
    txtFecApe.Text = Format(m_oProcesoSeleccionado.FechaApertura, "Short date")
    txtFecLimitOfer.Text = Format(m_oProcesoSeleccionado.FechaMinimoLimOfertas, "Short date")
    txtFecNec.Text = Format(m_oProcesoSeleccionado.FechaNecesidad, "Short date")
    txtFecPres.Text = Format(m_oProcesoSeleccionado.FechaPresentacion, "Short date")
    txtProceEsp.Text = NullToStr(m_oProcesoSeleccionado.esp)
    txtSol.Text = NullToStr(m_oProcesoSeleccionado.Referencia)
    
    If m_oProcesoSeleccionado.PermitirAdjDirecta Then
        chkPermAdjDirecta.Value = vbChecked
    Else
        chkPermAdjDirecta.Value = vbUnchecked
    End If
    
    'Los siguientes datos se visualizar�n o no dependiendo de si est�nb definidos o no a nivel de proceso:
    If m_oProcesoSeleccionado.DefFechasSum = EnProceso Then
        txtFin.Text = Format(m_oProcesoSeleccionado.FechaFinSuministro, "Short date")
        txtIni.Text = Format(m_oProcesoSeleccionado.FechaInicioSuministro, "Short date")
    Else
        txtFin.Visible = False
        txtIni.Visible = False
        lblFecFin.Visible = False
        lblFecIni.Visible = False
        
        'Redimensiona el resto de controles
        If m_bMostrarSolicit = True Then
            lblSolicitud.Top = lblFormaPago.Top
            lblSolicit.Top = lblPag.Top
            lblProveedor.Top = lblSolicitud.Top
            lblProve.Top = lblSolicit.Top
        Else
            lblProveedor.Top = lblFormaPago.Top
            lblProve.Top = lblPag.Top
        End If
        
        lblFormaPago.Top = lblDestino.Top
        lblPag.Top = lblDest.Top
        lblDestino.Top = txtFin.Top
        lblDest.Top = lblFecFin.Top
    End If
    
    If m_oProcesoSeleccionado.DefDestino = EnProceso Then
        lblDestino.caption = NullToStr(m_oProcesoSeleccionado.DestCod)
        If m_oProcesoSeleccionado.DestDen <> "" And Not IsNull(m_oProcesoSeleccionado.DestDen) Then
            lblDestino.caption = lblDestino.caption & " - " & m_oProcesoSeleccionado.DestDen
        End If
    Else
        lblDestino.Visible = False
        lblDest.Visible = False
        
        'Redimensiona el resto de controles
        If m_bMostrarSolicit = True Then
            lblSolicitud.Top = lblFormaPago.Top
            lblSolicit.Top = lblPag.Top
            lblProveedor.Top = lblSolicitud.Top
            lblProve.Top = lblSolicit.Top
        Else
            lblProveedor.Top = lblFormaPago.Top
            lblProve.Top = lblPag.Top
        End If
        lblFormaPago.Top = lblDestino.Top
        lblPag.Top = lblDest.Top
    End If
    
    If m_oProcesoSeleccionado.DefFormaPago = EnProceso Then
        lblFormaPago.caption = NullToStr(m_oProcesoSeleccionado.PagCod) & " - " & NullToStr(m_oProcesoSeleccionado.PagDen)
    Else
        lblFormaPago.Visible = False
        lblPag.Visible = False
        
        'Redimensiona el resto de controles
        If m_bMostrarSolicit = True Then
            lblSolicitud.Top = lblFormaPago.Top
            lblSolicit.Top = lblPag.Top
            lblProveedor.Top = lblSolicitud.Top
            lblProve.Top = lblSolicit.Top
        Else
            lblProveedor.Top = lblFormaPago.Top
            lblProve.Top = lblPag.Top
        End If
    End If
    
    If m_oProcesoSeleccionado.DefProveActual = EnProceso Then
        lblProveedor.caption = NullToStr(m_oProcesoSeleccionado.ProveActual) & " - " & m_oProcesoSeleccionado.ProveActDen
    Else
        lblProveedor.Visible = False
        lblProve.Visible = False
        lblSolicitud.Top = lblProveedor.Top
        lblSolicit.Top = lblProve.Top
    End If
    
    If m_bMostrarSolicit = True Then
        If m_oProcesoSeleccionado.DefSolicitud = EnProceso Then
            If Not IsNull(m_oProcesoSeleccionado.SolicitudId) Then
                lblSolicitud.caption = NullToStr(m_oProcesoSeleccionado.SolicitudId) & " - " & m_oProcesoSeleccionado.SolicitudDen
            End If
        Else
            lblSolicitud.Visible = False
            lblSolicit.Visible = False
        End If
    Else
        lblSolicitud.Visible = False
        lblSolicit.Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "RellenarDatosGenerales", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Form_Resize()
    
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

    If Me.Height <= 1500 Then Exit Sub
    If Me.Width <= 1700 Then Exit Sub
    
    'Picture para el proceso:
    picProce.Width = Me.Width - 60
    lblProceso.Width = Me.Width - lblProceso.Left
    lblDenEstado.Left = lblProceso.Left + lblProceso.Width - lblDenEstado.Width - 200
    
    'Picture de los adjuntos:
    picEsp.Width = Me.Width - 180
    picEsp.Height = Me.Height - 765
    
    cmdAbrirEspGen.Left = Me.Width - 705
    cmdAbrirEspGen.Top = Me.Height - 1080
    cmdSalvarEspGen.Left = Me.Width - 1185
    cmdSalvarEspGen.Top = Me.Height - 1080
    
    txtProceEsp.Width = Me.Width - 420
    txtProceEsp.Height = Me.Height / 3 + 300
    lblArchivosAdj.Top = txtProceEsp.Height + 250
    lstvwEsp.Top = lblArchivosAdj.Top + 245
    lstvwEsp.Width = Me.Width - 420
    If (cmdAbrirEspGen.Top - lstvwEsp.Top - 90) < 0 Then
        lstvwEsp.Height = (Me.Height / 3) + 120
    Else
        lstvwEsp.Height = cmdAbrirEspGen.Top - lstvwEsp.Top - 90
    End If
    
    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.3
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.15
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.34
    lstvwEsp.ColumnHeaders(4).Width = lstvwEsp.Width * 0.2
    
    'Picture para las personas
    picPer.Height = Me.Height - 765
    picPer.Width = Me.Width - 180
    sdbgPersonas.Height = Me.Height - 1125
    sdbgPersonas.Width = Me.Width - 330
    
    'Picture de los atributos:
    picAtributos.Height = Me.Height - 765
    picAtributos.Width = Me.Width - 180
    sdbgAtributos.Height = Me.Height - 1245
    sdbgAtributos.Width = Me.Width - 360
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oProcesoSeleccionado = Nothing
    Set m_oPersonasAsignadas = Nothing
    Set g_oProveedores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgAtributos_BtnClick()
    'Mostrar el detalle del atributo
    
    Dim sIdAtrib As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sIdAtrib = sdbgAtributos.Columns("ID").Value
    frmDetAtribProce.g_sOrigen = "frmDetalleProceso"
    Set frmDetAtribProce.g_oProceso = m_oProcesoSeleccionado
    
    Set frmDetAtribProce.g_oProveedores = g_oProveedores
    
    'Comprueba si es un atributo de proceso
    If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        If Not m_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib) Is Nothing Then
            Set frmDetAtribProce.g_oAtributo = m_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib)
            frmDetAtribProce.sdbcGrupos.Value = m_sProceso
            frmDetAtribProce.sdbcGrupos.Text = m_sProceso
        End If
    End If
    
    frmDetAtribProce.sstabGeneral.Tab = 0
    frmDetAtribProce.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetalleProceso", "sdbgAtributos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

