VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSOLAbrirFaltan 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DApertura de proceso de compra desde solicitud:"
   ClientHeight    =   9195
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10365
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLAbrirFaltan.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9195
   ScaleWidth      =   10365
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picAtrbEsp 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1815
      Left            =   120
      ScaleHeight     =   1815
      ScaleWidth      =   10095
      TabIndex        =   64
      Top             =   6840
      Width           =   10095
      Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
         Height          =   915
         Left            =   4800
         TabIndex        =   65
         Top             =   1200
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSOLAbrirFaltan.frx":0CB2
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns(0).Width=   3201
         Columns(0).Name =   "NOMBRE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
         Height          =   1470
         Left            =   0
         TabIndex        =   66
         Top             =   360
         Width           =   10095
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   8
         stylesets.count =   3
         stylesets(0).Name=   "Checked"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSOLAbrirFaltan.frx":0CCE
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSOLAbrirFaltan.frx":0F2A
         stylesets(2).Name=   "UnChecked"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSOLAbrirFaltan.frx":0F46
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   8
         Columns(0).Width=   2143
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasHeadForeColor=   -1  'True
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).HeadBackColor=   -2147483644
         Columns(0).BackColor=   16777088
         Columns(1).Width=   5741
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   200
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).HeadBackColor=   -2147483644
         Columns(1).BackColor=   16777088
         Columns(2).Width=   3519
         Columns(2).Caption=   "Valor"
         Columns(2).Name =   "VALOR"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   800
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadBackColor=   -2147483644
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "TIPO"
         Columns(3).Name =   "TIPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "INTRO"
         Columns(4).Name =   "INTRO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "ID_A"
         Columns(5).Name =   "ID_A"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "ID_P"
         Columns(6).Name =   "ID_P"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "OBL"
         Columns(7).Name =   "OBL"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   11
         Columns(7).FieldLen=   256
         _ExtentX        =   17806
         _ExtentY        =   2593
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblOtrosDatos 
         BackColor       =   &H00808000&
         Caption         =   "Otros datos:"
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   0
         TabIndex        =   67
         Top             =   120
         Width           =   975
      End
   End
   Begin VB.PictureBox picProveedor 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   90
      ScaleHeight     =   300
      ScaleWidth      =   10005
      TabIndex        =   57
      Top             =   4335
      Width           =   10000
      Begin VB.CommandButton cmdBuscarProveedor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7500
         Picture         =   "frmSOLAbrirFaltan.frx":11A2
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   0
         Width           =   345
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveedor 
         Height          =   285
         Left            =   1560
         TabIndex        =   60
         Top             =   0
         Width           =   2055
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2408
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4630
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3625
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcDenProveedor 
         Height          =   285
         Left            =   3630
         TabIndex        =   61
         Top             =   0
         Width           =   3825
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2408
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4630
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6747
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblProveedor 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DProveedor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   62
         Top             =   45
         Width           =   1500
      End
   End
   Begin VB.PictureBox picDistribucion 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10005
      TabIndex        =   54
      Top             =   3960
      Width           =   10000
      Begin VB.CommandButton cmdDist 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7500
         Picture         =   "frmSOLAbrirFaltan.frx":122F
         Style           =   1  'Graphical
         TabIndex        =   59
         TabStop         =   0   'False
         Top             =   0
         Width           =   345
      End
      Begin VB.CommandButton cmdBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   7110
         Picture         =   "frmSOLAbrirFaltan.frx":129B
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   0
         Width           =   345
      End
      Begin VB.Label lblDist 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1560
         TabIndex        =   56
         Top             =   0
         Width           =   5500
      End
      Begin VB.Label lblDistribucion 
         BackColor       =   &H00808000&
         Caption         =   "DDIstribuci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   55
         Top             =   30
         Width           =   1500
      End
   End
   Begin VB.PictureBox picPres4 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10005
      TabIndex        =   46
      Top             =   3540
      Width           =   10000
      Begin VB.CommandButton cmdSelPres 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   7500
         Picture         =   "frmSOLAbrirFaltan.frx":1340
         Style           =   1  'Graphical
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   0
         Width           =   345
      End
      Begin VB.CommandButton cmdBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   7110
         Picture         =   "frmSOLAbrirFaltan.frx":13AC
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   0
         Width           =   345
      End
      Begin VB.Label lblLitPres 
         BackColor       =   &H00808000&
         Caption         =   "DCuenta de gasto"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   3
         Left            =   0
         TabIndex        =   48
         Top             =   40
         Width           =   1500
      End
      Begin VB.Label lblPres 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   1560
         TabIndex        =   47
         Top             =   0
         Width           =   5500
      End
   End
   Begin VB.PictureBox picPres3 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10005
      TabIndex        =   39
      Top             =   3100
      Width           =   10000
      Begin VB.CommandButton cmdSelPres 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   7500
         Picture         =   "frmSOLAbrirFaltan.frx":1451
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   0
         Width           =   345
      End
      Begin VB.CommandButton cmdBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   7110
         Picture         =   "frmSOLAbrirFaltan.frx":14BD
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   0
         Width           =   345
      End
      Begin VB.Label lblLitPres 
         BackColor       =   &H00808000&
         Caption         =   "DProyecto"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   2
         Left            =   0
         TabIndex        =   45
         Top             =   40
         Width           =   1500
      End
      Begin VB.Label lblPres 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   1560
         TabIndex        =   44
         Top             =   0
         Width           =   5500
      End
   End
   Begin VB.PictureBox picPres2 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10005
      TabIndex        =   38
      Top             =   2660
      Width           =   10000
      Begin VB.CommandButton cmdSelPres 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   7500
         Picture         =   "frmSOLAbrirFaltan.frx":1562
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   0
         Width           =   345
      End
      Begin VB.CommandButton cmdBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   7110
         Picture         =   "frmSOLAbrirFaltan.frx":15CE
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   0
         Width           =   345
      End
      Begin VB.Label lblLitPres 
         BackColor       =   &H00808000&
         Caption         =   "DCentro de coste"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   0
         TabIndex        =   43
         Top             =   40
         Width           =   1500
      End
      Begin VB.Label lblPres 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   1560
         TabIndex        =   42
         Top             =   0
         Width           =   5500
      End
   End
   Begin VB.PictureBox picPres1 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10005
      TabIndex        =   37
      Top             =   2220
      Width           =   10000
      Begin VB.CommandButton cmdSelPres 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   7500
         Picture         =   "frmSOLAbrirFaltan.frx":1673
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   0
         Width           =   345
      End
      Begin VB.CommandButton cmdBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   7110
         Picture         =   "frmSOLAbrirFaltan.frx":16DF
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   0
         Width           =   345
      End
      Begin VB.Label lblLitPres 
         BackColor       =   &H00808000&
         Caption         =   "DPartida contable"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   0
         TabIndex        =   41
         Top             =   40
         Width           =   1500
      End
      Begin VB.Label lblPres 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   0
         Left            =   1560
         TabIndex        =   40
         Top             =   0
         Width           =   5500
      End
   End
   Begin VB.Frame fraPres 
      BackColor       =   &H00808000&
      Caption         =   "DPresupuesto de los items"
      ForeColor       =   &H00FFFFFF&
      Height          =   2000
      Left            =   120
      TabIndex        =   36
      Top             =   4755
      Width           =   10150
      Begin VB.PictureBox picLineas 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   340
         Left            =   360
         ScaleHeight     =   345
         ScaleWidth      =   6000
         TabIndex        =   51
         Top             =   1460
         Width           =   6000
         Begin VB.TextBox txtPres 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2640
            TabIndex        =   23
            Top             =   0
            Width           =   1800
         End
         Begin VB.Label lblLineas 
            BackColor       =   &H00808000&
            Caption         =   "DPara las l�neas sin presupuesto"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   0
            TabIndex        =   53
            Top             =   40
            Width           =   2500
         End
         Begin VB.Label lblMon 
            BackColor       =   &H00808000&
            Caption         =   "EUR"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   4580
            TabIndex        =   52
            Top             =   40
            Width           =   975
         End
      End
      Begin VB.PictureBox picADJ 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   1100
         Left            =   6120
         ScaleHeight     =   1095
         ScaleWidth      =   3795
         TabIndex        =   49
         Top             =   360
         Visible         =   0   'False
         Width           =   3800
         Begin VB.OptionButton optCompartida 
            BackColor       =   &H00808000&
            Caption         =   "DPrecio m�s barato"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   19
            Top             =   380
            Width           =   1815
         End
         Begin VB.OptionButton optCompartida 
            BackColor       =   &H00808000&
            Caption         =   "DPrecio m�s caro"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   20
            Top             =   740
            Width           =   1815
         End
         Begin VB.OptionButton optCompartida 
            BackColor       =   &H00808000&
            Caption         =   "DMedia simple"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Index           =   2
            Left            =   2080
            TabIndex        =   21
            Top             =   380
            Width           =   1600
         End
         Begin VB.OptionButton optCompartida 
            BackColor       =   &H00808000&
            Caption         =   "DMedia ponderada"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Index           =   3
            Left            =   2080
            TabIndex        =   22
            Top             =   740
            Width           =   1800
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00FFFFFF&
            X1              =   0
            X2              =   0
            Y1              =   0
            Y2              =   1080
         End
         Begin VB.Label lblCompartida 
            BackColor       =   &H00808000&
            Caption         =   "DSi la adjudicaci�n es compartida"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   120
            TabIndex        =   50
            Top             =   0
            Width           =   3375
         End
      End
      Begin VB.OptionButton optPresItem 
         BackColor       =   &H00808000&
         Caption         =   "DActualizar el presupuesto unitario seg�n el precio de la l�nea de solicitud"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   18
         Top             =   1080
         Value           =   -1  'True
         Width           =   6000
      End
      Begin VB.OptionButton optPresItem 
         BackColor       =   &H00808000&
         Caption         =   "DActualizar el presupuesto unitario seg�n la �ltima adjudicaci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   720
         Width           =   5500
      End
      Begin VB.OptionButton optPresItem 
         BackColor       =   &H00808000&
         Caption         =   "DActualizar el presupuesto unitario seg�n planificaci�n anual"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   360
         Width           =   5500
      End
   End
   Begin VB.PictureBox picPago 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10095
      TabIndex        =   34
      Top             =   1780
      Width           =   10095
      Begin SSDataWidgets_B.SSDBCombo sdbcPagoCod 
         Height          =   285
         Left            =   1560
         TabIndex        =   6
         Top             =   0
         Width           =   1125
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1826
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   6033
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   1984
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483642
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPagoDen 
         Height          =   285
         Left            =   2740
         TabIndex        =   7
         Top             =   0
         Width           =   5100
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4815
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2302
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   8996
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblPago 
         BackColor       =   &H00808000&
         Caption         =   "DForma de pago"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   35
         Top             =   40
         Width           =   1500
      End
   End
   Begin VB.PictureBox picDestino 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10095
      TabIndex        =   32
      Top             =   1340
      Width           =   10095
      Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
         Height          =   285
         Left            =   1560
         TabIndex        =   4
         Top             =   0
         Width           =   1125
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   1905
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3201
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Direcci�n"
         Columns(2).Name =   "DIR"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2143
         Columns(3).Caption=   "Poblaci�n"
         Columns(3).Name =   "POB"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1826
         Columns(4).Caption=   "CP"
         Columns(4).Name =   "CP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "Pais"
         Columns(5).Name =   "PAIS"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Caption=   "Provincia"
         Columns(6).Name =   "PROVI"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   1984
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
         Height          =   285
         Left            =   2740
         TabIndex        =   5
         Top             =   0
         Width           =   5100
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   3307
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 1"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2143
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 0"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2487
         Columns(2).Caption=   "Direcci�n"
         Columns(2).Name =   "DIR"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1958
         Columns(3).Caption=   "Poblaci�n"
         Columns(3).Name =   "POB"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1773
         Columns(4).Caption=   "CP"
         Columns(4).Name =   "CP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "Pais"
         Columns(5).Name =   "PAIS"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   2487
         Columns(6).Caption=   "Provincia"
         Columns(6).Name =   "PROVI"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   8996
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblDest 
         BackColor       =   &H00808000&
         Caption         =   "DDestino"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   33
         Top             =   40
         Width           =   1500
      End
   End
   Begin VB.PictureBox picFecFin 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10095
      TabIndex        =   30
      Top             =   900
      Width           =   10095
      Begin VB.CommandButton cmdCalendarFin 
         Height          =   315
         Left            =   3100
         Picture         =   "frmSOLAbrirFaltan.frx":1784
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   0
         Width           =   315
      End
      Begin VB.TextBox txtFecFin 
         Height          =   285
         Left            =   1560
         TabIndex        =   2
         Top             =   0
         Width           =   1500
      End
      Begin VB.Label lblFecfin 
         BackColor       =   &H00808000&
         Caption         =   "DFin suministro"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   31
         Top             =   40
         Width           =   1500
      End
   End
   Begin VB.PictureBox picFecIni 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   10095
      TabIndex        =   28
      Top             =   460
      Width           =   10095
      Begin VB.CommandButton cmdCalendarIni 
         Height          =   315
         Left            =   3100
         Picture         =   "frmSOLAbrirFaltan.frx":1A96
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   0
         Width           =   315
      End
      Begin VB.TextBox txtFecIni 
         Height          =   285
         Left            =   1560
         TabIndex        =   0
         Top             =   0
         Width           =   1500
      End
      Begin VB.Label lblFecIni 
         BackColor       =   &H00808000&
         Caption         =   "DInicio suministro:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   29
         Top             =   40
         Width           =   1500
      End
   End
   Begin VB.PictureBox picNavigate 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   3480
      ScaleHeight     =   375
      ScaleWidth      =   3255
      TabIndex        =   26
      Top             =   8775
      Width           =   3255
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "DAceptar"
         Height          =   315
         Left            =   0
         TabIndex        =   24
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "DCancelar"
         Height          =   315
         Left            =   1560
         TabIndex        =   25
         Top             =   0
         Width           =   1215
      End
   End
   Begin VB.Label lblMensaje 
      BackColor       =   &H00808000&
      Caption         =   "DDebe dar de alta la siguiente informaci�n para poder dar de alta el proceso de compra"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   120
      Width           =   10095
   End
End
Attribute VB_Name = "frmSOLAbrirFaltan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Conrtol de Errores
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String

'Variables p�blicas
Public g_oSolicitudSeleccionada As CInstancia
Public g_oProceso As CProceso
Public g_bTraspasarAtributos As Boolean
Public g_bTraspasarAdjuntos As Boolean
Public g_oProcesoAEnviar As CProceso
Public g_bSoloAbrirProc As Boolean
Public g_oPlantillaSeleccionada As CPlantilla
Public g_bGruposPlantillaCoinciden As Boolean
Public g_bModificarEstructuraProceso As Boolean
Public g_arSolicitudes As Variant

'Variables para los combos:
Private m_bDestRespetarCombo As Boolean
Private m_bDestCargarComboDesde As Boolean
Private m_bPagoRespetarCombo As Boolean
Private m_bPagoCargarComboDesde As Boolean
Private m_oDestinos As CDestinos
Private m_oPagos As CPagos

'Seguridad
Private m_bRestrDest As Boolean
Private m_bRDestPerf As Boolean
Private m_lIdPerfil As Long

'Presupuestos:
Private m_vPresup1 As String
Private m_vPresup2 As String
Private m_vPresup3 As String
Private m_vPresup4 As String

'Variables de idiomas:
Private m_sIdiDestino As String
Private m_sIdiFPago As String
Private m_sIdiFecIni As String
Private m_sIdiFecFin As String
Private m_sMensajes(7) As String
Private m_sMensajes2(10) As String
Private m_sIdiCaption As String
Private m_sIdiErrorEvaluacion(2) As String
Private m_bValSaltarseVolMaxAdjDir As Boolean
Private m_bRUsuUON As Boolean
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String

'3114
Private m_bRestrMatComp As Boolean
Private bRespetarComboProve As Boolean
Private bCargarComboDesde As Boolean
Public g_bGenerarPedido_VerProve As Boolean
Public g_bGenerarPedido_EstasGenerando As Boolean

'3275
Public g_bMostrarAtribEspePlatillaProceso As Boolean

'3275
Private sIdiMayor As String
Private sIdiMenor As String
Private sIdiEntre As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String


'3275
Private m_oAtribEspePlantP As CAtributos
Private m_oAtribEspeSinValorP As CAtributo
Private m_oAtributoP As CAtributo
Private bGuardar As Boolean
Private bUpdate As Boolean
Private iFila As Variant


''' <summary>
''' Tras pulsar al boton de aceptar se genera el proceso, para ello configuramos el proceso
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim oDistsNivel1 As CDistItemsNivel1
    Dim oDistsNivel2 As CDistItemsNivel2
    Dim oDistsNivel3 As CDistItemsNivel3
    Dim bDistOk As Boolean
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    
    '3275
    Dim oAtributoEsp As CAtributo
    Dim sMensaje As String
    Dim bPrimero As Boolean
    Dim bEncontrado As Boolean
    Dim iFilaProceso As Integer
    Dim iFilaProcesoError As Integer
    
    'Si se han pedido presupuestos comprueba que se hayan seleccionado valores:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If picPres1.Visible = True Then
        If lblPres(0).caption = "" Then
            oMensajes.FaltanDatos (gParametrosGenerales.gsSingPres1)
            Exit Sub
        End If
    End If
    
    If picPres2.Visible = True Then
        If lblPres(1).caption = "" Then
            oMensajes.FaltanDatos (gParametrosGenerales.gsSingPres2)
            Exit Sub
        End If
    End If
    
    If picPres3.Visible = True Then
        If lblPres(2).caption = "" Then
            oMensajes.FaltanDatos (gParametrosGenerales.gsSingPres3)
            Exit Sub
        End If
    End If
    
    If picPres4.Visible = True Then
        If lblPres(3).caption = "" Then
            oMensajes.FaltanDatos (gParametrosGenerales.gsSingPres4)
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    If picDistribucion.Visible Then
        If lblDist.caption = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.FaltanDatos (lblDistribucion.caption)
            Exit Sub
        Else
            'Comprobar que la distribuci�n se ha hecho al nivel necesario
            bDistOk = True
            Select Case gParametrosGenerales.giNIVDIST
                Case 1
                    bDistOk = (m_sUON1 <> "")
                Case 2
                    bDistOk = (m_sUON2 <> "")
                Case 3
                    bDistOk = (m_sUON3 <> "")
            End Select
            If Not bDistOk Then
                Screen.MousePointer = vbNormal
                oMensajes.Distribuci�nObligatoriaANivel gParametrosGenerales.giNIVDIST
                Exit Sub
            End If
            
            'Hasta ahora la distribuci�n se indicaba solo a nivel de proceso
            'Ahora la distribuci�n puede ponerse a nivel de grupo o item si se han rellenado valores de antes
            Select Case g_oProceso.DefDistribUON
            Case EnProceso
                If m_sUON3 <> "" Then
                    Set oDistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                    oDistsNivel3.Add Nothing, m_sUON1, m_sUON2, m_sUON3, 100, , 0
                Else
                    If m_sUON2 <> "" Then
                        Set oDistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                        oDistsNivel2.Add Nothing, m_sUON1, m_sUON2, 100, , 0
                    Else
                        Set oDistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                        oDistsNivel1.Add Nothing, m_sUON1, 100, , 0
                    End If
                End If
                
                Set g_oProceso.DistsNivel1 = oDistsNivel1
                Set g_oProceso.DistsNivel2 = oDistsNivel2
                Set g_oProceso.DistsNivel3 = oDistsNivel3


            Case EnGrupo
                If m_sUON3 <> "" Then
                    If Not g_oProceso.Grupos Is Nothing Then
                        For Each oGrupo In g_oProceso.Grupos
                            If oGrupo.DistsNivel1 Is Nothing And oGrupo.DistsNivel2 Is Nothing And oGrupo.DistsNivel3 Is Nothing Then
                                Set oDistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                                oDistsNivel3.Add Nothing, m_sUON1, m_sUON2, m_sUON3, 100, , 0
                                Set oGrupo.DistsNivel3 = oDistsNivel3
                                'Set g_oProceso.Grupos = oGrupo
                            End If
                        Next
                    End If
                Else
                    If m_sUON2 <> "" Then
                        If Not g_oProceso.Grupos Is Nothing Then
                            For Each oGrupo In g_oProceso.Grupos
                                If oGrupo.DistsNivel1 Is Nothing And oGrupo.DistsNivel2 Is Nothing And oGrupo.DistsNivel3 Is Nothing Then
                                    Set oDistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                                    oDistsNivel2.Add Nothing, m_sUON1, m_sUON2, 100, , 0
                                    Set oGrupo.DistsNivel2 = oDistsNivel2
                                End If
                            Next
                        End If
                    Else
                        If Not g_oProceso.Grupos Is Nothing Then
                            For Each oGrupo In g_oProceso.Grupos
                                If oGrupo.DistsNivel1 Is Nothing And oGrupo.DistsNivel2 Is Nothing And oGrupo.DistsNivel3 Is Nothing Then
                                    Set oDistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                                    oDistsNivel1.Add Nothing, m_sUON1, 100, , 0
                                    Set oGrupo.DistsNivel1 = oDistsNivel1
                                End If
                            Next
                        End If
                    End If
                End If
                
            
            Case EnItem
                If m_sUON3 <> "" Then
                    If Not g_oProceso.Grupos Is Nothing Then
                        For Each oGrupo In g_oProceso.Grupos
                            If Not oGrupo.Items Is Nothing Then
                                For Each oItem In oGrupo.Items
                                    If NoHayParametro(oItem.UON1Cod) And NoHayParametro(oItem.UON2Cod) And NoHayParametro(oItem.UON3Cod) Then
                                        oItem.UON1Cod = m_sUON1
                                        oItem.UON2Cod = m_sUON2
                                        oItem.UON3Cod = m_sUON3
                                    End If
                                Next
                            End If
                        Next
                    End If
               
                Else
                  If m_sUON2 <> "" Then
                        If Not g_oProceso.Grupos Is Nothing Then
                            For Each oGrupo In g_oProceso.Grupos
                                If Not oGrupo.Items Is Nothing Then
                                    For Each oItem In oGrupo.Items
                                        If NoHayParametro(oItem.UON1Cod) And NoHayParametro(oItem.UON2Cod) And NoHayParametro(oItem.UON3Cod) Then
                                            oItem.UON1Cod = m_sUON1
                                            oItem.UON2Cod = m_sUON2
                                            oItem.UON3Cod = Null
                                        End If
                                    Next
                                End If
                            Next
                        End If

                    Else
                        If Not g_oProceso.Grupos Is Nothing Then
                            For Each oGrupo In g_oProceso.Grupos
                                If Not oGrupo.Items Is Nothing Then
                                    For Each oItem In oGrupo.Items
                                        If NoHayParametro(oItem.UON1Cod) And NoHayParametro(oItem.UON2Cod) And NoHayParametro(oItem.UON3Cod) Then
                                            oItem.UON1Cod = m_sUON1
                                            oItem.UON2Cod = Null
                                            oItem.UON3Cod = Null
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    
                    End If
                End If
        
            End Select
        End If
    End If
    
    If picProveedor.Visible = True Then
        If Me.sdbcProveedor.Value = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.FaltanDatos (lblProveedor.caption)
            Exit Sub
        Else
            g_oProceso.ProveActual = Me.sdbcProveedor.Value
            g_oProceso.DefProveActual = EnProceso
            
            If Not g_oProceso.Grupos Is Nothing Then
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            oItem.ProveAct = Me.sdbcProveedor.Value
                        Next
                    End If
                Next
            End If
        End If
    End If
    
    '3275
    If g_bMostrarAtribEspePlatillaProceso Then
        'Comprueba si quedan datos del grid sin actualizar
        bGuardar = True
        ActualizarAtribEspeProcePlantilla
        If bGuardar = False Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If

        '3275
        'Recorro todos los atributos de especificacion de plantilla
        'a nivel de ITEM  y GRUPO a�adidos al proceso para ver si estan informados
        sMensaje = ""
        bPrimero = True
        Set oAtributoEsp = Nothing
        Set oAtributoEsp = oFSGSRaiz.Generar_CAtributo
        Set m_oAtribEspeSinValorP = Nothing
        iFilaProceso = 0
        iFilaProcesoError = 0
        
        If Not g_oProceso Is Nothing Then
            'Si el PROCESO tiene atributos de especificacion, miro si alguno es de plantilla y valido si tiene o no valor
            If Not g_oProceso.AtributosEspecificacion Is Nothing And Not m_oAtribEspePlantP Is Nothing Then
                For Each oAtributoEsp In g_oProceso.AtributosEspecificacion
                    'Si el atributo de especificacion en curso, es de la plantilla, se comprueba si esta informado
                    If Not m_oAtribEspePlantP.Item(oAtributoEsp.Atrib) Is Nothing Then
                        Select Case oAtributoEsp.Tipo
                            Case TiposDeAtributos.TipoString
                                If IsNull(oAtributoEsp.valorText) Or oAtributoEsp.valorText = "" Then
                                    'marcar como sin informar
                                    sMensaje = sMensaje & " " & oAtributoEsp.Cod & "-" & oAtributoEsp.CodDen & vbCrLf
                                    If bPrimero Then
                                        Set m_oAtribEspeSinValorP = oFSGSRaiz.Generar_CAtributo
                                        Set m_oAtribEspeSinValorP = oAtributoEsp
                                        bPrimero = False
                                        iFilaProcesoError = iFilaProceso
                                    End If
                                End If
                            Case TiposDeAtributos.TipoNumerico
                                If IsNull(oAtributoEsp.valorNum) Or oAtributoEsp.valorNum = "" Then
                                    'marcar como sin informar
                                    sMensaje = sMensaje & " " & oAtributoEsp.Cod & "-" & oAtributoEsp.CodDen & vbCrLf
                                    If bPrimero Then
                                        Set m_oAtribEspeSinValorP = oFSGSRaiz.Generar_CAtributo
                                        Set m_oAtribEspeSinValorP = oAtributoEsp
                                        bPrimero = False
                                        iFilaProcesoError = iFilaProceso
                                    End If
                                End If
                            Case TiposDeAtributos.TipoFecha
                                If IsNull(oAtributoEsp.valorFec) Or oAtributoEsp.valorFec = "" Then
                                    'marcar como sin informar
                                    sMensaje = sMensaje & " " & oAtributoEsp.Cod & "-" & oAtributoEsp.CodDen & vbCrLf
                                    If bPrimero Then
                                        Set m_oAtribEspeSinValorP = oFSGSRaiz.Generar_CAtributo
                                        Set m_oAtribEspeSinValorP = oAtributoEsp
                                        bPrimero = False
                                        iFilaProcesoError = iFilaProceso
                                    End If
                                End If
                            Case TiposDeAtributos.TipoBoolean
                                If IsNull(oAtributoEsp.valorBool) Or oAtributoEsp.valorBool = "" Then
                                    'marcar como sin informar
                                    sMensaje = sMensaje & " " & oAtributoEsp.Cod & "-" & oAtributoEsp.CodDen & vbCrLf
                                    If bPrimero Then
                                        Set m_oAtribEspeSinValorP = oFSGSRaiz.Generar_CAtributo
                                        Set m_oAtribEspeSinValorP = oAtributoEsp
                                        bPrimero = False
                                        iFilaProcesoError = iFilaProceso
                                    End If
                                End If
                        End Select
                        iFilaProceso = iFilaProceso + 1
                    End If
                Next
            End If
        End If
        
        bEncontrado = False
        If Not m_oAtribEspeSinValorP Is Nothing Then
            Screen.MousePointer = vbNormal
            oMensajes.AtributosEspObl sMensaje, 0
            If Me.Visible Then sdbgAtributos.SetFocus
            sdbgAtributos.CancelUpdate
            sdbgAtributos.DataChanged = False
            bUpdate = False
            iFila = iFilaProcesoError
            sdbgAtributos_PositionList (m_oAtribEspeSinValorP.Cod)
            'Nos posicionamos en la columna correspondiente al atributo sin valor
            bGuardar = False
            Exit Sub
        End If

    End If
    
    If g_oProceso.DefPresAnualTipo1 = NoDefinido Then
        g_oProceso.DefPresAnualTipo1 = gParametrosInstalacion.gCPPresAnu1
    End If
    If g_oProceso.DefPresAnualTipo2 = NoDefinido Then
        g_oProceso.DefPresAnualTipo2 = gParametrosInstalacion.gCPPresAnu2
    End If
    If g_oProceso.DefPresTipo1 = NoDefinido Then
        g_oProceso.DefPresTipo1 = gParametrosInstalacion.gCPPres1
    End If
    
    
    If g_oProceso.DefPresTipo2 = NoDefinido Then
        g_oProceso.DefPresTipo2 = gParametrosInstalacion.gCPPres2
    End If
    
    If g_oProcesoAEnviar Is Nothing Then
        'Abre un proceso nuevo
        AbrirProceso
    Else
        'Env�a la solicitud a un proceso ya existente:
        EnviarAProceso
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBorrar_Click(Index As Integer)
    Select Case Index
        Case 0:
            Me.lblPres(Index).caption = ""
            m_vPresup1 = ""

        Case 1:
            Me.lblPres(Index).caption = ""
            m_vPresup2 = ""

        Case 2:
            Me.lblPres(Index).caption = ""
            m_vPresup3 = ""
        
        Case 3:
            Me.lblPres(Index).caption = ""
            m_vPresup4 = ""
            
        Case 4:
            lblDist.caption = ""
    End Select
End Sub

''' <summary>
''' Buscar proveedor
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdBuscarProveedor_Click()
    frmPROVEBuscar.sOrigen = "frmSOLAbrirFaltan"
    frmPROVEBuscar.bRMat = m_bRestrMatComp

    frmPROVEBuscar.Show 1
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendarFin_Click()
    AbrirFormCalendar Me, txtFecFin, "FECFIN"
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendarIni_Click()
    AbrirFormCalendar Me, txtFecIni, "FECINI"
End Sub

Private Sub cmdCancelar_Click()
    If g_bSoloAbrirProc = False Then
        Unload frmSOLAbrirProc
    End If
    Unload Me
End Sub

Private Sub cmdDist_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
frmSELUO.sOrigen = "frmSOLAbrirFaltan"
frmSELUO.bRUO = m_bRUsuUON
frmSELUO.Show vbModal

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirFaltan", "cmdDist_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub cmdSelPres_Click(Index As Integer)
    frmPRESAsig.g_dblAsignado = 0
    frmPRESAsig.g_bHayPres = False
    frmPRESAsig.g_bHayPresBajaLog = False
    frmPRESAsig.g_sOrigen = "frmSOLAbrirFaltan"
    frmPRESAsig.g_iTipoPres = Index + 1
    frmPRESAsig.g_bModif = True
    frmPRESAsig.g_bSinPermisos = False
    Select Case Index
    
        Case 0
            frmPRESAsig.g_sValorPresFormulario = m_vPresup1
        Case 1
            frmPRESAsig.g_sValorPresFormulario = m_vPresup2
        Case 2
            frmPRESAsig.g_sValorPresFormulario = m_vPresup3
        Case 3
            frmPRESAsig.g_sValorPresFormulario = m_vPresup4
    End Select
    
    frmPRESAsig.Show vbModal


End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not m_bActivado Then
        m_bActivado = True
   End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Carga la pantalla. Se ocupa de cargar, ocultar y habilitar combos, de cargar el arbol de proceso, etc.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    'Set m_bDescargarFrm = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    m_bActivado = False
    
    Me.Height = 7755
    Me.Width = 10455
    
    m_vPresup1 = ""
    m_vPresup2 = ""
    m_vPresup3 = ""
    m_vPresup4 = ""
        
    ConfigurarSeguridad
    
    If Not oUsuarioSummit.Perfil Is Nothing Then m_lIdPerfil = oUsuarioSummit.Perfil.Id
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    Set m_oPagos = oFSGSRaiz.generar_CPagos
    
    ConfigurarCamposNecesarios
    
    Me.caption = m_sIdiCaption
    If Not IsNull(g_arSolicitudes) And Not IsEmpty(g_arSolicitudes) Then
        If UBound(g_arSolicitudes) = 0 Then Me.caption = Me.caption & " " & g_oSolicitudSeleccionada.Id & " " & g_oSolicitudSeleccionada.DescrBreve
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

''' <summary>
''' Carga los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_ABRIR_FALTAN, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sIdiCaption = Ador(0).Value   '1 Apertura de proceso de compra desde solicitud:
        Ador.MoveNext
        Me.lblMensaje.caption = Ador(0).Value '2 Debe completar la siguiente informaci�n para poder dar de alta el proceso de compra
        Ador.MoveNext
        Me.lblFecIni.caption = Ador(0).Value & ":"  '3 Inicio suministro:
        m_sIdiFecIni = Ador(0).Value
        Ador.MoveNext
        Me.lblFecfin.caption = Ador(0).Value & ":"   '4 Fin suministro:
        m_sIdiFecFin = Ador(0).Value
        Ador.MoveNext
        Me.lblDest.caption = Ador(0).Value & ":"  '5 Destino
        m_sIdiDestino = Ador(0).Value
        Ador.MoveNext
        Me.lblPago.caption = Ador(0).Value & ":"  '6 Forma de pago:
        m_sIdiFPago = Ador(0).Value
        Ador.MoveNext
        Me.fraPres.caption = Ador(0).Value  '7 Presupuesto de los items
        Ador.MoveNext
        Me.optPresItem(0).caption = Ador(0).Value  '8 Actualizar el presupuesto unitario seg�n la planificaci�n anual
        Ador.MoveNext
        Me.optPresItem(1).caption = Ador(0).Value  '9 Actualizar el presupuesto unitario seg�n la �ltima adjudicaci�n
        Ador.MoveNext
        Me.optPresItem(2).caption = Ador(0).Value '10 Actualizar el presupuesto unitario seg�n el precio de la l�nea de solicitud
        Ador.MoveNext
        Me.lblLineas.caption = Ador(0).Value '11 Para las l�neas sin presupuesto
        Ador.MoveNext
        Me.lblCompartida.caption = Ador(0).Value ' 12 Si la adjudicaci�n es compartida:
        Ador.MoveNext
        Me.optCompartida(0).caption = Ador(0).Value ' 13 Precio m�s barato
        Ador.MoveNext
        Me.optCompartida(1).caption = Ador(0).Value ' 14 Precio m�s caro
        Ador.MoveNext
        Me.optCompartida(2).caption = Ador(0).Value ' 15 Media simple
        Ador.MoveNext
        Me.optCompartida(3).caption = Ador(0).Value ' 16 Media ponderada
        Ador.MoveNext
        Me.cmdAceptar.caption = Ador(0).Value '17 &Aceptar
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).Value '18 &Cancelar
        
        Ador.MoveNext
        sdbcDestCod.Columns("COD").caption = Ador(0).Value  '19 C�digo
        sdbcDestDen.Columns("COD").caption = Ador(0).Value
        sdbcPagoCod.Columns("COD").caption = Ador(0).Value
        sdbcPagoDen.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbcDestCod.Columns("DEN").caption = Ador(0).Value '20 Denominaci�n
        sdbcDestDen.Columns("DEN").caption = Ador(0).Value
        sdbcPagoCod.Columns("DEN").caption = Ador(0).Value
        sdbcPagoDen.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbcDestCod.Columns("DIR").caption = Ador(0).Value  '21 Direcci�n
        Ador.MoveNext
        sdbcDestCod.Columns("POB").caption = Ador(0).Value  '22  Poblaci�n
        Ador.MoveNext
        sdbcDestCod.Columns("CP").caption = Ador(0).Value   '23 C�digo postal
        Ador.MoveNext
        sdbcDestCod.Columns("PAIS").caption = Ador(0).Value  '24 Pa�s
        Ador.MoveNext
        sdbcDestCod.Columns("PROVI").caption = Ador(0).Value  '25 Provincia
        
        For i = 1 To 7
            Ador.MoveNext
            m_sMensajes(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '33 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '34 Error al realizar el c�lculo:Valores incorrectos.
        
        For i = 1 To 10
            Ador.MoveNext
            m_sMensajes2(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        lblDistribucion.caption = Ador(0).Value
        
        Ador.MoveNext
        lblProveedor.caption = Ador(0).Value 'Proveedor
        
        '3275
        Ador.MoveNext
        sdbgAtributos.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sIdiMayor = Ador(0).Value
        Ador.MoveNext
        sIdiMenor = Ador(0).Value
        Ador.MoveNext
        sIdiEntre = Ador(0).Value
        Ador.MoveNext
        lblOtrosDatos.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        Ador.MoveNext
        If Not (g_oProcesoAEnviar Is Nothing) Then m_sIdiCaption = Ador(0).Value          '56 - Envio de datos de solicitud a proceso:
        Ador.MoveNext
        If UBound(g_arSolicitudes) > 0 Then m_sIdiCaption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing
    
    Me.lblLitPres(0).caption = gParametrosGenerales.gsSingPres1 & ":"
    Me.lblLitPres(1).caption = gParametrosGenerales.gsSingPres2 & ":"
    Me.lblLitPres(2).caption = gParametrosGenerales.gsSingPres3 & ":"
    Me.lblLitPres(3).caption = gParametrosGenerales.gsSingPres4 & ":"
End Sub

''' <summary>
''' Descargar las variables de pantalla
''' </summary>
''' <param name="Cancel">Cancelar el cierre</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
      oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_bGenerarPedido_VerProve = False
    g_bGenerarPedido_EstasGenerando = False
    
    Set m_oDestinos = Nothing
    Set m_oPagos = Nothing
    Set g_oProceso = Nothing
    Set g_oSolicitudSeleccionada = Nothing
    Set g_oProcesoAEnviar = Nothing
    
    g_bTraspasarAtributos = False
    g_bTraspasarAdjuntos = False
    g_bSoloAbrirProc = False
    Set g_oPlantillaSeleccionada = Nothing
    g_bGruposPlantillaCoinciden = False
    g_bModificarEstructuraProceso = False
    
    m_vPresup1 = ""
    m_vPresup2 = ""
    m_vPresup3 = ""
    m_vPresup4 = ""
    m_sUON1 = ""
    m_sUON2 = ""
    m_sUON3 = ""
        
    Set m_oAtribEspePlantP = Nothing
    Set m_oAtribEspeSinValorP = Nothing
    Set m_oAtributoP = Nothing
    bGuardar = False
    bUpdate = False
    iFila = Empty
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub optPresItem_Click(Index As Integer)
    Select Case Index
        Case 0:
            'Actualizar el presupuesto unitario seg�n planificaci�n anual
            picADJ.Visible = False
            picLineas.Visible = False
            
        Case 1:
            'Actualizar el presupuesto unitario seg�n la �ltima adjudicaci�n
            picADJ.Visible = True
            picLineas.Visible = False
            
        Case 2:
            'Actualizar el presupuesto unitario seg�n el precio de la l�nea de solicitud
            picADJ.Visible = False
            picLineas.Visible = True
    End Select
End Sub



''' <summary>
''' Evento que salta al producirse un cambio en el combo. Limpia el c�digo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_Change()
    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveedor.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
End Sub

''' <summary>
''' Evento que salta al producirse un click en el combo. Limpia si no esta desplagado
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_Click()
    
    If Not sdbcDenProveedor.DroppedDown Then
        sdbcProveedor = ""
        sdbcDenProveedor = ""
    End If
End Sub

''' <summary>
''' Evento que salta al cerrarse el combo. Establece el codigo y denominaci�n elegidos.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_CloseUp()
    If sdbcDenProveedor.Value = "..." Then
        sdbcDenProveedor.Text = ""
        Exit Sub
    End If
    
    If sdbcDenProveedor.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveedor.Text = sdbcDenProveedor.Columns(1).Text
    sdbcProveedor.Columns(0).Value = sdbcDenProveedor.Columns(1).Text
    sdbcDenProveedor.Text = sdbcDenProveedor.Columns(0).Text
    bRespetarComboProve = False
    
    DoEvents
       
    bCargarComboDesde = False
End Sub

''' <summary>
''' Evento que salta al desplegarse el combo. Carga las denominaciones.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_DropDown()
    Dim i As Integer
    Dim oProves As CProveedores
    Dim Codigos As TipoDatosCombo

    Set oProves = oFSGSRaiz.generar_CProveedores
    
    Screen.MousePointer = vbHourglass
    
    sdbcDenProveedor.RemoveAll
    If bCargarComboDesde Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcDenProveedor.Text), , True, , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , , True, , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcDenProveedor.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    If Not oProves.EOF Then
        sdbcDenProveedor.AddItem "..."
    End If

    sdbcDenProveedor.SelStart = 0
    sdbcDenProveedor.SelLength = Len(sdbcDenProveedor.Text)
    sdbcProveedor.Refresh

    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
    
    Set oProves = Nothing
End Sub

''' <summary>
''' Evento que salta al seleccionar en el combo. Posicionarse en el combo segun la seleccion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_InitColumnProps()
    sdbcDenProveedor.DataFieldList = "Column 0"
    sdbcDenProveedor.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>
''' Evento que salta al seleccionar en el combo. Posicionarse en el combo segun la seleccion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcDenProveedor_PositionList(ByVal Text As String)
PositionList sdbcDenProveedor, Text
End Sub

Private Sub sdbcDestCod_Change()
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
End Sub

Private Sub sdbcDestCod_Click()
    If Not sdbcDestCod.DroppedDown Then
        sdbcDestCod.Value = ""
        sdbcDestDen.Value = ""
    End If
End Sub

Private Sub sdbcDestCod_CloseUp()
    If sdbcDestCod.Value = "..." Then
        sdbcDestCod.Text = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
    sdbcDestCod.Text = sdbcDestCod.Columns(0).Text
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
End Sub

''' <summary>
''' Cargar el combo Destinos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestCod_DropDown()
Dim ADORs As Ador.Recordset
    
    Screen.MousePointer = vbHourglass
    
    sdbcDestCod.RemoveAll
    
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , , m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , , m_bRestrDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCod.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestCod.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestCod.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestCod.Rows = 0 Then
        sdbcDestCod.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcDestCod_InitColumnProps()
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcDestCod_PositionList(ByVal Text As String)
PositionList sdbcDestCod, Text
End Sub

''' <summary>
''' Valida el combo
''' </summary>
''' <param name="Cancel">Cancela la edici�n si no pasa la validaci�n</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestCod_Validate(Cancel As Boolean)
    Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCod.Text = "" Then Exit Sub

    If sdbcDestCod.Text = sdbcDestCod.Columns(0).Text Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
        m_bDestRespetarCombo = False
        Exit Sub
    End If

    If sdbcDestCod.Text = sdbcDestDen.Columns(1).Text Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
        m_bDestRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el destino
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , True, m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , True, m_bRestrDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCod.Text = ""
        Screen.MousePointer = vbNormal
    ElseIf ADORs.EOF Then
        sdbcDestCod.Text = ""
        Screen.MousePointer = vbNormal
        ADORs.Close
        Set ADORs = Nothing
    Else
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value

        sdbcDestCod.Columns(0).Text = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Text = sdbcDestDen.Text

        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = False
        ADORs.Close
        Set ADORs = Nothing
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "sdbcDestCod_Validate", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If


End Sub


Private Sub sdbcDestDen_Change()
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestCod.Text = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
End Sub

Private Sub sdbcDestDen_Click()
    If Not sdbcDestDen.DroppedDown Then
        sdbcDestDen.Value = ""
        sdbcDestCod.Value = ""
    End If
End Sub

Private Sub sdbcDestDen_CloseUp()
    If sdbcDestDen.Value = "..." Then
        sdbcDestDen.Text = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestCod.Text = sdbcDestDen.Columns(1).Text
    sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
End Sub

''' <summary>
''' Cargar el combo Denom Destinos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestDen_DropDown()
Dim ADORs As Ador.Recordset
    
    Screen.MousePointer = vbHourglass
    
    sdbcDestDen.RemoveAll
    
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestDen.Value), , , m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestDen.Value), , , m_bRestrDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbcDestDen.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestDen.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestDen.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestDen.Rows = 0 Then
        sdbcDestDen.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcDestDen_InitColumnProps()
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcDestDen_PositionList(ByVal Text As String)
PositionList sdbcDestDen, Text
End Sub


Private Sub sdbcPagoCod_Change()
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Text = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
End Sub

Private Sub sdbcPagoCod_Click()
    If Not sdbcPagoCod.DroppedDown Then
        sdbcPagoCod.Value = ""
        sdbcPagoDen.Value = ""
    End If
End Sub

Private Sub sdbcPagoCod_CloseUp()
    If sdbcPagoCod.Value = "..." Then
        sdbcPagoCod.Text = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoDen.Text = sdbcPagoCod.Columns(1).Text
    sdbcPagoCod.Text = sdbcPagoCod.Columns(0).Text
    m_bPagoRespetarCombo = False
    
    m_bPagoCargarComboDesde = False
End Sub


Private Sub sdbcPagoCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    sdbcPagoCod.RemoveAll
    
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbcPagoCod.Value), , , False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    End If
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoCod.AddItem "..."
    End If

    sdbcPagoCod.SelStart = 0
    sdbcPagoCod.SelLength = Len(sdbcPagoCod.Text)
    sdbcPagoCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcPagoCod_InitColumnProps()
    sdbcPagoCod.DataFieldList = "Column 0"
    sdbcPagoCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcPagoCod_PositionList(ByVal Text As String)
PositionList sdbcPagoCod, Text
End Sub


Private Sub sdbcPagoCod_Validate(Cancel As Boolean)
Dim oPagos As CPagos
Dim bExiste As Boolean
        
    Set oPagos = oFSGSRaiz.generar_CPagos
    If sdbcPagoCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oPagos.CargarTodosLosPagos sdbcPagoCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPagos.Count = 0)
    
    If Not bExiste Then
        sdbcPagoCod.Text = ""
    Else
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Text = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcPagoCod.Columns(0).Value = sdbcPagoCod.Text
        sdbcPagoCod.Columns(1).Value = sdbcPagoDen.Text
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = False
    End If
    
    Set oPagos = Nothing

End Sub


Private Sub sdbcPagoDen_Change()
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoCod.Text = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
End Sub

Private Sub sdbcPagoDen_Click()
    If Not sdbcPagoDen.DroppedDown Then
        sdbcPagoDen.Value = ""
        sdbcPagoCod.Value = ""
    End If
End Sub

Private Sub sdbcPagoDen_CloseUp()
    If sdbcPagoDen.Value = "..." Then
        sdbcPagoDen.Text = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoCod.Text = sdbcPagoDen.Columns(1).Text
    sdbcPagoDen.Text = sdbcPagoDen.Columns(0).Text
    m_bPagoRespetarCombo = False
    
    m_bPagoCargarComboDesde = False
End Sub


Private Sub sdbcPagoDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.generar_CPagos
     
    sdbcPagoDen.RemoveAll
    
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , CStr(sdbcPagoDen.Value), True, False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
    End If
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoDen.AddItem "..."
    End If

    sdbcPagoDen.SelStart = 0
    sdbcPagoDen.SelLength = Len(sdbcPagoDen.Text)
    sdbcPagoDen.Refresh

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcPagoDen_InitColumnProps()
    sdbcPagoDen.DataFieldList = "Column 0"
    sdbcPagoDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcPagoDen_PositionList(ByVal Text As String)
PositionList sdbcPagoDen, Text
End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub ConfigurarSeguridad()
    'Restricci�n de material:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDest)) Is Nothing) Then
        m_bRestrDest = True
    End If
    
    m_bRDestPerf = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDestPerf)) Is Nothing)
    
    '
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermSaltarseVolMaxAdjDir)) Is Nothing) Then
        m_bValSaltarseVolMaxAdjDir = True
    End If
    m_bRUsuUON = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUsuUON)) Is Nothing)
    
    'Restriccion material usuario comprador
    If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing Then
        m_bRestrMatComp = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Evento que salta al producirse un cambio en el combo. Limpia la denominaci�n.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_Change()

    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcDenProveedor.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
    
End Sub

''' <summary>
''' Evento que salta al producirse un click en el combo. Limpia si no esta desplagado
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_Click()
     If Not sdbcProveedor.DroppedDown Then
        sdbcProveedor = ""
        sdbcDenProveedor = ""
     End If
End Sub

''' <summary>
''' Evento que salta al cerrarse el combo. Establece el codigo y denominaci�n elegidos.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_CloseUp()
    If sdbcProveedor.Value = "..." Then
        sdbcProveedor.Text = ""
        Exit Sub
    End If
    
    If sdbcProveedor.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcDenProveedor.Text = sdbcProveedor.Columns(1).Text
    sdbcDenProveedor.Columns(0).Value = sdbcProveedor.Columns(1).Text
    sdbcProveedor.Text = sdbcProveedor.Columns(0).Text
    bRespetarComboProve = False
    
    DoEvents
        
    bCargarComboDesde = False
End Sub

''' <summary>
''' Evento que salta al desplegarse el combo. Carga los codigo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_DropDown()
    Dim i As Integer
    Dim oProves As CProveedores
    Dim Codigos As TipoDatosCombo

    Set oProves = oFSGSRaiz.generar_CProveedores

    Screen.MousePointer = vbHourglass
    
    sdbcProveedor.RemoveAll
    
    If bCargarComboDesde Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveedor.Text), , , , , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , , , , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If


    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveedor.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveedor.AddItem "..."
    End If

    sdbcProveedor.SelStart = 0
    sdbcProveedor.SelLength = Len(sdbcProveedor.Text)
    sdbcProveedor.Refresh
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
    
    Set oProves = Nothing
End Sub

''' <summary>
''' Evento que salta al inicializarse el combo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_InitColumnProps()
    sdbcProveedor.DataFieldList = "Column 0"
    sdbcProveedor.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>
''' Evento que salta al seleccionar en el combo. Posicionarse en el combo segun la seleccion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_PositionList(ByVal Text As String)
PositionList sdbcProveedor, Text
End Sub

''' <summary>
''' Evento que salta al escribir y salir del combo. Validar q es un c�digo valido.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProveedor_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oProves As CProveedores
    
    If sdbcProveedor.Text = "" Then Exit Sub
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    ''' Solo continuamos si existe el proveedor
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveedor.Text), , True, , , m_bRestrMatComp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario

    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveedor.Text))
    
    If Not bExiste Then
        sdbcProveedor.Text = ""
    Else
        bRespetarComboProve = True
        sdbcDenProveedor.Text = oProves.Item(1).Den
        
        sdbcProveedor.Columns(0).Value = sdbcProveedor.Text
        sdbcProveedor.Columns(1).Value = sdbcDenProveedor.Text
        
        bRespetarComboProve = False
    End If
    bCargarComboDesde = False
    
    Set oProves = Nothing
End Sub

Private Sub sdbgAtributos_BtnClick()
Dim sUON1 As String
Dim sUON2 As String
Dim sUON3 As String
Dim sValAtr As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
With sdbgAtributos
    Select Case .Columns(.col).Name
        Case "VALOR"
            If m_oAtribEspePlantP.Item(CStr(sdbgAtributos.Columns("ID_A").Value)).ListaExterna Then
                If picDistribucion.Visible And lblDist.caption = "" Then
                    'Es obligatorio que indique previamente la distribuci�n de la compra
                    oMensajes.DistribucionObligatoria
                    Exit Sub
                Else
                    If picDistribucion.Visible Then
                        sUON1 = m_sUON1
                        sUON2 = m_sUON2
                        sUON3 = m_sUON3
                    Else
                        'Si el proceso ya tiene Distribuci�n se coge la primera que se encuentre, da igual el ambito y el porcentaje,
                        'se coge la primera que se encuentre,
                        'se necesita para pasar a la dll de integraci�n aunque despu�s en la consulta no se va a usar
                        Select Case g_oProceso.DefDistribUON
                            Case EnProceso
                                If Not g_oProceso.DistsNivel3 Is Nothing Then
                                    sUON1 = g_oProceso.DistsNivel3.Item(1).CodUON1
                                    sUON2 = g_oProceso.DistsNivel3.Item(1).CodUON2
                                    sUON3 = g_oProceso.DistsNivel3.Item(1).CodUON3
                                ElseIf Not g_oProceso.DistsNivel2 Is Nothing Then
                                    sUON1 = g_oProceso.DistsNivel2.Item(1).CodUON1
                                    sUON2 = g_oProceso.DistsNivel2.Item(1).CodUON2
                                    sUON3 = ""
                                ElseIf Not g_oProceso.DistsNivel1 Is Nothing Then
                                    sUON1 = g_oProceso.DistsNivel1.Item(1).CodUON1
                                    sUON2 = ""
                                    sUON3 = ""
                                End If
                            Case EnGrupo
                                If Not g_oProceso.Grupos Is Nothing Then
                                    If Not g_oProceso.Grupos.Item(1).DistsNivel3 Is Nothing Then
                                        sUON1 = g_oProceso.Grupos.Item(1).DistsNivel3.Item(1).CodUON1
                                        sUON2 = g_oProceso.Grupos.Item(1).DistsNivel3.Item(1).CodUON2
                                        sUON3 = g_oProceso.Grupos.Item(1).DistsNivel3.Item(1).CodUON3
                                    ElseIf Not g_oProceso.Grupos.Item(1).DistsNivel2 Is Nothing Then
                                        sUON1 = g_oProceso.Grupos.Item(1).DistsNivel2.Item(1).CodUON1
                                        sUON2 = g_oProceso.Grupos.Item(1).DistsNivel2.Item(1).CodUON2
                                        sUON3 = ""
                                    ElseIf Not g_oProceso.Grupos.Item(1).DistsNivel1 Is Nothing Then
                                        sUON1 = g_oProceso.Grupos.Item(1).DistsNivel1.Item(1).CodUON1
                                        sUON2 = ""
                                        sUON3 = ""
                                    End If
                                End If
                            Case EnItem
                                sUON1 = g_oProceso.Grupos.Item(1).Items.Item(1).UON1Cod
                                sUON2 = g_oProceso.Grupos.Item(1).Items.Item(1).UON2Cod
                                sUON3 = g_oProceso.Grupos.Item(1).Items.Item(1).UON3Cod
                        End Select
                    End If
                End If
                sValAtr = MostrarFormSelAtribListaExterna(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, _
                                                .Columns("COD").Value, .Columns("DEN").Value, .Columns("ID_A").Value, _
                                                oUsuarioSummit.Cod, oFSGSRaiz, sUON1, sUON2, sUON3, , m_oAtribEspePlantP, , g_oSolicitudSeleccionada.Id)
                If sValAtr <> "" Then
                    .Columns(.col).Value = sValAtr
                End If
            End If
    End Select
End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirFaltan", "sdbgAtributos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAtributos_KeyPress(KeyAscii As Integer)
Dim oatrib As CAtributo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbgAtributos.col = -1 Then Exit Sub
Select Case sdbgAtributos.Columns(sdbgAtributos.col).Name
    Case "VALOR"
        Set oatrib = m_oAtribEspePlantP.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
        If oatrib Is Nothing Then Exit Sub
        If oatrib.ListaExterna Then
            If KeyAscii = 8 Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = ""
            End If
        End If
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirFaltan", "sdbgAtributos_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub txtPres_Validate(Cancel As Boolean)
    If Trim(txtPres.Text) <> "" Then
        If Not IsNumeric(txtPres.Text) Then Cancel = True
    End If
End Sub

''' <summary>
''' 'Dependiendo de si hay valor en el proceso o en los grupos muestra o no los campos,configurando la pantalla
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub ConfigurarCamposNecesarios()
    Dim bFecIni As Boolean
    Dim bFecFin As Boolean
    Dim bDest As Boolean
    Dim bPago As Boolean
    Dim bUON As Boolean
    Dim bPres1 As Boolean
    Dim bPres2 As Boolean
    Dim bPres3 As Boolean
    Dim bPres4 As Boolean
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim oUsuario As CUsuario
    Dim oGRSolic As CFormGrupo
    Dim oCampo As CFormItem
    Dim oDesglose As CLineaDesglose
    Dim iNivel As String
    Dim lIdPresup As String
    Dim bFaltaDist As Boolean
    Dim bFaltaProve As Boolean
    Dim bFinBusquedaProve As Boolean
    
    'Dependiendo de si hay valor en el proceso o en los grupos muestra o no los campos,configurando la pantalla:
    
    'Destino:
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bDest = True
    Select Case g_oProceso.DefDestino
        Case EnProceso
            If NoHayParametro(g_oProceso.DestCod) Then
                bDest = False
            End If
            
        Case EnGrupo
            For Each oGrupo In g_oProceso.Grupos
                If NoHayParametro(oGrupo.DestCod) Then
                    bDest = False
                    Exit For
                End If
            Next
            
        Case EnItem
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If NoHayParametro(oItem.DestCod) Then
                            bDest = False
                            Exit For
                        End If
                    Next
                End If
            Next
            
        Case Else
            bDest = False
    End Select
    
    If Not g_oProcesoAEnviar Is Nothing Then
        If bDest = True Then
            If g_oProceso.DefDestino = EnItem Then
                If g_oProcesoAEnviar.DefDestino = EnGrupo Then
                    bDest = False
                End If
            End If
        End If
    End If
    
    'forma de pago:
    bPago = True
    Select Case g_oProceso.DefFormaPago
        Case EnProceso
            If NoHayParametro(g_oProceso.PagCod) Then
                bPago = False
            End If

        Case EnGrupo
            For Each oGrupo In g_oProceso.Grupos
                If NoHayParametro(oGrupo.PagCod) Then
                    bPago = False
                    Exit For
                End If
            Next

        Case EnItem
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If NoHayParametro(oItem.PagCod) Then
                            bPago = False
                            Exit For
                        End If
                    Next
                    If bPago = False Then Exit For
                End If
            Next
            
        Case Else
            bPago = False
    End Select
    
    If Not g_oProcesoAEnviar Is Nothing Then
        If bPago = True Then
            If g_oProceso.DefFormaPago = EnItem Then
                If g_oProcesoAEnviar.DefFormaPago = EnGrupo Then
                    bPago = False
                End If
            End If
        End If
    End If
    
    'Fecha de inicio de suministro:
    bFecIni = True
    Select Case g_oProceso.DefFechasSum
        Case EnProceso
            If NoHayParametro(g_oProceso.FechaInicioSuministro) Then bFecIni = False

        Case EnGrupo
            For Each oGrupo In g_oProceso.Grupos
                If NoHayParametro(oGrupo.FechaInicioSuministro) Then
                    bFecIni = False
                    Exit For
                End If
            Next

        Case EnItem
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If NoHayParametro(oItem.FechaInicioSuministro) Or oItem.FechaInicioSuministro = TimeValue("0:00:0") Then
                            bFecIni = False
                            Exit For
                        End If
                    Next
                    If bFecIni = False Then Exit For
                End If
            Next
            
        Case Else
            bFecIni = False
    End Select
    
    'Fecha de fin de suministro:
    bFecFin = True
     Select Case g_oProceso.DefFechasSum
        Case EnProceso
            If NoHayParametro(g_oProceso.FechaFinSuministro) Then
                bFecFin = False
                bFecIni = False
            End If
            
        Case EnGrupo
            For Each oGrupo In g_oProceso.Grupos
                If NoHayParametro(oGrupo.FechaFinSuministro) Then
                    bFecFin = False
                    bFecIni = False
                    Exit For
                End If
            Next

        Case EnItem
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If NoHayParametro(oItem.FechaFinSuministro) Or oItem.FechaFinSuministro = TimeValue("0:00:0") Then
                            bFecFin = False
                            bFecIni = False
                            Exit For
                        End If
                    Next
                    If bFecFin = False Then Exit For
                End If
            Next
            
        Case Else
            bFecFin = False
    End Select
    
    If Not g_oProcesoAEnviar Is Nothing Then
        If bFecIni = True Or bFecFin = True Then
            If g_oProceso.DefFechasSum = EnItem Then
                If g_oProcesoAEnviar.DefFechasSum = EnGrupo Then
                    bFecIni = False
                    bFecFin = False
                End If
            End If
        End If
    End If
    
    'Presupuesto 1:
    bPres1 = True
    If gParametrosGenerales.gbUsarPres1 = True And gParametrosGenerales.gbOBLPP = True Then
        Select Case g_oProceso.DefPresAnualTipo1
            Case EnProceso
                For Each oCampo In g_oSolicitudSeleccionada.Grupos.Item(1).Campos
                    If oCampo.CampoGS = TipoCampoGS.Pres1 And NoHayParametro(oCampo.valorText) Then
                        bPres1 = False
                        Exit For
                    End If
                Next
                
            Case EnGrupo
                For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
                    If oGRSolic.Orden > 1 Then
                        For Each oCampo In oGRSolic.Campos
                            If oCampo.CampoGS = TipoCampoGS.Pres1 And NoHayParametro(oCampo.valorText) Then
                                bPres1 = False
                                Exit For
                            End If
                        Next
                    End If
                    If bPres1 = False Then Exit For
                Next
                
            Case EnItem
                For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
                    For Each oCampo In oGRSolic.Campos
                        If Not oCampo.LineasDesglose Is Nothing Then
                            For Each oDesglose In oCampo.LineasDesglose
                                If oDesglose.CampoHijo.CampoGS = TipoCampoGS.Pres1 And NoHayParametro(oDesglose.valorText) Then
                                    For Each oGrupo In g_oProceso.Grupos
                                        If oGrupo.IdGrupoSolicit = oGRSolic.Id Then
                                            If oGrupo.Items.Item(CStr(oDesglose.Linea)) Is Nothing Then
                                                bPres1 = False
                                            End If
                                        End If
                                    Next
                                    If bPres1 = False Then Exit For
                                End If
                            Next
                        End If
                        If bPres1 = False Then Exit For
                    Next
                    If bPres1 = False Then Exit For
                Next
                
            Case Else
                bPres1 = False
        End Select
    End If
    
    'Presupuesto 2:
    bPres2 = True
    If gParametrosGenerales.gbUsarPres2 = True And gParametrosGenerales.gbOBLPC = True Then
        Select Case g_oProceso.DefPresAnualTipo2
            Case EnProceso
                For Each oCampo In g_oSolicitudSeleccionada.Grupos.Item(1).Campos
                    If oCampo.CampoGS = TipoCampoGS.Pres2 And NoHayParametro(oCampo.valorText) Then
                        bPres2 = False
                        Exit For
                    End If
                Next
                
            Case EnGrupo
                For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
                    If oGRSolic.Orden > 1 Then
                        For Each oCampo In oGRSolic.Campos
                            If oCampo.CampoGS = TipoCampoGS.Pres2 And NoHayParametro(oCampo.valorText) Then
                                bPres2 = False
                                Exit For
                            End If
                        Next
                    End If
                    If bPres2 = False Then Exit For
                Next
                
            Case EnItem
                For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
                    For Each oCampo In oGRSolic.Campos
                        If Not oCampo.LineasDesglose Is Nothing Then
                            For Each oDesglose In oCampo.LineasDesglose
                                If oDesglose.CampoHijo.CampoGS = TipoCampoGS.Pres2 And NoHayParametro(oDesglose.valorText) Then
                                    For Each oGrupo In g_oProceso.Grupos
                                        If oGrupo.IdGrupoSolicit = oGRSolic.Id Then
                                            If oGrupo.Items.Item(CStr(oDesglose.Linea)) Is Nothing Then
                                                bPres2 = False
                                            End If
                                        End If
                                    Next
                                    If bPres2 = False Then Exit For
                                End If
                            Next
                        End If
                        If bPres2 = False Then Exit For
                    Next
                    If bPres2 = False Then Exit For
                Next
                
            Case Else
                bPres2 = False
        End Select
    End If
    
    'Presupuesto 3:
    bPres3 = True
    If gParametrosGenerales.gbUsarPres3 = True And gParametrosGenerales.gbOBLPres3 = True Then
        Select Case g_oProceso.DefPresTipo1
            Case EnProceso
                For Each oCampo In g_oSolicitudSeleccionada.Grupos.Item(1).Campos
                    If oCampo.CampoGS = TipoCampoGS.Pres3 And NoHayParametro(oCampo.valorText) Then
                        bPres3 = False
                        Exit For
                    End If
                Next
                
            Case EnGrupo
                For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
                    If oGRSolic.Orden > 1 Then
                        For Each oCampo In oGRSolic.Campos
                            If oCampo.CampoGS = TipoCampoGS.Pres3 And NoHayParametro(oCampo.valorText) Then
                                bPres3 = False
                                Exit For
                            End If
                        Next
                    End If
                    If bPres3 = False Then Exit For
                Next
                
            Case EnItem
                For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
                    For Each oCampo In oGRSolic.Campos
                        If Not oCampo.LineasDesglose Is Nothing Then
                            For Each oDesglose In oCampo.LineasDesglose
                                If oDesglose.CampoHijo.CampoGS = TipoCampoGS.Pres3 And NoHayParametro(oDesglose.valorText) Then
                                    For Each oGrupo In g_oProceso.Grupos
                                        If oGrupo.IdGrupoSolicit = oGRSolic.Id Then
                                            If oGrupo.Items.Item(CStr(oDesglose.Linea)) Is Nothing Then
                                                bPres3 = False
                                            End If
                                        End If
                                    Next
                                    If bPres3 = False Then Exit For
                                End If
                            Next
                        End If
                        If bPres3 = False Then Exit For
                    Next
                    If bPres3 = False Then Exit For
                Next
                
            Case Else
                bPres3 = False
        End Select
    End If
    
    'Presupuesto 4:
    bPres4 = True
    If gParametrosGenerales.gbUsarPres4 = True And gParametrosGenerales.gbOBLPres4 = True Then
        Select Case g_oProceso.DefPresTipo2
            Case EnProceso
                For Each oCampo In g_oSolicitudSeleccionada.Grupos.Item(1).Campos
                    If oCampo.CampoGS = TipoCampoGS.Pres4 And NoHayParametro(oCampo.valorText) Then
                        bPres4 = False
                        Exit For
                    End If
                Next
                
            Case EnGrupo
                For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
                    If oGRSolic.Orden > 1 Then
                        For Each oCampo In oGRSolic.Campos
                            If oCampo.CampoGS = TipoCampoGS.Pres4 And NoHayParametro(oCampo.valorText) Then
                                bPres4 = False
                                Exit For
                            End If
                        Next
                    End If
                    If bPres4 = False Then Exit For
                Next
                
            Case EnItem
                For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
                    For Each oCampo In oGRSolic.Campos
                        If Not oCampo.LineasDesglose Is Nothing Then
                            For Each oDesglose In oCampo.LineasDesglose
                                If oDesglose.CampoHijo.CampoGS = TipoCampoGS.Pres4 And NoHayParametro(oDesglose.valorText) Then
                                    For Each oGrupo In g_oProceso.Grupos
                                        If oGrupo.IdGrupoSolicit = oGRSolic.Id Then
                                            If oGrupo.Items.Item(CStr(oDesglose.Linea)) Is Nothing Then
                                                bPres4 = False
                                            End If
                                        End If
                                    Next
                                    If bPres4 = False Then Exit For
                                End If
                            Next
                        End If
                        If bPres4 = False Then Exit For
                    Next
                    If bPres4 = False Then Exit For
                Next
                
            Case Else
                bPres4 = False
        End Select
    End If
    
    'Configura la pantalla:
    If bFecIni Then
        Me.picFecIni.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.picDistribucion.Top = Me.picPres4.Top
        Me.picPres4.Top = Me.picPres3.Top
        Me.picPres3.Top = Me.picPres2.Top
        Me.picPres2.Top = Me.picPres1.Top
        Me.picPres1.Top = Me.picPago.Top
        Me.picPago.Top = Me.picDestino.Top
        Me.picDestino.Top = Me.picFecFin.Top
        Me.picFecFin.Top = Me.picFecIni.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    Else
        Select Case g_oProceso.DefFechasSum
            Case TipoDefinicionDatoProceso.EnProceso
                If Not IsNull(g_oProceso.FechaInicioSuministro) And Not IsEmpty(g_oProceso.FechaInicioSuministro) Then
                    'Es posible que se visualice porque la fecha de fin no tiene valor
                    txtFecIni.Text = Format(g_oProceso.FechaInicioSuministro, "Short Date")
                ElseIf Not IsNull(g_oProceso.FechaNecesidad) And Not IsEmpty(g_oProceso.FechaNecesidad) Then
                    txtFecIni.Text = Format(g_oProceso.FechaNecesidad, "Short Date")
                Else
                    txtFecIni.Text = Format(Now, "Short Date")
                End If
            Case TipoDefinicionDatoProceso.EnGrupo
                For Each oGrupo In g_oProceso.Grupos
                    If Not IsNull(oGrupo.FechaInicioSuministro) And Not IsEmpty(oGrupo.FechaInicioSuministro) Then
                        'Es posible que se visualice porque la fecha de fin no tiene valor
                        txtFecIni.Text = Format(oGrupo.FechaInicioSuministro, "Short Date")
                        Exit For
                    Else
                        If Not IsNull(g_oProceso.FechaInicioSuministro) And Not IsEmpty(g_oProceso.FechaInicioSuministro) Then
                            'Es posible que las fechas est�n definidas a nivel de grupo pero informadas a nivel de proceso en la solicitud
                            txtFecIni.Text = Format(g_oProceso.FechaInicioSuministro, "Short Date")
                        Else
                            txtFecIni.Text = Format(Now, "Short Date")
                        End If
                    End If
                Next
                Set oGrupo = Nothing
            Case TipoDefinicionDatoProceso.EnItem
                If Not IsNull(g_oProceso.FechaInicioSuministro) And Not IsEmpty(g_oProceso.FechaInicioSuministro) Then
                    'Es posible que las fechas est�n definidas a nivel de �tem pero informadas a nivel de proceso en la solicitud
                    txtFecIni.Text = Format(g_oProceso.FechaInicioSuministro, "Short Date")
                Else
                    txtFecIni.Text = Format(Now, "Short Date")
                End If
        End Select
    End If
    
    If bFecFin Then
        Me.picFecFin.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.picDistribucion.Top = Me.picPres4.Top
        Me.picPres4.Top = Me.picPres3.Top
        Me.picPres3.Top = Me.picPres2.Top
        Me.picPres2.Top = Me.picPres1.Top
        Me.picPres1.Top = Me.picPago.Top
        Me.picPago.Top = Me.picDestino.Top
        Me.picDestino.Top = Me.picFecFin.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    Else
        Select Case g_oProceso.DefFechasSum
            Case TipoDefinicionDatoProceso.EnProceso
                If Not IsNull(g_oProceso.FechaInicioSuministro) And Not IsEmpty(g_oProceso.FechaInicioSuministro) Then
                    txtFecFin.Text = Format(g_oProceso.FechaInicioSuministro, "Short Date")
                ElseIf txtFecIni.Text <> "" Then
                    txtFecFin.Text = txtFecIni.Text
                Else
                    txtFecFin.Text = Format(Now, "Short Date")
                End If
            Case TipoDefinicionDatoProceso.EnGrupo
                For Each oGrupo In g_oProceso.Grupos
                    If Not IsNull(oGrupo.FechaInicioSuministro) And Not IsEmpty(oGrupo.FechaInicioSuministro) Then
                        'Es posible que se visualice porque la fecha de fin no tiene valor
                        txtFecFin.Text = Format(oGrupo.FechaInicioSuministro, "Short Date")
                        Exit For
                    Else
                        If Not IsNull(g_oProceso.FechaFinSuministro) And Not IsEmpty(g_oProceso.FechaFinSuministro) Then
                            'Es posible que las fechas est�n definidas a nivel de �tem pero informadas a nivel de proceso en la solicitud
                            txtFecFin.Text = Format(g_oProceso.FechaFinSuministro, "Short Date")
                        ElseIf Not IsNull(g_oProceso.FechaInicioSuministro) And Not IsEmpty(g_oProceso.FechaInicioSuministro) Then
                            txtFecFin.Text = Format(g_oProceso.FechaInicioSuministro, "Short Date")
                        Else
                            txtFecFin.Text = Format(Now, "Short Date")
                        End If
                    End If
                Next
                Set oGrupo = Nothing
            Case TipoDefinicionDatoProceso.EnItem
                If Not IsNull(g_oProceso.FechaFinSuministro) And Not IsEmpty(g_oProceso.FechaFinSuministro) Then
                    'Es posible que las fechas est�n definidas a nivel de �tem pero informadas a nivel de proceso en la solicitud
                    txtFecFin.Text = Format(g_oProceso.FechaFinSuministro, "Short Date")
                ElseIf Not IsNull(g_oProceso.FechaInicioSuministro) And Not IsEmpty(g_oProceso.FechaInicioSuministro) Then
                    txtFecFin.Text = Format(g_oProceso.FechaInicioSuministro, "Short Date")
                Else
                    txtFecFin.Text = Format(Now, "Short Date")
                End If
        End Select
    End If
    
    If bDest Then
        Me.picDestino.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.picDistribucion.Top = Me.picPres4.Top
        Me.picPres4.Top = Me.picPres3.Top
        Me.picPres3.Top = Me.picPres2.Top
        Me.picPres2.Top = Me.picPres1.Top
        Me.picPres1.Top = Me.picPago.Top
        Me.picPago.Top = Me.picDestino.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    Else
        'Destino y forma de pago por defecto la del usuario:
        sdbcDestCod.Value = gParametrosInstalacion.gsDestino
        sdbcDestCod_Validate False
    End If
    
    If bPago Then
        Me.picPago.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.picDistribucion.Top = Me.picPres4.Top
        Me.picPres4.Top = Me.picPres3.Top
        Me.picPres3.Top = Me.picPres2.Top
        Me.picPres2.Top = Me.picPres1.Top
        Me.picPres1.Top = Me.picPago.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    Else
        'Forma de pago por defecto la del usuario:
        sdbcPagoCod.Value = gParametrosInstalacion.gsFormaPago
        sdbcPagoCod_Validate False
    End If
    
    
    If (bPres1 = False Or bPres2 = False Or bPres3 = False Or bPres4 = False) And basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        Set oUsuario = oFSGSRaiz.generar_cusuario
        oUsuario.Cod = basOptimizacion.gvarCodUsuario
        oUsuario.ExpandirUsuario
    End If
    
    If bPres1 = True Then
        Me.picPres1.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.picDistribucion.Top = Me.picPres4.Top
        Me.picPres4.Top = Me.picPres3.Top
        Me.picPres3.Top = Me.picPres2.Top
        Me.picPres2.Top = Me.picPres1.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    ElseIf basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        'Presupuesto de nivel 1 definido para el usuario en seguridad del usuario
        If Not IsNull(oUsuario.FSWSPres1) Then
            
            iNivel = Left(CStr(oUsuario.FSWSPres1), 1)
            lIdPresup = Mid(CStr(oUsuario.FSWSPres1), 2)
            
            lblPres(0).caption = GenerarDenominacionPresupuesto(iNivel & "_" & lIdPresup & "_1", 1)
        End If
    End If
    
    If bPres2 = True Then
        Me.picPres2.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.picDistribucion.Top = Me.picPres4.Top
        Me.picPres4.Top = Me.picPres3.Top
        Me.picPres3.Top = Me.picPres2.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    ElseIf basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        'Presupuesto de nivel 2 definido para el usuario en seguridad del usuario
        If Not IsNull(oUsuario.FSWSPres2) Then
            iNivel = Left(CStr(oUsuario.FSWSPres2), 1)
            lIdPresup = Mid(CStr(oUsuario.FSWSPres2), 2)
            lblPres(1).caption = GenerarDenominacionPresupuesto(iNivel & "_" & lIdPresup & "_1", 2)
        End If
    End If
    
    If bPres3 = True Then
        Me.picPres3.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.picDistribucion.Top = Me.picPres4.Top
        Me.picPres4.Top = Me.picPres3.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    ElseIf basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        'Presupuesto de nivel 3 definido para el usuario en seguridad del usuario
        If Not IsNull(oUsuario.FSWSPres3) Then
            iNivel = Left(CStr(oUsuario.FSWSPres3), 1)
            lIdPresup = Mid(CStr(oUsuario.FSWSPres3), 2)
            lblPres(2).caption = GenerarDenominacionPresupuesto(iNivel & "_" & lIdPresup & "_1", 3)
        End If
    End If
    
    If bPres4 = True Then
        Me.picPres4.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.picDistribucion.Top = Me.picPres4.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    ElseIf basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        'Presupuesto de nivel 4 definido para el usuario en seguridad del usuario
        If Not IsNull(oUsuario.FSWSPres4) Then
            iNivel = Left(CStr(oUsuario.FSWSPres4), 1)
            lIdPresup = Mid(CStr(oUsuario.FSWSPres4), 2)
            lblPres(3).caption = GenerarDenominacionPresupuesto(iNivel & "_" & lIdPresup & "_1", 4)
        End If
    End If
    
    Set oUsuario = Nothing
    
    

    'Unidad Organizativa:
    '--------------------
    'bFaltaDist = True en aquellos que no ten�an campos v�lidos y el peticionario asignado tampoco cumpl�a con el nivel m�nimo
    bUON = True
    bFaltaDist = False
    
    Select Case g_oProceso.DefDistribUON
    Case EnProceso
        If g_oProceso.DistsNivel1 Is Nothing And g_oProceso.DistsNivel2 Is Nothing And g_oProceso.DistsNivel3 Is Nothing Then bFaltaDist = True
    
    Case EnGrupo
        If Not g_oProceso.Grupos Is Nothing Then
            For Each oGrupo In g_oProceso.Grupos
                If oGrupo.DistsNivel1 Is Nothing And oGrupo.DistsNivel2 Is Nothing And oGrupo.DistsNivel3 Is Nothing Then bFaltaDist = True
            Next
        End If
    
    Case EnItem
        If Not g_oProceso.Grupos Is Nothing Then
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If NoHayParametro(oItem.UON1Cod) And NoHayParametro(oItem.UON2Cod) And NoHayParametro(oItem.UON3Cod) Then bFaltaDist = True
                    Next
                End If
            Next
        End If
    
    Case Else
        bFaltaDist = True
    
    End Select
    

    If Not g_oProcesoAEnviar Is Nothing Then
        If bUON = True Then
            If g_oProceso.DefDistribUON = EnItem Then
                If g_oProcesoAEnviar.DefDistribUON = EnGrupo Then
                    bUON = False
                End If
            End If
        End If
    End If

   
    'Si faltaba la Distribuci�n, se solicita en segunda pantalla del wizard
    If bFaltaDist Then
        picDistribucion.Visible = True
    Else
        picDistribucion.Visible = False
        picProveedor.Top = picDistribucion.Top
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    End If
    
    'Proveedor
    bFaltaProve = False
    If g_bGenerarPedido_VerProve Then
        bFaltaProve = True
        bFinBusquedaProve = False
        
        For Each oGRSolic In g_oSolicitudSeleccionada.Grupos
            For Each oCampo In oGRSolic.Campos
                If oCampo.CampoGS = TipoCampoGS.Proveedor Then
                    If NoHayParametro(oCampo.valorText) Then
                        bFaltaProve = True
                    Else
                        g_oProceso.ProveActual = oCampo.valorText
                        bFaltaProve = False
                    End If
                    bFinBusquedaProve = True
                    
                    If Not g_oProceso.Grupos Is Nothing Then
                        For Each oGrupo In g_oProceso.Grupos
                            If Not oGrupo.Items Is Nothing Then
                                For Each oItem In oGrupo.Items
                                    oItem.ProveAct = oCampo.valorText
                                Next
                            End If
                        Next
                    End If
                    
                    Exit For
                End If
            Next
            If bFinBusquedaProve Then Exit For
        Next
    End If
    
    If bFaltaProve Then
        Me.picProveedor.Visible = True
    Else
        Me.picProveedor.Visible = False
        Me.Height = Me.Height - 440
        fraPres.Top = fraPres.Top - 440
    End If
    
    '3275
    If g_bMostrarAtribEspePlatillaProceso = True Then
        
        EliminarAtribEspePlantillaDelProceso
        
        AnyadirAtribEspeObligatoriosPlantillaAProceso
    Else
        picAtrbEsp.Visible = False
        'Botonera
        picNavigate.Top = fraPres.Top + fraPres.Height + 200
    End If
    
    lblMon.caption = gParametrosGenerales.gsMONCEN
    
    'Botonera
    'picNavigate.Top = fraPres.Top + fraPres.Height + 200
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "ConfigurarCamposNecesarios", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Abrir Proceso
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub AbrirProceso()
    Dim teserror As TipoErrorSummit
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim iTipoPrecio As Integer
    Dim iActualizarPrecio As Integer
    Dim bEMail As Boolean
    Dim lIdPlant As Long
    Dim oMat As CGruposMatNivel4
    Dim i As Integer
    Dim bEncontrado As Boolean
    Dim bBloqueoEtapa As Boolean
    Dim bBloqueoCondiciones As Boolean
        
    '*********** Comprueba que se han rellenado todos los campos necesarios *******************
    'Destino:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If picDestino.Visible = True Then
        If sdbcDestCod.Text = "" Then
            oMensajes.FaltanDatos (m_sIdiDestino)
            Exit Sub
        End If
        Select Case g_oProceso.DefDestino
            Case EnProceso
                g_oProceso.DestCod = sdbcDestCod.Columns(0).Value
            Case EnGrupo
                For Each oGrupo In g_oProceso.Grupos
                    If NoHayParametro(oGrupo.DestCod) Then
                        oGrupo.DestCod = sdbcDestCod.Columns(0).Value
                    End If
                Next
            Case EnItem
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If NoHayParametro(oItem.DestCod) Then
                                oItem.DestCod = sdbcDestCod.Columns(0).Value
                            End If
                        Next
                    End If
                Next
        End Select
    End If
    
    'Forma de pago:
    If picPago.Visible = True Then
        If sdbcPagoCod.Text = "" Then
            oMensajes.FaltanDatos (m_sIdiFPago)
            Exit Sub
        End If
        Select Case g_oProceso.DefFormaPago
            Case EnProceso
                g_oProceso.PagCod = sdbcPagoCod.Columns(0).Value
            Case EnGrupo
                For Each oGrupo In g_oProceso.Grupos
                    If NoHayParametro(oGrupo.PagCod) Then
                        oGrupo.PagCod = sdbcPagoCod.Columns(0).Value
                    End If
                Next
            Case EnItem
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If NoHayParametro(oItem.PagCod) Then
                                oItem.PagCod = sdbcPagoCod.Columns(0).Value
                            End If
                        Next
                    End If
                Next
        End Select
    End If
    
    'Fecha de inicio de suministro:
    If picFecIni.Visible = True Then
        If Trim(txtFecIni.Text) = "" Or Not IsDate(Trim(txtFecIni.Text)) Then
            oMensajes.FaltanDatos (m_sIdiFecIni)
            Exit Sub
        End If
        If picFecFin.Visible = False Then
            Select Case g_oProceso.DefFechasSum
                Case EnProceso
                    If CDate(Trim(txtFecIni.Text)) > CDate(g_oProceso.FechaFinSuministro) Then
                        oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, g_oProceso.FechaFinSuministro
                        If Me.Visible Then txtFecIni.SetFocus
                        Exit Sub
                    Else
                        g_oProceso.FechaInicioSuministro = Trim(txtFecIni.Text)
                    End If
                
                    
                Case EnGrupo
                    For Each oGrupo In g_oProceso.Grupos
                        If NoHayParametro(oGrupo.FechaInicioSuministro) Then
                            If CDate(Trim(txtFecIni.Text)) > CDate(oGrupo.FechaFinSuministro) Then
                                oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, oGrupo.FechaFinSuministro
                                If Me.Visible Then txtFecIni.SetFocus
                                Exit Sub
                            Else
                                oGrupo.FechaInicioSuministro = Trim(txtFecIni.Text)
                            End If
                        End If
                    Next
                Case EnItem
                    For Each oGrupo In g_oProceso.Grupos
                        If Not oGrupo.Items Is Nothing Then
                            For Each oItem In oGrupo.Items
                                If NoHayParametro(oItem.FechaInicioSuministro) Or oItem.FechaInicioSuministro = TimeValue("0:00:0") Then
                                    If CDate(Trim(txtFecIni.Text)) > CDate(oItem.FechaFinSuministro) Then
                                        oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, oItem.FechaFinSuministro
                                        If Me.Visible Then txtFecIni.SetFocus
                                        Exit Sub
                                    Else
                                        oItem.FechaInicioSuministro = Trim(txtFecIni.Text)
                                    End If
                                End If
                            Next
                        End If
                    Next
            End Select
        
        Else
            If Trim(txtFecFin.Text) = "" Or Not IsDate(Trim(txtFecFin.Text)) Then
                oMensajes.FaltanDatos (m_sIdiFecFin)
                Exit Sub
            End If
            If CDate(Trim(txtFecIni.Text)) > CDate(Trim(txtFecFin.Text)) Then
                oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, txtFecFin.Text
                If Me.Visible Then txtFecIni.SetFocus
                Exit Sub
            Else
                Select Case g_oProceso.DefFechasSum
                Case EnProceso
                    g_oProceso.FechaInicioSuministro = Trim(txtFecIni.Text)
                    g_oProceso.FechaFinSuministro = Trim(txtFecFin.Text)
                Case EnGrupo
                    For Each oGrupo In g_oProceso.Grupos
                        If NoHayParametro(oGrupo.FechaInicioSuministro) Then
                            If NoHayParametro(oGrupo.FechaFinSuministro) Then
                                oGrupo.FechaInicioSuministro = Trim(txtFecIni.Text)
                                oGrupo.FechaFinSuministro = Trim(txtFecFin.Text)
                            ElseIf CDate(Trim(txtFecIni.Text)) > CDate(oGrupo.FechaFinSuministro) Then
                                oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, oGrupo.FechaFinSuministro
                                If Me.Visible Then txtFecIni.SetFocus
                                Exit Sub
                            Else
                                oGrupo.FechaInicioSuministro = Trim(txtFecIni.Text)
                            End If
                        ElseIf NoHayParametro(oGrupo.FechaFinSuministro) Then
                            If CDate(oGrupo.FechaInicioSuministro) > CDate(Trim(txtFecFin.Text)) Then
                                oMensajes.FechaSuministrosDesdeMayorFechaHasta oGrupo.FechaInicioSuministro, txtFecFin.Text
                                If Me.Visible Then txtFecFin.SetFocus
                                Exit Sub
                            Else
                                oGrupo.FechaFinSuministro = Trim(txtFecFin.Text)
                            End If
                        End If
                    Next
                Case EnItem
                    For Each oGrupo In g_oProceso.Grupos
                        If Not oGrupo.Items Is Nothing Then
                            For Each oItem In oGrupo.Items
                                oItem.FechaInicioSuministro = Trim(txtFecIni.Text)
                                oItem.FechaFinSuministro = Trim(txtFecFin.Text)
                            Next
                        End If
                    Next
                
                End Select
            End If
        
        End If
    
  
    'Fecha de fin de suministro:
    ElseIf picFecFin.Visible = True Then
        If Trim(txtFecFin.Text) = "" Or Not IsDate(Trim(txtFecFin.Text)) Then
            oMensajes.FaltanDatos (m_sIdiFecFin)
            Exit Sub
        End If
        Select Case g_oProceso.DefFechasSum
            Case EnProceso
                If CDate(g_oProceso.FechaInicioSuministro) > CDate(Trim(txtFecFin.Text)) Then
                    oMensajes.FechaSuministrosDesdeMayorFechaHasta g_oProceso.FechaInicioSuministro, txtFecFin.Text
                    If Me.Visible Then txtFecFin.SetFocus
                    Exit Sub
                Else
                    g_oProceso.FechaFinSuministro = Trim(txtFecFin.Text)
                End If
                
            Case EnGrupo
                For Each oGrupo In g_oProceso.Grupos
                    If NoHayParametro(oGrupo.FechaFinSuministro) Then
                        If CDate(oGrupo.FechaInicioSuministro) > CDate(Trim(txtFecFin.Text)) Then
                            oMensajes.FechaSuministrosDesdeMayorFechaHasta oGrupo.FechaInicioSuministro, txtFecFin.Text
                            If Me.Visible Then txtFecFin.SetFocus
                            Exit Sub
                        Else
                            oGrupo.FechaFinSuministro = Trim(txtFecFin.Text)
                        End If
                    End If
                Next
            Case EnItem
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If NoHayParametro(oItem.FechaFinSuministro) Or oItem.FechaFinSuministro = TimeValue("0:00:0") Then
                                If CDate(oItem.FechaInicioSuministro) > CDate(Trim(txtFecFin.Text)) Then
                                    oMensajes.FechaSuministrosDesdeMayorFechaHasta oItem.FechaInicioSuministro, txtFecFin.Text
                                    If Me.Visible Then txtFecFin.SetFocus
                                    Exit Sub
                                Else
                                    oItem.FechaFinSuministro = Trim(txtFecFin.Text)
                                End If
                            End If
                        Next
                    End If
                Next
        End Select
    End If
    

    
    '***********************************************************************************************
    'Si el destino est� a nivel de proceso o grupo los items tendr�n esos valores
    If g_oProceso.DefDestino = EnGrupo Or g_oProceso.DefDestino = EnProceso Then
        If Not g_oProceso.Grupos Is Nothing Then
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If g_oProceso.DefDestino = EnGrupo Then
                            oItem.DestCod = oGrupo.DestCod
                        Else
                            oItem.DestCod = g_oProceso.DestCod
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    'Si la forma de pago est� a nivel de proceso o grupo los items tendr�n esos valores
    If g_oProceso.DefFormaPago = EnGrupo Or g_oProceso.DefFormaPago = EnProceso Then
        If Not g_oProceso.Grupos Is Nothing Then
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If g_oProceso.DefFormaPago = EnGrupo Then
                            oItem.PagCod = oGrupo.PagCod
                        Else
                            oItem.PagCod = g_oProceso.PagCod
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    'Si las fechas de inicio y fin de suministro est�n a nivel de proceso o grupo los items tendr�n esos valores
    If g_oProceso.DefFechasSum = EnGrupo Or g_oProceso.DefFechasSum = EnProceso Then
        If Not g_oProceso.Grupos Is Nothing Then
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If g_oProceso.DefFechasSum = EnGrupo Then
                            oItem.FechaInicioSuministro = oGrupo.FechaInicioSuministro
                            oItem.FechaFinSuministro = oGrupo.FechaFinSuministro
                        Else
                            oItem.FechaInicioSuministro = g_oProceso.FechaInicioSuministro
                            oItem.FechaFinSuministro = g_oProceso.FechaFinSuministro
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    '***********************************************************************************************
    'Precios:
    If optPresItem(0).Value = True Then iActualizarPrecio = 0
    If optPresItem(1).Value = True Then
        iActualizarPrecio = 1
        If optCompartida(0).Value = True Then iTipoPrecio = 0
        If optCompartida(1).Value = True Then iTipoPrecio = 1
        If optCompartida(2).Value = True Then iTipoPrecio = 2
        If optCompartida(3).Value = True Then iTipoPrecio = 3
    End If
    If optPresItem(2).Value = True Then
        iActualizarPrecio = 2
        If txtPres.Text = "" Then
            If Not g_oProceso.Grupos Is Nothing Then
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If Not IsNumeric(oItem.Precio) Then
                                oMensajes.NoValido lblLineas.caption
                                Exit Sub
        
                            End If
                        Next
                    End If
                Next
            End If
        End If
    End If
    
    'Materiales del proceso
     Set oMat = oFSGSRaiz.Generar_CGruposMatNivel4
     If Not g_oProceso.Grupos Is Nothing Then
        For Each oGrupo In g_oProceso.Grupos
            If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    If oMat.Count = 0 Then
                        oMat.Add oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, "", "", "", oItem.GMN4Cod, ""
                    Else
                        'Si ya existe el material no se vuelve a meter.
                        bEncontrado = False
                        For i = 1 To oMat.Count
                            If oMat.Item(i).GMN1Cod = oItem.GMN1Cod And oMat.Item(i).GMN2Cod = oItem.GMN2Cod And oMat.Item(i).GMN3Cod = oItem.GMN3Cod And oMat.Item(i).Cod = oItem.GMN4Cod Then
                               bEncontrado = True
                            End If
                        Next
                        If Not bEncontrado Then
                            oMat.Add oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, "", "", "", oItem.GMN4Cod, ""
                        End If
                    End If
                Next
            End If
        Next
     End If
   
    'Genera el proceso de compra:
    If g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Pendiente Then
        bEMail = True
    End If

    Dim sEspNoExisten As String
    
    Screen.MousePointer = vbHourglass

    lIdPlant = -1
    If Not (g_oPlantillaSeleccionada Is Nothing) Then
        If IsNumeric(g_oPlantillaSeleccionada.Id) Then
            lIdPlant = g_oPlantillaSeleccionada.Id
        End If
    End If

    g_oProceso.DesdeSolicitud = 1
    g_oProceso.PermSaltarseVolMaxAdjDir = m_bValSaltarseVolMaxAdjDir
           
    If gParametrosGenerales.gbSolicitudesCompras Then
        bBloqueoEtapa = False
        bBloqueoCondiciones = False
                                        
        'Se comprueban las condiciones de bloqueo por etapa
        For i = 0 To UBound(g_arSolicitudes)
            bBloqueoEtapa = BloqueoEtapa(g_arSolicitudes)
            If bBloqueoEtapa Then
                oMensajes.MensajeOKOnly 897, Exclamation  'Existen condiciones de bloqueo.
                Exit For
            End If
        Next
                            
        If Not bBloqueoEtapa Then
            'Se comprueban las condiciones de bloqueo por f�rmula
            bBloqueoCondiciones = BloqueoCondiciones(g_arSolicitudes, TipoBloqueo.Apertura, iActualizarPrecio, iTipoPrecio, g_oProceso)
        End If
        
        If bBloqueoEtapa Or bBloqueoCondiciones Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If

    Screen.MousePointer = vbHourglass
    
    Dim strSessionId As String
    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
        
    teserror = g_oProceso.GenerarProcesoDesdeSolicitud(g_oSolicitudSeleccionada.Id, iActualizarPrecio, iTipoPrecio _
        , g_bTraspasarAdjuntos, g_bTraspasarAtributos, IIf(iActualizarPrecio = 2, txtPres.Text, Null), m_vPresup1 _
        , m_vPresup2, m_vPresup3, m_vPresup4, sEspNoExisten, basOptimizacion.gvarCodUsuario, lIdPlant, g_bGruposPlantillaCoinciden _
        , basOptimizacion.gCodPersonaUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario _
        , basOptimizacion.gCodDepUsuario, oMat, ((g_bGenerarPedido_VerProve = False) And (g_bGenerarPedido_EstasGenerando = True)), g_oSolicitudSeleccionada.Estado, strSessionId)

    Screen.MousePointer = vbNormal
    
    If sEspNoExisten <> "" Then
        sEspNoExisten = Left(sEspNoExisten, Len(sEspNoExisten) - 1)
        Dim arrArchivos() As String
        arrArchivos = Split(sEspNoExisten, ",")
        oMensajes.ImposibleAccederAFicheroEnAltaProcesoDesdeSolicitud arrArchivos
    End If
         
    If teserror.NumError = TESnoerror Or teserror.NumError = TESSolicitudAbrirMaxAdjDir Or teserror.NumError = TESSolicitudAbrirMaxAdjDirPermiso Then
        If teserror.NumError = TESSolicitudAbrirMaxAdjDir Or teserror.NumError = TESSolicitudAbrirMaxAdjDirPermiso Then
            oMensajes.VolAdjDirectaSuperadoSolicitud CDbl(teserror.Arg2), CDbl(teserror.Arg1), teserror.NumError, False, g_oProceso.Anyo, g_oProceso.GMN1Cod, g_oProceso.Cod, g_bSoloAbrirProc
        ElseIf g_bSoloAbrirProc = True Then
            oMensajes.ProcesoCreadoDesdeSolicit g_oProceso.Anyo, g_oProceso.GMN1Cod, g_oProceso.Cod
        End If
        
        If bEMail Then  'Env�o de email para aprobar la solicitud:
            For i = 0 To UBound(g_arSolicitudes)
                Set frmSolicitudAprobar.g_oSolicitud = g_arSolicitudes(i)
            Next
            frmSolicitudAprobar.Show vbModal
        End If

        RegistrarAccion AccionesSummit.ACCProceCreaDesdeSol, "Anyo:" & Trim(g_oProceso.Anyo) & " Gmn1:" & Trim(g_oProceso.GMN1Cod) & " Proce:" & g_oProceso.Cod
        
        Unload Me
    Else
        basErrores.TratarError teserror
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "AbrirProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub

''' <summary>
''' Env�a la solicitud a un proceso ya existente
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub EnviarAProceso()
    Dim teserror As TipoErrorSummit
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim iTipoPrecio As Integer
    Dim iActualizarPrecio As Integer
    Dim sMensaje As String
    Dim irespuesta As Integer
    Dim bBloqueoEtapa As Boolean
    Dim bBloqueoCondiciones As Boolean
    Dim i As Integer

    'Comprueba que se han rellenado todos los campos necesarios:
    
    'Fecha de inicio de suministro:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If picFecIni.Visible = True Then
        If Trim(txtFecIni.Text) = "" Or Not IsDate(Trim(txtFecIni.Text)) Then
            oMensajes.FaltanDatos (m_sIdiFecIni)
            Exit Sub
        End If
        If picFecFin.Visible = False Then
            Select Case g_oProceso.DefFechasSum
                Case EnProceso
                    If CDate(Trim(txtFecIni.Text)) > CDate(g_oProceso.FechaFinSuministro) Then
                        oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, g_oProceso.FechaFinSuministro
                        If Me.Visible Then txtFecIni.SetFocus
                        Exit Sub
                    Else
                        g_oProceso.FechaInicioSuministro = Trim(txtFecIni.Text)
                    End If
                
                Case EnGrupo
                    For Each oGrupo In g_oProceso.Grupos
                        If IsNull(oGrupo.FechaInicioSuministro) Then
                            If CDate(Trim(txtFecIni.Text)) > CDate(oGrupo.FechaFinSuministro) Then
                                oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, oGrupo.FechaFinSuministro
                                If Me.Visible Then txtFecIni.SetFocus
                                Exit Sub
                            Else
                                oGrupo.FechaInicioSuministro = Trim(txtFecIni.Text)
                            End If
                        End If
                    Next
                Case EnItem
                    For Each oGrupo In g_oProceso.Grupos
                        If Not oGrupo.Items Is Nothing Then
                            For Each oItem In oGrupo.Items
                                If IsEmpty(oItem.FechaInicioSuministro) Or oItem.FechaInicioSuministro = TimeValue("0:00:0") Then
                                    If CDate(Trim(txtFecIni.Text)) > CDate(oItem.FechaFinSuministro) Then
                                        oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, oItem.FechaFinSuministro
                                        If Me.Visible Then txtFecIni.SetFocus
                                        Exit Sub
                                    Else
                                        oItem.FechaInicioSuministro = Trim(txtFecIni.Text)
                                    End If
                                End If
                            Next
                        End If
                    Next
            End Select
        Else
            If Trim(txtFecFin.Text) = "" Or Not IsDate(Trim(txtFecFin.Text)) Then
                oMensajes.FaltanDatos (m_sIdiFecFin)
                Exit Sub
            End If
            If CDate(txtFecIni.Text) > CDate(txtFecFin.Text) Then
                oMensajes.FechaDesdeMayorFechaHasta 2
                If Me.Visible Then txtFecIni.SetFocus
                Exit Sub
            Else
                Select Case g_oProceso.DefFechasSum
                Case EnProceso
                    g_oProceso.FechaInicioSuministro = Trim(txtFecIni.Text)
                    g_oProceso.FechaFinSuministro = Trim(txtFecFin.Text)
                Case EnGrupo
                    For Each oGrupo In g_oProceso.Grupos
                        If NoHayParametro(oGrupo.FechaInicioSuministro) Then
                            If NoHayParametro(oGrupo.FechaFinSuministro) Then
                                oGrupo.FechaInicioSuministro = Trim(txtFecIni.Text)
                                oGrupo.FechaFinSuministro = Trim(txtFecFin.Text)
                            ElseIf CDate(Trim(txtFecIni.Text)) > CDate(oGrupo.FechaFinSuministro) Then
                                oMensajes.FechaSuministrosDesdeMayorFechaHasta txtFecIni.Text, oGrupo.FechaFinSuministro
                                If Me.Visible Then txtFecIni.SetFocus
                                Exit Sub
                            Else
                                oGrupo.FechaInicioSuministro = Trim(txtFecIni.Text)
                            End If
                        ElseIf NoHayParametro(oGrupo.FechaFinSuministro) Then
                            If CDate(oGrupo.FechaInicioSuministro) > CDate(Trim(txtFecFin.Text)) Then
                                oMensajes.FechaSuministrosDesdeMayorFechaHasta oGrupo.FechaInicioSuministro, txtFecFin.Text
                                If Me.Visible Then txtFecFin.SetFocus
                                Exit Sub
                            Else
                                oGrupo.FechaFinSuministro = Trim(txtFecFin.Text)
                            End If
                        End If
                    Next
                Case EnItem
                    For Each oGrupo In g_oProceso.Grupos
                        If Not oGrupo.Items Is Nothing Then
                            For Each oItem In oGrupo.Items
                                oItem.FechaInicioSuministro = Trim(txtFecIni.Text)
                                oItem.FechaFinSuministro = Trim(txtFecFin.Text)
                            Next
                        End If
                    Next
                
                End Select
            End If
        
        End If
    
    
    'Fecha de fin de suministro:
    ElseIf picFecFin.Visible = True Then
        If Trim(txtFecFin.Text) = "" Or Not IsDate(Trim(txtFecFin.Text)) Then
            oMensajes.FaltanDatos (m_sIdiFecFin)
            Exit Sub
        End If
        Select Case g_oProceso.DefFechasSum
            Case EnProceso
                If g_oProceso.FechaInicioSuministro > txtFecFin.Text Then
                    oMensajes.FechaDesdeMayorFechaHasta 2
                    If Me.Visible Then txtFecFin.SetFocus
                    Exit Sub
                Else
                    g_oProceso.FechaFinSuministro = Trim(txtFecFin.Text)
                End If
                
            Case EnGrupo
                For Each oGrupo In g_oProceso.Grupos
                    If IsNull(oGrupo.FechaFinSuministro) Or IsEmpty(oGrupo.FechaFinSuministro) Then
                        If CDate(oGrupo.FechaInicioSuministro) > CDate(Trim(txtFecFin.Text)) Then
                            oMensajes.FechaSuministrosDesdeMayorFechaHasta oGrupo.FechaInicioSuministro, txtFecFin.Text
                            If Me.Visible Then txtFecFin.SetFocus
                            Exit Sub
                        Else
                            oGrupo.FechaFinSuministro = Trim(txtFecFin.Text)
                        End If
                    End If
                Next
            Case EnItem
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If IsEmpty(oItem.FechaFinSuministro) Or oItem.FechaFinSuministro = TimeValue("0:00:0") Then
                                If CDate(oItem.FechaInicioSuministro) > CDate(Trim(txtFecFin.Text)) Then
                                    oMensajes.FechaSuministrosDesdeMayorFechaHasta oItem.FechaInicioSuministro, txtFecFin.Text
                                    If Me.Visible Then txtFecFin.SetFocus
                                    Exit Sub
                                Else
                                    oItem.FechaFinSuministro = Trim(txtFecFin.Text)
                                End If
                            End If
                        Next
                    End If
                Next
        End Select
    End If

    'Destino:
    If picDestino.Visible = True Then
        If sdbcDestCod.Text = "" Then
            oMensajes.FaltanDatos (m_sIdiDestino)
            Exit Sub
        End If

        Select Case g_oProceso.DefDestino
            Case EnProceso
                g_oProceso.DestCod = sdbcDestCod.Columns(0).Value
            Case EnGrupo
                For Each oGrupo In g_oProceso.Grupos
                    If NoHayParametro(oGrupo.DestCod) Then
                        oGrupo.DestCod = sdbcDestCod.Columns(0).Value
                    End If
                Next
            Case EnItem
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If NoHayParametro(oItem.DestCod) Then
                                oItem.DestCod = sdbcDestCod.Columns(0).Value
                            End If
                        Next
                    End If
                Next
        End Select
    End If
    
    'Forma de pago:
    If picPago.Visible = True Then
        If sdbcPagoCod.Text = "" Then
            oMensajes.FaltanDatos (m_sIdiFPago)
            Exit Sub
        End If
                
        Select Case g_oProceso.DefFormaPago
            Case EnProceso
                g_oProceso.PagCod = sdbcPagoCod.Columns(0).Value
            Case EnGrupo
                For Each oGrupo In g_oProceso.Grupos
                    If NoHayParametro(oGrupo.PagCod) Then
                        oGrupo.PagCod = sdbcPagoCod.Columns(0).Value
                    End If
                Next
            Case EnItem
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If NoHayParametro(oItem.PagCod) Then
                                oItem.PagCod = sdbcPagoCod.Columns(0).Value
                            End If
                        Next
                    End If
                Next
        End Select
    End If
    

    '***** Comprueba el �mbito de los datos de la solicitud y los de el proceso a enviar.Puede ser necesario modificar estos �ltimos:
    sMensaje = ""
    
    Select Case g_oProceso.DefDestino
        Case EnGrupo
            If g_oProcesoAEnviar.DefDestino = EnProceso Then
                'Se cambia la definici�n de proceso a grupo (pregunta)
                sMensaje = sMensaje & vbTab & "- " & m_sMensajes2(1) & " " & m_sMensajes2(8) & vbCrLf
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefDestino = EnProceso Then
                'Se cambia la definici�n de proceso a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & m_sMensajes2(1) & " " & m_sMensajes2(9) & vbCrLf
            ElseIf g_oProcesoAEnviar.DefDestino = EnGrupo Then
                'Se cambia la definici�n de grupo a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & m_sMensajes2(1) & " " & m_sMensajes2(10) & vbCrLf
            End If
    End Select
    
    Select Case g_oProceso.DefFormaPago
        Case EnGrupo
            If g_oProcesoAEnviar.DefFormaPago = EnProceso Then
                'Se cambia la definici�n de proceso a grupo (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & m_sMensajes2(2) & " " & m_sMensajes2(8) & vbCrLf
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefFormaPago = EnProceso Then
                'Se cambia la definici�n de proceso a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & m_sMensajes2(2) & " " & m_sMensajes2(9) & vbCrLf
            ElseIf g_oProcesoAEnviar.DefFormaPago = EnGrupo Then
                'Se cambia la definici�n de grupo a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & m_sMensajes2(2) & " " & m_sMensajes2(10) & vbCrLf
            End If
    End Select
    
    Select Case g_oProceso.DefFechasSum
        Case EnGrupo
            If g_oProcesoAEnviar.DefFechasSum = EnProceso Then
                'Se cambia la definici�n de proceso a grupo (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & m_sMensajes2(3) & " " & m_sMensajes2(8) & vbCrLf
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefFechasSum = EnProceso Then
                'Se cambia la definici�n de proceso a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & m_sMensajes2(3) & " " & m_sMensajes2(9) & vbCrLf
            ElseIf g_oProcesoAEnviar.DefFechasSum = EnGrupo Then
                'Se cambia la definici�n de grupo a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & m_sMensajes2(3) & " " & m_sMensajes2(10) & vbCrLf
            End If
    End Select
    
    
    Select Case g_oProceso.DefPresAnualTipo1
        Case EnGrupo
            If g_oProcesoAEnviar.DefPresAnualTipo1 = EnProceso Then
                'Se cambia la definici�n de proceso a grupo (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres1 & " " & m_sMensajes2(8) & vbCrLf
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefPresAnualTipo1 = EnProceso Then
                'Se cambia la definici�n de proceso a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres1 & " " & m_sMensajes2(9) & vbCrLf
            ElseIf g_oProcesoAEnviar.DefPresAnualTipo1 = EnGrupo Then
                'Se cambia la definici�n de grupo a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres1 & " " & m_sMensajes2(10) & vbCrLf
            End If
    End Select
    
    Select Case g_oProceso.DefPresAnualTipo2
        Case EnGrupo
            If g_oProcesoAEnviar.DefPresAnualTipo2 = EnProceso Then
                'Se cambia la definici�n de proceso a grupo (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres2 & " " & m_sMensajes2(8) & vbCrLf
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefPresAnualTipo2 = EnProceso Then
                'Se cambia la definici�n de proceso a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres2 & " " & m_sMensajes2(9) & vbCrLf
            ElseIf g_oProcesoAEnviar.DefPresAnualTipo2 = EnGrupo Then
                'Se cambia la definici�n de grupo a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres2 & " " & m_sMensajes2(10) & vbCrLf
            End If
    End Select
    
    Select Case g_oProceso.DefPresTipo1
        Case EnGrupo
            If g_oProcesoAEnviar.DefPresTipo1 = EnProceso Then
                'Se cambia la definici�n de proceso a grupo (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres3 & " " & m_sMensajes2(8) & vbCrLf
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefPresTipo1 = EnProceso Then
                'Se cambia la definici�n de proceso a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres3 & " " & m_sMensajes2(9) & vbCrLf
            ElseIf g_oProcesoAEnviar.DefPresTipo1 = EnGrupo Then
                'Se cambia la definici�n de grupo a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres3 & " " & m_sMensajes2(10) & vbCrLf
            End If
    End Select
    
    Select Case g_oProceso.DefPresTipo2
        Case EnGrupo
            If g_oProcesoAEnviar.DefPresTipo2 = EnProceso Then
                'Se cambia la definici�n de proceso a grupo (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres4 & " " & m_sMensajes2(8) & vbCrLf
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefPresTipo2 = EnProceso Then
                'Se cambia la definici�n de proceso a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres4 & " " & m_sMensajes2(9) & vbCrLf
            ElseIf g_oProcesoAEnviar.DefPresTipo2 = EnGrupo Then
                'Se cambia la definici�n de grupo a �tem (pregunta)
                sMensaje = sMensaje & Space(4) & "-" & Space(2) & basParametros.gParametrosGenerales.gsSingPres4 & " " & m_sMensajes2(10) & vbCrLf
            End If
    End Select
    
    If sMensaje <> "" Then
        irespuesta = oMensajes.PreguntaModificarAmbitoDatos(sMensaje)
        If irespuesta = vbNo Then Exit Sub
    End If
    
    
    '***********************************************************************************************
    
    Select Case g_oProceso.DefDestino
        Case EnGrupo
            If g_oProcesoAEnviar.DefDestino = EnProceso Then
                g_oProceso.DefDestino = EnProceso
                If Not g_oProceso.Grupos Is Nothing Then
                    For Each oGrupo In g_oProceso.Grupos
                        oGrupo.DestCod = g_oProcesoAEnviar.DestCod
                    Next
                End If
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefDestino = EnProceso Then
                g_oProceso.DefDestino = EnProceso
                g_oProceso.DestCod = g_oProcesoAEnviar.DestCod
            ElseIf g_oProcesoAEnviar.DefDestino = EnGrupo Then
                g_oProceso.DefDestino = EnGrupo
                If Not g_oProceso.Grupos Is Nothing Then
                    For Each oGrupo In g_oProceso.Grupos
                        oGrupo.DestCod = sdbcDestCod.Columns(0).Value
                    Next
                End If
            End If
    End Select
    
    'Si el destino est� a nivel de proceso o grupo los items tendr�n esos valores
    If g_oProceso.DefDestino = EnGrupo Or g_oProceso.DefDestino = EnProceso Then
        If Not g_oProceso.Grupos Is Nothing Then
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If g_oProceso.DefDestino = EnGrupo Then
                            oItem.DestCod = oGrupo.DestCod
                        Else
                            oItem.DestCod = g_oProceso.DestCod
                        End If
                    Next
                End If
            Next
        End If
    End If

    Select Case g_oProceso.DefFormaPago
        Case EnGrupo
            If g_oProcesoAEnviar.DefFormaPago = EnProceso Then
                g_oProceso.DefFormaPago = EnProceso
                If Not g_oProceso.Grupos Is Nothing Then
                    For Each oGrupo In g_oProceso.Grupos
                        oGrupo.PagCod = g_oProcesoAEnviar.PagCod
                    Next
                End If
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefFormaPago = EnProceso Then
                g_oProceso.DefFormaPago = EnProceso
                g_oProceso.PagCod = g_oProcesoAEnviar.PagCod
            ElseIf g_oProcesoAEnviar.DefFormaPago = EnGrupo Then
                g_oProceso.DefFormaPago = EnGrupo
                If Not g_oProceso.Grupos Is Nothing Then
                    For Each oGrupo In g_oProceso.Grupos
                        oGrupo.PagCod = sdbcPagoCod.Columns(0).Value
                    Next
                End If
            End If
    End Select
    
    'Si la forma de pago est� a nivel de proceso o grupo los items tendr�n esos valores
    If g_oProceso.DefFormaPago = EnGrupo Or g_oProceso.DefFormaPago = EnProceso Then
        If Not g_oProceso.Grupos Is Nothing Then
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If g_oProceso.DefFormaPago = EnGrupo Then
                            oItem.PagCod = oGrupo.PagCod
                        Else
                            oItem.PagCod = g_oProceso.PagCod
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    Select Case g_oProceso.DefFechasSum
        Case EnGrupo
            If g_oProcesoAEnviar.DefFechasSum = EnProceso Then
                g_oProceso.DefFechasSum = EnProceso
                
                g_oProceso.FechaInicioSuministro = g_oProcesoAEnviar.FechaInicioSuministro
                g_oProceso.FechaFinSuministro = g_oProcesoAEnviar.FechaFinSuministro

                If Not g_oProceso.Grupos Is Nothing Then
                    For Each oGrupo In g_oProceso.Grupos
                        oGrupo.FechaInicioSuministro = g_oProcesoAEnviar.FechaInicioSuministro
                        oGrupo.FechaFinSuministro = g_oProcesoAEnviar.FechaFinSuministro
                    Next
                End If
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefFechasSum = EnProceso Then
                g_oProceso.DefFechasSum = EnProceso
                g_oProceso.FechaInicioSuministro = g_oProcesoAEnviar.FechaInicioSuministro
                g_oProceso.FechaFinSuministro = g_oProcesoAEnviar.FechaFinSuministro
            ElseIf g_oProcesoAEnviar.DefFechasSum = EnGrupo Then
                g_oProceso.DefFechasSum = EnGrupo
                If Not g_oProceso.Grupos Is Nothing Then
                    For Each oGrupo In g_oProceso.Grupos
                        oGrupo.FechaInicioSuministro = Trim(txtFecIni.Text)
                        oGrupo.FechaFinSuministro = Trim(txtFecFin.Text)
                    Next
                End If
            End If
    End Select
    
    'Si las fechas de inicio y fin de suministro est�n a nivel de proceso o grupo los items tendr�n esos valores
    If g_oProceso.DefFechasSum = EnGrupo Or g_oProceso.DefFechasSum = EnProceso Then
        If Not g_oProceso.Grupos Is Nothing Then
            For Each oGrupo In g_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If g_oProceso.DefFechasSum = EnGrupo Then
                            oItem.FechaInicioSuministro = oGrupo.FechaInicioSuministro
                            oItem.FechaFinSuministro = oGrupo.FechaFinSuministro
                        Else
                            oItem.FechaInicioSuministro = g_oProceso.FechaInicioSuministro
                            oItem.FechaFinSuministro = g_oProceso.FechaFinSuministro
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    
    Select Case g_oProceso.DefPresAnualTipo1
        Case EnGrupo
            If g_oProcesoAEnviar.DefPresAnualTipo1 = EnProceso Then
                g_oProceso.DefPresAnualTipo1 = EnProceso
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefPresAnualTipo1 = EnProceso Then
                g_oProceso.DefPresAnualTipo1 = EnProceso
            ElseIf g_oProcesoAEnviar.DefPresAnualTipo1 = EnGrupo Then
                g_oProceso.DefPresAnualTipo1 = EnGrupo
            End If
    End Select
    
    Select Case g_oProceso.DefPresAnualTipo2
        Case EnGrupo
            If g_oProcesoAEnviar.DefPresAnualTipo2 = EnProceso Then
                g_oProceso.DefPresAnualTipo2 = EnProceso
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefPresAnualTipo2 = EnProceso Then
                g_oProceso.DefPresAnualTipo2 = EnProceso
            ElseIf g_oProcesoAEnviar.DefPresAnualTipo2 = EnGrupo Then
                g_oProceso.DefPresAnualTipo2 = EnGrupo
            End If
    End Select
    
    Select Case g_oProceso.DefPresTipo1
        Case EnGrupo
            If g_oProcesoAEnviar.DefPresTipo1 = EnProceso Then
                g_oProceso.DefPresTipo1 = EnProceso
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefPresTipo1 = EnProceso Then
                g_oProceso.DefPresTipo1 = EnProceso
            ElseIf g_oProcesoAEnviar.DefPresTipo1 = EnGrupo Then
                g_oProceso.DefPresTipo1 = EnGrupo
            End If
    End Select
    
    Select Case g_oProceso.DefPresTipo2
        Case EnGrupo
            If g_oProcesoAEnviar.DefPresTipo2 = EnProceso Then
                g_oProceso.DefPresTipo2 = EnProceso
            End If
        Case EnItem
            If g_oProcesoAEnviar.DefPresTipo2 = EnProceso Then
                g_oProceso.DefPresTipo2 = EnProceso
            ElseIf g_oProcesoAEnviar.DefPresTipo2 = EnGrupo Then
                g_oProceso.DefPresTipo2 = EnGrupo
            End If
    End Select

    
    '***********************************************************************************************
    
    'Precios:
    If optPresItem(0).Value = True Then iActualizarPrecio = 0
    If optPresItem(1).Value = True Then
        iActualizarPrecio = 1
        If optCompartida(0).Value = True Then iTipoPrecio = 0
        If optCompartida(1).Value = True Then iTipoPrecio = 1
        If optCompartida(2).Value = True Then iTipoPrecio = 2
        If optCompartida(3).Value = True Then iTipoPrecio = 3
    End If
    
    If optPresItem(2).Value = True Then
        iActualizarPrecio = 2
        If txtPres.Text = "" Then
            If Not g_oProceso.Grupos Is Nothing Then
                For Each oGrupo In g_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If Not IsNumeric(oItem.Precio) Then
                                oMensajes.NoValido lblLineas.caption
                                Exit Sub
        
                            End If
                        Next
                    End If
                Next
            End If
        End If
    End If
    Dim sEspNoExisten As String
    Screen.MousePointer = vbHourglass
    
    g_oProceso.DesdeSolicitud = 1
        
    If gParametrosGenerales.gbSolicitudesCompras Then
        bBloqueoEtapa = False
        bBloqueoCondiciones = False
                        
        'frmSOLAbrirFaltan se abre cuando se abre un proceso desde solicitud o se env�a la solicitud a un proceso existente
        'Se pueden abrir un proceso desde varias solicitudes, pero s�lo se puede enviar a proceso una solicitud
        'Como la funci�n BloqueoCondiciones es com�n para ambas opciones se modific� para que aceptara un array de solicitudes como par�metro de entrada
        'Cuando se env�a a proceso una solicitud este array no est� incializado, as� que se crea uno y se asigna la solicitud seleccionada al primer elemento.
        If IsEmpty(g_arSolicitudes) Then
            Dim arSolicitudes(0) As CInstancia
            Set arSolicitudes(0) = g_oSolicitudSeleccionada
            g_arSolicitudes = arSolicitudes
        End If
        
        'Se comprueban las condiciones de bloqueo por etapa
        For i = 0 To UBound(g_arSolicitudes)
            bBloqueoEtapa = BloqueoEtapa(g_arSolicitudes)
            If bBloqueoEtapa Then
                oMensajes.MensajeOKOnly 897, Exclamation  'Existen condiciones de bloqueo.
                Exit For
            End If
        Next
        
        If Not bBloqueoEtapa Then
            'Se comprueban las condiciones de bloqueo por f�rmula
            bBloqueoCondiciones = BloqueoCondiciones(g_arSolicitudes, TipoBloqueo.Apertura, iActualizarPrecio, iTipoPrecio, g_oProceso, g_oProcesoAEnviar.Anyo)
        End If
        
        If bBloqueoEtapa Or bBloqueoCondiciones Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If

    Screen.MousePointer = vbHourglass
    
    g_oProceso.MonCod = g_oProcesoAEnviar.MonCod
    g_oProceso.Cambio = g_oProcesoAEnviar.Cambio
    g_oProceso.PermSaltarseVolMaxAdjDir = m_bValSaltarseVolMaxAdjDir
    g_oProceso.PermitirAdjDirecta = g_oProcesoAEnviar.PermitirAdjDirecta
    g_oProceso.IgnorarRestricAdjDirecta = g_oProcesoAEnviar.IgnorarRestricAdjDirecta
    
    teserror = g_oProceso.EnviarSolicitudAProceso(g_oProcesoAEnviar.Anyo, g_oProcesoAEnviar.GMN1Cod, g_oProcesoAEnviar.Cod, g_oSolicitudSeleccionada.Id, iActualizarPrecio, iTipoPrecio, g_bTraspasarAdjuntos, g_bTraspasarAtributos, g_oProcesoAEnviar.MaterialProce, IIf(iActualizarPrecio = 2, txtPres.Text, Null), m_vPresup1, m_vPresup2, m_vPresup3, m_vPresup4, sEspNoExisten, oUsuarioSummit.Cod, g_bModificarEstructuraProceso, g_oProcesoAEnviar.DefEspecificaciones, g_oProcesoAEnviar.DefEspGrupos, g_oProcesoAEnviar.DefEspItems, (g_oProcesoAEnviar.Estado = ParcialmenteCerrado), g_oSolicitudSeleccionada.Estado, CodPersona:=oUsuarioSummit.Persona.Cod)
    Screen.MousePointer = vbNormal
    If sEspNoExisten <> "" Then
        sEspNoExisten = Left(sEspNoExisten, Len(sEspNoExisten) - 1)
        Dim arrArchivos() As String
        arrArchivos = Split(sEspNoExisten, ",")
        oMensajes.ImposibleAccederAFicheroEnAltaProcesoDesdeSolicitud arrArchivos
    End If
    
    If teserror.NumError = TESSolicitudAbrirMaxAdjDirPermiso Or teserror.NumError = TESSolicitudAbrirMaxAdjDir Then
        Screen.MousePointer = vbNormal
        oMensajes.VolAdjDirectaSuperadoSolicitud CDbl(teserror.Arg2), CDbl(teserror.Arg1), teserror.NumError, True
        Exit Sub
    ElseIf teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    Else
        Screen.MousePointer = vbNormal
        oMensajes.SolicitudEnviadaProceso
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "EnviarAProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Sub MostrarPresSeleccionado(ByVal sValor As String, ByVal iTipo As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Me.lblPres(iTipo - 1).caption = GenerarDenominacionPresupuesto(sValor, iTipo)

Select Case iTipo
    Case 1
        m_vPresup1 = sValor
    Case 2
        m_vPresup2 = sValor
    Case 3
        m_vPresup3 = sValor
    Case 4
        m_vPresup4 = sValor
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "MostrarPresSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Function BloqueoEtapa(ByVal g_arSolicitudes As Variant) As Boolean
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim bExisteBloqueo As Boolean
    
    bExisteBloqueo = False
    For i = 0 To UBound(g_arSolicitudes)
        Set Ador = g_arSolicitudes(i).ComprobarBloqueoEtapa()
        
        If Not Ador.EOF Then
            bExisteBloqueo = IIf(Ador.Fields("BLOQUEO_APERTURA").Value = 1, False, True)
            If bExisteBloqueo Then Exit For
        Else
            bExisteBloqueo = False
        End If
    Next
    
    BloqueoEtapa = bExisteBloqueo
    
    Ador.Close
    Set Ador = Nothing
End Function

''' <summary>Comprueba las condiciones de bloqueo</summary>
''' <returns>Booleano indicando si hay bloqueo</returns>
''' <remarks>Llamada desde: AbrirProceso, EnviarAProceso</remarks>

Private Function BloqueoCondiciones(ByVal g_arSolicitudes As Variant, iTipoBloqueo As Integer, ByVal iActualizarPrecio As Integer, ByVal iTipoPrecio As Integer, Optional ByRef oProceso As CProceso, _
        Optional ByVal iAnyoAEnviar As Variant) As Boolean
    Dim iBloqueo As Integer
    Dim bExisteBloqueo As Boolean
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim dImporteAbierto As Variant
    
    If IsEmpty(g_arSolicitudes) Then Exit Function
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Function
   
    bExisteBloqueo = False
    For i = 0 To UBound(g_arSolicitudes)
        Set Ador = g_arSolicitudes(i).ComprobarBloqueoCond(iTipoBloqueo)
    
        If Ador.EOF Then
            BloqueoCondiciones = False
            Ador.Close
            Set Ador = Nothing
            Exit Function
        Else
            While Not Ador.EOF And Not bExisteBloqueo
                If Not IsNull(Ador.Fields("FORMULA").Value) Then
                    dImporteAbierto = g_oProceso.DevolverImporteAbiertoDesdeSolicitud(g_arSolicitudes(i).Id, iActualizarPrecio, iTipoPrecio, IIf(iActualizarPrecio = 2, txtPres.Text, Null), iAnyoAEnviar)
    
                    iBloqueo = g_arSolicitudes(i).ComprobarBloqueoCondiciones(Ador.Fields("ID").Value, Ador.Fields("FORMULA").Value, dImporteAbierto, , , , , , oProceso)
                    Select Case iBloqueo
                        Case 0  'No existe bloqueo
                        
                        Case 1  'Existe bloqueo
                            oMensajes.Bloqueo NullToStr(Ador.Fields("MENSAJE_" & gParametrosInstalacion.gIdioma).Value), g_arSolicitudes(i).Id
                            If Ador.Fields("TIPO").Value = TipoAvisoBloqueo.Bloquea Then
                                bExisteBloqueo = True
                                Exit For
                            End If
                        Case 3  'Error al realizar el c�lculo:F�rmula inv�lida.
                            oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                        Case 4  'Error al realizar el c�lculo:Valores incorrectos.
                            oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
                    End Select
                End If
                Ador.MoveNext
            Wend
        End If
    Next
    
    BloqueoCondiciones = bExisteBloqueo

    Ador.Close
    Set Ador = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "BloqueoCondiciones", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Public Sub UOSeleccionada(sUON1 As String, sUON2 As String, sUON3 As String, Optional sDen As String = "")
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim oUON As IUon

'Despuies de pedirla miramos si los art�culos son validos
If sUON1 <> "" Then
    Set oUON = createUon(NullToStr(sUON1), NullToStr(sUON2), NullToStr(sUON3))
    'Primero validaremos que la distribuci�n es v�lida para todos los items del proceso
    For Each oGrupo In g_oProceso.Grupos
        If Not oGrupo.Items Is Nothing Then
        For Each oItem In oGrupo.Items
            If ("" & oItem.ArticuloCod) <> "" Then 'Artic NO codificados-> todos uons. Cumple.
                If Not oItem.Articulo Is Nothing Then
                    If Not oItem.Articulo.uons.EsDistribuibleUON(oUON) Then
                        oMensajes.MensajeOKOnly 1467, Critical
                        Exit Sub
                    End If
                End If
            End If
        Next
        End If
    Next
    
    Set oUON = Nothing
End If

If sUON3 <> "" Then
    lblDist.caption = sUON1 & " - " & sUON2 & " - " & sUON3 & " - " & sDen
    m_sUON1 = sUON1
    m_sUON2 = sUON2
    m_sUON3 = sUON3
    
ElseIf sUON2 <> "" Then
    lblDist.caption = sUON1 & " - " & sUON2 & " - " & sDen
    m_sUON1 = sUON1
    m_sUON2 = sUON2
    m_sUON3 = ""
    
ElseIf sUON1 <> "" Then
    lblDist.caption = sUON1 & " - " & sDen
    m_sUON1 = sUON1
    m_sUON2 = ""
    m_sUON3 = ""
Else
    lblDist.caption = ""
    m_sUON1 = ""
    m_sUON2 = ""
    m_sUON3 = ""
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirFaltan", "UOSeleccionada", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
''' <summary>
''' Establece el proveedor desde la busqueda de proveedor
''' </summary>
''' <remarks>Llamada desde: frmproveBuscar ; Tiempo m�ximo: 0,2</remarks>
Public Sub CargarProveedorConBusqueda()

    Dim oProves As CProveedores

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    
    bRespetarComboProve = True
    sdbcProveedor = oProves.Item(1).Cod
    sdbcProveedor.Columns(0).Value = oProves.Item(1).Cod
    sdbcDenProveedor.Text = oProves.Item(1).Den
    sdbcDenProveedor.Columns(0).Value = oProves.Item(1).Den
    bRespetarComboProve = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "CargarProveedorConBusqueda", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub



''' <summary>
''' Muestra los atributos de especificacion de ambito GRUPO en el grid "Otros Datos:"
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Tiempo m�ximo:0</remarks>

Private Sub MostrarAtributosGrupo()
Dim oAtr As CAtributo
Dim sCod As String
Dim sValor As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.RemoveAll
    sdbgAtributos.Refresh
    
    If Not m_oAtribEspePlantP Is Nothing Then
    
        If g_bMostrarAtribEspePlatillaProceso = True Then
            picAtrbEsp.Visible = True
            picAtrbEsp.Top = fraPres.Top + fraPres.Height
            picAtrbEsp.Width = fraPres.Width
            sdbgAtributos.Columns("COD").Width = Me.sdbgAtributos.Width * 0.15
            sdbgAtributos.Columns("DEN").Width = Me.sdbgAtributos.Width * 0.5
            sdbgAtributos.Columns("VALOR").Width = Me.sdbgAtributos.Width * 0.3
            Me.Height = Me.Height + picAtrbEsp.Height
            'Botonera
            picNavigate.Top = picAtrbEsp.Top + picAtrbEsp.Height + 200
        Else
            picAtrbEsp.Visible = False
            'Botonera
            picNavigate.Top = fraPres.Top + fraPres.Height + 200
        End If
    
        If Not g_oProceso.AtributosEspecificacion Is Nothing Then
            For Each oAtr In g_oProceso.AtributosEspecificacion
                If m_oAtribEspePlantP.ExisteAtributo(oAtr.Atrib) Then
                    If oAtr.Obligatorio Then
                        sCod = "(*) " & oAtr.Cod
                    Else
                        sCod = oAtr.Cod
                    End If
                    
                    sValor = ""
                    
                    Select Case oAtr.Tipo
                        Case TiposDeAtributos.TipoString
                            If Not IsNull(oAtr.valorText) Then
                                sValor = oAtr.valorText
                            End If
                        Case TiposDeAtributos.TipoNumerico
                            If Not IsNull(oAtr.valorNum) Then
                                sValor = CStr(oAtr.valorNum)
                            End If
                        Case TiposDeAtributos.TipoFecha
                            If Not IsNull(oAtr.valorFec) Then
                                sValor = CStr(oAtr.valorFec)
                            End If
                        Case TiposDeAtributos.TipoBoolean
                            If Not IsNull(oAtr.valorBool) Then
                                If oAtr.valorBool = True Then
                                    sValor = m_sIdiTrue
                                Else
                                    sValor = m_sIdiFalse
                                End If
                            End If
                        End Select
    
                    sdbgAtributos.AddItem sCod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & sValor & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Atrib & Chr(m_lSeparador) & oAtr.idAtribProce & Chr(m_lSeparador) & oAtr.Obligatorio
                End If
            Next
        End If
    Else
        picAtrbEsp.Visible = False
        'Botonera
        picNavigate.Top = fraPres.Top + fraPres.Height + 200
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "MostrarAtributosGrupo", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub



'3275
Private Sub AnyadirAtribEspeObligatoriosPlantillaAProceso()
'A�ade los Atributos de Especificacion de la plantilla al proceso de compra
'tanto a nivel de proceso, como de grupo o de item

    Dim oAtribEsp As CAtributo
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oPlantillaSeleccionada Is Nothing Then
        'cargamos todos los atributos de especificacion de ambito PROCESO
        g_oPlantillaSeleccionada.CargarTodosLosAtributosEspecificacion ("")
        
        For Each oAtribEsp In g_oPlantillaSeleccionada.AtributosEspecificacion
            If oAtribEsp.Obligatorio = True Then
                
                Select Case oAtribEsp.ambito
                
                    Case TipoAmbitoProceso.AmbProceso
                        If g_oProceso.AtributosEspecificacion Is Nothing Then
                            Set g_oProceso.AtributosEspecificacion = oFSGSRaiz.Generar_CAtributos
                        End If
                        g_oProceso.AtributosEspecificacion.AddAtrEsp g_oProceso.Anyo, g_oProceso.GMN1Cod, g_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbProceso, , , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion
                        
                        If m_oAtribEspePlantP Is Nothing Then
                            Set m_oAtribEspePlantP = oFSGSRaiz.Generar_CAtributos
                        End If
                        m_oAtribEspePlantP.AddAtrEsp g_oProceso.Anyo, g_oProceso.GMN1Cod, g_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbProceso, , , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , SQLBinaryToBoolean(oAtribEsp.ListaExterna)
                End Select
                
            End If
        Next
        
        'MostrarAtributosGrupo

    End If
    
    MostrarAtributosGrupo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "AnyadirAtribEspeObligatoriosPlantillaAProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub




Private Sub sdbgatributos_AfterUpdate(RtnDispErrMsg As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    RtnDispErrMsg = 0

    If Not sdbgAtributos.Columns("VALOR").Value = "" And bUpdate = True Then
        'Si el grupo tiene atributos de especificacion...
        If Not g_oProceso.AtributosEspecificacion Is Nothing Then
            'Si existe el atributo en este grupo, se actualiza su valor
            If Not g_oProceso.AtributosEspecificacion.Item(m_oAtributoP.Atrib) Is Nothing Then
                Select Case m_oAtributoP.Tipo
                    Case TiposDeAtributos.TipoString
                        g_oProceso.AtributosEspecificacion.Item(m_oAtributoP.Atrib).valorText = CStr(sdbgAtributos.Columns("VALOR").Value)
                    Case TiposDeAtributos.TipoNumerico
                        g_oProceso.AtributosEspecificacion.Item(m_oAtributoP.Atrib).valorNum = CDbl(sdbgAtributos.Columns("VALOR").Value)
                    Case TiposDeAtributos.TipoFecha
                        g_oProceso.AtributosEspecificacion.Item(m_oAtributoP.Atrib).valorFec = CDate(sdbgAtributos.Columns("VALOR").Value)
                    Case TiposDeAtributos.TipoBoolean
                        If UCase(sdbgAtributos.Columns("VALOR").Value) = UCase(m_sIdiTrue) Then
                            g_oProceso.AtributosEspecificacion.Item(m_oAtributoP.Atrib).valorBool = True
                        Else
                            g_oProceso.AtributosEspecificacion.Item(m_oAtributoP.Atrib).valorBool = False
                        End If
                    End Select
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "sdbgatributos_AfterUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If


End Sub



Private Sub sdbgatributos_BeforeUpdate(Cancel As Integer)
    Dim bSalir As Boolean
    Dim bEncontrado As Boolean
    Dim sError As String
    Dim oElem As CValorPond
    Dim vValorM As Variant
    Dim oAtributo As CAtributo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iFila = sdbgAtributos.Bookmark
    
    If bUpdate = False Then
        Exit Sub
    End If
    
    Set oAtributo = Nothing
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    Set oAtributo = m_oAtribEspePlantP.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
    
    If sdbgAtributos.Columns("VALOR").Text = "" Then
        oMensajes.DatoObligatorio 111
        bUpdate = False
        sdbgAtributos_PositionList oAtributo.Cod
        InicializaValorAtributo oAtributo
        bGuardar = False
        Exit Sub
    End If

    If sdbgAtributos.Columns("VALOR").Text <> "" Then
        bSalir = False
        Select Case sdbgAtributos.Columns("TIPO").Value
            Case TiposDeAtributos.TipoNumerico
                sError = "TIPO2"
                If Not IsNumeric(sdbgAtributos.Columns("VALOR").Text) Then
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoFecha
                sError = "TIPO3"
                If Not IsDate(sdbgAtributos.Columns("VALOR").Text) Then
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoBoolean
                sError = "TIPO4"
                If UCase(sdbgAtributos.Columns("VALOR").Text) <> UCase(m_sIdiTrue) And UCase(sdbgAtributos.Columns("VALOR").Text) <> UCase(m_sIdiFalse) Then
                    bSalir = True
                End If
        End Select
        If bSalir Then
            sdbgAtributos.Columns("VALOR").Text = ""
            sdbgAtributos.CancelUpdate
            sdbgAtributos.DataChanged = False
            InicializaValorAtributo oAtributo
            oMensajes.AtributoValorNoValido sError
            bUpdate = False
            sdbgAtributos_PositionList oAtributo.Cod
            bGuardar = False
            Exit Sub
        End If
        
        bEncontrado = False

        If m_oAtributoP.TipoIntroduccion = Introselec And m_oAtributoP.ListaExterna = 0 Then
            m_oAtributoP.CargarListaDeValores
            For Each oElem In m_oAtributoP.ListaPonderacion
                Select Case sdbgAtributos.Columns("TIPO").Value
                Case TiposDeAtributos.TipoString
                    If oElem.ValorLista = sdbgAtributos.Columns("VALOR").Text Then
                        bEncontrado = True
                        Exit For
                    End If
                Case TiposDeAtributos.TipoNumerico
                    If CDbl(oElem.ValorLista) = CDbl(sdbgAtributos.Columns("VALOR").Text) Then
                        bEncontrado = True
                        Exit For
                    End If
                Case TiposDeAtributos.TipoFecha
                    If CDate(oElem.ValorLista) = CDate(sdbgAtributos.Columns("VALOR").Text) Then
                        bEncontrado = True
                        Exit For
                    End If
                End Select
            Next
            If Not bEncontrado Then
                oMensajes.AtributoValorNoValido "NO_LISTA"
                bGuardar = False
                sdbgAtributos.CancelUpdate
                sdbgAtributos.Columns("VALOR").Text = ""
                sdbgAtributos.DataChanged = False
                InicializaValorAtributo oAtributo
                bUpdate = False
                sdbgAtributos_PositionList oAtributo.Cod
                Exit Sub
            End If
        End If
        bSalir = False
        sError = ""
        If m_oAtributoP.TipoIntroduccion = IntroLibre Then
            Select Case sdbgAtributos.Columns("TIPO").Value
                Case TiposDeAtributos.TipoNumerico

                        vValorM = CDec(sdbgAtributos.Columns("VALOR").Text)

                    If IsNumeric(m_oAtributoP.Maximo) Then
                    
                        sError = FormateoNumerico(m_oAtributoP.Maximo)
                        If CDbl(m_oAtributoP.Maximo) < CDbl(vValorM) Then
                            bSalir = True
                        End If
                        
                    End If
                    If IsNumeric(m_oAtributoP.Minimo) Then
                    
                        If sError = "" Then
                            sError = sIdiMayor & " " & FormateoNumerico(m_oAtributoP.Minimo)
                        Else
                            sError = sIdiEntre & "  " & FormateoNumerico(m_oAtributoP.Minimo) & " - " & sError
                        End If
                        If CDbl(m_oAtributoP.Minimo) > CDbl(vValorM) Then
                            bSalir = True
                        End If
                        
                    Else
                    
                        If sError <> "" Then
                            sError = sIdiMenor & " " & sError
                        End If
                        
                    End If
                Case TiposDeAtributos.TipoFecha
                    If IsDate(m_oAtributoP.Maximo) Then
                        sError = m_oAtributoP.Maximo
                        If CDate(m_oAtributoP.Maximo) < CDate(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    End If
                    If IsDate(m_oAtributoP.Minimo) Then
                        If sError = "" Then
                            sError = sIdiMayor & " " & m_oAtributoP.Minimo
                        Else
                            sError = sIdiEntre & "  " & m_oAtributoP.Minimo & " - " & sError
                        End If
                        If CDate(m_oAtributoP.Minimo) > CDate(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    Else
                        If sError <> "" Then
                            sError = sIdiMenor & " " & sError
                        End If
                    End If
            End Select
            If bSalir Then
                sdbgAtributos.Columns("VALOR").Text = ""
                sdbgAtributos.CancelUpdate
                sdbgAtributos.DataChanged = False
                InicializaValorAtributo oAtributo
                oMensajes.AtributoValorNoValido sError
                bUpdate = False
                sdbgAtributos_PositionList oAtributo.Cod
                bGuardar = False
                Exit Sub
            End If
        End If
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "sdbgatributos_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbgatributos_Change()

    'DoEvents
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bUpdate = True
    
    Set m_oAtributoP = Nothing
    Set m_oAtributoP = oFSGSRaiz.Generar_CAtributo
    Set m_oAtributoP = m_oAtribEspePlantP.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
    
    iFila = sdbgAtributos.Bookmark
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "sdbgatributos_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub




Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bUpdate = False Then
        bUpdate = True
        sdbgAtributos.Bookmark = iFila
        sdbgAtributos.col = 2
    End If
    
    Set m_oAtributoP = Nothing
    Set m_oAtributoP = oFSGSRaiz.Generar_CAtributo
    Set m_oAtributoP = m_oAtribEspePlantP.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
    sdbgAtributos.Columns("VALOR").Locked = False
    Select Case sdbgAtributos.Columns("INTRO").Value
        Case 1:  'Seleccion
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            If sdbgAtributos.Columns("TIPO").Value = 2 Then
                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
            Else
                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
            End If
            If m_oAtributoP.ListaExterna Then
                sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                sdbgAtributos.Columns("VALOR").Locked = True
                sdbgAtributos.Columns("VALOR").Style = ssStyleEditButton
            Else
                sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                sdbddValor.Enabled = True
            End If
            
        Case 0: 'Libre
            If sdbgAtributos.Columns("TIPO").Value = 4 Then
                sdbddValor.RemoveAll
                sdbddValor.AddItem ""
                sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                sdbddValor.Enabled = True
            Else
                sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                sdbddValor.Enabled = False
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "sdbgAtributos_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub InicializaValorAtributo(ByRef oAtributo As CAtributo)
    Dim oAtributoEsp As CAtributo
  
    Dim bEncontrado As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bEncontrado = False

    If Not g_oProceso Is Nothing Then
        If Not g_oProceso.AtributosEspecificacion Is Nothing Then
            For Each oAtributoEsp In g_oProceso.AtributosEspecificacion
                'Si el atributo de especificacion en curso, es de la plantilla, se comprueba si esta informado
                If oAtributoEsp.Atrib = oAtributo.Atrib Then
                        
                    oAtributoEsp.valorBool = Null
                    oAtributoEsp.valorFec = Null
                    oAtributoEsp.valorNum = Null
                    oAtributoEsp.valorText = Null
                        
                    bEncontrado = True
                        
                End If
                
                If bEncontrado Then
                    Exit For
                End If
            Next
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "InicializaValorAtributo", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub sdbddValor_DropDown()

    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.RemoveAll

    If sdbgAtributos.Columns("INTRO").Value = "1" Then
        
        Set oatrib = g_oProceso.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
    
        oatrib.CargarListaDeValores
        
        Set oLista = oatrib.ListaPonderacion
        For Each oElem In oLista
            sdbddValor.AddItem oElem.ValorLista
        Next
        Set oatrib = Nothing
        Set oLista = Nothing
    Else
        If sdbgAtributos.Columns("TIPO").Value = 4 Then
            sdbddValor.AddItem m_sIdiTrue
            sdbddValor.AddItem m_sIdiFalse
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "sdbddValor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Sub sdbddValor_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "sdbddValor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then

                sdbgAtributos.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))

                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion

    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bExiste = False
    ''' Comprobar la existencia en la lista
    If sdbgAtributos.Columns("INTRO").Value = "1" Then

        Set oatrib = g_oProceso.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ID_A").Value))

        oatrib.CargarListaDeValores
        Set oLista = oatrib.ListaPonderacion
        For Each oElem In oLista
            If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                bExiste = True
                Exit For
            End If
        Next
    Else
        If sdbgAtributos.Columns("TIPO").Value = 4 Then
            If UCase(m_sIdiTrue) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                bExiste = True
            End If
            If UCase(m_sIdiFalse) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                bExiste = True
            End If
            
        End If
    End If
    If Not bExiste Then
        bUpdate = False
        oMensajes.NoValido sdbgAtributos.Columns("VALOR").caption
        sdbgAtributos.DataChanged = False
        bUpdate = False
        sdbgAtributos_PositionList oatrib.Cod
        RtnPassed = False
        bGuardar = False
        Exit Sub
    End If
    RtnPassed = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "sdbddValor_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Sub ActualizarAtribEspeProcePlantilla()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAtributos.DataChanged Then
        If sdbgAtributos.Rows = 1 Then
            sdbgAtributos.Update
        Else
            sdbgAtributos.MoveNext
            If bGuardar = True Then
                sdbgAtributos.MovePrevious
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "ActualizarAtribEspeProcePlantilla", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub



Private Sub EliminarAtribEspePlantillaDelProceso()

    Dim m_oAtribEspe As CAtributo
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oAtributoP = Nothing
    
    sdbgAtributos.RemoveAll
    
    If Not m_oAtribEspePlantP Is Nothing Then
        For Each m_oAtribEspe In m_oAtribEspePlantP
            If Not g_oProceso.AtributosEspecificacion Is Nothing Then
                If Not g_oProceso.AtributosEspecificacion.Item(m_oAtribEspe.Atrib) Is Nothing Then
                    g_oProceso.AtributosEspecificacion.Remove (m_oAtribEspe.Atrib)
                End If
            End If
        Next
        Set m_oAtribEspePlantP = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirFaltan", "EliminarAtribEspePlantillaDelProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbgAtributos_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbgAtributos.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbgAtributos.Rows - 1
            bm = sdbgAtributos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbgAtributos.Columns(0).CellText(bm), 1, Len(Text))) Then
                If Me.Visible Then sdbgAtributos.SetFocus
                sdbgAtributos.Bookmark = bm
                sdbgAtributos.col = 2
                Exit For
            End If
        Next i
    End If
End Sub
