VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{1E673DEF-C82F-4FBB-BAB9-77B0634E8F4D}#1.0#0"; "ProceSelector.ocx"
Begin VB.Form frmSELPROVE 
   Caption         =   "Selecci�n de proveedores"
   ClientHeight    =   5970
   ClientLeft      =   2415
   ClientTop       =   2190
   ClientWidth     =   12375
   Icon            =   "frmSELPROVE.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5970
   ScaleWidth      =   12375
   Begin VB.PictureBox picSel 
      BackColor       =   &H00808080&
      Height          =   4815
      Left            =   30
      ScaleHeight     =   4755
      ScaleWidth      =   11580
      TabIndex        =   20
      Top             =   600
      Width           =   11640
      Begin SSDataWidgets_B.SSDBGrid sdbgProveERP 
         Height          =   975
         Left            =   3120
         TabIndex        =   29
         Top             =   1200
         Visible         =   0   'False
         Width           =   6675
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   3
         AllowColumnMoving=   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3678
         Columns(0).Caption=   "ERP"
         Columns(0).Name =   "ERP"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2355
         Columns(1).Caption=   "PROVE_ERP"
         Columns(1).Name =   "PROVE_ERP"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   5609
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   11774
         _ExtentY        =   1720
         _StockProps     =   79
         BackColor       =   12632256
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgSelProve 
         Height          =   4755
         Left            =   0
         TabIndex        =   7
         Top             =   0
         Width           =   25830
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         Col.Count       =   20
         stylesets.count =   10
         stylesets(0).Name=   "Asignado"
         stylesets(0).BackColor=   11007475
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSELPROVE.frx":014A
         stylesets(1).Name=   "Premium"
         stylesets(1).BackColor=   10079487
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSELPROVE.frx":0166
         stylesets(1).AlignmentText=   0
         stylesets(1).AlignmentPicture=   1
         stylesets(2).Name=   "AsignadoConAdj"
         stylesets(2).BackColor=   11007475
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSELPROVE.frx":04D7
         stylesets(2).AlignmentPicture=   1
         stylesets(3).Name=   "PremiumAsig"
         stylesets(3).BackColor=   11007475
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmSELPROVE.frx":091E
         stylesets(3).AlignmentText=   0
         stylesets(3).AlignmentPicture=   1
         stylesets(4).Name=   "NormalConAdjOtroMat"
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmSELPROVE.frx":093A
         stylesets(4).AlignmentPicture=   1
         stylesets(5).Name=   "Normal"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmSELPROVE.frx":0CF1
         stylesets(6).Name=   "AsignadoConAdjOtroMat"
         stylesets(6).BackColor=   11007475
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmSELPROVE.frx":0D0D
         stylesets(6).AlignmentPicture=   1
         stylesets(7).Name=   "Calidad"
         stylesets(7).ForeColor=   16777215
         stylesets(7).BackColor=   16777215
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmSELPROVE.frx":10C4
         stylesets(7).AlignmentText=   2
         stylesets(7).AlignmentPicture=   2
         stylesets(8).Name=   "Tan"
         stylesets(8).BackColor=   10079487
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmSELPROVE.frx":1159
         stylesets(9).Name=   "NormalConAdj"
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmSELPROVE.frx":1175
         stylesets(9).AlignmentPicture=   1
         UseGroups       =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         MaxSelectedRows =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Groups.Count    =   3
         Groups(0).Width =   6959
         Groups(0).Caption=   "GSplit"
         Groups(0).Columns.Count=   3
         Groups(0).Columns(0).Width=   2752
         Groups(0).Columns(0).Caption=   "SEL"
         Groups(0).Columns(0).Name=   "SEL"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Style=   2
         Groups(0).Columns(1).Width=   2143
         Groups(0).Columns(1).Caption=   "C�digo"
         Groups(0).Columns(1).Name=   "PROVECOD"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(1).Locked=   -1  'True
         Groups(0).Columns(1).HasBackColor=   -1  'True
         Groups(0).Columns(1).BackColor=   16777215
         Groups(0).Columns(2).Width=   2064
         Groups(0).Columns(2).Caption=   "Denominaci�n"
         Groups(0).Columns(2).Name=   "PROVEDEN"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(2).Locked=   -1  'True
         Groups(1).Width =   16933
         Groups(1).Caption=   "G1"
         Groups(1).Columns.Count=   11
         Groups(1).Columns(0).Width=   2910
         Groups(1).Columns(0).Caption=   "C�d Portal"
         Groups(1).Columns(0).Name=   "PROVEPORT"
         Groups(1).Columns(0).DataField=   "Column 3"
         Groups(1).Columns(0).DataType=   8
         Groups(1).Columns(0).FieldLen=   256
         Groups(1).Columns(0).Locked=   -1  'True
         Groups(1).Columns(0).HasBackColor=   -1  'True
         Groups(1).Columns(0).BackColor=   16777215
         Groups(1).Columns(1).Width=   2805
         Groups(1).Columns(1).Caption=   "COD_ERP"
         Groups(1).Columns(1).Name=   "COD_ERP"
         Groups(1).Columns(1).DataField=   "Column 4"
         Groups(1).Columns(1).DataType=   8
         Groups(1).Columns(1).FieldLen=   256
         Groups(1).Columns(1).Style=   4
         Groups(1).Columns(1).ButtonsAlways=   -1  'True
         Groups(1).Columns(2).Width=   2805
         Groups(1).Columns(2).Caption=   "Eqp."
         Groups(1).Columns(2).Name=   "EQP"
         Groups(1).Columns(2).DataField=   "Column 5"
         Groups(1).Columns(2).DataType=   8
         Groups(1).Columns(2).FieldLen=   256
         Groups(1).Columns(3).Width=   2805
         Groups(1).Columns(3).Caption=   "Com."
         Groups(1).Columns(3).Name=   "COM"
         Groups(1).Columns(3).DataField=   "Column 6"
         Groups(1).Columns(3).DataType=   8
         Groups(1).Columns(3).FieldLen=   256
         Groups(1).Columns(4).Width=   3200
         Groups(1).Columns(4).Visible=   0   'False
         Groups(1).Columns(4).Caption=   "Prem."
         Groups(1).Columns(4).Name=   "PREMIUM"
         Groups(1).Columns(4).DataField=   "Column 7"
         Groups(1).Columns(4).DataType=   11
         Groups(1).Columns(4).FieldLen=   256
         Groups(1).Columns(4).Locked=   -1  'True
         Groups(1).Columns(5).Width=   2884
         Groups(1).Columns(5).Visible=   0   'False
         Groups(1).Columns(5).Caption=   "ACTIVO"
         Groups(1).Columns(5).Name=   "ACTIVO"
         Groups(1).Columns(5).DataField=   "Column 8"
         Groups(1).Columns(5).DataType=   11
         Groups(1).Columns(5).FieldLen=   256
         Groups(1).Columns(5).Locked=   -1  'True
         Groups(1).Columns(6).Width=   2884
         Groups(1).Columns(6).Visible=   0   'False
         Groups(1).Columns(6).Caption=   "ANTCIERRE"
         Groups(1).Columns(6).Name=   "ANTCIERRE"
         Groups(1).Columns(6).DataField=   "Column 9"
         Groups(1).Columns(6).DataType=   11
         Groups(1).Columns(6).FieldLen=   256
         Groups(1).Columns(7).Width=   2910
         Groups(1).Columns(7).Visible=   0   'False
         Groups(1).Columns(7).Caption=   "HAYADJMATP"
         Groups(1).Columns(7).Name=   "HAYADJMATP"
         Groups(1).Columns(7).DataField=   "Column 10"
         Groups(1).Columns(7).DataType=   8
         Groups(1).Columns(7).FieldLen=   256
         Groups(1).Columns(8).Width=   1614
         Groups(1).Columns(8).Visible=   0   'False
         Groups(1).Columns(8).Caption=   "HAYADJMATNOP"
         Groups(1).Columns(8).Name=   "HAYADJMATNOP"
         Groups(1).Columns(8).DataField=   "Column 11"
         Groups(1).Columns(8).DataType=   8
         Groups(1).Columns(8).FieldLen=   256
         Groups(1).Columns(9).Width=   5609
         Groups(1).Columns(9).Caption=   "Punt."
         Groups(1).Columns(9).Name=   "PUNT"
         Groups(1).Columns(9).Alignment=   2
         Groups(1).Columns(9).CaptionAlignment=   0
         Groups(1).Columns(9).DataField=   "Column 12"
         Groups(1).Columns(9).DataType=   8
         Groups(1).Columns(9).FieldLen=   256
         Groups(1).Columns(9).Style=   4
         Groups(1).Columns(9).ButtonsAlways=   -1  'True
         Groups(1).Columns(10).Width=   3200
         Groups(1).Columns(10).Visible=   0   'False
         Groups(1).Columns(10).Caption=   "PUNT_VALOR"
         Groups(1).Columns(10).Name=   "PUNT_VALOR"
         Groups(1).Columns(10).DataField=   "Column 13"
         Groups(1).Columns(10).DataType=   8
         Groups(1).Columns(10).FieldLen=   256
         Groups(2).Width =   6562
         Groups(2).Caption=   "G2"
         Groups(2).Columns.Count=   6
         Groups(2).Columns(0).Width=   1482
         Groups(2).Columns(0).Visible=   0   'False
         Groups(2).Columns(0).Caption=   "Val1"
         Groups(2).Columns(0).Name=   "VAL1"
         Groups(2).Columns(0).DataField=   "Column 14"
         Groups(2).Columns(0).DataType=   8
         Groups(2).Columns(0).FieldLen=   256
         Groups(2).Columns(0).Locked=   -1  'True
         Groups(2).Columns(1).Width=   953
         Groups(2).Columns(1).Visible=   0   'False
         Groups(2).Columns(1).Caption=   "Cal1"
         Groups(2).Columns(1).Name=   "CAL1"
         Groups(2).Columns(1).DataField=   "Column 15"
         Groups(2).Columns(1).DataType=   8
         Groups(2).Columns(1).FieldLen=   256
         Groups(2).Columns(1).Locked=   -1  'True
         Groups(2).Columns(2).Width=   2487
         Groups(2).Columns(2).Visible=   0   'False
         Groups(2).Columns(2).Caption=   "Val2"
         Groups(2).Columns(2).Name=   "VAL2"
         Groups(2).Columns(2).DataField=   "Column 16"
         Groups(2).Columns(2).DataType=   8
         Groups(2).Columns(2).FieldLen=   256
         Groups(2).Columns(2).Locked=   -1  'True
         Groups(2).Columns(3).Width=   926
         Groups(2).Columns(3).Visible=   0   'False
         Groups(2).Columns(3).Caption=   "Cal2"
         Groups(2).Columns(3).Name=   "CAL2"
         Groups(2).Columns(3).DataField=   "Column 17"
         Groups(2).Columns(3).DataType=   8
         Groups(2).Columns(3).FieldLen=   256
         Groups(2).Columns(3).Locked=   -1  'True
         Groups(2).Columns(4).Width=   6562
         Groups(2).Columns(4).Visible=   0   'False
         Groups(2).Columns(4).Caption=   "Val3"
         Groups(2).Columns(4).Name=   "VAL3"
         Groups(2).Columns(4).DataField=   "Column 18"
         Groups(2).Columns(4).DataType=   8
         Groups(2).Columns(4).FieldLen=   256
         Groups(2).Columns(4).Locked=   -1  'True
         Groups(2).Columns(5).Width=   1217
         Groups(2).Columns(5).Visible=   0   'False
         Groups(2).Columns(5).Caption=   "Cal3"
         Groups(2).Columns(5).Name=   "CAL3"
         Groups(2).Columns(5).DataField=   "Column 19"
         Groups(2).Columns(5).DataType=   8
         Groups(2).Columns(5).FieldLen=   256
         Groups(2).Columns(5).Locked=   -1  'True
         _ExtentX        =   45561
         _ExtentY        =   8387
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddEqpCod 
         Height          =   1635
         Left            =   3720
         TabIndex        =   24
         Top             =   0
         Width           =   3135
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSELPROVE.frx":15BC
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4868
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5530
         _ExtentY        =   2884
         _StockProps     =   77
         BackColor       =   8421376
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddComCod 
         Height          =   1635
         Left            =   6120
         TabIndex        =   25
         Top             =   0
         Width           =   3735
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSELPROVE.frx":15D8
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1746
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6006
         Columns(1).Caption=   "Apellidos"
         Columns(1).Name =   "APE"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Nombre"
         Columns(2).Name =   "NOM"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   2884
         _StockProps     =   77
         BackColor       =   8421376
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdConfQA 
      Height          =   285
      Left            =   12020
      Picture         =   "frmSELPROVE.frx":15F4
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   105
      Width           =   315
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   12375
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   5415
      Width           =   12375
      Begin VB.CommandButton cmdGruposBlq 
         Caption         =   "D&Bloquear grupos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7710
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   180
         Width           =   1395
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1215
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdGrupos 
         Caption         =   "&Asignar grupos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6195
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   180
         Width           =   1395
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&Otro proveedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4620
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   180
         Width           =   1440
      End
      Begin VB.CommandButton cmdValidar 
         Caption         =   "&Validar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3480
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   9225
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
   End
   Begin VB.PictureBox picProceAnya 
      BorderStyle     =   0  'None
      Height          =   525
      Left            =   60
      ScaleHeight     =   525
      ScaleWidth      =   12000
      TabIndex        =   15
      Top             =   0
      Width           =   12000
      Begin VB.CommandButton cmdColaboracion 
         Enabled         =   0   'False
         Height          =   285
         Left            =   11250
         Picture         =   "frmSELPROVE.frx":169A
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   105
         Width           =   315
      End
      Begin VB.CommandButton cmdMicro 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11610
         Picture         =   "frmSELPROVE.frx":1923
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   105
         Width           =   315
      End
      Begin VB.CommandButton cmdResponsable 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10890
         Picture         =   "frmSELPROVE.frx":1CAD
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   105
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   10530
         Picture         =   "frmSELPROVE.frx":1D34
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   105
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   2070
         TabIndex        =   1
         Top             =   105
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   480
         TabIndex        =   0
         Top             =   105
         Width           =   960
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1693
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
         Height          =   285
         Left            =   4305
         TabIndex        =   2
         Top             =   105
         Width           =   1305
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1958
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8599
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INVI"
         Columns(2).Name =   "INVI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   2302
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SelectorDeProcesos.ProceSelector ProceSelector1 
         Height          =   315
         Left            =   3165
         TabIndex        =   23
         Top             =   105
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
         Height          =   285
         Left            =   5610
         TabIndex        =   3
         Top             =   105
         Width           =   4860
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7858
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1826
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INVI"
         Columns(2).Name =   "INVI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8572
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblProceCod 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   3082
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         TabIndex        =   19
         Top             =   105
         Width           =   1020
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Cmd.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1575
         TabIndex        =   18
         Top             =   150
         Width           =   450
      End
      Begin VB.Label lblAnyo 
         Caption         =   "A�o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   45
         TabIndex        =   17
         Top             =   150
         Width           =   435
      End
      Begin VB.Label Label2 
         Caption         =   "Proceso:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3555
         TabIndex        =   16
         Top             =   150
         Width           =   705
      End
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   12375
      TabIndex        =   22
      Top             =   4920
      Visible         =   0   'False
      Width           =   12375
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4800
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3600
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmSELPROVE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables para func. combos
Public bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
' Variables para func. grid
Private bCargarComboDesdeDD As Boolean
Private sIdioma() As String
Private sidiProveSel As String
Private g_bAsignarResp As Boolean

'Variable de control de flujo de proceso
Public Accion As AccionesSummit
Private Tord As TipoOrdenacionAsignaciones
'Variable para bloquear cuando se viene del visor
Public g_bVisor As Boolean

'Variables de seguridad
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRPerfUON As Boolean
Private m_bIgnBlqEscalacion As Boolean
Private m_bBloquearGrupo As Boolean
Private m_bAsignarGrupo As Boolean
Private bRMat As Boolean 'Se aplica a la combo de procesos
Public bREqp As Boolean  'Se aplica a la combo de procesos y a los proveedores
Public bRComp As Boolean ' 'Se aplica a la combo de procesos y a los proveedores
Private bModif As Boolean
Public bAnya As Boolean   ' Saltarse las restricciones y poder anyadir cualquier asignacion
Public bAsigRes As Boolean '
Public bVal As Boolean
Private bRCompResp As Boolean
Private bRUsuAper As Boolean
Public bVerAsigEqp As Boolean
Public bVerAsigComp As Boolean
Private g_bSoloInvitado As Boolean
Private mbSaltar_sdbgSelProve_Change As Boolean

'Proceso seleccionado
Public oProcesoSeleccionado As CProceso
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGruposMN1 As CGruposMatNivel1

Public oIasig As IAsignaciones

'Coleccion que contendra los provedores /compradores a asignar
Public oAsigSeleccionadas As CAsignaciones
Public oAsigDesSeleccionadas As CAsignaciones
'Como optimizacion quedaria que fuesen colecciones locales
Public oProveedores As CProveedores
'Coleccion para cargar grids de selecci�n de equipos y compradores
Private oEquipos As CEquipos
Private oEquipo As CEquipo
Private oCompradores As CCompradores


Private oVarsCalidad As CVariablesCalidad  'Variables de calidad
'Coleccion con los datos del proveedor seleccionado para pasarselo a la comparativaQA
Private oProveedoresCalidad As CProveedores
Private blnPermisoVariablesCalidad As Boolean

' Coleccion de procesos a cargar
Public oProcesos As CProcesos
Public oAsigs As CAsignaciones
' variable para listado
Public ofrmLstPROCE As frmLstPROCE
' idiomas
Private sComprador As String
Private sproveedor As String
Private sEquipo As String
Private marTextosAdj(10) As String

'Variables para control de movimiento del rat�n y bloqueo
Private Enum OrigenBloqueo
    NoBloqueo = 0
    Eliminar = 1
    Modificar = 2
    responsable = 3
    AnyadirProv = 4
    PermisoValidar = 5
End Enum
Private m_udtOrigBloqueo As OrigenBloqueo

Private m_bMicrostrategy As Boolean

'm_aBookmarks contiene la colecci�n de proveedores seleccionados
Private m_aBookmarks As Collection

'm_aBookmarksSel contiene los proveedores que vamos a modificar porque se ha cambiado el check de Selecci�n o el equpamiento o el comprador
Private m_aBookmarksModif As Collection

Private m_bModoEdicion As Boolean

Private Declare Function SetCursorPos Lib "user32" (ByVal X As Long, ByVal Y As Long) As Long
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_oUnidadesQa As CUnidadesNegQA
Private m_oUnqaEnProve As CUnidadesNegQA 'coleccion unidades con puntos. Rellenar grids/arboles visibilidad
Private m_oUnqaVarcalEnProve As CVariablesCalidad 'coleccion unidad-proveedor con puntos. Rellenar grids/arboles visibilidad
Private m_oVarsCalidad As CVariablesCalidad
Private m_oConfProve As CConfProveQA
Private m_altaNueva As Boolean
Public m_bHacer_ProcesoSeleccionado As Boolean
Private m_sMsgError As String
Private m_bArrangeColFijas As Boolean

Private Sub CargarComboConEquipos()

    ''' * Objetivo: Cargar combo con la coleccion de equipos
    
    Dim oeqp As CEquipo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddEqpCod.RemoveAll
    
    For Each oeqp In oEquipos
        sdbddEqpCod.AddItem oeqp.Cod & Chr(m_lSeparador) & oeqp.Den
    Next
    
    sdbddEqpCod.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "CargarComboConEquipos", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub
Private Sub CargarComboConCompradores()

    ''' * Objetivo: Cargar combo con la coleccion de equipos
    
    Dim oComp As CComprador
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddComCod.RemoveAll
    
    For Each oComp In oCompradores
        sdbddComCod.AddItem oComp.Cod & Chr(m_lSeparador) & oComp.Apel & Chr(m_lSeparador) & oComp.nombre
    Next
    
    sdbddComCod.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "CargarComboConCompradores", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub



Private Function f_bValidacionAsignarBloquearGrupos() As Boolean
Dim irespuesta As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
f_bValidacionAsignarBloquearGrupos = False
If bVal Then
    If oProcesoSeleccionado.Estado >= conasignacionvalida Then
        irespuesta = oMensajes.PreguntaModificarSelProveValidada
        If irespuesta = vbNo Then Exit Function
    End If
Else
    'Si el usuario no tiene permiso de validar, no puede modificar una selprove ya validada
    If oProcesoSeleccionado.Estado >= conasignacionvalida Then
        oMensajes.ImposibleModSelProve
        Exit Function
    End If
End If

If Not BloquearProceso(1) Then Exit Function

f_bValidacionAsignarBloquearGrupos = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "f_bValidacionAsignarBloquearGrupos", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Sub p_Llamar_frmSELPROVEGrupos(p_sOrigen As String)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If Not f_bValidacionAsignarBloquearGrupos Then
    Exit Sub
End If

m_udtOrigBloqueo = Modificar

Set frmSELPROVEGrupos.g_oProceso = oProcesoSeleccionado
frmSELPROVEGrupos.sOrigen = p_sOrigen
frmSELPROVEGrupos.Show 1

DesbloquearProceso 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "ProcesoSeleccionado", err, Erl, , m_bActivado)
        Exit Sub
    End If
    

End Sub

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim b As Boolean
Dim i As Integer
Dim sEqpOld As String
Dim sEqpNew As String
Dim sCompOld As String
Dim sCompNew As String
Dim vbm As Variant
Dim oIAsigs As IAsignaciones

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oIasig Is Nothing Then
        If Not BloquearProceso(1) Then Exit Sub
    End If
    
    If Accion = ACCSelProveMod Then
        Set oAsigSeleccionadas = Nothing
        Set oAsigDesSeleccionadas = Nothing
        Set oAsigSeleccionadas = oFSGSRaiz.Generar_CAsignaciones
        Set oAsigDesSeleccionadas = oFSGSRaiz.Generar_CAsignaciones
        Set oIAsigs = oProcesoSeleccionado

        For Each vbm In m_aBookmarksModif
            i = sdbgSelProve.AddItemRowIndex(vbm)
            If bVerAsigEqp Then
                sEqpOld = basOptimizacion.gCodEqpUsuario
                sEqpNew = basOptimizacion.gCodEqpUsuario
                sCompNew = sdbgSelProve.Columns("COM").CellValue(vbm)
                sCompOld = NullToStr(oAsigs.Item(i + 1).CodComp)
            Else
                If bVerAsigComp Then
                    sEqpOld = basOptimizacion.gCodEqpUsuario
                    sCompOld = basOptimizacion.gCodCompradorUsuario
                    sEqpNew = basOptimizacion.gCodEqpUsuario
                    sCompNew = basOptimizacion.gCodCompradorUsuario
                Else
                    sEqpNew = sdbgSelProve.Columns("EQP").CellValue(vbm)
                    sCompNew = sdbgSelProve.Columns("COM").CellValue(vbm)
                    sEqpOld = NullToStr(oAsigs.Item(i + 1).codEqp)
                    sCompOld = NullToStr(oAsigs.Item(i + 1).CodComp)
                End If
            End If
            
            If (sdbgSelProve.Columns("EQP").CellValue(vbm) = "" And sdbgSelProve.Columns("COM").CellValue(vbm) = "") Then
                oMensajes.FaltanDatos sComprador & vbLf & sproveedor & " " & sdbgSelProve.Columns("PROVECOD").CellValue(vbm)
                Exit Sub
            Else
                If bVerAsigEqp And sdbgSelProve.Columns("COM").CellValue(vbm) = "" Then
                    oMensajes.FaltanDatos sComprador & vbLf & sproveedor & " " & sdbgSelProve.Columns("PROVECOD").CellValue(vbm)
                    Exit Sub
                End If
            End If
            
            If sdbgSelProve.Columns("SEL").CellValue(vbm) = "1" Or sdbgSelProve.Columns("SEL").CellValue(vbm) = "-1" Then
                b = True
                If Not bVerAsigComp And Not bVerAsigEqp Then ' Usuario sin restricciones de equipo o comprador asignado, ve las dos columnas de equipos y compradores
                    If sCompNew = "" Then
                        oMensajes.FaltanDatos sComprador & vbLf & sproveedor & " " & sdbgSelProve.Columns("PROVECOD").CellValue(vbm)
                        Exit Sub
                    End If
                End If
            Else
                b = False
            End If
            If b = oAsigs.Item(i + 1).Asignado Then
               ' NO hay cambio de Estado
               If oAsigs.Item(i + 1).Asignado Then
                    ' si estaba asignado, miramos a ver si se ha producido cambio de equipo y/o comprador
                    If (oAsigs.Item(i + 1).codEqp <> sdbgSelProve.Columns("EQP").CellValue(vbm)) Or (oAsigs.Item(i + 1).CodComp <> sdbgSelProve.Columns("COM").CellValue(vbm)) Then
                        ' se utiliza la propiedad "Asignado" de la coleccion, true para controlar luego si es una asignaci�n (insert) o false s�lo modificaci�n de eqp o com (update)
                        'oAsigDesSeleccionadas.Add sdbgSelProve.Columns("PROVECOD").CellValue(vbm), "", True, sEqpOld, sCompOld
                        oAsigSeleccionadas.Add sdbgSelProve.Columns("PROVECOD").CellValue(vbm), "", False, sEqpNew, sCompNew
                    End If ' si no se ha cambiado nada, no hacemos nada
'               Else
'                    'Si no estaba asignado miramos a ver si se ha producido cambio de equipo y/o comprador
'                    If (oAsigs.Item(i + 1).codEqp <> sdbgSelProve.Columns("EQP").CellValue(vbm)) Or (oAsigs.Item(i + 1).CodComp <> sdbgSelProve.Columns("COM").CellValue(vbm)) Then
'                        oAsigDesSeleccionadas.Add sdbgSelProve.Columns("PROVECOD").CellValue(vbm), "", False, sEqpOld, sCompOld
'                        oAsigSeleccionadas.Add sdbgSelProve.Columns("PROVECOD").CellValue(vbm), "", False, sEqpNew, sCompNew
'                    End If
                End If
            Else
                'Cambio de estado
                If oAsigs.Item(i + 1).Asignado Then
                    ' Ahora se ha desasignado
                    oAsigDesSeleccionadas.Add sdbgSelProve.Columns("PROVECOD").CellValue(vbm), "", False, sEqpOld, sCompOld
'                    If (oAsigs.Item(i + 1).codEqp <> sdbgSelProve.Columns("EQP").CellValue(vbm)) Or (oAsigs.Item(i + 1).CodComp <> sdbgSelProve.Columns("COM").CellValue(vbm)) Then
'                        oAsigSeleccionadas.Add sdbgSelProve.Columns("PROVECOD").CellValue(vbm), "", False, sEqpNew, sCompNew
'                    End If
                Else
                    ' Ahora se ha Asignado, ASig a true para que inserte
                    oAsigSeleccionadas.Add sdbgSelProve.Columns("PROVECOD").CellValue(vbm), "", True, sEqpNew, sCompNew
                End If
            End If
'        Next i
        Next
        
        
        If bVerAsigComp Then
            Screen.MousePointer = vbHourglass
            teserror = oIasig.Asignar(oAsigSeleccionadas, oAsigDesSeleccionadas, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            Screen.MousePointer = vbNormal
        Else
            If bVerAsigEqp Then
                Screen.MousePointer = vbHourglass
                teserror = oIasig.Asignar(oAsigSeleccionadas, oAsigDesSeleccionadas, basOptimizacion.gCodEqpUsuario)
                Screen.MousePointer = vbNormal
            Else
                Screen.MousePointer = vbHourglass
                teserror = oIasig.Asignar(oAsigSeleccionadas, oAsigDesSeleccionadas)
                Screen.MousePointer = vbNormal
            End If
        End If
        
        If teserror.NumError <> TESnoerror Then
            DesbloquearProceso 1
            basErrores.TratarError teserror
            Exit Sub
        End If
        
        If Not bVal Then
            oIasig.FinalizarAsignacion
        End If
        
        basSeguridad.RegistrarAccion AccionesSummit.ACCSelProveMod, "Anyo:" & CStr(sdbcAnyo.Value) & "GMN1:" & CStr(sdbcGMN1_4Cod.Value) & "Proce:" & Int(sdbcProceCod.Value)
        
    End If
    
    sdbgSelProve.Update
    
    Accion = ACCSelProveCon
    Set oIAsigs = Nothing
    
    'Liberamos las colecciones de proveedores que se han modificado
    Set m_aBookmarksModif = Nothing
    
    sdbgSelProve.AllowUpdate = False
    
    picNavigate.Visible = True
    picEdit.Visible = False
    picProceAnya.Enabled = True
    
    cmdRestaurar_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub cmdA�adir_Click()

Dim irespuesta As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oProcesoSeleccionado.Estado >= conasignacionvalida Then
        If bVal Then
            irespuesta = oMensajes.PreguntaModificarSelProveValidada
            If irespuesta = vbNo Then Exit Sub
        Else
            oMensajes.ImposibleModSelProve
            Exit Sub
        End If
    End If
    
    
    If Not BloquearProceso(1) Then Exit Sub
    m_udtOrigBloqueo = AnyadirProv
    
    Accion = ACCSelProveMod
    
    Set frmSELPROVEAnya.oActsN1 = Nothing
    frmSELPROVEAnya.bAnya = bAnya
    frmSELPROVEAnya.sdbcCompCod.Text = ""
    frmSELPROVEAnya.sdbcEqpCod.Text = ""
    frmSELPROVEAnya.sdbcProveCod.Text = ""
    frmSELPROVEAnya.Show
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdA�adir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub cmdBuscar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Accion = ACCSelProveCon Then
        frmPROCEBuscar.bRDest = False
        frmPROCEBuscar.bRUsuAper = bRUsuAper
        frmPROCEBuscar.bRCompResponsable = bRCompResp
        frmPROCEBuscar.bRAsig = bRComp
        frmPROCEBuscar.bREqpAsig = bREqp
        frmPROCEBuscar.bRMat = bRMat
        frmPROCEBuscar.bRUsuDep = m_bRUsuDep
        frmPROCEBuscar.bRUsuUON = m_bRUsuUON
        frmPROCEBuscar.m_bProveAsigComp = bVerAsigComp
        frmPROCEBuscar.m_bProveAsigEqp = bVerAsigEqp
        frmPROCEBuscar.sOrigen = "frmSELPROVE"
        frmPROCEBuscar.sdbcAnyo = sdbcAnyo
        frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
        frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
        frmPROCEBuscar.Show 1
        
        Set oProcesos = Nothing
        Set oProcesos = frmPROCEBuscar.oProceEncontrados
        Set frmPROCEBuscar.oProceEncontrados = Nothing
        Unload frmPROCEBuscar
               
        If Not oProcesos Is Nothing Then
            If oProcesos.Count > 0 Then
                If Not oProcesos.Item(1) Is Nothing Then
                    If oProcesos.Item(1).Estado >= TipoEstadoProceso.validado And oProcesos.Item(1).Estado < TipoEstadoProceso.conasignacionvalida Then
                        ProceSelector1.Seleccion = 0
                    Else
                        If oProcesos.Item(1).Estado >= TipoEstadoProceso.conasignacionvalida And oProcesos.Item(1).Estado < TipoEstadoProceso.ParcialmenteCerrado Then
                            ProceSelector1.Seleccion = 1
                        Else
                            If oProcesos.Item(1).Estado = TipoEstadoProceso.ParcialmenteCerrado Then
                                ProceSelector1.Seleccion = 7
                            Else
                                ProceSelector1.Seleccion = 2
                            End If
                        End If
                    End If
                    
                    sdbcAnyo = oProcesos.Item(1).Anyo
                    bRespetarCombo = True
                    sdbcGMN1_4Cod = oProcesos.Item(1).GMN1Cod
                    sdbcGMN1_4Cod_Validate False
                    sdbcProceCod = oProcesos.Item(1).Cod
                    sdbcProceDen = oProcesos.Item(1).Den
                    sdbcProceCod_Validate False
                    bRespetarCombo = False
                    RestaurarBotones
                    ProcesoSeleccionado
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdBuscar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub cmdCancelar_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Accion = ACCSelProveMod Then
        
        Accion = ACCSelProveCon
        
        DesbloquearProceso (1)
        
        sdbgSelProve.CancelUpdate
        sdbgSelProve.AllowUpdate = False
        
        picNavigate.Visible = True
        picEdit.Visible = False
        picProceAnya.Enabled = True
        
        Set oAsigSeleccionadas = Nothing
        Set oAsigDesSeleccionadas = Nothing
        
        'Liberamos las colecciones de proveedores que se han modificado
        Set m_aBookmarksModif = Nothing
        
        cmdRestaurar_Click
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub cmdColaboracion_Click()
    Dim strSessionId As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    
    With frmEditor
        .g_sRuta = gParametrosGenerales.gsURLCN & "cn_NuevoMensaje_GS.aspx?sessionId=" & strSessionId & "&desdeGS=1"
        .g_sOrigen = "Colaboracion"
        .g_iAnio_ProceCompra = sdbcAnyo.Value
        .g_sGMN1_ProceCompra = sdbcGMN1_4Cod.Value
        .g_iCod_ProceCompra = sdbcProceCod.Value
        .g_bReadOnly = False
        .Show vbModal
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdColaboracion_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdConfQA_Click()
    Dim i As Integer
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    'Variables de calidad
    Set oProveedoresCalidad = Nothing
    Set oProveedoresCalidad = oFSGSRaiz.generar_CProveedores

    'Cargar los datos del proveedor seleccionado para pasarlos al frmConfProveQA
    oProveedoresCalidad.Add sdbgSelProve.Columns("PROVECOD").Value, sdbgSelProve.Columns("PROVEDEN").Value
    
    If m_oVarsCalidad Is Nothing Then
        Set m_oVarsCalidad = oFSGSRaiz.Generar_CVariablesCalidad

        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            m_oVarsCalidad.CargarVariables , oUsuarioSummit.Cod, oProcesoSeleccionado.Anyo, oProcesoSeleccionado.GMN1Cod, oProcesoSeleccionado.Cod, gParametrosGenerales.gbQAVariableMaterialAsig, True, IIf(gParametrosGenerales.gbPymes, basOptimizacion.gPYMEUsuario, 0)
        Else
            m_oVarsCalidad.CargarVariables , , oProcesoSeleccionado.Anyo, oProcesoSeleccionado.GMN1Cod, oProcesoSeleccionado.Cod, gParametrosGenerales.gbQAVariableMaterialAsig, True, IIf(gParametrosGenerales.gbPymes, basOptimizacion.gPYMEUsuario, 0)
        End If
    End If
        
    'Unidades de negocio
    Set m_oUnidadesQa = oFSGSRaiz.Generar_CUnidadesNegQA
    Set m_oUnqaEnProve = oFSGSRaiz.Generar_CUnidadesNegQA
    Set m_oUnqaVarcalEnProve = oFSGSRaiz.Generar_CVariablesCalidad
    m_oUnidadesQa.GenerarEstructuraUnidadesNegQA oUsuarioSummit.Cod, gParametrosInstalacion.gIdioma, gParametrosGenerales.gbPymes, basOptimizacion.gPYMEUsuario, True
    m_bHacer_ProcesoSeleccionado = False
    m_altaNueva = m_oConfProve.DevolverUsuCod(oUsuarioSummit.Cod)
    MostrarFormConfProveQA m_oVarsCalidad, m_oUnidadesQa, m_oUnqaEnProve, m_oConfProve, m_altaNueva, m_bHacer_ProcesoSeleccionado, oUsuarioSummit.Cod, oMensajes, gParametrosGenerales, basOptimizacion.gTipoDeUsuario, _
                            gParametrosInstalacion.gIdioma, oGestorIdiomas
    
    If m_bHacer_ProcesoSeleccionado Then
    
        LockWindowUpdate Me.hWnd
        i = sdbgSelProve.Groups(0).Position
        Me.sdbgSelProve.Scroll -i, 0
        sdbgSelProve.Update
        LockWindowUpdate 0&
    
        Dim arProvesSel() As Long
        
        'Leer los proveedores seleccionados
        With sdbgSelProve
            ReDim Preserve arProvesSel(0)
                     
            .Update
            For i = 0 To .Rows - 1
                If .Columns("SEL").CellValue(.AddItemBookmark(i)) <> "0" Then
                    ReDim Preserve arProvesSel(0 To UBound(arProvesSel) + 1)
                    arProvesSel(UBound(arProvesSel)) = i
                End If
            Next
        End With
        ConfigurarYCargarGrid Tord
        
        LockWindowUpdate Me.hWnd
        
        'Comprobar los proveedores seleccionados
        mbSaltar_sdbgSelProve_Change = True
        If UBound(arProvesSel) > 0 Then
            With sdbgSelProve
                For i = 1 To UBound(arProvesSel)
                    .Bookmark = .AddItemBookmark(arProvesSel(i))
                    .Columns("SEL").Value = "-1"
                Next
                
                .MoveFirst
            End With
        End If
        mbSaltar_sdbgSelProve_Change = False
        
        LockWindowUpdate 0&
    End If
    
Salir:
    mbSaltar_sdbgSelProve_Change = False
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdConfQA_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Resume Salir
    End If
End Sub

''' <summary>
''' Evento al pulsar el bot�n de Eliminar
''' </summary>
''' <returns></returns>
''' <remarks>Tiempo m�ximo</remarks>
''' <revision>JVS 17/03/2011</revision>

Private Sub cmdEliminar_Click()

Dim teserror As TipoErrorSummit
Dim i As Integer
Dim j As Integer
Dim irespuesta As Integer
Dim vbm As Variant
Dim oIAsigs As IAsignaciones
Dim bResponsable As Boolean

Dim arrProveedoresError() As Variant
Dim indiceErr As Integer
Dim bValidar As Boolean

Dim iImposibleDesasignarProveedorPremiumActivo As Integer
Dim iNoModificarProveedorAsignado As Integer
Dim iPremiumAsignadoOtroComprador As Integer
Dim iImposibleDesasignarProveedorOfertas As Integer
Dim iImposibleDesasignarProveedorPeticionesOferta As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    indiceErr = 0
    iImposibleDesasignarProveedorPremiumActivo = 162 ' error ImposibleDesasignarProveedorPremiumActivo
    iNoModificarProveedorAsignado = 204 ' error NoModificarProveedorAsignado
    iPremiumAsignadoOtroComprador = 285 ' error PremiumAsignadoOtroComprador
    iImposibleDesasignarProveedorOfertas = 1213 ' error ImposibleDesasignarProveedorOfertas
    iImposibleDesasignarProveedorPeticionesOferta = 1214 ' error ImposibleDesasignarProveedorPeticionesOferta
    
    ReDim Preserve arrProveedoresError(0 To 1, 0)
    
    If sdbgSelProve.SelBookmarks.Count = 0 Then Exit Sub
    

    If bVal Then
        If oProcesoSeleccionado.Estado >= conasignacionvalida Then
            irespuesta = oMensajes.PreguntaModificarSelProveValidada
            If irespuesta = vbNo Then Exit Sub
        End If
    Else
        'Si el usuario no tiene permiso de validar, no puede modificar una selprove ya validada
        If oProcesoSeleccionado.Estado >= conasignacionvalida Then
            oMensajes.ImposibleModSelProve
            Exit Sub
        End If
    End If
    
    irespuesta = oMensajes.PreguntaEliminar(sidiProveSel)
    If irespuesta = vbNo Then Exit Sub
        
    
    If Not BloquearProceso(1) Then Exit Sub
    m_udtOrigBloqueo = Eliminar

    Accion = ACCSelProveMod
    Set oAsigSeleccionadas = Nothing
    Set oAsigSeleccionadas = oFSGSRaiz.Generar_CAsignaciones
    Set oIAsigs = oProcesoSeleccionado
    bResponsable = False

    For j = 0 To sdbgSelProve.SelBookmarks.Count - 1
        bValidar = True
        sdbgSelProve.Bookmark = sdbgSelProve.SelBookmarks(j)

        vbm = sdbgSelProve.Bookmark
        i = sdbgSelProve.AddItemRowIndex(vbm)
       'Si el proveedor est� asignado tengo que ver si se puede desasignar
        If GridCheckToBoolean(sdbgSelProve.Columns("SEL").Value) = True Then
            'Si es Premium activo no dejo que lo desasignen
            If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                'oMensajes.ImposibleDesasignarProveedorPremiumActivo
                arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                arrProveedoresError(1, indiceErr) = iImposibleDesasignarProveedorPremiumActivo
                indiceErr = indiceErr + 1
                ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                bValidar = False
            End If
            
            'Si tiene dependencias con otras tablas, no dejo que lo desasignen
            If oProcesoSeleccionado.HayOfertasProveedor(sdbgSelProve.Columns("PROVECOD").Value) And bValidar Then
                arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                arrProveedoresError(1, indiceErr) = iImposibleDesasignarProveedorOfertas
                indiceErr = indiceErr + 1
                ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                bValidar = False
            End If
            'Si tiene dependencias con otras tablas, no dejo que lo desasignen
            If oProcesoSeleccionado.HayPeticionesOfertaProveedor(sdbgSelProve.Columns("PROVECOD").Value) And bValidar Then
                arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                arrProveedoresError(1, indiceErr) = iImposibleDesasignarProveedorPeticionesOferta
                indiceErr = indiceErr + 1
                ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                bValidar = False
            End If
            
             ' Si es parcialmente cerrado y se asign� antes del cierre no se puede desasignar
            If sdbgSelProve.Columns("ANTCIERRE").Value And bValidar Then
                'oMensajes.NoModificarProveedorAsignado
                arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                arrProveedoresError(1, indiceErr) = iNoModificarProveedorAsignado
                indiceErr = indiceErr + 1
                ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                bValidar = False
            End If
        End If
        
        If bValidar Then
            oAsigSeleccionadas.Add sdbgSelProve.Columns("PROVECOD").CellValue(vbm), "", GridCheckToBoolean(sdbgSelProve.Columns("SEL").CellValue(vbm)), Null, Null
        End If
    Next j
   
    ' Gestion de Errores
    If arrProveedoresError(0, 0) <> "" Then
        oMensajes.ImposibleDesasignarProveedor arrProveedoresError
    End If
        
    Screen.MousePointer = vbHourglass
    teserror = oIasig.Eliminar(oAsigSeleccionadas)
    Screen.MousePointer = vbNormal
        
    If teserror.NumError <> TESnoerror Then
        DesbloquearProceso (1)
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    DesbloquearProceso 1
        
    basSeguridad.RegistrarAccion AccionesSummit.ACCSelProveMod, "Anyo:" & CStr(sdbcAnyo.Value) & "GMN1:" & CStr(sdbcGMN1_4Cod.Value) & "Proce:" & Int(sdbcProceCod.Value)
        
        
    sdbgSelProve.Update
   
    
    Accion = ACCSelProveCon
    
    sdbgSelProve.AllowUpdate = False
    
    picNavigate.Visible = True
    picEdit.Visible = False
    picProceAnya.Enabled = True
    
    cmdRestaurar_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdEliminar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub cmdGrupos_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
p_Llamar_frmSELPROVEGrupos "ASIGNAR"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdGrupos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub cmdGruposBlq_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

p_Llamar_frmSELPROVEGrupos "BLOQUEAR"

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdGruposBlq_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub cmdlistado_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set ofrmLstPROCE = New frmLstPROCE
    ofrmLstPROCE.sOrigen = "frmSELPROVE"
    ofrmLstPROCE.g_bEsInvitado = oProcesoSeleccionado.Invitado
    If Not oProcesoSeleccionado Is Nothing Then
        ofrmLstPROCE.sdbcAnyo.Text = oProcesoSeleccionado.Anyo
        ofrmLstPROCE.sdbcGMN1Proce_4Cod.Text = oProcesoSeleccionado.GMN1Cod
        ofrmLstPROCE.sdbcGMN1Proce_4Cod_Validate False
        ofrmLstPROCE.txtCod.Text = CStr(oProcesoSeleccionado.Cod)
        ofrmLstPROCE.txtDen.Text = oProcesoSeleccionado.Den
    End If
        
    ofrmLstPROCE.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdlistado_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Al pulsar el bot�n llamaremos a frmInternet para que nos muestre el listado de Microstrategy que corresponda seg�n el origen (guardado en el TAG del bot�n)
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el bot�n cmdMicro; Tiempo m�ximo: 0</remarks>
Private Sub cmdMicro_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
With frmInternet
    .g_sOrigen = Me.Name
    .g_sNombre = cmdMicro.ToolTipText
    .g_sRuta = cmdMicro.Tag
    '.Show
    If MDI.ActiveForm Is Nothing Then
        Me.WindowState = vbNormal
    Else
        If MDI.ActiveForm.WindowState = vbMaximized Then
            Me.WindowState = vbMaximized
        Else
            Me.WindowState = vbNormal
        End If
    End If
    .SetFocus
End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdMicro_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdModificar_Click()
Dim irespuesta As Integer
    
    'Mensaje de advertencia
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bVal Then
        If oProcesoSeleccionado.Estado >= conasignacionvalida Then
            irespuesta = oMensajes.PreguntaModificarSelProveValidada
            If irespuesta = vbNo Then Exit Sub
        End If
    Else
        'Si el usuario no tiene permiso de validar, no puede modificar una selprove ya validada
        If oProcesoSeleccionado.Estado >= conasignacionvalida Then
            oMensajes.ImposibleModSelProve
            Exit Sub
        End If
    End If
    
    If Not BloquearProceso(1) Then Exit Sub
    m_udtOrigBloqueo = Modificar
    
    sdbgSelProve.AllowUpdate = True
    
    picNavigate.Visible = False
    picEdit.Visible = True
    picProceAnya.Enabled = False
    Accion = ACCSelProveMod
        
    
    Set oAsigSeleccionadas = Nothing
    Set oAsigDesSeleccionadas = Nothing
    Set oAsigSeleccionadas = oFSGSRaiz.Generar_CAsignaciones
    Set oAsigDesSeleccionadas = oFSGSRaiz.Generar_CAsignaciones
    
    sdbgSelProve.Columns("EQP").DropDownHwnd = sdbddEqpCod.hWnd
    sdbgSelProve.Columns("COM").DropDownHwnd = sdbddComCod.hWnd
    ' no quitar,si se quita no se desplegan las combos y  no a�ade un blanco
    sdbddEqpCod.AddItem ""
    sdbddComCod.AddItem ""
    
                
    Set m_aBookmarksModif = Nothing
     
    Set m_aBookmarksModif = New Collection
    
    m_bModoEdicion = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdModificar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub cmdResponsable_Click()
    'Si no tiene permisos para modificar el responsable del proceso muestra directamente el detalle del responsable,
    'si no muestra el men� del responsable de proceso
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bAsigRes = False Or oProcesoSeleccionado.Estado >= conadjudicaciones Or oProcesoSeleccionado.Invitado Then
        VerDetalleResponsable
    Else
        Set MDI.g_ofrmOrigenResponsable = Me
        PopupMenu MDI.mnuPOPUPResponsable
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdResponsable_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Public Sub cmdRestaurar_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    ProcesoSeleccionado
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdRestaurar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdValidar_Click()
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim oProcesos As CProcesos
    Dim bRMatC As Boolean
    Dim bRAsigC As Boolean
    Dim bREqpAsigC As Boolean
    Dim bRCompRespC As Boolean
    Dim bRUsuAperC As Boolean
    Dim bRUsuUONC As Boolean
    Dim bRUsuDepC As Boolean
    Dim iRespuestaResp As Integer
    Dim IMatAsig As IMaterialAsignado
    Dim oGruposMAt4 As CGruposMatNivel4
    Dim lIdPerfil As Long
    Dim s As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    irespuesta = oMensajes.PreguntaValidarSelProve
    If irespuesta = vbYes Then
        teserror = oProcesoSeleccionado.ValidacionComprobar(True)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
        'Validamos que todos los proveedores selecionados no est�n dentro del nivel de escalaci�n m�nimo para blquear
        s = oProcesoSeleccionado.BloqProveNivelEscalacion
        If s <> "" Then
            irespuesta = oMensajes.BloqProveNivelEscalacion(Not m_bIgnBlqEscalacion, s)
            If irespuesta = vbNo Then
                Exit Sub
            End If
        End If
        
        'Si el responsable del proceso es distinto del usuario que est� realizando la validaci�n y tiene permisos
        'para modificar el responsable le pregunta si desea asignar otro responsable:
        If bAsigRes = True Then
            If Not oProcesoSeleccionado.responsable Is Nothing Then
                If oProcesoSeleccionado.responsable.Cod = "" Then
                    iRespuestaResp = oMensajes.PreguntaModificarResponsable
                Else
                    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
                        If Not (oUsuarioSummit.comprador.Cod = oProcesoSeleccionado.responsable.Cod And oUsuarioSummit.comprador.codEqp = oProcesoSeleccionado.responsable.codEqp) Then
                            iRespuestaResp = oMensajes.PreguntaModificarResponsable(NullToStr(oProcesoSeleccionado.responsable.nombre) & " " & oProcesoSeleccionado.responsable.Apel)
                        End If
                    Else
                        If Not (basOptimizacion.gvarCodUsuario = oProcesoSeleccionado.responsable.Cod And basOptimizacion.gCodEqpUsuario = oProcesoSeleccionado.responsable.codEqp) Then
                            iRespuestaResp = oMensajes.PreguntaModificarResponsable(NullToStr(oProcesoSeleccionado.responsable.nombre) & " " & oProcesoSeleccionado.responsable.Apel)
                        End If
                    End If
                End If
            Else
                iRespuestaResp = oMensajes.PreguntaModificarResponsable
            End If
            If iRespuestaResp = vbYes Then
                'Muestra el form para seleccionar comprador responsable
                SustituirResponsable True
            ElseIf iRespuestaResp = 10 Then
                'Pregunta si asignar a todos los proveedores el responsable
                g_bAsignarResp = False
                'Se asigna a si mismo como comprador responsable
                If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
                    iRespuestaResp = oMensajes.PreguntaModificarResponsable(iTipoMod:=1)
                    If iRespuestaResp = vbYes Then
                        Set IMatAsig = oUsuarioSummit.comprador
                       'nzg  Set oGruposMAt4 = IMatAsig.DevolverGruposMN4Visibles(oProcesoSeleccionado.GMN1Cod, oProcesoSeleccionado.GMN2Cod, oProcesoSeleccionado.GMN3Cod, oProcesoSeleccionado.GMN4Cod, , True)
                        Dim oMat As CGrupoMatNivel4
                        For Each oMat In oProcesoSeleccionado.MaterialProce
                            Set oGruposMAt4 = IMatAsig.DevolverGruposMN4Visibles(oMat.GMN1Cod, oMat.GMN2Cod, oMat.GMN3Cod, oMat.Cod)
                        Next
                        If oGruposMAt4.Count = 0 Then
                            iRespuestaResp = oMensajes.PreguntaModificarResponsable(iTipoMod:=2)
                            If iRespuestaResp = vbYes Then
                                g_bAsignarResp = True
                            End If
                        Else
                            g_bAsignarResp = True
                        End If
                    End If
                    teserror = oIasig.AsignarResponsable(oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod, g_bAsignarResp)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    RegistrarAccion AccionesSummit.ACCModRespSelProve, "Proceso:" & CStr(oProcesoSeleccionado.Anyo) & " / " & CStr(oProcesoSeleccionado.GMN1Cod) & " / " & CStr(oProcesoSeleccionado.Cod) & "  Responsable: " & oUsuarioSummit.comprador.codEqp & "; " & oUsuarioSummit.comprador.Cod
                    If oProcesoSeleccionado.responsable Is Nothing Then
                        Set oProcesoSeleccionado.responsable = oFSGSRaiz.generar_CComprador
                    End If
                    oProcesoSeleccionado.responsable.Cod = oUsuarioSummit.comprador.Cod
                    oProcesoSeleccionado.responsable.Apel = NullToStr(oUsuarioSummit.comprador.Apel)
                    oProcesoSeleccionado.responsable.nombre = NullToStr(oUsuarioSummit.comprador.nombre)
                    If oProcesoSeleccionado.responsable.nombre <> "" Then
                        cmdResponsable.ToolTipText = NullToStr(oProcesoSeleccionado.responsable.nombre) & " " & NullToStr(oProcesoSeleccionado.responsable.Apel)
                    Else
                        cmdResponsable.ToolTipText = NullToStr(oProcesoSeleccionado.responsable.Apel)
                    End If
                Else
                    'Debe ser un comprador
                    irespuesta = oMensajes.UsuarioNoComprador
                    If irespuesta = vbNo Then
                        Exit Sub
                    End If
                End If
            End If
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIasig.Validar(basOptimizacion.gvarCodUsuario)
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            oMensajes.SelProveValidada
            If IsArray(teserror.Arg1) And gParametrosGenerales.gbPremium Then
                oMensajes.ImposiblePublicarPremiums (teserror.Arg1)
            End If
            basSeguridad.RegistrarAccion AccionesSummit.ACCSelProveVal, "Anyo:" & CStr(sdbcAnyo.Value) & "GMN1:" & CStr(sdbcGMN1_4Cod.Value) & "Proce:" & Int(sdbcProceCod.Value)
            
            ' Paso Automatico Compradores
            If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
                If basPublic.gParametrosInstalacion.giPasoAutomatico <> NoPasar Then
                    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEConsulta)) Is Nothing) Then
                        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestMatComp)) Is Nothing) Then
                            bRMatC = True
                        End If
                        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestComprador)) Is Nothing) Then
                            bRAsigC = True
                        End If
                        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestEquipo)) Is Nothing) Then
                            bREqpAsigC = True
                        End If
                        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestResponsable)) Is Nothing) Then
                            bRCompRespC = True
                        End If
                        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuAper)) Is Nothing) Then
                            bRUsuAperC = True
                        End If
                        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuUON)) Is Nothing) Then
                            bRUsuUONC = True
                        End If
                        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuDep)) Is Nothing) Then
                            bRUsuDepC = True
                        End If
                        
                        If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
                        Set oProcesos = oFSGSRaiz.generar_CProcesos
                        oProcesos.CargarTodosLosProcesosDesde 1, conasignacionvalida, conasignacionvalida, TipoOrdenacionProcesos.OrdPorCod, oProcesoSeleccionado.Anyo, oProcesoSeleccionado.GMN1Cod, , , , oProcesoSeleccionado.Cod, , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMatC, bRAsigC, bRCompRespC, bREqpAsigC, bRUsuAperC, bRUsuUONC, bRUsuDepC, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRPerfUON, lIdPerfil
                        If oProcesos.Count = 0 Then
                            Exit Sub
                        End If
                    Else
                        Exit Sub 'si no tiene permiso para comunicacion con proveedores
                    End If
                Else
                    Exit Sub 'si es = no pasar
                End If
            Else 'si es Administrador
                If basPublic.gParametrosInstalacion.giPasoAutomatico = NoPasar Then
                    Exit Sub
                End If
            End If
            If gParametrosInstalacion.giPasoAutomatico = TipoPasoAutomatico.Preguntar Then
                irespuesta = oMensajes.PreguntaPasoAutOFEpet
                If irespuesta = vbNo Then
                    Exit Sub
                End If
            End If
            'ir a comunicacion
            If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
            Set oProcesos = oFSGSRaiz.generar_CProcesos
            oProcesos.CargarTodosLosProcesosDesde 1, conasignacionvalida, conasignacionvalida, TipoOrdenacionProcesos.OrdPorCod, oProcesoSeleccionado.Anyo, oProcesoSeleccionado.GMN1Cod, , , , oProcesoSeleccionado.Cod, , True, , , , , , , , , , , , , , , , , , , , , , m_bRPerfUON, lIdPerfil
            frmOFEPet.GMN1Seleccionado
            frmOFEPet.bRespetarCombo = True
            frmOFEPet.sdbcAnyo.Value = oProcesoSeleccionado.Anyo
            frmOFEPet.sdbcGMN1_4Cod.Value = oProcesoSeleccionado.GMN1Cod
            frmOFEPet.sdbcProceCod.Value = oProcesoSeleccionado.Cod
            frmOFEPet.sdbcProceDen.Value = oProcesoSeleccionado.Den
            Set frmOFEPet.oProcesos = oProcesos
            frmOFEPet.ProcesoSeleccionado
            frmOFEPet.bRespetarCombo = False
            Unload frmOFEPetAnya
            Unload FrmOBJAnya
            Unload frmADJAnya
            Unload frmSELPROVE
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "cmdValidar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Al entrar se posiciona en el primer proveedor que aparece en la lista
''' Cierra/activa la ventana de "otro provedor"
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Activate()
    Dim mform As Form
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
'        If m_bHacer_ProcesoSeleccionado Then
'            ProcesoSeleccionado
'        End If
        Arrange
    End If
    For Each mform In Forms
        If mform.Name = "frmSELPROVEAnya" Then
            If frmSELPROVEAnya.Visible = True Then
                If frmSELPROVEAnya.bNoDescargar = True Then
                    frmSELPROVEAnya.SetFocus
                    frmSELPROVEAnya.bNoDescargar = False
                Else
                    Unload frmSELPROVEAnya
                End If
            End If
        End If
    Next
    
    If g_bVisor Then
        g_bVisor = False
        LockWindowUpdate 0&
    End If
    
    If Not oProcesoSeleccionado Is Nothing Then
        If Me.Visible Then Me.sdbgSelProve.SetFocus
        Me.sdbgSelProve.MoveFirst
        sdbgSelProve.SelBookmarks.Add sdbgSelProve.Bookmark
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga de recursos
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 3)
        For i = 1 To 3
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        Label2.caption = sIdioma(2) & ":"
        sdbgSelProve.Columns("PROVECOD").caption = sIdioma(1)
        
        'chkVal.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value '5
        Ador.MoveNext
        cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        'cmdResponsable.Caption = Ador(0).Value '10
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        cmdValidar.caption = Ador(0).Value
        Ador.MoveNext
        frmSELPROVE.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value '14
        Ador.MoveNext
        ProceSelector1.DenParaPendientes = Ador(0).Value     '15 ProceSelector -> Pendientes
        Ador.MoveNext
        ProceSelector1.DenParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaTodos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaPendientes = Ador(0).Value    '19 ProceSelector -> P(endientes)
        Ador.MoveNext
        ProceSelector1.AbrevParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaTodos = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value '23 Cod
        sdbcProceCod.Columns(0).caption = Ador(0).Value
        sdbcProceDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value '24 Denominaci�n
        sdbcProceCod.Columns(1).caption = Ador(0).Value
        sdbcProceDen.Columns(0).caption = Ador(0).Value
        sdbgSelProve.Columns("PROVEDEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgSelProve.Columns("EQP").caption = Ador(0).Value
        Ador.MoveNext
        sdbgSelProve.Columns("COM").caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sdbgSelProve.Columns("PROVEPORT").caption = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        sComprador = Ador(0).Value
        Ador.MoveNext
        sproveedor = Ador(0).Value
        Ador.MoveNext
        sEquipo = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        sidiProveSel = Ador(0).Value
        Ador.MoveNext
        sdbgSelProve.Columns("SEL").caption = Ador(0).Value
        Ador.MoveNext
        cmdGrupos.caption = Ador(0).Value '37
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        marTextosAdj(0) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(1) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(2) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(3) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(4) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(5) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(6) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(7) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(8) = Ador(0).Value
        Ador.MoveNext
        marTextosAdj(9) = Ador(0).Value '49
        Ador.MoveNext
        sdbgSelProve.Columns("PUNT").caption = Ador(0).Value '50 Cabecera de la columna puntuaci�n de QA
        Ador.MoveNext
        sdbgSelProve.Columns("COD_ERP").caption = Ador(0).Value '51 C�digo en ERP
        Ador.MoveNext
        sdbgProveERP.Columns("PROVE_ERP").caption = Ador(0).Value '52 Proveedor en ERP
        Ador.MoveNext
        sdbgProveERP.Columns("DEN").caption = Ador(0).Value '53 Descripci�n
        Ador.MoveNext
        cmdGruposBlq.caption = Ador(0).Value '54 Bloquear grupos
        Ador.MoveNext
        '55 - Descripcion de la pantalla frmSelPROVEGrupos, no se pone aqu� pero hay que hacer el movenext para las siguientes descriciones
        Ador.MoveNext
        marTextosAdj(10) = Ador(0).Value 'calif.:
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>
''' Evento que se produce al presionar una tecla
''' </summary>
''' <returns></returns>
''' <param name="KeyPress">C�digo de la tecla</param>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub Form_KeyPress(KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyDelete Then
        cmdEliminar_Click
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "Form_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Evento que se lanza cuando se carga el formulario
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde el evento; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub Form_Load()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
m_bDescargarFrm = False
    Me.Height = 6375
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    If gParametrosGenerales.giINSTWEB = ConPortal Then
        Me.Width = 11820
        sdbgSelProve.Columns("PROVEPORT").Visible = True
    Else
        Me.Width = 9600
        sdbgSelProve.Columns("PROVEPORT").Visible = False
    End If
    
    sdbgSelProve.SplitterVisible = True
    sdbgSelProve.SplitterPos = 1
    
    If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
        sdbgSelProve.Columns("PUNT").Visible = True
    Else
        sdbgSelProve.Columns("PUNT").Visible = False
    End If
    If Not gParametrosGenerales.gbActivarCodProveErp Then
        sdbgSelProve.Columns("COD_ERP").Visible = False
    End If
    
    'Se comprueba que el usuario tenga permiso para consultar las variables de calidad
    blnPermisoVariablesCalidad = oUsuarioSummit.ConsultarVariablesCalidad

    CargarRecursos
    
    PonerFieldSeparator Me
    
    Accion = ACCSelProveCon
    
    CargarAnyos
    ConfigurarSeguridad
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    sdbgSelProve.Columns("CAL1").caption = gParametrosGenerales.gsDEN_CAL1
    sdbgSelProve.Columns("CAL2").caption = gParametrosGenerales.gsDEN_CAL2
    sdbgSelProve.Columns("CAL3").caption = gParametrosGenerales.gsDEN_CAL3
    sdbgSelProve.Columns("VAL1").caption = gParametrosGenerales.gsDEN_CAL1
    sdbgSelProve.Columns("VAL2").caption = gParametrosGenerales.gsDEN_CAL2
    sdbgSelProve.Columns("VAL3").caption = gParametrosGenerales.gsDEN_CAL3
    
    cmdA�adir.Enabled = False
    cmdModificar.Enabled = False
    cmdValidar.Enabled = False
    cmdRestaurar.Enabled = False
    cmdResponsable.Enabled = False
    If m_bMicrostrategy Then
        cmdMicro.Enabled = False
    End If
    cmdListado.Enabled = False
    cmdEliminar.Enabled = False
    cmdGrupos.Enabled = False
    cmdGruposBlq.Enabled = False
    m_bMicrostrategy = gParametrosGenerales.gbListadosMicro And oGestorLisPerExternos.HayLisPerExternos(Me.Name)
    cmdMicro.Visible = m_bMicrostrategy
    If m_bMicrostrategy Then
        Dim Ador As ADODB.Recordset
        Set Ador = oGestorLisPerExternos.DevolverLisPerExterno(Me.Name, gParametrosInstalacion.gIdioma)
        
        If Not Ador Is Nothing Then
            cmdMicro.Tag = Ador(0)
            cmdMicro.ToolTipText = Ador(1)
            
            Ador.Close
            Set Ador = Nothing
        End If
        
        cmdMicro.Enabled = False
    End If
    
    Set oProcesos = oFSGSRaiz.generar_CProcesos
    
    ' Para cargar los compradores del equipo asignado
    Set oEquipo = oFSGSRaiz.generar_CEquipo
    
     If basPublic.gParametrosInstalacion.giLockTimeOut <= 0 Then
        basPublic.gParametrosInstalacion.giLockTimeOut = basParametros.gParametrosGenerales.giLockTimeOut
    End If
    
    oProcesos.EstablecerTiempoDeBloqueo basPublic.gParametrosInstalacion.giLockTimeOut
    
    If g_bVisor Then
        LockWindowUpdate Me.hWnd
    End If
    g_bSoloInvitado = False
    If oUsuarioSummit.EsInvitado Then
        If oUsuarioSummit.Acciones Is Nothing Then
            g_bSoloInvitado = True
        Else
            If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEConsultar)) Is Nothing Then
                g_bSoloInvitado = True
            End If
        End If
    End If
     'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And bRMat Then
        CargarGMN1Automaticamente
    End If
    
    Set m_aBookmarksModif = New Collection
       
    cmdColaboracion.Visible = (gParametrosGenerales.gsAccesoFSCN = TipoAccesoFSCN.AccesoFSCN)
    
    If gParametrosGenerales.gsAccesoFSCN <> TipoAccesoFSCN.AccesoFSCN Then
        If m_bMicrostrategy Then cmdMicro.Left = cmdColaboracion.Left
    End If
    
    cmdConfQA.Top = 105
    If Not m_bMicrostrategy Then
        If gParametrosGenerales.gsAccesoFSCN <> TipoAccesoFSCN.AccesoFSCN Then
            cmdConfQA.Left = cmdResponsable.Left + cmdResponsable.Width + 100
        Else
            cmdConfQA.Left = cmdColaboracion.Left + cmdColaboracion.Width + 100
        End If
    Else
        cmdConfQA.Left = cmdMicro.Left + cmdMicro.Width + 100
    End If
    
    Set m_oConfProve = oFSGSRaiz.Generar_CConfProveQA
    m_altaNueva = m_oConfProve.DevolverUsuCod(oUsuarioSummit.Cod)
    If Not m_altaNueva Then
        'Usuario con configuracion definida
        m_oConfProve.ObtenerConfiguracionUsuario (oUsuarioSummit.Cod)
        
        If m_oConfProve.calificacionesProveedores = 1 Then
            sdbgSelProve.Columns("CAL1").Visible = True
            sdbgSelProve.Columns("CAL2").Visible = True
            sdbgSelProve.Columns("CAL3").Visible = True
        ElseIf m_oConfProve.calificacionesProveedores = 2 Then
            sdbgSelProve.Columns("CAL1").Visible = True
            sdbgSelProve.Columns("CAL2").Visible = True
            sdbgSelProve.Columns("CAL3").Visible = True
            sdbgSelProve.Columns("VAL1").Visible = True
            sdbgSelProve.Columns("VAL2").Visible = True
            sdbgSelProve.Columns("VAL3").Visible = True
        End If
    End If

    cmdConfQA.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Resize()
    
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bActivado Then
    Arrange
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DesbloquearProceso
    
    Unload frmAdmPROVEPortalResult
    Unload frmSELPROVEAnya
    
    Set oGMN1Seleccionado = Nothing
    Set oProcesoSeleccionado = Nothing
    Set oProcesos = Nothing
    Set oGruposMN1 = Nothing
    Set oAsigs = Nothing
    Set oIasig = Nothing
    Set oProveedoresCalidad = Nothing
    Set oVarsCalidad = Nothing
    Set oEquipos = Nothing
    Set oCompradores = Nothing
    'Liberamos la colecci�n de proveedores seleccionados
    Set m_aBookmarks = Nothing
    
    Me.Visible = False
    
    m_bArrangeColFijas = False
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcAnyo_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcAnyo_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcGMN1_4Cod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Evento que se lanza cuando se cambia el valor de la sdbcProceCod
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde el evento; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub sdbcProceCod_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarCombo Then
        bRespetarCombo = True
    
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        
        DesbloquearProceso
        
        If Not oProcesoSeleccionado Is Nothing Then RestaurarBotones
        Set oProcesoSeleccionado = Nothing

        cmdA�adir.Enabled = False
        cmdModificar.Enabled = False
        cmdValidar.Enabled = False
        cmdRestaurar.Enabled = False
        cmdListado.Enabled = False
        cmdEliminar.Enabled = False
        cmdResponsable.Enabled = False
        If m_bMicrostrategy Then
            cmdMicro.Enabled = False
        End If
        cmdResponsable.ToolTipText = ""
        cmdGrupos.Enabled = False
        cmdGruposBlq.Enabled = False
        sdbgSelProve.RemoveAll
        
        bRespetarCombo = False
        
        bCargarComboDesde = True
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceCod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProceCod.Value = "..." Or Trim(sdbcProceCod.Value) = "" Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Exit Sub
    End If
    
    If sdbcProceCod.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
    cmdConfQA.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
        
End Sub

''' <summary>
''' Evento que se lanza cuando se despliega la sdbcProceCod
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde el evento; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub sdbcProceCod_DropDown()
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim oProceso As CProceso
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProceCod.RemoveAll
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    DesbloquearProceso

    If Not oProcesoSeleccionado Is Nothing Then RestaurarBotones
    Set oProcesoSeleccionado = Nothing
    
    cmdModificar.Enabled = False
    cmdValidar.Enabled = False
    cmdRestaurar.Enabled = False
    cmdListado.Enabled = False
    cmdEliminar.Enabled = False
    cmdA�adir.Enabled = False
    cmdResponsable.Enabled = False
    If m_bMicrostrategy Then
        cmdMicro.Enabled = False
    End If
    cmdResponsable.ToolTipText = ""
    cmdGrupos.Enabled = False
    cmdGruposBlq.Enabled = False
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.Conproveasignados
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    End If
    
    For Each oProceso In oProcesos
        sdbcProceCod.AddItem oProceso.Cod & Chr(m_lSeparador) & oProceso.Den & Chr(m_lSeparador) & BooleanToSQLBinary(oProceso.Invitado)
    Next
            
    'If bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbcProceCod_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub
Private Sub sdbcProceCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcProceCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceCod.Rows - 1
            bm = sdbcProceCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceCod_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub
Public Sub sdbcProceCod_Validate(Cancel As Boolean)
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Trim(sdbcProceCod.Text = "") Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        DesbloquearProceso
        Set oProcesoSeleccionado = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdioma(1)
        DesbloquearProceso
        Set oProcesoSeleccionado = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdioma(1)
        DesbloquearProceso
        Set oProcesoSeleccionado = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proceso
    Screen.MousePointer = vbHourglass
            
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.Conproveasignados
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    oProcesos.CargarTodosLosProcesosDesde 1, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod), , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
        
    If oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdioma(2)
        DesbloquearProceso
        Set oProcesoSeleccionado = Nothing
    Else
        bRespetarCombo = True
        sdbcProceDen.Text = oProcesos.Item(1).Den
        
        sdbcProceCod.Columns("COD").Text = sdbcProceCod.Text
        sdbcProceCod.Columns("DEN").Text = sdbcProceDen.Text
        sdbcProceCod.Columns("INVI").Text = BooleanToSQLBinary(oProcesos.Item(1).Invitado)
        
        bRespetarCombo = False
        
        DesbloquearProceso

        Set oProcesoSeleccionado = Nothing
        Set oProcesoSeleccionado = oProcesos.Item(1)
      
        bCargarComboDesde = False
        ProcesoSeleccionado
      
    End If
   
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "CargarAnyos", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 07/01/2013</revision>

Private Sub ConfigurarSeguridad()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEAnyadirProve)) Is Nothing) Then
        bAnya = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPRoveModificar)) Is Nothing) Then
        bModif = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEAsigResp)) Is Nothing) Then
        bAsigRes = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestEquipo)) Is Nothing) Then
        bREqp = True
    End If
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestComprador)) Is Nothing) Then
        bRComp = True
    End If
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestResponsable)) Is Nothing) Then
        bRCompResp = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEValidar)) Is Nothing) Then
        bVal = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestUsuAper)) Is Nothing) Then
        bRUsuAper = True
    End If
    
    If ((basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestAsigEqp)) Is Nothing)) Then
        bVerAsigEqp = True
    End If
    
    If ((basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestAsigComp)) Is Nothing)) Then
        bVerAsigComp = True
    End If
    'Solo se tendr� en cuenta el permiso en caso de que este activado el par�metro interno PARGEN_INTERNO.PROCE_PROVE_GRUPOS
    If gParametrosGenerales.gbProveGrupos Then
        m_bAsignarGrupo = Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEAsignarGrupos)) Is Nothing
        m_bBloquearGrupo = Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEBloquearGrupos)) Is Nothing
    Else
        m_bAsignarGrupo = False
        m_bBloquearGrupo = False
    End If
    m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestUsuUON)) Is Nothing)
    m_bRUsuDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestUsuDep)) Is Nothing)
    m_bRPerfUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestPerfUON)) Is Nothing)
    m_bIgnBlqEscalacion = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEIgnBlqEscalacion)) Is Nothing)
    ConfigurarBotones
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "ConfigurarSeguridad", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Configuraci�n de los botones del formulario
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub ConfigurarBotones()
Dim lLeft As Long
Dim lWidth As Long
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not bModif Then
    cmdModificar.Visible = False
    cmdEliminar.Visible = False
    cmdRestaurar.Left = cmdModificar.Left
    cmdValidar.Left = cmdRestaurar.Left + cmdRestaurar.Width + 150
    cmdA�adir.Visible = False
    cmdGrupos.Visible = False
    cmdGruposBlq.Visible = False
    cmdListado.Left = cmdValidar.Left + cmdValidar.Width + 150
Else
    cmdModificar.Visible = True
    cmdEliminar.Visible = True
    cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 150
    cmdValidar.Left = cmdRestaurar.Left + cmdRestaurar.Width + 150
    cmdA�adir.Left = cmdValidar.Left + cmdValidar.Width + 150
    lLeft = cmdA�adir.Left
    lWidth = cmdA�adir.Width
    If Not m_bAsignarGrupo Then
        cmdGrupos.Visible = False
    Else
        cmdGrupos.Visible = True
        cmdGrupos.Left = cmdA�adir.Left + cmdA�adir.Width + 150
        lLeft = cmdGrupos.Left
        lWidth = cmdGrupos.Width
    End If
    If Not m_bBloquearGrupo Then
        cmdGruposBlq.Visible = False
    Else
        cmdGruposBlq.Visible = True
        cmdGruposBlq.Left = lLeft + lWidth + 150
        lLeft = cmdGruposBlq.Left
        lWidth = cmdGruposBlq.Width
    End If
    cmdListado.Left = lLeft + lWidth + 150
End If



If Not bVal Then
    cmdValidar.Visible = False
    cmdA�adir.Left = cmdValidar.Left
    If Not bModif Then
        cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 150
    ElseIf Not m_bBloquearGrupo Then
        cmdListado.Left = cmdGrupos.Left + cmdGrupos.Width + 150
    ElseIf Not m_bAsignarGrupo Then
        cmdListado.Left = cmdA�adir.Left + cmdA�adir.Width + 150
    Else
        cmdGrupos.Left = cmdA�adir.Left + cmdA�adir.Width + 150
        cmdGruposBlq.Left = cmdGrupos.Left + cmdGrupos.Width + 150
        cmdListado.Left = cmdGruposBlq.Left + cmdGruposBlq.Width + 150
    End If
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "ConfigurarBotones", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>
''' Restauraci�n de los botones del formulario
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub RestaurarBotones()
Dim lLeft As Long
Dim lWidth As Long
    'Visiblilizamos todos los botones
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdColaboracion.Enabled = False
    cmdModificar.Visible = True
    cmdEliminar.Visible = True
    cmdValidar.Visible = True
    cmdA�adir.Visible = True
    cmdEliminar.Left = cmdModificar.Left + cmdModificar.Width + 150
    cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 150
    cmdValidar.Left = cmdRestaurar.Left + cmdRestaurar.Width + 150
    cmdA�adir.Left = cmdValidar.Left + cmdValidar.Width + 150
    lLeft = cmdA�adir.Left
    lWidth = cmdA�adir.Width
    If Not m_bAsignarGrupo Then
        cmdGrupos.Visible = False
    Else
        cmdGrupos.Visible = True
        cmdGrupos.Left = cmdA�adir.Left + cmdA�adir.Width + 150
        lLeft = cmdGrupos.Left
        lWidth = cmdGrupos.Width
    End If
    If Not m_bBloquearGrupo Then
        cmdGruposBlq.Visible = False
    Else
        cmdGruposBlq.Visible = True
        cmdGruposBlq.Left = lLeft + lWidth + 150
        lLeft = cmdGruposBlq.Left
        lWidth = cmdGruposBlq.Width
    End If
    cmdListado.Left = lLeft + lWidth + 150
    
    'Configuramos seguridad para ocultar los que no tienen permiso
    ConfigurarSeguridad
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "RestaurarBotones", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>
''' Procedimiento que se llama cuando hemos seleccionado un proceso
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde las combos de seleccion de proceso; Tiempo m�ximo: 0</remarks>
Public Sub ProcesoSeleccionado()
    Dim sCod As String
    Dim oSobre As CSobre
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sCod = sdbcGMN1_4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod))
    sCod = CStr(sdbcAnyo) & sCod & sdbcProceCod

    DesbloquearProceso

    'Carga los datos del proceso y la vista por defecto del mismo
    oProcesos.CargarProcesoComparativa sdbcAnyo, sdbcGMN1_4Cod, val(sdbcProceCod), oUsuarioSummit.Cod

    Set oProcesoSeleccionado = Nothing
    Set oProcesoSeleccionado = oProcesos.Item(sCod)
        
    If oProcesoSeleccionado Is Nothing Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    oProcesoSeleccionado.CargarDatosGeneralesProceso bSinpres:=True
    'Si es de Admin pub y alguna de las fechas de apertura prevista es null es pq han cambiado a mano los datos
    'le damos un mensaje y no lo cargamos
    If oProcesoSeleccionado.AdminPublica Then
        For Each oSobre In oProcesoSeleccionado.Sobres
            If IsNull(oSobre.FechaAperturaPrevista) Then
                oMensajes.FechaDeSobreCorrupta
                sdbcProceCod.Value = ""
                Set oProcesoSeleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Next
    End If
    
    If oProcesoSeleccionado.Invitado Then
        bAnya = False
        bVal = False
        bModif = False
        bAsigRes = False
        ConfigurarBotones
    End If
    
    'Configurar botones
    cmdA�adir.Enabled = True
    If bVal Then
        cmdValidar.Enabled = True
    Else
        cmdValidar.Enabled = False
    End If
    If bModif Then
        cmdModificar.Enabled = True
        cmdEliminar.Enabled = True
        cmdGrupos.Enabled = True
        cmdGruposBlq.Enabled = True
    Else
        cmdModificar.Enabled = False
        cmdEliminar.Enabled = False
        cmdGrupos.Enabled = False
        cmdGruposBlq.Enabled = False
    End If
    cmdRestaurar.Enabled = True
    cmdListado.Enabled = True
    If oProcesoSeleccionado.Estado >= conadjudicaciones Then
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdModificar.Enabled = False
        cmdValidar.Enabled = False
        cmdGrupos.Enabled = False
        cmdGruposBlq.Enabled = False
    End If
    
    If bVal And oProcesoSeleccionado.Estado < conadjudicaciones Then 'Si pueden validar Bloqueo el proceso y no est� cerrado
        If Not BloquearProceso Then
            sdbcProceCod.Text = ""
            sdbcProceCod.Columns(0).Value = ""
            sdbcProceCod.Columns(1).Value = ""
            sdbcProceCod.RemoveAll
            Exit Sub
        End If
        m_udtOrigBloqueo = PermisoValidar
    End If
    Tord = OrdAsigPorDenProve
    ConfigurarYCargarGrid Tord
        
    'Muestra el responsable del proceso en el tooltiptext
    cmdResponsable.Enabled = True
    If m_bMicrostrategy Then
        cmdMicro.Enabled = True
    End If
    If Not oProcesoSeleccionado.responsable Is Nothing Then
        If oProcesoSeleccionado.responsable.Cod <> "" Then
            If oProcesoSeleccionado.responsable.nombre <> "" Then
                cmdResponsable.ToolTipText = oProcesoSeleccionado.responsable.nombre & " " & oProcesoSeleccionado.responsable.Apel
            Else
                cmdResponsable.ToolTipText = oProcesoSeleccionado.responsable.Apel
            End If
        End If
    End If
        
    cmdConfQA.Enabled = True
    Screen.MousePointer = vbNormal
    
    m_bModoEdicion = False
    
    basSeguridad.RegistrarAccion AccionesSummit.ACCSelProveCon, "Anyo:" & CStr(sdbcAnyo.Value) & " GMN1:" & CStr(sdbcGMN1_4Cod.Value) & " Proce:" & Int(sdbcProceCod.Value)
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "ProcesoSeleccionado", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Configura y carga el grid</summary>
''' <remarks>Llamada desde: ProcesoSeleccionado</remarks>

Private Sub ConfigurarYCargarGrid(ByVal Tord As TipoOrdenacionAsignaciones)
    Dim kx As Long
    Dim z As Integer
    Dim j As Integer
    Dim columnasPunt As Collection
    Dim columnasValor As Collection
    Dim columnaProve As Integer
    Dim oColumn As SSDataWidgets_B.Column
    Dim oAsig As CAsignacion
    Dim b As Integer
    Dim cadenaRs As String
    Dim spunt As Variant
        
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    LockWindowUpdate Me.hWnd
    Screen.MousePointer = vbHourglass
    
    'Cargar los proveedores asignables
    Set oIasig = oProcesoSeleccionado
    Set oAsigs = oIasig.DevolverPosiblesAsignaciones(Tord, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bAnya, bVerAsigComp, bVerAsigEqp, oUsuarioSummit.Cod)
    Set oProveedores = oFSGSRaiz.generar_CProveedores
    
    With sdbgSelProve
        'Configurar grid
        'Nos movemos a la primera columna para que se posicione el scroll al principio y no haya problemas a la hora de pintar la grid
        .col = 0
        If m_oConfProve.calificacionesProveedores = 0 Then
            .Columns("CAL1").Visible = False
            .Columns("CAL2").Visible = False
            .Columns("CAL3").Visible = False
            .Columns("VAL1").Visible = False
            .Columns("VAL2").Visible = False
            .Columns("VAL3").Visible = False
        ElseIf m_oConfProve.calificacionesProveedores = 1 Then
            .Columns("CAL1").Visible = True
            .Columns("CAL2").Visible = True
            .Columns("CAL3").Visible = True
            .Columns("VAL1").Visible = False
            .Columns("VAL2").Visible = False
            .Columns("VAL3").Visible = False
        ElseIf m_oConfProve.calificacionesProveedores = 2 Then
            .Columns("CAL1").Visible = True
            .Columns("CAL2").Visible = True
            .Columns("CAL3").Visible = True
            .Columns("VAL1").Visible = True
            .Columns("VAL2").Visible = True
            .Columns("VAL3").Visible = True
        End If
    
        .Scroll 0, 0
        
        For z = .Columns.Count - 1 To 20 Step -1
            .Columns.Remove (z)
        Next
           
        j = 0
        Set columnasPunt = New Collection
        Set columnasValor = New Collection
        
        'Eliminar los grupos de las var. de calidad
        Dim m As Integer
        'Se ocultan todos los griupos no fijos, que mas tarde se visualizar�n en funcion de las variables de calidad a mostrar
        'Se hace as� y no eliminando los grupos porque sino no hace bien el scroll de lagrid.
        For m = .Groups.Count - 1 To 3 Step -1
            .Groups(m).Visible = False
        Next
        'Si no hay QA no se muestran los grupos de variables de calidad
        'Para las variables de QA se crea un grupo por cada 3 columnas. Si mantenemos un �nico grupo para todas las columnas llega un momento para un
        'n� determinado de columnas que ya no se ven porque el grid no escrolla m�s. El grid, cuando hay grupos, escrolla de grupo en grupo poniendo
        'la primera col. del grupo como primera del grid. Si el resto de columnas del grupo no entran ya no se ven.
        Dim oGrupo As SSDataWidgets_B.Group
        Dim iGroupIndex As Integer
        If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
            If oAsigs.Count > 1 Then
                Dim puntEnc As Boolean
                Dim CaptionAux As String
                Dim TextoCaptionCalif As String
                For j = 1 To oAsigs.Item(1).claves.Count
                    puntEnc = False
                    
                    If (oAsigs.Item(1).claves.Item(j) = "prove") Then
                        columnaProve = j
                        CaptionAux = "##" & oAsigs.Item(1).claves.Item(j)
                    Else
                        CaptionAux = oAsigs.Item(1).claves.Item(j)
                    End If
                    If (InStr(oAsigs.Item(1).claves.Item(j), "#PUNT#") <> 0) Then
                        columnasPunt.Add j
                        puntEnc = True
                        TextoCaptionCalif = ""
                    Else
                        TextoCaptionCalif = marTextosAdj(10)
                        columnasValor.Add j
                    End If
                    
                    'A�adir un grupo cada 3 elementos
                    iGroupIndex = ((j - 1) \ 3) + 3
                    If iGroupIndex > .Groups.Count - 1 Then
                        .Groups.Add (iGroupIndex)
                    Else
                        .Groups(iGroupIndex).Visible = True
                    End If
                    Set oGrupo = .Groups(iGroupIndex)
                    oGrupo.Columns.Add j - (3 * (iGroupIndex - 3)) - 1
                    Set oColumn = oGrupo.Columns(j - (3 * (iGroupIndex - 3)) - 1)
                    If (puntEnc = True) Then
                        CaptionAux = Replace(CaptionAux, "#PUNT#", "")
                    End If
                    oColumn.caption = TextoCaptionCalif & Right(CaptionAux, Len(CaptionAux) - InStr(CaptionAux, "##") - 1)
                    
                    oColumn.Alignment = ssCaptionAlignmentLeft
                Next
            End If
            
            If (m_oConfProve.puntuacionCalificacion = 0) Then
                For m = 3 To .Groups.Count - 1
                    Dim k As Integer
                    For k = .Groups(m).Columns.Count To 1 Step -1
                        .Groups(m).Columns(k - 1).Visible = False
                    Next
                    .Groups(m).Visible = False
                Next
            ElseIf (m_oConfProve.puntuacionCalificacion = 1) Then
                Dim sds As Variant
                
                For Each sds In columnasPunt
                    .Groups(((sds - 1) \ 3) + 3).Columns(sds - (3 * ((sds - 1) \ 3)) - 1).Visible = False
                Next
                
                .ScrollBars = ssScrollBarsBoth
            ElseIf (m_oConfProve.puntuacionCalificacion = 2) Then
                Dim sdv As Variant
                
                For Each sdv In columnasValor
                    .Groups(((sdv - 1) \ 3) + 3).Columns(sdv - (3 * ((sdv - 1) \ 3)) - 1).Visible = False
                Next
            End If
            
            If (columnaProve <> 0) Then .Groups(((columnaProve - 1) \ 3) + 3).Columns(columnaProve - (3 * ((columnaProve - 1) \ 3)) - 1).Visible = False
        End If
        
        Arrange
        
        'Cargar grid
        .RemoveAll
        For Each oAsig In oAsigs
            If oAsig.Asignado Then
                b = 1
                If oProveedores.Item(oAsig.CodProve) Is Nothing Then
                    oProveedores.Add oAsig.CodProve, "", "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                End If
            Else
                b = 0
            End If
            
            cadenaRs = b & Chr(m_lSeparador) & oAsig.CodProve & Chr(m_lSeparador) & oAsig.DenProve & Chr(m_lSeparador) & oAsig.CodPortal & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oAsig.codEqp) & Chr(m_lSeparador) & NullToStr(oAsig.CodComp) & Chr(m_lSeparador) & oAsig.Premium & Chr(m_lSeparador) & oAsig.PremActivo & Chr(m_lSeparador) & oAsig.AntesDeCierreParcial & Chr(m_lSeparador) & oAsig.HayAdjMaterialesProce & Chr(m_lSeparador) & oAsig.HayAdjMaterialesNoProce & Chr(m_lSeparador) & " " & Chr(m_lSeparador) & oAsig.Calidad & Chr(m_lSeparador) & oAsig.Calif1 & Chr(m_lSeparador) & oAsig.Calif2 & Chr(m_lSeparador) & oAsig.Calif3 & Chr(m_lSeparador) & oAsig.Val1 & Chr(m_lSeparador) & oAsig.Val2 & Chr(m_lSeparador) & oAsig.Val3
            
            For Each spunt In oAsig.puntuaciones
                cadenaRs = cadenaRs & Chr(m_lSeparador) & spunt
            Next
            
            .AddItem cadenaRs
        Next
            
        If Me.Visible And Me.Enabled Then .SetFocus
        .MoveFirst
        .SelBookmarks.Add .Bookmark
    End With
    
Salir:
    Screen.MousePointer = vbDefault
    LockWindowUpdate 0&
    
    Set columnasPunt = Nothing
    Set columnasValor = Nothing
    Set oColumn = Nothing
    Set oAsig = Nothing
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "ConfigurarYCargarGrid", err, Erl, , m_bActivado)
        Resume Salir
    End If
End Sub

Private Sub sdbcGMN1_4Cod_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcGMN1_4Cod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcGMN1_4Cod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
      
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1

    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        
        
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    'If bCargarComboDesde And Not oGruposMN1.EOF Then
    If Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcGMN1_4Cod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcGMN1_4Cod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcGMN1_4Cod_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub
Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN1 = Nothing
        
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdioma(3)
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
           
            bCargarComboDesde = False
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
      
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdioma(3)
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
          
            bCargarComboDesde = False
        End If
    
    End If
   
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcGMN1_4Cod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Public Sub GMN1Seleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "GMN1Seleccionado", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>Ajustar el tamanyo y posicion de los controles al tamanyo del formulario</summary>
''' <remarks>Llamada desde: Form_Resize</remarks>

Private Sub Arrange()
    Dim iNumColVis As Integer
    Dim dAnchoFijo As Double
    
    On Error Resume Next
    
    sdbgSelProve.SplitterPos = 1
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Height >= 2200 Then picSel.Height = Height - 1560
    If Width >= 1000 Then picSel.Width = Width - 225
    
    With sdbgSelProve
        .Width = picSel.Width - 100
        .Height = picSel.Height - 140
        
        cmdAceptar.Left = (.Width / 2) - (cmdAceptar.Width / 2) - 300
        cmdCancelar.Left = (.Width / 2) + 300
                
        Dim i As Integer
        For i = 3 To .Groups.Count - 1
            iNumColVis = 0
            If .Groups(i).Visible Then
                If m_oConfProve.puntuacionCalificacion > 0 Then
                    Dim oCol As SSDataWidgets_B.Column
                    For Each oCol In .Groups(i).Columns
                        If oCol.Visible Then
                            iNumColVis = iNumColVis + 1
                            oCol.Width = 2400#
                        End If
                    Next
                End If
                .Groups(i).Width = (2400# * iNumColVis) - 700
            End If
        Next
        
        'Ancho grupo 0
        'Columnas de tama�o fijo
        If Not m_bArrangeColFijas Then
        
            .Columns("SEL").Width = 490
            .Columns("PROVECOD").Width = 2300
            .Columns("PROVEDEN").Width = 5000
            .Groups(0).Width = 490 + 2300 + 5000
        
            If .Columns("PUNT").Visible Then
                dAnchoFijo = 9390#
            Else
                dAnchoFijo = 8400#
            End If
            If .Columns("EQP").Visible Then dAnchoFijo = dAnchoFijo + 1200#
            If .Columns("COM").Visible Then dAnchoFijo = dAnchoFijo + 1500#
            If .Columns("PROVEPORT").Visible Then dAnchoFijo = dAnchoFijo + 2500
            .Groups(1).Width = dAnchoFijo - .Groups(0).Width
        
            .Columns("COD_ERP").Width = 1300
            If .Columns("PUNT").Visible Then
                .Columns("PUNT").Width = 300
            Else
                .Columns("PUNT").Width = 0
            End If
                    
            If bVerAsigEqp And Not bVerAsigComp Then
                .Columns("EQP").Visible = False
                .Columns("COM").Visible = True
                                                        
                .Columns("COM").Width = 1200
            End If
            
            If bVerAsigComp Then
                .Columns("EQP").Visible = False
                .Columns("COM").Visible = False
            End If
            
            If Not bVerAsigEqp And Not bVerAsigComp Then
                .Columns("EQP").Visible = True
                .Columns("COM").Visible = True
                                                        
                .Columns("EQP").Width = 1200
                .Columns("COM").Width = 1500
            End If
                
            If gParametrosGenerales.giINSTWEB <> ConPortal Then
                .Columns("PROVEPORT").Visible = False
            Else
                .Columns("PROVEPORT").Visible = True
                
                .Columns("PROVEPORT").Width = 2500
            End If
            
            m_bArrangeColFijas = True
        Else
            dAnchoFijo = .Groups(0).Width + .Groups(1).Width
        End If
        
        'Ancho grupo 1
        .Groups(2).Visible = True
        If gParametrosGenerales.giINSTWEB <> ConPortal Then
            ' Con restricci�n de Equipo
            If bVerAsigEqp And Not bVerAsigComp Then
                If m_oConfProve.calificacionesProveedores = 2 Then
                    .Columns("VAL1").Visible = True
                    .Columns("VAL2").Visible = True
                    .Columns("VAL3").Visible = True
                                       
                    .Groups(2).Width = 7200
                    .Columns("CAL1").Width = 1200
                    .Columns("CAL2").Width = 1200
                    .Columns("CAL3").Width = 1200
                    .Columns("VAL1").Width = 1200
                    .Columns("VAL2").Width = 1200
                    .Columns("VAL3").Width = 1200
                Else
                    .Columns("VAL1").Visible = False
                    .Columns("VAL2").Visible = False
                    .Columns("VAL3").Visible = False
                                        
                    If m_oConfProve.calificacionesProveedores = 1 Then
                        .Groups(2).Width = 3600
                        .Columns("CAL1").Width = 1200
                        .Columns("CAL2").Width = 1200
                        .Columns("CAL3").Width = 1200
                    Else
                        .Columns("CAL1").Width = 0
                        .Columns("CAL2").Width = 0
                        .Columns("CAL3").Width = 0
                        
                        .Groups(2).Visible = False
                    End If
                End If
            End If
            
            If bVerAsigComp Then
                If m_oConfProve.calificacionesProveedores = 2 Then
                    .Columns("VAL1").Visible = True
                    .Columns("VAL2").Visible = True
                    .Columns("VAL3").Visible = True
                                     
                    .Groups(2).Width = 7200
                    .Columns("CAL1").Width = 1200
                    .Columns("CAL2").Width = 1200
                    .Columns("CAL3").Width = 1200
                    .Columns("VAL1").Width = 1200
                    .Columns("VAL2").Width = 1200
                    .Columns("VAL3").Width = 1200
                Else
                    .Columns("VAL1").Visible = False
                    .Columns("VAL2").Visible = False
                    .Columns("VAL3").Visible = False
                                        
                    If m_oConfProve.calificacionesProveedores = 1 Then
                        .Groups(2).Width = 3600
                        .Columns("CAL1").Width = 1200
                        .Columns("CAL2").Width = 1200
                        .Columns("CAL3").Width = 1200
                    Else
                        .Columns("CAL1").Width = 0
                        .Columns("CAL2").Width = 0
                        .Columns("CAL3").Width = 0
                        
                        .Groups(2).Visible = False
                    End If
                End If
            End If
            ' TODAS LAS COLUMNAS VISIBLES
            If Not bVerAsigEqp And Not bVerAsigComp Then
                If m_oConfProve.calificacionesProveedores = 2 Then
                    .Columns("VAL1").Visible = True
                    .Columns("VAL2").Visible = True
                    .Columns("VAL3").Visible = True
                                        
                    .Groups(2).Width = 7200
                    .Columns("CAL1").Width = 1200
                    .Columns("CAL2").Width = 1200
                    .Columns("CAL3").Width = 1200
                    .Columns("VAL1").Width = 1200
                    .Columns("VAL2").Width = 1200
                    .Columns("VAL3").Width = 1200
                Else
                    .Columns("VAL3").Visible = False
                    .Columns("VAL1").Visible = False
                    .Columns("VAL2").Visible = False
                                    
                    If m_oConfProve.calificacionesProveedores = 1 Then
                        .Groups(2).Width = 3600
                        .Columns("CAL1").Width = 1200
                        .Columns("CAL2").Width = 1200
                        .Columns("CAL3").Width = 1200
                    Else
                        .Columns("CAL1").Width = 0
                        .Columns("CAL2").Width = 0
                        .Columns("CAL3").Width = 0
                        
                        .Groups(2).Visible = False
                    End If
                End If
            End If
        Else
            If bVerAsigEqp And Not bVerAsigComp Then
                If m_oConfProve.calificacionesProveedores = 2 Then
                    .Columns("VAL1").Visible = True
                    .Columns("VAL2").Visible = True
                    .Columns("VAL3").Visible = True
                                
                    .Groups(2).Width = 7200
                    .Columns("CAL1").Width = 1200
                    .Columns("CAL2").Width = 1200
                    .Columns("CAL3").Width = 1200
                    .Columns("VAL1").Width = 1200
                    .Columns("VAL2").Width = 1200
                    .Columns("VAL3").Width = 1200
                Else
                    .Columns("VAL1").Visible = False
                    .Columns("VAL2").Visible = False
                    .Columns("VAL3").Visible = False
                                       
                    If m_oConfProve.calificacionesProveedores = 1 Then
                        .Groups(2).Width = 3600
                        .Columns("CAL1").Width = 1200
                        .Columns("CAL2").Width = 1200
                        .Columns("CAL3").Width = 1200
                    Else
                        .Columns("CAL1").Width = 0
                        .Columns("CAL2").Width = 0
                        .Columns("CAL3").Width = 0
                        
                        .Groups(2).Visible = False
                    End If
                End If
            End If
            
            If bVerAsigComp Then
                If m_oConfProve.calificacionesProveedores = 2 Then
                    .Columns("VAL1").Visible = True
                    .Columns("VAL2").Visible = True
                    .Columns("VAL3").Visible = True
                                        
                    .Groups(2).Width = 7200
                    .Columns("CAL1").Width = 1200
                    .Columns("CAL2").Width = 1200
                    .Columns("CAL3").Width = 1200
                    .Columns("VAL1").Width = 1200
                    .Columns("VAL2").Width = 1200
                    .Columns("VAL3").Width = 1200
                Else
                    DoEvents
                    
                    .Columns("VAL1").Visible = False
                    .Columns("VAL2").Visible = False
                    .Columns("VAL3").Visible = False
                                      
                    If m_oConfProve.calificacionesProveedores = 1 Then
                        .Groups(2).Width = 3600
                        .Columns("CAL1").Width = 1200
                        .Columns("CAL2").Width = 1200
                        .Columns("CAL3").Width = 1200
                    Else
                        .Columns("CAL1").Width = 0
                        .Columns("CAL2").Width = 0
                        .Columns("CAL3").Width = 0
                        
                        .Groups(2).Visible = False
                    End If
                End If
            End If
            
            If Not bVerAsigEqp And Not bVerAsigComp Then
                If m_oConfProve.calificacionesProveedores = 2 Then
                    .Columns("VAL1").Visible = True
                    .Columns("VAL2").Visible = True
                    .Columns("VAL3").Visible = True
                                        
                    .Groups(2).Width = 7200
                    .Columns("CAL1").Width = 1200
                    .Columns("CAL2").Width = 1200
                    .Columns("CAL3").Width = 1200
                    .Columns("VAL1").Width = 1200
                    .Columns("VAL2").Width = 1200
                    .Columns("VAL3").Width = 1200
                Else
                    ' he a�adido 1 al por
                    .Columns("VAL1").Visible = False
                    .Columns("VAL2").Visible = False
                    .Columns("VAL3").Visible = False
                               
                    If m_oConfProve.calificacionesProveedores = 1 Then
                        .Groups(2).Width = 3600
                        .Columns("CAL1").Width = 1200
                        .Columns("CAL2").Width = 1200
                        .Columns("CAL3").Width = 1200
                    Else
                        .Columns("CAL1").Width = 0
                        .Columns("CAL2").Width = 0
                        .Columns("CAL3").Width = 0
                        
                        .Groups(2).Visible = False
                    End If
                End If
            End If
        End If
    End With
    
    AsegurarWidthHomogeneo m_oConfProve.calificacionesProveedores
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>
''' �Alguna se queda con 1010 y otra con 1370? S�, pues otra vez establece el width. Q VB6 redimensiona todas las columns del grupo cada vez q tocas el width de una column.
''' Todas deber�an estar en unos 1200
''' </summary>
''' <param name="QEstaVisible">2-> CAL y VAL    1->CAL           0->nada</param>
Private Sub AsegurarWidthHomogeneo(ByVal QEstaVisible As Integer)
    Dim Vueltas As Integer
    
    With sdbgSelProve
        Select Case QEstaVisible
        Case 2
            While (Vueltas < 3) And (NoValidaWidth(1) Or NoValidaWidth(2) Or NoValidaWidth(3) Or NoValidaWidth(4) Or NoValidaWidth(5) Or NoValidaWidth(6))
                
                .Columns("CAL1").Width = 1200
                .Columns("CAL2").Width = 1200
                .Columns("CAL3").Width = 1200
                .Columns("VAL1").Width = 1200
                .Columns("VAL2").Width = 1200
                .Columns("VAL3").Width = 1200
                            
                Vueltas = Vueltas + 1
            Wend
        Case 1
            While (Vueltas < 3) And (NoValidaWidth(1) Or NoValidaWidth(2) Or NoValidaWidth(3))
                .Columns("CAL1").Width = 1200
                .Columns("CAL2").Width = 1200
                .Columns("CAL3").Width = 1200
            
                Vueltas = Vueltas + 1
            Wend
        Case 0
        End Select
    End With
End Sub

''' <summary>
''' �Alguna se queda con 1010 y otra con 1370?
''' </summary>
''' <param name="Quien">1,2,3-> CAL(quien)   4,5,6->VAL(quien -3)</param>
Private Function NoValidaWidth(ByVal Quien As Integer) As Boolean
    Dim Ancho As Single
    Dim NameCol As String
    
    NoValidaWidth = False
    NameCol = IIf(Quien < 4, "CAL", "VAL") & IIf(Quien < 4, CStr(Quien), CStr(Quien - 3))
    Ancho = sdbgSelProve.Columns(NameCol).Width
    If Ancho < 1100 Or Ancho > 1300 Then NoValidaWidth = True
    
End Function

''' <summary>
''' Evento que se lanza cuando se cambia el valor de la sdbcProceDen
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde el evento; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub sdbcProceDen_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll

        DesbloquearProceso

        If Not oProcesoSeleccionado Is Nothing Then RestaurarBotones
        Set oProcesoSeleccionado = Nothing
        
        bRespetarCombo = False
        
        sdbgSelProve.RemoveAll
        cmdA�adir.Enabled = False
        cmdModificar.Enabled = False
        cmdEliminar.Enabled = False
        cmdValidar.Enabled = False
        cmdResponsable.Enabled = False
        If m_bMicrostrategy Then
            cmdMicro.Enabled = False
        End If
        cmdResponsable.ToolTipText = ""
        cmdListado.Enabled = False
        cmdRestaurar.Enabled = False
        cmdGrupos.Enabled = False
        cmdGruposBlq.Enabled = False
        bCargarComboDesde = True
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceDen_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProceDen.Value = "....." Or Trim(sdbcProceDen.Value) = "" Then
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        Exit Sub
    End If
    
    If sdbcProceDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
        
End Sub


''' <summary>
''' Evento que se lanza cuando se despliega la sdbcProceDen
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde el evento; Tiempo m�ximo: 0</remarks>
Private Sub sdbcProceDen_DropDown()
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim oProceso As CProceso
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProceDen.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
        
    DesbloquearProceso

    If Not oProcesoSeleccionado Is Nothing Then RestaurarBotones
    Set oProcesoSeleccionado = Nothing
    
    cmdA�adir.Enabled = False
    cmdModificar.Enabled = False
    cmdEliminar.Enabled = False
    cmdValidar.Enabled = False
    cmdRestaurar.Enabled = False
    cmdListado.Enabled = False
    cmdResponsable.Enabled = False
    If m_bMicrostrategy Then
        cmdMicro.Enabled = False
    End If
    cmdResponsable.ToolTipText = ""
    cmdGrupos.Enabled = False
    cmdGruposBlq.Enabled = False
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.Conproveasignados
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.validado
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , sdbcProceDen, False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    End If
    
    
    For Each oProceso In oProcesos
        sdbcProceDen.AddItem oProceso.Den & Chr(m_lSeparador) & oProceso.Cod & Chr(m_lSeparador) & BooleanToSQLBinary(oProceso.Invitado)
    Next
    
 
    'If bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbcProceDen_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub
Private Sub sdbcProceDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcProceDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceDen.Rows - 1
            bm = sdbcProceDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbcProceDen_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub sdbddComCod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSelProve.Columns("ANTCIERRE").Value Then
        oMensajes.NoModificarProveedorAsignado
        sdbgSelProve.CancelUpdate
        Exit Sub
    End If

    If bVerAsigEqp Then
        If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
            If sdbgSelProve.Columns("SEL").Value = "-1" Or sdbgSelProve.Columns("SEL").Value = "1" Then
            Else
                ' se intenta asignar el premium, aparece en blanco porque no lo tiene asignado a ninguno de su equipo
                oMensajes.PremiumAsignadoOtroComprador
                sdbgSelProve.CancelUpdate
                Exit Sub
            End If
        End If
    End If

    Screen.MousePointer = vbHourglass
    
    sdbgSelProve.Columns("COM").Value = sdbddComCod.Columns(0).Value
    sdbgSelProve.Columns("COM").Text = sdbddComCod.Columns(0).Value
    sdbgSelProve.Update
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddComCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub



''' <summary>
''' Responde al evento de click en el dropdow del comprador.
''' Si tiene checkeado el permiso de procesos-->seleccion de proveedores-->Permitir seleccionar proveedores sin ajustarse al material o equipo Carga todos , si no los "potenciales"
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddComCod_DropDown()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
                
    oEquipo.Cod = sdbgSelProve.Columns("EQP").Value
        
    sdbddComCod.RemoveAll
    
    If bAnya Then
    
        If bCargarComboDesdeDD Then
            oEquipo.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbddComCod.Text), , , False, True
        Else
            oEquipo.CargarTodosLosCompradores , , , False, False, False, False
        End If
        
        Set oCompradores = oEquipo.Compradores
        
        
    Else
            
        If bCargarComboDesdeDD Then
            Set oCompradores = oIasig.DevolverCompDeEqpAsignables(sdbgSelProve.Columns("PROVECOD").Value, sdbgSelProve.Columns("EQP").Value, sdbgSelProve.ActiveCell.Value, True, False)
        Else
            Set oCompradores = oIasig.DevolverCompDeEqpAsignables(sdbgSelProve.Columns("PROVECOD").Value, sdbgSelProve.Columns("EQP").Value, "")
        End If
    
    End If

    CargarComboConCompradores
    
    sdbgSelProve.ActiveCell.SelStart = 0
    sdbgSelProve.ActiveCell.SelLength = Len(sdbgSelProve.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddComCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbddComCod_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddComCod.DataFieldList = "Column 0"
    sdbddComCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddComCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub sdbddComCod_ValidateList(Text As String, RtnPassed As Integer)
Dim oCompradores As CCompradores
Dim sCompOld As String

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    ''' Si es nulo, correcto
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSelProve.Columns("ANTCIERRE").Value Then ' con proveedor adjudicado en el cierre parcial de proceso
        sCompOld = NullToStr(oAsigs.Item(sdbgSelProve.Row + 1).CodComp)
        If StrComp(sCompOld, Text, vbTextCompare) = 0 Then ' si escriben el mismo c�digo es como si no modificaran
        Else
            RtnPassed = 0
            oMensajes.NoModificarProveedorAsignado
        End If
    Else
        If Text = "" Then
            If sdbgSelProve.Columns("SEL").Value = "-1" Or sdbgSelProve.Columns("SEL").Value = "1" Then
                If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                    RtnPassed = 0
                    oMensajes.ImposibleDesasignarProveedorPremiumActivo
                End If
            End If
        Else
        
            ''' Comprobar la existencia en la lista
            If sdbddComCod.Columns(0).Value = Text Then
                RtnPassed = True
                Exit Sub
            End If
            
            Screen.MousePointer = vbHourglass
            Set oCompradores = oIasig.DevolverCompDeEqpAsignables(sdbgSelProve.Columns("PROVECOD").Value, sdbgSelProve.Columns("EQP").Value, sdbgSelProve.ActiveCell.Value, True)
                    
            If Not oCompradores.Item(1) Is Nothing Then
                RtnPassed = True
                sdbgSelProve.Columns("COM").Value = oCompradores.Item(1).Cod
            Else
                RtnPassed = 0
                oMensajes.NoValido sComprador
            End If
        End If
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddComCod_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbddEqpCod_CloseUp()


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSelProve.Columns("ANTCIERRE").Value Then
        oMensajes.NoModificarProveedorAsignado
        sdbgSelProve.CancelUpdate
        Exit Sub
    End If
    
    sdbgSelProve.Columns("EQP").Value = sdbddEqpCod.Columns(0).Value
    sdbgSelProve.Columns("EQP").Text = sdbddEqpCod.Columns(0).Value
    sdbddComCod_DropDown
    sdbddComCod_CloseUp
        
    sdbgSelProve.Update
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddEqpCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
         
End Sub


''' <summary>
''' Responde al evento de click en el dropdow del equipo.
''' Si tiene checkeado el permiso de procesos-->seleccion de proveedores-->Permitir seleccionar proveedores sin ajustarse al material o equipo Carga todos , si no los "potenciales"
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Private Sub sdbddEqpCod_DropDown()


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    sdbddEqpCod.RemoveAll
    If bAnya Then
    
        Set oEquipos = Nothing
        Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
        If bCargarComboDesdeDD Then
            oEquipos.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbddEqpCod.Text), False, False
        Else
            oEquipos.CargarTodosLosEquipos , , False, True, False
        End If
        
    Else
    
        If bCargarComboDesdeDD Then
            Set oEquipos = oIasig.DevolverEquiposAsignables(sdbgSelProve.Columns("PROVECOD").Value, sdbgSelProve.ActiveCell.Value, True, False)
        Else
            Set oEquipos = oIasig.DevolverEquiposAsignables(sdbgSelProve.Columns("PROVECOD").Value, "")
        End If
        
    End If
    
    CargarComboConEquipos
    
    sdbgSelProve.ActiveCell.SelStart = 0
    sdbgSelProve.ActiveCell.SelLength = Len(sdbgSelProve.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddEqpCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbddEqpCod_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddEqpCod.DataFieldList = "Column 0"
    sdbddEqpCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddEqpCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbddEqpCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbddEqpCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddEqpCod.Rows - 1
            bm = sdbddEqpCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddEqpCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddEqpCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddEqpCod_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbddEqpCod_ValidateList(Text As String, RtnPassed As Integer)
Dim oEqps As CEquipos
Dim sEqpOld As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

    If sdbgSelProve.Columns("ANTCIERRE").Value Then ' con proveedor adjudicado en el cierre parcial de proceso
        sEqpOld = NullToStr(oAsigs.Item(sdbgSelProve.Row + 1).codEqp)
        If StrComp(sEqpOld, Text, vbTextCompare) = 0 Then ' si escriben el mismo c�digo es como si no modificaran
        Else
            RtnPassed = 0
            oMensajes.NoModificarProveedorAsignado
        End If
    Else
        If Text = "" Then
            'DoEvents
            If sdbgSelProve.Columns("SEL").Value = "-1" Or sdbgSelProve.Columns("SEL").Value = "1" Then
                If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                    RtnPassed = 0
                    oMensajes.ImposibleDesasignarProveedorPremiumActivo
                Else
                    sdbgSelProve.Columns("COM").Value = ""
                    RtnPassed = True
                    'sdbgSelProve.Update
                End If
            Else
                sdbgSelProve.Columns("COM").Value = ""
                RtnPassed = True
            End If
            '''sdbgSelProve.Update ' Esto lo hace que se bucle
        Else
            ''' Comprobar la existencia en la lista
            If sdbddEqpCod.Columns(0).Value = Text Then
                RtnPassed = True
                Exit Sub
            End If
            
            Screen.MousePointer = vbHourglass
            Set oEqps = oIasig.DevolverEquiposAsignables(sdbgSelProve.Columns("PROVECOD").Value, sdbgSelProve.ActiveCell.Value, True, False)
            
            If Not oEqps.Item(1) Is Nothing Then
                RtnPassed = True
                sdbgSelProve.Columns("EQP").Value = oEqps.Item(1).Cod
                sdbddComCod_DropDown
                sdbddComCod_CloseUp
            Else
                RtnPassed = 0
                oMensajes.NoValido sEquipo
            End If
            Screen.MousePointer = vbNormal
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbddEqpCod_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Evento que se produce al realizar click en un bot�n del grid
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>EFS 01/10/2014</revision>
Private Sub sdbgSelProve_BtnClick()
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSelProve.col < 0 Then Exit Sub
    
    Select Case sdbgSelProve.Columns(sdbgSelProve.col).Name
        Case "PUNT"
            If sdbgSelProve.Columns("PUNT_VALOR").Value = "1" Then
                If blnPermisoVariablesCalidad Then
                    Call ComparativaQA
                End If
            End If
        Case "COD_ERP"
            sdbgProveERP.RemoveAll
            CargarCodigosERP sdbgSelProve.Columns("PROVECOD").Value
            If sdbgProveERP.Rows = 0 Then Exit Sub
            
            sdbgProveERP.Left = sdbgSelProve.Columns(sdbgSelProve.col).Left + 150
            sdbgProveERP.Top = (sdbgSelProve.Row * 240) + 850
            sdbgProveERP.Visible = True
            sdbgProveERP.ZOrder
            SetCursorPos sdbgSelProve.ActiveCell.Left + 100, sdbgSelProve.ActiveCell.Top + 100
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbgSelProve_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Evento que se produce al realizar alg�n cambio en la grid
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>JVS 15/03/2011</revision>
Private Sub sdbgSelProve_Change()
    Dim bk As Variant
    Dim arrProveedoresError() As Variant
    Dim vValor, vValorAnt As Integer
    Dim indiceErr As Integer
    Dim bValidar As Boolean
    Dim iImposibleDesasignarProveedorPremiumActivo As Integer
    Dim iNoModificarProveedorAsignado As Integer
    Dim iPremiumAsignadoOtroComprador As Integer
    Dim iImposibleDesasignarProveedorOfertas As Integer
    Dim iImposibleDesasignarProveedorPeticionesOferta As Integer
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    'insercion por incorporacion del estado parcialmente cerrado,
    'si no esta el proceso parcialmente cerrado

    If mbSaltar_sdbgSelProve_Change Then Exit Sub

    indiceErr = 0
    iImposibleDesasignarProveedorPremiumActivo = 162 ' error ImposibleDesasignarProveedorPremiumActivo
    iNoModificarProveedorAsignado = 204 ' error NoModificarProveedorAsignado
    iPremiumAsignadoOtroComprador = 285 ' error PremiumAsignadoOtroComprador
    iImposibleDesasignarProveedorOfertas = 1213 ' error ImposibleDesasignarProveedorOfertas
    iImposibleDesasignarProveedorPeticionesOferta = 1214 ' error ImposibleDesasignarProveedorPeticionesOferta
    
    ReDim Preserve arrProveedoresError(0 To 1, 0)

    If sdbgSelProve.col = -1 Then Exit Sub
    
    If sdbgSelProve.Columns(sdbgSelProve.col).Name = "SEL" Then
        If m_bModoEdicion Then
            If Not m_aBookmarks Is Nothing Then
                If RegistroEstaSeleccionado(sdbgSelProve.Bookmark, m_aBookmarks) Then
                    If m_aBookmarks.Count > 0 Then
                        Screen.MousePointer = vbHourglass
                        'sdbgSelProve.Visible = False
                        vValor = sdbgSelProve.Columns(sdbgSelProve.col).Value
                        'sdbgSelProve.Update
                        LockWindowUpdate Me.hWnd
                        For Each bk In m_aBookmarks
                            bValidar = True
                            sdbgSelProve.SelBookmarks.Add bk
                            sdbgSelProve.Bookmark = bk
                            vValorAnt = sdbgSelProve.Columns("SEL").Value
                            If vValorAnt <> vValor Then
                                sdbgSelProve.Columns("SEL").Value = vValor
                            End If
                            A�adeProveedor bk
                            If GridCheckToBoolean(sdbgSelProve.Columns("SEL").Value) = False Then
                                'Si es Premium activo no dejo que lo desasignen
                                If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                                    'oMensajes.ImposibleDesasignarProveedorPremiumActivo
                                    arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                                    arrProveedoresError(1, indiceErr) = iImposibleDesasignarProveedorPremiumActivo
                                    indiceErr = indiceErr + 1
                                    ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                                    If vValorAnt <> vValor Then
                                        sdbgSelProve.Columns("SEL").Value = vValorAnt
                                    Else
                                        sdbgSelProve.Columns("SEL").Value = Not vValorAnt
                                    End If
                                    bValidar = False
                                End If
                                
                                'Si tiene dependencias con otras tablas, no dejo que lo desasignen
                                If oProcesoSeleccionado.HayOfertasProveedor(sdbgSelProve.Columns("PROVECOD").Value) And bValidar Then
                                    arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                                    arrProveedoresError(1, indiceErr) = iImposibleDesasignarProveedorOfertas
                                    indiceErr = indiceErr + 1
                                    ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                                    If vValorAnt <> vValor Then
                                        sdbgSelProve.Columns("SEL").Value = vValorAnt
                                    Else
                                        sdbgSelProve.Columns("SEL").Value = Not vValorAnt
                                    End If
                                    bValidar = False
                                End If
                                'Si tiene dependencias con otras tablas, no dejo que lo desasignen
                                If oProcesoSeleccionado.HayPeticionesOfertaProveedor(sdbgSelProve.Columns("PROVECOD").Value) And bValidar Then
                                    arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                                    arrProveedoresError(1, indiceErr) = iImposibleDesasignarProveedorPeticionesOferta
                                    indiceErr = indiceErr + 1
                                    ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                                    If vValorAnt <> vValor Then
                                        sdbgSelProve.Columns("SEL").Value = vValorAnt
                                    Else
                                        sdbgSelProve.Columns("SEL").Value = Not vValorAnt
                                    End If
                                    bValidar = False
                                End If
                                
                                
                            End If
                            If bVerAsigComp Then
                            ' Con restricci�n de comprador se mantienen las validaciones en sdbgSelprove
                                If sdbgSelProve.Columns("ANTCIERRE").Value Then
                                    'sdbgSelProve.CancelUpdate
                                    sdbgSelProve.AllowUpdate = True
                                    picNavigate.Visible = False
                                    picEdit.Visible = True
                                    picProceAnya.Enabled = False
                                    Accion = ACCSelProveMod
                                    'oMensajes.NoModificarProveedorAsignado
                                    arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                                    arrProveedoresError(1, indiceErr) = iNoModificarProveedorAsignado
                                    indiceErr = indiceErr + 1
                                    ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                                    'Exit Sub
                                End If
                                If sdbgSelProve.Columns("SEL").Value = "-1" Or sdbgSelProve.Columns("SEL").Value = "1" Then
                                    If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                                        'oMensajes.PremiumAsignadoOtroComprador
                                        'sdbgSelProve.CancelUpdate
                                        arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                                        arrProveedoresError(1, indiceErr) = iPremiumAsignadoOtroComprador
                                        indiceErr = indiceErr + 1
                                        ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                                        'Exit Sub
                                    Else
                                        sdbgSelProve.Update
                                    End If
                                Else
                                    'Si es Premium activo no dejo que lo desasignen
                                    If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                                        'oMensajes.ImposibleDesasignarProveedorPremiumActivo
                                        arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                                        arrProveedoresError(1, indiceErr) = iImposibleDesasignarProveedorPremiumActivo
                                        indiceErr = indiceErr + 1
                                        ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                                        'sdbgSelProve.CancelUpdate
                                        'Exit Sub
                                    Else
                                        sdbgSelProve.Update
                                    End If
                                End If
                            Else
                                If (sdbgSelProve.Columns("EQP").Value = "" Or sdbgSelProve.Columns("COM").Value = "") And (sdbgSelProve.Columns("SEL").Value = "-1" Or sdbgSelProve.Columns("SEL").Value = "1") Then
                                    If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                                        arrProveedoresError(0, indiceErr) = sdbgSelProve.Columns("PROVECOD").Value
                                        arrProveedoresError(1, indiceErr) = iImposibleDesasignarProveedorPremiumActivo
                                        indiceErr = indiceErr + 1
                                        ReDim Preserve arrProveedoresError(0 To 1, indiceErr)
                                        'oMensajes.ImposibleDesasignarProveedorPremiumActivo
                                        'sdbgSelProve.CancelUpdate
                                        'Exit Sub
                                    'ElseIf (sdbgSelProve.Columns("EQP").Value = "" And sdbgSelProve.Columns("COM").Value = "") Then
                                        'oMensajes.FaltanDatos sComprador & vbLf & sproveedor & " " & sdbgSelProve.Columns("PROVECOD").Value
                                        'sdbgSelProve.CancelUpdate
'                                    Else
'                                        sdbgSelProve.Update
                                    End If
                                End If
                                sdbgSelProve.Update
                            End If
                        Next
                        LockWindowUpdate 0&
                        Set m_aBookmarks = Nothing
                        
                        sdbgSelProve.SelBookmarks.RemoveAll
                        sdbgSelProve.Refresh
                        
                        ' Gestion de Errores
                        If arrProveedoresError(0, 0) <> "" Then
                            oMensajes.ImposibleDesasignarProveedor arrProveedoresError
                        End If

                        Screen.MousePointer = vbNormal
                        'sdbgSelProve.Visible = True
                        Exit Sub
                    End If   ' m_aBookmarks.Count > 0
                Else
                    Set m_aBookmarks = Nothing
                    
                    sdbgSelProve.SelBookmarks.RemoveAll
                    sdbgSelProve.Refresh
                End If   ' RegistroEstaSeleccionado(sdbgSelProve.Bookmark, m_aBookmarks)
            End If   ' Not m_aBookmarks Is Nothing
        Else
            'Si no estamos en modo edicion quitamos la seleccion del check
            sdbgSelProve.CancelUpdate
            Exit Sub
        End If
    End If   ' sdbgSelProve.Columns(sdbgSelProve.col).Name = "SEL"
    
    If sdbgSelProve.Columns(sdbgSelProve.col).Name = "EQP" Then    'Columna de Equipo
            A�adeProveedor sdbgSelProve.Bookmark
    End If
        
    If sdbgSelProve.Columns(sdbgSelProve.col).Name = "COM" Then    'Columna de Comprador
            A�adeProveedor sdbgSelProve.Bookmark
    End If
        
    If sdbgSelProve.col = 0 Then    'Columna de la selecci�n positiva
         A�adeProveedor sdbgSelProve.Bookmark
         If GridCheckToBoolean(sdbgSelProve.Columns("SEL").Value) = False Then
            'Si es Premium activo no dejo que lo desasignen
            If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                oMensajes.ImposibleDesasignarProveedorPremiumActivo
                sdbgSelProve.CancelUpdate
                Exit Sub
            End If
            'Si tiene dependencias con otras tablas, no dejo que lo desasignen
            If oProcesoSeleccionado.HayOfertasProveedor(sdbgSelProve.Columns("PROVECOD").Value) Then
                oMensajes.ImposibleDesasignarProveedorOfertas
                sdbgSelProve.CancelUpdate
                Exit Sub
            End If
            'Si tiene dependencias con otras tablas, no dejo que lo desasignen
            If oProcesoSeleccionado.HayPeticionesOfertaProveedor(sdbgSelProve.Columns("PROVECOD").Value) Then
                oMensajes.ImposibleDesasignarProveedorPeticionesOferta
                sdbgSelProve.CancelUpdate
                Exit Sub
            End If
        End If
        sdbgSelProve.Update
        Exit Sub
    End If
    
    If bVerAsigComp Then
    ' Con restricci�n de comprador se mantienen las validaciones en sdbgSelprove
        If sdbgSelProve.Columns("ANTCIERRE").Value Then
            sdbgSelProve.CancelUpdate
            sdbgSelProve.AllowUpdate = True
            picNavigate.Visible = False
            picEdit.Visible = True
            picProceAnya.Enabled = False
            Accion = ACCSelProveMod
            oMensajes.NoModificarProveedorAsignado
'            Exit Sub
        End If
        If sdbgSelProve.Columns("SEL").Value = "-1" Or sdbgSelProve.Columns("SEL").Value = "1" Then
            If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                oMensajes.PremiumAsignadoOtroComprador
                sdbgSelProve.CancelUpdate
                Exit Sub
            Else
                sdbgSelProve.Update
            End If
        Else
            'Si es Premium activo no dejo que lo desasignen
            If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                oMensajes.ImposibleDesasignarProveedorPremiumActivo
                sdbgSelProve.CancelUpdate
                Exit Sub
            Else
                sdbgSelProve.Update
            End If
        End If
    Else
        If (sdbgSelProve.Columns("EQP").Value = "" Or sdbgSelProve.Columns("COM").Value = "") And (sdbgSelProve.Columns("SEL").Value = "-1" Or sdbgSelProve.Columns("SEL").Value = "1") Then
            If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgSelProve.Columns("ACTIVO").Value Then
                oMensajes.ImposibleDesasignarProveedorPremiumActivo
                sdbgSelProve.CancelUpdate
                Exit Sub
            'ElseIf (sdbgSelProve.Columns("EQP").Value = "" And sdbgSelProve.Columns("COM").Value = "") Then
                'oMensajes.FaltanDatos sComprador & vbLf & sproveedor & " " & sdbgSelProve.Columns("PROVECOD").Value
                'sdbgSelProve.CancelUpdate
            Else
                sdbgSelProve.Update
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbgSelProve_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgSelProve_DblClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSelProve.Rows > 0 Then
        If sdbgSelProve.Row >= 0 Then
            If sdbgSelProve.Columns("HAYADJMATP").Value Or sdbgSelProve.Columns("HAYADJMATNOP").Value Then
                AbrirAdjudicacionesProveedor sdbgSelProve.Columns("PROVECOD").Value, sdbgSelProve.Columns("PROVEDEN").Value
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbgSelProve_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Abre la pantalla de adjudicaciones del proveedor</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <param name="sProveDen">Denominaci�n del proveedor</param>
''' <remarks>Llamada desde Evento sdbgSelProve_DblClick</remarks>
Private Sub AbrirAdjudicacionesProveedor(ByVal scodProve As String, ByVal sProveDen As String)

    MostarFormAdjProve marTextosAdj, scodProve, sProveDen, oProcesoSeleccionado.Anyo, oProcesoSeleccionado.GMN1Cod, oProcesoSeleccionado.Cod, oProcesoSeleccionado.MonCod, oProcesoSeleccionado.Cambio, oFSGSRaiz, gParametrosGenerales.gIdioma, gParametrosInstalacion.gIdiomaPortal, basParametros.gParametrosGenerales.gsDEN_GMN1, basParametros.gParametrosGenerales.gsDEN_GMN2, basParametros.gParametrosGenerales.gsDEN_GMN3, basParametros.gParametrosGenerales.gsDEN_GMN4

End Sub

Private Sub sdbgSelProve_HeadClick(ByVal ColIndex As Integer)
    Dim oAsig As CAsignacion
    Dim b As Integer
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Accion = ACCSelProveMod Then Exit Sub
    
    If oProcesoSeleccionado Is Nothing Then Exit Sub
    
    DesbloquearProceso
    
    'Cargar los proveedores asignables
    Set oIasig = oProcesoSeleccionado
    Set oAsigs = Nothing
    
    Select Case ColIndex
        Case 0
                Tord = OrdAsigPorAsig
        Case 1
                Tord = OrdAsigPorCodProve
        Case 2
                Tord = OrdAsigPorDenProve
        Case 3
                Tord = OrdAsigPorCodPortal
        Case 4
                Tord = OrdAsigPorCal1
        Case 5
                Tord = OrdAsigPorCal2
        Case 6
                Tord = OrdAsigPorCal3
        Case 7
                Tord = OrdAsigPorval1
        Case 8
                Tord = OrdAsigPorval2
        Case 9
                Tord = OrdAsigPorval3
        Case 10
                Tord = OrdAsigPorEqp
        Case 11
                Tord = OrdAsigPorComp
        Case 17
                Tord = OrdAsigPorCalidad
        Case Else
                Tord = OrdAsigPorDenProve
    End Select
    
    ConfigurarYCargarGrid Tord
        
    sdbgSelProve.ScrollBars = ssScrollBarsBoth

    'Bloquea otra vez el proceso:
    If bVal And oProcesoSeleccionado.Estado < conadjudicaciones Then 'Si pueden validar Bloqueo el proceso y no est� cerrado
        If Not BloquearProceso Then Exit Sub
        m_udtOrigBloqueo = PermisoValidar
    End If
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbgSelProve_HeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Evento que se produce al presionar una tecla
''' </summary>
''' <returns></returns>
''' <param name="KeyCode">C�digo de la tecla</param>
''' <param name="Shift">Pulsaci�n de la tecla Shift</param>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>JVS 18/03/2011</revision>
Private Sub sdbgSelProve_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyDelete Then
        cmdEliminar_Click
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbgSelProve_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgSelProve_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iVisibleRow As Integer
    Dim iVisibleCol As Integer
    Dim vBmk As Variant
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgProveERP.Visible = False
    
    With sdbgSelProve
        If .WhereIs(X, Y) = ssWhereIsData Then
            iVisibleRow = .RowContaining(Y)
            iVisibleCol = .ColContaining(X, Y)
            
            vBmk = .RowBookmark(iVisibleRow)
            
            If iVisibleRow > -1 Then
                If iVisibleCol >= 20 Then
                    .ToolTipText = .Columns(iVisibleCol).CellValue(vBmk)
                Else
                    .ToolTipText = ""
                End If
            End If
        Else
            iVisibleCol = .ColContaining(X, Y)
            
            If iVisibleCol >= 20 Then
                .ToolTipText = .Columns(iVisibleCol).caption
            Else
                .ToolTipText = ""
            End If
        End If
    End With
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbgSelProve_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Evento que se produce al cambiar de columna o de fila de la grid
''' </summary>
''' <returns></returns>
''' <param name="LastRow">Linea</param>
''' <param name="LastCol">Columna</param>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>JVS 15/03/2011</revision>
Private Sub sdbgSelProve_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    Dim bk As Variant

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSelProve.col = -1 Then
        Set m_aBookmarks = New Collection
        For Each bk In sdbgSelProve.SelBookmarks
            m_aBookmarks.Add bk
        Next
    Else
        If sdbgSelProve.Columns(sdbgSelProve.col).Name = "SEL" Then
            If sdbgSelProve.SelBookmarks.Count = 0 And Not m_aBookmarks Is Nothing Then
                For Each bk In m_aBookmarks
                    sdbgSelProve.SelBookmarks.Add bk
                Next
            End If
        Else
            Set m_aBookmarks = Nothing
        End If
    End If
    
    'Si es un potencial no le dejo modificar eqp y com
    If GridCheckToBoolean(sdbgSelProve.Columns("SEL").Value) = True Then
        sdbgSelProve.Columns("EQP").Locked = False
        sdbgSelProve.Columns("COM").Locked = False
        sdbgSelProve.Columns("EQP").DropDownHwnd = sdbddEqpCod.hWnd
        sdbgSelProve.Columns("COM").DropDownHwnd = sdbddComCod.hWnd
    Else
        sdbgSelProve.Columns("EQP").Locked = True
        sdbgSelProve.Columns("COM").Locked = True
        sdbgSelProve.Columns("EQP").DropDownHwnd = 0
        sdbgSelProve.Columns("COM").DropDownHwnd = 0
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbgSelProve_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbgSelProve_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSelProve.Columns("SEL").Value = "-1" Or sdbgSelProve.Columns("SEL").Value = "1" Then
        For i = 0 To sdbgSelProve.Cols - 1
            sdbgSelProve.Columns(i).CellStyleSet "Asignado"
        Next i
        
        If sdbgSelProve.Columns("HAYADJMATP").Value Then
            sdbgSelProve.Columns("PROVECOD").CellStyleSet "AsignadoConAdj"
        ElseIf sdbgSelProve.Columns("HAYADJMATNOP").Value Then
            sdbgSelProve.Columns("PROVECOD").CellStyleSet "AsignadoConAdjOtroMat"
        Else
            sdbgSelProve.Columns("PROVECOD").CellStyleSet "Asignado"
        End If
    Else
        If sdbgSelProve.Columns("HAYADJMATP").Value Then
            sdbgSelProve.Columns("PROVECOD").CellStyleSet "NormalConAdj"
        ElseIf sdbgSelProve.Columns("HAYADJMATNOP").Value Then
            sdbgSelProve.Columns("PROVECOD").CellStyleSet "NormalConAdjOtroMat"
        Else
            sdbgSelProve.Columns("PROVECOD").CellStyleSet "Normal"
        End If
    End If
    
    If sdbgSelProve.Columns("PUNT_VALOR").Value = "1" Then
        'sdbgSelProve.Columns("PUNT").Text = " "
        sdbgSelProve.Columns("PUNT").CellStyleSet "Calidad"
    End If
        
    If sdbgSelProve.Columns("PROVEPORT").Text <> "" Then
        sdbgSelProve.Columns("PROVEPORT").CellStyleSet "Tan"
    End If

    If sdbgSelProve.Columns("PREMIUM").Value And gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium Then
        If sdbgSelProve.Columns("ACTIVO").Value Then
            sdbgSelProve.Columns("PROVEPORT").CellStyleSet "Premium"
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "sdbgSelProve_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub ProceSelector1_Click(ByVal Opcion As SelectorDeProcesos.PSSeleccion)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "ProceSelector1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Procedimiento para abrir el formulario de valoracion de QA
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde el evento sdbgSelProve_btnClick ; Tiempo m�ximo</remarks>
''' <revision>EFS 01/10/2014</revision>

Public Sub ComparativaQA()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProveedoresCalidad = Nothing
    Set oProveedoresCalidad = oFSGSRaiz.generar_CProveedores
        
    If oProveedoresCalidad.TienePuntuaciones(sdbgSelProve.Columns("PROVECOD").Value) Then
       'Cargar los datos del proveedor seleccionado para pasarlos al frmComparativaQA
       oProveedoresCalidad.Add sdbgSelProve.Columns("PROVECOD").Value, sdbgSelProve.Columns("PROVEDEN").Value
       
       If oVarsCalidad Is Nothing Then
           Set oVarsCalidad = oFSGSRaiz.Generar_CVariablesCalidad
           'Cargar las variables de calidad
            oVarsCalidad.CargarVariables , oUsuarioSummit.Cod, oProcesoSeleccionado.Anyo, oProcesoSeleccionado.GMN1Cod, oProcesoSeleccionado.Cod, gParametrosGenerales.gbQAVariableMaterialAsig, True, IIf(gParametrosGenerales.gbPymes, basOptimizacion.gPYMEUsuario, 0)
       End If
     
       Set frmComparativaQA.m_oProcesoSeleccionado = oProcesoSeleccionado
       Set frmComparativaQA.m_oProvesAsig = oProveedoresCalidad
       Set frmComparativaQA.m_oVarsCalidad = oVarsCalidad
        
       frmComparativaQA.Show vbNormal, MDI
    Else
        oMensajes.ProveedorSinPuntuaciones
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "ComparativaQA", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


Private Function BloquearProceso(Optional ByVal iComo As Integer) As Boolean
Dim udtTeserror As TipoErrorSummit

'***************************************************************************************************************
'Bloquea el proceso
'iComo = 1 Tengo que comprobar si tiene permiso de validar, si lo tiene ya est� bloqueado
'***************************************************************************************************************
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbNormal
    
    If Not oIasig Is Nothing Then
        If iComo = 1 And bVal Then
            BloquearProceso = True
            Exit Function
        End If
    Else
        Set oIasig = oProcesoSeleccionado
    End If
    
    Screen.MousePointer = vbHourglass
    
    udtTeserror = oIasig.IniciarAsignacion(basOptimizacion.gvarCodUsuario)

    If udtTeserror.NumError <> TESnoerror And udtTeserror.NumError <> TESInfActualModificada Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError udtTeserror
        oIasig.CancelarAsignacion
        Set oIasig = Nothing
        BloquearProceso = False
        Exit Function
    End If
    
    Screen.MousePointer = vbNormal
    
    'A�ado a la variable de control de bloqueos el form y activo el timer del MDI
    If Not g_dicBloqueos.Exists("frmSELPROVE") Then g_dicBloqueos.Add "frmSELPROVE", "frmSELPROVE"
    g_iContadorBloqueo = 0
    MDI.Timer1.Enabled = True
    
    BloquearProceso = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "BloquearProceso", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Public Sub DesbloquearProceso(Optional ByVal iComo As Integer)
'***************************************************************************************************************
'Desbloquea el proceso
'iComo = 1 Tengo que comprobar si tiene permiso de validar, si lo tiene no lo desbloqueo
'***************************************************************************************************************
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbNormal
    If iComo = 1 And bVal Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If Not oIasig Is Nothing Then
        oIasig.CancelarAsignacion
        Set oIasig = Nothing
    End If
    
    g_iContadorBloqueo = 0
    MDI.Timer1.Enabled = False
    If g_dicBloqueos.Exists("frmSELPROVE") Then g_dicBloqueos.Remove "frmSELPROVE"
    m_udtOrigBloqueo = OrigenBloqueo.NoBloqueo
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "DesbloquearProceso", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Public Sub TimeoutBloqueo()
Dim bMensaje As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bMensaje = True
    frmMessageBox.g_bCancel = True
    Unload frmMessageBox
    Unload frmSELPROVEGrupos
    Select Case m_udtOrigBloqueo
        Case PermisoValidar
            sdbcProceCod.Text = ""
            sdbcProceCod.Columns(0).Value = ""
            sdbcProceCod.Columns(1).Value = ""
            sdbcProceCod.RemoveAll
        Case Modificar
            cmdCancelar_Click
        Case responsable
            Unload frmMensaje
            Unload frmSELPROVEResp
            DesbloquearProceso 1
        Case AnyadirProv
            'Unload frmACT_GMN
            Unload frmProgreso
            Unload frmPROVEPortalDetalle
            frmAdmProveMat.bAceptar = True
            Unload frmAdmProveMat
            Unload frmSELMAT
            Unload frmEsperaPortal
            Unload frmESPERA
            Unload frmAdmPROVEPortalResult
            Unload frmAdmPROVEPortalBuscar
            Unload frmAdmProvePortal
            Unload frmPROVEBuscar
            Unload frmSELPROVEAnya
        Case Eliminar
    
        Case Else
            bMensaje = False
    End Select

    If bMensaje Then
        If Not oIasig Is Nothing Then
            DesbloquearProceso
        End If
        g_iContadorBloqueo = 0
        If bVal Then
            sdbcProceCod.Text = ""
            sdbcProceCod.Columns(0).Value = ""
            sdbcProceCod.Columns(1).Value = ""
            sdbcProceCod.RemoveAll
        End If
    End If
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "TimeoutBloqueo", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Public Sub VerDetalleResponsable()
Dim oPer As CPersona
Dim teserror As TipoErrorSummit

    'Muestra el detalle del responsable del proceso
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oProcesoSeleccionado.responsable Is Nothing Then
        oMensajes.ProcesoSinResponsable
        Exit Sub
    End If
    If oProcesoSeleccionado.responsable.Cod = "" Then
        oMensajes.ProcesoSinResponsable
        Exit Sub
    End If
    
    Set oPer = oFSGSRaiz.Generar_CPersona
    oPer.Cod = oProcesoSeleccionado.responsable.Cod
            
    teserror = oPer.CargarTodosLosDatos
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Exit Sub
    End If
        
    Set frmESTRORGPersona.frmOrigen = Me
    frmESTRORGPersona.txtCod = oPer.Cod
    frmESTRORGPersona.txtNom = NullToStr(oPer.nombre)
    frmESTRORGPersona.txtApel = oPer.Apellidos
    frmESTRORGPersona.txtCargo = NullToStr(oPer.Cargo)
    frmESTRORGPersona.txtFax = NullToStr(oPer.Fax)
    frmESTRORGPersona.txtMail = NullToStr(oPer.mail)
    frmESTRORGPersona.txtTfno = NullToStr(oPer.Tfno)
    frmESTRORGPersona.txtTfno2 = NullToStr(oPer.Tfno2)
    frmESTRORGPersona.sdbcEquipo.Text = NullToStr(oPer.codEqp)
    If NullToStr(oPer.codEqp) <> "" Then
        frmESTRORGPersona.chkFunCompra.Value = vbChecked
    End If
    Screen.MousePointer = vbNormal
    frmESTRORGPersona.Show vbModal
    Set oPer = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "VerDetalleResponsable", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Public Sub SustituirResponsable(Optional ByVal bValida As Boolean)
    'Muestra el formulario para seleccionar el comprador responsable del proceso.
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If Not BloquearProceso(1) Then Exit Sub
    m_udtOrigBloqueo = responsable
    
    Set frmSELPROVEResp.g_oOrigen = Me
    Set frmSELPROVEResp.g_oProcesoSeleccionado = oProcesoSeleccionado
    frmSELPROVEResp.g_bValida = bValida
    frmSELPROVEResp.g_udtAccion = AccionesSummit.ACCModRespSelProve
    
    Screen.MousePointer = vbNormal
    frmSELPROVEResp.Show vbModal
    
    DesbloquearProceso (1)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "SustituirResponsable", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Public Sub ResponsableSustituido(ByVal oPer As CPersona)
Dim oAsig As CAsignacion

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If oProcesoSeleccionado.responsable Is Nothing Then
        Set oProcesoSeleccionado.responsable = oFSGSRaiz.generar_CComprador
    End If

    oProcesoSeleccionado.responsable.Cod = oPer.Cod
    oProcesoSeleccionado.responsable.codEqp = oPer.codEqp
    oProcesoSeleccionado.responsable.nombre = NullToStr(oPer.nombre)
    oProcesoSeleccionado.responsable.Apel = oPer.Apellidos
    oProcesoSeleccionado.responsable.Cargo = NullToStr(oPer.Cargo)
    oProcesoSeleccionado.responsable.Fax = NullToStr(oPer.Fax)
    oProcesoSeleccionado.responsable.mail = NullToStr(oPer.mail)
    oProcesoSeleccionado.responsable.Tfno = NullToStr(oPer.Tfno)
    oProcesoSeleccionado.responsable.Tfno2 = NullToStr(oPer.Tfno2)

    Set oPer = Nothing

    If oProcesoSeleccionado.responsable.nombre <> "" Then
        cmdResponsable.ToolTipText = oProcesoSeleccionado.responsable.nombre & " " & oProcesoSeleccionado.responsable.Apel
    Else
        cmdResponsable.ToolTipText = oProcesoSeleccionado.responsable.Apel
    End If
    
    Set oAsigs = Nothing
    If oIasig Is Nothing Then Set oIasig = oProcesoSeleccionado
    
    'Set oAsigs = oIasig.DevolverPosiblesAsignaciones(OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bAnya, bVerAsigComp, bVerAsigEqp)
    Set oAsigs = oIasig.DevolverPosiblesAsignaciones(OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bAnya, bVerAsigComp, bVerAsigEqp, oUsuarioSummit.Cod)
     
    ConfigurarYCargarGrid Tord
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "ResponsableSustituido", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub
'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
Private Sub CargarGMN1Automaticamente()
    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oIMAsig = oUsuarioSummit.comprador
    Set oGMN1s = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
    If Not oGMN1s.Count = 0 Then
        bRespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4Cod.Columns(0).Text = sdbcGMN1_4Cod.Text
        bRespetarCombo = False
        GMN1Seleccionado
    End If
    Set oGMN1s = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "CargarGMN1Automaticamente", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Comprueba si la fila actual est� seleccionada o no
''' </summary>
''' <returns></returns>
''' <param name="bkAct">Bookmark del registro que ha cambiado</param>
''' <param name="vBookmarks">Bookmarks de los registros seleccionados</param>
''' <remarks>Llamada desde  sdbgSelProve_Change; Tiempo m�ximo</remarks>
''' <revision>JVS 11/03/2011</revision>
Private Function RegistroEstaSeleccionado(ByVal bkAct As Variant, ByVal vBookmarks As Variant) As Boolean
    
    Dim bk As Variant
    Dim bkActText As String
    Dim bkText As String
    Dim encontrado As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bkActText = bkAct
    
    encontrado = False
    For Each bk In vBookmarks
        bkText = bk
        If bkText = bkActText Then
            encontrado = True
            Exit For
        End If
    Next
    RegistroEstaSeleccionado = encontrado
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "RegistroEstaSeleccionado", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function


''' <summary>
''' A�ade el proveedor a la estructura de proveedores modificados tras comprobar que el proveedor no se encuentra almacenado previamente en la estructura de Proveedores
''' </summary>
''' <returns></returns>
''' <param name="bk">Bookmark a a�adir</param>
''' <remarks>Llamada desde  sdbgSelProve_Change; Tiempo m�ximo</remarks>
''' <revision>JVS 14/03/2011</revision>
Private Sub A�adeProveedor(ByVal bk As Variant)
    
    Dim bkRecorrido As Variant
    Dim bkRecorridoStr As String
    Dim bkStr As String
    Dim yaExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    yaExiste = False
    bkStr = bk
    For Each bkRecorrido In m_aBookmarksModif
        bkRecorridoStr = bkRecorrido
        If bkRecorridoStr = bkStr Then
            yaExiste = True
        End If
    Next
    
    If Not yaExiste Then
        m_aBookmarksModif.Add bk
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "A�adeProveedor", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub CargarCodigosERP(ByVal Cod As String)
    Dim AdorERP As Ador.Recordset
    Dim oProve As CProveedores
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProve = Nothing
    Set oProve = oFSGSRaiz.generar_CProveedores
    
    Set AdorERP = oProve.DevolverCodERPProve(Cod)
    If AdorERP Is Nothing Then Exit Sub
    If AdorERP.EOF Then
        AdorERP.Close
        Set AdorERP = Nothing
        Exit Sub
    Else
        sdbgProveERP.RemoveAll
        While Not AdorERP.EOF
            sdbgProveERP.AddItem NullToStr(AdorERP.Fields("DEN").Value) & Chr(m_lSeparador) & NullToStr(AdorERP.Fields("COD_ERP").Value) & Chr(m_lSeparador) & NullToStr(AdorERP.Fields("DEN_ERP").Value)
            AdorERP.MoveNext
        Wend
        AdorERP.Close
        Set AdorERP = Nothing
        Exit Sub
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVE", "CargarCodigosERP", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

