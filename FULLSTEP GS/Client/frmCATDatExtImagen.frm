VERSION 5.00
Object = "{1E47E980-2496-11D3-ACB1-C0A64FC10000}#1.52#0"; "Pajant.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCATDatExtImagen 
   BackColor       =   &H00808000&
   Caption         =   "DArticulo"
   ClientHeight    =   5025
   ClientLeft      =   375
   ClientTop       =   3450
   ClientWidth     =   9540
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "frmCATDatExtImagen.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5025
   ScaleWidth      =   9540
   Begin MSComDlg.CommonDialog cmmdSust 
      Left            =   5880
      Top             =   4710
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSustituirIma 
      Caption         =   "&Sustituir"
      Height          =   315
      Left            =   1230
      TabIndex        =   3
      Top             =   4620
      Width           =   1095
   End
   Begin VB.CommandButton cmdEliminarIma 
      Caption         =   "&Eliminar"
      Height          =   315
      Left            =   60
      TabIndex        =   2
      Top             =   4620
      Width           =   1095
   End
   Begin VB.Frame fraImagen 
      BackColor       =   &H00808000&
      Caption         =   "Imagen"
      ForeColor       =   &H80000005&
      Height          =   4485
      Left            =   60
      TabIndex        =   0
      Top             =   30
      Width           =   9435
      Begin VB.Frame fraThumb 
         BackColor       =   &H00808000&
         Caption         =   "Frame1"
         ForeColor       =   &H80000005&
         Height          =   2205
         Left            =   6990
         TabIndex        =   4
         Top             =   150
         Width           =   2055
         Begin VB.CommandButton cmdSustituirThumb 
            Caption         =   "Sustituir"
            Height          =   315
            Left            =   1050
            TabIndex        =   7
            Top             =   1800
            Width           =   795
         End
         Begin VB.CommandButton cmdEliminarThumb 
            Caption         =   "Eliminar"
            Height          =   315
            Left            =   180
            TabIndex        =   6
            Top             =   1800
            Width           =   825
         End
         Begin PAJANTLibCtl.PajantImageCtrl PajantThumbnail 
            Height          =   1515
            Left            =   150
            OleObjectBlob   =   "frmCATDatExtImagen.frx":014A
            TabIndex        =   5
            Top             =   240
            Width           =   1775
         End
      End
      Begin PAJANTLibCtl.PajantImageCtrl PajantImagen 
         Height          =   4095
         Left            =   150
         OleObjectBlob   =   "frmCATDatExtImagen.frx":016E
         TabIndex        =   1
         Top             =   240
         Width           =   9150
      End
   End
End
Attribute VB_Name = "frmCATDatExtImagen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oImg As CImagen
Private teserror As TipoErrorSummit
Private m_sIdiSelecImagen As String
Private m_sIdiSelecImagenThumb As String
Private m_sIdiTodosArchivos As String
Private m_sIdiArchivo As String
Private m_sIdiAnyadir As String
Private m_sIdiSustituir As String
Private m_arImagenes(1 To 2) As Boolean

Private Sub cmdEliminarIma_Click()
Dim iRes As Integer

If oImg Is Nothing Then Exit Sub

On Error GoTo Error
    iRes = oMensajes.PreguntaEliminar(fraImagen.caption)
    If iRes = vbNo Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    teserror = oImg.EliminarImagen(0)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If
    m_arImagenes(1) = False
    PajantImagen.pi.DeleteFrames PJT_FRAME
    cmdEliminarIma.Enabled = False
    cmdSustituirIma.caption = m_sIdiAnyadir
    Screen.MousePointer = vbNormal
Exit Sub

Error:
    MsgBox err.Description, vbCritical + vbOKOnly, "FULLSTEP"
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdEliminarThumb_Click()
Dim iRes As Integer

If oImg Is Nothing Then Exit Sub

On Error GoTo Error:
    iRes = oMensajes.PreguntaEliminar(fraThumb.caption)
    If iRes = vbNo Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    teserror = oImg.EliminarImagen(1)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If
    
    m_arImagenes(2) = False
    PajantThumbnail.pi.DeleteFrames PJT_FRAME
    cmdEliminarThumb.Enabled = False
    cmdSustituirThumb.caption = m_sIdiAnyadir
    Screen.MousePointer = vbNormal
Exit Sub

Error:
    MsgBox err.Description, vbCritical + vbOKOnly, "FULLSTEP"
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdSustituirIma_Click()
Dim i As Integer
Dim sFileName As String
Dim sFileTitle As String
Dim irespuesta As Integer
Dim imgProve As Variant
Dim sExtension As String
Dim bGIF As Boolean
Dim oFos As FileSystemObject
Dim sFileNameSinEx As String
Dim s As Variant
Dim arDatosArt() As String
Dim arDatosFmt(1) As String
Dim imgOrig As Variant

On Error GoTo Cancelar:

        If oImg Is Nothing Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        cmmdSust.DialogTitle = m_sIdiSelecImagen
        cmmdSust.Filter = m_sIdiTodosArchivos & "|*.*"
    
        cmmdSust.filename = ""
        cmmdSust.ShowOpen
    
        sFileName = cmmdSust.filename
        sFileTitle = cmmdSust.FileTitle
        If sFileName = "" Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        s = DevolverNombreExtension(sFileTitle)
        sFileNameSinEx = s(1)
        sExtension = s(2)
        'sExtension = Right(sFileTitle, Len(sFileTitle) - InStr(1, sFileTitle, "."))
        bGIF = False
        If UCase(sExtension) = "GIF" Then
            If FSGSLibrary.ConvertirGIFaJPEG(sFileName, sExtension) Then
                sFileName = Left(sFileName, Len(sFileName) - Len(sFileTitle)) & sFileNameSinEx & ".jpeg"
                sFileTitle = sFileNameSinEx & ".jpeg"
                sExtension = "JPEG"
                bGIF = True
            Else
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleCargarGIF
                Exit Sub
            End If
        End If
        If UCase(sExtension) <> "JPG" And UCase(sExtension) <> "JPEG" And UCase(sExtension) <> "GIF" And _
           UCase(sExtension) <> "BMP" And UCase(sExtension) <> "TIFF" And UCase(sExtension) <> "PNG" And UCase(sExtension) <> "MNG" Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido m_sIdiArchivo
            Exit Sub
        End If
        If PajantImagen.pi.TotalFrames = 0 Then
            PajantImagen.pi.AddFrames 1
        Else
            PajantImagen.pi.ExportTo PJT_MEMJPEG, 0, 0, imgOrig, PJT_FRAME
        End If
        PajantImagen.pi.Load sFileName, 0

        'Guarda la imagen en imgProve para a�adirla despu�s a la base de datos
        PajantImagen.pi.ExportTo PJT_MEMPNG, 0, 0, imgProve, PJT_IMAGE Or PJT_TONEWFRAME
        DoEvents
        oImg.DataImagen = imgProve
        
        teserror = oImg.ModificarImagen(0)
        If teserror.NumError <> TESnoerror Or IsEmpty(oImg.DataImagen) Then
            Screen.MousePointer = vbNormal
            PajantImagen.pi.DeleteFrames PJT_FRAME
            If Not IsEmpty(imgOrig) Then
                PajantImagen.pi.ImportFrom PJT_MEMJPEG, 0, 0, imgOrig, PJT_TONEWIMAGE
            End If
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                arDatosFmt(1) = sFileName
                oMensajes.ImposibleCargarImagenes arDatosArt, arDatosFmt
            End If
            Exit Sub
        End If
        m_arImagenes(1) = True
        cmdEliminarIma.Enabled = True
        cmdSustituirIma.caption = m_sIdiSustituir
'            basSeguridad.RegistrarAccion Accionessummit.ACCProceEspAnya, "Anyo:" & sdbcAnyo.Value & "GMN1:" & sdbcGMN1_4Cod.Value & "Cod:" & sdbcProceCod.Value & "Archivo:" & sFileTitle
        Screen.MousePointer = vbNormal
        If bGIF Then
            Set oFos = New FileSystemObject
            oFos.DeleteFile sFileName
            Set oFos = Nothing
        End If

Exit Sub

Cancelar:
    MsgBox err.Description, vbCritical + vbOKOnly, "FULLSTEP"
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdSustituirThumb_Click()
Dim i As Integer
Dim sFileName As String
Dim sFileTitle As String
Dim irespuesta As Integer
Dim imgProve As Variant
Dim sExtension As String
Dim bGIF As Boolean
Dim oFos As FileSystemObject
Dim s As Variant
Dim sFileNameSinEx As String
Dim arDatosArt() As String
Dim arDatosFmt(1) As String
Dim imgOrig As Variant

On Error GoTo Cancelar:

        If oImg Is Nothing Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        cmmdSust.DialogTitle = m_sIdiSelecImagenThumb
        cmmdSust.Filter = m_sIdiTodosArchivos & "|*.*"
    
        cmmdSust.filename = ""
        cmmdSust.ShowOpen
    
        sFileName = cmmdSust.filename
        sFileTitle = cmmdSust.FileTitle
        If sFileName = "" Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        s = DevolverNombreExtension(sFileTitle)
        sFileNameSinEx = s(1)
        sExtension = s(2)
        'sExtension = Right(sFileTitle, Len(sFileTitle) - InStr(1, sFileTitle, "."))
        bGIF = False
        If UCase(sExtension) = "GIF" Then
            If FSGSLibrary.ConvertirGIFaJPEG(sFileName, sExtension) Then
                sFileName = Left(sFileName, Len(sFileName) - Len(sFileTitle)) & sFileNameSinEx & ".jpeg"
                sFileTitle = sFileNameSinEx & ".jpeg"
                sExtension = "JPEG"
                bGIF = True
            Else
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleCargarGIF
                Exit Sub
            End If
        End If
        
        If UCase(sExtension) <> "JPG" And UCase(sExtension) <> "JPEG" And UCase(sExtension) <> "GIF" And _
           UCase(sExtension) <> "BMP" And UCase(sExtension) <> "TIFF" And UCase(sExtension) <> "PNG" And UCase(sExtension) <> "MNG" Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido m_sIdiArchivo
            Exit Sub
        End If
        
        If PajantThumbnail.pi.TotalFrames = 0 Then
            PajantThumbnail.pi.AddFrames 1
        Else
            PajantThumbnail.pi.ExportTo PJT_MEMJPEG, 0, 0, imgOrig, PJT_FRAME
        End If
        PajantThumbnail.pi.Load sFileName, 0

        PajantThumbnail.pi.ResizeImage 100, 100, PJT_INTERPOLATE

        'Guarda la imagen en imgProve para a�adirla despu�s a la base de datos
        PajantThumbnail.pi.ExportTo PJT_MEMJPEG, 0, 0, imgProve, PJT_FRAME
        DoEvents
        oImg.DataImagen = imgProve
        
        teserror = oImg.ModificarImagen(1)
        If teserror.NumError <> TESnoerror Or IsEmpty(oImg.DataImagen) Then
            Screen.MousePointer = vbNormal
            PajantThumbnail.pi.DeleteFrames PJT_FRAME
            If Not IsEmpty(imgOrig) Then
                PajantThumbnail.pi.ImportFrom PJT_MEMJPEG, 0, 0, imgOrig, PJT_TONEWIMAGE
            End If
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                arDatosFmt(1) = sFileName
                oMensajes.ImposibleCargarImagenes arDatosArt, arDatosFmt
            End If
            Exit Sub
        End If
        m_arImagenes(2) = True
        cmdEliminarThumb.Enabled = True
        cmdSustituirThumb.caption = m_sIdiSustituir
'            basSeguridad.RegistrarAccion Accionessummit.ACCProceEspAnya, "Anyo:" & sdbcAnyo.Value & "GMN1:" & sdbcGMN1_4Cod.Value & "Cod:" & sdbcProceCod.Value & "Archivo:" & sFileTitle
        Screen.MousePointer = vbNormal
        If bGIF Then
            Set oFos = New FileSystemObject
            oFos.DeleteFile sFileName
            Set oFos = Nothing
        End If

Exit Sub

Cancelar:
    MsgBox err.Description, vbCritical + vbOKOnly, "FULLSTEP"
    Screen.MousePointer = vbNormal


End Sub

Private Sub Form_Load()
Dim Fl As Long
Dim imgBD As Variant
Dim thmBD As Variant
Dim bErrorLectura As Boolean

On Error GoTo Error
 
    Me.Width = 9660
    Me.Height = 5430
    
    CargarRecursos
    
    Screen.MousePointer = vbHourglass
    'Comienza la lectura de la imagen
    bErrorLectura = False
    teserror = oImg.ComenzarLecturaData
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        If teserror.NumError <> TESDatoEliminado Then
            basErrores.TratarError teserror
        End If
        bErrorLectura = True
       ' Exit Sub
    End If
    If Not bErrorLectura Then
        Fl = oImg.dataSize
        imgBD = oImg.ReadData(Fl)
        
        oImg.FinalizarLecturaData
    Else
        imgBD = Null
    End If
   
    bErrorLectura = False
    teserror = oImg.ComenzarLecturaThumbnail
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        If teserror.NumError <> TESDatoEliminado Then
            basErrores.TratarError teserror
        End If
        bErrorLectura = True
        'Exit Sub
    End If
    
    If Not bErrorLectura Then
        Fl = oImg.dataSize
        thmBD = oImg.ReadData(Fl)
        
        oImg.FinalizarLecturaData
    Else
        thmBD = Null
    End If
    
    If IsNull(thmBD) And IsNull(imgBD) Then
        frmCatalogoDatosExt.bNoHayImagen = True
    End If
    
    'Muestra la imagen
    PajantImagen.pi.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"
    PajantThumbnail.pi.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"

    If IsNull(imgBD) Then
        m_arImagenes(1) = False
        PajantImagen.pi.DisplayMode = PJT_NONE
        cmdSustituirIma.caption = m_sIdiAnyadir
        cmdEliminarIma.Enabled = False
    Else
        m_arImagenes(1) = True
        PajantImagen.pi.DisplayMode = PJT_SCROLL
        cmdSustituirIma.caption = m_sIdiSustituir
        cmdEliminarIma.Enabled = True
    End If

    If IsNull(thmBD) Then
        m_arImagenes(2) = False
        PajantThumbnail.pi.DisplayMode = PJT_NONE
        cmdSustituirThumb.caption = m_sIdiAnyadir
        cmdEliminarThumb.Enabled = False
    Else
        m_arImagenes(2) = True
        PajantThumbnail.pi.DisplayMode = PJT_SCROLL
        cmdSustituirThumb.caption = m_sIdiSustituir
        cmdEliminarThumb.Enabled = True
        PajantThumbnail.pi.SetBackground &H808000, 0
         
        PajantThumbnail.pi.ImportFrom PJT_MEMJPEG, 0, 0, thmBD, PJT_TONEWIMAGE
        
    End If
    
    'Fondo verde de la imagen
    PajantImagen.pi.SetBackground &H808000, 0
     
    PajantImagen.pi.ImportFrom PJT_MEMPNG, 0, 0, imgBD, PJT_TONEWIMAGE
    
    'Me.ScaleMode = vbPixels
    Screen.MousePointer = vbNormal
    Exit Sub
    
Error:
    
    MsgBox err.Description, vbCritical + vbOKOnly, "FULLSTEP"
    Screen.MousePointer = vbNormal
    Unload Me
End Sub

Private Sub Form_Resize()
    If Me.Width < 2000 Then Exit Sub
    If Me.Height < 600 Then Exit Sub

    On Error Resume Next
    'Redimensiona el formulario
    fraImagen.Height = Me.Height - 945
    fraImagen.Width = Me.Width - 225
    Me.PajantImagen.Height = Me.Height - 1335
    Me.PajantImagen.Width = Me.Width - 510
    cmdEliminarIma.Top = fraImagen.Height + fraImagen.Top + 105
    cmdSustituirIma.Top = fraImagen.Height + fraImagen.Top + 105
    fraThumb.Left = PajantImagen.Left + PajantImagen.Width - fraThumb.Width - 245
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_Imagenes1, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Me.caption = Ador(0).Value
        fraImagen.caption = Ador(0).Value
        For i = 1 To 9
            Ador.MoveNext
        Next
        fraThumb.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminarIma.caption = "&" & Ador(0).Value
        cmdEliminarThumb.caption = Ador(0).Value
        Ador.MoveNext
        cmdSustituirIma.caption = "&" & Ador(0).Value
        cmdSustituirThumb.caption = Ador(0).Value
        m_sIdiSustituir = Ador(0).Value
        Ador.MoveNext
        m_sIdiSelecImagen = Ador(0).Value
        Ador.MoveNext
        m_sIdiSelecImagenThumb = Ador(0).Value
        Ador.MoveNext
        m_sIdiTodosArchivos = Ador(0).Value
        Ador.MoveNext
        m_sIdiAnyadir = Ador(0).Value
        Ador.MoveNext
        m_sIdiArchivo = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)

If m_arImagenes(1) = True Or m_arImagenes(2) = True Then
    frmCatalogoDatosExt.sdbgArt.Columns("CONIMAGEN").Value = "1"
    frmCatalogoDatosExt.sdbgArt.Columns("Imagen").CellStyleSet "Imagen"
Else
    frmCatalogoDatosExt.sdbgArt.Columns("CONIMAGEN").Value = "0"
    frmCatalogoDatosExt.sdbgArt.Columns("Imagen").CellStyleSet ""
End If
frmCatalogoDatosExt.sdbgArt.Update

End Sub
