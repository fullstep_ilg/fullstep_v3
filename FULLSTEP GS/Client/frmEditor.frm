VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form frmEditor 
   Caption         =   "Form1"
   ClientHeight    =   8115
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13500
   Icon            =   "frmEditor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8115
   ScaleWidth      =   13500
   Begin SHDocVwCtl.WebBrowser webVisorEditor 
      Height          =   7785
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   13515
      ExtentX         =   23839
      ExtentY         =   13732
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
End
Attribute VB_Name = "frmEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_sRuta As String
Public g_bReadOnly As Boolean
Public g_iAnio_ProceCompra As Integer
Public g_sGMN1_ProceCompra As String
Public g_iCod_ProceCompra As Long

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
''' <summary>
''' Evento que salta cuando se activa el formulario y en el que pasaremos al visor la direcci�n a mostrar
''' </summary>
''' <remarks>Tiempo maximo 0 sec</remarks>
Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    Screen.MousePointer = vbHourglass
    If g_sOrigen = "Colaboracion" Then
        Dim parametrosProceCompra As String
        parametrosProceCompra = "&proceanio=" & g_iAnio_ProceCompra & "&procemat=" & g_sGMN1_ProceCompra & "&procecod=" & g_iCod_ProceCompra
        webVisorEditor.Navigate2 g_sRuta & parametrosProceCompra, 4 'que no coja de la cache
        Me.Width = 18000
        Me.Height = 12000
    Else
        webVisorEditor.Navigate2 g_sRuta, 4 'que no coja de la cache
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEditor", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, True
m_bActivado = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEditor", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Evento que Redimensiona el formulario
''' </summary>
''' <remarks>Tiempo maximo 0 sec</remarks>
Private Sub Form_Resize()
    'Redimensiona el formulario
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Width < 600 Then Exit Sub
    If Me.Height < 5000 Then Exit Sub
    
    'redimensiona el width del visor
    webVisorEditor.Width = Me.Width - 200
    webVisorEditor.Height = Me.Height - 600
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEditor", "Form_Resize", err, Erl)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Evento que se lanza cuando el control se dispone a navegar a una URL, en el aprovecharemos para mostrar un formulario de espera
''' </summary>
''' <remarks>Llamada desde el propio control webVisor</remarks>
Private Sub webVisorEditor_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, FLAGS As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEditor", "webVisorEditor_BeforeNavigate2", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que se lanza cuando el estado de la carga de la pagn se dispone a navegar a una URL, en el aprovecharemos para mostrar un formulario de espera
''' </summary>
''' <remarks>Llamada desde el propio control webVisor</remarks>
Private Sub webVisorEditor_ProgressChange(ByVal Progress As Long, ByVal ProgressMax As Long)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Progress = -1 Then
        Screen.MousePointer = vbNormal
    Else
        webVisorEditor.Visible = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEditor", "webVisorEditor_ProgressChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Guarda los cambios realizados en el editor.
''' <param name="Cancel"> si de ha pulsado ESC</param>
''' </summary>
''' <remarks>Cierre formulario</remarks>
Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
    If Not g_sOrigen = "Colaboracion" Then
        If Not g_bReadOnly Then
            If g_sOrigen = "frmFormularios" Then
                frmFormularios.sdbgCampos.Columns("VALOR").Value = Trim(webVisorEditor.Document.Forms(0)(2).innerText)
                frmFormularios.g_oGrupoSeleccionado.Campos.Item(CStr(frmFormularios.sdbgCampos.Columns("ID").Value)).valorText = Trim(webVisorEditor.Document.Forms(0)(2).innerText)
                frmFormularios.sdbgCampos.Update
            ElseIf g_sOrigen = "frmSolicitudDetalle" Then
                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = Trim(webVisorEditor.Document.Forms(0)(2).innerText)
                frmSolicitudes.g_ofrmDetalleSolic.g_oGrupoSeleccionado.Campos.Item(CStr(frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("ID").Value)).valorText = Trim(webVisorEditor.Document.Forms(0)(2).innerText)
                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Update
            ElseIf g_sOrigen = "frmSolicitudDesglose" Then
                frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = Trim(webVisorEditor.Document.Forms(0)(2).innerText)
                frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.Update
            ElseIf g_sOrigen = "frmSolicitudDesgloseP" Then
                frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = Trim(webVisorEditor.Document.Forms(0)(2).innerText)
                frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.Update
            ElseIf g_sOrigen = "frmDesgloseValores" Then
                frmDesgloseValores.ssLineas.ActiveCell.Value = Trim(webVisorEditor.Document.Forms(0)(2).innerText)
                frmDesgloseValores.ssLineas.Update
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEditor", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub webVisorEditor_WindowClosing(ByVal IsChildWindow As Boolean, Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEditor", "webVisorEditor_WindowClosing", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
