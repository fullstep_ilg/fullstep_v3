VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCALPuntuacionesPortal 
   Caption         =   "frmCALPuntuacionesPortal"
   ClientHeight    =   6990
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9885
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCALPuntuacionesPortal.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6990
   ScaleWidth      =   9885
   Begin VB.PictureBox PicPymes 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      ScaleHeight     =   255
      ScaleWidth      =   4215
      TabIndex        =   2
      Top             =   120
      Width           =   4215
      Begin SSDataWidgets_B.SSDBCombo sdbcPYME 
         Height          =   285
         Left            =   720
         TabIndex        =   0
         Top             =   0
         Width           =   3435
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1773
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   4260
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6068
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblPYME 
         AutoSize        =   -1  'True
         Caption         =   "PYME:"
         Height          =   195
         Left            =   50
         TabIndex        =   3
         Top             =   0
         Width           =   450
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgVariables 
      Height          =   6465
      Left            =   0
      TabIndex        =   1
      Top             =   480
      Width           =   9855
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   8
      stylesets.count =   6
      stylesets(0).Name=   "Nivel1"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8421504
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCALPuntuacionesPortal.frx":014A
      stylesets(0).AlignmentText=   0
      stylesets(1).Name=   "Nivel2"
      stylesets(1).BackColor=   13619151
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCALPuntuacionesPortal.frx":0166
      stylesets(1).AlignmentText=   0
      stylesets(2).Name=   "Nivel3"
      stylesets(2).BackColor=   13417139
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmCALPuntuacionesPortal.frx":0182
      stylesets(2).AlignmentText=   0
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmCALPuntuacionesPortal.frx":019E
      stylesets(4).Name=   "Nivel4"
      stylesets(4).BackColor=   12566463
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmCALPuntuacionesPortal.frx":01BA
      stylesets(4).AlignmentText=   0
      stylesets(5).Name=   "Nivel5"
      stylesets(5).BackColor=   11513775
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmCALPuntuacionesPortal.frx":01D6
      stylesets(5).AlignmentText=   0
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "NIVEL"
      Columns(1).Name =   "NIVEL"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   5
      Columns(2).Width=   10451
      Columns(2).Caption=   "DEN"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   150
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   2540
      Columns(3).Caption=   "PUB"
      Columns(3).Name =   "PUB"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "IDNIVEL1"
      Columns(4).Name =   "IDNIVEL1"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "IDNIVEL2"
      Columns(5).Name =   "IDNIVEL2"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "IDNIVEL3"
      Columns(6).Name =   "IDNIVEL3"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "IDNIVEL4"
      Columns(7).Name =   "IDNIVEL4"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   17383
      _ExtentY        =   11404
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCALPuntuacionesPortal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bRespetarChange As Boolean

Private Sub Form_Load()
    Me.Width = 10000
    Me.Height = 7500
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
   
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If gParametrosGenerales.gbPymes Then
        PicPymes.Visible = True
    Else
        PicPymes.Visible = False
        CargarVariablesCalidad
    End If
End Sub

Private Sub Form_Resize()
    If Me.Width < 500 Then Exit Sub
    If Me.Height < 500 Then Exit Sub
    
    If gParametrosGenerales.gbPymes Then
        sdbgVariables.Height = Me.Height - 960
        sdbgVariables.Top = 450
    Else
        sdbgVariables.Top = 0
        sdbgVariables.Height = Me.Height - 560
        
    End If
    sdbgVariables.Width = Me.Width - 160
    
    sdbgVariables.Columns("PUB").Width = 1450
    sdbgVariables.Columns("DEN").Width = sdbgVariables.Width - 2050
End Sub

Private Sub sdbgVariables_Change()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Evento que salta al cambiar el valor del chech "Publicar" de una variable de calidad.
'                   Almacena si una variable de calidad Se va a Publicar o no en el portal
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,125seg
'************************************************************
Dim vb As Variant
Dim teserror As TipoErrorSummit
Dim oCalidad As CVariableCalidad
Dim lIdPyme As Long

    If m_bRespetarChange = True Then Exit Sub
    If sdbgVariables.Columns(sdbgVariables.Col).Name <> "PUB" Then Exit Sub
    
    m_bRespetarChange = True

    Set oCalidad = oFSGSRaiz.Generar_CVariableCalidad
    oCalidad.Id = sdbgVariables.Columns("ID").Value
    oCalidad.Nivel = sdbgVariables.Columns("NIVEL").Value
    
    lIdPyme = 0
    If gParametrosGenerales.gbPymes Then
        lIdPyme = Me.sdbcPYME.Columns("ID").Value
    End If
        
    teserror = oCalidad.ModificarPublicacionPortal(GridCheckToBoolean(sdbgVariables.Columns("PUB").Value), lIdPyme)
    
    If teserror.NumError <> TESnoerror Then
        vb = sdbgVariables.ActiveCell.Value
        sdbgVariables.ActiveCell.Value = IIf(vb = "0", "1", "0")
    End If
    sdbgVariables.Update

    Set oCalidad = Nothing
    m_bRespetarChange = False
End Sub

Private Sub sdbgVariables_RowLoaded(ByVal Bookmark As Variant)
    Select Case sdbgVariables.Columns("NIVEL").Value
        Case 1
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel1"
        Case 2
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel2"
        Case 3
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel3"
        Case 4
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel4"
        Case 5
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel5"
    End Select
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAL_PUB_PUNTUACIONES_PORTAL, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value
        ador.MoveNext
        Me.sdbgVariables.Columns("DEN").caption = ador(0).Value
        ador.MoveNext
        Me.sdbgVariables.Columns("PUB").caption = ador(0).Value
        ador.MoveNext
        lblPYME.caption = ador(0).Value & ":" 'PYME
        
        ador.Close
    End If
End Sub

Private Sub CargarVariablesCalidad(Optional ByVal lIdPyme As Long = 0)
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Carga las variable de calidad que se van a publicar en el portal en la coleccion A partir del nivel 1.

'*** Par�metros de entrada: idPyme = Id de la PYME si se esta trabajando en modo PYME
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,34seg
'************************************************************
    Dim oVarsCalidad As CVariablesCalidad
    Dim oVarCal1 As CVariableCalidad
    Dim oVarCal2 As CVariableCalidad
    Dim oVarCal3 As CVariableCalidad
    Dim oVarCal4 As CVariableCalidad
    Dim oVarCal5 As CVariableCalidad
    
    sdbgVariables.RemoveAll
    
    Set oVarsCalidad = oFSGSRaiz.Generar_CVariablesCalidad
    oVarsCalidad.CargarVariablesPubPortal lIdPyme
    
    For Each oVarCal1 In oVarsCalidad
        sdbgVariables.AddItem oVarCal1.Id & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal1.PublicarPortal) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        If Not oVarCal1.VariblesCal Is Nothing Then
            For Each oVarCal2 In oVarCal1.VariblesCal
                sdbgVariables.AddItem oVarCal2.Id & Chr(m_lSeparador) & "2" & Chr(m_lSeparador) & "    " & oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal2.PublicarPortal) & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                If Not oVarCal2.VariblesCal Is Nothing Then
                    For Each oVarCal3 In oVarCal2.VariblesCal
                        sdbgVariables.AddItem oVarCal3.Id & Chr(m_lSeparador) & "3" & Chr(m_lSeparador) & "          " & oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal3.PublicarPortal) & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                        If Not oVarCal3.VariblesCal Is Nothing Then
                            For Each oVarCal4 In oVarCal3.VariblesCal
                                sdbgVariables.AddItem oVarCal4.Id & Chr(m_lSeparador) & "4" & Chr(m_lSeparador) & "               " & oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal4.PublicarPortal) & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & oVarCal3.Id & Chr(m_lSeparador) & ""
                                If Not oVarCal4.VariblesCal Is Nothing Then
                                    For Each oVarCal5 In oVarCal4.VariblesCal
                                        sdbgVariables.AddItem oVarCal5.Id & Chr(m_lSeparador) & "5" & Chr(m_lSeparador) & "                   " & oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal5.PublicarPortal) & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & oVarCal3.Id & Chr(m_lSeparador) & oVarCal4.Id
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Next
        End If
    Next
    
    Set oVarsCalidad = Nothing
End Sub


Private Sub sdbcPYME_CloseUp()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Evento que salta al seleccionar una PYME de la combo.
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,8seg
'************************************************************
    If sdbcPYME.Value = "" Then Exit Sub
    CargarVariablesCalidad sdbcPYME.Columns("ID").Value
End Sub

Private Sub sdbcPYME_DropDown()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Carga la combo con todas las pymes que hay en la aplicacion
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,1seg
'************************************************************
    Dim oPymes As cPymes
    Dim oPyme As cPyme

    Screen.MousePointer = vbHourglass
    
    sdbcPYME.RemoveAll
    
    Set oPymes = oFSGSRaiz.Generar_CPymes
    oPymes.CargarTodasLasPymes , True
    
    For Each oPyme In oPymes
        sdbcPYME.AddItem oPyme.Id & Chr(m_lSeparador) & oPyme.Cod & Chr(m_lSeparador) & oPyme.Den
    Next
    
    Set oPymes = Nothing
    Set oPyme = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPYME_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcPYME.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPYME.Rows - 1
            bm = sdbcPYME.GetBookmark(i)
            If UCase(Text) = UCase(sdbcPYME.Columns("DEN").CellText(bm)) Then
                sdbcPYME.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcPYME_InitColumnProps()
    sdbcPYME.DataFieldList = "Column 0"
    sdbcPYME.DataFieldToDisplay = "Column 2"
End Sub
