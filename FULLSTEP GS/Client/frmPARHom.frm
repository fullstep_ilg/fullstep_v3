VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPARHom 
   Caption         =   "Definici�n de calificaciones de proveedores"
   ClientHeight    =   4320
   ClientLeft      =   2100
   ClientTop       =   2445
   ClientWidth     =   6495
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPARHom.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4320
   ScaleWidth      =   6495
   Begin TabDlg.SSTab tabHom 
      Height          =   4200
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   7408
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Definici�n"
      TabPicture(0)   =   "frmPARHom.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "picEdit"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdModifGen"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdListado1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Valores"
      TabPicture(1)   =   "frmPARHom.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label4"
      Tab(1).Control(1)=   "lblIidMat2"
      Tab(1).Control(2)=   "Line2"
      Tab(1).Control(3)=   "sdbcIdi2"
      Tab(1).Control(4)=   "sdbgCalif"
      Tab(1).Control(5)=   "sdbcCalif"
      Tab(1).Control(6)=   "picNavigate"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).ControlCount=   7
      Begin VB.CommandButton cmdListado1 
         Caption         =   "&Listado"
         Height          =   345
         Left            =   165
         TabIndex        =   11
         Top             =   3720
         Width           =   1005
      End
      Begin VB.Frame Frame1 
         Caption         =   "Calificaciones"
         Height          =   3015
         Left            =   180
         TabIndex        =   25
         Top             =   420
         Width           =   6015
         Begin VB.PictureBox picEdicion 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1650
            Left            =   120
            ScaleHeight     =   1650
            ScaleWidth      =   5760
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   1140
            Width           =   5760
            Begin VB.CheckBox chkDesc3 
               Caption         =   "Valor descendente"
               Height          =   285
               Left            =   3720
               TabIndex        =   7
               Top             =   1200
               Width           =   1950
            End
            Begin VB.CheckBox chkDesc2 
               Caption         =   "Valor descendente"
               Height          =   285
               Left            =   3720
               TabIndex        =   5
               Top             =   780
               Width           =   1980
            End
            Begin VB.CheckBox chkDesc1 
               Caption         =   "Valor descendente"
               Height          =   285
               Left            =   3720
               TabIndex        =   3
               Top             =   360
               Width           =   1980
            End
            Begin VB.TextBox txtDenCalif1 
               Height          =   285
               Left            =   60
               MaxLength       =   50
               TabIndex        =   2
               Top             =   360
               Width           =   3450
            End
            Begin VB.TextBox txtDenCalif2 
               Height          =   285
               Left            =   60
               MaxLength       =   50
               TabIndex        =   4
               Top             =   780
               Width           =   3450
            End
            Begin VB.TextBox txtDenCalif3 
               Height          =   285
               Left            =   60
               MaxLength       =   50
               TabIndex        =   6
               Top             =   1200
               Width           =   3450
            End
            Begin VB.CheckBox chkProdHom 
               Caption         =   "Por defecto, considerar los productos como homologados en las ofertas"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   420
               TabIndex        =   27
               TabStop         =   0   'False
               Top             =   3000
               Visible         =   0   'False
               Width           =   5385
            End
            Begin VB.Label Label5 
               Caption         =   "Denominaciones:"
               Height          =   255
               Left            =   60
               TabIndex        =   28
               Top             =   60
               Width           =   2115
            End
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcIdi 
            Height          =   285
            Left            =   1440
            TabIndex        =   1
            Top             =   420
            Width           =   2895
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   6773
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "ID"
            Columns(1).Name =   "ID"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "OFFSET"
            Columns(2).Name =   "OFFSET"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   5106
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Line Line1 
            X1              =   240
            X2              =   5640
            Y1              =   900
            Y2              =   900
         End
         Begin VB.Label lblIdiMat 
            Caption         =   "DIdioma"
            Height          =   195
            Left            =   240
            TabIndex        =   29
            Top             =   480
            Width           =   1155
         End
      End
      Begin VB.PictureBox picNavigate 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   585
         Left            =   -74805
         ScaleHeight     =   585
         ScaleWidth      =   5940
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   3510
         Width           =   5940
         Begin VB.CommandButton cmdModoEdicion 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Edici�n"
            Height          =   345
            Left            =   4845
            TabIndex        =   15
            Top             =   180
            Width           =   1005
         End
         Begin VB.CommandButton cmdCodigo 
            BackColor       =   &H00C9D2D6&
            Caption         =   "C�&digo"
            Height          =   345
            Left            =   3480
            TabIndex        =   21
            Top             =   180
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Restaurar"
            Height          =   345
            Left            =   60
            TabIndex        =   16
            Top             =   180
            Width           =   1005
         End
         Begin VB.CommandButton cmdListado 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Listado"
            Height          =   345
            Left            =   1200
            TabIndex        =   17
            Top             =   180
            Width           =   1005
         End
         Begin VB.CommandButton cmdA�adir 
            Caption         =   "&A�adir"
            Height          =   345
            Left            =   60
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   180
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliminar 
            Caption         =   "&Eliminar"
            Height          =   345
            Left            =   1200
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   180
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdDeshacer 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Deshacer"
            Enabled         =   0   'False
            Height          =   345
            Left            =   2340
            TabIndex        =   20
            Top             =   180
            Visible         =   0   'False
            Width           =   1005
         End
      End
      Begin VB.CommandButton cmdModifGen 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   5160
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   3720
         Width           =   1005
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCalif 
         Height          =   285
         Left            =   -73395
         TabIndex        =   12
         Top             =   525
         Width           =   3105
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   5477
         Columns(0).Caption=   "CAL"
         Columns(0).Name =   "CAL"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   5477
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCalif 
         Height          =   1845
         Left            =   -74745
         TabIndex        =   14
         Top             =   1605
         Width           =   5850
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   1
         stylesets.count =   2
         stylesets(0).Name=   "ActiveRowDesktop"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   8421376
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPARHom.frx":0CEA
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmPARHom.frx":0D06
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   1138
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777152
         Columns(1).Width=   4366
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "CAL"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   100
         Columns(2).Width=   1826
         Columns(2).Caption=   "Valor inferior"
         Columns(2).Name =   "INF"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16777215
         Columns(3).Width=   1931
         Columns(3).Caption=   "Valor superior"
         Columns(3).Name =   "SUP"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16777215
         _ExtentX        =   10319
         _ExtentY        =   3254
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox picEdit 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   1320
         ScaleHeight     =   420
         ScaleWidth      =   3510
         TabIndex        =   23
         Top             =   3660
         Visible         =   0   'False
         Width           =   3510
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Height          =   315
            Left            =   780
            TabIndex        =   8
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "Cancelar"
            Height          =   315
            Left            =   1965
            TabIndex        =   9
            Top             =   60
            Width           =   1005
         End
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcIdi2 
         Height          =   285
         Left            =   -73395
         TabIndex        =   13
         Top             =   920
         Width           =   3105
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   6773
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "ID"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "OFFSET"
         Columns(2).Name =   "OFFSET"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   5477
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   16777215
      End
      Begin VB.Line Line2 
         X1              =   -74745
         X2              =   -68930
         Y1              =   1380
         Y2              =   1380
      End
      Begin VB.Label lblIidMat2 
         Caption         =   "DIdioma"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   -74745
         TabIndex        =   30
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Calificaci�n:"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   -74745
         TabIndex        =   22
         Top             =   585
         Width           =   1290
      End
   End
End
Attribute VB_Name = "frmPARHom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmParHom
''' *** Creacion: 2/01/1998 (Javier Arana)
''' *** Ultima revision:
Option Explicit

Public oCalificaciones As CCalificaciones

Private oIdiomas As CIdiomas
Private sIdioma()  As String
Private oCalificacionEnEdicion As CCalificacion
Private oIBAseDatosEnEdicion As IBaseDatos
''' Control de errores
Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean
''' Variables de control
Public Accion As accionessummit
Private bModoEdicion As Boolean
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
''' Posicion para UnboundReadData
Private p As Long
''' Listado de Calificaciones
Public bOrdenListadoDen As Boolean
Private oCalificaciones1 As CCalificaciones
Private oCalificaciones2 As CCalificaciones
Private oCalificaciones3 As CCalificaciones
Private oLiterales As CLiterales
Private oLiteralesAModificar As CLiterales

Private Sub PrepararParametros(pParametrosGenerales As ParametrosGenerales)

    pParametrosGenerales = gParametrosGenerales
   
    If chkDesc1.Value = vbChecked Then
        pParametrosGenerales.gbSENT_ORD_CAL1 = True
        gParametrosGenerales.gbSENT_ORD_CAL1 = True
    Else
        pParametrosGenerales.gbSENT_ORD_CAL1 = False
        gParametrosGenerales.gbSENT_ORD_CAL1 = False
    End If
    
    If chkDesc2.Value = vbChecked Then
        pParametrosGenerales.gbSENT_ORD_CAL2 = True
        gParametrosGenerales.gbSENT_ORD_CAL2 = True
    Else
        pParametrosGenerales.gbSENT_ORD_CAL2 = False
        gParametrosGenerales.gbSENT_ORD_CAL2 = False
    End If
    
    If chkDesc3.Value = vbChecked Then
        pParametrosGenerales.gbSENT_ORD_CAL3 = True
        gParametrosGenerales.gbSENT_ORD_CAL3 = True
    Else
        pParametrosGenerales.gbSENT_ORD_CAL3 = False
        gParametrosGenerales.gbSENT_ORD_CAL3 = False
    End If
End Sub
Private Function VerificarParametros() As Boolean

    VerificarParametros = False
    
    If txtDenCalif1 = "" Then
        MsgBox sIdioma(1) & Chr(13) & sIdioma(2), vbExclamation, "FullStep"
        Exit Function
    End If
    
    If txtDenCalif2 = "" Then
        MsgBox sIdioma(1) & Chr(13) & sIdioma(3), vbExclamation, "FullStep"
        Exit Function
    End If
    
    If txtDenCalif3 = "" Then
        MsgBox sIdioma(1) & Chr(13) & sIdioma(4), vbExclamation, "FullStep"
        Exit Function
    End If
    
    VerificarParametros = True
End Function

Private Sub cmdAceptar_Click()
    
    Dim lParametrosGenerales As ParametrosGenerales
    Dim teserror As TipoErrorSummit
    
    teserror.NumError = TESnoerror
   
    If Not VerificarParametros Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    PrepararParametros lParametrosGenerales
    teserror = oGestorParametros.GuardarParametrosGenerales(lParametrosGenerales, oLiteralesAModificar)
        
    If teserror.NumError <> TESnoerror Then
        MsgBox sIdioma(5), vbCritical, "FullStep"
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcIdi.Columns("ID").Value = gParametrosInstalacion.gIdioma Then
        sdbcCalif.RemoveAll
        sdbcCalif.AddItem txtDenCalif1.Text
        sdbcCalif.AddItem txtDenCalif2.Text
        sdbcCalif.AddItem txtDenCalif3.Text
        gParametrosGenerales.gsDEN_CAL1 = txtDenCalif1
        gParametrosGenerales.gsDEN_CAL2 = txtDenCalif2
        gParametrosGenerales.gsDEN_CAL3 = txtDenCalif3
    End If
    
    cmdModifGen.Visible = True
    cmdListado1.Visible = True
    picEdicion.Enabled = False
    picEdit.Visible = False
    
    sdbcIdi.Enabled = True
    Set oLiteralesAModificar = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdA�adir_Click()

   ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgCalif.Scroll 0, sdbgCalif.Rows - sdbgCalif.Row
    
    If sdbgCalif.VisibleRows > 0 Then
        If sdbgCalif.VisibleRows >= sdbgCalif.Rows Then
            If sdbgCalif.VisibleRows = sdbgCalif.Rows Then
                sdbgCalif.Row = sdbgCalif.Rows - 1
            Else
                sdbgCalif.Row = sdbgCalif.Rows
            End If
        Else
            sdbgCalif.Row = sdbgCalif.Rows - (sdbgCalif.Rows - sdbgCalif.VisibleRows) - 1
       End If
    End If
    
    If Me.Visible Then sdbgCalif.SetFocus
    DoEvents
    sdbgCalif.DoClick
    DoEvents
End Sub

Private Sub cmdCancelar_Click()
    sdbcIdi.Enabled = True
    picEdit.Visible = False
    cmdModifGen.Visible = True
    cmdListado1.Visible = True
    picEdicion.Enabled = False
    
    Screen.MousePointer = vbHourglass
    Set oLiterales = oGestorParametros.DevolverLiterales(16, 18, sdbcIdi.Columns("ID").Value)
    Screen.MousePointer = vbNormal
    
    '''''''''''''''''''''''''''''''''''''''''''''
    txtDenCalif1 = oLiterales.Item(1).Den
    txtDenCalif2 = oLiterales.Item(2).Den
    txtDenCalif3 = oLiterales.Item(3).Den
    '''''''''''''''''''''''''''''''''''''''''''
    
    If gParametrosGenerales.gbSENT_ORD_CAL1 Then
        chkDesc1 = vbChecked
    Else
        chkDesc1 = vbUnchecked
    End If
    If gParametrosGenerales.gbSENT_ORD_CAL2 Then
        chkDesc2 = vbChecked
    Else
        chkDesc2 = vbUnchecked
    End If
    If gParametrosGenerales.gbSENT_ORD_CAL3 Then
        chkDesc3 = vbChecked
    Else
        chkDesc3 = vbUnchecked
    End If
End Sub

Private Sub cmdCodigo_Click()

    ''' * Objetivo: Cambiar de codigo la calificaci�n actual
    Dim teserror As TipoErrorSummit
    
    ''' Resaltar la Pago actual
    If sdbgCalif.Rows = 0 Then Exit Sub
    
    sdbgCalif.SelBookmarks.Add sdbgCalif.Bookmark
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    Set oCalificacionEnEdicion = oCalificaciones.Item(sdbgCalif.Bookmark)
    Set oIBAseDatosEnEdicion = oCalificacionEnEdicion
 
    
    teserror = oIBAseDatosEnEdicion.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgCalif.SetFocus
        Exit Sub
    End If
        
    oIBAseDatosEnEdicion.CancelarEdicion
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
    frmMODCOD.caption = "Calificaci�n (Codigo)"
    frmMODCOD.Left = frmPARHom.Left + 500
    frmMODCOD.Top = frmPARHom.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAL
    frmMODCOD.txtCodAct.Text = oCalificacionEnEdicion.Cod
    Set frmMODCOD.fOrigen = frmPARHom
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido sIdioma(6)
        Set oIBAseDatosEnEdicion = Nothing
        Set oCalificacionEnEdicion = Nothing
        Exit Sub
    End If
        
    If UCase(g_sCodigoNuevo) = UCase(oCalificacionEnEdicion.Cod) Then
        oMensajes.NoValido sIdioma(6)
        Set oIBAseDatosEnEdicion = Nothing
        Set oCalificacionEnEdicion = Nothing
        Exit Sub
    End If
            
    ''' Cambiar el codigo
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.CambiarCodigo(g_sCodigoNuevo)
    Screen.MousePointer = vbNormal
             
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    ''' Actualizar los datos
    CargarCalificaciones
    sdbgCalif.MoveFirst
    sdbgCalif.Refresh
    sdbgCalif.Bookmark = sdbgCalif.SelBookmarks(0)
    sdbgCalif.SelBookmarks.RemoveAll
        
    Set oIBAseDatosEnEdicion = Nothing
    Set oCalificacionEnEdicion = Nothing
End Sub

Private Sub cmdDeshacer_Click()
    ''' * Objetivo: Deshacer la edicion en la Calificacion actual
    sdbgCalif.CancelUpdate
    sdbgCalif.DataChanged = False
    
    If Not oCalificacionEnEdicion Is Nothing Then
        oIBAseDatosEnEdicion.CancelarEdicion
        Set oIBAseDatosEnEdicion = Nothing
        Set oCalificacionEnEdicion = Nothing
    End If
    
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    cmdCodigo.Enabled = True
    
    Accion = ACCCalifCon
End Sub

Private Sub cmdEliminar_Click()
    If sdbgCalif.IsAddRow Then Exit Sub

    If Not IsNull(sdbgCalif.Bookmark) Then
        sdbgCalif.SelBookmarks.Add sdbgCalif.Bookmark
        sdbgCalif.DeleteSelected
    End If
End Sub

Private Sub cmdListado_Click()
    AbrirLstParametros "frmPARHom", bOrdenListadoDen
End Sub

Private Sub cmdListado1_Click()
    AbrirLstParametros "frmPARHom", bOrdenListadoDen
End Sub

Private Sub cmdModifGen_Click()
    sdbcIdi.Enabled = False
    picEdit.Visible = True
    cmdModifGen.Visible = False
    cmdListado1.Visible = False
    picEdicion.Enabled = True
    
    'Tengo que rellenar los literales
    Set oLiteralesAModificar = Nothing
    Set oLiteralesAModificar = New CLiterales
    
    oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif1.Text, 16
    oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif2.Text, 17
    oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif3.Text, 18
End Sub

Private Sub cmdModoEdicion_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    Dim v As Variant
    
    If Not bModoEdicion Then
        
        sdbcCalif.Enabled = False
        sdbcIdi2.Enabled = False
        
        sdbgCalif.AllowAddNew = True
        sdbgCalif.AllowUpdate = True
        sdbgCalif.AllowDelete = True
        
        frmPARHom.caption = sIdioma(7) & " (" & sIdioma(9) & ")"
        
        cmdModoEdicion.caption = "&" & sIdioma(16)
        cmdModoEdicion.Enabled = True
        
        cmdRestaurar.Visible = False
        cmdListado.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
        cmdCodigo.Visible = True
            
        bModoEdicion = True
        
        Accion = ACCCalifCon
    
    Else
                
        If sdbgCalif.DataChanged = True Then
        
            v = sdbgCalif.ActiveCell.Value
            If Me.Visible Then sdbgCalif.SetFocus
            If (v <> "") Then
                sdbgCalif.ActiveCell.Value = v
            End If
            
            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        End If
        
        sdbcCalif.Enabled = True
        sdbcIdi2.Enabled = True
        
        sdbgCalif.AllowAddNew = False
        sdbgCalif.AllowUpdate = False
        sdbgCalif.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdCodigo.Visible = False
        
        cmdRestaurar.Visible = True
        cmdListado.Visible = True
        
        If oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador Then
            cmdCodigo.Visible = False
        End If
    
        frmPARHom.caption = sIdioma(7) & " (" & sIdioma(10) & ")"
        
        cmdModoEdicion.caption = "&" & sIdioma(9)
                
        bModoEdicion = False
    End If
    
    If Me.Visible Then sdbgCalif.SetFocus
End Sub

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgCalif.DataChanged = True Then
        bValError = False
        bAnyaError = False
        bModError = False
        If sdbgCalif.Row = 0 Then
            sdbgCalif.MoveNext
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgCalif.MovePrevious
            End If
        Else
            sdbgCalif.MovePrevious
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgCalif.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim I As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PARHOM, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 15)
        For I = 1 To 15
            sIdioma(I) = Ador(0).Value '1
            Ador.MoveNext
        Next
        If basPublic.gParametrosInstalacion.gIdioma = "SPA" Then
            cmdCodigo.caption = Mid(sIdioma(6), 1, Len(sIdioma(6)) - 1) & "&" & Right(sIdioma(6), 1)
        Else
            cmdCodigo.caption = Left(sIdioma(6), 1) & "&" & Mid(sIdioma(6), 2, Len(sIdioma(6)) - 1)
        End If
        cmdModifGen.caption = "&" & sIdioma(9)
        cmdModoEdicion.caption = "&" & sIdioma(9)
        Frame1.caption = sIdioma(7)
        frmPARHom.caption = sIdioma(15)
        Label4.caption = sIdioma(8)
        sdbgCalif.Columns(0).caption = sIdioma(6)
        sdbgCalif.Columns(1).caption = sIdioma(11)
        sdbgCalif.Columns(2).caption = sIdioma(12)
        sdbgCalif.Columns(3).caption = sIdioma(13)
        tabHom.TabCaption(0) = sIdioma(11)
        chkDesc1.caption = Ador(0).Value '16
        chkDesc2.caption = Ador(0).Value
        chkDesc3.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value '20
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        cmdListado1.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        Label5.caption = Ador(0).Value
        Ador.MoveNext
        tabHom.TabCaption(1) = Ador(0).Value '25
        Ador.MoveNext
        lblIdiMat.caption = Ador(0).Value & ":"
        lblIidMat2.caption = Ador(0).Value & ":"
        Ador.MoveNext
        ReDim Preserve sIdioma(1 To 16)
        sIdioma(16) = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub cmdRestaurar_Click()
    '''Objetivo : Restaurar los datos
    
    'Carga la coleccion en el spread
    If Me.sdbcIdi2.Text <> "" And Me.sdbcCalif.Text <> "" Then
        'Carga las calificaciones en la colecci�n
        CargarCalificaciones
        sdbgCalif.ReBind
    End If
    
End Sub

Private Sub Form_Load()
Dim oIdioma As CIdioma

    Me.Height = 4725
    Me.Width = 6615
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
  
    CargarRecursos
  
    cmdListado.Enabled = False
    cmdModoEdicion.Enabled = False
    
    If gParametrosGenerales.gbSENT_ORD_CAL1 Then
        chkDesc1 = vbChecked
    End If
    If gParametrosGenerales.gbSENT_ORD_CAL2 Then
        chkDesc2 = vbChecked
    End If
    If gParametrosGenerales.gbSENT_ORD_CAL3 Then
        chkDesc3 = vbChecked
    End If
    
    sdbgCalif.SelectTypeRow = ssSelectionTypeSingleSelect

    Screen.MousePointer = vbHourglass
    Set oCalificaciones1 = oGestorParametros.DevolverTodasLasCalificaciones(1, , , , , , , True)
    Set oCalificaciones2 = oGestorParametros.DevolverTodasLasCalificaciones(2, , , , , , , True)
    Set oCalificaciones3 = oGestorParametros.DevolverTodasLasCalificaciones(3, , , , , , , True)
    Screen.MousePointer = vbNormal
    
    
    picEdicion.Enabled = False
    
    bOrdenListadoDen = False
    
    Set oIdiomas = oGestorParametros.DevolverIdiomas
    
    'Carga los combos con los idiomas
    For Each oIdioma In oIdiomas
        
        sdbcIdi.AddItem oIdioma.Den & Chr(9) & oIdioma.Cod & Chr(9) & oIdioma.OFFSET
        sdbcIdi2.AddItem oIdioma.Den & Chr(9) & oIdioma.Cod & Chr(9) & oIdioma.OFFSET
    Next
    
    'Carga la pesta�a de Definici�n
    sdbcIdi.Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdi.Columns(1).Value = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Cod
    Set oLiterales = oGestorParametros.DevolverLiterales(16, 18, oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Cod)
    txtDenCalif1 = oLiterales.Item(1).Den
    txtDenCalif2 = oLiterales.Item(2).Den
    txtDenCalif3 = oLiterales.Item(3).Den
    
    'Carga la pesta�a de Valores
    sdbcIdi2.Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdi2.Columns(1).Value = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Cod
    sdbcCalif.AddItem txtDenCalif1
    sdbcCalif.AddItem txtDenCalif2
    sdbcCalif.AddItem txtDenCalif3
    sdbcCalif = txtDenCalif1
    sdbcCalif_CloseUp
    bModoEdicion = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If actualizarYSalir() Then
        Cancel = True
        Exit Sub
    End If
        
    Set oIdiomas = Nothing
    Set oLiterales = Nothing
    Set oLiteralesAModificar = Nothing
    
    Set oCalificaciones1 = Nothing
    Set oCalificaciones2 = Nothing
    Set oCalificaciones3 = Nothing
    Set oCalificaciones = Nothing
    Me.Visible = False
End Sub


Private Sub sdbcCalif_Click()
    If sdbcCalif.DroppedDown Then
        Exit Sub
    Else
        Select Case sdbcCalif
            Case txtDenCalif1
                Set oCalificaciones = oCalificaciones1
            Case txtDenCalif2
                Set oCalificaciones = oCalificaciones2
            Case txtDenCalif3
                Set oCalificaciones = oCalificaciones3
        End Select
        
        sdbgCalif.ReBind
    End If
End Sub

Private Sub sdbcCalif_CloseUp()
    'Carga las calificaciones en la colecci�n
    CargarCalificaciones
    
    'Carga la coleccion en el spread
    If Me.sdbcIdi2.Text <> "" Then sdbgCalif.ReBind
End Sub

Private Sub sdbcCalif_InitColumnProps()
    sdbcCalif.DataFieldList = "Column 0"
    sdbcCalif.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCalif_PositionList(ByVal Text As String)
     ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCalif.MoveFirst
    
    If Text <> "" Then
        For I = 0 To sdbcCalif.Rows - 1
            bm = sdbcCalif.GetBookmark(I)
            If UCase(Text) = UCase(Mid(sdbcCalif.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCalif.Bookmark = bm
                Exit For
            End If
        Next I
    End If
End Sub

Private Sub sdbcIdi_CloseUp()
    Dim oLiterales As CLiterales

    If picEdicion.Enabled = False Then
        'Estamos en modo consulta
        If sdbcIdi.Value <> "" Then
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(16, 18, sdbcIdi.Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtDenCalif1.Text = oLiterales.Item(1).Den
                txtDenCalif2.Text = oLiterales.Item(2).Den
                txtDenCalif3.Text = oLiterales.Item(3).Den
            Else
                txtDenCalif1.Text = ""
                txtDenCalif2.Text = ""
                txtDenCalif3.Text = ""
            End If
        End If
            
        Set oLiterales = Nothing
    Else
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
        
        If oLiteralesAModificar.Item(sdbcIdi.Columns("ID").Value & CStr(16)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(16, 18, sdbcIdi.Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtDenCalif1.Text = oLiterales.Item(1).Den
                oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif1.Text, 16
                
                txtDenCalif2.Text = oLiterales.Item(2).Den
                oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif2.Text, 17
               
                txtDenCalif3.Text = oLiterales.Item(3).Den
                oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif3, 18
            Else
                txtDenCalif1.Text = ""
                oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif1.Text, 16
                txtDenCalif2.Text = ""
                oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif2.Text, 17
                txtDenCalif3.Text = ""
                oLiteralesAModificar.Add sdbcIdi.Columns("ID").Value, txtDenCalif3.Text, 18
            End If
        Else
            'Cargamos desde oLiteralesAModificar
            txtDenCalif1.Text = oLiteralesAModificar.Item(sdbcIdi.Columns("ID").Value & CStr(16)).Den
            txtDenCalif2.Text = oLiteralesAModificar.Item(sdbcIdi.Columns("ID").Value & CStr(17)).Den
            txtDenCalif3.Text = oLiteralesAModificar.Item(sdbcIdi.Columns("ID").Value & CStr(18)).Den
    End If
End If

End Sub

Private Sub sdbcIdi_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcIdi.MoveFirst
    
    If Text <> "" Then
        For I = 0 To sdbcIdi.Rows - 1
            bm = sdbcIdi.GetBookmark(I)
            If UCase(Text) = UCase(Mid(sdbcIdi.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcIdi.Bookmark = bm
                Exit For
            End If
        Next I
    End If
End Sub

Private Sub sdbcIdi2_CloseUp()
    ''' * Objetivo: Cargar el grid con las calificaciones
    
    sdbcIdi2.Text = sdbcIdi2.Columns(0).Value
    'Carga las calificaciones en la colecci�n
    CargarCalificaciones
    
    'Carga la coleccion en el spread
    If Me.sdbcCalif.Text <> "" Then
        sdbgCalif.ReBind
    End If
    
End Sub

Private Sub sdbcIdi2_DropDown()
    Dim oIdioma As CIdioma
    
    Set oIdiomas = Nothing
    Set oIdiomas = oGestorParametros.DevolverIdiomas
    sdbcIdi2.RemoveAll
    
    'Carga los combos con los idiomas
    For Each oIdioma In oIdiomas
        sdbcIdi2.AddItem oIdioma.Den & Chr(9) & oIdioma.Cod & Chr(9) & oIdioma.OFFSET
    Next
End Sub

Private Sub sdbcIdi2_InitColumnProps()
    sdbcIdi2.DataFieldList = "Column 0"
    sdbcIdi2.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcIdi2_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcIdi2.MoveFirst
    
    If Text <> "" Then
        For I = 0 To sdbcIdi2.Rows - 1
            bm = sdbcIdi2.GetBookmark(I)
            If UCase(Text) = UCase(Mid(sdbcIdi2.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcIdi2.Bookmark = bm
                Exit For
            End If
        Next I
    End If
End Sub

Private Sub sdbgCalif_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    RtnDispErrMsg = 0
    
    If Me.Visible Then sdbgCalif.SetFocus
    sdbgCalif.Bookmark = sdbgCalif.RowBookmark(sdbgCalif.Row)
End Sub

Private Sub sdbgCalif_AfterInsert(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgCalif_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgCalif_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

    ''' * Objetivo: Confirmacion antes de eliminar
    
    Dim irespuesta As Integer
    
    DispPromptMsg = 0
    
    If sdbgCalif.IsAddRow Or Accion = ACCCalifMod Then
        Cancel = True
        Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminar(sIdioma(8) & " " & sdbgCalif.Columns(0).Value & " (" & sdbgCalif.Columns(1).Value & ")")
    
    If irespuesta = vbNo Then Cancel = True
    
End Sub

Private Sub sdbgCalif_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    
    bValError = False
    Cancel = False
    
    If Trim(sdbgCalif.Columns(0).Value) = "" Then
        oMensajes.NoValido sIdioma(6)
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgCalif.Columns(1).Value) = "" Then
        oMensajes.NoValida sIdioma(11)
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgCalif.Columns(2).Value) = "" Then
        oMensajes.NoValido sIdioma(12)
        Cancel = True
        GoTo Salir
    Else
        If Not IsNumeric(sdbgCalif.Columns(2).Value) Then
            oMensajes.NoValido sIdioma(12)
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If Trim(sdbgCalif.Columns(3).Value) = "" Then
        oMensajes.NoValido sIdioma(13)
        Cancel = True
        GoTo Salir
    Else
        If Not IsNumeric(sdbgCalif.Columns(3).Value) Then
            oMensajes.NoValido sIdioma(13)
            Cancel = True
            GoTo Salir
        End If
    End If
    
Salir:
    bValError = Cancel
    If Me.Visible Then sdbgCalif.SetFocus
End Sub
Private Sub sdbgCalif_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If cmdDeshacer.Enabled = False Then
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
        cmdCodigo.Enabled = False
    End If
    
    If Accion = ACCCalifCon And Not sdbgCalif.IsAddRow Then
        Set oCalificacionEnEdicion = Nothing
        
        If Not IsNull(sdbgCalif.Bookmark) Then
            Set oCalificacionEnEdicion = oCalificaciones.Item(sdbgCalif.Bookmark)
        Else
            Set oCalificacionEnEdicion = oCalificaciones.Item(sdbgCalif.Row + 1)
        End If
      
        Set oIBAseDatosEnEdicion = oCalificacionEnEdicion
        
        teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            TratarError teserror
            sdbgCalif.DataChanged = False
            sdbgCalif.Columns(0).Value = oCalificacionEnEdicion.Cod
            sdbgCalif.Columns(1).Value = oCalificacionEnEdicion.Den
            sdbgCalif.Columns(2).Value = oCalificacionEnEdicion.ValorInf
            sdbgCalif.Columns(3).Value = oCalificacionEnEdicion.ValorSup
            
            teserror.NumError = TESnoerror
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgCalif.SetFocus
        Else
            Accion = ACCCalifMod
        End If
    End If
End Sub
Private Sub sdbgCalif_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    Dim sCodigo As String
    Dim sDenominacion As String
    Dim sHeadCaption As String
    
    If bModoEdicion Then Exit Sub

    If sdbgCalif.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgCalif.Columns(ColIndex).caption
    sdbgCalif.Columns(ColIndex).caption = sIdioma(14)
    
    ''' Volvemos a cargar las Calificaciones, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    
    Set oCalificaciones = Nothing
    Set oCalificaciones = oFSGSRaiz.generar_CCalificaciones

    If caption <> sIdioma(7) & " (" & sIdioma(10) & ")" Then
        If InStr(1, caption, sIdioma(6), vbTextCompare) <> 0 Then
            ''' Datos filtrados por codigo
            If InStr(1, caption, "*", vbTextCompare) <> 0 Then
                sCodigo = Right(caption, Len(caption) - 27)
                sCodigo = Left(sCodigo, Len(sCodigo) - 2)
            
                Select Case ColIndex
                Case 0
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sCodigo), , , , , , True, sdbcIdi2.Columns(1).Value
                Case 1
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sCodigo), , , True, , , True, sdbcIdi2.Columns(1).Value
                Case 2, 3
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sCodigo), , , , True, , True, sdbcIdi2.Columns(1).Value
                End Select
            Else
                sCodigo = Right(caption, Len(caption) - 27)
                sCodigo = Left(sCodigo, Len(sCodigo) - 1)
            
                Select Case ColIndex
                Case 0
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sCodigo), , True, , , , True, sdbcIdi2.Columns(1).Value
                Case 1
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sCodigo), , True, True, , , True, sdbcIdi2.Columns(1).Value
                Case 2, 3
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sCodigo), , True, , True, , True, sdbcIdi2.Columns(1).Value
                End Select
           End If
        Else
            ''' Datos filtrados por denominacion
            sDenominacion = Right(caption, Len(caption) - 34)
            If InStr(1, sDenominacion, "*", vbTextCompare) <> 0 Then
                sDenominacion = Left(sDenominacion, Len(sDenominacion) - 2)
                Select Case ColIndex
                Case 0
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, , Trim(sDenominacion), , , , , True, sdbcIdi2.Columns(1).Value
                Case 1
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, , Trim(sDenominacion), , True, , , True, sdbcIdi2.Columns(1).Value
                Case 2, 3
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, , Trim(sDenominacion), , , True, , True, sdbcIdi2.Columns(1).Value
                End Select
            Else
                sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
                Select Case ColIndex
                Case 0
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sDenominacion), True, , , , True, , sdbcIdi2.Columns(1).Value
                Case 1
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sDenominacion), True, True, , , True, , sdbcIdi2.Columns(1).Value
                Case 2, 3
                    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, Trim(sDenominacion), True, , True, , True, , sdbcIdi2.Columns(1).Value
                End Select
            End If
        End If
    Else
        ''' Datos no filtrados
        Select Case ColIndex
        Case 0
            oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, , , , , , , True, sdbcIdi2.Columns(1).Value
        Case 1
            oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, , , , True, , , True, sdbcIdi2.Columns(1).Value
        Case 2, 3
            oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, , , , , True, , True, sdbcIdi2.Columns(1).Value
        End Select
    End If
    
    'orden listado
    Select Case ColIndex
        Case 0
            bOrdenListadoDen = False
        Case 1
            bOrdenListadoDen = True
        Case 2, 3
            bOrdenListadoDen = False
    End Select
   
    sdbgCalif.ReBind
    sdbgCalif.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgCalif_InitColumnProps()
    sdbgCalif.Columns(0).FieldLen = basParametros.gLongitudesDeCodigos.giLongCodCAL
End Sub

Private Sub sdbgCalif_KeyPress(KeyAscii As Integer)
    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
        If sdbgCalif.DataChanged = False Then
            sdbgCalif.CancelUpdate
            sdbgCalif.DataChanged = False
            
            If Not oCalificacionEnEdicion Is Nothing Then
                oIBAseDatosEnEdicion.CancelarEdicion
                Set oIBAseDatosEnEdicion = Nothing
                Set oCalificacionEnEdicion = Nothing
            End If
           
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True

            Accion = ACCCalifCon
        Else
            If sdbgCalif.IsAddRow Then
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
           
                Accion = ACCCalifCon
            End If
        End If
    End If
End Sub

Private Sub sdbgCalif_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    If Not sdbgCalif.IsAddRow Then
        sdbgCalif.Columns(0).Locked = True
        
        If sdbgCalif.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True
        End If
    Else
        If sdbgCalif.DataChanged = True Then cmdDeshacer.Enabled = True
        sdbgCalif.Columns(0).Locked = False
        
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgCalif.Row) Then
                sdbgCalif.col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdCodigo.Enabled = False
    End If
End Sub
Private Sub sdbgCalif_UnboundAddData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, NewRowBookmark As Variant)
    ''' * Objetivo: Hemos anyadido una fila al grid,
    ''' * Objetivo: ahora hay que anyadirla a la
    ''' * Objetivo: coleccion
    ''' * Recibe: Buffer con los datos a anyadir y bookmark de la fila
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    Dim inum As Integer
    
    bAnyaError = False
    
    '' Anyadir a la base de datos
    
    ''' Anyadir a la coleccion
    inum = sdbcCalif.Row + 1
    oCalificaciones.Add "-1", "", inum, "", 0, 0, oCalificaciones.Count
    
    
    If sdbgCalif.Rows = 1 Then
        Set oCalificacionEnEdicion = oCalificaciones.Item(1)
    Else
        Set oCalificacionEnEdicion = oCalificaciones.Item(oCalificaciones.Count)
    End If
   
    oCalificacionEnEdicion.Cod = sdbgCalif.Columns(0).Value
    oCalificacionEnEdicion.Den = sdbgCalif.Columns(1).Value
    oCalificacionEnEdicion.ValorInf = sdbgCalif.Columns(2).Value
    oCalificacionEnEdicion.ValorSup = sdbgCalif.Columns(3).Value
    oCalificacionEnEdicion.idioma = sdbcIdi2.Columns(1).Value
    
    Set oIBAseDatosEnEdicion = oCalificacionEnEdicion
    
    teserror = oIBAseDatosEnEdicion.AnyadirABaseDatos
    
    If teserror.NumError <> TESnoerror Then
        
        v = sdbgCalif.ActiveCell.Value
        oCalificaciones.Remove (oCalificaciones.Count)
        
        TratarError teserror
        If Me.Visible Then sdbgCalif.SetFocus
        bAnyaError = True
        RowBuf.RowCount = 0
        sdbgCalif.ActiveCell.Value = v
        
    Else
        ''' Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCCalifAnya, "Cod:" & oCalificacionEnEdicion.Cod
        
        Accion = ACCCalifCon
    
    End If
    
    Set oIBAseDatosEnEdicion = Nothing
    Set oCalificacionEnEdicion = Nothing
        
End Sub
Private Sub sdbgCalif_UnboundDeleteRow(Bookmark As Variant)

    ''' * Objetivo: Hemos eliminado una fila al grid,
    ''' * Objetivo: ahora hay que eliminarla de la
    ''' * Objetivo: coleccion
    ''' * Recibe: Bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    Dim IndFor As Long, IndCalif As Long
    Dim inum As Integer
        
    If sdbcCalif.Text = txtDenCalif1 Then
        inum = 1
    Else
        If sdbcCalif.Text = txtDenCalif2 Then
            inum = 2
        Else
            inum = 3
        End If
    End If
        
    ''' Eliminamos de la base de datos
    
    Set oCalificacionEnEdicion = oCalificaciones.Item(Bookmark)
    
    Set oIBAseDatosEnEdicion = oCalificacionEnEdicion
    teserror = oIBAseDatosEnEdicion.EliminarDeBaseDatos
        
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        sdbgCalif.Refresh
        If Me.Visible Then sdbgCalif.SetFocus
    Else
        ''' Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCCalifEli, "Cod:" & oCalificacionEnEdicion.Cod
        
        ''' Eliminar de la coleccion
        
        IndCalif = val(Bookmark)
        
        For IndFor = IndCalif To oCalificaciones.Count - 2
            
            oCalificaciones.Remove (IndFor)
            Set oCalificacionEnEdicion = oCalificaciones.Item(IndFor + 1)
            oCalificaciones.Add oCalificacionEnEdicion.Cod, oCalificacionEnEdicion.Den, inum, oCalificacionEnEdicion.ValorInf, oCalificacionEnEdicion.ValorSup, IndFor
            Set oCalificacionEnEdicion = Nothing
            
        Next IndFor
        
        oCalificaciones.Remove (IndFor)
       
        Accion = ACCCalifCon
        
    End If
    
    Set oIBAseDatosEnEdicion = Nothing
    Set oCalificacionEnEdicion = Nothing
    
End Sub
Private Sub sdbgCalif_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim I As Integer
    Dim j As Integer
    
    Dim oCalif As CCalificacion
    
    Dim iNumCalificaciones As Integer
    
    If oCalificaciones Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If

    
    iNumCalificaciones = oCalificaciones.Count
    
    If IsNull(StartLocation) Then       'If the grid is empty then
        If ReadPriorRows Then               'If moving backwards through grid then
            p = iNumCalificaciones - 1                             'pointer = # of last grid row
        Else                                        'else
            p = 1
             
    'pointer = # of first grid row
        End If
    Else                                        'If the grid already has data in it then
        p = StartLocation                       'pointer = location just before or after the row where data will be added
    
        If ReadPriorRows Then               'If moving backwards through grid then
                p = p - 1                               'move pointer back one row
        Else                                        'else
                p = p + 1                               'move pointer ahead one row
        End If
    End If
    
    'The pointer (p) now points to the row of the grid where you will start adding data.
    
    For I = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
        
        If p < 0 Or p > iNumCalificaciones Then Exit For           'If the pointer is outside the grid then stop this
    
        Set oCalif = Nothing
        Set oCalif = oCalificaciones.Item(p)
    
        For j = 0 To 3
      
            Select Case j
                    
                    Case 0:
                            RowBuf.Value(I, 0) = oCalif.Cod
                    Case 1:
                            RowBuf.Value(I, 1) = oCalif.Den
                    Case 2:
                            RowBuf.Value(I, 2) = oCalif.ValorInf
                    Case 3:
                            RowBuf.Value(I, 3) = oCalif.ValorSup
                    
            End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
        Next j
    
        RowBuf.Bookmark(I) = p                              'set the value of the bookmark for the current row in the rowbuffer
    
        If ReadPriorRows Then                               'move the pointer forward or backward, depending
            p = p - 1                                           'on which way it's supposed to move
        Else
            p = p + 1
        End If
            r = r + 1                                               'increment the number of rows read
        Next I
    
    RowBuf.RowCount = r                                         'set the size of the row buffer to the number of rows read

    Set oCalif = Nothing
    
End Sub
Private Sub sdbgCalif_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim v As Variant
    
    bModError = False
    
    ''' Modificamos en la base de datos
    
    oCalificacionEnEdicion.Cod = sdbgCalif.Columns(0).Value
    oCalificacionEnEdicion.Den = sdbgCalif.Columns(1).Value
    oCalificacionEnEdicion.ValorInf = sdbgCalif.Columns(2).Value
    oCalificacionEnEdicion.ValorSup = sdbgCalif.Columns(3).Value
    
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
    Screen.MousePointer = vbNormal
       
    If teserror.NumError <> TESnoerror Then
        v = sdbgCalif.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgCalif.SetFocus
        bModError = True
        RowBuf.RowCount = 0
        sdbgCalif.ActiveCell.Value = v
    Else
        ''' Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCCalifAnya, "Cod:" & oCalificacionEnEdicion.Cod
     
        Accion = ACCCalifCon
        
        Set oIBAseDatosEnEdicion = Nothing
        Set oCalificacionEnEdicion = Nothing
    End If
End Sub

Private Sub tabHom_Click(PreviousTab As Integer)
    
    If PreviousTab = 0 And cmdModifGen.Visible = False Then
        tabHom.Tab = 0
    ElseIf PreviousTab = 0 Then
        cmdModifGen.Enabled = False
        cmdListado1.Enabled = False
        cmdModoEdicion.Enabled = True
        cmdListado.Enabled = True
    End If
    
    If PreviousTab = 1 And cmdModoEdicion.caption = "&" & sIdioma(16) Then
        tabHom.Tab = 1
    ElseIf PreviousTab = 1 Then
        cmdModifGen.Enabled = True
        cmdListado1.Enabled = True
        cmdModoEdicion.Enabled = False
        cmdListado.Enabled = False
    End If
    
    If tabHom.Tab = 0 Then
        Me.caption = sIdioma(15)
    Else
        Me.caption = sIdioma(7) & " (" & sIdioma(10) & ")"
    End If
End Sub

Private Sub txtDenCalif1_Validate(Cancel As Boolean)
     oLiteralesAModificar.Item(sdbcIdi.Columns("ID").Value & CStr(16)).Den = txtDenCalif1.Text
End Sub

Private Sub txtDenCalif2_Validate(Cancel As Boolean)
     oLiteralesAModificar.Item(sdbcIdi.Columns("ID").Value & CStr(17)).Den = txtDenCalif2.Text
End Sub

Private Sub txtDenCalif3_Validate(Cancel As Boolean)
    oLiteralesAModificar.Item(sdbcIdi.Columns("ID").Value & CStr(18)).Den = txtDenCalif3.Text
End Sub

Private Sub CargarCalificaciones()
    
    ''' Objetivo : Cargar las calificaciones seg�n el idioma seleccionado
    Set oCalificaciones = Nothing
    Set oCalificaciones = oFSGSRaiz.generar_CCalificaciones
    
    Screen.MousePointer = vbHourglass
    oCalificaciones.CargarTodasLasCalificaciones sdbcCalif.Row + 1, , , , , , , True, sdbcIdi2.Columns(1).Value
    Screen.MousePointer = vbNormal
End Sub
