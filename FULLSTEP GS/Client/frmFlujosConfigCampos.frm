VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmFlujosConfigCampos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DConfiguraci�n de campos en las notificaciones"
   ClientHeight    =   8145
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9525
   Icon            =   "frmFlujosConfigCampos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8145
   ScaleWidth      =   9525
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton optConfiguracionPersonalizada 
      Caption         =   "DUtilizar configuraci�n personalizada"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   3855
   End
   Begin VB.OptionButton optConfiguracionRol 
      Caption         =   "DUtilizar configuraci�n de campos del rol"
      Height          =   255
      Left            =   4200
      TabIndex        =   3
      Top             =   120
      Width           =   5055
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   6015
      Left            =   480
      TabIndex        =   2
      Top             =   1440
      Width           =   8595
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   675
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      GroupHeaders    =   0   'False
      Col.Count       =   5
      stylesets.count =   7
      stylesets(0).Name=   "No"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosConfigCampos.frx":0CB2
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "Calculado"
      stylesets(1).BackColor=   16766421
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFlujosConfigCampos.frx":0D19
      stylesets(1).AlignmentPicture=   1
      stylesets(2).Name=   "S�"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFlujosConfigCampos.frx":0D35
      stylesets(2).AlignmentPicture=   2
      stylesets(3).Name=   "Gris"
      stylesets(3).BackColor=   12632256
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFlujosConfigCampos.frx":0D51
      stylesets(4).Name=   "Normal"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmFlujosConfigCampos.frx":0D6D
      stylesets(5).Name=   "ActiveRow"
      stylesets(5).ForeColor=   16777215
      stylesets(5).BackColor=   8388608
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmFlujosConfigCampos.frx":0D89
      stylesets(6).Name=   "Amarillo"
      stylesets(6).BackColor=   12648447
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmFlujosConfigCampos.frx":0DA5
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   13388
      Columns(1).Caption=   "DDato"
      Columns(1).Name =   "DATO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1323
      Columns(2).Caption=   "DVisible"
      Columns(2).Name =   "VISIBLE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   11
      Columns(2).FieldLen=   256
      Columns(2).Style=   2
      Columns(2).StyleSet=   "Normal"
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "DESGLOSE"
      Columns(3).Name =   "DESGLOSE"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   11
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "MODIFICAR"
      Columns(4).Name =   "MODIFICAR"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   15161
      _ExtentY        =   10610
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   675
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.TabStrip ssTabGrupos 
      Height          =   6900
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   12171
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   1
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCampos 
      Caption         =   "DIndique qu� campos se visualizar�n en los emails de notificaciones:"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   8280
   End
End
Attribute VB_Name = "frmFlujosConfigCampos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables Publicas
Public lIdFormulario As Long
Public lIdNotificado As Long
Public iOrigenNotificado As OrigenNotificadoAccion
Public lCampoDesglose As Long
Public bConfiguracionCamposRolVisible As Boolean
Public bConfiguracionRol As Boolean
Public oNotificadoEnEdicion As CNotificadoAccion
Public mlGrupo As Long
Public mbDesgloseConConfiguracionRol As Boolean
'Variables Privadas
Dim msColDato As String
Dim msColVisible As String
Dim mlRolNotificacion As Long
Dim mlBloqueNotificacion As Long
Dim gGrupos As Collection
Private moCumplimentaciones As CPMConfCumplimentaciones
Private m_bCargaIni As Boolean

Private m_bActivado As Boolean

Private Sub Form_Activate()
If Not m_bActivado Then
    m_bActivado = True
End If
End Sub

Private Sub Form_Load()
    m_bActivado = False
    Me.Height = 8350
    Me.Width = 9615
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    PonerFieldSeparator Me
        
    If iOrigenNotificado = OrigenNotificadoAccion.Expiracion Then
        optConfiguracionRol.Visible = False
        optConfiguracionPersonalizada.Visible = False
        
        Me.optConfiguracionPersonalizada.Value = True
        mbDesgloseConConfiguracionRol = False
        
        ssTabGrupos.Top = lblCampos.Top
        Me.sdbgCampos.Top = Me.sdbgCampos.Top + 50
        lblCampos.Top = Me.optConfiguracionPersonalizada.Top
        
        Me.Height = Me.Height - 200
        
    Else
        ConfigurarCamposNotificaciones
    End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSCONFIGCAMPOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value ' Configuraci�n de campos en las notificaciones
        Ador.MoveNext
        lblCampos.caption = Ador(0).Value ' Indique qu� campos se visualizar�n en los emails de notificaciones
        Ador.MoveNext
        msColDato = Ador(0).Value ' Dato
        Ador.MoveNext
        msColVisible = Ador(0).Value ' Visible
        Ador.MoveNext
        Me.optConfiguracionRol.caption = Ador(0).Value ' Configuracion Rol
        Ador.MoveNext
        Me.optConfiguracionPersonalizada.caption = Ador(0).Value   ' Configuracion personalizada
        
        Ador.Close
    End If
End Sub

''' <summary>Procedimiento que Configura si sera visible o no la configuracion de Rol</summary>
''' <remarks></remarks>
''' <revision>dsl 13/12/2012</revision>
Private Sub ConfigurarCamposNotificaciones()
    m_bCargaIni = True
    
    If lCampoDesglose = 0 Then
        Me.optConfiguracionPersonalizada.Visible = bConfiguracionCamposRolVisible
        Me.optConfiguracionRol.Visible = bConfiguracionCamposRolVisible
    Else
        Me.optConfiguracionPersonalizada.Visible = False
        Me.optConfiguracionRol.Visible = False
    End If
    
    If oNotificadoEnEdicion.Configuracion_Rol Then
        Me.optConfiguracionRol.Value = True
        mbDesgloseConConfiguracionRol = True
    Else
        Me.optConfiguracionPersonalizada.Value = True
        mbDesgloseConConfiguracionRol = False
    End If
    
    m_bCargaIni = False
End Sub



''' <summary>Procedimiento que se ejecuta cuando se pulsa en la opcion configuracion personalizada</summary>
''' <remarks></remarks>
''' <revision>dsl 13/12/2012</revision>
Private Sub optConfiguracionPersonalizada_Click()
Dim teserror As TipoErrorSummit
Dim oNotifAccion As CNotificadoAccion
Dim oIBaseDatos As IBaseDatos

Set oNotifAccion = oFSGSRaiz.Generar_CNotificadoAccion

oNotifAccion.Origen = iOrigenNotificado
oNotifAccion.Id = Me.lIdNotificado
Set oIBaseDatos = oNotifAccion
teserror = oIBaseDatos.IniciarEdicion
If teserror.NumError <> TESnoerror Then
     sdbgCampos.DataChanged = False
     basErrores.TratarError teserror
     If Me.Visible Then sdbgCampos.SetFocus
     GoTo Salir
End If

oNotifAccion.Configuracion_Rol = False
If lCampoDesglose = 0 Then
    oNotificadoEnEdicion.Configuracion_Rol = False
End If
If m_bActivado Then
    teserror = oIBaseDatos.FinalizarEdicionModificando
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgCampos.SetFocus
    Else
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
End If
If lCampoDesglose > 0 Then
    CargarDesglose
Else
    CargarGrupos
    CargarCamposGrupo m_bActivado
End If

Salir:
    
End Sub
''' <summary>Procedimiento que se ejecuta cuando se pulsa en la opcion configuracion por rol</summary>
''' <remarks></remarks>
''' <revision>dsl 13/12/2012</revision>
Private Sub optConfiguracionRol_Click()
Dim teserror As TipoErrorSummit
Dim oNotifAccion As CNotificadoAccion
Dim oIBaseDatos As IBaseDatos

Set oNotifAccion = oFSGSRaiz.Generar_CNotificadoAccion


oNotifAccion.Origen = iOrigenNotificado
oNotifAccion.Id = Me.lIdNotificado

Set oIBaseDatos = oNotifAccion
teserror = oIBaseDatos.IniciarEdicion
If teserror.NumError <> TESnoerror Then
     sdbgCampos.DataChanged = False
     basErrores.TratarError teserror
     If Me.Visible Then sdbgCampos.SetFocus
     GoTo Salir
End If
oNotifAccion.Configuracion_Rol = True

If lCampoDesglose = 0 Then
    oNotificadoEnEdicion.Configuracion_Rol = True
End If
If m_bActivado Then
    teserror = oIBaseDatos.FinalizarEdicionModificando
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgCampos.SetFocus
    Else
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
End If
oNotifAccion.DevolverRolBloqueNotificacion Me.lIdNotificado, iOrigenNotificado
mlRolNotificacion = oNotifAccion.Rol_Notificacion
mlBloqueNotificacion = oNotifAccion.Bloque_Notificacion

'Se carga la cumplimentacion de los campos
If lCampoDesglose > 0 Then
    If Me.mbDesgloseConConfiguracionRol Then
        'Si ya tiene grabada la configuracion de Rol(CONFIGURACION_ROL en PM_NOTIFICADO_ENLACE o PM_NOTIFICADO_ACCION)
        'la visibilidad de los campos ya estara grabada en la tabla PM_NOTIFICADO_ENLACE_CAMPOS o PM_NOTIFICADO_ACCION_CAMPOS
        CargarDesglose
    Else
        'Si no tiene configuracion de rol habra que cargar la visibilidad de la cumplimentacion del Rol
        CargarCumplimentacionDesglose
    End If
Else
    CargarGrupos
    
    If bConfiguracionRol Then
        'Si ya tiene grabada la configuracion de Rol(CONFIGURACION_ROL en PM_NOTIFICADO_ENLACE o PM_NOTIFICADO_ACCION)
        'la visibilidad de los campos ya estara grabada en la tabla PM_NOTIFICADO_ENLACE_CAMPOS o PM_NOTIFICADO_ACCION_CAMPOS
        CargarCamposGrupo m_bActivado
    Else
        'Si no tiene configuracion de rol habra que cargar la visibilidad de la cumplimentacion del Rol
        CargarCumplimentacionRol
    End If

    
End If

Salir:
    
End Sub

Private Sub sdbgCampos_BtnClick()
    Dim frmDesglose As New frmFlujosConfigCampos
    With frmDesglose
        .lIdFormulario = lIdFormulario
        .lIdNotificado = lIdNotificado
        .iOrigenNotificado = iOrigenNotificado
        .lCampoDesglose = sdbgCampos.Columns("ID").Value
        .bConfiguracionRol = bConfiguracionRol
        .mbDesgloseConConfiguracionRol = mbDesgloseConConfiguracionRol
        Set .oNotificadoEnEdicion = Me.oNotificadoEnEdicion
        .mlGrupo = mlGrupo
        Load frmDesglose
        .caption = sdbgCampos.Columns("DATO").Text
        .Show vbModal
        mbDesgloseConConfiguracionRol = True
    End With
End Sub
''' <summary>Procedimiento que graba la visibilidad del campo</summary>
''' <remarks></remarks>
''' <revision>dsl 13/12/2012</revision>
Private Sub GrabarCambioCampo(ByVal lID As Long, ByVal bVisible As Boolean)
    Dim teserror As TipoErrorSummit
    Dim oCampoNotif As CNotificadoAccionCampo
    Dim oIBaseDatos As IBaseDatos
    Set oCampoNotif = oFSGSRaiz.Generar_CNotificadoAccionCampo
    oCampoNotif.Campo = lID
    oCampoNotif.Notificado = lIdNotificado
    oCampoNotif.Origen = iOrigenNotificado
    oCampoNotif.Visible = bVisible
    If m_bActivado Then
        Set oIBaseDatos = oCampoNotif
        teserror = oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            sdbgCampos.DataChanged = False
            basErrores.TratarError teserror
            If Me.Visible Then sdbgCampos.SetFocus
            GoTo Salir
        End If
    
        teserror = oIBaseDatos.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            If Me.Visible Then sdbgCampos.SetFocus
        Else
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
Salir:
   Set oIBaseDatos = Nothing
   Set oCampoNotif = Nothing
End Sub


Private Sub sdbgCampos_Change()

    If Me.optConfiguracionRol.Value = True Then
        If sdbgCampos.Columns("MODIFICAR").Value = 1 Then
            sdbgCampos.CancelUpdate
        End If
    End If
    
    Dim teserror As TipoErrorSummit
    Dim oCampoNotif As CNotificadoAccionCampo
    Dim oIBaseDatos As IBaseDatos
    Set oCampoNotif = oFSGSRaiz.Generar_CNotificadoAccionCampo
    oCampoNotif.Campo = sdbgCampos.Columns("ID").Value
    oCampoNotif.Notificado = lIdNotificado
    oCampoNotif.Origen = iOrigenNotificado
    Set oIBaseDatos = oCampoNotif
    teserror = oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        sdbgCampos.DataChanged = False
        basErrores.TratarError teserror
        If Me.Visible Then sdbgCampos.SetFocus
        GoTo Salir
    End If
    oCampoNotif.Visible = sdbgCampos.Columns("VISIBLE").Value
    teserror = oIBaseDatos.FinalizarEdicionModificando
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgCampos.SetFocus
    Else
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
Salir:
   Set oIBaseDatos = Nothing
   Set oCampoNotif = Nothing
End Sub

Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)
    If Me.optConfiguracionRol.Value = True Then
        If sdbgCampos.Columns("MODIFICAR").Value = 1 Then
            sdbgCampos.Columns("DATO").CellStyleSet "Gris"
            sdbgCampos.Columns("VISIBLE").CellStyleSet "Gris"
            sdbgCampos.Columns("ID").CellStyleSet "Gris"
            sdbgCampos.Columns("DESGLOSE").CellStyleSet "Gris"
        End If
    End If
End Sub

Private Sub sdbgCampos_InitColumnProps()
    sdbgCampos.Columns("DATO").caption = msColDato
    sdbgCampos.Columns("VISIBLE").caption = msColVisible
End Sub

Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    If sdbgCampos.Columns("DESGLOSE").Value Then
        sdbgCampos.Columns("DATO").Style = ssStyleEditButton
    Else
        sdbgCampos.Columns("DATO").Style = ssStyleEdit
    End If
   
End Sub



Private Sub SSTabGrupos_Click()
    If sdbgCampos.DataChanged Then
        sdbgCampos.Update
    End If
    CargarCamposGrupo False
    mlGrupo = ssTabGrupos.selectedItem.Tag
    If Me.Visible Then sdbgCampos.SetFocus
End Sub

Private Sub CargarGrupos()
Dim oFormulario As CFormulario
Dim iTab As Integer
Dim oGrupo As CFormGrupo

    Set gGrupos = New Collection
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = lIdFormulario
    oFormulario.CargarTodosLosGrupos
    
    ssTabGrupos.Tabs.clear
    iTab = 1
    
    For Each oGrupo In oFormulario.Grupos
        ssTabGrupos.Tabs.Add iTab, "A" & oGrupo.Id, NullToStr(oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        ssTabGrupos.Tabs(iTab).Tag = CStr(oGrupo.Id)
        gGrupos.Add oGrupo.Id, CStr(oGrupo.Id)
        iTab = iTab + 1
    Next
    
    Set oGrupo = Nothing
    Set oFormulario = Nothing
    
End Sub

Private Sub CargarCamposGrupo(ByVal bGuarda As Boolean)
    Dim oNotificado As CNotificadoAccion
    Dim oCampoNotificado As CNotificadoAccionCampo
    Dim oCumplimentacion As CPMConfCumplimentacion
    Dim oCampoCumplimentacion As CPMConfCumplimentacion
    sdbgCampos.RemoveAll
    If ssTabGrupos.selectedItem.Tag <> "" Then
        Set oNotificado = oFSGSRaiz.Generar_CNotificadoAccion
        With oNotificado
            .Id = lIdNotificado
            .Origen = iOrigenNotificado
            .CargarCamposVisibles CLng(ssTabGrupos.selectedItem.Tag)
            If Not .CamposVisibles Is Nothing Then
                sdbgCampos.Redraw = False
                
                If Me.optConfiguracionRol.Value = True Then
                    Dim oBloque As CBloque
                    Dim oBloqueDesg As CBloque
                    Set oBloque = oFSGSRaiz.Generar_CBloque
                    Set oBloqueDesg = oFSGSRaiz.Generar_CBloque
                    If ssTabGrupos.selectedItem.Tag <> "" Then
                        mlGrupo = ssTabGrupos.selectedItem.Tag
                        oBloque.Id = mlBloqueNotificacion
                        oBloque.CargarCumplimentacion CLng(mlRolNotificacion), CLng(ssTabGrupos.selectedItem.Tag)
                        If Not oBloque.Cumplimentaciones Is Nothing Then
                            sdbgCampos.Redraw = False
                            
                            Dim X As Integer
                            X = 1
                            For Each oCampoNotificado In .CamposVisibles
                                If bGuarda Then
                                    For Each oCampoCumplimentacion In oBloque.Cumplimentaciones
                                        'Recorro los campos visibles y por cada campo, busco ese campo en la cumplimentacion del rol para saber su visibilidad
                                        If oCampoCumplimentacion.IdCampo = oCampoNotificado.Campo Then
                                            Set oCumplimentacion = oCampoCumplimentacion
                                            Exit For
                                        End If
                                    Next
                                                                    
                                    GrabarCambioCampo oCampoNotificado.Campo, oCumplimentacion.Visible
                                    If oCampoNotificado.Desglose = True Then
                                        'Cargamos los campos del desglose para grabar su cumplimentacion, si en ese grupo algun campo fuera de desglose
                                        oBloqueDesg.CargarCumplimentacion mlRolNotificacion, mlGrupo, oCampoNotificado.Campo
                                        Set moCumplimentaciones = oBloqueDesg.Cumplimentaciones
                                        If Not moCumplimentaciones Is Nothing Then
                                            For Each oCumplimentacion In moCumplimentaciones
                                                If Not (oCampoNotificado.Campo = oCumplimentacion.IdCampo) Then
                                                    GrabarCambioCampo oCumplimentacion.IdCampo, oCumplimentacion.Visible
                                                End If
                                            Next
                                        End If
                                    End If
                                    X = X + 1
                                End If
                                
                                sdbgCampos.AddItem oCampoNotificado.Campo & Chr(m_lSeparador) & oCampoNotificado.DenominacionesCampo.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCampoNotificado.Visible & Chr(m_lSeparador) & oCampoNotificado.Desglose & Chr(m_lSeparador) & IIf(Me.optConfiguracionRol.Value = True And oCampoNotificado.Visible = False, 1, 0)
                            Next
                            
                            If bGuarda Then
                                Dim oGrupo As Variant
                                For Each oGrupo In gGrupos
                                    If oGrupo <> CLng(ssTabGrupos.selectedItem.Tag) Then
                                        'Hemos grabado previamente la cumplimentacion de los campos del grupo seleccionado, ahora grabamos los del resto de grupos
                                        oBloque.Id = mlBloqueNotificacion
                                        oBloque.CargarCumplimentacion CLng(mlRolNotificacion), oGrupo
                                        If Not oBloque.Cumplimentaciones Is Nothing Then
                                            With oNotificado
                                                .CargarCamposVisibles CLng(oGrupo)
                                                If Not .CamposVisibles Is Nothing Then
                                                    For Each oCampoNotificado In .CamposVisibles
                                                        For Each oCampoCumplimentacion In oBloque.Cumplimentaciones
                                                            'Recorro los campos visibles y por cada campo, busco ese campo en la cumplimentacion del rol para saber su visibilidad
                                                            If oCampoCumplimentacion.IdCampo = oCampoNotificado.Campo Then
                                                                Set oCumplimentacion = oCampoCumplimentacion
                                                                Exit For
                                                            End If
                                                        Next
                                                        GrabarCambioCampo oCampoNotificado.Campo, oCumplimentacion.Visible
                                                        'Si el campo es de desglose habra que grabar su cumplimentacion
                                                        If oCampoNotificado.Desglose = True Then
                                                            'Cargamos los campos del desglose para grabar su cumplimentacion
                                                            oBloque.CargarCumplimentacion mlRolNotificacion, mlGrupo, oCampoNotificado.Campo
                                                            Set moCumplimentaciones = oBloque.Cumplimentaciones
                                                            If Not moCumplimentaciones Is Nothing Then
                                                                For Each oCumplimentacion In moCumplimentaciones
                                                                    If Not (oCampoNotificado.Campo = oCumplimentacion.IdCampo) Then
                                                                        GrabarCambioCampo oCumplimentacion.IdCampo, oCumplimentacion.Visible
                                                                    End If
                                                                Next
                                                            End If
                                                        End If
                                                    Next
                                                End If
                                            End With
                                        End If
                                    End If
                                Next
                            End If
                            
                            sdbgCampos.Redraw = True
                            sdbgCampos.Columns("DATO").Style = ssStyleEdit
                        End If
                        
                    End If
                Else
                    For Each oCampoNotificado In .CamposVisibles
                        '''''''''sdbgCampos.AddItem oCampoNotificado.Campo & Chr(m_lSeparador) & oCampoNotificado.DenominacionesCampo.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCampoNotificado.Visible & Chr(m_lSeparador) & oCampoNotificado.Desglose
                        
                        
                        
                        
                        sdbgCampos.AddItem oCampoNotificado.Campo & Chr(m_lSeparador) & oCampoNotificado.DenominacionesCampo.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & IIf(oCampoNotificado.Visible, 1, 0) & Chr(m_lSeparador) & oCampoNotificado.Desglose
                        
                        
                        
                        
                        If bGuarda Then GrabarCambioCampo oCampoNotificado.Campo, oCampoNotificado.Visible
                    Next
                    sdbgCampos.Redraw = True
                    sdbgCampos.Columns("DATO").Style = ssStyleEdit
                End If

            End If
        End With
    End If
End Sub

Private Sub CargarDesglose()
    Dim oNotificado As CNotificadoAccion
    Dim oCampoNotificado As CNotificadoAccionCampo
    Dim oCumplimentacion As CPMConfCumplimentacion
    Dim oCampoCumplimentacion As CPMConfCumplimentacion
    sdbgCampos.RemoveAll
    ssTabGrupos.Visible = False
    sdbgCampos.Top = ssTabGrupos.Top
    sdbgCampos.Left = ssTabGrupos.Left
    sdbgCampos.Height = ssTabGrupos.Height
    sdbgCampos.Width = ssTabGrupos.Width
    sdbgCampos.Columns("DATO").Width = sdbgCampos.Width - 990
    Set oNotificado = oFSGSRaiz.Generar_CNotificadoAccion
    With oNotificado
        .Id = lIdNotificado
        .Origen = iOrigenNotificado
        .CargarCamposDesgloseVisibles lCampoDesglose
        If Not .CamposVisibles Is Nothing Then
            
            If Me.optConfiguracionRol.Value = True Then
                Dim oBloque As CBloque
                Set oBloque = oFSGSRaiz.Generar_CBloque
                If mlGrupo <> 0 Then
                    oBloque.Id = mlBloqueNotificacion
                    oBloque.CargarCumplimentacion CLng(mlRolNotificacion), mlGrupo, lCampoDesglose
                    If Not oBloque.Cumplimentaciones Is Nothing Then
                        sdbgCampos.Redraw = False
                        For Each oCampoNotificado In .CamposVisibles
                            For Each oCampoCumplimentacion In oBloque.Cumplimentaciones
                                    'Recorro los campos visibles y por cada campo, busco ese campo en la cumplimentacion del rol para saber su visibilidad
                                    If oCampoCumplimentacion.IdCampo = oCampoNotificado.Campo Then
                                        Set oCumplimentacion = oCampoCumplimentacion
                                        Exit For
                                    End If
                                Next
                            If oCumplimentacion.Campo.Vinculado = True Then
                                'Si hay campo vinculado se omite
                                GoTo SiguienteCampo
                            End If
                            sdbgCampos.AddItem oCampoNotificado.Campo & Chr(m_lSeparador) & oCampoNotificado.DenominacionesCampo.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCumplimentacion.Visible & Chr(m_lSeparador) & oCampoNotificado.Desglose & Chr(m_lSeparador) & IIf(Me.optConfiguracionRol.Value = True And oCumplimentacion.Visible = False, 1, 0)
                            GrabarCambioCampo oCampoNotificado.Campo, oCumplimentacion.Visible
SiguienteCampo:
                        Next
                        sdbgCampos.Redraw = True
                        sdbgCampos.Columns("DATO").Style = ssStyleEdit
                    End If
                End If
            Else
                For Each oCampoNotificado In .CamposVisibles
                    sdbgCampos.AddItem oCampoNotificado.Campo & Chr(m_lSeparador) & oCampoNotificado.DenominacionesCampo.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCampoNotificado.Visible & Chr(m_lSeparador) & oCampoNotificado.Desglose & Chr(m_lSeparador) & 0
                    GrabarCambioCampo oCampoNotificado.Campo, oCampoNotificado.Visible
                Next
                sdbgCampos.Redraw = True
                sdbgCampos.Columns("DATO").Style = ssStyleEdit
            End If
            
           
        End If
    End With
End Sub

Private Sub CargarCumplimentacionRol()
    Dim oBloque As CBloque
    Set oBloque = oFSGSRaiz.Generar_CBloque
    Dim oCumplimentacion As CPMConfCumplimentacion
    sdbgCampos.RemoveAll
    If ssTabGrupos.selectedItem.Tag <> "" Then
        mlGrupo = ssTabGrupos.selectedItem.Tag
        oBloque.Id = mlBloqueNotificacion
        oBloque.CargarCumplimentacion CLng(mlRolNotificacion), CLng(ssTabGrupos.selectedItem.Tag)
        If Not oBloque.Cumplimentaciones Is Nothing Then
            sdbgCampos.Redraw = False
            For Each oCumplimentacion In oBloque.Cumplimentaciones
                sdbgCampos.AddItem oCumplimentacion.IdCampo & Chr(m_lSeparador) & oCumplimentacion.Campo.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCumplimentacion.Visible & Chr(m_lSeparador) & IIf(oCumplimentacion.Campo.Tipo = TipoDesglose, True, False) & Chr(m_lSeparador) & IIf(Me.optConfiguracionRol.Value = True And oCumplimentacion.Visible = False, 1, 0)
                GrabarCambioCampo oCumplimentacion.IdCampo, oCumplimentacion.Visible
            Next
            sdbgCampos.Redraw = True
            sdbgCampos.Columns("DATO").Style = ssStyleEdit
        Else
            oMensajes.MensajeRolNoEstaEnEtapas
        End If
    End If
End Sub


''' <summary>
''' Cargar la configuraci�n de cumplimentaci�n para un rol, bloque y campo desglose
''' </summary>
''' <remarks></remarks>
''' <remarks></remarks>
Private Sub CargarCumplimentacionDesglose()
    Dim oBloque As CBloque
    Dim oCumplimentacion As CPMConfCumplimentacion
        
    sdbgCampos.RemoveAll
    If mlBloqueNotificacion > 0 Then
        Set oBloque = oFSGSRaiz.Generar_CBloque
        oBloque.Id = mlBloqueNotificacion

        If mlRolNotificacion > 0 And lCampoDesglose > 0 Then
            oBloque.CargarCumplimentacion mlRolNotificacion, mlGrupo, lCampoDesglose
            Set moCumplimentaciones = oBloque.Cumplimentaciones
            If Not moCumplimentaciones Is Nothing Then
            
                For Each oCumplimentacion In moCumplimentaciones
                    If Not (lCampoDesglose = oCumplimentacion.IdCampo) Then
                        sdbgCampos.AddItem oCumplimentacion.IdCampo & Chr(m_lSeparador) & oCumplimentacion.Campo.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCumplimentacion.Visible & Chr(m_lSeparador) & IIf(oCumplimentacion.Campo.Tipo = TipoDesglose, True, False) & Chr(m_lSeparador) & IIf(oCumplimentacion.Visible = False, 1, 0)
                        GrabarCambioCampo oCumplimentacion.IdCampo, oCumplimentacion.Visible
                    End If
                Next
            End If
        End If
    End If
End Sub


