VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPAROfeEst 
   Caption         =   "Estados de ofertas (Consulta)"
   ClientHeight    =   3945
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5940
   Icon            =   "frmPAROfeEst.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3945
   ScaleWidth      =   5940
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   5940
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   3390
      Width           =   5940
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4860
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "&Filtrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdCodigo 
         Caption         =   "&C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3480
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgOfeEst 
      Height          =   2875
      Left            =   60
      TabIndex        =   9
      Top             =   500
      Width           =   5820
      _Version        =   196617
      DataMode        =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPAROfeEst.frx":014A
      stylesets(1).Name=   "chkBloqueado"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPAROfeEst.frx":0166
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   1323
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777152
      Columns(1).Width=   4577
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   100
      Columns(2).Width=   1720
      Columns(2).Caption=   "Comparable"
      Columns(2).Name =   "COMP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   2
      Columns(3).Width=   1720
      Columns(3).Caption=   "Adjudicable"
      Columns(3).Name =   "ADJ"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      _ExtentX        =   10266
      _ExtentY        =   5071
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcIdi 
      Height          =   285
      Left            =   1320
      TabIndex        =   10
      Top             =   120
      Width           =   2895
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   6773
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "OFFSET"
      Columns(2).Name =   "OFFSET"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblIdiMat 
      Caption         =   "DIdioma"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   11
      Top             =   180
      Width           =   1155
   End
End
Attribute VB_Name = "frmPAROfeEst"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPAROfeEst
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit

''' Coleccion de OfeEstados
Public oOfeEstados As COfeEstados
''' OfeEstado en edicion
Private oOfeEstadoEnEdicion As COfeEstado
Private oIBAseDatosEnEdicion As IBaseDatos
''' Listado de OfeEstados
Public bOrdenListadoDen As Boolean
''' Propiedades de seguridad
Private bModif  As Boolean
''' Control de errores
Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean
''' Variables de control
Private bCambioGeneral As Boolean
Public Accion As accionessummit
Private bModoEdicion As Boolean
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
Private iDifBoomark As Integer
''' Posicion para UnboundReadData
Private p As Long
Private sCod As String
'Multilenguaje
Private sIdiTitulos(1 To 4) As String
Private sIdiEstadosCod As String
Private sIdiCodigo As String
Private sIdiConsulta As String
Private sIdiEdicion As String
Private sIdiLaOfeEstado As String
Private sIdiDenominacion As String
Private sIdiOrdenando As String
Private sIdiEliminacionMultiple As String
Private oIdiomas As CIdiomas

Private Sub ConfigurarSeguridad()
    ''' * Objetivo: Configurar el formulario segun los permisos
    ''' * Objetivo: del usuario
    bModif = True
    cmdModoEdicion.Visible = True
End Sub

Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 5770 Then   'de tama�o de la ventana
            Me.Width = 5970        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 2300 Then  'cuando no se maximiza ni
            Me.Height = 2500       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width

    
    If Height >= 1475 Then sdbgOfeEst.Height = Height - 1475
    If Width >= 250 Then sdbgOfeEst.Width = Width - 250
    
    sdbgOfeEst.Columns(0).Width = sdbgOfeEst.Width * 15 / 100
    'sdbgOfeEst.Columns(1).Width = sdbgOfeEst.Width * 35 / 100
    'sdbgOfeEst.Columns(2).Width = sdbgOfeEst.Width * 25 / 100
    'sdbgOfeEst.Columns(3).Width = sdbgOfeEst.Width * 25 / 100 - 570
    sdbgOfeEst.Columns(1).Width = sdbgOfeEst.Width * 40 / 100
    sdbgOfeEst.Columns(2).Width = sdbgOfeEst.Width * 20 / 100
    sdbgOfeEst.Columns(3).Width = sdbgOfeEst.Width * 20 / 100
    
    
    cmdModoEdicion.Left = sdbgOfeEst.Left + sdbgOfeEst.Width - cmdModoEdicion.Width
    
End Sub
Private Sub cmdA�adir_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgOfeEst.Scroll 0, sdbgOfeEst.Rows - sdbgOfeEst.Row
    
    If sdbgOfeEst.VisibleRows > 0 Then
        
        If sdbgOfeEst.VisibleRows > sdbgOfeEst.Rows Then
            sdbgOfeEst.Row = sdbgOfeEst.Rows
        Else
            sdbgOfeEst.Row = sdbgOfeEst.Rows - (sdbgOfeEst.Rows - sdbgOfeEst.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgOfeEst.SetFocus
End Sub
Private Sub cmdFiltrar_Click()

    ''' * Objetivo: Activar el formulario de filtrar.
    
    frmPAROfeEstFiltrar.WindowState = vbNormal
    frmPAROfeEstFiltrar.Show 1
    If frmPAROfeEst.caption = "" Then frmPAROfeEst.caption = sIdiTitulos(3)
End Sub
Private Sub cmdCodigo_Click()
    
    ''' * Objetivo: Cambiar de codigo la OfeEstado actual
    
    Dim teserror As TipoErrorSummit
    
    ''' Resaltar la OfeEstado actual
    
    If sdbgOfeEst.Rows = 0 Then Exit Sub
    
    sdbgOfeEst.SelBookmarks.Add sdbgOfeEst.Bookmark
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    
    Set oOfeEstadoEnEdicion = oOfeEstados.Item(CStr(sdbgOfeEst.Bookmark))
    Set oIBAseDatosEnEdicion = oOfeEstadoEnEdicion
   
    
    teserror = oIBAseDatosEnEdicion.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgOfeEst.SetFocus
        Exit Sub
    End If
        
    oIBAseDatosEnEdicion.CancelarEdicion
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
'    frmMODCOD.Caption = "Estados de ofertas (C�digo)"
    frmMODCOD.caption = sIdiEstadosCod
    frmMODCOD.Left = frmPAROfeEst.Left + 500
    frmMODCOD.Top = frmPAROfeEst.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodOFEEST
    frmMODCOD.txtCodAct.Text = oOfeEstadoEnEdicion.Cod
    Set frmMODCOD.fOrigen = frmPAROfeEst
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
'        oMensajes.NoValido "Codigo"
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set oOfeEstadoEnEdicion = Nothing
        Exit Sub
    End If
        
    If UCase(g_sCodigoNuevo) = UCase(oOfeEstadoEnEdicion.Cod) Then
'        oMensajes.NoValido "Codigo"
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set oOfeEstadoEnEdicion = Nothing
        Exit Sub
    End If
            
    bCambioGeneral = False
    
    If UCase(oOfeEstadoEnEdicion.Cod) = UCase(gParametrosGenerales.gsESTINI) Then
        bCambioGeneral = True
    End If
    
    ''' Cambiar el codigo
    
    
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.CambiarCodigo(g_sCodigoNuevo)
    Screen.MousePointer = vbNormal
             
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    ''' Actualizar los datos
    
    If bCambioGeneral Then
          gParametrosGenerales.gsESTINI = g_sCodigoNuevo
    End If
    
    sdbgOfeEst.MoveFirst
    sdbgOfeEst.Refresh
    sdbgOfeEst.Bookmark = sdbgOfeEst.SelBookmarks(0)
    sdbgOfeEst.SelBookmarks.RemoveAll
        
    Set oIBAseDatosEnEdicion = Nothing
    Set oOfeEstadoEnEdicion = Nothing
    
End Sub
Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en la OfeEstado actual
    
    sdbgOfeEst.CancelUpdate
    sdbgOfeEst.DataChanged = False
    
    If Not oOfeEstadoEnEdicion Is Nothing Then
        oIBAseDatosEnEdicion.CancelarEdicion
        Set oIBAseDatosEnEdicion = Nothing
        Set oOfeEstadoEnEdicion = Nothing
    End If
    
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    cmdCodigo.Enabled = True
    
    Accion = ACCOfeEstCon
        
End Sub
Private Sub cmdEliminar_Click()
'*********************************************************************
'*** Descripci�n: Elimina los estados de ofertas seleccionados.    ***
'***              En caso de no poder informa de los estados de    ***
'***              ofertas que no pudieron ser eliminados.          ***
'*** Par�metros:         ----------                                ***
'*** Valor que devuelve: ----------                                ***
'*********************************************************************
Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit

    If sdbgOfeEst.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    iDifBoomark = 0  'Vable. del form para eliminar las filas en el UnboundDeleteRow
    
    Select Case sdbgOfeEst.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
            irespuesta = oMensajes.PreguntaEliminar(sIdiLaOfeEstado & " " & sdbgOfeEst.Columns(0).Value & " (" & sdbgOfeEst.Columns(1).Value & ")")
        Case Is > 1
            irespuesta = oMensajes.PreguntaEliminarOfeEst(sIdiEliminacionMultiple)
    End Select
    
    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    ReDim aIdentificadores(sdbgOfeEst.SelBookmarks.Count)
    ReDim aBookmarks(sdbgOfeEst.SelBookmarks.Count)
    
    i = 0
    While i < sdbgOfeEst.SelBookmarks.Count
        aIdentificadores(i + 1) = oOfeEstados.Item(CStr(sdbgOfeEst.SelBookmarks(i))).Cod
        aBookmarks(i + 1) = sdbgOfeEst.SelBookmarks(i)
        sdbgOfeEst.Bookmark = sdbgOfeEst.SelBookmarks(i)
        i = i + 1
    Wend
    udtTeserror = oOfeEstados.EliminarOfeEstadosDeBaseDatos(aIdentificadores)
    If udtTeserror.NumError <> TESnoerror Then
      If udtTeserror.NumError = TESImposibleEliminar Then
          iIndice = 1
          aAux = udtTeserror.Arg1
          inum = UBound(udtTeserror.Arg1, 2)

          For i = 0 To inum
              udtTeserror.Arg1(2, i) = oOfeEstados.Item(CStr(sdbgOfeEst.SelBookmarks(aAux(2, i) - iIndice))).Den
              sdbgOfeEst.SelBookmarks.Remove (aAux(2, i) - iIndice)
              iIndice = iIndice + 1
          Next i
          'Mensaje de cuales no se pudieron eliminar y porque
          oMensajes.ImposibleEliminacionMultiple 308, udtTeserror.Arg1
          If sdbgOfeEst.SelBookmarks.Count > 0 Then
                sdbgOfeEst.DeleteSelected
                DoEvents
                sdbgOfeEst.ReBind
          End If
      Else
          TratarError udtTeserror
          Screen.MousePointer = vbNormal
          Exit Sub
      End If
    Else
        sdbgOfeEst.DeleteSelected
        DoEvents
        sdbgOfeEst.ReBind
    End If
        
    sdbgOfeEst.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdListado_Click()
    AbrirLstParametros "frmPAROfeEst", bOrdenListadoDen
End Sub

Private Sub cmdModoEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
                
        'cmdRestaurar_Click
        
        sdbgOfeEst.AllowAddNew = True
        sdbgOfeEst.AllowUpdate = True
        sdbgOfeEst.AllowDelete = False
        
'        frmPAROfeEst.Caption = "Estados de ofertas (Edici�n)"
        frmPAROfeEst.caption = sIdiTitulos(4)
        
'        cmdModoEdicion.Caption = "&Consulta"
        cmdModoEdicion.caption = sIdiConsulta
        
        cmdFiltrar.Visible = False
        cmdRestaurar.Visible = False
        cmdListado.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
            
        bModoEdicion = True
        
        Accion = ACCOfeEstCon
        
        If oUsuarioSummit.Tipo = TipoDeUsuario.Administrador Then
            cmdCodigo.Visible = True
        Else
            cmdCodigo.Visible = False
        End If
            
    
    Else
                
        If sdbgOfeEst.DataChanged = True Then
        
            v = sdbgOfeEst.ActiveCell.Value
            If Me.Visible Then sdbgOfeEst.SetFocus
            If (v <> "") Then
                sdbgOfeEst.ActiveCell.Value = v
            End If
                        
            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        End If
        
        sdbgOfeEst.AllowAddNew = False
        sdbgOfeEst.AllowUpdate = False
        sdbgOfeEst.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdCodigo.Visible = False
        cmdFiltrar.Visible = True
        cmdRestaurar.Visible = True
        cmdListado.Visible = True
        
        If oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador Then
            cmdCodigo.Visible = False
        End If
    
'        frmPAROfeEst.Caption = "Estados de ofertas (Consulta)"
        frmPAROfeEst.caption = sIdiTitulos(3)
        
'        cmdModoEdicion.Caption = "&Edici�n"
        cmdModoEdicion.caption = sIdiEdicion
        
        cmdRestaurar_Click
        
        bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgOfeEst.SetFocus
    
End Sub

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgOfeEst.DataChanged = True Then
        bValError = False
        bAnyaError = False
        bModError = False
        If sdbgOfeEst.Row = 0 Then
            sdbgOfeEst.MoveNext
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgOfeEst.MovePrevious
            End If
        Else
            sdbgOfeEst.MovePrevious
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgOfeEst.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function

Private Sub cmdRestaurar_Click()

    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    
'    Me.Caption = "Estados de ofertas (Consulta)"
    Screen.MousePointer = vbHourglass
    
    Me.caption = sIdiTitulos(3)
    
    Set oOfeEstados = Nothing
    
    Set oOfeEstados = oFSGSRaiz.generar_COfeEstados
   
    oOfeEstados.CargarTodosLosOfeEstados , , , , , , True, sdbcIdi.Columns(1).Value
    
    sdbgOfeEst.ReBind
    
    Screen.MousePointer = vbNormal
    
    If Me.Visible Then sdbgOfeEst.SetFocus
    
End Sub
Private Sub Form_Load()

    ''' * Objetivo: Cargar las OfeEstados e iniciar
    ''' * Objetivo: el formulario
    Dim oIdioma As CIdioma
    
    Me.Width = 6060
    Me.Height = 4350
    
    CargarRecursos
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    Show
    
    'Carga el combo de los idiomas
    Set oIdiomas = oGestorParametros.DevolverIdiomas

    For Each oIdioma In oIdiomas
        sdbcIdi.AddItem oIdioma.Den & Chr(9) & oIdioma.Cod & Chr(9) & oIdioma.OFFSET
    Next
    sdbcIdi.Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdi.Columns(0).Value = sdbcIdi.Text
    sdbcIdi.Columns(1).Value = basPublic.gParametrosInstalacion.gIdioma
    
    'Carga los estados de las ofertas
    Set oOfeEstados = oFSGSRaiz.generar_COfeEstados
    
    Screen.MousePointer = vbHourglass
    oOfeEstados.CargarTodosLosOfeEstados , , , , , , True, basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    sdbgOfeEst.ReBind
    
    bModoEdicion = False
    
    Accion = ACCOfeEstCon
        
    bOrdenListadoDen = False
    
    ConfigurarSeguridad
    
End Sub
Private Sub Form_Resize()
    Arrange
End Sub

''' * Objetivo: Descargar el formulario si no
''' * Objetivo: hay cambios pendientes
Private Sub Form_Unload(Cancel As Integer)
    Dim v As Variant
    
    If sdbgOfeEst.DataChanged Then
        v = sdbgOfeEst.ActiveCell.Value
        If Me.Visible Then sdbgOfeEst.SetFocus
        sdbgOfeEst.ActiveCell.Value = v
        
        If actualizarYSalir() Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    Set oOfeEstados = Nothing
    Set oOfeEstadoEnEdicion = Nothing
    Set oIBAseDatosEnEdicion = Nothing
    Me.Visible = False
End Sub

Private Sub sdbcIdi_CloseUp()
    ''' * Objetivo: Cargar el grid con los estados de las ofertas
    '''para el idioma seleccionado
    
    sdbcIdi.Text = sdbcIdi.Columns(0).Value
    
    'Carga los estados de las ofertas en la colecci�n
    CargarEstadoOfertas
    sdbgOfeEst.ReBind
End Sub

Private Sub sdbcIdi_DropDown()
    Dim oIdioma As CIdioma
    
    Set oIdiomas = Nothing
    Set oIdiomas = oGestorParametros.DevolverIdiomas
    sdbcIdi.RemoveAll
    
    'Carga los combos con los idiomas
    For Each oIdioma In oIdiomas
        sdbcIdi.AddItem oIdioma.Den & Chr(9) & oIdioma.Cod & Chr(9) & oIdioma.OFFSET
    Next

End Sub

Private Sub sdbcIdi_InitColumnProps()
    sdbcIdi.DataFieldList = "Column 0"
    sdbcIdi.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcIdi_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcIdi.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcIdi.Rows - 1
            bm = sdbcIdi.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcIdi.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcIdi.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgOfeEst_AfterDelete(RtnDispErrMsg As Integer)

    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    RtnDispErrMsg = 0
    If (sdbgOfeEst.Rows = 0) Then
        Exit Sub
    End If
    If Me.Visible Then sdbgOfeEst.SetFocus
    sdbgOfeEst.Bookmark = sdbgOfeEst.RowBookmark(sdbgOfeEst.Row)
    
End Sub
Private Sub sdbgOfeEst_AfterInsert(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgOfeEst_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgOfeEst_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub
Private Sub sdbgOfeEst_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    
    bValError = False
    Cancel = False
    
    If Trim(sdbgOfeEst.Columns(0).Value) = "" Then
'        oMensajes.NoValido "Codigo"
        oMensajes.NoValido sIdiCodigo
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgOfeEst.Columns(1).Value) = "" Then
'        oMensajes.NoValida "Denominacion"
        oMensajes.NoValida sIdiDenominacion
        Cancel = True
        GoTo Salir
    End If
    
    'If Not sdbgOfeEst.Columns(2).Value And sdbgOfeEst.Columns(3).Value Then
    If Not sdbgOfeEst.Columns(2).Value = True And sdbgOfeEst.Columns(3).Value = True Then
'        oMensajes.NoValida "Denominacion"
        oMensajes.NoComparableYAdjudicable
        Cancel = True
        GoTo Salir
    End If
    
Salir:
        
    bValError = Cancel
    If Me.Visible Then sdbgOfeEst.SetFocus
        
End Sub
Private Sub sdbgOfeEst_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If sdbgOfeEst.col = 2 Then
        'If Not sdbgOfeEst.Columns(2).Value And sdbgOfeEst.Columns(3).Value Then
        If Not sdbgOfeEst.Columns(2).Value = True And sdbgOfeEst.Columns(3).Value = True Then
            sdbgOfeEst.Columns(2).Value = True
        End If
    ElseIf sdbgOfeEst.col = 3 Then
        'If Not sdbgOfeEst.Columns(2).Value And sdbgOfeEst.Columns(3).Value Then
        If Not sdbgOfeEst.Columns(2).Value = True And sdbgOfeEst.Columns(3).Value = True Then
            sdbgOfeEst.Columns(3).Value = False
        End If
    End If
        
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
        cmdCodigo.Enabled = False
    
    End If
    
    If Accion = ACCOfeEstCon And Not sdbgOfeEst.IsAddRow Then
    
        Set oOfeEstadoEnEdicion = Nothing
        Set oOfeEstadoEnEdicion = oOfeEstados.Item(CStr(sdbgOfeEst.Bookmark))
      
        Set oIBAseDatosEnEdicion = oOfeEstadoEnEdicion
        
        teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            
            TratarError teserror
            sdbgOfeEst.DataChanged = False
            sdbgOfeEst.Columns(0).Value = oOfeEstadoEnEdicion.Cod
            sdbgOfeEst.Columns(1).Value = oOfeEstadoEnEdicion.Den
            teserror.NumError = TESnoerror
            
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgOfeEst.SetFocus
        Else
            Accion = ACCOfeEstMod
        End If
       
    
    End If
    
End Sub
Private Sub sdbgOfeEst_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    
    Dim sCodigo As String
    Dim sDenominacion As String
    Dim lCodigo As Integer
    Dim lDenominacion As Integer
    Dim sHeadCaption As String
    
    If bModoEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgOfeEst.Columns(ColIndex).caption

    sdbgOfeEst.Columns(ColIndex).caption = sIdiOrdenando
    
    ''' Volvemos a cargar las OfeEstados, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    
    Set oOfeEstados = Nothing
    Set oOfeEstados = oFSGSRaiz.generar_COfeEstados
  
    
    If caption <> sIdiTitulos(3) Then
    
'        If InStr(1, Caption, "C�digo", vbTextCompare) <> 0 Then
        If InStr(1, caption, sIdiTitulos(1), vbTextCompare) <> 0 Then
        
            ''' Datos filtrados por codigo
            
            If InStr(1, caption, "*", vbTextCompare) <> 0 Then
                
                lCodigo = Len(caption) - Len(sIdiTitulos(1))
                sCodigo = Mid(caption, Len(sIdiTitulos(1)) + 1, lCodigo - 1)
'                sCodigo = Right(Caption, Len(Caption) - 38)
'                sCodigo = Left(sCodigo, Len(sCodigo) - 2)
            
                Select Case ColIndex
                Case 0
                    oOfeEstados.CargarTodosLosOfeEstados Trim(sCodigo), , , , , , True, sdbcIdi.Columns(1).Value
                Case 1
                    oOfeEstados.CargarTodosLosOfeEstados Trim(sCodigo), , , True, , , True, sdbcIdi.Columns(1).Value
                Case 2
                    oOfeEstados.CargarTodosLosOfeEstados Trim(sCodigo), , , , True, , True, sdbcIdi.Columns(1).Value
                Case 3
                    oOfeEstados.CargarTodosLosOfeEstados Trim(sCodigo), , , , , True, True, sdbcIdi.Columns(1).Value
                End Select
                
            Else
            
                lCodigo = Len(caption) - Len(sIdiTitulos(1))
                sCodigo = Mid(caption, Len(sIdiTitulos(1)) + 1, lCodigo)
'               sCodigo = Right(Caption, Len(Caption) - 38)
'               sCodigo = Left(sCodigo, Len(sCodigo) - 1)
            
                Select Case ColIndex
                Case 0
                    oOfeEstados.CargarTodosLosOfeEstados Trim(sCodigo), , True, , , , True, sdbcIdi.Columns(1).Value
                Case 1
                    oOfeEstados.CargarTodosLosOfeEstados Trim(sCodigo), , True, True, , , True, sdbcIdi.Columns(1).Value
                Case 2
                    oOfeEstados.CargarTodosLosOfeEstados Trim(sCodigo), , True, , True, , True, sdbcIdi.Columns(1).Value
                Case 3
                    oOfeEstados.CargarTodosLosOfeEstados Trim(sCodigo), , True, , , True, True, sdbcIdi.Columns(1).Value
                End Select
                
           End If
           
        Else
            
            ''' Datos filtrados por denominacion
            
            lDenominacion = Len(caption) - Len(sIdiTitulos(2))
            sDenominacion = Mid(caption, Len(sIdiTitulos(2)) + 1, lDenominacion)
'           sDenominacion = Right(Caption, Len(Caption) - 44)
            
            If InStr(1, sDenominacion, "*", vbTextCompare) <> 0 Then
            
                sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
'                sDenominacion = Left(sDenominacion, Len(sDenominacion) - 2)
                
                Select Case ColIndex
                Case 0
                    oOfeEstados.CargarTodosLosOfeEstados , Trim(sDenominacion), , , , , True, sdbcIdi.Columns(1).Value
                Case 1
                    oOfeEstados.CargarTodosLosOfeEstados , Trim(sDenominacion), , True, , , True, sdbcIdi.Columns(1).Value
                Case 2
                    oOfeEstados.CargarTodosLosOfeEstados , Trim(sDenominacion), , , True, , True, sdbcIdi.Columns(1).Value
                Case 3
                    oOfeEstados.CargarTodosLosOfeEstados , Trim(sDenominacion), , , , True, True, sdbcIdi.Columns(1).Value
                    
                End Select
                            
            Else
                
                sDenominacion = Left(sDenominacion, Len(sDenominacion))
'                sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
                
                Select Case ColIndex
                Case 0
                    oOfeEstados.CargarTodosLosOfeEstados , Trim(sDenominacion), True, , True, , , sdbcIdi.Columns(1).Value
                Case 1
                    oOfeEstados.CargarTodosLosOfeEstados , Trim(sDenominacion), True, True, True, , , sdbcIdi.Columns(1).Value
                Case 2
                    oOfeEstados.CargarTodosLosOfeEstados , Trim(sDenominacion), True, , True, , True, sdbcIdi.Columns(1).Value
                Case 3
                    oOfeEstados.CargarTodosLosOfeEstados , Trim(sDenominacion), True, , True, True, , sdbcIdi.Columns(1).Value
                End Select
                    
            End If
                
        End If
                
    Else
    
        ''' Datos no filtrados
        
        Select Case ColIndex
        Case 0
            oOfeEstados.CargarTodosLosOfeEstados , , , , , , True, sdbcIdi.Columns(1).Value
        Case 1
            oOfeEstados.CargarTodosLosOfeEstados , , , True, , , True, sdbcIdi.Columns(1).Value
        Case 2
            oOfeEstados.CargarTodosLosOfeEstados , , , , True, , True, sdbcIdi.Columns(1).Value
        Case 3
            oOfeEstados.CargarTodosLosOfeEstados , , , , , True, True, sdbcIdi.Columns(1).Value
        End Select
    
    End If
        
    Select Case ColIndex
        Case 0
            bOrdenListadoDen = False
        Case 1
            bOrdenListadoDen = True
        Case 2, 3
            bOrdenListadoDen = False
    End Select
            
    sdbgOfeEst.ReBind
    sdbgOfeEst.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgOfeEst_InitColumnProps()
    
    sdbgOfeEst.Columns(0).FieldLen = basParametros.gLongitudesDeCodigos.giLongCodOFEEST
    
End Sub

Private Sub sdbgOfeEst_KeyDown(KeyCode As Integer, Shift As Integer)
'*********************************************************************
'*** Descripci�n: Captura la tecla Supr (Delete) y cuando          ***
'***              �sta es presionada se lanza el evento            ***
'***              Click asociado al bot�n eliminar para            ***
'***              borrar de la BD y de la grid las lineas          ***
'***              seleccionadas.                                   ***
'*** Par�metros:  KeyCode ::> Contiene el c�digo interno           ***
'***                          de la tecla pulsada.                 ***
'***              Shift   ::> Es la m�scara, sin uso en esta       ***
'***                          subrutina.                           ***
'*** Valor que devuelve: ----------                                ***
'*********************************************************************
    If KeyCode = vbKeyDelete Then
        If bModoEdicion And sdbgOfeEst.SelBookmarks.Count > 0 Then
            cmdEliminar_Click
        End If
    End If
End Sub

Private Sub sdbgOfeEst_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgOfeEst.DataChanged = False Then
            
            sdbgOfeEst.CancelUpdate
            sdbgOfeEst.DataChanged = False
            
            If Not oOfeEstadoEnEdicion Is Nothing Then
                oIBAseDatosEnEdicion.CancelarEdicion
                Set oIBAseDatosEnEdicion = Nothing
                Set oOfeEstadoEnEdicion = Nothing
            End If
           
            If Not sdbgOfeEst.IsAddRow Then
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
            Else
                cmdA�adir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = False
            End If
            
            Accion = ACCOfeEstCon
            
        Else
        
            If sdbgOfeEst.IsAddRow Then
           
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
           
                Accion = ACCOfeEstCon
            
            End If
            
        End If
        
    End If
    
End Sub

Private Sub sdbgOfeEst_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgOfeEst.IsAddRow Then
        sdbgOfeEst.Columns(0).Locked = True
        If sdbgOfeEst.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True
        End If
    Else
        If sdbgOfeEst.DataChanged = True Then cmdDeshacer.Enabled = True
        sdbgOfeEst.Columns(0).Locked = False
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgOfeEst.Row) Then
                sdbgOfeEst.col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdCodigo.Enabled = False
        
    End If
        
End Sub

Private Sub sdbgOfeEst_UnboundAddData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, NewRowBookmark As Variant)

    ''' * Objetivo: Hemos anyadido una fila al grid,
    ''' * Objetivo: ahora hay que anyadirla a la
    ''' * Objetivo: coleccion
    ''' * Recibe: Buffer con los datos a anyadir y bookmark de la fila
        
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    Dim bComp As Boolean
    Dim bAdj As Boolean
    
    bAnyaError = False
    
    ''' Anyadir a la coleccion
    
    If sdbgOfeEst.Columns(2).Value = "" Then
        bComp = False
    Else
        bComp = True
    End If
    
    If sdbgOfeEst.Columns(3).Value = "" Then
        bAdj = False
    Else
        bAdj = True
    End If
    
    oOfeEstados.Add sdbgOfeEst.Columns(0).Value, sdbcIdi.Columns(1).Value, sdbgOfeEst.Columns(1).Value, bComp, bAdj, oOfeEstados.Count
    
    ''' Anyadir a la base de datos
    
    Set oOfeEstadoEnEdicion = oOfeEstados.Item(CStr(oOfeEstados.Count - 1))
   
    
    Set oIBAseDatosEnEdicion = oOfeEstadoEnEdicion
    
    teserror = oIBAseDatosEnEdicion.AnyadirABaseDatos
    
    If teserror.NumError <> TESnoerror Then
        
        v = sdbgOfeEst.ActiveCell.Value
        oOfeEstados.Remove (CStr(oOfeEstados.Count - 1))
        TratarError teserror
        If Me.Visible Then sdbgOfeEst.SetFocus
        bAnyaError = True
        RowBuf.RowCount = 0
        sdbgOfeEst.ActiveCell.Value = v
        
    Else
        ''' Registro de acciones
        If gParametrosGenerales.gbActivLog Then
            oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCOfeEstAnya, "Cod:" & oOfeEstadoEnEdicion.Cod
        End If
        Accion = ACCOfeEstCon
    End If
    
    Set oIBAseDatosEnEdicion = Nothing
    Set oOfeEstadoEnEdicion = Nothing
        
End Sub
Private Sub sdbgOfeEst_UnboundDeleteRow(vBookmark As Variant)
    ''' * Objetivo: Hemos eliminado una fila al grid,
    ''' * Objetivo: ahora hay que eliminarla de la
    ''' * Objetivo: coleccion
    ''' * Recibe: bookmark de la fila (vBookmark)
    Dim teserror As TipoErrorSummit
    Dim IndFor As Long, IndDest As Long
                
    ' Registro de acciones
    IndDest = val(vBookmark) - iDifBoomark
    iDifBoomark = iDifBoomark + 1
    If gParametrosGenerales.gbActivLog Then
        oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCOfeEstEli, sCod & oOfeEstados.Item(CStr(IndDest)).Cod
    End If
    ' Eliminar de la coleccion
    For IndFor = IndDest To oOfeEstados.Count - 2
        oOfeEstados.Remove (CStr(IndFor))
        Set oOfeEstadoEnEdicion = oOfeEstados.Item(CStr(IndFor + 1))
        oOfeEstados.Add oOfeEstadoEnEdicion.Cod, oOfeEstadoEnEdicion.idioma, oOfeEstadoEnEdicion.Den, oOfeEstadoEnEdicion.Comparable, oOfeEstadoEnEdicion.adjudicable, IndFor
        Set oOfeEstadoEnEdicion = Nothing
    Next IndFor
    oOfeEstados.Remove (CStr(IndFor))
    Accion = ACCOfeEstCon
End Sub
Private Sub sdbgOfeEst_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim i As Integer
    Dim j As Integer
    
    Dim oOfeEst As COfeEstado
    
    Dim iNumOfeEstados As Integer

    If oOfeEstados Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If

    iNumOfeEstados = oOfeEstados.Count
    
    If IsNull(StartLocation) Then       'If the grid is empty then
        If ReadPriorRows Then               'If moving backwards through grid then
            p = iNumOfeEstados - 1                             'pointer = # of last grid row
        Else                                        'else
            p = 0                                       'pointer = # of first grid row
        End If
    Else                                        'If the grid already has data in it then
        p = StartLocation                       'pointer = location just before or after the row where data will be added
    
        If ReadPriorRows Then               'If moving backwards through grid then
                p = p - 1                               'move pointer back one row
        Else                                        'else
                p = p + 1                               'move pointer ahead one row
        End If
    End If
    
    'The pointer (p) now points to the row of the grid where you will start adding data.
    
    For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
        
        If p < 0 Or p > iNumOfeEstados - 1 Then Exit For           'If the pointer is outside the grid then stop this
    
        Set oOfeEst = Nothing
        Set oOfeEst = oOfeEstados.Item(CStr(p))
    
        For j = 0 To 3
      
            Select Case j
                    
                    Case 0:
                            RowBuf.Value(i, 0) = oOfeEst.Cod
                    Case 1:
                            RowBuf.Value(i, 1) = oOfeEst.Den
                    Case 2:
                            RowBuf.Value(i, 2) = oOfeEst.Comparable
                    Case 3:
                            RowBuf.Value(i, 3) = oOfeEst.adjudicable
            End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
        Next j
    
        RowBuf.Bookmark(i) = p                              'set the value of the bookmark for the current row in the rowbuffer
    
        If ReadPriorRows Then                               'move the pointer forward or backward, depending
            p = p - 1                                           'on which way it's supposed to move
        Else
            p = p + 1
        End If
            r = r + 1                                               'increment the number of rows read
        Next i
    
    RowBuf.RowCount = r                                         'set the size of the row buffer to the number of rows read

    Set oOfeEst = Nothing
    
End Sub
Private Sub sdbgOfeEst_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim v As Variant
    
    bModError = False
    
    ''' Modificamos en la base de datos
    
    oOfeEstadoEnEdicion.Cod = sdbgOfeEst.Columns(0).Value
    oOfeEstadoEnEdicion.Den = sdbgOfeEst.Columns(1).Value
    oOfeEstadoEnEdicion.Comparable = Int(val(sdbgOfeEst.Columns(2).Value))
    oOfeEstadoEnEdicion.adjudicable = Int(val(sdbgOfeEst.Columns(3).Value))
    
    
    teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
       
    If teserror.NumError <> TESnoerror Then
        v = sdbgOfeEst.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgOfeEst.SetFocus
        bModError = True
        RowBuf.RowCount = 0
        sdbgOfeEst.ActiveCell.Value = v
    Else
        ''' Registro de acciones
        
        If gParametrosGenerales.gbActivLog Then
            oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCOfeEstAnya, sCod & oOfeEstadoEnEdicion.Cod
        End If
        
        Accion = ACCOfeEstCon
        
        Set oIBAseDatosEnEdicion = Nothing
        Set oOfeEstadoEnEdicion = Nothing
    End If
End Sub




Public Sub ponerCaption(Texto, indice, comodin)
    
caption = sIdiTitulos(indice) & Texto & IIf(comodin, "*", "")
  

End Sub

    
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PAROFE_EST, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    sIdiTitulos(1) = Ador(0).Value & " "
    Ador.MoveNext
    sIdiTitulos(2) = Ador(0).Value & " "
    Ador.MoveNext
    sIdiTitulos(3) = Ador(0).Value
    caption = sIdiTitulos(3)
    Ador.MoveNext
    sIdiTitulos(4) = Ador(0).Value
    Ador.MoveNext
    sdbgOfeEst.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbgOfeEst.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    sdbgOfeEst.Columns(2).caption = Ador(0).Value
    Ador.MoveNext
    sdbgOfeEst.Columns(3).caption = Ador(0).Value
    Ador.MoveNext
    
    cmdA�adir.caption = Ador(0).Value
    Ador.MoveNext
    cmdCodigo.caption = Ador(0).Value
    sCod = cmdCodigo.caption & ":"
    Ador.MoveNext
    cmdDeshacer.caption = Ador(0).Value
    Ador.MoveNext
    cmdEliminar.caption = Ador(0).Value
    Ador.MoveNext
    cmdFiltrar.caption = Ador(0).Value
    Ador.MoveNext
    cmdListado.caption = Ador(0).Value
    Ador.MoveNext
    cmdModoEdicion.caption = Ador(0).Value
    Ador.MoveNext
    cmdRestaurar.caption = Ador(0).Value
    Ador.MoveNext
    
    
    sIdiEstadosCod = Ador(0).Value
    Ador.MoveNext
    sIdiCodigo = Ador(0).Value
    Ador.MoveNext
    sIdiConsulta = Ador(0).Value
    Ador.MoveNext
    sIdiEdicion = Ador(0).Value
    Ador.MoveNext
    sIdiLaOfeEstado = Ador(0).Value
    Ador.MoveNext
    sIdiDenominacion = Ador(0).Value
    Ador.MoveNext
    sIdiOrdenando = Ador(0).Value
    
    Ador.MoveNext
    lblIdiMat.caption = Ador(0).Value & ":"
    Ador.MoveNext
    sIdiEliminacionMultiple = Ador(0).Value '"m�ltiples estados de ofertas."
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub


Private Sub CargarEstadoOfertas()
    ''' Objetivo : Cargar los estados de las ofertas seg�n el idioma seleccionado
    Set oOfeEstados = Nothing
    Set oOfeEstados = oFSGSRaiz.generar_COfeEstados
    
    Screen.MousePointer = vbHourglass
    
    oOfeEstados.CargarTodosLosOfeEstados , , , , , , True, sdbcIdi.Columns(1).Value
    
    Screen.MousePointer = vbNormal
End Sub
