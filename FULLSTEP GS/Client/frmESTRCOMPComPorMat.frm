VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmESTRCOMPComPorMat 
   Caption         =   "Compradores por material"
   ClientHeight    =   4680
   ClientLeft      =   930
   ClientTop       =   2625
   ClientWidth     =   7395
   Icon            =   "frmESTRCOMPComPorMat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4680
   ScaleWidth      =   7395
   Begin TabDlg.SSTab stabComp 
      Height          =   4515
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   7964
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selección de material"
      TabPicture(0)   =   "frmESTRCOMPComPorMat.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblGMN4_4"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblGMN1_4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblGMN2_4"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblGMN3_4"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "sdbcGMN1_4Den"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "sdbcGMN4_4Den"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "sdbcGMN3_4Den"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "sdbcGMN2_4Den"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "sdbcGMN4_4Cod"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "sdbcGMN3_4Cod"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "sdbcGMN2_4Cod"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "sdbcGMN1_4Cod"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "cmdSelMat"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).ControlCount=   13
      TabCaption(1)   =   "Compradores"
      TabPicture(1)   =   "frmESTRCOMPComPorMat.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picNavigate"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "picEdit"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "tvwCompSel"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "tvwEstrComp"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "picCheck"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      Begin VB.PictureBox picCheck 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   210
         Left            =   -74820
         Picture         =   "frmESTRCOMPComPorMat.frx":0182
         ScaleHeight     =   210
         ScaleWidth      =   195
         TabIndex        =   23
         Top             =   480
         Visible         =   0   'False
         Width           =   200
      End
      Begin MSComctlLib.TreeView tvwEstrComp 
         Height          =   3525
         Left            =   -74880
         TabIndex        =   10
         Top             =   420
         Width           =   6990
         _ExtentX        =   12330
         _ExtentY        =   6218
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdSelMat 
         Height          =   315
         Left            =   5760
         Picture         =   "frmESTRCOMPComPorMat.frx":037C
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   900
         Width           =   345
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   1530
         TabIndex        =   2
         Top             =   1350
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2408
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5900
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
         Height          =   285
         Left            =   1530
         TabIndex        =   4
         Top             =   1815
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2408
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5900
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
         Height          =   285
         Left            =   1530
         TabIndex        =   6
         Top             =   2265
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2408
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5900
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
         Height          =   285
         Left            =   1530
         TabIndex        =   8
         Top             =   2730
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2408
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5900
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
         Height          =   285
         Left            =   2610
         TabIndex        =   5
         Top             =   1815
         Width           =   3525
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5900
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2408
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6218
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
         Height          =   285
         Left            =   2610
         TabIndex        =   7
         Top             =   2265
         Width           =   3525
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5900
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2408
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6218
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
         Height          =   285
         Left            =   2610
         TabIndex        =   9
         Top             =   2730
         Width           =   3525
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5900
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2408
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6218
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
         Height          =   285
         Left            =   2610
         TabIndex        =   3
         Top             =   1350
         Width           =   3525
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5900
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2408
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6218
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwCompSel 
         Height          =   3525
         Left            =   -74880
         TabIndex        =   22
         Top             =   420
         Width           =   6990
         _ExtentX        =   12330
         _ExtentY        =   6218
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         HotTracking     =   -1  'True
         SingleSel       =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.PictureBox picEdit 
         BorderStyle     =   0  'None
         Height          =   420
         Left            =   -74880
         ScaleHeight     =   420
         ScaleWidth      =   6810
         TabIndex        =   17
         Top             =   4020
         Visible         =   0   'False
         Width           =   6810
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "Cancelar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3660
            TabIndex        =   15
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2340
            TabIndex        =   14
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         Height          =   450
         Left            =   -74940
         ScaleHeight     =   450
         ScaleWidth      =   6090
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   4020
         Width           =   6090
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            Height          =   345
            Left            =   2340
            TabIndex        =   13
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "&Restaurar"
            Height          =   345
            Left            =   1200
            TabIndex        =   12
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdModificar 
            Caption         =   "&Modificar"
            Height          =   345
            Left            =   60
            TabIndex        =   11
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.Label lblGMN3_4 
         Caption         =   "Subfamilia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   195
         TabIndex        =   21
         Top             =   2310
         Width           =   1365
      End
      Begin VB.Label lblGMN2_4 
         Caption         =   "Familia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   195
         TabIndex        =   20
         Top             =   1845
         Width           =   1365
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Commodity:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   195
         TabIndex        =   19
         Top             =   1395
         Width           =   1365
      End
      Begin VB.Label lblGMN4_4 
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   195
         TabIndex        =   18
         Top             =   2775
         Width           =   1365
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPComPorMat.frx":03E8
            Key             =   "Rai"
            Object.Tag             =   "Rai"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPComPorMat.frx":083C
            Key             =   "COM"
            Object.Tag             =   "COM"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPComPorMat.frx":0950
            Key             =   "EQP"
            Object.Tag             =   "EQP"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmESTRCOMPComPorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Variables para el resize
Private iAltura As Integer
Private iAnchura As Integer

'Variable para no ejecutar el node check si se ha disparado el mousedown
Private bMouseDown As Boolean
Private SelNode As MSComctlLib.node

' Variables de seguridad
Private bModif As Boolean
Private bRMat As Boolean
Public bREqp As Boolean

'Variables para caso de bRMat
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4
Private oIMAsig As IMaterialAsignado

'Variables para la seleccion en combos
Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4
Public iNivelSeleccion As Integer

'variable para guardar los compradores que estan asignados
Private oCompsAsig As CCompradores
' variable para guardar los compradores a asignar
Private oComsSeleccionados As CCompradores
Private oComsDesSeleccionados As CCompradores
Private oCompAsignados As ICompProveAsignados
Private Accion As accionessummit
'''Combos
Private GMN1RespetarCombo As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN2RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Private GMN3RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean
Private GMN4RespetarCombo As Boolean
Private GMN4CargarComboDesde As Boolean

'HBA
Private sIdiMaterial As String
Private sIdiRaiz As String


Private Sub Arrange()

    If Width >= 1000 Then stabComp.Width = Width - 275
    If Height >= 1000 Then stabComp.Height = Height - 575
    If stabComp.Width >= 75 Then
        tvwEstrComp.Width = stabComp.Width - 200
        tvwCompSel.Width = stabComp.Width - 200
    End If
    If stabComp.Height >= 1075 Then
        tvwEstrComp.Height = stabComp.Height - 975
        tvwCompSel.Height = stabComp.Height - 975
    End If
    picNavigate.Top = tvwEstrComp.Top + tvwEstrComp.Height + 75
    picEdit.Top = picNavigate.Top
    picEdit.Left = (tvwEstrComp.Width / 2) - (picEdit.Width / 2)
    
End Sub

Private Sub CargarCompradores()

Dim oICompAsig As ICompProveAsignados
Dim iNivel As Integer

If picEdit.Visible Then Exit Sub

    If oGMN1Seleccionado Is Nothing _
        And oGMN2Seleccionado Is Nothing _
        And oGMN3Seleccionado Is Nothing _
        And oGMN4Seleccionado Is Nothing Then Exit Sub
        
    
    Set oCompsAsig = Nothing
    Screen.MousePointer = vbHourglass
    Set oCompsAsig = oFSGSRaiz.generar_CCompradores
    
    
    If bModif Then
        cmdModificar.Enabled = True
    End If
    
    cmdRestaurar.Enabled = True
    
    If Me.Visible Then tvwEstrComp.SetFocus
    tvwEstrComp.Nodes.clear
    tvwCompSel.Nodes.clear
    
    If Not oGMN4Seleccionado Is Nothing Then
     
        Set oICompAsig = oGMN4Seleccionado
        
        If bREqp Then
            Set oCompsAsig = oICompAsig.DevolverCompradoresAsignados(basOptimizacion.gCodEqpUsuario, True)
        Else
            Set oCompsAsig = oICompAsig.DevolverCompradoresAsignados(, True)
        End If
        Set oICompAsig = Nothing
        AnyadirCompradoresAEstructura oCompsAsig
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        
        Set oICompAsig = oGMN3Seleccionado
        
        If bRMat Then
            'Si el comprador no tiene ese material asignado no debe poder asignarle compradores
            iNivel = oICompAsig.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivel = 0 Or iNivel > 3 Then Exit Sub
        End If
        
        If bREqp Then
            Set oCompsAsig = oICompAsig.DevolverCompradoresAsignados(basOptimizacion.gCodEqpUsuario, True)
        Else
            Set oCompsAsig = oICompAsig.DevolverCompradoresAsignados(, True)
        End If
        Set oICompAsig = Nothing
        AnyadirCompradoresAEstructura oCompsAsig
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If Not oGMN2Seleccionado Is Nothing Then
       
        Set oICompAsig = oGMN2Seleccionado
        
        If bRMat Then
            'Si el comprador no tiene ese material asignado no debe poder asignarle compradores
            iNivel = oICompAsig.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivel = 0 Or iNivel > 2 Then Exit Sub
        End If
        
        If bREqp Then
            Set oCompsAsig = oICompAsig.DevolverCompradoresAsignados(basOptimizacion.gCodEqpUsuario, True)
        Else
            Set oCompsAsig = oICompAsig.DevolverCompradoresAsignados(, True)
        End If
        Set oICompAsig = Nothing
        AnyadirCompradoresAEstructura oCompsAsig
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    If Not oGMN1Seleccionado Is Nothing Then
      
        Set oICompAsig = oGMN1Seleccionado
        
        If bRMat Then
            'Si el comprador no tiene ese material asignado no debe poder asignarle compradores
            If oICompAsig.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario) <> 1 Then Exit Sub
        End If
        
        If bREqp Then
            Set oCompsAsig = oICompAsig.DevolverCompradoresAsignados(basOptimizacion.gCodEqpUsuario, True)
        Else
            Set oCompsAsig = oICompAsig.DevolverCompradoresAsignados(, True)
        End If
        Set oICompAsig = Nothing
        AnyadirCompradoresAEstructura oCompsAsig
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdlistado_Click()

    Screen.MousePointer = vbHourglass

    ''' * Objetivo: Obtener un listado de compradores por material
    
    If Not oGMN1Seleccionado Is Nothing Then
        frmLstESTRCOMPComPorMat.sGMN1Cod = oGMN1Seleccionado.Cod
        frmLstESTRCOMPComPorMat.txtEstMat = oGMN1Seleccionado.Cod
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        frmLstESTRCOMPComPorMat.sGMN2Cod = oGMN2Seleccionado.Cod
        frmLstESTRCOMPComPorMat.txtEstMat = frmLstESTRCOMPComPorMat.txtEstMat & " - " & oGMN2Seleccionado.Cod
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        frmLstESTRCOMPComPorMat.sGMN3Cod = oGMN3Seleccionado.Cod
        frmLstESTRCOMPComPorMat.txtEstMat = frmLstESTRCOMPComPorMat.txtEstMat & " - " & oGMN3Seleccionado.Cod
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        frmLstESTRCOMPComPorMat.sGMN4Cod = oGMN4Seleccionado.Cod
        frmLstESTRCOMPComPorMat.txtEstMat = frmLstESTRCOMPComPorMat.txtEstMat & " - " & oGMN4Seleccionado.Cod
    End If
    Screen.MousePointer = vbNormal
    
    frmLstESTRCOMPComPorMat.Show 1
End Sub

Private Sub cmdModificar_Click()
Dim oComs As CCompradores

    If oGMN1Seleccionado Is Nothing _
        And oGMN2Seleccionado Is Nothing _
        And oGMN3Seleccionado Is Nothing _
        And oGMN4Seleccionado Is Nothing Then Exit Sub
    
    Accion = ACCCompPorMatMod
    
    picCheck.Visible = True
    
    Set oComsSeleccionados = Nothing
    Set oComsDesSeleccionados = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oComsSeleccionados = oFSGSRaiz.generar_CCompradores
    Set oComsDesSeleccionados = oFSGSRaiz.generar_CCompradores
    
    Set oComs = oFSGSRaiz.generar_CCompradores
    
    AnyadirCompradoresAEstructuraMod
    
    If Not oGMN4Seleccionado Is Nothing Then
        Set oCompAsignados = oGMN4Seleccionado
    Else
        If Not oGMN3Seleccionado Is Nothing Then
            Set oCompAsignados = oGMN3Seleccionado
           
        Else
            If Not oGMN2Seleccionado Is Nothing Then
                Set oCompAsignados = oGMN2Seleccionado
               
            Else
                If Not oGMN1Seleccionado Is Nothing Then
                    Set oCompAsignados = oGMN1Seleccionado
                   
                End If
            End If
        End If
    End If
    
   tvwEstrComp.Visible = False
   tvwCompSel.Visible = True
   picNavigate.Visible = False
   picEdit.Visible = True
   Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdRestaurar_Click()

    Screen.MousePointer = vbHourglass
    tvwEstrComp.Nodes.clear
    CargarCompradores
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdSelMat_Click()
    
    frmSELMAT.sOrigen = "frmESTRCOMPComPorMat"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Hide
    frmSELMAT.Show 1
    
End Sub

Private Sub Form_Load()

iAltura = 5085
iAnchura = 7515
Me.Height = 5085
Me.Width = 7515
If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If

Arrange

Accion = ACCCompPorMatCon

cmdModificar.Enabled = False
cmdRestaurar.Enabled = False

ConfigurarNombres

ConfigurarSeguridad

CargarRecursos

    PonerFieldSeparator Me

End Sub

''' <summary>Configuración de la seguridad de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPPorGRUPModificar)) Is Nothing) Then
        bModif = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPPorGRUPRestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPPorGRUPRestEquipo)) Is Nothing) Then
        bREqp = True
    End If

    If Not bModif Then
        cmdModificar.Visible = False
        cmdListado.Left = cmdRestaurar.Left
        cmdRestaurar.Left = cmdModificar.Left
    End If
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)

Set oGruposMN1 = Nothing
Set oGruposMN2 = Nothing
Set oGruposMN3 = Nothing
Set oGruposMN4 = Nothing
Set oIMAsig = Nothing
Set oGMN1Seleccionado = Nothing
Set oGMN2Seleccionado = Nothing
Set oGMN3Seleccionado = Nothing
Set oGMN4Seleccionado = Nothing
Set oCompsAsig = Nothing
Me.Visible = False

End Sub

Public Function DevolverCod(ByVal nodx As node) As Variant

Select Case Left(nodx.Tag, 3)
    Case "EQP", "COM"
        DevolverCod = Right(nodx.Tag, Len(nodx.Tag) - 3)
End Select

End Function

Private Sub ConfigurarNombres()
    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"
End Sub
Private Sub AnyadirCompradoresAEstructuraMod()
Dim oComs As CCompradores
Dim oCom As CComprador
Dim nodo As MSComctlLib.node
Dim oeqp As CEquipo
Dim nodEqp As MSComctlLib.node
Dim nodx As MSComctlLib.node


On Error Resume Next
    
    Set nodo = tvwCompSel.Nodes.Add(, , "Raiz", sIdiRaiz, "Rai")
    nodo.Tag = "Rai"
    nodo.Expanded = True
    
    Screen.MousePointer = vbHourglass
    
    If bREqp Then
        Set oeqp = oFSGSRaiz.generar_CEquipo
        oeqp.Cod = basOptimizacion.gCodEqpUsuario
               
        oeqp.CargarTodosLosCompradores , , , , True, , False
        
        Set nodo = tvwCompSel.Nodes.Add("Raiz", tvwChild, "EQP" & CStr(basOptimizacion.gCodEqpUsuario), oUsuarioSummit.comprador.DenEqp, "EQP")
        nodo.Tag = "EQP" & CStr(basOptimizacion.gCodEqpUsuario)
        nodo.Expanded = True
            
        For Each oCom In oeqp.Compradores
                Set nodo = Nothing
                Set nodo = tvwCompSel.Nodes.Add("EQP" & CStr(oeqp.Cod), tvwChild, "COM" & CStr(oeqp.Cod) & String(gLongitudesDeCodigos.giLongCodeqp - Len(oeqp.Cod), " ") & CStr(oCom.Cod), CStr(oCom.Cod) & " " & oCom.Apel & " " & oCom.nombre, "COM")
                nodo.Tag = "COM" & CStr(oCom.Cod)
                Set nodo = Nothing
        Next
        Set oeqp = Nothing
    
    Else
        
        Set oComs = oFSGSRaiz.generar_CCompradores
        oComs.CargarTodosLosCompradores , , , , , , , True, False, True
        
        For Each oCom In oComs
            Set nodo = tvwCompSel.Nodes.Add("Raiz", tvwChild, "EQP" & CStr(oCom.codEqp), CStr(oCom.codEqp) & " " & oCom.DenEqp, "EQP")
            nodo.Tag = "EQP" & CStr(oCom.codEqp)
            nodo.Expanded = True
            Set nodo = Nothing
            Set nodo = tvwCompSel.Nodes.Add("EQP" & CStr(oCom.codEqp), tvwChild, "COM" & CStr(oCom.codEqp) & String(gLongitudesDeCodigos.giLongCodeqp - Len(oCom.codEqp), " ") & CStr(oCom.Cod), CStr(oCom.Cod) & " " & oCom.Apel & " " & oCom.nombre, "COM")
            nodo.Tag = "COM" & CStr(oCom.Cod)
            Set nodo = Nothing
        Next
        Set oComs = Nothing
    End If
    
    Set nodEqp = tvwEstrComp.Nodes(1).Child

    While Not nodEqp Is Nothing
        Set nodx = nodEqp.Child
        While Not nodx Is Nothing
            tvwCompSel.Nodes(nodx.key).Checked = True
            Set nodx = nodx.Next
        Wend
        Set nodEqp = nodEqp.Next
    Wend
    
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub cmdAceptar_Click()

 Dim teserror As TipoErrorSummit
 Dim oIBaseDatos As IBaseDatos

    ''' Comprobación de existencia del grupo de material
    
    Screen.MousePointer = vbHourglass
    Set oIBaseDatos = oGMN1Seleccionado
       
    If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
        oMensajes.DatoEliminado gParametrosGenerales.gsDEN_GMN1
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Set oIBaseDatos = Nothing
    If Not oGMN2Seleccionado Is Nothing Then
        Set oIBaseDatos = oGMN2Seleccionado
      
        If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            oMensajes.DatoEliminado gParametrosGenerales.gsDEN_GMN2
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    Set oIBaseDatos = Nothing
    
    If Not oGMN3Seleccionado Is Nothing Then
        Set oIBaseDatos = oGMN3Seleccionado
     
        If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            oMensajes.DatoEliminado gParametrosGenerales.gsDEN_GMN3
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    Set oIBaseDatos = Nothing
    
    If Not oGMN4Seleccionado Is Nothing Then
        Set oIBaseDatos = oGMN4Seleccionado
     
        If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            oMensajes.DatoEliminado gParametrosGenerales.gsDEN_GMN4
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    Set oIBaseDatos = Nothing
    
    ''' Fin de comprobación de existencia
    
    If oComsSeleccionados.Count > 0 Then
        
        Set oCompAsignados.Compradores = oComsSeleccionados
                        
        teserror = oCompAsignados.AsignarCompradores
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        Else
            'AnyadirCompradoresAEstructura oComsSeleccionados
            Set oComsSeleccionados = Nothing
            
        End If
    End If
    
    If oComsDesSeleccionados.Count > 0 Then
        
        Set oCompAsignados.Compradores = oComsDesSeleccionados
        
        teserror = oCompAsignados.DesAsignarCompradores
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        Else
            Set oComsDesSeleccionados = Nothing
        End If
    End If
    
    Accion = ACCCompPorMatCon
    Set oComsSeleccionados = Nothing
    Set oComsDesSeleccionados = Nothing
    
    basSeguridad.RegistrarAccion accionessummit.ACCCompPorMatMod, "GMN1:" & CStr(sdbcGMN1_4Cod.Value) & "GMN1:" & CStr(sdbcGMN1_4Cod.Value) & "GMN2:" & CStr(sdbcGMN2_4Cod.Value) & "GMN3:" & CStr(sdbcGMN3_4Cod.Value) & "GMN4:" & CStr(sdbcGMN4_4Cod.Value)
    
    picCheck.Visible = False
    tvwEstrComp.Visible = True
    tvwCompSel.Visible = False
    picNavigate.Visible = True
    picEdit.Visible = False
   
    cmdRestaurar_Click
    Screen.MousePointer = vbNormal
End Sub

Private Sub AsignarCompradores()
Dim nodx As MSComctlLib.node
Dim teserror As TipoErrorSummit
Dim nodEqp As MSComctlLib.node

Screen.MousePointer = vbHourglass
teserror.NumError = TESnoerror

Set oComsSeleccionados = Nothing
Set oComsSeleccionados = oFSGSRaiz.generar_CCompradores

Set nodEqp = tvwCompSel.Nodes(1).Child

While Not nodEqp Is Nothing
    
    Set nodx = nodEqp.Child
    While Not nodx Is Nothing
        If nodx.Checked Then
            If tvwEstrComp.Nodes(nodx.key) Is Nothing Then
                oComsSeleccionados.Add DevolverCod(nodEqp), "", DevolverCod(nodx), "", "", "", "", ""
            End If
        End If
        Set nodx = nodx.Next
   Wend
   
   Set nodEqp = nodEqp.Next

Wend
    
Screen.MousePointer = vbNormal
    
    
End Sub

Private Sub cmdCancelar_Click()
    
    Accion = ACCCompPorMatCon
    Screen.MousePointer = vbHourglass
    
    picCheck.Visible = False
    tvwEstrComp.Visible = True
    tvwCompSel.Visible = False
    picNavigate.Visible = True
    picEdit.Visible = False
    
    Set oComsSeleccionados = Nothing
    Set oComsDesSeleccionados = Nothing
  
    cmdRestaurar_Click
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub DesAsignarCompradores()
Dim nodx As MSComctlLib.node
Dim teserror As TipoErrorSummit
Dim nodEqp As MSComctlLib.node
Dim oCompAsignados As ICompProveAsignados

Screen.MousePointer = vbHourglass
teserror.NumError = TESnoerror

Set oComsDesSeleccionados = Nothing
Set oComsDesSeleccionados = oFSGSRaiz.generar_CCompradores

Set nodEqp = tvwCompSel.Nodes(1).Child

While Not nodEqp Is Nothing
    
    Set nodx = nodEqp.Child
    While Not nodx Is Nothing
        If nodx.Checked Then
            If tvwEstrComp.Nodes(nodx.key) Is Nothing Then
                oComsSeleccionados.Add DevolverCod(nodEqp), "", DevolverCod(nodx), "", "", "", "", ""
            End If
        End If
        Set nodx = nodx.Next
   Wend
   
   Set nodEqp = nodEqp.Next

Wend
    
    If Not oGMN4Seleccionado Is Nothing Then
        Set oCompAsignados = oGMN4Seleccionado
      
    Else
        If Not oGMN3Seleccionado Is Nothing Then
            Set oCompAsignados = oGMN3Seleccionado
           
        Else
            If Not oGMN2Seleccionado Is Nothing Then
                Set oCompAsignados = oGMN2Seleccionado
               
            Else
                If Not oGMN1Seleccionado Is Nothing Then
                    Set oCompAsignados = oGMN1Seleccionado
                   
                End If
            End If
        End If
    End If
    
    If oComsSeleccionados.Count = 0 Then
        Unload Me
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    teserror = oCompAsignados.AsignarCompradores
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    Else
        Set oComsSeleccionados = Nothing
    End If
        
    Screen.MousePointer = vbNormal
   
End Sub

Private Sub sdbcGMN1_4Cod_Click()
    
    If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN1_4Den_Click()
    If Not sdbcGMN1_4Den.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
End Sub

Private Sub sdbcGMN2_4Cod_Click()
    If Not sdbcGMN2_4Cod.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
End Sub

Private Sub sdbcGMN2_4Den_Click()
    If Not sdbcGMN2_4Den.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
End Sub

Private Sub sdbcGMN3_4Cod_Click()
    If Not sdbcGMN3_4Cod.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
End Sub

Private Sub sdbcGMN3_4Den_Click()
    If Not sdbcGMN3_4Den.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Cod_Click()
    If Not sdbcGMN4_4Cod.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Den_Click()
    If Not sdbcGMN4_4Den.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Screen.MousePointer = vbHourglass
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4Den.Text = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
        
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
           
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
        
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub stabComp_Click(PreviousTab As Integer)
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer
Dim oIBaseDatos As IBaseDatos

Screen.MousePointer = vbHourglass

If stabComp.Tab = 1 Then
    
    If oGMN4Seleccionado Is Nothing And oGMN3Seleccionado Is Nothing And oGMN2Seleccionado Is Nothing And oGMN1Seleccionado Is Nothing Then
        stabComp.Tab = 0
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
         
''' Comprobación de existencia del grupo de material
    
    Set oIBaseDatos = oGMN1Seleccionado
   
    If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
        stabComp.Tab = 0
        Screen.MousePointer = vbNormal
        oMensajes.DatoEliminado gParametrosGenerales.gsDEN_GMN1
        Exit Sub
    End If
    
    Set oIBaseDatos = Nothing
    If Not oGMN2Seleccionado Is Nothing Then
        Set oIBaseDatos = oGMN2Seleccionado
       
        If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            stabComp.Tab = 0
            Screen.MousePointer = vbNormal
            oMensajes.DatoEliminado gParametrosGenerales.gsDEN_GMN2
            Exit Sub
        End If
    End If
    Set oIBaseDatos = Nothing
    If Not oGMN3Seleccionado Is Nothing Then
        Set oIBaseDatos = oGMN3Seleccionado
       
        If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            stabComp.Tab = 0
            Screen.MousePointer = vbNormal
            oMensajes.DatoEliminado gParametrosGenerales.gsDEN_GMN3
            Exit Sub
        End If
    End If
    Set oIBaseDatos = Nothing
    If Not oGMN4Seleccionado Is Nothing Then
        Set oIBaseDatos = oGMN4Seleccionado
       
        If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            stabComp.Tab = 0
            Screen.MousePointer = vbNormal
            oMensajes.DatoEliminado gParametrosGenerales.gsDEN_GMN4
            Exit Sub
        End If
    End If
    Set oIBaseDatos = Nothing
    
    '' Fin de la comprobación
    If Not oGMN4Seleccionado Is Nothing Then
        Set oCompAsignados = oGMN4Seleccionado
        CargarCompradores
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If Not oGMN3Seleccionado Is Nothing Then
        Set oCompAsignados = oGMN3Seleccionado
      
        If bRMat Then
            Set oICompAsignado = oGMN3Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivelAsig = 0 Or iNivelAsig >= 3 Then
                stabComp.Tab = 0
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
        CargarCompradores
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If Not oGMN2Seleccionado Is Nothing Then
        Set oICompAsignado = oGMN2Seleccionado
        Set oCompAsignados = oGMN2Seleccionado
       
        If bRMat Then
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivelAsig = 0 Or iNivelAsig >= 2 Then
                stabComp.Tab = 0
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
                
        End If
        CargarCompradores
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If Not oGMN1Seleccionado Is Nothing Then
        Set oICompAsignado = oGMN1Seleccionado
        Set oCompAsignados = oGMN1Seleccionado
        
        If bRMat Then
            stabComp.Tab = 0
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        CargarCompradores
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
Else
        'Tab 0 pulsado
        If Accion = accionessummit.ACCCompPorMatMod Then
            stabComp.Tab = 1
        Else
            
            tvwEstrComp.Nodes.clear
            tvwCompSel.Nodes.clear
            cmdModificar.Enabled = False
            cmdRestaurar.Enabled = False
            
        End If
        
End If

Screen.MousePointer = vbNormal

End Sub

Private Sub tvwCompSel_KeyUp(KeyCode As Integer, Shift As Integer)
Dim sCod As String
Dim iNivel As Integer

If bMouseDown Then
    
    bMouseDown = False
    
    If Left(SelNode.Tag, 3) = "EQP" Or Left(SelNode.Tag, 3) = "Rai" Then
        SelNode.Checked = Not SelNode.Checked
        Exit Sub
     End If
                        
    sCod = DevolverCod(SelNode.Parent)
    sCod = sCod & String(basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(sCod), " ")
    sCod = sCod & DevolverCod(SelNode)
    
    Screen.MousePointer = vbHourglass
    
    If SelNode.Checked Then
        
        sCod = DevolverCod(SelNode.Parent)
        sCod = sCod & String(basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(sCod), " ")
        sCod = sCod & DevolverCod(SelNode)

        If oCompsAsig.Item(sCod) Is Nothing Then
            If oComsSeleccionados.Item(sCod) Is Nothing Then
                oComsSeleccionados.Add DevolverCod(SelNode.Parent), "", DevolverCod(SelNode), SelNode.Text, "", "", "", ""
            End If
        End If
        
        oComsDesSeleccionados.Remove CStr(DevolverCod(SelNode.Parent)) & CStr(DevolverCod(SelNode))
    
    Else
        
        If Not oCompsAsig.Item(sCod) Is Nothing Then
            iNivel = oCompAsignados.NivelAsignadoComprador(DevolverCod(SelNode.Parent), DevolverCod(SelNode))
            If iNivel < iNivelSeleccion Then
                ' el comprador esta asignado a traves de un material superior
                Select Case iNivel
                    Case 1
                        oMensajes.AsignacionCompAMaterialSuperior (lblGMN1_4)
                    Case 2
                        oMensajes.AsignacionCompAMaterialSuperior (lblGMN2_4)
                    Case 3
                        oMensajes.AsignacionCompAMaterialSuperior (lblGMN3_4)
                    Case 4
                        oMensajes.AsignacionCompAMaterialSuperior (lblGMN4_4)
                End Select
                
                oComsDesSeleccionados.Add DevolverCod(SelNode.Parent), "", DevolverCod(SelNode), "", "", "", "", ""
    
            Else
                oComsDesSeleccionados.Add DevolverCod(SelNode.Parent), "", DevolverCod(SelNode), "", "", "", "", ""
                
            End If
                        
        End If
        
        oComsSeleccionados.Remove CStr(DevolverCod(SelNode.Parent)) & CStr(DevolverCod(SelNode))
        
    End If
 
    Screen.MousePointer = vbNormal
 
 End If
 
End Sub

Private Sub tvwCompSel_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.node
Dim tvhti As TVHITTESTINFO
                    
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwCompSel.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwCompSel.hWnd, TVM_HITTEST, 0, tvhti)) Then
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiará al finalizar este procedimiento
                 ' por lo que hay que operar con lógica inversa
                    bMouseDown = True
               
                    Set SelNode = nod
                End If
            End If
        End If
    End If
    
End Sub

Private Sub tvwCompSel_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim sCod As String
Dim iNivel As Integer

If bMouseDown Then
    
    bMouseDown = False
    
    If Left(SelNode.Tag, 3) = "EQP" Or Left(SelNode.Tag, 3) = "Rai" Then
        SelNode.Checked = Not SelNode.Checked
        Exit Sub
     End If
                        
    sCod = DevolverCod(SelNode.Parent)
    sCod = sCod & String(basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(sCod), " ")
    sCod = sCod & DevolverCod(SelNode)

    If SelNode.Checked Then
        
        sCod = DevolverCod(SelNode.Parent)
        sCod = sCod & String(basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(sCod), " ")
        sCod = sCod & DevolverCod(SelNode)

        If oCompsAsig.Item(sCod) Is Nothing Then
            If oComsSeleccionados.Item(sCod) Is Nothing Then
                oComsSeleccionados.Add DevolverCod(SelNode.Parent), "", DevolverCod(SelNode), SelNode.Text, "", "", "", ""
            End If
        End If
        
        oComsDesSeleccionados.Remove sCod
    Else
        
        If Not oCompsAsig.Item(sCod) Is Nothing Then
            iNivel = oCompAsignados.NivelAsignadoComprador(DevolverCod(SelNode.Parent), DevolverCod(SelNode))
            If iNivel < iNivelSeleccion Then
                ' el comprador esta asignado a traves de un material superior
                Select Case iNivel
                    Case 1
                        oMensajes.AsignacionCompAMaterialSuperior (lblGMN1_4)
                    Case 2
                        oMensajes.AsignacionCompAMaterialSuperior (lblGMN2_4)
                    Case 3
                        oMensajes.AsignacionCompAMaterialSuperior (lblGMN3_4)
                    Case 4
                        oMensajes.AsignacionCompAMaterialSuperior (lblGMN4_4)
                End Select
                                
                oComsDesSeleccionados.Add DevolverCod(SelNode.Parent), "", DevolverCod(SelNode), "", "", "", "", ""
            Else
                oComsDesSeleccionados.Add DevolverCod(SelNode.Parent), "", DevolverCod(SelNode), "", "", "", "", ""
            End If
                        
        End If
        
        oComsSeleccionados.Remove CStr(DevolverCod(SelNode.Parent)) & CStr(DevolverCod(SelNode))
    End If
 End If

End Sub

Private Sub tvwCompSel_NodeCheck(ByVal node As MSComctlLib.node)
If bMouseDown Then
    Exit Sub
End If

bMouseDown = True
Set SelNode = node
     
End Sub

Private Sub AnyadirCompradoresAEstructura(ByVal oComs As CCompradores)
Dim oCom As CComprador
Dim nodo As MSComctlLib.node
Dim sCod As String

On Error Resume Next
    
    tvwCompSel.Nodes.clear
    Set nodo = tvwEstrComp.Nodes.Add(, , "Raiz", sIdiRaiz, "Rai")
    nodo.Tag = "Rai"
    nodo.Expanded = True
    
    For Each oCom In oComs
        Set nodo = tvwEstrComp.Nodes.Add("Raiz", tvwChild, "EQP" & CStr(oCom.codEqp), CStr(oCom.codEqp) & " " & oCom.DenEqp, "EQP")
        nodo.Tag = "EQP" & CStr(oCom.codEqp)
        nodo.Expanded = True
        Set nodo = Nothing
        Set nodo = tvwEstrComp.Nodes.Add("EQP" & CStr(oCom.codEqp), tvwChild, "COM" & CStr(oCom.codEqp) & String(gLongitudesDeCodigos.giLongCodeqp - Len(oCom.codEqp), " ") & CStr(oCom.Cod), CStr(oCom.Cod) & " " & oCom.Apel & " " & oCom.nombre, "COM")
        nodo.Tag = "COM" & CStr(oCom.Cod)
        Set nodo = Nothing
        
        sCod = oCom.codEqp
        sCod = sCod & String(basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(sCod), " ")
        sCod = sCod & oCom.Cod
        If oCompsAsig.Item(sCod) Is Nothing Then
            oCompsAsig.Add oCom.codEqp, "", oCom.Cod, "", "", "", "", ""
        End If
    Next
    
    Set oComs = Nothing
End Sub

Private Sub GMN1Seleccionado()
        
    iNivelSeleccion = 1
    
    sdbcGMN2_4Cod.RemoveAll
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN2_4Den.RemoveAll
    sdbcGMN2_4Cod.Text = ""
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcGMN2_4Den.Text = ""
    
    Set oGMN1Seleccionado = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    stabComp.Enabled = True
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
    
    Screen.MousePointer = vbNormal
        
End Sub

Private Sub GMN2Seleccionado()
        
    iNivelSeleccion = 2
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    
    Screen.MousePointer = vbHourglass
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    
    stabComp.Enabled = True
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
    Screen.MousePointer = vbNormal

End Sub
Private Sub GMN3Seleccionado()
        
    iNivelSeleccion = 3

    Set oGMN3Seleccionado = Nothing
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    Screen.MousePointer = vbHourglass
    
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    
    stabComp.Enabled = True
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub GMN4Seleccionado()
        
    iNivelSeleccion = 4

    Set oGMN4Seleccionado = Nothing
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    Screen.MousePointer = vbHourglass

    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
    stabComp.Enabled = True
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
    
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN1_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
        
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    Else
        
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
     
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
         
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN1_4Cod.Text = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
   
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
    Else
      
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        GMN1RespetarCombo = False
        GMN1Seleccionado
        GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub


Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN1_4Den_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN1_4Den.Text = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
     
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, , False)
    Else
       
        oGMN1s.CargarTodosLosGruposMat , sdbcGMN1_4Den.Text, True, , False
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Den.Text = ""
        
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        
        sdbcGMN1_4Den.Columns(0).Value = sdbcGMN1_4Den.Text
        sdbcGMN1_4Den.Columns(1).Value = sdbcGMN1_4Cod.Text
        
        GMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN2_4Cod.Text = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN1Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
           
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
        
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN2_4Den.Text = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN1Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, , Trim(sdbcGMN2_4Den), True, , False)
        Else
          
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
        oMensajes.NoValido sIdiMaterial
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2s.Item(1).Cod
        
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
        
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN3_4Cod.Text = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
          
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
    Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not GMN3RespetarCombo Then

        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Den.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False)
        End If
    Else
        
        
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    Screen.MousePointer = vbNormal
     
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub

Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN3_4Den.Text = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False)
        Else
           
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
    Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3s.Item(1).Cod
        
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
        
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Private Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    Screen.MousePointer = vbHourglass

    If sdbcGMN4_4Cod.Text = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
     
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
         
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub
Private Sub sdbcGMN1_4Cod_Change()
    If Not GMN1RespetarCombo Then
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
    End If
End Sub
Private Sub sdbcGMN1_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer


    sdbcGMN1_4Den.RemoveAll
    Screen.MousePointer = vbHourglass

    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
       
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        
        GMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass

    If oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
     
        Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
   
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not GMN2RespetarCombo Then

        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
        
    If sdbcGMN2_4Den.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Den.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
     
        Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , False)
        End If
    
    Else
        
       
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcGMN3_4Cod_Change()
    
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass

    If oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
     
        Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
        
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal

    
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not GMN1RespetarCombo Then

        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Den.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
    If Not GMN4RespetarCombo Then
    
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        
        GMN4CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Cod.RemoveAll

    Screen.MousePointer = vbHourglass

    If oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else
        
       
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not GMN4RespetarCombo Then

        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Den.RemoveAll
    Screen.MousePointer = vbHourglass

    If oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False)
        End If
    Else
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
    End If
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub


Public Sub PonerMatSeleccionadoEnCombos()
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
    End If
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTR_COMPCOMPORMAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        frmESTRCOMPComPorMat.caption = Ador(0).Value
        Ador.MoveNext
        stabComp.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        stabComp.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        sIdiMaterial = Ador(0).Value
        Ador.MoveNext
        sIdiRaiz = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
        
End Sub

