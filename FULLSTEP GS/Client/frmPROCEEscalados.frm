VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPROCEEscalados 
   BackColor       =   &H00808000&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Escalados de precios"
   ClientHeight    =   5790
   ClientLeft      =   4290
   ClientTop       =   5430
   ClientWidth     =   4845
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEEscalados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5790
   ScaleWidth      =   4845
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin SSDataWidgets_B.SSDBGrid SSDBGridEscalados 
      Height          =   2910
      Left            =   315
      TabIndex        =   9
      Top             =   2250
      Width           =   4215
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   4
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPROCEEscalados.frx":014A
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   6403
      Columns(0).Caption=   "Cantidades directas"
      Columns(0).Name =   "DIR"
      Columns(0).Alignment=   1
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).NumberFormat=   "standard"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Cantidad inicial"
      Columns(1).Name =   "INI"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "standard"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Cantidad final"
      Columns(2).Name =   "FIN"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "standard"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   7435
      _ExtentY        =   5133
      _StockProps     =   79
      Caption         =   "Escalados de precios"
      ForeColor       =   0
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdAyuda 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4320
      Picture         =   "frmPROCEEscalados.frx":0166
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   585
      Visible         =   0   'False
      Width           =   350
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   1395
      TabIndex        =   6
      Top             =   5355
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   345
      Left            =   2464
      TabIndex        =   5
      Top             =   5355
      Width           =   1005
   End
   Begin VB.Frame FrameEscalado 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   5280
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5010
      Begin VB.PictureBox picUnidades 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   300
         ScaleHeight     =   495
         ScaleWidth      =   4245
         TabIndex        =   10
         Top             =   1530
         Width           =   4245
         Begin SSDataWidgets_B.SSDBCombo sdbcUnidadCod 
            Height          =   285
            Left            =   0
            TabIndex        =   11
            Top             =   90
            Width           =   1125
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1984
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcUnidadDen 
            Height          =   285
            Left            =   1140
            TabIndex        =   12
            Top             =   90
            Width           =   2985
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6985
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2540
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5265
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
      End
      Begin VB.CheckBox chkTodoProceso 
         BackColor       =   &H00808000&
         Caption         =   "Aplicar a todo el proceso"
         ForeColor       =   &H00FFFFFF&
         Height          =   360
         Left            =   90
         TabIndex        =   3
         Top             =   0
         Width           =   2085
      End
      Begin VB.OptionButton optRangosCantidades 
         BackColor       =   &H00808000&
         Caption         =   "Rangos de cantidades"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   405
         TabIndex        =   2
         Top             =   450
         Value           =   -1  'True
         Width           =   2535
      End
      Begin VB.OptionButton optCantidadesDirectas 
         BackColor       =   &H00808000&
         Caption         =   "Cantidades directas"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   405
         TabIndex        =   1
         Top             =   765
         Width           =   2535
      End
      Begin VB.Line Line14 
         BorderColor     =   &H00FFFFFF&
         X1              =   90
         X2              =   4725
         Y1              =   360
         Y2              =   360
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   90
         X2              =   4680
         Y1              =   1170
         Y2              =   1170
      End
      Begin VB.Label lblSelecUnid 
         BackColor       =   &H00808000&
         Caption         =   "Seleccione la unidad de medida para los items"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   90
         TabIndex        =   4
         Top             =   1305
         Width           =   4470
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00FFFFFF&
         X1              =   90
         X2              =   4725
         Y1              =   2070
         Y2              =   2070
      End
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   345
      Left            =   2018
      TabIndex        =   7
      Top             =   5355
      Width           =   1005
   End
End
Attribute VB_Name = "frmPROCEEscalados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public modo As TModoEscalado
Private SSDBGridEscalados_enEdicion As Boolean
Public sCodUnidades As String
Public sDenUnidades As String

Private m_oUnidades As CUnidades
Private m_bUniRespetarCombo As Boolean
Private m_bUniCargarComboDesde As Boolean

Public g_bModoEdicionItem As Boolean
Public g_sCodGrupoSeleccionado As String
Public g_oGrupoSeleccionado As CGrupo

Private m_oEscaladosAux As CEscalados ' Objeto auxiliar para las comprobaciones

Private m_bOptionAuto As Boolean

Public oGrupoEsc As CGrupo ' Objeto grupo que contiene los escalados originales cuando se hace un cambio aplicado a todo el proceso

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean


Private m_sMsgError As String
Private Sub chkTodoProceso_Click()
Dim oGrupo As CGrupo
Dim oEsc As CEscalado
Dim oEscSel As CEscalado
Dim bEncontrado As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If frmPROCE.g_oProcesoSeleccionado.Estado >= conofertas Then
    'Si el proceso no tiene los mismos escalados y hay ofertas no deja marcarlo porque no podemos cambiar los escalados
    If chkTodoProceso.Value = vbChecked And frmPROCE.g_oProcesoSeleccionado.Escalados = False Then
        For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
            If oGrupo.ComprobarOfertas Then
                'Se conprueba que el grupo tiene distintos escalados del seleccionado
                'Si no hay ofertas para los grupos de escalados distintos podemos cambiarlos
                If oGrupo.Id <> g_oGrupoSeleccionado.Id Then
                    oGrupo.CargarEscalados
                    If oGrupo.TipoEscalados <> g_oGrupoSeleccionado.TipoEscalados Then
                        oMensajes.MensajeOKOnly 1257
                        chkTodoProceso.Value = vbUnchecked
                        Exit Sub
                    End If
                    If oGrupo.UnidadEscalado <> g_oGrupoSeleccionado.UnidadEscalado Then
                        oMensajes.MensajeOKOnly 1257
                        chkTodoProceso.Value = vbUnchecked
                        Exit Sub
                    End If
                    For Each oEsc In oGrupo.Escalados
                        bEncontrado = False
                        For Each oEscSel In g_oGrupoSeleccionado.Escalados
                            If oEsc.Inicial = oEscSel.Inicial Then
                                If oGrupo.TipoEscalados = ModoRangos Then
                                    If oEsc.final = oEscSel.final Then
                                        bEncontrado = True
                                        Exit For
                                    End If
                                Else
                                    bEncontrado = True
                                    Exit For
                                End If
                            End If
                        Next
                        If Not bEncontrado Then
                            oMensajes.MensajeOKOnly 1257
                            chkTodoProceso.Value = vbUnchecked
                            Exit Sub
                        End If
                    Next
                End If
            End If
        Next
    End If
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "chkTodoProceso_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdAceptar_Click()

    'Si el grid est� en modo update hay que forzar actualizaci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If SSDBGridEscalados.DataChanged Then
        'SSDBGridEscalados_BeforeUpdate (False)
        SSDBGridEscalados.Update
        If Not SSDBGridEscalados_enEdicion Then
            If ComprobarFormulario() Then
                If GuardarDatos() Then
                    Unload Me
                End If
            End If
        Else
            SSDBGridEscalados_enEdicion = False
        End If
    Else
        If ComprobarFormulario() Then
            If GuardarDatos() Then
                Unload Me
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCerrar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "cmdCerrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    CargarRecursos
    PonerFieldSeparator Me
    SSDBGridEscalados_enEdicion = False

    'Listas de unidades
    Set m_oUnidades = oFSGSRaiz.Generar_CUnidades
    If Me.sCodUnidades <> "" Then
        Me.sdbcUnidadCod.Text = Me.sCodUnidades
        sdbcUnidadCod_Validate (False)
        
    End If
    If modo = ModoRangos Then
        ModoARangos
    Else
        ModoADirecto
    End If
    PrepararLista
    
    If g_sCodGrupoSeleccionado = "" Then   'Todos los Items
        Me.chkTodoProceso.Value = vbGrayed
    Else
        If frmPROCE.g_oProcesoSeleccionado.Escalados Then
            Me.chkTodoProceso.Value = vbChecked
        End If
    End If
    
    If g_bModoEdicionItem Then
        'Si hay ofertas no se deja modificar
        If g_oGrupoSeleccionado.ComprobarOfertas Then
            ModoConsulta
        Else
            ModoEdicion
        End If
    Else
        ModoConsulta
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub optCantidadesDirectas_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bOptionAuto Then
        If Not ModoADirecto(True) Then
            Me.modo = ModoRangos
            m_bOptionAuto = True
            Me.optRangosCantidades.Value = True
            m_bOptionAuto = False
        End If
    End If
    m_bOptionAuto = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "optCantidadesDirectas_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub optRangosCantidades_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bOptionAuto Then
        ModoARangos
    End If
    m_bOptionAuto = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "optRangosCantidades_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcUnidadCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bUniRespetarCombo Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = ""
        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcUnidadCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcUnidadCod.DroppedDown Then
        sdbcUnidadCod.Value = ""
        sdbcUnidadDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcUnidadCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Value = "..." Then
        sdbcUnidadCod.Text = ""
        Exit Sub
    End If
    
    m_bUniRespetarCombo = True
    sdbcUnidadDen.Text = sdbcUnidadCod.Columns(1).Text
    sdbcUnidadCod.Text = sdbcUnidadCod.Columns(0).Text
    m_bUniRespetarCombo = False
    
    m_bUniCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadCod_DropDown()
Dim oUni As CUnidad

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.RemoveAll

    Screen.MousePointer = vbHourglass
    If m_bUniCargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidades CStr(sdbcUnidadCod.Value)
    Else
        m_oUnidades.CargarTodasLasUnidades
    End If

    For Each oUni In m_oUnidades
        sdbcUnidadCod.AddItem oUni.Cod & Chr(m_lSeparador) & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next

    If sdbcUnidadCod.Rows = 0 Then
        sdbcUnidadCod.AddItem ""
    End If

    Set oUni = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcUnidadCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.DataFieldList = "Column 0"
    sdbcUnidadCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcUnidadCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcUnidadCod.Rows - 1
            bm = sdbcUnidadCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcUnidadCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcUnidadCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadCod_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcUnidadCod_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Text = "" Then Exit Sub

    If sdbcUnidadCod.Text = sdbcUnidadCod.Columns(0).Text Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = sdbcUnidadCod.Columns(1).Text
        m_bUniRespetarCombo = False
        Exit Sub
    End If

    If sdbcUnidadCod.Text = sdbcUnidadDen.Columns(1).Text Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = sdbcUnidadDen.Columns(0).Text
        m_bUniRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el destino
    m_oUnidades.CargarTodasLasUnidades CStr(sdbcUnidadCod.Value), , True
    If m_oUnidades.Count = 0 Then
        sdbcUnidadCod.Text = ""
        Screen.MousePointer = vbNormal
    Else
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = m_oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den

        sdbcUnidadCod.Columns(0).Text = sdbcUnidadCod.Text
        sdbcUnidadCod.Columns(1).Text = sdbcUnidadDen.Text

        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bUniRespetarCombo Then
        m_bUniRespetarCombo = True
        sdbcUnidadCod.Text = ""
        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcUnidadDen.DroppedDown Then
        sdbcUnidadDen.Value = ""
        sdbcUnidadCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadDen.Value = "..." Then
        sdbcUnidadDen.Text = ""
        Exit Sub
    End If
    
    m_bUniRespetarCombo = True
    sdbcUnidadCod.Text = sdbcUnidadDen.Columns(1).Text
    sdbcUnidadDen.Text = sdbcUnidadDen.Columns(0).Text
    m_bUniRespetarCombo = False
    
    m_bUniCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_DropDown()
Dim oUni As CUnidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If m_bUniCargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidades , CStr(sdbcUnidadDen.Value), , True
    Else
        m_oUnidades.CargarTodasLasUnidades , , , True
    End If

    For Each oUni In m_oUnidades
        sdbcUnidadDen.AddItem oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oUni.Cod
    Next

    If sdbcUnidadDen.Rows = 0 Then
        sdbcUnidadDen.AddItem ""
    End If

    Set oUni = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcUnidadDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadDen.DataFieldList = "Column 0"
    sdbcUnidadDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcUnidadDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcUnidadDen.Rows - 1
            bm = sdbcUnidadDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcUnidadDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcUnidadDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "sdbcUnidadDen_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub SSDBGridEscalados_AfterColUpdate(ByVal ColIndex As Integer)

    Dim vb As Variant
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    vb = SSDBGridEscalados.AddItemBookmark(ColIndex)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "SSDBGridEscalados_AfterColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub SSDBGridEscalados_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DispPromptMsg = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "SSDBGridEscalados_BeforeDelete", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub SSDBGridEscalados_BeforeUpdate(Cancel As Integer)
    'Before Update
    ' Comprobar que se ajusta al patr�n de escalados...
    Dim vb As Variant
    Dim actual As Variant
    Dim i As Long
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados_enEdicion = True
    
    
    If modo = ModoRangos Then
        Dim ini As Variant
        Dim fin As Variant
        
        ini = SSDBGridEscalados.Columns("INI").Value
        fin = SSDBGridEscalados.Columns("FIN").Value
        
        
        '1� - Comprobar que es una l�nea v�lida. Descartamos l�nea en blanco
        If ini = "" And fin = "" Then
            SSDBGridEscalados.CancelUpdate
            Cancel = True
            If Me.Visible Then SSDBGridEscalados.SetFocus
            Exit Sub
        
        End If
        
        '2� - Comprobar que se han introducido los dos valores
        If ini = "" Or fin = "" Then
            oMensajes.escaladosRangoIncompleto
            Cancel = True
            If Me.Visible Then SSDBGridEscalados.SetFocus
            Exit Sub
        End If
        
        '3� - Comprobar que se trata de valores num�ricos
        If (Not IsNumeric(ini)) Or (Not IsNumeric(fin)) Then
            oMensajes.escaladosValoresInicioFin
            Cancel = True
            If Me.Visible Then SSDBGridEscalados.SetFocus
            Exit Sub
            
        End If
            
        '4� - Comprobar que el orden del rango es correcto inicio < fin
        If CDbl(ini) >= CDbl(fin) Then
            oMensajes.escaladosValoresFinInicio
            Cancel = True
            If Me.Visible Then SSDBGridEscalados.SetFocus
            Exit Sub
        End If
        
        '5� - Comprobar que el rango no est� incluido en otro anterior
                
        If Not SSDBGridEscalados.IsAddRow Then
            'Modificamos un rango
            actual = SSDBGridEscalados.Bookmark
        End If
                
        For i = 0 To SSDBGridEscalados.Rows - 1
            vb = SSDBGridEscalados.AddItemBookmark(i)
            If Not SSDBGridEscalados.IsAddRow Then
                'Modificamos un rango
                If CStr(actual) <> CStr(vb) Then
                    'Comprobamos solo en el resto de rangos
                    'S�lo aquellos rangos bien formados
                    If IsNumeric(SSDBGridEscalados.Columns("INI").CellValue(vb)) And IsNumeric(SSDBGridEscalados.Columns("FIN").CellValue(vb)) Then
                        If (CDbl(SSDBGridEscalados.Columns("INI").CellValue(vb)) <= CDbl(ini) And CDbl(SSDBGridEscalados.Columns("FIN").CellValue(vb)) > CDbl(ini)) Or (CDbl(SSDBGridEscalados.Columns("INI").CellValue(vb)) < CDbl(fin) And CDbl(SSDBGridEscalados.Columns("FIN").CellValue(vb)) >= CDbl(fin)) Then
                            'Est� incluido en un rango existente
                            oMensajes.escaladosValorOtroRango
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                End If
            Else
                vb = SSDBGridEscalados.AddItemBookmark(i)
                If Not IsEmpty(vb) Then
                    If IsNumeric(SSDBGridEscalados.Columns("INI").CellValue(vb)) And IsNumeric(SSDBGridEscalados.Columns("FIN").CellValue(vb)) Then
                        If (CDbl(SSDBGridEscalados.Columns("INI").CellValue(vb)) <= CDbl(ini) And CDbl(SSDBGridEscalados.Columns("FIN").CellValue(vb)) > CDbl(ini)) Or (CDbl(SSDBGridEscalados.Columns("INI").CellValue(vb)) < CDbl(fin) And CDbl(SSDBGridEscalados.Columns("FIN").CellValue(vb)) >= CDbl(fin)) Then
                            'Est� incluido en un rango existente
                            oMensajes.escaladosValorOtroRango
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                End If
            End If
        Next i
        
    SSDBGridEscalados.Columns("DIR").Value = SSDBGridEscalados.Columns("INI").Value
        
    
    Else
        Dim dir As Variant
        
        
        If Not SSDBGridEscalados.IsAddRow Then
            'Modificamos un rango
            actual = SSDBGridEscalados.Bookmark
        End If
        
        dir = SSDBGridEscalados.Columns("DIR").Value
        
        
        If dir = "" Then
        
            SSDBGridEscalados.CancelUpdate
            SSDBGridEscalados_enEdicion = False
            Cancel = True
            If Me.Visible Then SSDBGridEscalados.SetFocus
            Exit Sub

        End If
        
        
        
        '1� - Comprobar que se ha introducido un valor num�rico
        If Not IsNumeric(dir) Then
            oMensajes.escaladosValorCantidadNumerico
            Cancel = True
            If Me.Visible Then SSDBGridEscalados.SetFocus
            Exit Sub
        End If
        
        '2� - Comprobar que no se repite la cantidad
        For i = 0 To SSDBGridEscalados.Rows - 1
            vb = SSDBGridEscalados.AddItemBookmark(i)
            
            If Not SSDBGridEscalados.IsAddRow Then
                If CStr(actual) <> CStr(vb) Then
                    'Comprobamos solo en el resto de rangos
            
                    If CDbl(SSDBGridEscalados.Columns("DIR").CellValue(vb)) = CDbl(dir) Then
                        'Existe un valor similar
                        oMensajes.escaladosValorCantidadYaEnEscalados
                        Cancel = True
                        If Me.Visible Then SSDBGridEscalados.SetFocus
                        Exit Sub
                    End If
                End If
            End If
        Next i
    
    End If
    SSDBGridEscalados.Columns("INI").Value = SSDBGridEscalados.Columns("DIR").Value
    SSDBGridEscalados_enEdicion = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "SSDBGridEscalados_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub




Function ComprobarFormulario() As Boolean

    'Comprobar que ha seleccionado un unidad
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Text = "" Then
        oMensajes.escaladosSeleccioneUnidadMedida
        If Me.Visible Then sdbcUnidadCod.SetFocus
        ComprobarFormulario = False
        Exit Function
    End If
    Dim i As Long
    Dim bm As Variant
    'Comprobar los datos del Grid Son correctos
    If modo = ModoRangos Then
        For i = 0 To SSDBGridEscalados.Rows - 1
            bm = SSDBGridEscalados.AddItemBookmark(i)
            If (SSDBGridEscalados.Columns("FIN").CellValue(bm) = "") Then
                SSDBGridEscalados.Bookmark = bm
                oMensajes.escaladosComprobarRangos
                SSDBGridEscalados.Col = SSDBGridEscalados.Columns("FIN").Position
                ComprobarFormulario = False
                Exit Function
            End If
        Next i
    End If
    
    If HayQueCambiarUnidades() Then
        If oMensajes.CambioUnidadesAEscalados(Me.chkTodoProceso.Value) = vbNo Then
            ComprobarFormulario = False
            Exit Function
        End If
    End If
    
    ComprobarFormulario = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "ComprobarFormulario", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function



'--<summary>
'--Realiza las operaciones asociadas a aplicar los escalados
'--</summary>
'--<returns>Boolean true si la operci�n ha resultado correcta, false encaso contrario</returns>
'--<remarks>Llamada desde Comprobar Formulario</remarks>
'--<revision>DPD 02/12/2011</revision>
Function GuardarDatos() As Boolean

    Dim teserror As TipoErrorSummit
    Dim vPresupuestoGrupoAntesCambio ' Para el rec�lculo de presupuesto
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    vPresupuestoGrupoAntesCambio = frmPROCE.g_dPresGrupo
    
    
    Set oGrupoEsc = oFSGSRaiz.generar_cgrupo
    oGrupoEsc.Id = frmPROCE.g_oGrupoSeleccionado.Id
    Set oGrupoEsc.proceso = frmPROCE.g_oProcesoSeleccionado
    oGrupoEsc.CargarEscalados
    
    frmPROCE.g_bEscaladosComunes = frmPROCE.g_oProcesoSeleccionado.Escalados
    
    
    If Me.chkTodoProceso.Value = vbChecked Or Me.chkTodoProceso.Value = vbGrayed Then
        teserror = GuardarDatosProceso()
    Else
        teserror = GuardarDatosGrupo(frmPROCE.g_oGrupoSeleccionado)
    End If
    
    '�Hay errores?
    GuardarDatos = True
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        If teserror.NumError <> TESVolumenAdjDirSuperado Then
            GuardarDatos = False
            Exit Function
        End If
    End If
    
    'Actualizar presupuestos
    LockWindowUpdate frmPROCE.hWnd
    frmPROCE.CargarGridItems 0
    frmPROCE.g_oProcesoSeleccionado.PresAbierto = frmPROCE.g_oProcesoSeleccionado.PresAbierto - vPresupuestoGrupoAntesCambio + val(frmPROCE.g_dPresGrupo)
    frmPROCE.lblPresTotalProce.caption = Format(frmPROCE.g_oProcesoSeleccionado.PresAbierto, "#,##0.00####################")
    LockWindowUpdate 0&
    
    'Nota: Al hacer un cambio en la tabla PROCE_GR_ESC
    'se desencadena un trigger que comprobar� si todos los grupos del proceso tienen los mismos escalados
    ' y en caso afirmativo, se establecer� el valor ESCALADOS de la table PROCE_DEF a 1
    ' en caso contrario se establece a 0
    ' De esta manera se mantiene controlado si los escalados son v�lidos a nivel de proceso.
            
    frmPROCE.g_oProcesoSeleccionado.ComprobarEscalados
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "GuardarDatos", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function



'--<summary>
'--Realiza las operaciones asociadas a aplicar los escalados a todos los grupos del proceso
'--</summary>
'--<returns>Error en caso de producirse</returns>
'--<remarks>Llamada desde Comprobar Formulario</remarks>
'--<revision>DPD 02/12/2011</revision>
Function GuardarDatosProceso() As TipoErrorSummit

    Dim oGrupo As CGrupo
    Dim teserror As TipoErrorSummit
    Dim teserrorAux As TipoErrorSummit
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    teserror.NumError = TESnoerror
    teserrorAux.NumError = TESnoerror
    
    
    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
        teserrorAux = GuardarDatosGrupo(oGrupo)
        
        If teserrorAux.NumError <> TESnoerror And teserrorAux.NumError <> TESVolumenAdjDirSuperado Then
            teserror.NumError = teserrorAux.NumError
            teserror.Arg1 = teserror.Arg1 & ";" & teserrorAux.Arg1
            teserror.Arg1 = teserror.Arg2 & ";" & teserrorAux.Arg2
        End If
    Next

    GuardarDatosProceso = teserror
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "GuardarDatosProceso", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function




'--<summary>
'--Realiza las operaciones asociadas a aplicar los escalados al grupo
'--</summary>
'--<returns>Error en caso de producirse</returns>
'--<remarks>Llamada desde Comprobar Formulario</remarks>
'--<revision>DPD 02/12/2011</revision>
Public Function GuardarDatosGrupo(oGrupo As CGrupo) As TipoErrorSummit
    Dim oEscalados As CEscalados
    Dim oEscalado As CEscalado
    Dim Id As Long
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oEscalados = oFSGSRaiz.Generar_CEscalados
    Set oEscalado = oFSGSRaiz.Generar_CEscalado
    Dim i As Long
    Dim vb As Variant
    Dim bCrearNuevos As Boolean
    Dim bAplicaraProceso As Boolean
    
    'Comprobamos si estamos haciendo un cambio a un solo grupo o a todo el proceso
    If Me.chkTodoProceso.Value = vbChecked Or Me.chkTodoProceso.Value = vbGrayed Then
        'Se aplica a todo el proceso
        bAplicaraProceso = True
        If frmPROCE.g_bEscaladosComunes Then
            'Todos los grupos tienen escalados en com�n
        Else
            'Cada grupo tiene escalados distintos
            bCrearNuevos = True
            'Para el grupo en curso mantenemos los escalados
            'Para el resto, quitamos los que ten�a y le a�adimos nuevos
            If oGrupo.Id <> frmPROCE.g_oGrupoSeleccionado.Id Then
                oGrupo.CargarEscalados
                If oGrupo.Escalados Is Nothing Then
                    Set oGrupo.Escalados = oFSGSRaiz.Generar_CEscalados
                Else
                    oGrupo.Escalados.clear
                End If
            End If
        End If
    Else
        'Se aplica al grupo en curso
    End If


    If oGrupo.Escalados Is Nothing Then
        Set oGrupo.Escalados = oFSGSRaiz.Generar_CEscalados
    End If
        
    For i = 0 To SSDBGridEscalados.Rows - 1
        vb = SSDBGridEscalados.AddItemBookmark(i)
        If CStr(SSDBGridEscalados.Columns("ID").CellValue(vb)) = "" Then
            'Nuevo Escalado
            Id = 0
        Else
            'Escalado existente
            Id = SSDBGridEscalados.Columns("ID").CellValue(vb)
            If oGrupo.Id <> frmPROCE.g_oGrupoSeleccionado.Id Then
                ' Aplicar a todo el proceso
                'Para un grupo que no es el que hemos cargado en el grid
                If frmPROCE.g_bEscaladosComunes Then
                    'Buscamos el escalado equivalente para el grupo
                    Id = 0
                    For Each oEscalado In oGrupo.Escalados
                        If oEscalado.Inicial = oGrupoEsc.Escalados.Item(CStr(SSDBGridEscalados.Columns("ID").CellValue(vb))).Inicial Then
                            If oGrupo.Escalados.modo = ModoRangos Then
                                If oEscalado.final = oGrupoEsc.Escalados.Item(CStr(SSDBGridEscalados.Columns("ID").CellValue(vb))).final Then
                                    Id = oEscalado.Id
                                    Exit For
                                End If
                            Else
                                Id = oEscalado.Id
                                Exit For
                            End If
                        End If
                    Next
                Else
                    'Se han debido eliminar los escalados, as� que los creamos como nuevos
                    Id = 0
                End If
            End If
        End If
                
                
        If Id = 0 Then
            'A�adir Escalado
            If modo = ModoRangos Then
                Set oEscalado = oGrupo.Escalados.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(vb), SSDBGridEscalados.Columns("FIN").CellValue(vb), Null)
            Else
                Set oEscalado = oGrupo.Escalados.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(vb), Null, Null)
            End If
        Else
            'ModificarEscalado
            oGrupo.Escalados.Item(CStr(Id)).Inicial = SSDBGridEscalados.Columns("INI").CellValue(vb)
            If modo = ModoRangos Then
                oGrupo.Escalados.Item(CStr(Id)).final = SSDBGridEscalados.Columns("FIN").CellValue(vb)
            End If
        
        End If
        
    Next i
    
    
    'Ahora comprobamos si se ha quitado alguno de los escalados que hab�a previamente
    Dim bQuitarEscalado As Boolean
    For Each oEscalado In oGrupo.Escalados
        bQuitarEscalado = True
        For i = 0 To SSDBGridEscalados.Rows - 1
            vb = SSDBGridEscalados.AddItemBookmark(i)
            If modo = ModoDirecto Then
                If oEscalado.Inicial = CStr(SSDBGridEscalados.Columns("INI").CellValue(vb)) Then
                    bQuitarEscalado = False
                    Exit For
                End If
            Else
                If oEscalado.Inicial = CStr(SSDBGridEscalados.Columns("INI").CellValue(vb)) And oEscalado.final = CStr(SSDBGridEscalados.Columns("FIN").CellValue(vb)) Then
                    bQuitarEscalado = False
                    Exit For
                End If
            End If
        Next i
        
        If bQuitarEscalado Then
            oGrupo.Escalados.Remove (CStr(oEscalado.Id))
        End If
    Next
    
    
    
    
    
    Set oGrupo.Escalados.Grupo = oGrupo
    oGrupo.UnidadEscalado = Me.sdbcUnidadCod.Value

    If oGrupo.Escalados.Count > 0 Then
        oGrupo.UsarEscalados = 1
        oGrupo.TipoEscalados = modo
        oGrupo.Escalados.modo = modo
    Else
        oGrupo.UsarEscalados = 0
        oGrupo.TipoEscalados = ModoRangos
        
    End If
    
    Dim oItems As CItems
    Set oItems = oFSGSRaiz.Generar_CItems
    'Colecci�n para almacenar los valores de los �tems que cambien al aplicar los escalados
    
    
    oGrupo.Items.EstablecerUnidadesItems oGrupo.UnidadEscalado, oItems
    oGrupo.Items.EstablecerPresupuestos oGrupo.Escalados, oItems
    
    
    Dim teserror As TipoErrorSummit
    'Guardar los escalados
    'Aqu� se inicia una operaci�n transaccional que guarde los escalados para un grupo
    'y establece los cambios a todos los items del grupo
    
    teserror = oGrupo.GuardarEscalados(oItems, , , , oUsuarioSummit.Cod)
    
    If teserror.NumError <> TESnoerror Then
        If teserror.NumError <> TESVolumenAdjDirSuperado Then
            teserror.Arg1 = oGrupo.Codigo
        End If
    End If
    
    
    
    GuardarDatosGrupo = teserror
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "GuardarDatosGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function






'--<summary>
'--Crea un objeto CEscalados de forma temporal con los datos del grid
'--Para poder hacer comprobaciones previas al guardado
'--</summary>
'--<remarks>Bot�n Aceptar</remarks>
'--<revision>DPD 08/08/2011</revision>
Sub GuardarDatosAux(Optional ByVal paramModo As TModoEscalado)

Dim modoaux As TModoEscalado
Dim Escalado As CEscalado
Dim Id As Long
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set m_oEscaladosAux = oFSGSRaiz.Generar_CEscalados
Set Escalado = oFSGSRaiz.Generar_CEscalado
Dim i As Long
Dim vb As Variant

    If IsMissing(paramModo) Then modoaux = modo Else modoaux = paramModo

    m_oEscaladosAux.clear

    For i = 0 To SSDBGridEscalados.Rows - 1
        vb = SSDBGridEscalados.AddItemBookmark(i)
        If CStr(SSDBGridEscalados.Columns("ID").CellValue(vb)) = "" Then
            Id = 0
        Else
            Id = SSDBGridEscalados.Columns("ID").CellValue(vb)
        End If
            
        If paramModo = ModoRangos Then
           Set Escalado = m_oEscaladosAux.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(vb), SSDBGridEscalados.Columns("FIN").CellValue(vb))
        Else
            Set Escalado = m_oEscaladosAux.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(vb), Null)
        End If
        
    Next i
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "GuardarDatosAux", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub







Private Sub SSDBGridEscalados_KeyPress(KeyAscii As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyEscape Then

        If SSDBGridEscalados.DataChanged = False Then

            SSDBGridEscalados.CancelUpdate
            SSDBGridEscalados.DataChanged = False

        Else

            If SSDBGridEscalados.IsAddRow Then

                DoEvents
                SSDBGridEscalados.CancelUpdate
                SSDBGridEscalados.DataChanged = False


            End If

        End If
    
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "SSDBGridEscalados_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

'--<summary>Comprueba si hay que cambiar las unidades de los �tems existentes para aplicar escalados</summary>
'--<returns>True si hay distintas unidades en los items false si todas las unidades son iguales</returns>
'--<remarks>Llamada desde Comprobar Formulario</remarks>
'--<revision>DPD 04/08/2011</revision>

Private Function HayQueCambiarUnidades() As Boolean
    Dim bCambiarUnidades As Boolean
    Dim i As Long
    Dim uni As String
    Dim uniAnt As String
    Dim bm As Variant
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.chkTodoProceso.Value = vbChecked Or Me.chkTodoProceso.Value = vbGrayed Then
        'Para todos los items de todos los grupos del proceso
        Dim oGrupo As CGrupo
        Dim oItem As CItem
        For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
            If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    uni = oItem.UniCod
                    If uniAnt <> "" Then
                        If uni <> uniAnt Then
                            HayQueCambiarUnidades = True
                            Exit Function
                        End If
                    End If
                    uniAnt = uni
                Next
            End If
        Next
    Else
        'para los items del grupo (mostrados en frmproce)
    
        'Para cada item
        For i = 0 To frmPROCE.sdbgItems.Rows - 1
            'Obtenemos la cantidad
            bm = frmPROCE.sdbgItems.AddItemBookmark(i)
            uni = frmPROCE.sdbgItems.Columns("UNI").CellValue(bm)
            If uniAnt <> "" Then
                If uni <> uniAnt Then
                    bCambiarUnidades = True
                    Exit For
                End If
            End If
            uniAnt = uni
        Next i
    End If
    HayQueCambiarUnidades = bCambiarUnidades
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "HayQueCambiarUnidades", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function




'--<summary>
'--Establece los presupuestos (unitarios y totales) de los items al aplicar los escalados
'--Para todos los items del proceso
'--</summary>
'--<remarks>Llamada desde GuardarDatos</remarks>
'--<revision>DPD 08/09/2011</revision>
Sub EstablecerPrespupuestosProce()

    Dim oGrupo As CGrupo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
        ' @@DPD Quitar EstablecerPrespupuestos oGrupo
    Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "EstablecerPrespupuestosProce", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

''' <summary>Establece el modo del formulario en modo edici�n</summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 04/10/2012</revision>

Sub ModoEdicion()
    Dim bUnidadEdit As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.cmdAceptar.Visible = True
    Me.cmdCancelar.Visible = True
    Me.cmdCerrar.Visible = False
    Me.FrameEscalado.Enabled = True
    
    bUnidadEdit = True
    If gParametrosGenerales.gbOBLUnidadMedida Then
        If sdbcUnidadCod.Text <> "" Then
            bUnidadEdit = False
        End If
    End If
    
    If bUnidadEdit Then
        picUnidades.Enabled = True
        sdbcUnidadCod.BackColor = &HFFFFFF  'Blanco
        sdbcUnidadDen.BackColor = &HFFFFFF
    Else
        picUnidades.Enabled = False
        sdbcUnidadCod.BackColor = RGB(192, 255, 255)
        sdbcUnidadDen.BackColor = RGB(192, 255, 255)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "ModoEdicion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Establece el modo del formulario en modo consulta
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo: 0,1</remarks>

Sub ModoConsulta()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.cmdAceptar.Visible = False
    Me.cmdCancelar.Visible = False
    Me.cmdCerrar.Visible = True
    Me.FrameEscalado.Enabled = False
    Me.SSDBGridEscalados.AllowAddNew = False
    Me.SSDBGridEscalados.AllowUpdate = False
    Me.SSDBGridEscalados.AllowDelete = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "ModoConsulta", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Inicializa la lista de Escalados
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo: 0,1</remarks>

Sub PrepararLista()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not Me.g_oGrupoSeleccionado.Escalados Is Nothing Then
        Dim cEsc As CEscalado
        Dim Linea As String
        For Each cEsc In Me.g_oGrupoSeleccionado.Escalados
            Linea = cEsc.Inicial & Chr(m_lSeparador) & cEsc.Inicial & Chr(m_lSeparador)
            If Me.modo = ModoRangos Then
                Linea = Linea & cEsc.final & Chr(m_lSeparador)
            Else
                Linea = Linea & "" & Chr(m_lSeparador)
            End If
            Linea = Linea & cEsc.Id
            
            Me.SSDBGridEscalados.AddItem Linea
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "PrepararLista", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub



''' <summary>
''' Establecer el multiidioma
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCEESCALADOS, basPublic.gParametrosInstalacion.gIdioma)
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value ' Escalado de precios
        Me.SSDBGridEscalados.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkTodoProceso.caption = Ador(0).Value 'Aplicar a todo el proceso
        Ador.MoveNext
        Me.optRangosCantidades.caption = Ador(0).Value ' Rangos de cantidades
        Ador.MoveNext
        Me.optCantidadesDirectas.caption = Ador(0).Value ' Cantidades directas
        Ador.MoveNext
        Me.lblSelecUnid.caption = Ador(0).Value 'Seleccione la unidad de medida para los items:
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value ' Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value ' Cancelar
        Ador.MoveNext
        Me.SSDBGridEscalados.Columns.Item("INI").caption = Ador(0).Value ' Cantidad inicial
        Ador.MoveNext
        Me.SSDBGridEscalados.Columns.Item("FIN").caption = Ador(0).Value ' Cantidad final
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value 'Cerrar
        Ador.MoveNext
        Me.SSDBGridEscalados.Columns.Item("DIR").caption = Ador(0).Value ' Cantidad directa
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Establece el modo de escalado en Rangos
''' </summary>
''' <remarks>Llamada desde:form optRangosCantidades; Tiempo m�ximo: 0,1</remarks>

Function ModoARangos() As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If SSDBGridEscalados.DataChanged Then
        SSDBGridEscalados.Update
        'SSDBGridEscalados_BeforeUpdate (False)
    End If
        
    If SSDBGridEscalados_enEdicion Then
        'Ha habido problemas con el Update
        m_bOptionAuto = True
        Me.optCantidadesDirectas.Value = True
        SSDBGridEscalados_enEdicion = False
        ModoARangos = False
        Exit Function
    End If

    
    SSDBGridEscalados.Columns("DIR").Visible = False
    SSDBGridEscalados.Columns("INI").Visible = True
    SSDBGridEscalados.Columns("FIN").Visible = True
    modo = ModoRangos
    'If Not Me.optRangosCantidades.Value Then Me.optRangosCantidades.Value = True
    ModoARangos = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "ModoARangos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


''' <summary>
''' Establece el modo de escalado en Cantidades directas
''' </summary>
''' <remarks>Llamada desde:form optCantidadesDirectas; Tiempo m�ximo: 0,1</remarks>

Function ModoADirecto(Optional ByVal bCambio As Boolean) As Boolean

    'Si ten�a rangos, borro la columna final para forzar que los vuelvan a escribir
    Dim i As Long
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    modo = ModoDirecto
    If bCambio Then
        If SSDBGridEscalados.DataChanged Then
            'SSDBGridEscalados_BeforeUpdate (False)
            SSDBGridEscalados.Columns("DIR").Value = SSDBGridEscalados.Columns("INI").Value
            SSDBGridEscalados.Update
            
            If SSDBGridEscalados.DataChanged Then
                ModoADirecto = False
                Exit Function
            End If
            
            

        End If
        If SSDBGridEscalados_enEdicion Then
            ' Ha habido problemas con el Update
            SSDBGridEscalados_enEdicion = False
            ModoADirecto = False
            Exit Function
        End If
        For i = 0 To SSDBGridEscalados.Rows - 1
            SSDBGridEscalados.Bookmark = SSDBGridEscalados.AddItemBookmark(i)
            SSDBGridEscalados.Columns("FIN").Value = ""
        Next i
    End If
    
    SSDBGridEscalados.Columns("DIR").Visible = True
    SSDBGridEscalados.Columns("INI").Visible = False
    SSDBGridEscalados.Columns("FIN").Visible = False
    
    If Not Me.optCantidadesDirectas.Value Then
        m_bOptionAuto = True
        Me.optCantidadesDirectas.Value = True
    End If
    ModoADirecto = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscalados", "ModoADirecto", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

