VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmFormAnyaCampos 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DAnyadir campo"
   ClientHeight    =   6240
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7965
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormAnyaCampos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   7965
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Height          =   315
      Left            =   2760
      TabIndex        =   21
      Top             =   5760
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   3960
      TabIndex        =   20
      Top             =   5760
      Width           =   975
   End
   Begin VB.Frame fraNuevo 
      BackColor       =   &H00808000&
      ForeColor       =   &H00FFFFFF&
      Height          =   5475
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   7695
      Begin VB.PictureBox picDatos 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   4620
         Left            =   180
         ScaleHeight     =   4620
         ScaleMode       =   0  'User
         ScaleWidth      =   7055.42
         TabIndex        =   9
         Top             =   720
         Width           =   7200
         Begin VB.CheckBox chkRecordarValores 
            BackColor       =   &H00808000&
            Caption         =   "DRecordar valores"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   5160
            TabIndex        =   19
            Top             =   0
            Visible         =   0   'False
            Width           =   2055
         End
         Begin VB.Frame FraLista 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            ForeColor       =   &H80000008&
            Height          =   3735
            Left            =   0
            TabIndex        =   14
            Top             =   840
            Width           =   7348
            Begin VB.CheckBox chkNoFecAntSis 
               BackColor       =   &H00808000&
               Caption         =   "DNo permitir fecha anterior a la fecha del sistema"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Left            =   0
               MaskColor       =   &H00FFFFFF&
               TabIndex        =   37
               Top             =   840
               Visible         =   0   'False
               Width           =   5175
            End
            Begin VB.CommandButton cmdComprob 
               Height          =   285
               Left            =   6720
               Picture         =   "frmFormAnyaCampos.frx":0CB2
               Style           =   1  'Graphical
               TabIndex        =   36
               Top             =   780
               Visible         =   0   'False
               Width           =   321
            End
            Begin VB.TextBox txtValorComprob 
               Height          =   285
               Left            =   2040
               TabIndex        =   35
               Top             =   780
               Visible         =   0   'False
               Width           =   4580
            End
            Begin VB.TextBox txtFormato 
               Height          =   285
               Left            =   2040
               MaxLength       =   500
               TabIndex        =   33
               Top             =   390
               Visible         =   0   'False
               Width           =   4965
            End
            Begin VB.Frame FraListasEnlazadas 
               BackColor       =   &H00808000&
               BorderStyle     =   0  'None
               Caption         =   "Frame1"
               Height          =   375
               Left            =   0
               TabIndex        =   29
               Top             =   1590
               Visible         =   0   'False
               Width           =   4815
               Begin SSDataWidgets_B.SSDBCombo sdbcListaCampoPadre 
                  Height          =   285
                  Left            =   2040
                  TabIndex        =   30
                  Top             =   0
                  Width           =   2725
                  DataFieldList   =   "Column 1"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  ListWidthAutoSize=   0   'False
                  AllowNull       =   0   'False
                  ListWidth       =   4833
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   -2147483640
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "ID"
                  Columns(0).Name =   "ID"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   11456
                  Columns(1).Caption=   "DENOMINACION"
                  Columns(1).Name =   "DENOMINACION"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "TIPO_CAMPO_GS"
                  Columns(2).Name =   "TIPO_CAMPO_GS"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   4807
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblCampoPadre 
                  BackColor       =   &H00808000&
                  Caption         =   "dCampo padre:"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   195
                  Left            =   0
                  TabIndex        =   31
                  Top             =   0
                  Width           =   1275
               End
            End
            Begin VB.Frame FrameBotonesGrid 
               Appearance      =   0  'Flat
               BackColor       =   &H00808000&
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   495
               Left            =   4560
               TabIndex        =   22
               Top             =   1560
               Width           =   2415
               Begin VB.CommandButton cmdImportarExcel 
                  Enabled         =   0   'False
                  Height          =   285
                  Left            =   480
                  Picture         =   "frmFormAnyaCampos.frx":1024
                  Style           =   1  'Graphical
                  TabIndex        =   28
                  Top             =   120
                  Width           =   321
               End
               Begin VB.CommandButton cmdMvtoElemLista 
                  Enabled         =   0   'False
                  Height          =   285
                  Index           =   1
                  Left            =   1995
                  Picture         =   "frmFormAnyaCampos.frx":1536
                  Style           =   1  'Graphical
                  TabIndex        =   26
                  Top             =   120
                  UseMaskColor    =   -1  'True
                  Width           =   321
               End
               Begin VB.CommandButton cmdMvtoElemLista 
                  Enabled         =   0   'False
                  Height          =   285
                  Index           =   0
                  Left            =   1605
                  Picture         =   "frmFormAnyaCampos.frx":1878
                  Style           =   1  'Graphical
                  TabIndex        =   25
                  Top             =   120
                  UseMaskColor    =   -1  'True
                  Width           =   321
               End
               Begin VB.CommandButton cmdAnyaValor 
                  Enabled         =   0   'False
                  Height          =   285
                  Left            =   840
                  Picture         =   "frmFormAnyaCampos.frx":1BBA
                  Style           =   1  'Graphical
                  TabIndex        =   24
                  Top             =   120
                  Width           =   321
               End
               Begin VB.CommandButton cmdElimValor 
                  Enabled         =   0   'False
                  Height          =   285
                  Left            =   1200
                  Picture         =   "frmFormAnyaCampos.frx":1C3C
                  Style           =   1  'Graphical
                  TabIndex        =   23
                  Top             =   120
                  Width           =   321
               End
            End
            Begin VB.CheckBox chkLista 
               BackColor       =   &H00808000&
               Caption         =   "DEdici�n mediante selecci�n en lista de valores"
               Enabled         =   0   'False
               ForeColor       =   &H00FFFFFF&
               Height          =   192
               Left            =   0
               TabIndex        =   5
               Top             =   1290
               Width           =   3980
            End
            Begin VB.TextBox txtMin 
               Enabled         =   0   'False
               Height          =   285
               Left            =   2041
               TabIndex        =   3
               Top             =   0
               Width           =   1272
            End
            Begin VB.TextBox txtMax 
               Enabled         =   0   'False
               Height          =   285
               Left            =   5730
               TabIndex        =   4
               Top             =   0
               Width           =   1272
            End
            Begin SSDataWidgets_B.SSDBGrid sdbgLista 
               Height          =   1695
               Left            =   0
               TabIndex        =   6
               Top             =   2065
               Width           =   6975
               ScrollBars      =   2
               _Version        =   196617
               DataMode        =   2
               stylesets.count =   1
               stylesets(0).Name=   "Normal"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   7.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmFormAnyaCampos.frx":1CCE
               AllowAddNew     =   -1  'True
               AllowDelete     =   -1  'True
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               RowSelectionStyle=   1
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   9790
               Columns(0).Caption=   "VALOR"
               Columns(0).Name =   "VALOR"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   12303
               _ExtentY        =   2990
               _StockProps     =   79
               ForeColor       =   0
               BackColor       =   16777215
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblValorComprob 
               BackColor       =   &H00808000&
               Caption         =   "DValor comprobaci�n:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   34
               Top             =   840
               Visible         =   0   'False
               Width           =   1695
            End
            Begin VB.Label lblFormato 
               BackColor       =   &H00808000&
               Caption         =   "DFormato:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   32
               Top             =   450
               Visible         =   0   'False
               Width           =   1275
            End
            Begin VB.Label lblMin 
               BackColor       =   &H00808000&
               Caption         =   "DM�nimo"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   16
               Top             =   30
               Width           =   1275
            End
            Begin VB.Label lblMax 
               BackColor       =   &H00808000&
               Caption         =   "DM�ximo"
               ForeColor       =   &H00FFFFFF&
               Height          =   285
               Left            =   3690
               TabIndex        =   15
               Top             =   0
               Width           =   1275
            End
         End
         Begin VB.Frame fraMaxLength 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            ForeColor       =   &H80000008&
            Height          =   450
            Left            =   0
            TabIndex        =   11
            Top             =   300
            Width           =   7143
            Begin VB.TextBox txtMaxLength 
               Height          =   315
               Left            =   2040
               MaxLength       =   5
               TabIndex        =   17
               Top             =   120
               Width           =   600
            End
            Begin MSComCtl2.UpDown UpdMaxLength 
               Height          =   315
               Left            =   2641
               TabIndex        =   18
               Top             =   120
               Width           =   255
               _ExtentX        =   450
               _ExtentY        =   556
               _Version        =   393216
               Value           =   1
               BuddyControl    =   "txtMaxLength"
               BuddyDispid     =   196636
               OrigLeft        =   2880
               OrigTop         =   120
               OrigRight       =   3120
               OrigBottom      =   435
               Max             =   4000
               Min             =   1
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin VB.Label lblMaxLength 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DN� m�ximo de caracteres"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   13
               Top             =   120
               Width           =   1905
            End
            Begin VB.Label lblHastaCaracteres 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "(hasta XX caracteres)"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   3200
               TabIndex        =   12
               Top             =   135
               Visible         =   0   'False
               Width           =   1560
            End
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcTipo 
            Height          =   285
            Left            =   2041
            TabIndex        =   2
            Top             =   0
            Width           =   2712
            DataFieldList   =   "Column 1"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            MaxDropDownItems=   9
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4736
            Columns(1).Caption=   "TIPO"
            Columns(1).Name =   "TIPO"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4784
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblTipo 
            BackColor       =   &H00808000&
            Caption         =   "DTipo:"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   0
            TabIndex        =   10
            Top             =   20
            Width           =   1275
         End
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   2200
         TabIndex        =   0
         Top             =   360
         Width           =   4995
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgDen 
         Height          =   615
         Left            =   2205
         TabIndex        =   1
         Top             =   360
         Visible         =   0   'False
         Width           =   4950
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2170
         Columns(0).Caption=   "IDI"
         Columns(0).Name =   "IDI"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   14671839
         Columns(1).Width=   5477
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "COD_IDI"
         Columns(2).Name =   "COD_IDI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8731
         _ExtentY        =   1085
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblNombre 
         BackColor       =   &H00808000&
         Caption         =   "DNombre:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   180
         TabIndex        =   8
         Top             =   375
         Width           =   1275
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValoresLista 
      Height          =   1140
      Left            =   5160
      TabIndex        =   27
      Top             =   5760
      Width           =   3510
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormAnyaCampos.frx":1CEA
      BevelColorFace  =   12632256
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "CAMPO_PADRE"
      Columns(0).Name =   "CAMPO_PADRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ORDEN"
      Columns(1).Name =   "ORDEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3201
      Columns(2).Caption=   "TEXTO"
      Columns(2).Name =   "TEXTO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   6191
      _ExtentY        =   2011
      _StockProps     =   77
   End
   Begin MSComDlg.CommonDialog cmmdExcel 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
      MaxFileSize     =   1500
   End
End
Attribute VB_Name = "frmFormAnyaCampos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oIdiomas As CIdiomas
Public g_sOrigen As String
Public g_lIdGrupo As Long

Private m_bRespetarLista As Boolean
Private m_bMultiIdioma As Boolean
Private m_bError As Boolean

'Variables de idiomas:
Private m_sIdiForm As String
Private m_sIdiCaption As String
Private m_sIdiTipo(2 To 11) As String
Private m_sIdiNombre As String
Private m_sIdiTp As String
Private m_sIdiValor As String
Private m_sIdiMaximo As String
Private m_sIdiMinimo As String
Private m_sAtributoGS As String
Private m_sMensaje(1 To 4) As String
Private m_sMensajeHasta(1 To 3) As String
Private m_lCampoPadreSeleccionado As Long

Private m_sMaxLength As String
Private m_sTipo As String

Private m_iNumeroPadres As Integer
Private m_bForzarCloseUp As Boolean
Private Sub chkLista_Click()
Dim oIdioma As CIdioma

    If sdbgLista.DataChanged Then
        m_bError = False
        sdbgLista.Update
        If m_bError Then
            sdbgLista.CancelUpdate
            sdbgLista.Refresh
        End If
    End If

    If txtMaxLength.Text <> "" Then
        If Comprobar_txtMaxLength = False Then
            If Me.Visible Then txtMaxLength.SetFocus
            Exit Sub
        End If
    End If
    chkNoFecAntSis.Enabled = Not (chkLista.Value = vbChecked)
    If chkLista.Value = vbChecked Then
        chkNoFecAntSis.Value = 0
        
        cmdAnyaValor.Enabled = True
        cmdMvtoElemLista(0).Enabled = True
        cmdMvtoElemLista(1).Enabled = True
        cmdImportarExcel.Enabled = True
        lblFormato.Visible = False
        txtFormato.Visible = False
        lblValorComprob.Visible = False
        txtValorComprob.Visible = False
        cmdComprob.Visible = False
        sdbgLista.AllowUpdate = True
        
        FraListasEnlazadas.Visible = (sdbcListaCampoPadre.Rows > 0 And g_sOrigen = "frmFormularios")
        
        If sdbgLista.Columns.Count > 1 And m_bMultiIdioma Then
            Select Case sdbcTipo.Columns("ID").Value
                Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                    For Each oIdioma In g_oIdiomas
                        sdbgLista.Columns(oIdioma.Cod).FieldLen = val(txtMaxLength.Text)
                    Next
                Case Else
                    sdbgLista.Columns.Item(m_sIdiValor).FieldLen = val(txtMaxLength.Text)
            End Select
        Else
            sdbgLista.Columns.Item(m_sIdiValor).FieldLen = val(txtMaxLength.Text)
        
        End If
    Else
        cmdAnyaValor.Enabled = False
        cmdElimValor.Enabled = False
        cmdMvtoElemLista(0).Enabled = False
        cmdMvtoElemLista(1).Enabled = False
        cmdImportarExcel.Enabled = False
        
        sdbgLista.AllowUpdate = False
        m_bForzarCloseUp = True
        sdbcTipo_CloseUp
        m_bForzarCloseUp = False
        FraListasEnlazadas.Visible = False
    End If
    
    txtMax.Text = ""
    txtMin.Text = ""
    If sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoFecha And sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoNumerico Then
        txtMax.Enabled = False
        txtMin.Enabled = False
    Else
        If chkLista.Value = vbChecked Then
            txtMax.Enabled = False
            txtMin.Enabled = False
        Else
            txtMax.Enabled = True
            txtMin.Enabled = True
        End If
    End If
    
    MostrarOpcionRecordarValores
       
End Sub

Private Sub chkRecordarValores_Click()
    ComprobarCheckLista
End Sub

''' <summary>
''' Graba el campo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oIdioma As CIdioma
    Dim oDenominaciones As CMultiidiomas
    Dim oCampo As CFormItem
    Dim i As Integer
    Dim vbm As Variant
    Dim oValores As CCampoValorListas
    Dim oValoresTxt As CMultiidiomas
    Dim oCampos As CFormItems
    Dim oCampoPredef As CCampoPredef
    Dim oCamposPredef As CCamposPredef
    Dim oCampo2 As CFormItem
    Dim arGMNs() As String
    Dim j As Integer
    Dim k As Integer
                        
    If sdbgLista.DataChanged Then
        m_bError = False
        sdbgLista.Update
        If m_bError Then
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If
    
    If txtMaxLength.Text <> "" Then
        If Comprobar_txtMaxLength = False Then
            If Me.Visible Then txtMaxLength.SetFocus
            Exit Sub
        End If
    End If
    
    'Comprobacion del valor m�nimo
    If txtMin.Text <> "" Then
        If Comprobar_txtMin = False Then
            If Me.Visible Then txtMin.SetFocus
            Exit Sub
        End If
    End If
    
    'Comprobacion del valor m�ximo
    If txtMax.Text <> "" Then
        If Comprobar_txtMax = False Then
            If Me.Visible Then txtMax.SetFocus
            Exit Sub
        End If
    End If
    
    If txtMin.Text <> "" And txtMax.Text <> "" Then
        If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
            If CDbl(txtMin.Text) > CDbl(txtMax.Text) Then
                oMensajes.NoValido m_sIdiMinimo & ". " & m_sMensaje(4)
                If Me.Visible Then txtMin.SetFocus
                Exit Sub
            End If
        Else
            If CDate(txtMin.Text) > CDate(txtMax.Text) Then
                oMensajes.NoValido m_sIdiMinimo & ". " & m_sMensaje(4)
                If Me.Visible Then txtMin.SetFocus
                Exit Sub
            End If
        End If
    End If
    
    If m_bMultiIdioma Then
        'Tiene que introducir la denominaci�n por lo menos en el idioma de la aplicaci�n:
        sdbgDen.MoveFirst
        For i = 0 To sdbgDen.Rows - 1
            vbm = sdbgDen.AddItemBookmark(i)
            
            If sdbgDen.Columns("DEN").CellValue(vbm) = "" Then
                oMensajes.NoValido sdbgDen.Columns("IDI").CellValue(vbm)
                Exit Sub
            End If
        Next i
    Else
        If Trim(txtNombre.Text) = "" Then
            oMensajes.NoValido m_sIdiNombre
            If Me.Visible Then txtNombre.SetFocus
            Exit Sub
        End If
    End If
    
    If sdbcTipo.Text = "" Then
        oMensajes.NoValido m_sIdiTp
        If Me.Visible Then sdbcTipo.SetFocus
        Exit Sub
    End If
    
    'Si es de tipo lista comprueba que se haya introducido alg�n valor a la lista:
    If chkLista.Value = vbChecked Then
        If sdbgLista.Rows = 0 Then
            oMensajes.IntroducirListaValores
            Exit Sub
        Else
            sdbgLista.MoveFirst
            If m_bError Then
                If Me.Visible Then sdbgLista.SetFocus
                Exit Sub
            End If
            If m_bMultiIdioma And (sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo) Then
                If sdbgLista.Columns(CStr(gParametrosInstalacion.gIdioma)).Value = "" Then
                    oMensajes.IntroducirListaValores
                    Exit Sub
                End If
            Else
                If sdbgLista.Columns("VALOR").Value = "" Then
                    oMensajes.IntroducirListaValores
                    Exit Sub
                End If
            End If
        End If
    End If
        
    'A�ade el campo nuevo a BD:
    Screen.MousePointer = vbHourglass

    If g_sOrigen = "frmPARTipoSolicit" Or g_sOrigen = "frmPARTipoSolicitDesglose" Then 'Es un campo de tipo de solicitud
        Set oCampoPredef = oFSGSRaiz.Generar_CCampoPredef
        
        oCampoPredef.TipoSolicitud = frmPARTipoSolicit.g_oTipoSeleccionado.Id
        oCampoPredef.Tipo = sdbcTipo.Columns("ID").Value
        
        'Lo a�ade a la colecci�n y grid de formularios
        If oCampoPredef.Tipo = TiposDeAtributos.TipoNumerico Then
            oCampoPredef.MinFec = Null
            oCampoPredef.MaxFec = Null
            oCampoPredef.MinNum = Trim(txtMin.Text)
            oCampoPredef.MaxNum = Trim(txtMax.Text)
        ElseIf oCampoPredef.Tipo = TiposDeAtributos.TipoFecha Then
            oCampoPredef.MinFec = Trim(txtMin.Text)
            oCampoPredef.MaxFec = Trim(txtMax.Text)
            oCampoPredef.MinNum = Null
            oCampoPredef.MaxNum = Null
        Else
            oCampoPredef.MinFec = Null
            oCampoPredef.MaxFec = Null
            oCampoPredef.MinNum = Null
            oCampoPredef.MaxNum = Null
        End If
            
        If chkLista.Value = vbChecked Then
            oCampoPredef.TipoIntroduccion = Introselec
        Else
            oCampoPredef.TipoIntroduccion = IntroLibre
        End If
        oCampoPredef.idAtrib = Null
        oCampoPredef.TipoCampoGS = Null
        If g_sOrigen = "frmPARTipoSolicit" Then
            oCampoPredef.EsSubCampo = False
        Else
            oCampoPredef.EsSubCampo = True
            Set oCampoPredef.CampoPadre = frmPARTipoSolicit.g_ofrmDesglose.g_oCampoDesglose
        End If
        oCampoPredef.TipoPredef = TipoCampoPredefinido.Normal
        
        'Carga las denominaciones del grupo de datos generales en todos los idiomas
        Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdioma In g_oIdiomas
            For i = 0 To sdbgDen.Rows - 1
                vbm = sdbgDen.AddItemBookmark(i)
                If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                    oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                    Exit For
                End If
            Next i
        Next
        Set oCampoPredef.Denominaciones = oDenominaciones
    
        'Carga la colecci�n de valores de lista:
        If chkLista.Value = vbChecked Then
            sdbgLista.MoveFirst
            Set oValores = oFSGSRaiz.Generar_CCampoValorListas
            For i = 0 To sdbgLista.Rows - 1
                vbm = sdbgLista.AddItemBookmark(i)
                
                Select Case oCampoPredef.Tipo
                    Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                        Set oValoresTxt = oFSGSRaiz.Generar_CMultiidiomas
                        For Each oIdioma In g_oIdiomas
                            If (m_bMultiIdioma = True) Then
                                If sdbgLista.Columns(oIdioma.Cod).CellValue(vbm) <> "" Then
                                    oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns(oIdioma.Cod).CellValue(vbm)
                                End If
                            Else
                                If sdbgLista.Columns("VALOR").CellValue(vbm) <> "" Then
                                    oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns("VALOR").CellValue(vbm)
                                End If
                            End If
                        Next
                        oValores.Add i + 1, , , oValoresTxt, , , , oCampoPredef
                        Set oValoresTxt = Nothing
                        
                    Case TiposDeAtributos.TipoNumerico
                        If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                            oValores.Add i + 1, oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , , oCampoPredef
                        End If
                        
                    Case TiposDeAtributos.TipoFecha
                        If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                            oValores.Add i + 1, oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , oCampoPredef
                        End If
                        
                End Select
            Next i
            Set oCampoPredef.ValoresLista = oValores
        End If
        
        Set oIBaseDatos = oCampoPredef
        udtTeserror = oIBaseDatos.AnyadirABaseDatos
    
        If udtTeserror.NumError = TESnoerror Then
            Set oCamposPredef = oFSGSRaiz.Generar_CCamposPredef
            If g_sOrigen = "frmPARTipoSolicit" Then
                oCamposPredef.Add oCampoPredef.Id, oCampoPredef.TipoSolicitud, oCampoPredef.Denominaciones, oCampoPredef.Ayudas, oCampoPredef.TipoPredef, oCampoPredef.Tipo, oCampoPredef.TipoIntroduccion, oCampoPredef.MinNum, oCampoPredef.MaxNum, oCampoPredef.MinFec, oCampoPredef.MaxFec, oCampoPredef.TipoCampoGS, oCampoPredef.idAtrib, oCampoPredef.Orden, oCampoPredef.EsSubCampo, oCampoPredef.FECACT
                frmPARTipoSolicit.AnyadirCampos oCamposPredef
                frmPARTipoSolicit.g_Accion = ACCTipoSolicitudCons
            Else
                oCamposPredef.Add oCampoPredef.Id, oCampoPredef.TipoSolicitud, oCampoPredef.Denominaciones, oCampoPredef.Ayudas, oCampoPredef.TipoPredef, oCampoPredef.Tipo, oCampoPredef.TipoIntroduccion, oCampoPredef.MinNum, oCampoPredef.MaxNum, oCampoPredef.MinFec, oCampoPredef.MaxFec, oCampoPredef.TipoCampoGS, oCampoPredef.idAtrib, oCampoPredef.Orden, oCampoPredef.EsSubCampo, oCampoPredef.FECACT, oCampoPredef.CampoPadre
                frmPARTipoSolicit.g_ofrmDesglose.AnyadirCampos oCamposPredef
                frmPARTipoSolicit.g_ofrmDesglose.g_Accion = ACCTipoSolicitudCons
            End If
            Set oCamposPredef = Nothing
            RegistrarAccion ACCTipoSolicitudItemAnyadir, "Id:" & oCampoPredef.Id
        Else
            Screen.MousePointer = vbNormal
            TratarError udtTeserror
            Exit Sub
        End If
        
        Set oCampoPredef = Nothing
    Else
        'Es un campo de formulario
        Set oCampo = oFSGSRaiz.Generar_CFormCampo
        
        If g_sOrigen = "frmFormularios" Then
            Set oCampo.Grupo = frmFormularios.g_oGrupoSeleccionado
        Else
            Set oCampo.Grupo = frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Grupo
        End If
        oCampo.Tipo = sdbcTipo.Columns("ID").Value
        oCampo.Minimo = Trim(txtMin.Text)
        oCampo.Maximo = Trim(txtMax.Text)
        If chkLista.Value = vbChecked Then
            oCampo.TipoIntroduccion = Introselec
        Else
            oCampo.TipoIntroduccion = IntroLibre
        End If
        oCampo.idAtrib = Null
        oCampo.CampoGS = Null
        oCampo.Formato = txtFormato.Text
        oCampo.TipoPredef = TipoCampoPredefinido.Normal
        If txtMaxLength.Text = "" Then
            oCampo.MaxLength = Null
        Else
            oCampo.MaxLength = val(txtMaxLength.Text)
        End If
        If oCampo.Tipo = TipoDesglose Then
            oCampo.MostrarPOPUP = True
        End If
        
        oCampo.NoFecAntSis = SQLBinaryToBoolean(chkNoFecAntSis.Value)
        
        'Carga las denominaciones del grupo de datos generales en todos los idiomas
        Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdioma In g_oIdiomas
            If m_bMultiIdioma = True Then
                For i = 0 To sdbgDen.Rows - 1
                    vbm = sdbgDen.AddItemBookmark(i)
                    If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                        oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                        Exit For
                    End If
                Next i
            Else
                oDenominaciones.Add oIdioma.Cod, Trim(txtNombre.Text)
            End If
        Next
        Set oCampo.Denominaciones = oDenominaciones
    
        'Carga la colecci�n de valores de lista:
        If chkLista.Value = vbChecked Then
            If sdbcListaCampoPadre.Visible Then
                If Me.sdbcListaCampoPadre.Columns("ID").Value = "" Then
                    oCampo.IDListaCampoPadre = 0
                Else
                    oCampo.IDListaCampoPadre = Me.sdbcListaCampoPadre.Columns("ID").Value
                End If
            Else
                oCampo.IDListaCampoPadre = 0
            End If
            
            sdbgLista.MoveFirst
            Set oValores = oFSGSRaiz.Generar_CCampoValorListas
                        
            For i = 0 To sdbgLista.Rows - 1
                vbm = sdbgLista.AddItemBookmark(i)
                
                If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value = TipoCampoGS.material Then
                    'Comprobar valores introducidos
                    j = 0
                    k = 0
                    While (j < sdbgLista.Rows)
                        For k = sdbgLista.Columns.Count - 1 To 0 Step -1
                            If Left(sdbgLista.Columns(k).Name, 16) = "CAMPOPADREVALOR_" Then
                                If sdbgLista.Columns(k).Value = "" Then
                                    oMensajes.IntroducirCampoPadre
                                    Exit Sub
                                End If
                            End If
                        Next k
                        
                        j = j + 1
                    Wend
                    
                    'Origen campo material
                    arGMNs = Split(sdbgLista.Columns("CAMPOPADREVALOR_" & oCampo.IDListaCampoPadre).CellValue(vbm), "|")
                    ReDim Preserve arGMNs(0 To 3)
                    
                    Select Case oCampo.Tipo
                        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                            Set oValoresTxt = oFSGSRaiz.Generar_CMultiidiomas
                            For Each oIdioma In g_oIdiomas
                                If m_bMultiIdioma Then
                                    If sdbgLista.Columns(oIdioma.Cod).CellValue(vbm) <> "" Then
                                        oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns(oIdioma.Cod).CellValue(vbm)
                                    End If
                                Else
                                    If sdbgLista.Columns("VALOR").CellValue(vbm) <> "" Then
                                        oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns("VALOR").CellValue(vbm)
                                    End If
                                End If
                            Next
                            oValores.Add i + 1, oCampo, , oValoresTxt, , , , , oCampo.IDListaCampoPadre, , , arGMNs(0), arGMNs(1), arGMNs(2), arGMNs(3)
                            Set oValoresTxt = Nothing
                            
                        Case TiposDeAtributos.TipoNumerico
                            If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                oValores.Add i + 1, oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , , , oCampo.IDListaCampoPadre, , , arGMNs(0), arGMNs(1), arGMNs(2), arGMNs(3)
                            End If
                            
                        Case TiposDeAtributos.TipoFecha
                            If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                oValores.Add i + 1, oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , oCampo.IDListaCampoPadre, , , arGMNs(0), arGMNs(1), arGMNs(2), arGMNs(3)
                            End If
                    End Select
                Else
                    'Origen campo lista
                    If oCampo.IDListaCampoPadre > 0 Then
                        j = 0
                        k = 0
                        While (j < sdbgLista.Rows)
                            For k = sdbgLista.Columns.Count - 1 To 0 Step -1
                                If Left(sdbgLista.Columns(k).Name, 11) = "ORDENPADRE_" Then
                                    If sdbgLista.Columns(k).Value = "" Then
                                        oMensajes.IntroducirCampoPadre
                                        Exit Sub
                                    End If
                                End If
                            Next k
                            
                            j = j + 1
                        Wend
                        
                        'Tiene un campo padre
                        Dim iOrdenCampoPadre As Integer
                        iOrdenCampoPadre = sdbgLista.Columns("ORDENPADRE_" & oCampo.IDListaCampoPadre).CellValue(vbm)
                        
                        Select Case oCampo.Tipo
                            Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                                Set oValoresTxt = oFSGSRaiz.Generar_CMultiidiomas
                                For Each oIdioma In g_oIdiomas
                                    If m_bMultiIdioma Then
                                        If sdbgLista.Columns(oIdioma.Cod).CellValue(vbm) <> "" Then
                                            oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns(oIdioma.Cod).CellValue(vbm)
                                        End If
                                    Else
                                        If sdbgLista.Columns("VALOR").CellValue(vbm) <> "" Then
                                            oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns("VALOR").CellValue(vbm)
                                        End If
                                    End If
                                Next
                                oValores.Add i + 1, oCampo, , oValoresTxt, , , , , oCampo.IDListaCampoPadre, iOrdenCampoPadre
                                Set oValoresTxt = Nothing
                                
                            Case TiposDeAtributos.TipoNumerico
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    oValores.Add i + 1, oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , , , oCampo.IDListaCampoPadre, iOrdenCampoPadre
                                End If
                                
                            Case TiposDeAtributos.TipoFecha
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    oValores.Add i + 1, oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , oCampo.IDListaCampoPadre, iOrdenCampoPadre
                                End If
                        End Select
                    Else
                        Select Case oCampo.Tipo
                            Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                                Set oValoresTxt = oFSGSRaiz.Generar_CMultiidiomas
                                For Each oIdioma In g_oIdiomas
                                    If m_bMultiIdioma Then
                                        If sdbgLista.Columns(oIdioma.Cod).CellValue(vbm) <> "" Then
                                            oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns(oIdioma.Cod).CellValue(vbm)
                                        End If
                                    Else
                                        If sdbgLista.Columns("VALOR").CellValue(vbm) <> "" Then
                                            oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns("VALOR").CellValue(vbm)
                                        End If
                                    End If
                                Next
                                oValores.Add i + 1, oCampo, , oValoresTxt
                                Set oValoresTxt = Nothing
                                
                            Case TiposDeAtributos.TipoNumerico
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    oValores.Add i + 1, oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm))
                                End If
                                
                            Case TiposDeAtributos.TipoFecha
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    oValores.Add i + 1, oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm))
                                End If
                        End Select
                    End If
                End If
            Next i
            Set oCampo.ValoresLista = oValores
        End If
        
        If g_sOrigen = "frmDesglose" Then
            Set oCampo.CampoPadre = frmFormularios.g_ofrmDesglose.g_oCampoDesglose
            oCampo.EsSubCampo = True
        Else
            oCampo.EsSubCampo = False
        End If
        
        oCampo.RecordarValores = (chkRecordarValores.Value = vbChecked)
        
        Set oIBaseDatos = oCampo
        udtTeserror = oIBaseDatos.AnyadirABaseDatos
    
        If udtTeserror.NumError = TESnoerror Then
            Set oCampos = oFSGSRaiz.Generar_CFormCampos
            If g_sOrigen = "frmFormularios" Then
                'Lo a�ade a la colecci�n y grid de formularios
                Set oCampo2 = oCampos.Add(oCampo.Id, frmFormularios.g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, , , oCampo.FECACT, , , , , , , , , , , , , , , , oCampo.MostrarPOPUP, , , , , oCampo.MaxLength, , , , , , , , , , oCampo.RecordarValores, oCampo.IDListaCampoPadre, , , , , , , oCampo.Formato)
                oCampo2.NoFecAntSis = SQLBinaryToBoolean(chkNoFecAntSis.Value)
                frmFormularios.AnyadirCampos oCampos
                Set oCampos = Nothing
                RegistrarAccion ACCFormItemAnyadir, "Id:" & oCampo.Id
            Else
                'Lo a�ade a la colecci�n y grid de desglose:
                Set oCampo2 = oCampos.Add(oCampo.Id, frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, , , oCampo.FECACT, , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, oCampo.EsSubCampo, , , , , , , , , , , , , , , , , , oCampo.MaxLength, , , , , , , , , , , oCampo.IDListaCampoPadre, , , , , , , oCampo.Formato)
                oCampo2.NoFecAntSis = SQLBinaryToBoolean(chkNoFecAntSis.Value)
                frmFormularios.g_ofrmDesglose.AnyadirCampos oCampos
                Set oCampos = Nothing
                RegistrarAccion ACCDesgloseCampoAnyadir, "Id:" & oCampo.Id
            End If
        Else
            Screen.MousePointer = vbNormal
            TratarError udtTeserror
            Exit Sub
        End If
        
        Set oCampo = Nothing
        
        frmFormularios.g_Accion = ACCFormularioCons
    End If
    
    Set oIBaseDatos = Nothing
    Set oDenominaciones = Nothing
    Set oValores = Nothing
    
    Screen.MousePointer = vbNormal
        
    Unload Me
End Sub

Private Sub cmdAnyaValor_Click()
    sdbgLista.Scroll 0, sdbgLista.Rows - sdbgLista.Row
    
    If sdbgLista.VisibleRows > 0 Then
        
        If sdbgLista.VisibleRows > sdbgLista.Rows Then
            sdbgLista.Row = sdbgLista.Rows
        Else
            sdbgLista.Row = sdbgLista.Rows - (sdbgLista.Rows - sdbgLista.VisibleRows) - 1
        End If
        
    End If

    If Me.Visible Then sdbgLista.SetFocus
End Sub



Private Sub cmdComprob_Click()

Dim rex As RegExp
Set rex = New RegExp
'rex.Pattern = "^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$"

Dim colMatches   As MatchCollection
Dim objMatch As Match
Dim RetStr As String

If (txtFormato.Text <> "") Then

    rex.Pattern = txtFormato.Text
    
    ' Set Case Insensitivity.
    rex.IgnoreCase = True
    
    'Set global applicability.
    rex.Global = True
    
    'Test whether the String can be compared.
    If (rex.Test(txtValorComprob.Text) = True) Then
    
    'Get the matches.
    Set colMatches = rex.Execute(txtValorComprob.Text)   ' Execute search.
    
    For Each objMatch In colMatches   ' Iterate Matches collection.
        RetStr = RetStr & "Match found at position "
        RetStr = RetStr & objMatch.FirstIndex & ". Match Value is '"
        RetStr = RetStr & objMatch.Value & "'." & vbCrLf
    Next
        txtValorComprob.Backcolor = &HC000&
    Else
        RetStr = "String Matching Failed"
        txtValorComprob.Backcolor = &HFF&
    End If
    
    txtValorComprob.SetFocus

Else

    txtValorComprob.Backcolor = &H80000005

End If

End Sub

'Descripcion:=  Traslada la fila seleccionada hacia arriba o hacia abajo.
'               Metemos los valores de la fila en un array y luego nos movemos de fila y metemos los valores de esa lista en otro array, a su vez copiamos los elementos de la primera lista en la nueva fila
'Parametro  index = 0 Subir elemento
'           index = 1 Bajar elemento
'Tiempo ejecucion:=0,15seg.
Private Sub cmdMvtoElemLista_Click(Index As Integer)
Dim sArrText() As String
Dim sArrText2() As String

Dim i As Integer
Dim iTope As Integer

    If sdbgLista.SelBookmarks.Count = 0 Then Exit Sub
    If Index = 0 Then
        If sdbgLista.AddItemRowIndex(sdbgLista.SelBookmarks.Item(0)) = 0 Then Exit Sub
    Else
        If sdbgLista.AddItemRowIndex(sdbgLista.SelBookmarks.Item(0)) = sdbgLista.Rows - 1 Then Exit Sub
    End If
  
    iTope = sdbgLista.Columns.Count - 1
    ReDim Preserve sArrText(iTope)
    ReDim Preserve sArrText2(iTope)
    
    For i = 0 To iTope
        sArrText(i) = sdbgLista.Columns(i).Value
    Next
    If Index = 0 Then
        sdbgLista.MovePrevious
    Else
        sdbgLista.MoveNext
    End If
    For i = 0 To iTope
        sArrText2(i) = sdbgLista.Columns(i).Value
        sdbgLista.Columns(i).Value = sArrText(i)
    Next
    m_bRespetarLista = True
    If Index = 0 Then
        sdbgLista.MoveNext
    Else
        sdbgLista.MovePrevious
    End If
    For i = 0 To iTope
        sdbgLista.Columns(i).Value = sArrText2(i)
    Next
     
    sdbgLista.SelBookmarks.RemoveAll
    If Index = 0 Then
        sdbgLista.MovePrevious
    Else
        sdbgLista.MoveNext
    End If
    m_bRespetarLista = False
    sdbgLista.SelBookmarks.Add sdbgLista.Bookmark

End Sub


Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdElimValor_Click()
        
    If sdbgLista.Rows > 0 Then
        Dim i As Integer
        For i = 0 To sdbgLista.Cols - 1
            sdbgLista.Columns(i).DropDownHwnd = 0
        Next
        
        Screen.MousePointer = vbHourglass
        
        sdbgLista.SelBookmarks.Add sdbgLista.Bookmark
        
        If sdbgLista.AddItemRowIndex(sdbgLista.Bookmark) > -1 Then
            sdbgLista.RemoveItem (sdbgLista.AddItemRowIndex(sdbgLista.Bookmark))
        Else
            sdbgLista.RemoveItem (0)
        End If
    
        If sdbgLista.Rows > 0 Then
            If IsEmpty(sdbgLista.RowBookmark(sdbgLista.Row)) Then
                sdbgLista.Bookmark = sdbgLista.RowBookmark(sdbgLista.Row - 1)
            Else
                sdbgLista.Bookmark = sdbgLista.RowBookmark(sdbgLista.Row)
            End If
        End If
        sdbgLista.SelBookmarks.RemoveAll
        If Me.Visible Then sdbgLista.SetFocus

        Screen.MousePointer = vbNormal
        
    End If
End Sub

''' Revisado por: blp. Fecha: 23/10/2012
''' Carga de la p�gina
'''Llamada desde evento Load. M�x. 0,3 seg.
Private Sub Form_Load()
    Dim i As Integer
    
    Me.Height = 6650
    Me.Width = 8055
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    For i = 2 To 8
        sdbcTipo.AddItem i & Chr(m_lSeparador) & m_sIdiTipo(i)
    Next i
    If (gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSCompleto Or gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.AccesoContrato Or gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM) _
    And g_sOrigen <> "frmDesglose" And g_sOrigen <> "frmPARTipoSolicitDesglose" Then
        sdbcTipo.AddItem 9 & Chr(m_lSeparador) & m_sIdiTipo(9)
    End If
    sdbcTipo.AddItem TiposDeAtributos.TipoEditor & Chr(m_lSeparador) & m_sIdiTipo(10)
    sdbcTipo.AddItem TiposDeAtributos.TipoEnlace & Chr(m_lSeparador) & m_sIdiTipo(11)
    Select Case g_sOrigen
        Case "frmFormularios"
            Me.caption = m_sIdiForm & " " & frmFormularios.g_oFormSeleccionado.Den & " " & m_sIdiCaption
            m_bMultiIdioma = frmFormularios.g_oFormSeleccionado.Multiidioma
            
        Case "frmDesglose"
            Me.caption = NullToStr(frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & " " & m_sIdiCaption
            m_bMultiIdioma = frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Grupo.Formulario.Multiidioma
            
            lblCampoPadre.Visible = False
            sdbcListaCampoPadre.Visible = False
            
        Case "frmPARTipoSolicit"
            Me.caption = NullToStr(frmPARTipoSolicit.g_oTipoSeleccionado.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den) & " " & m_sIdiCaption
            m_bMultiIdioma = True
            FraListasEnlazadas.Visible = False
            fraMaxLength.Visible = False
            FrameBotonesGrid.Top = FrameBotonesGrid.Top - 400
            FraLista.Top = FraLista.Top - 400
            picDatos.Height = picDatos.Height - 700
            Me.sdbgLista.Top = sdbgLista.Top - 400
            Me.Height = 4590
        
        Case "frmPARTipoSolicitDesglose"
            Me.caption = NullToStr(frmPARTipoSolicit.g_ofrmDesglose.g_oCampoDesglose.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den) & " " & m_sIdiCaption
            m_bMultiIdioma = True
            
            fraMaxLength.Visible = False
            FraLista.Top = FraLista.Top - 400
            picDatos.Height = picDatos.Height - 300
            Me.Height = 4590
    End Select
    
    'Carga las columnas de la grid de valores dependiendo si es multiidioma o no
    If m_bMultiIdioma Then
        'Es multiidioma,muestra la grid en lugar del texbox del nombre:
        sdbgDen.Visible = True
        txtNombre.Visible = False
        CargarGridDenIdiomas
        
        'Ajusta la pantalla
        sdbgDen.Height = (sdbgDen.Rows) * sdbgDen.RowHeight + 10
        picDatos.Top = sdbgDen.Top + sdbgDen.Height + 75
        fraNuevo.Height = picDatos.Height + sdbgDen.Height + 500
        cmdAceptar.Top = fraNuevo.Top + fraNuevo.Height + 80
        cmdCancelar.Top = cmdAceptar.Top
        Me.Height = cmdAceptar.Top + cmdAceptar.Height + 550
    Else
        'Se muestra solo el textbox del nombre,no la grid
        sdbgDen.Visible = False
        txtNombre.Visible = True
    End If
    
    If g_sOrigen = "frmFormularios" Or g_sOrigen = "frmDesglose" Then
        CargarComboCamposPadre
    End If

    'Longitud del nombre del campo.
    txtNombre.MaxLength = 300
    Me.sdbgDen.Columns("DEN").FieldLen = 300
    txtMaxLength.Text = ""
    txtMaxLength.Enabled = False
    UpdMaxLength.Enabled = False
    
    m_sTipo = ""
    m_sMaxLength = ""
End Sub

Private Sub CargarRecursos()
    Dim i As Integer
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FORM_ITEM_ANYA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sIdiForm = Ador(0).Value   '1 Formulario
        Ador.MoveNext
        m_sIdiCaption = Ador(0).Value    '2 A�adir campo
        Ador.MoveNext
        lblNombre.caption = Ador(0).Value & ":"  '3 Nombre
        m_sIdiNombre = Ador(0).Value
        Ador.MoveNext
        lblTipo.caption = Ador(0).Value & ":" '4 Tipo
        m_sIdiTp = Ador(0).Value
        Ador.MoveNext
        lblMin.caption = Ador(0).Value & ":" '5 M�nimo
        m_sIdiMinimo = Ador(0).Value
        Ador.MoveNext
        lblMax.caption = Ador(0).Value & ":" '6 M�ximo
        m_sIdiMaximo = Ador(0).Value
        Ador.MoveNext
        chkLista.caption = Ador(0).Value  '7 Edici�n mediante selecci�n de lista de valores
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '8 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '9 &Cancelar
        
        For i = 2 To 8
            Ador.MoveNext
            m_sIdiTipo(i) = Ador(0).Value
        Next i
        
        
        Ador.MoveNext
        m_sIdiValor = Ador(0).Value  '17 Valor
        sdbgLista.Columns.Item("VALOR").caption = Ador(0).Value
        
        Ador.MoveNext
        cmdAnyaValor.ToolTipText = Ador(0).Value  '18 A�adir valor
        Ador.MoveNext
        cmdElimValor.ToolTipText = Ador(0).Value  '19 Eliminar valor
        
        For i = 1 To 4
            Ador.MoveNext
            m_sMensaje(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sAtributoGS = Ador(0).Value  '25 Atributos de GS
        
        Ador.MoveNext
        m_sIdiTipo(9) = Ador(0).Value '25 Desglose
        
        Ador.MoveNext
        lblMaxLength.caption = Ador(0).Value 'N� m�ximo caracteres:
        
        For i = 1 To 3
            Ador.MoveNext
            m_sMensajeHasta(i) = Ador(0).Value 'hasta XX caracteres
        Next i
        
        Ador.MoveNext
        Me.chkRecordarValores.caption = Ador(0).Value  '30 Recordar valores
        
        Ador.MoveNext
        Me.lblCampoPadre.caption = Ador(0).Value   '31 Campo Padre
        
        Ador.MoveNext
        m_sIdiTipo(10) = Ador(0).Value '32 Editor
        Ador.MoveNext
        lblFormato.caption = Ador(0).Value  '
        Ador.MoveNext
        lblValorComprob.caption = Ador(0).Value  '
        Ador.MoveNext
        chkNoFecAntSis.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiTipo(11) = Ador(0).Value ' Enlace
        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_sOrigen = ""
    m_lCampoPadreSeleccionado = 0
    Set g_oIdiomas = Nothing
    
    Me.Visible = False
End Sub


Private Sub sdbcListaCampoPadre_DropDown()
    If sdbcListaCampoPadre.Rows = 0 Then
        CargarComboCamposPadre
    End If
End Sub

Private Sub sdbcTipo_CloseUp()
    
    If val(m_sTipo) = sdbcTipo.Columns("ID").Value And Not m_bForzarCloseUp Then
        Exit Sub
    End If
    
    MostrarOpcionRecordarValores
    
    txtMax.Text = ""
    txtMin.Text = ""
    txtMaxLength.Text = ""
    lblHastaCaracteres.Visible = False
    UpdMaxLength.Enabled = False
    txtMaxLength.Enabled = False
    If sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoFecha And sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoNumerico Then
        txtMax.Enabled = False
        txtMin.Enabled = False
        lblFormato.Visible = False
        txtFormato.Visible = False
        lblValorComprob.Visible = False
        txtValorComprob.Visible = False
        cmdComprob.Visible = False
        chkLista.Enabled = Not (sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoEnlace)
             
        If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Or _
            sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio Or _
            sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Then
            txtMaxLength.Enabled = True
            UpdMaxLength.Enabled = True
            If chkLista.Value <> vbChecked Then
                lblFormato.Visible = True
                txtFormato.Visible = True
                lblValorComprob.Visible = True
                txtValorComprob.Visible = True
                cmdComprob.Visible = True
            End If
            Select Case sdbcTipo.Columns("ID").Value
                Case TiposDeAtributos.TipoTextoCorto
                    lblHastaCaracteres.caption = m_sMensajeHasta(1)
                    UpdMaxLength.min = 1
                    UpdMaxLength.Max = 100
                    txtMaxLength.Text = "100"
                Case TiposDeAtributos.TipoTextoMedio
                    lblHastaCaracteres.caption = m_sMensajeHasta(2)
                    UpdMaxLength.min = 100
                    UpdMaxLength.Max = 800
                    txtMaxLength.Text = "800"
                Case TiposDeAtributos.TipoTextoLargo
                    lblHastaCaracteres.caption = m_sMensajeHasta(3)
                    UpdMaxLength.min = 800
                    UpdMaxLength.Max = 99999
                    txtMaxLength.Text = ""
            End Select
            lblHastaCaracteres.Visible = True
        End If
    Else
        If g_sOrigen = "frmFormularios" Or g_sOrigen = "frmDesglose" Then
            Select Case sdbcTipo.Columns("ID").Value
                Case TiposDeAtributos.TipoFecha
                    chkNoFecAntSis.Visible = True
            End Select
        End If
        txtMaxLength.Enabled = False
        UpdMaxLength.Enabled = False
        lblFormato.Visible = False
        txtFormato.Visible = False
        lblValorComprob.Visible = False
        txtValorComprob.Visible = False
        cmdComprob.Visible = False
        If chkLista.Value = vbChecked Then
            txtMax.Enabled = False
            txtMin.Enabled = False
        Else
            txtMax.Enabled = True
            txtMin.Enabled = True
        End If
    End If
    
    m_sTipo = sdbcTipo.Columns("ID").Value
    
    CrearGridLista
    
    ConfigurarGridLista
    
    
    m_sMaxLength = txtMaxLength.Text
       
End Sub

Private Sub sdbcTipo_GotFocus()
    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If
End Sub

Private Sub sdbgLista_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgLista_BtnClick()
    If Mid(sdbgLista.Columns(sdbgLista.col).Name, 1, 11) = "CAMPOPADRE_" Then
        If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value = TipoCampoGS.material Then
            'Abrir el buscador de materiales
            frmSELMAT.sOrigen = "frmFormAnyaCampos"
            frmSELMAT.Show vbModal
        End If
    End If
End Sub

''' <summary>Pone en el grid el material seleccionado en el buscador de material</summary>
''' <remarks>Llamada desde: frmSELMAT</remarks>

Public Sub PonerMatSeleccionado()
    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    Dim sValor As String
    Dim sDen As String
    Dim lIdCampoPadre As Long
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sValor = oGMN1Seleccionado.Cod
        sDen = oGMN1Seleccionado.Cod & " - " & oGMN1Seleccionado.Den
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sValor = oGMN2Seleccionado.GMN1Cod & "|" & oGMN2Seleccionado.Cod
        sDen = oGMN2Seleccionado.GMN1Cod & " - " & oGMN2Seleccionado.Cod & " - " & oGMN2Seleccionado.Den
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sValor = oGMN3Seleccionado.GMN1Cod & "|" & oGMN3Seleccionado.GMN2Cod & "|" & oGMN3Seleccionado.Cod
        sDen = oGMN3Seleccionado.GMN1Cod & " - " & oGMN3Seleccionado.GMN2Cod & " - " & oGMN3Seleccionado.Cod & " - " & oGMN3Seleccionado.Den
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sValor = oGMN4Seleccionado.GMN1Cod & "|" & oGMN4Seleccionado.GMN2Cod & "|" & oGMN4Seleccionado.GMN3Cod & "|" & oGMN4Seleccionado.Cod
        sDen = oGMN4Seleccionado.GMN1Cod & " - " & oGMN4Seleccionado.GMN2Cod & " - " & oGMN4Seleccionado.GMN3Cod & " - " & oGMN4Seleccionado.Cod & " - " & oGMN4Seleccionado.Den
    End If
            
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    lIdCampoPadre = Mid(sdbgLista.Columns(sdbgLista.col).Name, 12, Len(sdbgLista.Columns(sdbgLista.col).Name) - 11)
    sdbgLista.Columns("CAMPOPADREVALOR_" & lIdCampoPadre).Value = sValor
    sdbgLista.Columns("CAMPOPADRE_" & lIdCampoPadre).Value = sDen
End Sub

Private Sub txtMaxLength_GotFocus()

    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If
    
    m_sMaxLength = txtMaxLength.Text

End Sub

''' <summary>
''' Comprobaciones antes de grabar
''' </summary>
''' <param name="Cancel">Cancelar la grabaci�n</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgLista_BeforeUpdate(Cancel As Integer)
    Dim i As Integer
    Dim vbm As Variant
    Dim oIdioma As CIdioma
    Dim j As Integer
    Dim DifiereEnPadre As Boolean
    
    If m_bRespetarLista = True Then Exit Sub
    m_bError = False
    
    Dim NumeroColumnas As Integer
    m_iNumeroPadres = 0
    
    If m_bMultiIdioma Then
        Select Case sdbcTipo.Columns("ID").Value
        Case TiposDeAtributos.TipoFecha
            NumeroColumnas = sdbgLista.Columns.Count
        Case TiposDeAtributos.TipoNumerico
            NumeroColumnas = sdbgLista.Columns.Count
        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
            NumeroColumnas = sdbgLista.Columns.Count - g_oIdiomas.Count + 1
        End Select
    Else
        NumeroColumnas = sdbgLista.Columns.Count
    End If
    m_iNumeroPadres = NumeroColumnas / 3
    
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
        If Not IsNumeric(sdbgLista.Columns("VALOR").Value) Then
            oMensajes.NoValido m_sIdiValor & " " & m_sMensaje(1)
            If Me.Visible Then sdbgLista.SetFocus
            Cancel = True
            m_bError = True
            Exit Sub
        End If
        If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
            For j = 1 To m_iNumeroPadres
                sdbgLista.Columns((2 * j) - 1).DropDownHwnd = 0
            Next
        End If
        For i = 0 To sdbgLista.Rows - 1
            vbm = sdbgLista.AddItemBookmark(i)
            If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                If sdbgLista.Columns("VALOR").CellValue(vbm) = sdbgLista.Columns("VALOR").Value Then
                    DifiereEnPadre = False
                    For j = 1 To m_iNumeroPadres
                        If sdbgLista.Columns((2 * j) - 1).CellValue(vbm) <> sdbgLista.Columns((2 * j) - 1).Text Then
                            DifiereEnPadre = True
                            Exit For
                        End If
                    Next
                    If Not DifiereEnPadre Then
                        oMensajes.NoValido m_sMensaje(3)
                        If Me.Visible Then sdbgLista.SetFocus
                        Cancel = True
                        m_bError = True
                        For j = 1 To m_iNumeroPadres
                            sdbgLista.Columns((2 * j) - 1).DropDownHwnd = sdbddValoresLista.hWnd
                        Next
                        Exit Sub
                    End If
                End If
            End If
        Next i
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Then
        If Not IsDate(sdbgLista.Columns("VALOR").Value) Then
            oMensajes.NoValido m_sIdiValor & " " & m_sMensaje(2)
            If Me.Visible Then sdbgLista.SetFocus
            Cancel = True
            m_bError = True
            Exit Sub
        End If
        If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
            For j = 1 To m_iNumeroPadres
                sdbgLista.Columns((2 * j) - 1).DropDownHwnd = 0
            Next
        End If
        For i = 0 To sdbgLista.Rows - 1
            vbm = sdbgLista.AddItemBookmark(i)
            If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                If sdbgLista.Columns("VALOR").CellValue(vbm) = sdbgLista.Columns("VALOR").Value Then
                    DifiereEnPadre = False
                    For j = 1 To m_iNumeroPadres
                        If sdbgLista.Columns((2 * j) - 1).CellValue(vbm) <> sdbgLista.Columns((2 * j) - 1).Text Then
                            DifiereEnPadre = True
                            Exit For
                        End If
                    Next
                    If Not DifiereEnPadre Then
                        oMensajes.NoValido m_sMensaje(3)
                        If Me.Visible Then sdbgLista.SetFocus
                        Cancel = True
                        m_bError = True
                        For j = 1 To m_iNumeroPadres
                            sdbgLista.Columns((2 * j) - 1).DropDownHwnd = sdbddValoresLista.hWnd
                        Next
                        Exit Sub
                    End If
                End If
            End If
        Next i
    ElseIf (sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio) Then
        If m_bMultiIdioma = True Then
            If sdbgLista.Columns(gParametrosInstalacion.gIdioma).Value = "" Then
                oMensajes.NoValido sdbgLista.Columns(gParametrosInstalacion.gIdioma).caption
                If Me.Visible Then sdbgLista.SetFocus
                m_bError = True
                Cancel = True
                Exit Sub
            End If
            If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
                For j = 1 To m_iNumeroPadres
                    sdbgLista.Columns((2 * j) + 1).DropDownHwnd = 0
                Next
            End If
            For i = 0 To sdbgLista.Rows - 1
                vbm = sdbgLista.AddItemBookmark(i)
                If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                    For Each oIdioma In g_oIdiomas
                        If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                            If sdbgLista.Columns(oIdioma.Cod).CellValue(vbm) = sdbgLista.Columns(oIdioma.Cod).Value Then
                                DifiereEnPadre = False
                                For j = 1 To m_iNumeroPadres
                                    If sdbgLista.Columns((2 * j) + 1).CellValue(vbm) <> sdbgLista.Columns((2 * j) + 1).Text Then
                                        DifiereEnPadre = True
                                        Exit For
                                    End If
                                Next
                                If Not DifiereEnPadre Then
                                    oMensajes.NoValido m_sMensaje(3)
                                    If Me.Visible Then sdbgLista.SetFocus
                                    Cancel = True
                                    m_bError = True
                                    For j = 1 To m_iNumeroPadres
                                        sdbgLista.Columns((2 * j) + 1).DropDownHwnd = sdbddValoresLista.hWnd
                                    Next
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next
                End If
            Next i
        Else
            If sdbgLista.Columns("VALOR").Value = "" Then
                oMensajes.NoValido m_sIdiValor
                If Me.Visible Then sdbgLista.SetFocus
                m_bError = True
                Cancel = True
                Exit Sub
            End If
            If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
                For j = 1 To m_iNumeroPadres
                    sdbgLista.Columns((2 * j) - 1).DropDownHwnd = 0
                Next
            End If
            For i = 0 To sdbgLista.Rows - 1
                vbm = sdbgLista.AddItemBookmark(i)
                If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                    If sdbgLista.Columns("VALOR").CellValue(vbm) = sdbgLista.Columns("VALOR").Value Then
                        DifiereEnPadre = False
                        For j = 1 To m_iNumeroPadres
                            If sdbgLista.Columns((2 * j) - 1).CellValue(vbm) <> sdbgLista.Columns((2 * j) - 1).Text Then
                                DifiereEnPadre = True
                                Exit For
                            End If
                        Next
                        
                        If Not DifiereEnPadre Then
                            oMensajes.NoValido m_sMensaje(3)
                            If Me.Visible Then sdbgLista.SetFocus
                            Cancel = True
                            m_bError = True
                            For j = 1 To m_iNumeroPadres
                                sdbgLista.Columns((2 * j) - 1).DropDownHwnd = sdbddValoresLista.hWnd
                            Next
                            Exit Sub
                        End If
                    End If
                End If
            Next i

        End If
    End If
           
    j = 0
    i = 0
    While (j < sdbgLista.Rows)
        For i = sdbgLista.Columns.Count - 1 To 0 Step -1
            If Left(sdbgLista.Columns(i).Name, 11) = "ORDENPADRE_" Or Left(sdbgLista.Columns(i).Name, 16) = "CAMPOPADREVALOR_" Then
                If sdbgLista.Columns(i).Value = "" Then
                    oMensajes.IntroducirCampoPadre
                    If Me.Visible Then sdbgLista.SetFocus
                    m_bError = True
                    Cancel = True
                    Exit Sub
                End If
            End If
        Next i
        
        j = j + 1
    Wend
End Sub


'Descripcion:=
    ''' * Objetivo: Capturar la tecla Supr para
    ''' * Objetivo: poder no dejar eliminar desde el teclado
    '''   Si se trata de una lista anidada y es de tipo combo, cuando le damos al back key eliminar el contenido
'Parametro:=
    'KeyCode:=n� de la tecla que hemos pulsado
    'Shift:=Si tenemos pulsado la tecla.
'Llamada desde:= Evento que salta al pulsar una tecla
'Tiempo ejecucion:=0seg.
Private Sub sdbgLista_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyDelete Then
        If sdbgLista.col = -1 Then
            KeyCode = 0
        End If
        Exit Sub
    ElseIf KeyCode = vbKeyBack Then
        If Mid(sdbgLista.Columns(sdbgLista.col).Name, 1, 11) = "CAMPOPADRE_" Then
                sdbgLista.Columns(sdbgLista.col).Text = ""
                sdbgLista.Columns(sdbgLista.col + 1).Text = ""
        End If
    End If
    
End Sub

''' <summary>
''' Tras moverse de fila/columna realiza los cambios correspondientes en las celdas (locked, cargar combo, etc)
''' </summary>
''' <param name="LastRow">ultima fila</param>
''' <param name="LastCol">ultima columna</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgLista_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim oCampo As CFormItem
    'Comprobar si el campo padre seleccionado tb dispone de otro campo padre...y sucesivos
    Dim sCampoPadre_CampoPadre As String
    Dim lCampoPadreAnt As Long
    Dim aux() As String
        
    If sdbgLista.AllowUpdate = False Then Exit Sub
    
    If sdbgLista.IsAddRow Then
        cmdElimValor.Enabled = False
    Else
        cmdElimValor.Enabled = True
    End If
        
    '''�Todo esto del .DropDownHwnd = 0 y mas tarde .DropDownHwnd = sdbddValoresLista.hwnd? Me hace perder el padre cuando llegas a nivel biznieto (3 padres)
    lCampoPadreAnt = m_lCampoPadreSeleccionado
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    sCampoPadre_CampoPadre = oCampo.CargarCampoPadre(m_lCampoPadreSeleccionado, True)
        
    While sCampoPadre_CampoPadre <> ""
        aux = Split(sCampoPadre_CampoPadre, "$$$")
        
        If sdbgLista.Columns("CAMPOPADREPADRE_" & lCampoPadreAnt).Value = "" Then
            sdbgLista.Columns("CAMPOPADREPADRE_" & lCampoPadreAnt).Value = aux(0)
        End If
        lCampoPadreAnt = aux(0)
        sCampoPadre_CampoPadre = oCampo.CargarCampoPadre(aux(0), True)
    Wend
    Set oCampo = Nothing
        
    cmdAnyaValor.Enabled = True
    
    If sdbgLista.col > -1 Then
        If Mid(sdbgLista.Columns(sdbgLista.col).Name, 1, 11) = "CAMPOPADRE_" Then
            If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value = TipoCampoGS.material Then
                'Campo de tipo material
                sdbgLista.Columns(sdbgLista.col).DropDownHwnd = 0
                sdbgLista.Columns(sdbgLista.col).Style = ssStyleEditButton
            Else
                'Campo de tipo lista
                If LastCol >= 0 Then sdbgLista.Columns(LastCol).DropDownHwnd = 0
                sdbgLista.Columns(sdbgLista.col).Style = ssStyleComboBox
                sdbddValoresLista.RemoveAll
                sdbddValoresLista.AddItem ""
                sdbgLista.Columns(sdbgLista.col).DropDownHwnd = sdbddValoresLista.hWnd
                sdbddValoresLista.Enabled = True
                sdbddValoresLista.DroppedDown = True
            End If
        End If
    End If
End Sub

Private Sub sdbgLista_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    
    If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
        For i = 0 To sdbgLista.Cols - 1
            sdbgLista.Columns(i).DropDownHwnd = 0
        Next
    End If
    
    If sdbgLista.IsAddRow Then
        cmdElimValor.Enabled = False
    Else
        cmdElimValor.Enabled = True
    End If
End Sub

Private Sub sdbgLista_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
    If sdbgLista.SelBookmarks.Count > 0 Then
        cmdMvtoElemLista(0).Enabled = True
        cmdMvtoElemLista(1).Enabled = True
    Else
        cmdMvtoElemLista(0).Enabled = False
        cmdMvtoElemLista(1).Enabled = False
    End If
End Sub


Private Sub txtMaxLength_LostFocus()
    If m_sMaxLength <> txtMaxLength.Text Then
        If Comprobar_txtMaxLength = False Then
            txtMaxLength.Text = m_sMaxLength
        End If
    End If
End Sub

Private Sub txtNombre_GotFocus()

    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If
End Sub

Private Sub UpdMaxLength_UpClick()
Dim i As Integer
Dim oIdioma As CIdioma

    m_bError = False

    UpdMaxLength.Enabled = False

    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If

    'Si se modifica el n� m�ximo de caracteres permitidos.
    sdbgLista.MoveLast
    If sdbgLista.Rows > 0 Then
        If m_bMultiIdioma = True Then
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                sdbgLista.Row = i
                For Each oIdioma In g_oIdiomas
                    If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                        If Len(sdbgLista.Columns(oIdioma.Cod).Value) > CInt(val(txtMaxLength.Text)) Then
                          
                            sdbgLista.Columns(oIdioma.Cod).FieldLen = val(txtMaxLength.Text)
                   
                        End If
                    End If
                Next
                sdbgLista.MovePrevious
            Next i
        Else
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                
                sdbgLista.Columns("VALOR").FieldLen = val(txtMaxLength.Text)
                   
                sdbgLista.MovePrevious
            Next i
            sdbgLista.MoveFirst
        End If
    End If
    
    UpdMaxLength.Enabled = True
'    m_bSaltar = False
    m_sMaxLength = txtMaxLength.Text
End Sub

Private Sub UpdMaxLength_DownClick()
    Dim i As Integer
    Dim oIdioma As CIdioma
    Dim bMensajeMostrado As Boolean 'Por si hay varios valores de la lista que superan la maxima longitud, que s�lo nos muestre una vez el mensaje
    Dim irespuesta As Integer

    UpdMaxLength.Enabled = False
        
    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If
    
    m_bError = False
    
    'Si se modifica el n� m�ximo de caracteres permitidos, si los valores definidos no cumplen esta longitud se eliminar�n.
    sdbgLista.MoveLast
    If sdbgLista.Rows > 0 Then
        If m_bMultiIdioma = True Then
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                sdbgLista.Row = i
                For Each oIdioma In g_oIdiomas
                    If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                        
                            If Len(sdbgLista.Columns(oIdioma.Cod).Value) > CInt(val(txtMaxLength.Text)) Then
                                If Not bMensajeMostrado Then
                                    irespuesta = oMensajes.TruncarValor
                                Else
                                    irespuesta = vbYes
                                End If
                                If irespuesta = vbYes Then
                                    bMensajeMostrado = True
                                    sdbgLista.Columns(oIdioma.Cod).Value = Left(sdbgLista.Columns(oIdioma.Cod).Value, CInt(val(txtMaxLength.Text)))
                                Else
                                    UpdMaxLength.Enabled = True
                                    txtMaxLength.Text = txtMaxLength.Text + 1
                                    If Me.Visible Then txtMaxLength.SetFocus
                                    Exit Sub
                                End If
                            End If
                        
                    End If
                Next
                sdbgLista.MovePrevious
                If m_bError Then
                    txtMaxLength.Text = m_sMaxLength
                    GoTo Salir
                End If
            Next i
            
            sdbgLista.MoveFirst
            
            If m_bError Then
                txtMaxLength.Text = m_sMaxLength
                GoTo Salir
            End If
            
        Else
            
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                
                    If Len(sdbgLista.Columns("VALOR").Value) > CInt(val(txtMaxLength.Text)) Then
                        If Not bMensajeMostrado Then
                            irespuesta = oMensajes.TruncarValor
                        Else
                            irespuesta = vbYes
                        End If
                        If irespuesta = vbYes Then
                            bMensajeMostrado = True
                            sdbgLista.Columns("VALOR").Value = Left(sdbgLista.Columns("VALOR").Value, CInt(val(txtMaxLength.Text)))
                            sdbgLista.Columns("VALOR").FieldLen = val(txtMaxLength.Text)
                        Else
                            UpdMaxLength.Enabled = True
                            txtMaxLength.Text = txtMaxLength.Text + 1
                            Exit Sub
                        End If
                    End If
                
                sdbgLista.MovePrevious
                If m_bError Then
                    txtMaxLength.Text = m_sMaxLength
                    GoTo Salir
                End If
            Next i
            sdbgLista.MoveFirst
            If m_bError Then
                txtMaxLength.Text = m_sMaxLength
                GoTo Salir
            End If
        End If
    End If

    If m_bMultiIdioma = True And m_sTipo <> TiposDeAtributos.TipoTextoLargo Then
        For Each oIdioma In g_oIdiomas
            sdbgLista.Columns(oIdioma.Cod).FieldLen = val(txtMaxLength.Text)
        Next
    ElseIf m_sTipo <> TiposDeAtributos.TipoTextoLargo Then
        sdbgLista.Columns("VALOR").FieldLen = val(txtMaxLength.Text)
    End If
    
    
Salir:
    UpdMaxLength.Enabled = True
    m_sMaxLength = txtMaxLength.Text
End Sub

Private Sub CargarGridDenIdiomas()
Dim oIdioma As CIdioma

    'Carga las denominaciones del grupo de datos generales en todos los idiomas:
    For Each oIdioma In g_oIdiomas
        sdbgDen.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
    Next
    sdbgDen.MoveFirst
End Sub

Private Sub ConfigurarGridLista()
Dim oIdioma As CIdioma
Dim i As Integer
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoBoolean Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoArchivo Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoDesglose Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoEditor Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoEnlace Then
    
        sdbgLista.RemoveAll
        
        chkLista.Enabled = False
        If chkLista.Value = vbChecked Then
            chkLista.Value = vbUnchecked
            sdbgLista.AllowUpdate = False
        End If
        
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Then
                
        chkLista.Enabled = True
        
        If val(m_sTipo) <> sdbcTipo.Columns("ID").Value Then
            sdbgLista.RemoveAll
        End If
        
        If chkLista.Value = vbChecked Then
            sdbgLista.AllowUpdate = True
        Else
            sdbgLista.AllowUpdate = False
        End If
        
        sdbgLista.Columns.Item(m_sIdiValor).FieldLen = 0
        
    Else
        chkLista.Enabled = True
        
        If chkLista.Value = vbChecked Then
            sdbgLista.AllowUpdate = True
        Else
            sdbgLista.AllowUpdate = False
        End If
        
        
        
        If val(m_sMaxLength) > val(txtMaxLength.Text) Then
        
            Comprobar_txtMaxLength
        Else
            If m_bMultiIdioma = True Then
            
                If sdbgLista.Columns.Count > 0 Then sdbgLista.Columns.RemoveAll
                    i = 0
                             
                For Each oIdioma In g_oIdiomas
                    sdbgLista.Columns.Add i
                    sdbgLista.Columns(i).Name = oIdioma.Cod
                    sdbgLista.Columns.Item(oIdioma.Cod).Name = oIdioma.Cod
                    sdbgLista.Columns.Item(oIdioma.Cod).caption = oIdioma.Den
                    sdbgLista.Columns.Item(oIdioma.Cod).Width = sdbgLista.Width / g_oIdiomas.Count - 300
                    i = i + 1
                Next
            
                For Each oIdioma In g_oIdiomas
                    sdbgLista.Columns(oIdioma.Cod).FieldLen = val(txtMaxLength.Text)
                Next
            Else
            
                sdbgLista.Columns.Item(m_sIdiValor).FieldLen = val(txtMaxLength.Text)
            End If
        End If
        
    End If
    
End Sub


Private Function Comprobar_txtMax() As Boolean
    If Trim(txtMax.Text) = "" Then Exit Function
    
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
        If Not IsNumeric(Trim(txtMax.Text)) Then
            oMensajes.NoValido m_sIdiMaximo & " " & m_sMensaje(1)
            Comprobar_txtMax = False
            Exit Function
        End If
        
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Then
        If Not IsDate(Trim(txtMax.Text)) Then
            oMensajes.NoValido m_sIdiMaximo & " " & m_sMensaje(2)
            Comprobar_txtMax = False
            Exit Function
        End If
    End If
    Comprobar_txtMax = True
End Function


Private Function Comprobar_txtMin() As Boolean
    If Trim(txtMin.Text) = "" Then Exit Function
    
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
        If Not IsNumeric(Trim(txtMin.Text)) Then
            oMensajes.NoValido m_sIdiMinimo & " " & m_sMensaje(1)
            Comprobar_txtMin = False
            Exit Function
        End If
        
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Then
        If Not IsDate(Trim(txtMin.Text)) Then
            oMensajes.NoValido m_sIdiMinimo & " " & m_sMensaje(2)
            Comprobar_txtMin = False
            Exit Function
        End If
    End If
    Comprobar_txtMin = True
End Function


Private Function Comprobar_txtMaxLength() As Boolean
Dim i As Integer
Dim oIdioma As CIdioma
Dim bMensajeMostrado As Boolean
Dim irespuesta As Integer
    
    If Not IsNumeric(Trim(txtMaxLength.Text)) Then
        oMensajes.NoValido Left(lblMaxLength.caption, Len(lblMaxLength.caption) - 1) & " " & m_sMensaje(1)
        Comprobar_txtMaxLength = False
        Exit Function
    End If
    
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Then
        If val(txtMaxLength.Text) > 100 Or val(txtMaxLength.Text) < 1 Then
            oMensajes.NoValido Chr(10) & lblMaxLength.caption & " " & m_sMensajeHasta(1)
            Comprobar_txtMaxLength = False
            Exit Function
        End If
        
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio Then
        If val(txtMaxLength.Text) > 800 Or val(txtMaxLength.Text) < 100 Then
            oMensajes.NoValido Chr(10) & lblMaxLength.caption & " " & m_sMensajeHasta(2)
            Comprobar_txtMaxLength = False
            Exit Function
        End If
        
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Then
        If val(txtMaxLength.Text) < 800 Then
            oMensajes.NoValido Chr(10) & lblMaxLength.caption & " " & m_sMensajeHasta(3)
            Comprobar_txtMaxLength = False
            Exit Function
        End If
    End If
    
    'Si se modifica el n� m�ximo de caracteres permitidos, si los valores definidos no cumplen esta longitud se eliminar�n.
    m_bError = False
    sdbgLista.MoveLast
    If sdbgLista.Rows > 0 Then
    
        If m_bMultiIdioma = True Then
        
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                For Each oIdioma In g_oIdiomas
                    If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                        
                            If Len(sdbgLista.Columns(oIdioma.Cod).Value) > CInt(val(txtMaxLength.Text)) Then
                                If Not bMensajeMostrado Then
                                    irespuesta = oMensajes.TruncarValor
                                Else
                                    irespuesta = vbYes
                                End If
                                If irespuesta = vbYes Then
                                    bMensajeMostrado = True
                                    sdbgLista.Columns(oIdioma.Cod).Value = Left(sdbgLista.Columns(oIdioma.Cod).Value, CInt(val(txtMaxLength.Text)))
                                Else
                                    Comprobar_txtMaxLength = False
                                    txtMaxLength.Text = m_sMaxLength
                                    Exit Function
                                End If
                            End If
                        
                    End If
                Next
                sdbgLista.MovePrevious
                If m_bError Then
                    txtMaxLength.Text = m_sMaxLength
                    Comprobar_txtMaxLength = False
                    Exit Function
                End If
            Next
            sdbgLista.MoveFirst
            If m_bError Then
                txtMaxLength.Text = m_sMaxLength
                Comprobar_txtMaxLength = False
                Exit Function
            End If
            
        Else 'NO ES MULTIIDIOMA
        
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                
                If Len(sdbgLista.Columns("VALOR").Value) > CInt(val(txtMaxLength.Text)) Then
                    If Not bMensajeMostrado Then
                        irespuesta = oMensajes.TruncarValor
                    Else
                        irespuesta = vbYes
                    End If
                    If irespuesta = vbYes Then
                        bMensajeMostrado = True
                        sdbgLista.Columns("VALOR").Value = Left(sdbgLista.Columns("VALOR").Value, CInt(val(txtMaxLength.Text)))
                    Else
                        Comprobar_txtMaxLength = False
                        txtMaxLength.Text = m_sMaxLength
                        Exit Function
                    End If
                End If
                
                sdbgLista.MovePrevious
                If m_bError Then
                    txtMaxLength.Text = m_sMaxLength
                    Comprobar_txtMaxLength = False
                    Exit Function
                End If
            Next i
            
            sdbgLista.MoveFirst
            If m_bError Then
                txtMaxLength.Text = m_sMaxLength
                Comprobar_txtMaxLength = False
                Exit Function
            End If
        End If
    End If
    
    If sdbgLista.Columns.Count > 1 And m_sTipo <> TiposDeAtributos.TipoTextoLargo Then
        If m_bMultiIdioma = True Then
            For Each oIdioma In g_oIdiomas
                sdbgLista.Columns(oIdioma.Cod).FieldLen = val(txtMaxLength.Text)
            Next
        End If
    ElseIf m_sTipo <> TiposDeAtributos.TipoTextoLargo Then
        sdbgLista.Columns("VALOR").FieldLen = val(txtMaxLength.Text)
    End If
    
    Comprobar_txtMaxLength = True
End Function

Private Sub CrearGridLista()
    Dim i As Integer
    Dim oIdioma As CIdioma
    
    If (sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio) _
    And m_bMultiIdioma = True Then
        sdbgLista.Columns.RemoveAll
        
        i = 0
        For Each oIdioma In g_oIdiomas
            sdbgLista.Columns.Add i
            sdbgLista.Columns.Item(i).Name = oIdioma.Cod
            sdbgLista.Columns.Item(i).caption = oIdioma.Den
            sdbgLista.Columns.Item(i).Width = sdbgLista.Width / g_oIdiomas.Count - 100
            i = i + 1
        Next
    Else
        sdbgLista.Columns.RemoveAll
        
        i = 0
        sdbgLista.Columns.Add i
        sdbgLista.Columns.Item(i).Name = "VALOR"
        sdbgLista.Columns.Item(i).caption = m_sIdiValor
        sdbgLista.Columns.Item(i).Width = sdbgLista.Width - 300
    End If
End Sub

''' <summary>Esta funci�n muestra u oculta el check de Recordar Valores dependiendo del tipo de campo</summary>
''' Tiempo m�ximo: 1 sec </remarks>
Private Sub MostrarOpcionRecordarValores()
    If g_sOrigen = "frmFormularios" Or g_sOrigen = "frmDesglose" Then
        Select Case sdbcTipo.Columns("ID").Value
            Case TiposDeAtributos.TipoTextoCorto
                chkRecordarValores.Value = vbUnchecked
                chkRecordarValores.Visible = True
            Case TiposDeAtributos.TipoTextoMedio
                chkRecordarValores.Value = vbUnchecked
                chkRecordarValores.Visible = True
            Case TiposDeAtributos.TipoTextoLargo
                chkRecordarValores.Value = vbUnchecked
                chkRecordarValores.Visible = True
            Case Else
                chkRecordarValores.Value = vbUnchecked
                chkRecordarValores.Visible = False
        End Select
        'Reviso que tambien sea campo de edicion libre
        If chkRecordarValores.Visible = True And Me.chkLista.Value = vbChecked Then
            chkRecordarValores.Value = vbUnchecked
            chkRecordarValores.Visible = False
        End If
    End If
End Sub

''' <summary>Esta funci�n inhabilita que se pueda elegir la opcion lista si esta seleccionada la opci�n recordar valores</summary>
''' Tiempo m�ximo: 0,1 sec </remarks>
''' revisado por ilg (15/11/2011)
Private Sub ComprobarCheckLista()
    If chkRecordarValores.Value = vbChecked Then
        Me.chkLista.Value = vbUnchecked
        Me.chkLista.Enabled = False
    ElseIf chkRecordarValores.Value = vbUnchecked And sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Then
        Me.chkLista.Enabled = False
    Else
        Me.chkLista.Enabled = True
    End If
End Sub

Private Sub CargarComboCamposPadre()
    Dim oCampo As CFormItem
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Dim rs As Recordset
    
    Set rs = oCampo.DevolverListasCampoPadre(frmFormularios.g_oFormSeleccionado.Id, g_lIdGrupo, 0)
    If Not rs.EOF Then
        sdbcListaCampoPadre.AddItem 0 & ""
        While Not rs.EOF
            sdbcListaCampoPadre.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("DEN").Value & Chr(m_lSeparador) & rs("TIPO_CAMPO_GS").Value
            rs.MoveNext
        Wend
    End If
    
    Set oCampo = Nothing
    rs.Close
    Set rs = Nothing
End Sub

Private Sub sdbcListaCampoPadre_CloseUp()
    Dim oCampo As CFormItem
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Dim oColumn As SSDataWidgets_B.Column
    Dim i As Integer
    
    If m_lCampoPadreSeleccionado <> sdbcListaCampoPadre.Columns("ID").Value Then
        m_bRespetarLista = True
        
        'Lo primero que hare sera no mostrar el combo en las celdas de campos padre para que al eliminar no desaparezcan los valores
        For i = 0 To sdbgLista.Cols - 1
            sdbgLista.Columns(i).DropDownHwnd = 0
        Next
            
        For i = sdbgLista.Columns.Count - 1 To 0 Step -1
            If Left(sdbgLista.Columns(i).Name, 10) = "CAMPOPADRE" Or Left(sdbgLista.Columns(i).Name, 11) = "ORDENPADRE_" Then
                sdbgLista.Columns.Remove (i)
            End If
        Next i
    
        m_lCampoPadreSeleccionado = sdbcListaCampoPadre.Columns("ID").Value
    
        If m_lCampoPadreSeleccionado <> 0 Then
            sdbgLista.Columns.Add sdbgLista.Columns.Count
            Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
            oColumn.Name = "CAMPOPADRE_" & m_lCampoPadreSeleccionado
            oColumn.caption = sdbcListaCampoPadre.Columns("DENOMINACION").Value
            oColumn.Alignment = ssCaptionAlignmentLeft
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            oColumn.Position = 0
            oColumn.Text = ""
                
            If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value = TipoCampoGS.material Then
                cmdImportarExcel.Visible = False
                
                'Si es un campo de tipo material
                oColumn.Style = ssStyleEditButton
                'oColumn.DropDownHwnd = 0
                oColumn.Locked = True
                
                sdbgLista.Columns.Add sdbgLista.Columns.Count
                Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
                oColumn.Name = "CAMPOPADREVALOR_" & m_lCampoPadreSeleccionado
                oColumn.Position = 1
                oColumn.Text = ""
                oColumn.Visible = False
                
                sdbgLista.MoveFirst
                For i = 0 To sdbgLista.Rows
                   sdbgLista.Columns("CAMPOPADRE_" & m_lCampoPadreSeleccionado).Text = ""
                   sdbgLista.Columns("CAMPOPADREVALOR_" & m_lCampoPadreSeleccionado).Text = ""
                   
                   sdbgLista.MoveNext
                Next i
            Else
                cmdImportarExcel.Visible = True
                
                oColumn.Style = ssStyleComboBox
                oColumn.DropDownHwnd = sdbddValoresLista.hWnd
                oColumn.Locked = True
            
                'Si es un campo de tipo lista
                sdbgLista.Columns.Add sdbgLista.Columns.Count
                Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
                oColumn.Name = "ORDENPADRE_" & m_lCampoPadreSeleccionado
                oColumn.Position = 1
                oColumn.Text = ""
                
                oColumn.Visible = False
                
                sdbgLista.MoveFirst
                For i = 0 To sdbgLista.Rows
                   sdbgLista.Columns("CAMPOPADRE_" & m_lCampoPadreSeleccionado).Text = ""
                   sdbgLista.Columns("ORDENPADRE_" & m_lCampoPadreSeleccionado).Text = ""
                   
                   sdbgLista.MoveNext
                Next i
                        
                'Comprobar si el campo padre seleccionado tb dispone de otro campo padre...y sucesivos
                Dim sCampoPadre_CampoPadre As String
                Dim lCampoPadreAnt As Long
                lCampoPadreAnt = m_lCampoPadreSeleccionado
                sCampoPadre_CampoPadre = oCampo.CargarCampoPadre(m_lCampoPadreSeleccionado, True)
                Dim aux() As String
                
                While sCampoPadre_CampoPadre <> ""
                    aux = Split(sCampoPadre_CampoPadre, "$$$")
                                        
                    oCampo.Id = aux(0)
                    oCampo.CargarDatosFormCampo
                    If oCampo.CampoGS = TipoCampoGS.material Then cmdImportarExcel.Visible = False
                            
                    sdbgLista.Columns.Add sdbgLista.Columns.Count
                    Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
                    oColumn.Name = "CAMPOPADRE_" & aux(0)
                    oColumn.caption = aux(1)
                    oColumn.Alignment = ssCaptionAlignmentLeft
                    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                    oColumn.Position = 0
                    oColumn.Text = ""
                    
                    oColumn.Style = ssStyleComboBox
                    oColumn.Locked = True
                    
                    sdbgLista.Columns.Add sdbgLista.Columns.Count
                    Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
                    If aux(2) = TipoCampoGS.material Then
                        oColumn.Name = "CAMPOPADREVALOR_" & aux(0)
                    Else
                        oColumn.Name = "ORDENPADRE_" & aux(0)
                    End If
                    oColumn.Position = 1
                    oColumn.Text = ""
                    oColumn.Visible = False
                    
                    'Si Tiene algun campo padre lo primero asociamos al hijo q el padre
                    sdbgLista.Columns.Add sdbgLista.Columns.Count
                    Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
                    oColumn.Name = "CAMPOPADREPADRE_" & lCampoPadreAnt
                    oColumn.Position = 2
                    oColumn.Visible = False
                    oColumn.Value = aux(0)
                    oColumn.Locked = True
                    
                    lCampoPadreAnt = aux(0)
                    sCampoPadre_CampoPadre = oCampo.CargarCampoPadre(aux(0), True)
                Wend
            End If
        End If
           
        If sdbcListaCampoPadre.Columns("ID").Value <> "0" Then
            sdbgLista.ScrollBars = ssScrollBarsBoth
        Else
            sdbgLista.ScrollBars = ssScrollBarsVertical
        End If
        Set oCampo = Nothing
        
        m_bRespetarLista = False
    End If
End Sub


Private Sub sdbddValoresLista_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddValoresLista.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValoresLista.Rows - 1
            bm = sdbddValoresLista.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValoresLista.Columns(2).CellText(bm), 1, Len(Text))) Then
                sdbddValoresLista.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValoresLista_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddValoresLista.DataFieldList = "Column 0"
    sdbddValoresLista.DataFieldToDisplay = "Column 2"
End Sub

''' <summary>
''' Carga el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddValoresLista_DropDown()
    Dim oCampo As CFormItem
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Dim lIdCampoPadre As Long
    Dim rs As Recordset
    Dim lCampoPadrePadre As Long
    Dim iOrdenPadrePadre As Integer
    Dim arCampoPadre() As String
    Dim dColWidth As Single
    Dim sGMN As String
    
    lIdCampoPadre = Mid(sdbgLista.Columns(sdbgLista.col).Name, 12, Len(sdbgLista.Columns(sdbgLista.col).Name) - 11)
      
    'A�adimos un marcador para obtener el Id del campoPadre del padre, (Se almacena en la primera fila) cndo se creo las cabeceras
    If Not sdbgLista.Columns("CAMPOPADREPADRE_" & lIdCampoPadre) Is Nothing Then
        If sdbgLista.Columns("CAMPOPADREPADRE_" & lIdCampoPadre).Value <> "" Then lCampoPadrePadre = sdbgLista.Columns("CAMPOPADREPADRE_" & lIdCampoPadre).Value
        If Not sdbgLista.Columns("CAMPOPADREVALOR_" & lCampoPadrePadre) Is Nothing Then sGMN = sdbgLista.Columns("CAMPOPADREVALOR_" & lCampoPadrePadre).Value
        
        If lCampoPadrePadre > 0 Then
            If Not sdbgLista.Columns("ORDENPADRE_" & lCampoPadrePadre) Is Nothing Then
                If sdbgLista.Columns("ORDENPADRE_" & lCampoPadrePadre).Value <> "" Then
                    iOrdenPadrePadre = sdbgLista.Columns("ORDENPADRE_" & lCampoPadrePadre).Value
                Else
                    Exit Sub
                End If
            End If
        Else
            Exit Sub
        End If
    End If

    Set rs = oCampo.DevolverValoresListasCampoPadre(lIdCampoPadre, iOrdenPadrePadre, sdbcTipo.Columns("ID").Value, sGMN)
    sdbddValoresLista.RemoveAll

    While Not rs.EOF
        If rs("TIPO_CAMPO_GS").Value = TipoCampoGS.material Then
            dColWidth = 3500
            
            Dim sCod As String
            Dim sDen As String
            sCod = rs("GMN1") & IIf(IsNull(rs("GMN2")), "", "|" & rs("GMN2") & IIf(IsNull(rs("GMN3")), "", "|" & rs("GMN3") & IIf(IsNull(rs("GMN4")), "", "|" & rs("GMN4"))))
            sDen = rs("GMN1") & IIf(IsNull(rs("GMN2")), "", " - " & rs("GMN2") & IIf(IsNull(rs("GMN3")), "", " - " & rs("GMN3") & IIf(IsNull(rs("GMN4")), "", " - " & rs("GMN4")))) & " - " & rs("GMN_DEN").Value
            sdbddValoresLista.AddItem rs("CAMPO").Value & Chr(m_lSeparador) & sCod & Chr(m_lSeparador) & sDen
        Else
            dColWidth = 1814
            sdbddValoresLista.AddItem rs("CAMPO").Value & Chr(m_lSeparador) & rs("ORDEN").Value & Chr(m_lSeparador) & rs("TEXTO").Value
        End If
        
        rs.MoveNext
    Wend
    
    sdbddValoresLista.Columns("TEXTO").Width = dColWidth
    
    Set oCampo = Nothing
End Sub

''' <summary>
''' Tras seleccionar un combo padre se carga el combo hijo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddValoresLista_CloseUp()
    Dim lIdCampoPadre As Long
    Dim HayQLimpiar As Boolean
    
    If sdbgLista.col = -1 Then Exit Sub
    lIdCampoPadre = Mid(sdbgLista.Columns(sdbgLista.col).Name, 12, Len(sdbgLista.Columns(sdbgLista.col).Name) - 11)
    
    If Not sdbgLista.Columns("ORDENPADRE_" & lIdCampoPadre) Is Nothing Then
        HayQLimpiar = (sdbgLista.Columns("ORDENPADRE_" & lIdCampoPadre).Value <> sdbddValoresLista.Columns("ORDEN").Value)
        sdbgLista.Columns("ORDENPADRE_" & lIdCampoPadre).Value = sdbddValoresLista.Columns("ORDEN").Value
    Else
        If Not sdbgLista.Columns("CAMPOPADREVALOR_" & lIdCampoPadre) Is Nothing Then
            HayQLimpiar = (sdbgLista.Columns("CAMPOPADREVALOR_" & lIdCampoPadre).Value <> sdbddValoresLista.Columns("ORDEN").Value)
            sdbgLista.Columns("CAMPOPADREVALOR_" & lIdCampoPadre).Value = sdbddValoresLista.Columns("ORDEN").Value
        End If
    End If
    
    Dim i As Integer
    Dim b_Seguir As Boolean
    Dim l_IdCampoPadre As Long
    b_Seguir = True
    i = 0
    While b_Seguir
        b_Seguir = False
    
        For i = 0 To sdbgLista.Columns.Count - 1
            If Left(sdbgLista.Columns(i).Name, 16) = "CAMPOPADREPADRE_" Then
                If sdbgLista.Columns(i).Value = lIdCampoPadre Then
                    l_IdCampoPadre = Mid(sdbgLista.Columns(i).Name, 17, Len(sdbgLista.Columns(i).Name) - 16)
                    sdbgLista.Columns("CAMPOPADRE_" & l_IdCampoPadre).DropDownHwnd = 0
                    If HayQLimpiar Then sdbgLista.Columns("CAMPOPADRE_" & l_IdCampoPadre).Text = ""
                    b_Seguir = True
                    lIdCampoPadre = l_IdCampoPadre
                    Exit For
                End If
            End If
        Next
    Wend
End Sub


''' <summary>
''' Valida el valor introducido
''' </summary>
''' <param name="Text">texto introducido</param>
''' <param name="RtnPassed">Es el valor que devuelve al notificar al control que los datos son v�lidos o no</param>
''' <remarks>Llamada desde:=Evento que salta cuando cambiamos de valor en la celda que contiene la combo y cambiamos de celda; Tiempo m�ximo:=0seg.</remarks>
Private Sub sdbddValoresLista_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Long
Dim bm As Variant

On Error Resume Next

sdbddValoresLista.MoveFirst

If Text <> "" Then
    For i = 0 To sdbddValoresLista.Rows - 1
        bm = sdbddValoresLista.GetBookmark(i)
        If UCase(Text) = UCase(sdbddValoresLista.Columns(2).CellText(bm)) Then
            sdbddValoresLista.Bookmark = bm
            Exit For
        End If
    Next i
    If i > sdbddValoresLista.Rows - 1 Then
        'No ha encontrado el valor
        sdbgLista.Columns(sdbgLista.col).Text = ""
        sdbddValoresLista.Row = 0
    End If
End If
End Sub


Private Sub cmdImportarExcel_Click()
Dim sFileName As String
Dim sIdiDialogoExcel As String
Dim sIdiFiltroExcel As String
Dim sPorDefecto As String

Dim sConnect As String
Dim oExcelAdoConn As ADODB.Connection
Dim oExcelAdoRS As ADODB.Recordset
Dim Resultadoxls()
Dim iNumColumnas, i As Integer
Dim lNumFilas, j As Long
sPorDefecto = "Fichero"

On Error GoTo Error


sFileName = MostrarCommonDialog(sIdiDialogoExcel, sIdiFiltroExcel, sPorDefecto & ".xls")
If sFileName = "" Then
    Exit Sub
End If

Set oExcelAdoConn = New ADODB.Connection
sConnect = "Provider=MSDASQL.1;" _
         & "Extended Properties=""DBQ=" & sFileName & ";" _
         & "Driver={Microsoft Excel Driver (*.xls)};" _
         & "FIL=excel 8.0;" _
         & "ReadOnly=0;" _
         & "UID=admin;"""

oExcelAdoConn.Open sConnect

Set oExcelAdoRS = New ADODB.Recordset

oExcelAdoRS.Open "SELECT * FROM [" & "LISTA" & "$]", oExcelAdoConn

Dim k As Integer
For k = 0 To sdbgLista.Cols - 1
        sdbgLista.Columns(k).DropDownHwnd = 0
Next
        
Dim sCampoPadre_CampoPadre As String
Dim sTextoGrid As String
Dim sValor As String
Dim sCadena As String
Dim lIdCampoPadre As Long
Dim sValorABuscar As String
Dim sLinea As String
Dim sCamposPadrePadre As String

Dim oCampo As CFormItem
Set oCampo = oFSGSRaiz.Generar_CFormCampo
Set oCampo.Grupo = oFSGSRaiz.Generar_CFormGrupo
oCampo.Grupo.Id = g_lIdGrupo

Dim oValor As CCampoValorLista

Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas

Set oGestorParametros = oFSGSRaiz.Generar_CGestorParametros
Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)

If Not oExcelAdoRS.EOF Then
    
    Resultadoxls = oExcelAdoRS.GetRows()
    iNumColumnas = UBound(Resultadoxls) + 1
    lNumFilas = UBound(Resultadoxls, 2) + 1
    Dim filasMultiIdioma As Integer
    m_bRespetarLista = True
    If m_bMultiIdioma Then
        filasMultiIdioma = oIdiomas.Count
    Else
        filasMultiIdioma = 1
    End If
        
    For j = 0 To lNumFilas - 1
        sTextoGrid = ""
        sCamposPadrePadre = ""
        sCadena = ""
        lIdCampoPadre = StrToDbl0(Me.sdbcListaCampoPadre.Columns("ID").Value)
        For i = (iNumColumnas - 1) To 0 Step -1
            
            sValor = Resultadoxls(i, j)
            If i > iNumColumnas - filasMultiIdioma - 1 Then
                'Columna del valor
                If sCadena = "" Then
                    sCadena = sValor
                Else
                    sCadena = sCadena & Chr(m_lSeparador) & sValor
                End If
            
            Else
                'Comprobar si se trata de una lista enlazada
                If lIdCampoPadre > 0 Then
                    'Comprobar si el campo padre seleccionado tb dispone de otro campo padre...y sucesivos
                    oCampo.Id = lIdCampoPadre
                    oCampo.Tipo = sdbcTipo.Columns("ID").Value
                    oCampo.CargarValoresLista
                    
                    If Not oCampo.ValoresLista Is Nothing Then
                        'Recorrido de los valores de lista hasta que encontremos el valor a buscar
                        For Each oValor In oCampo.ValoresLista
                            Select Case sdbcTipo.Columns("ID").Value
                                Case TiposDeAtributos.TipoFecha
                                    sValorABuscar = oValor.valorFec
                                Case TiposDeAtributos.TipoNumerico
                                    sValorABuscar = oValor.valorNum
                                    
                                Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                                    sValorABuscar = NullToStr(oValor.valorText.Item(gParametrosInstalacion.gIdioma).Den)
                            End Select
                            
                            If sValor = sValorABuscar Then
                                If sTextoGrid <> "" Then sTextoGrid = sTextoGrid & Chr(m_lSeparador)
                                sTextoGrid = sTextoGrid & sValorABuscar & Chr(m_lSeparador) & oValor.Orden '& Chr(m_lSeparador) & oValor.IdCampoPadre
                                'Buscar el campo Padre del campo que estamos buscando
                                sCampoPadre_CampoPadre = oCampo.CargarCampoPadre(lIdCampoPadre, True)
                                If sCampoPadre_CampoPadre <> "" Then
                                    Dim aux() As String
                                    aux = Split(sCampoPadre_CampoPadre, "$$$")
                                    lIdCampoPadre = aux(0)
                                End If
                                Exit For
                            End If

                        Next
                        
                    End If
                    
                    sCamposPadrePadre = sCamposPadrePadre & Chr(m_lSeparador) & lIdCampoPadre
                                        
                End If
                
            End If
                        
        Next i
        
        'A�adimos la linea encontrada
        If sTextoGrid <> "" Then sCadena = sCadena & Chr(m_lSeparador)
        
        If iNumColumnas > filasMultiIdioma + 1 Then
            sTextoGrid = sTextoGrid & sCamposPadrePadre
        End If
        sLinea = sCadena & sTextoGrid
        If (lIdCampoPadre > 0 And sTextoGrid <> "") Or (lIdCampoPadre = 0 And sCadena <> "") Then
            sdbgLista.AddItem sLinea
        End If
        
    Next j
    sdbgLista.Update
    m_bRespetarLista = False
        
    oExcelAdoRS.Close

End If

Set oCampo = Nothing


Exit Sub


Error:

If err.Number <> cdlCancel Then
    oMensajes.MensajeOKOnly 754
    If oExcelAdoConn.State <> adStateClosed Then
        oExcelAdoConn.Close
    End If
End If



End Sub

'Muestra el cuadro de dialogo para cargar el fichero excel
Function MostrarCommonDialog(titulo As String, filtro As String, sFichero As String) As String
    
    cmmdExcel.CancelError = True

    On Error Resume Next
    cmmdExcel.DialogTitle = titulo
    cmmdExcel.Filter = filtro
    cmmdExcel.filename = ValidFilename(sFichero)
    cmmdExcel.Filter = "Excel files|*.xls"
    cmmdExcel.ShowOpen

    If err.Number = cdlCancel Then
        ' The user canceled.
        MostrarCommonDialog = ""
        Exit Function
    End If
    On Error GoTo 0
    
    If cmmdExcel.FileTitle = "" Then
        MostrarCommonDialog = ""
        Exit Function
    End If

    MostrarCommonDialog = cmmdExcel.filename
    
End Function

