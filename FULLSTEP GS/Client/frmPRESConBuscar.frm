VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPRESConBuscar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Presupuestos por partida contable (Buscar)"
   ClientHeight    =   3645
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6780
   Icon            =   "frmPRESConBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3645
   ScaleWidth      =   6780
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6375
      Picture         =   "frmPRESConBuscar.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Cargar"
      Top             =   435
      Width           =   315
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      Height          =   645
      Left            =   75
      ScaleHeight     =   585
      ScaleWidth      =   6105
      TabIndex        =   6
      Top             =   75
      Width           =   6165
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   855
         MaxLength       =   50
         TabIndex        =   0
         Top             =   120
         Width           =   1095
      End
      Begin VB.TextBox txtDen 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3195
         MaxLength       =   150
         TabIndex        =   1
         Top             =   120
         Width           =   2745
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2070
         TabIndex        =   8
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   135
         TabIndex        =   7
         Top             =   180
         Width           =   555
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2040
      TabIndex        =   4
      Top             =   3270
      Width           =   1125
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3390
      TabIndex        =   5
      Top             =   3270
      Width           =   1125
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPresupuestos 
      Height          =   2310
      Left            =   120
      TabIndex        =   3
      Top             =   855
      Width           =   6585
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   5
      stylesets.count =   2
      stylesets(0).Name=   "ActiveRow"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8421376
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPRESConBuscar.frx":01D5
      stylesets(1).Name=   "ActiveRowBlue"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8388608
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPRESConBuscar.frx":01F1
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Columns.Count   =   5
      Columns(0).Width=   1270
      Columns(0).Caption=   "C�digo1"
      Columns(0).Name =   "PRES1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1270
      Columns(1).Caption=   "C�digo2"
      Columns(1).Name =   "PRES2"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1349
      Columns(2).Caption=   "C�digo3"
      Columns(2).Name =   "PRES3"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1296
      Columns(3).Caption=   "C�digo4"
      Columns(3).Name =   "PRES4"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   6112
      Columns(4).Caption=   "Denominaci�n"
      Columns(4).Name =   "DEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   11615
      _ExtentY        =   4075
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPRESConBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sOrigen As String
Private oPresN1 As CPresContablesNivel1
Private oPresN2 As CPresContablesNivel2
Private oPresN3 As CPresContablesNivel3
Private oPresN4 As CPresContablesNivel4
'Multilenguaje
'Private sIdiPresupPor As String
Private sIdiBuscar As String

Private m_iAnyo As Integer
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Private m_bBajaLog As Boolean


Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim i As Integer
Dim j As Integer
On Error GoTo NoSeEncuentra

    If sdbgPresupuestos.Rows = 0 Then Exit Sub
    
    If Trim(sdbgPresupuestos.Columns(3).value) <> "" Then
        j = 4
    Else
        If Trim(sdbgPresupuestos.Columns(2).value) <> "" Then
            j = 3
        Else
            If Trim(sdbgPresupuestos.Columns(1).value) <> "" Then
                j = 2
            Else
                If Trim(sdbgPresupuestos.Columns(0).value) <> "" Then
                    j = 1
                End If
            End If
        End If
    End If
    
    If sOrigen = "frmPresupuestos2" Then

        Select Case j
    
            Case 1
                
                scod1 = sdbgPresupuestos.Columns(0).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(sdbgPresupuestos.Columns(0).value), " ")
                Set nodx = frmPresupuestos2.tvwestrPres.Nodes("PRES1" & scod1)
                nodx.Selected = True
                nodx.EnsureVisible
                frmPresupuestos2.tvwEstrPres_NodeClick nodx
            
            Case 2
                
                scod1 = sdbgPresupuestos.Columns(0).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(sdbgPresupuestos.Columns(0).value), " ")
                scod2 = sdbgPresupuestos.Columns(1).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(sdbgPresupuestos.Columns(1).value), " ")
                Set nodx = frmPresupuestos2.tvwestrPres.Nodes("PRES2" & scod1 & scod2)
                nodx.Selected = True
                nodx.EnsureVisible
                frmPresupuestos2.tvwEstrPres_NodeClick nodx
                
            Case 3
                
                scod1 = sdbgPresupuestos.Columns(0).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(sdbgPresupuestos.Columns(0).value), " ")
                scod2 = sdbgPresupuestos.Columns(1).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(sdbgPresupuestos.Columns(1).value), " ")
                scod3 = sdbgPresupuestos.Columns(2).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(sdbgPresupuestos.Columns(2).value), " ")
                Set nodx = frmPresupuestos2.tvwestrPres.Nodes("PRES3" & scod1 & scod2 & scod3)
                nodx.Selected = True
                nodx.EnsureVisible
                frmPresupuestos2.tvwEstrPres_NodeClick nodx
                        
            Case 4
                
                scod1 = sdbgPresupuestos.Columns(0).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(sdbgPresupuestos.Columns(0).value), " ")
                scod2 = sdbgPresupuestos.Columns(1).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(sdbgPresupuestos.Columns(1).value), " ")
                scod3 = sdbgPresupuestos.Columns(2).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(sdbgPresupuestos.Columns(2).value), " ")
                scod4 = sdbgPresupuestos.Columns(3).value & String(basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(sdbgPresupuestos.Columns(3).value), " ")
                Set nodx = frmPresupuestos2.tvwestrPres.Nodes("PRES4" & scod1 & scod2 & scod3 & scod4)
                nodx.Selected = True
                nodx.EnsureVisible
                frmPresupuestos2.tvwEstrPres_NodeClick nodx
                
        End Select

        frmPresupuestos2.MostrarDatosBarraInf
        Unload Me
        Exit Sub
            
    Else
    
        If sOrigen = "frmPRESAsig" Then
    
            Select Case j
    
                Case 1
                
                    scod1 = sdbgPresupuestos.Columns(0).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(sdbgPresupuestos.Columns(0).value))
                    Set nodx = frmPRESAsig.tvwestrPres.Nodes("PRES1" & scod1)
                    nodx.Selected = True
                    nodx.EnsureVisible
        
                Case 2
                    
                    scod1 = sdbgPresupuestos.Columns(0).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(sdbgPresupuestos.Columns(0).value))
                    scod2 = sdbgPresupuestos.Columns(1).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sdbgPresupuestos.Columns(1).value))
                    Set nodx = frmPRESAsig.tvwestrPres.Nodes("PRES2" & scod1 & scod2)
                    nodx.Selected = True
                    nodx.EnsureVisible
            
                Case 3
                
                    scod1 = sdbgPresupuestos.Columns(0).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(sdbgPresupuestos.Columns(0).value))
                    scod2 = sdbgPresupuestos.Columns(1).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sdbgPresupuestos.Columns(1).value))
                    scod3 = sdbgPresupuestos.Columns(2).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sdbgPresupuestos.Columns(2).value))
                    Set nodx = frmPRESAsig.tvwestrPres.Nodes("PRES3" & scod1 & scod2 & scod3)
                    nodx.Selected = True
                    nodx.EnsureVisible
            
                Case 4
                
                    scod1 = sdbgPresupuestos.Columns(0).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(sdbgPresupuestos.Columns(0).value))
                    scod2 = sdbgPresupuestos.Columns(1).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(sdbgPresupuestos.Columns(1).value))
                    scod3 = sdbgPresupuestos.Columns(2).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(sdbgPresupuestos.Columns(2).value))
                    scod4 = sdbgPresupuestos.Columns(3).value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(sdbgPresupuestos.Columns(3).value))
                    Set nodx = frmPRESAsig.tvwestrPres.Nodes("PRES4" & scod1 & scod2 & scod3 & scod4)
                    nodx.Selected = True
                    nodx.EnsureVisible
            
            End Select
        
            frmPRESAsig.MostrarDatosBarraInf
            frmPRESAsig.ConfigurarInterfazPresup nodx
            Unload Me
            If frmPRESAsig.Visible Then frmPRESAsig.tvwestrPres.SetFocus
            Exit Sub
        
        End If
    
    End If
    
NoSeEncuentra:
    oMensajes.PartidaNueva basParametros.gParametrosGenerales.gsSingPres2
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub cmdCargar_Click()

    sdbgPresupuestos.RemoveAll

    Select Case gParametrosGenerales.giNEPC
        
        Case 1
                    
            Set oPresN1 = oFSGSRaiz.Generar_CPresContablesNivel1
                  
            If m_bBajaLog Then
                'Busco entre todos los presupuestos, incluidos los de baja l�gica
                oPresN1.CargarTodosLosPresupuestos m_iAnyo, Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
            Else
                'S�lo busco entre los presupuestos que no esten de baja l�gica
                oPresN1.CargarTodosLosPresupuestos m_iAnyo, Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
            End If
                    
                    CargarGrid "PRES1"
                    
        Case 2
                    
            Set oPresN1 = oFSGSRaiz.Generar_CPresContablesNivel1
        
            Set oPresN2 = oFSGSRaiz.Generar_CPresContablesNivel2
             
            If m_bBajaLog Then
                'Busco entre todos los presupuestos, incluidos los de baja l�gica
                oPresN1.CargarTodosLosPresupuestos m_iAnyo, Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
                oPresN2.CargarTodosLosPresupuestos m_iAnyo, , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
            Else
                'S�lo busco entre los presupuestos que no esten de baja l�gica
                oPresN1.CargarTodosLosPresupuestos m_iAnyo, Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
                oPresN2.CargarTodosLosPresupuestos m_iAnyo, , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
            End If
                    
            CargarGrid "PRES1"
            CargarGrid "PRES2"
                    
        Case 3
                    
                    Set oPresN3 = oFSGSRaiz.Generar_CPresContablesNivel3
                
                    Set oPresN1 = oFSGSRaiz.Generar_CPresContablesNivel1
                 
                    Set oPresN2 = oFSGSRaiz.Generar_CPresContablesNivel2
                  
            If m_bBajaLog Then
                'Busco entre todos los presupuestos, incluidos los de baja l�gica
                oPresN1.CargarTodosLosPresupuestos m_iAnyo, Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
                oPresN2.CargarTodosLosPresupuestos m_iAnyo, , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
                oPresN3.CargarTodosLosPresupuestos m_iAnyo, , , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
            Else
                'S�lo busco entre los presupuestos que no esten de baja l�gica
                oPresN1.CargarTodosLosPresupuestos m_iAnyo, Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
                oPresN2.CargarTodosLosPresupuestos m_iAnyo, , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
                oPresN3.CargarTodosLosPresupuestos m_iAnyo, , , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
            End If
                    
            CargarGrid "PRES1"
            CargarGrid "PRES2"
            CargarGrid "PRES3"
                    
        Case 4
                    
            Set oPresN4 = oFSGSRaiz.Generar_CPresContablesNivel4
           
            Set oPresN3 = oFSGSRaiz.Generar_CPresContablesNivel3
         
            Set oPresN1 = oFSGSRaiz.Generar_CPresContablesNivel1
          
            Set oPresN2 = oFSGSRaiz.Generar_CPresContablesNivel2
    
                
            If m_bBajaLog Then
                'Busco entre todos los presupuestos, incluidos los de baja l�gica
                oPresN1.CargarTodosLosPresupuestos m_iAnyo, Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
                oPresN2.CargarTodosLosPresupuestos m_iAnyo, , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
                oPresN3.CargarTodosLosPresupuestos m_iAnyo, , , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
                oPresN4.CargarTodosLosPresupuestos m_iAnyo, , , , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3
            Else
                'S�lo busco entre los presupuestos que no esten de baja l�gica
                oPresN1.CargarTodosLosPresupuestos m_iAnyo, Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
                oPresN2.CargarTodosLosPresupuestos m_iAnyo, , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
                oPresN3.CargarTodosLosPresupuestos m_iAnyo, , , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
                oPresN4.CargarTodosLosPresupuestos m_iAnyo, , , , Trim(txtCod.Text), Trim(txtDen.Text), , , , , , False, m_sUON1, m_sUON2, m_sUON3, m_bBajaLog
            End If
            
            CargarGrid "PRES1"
            CargarGrid "PRES2"
            CargarGrid "PRES3"
            CargarGrid "PRES4"
        
    End Select
                
End Sub

Private Sub Form_Load()

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Me.caption = gParametrosGenerales.gsPlurPres2 & " " & sIdiBuscar
    
    If sOrigen = "frmPresupuestos2" Then
        m_iAnyo = frmPresupuestos2.sdbcAnyo.value
        m_sUON1 = frmPresupuestos2.m_sUON1
        m_sUON2 = frmPresupuestos2.m_sUON2
        m_sUON3 = frmPresupuestos2.m_sUON3
        m_bBajaLog = frmPresupuestos2.chkBajaLog.value
    Else
        If sOrigen = "frmPRESAsig" Then
            m_iAnyo = frmPRESAsig.sdbcAnyo.value
            m_sUON1 = frmPRESAsig.g_sUON1
            m_sUON2 = frmPRESAsig.g_sUON2
            m_sUON3 = frmPRESAsig.g_sUON3
            m_bBajaLog = False
        End If
    End If
    
    ConfigurarGrid

End Sub

Private Sub CargarGrid(ByVal Opcion As String)
Dim opres1 As CPresConNivel1
Dim oPRES2 As CPresconNivel2
Dim oPRES3 As CPresConNivel3
Dim oPRES4 As CPresconNivel4

'sdbgPresupuestos.RemoveAll

Select Case Opcion
    
    Case "PRES1"
     
        For Each opres1 In oPresN1
           
           sdbgPresupuestos.AddItem opres1.Cod & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & opres1.Den
        Next
        
        Set oPresN1 = Nothing
        
    Case "PRES2"
    
        For Each oPRES2 In oPresN2
            sdbgPresupuestos.AddItem oPRES2.CodPRES1 & Chr(m_lSeparador) & oPRES2.Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oPRES2.Den
        Next
        Set oPresN2 = Nothing
        
    Case "PRES3"
    
        For Each oPRES3 In oPresN3
            sdbgPresupuestos.AddItem oPRES3.CodPRES1 & Chr(m_lSeparador) & oPRES3.CodPRES2 & Chr(m_lSeparador) & oPRES3.Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oPRES3.Den
        Next
        
        Set oPresN3 = Nothing
        
    Case "PRES4"
        
        For Each oPRES4 In oPresN4
            sdbgPresupuestos.AddItem oPRES4.CodPRES1 & Chr(m_lSeparador) & oPRES4.CodPRES2 & Chr(m_lSeparador) & oPRES4.CodPRES3 & Chr(m_lSeparador) & oPRES4.Cod & Chr(m_lSeparador) & oPRES4.Den
        Next
        
        Set oPresN4 = Nothing
        
End Select

End Sub

Private Sub sdbcMateriales_Click()
    
    ConfigurarGrid
    
End Sub

Private Sub ConfigurarGrid()

sdbgPresupuestos.RemoveAll


Select Case gParametrosGenerales.giNEPC

Case 1
    
    sdbgPresupuestos.Columns(0).Visible = True
    sdbgPresupuestos.Columns(1).Visible = False
    sdbgPresupuestos.Columns(2).Visible = False
    sdbgPresupuestos.Columns(3).Visible = False
    sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(4).Width = sdbgPresupuestos.Width * 85 / 100
    
Case 2
    sdbgPresupuestos.Columns(0).Visible = True
    sdbgPresupuestos.Columns(1).Visible = True
    sdbgPresupuestos.Columns(2).Visible = False
    sdbgPresupuestos.Columns(3).Visible = False
    sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(1).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(4).Width = sdbgPresupuestos.Width * 70 / 100
Case 3
    sdbgPresupuestos.Columns(0).Visible = True
    sdbgPresupuestos.Columns(1).Visible = True
    sdbgPresupuestos.Columns(2).Visible = True
    sdbgPresupuestos.Columns(3).Visible = False
    sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(1).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(2).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(4).Width = sdbgPresupuestos.Width * 55 / 100

Case 4
    
    sdbgPresupuestos.Columns(0).Visible = True
    sdbgPresupuestos.Columns(1).Visible = True
    sdbgPresupuestos.Columns(2).Visible = True
    sdbgPresupuestos.Columns(3).Visible = True
    sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(1).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(2).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(3).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(4).Width = sdbgPresupuestos.Width * 40 / 100

End Select
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    Set oPresN1 = Nothing
    Set oPresN2 = Nothing
    Set oPresN3 = Nothing
    Set oPresN4 = Nothing
    
End Sub

Private Sub sdbgPresupuestos_DblClick()
    
    cmdAceptar_Click
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESCON_BUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    'Caption = Ador(0).Value
    'Ador.MoveNext
    lblCod.caption = Ador(0).value
    Ador.MoveNext
    lblDen.caption = Ador(0).value
    Ador.MoveNext
    sdbgPresupuestos.Columns(0).caption = Ador(0).value
    Ador.MoveNext
    sdbgPresupuestos.Columns(1).caption = Ador(0).value
    Ador.MoveNext
    sdbgPresupuestos.Columns(2).caption = Ador(0).value
    Ador.MoveNext
    sdbgPresupuestos.Columns(3).caption = Ador(0).value
    Ador.MoveNext
    sdbgPresupuestos.Columns(4).caption = Ador(0).value
    Ador.MoveNext
    cmdAceptar.caption = Ador(0).value
    Ador.MoveNext
    cmdCancelar.caption = Ador(0).value
    'Ador.MoveNext
    'sIdiPresupPor = Ador(0).Value
    Ador.MoveNext
    sIdiBuscar = Ador(0).value
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub




