VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPROCEXLSResumen 
   Caption         =   "Resultado de la importaci�n"
   ClientHeight    =   7320
   ClientLeft      =   1680
   ClientTop       =   2085
   ClientWidth     =   10995
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEXLSResumen.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7320
   ScaleWidth      =   10995
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin RichTextLib.RichTextBox rtxtBuenos 
      Height          =   3165
      Left            =   135
      TabIndex        =   1
      Top             =   450
      Width           =   10725
      _ExtentX        =   18918
      _ExtentY        =   5583
      _Version        =   393217
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"frmPROCEXLSResumen.frx":0CB2
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   10530
      Top             =   3555
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSalvarMalos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   135
      Picture         =   "frmPROCEXLSResumen.frx":0D32
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3690
      UseMaskColor    =   -1  'True
      Width           =   420
   End
   Begin VB.CommandButton cmdSalvarBuenos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   135
      Picture         =   "frmPROCEXLSResumen.frx":0DB3
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   420
   End
   Begin RichTextLib.RichTextBox rtxtMalos 
      Height          =   3165
      Left            =   180
      TabIndex        =   5
      Top             =   4140
      Width           =   10725
      _ExtentX        =   18918
      _ExtentY        =   5583
      _Version        =   393217
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"frmPROCEXLSResumen.frx":0E34
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Left            =   630
      TabIndex        =   4
      Top             =   3735
      Width           =   10140
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   630
      TabIndex        =   3
      Top             =   90
      Width           =   10140
   End
End
Attribute VB_Name = "frmPROCEXLSResumen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oItemBuenos As CItems
Public g_oItemMalos As CItems
Public g_lNumBuenos As Long
Public g_lNumMalos As Long

Private sIdiFila As String
Private sIdiDoc As String


Private Sub cmdSalvarBuenos_Click()
Dim sFileName As String
Dim sFileTitle As String
Dim DataFile As Integer
Dim oItem As CItem
Dim sTexto As String
Dim i As Integer

On Error GoTo ERROR

        cmmdEsp.DialogTitle = oMensajes.CargarTexto(273, 10)
        cmmdEsp.Filter = sIdiDoc & "|*.txt"
        'cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave

        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido 138 'Archivo
            Exit Sub
        End If
        
        DataFile = 1
        Open sFileName For Output As #1

        For Each oItem In g_oItemBuenos
            sTexto = oItem.GrupoCod & vbTab & oItem.ArticuloCod & vbTab & oItem.Descr
            Print #1, sTexto
        Next
        Close #1
                
        Exit Sub
ERROR:
    If err.Number = cdlCancel Then
        Exit Sub
    ElseIf err.Number = 75 Or err.Number = 70 Then
        oMensajes.ImposibleSalvarArchivo
        Exit Sub
    Else
        MsgBox err.Description, vbCritical, "FULLSTEP GS"
    End If

End Sub

Private Sub cmdSalvarMalos_Click()
Dim sFileName As String
Dim sFileTitle As String
Dim i As Integer
Dim sTexto As String
Dim oItem As CItem

On Error GoTo ERROR

        cmmdEsp.DialogTitle = oMensajes.CargarTexto(273, 10)
        cmmdEsp.Filter = sIdiDoc & "|*.txt"
        'cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave

        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido 138 'Archivo
            Exit Sub
        End If
        
        Open sFileName For Output As #1
        
        For Each oItem In g_oItemMalos
            
            If IsNumeric(oItem.DestCod) Then
                oItem.DestCod = oMensajes.CargarTextoMensaje(oItem.DestCod)
            End If
            sTexto = oItem.GrupoCod & vbTab & oItem.ArticuloCod & vbTab & oItem.Descr
            
            sTexto = sTexto & vbTab & oItem.DestCod & vbTab & sIdiFila & Space(1) & oItem.Id
            Print #1, sTexto
        Next
        
        Close #1
        
        Exit Sub
ERROR:
    If err.Number = cdlCancel Then
        Exit Sub
    ElseIf err.Number = 75 Or err.Number = 70 Then
        oMensajes.ImposibleSalvarArchivo
        Exit Sub
    Else
        MsgBox err.Description, vbCritical, "FULLSTEP GS"
    End If


End Sub


Private Sub Form_Load()

Me.caption = oMensajes.CargarTexto(OTROS, 165)
Label2.caption = oMensajes.CargarTexto(OTROS, 166) & " " & g_lNumBuenos
Label3.caption = oMensajes.CargarTexto(OTROS, 167) & " " & g_lNumMalos
sIdiFila = oMensajes.CargarTexto(OTROS, 168)
sIdiDoc = oMensajes.CargarTexto(OTROS, 169)
    
Screen.MousePointer = vbHourglass
Mostrar
Screen.MousePointer = vbNormal
End Sub
Private Sub Mostrar()
Dim i As Integer
Dim sTexto As String
Dim sCausa As String
Dim oItem As CItem
Dim sPrincipio As String
Dim sFinal As String
Dim iNumChars As Integer
Dim iSobran As Integer
Dim teserror As TipoErrorSummit

sTexto = ""
iNumChars = (rtxtBuenos.Width / 107.25)
For Each oItem In g_oItemBuenos
    sPrincipio = oItem.GrupoCod & Space(gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(oItem.GrupoCod)) & " "
    If Not IsNull(oItem.ArticuloCod) Then
        sPrincipio = sPrincipio & oItem.ArticuloCod & Space(gLongitudesDeCodigos.giLongCodART - Len(oItem.ArticuloCod)) & " "
    End If
    iSobran = iNumChars - Len(sPrincipio)
    If Len(oItem.Descr) < iSobran Then
        sTexto = sTexto & sPrincipio & oItem.Descr & Space(iSobran - Len(oItem.Descr)) & vbCrLf
    Else
        sTexto = sTexto & sPrincipio & Left(oItem.Descr, iSobran) & vbCrLf
    End If
Next
rtxtBuenos.Text = sTexto

sTexto = ""
iNumChars = (rtxtMalos.Width / 107.25)
For Each oItem In g_oItemMalos
    
    If IsNumeric(oItem.DestCod) Then
        If oItem.DestCod = 868 Then 'Caso de ART4_UON al importar el excel
            oItem.DestCod = vbCrLf & oMensajes.CargarTextoMensaje(oItem.DestCod) & oItem.ArticuloCod & oMensajes.CargarTextoMensaje(871) & vbCrLf & oItem.Articulo.uons.toString
        ElseIf oItem.DestCod = 962 Then 'restricci�n de la apertura de �tems al material del comprador
            oItem.DestCod = vbCrLf & oMensajes.CargarTextoMensaje(oItem.DestCod)
        ElseIf oItem.DestCod = 1414 Then
            oItem.DestCod = vbCrLf & oMensajes.CargarTextoMensaje(oItem.DestCod) & vbCrLf
        Else
            If (oItem.GMN1Cod <> "") And (oItem.GMN2Cod <> "") And (oItem.GMN3Cod <> "") And (oItem.GMN4Cod <> "") Then
                'El item que se quiere a�adir no pertenece al grupo de material del proceso
                oItem.DestCod = oMensajes.CargarTextoMensaje(914) & vbCrLf
            Else
                oItem.DestCod = oMensajes.CargarTextoMensaje(oItem.DestCod)
            End If
        End If
    End If
    sPrincipio = oItem.GrupoCod & Space(gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(oItem.GrupoID)) & " "
    sPrincipio = sPrincipio & oItem.ArticuloCod & Space(gLongitudesDeCodigos.giLongCodART - Len(oItem.ArticuloCod)) & " "
    sFinal = " " & oItem.DestCod & " " & sIdiFila & " " & oItem.Id & vbCrLf
    iSobran = iNumChars - Len(sPrincipio) - Len(sFinal)
    If iSobran < 0 Then iSobran = 0
    If Len(oItem.Descr) < iSobran Then
        sTexto = sTexto & sPrincipio & oItem.Descr & Space(iSobran - Len(oItem.Descr)) & sFinal
    Else
        sTexto = sTexto & sPrincipio & Left(oItem.Descr, iSobran) & sFinal
    End If
Next
rtxtMalos.Text = sTexto
End Sub
Private Sub Form_Resize()

On Error Resume Next
If Me.Height < 500 Then Exit Sub
If Me.Width < 500 Then Exit Sub

rtxtBuenos.Width = Me.Width - 400
rtxtBuenos.Height = Me.Height / 2 - cmdSalvarBuenos.Height - 500
cmdSalvarMalos.Top = rtxtBuenos.Top + rtxtBuenos.Height + 200
Label3.Top = cmdSalvarMalos.Top + 30
rtxtMalos.Top = cmdSalvarMalos.Top + cmdSalvarMalos.Height + 100
rtxtMalos.Width = rtxtBuenos.Width
rtxtMalos.Height = rtxtBuenos.Height
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oItemBuenos = Nothing
    Set g_oItemMalos = Nothing
End Sub

