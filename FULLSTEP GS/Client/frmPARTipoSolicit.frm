VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPARTipoSolicit 
   Caption         =   "DTipos de solicitudes"
   ClientHeight    =   6495
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10050
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPARTipoSolicit.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6495
   ScaleWidth      =   10050
   Begin VB.PictureBox picEdit 
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   120
      ScaleHeight     =   450
      ScaleWidth      =   9000
      TabIndex        =   10
      Top             =   6080
      Width           =   9000
      Begin VB.CommandButton cmdEliminarCampo 
         Caption         =   "DEliminar campo"
         Enabled         =   0   'False
         Height          =   345
         Left            =   6460
         TabIndex        =   9
         Top             =   0
         Width           =   1700
      End
      Begin VB.CommandButton cmdAnyadirCampoGS 
         Caption         =   "DA�adir campo del sistema"
         Enabled         =   0   'False
         Height          =   345
         Left            =   4140
         TabIndex        =   8
         Top             =   0
         Width           =   2200
      End
      Begin VB.CommandButton cmdAnyaAtrib 
         Caption         =   "DA�adir atributo de GS"
         Enabled         =   0   'False
         Height          =   345
         Left            =   1820
         TabIndex        =   7
         Top             =   0
         Width           =   2200
      End
      Begin VB.CommandButton cmdCrearCampo 
         Caption         =   "DCrear campo nuevo"
         Enabled         =   0   'False
         Height          =   345
         Left            =   0
         TabIndex        =   6
         Top             =   0
         Width           =   1700
      End
   End
   Begin VB.PictureBox picCabecera 
      BorderStyle     =   0  'None
      Height          =   500
      Left            =   120
      ScaleHeight     =   495
      ScaleWidth      =   9855
      TabIndex        =   11
      Top             =   120
      Width           =   9855
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoDen 
         Height          =   285
         Left            =   2500
         TabIndex        =   1
         Top             =   120
         Width           =   4695
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 2"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7805
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 0"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1905
         Columns(2).Caption=   "COD"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 1"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8281
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoCod 
         Height          =   285
         Left            =   960
         TabIndex        =   0
         Top             =   120
         Width           =   1500
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 2"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2196
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 0"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   6906
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 1"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   2646
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.CommandButton cmdAnyaTipo 
         Height          =   285
         Left            =   7300
         Picture         =   "frmPARTipoSolicit.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdElimTipo 
         Enabled         =   0   'False
         Height          =   285
         Left            =   7720
         Picture         =   "frmPARTipoSolicit.frx":01CC
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdModifTipo 
         Enabled         =   0   'False
         Height          =   285
         Left            =   8140
         Picture         =   "frmPARTipoSolicit.frx":025E
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   120
         Width           =   315
      End
      Begin VB.Label lblTipo 
         Caption         =   "DTipo:"
         Height          =   285
         Left            =   0
         TabIndex        =   12
         Top             =   160
         Width           =   900
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   5320
      Left            =   120
      TabIndex        =   5
      Top             =   700
      Width           =   9855
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   2
      stylesets.count =   2
      stylesets(0).Name=   "Gris"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPARTipoSolicit.frx":0345
      stylesets(1).Name=   "Amarillo"
      stylesets(1).BackColor=   12648447
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPARTipoSolicit.frx":0361
      DividerType     =   0
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1588
      Columns(1).Caption=   "AYUDA"
      Columns(1).Name =   "AYUDA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   4
      Columns(1).ButtonsAlways=   -1  'True
      _ExtentX        =   17383
      _ExtentY        =   9384
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPARTipoSolicit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_Accion As AccionesSummit
Public g_oTipos As CTiposSolicit
Public g_oTipoSeleccionado As CTipoSolicit
Public g_ofrmATRIB As frmAtrib
Public g_ofrmDesglose As frmPARTipoSolicitDesglose

Public m_oIdiomas As CIdiomas
Private m_oIBaseDatos As IBaseDatos

Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean
Private m_oCampoEnEdicion As CCampoPredef

'Variables de idiomas:
Private m_sDato As String
Private m_sCamposGS(37) As String
Private m_sCamposSC(9) As String
Private m_ConCampoDesgloseActividad As Boolean


Private Sub cmdAnyaAtrib_Click()
    'A�ade un campo de tipo atributo de GS
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If

    Set g_ofrmATRIB = New frmAtrib
    g_ofrmATRIB.g_sOrigen = "frmPARTipoSolicit"
    
    Screen.MousePointer = vbHourglass
    MDI.MostrarFormulario g_ofrmATRIB
    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' A�ade campos predefinidos del sistema
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAnyadirCampoGS_Click()
    Dim oArbolPres As cPresConceptos5Nivel0
    Dim ADORs As Ador.Recordset

    If Not basParametros.gParametrosGenerales.gbUsarPres1 Then
        MDI.mnuCamposGS.Item(11).Visible = False
    Else
        MDI.mnuCamposGS.Item(11).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres2 Then
        MDI.mnuCamposGS.Item(12).Visible = False
    Else
        MDI.mnuCamposGS.Item(12).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres3 Then
        MDI.mnuCamposGS.Item(13).Visible = False
    Else
        MDI.mnuCamposGS.Item(13).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres4 Then
        MDI.mnuCamposGS.Item(14).Visible = False
    Else
        MDI.mnuCamposGS.Item(14).Visible = True
    End If
    MDI.mnuCamposGS(7).Visible = True
    
    If gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.SolicitudPM) Or gParametrosIntegracion.gaExportar(EntidadIntegracion.SolicitudPM) Then
        MDI.mnuCamposGS.Item(16).Visible = True
    Else
        MDI.mnuCamposGS.Item(16).Visible = False
    End If
    
    If gParametrosGenerales.gbUsarOrgCompras Then
        MDI.mnuCamposGS.Item(19).Visible = True
        MDI.mnuCamposGS.Item(20).Visible = True
    Else
        MDI.mnuCamposGS.Item(19).Visible = False
        MDI.mnuCamposGS.Item(20).Visible = False
    End If
    
    MDI.mnuCamposGS(22).Visible = True 'Opcion de Menu Importe solicitudes vinculadas
    MDI.mnuCamposGS(23).Visible = True 'Opcion de Menu Referencia a solicitud
    MDI.mnuCamposGS(24).Visible = True 'Tipo de pedido
    MDI.mnuCamposGS(27).Visible = gParametrosGenerales.gbAccesoFSGA
    MDI.mnuCamposGS(32).Visible = True
    MDI.mnuCamposGS(33).Visible = True 'Unidad de pedido
    MDI.mnuCamposGS(35).Visible = True 'Comprador
    
    If Not g_oParametrosSM Is Nothing Then
        'Habr� un campo de sistema nuevo por cada �rbol presupuestario que hayamos definido en la secci�n presupuestaria del SM.
        Set oArbolPres = oFSGSRaiz.Generar_CPresConceptos5Nivel0
        Set ADORs = oArbolPres.DevolverArbolesPresupuestarios
              
        Dim cont As Integer
        cont = MDI.mnuCamposGS.Count
        While cont >= 38
            Unload MDI.mnuCamposGS(cont)
            cont = cont - 1
        Wend
        
        If Not ADORs Is Nothing Then
            While Not ADORs.EOF
                Load MDI.mnuCamposGS(MDI.mnuCamposGS.Count + 1)
                MDI.mnuCamposGS(MDI.mnuCamposGS.Count).caption = NullToStr(ADORs("DEN").Value)
                MDI.mnuCamposGS(MDI.mnuCamposGS.Count).Tag = TipoCampoGS.PartidaPresupuestaria & "#" & ADORs("COD").Value
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing
        End If
    End If
    
    MDI.PopupMenu MDI.mnuPOPUPCamposGS
End Sub

Private Sub cmdAnyaTipo_Click()
    g_Accion = ACCTipoSolicitudAnyadir
    
    Set frmPARTipoSolicitAnya.g_oIdiomas = m_oIdiomas
    frmPARTipoSolicitAnya.Show vbModal
End Sub

Private Sub cmdCrearCampo_Click()
    'Crea un campo predefinido nuevo:
    
    g_Accion = ACCTipoSolicitudItemAnyadir
    
    frmFormAnyaCampos.g_sOrigen = "frmPARTipoSolicit"
    Set frmFormAnyaCampos.g_oIdiomas = m_oIdiomas
    frmFormAnyaCampos.Show vbModal
    
End Sub

Private Sub cmdEliminarCampo_Click()
Dim udtTeserror As TipoErrorSummit
Dim irespuesta As Integer
Dim vbm As Variant
Dim bPaisMat As Boolean
Dim bArtDen As Boolean
Dim i As Integer
Dim aIdentificadores As Variant
Dim aBookmarks As Variant

'Elimina el campo predefinido
    
On Error GoTo Cancelar:
    
    If sdbgCampos.Rows = 0 Then Exit Sub
    If sdbgCampos.SelBookmarks.Count = 0 Then sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
    
    g_Accion = ACCTipoSolicitudItemEliminar

    vbm = sdbgCampos.GetBookmark(1)
    If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.Pais And g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.provincia Then
        bArtDen = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.Pais)
    ElseIf g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.material And g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.CodArticulo Then
        bArtDen = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.material)
    ElseIf g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.material And g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.NuevoCodArticulo Then
        bPaisMat = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.material)
    ElseIf g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.NuevoCodArticulo And g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.DenArticulo Then
        bArtDen = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.CodArticulo)
    ElseIf g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.DenArticulo Then
        bArtDen = True
        vbm = sdbgCampos.GetBookmark(-1)
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.DenArticulo)
        
    ElseIf g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.DesgloseActividad Then
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
        If irespuesta = vbYes Then m_ConCampoDesgloseActividad = False
        
    Else
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
    End If
    
    If irespuesta = vbNo Then
        g_Accion = ACCTipoSolicitudCons
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
        
    If bPaisMat = True Then
        ReDim aIdentificadores(sdbgCampos.SelBookmarks.Count + 2)
        ReDim aBookmarks(sdbgCampos.SelBookmarks.Count + 2)
    ElseIf bArtDen Then
        ReDim aIdentificadores(sdbgCampos.SelBookmarks.Count + 1)
        ReDim aBookmarks(sdbgCampos.SelBookmarks.Count + 1)
    Else
        ReDim aIdentificadores(sdbgCampos.SelBookmarks.Count)
        ReDim aBookmarks(sdbgCampos.SelBookmarks.Count)
    End If
        
    i = 0
    While i < sdbgCampos.SelBookmarks.Count
        sdbgCampos.Bookmark = sdbgCampos.SelBookmarks(i)
        aIdentificadores(i + 1) = sdbgCampos.Columns("ID").Value
        aBookmarks(i + 1) = sdbgCampos.SelBookmarks(i)
        i = i + 1
    Wend
    
    If bPaisMat = True Or bArtDen Then
        aIdentificadores(i + 1) = sdbgCampos.Columns("ID").CellValue(vbm)
        aBookmarks(i + 1) = vbm
        If bPaisMat Then
            'Almacenamiento del codigo de la denominacion
            aIdentificadores(i + 2) = sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(2))
            aBookmarks(i + 2) = sdbgCampos.GetBookmark(2)
        End If
    End If
    
    udtTeserror = g_oTipoSeleccionado.Campos.EliminarCamposPredefDeBaseDatos(aIdentificadores)
    
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError udtTeserror
    Else
        'Elimina los campos de la colecci�n:
        For i = 1 To UBound(aIdentificadores)
             g_oTipoSeleccionado.Campos.Remove (CStr(aIdentificadores(i)))
             basSeguridad.RegistrarAccion AccionesSummit.ACCTipoSolicitudItemEliminar, aIdentificadores(i)
        Next i
        
        'Elimina los campos de la grid:
        For i = 1 To UBound(aBookmarks)
            sdbgCampos.RemoveItem (sdbgCampos.AddItemRowIndex(aBookmarks(i)))
        Next i
        
        'Se posiciona en la fila correspondiente:
        If sdbgCampos.Rows > 0 Then
            If IsEmpty(sdbgCampos.RowBookmark(sdbgCampos.Row)) Then
                sdbgCampos.Bookmark = sdbgCampos.RowBookmark(sdbgCampos.Row - 1)
            Else
                sdbgCampos.Bookmark = sdbgCampos.RowBookmark(sdbgCampos.Row)
            End If
        End If
        
        sdbgCampos.SelBookmarks.RemoveAll
        If Me.Visible Then sdbgCampos.SetFocus

    End If

    g_Accion = ACCTipoSolicitudCons

    Screen.MousePointer = vbNormal
    
    Exit Sub
    
Cancelar:
    Screen.MousePointer = vbNormal
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub cmdElimTipo_Click()
Dim udtTeserror As TipoErrorSummit
Dim irespuesta As Integer

On Error GoTo Cancelar:
    
    g_Accion = ACCTipoSolicitudEliminar
    
    irespuesta = oMensajes.PreguntaEliminar(lblTipo.caption & " " & g_oTipoSeleccionado.Denominaciones.Item(CStr(gParametrosGenerales.gIdioma)).Den)
    If irespuesta = vbNo Then
        g_Accion = ACCTipoSolicitudCons
        Exit Sub
    End If
    
    'Elimina el TIPO de BD:
    Set m_oIBaseDatos = g_oTipoSeleccionado
    
    udtTeserror = m_oIBaseDatos.EliminarDeBaseDatos
    If udtTeserror.NumError <> TESnoerror Then
        basErrores.TratarError udtTeserror
    Else
        basSeguridad.RegistrarAccion AccionesSummit.ACCTipoSolicitudEliminar, g_oTipoSeleccionado.Id & "-" & g_oTipoSeleccionado.Cod
        g_oTipos.Remove (CStr(g_oTipoSeleccionado.Id))
        sdbcTipoCod.Text = ""
    End If
    
    Set m_oIBaseDatos = Nothing
    
    g_Accion = ACCTipoSolicitudCons
    
    Exit Sub
    
Cancelar:
    
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub cmdModifTipo_Click()
    Dim udtTeserror As TipoErrorSummit
    
    Screen.MousePointer = vbHourglass
    
    udtTeserror.NumError = TESnoerror
    g_Accion = ACCTipoSolicitudModificar
    
    Set m_oIBaseDatos = g_oTipoSeleccionado
    udtTeserror = m_oIBaseDatos.IniciarEdicion
    
    If udtTeserror.NumError = TESnoerror Then
        Screen.MousePointer = vbNormal
        Set frmPARTipoSolicitAnya.g_oIdiomas = m_oIdiomas
        frmPARTipoSolicitAnya.Show vbModal
    Else
        TratarError udtTeserror
    End If

    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Carga la pagina
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub Form_Load()
    Dim oIdioma As CIdioma
    Dim i As Integer
    Dim iPosition As Integer
    
    Me.Height = 7005
    Me.Width = 10170
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, False)
    i = sdbgCampos.Columns.Count
    iPosition = 1
    
    With sdbgCampos
        .Columns.Add i
        .Columns(i).Name = gParametrosInstalacion.gIdioma
        .Columns(i).caption = m_sDato
        .Columns(i).Style = ssStyleEditButton
        .Columns(i).ButtonsAlways = True
        .Columns(i).Position = iPosition
        .Columns(i).Locked = False
        .Columns(i).FieldLen = 300
        
        For Each oIdioma In m_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                i = i + 1
                iPosition = iPosition + 1
                .Columns.Add i
                .Columns(i).Name = oIdioma.Cod
                .Columns(i).caption = oIdioma.Den
                .Columns(i).Style = ssStyleEditButton
                .Columns(i).ButtonsAlways = True
                .Columns(i).Position = iPosition
                .Columns(i).Visible = True
                .Columns(i).Locked = False
                .Columns(i).FieldLen = 300
            End If
        Next
    End With
    
    Set g_oTipos = Nothing
    Set g_oTipos = oFSGSRaiz.Generar_CTiposSolicit

    g_Accion = ACCTipoSolicitudCons
End Sub
''' <summary>
''' Carga las cadenas de idiomas
''' </summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        For i = 1 To 10
            m_sCamposGS(i) = Ador(0).Value
            Ador.MoveNext
        Next i
        
        Me.caption = Ador(0).Value   '11 Tipos de solicitudes
        Ador.MoveNext
        lblTipo.caption = Ador(0).Value '12 Tipo:
        Ador.MoveNext
        cmdAnyaTipo.ToolTipText = Ador(0).Value         '13 A�adir tipo
        Ador.MoveNext
        cmdElimTipo.ToolTipText = Ador(0).Value  '14 Eliminar tipo
        
        
        Ador.MoveNext
        cmdModifTipo.ToolTipText = Ador(0).Value  '15 Modificar tipo
        Ador.MoveNext
        m_sDato = Ador(0).Value '16 Dato
        Ador.MoveNext
        sdbgCampos.Columns("AYUDA").caption = Ador(0).Value '17 Ayuda
        Ador.MoveNext
        sdbgCampos.caption = Ador(0).Value '18 Campos
        Ador.MoveNext
        cmdCrearCampo.caption = Ador(0).Value  ' 19 Crear campo nuevo
        Ador.MoveNext
        cmdAnyaAtrib.caption = Ador(0).Value  '20 A�adir atributo de GS
        Ador.MoveNext
        cmdAnyadirCampoGS.caption = Ador(0).Value '21 A�adir campo del sistema
        Ador.MoveNext
        cmdEliminarCampo.caption = Ador(0).Value  '22 Eliminar campo
        Ador.MoveNext
        sdbcTipoCod.Columns("COD").caption = Ador(0).Value     '23 C�digo
        sdbcTipoDen.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoCod.Columns("DEN").caption = Ador(0).Value    '24 Denominaci�n
        sdbcTipoDen.Columns("DEN").caption = Ador(0).Value

        For i = 1 To 9
            Ador.MoveNext
            m_sCamposSC(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sCamposGS(15) = Ador(0).Value '35 persona
        Ador.MoveNext
        'm_sCamposGS(15) = Ador(0).Value '36 Denominaci�n de Articulo
        Ador.MoveNext
        m_sCamposGS(16) = Ador(0).Value '37 Num solicitud ERP
        
        Ador.MoveNext
        m_sCamposGS(17) = Ador(0).Value  '38 Unidad organizativa
        Ador.MoveNext
        m_sCamposGS(18) = Ador(0).Value  '39 Departamento
        Ador.MoveNext
        m_sCamposGS(19) = Ador(0).Value  '40 Organizaci�n de compras
        Ador.MoveNext
        m_sCamposGS(20) = Ador(0).Value  '41 Centro
        Ador.MoveNext
        m_sCamposGS(21) = Ador(0).Value  '42 Almac�n
        
        Ador.MoveNext
        m_sCamposGS(22) = Ador(0).Value  '43 Importe solicitudes vinculadas
        Ador.MoveNext
        m_sCamposGS(23) = Ador(0).Value  '44 Referencia a solicitud
        
        
        For i = 1 To 3
            Ador.MoveNext
        Next
        
        Ador.MoveNext
        m_sCamposGS(25) = Ador(0).Value  '48 Centro de coste
        Ador.MoveNext
        m_sCamposGS(26) = Ador(0).Value  '49 Activo
        Ador.MoveNext
        m_sCamposGS(24) = Ador(0).Value  '50 Tipo de pedido
        Ador.MoveNext
        m_sCamposGS(27) = Ador(0).Value  '51 Desglose de actividad
        Ador.MoveNext
        m_sCamposGS(28) = Ador(0).Value  '53 Desglose de factura
        Ador.MoveNext
        m_sCamposGS(29) = Ador(0).Value  '53 Factura
        Ador.MoveNext
        m_sCamposGS(30) = Ador(0).Value  '54 Inicio abono
        Ador.MoveNext
        m_sCamposGS(31) = Ador(0).Value  '55 Fin abono
        Ador.MoveNext
        m_sCamposGS(32) = Ador(0).Value  '56 Retencion en garantia
        Ador.MoveNext
        m_sCamposGS(33) = Ador(0).Value  '57 Unidad de pedido
        Ador.MoveNext
        m_sCamposGS(34) = Ador(0).Value  '58 Proveedor ERP
        Ador.MoveNext
        m_sCamposGS(35) = Ador(0).Value  '59 Comprador
        Ador.MoveNext
        m_sCamposGS(36) = Ador(0).Value  '60 Desglose de pedido
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sCamposGS(37) = Ador(0).Value  '64 Empresa
        Ador.Close
    End If
    
    Set Ador = Nothing
    
    m_sCamposGS(11) = gParametrosGenerales.gsSingPres1
    m_sCamposGS(12) = gParametrosGenerales.gsSingPres2
    m_sCamposGS(13) = gParametrosGenerales.gsSingPres3
    m_sCamposGS(14) = gParametrosGenerales.gsSingPres4
End Sub


Private Sub Form_Resize()
Dim dblWGrid As Double
Dim oIdioma As CIdioma

    'Redimensiona el formulario
    
    If Me.Height < 2000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    sdbgCampos.Height = Me.Height - 1685
    sdbgCampos.Width = Me.Width - 315
    
    sdbgCampos.Columns("AYUDA").Width = sdbgCampos.Width / 12
    dblWGrid = sdbgCampos.Width - sdbgCampos.Columns("AYUDA").Width - 600
    
    If Not m_oIdiomas Is Nothing Then
        If sdbgCampos.Columns.Count > 2 Then
            For Each oIdioma In m_oIdiomas
                sdbgCampos.Columns(oIdioma.Cod).Width = dblWGrid / m_oIdiomas.Count
            Next
        End If
    End If
    
    picEdit.Top = sdbgCampos.Top + sdbgCampos.Height + 80
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.sdbgCampos.DataChanged = True Then Me.sdbgCampos.Update
    
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If
    
    If Not g_ofrmDesglose Is Nothing Then
        Unload g_ofrmDesglose
        Set g_ofrmDesglose = Nothing
    End If
    
    Set g_oTipos = Nothing
    Set g_oTipoSeleccionado = Nothing
    Set m_oIBaseDatos = Nothing
    Set m_oIdiomas = Nothing
    Set m_oCampoEnEdicion = Nothing
    
    Me.Visible = False
End Sub

Public Sub AnyaModifTipo(Optional ByVal Id As Long)
    If Id <> 0 Then
        'Se ha a�adido un TIPO nuevo
        m_bRespetarCombo = True
        sdbcTipoCod.Columns("ID").Value = Id
        sdbcTipoCod.Columns("COD").Value = g_oTipos.Item(CStr(Id)).Cod
        sdbcTipoCod.Columns("DEN").Value = g_oTipos.Item(CStr(Id)).Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
        sdbcTipoCod.Text = g_oTipos.Item(CStr(Id)).Cod
        sdbcTipoDen.Text = g_oTipos.Item(CStr(Id)).Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
        m_bRespetarCombo = False
        
        Set g_oTipoSeleccionado = g_oTipos.Item(CStr(Id))
        
        TipoSeleccionado
        
    Else
        'Se ha modificado el tipo
        m_bRespetarCombo = True
        sdbcTipoCod.Text = g_oTipoSeleccionado.Cod
        sdbcTipoDen.Text = g_oTipoSeleccionado.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
        m_bRespetarCombo = False
    End If
End Sub

''' <summary>
''' 'Carga los campos correspondientes al tipo seleccionado:
''' </summary>
''' <remarks>Llamada desde:AnyaModifTipo sdbcTipoCod_CloseUp    sdbcTipoCod_Validate    sdbcTipoDen_CloseUp; Tiempo m�ximo:0,2seg</remarks>
Private Sub TipoSeleccionado()
    If g_oTipoSeleccionado Is Nothing Then Exit Sub
    
    
    g_oTipoSeleccionado.CargarCamposTipo False, False
    m_ConCampoDesgloseActividad = False
    sdbgCampos.RemoveAll
    AnyadirCampos g_oTipoSeleccionado.Campos, True
    sdbgCampos.MoveFirst
    
    'Si se ha seleccionado el tipo "Solicitudes de compras","No conformidades" o "Certificados" no se podr� ni modificar ni eliminar
    Select Case g_oTipoSeleccionado.Tipo
        Case TipoSolicitud.AUTOFACTURA, TipoSolicitud.SolicitudDePedidoCatalogo, TipoSolicitud.SolicitudDePedidoContraPedidoAbierto
            cmdAnyaAtrib.Enabled = False
            cmdAnyadirCampoGS.Enabled = False
            cmdCrearCampo.Enabled = False
            cmdElimTipo.Enabled = False
            cmdModifTipo.Enabled = False
        Case TipoSolicitud.SolicitudCompras, TipoSolicitud.Certificados, TipoSolicitud.NoConformidades
            cmdAnyaAtrib.Enabled = True
            cmdAnyadirCampoGS.Enabled = True
            cmdCrearCampo.Enabled = True
            cmdElimTipo.Enabled = False
            cmdModifTipo.Enabled = False
        Case Else
            cmdAnyaAtrib.Enabled = True
            cmdAnyadirCampoGS.Enabled = True
            cmdCrearCampo.Enabled = True
            cmdElimTipo.Enabled = True
            cmdModifTipo.Enabled = True
    End Select
End Sub

Private Sub sdbcTipoCod_Change()
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcTipoDen.Text = ""
        m_bRespetarCombo = False
        Set g_oTipoSeleccionado = Nothing
        
        If sdbcTipoCod <> "" Then
            m_bCargarComboDesde = True
        End If
    
        sdbgCampos.RemoveAll
        cmdElimTipo.Enabled = False
        cmdModifTipo.Enabled = False
        cmdAnyaAtrib.Enabled = False
        cmdAnyadirCampoGS.Enabled = False
        cmdCrearCampo.Enabled = False
        cmdEliminarCampo.Enabled = False
                
    End If
    
End Sub

Private Sub sdbcTipoCod_Click()
    If Not sdbcTipoCod.DroppedDown Then
        sdbcTipoCod = ""
        sdbcTipoDen = ""
    End If
End Sub

Private Sub sdbcTipoCod_CloseUp()

    If sdbcTipoCod.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcTipoDen.Text = sdbcTipoCod.Columns("DEN").Text
    sdbcTipoCod.Text = sdbcTipoCod.Columns("COD").Text
    m_bRespetarCombo = False
    
    Set g_oTipoSeleccionado = g_oTipos.Item(sdbcTipoCod.Columns("ID").Value)
    TipoSeleccionado
    
    m_bCargarComboDesde = False
    
End Sub

Private Sub sdbcTipoCod_DropDown()
    Dim oTipo As CTipoSolicit
    Dim bOcultarSolicitudesContrato  As Boolean
    Dim bOcultarSolicitudesFactura As Boolean
    Dim bPermisoContratosUsuario As Boolean
    Dim bPermisoFacturasUsuario As Boolean
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoCod.RemoveAll
    
    Set g_oTipos = Nothing
    Set g_oTipos = oFSGSRaiz.Generar_CTiposSolicit
    
    If m_bCargarComboDesde Then
        g_oTipos.CargarTodosTiposSolicitud Trim(sdbcTipoCod.Text)
    Else
        g_oTipos.CargarTodosTiposSolicitud
    End If
    
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador) Then
        bPermisoContratosUsuario = True
        bPermisoFacturasUsuario = True
    Else
        bPermisoContratosUsuario = oUsuarioSummit.AccesoFSContratos
        bPermisoFacturasUsuario = oUsuarioSummit.AccesoFSIM
    End If
    bOcultarSolicitudesContrato = (gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.SinAcceso) Or Not bPermisoContratosUsuario
    bOcultarSolicitudesFactura = (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso) Or Not bPermisoFacturasUsuario
    
    For Each oTipo In g_oTipos
        If Not ((bOcultarSolicitudesContrato And oTipo.Tipo = TipoSolicitud.Contrato) _
            Or (bOcultarSolicitudesFactura And (oTipo.Tipo = TipoSolicitud.Factura Or oTipo.Tipo = TipoSolicitud.AUTOFACTURA))) Then
            sdbcTipoCod.AddItem oTipo.Id & Chr(m_lSeparador) & oTipo.Cod & Chr(m_lSeparador) & oTipo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
        End If
    Next
    
    sdbcTipoCod.SelStart = 0
    sdbcTipoCod.SelLength = Len(sdbcTipoCod.Text)
    sdbcTipoCod.Refresh

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcTipoCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoCod.Rows - 1
            bm = sdbcTipoCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcTipoCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcTipoCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcTipoCod_Validate(Cancel As Boolean)
Dim oTipos As CTiposSolicit
Dim bExiste As Boolean
    
    Set oTipos = oFSGSRaiz.Generar_CTiposSolicit
    
    If sdbcTipoCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
        
    oTipos.CargarTodosTiposSolicitud sdbcTipoCod.Text, , , True

    Screen.MousePointer = vbNormal
    
    bExiste = Not (oTipos.Count = 0)

    If Not bExiste Then
        sdbcTipoCod.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcTipoDen.Text = oTipos.Item(1).Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
        sdbcTipoCod.Columns("ID").Value = oTipos.Item(1).Id
        sdbcTipoCod.Columns("COD").Value = sdbcTipoCod.Text
        sdbcTipoCod.Columns("DEN").Value = sdbcTipoDen.Text
        m_bRespetarCombo = False
        Set g_oTipoSeleccionado = oTipos.Item(1)
        TipoSeleccionado
        m_bCargarComboDesde = False
    End If
    
    Set oTipos = Nothing

End Sub


Private Sub sdbcTipoDen_Change()
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcTipoCod.Text = ""
        m_bRespetarCombo = False
        Set g_oTipoSeleccionado = Nothing
        m_bCargarComboDesde = True
        
        If sdbcTipoDen <> "" Then
            m_bCargarComboDesde = True
        End If
        
        sdbgCampos.RemoveAll
        cmdElimTipo.Enabled = False
        cmdModifTipo.Enabled = False
        cmdAnyaAtrib.Enabled = False
        cmdAnyadirCampoGS.Enabled = False
        cmdCrearCampo.Enabled = False
        cmdEliminarCampo.Enabled = False
         
    End If
End Sub

Private Sub sdbcTipoDen_CloseUp()
    
    If sdbcTipoDen.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcTipoCod.Text = sdbcTipoDen.Columns("COD").Text
    sdbcTipoDen.Text = sdbcTipoDen.Columns("DEN").Text
    m_bRespetarCombo = False
    
    Set g_oTipoSeleccionado = g_oTipos.Item(sdbcTipoDen.Columns("ID").Value)
    TipoSeleccionado
    
    m_bCargarComboDesde = False
    
End Sub


Private Sub sdbcTipoDen_DropDown()
    Dim oTipo As CTipoSolicit
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoDen.RemoveAll
    
    Set g_oTipos = Nothing
    Set g_oTipos = oFSGSRaiz.Generar_CTiposSolicit
    
    If m_bCargarComboDesde Then
        g_oTipos.CargarTodosTiposSolicitud , Trim(sdbcTipoDen.Text)
    Else
        g_oTipos.CargarTodosTiposSolicitud
    End If
    
    For Each oTipo In g_oTipos
        sdbcTipoDen.AddItem oTipo.Id & Chr(m_lSeparador) & oTipo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & Chr(m_lSeparador) & oTipo.Cod
    Next
    
    sdbcTipoDen.SelStart = 0
    sdbcTipoDen.SelLength = Len(sdbcTipoDen.Text)
    sdbcTipoDen.Refresh

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcTipoDen_InitColumnProps()
    sdbcTipoDen.DataFieldList = "Column 1"
    sdbcTipoDen.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbcTipoDen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoDen.Rows - 1
            bm = sdbcTipoDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcTipoDen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcTipoDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Public Sub AnyadirCampos(ByVal oCampos As CCamposPredef, Optional ByVal bCargarCol As Boolean = False)
    Dim oCampo As CCampoPredef
    Dim sCadena As String
    Dim oIdioma As CIdioma
    
    If oCampos Is Nothing Then Exit Sub
    
    For Each oCampo In oCampos
      If Not oCampo.EsSubCampo = True Then
        If Not bCargarCol Then
            'Lo a�ade a la colecci�n:
            g_oTipoSeleccionado.Campos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        End If
        
        sCadena = oCampo.Id & Chr(m_lSeparador) & " "
        
        'Denominaci�n del campo en los diferentes idiomas:
        sCadena = sCadena & Chr(m_lSeparador) & oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
        
        For Each oIdioma In m_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                sCadena = sCadena & Chr(m_lSeparador) & oCampo.Denominaciones.Item(CStr(oIdioma.Cod)).Den
            End If
        Next
        
        If oCampo.TipoCampoGS = TipoCampoGS.DesgloseActividad Then
            m_ConCampoDesgloseActividad = True
        End If
        'a�ade la cadena a la grid:
        sdbgCampos.AddItem sCadena
     End If
    Next

End Sub


Private Sub sdbgCampos_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma

    If m_oCampoEnEdicion Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    '******** REALIZA LAS COMPROBACIONES CORRESPONDIENTES ANTES DE GUARDAR ***************
    'La denominaci�n no puede ser nula (por lo menos la del idioma de la aplicaci�n)
    If Trim(sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value) = "" Then
        Screen.MousePointer = vbNormal
        sdbgCampos.CancelUpdate
        sdbgCampos.DataChanged = False
        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).caption
        Cancel = True
        Exit Sub
    End If
    
    '************************ GUARDA EN BASE DE DATOS ***************************
    'Rellenamos la colecci�n con los nuevos valores dependiendo del tipo:
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In m_oIdiomas
        oDenominaciones.Add oIdioma.Cod, Trim(sdbgCampos.Columns(CStr(oIdioma.Cod)).Value)
    Next
    Set m_oCampoEnEdicion.Denominaciones = oDenominaciones
    
    Set oIBaseDatos = m_oCampoEnEdicion
    teserror = oIBaseDatos.FinalizarEdicionModificando
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        sdbgCampos.CancelUpdate
        If Me.Visible Then sdbgCampos.SetFocus
        sdbgCampos.DataChanged = False
    Else
        ''' Registro de acciones
        basSeguridad.RegistrarAccion AccionesSummit.ACCTipoSolicitudItemModif, "Id" & sdbgCampos.Columns("ID").Value
        Set oIBaseDatos = Nothing
        Set m_oCampoEnEdicion = Nothing
    End If
    
    g_Accion = ACCTipoSolicitudCons
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgCampos_BtnClick()
    If sdbgCampos.Columns(sdbgCampos.col).Name = "AYUDA" Then
        'Muestra el formulario con la ayuda:
        MostrarFormFormCampoAyuda oGestorIdiomas, gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, oGestorSeguridad, oUsuarioSummit.Cod, gParametrosGenerales, _
                FormCampoAyudaTipoAyuda.CampoPredefinido, True, , m_oIdiomas, g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
        
    Else
        'Detalle de campo:
        If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo = TiposDeAtributos.TipoDesglose And g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS <> TipoCampoGS.DesgloseActividad Then
            'Es un campo de desglose.Se muestran los subcampos:
            If Not g_ofrmDesglose Is Nothing Then
                Unload g_ofrmDesglose
                Set g_ofrmDesglose = Nothing
            End If
            
            Set g_ofrmDesglose = New frmPARTipoSolicitDesglose
            Set g_ofrmDesglose.g_oCampoDesglose = g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
            
            Screen.MousePointer = vbHourglass
            MDI.MostrarFormulario g_ofrmDesglose
            Screen.MousePointer = vbNormal
                    
        ElseIf g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.CampoGS Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.Certificado Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.NoConformidad Then
            'Es un campo predefinido de GS:
            frmDetalleCampoPredef.g_sOrigen = "frmPARTipoSolicit"
            frmDetalleCampoPredef.g_iTipo = g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo
            frmDetalleCampoPredef.g_sDenCampo = g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            frmDetalleCampoPredef.g_iTipoCampo = g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS
            frmDetalleCampoPredef.g_iTipoSolicit = g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef
            Set frmDetalleCampoPredef.g_oCampoPredef = g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
            frmDetalleCampoPredef.Show vbModal
            
        Else
            'Es un campo normal o un atributo de GS:
            frmDetalleCampos.g_sOrigen = "frmCamposSolic"
            frmDetalleCampos.g_bModif = False
            Set frmDetalleCampos.g_oIdiomas = m_oIdiomas
            Set frmDetalleCampos.g_oCampoPredef = g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
            frmDetalleCampos.Show vbModal
        End If
    End If
End Sub

Public Sub AnyadirCampoDeGS(ByVal iTipoCampo As Integer, Optional ByVal sPRES5 As String)
Dim oCampo As CCampoPredef

Dim teserror As TipoErrorSummit
Dim oCampos As CCamposPredef
Dim bVariosArt As Boolean

    bVariosArt = True
    
    Set oCampos = oFSGSRaiz.Generar_CCamposPredef
    
    If iTipoCampo = TipoCampoGS.provincia Then  'Si insertamos la provincia inserta tambi�n el pa�s:
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Pais)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
    ElseIf iTipoCampo = TipoCampoGS.CodArticulo Then 'Si insertamos el art�culo inserta tambi�n el material:
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.material)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
    
    ElseIf iTipoCampo = TipoCampoGS.NuevoCodArticulo Then 'Si insertamos el Nuevo art�culo inserta tambi�n el material y la denominacion:
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.material)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.DenArticulo)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
        
    ElseIf iTipoCampo = TipoCampoGS.Desglose Then  'Si se selecciona un campo de desglose se insertan todos los campos por defecto:
        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.material, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.NuevoCodArticulo, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.DenArticulo, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoSC.PrecioUnitario, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoSC.Cantidad, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Unidad, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoSC.IniSuministro, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoSC.FinSuministro, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Dest, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.FormaPago, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Proveedor, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
        If gParametrosGenerales.gbUsarPres1 Then
            Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Pres1, True)
            oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        End If
        If gParametrosGenerales.gbUsarPres2 Then
            Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Pres2, True)
            oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        End If
        If gParametrosGenerales.gbUsarPres3 Then
            Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Pres3, True)
            oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        End If
        If gParametrosGenerales.gbUsarPres4 Then
            Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Pres4, True)
            oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        End If
        
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoSC.ArchivoEspecific, True)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
    ElseIf iTipoCampo = TipoCampoGS.PartidaPresupuestaria Then
       
        Set oCampo = GenerarEstructuraArbolPresupuestario(iTipoCampo, sPRES5, False)
        oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, , , oCampo.PRES5
        
    
    ElseIf iTipoCampo = TipoCampoGS.DesgloseActividad Then
        If Not m_ConCampoDesgloseActividad Then
            Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
            oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        
            m_ConCampoDesgloseActividad = True
        Else
            oMensajes.MensajeOKOnly 1209
            Exit Sub
        End If
    ElseIf iTipoCampo = TipoCampoGS.CodComprador Then
        'Solo se a�ade si la solicitud es de compra
        If g_oTipoSeleccionado.Tipo = 1 Then
            'Aqu� no hago la comprobaci�n de que no exista ya un campo de tipo comprador en el formulario
            Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
            oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
        Else
            oMensajes.NoSolicitudCompra
            Exit Sub
        End If
    Else
        bVariosArt = False
        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
    End If

    'Inserta en BD:
    If bVariosArt = True Then
        teserror = oCampos.AnyadirCamposPredefABaseDatos
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
        Else
            'a�ade el nuevo campo a la colecci�n y a la grid:
            AnyadirCampos oCampos
            For Each oCampo In oCampos
                RegistrarAccion ACCTipoSolicitudItemAnyadir, "Id:" & oCampo.Id
            Next
        End If

    Else
        'Inserta el campo en BD:
        Set m_oIBaseDatos = oCampo
    
        teserror = m_oIBaseDatos.AnyadirABaseDatos
    
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
        Else
            'a�ade el nuevo campo a la colecci�n y a la grid:
            oCampos.Add oCampo.Id, g_oTipoSeleccionado.Id, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT
            AnyadirCampos oCampos
            RegistrarAccion ACCTipoSolicitudItemAnyadir, "Id:" & oCampo.Id
        End If
        
        Set m_oIBaseDatos = Nothing
    End If
    
    Set oCampos = Nothing
    Set oCampo = Nothing

    g_Accion = ACCTipoSolicitudCons
End Sub

''' <summary>
''' Generar Estructura de un Campo
''' </summary>
''' <param name="iTipoCampoGS">Id de campo</param>
''' <param name="bSubcampo">si va a nivel de cabecera o de desglose</param>
''' <returns>Objeto Campo</returns>
''' <remarks>Llamada desde: AnyadirCampoGS ; Tiempo m�ximo: 0,2</remarks>
Private Function GenerarEstructuraNuevoCampo(ByVal iTipoCampo, Optional ByVal bSubcampo As Boolean = False) As CCampoPredef
    Dim oCampo As CCampoPredef
    Dim oIdioma As CIdioma
    Dim Ador As Ador.Recordset
    Dim oParametros As CLiterales

    Set oCampo = oFSGSRaiz.Generar_CCampoPredef
    
    oCampo.Id = iTipoCampo
    
    If bSubcampo = True Then
        oCampo.EsSubCampo = True
    Else
        oCampo.EsSubCampo = False
    End If
    oCampo.idAtrib = Null
    oCampo.MaxFec = Null
    oCampo.MinFec = Null
    oCampo.MaxNum = Null
    oCampo.MinNum = Null

    oCampo.TipoIntroduccion = TAtributoIntroduccion.IntroLibre
    oCampo.TipoPredef = TipoCampoPredefinido.CampoGS
    oCampo.TipoSolicitud = g_oTipoSeleccionado.Id

    oCampo.TipoCampoGS = iTipoCampo
        
    Set oCampo.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
        
    For Each oIdioma In m_oIdiomas
        If oIdioma.Cod = gParametrosInstalacion.gIdioma Then
            Select Case iTipoCampo
                Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(5)
                Case TipoCampoGS.Desglose
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(7)
                Case TipoCampoGS.Dest
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(10)
                Case TipoCampoGS.FormaPago
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(2)
                Case TipoCampoGS.material
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(4)
                Case TipoCampoGS.Moneda
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(3)
                Case TipoCampoGS.Pais
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(8)
                Case TipoCampoGS.Proveedor
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(1)
                Case TipoCampoGS.provincia
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(9)
                Case TipoCampoGS.Unidad
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(6)
                Case TipoCampoGS.Pres1
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(11)
                Case TipoCampoGS.Pres2
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(12)
                Case TipoCampoGS.Pres3
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(13)
                Case TipoCampoGS.Pres4
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(14)
                Case TipoCampoSC.DescrBreve
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(1)
                Case TipoCampoSC.DescrDetallada
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(2)
                Case TipoCampoSC.importe
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(3)
                Case TipoCampoSC.Cantidad
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(4)
                Case TipoCampoSC.FecNecesidad
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(5)
                Case TipoCampoSC.IniSuministro
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(6)
                Case TipoCampoSC.FinSuministro
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(7)
                Case TipoCampoSC.ArchivoEspecific
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(8)
                Case TipoCampoSC.PrecioUnitario
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposSC(9)
                Case TipoCampoGS.DenArticulo
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(15)
                Case TipoCampoGS.CampoPersona
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(16)
                Case TipoCampoGS.NumSolicitERP
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(17)
                Case TipoCampoGS.UnidadOrganizativa
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(18)
                Case TipoCampoGS.Departamento
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(19)
                Case TipoCampoGS.OrganizacionCompras
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(20)
                Case TipoCampoGS.Centro
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(21)
                Case TipoCampoGS.Almacen
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(22)
                Case TipoCampoGS.CentroCoste
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(23)
                Case TipoCampoGS.Activo
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(24)
                Case TipoCampoGS.TipoPedido
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(25)
                Case TipoCampoGS.ImporteSolicitudesVinculadas
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(26)
                Case TipoCampoGS.RefSolicitud
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(27)
                Case TipoCampoGS.DesgloseActividad
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(28)
                Case TipoCampoGS.Factura
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(29)
                Case TipoCampoGS.InicioAbono
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(30)
                Case TipoCampoGS.FinAbono
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(31)
                Case TipoCampoGS.RetencionEnGarantia
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(32)
                Case TipoCampoGS.UnidadPedido
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(33)
                Case TipoCampoGS.ProveedorERP
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(34)
                Case TipoCampoGS.CodComprador
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(35)
                Case TipoCampoGS.DesgloseDePedido
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(36)
                Case TipoCampoGS.Empresa
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(37)
            End Select
            
        Else
            Select Case iTipoCampo
                Case TipoCampoGS.Pres1
                    Set oParametros = oGestorParametros.DevolverLiterales(20, 20, oIdioma.Cod)
                    oCampo.Denominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.Pres2
                    Set oParametros = oGestorParametros.DevolverLiterales(21, 21, oIdioma.Cod)
                    oCampo.Denominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.Pres3
                    Set oParametros = oGestorParametros.DevolverLiterales(27, 27, oIdioma.Cod)
                    oCampo.Denominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.Pres4
                    Set oParametros = oGestorParametros.DevolverLiterales(28, 28, oIdioma.Cod)
                    oCampo.Denominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case Else
                    Select Case iTipoCampo
                        Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 5)
                        Case TipoCampoGS.Desglose
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 7)
                        Case TipoCampoGS.Dest
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 10)
                        Case TipoCampoGS.FormaPago
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 2)
                        Case TipoCampoGS.material
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 4)
                        Case TipoCampoGS.Moneda
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 3)
                        Case TipoCampoGS.Pais
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 8)
                        Case TipoCampoGS.Proveedor
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 1)
                        Case TipoCampoGS.provincia
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 9)
                        Case TipoCampoGS.Unidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 6)
                        Case TipoCampoSC.DescrBreve
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 26)
                        Case TipoCampoSC.DescrDetallada
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 27)
                        Case TipoCampoSC.importe
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 28)
                        Case TipoCampoSC.Cantidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 29)
                        Case TipoCampoSC.FecNecesidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 30)
                        Case TipoCampoSC.IniSuministro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 31)
                        Case TipoCampoSC.FinSuministro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 32)
                        Case TipoCampoSC.ArchivoEspecific
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 33)
                        Case TipoCampoSC.PrecioUnitario
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 34)
                        Case TipoCampoGS.DenArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 36)
                        Case TipoCampoGS.CampoPersona
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 35)
                        Case TipoCampoGS.NumSolicitERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 37)
                        Case TipoCampoGS.UnidadOrganizativa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 38)
                        Case TipoCampoGS.Departamento
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 39)
                        Case TipoCampoGS.OrganizacionCompras
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 40)
                        Case TipoCampoGS.Centro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 41)
                        Case TipoCampoGS.Almacen
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 42)
                        Case TipoCampoGS.ImporteSolicitudesVinculadas
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 43)
                        Case TipoCampoGS.RefSolicitud
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 44)
                        Case TipoCampoGS.CentroCoste
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 48)
                        Case TipoCampoGS.Activo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 49)
                        Case TipoCampoGS.TipoPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 50)
                        Case TipoCampoGS.DesgloseActividad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 51)
                        Case TipoCampoGS.DesgloseFactura
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 52)
                        Case TipoCampoGS.Factura
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 53)
                        Case TipoCampoGS.InicioAbono
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 54)
                        Case TipoCampoGS.FinAbono
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 55)
                        Case TipoCampoGS.RetencionEnGarantia
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 56)
                        Case TipoCampoGS.UnidadPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 57)
                        Case TipoCampoGS.ProveedorERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 58)
                        Case TipoCampoGS.CodComprador
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 59)
                        Case TipoCampoGS.DesgloseDePedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 60)
                        Case TipoCampoGS.Empresa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 64)
                    End Select
                    oCampo.Denominaciones.Add oIdioma.Cod, Ador(0).Value
                    Ador.Close
            End Select
            Set oParametros = Nothing
        End If
    Next
    Set Ador = Nothing
    
    Select Case iTipoCampo
        Case TipoCampoSC.importe, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario, TipoCampoGS.Empresa
            oCampo.Tipo = TiposDeAtributos.TipoNumerico
            
        Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro, TipoCampoGS.InicioAbono, TipoCampoGS.FinAbono
            oCampo.Tipo = TiposDeAtributos.TipoFecha
            
        Case TipoCampoSC.DescrDetallada
            oCampo.Tipo = TiposDeAtributos.TipoTextoLargo
            
        Case TipoCampoSC.ArchivoEspecific
            oCampo.Tipo = TiposDeAtributos.TipoArchivo
            
        Case TipoCampoGS.Desglose, TipoCampoGS.DesgloseActividad
            oCampo.Tipo = TiposDeAtributos.TipoDesglose
            
        Case TipoCampoGS.RetencionEnGarantia
            oCampo.Tipo = TiposDeAtributos.TipoNumerico
            oCampo.MinNum = 0
            oCampo.MaxNum = 100
            
        Case Else
            oCampo.Tipo = TiposDeAtributos.TipoTextoMedio
            
    End Select
    
    Set GenerarEstructuraNuevoCampo = oCampo
End Function

Private Function GenerarEstructuraArbolPresupuestario(ByVal iTipoCampo As Integer, ByVal sPRES5 As String, Optional ByVal bSubcampo As Boolean = False) As CCampoPredef
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim oCampo As CCampoPredef
    Dim oPres5Niv0 As cPresConceptos5Nivel0
    Dim Adores As ADODB.Recordset
           
    Set oCampo = oFSGSRaiz.Generar_CCampoPredef
    
    oCampo.Id = iTipoCampo
    
    If bSubcampo = True Then
        oCampo.EsSubCampo = True
    Else
        oCampo.EsSubCampo = False
    End If
    oCampo.idAtrib = Null
    oCampo.MaxFec = Null
    oCampo.MinFec = Null
    oCampo.MaxNum = Null
    oCampo.MinNum = Null

    oCampo.TipoIntroduccion = TAtributoIntroduccion.IntroLibre
    oCampo.TipoPredef = TipoCampoPredefinido.CampoGS
    oCampo.TipoSolicitud = g_oTipoSeleccionado.Id

    oCampo.TipoCampoGS = iTipoCampo
    oCampo.Tipo = TiposDeAtributos.TipoTextoMedio
    oCampo.PRES5 = sPRES5
    
    
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    Set oPres5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    For Each oIdioma In m_oIdiomas
        Set Adores = oPres5Niv0.DevolverDenominacion(oIdioma.Cod, sPRES5)
        oDenominaciones.Add oIdioma.Cod, Adores(0).Value
    Next
    
    Set oCampo.Denominaciones = oDenominaciones
    Set GenerarEstructuraArbolPresupuestario = oCampo
End Function


Private Sub sdbgCampos_Change()
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    If g_oTipoSeleccionado Is Nothing Then Exit Sub
    If g_oTipoSeleccionado.Campos Is Nothing Then Exit Sub
    
    DoEvents
    
    Set m_oCampoEnEdicion = Nothing

    If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)) Is Nothing Then Exit Sub
    
    Set m_oCampoEnEdicion = g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
        
    g_Accion = ACCTipoSolicitudItemModif
    
End Sub

Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then

        If sdbgCampos.DataChanged = False Then
            sdbgCampos.CancelUpdate
            sdbgCampos.DataChanged = False
            
            Set m_oCampoEnEdicion = Nothing
            
            g_Accion = ACCTipoSolicitudCons
        End If
    End If
End Sub

Private Sub sdbgCampos_LostFocus()
    If sdbgCampos.DataChanged = True Then sdbgCampos.Update
End Sub

Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Select Case g_oTipoSeleccionado.Tipo
        
        Case TipoSolicitud.SolicitudCompras  'Solicitudes de compras
            If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.DescrBreve Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.DescrDetallada Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.importe Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.Cantidad Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.FecNecesidad Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.IniSuministro Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.FinSuministro Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.ArchivoEspecific Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.PrecioUnitario Then
                cmdEliminarCampo.Enabled = False
            Else
                cmdEliminarCampo.Enabled = True
            End If
        
        Case TipoSolicitud.Certificados   'Certificados
            If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoCertificado.Certificado Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoCertificado.FecObtencion Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoCertificado.NombreCertif Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoCertificado.Alcance Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoCertificado.NumCertificado Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoCertificado.EntidadCertific Then
               cmdEliminarCampo.Enabled = False
            Else
                cmdEliminarCampo.Enabled = True
            End If
         
        Case TipoSolicitud.NoConformidades  'No conformidad
            If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.CausasYConclus Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.Motivo Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.titulo Or g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.Acciones Then
               cmdEliminarCampo.Enabled = False
            Else
                cmdEliminarCampo.Enabled = True
            End If
            
        Case TipoSolicitud.Contrato  'Contrato
            If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.DescripcionBreveContrato Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.DescripcionDetalladaContrato Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.ArchivoContrato Then
                cmdEliminarCampo.Enabled = False
            Else
                cmdEliminarCampo.Enabled = True
            End If
        
        Case TipoSolicitud.Proyecto
            If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.DescrBreve Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.IniSuministro Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.FinSuministro Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.DesgloseActividad Then
               cmdEliminarCampo.Enabled = False
            Else
                cmdEliminarCampo.Enabled = True
            End If
                
        Case TipoSolicitud.OtrasSolicitudes, TipoSolicitud.SolicitudQA  'Es un tipo de solicitud normal:
            cmdEliminarCampo.Enabled = True

        Case TipoSolicitud.Abono
            If g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.Factura Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.InicioAbono Or _
               g_oTipoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.FinAbono Then
                cmdEliminarCampo.Enabled = True
            Else
                cmdEliminarCampo.Enabled = False
            End If
        
    End Select
End Sub
