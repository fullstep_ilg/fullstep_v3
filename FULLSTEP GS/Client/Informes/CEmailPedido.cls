VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmailPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public oFSGSRaiz As CRaiz
Public Mensajes As CMensajes

Private m_oParametrosInstalacion As ParametrosInstalacion
Private m_oParametrosGenerales As ParametrosGenerales

Public Property Get ParametrosInstalacion() As ParametrosInstalacion
    ParametrosInstalacion = m_oParametrosInstalacion
End Property

Public Property Let ParametrosInstalacion(ByRef vNewValue As ParametrosInstalacion)
    m_oParametrosInstalacion = vNewValue
End Property

Public Property Get ParametrosGenerales() As ParametrosGenerales
    ParametrosGenerales = m_oParametrosGenerales
End Property

Public Property Let ParametrosGenerales(ByRef vNewValue As ParametrosGenerales)
    m_oParametrosGenerales = vNewValue
End Property

'**************************************************************************
'*** Descripción: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSGSNotifPedidoReceptor.txt o                     ***
'***              ENG_FSGSNotifPedidoReceptor.txt dependiéndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
Public Function GenerarMensajeAceptacionTXT(ByVal sIdioma As String, ByRef oOrdenEntrega As COrdenEntrega) As String
    Dim sConsulta As String
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sFileName As String
    Dim sFile As String
    Dim sFileNew As String
    Dim oEmpresas As CEmpresas
    Dim sTablaLineas As String
    Dim oLinea As CLineaPedido
    
    Set oFos = New FileSystemObject
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , oOrdenEntrega.Empresa
    If InStr(m_oParametrosInstalacion.gsPEDNotifAcepTXT, m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        If oEmpresas.Count Then sCarpeta = oEmpresas.Item(1).Carpeta_Plantillas
    End If
    If sCarpeta = "" And m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    'Coge la plantilla de parámetros de la instalación,pero con el idioma del usuario
    sFileName = Replace(m_oParametrosInstalacion.gsPEDNotifAcepTXT, sMarcaPlantillas, sCarpeta)
    sFile = oFos.GetFileName(sFileName)
    If sFile <> "" Then
        sFileNew = sIdioma & "_" & Right(sFile, Len(sFile) - InStr(1, sFile, "_"))
        sFileName = Replace(sFileName, sFile, sFileNew)
    End If
    
    If Not oFos.FileExists(sFileName) Then
        Mensajes.PlantillaNoEncontrada sFileName
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeAceptacionTXT = ""
        Exit Function
    End If
    
    If oOrdenEntrega Is Nothing Then
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeAceptacionTXT = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    
    sCuerpoMensaje = ostream.ReadAll
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVE", NullToStr(oOrdenEntrega.ProveDen))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@ANYO", oOrdenEntrega.Anyo)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PEDIDO", oOrdenEntrega.NumPedido)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@ORDEN", oOrdenEntrega.Numero)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA", oOrdenEntrega.FechaEmision)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IMPORTE", FormateoNumerico(oOrdenEntrega.importe, "#,##0.00"))
    If Not oOrdenEntrega.Persona Is Nothing Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APROV_NOM", NullToStr(oOrdenEntrega.Persona.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APROV_APE", NullToStr(oOrdenEntrega.Persona.Apellidos))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APROV_NOM", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APROV_APE", "")
    End If
    If Not oOrdenEntrega.Receptor Is Nothing Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RECEP_NOM", NullToStr(oOrdenEntrega.Receptor.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RECEP_APE", NullToStr(oOrdenEntrega.Receptor.Apellidos))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RECEP_NOM", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RECEP_APE", "")
    End If
    
    If Not oEmpresas.Item(1) Is Nothing Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIF", oEmpresas.Item(1).NIF)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RAZON", oEmpresas.Item(1).Den)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIF", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RAZON", "")
    End If

    sCuerpoMensaje = Replace(sCuerpoMensaje, "@REFERENCIA", NullToStr(oOrdenEntrega.NumPedidoERP))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@OBSPEDIDO", NullToStr(oOrdenEntrega.Observaciones))
    
    sTablaLineas = ""
    oOrdenEntrega.CargarLineasDePedido True, "0", , m_oParametrosInstalacion.gIdioma

    If Not oOrdenEntrega.LineasPedido Is Nothing Then
        For Each oLinea In oOrdenEntrega.LineasPedido
            sTablaLineas = sTablaLineas & NullToStr(oLinea.ArtCod_Interno) & " - " & NullToStr(oLinea.ArtDen) & vbCrLf
        Next
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@LINEAS", sTablaLineas)
    

    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oEmpresas = Nothing
    
    GenerarMensajeAceptacionTXT = sCuerpoMensaje
End Function

'**************************************************************************
'*** Descripción: Genera el cuerpo del e-mail en HTML a partir          ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSGSNotifPedidoReceptor.htm o                     ***
'***              ENG_FSGSNotifPedidoReceptor.htm dependiéndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
Public Function GenerarMensajeAceptacionHTML(ByVal sIdioma As String, ByRef oOrdenEntrega As COrdenEntrega) As String
    Dim sConsulta As String
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sFileName As String
    Dim sFile As String
    Dim sFileNew As String
    Dim oEmpresas As CEmpresas
    Dim oLinea As CLineaPedido
    Dim lPosIni As Long
    Dim lPosFin As Long
    Dim stmpfila As String
    Dim sDetalle As String
    Dim sFila As String
    
    Set oFos = New FileSystemObject
    
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , oOrdenEntrega.Empresa
    If InStr(m_oParametrosInstalacion.gsPEDNotifAcepHTML, m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        If oEmpresas.Count Then sCarpeta = oEmpresas.Item(1).Carpeta_Plantillas
    End If
    If sCarpeta = "" And m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    'cogerá la plantilla con el idioma del usuario
    sFileName = Replace(m_oParametrosInstalacion.gsPEDNotifAcepHTML, sMarcaPlantillas, sCarpeta)
    sFile = oFos.GetFileName(sFileName)
    If sFile <> "" Then
        sFileNew = sIdioma & "_" & Right(sFile, Len(sFile) - InStr(1, sFile, "_"))
        sFileName = Replace(sFileName, sFile, sFileNew)
    End If
    
    If Not oFos.FileExists(sFileName) Then
        Mensajes.PlantillaNoEncontrada sFileName
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeAceptacionHTML = ""
        Exit Function
    End If
    
    If oOrdenEntrega Is Nothing Then
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeAceptacionHTML = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    
    sCuerpoMensaje = ostream.ReadAll
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVE", NullToStr(oOrdenEntrega.ProveDen))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@ANYO", oOrdenEntrega.Anyo)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PEDIDO", oOrdenEntrega.NumPedido)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@ORDEN", oOrdenEntrega.Numero)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_EMI", oOrdenEntrega.FechaEmision)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IMPORTE", FormateoNumerico(oOrdenEntrega.importe, "#,##0.00"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODMON", NullToStr(oOrdenEntrega.Moneda))
    If Not oOrdenEntrega.Persona Is Nothing Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APROV_NOM", NullToStr(oOrdenEntrega.Persona.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APROV_APE", NullToStr(oOrdenEntrega.Persona.Apellidos))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APROV_NOM", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APROV_APE", "")
    End If
    If Not oOrdenEntrega.Receptor Is Nothing Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RECEP_NOM", NullToStr(oOrdenEntrega.Receptor.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RECEP_APE", NullToStr(oOrdenEntrega.Receptor.Apellidos))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RECEP_NOM", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RECEP_APE", "")
    End If
    
    
    If Not oEmpresas.Item(1) Is Nothing Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIF", oEmpresas.Item(1).NIF)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RAZON", oEmpresas.Item(1).Den)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIF", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RAZON", "")
    End If

    sCuerpoMensaje = Replace(sCuerpoMensaje, "@REFERENCIA", NullToStr(oOrdenEntrega.NumPedidoERP))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@OBSPEDIDO", NullToStr(oOrdenEntrega.Observaciones))


    lPosIni = InStr(sCuerpoMensaje, "<!--INICIO FILA-->")
    lPosFin = InStr(sCuerpoMensaje, "<!--FIN FILA-->")
    If lPosIni > 0 Then
        stmpfila = Mid(sCuerpoMensaje, lPosIni, lPosFin - lPosIni)

        oOrdenEntrega.CargarLineasDePedido True, "0", , m_oParametrosInstalacion.gIdioma
    
        If Not oOrdenEntrega.LineasPedido Is Nothing Then
            sDetalle = ""
            For Each oLinea In oOrdenEntrega.LineasPedido
                sFila = stmpfila
            
                sFila = Replace(sFila, "@CODART", NullToStr(oLinea.ArtCod_Interno))
                sFila = Replace(sFila, "@DENART", NullToStr(oLinea.ArtDen))
                sFila = Replace(sFila, "@CODDEST", NullToStr(oLinea.CodDestino))
                sFila = Replace(sFila, "@CODUNI", NullToStr(oLinea.UnidadPedido))
                sFila = Replace(sFila, "@PRECIOUNITARIO", FormateoNumerico(oLinea.PrecioUP, "#,##0.00"))
                If oLinea.CantidadPedida = 0 Then
                    sFila = Replace(sFila, "@CANTIDAD", 0)
                Else
                    sFila = Replace(sFila, "@CANTIDAD", FormateoNumerico(oLinea.CantidadPedida, "#,##0.00"))
                End If
                If oLinea.CantidadPedida = 0 Then
                    sFila = Replace(sFila, "@PRECIO", 0)
                Else
                    sFila = Replace(sFila, "@PRECIO", FormateoNumerico(oLinea.PrecioUP * oLinea.CantidadPedida, "#,##0.00"))
                End If
                
                sFila = Replace(sFila, "@FECHA_ENT", NullToStr(oLinea.FechaEntrega))
                
                sDetalle = sDetalle & sFila
            Next
            
            sCuerpoMensaje = Left(sCuerpoMensaje, lPosIni - 1) & sDetalle & Mid(sCuerpoMensaje, lPosFin, Len(sCuerpoMensaje))
        End If

    End If

    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oEmpresas = Nothing
    
    GenerarMensajeAceptacionHTML = sCuerpoMensaje
End Function
