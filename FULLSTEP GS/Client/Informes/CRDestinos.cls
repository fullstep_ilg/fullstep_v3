VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRDestinos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public IdiomaInstalacion As String   'gParametrosInstalacion.gIdioma

Public Sub Listado(oReport As CRAXDRT.Report, Optional ByVal OrdenPorDen As Boolean, Optional ByVal Pais As Variant, Optional ByVal Provi As Variant, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal bRuo As Boolean, Optional ByVal sPais, Optional ByVal sProvi)
    Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSelectionFormula As String
    Dim GroupOrder1 As String
    Dim GroupOrder2 As String
    Dim RecordSortFields As String
    Dim i As Integer
    Dim SQLQueryString As String
    Dim FormulaUO As String
    Dim oConnectionInfo As CRAXDRT.ConnectionProperties

    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    
    For Each Table In oReport.Database.Tables
        'Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Set oConnectionInfo = Table.ConnectionProperties
        'Set the OLE DB Provider
        oConnectionInfo.Item("Provider") = "SQLOLEDB"
        'Set the physical server name
        oConnectionInfo.Item("Data Source") = scrs_Server
        'Set the database name
        oConnectionInfo.Item("Initial Catalog") = scrs_Database
        'Set the integrated security
        oConnectionInfo.Item("Integrated Security") = False
        'Set the user name
        oConnectionInfo.Item("User ID") = scrs_User
        'Set the password"
        oConnectionInfo.Item("Password") = scrs_Password
        'Set the fully qualified table name if different from �the original data source
        Table.Location = scrs_Database & ".dbo." & Table.Name
        
        Set oConnectionInfo = Nothing
    Next
    
    '*********** FORMULA FIELDS REPORT
    
    ParametrosListadoDestinos oReport, sPais, sProvi, OrdenPorDen, sUON1, sUON2, sUON3
    
    'SQLQueryString = ConstruirSQLListado(UON1, UON2, UON3, bRuo, sUON1, sUON2, sUON3, OrdenPorDen)
    
    oReport.SQLQueryString = SQLQueryString
    
    'filtros por si han elegido una UO desde la que empezar:
    Dim sConsultaUON As String
    If sUON3 <> "" Then
        sConsultaUON = sConsultaUON & " {Command.UON1_COD} = '" & sUON1 & "' And {Command.UON2_COD}= '" & sUON2 & "' And {Command.UON3_COD} = '" & sUON3 & "' "
    Else
        If sUON2 <> "" Then
                sConsultaUON = " {Command.UON1_COD} = '" & sUON1 & "' And {Command.UON2_COD}= '" & sUON2 & "' "
        Else
            If sUON1 <> "" Then
                sConsultaUON = " {Command.UON1_COD} = '" & sUON1 & "' "

            End If
        End If
    End If
    
    oReport.RecordSelectionFormula = sConsultaUON
    
End Sub

Public Function ConstruirSQLListado(Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal bRuo As Boolean = False, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal OrdenadosPorDen As Boolean)
Dim sConsultaUON As String

If Not IsMissing(UON1) Then
    If IsEmpty(UON1) Then
         UON1 = Null
    End If
    If Trim(UON1) = "" Then
         UON1 = Null
    End If
Else
     UON1 = Null
End If

If Not IsMissing(UON2) Then
    If IsEmpty(UON2) Then
         UON2 = Null
    End If
    If Trim(UON2) = "" Then
         UON2 = Null
    End If
Else
     UON2 = Null
End If

If Not IsMissing(UON3) Then
    If IsEmpty(UON3) Then
        UON3 = Null
    End If
    If Trim(UON3) = "" Then
        UON3 = Null
    End If
Else
    UON3 = Null
End If

sConsultaUON = "SELECT UON1.COD, UON1.DEN  ,UON2.COD , UON2.DEN , UON3.COD , UON3.DEN  "
sConsultaUON = sConsultaUON & " FROM UON1 LEFT JOIN (UON2 LEFT JOIN UON3 ON UON2.UON1=UON3.UON1 AND UON2.COD=UON3.UON2) ON UON1.COD=UON2.UON1"

If bRuo Then
'Restriccion de unidad
   If IsNull(UON1) Then
        sConsultaUON = sConsultaUON & " WHERE 1=1"
   Else
        If IsNull(UON2) Then
            sConsultaUON = sConsultaUON & " WHERE UON1.COD = '" & DblQuote(CStr(UON1)) & "'"
        Else
            If IsNull(UON3) Then
                 sConsultaUON = sConsultaUON & " WHERE UON1.COD ='" & DblQuote(CStr(UON1)) & "' AND UON2.COD='" & DblQuote(CStr(UON2)) & "'"
            Else
                 sConsultaUON = sConsultaUON & " WHERE UON1.COD ='" & DblQuote(CStr(UON1)) & "'" & _
                    " AND UON2.COD='" & DblQuote(CStr(UON2)) & "' AND UON3.COD='" & DblQuote(CStr(UON3)) & "'"
            End If
        End If
    End If
Else
' No hay restricciones
    sConsultaUON = sConsultaUON & " WHERE 1=1"
End If
    'Si han elegido una UO desde la que empezar
If sUON3 <> "" Then
    sConsultaUON = sConsultaUON & " AND UON1.COD = '" & sUON1 & "' AND UON2.COD = '" & sUON2 & "' AND UON3.COD = '" & sUON3 & "'"
                        
Else
    If sUON2 <> "" Then
            sConsultaUON = sConsultaUON & " AND UON1.COD = '" & sUON1 & "' AND UON2.COD = '" & sUON2 & "'"
    Else
            If sUON1 <> "" Then
                sConsultaUON = sConsultaUON & " AND UON1.COD = '" & sUON1 & "'"

            End If
    End If
End If

    sConsultaUON = sConsultaUON & " AND 1=1"
    
    If OrdenadosPorDen Then
        sConsultaUON = sConsultaUON & " ORDER BY UON1.DEN, UON2.DEN, UON3.DEN"
    Else
        sConsultaUON = sConsultaUON & " ORDER BY UON1.COD, UON2.COD, UON3.COD"
    End If
    
    ConstruirSQLListado = sConsultaUON

End Function

''' <summary>
''' Configura los parametros del listado de destinos
''' </summary>
''' <param name="oReport">El report</param>
''' <param name="sPais">Pais</param>
''' <param name="sProvi">provincia</param>
''' <param name="OrdenPorDen">OrdenacionPorDenominacion</param>
''' <returns></returns>
''' <remarks>Llamada desde:CrDestinos.Listado Tiempo m�ximo: 0,1</remarks>
Private Sub ParametrosListadoDestinos(oReport As CRAXDRT.Report, Optional ByVal sPais As String, Optional ByVal sProvi As String, Optional ByVal OrdenPorDen As Boolean _
, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String)
    Dim i As Integer
    Dim Srpt As CRAXDRT.Report
    Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSelFormula As String
    Dim GroupOrder1 As String
    Dim GroupOrder2 As String
    Dim RecordSortFields As String
    Dim oConnectionInfo As CRAXDRT.ConnectionProperties
    
    For i = 0 To 3
        Set Srpt = Nothing
        Set Srpt = oReport.OpenSubreport("rptDest_UON" & i)

        For Each Table In Srpt.Database.Tables
            'Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
            Set oConnectionInfo = Table.ConnectionProperties
            'Set the OLE DB Provider
            oConnectionInfo.Item("Provider") = "SQLOLEDB"
            'Set the physical server name
            oConnectionInfo.Item("Data Source") = scrs_Server
            'Set the database name
            oConnectionInfo.Item("Initial Catalog") = scrs_Database
            'Set the integrated security
            oConnectionInfo.Item("Integrated Security") = False
            'Set the user name
            oConnectionInfo.Item("User ID") = scrs_User
            'Set the password"
            oConnectionInfo.Item("Password") = scrs_Password
            'Set the fully qualified table name if different from �the original data source
            Table.Location = scrs_Database & ".dbo." & Table.Name
            Set oConnectionInfo = Nothing
        Next
      
        RecordSelFormula = ""
        If sPais <> "" Then RecordSelFormula = "{Command.PAI}='" & DblQuote(sPais) & "'"
        If sProvi <> "" Then RecordSelFormula = RecordSelFormula & " AND {Command.PROVI}='" & DblQuote(sProvi) & "'"
                    
        If RecordSelFormula <> "" Then
             Srpt.RecordSelectionFormula = Srpt.RecordSelectionFormula & " AND " & RecordSelFormula
        End If
            
        If OrdenPorDen Then
            GroupOrder1 = "{Command.DEN_PAIS}"
            GroupOrder2 = "{Command.DEN_PROV}"
            RecordSortFields = "+{Command.DEN_" & IdiomaInstalacion & "}"
        Else
            GroupOrder1 = "{Command.PAI}"
            GroupOrder2 = "{Command.PROVI}"
            RecordSortFields = "+{Command.COD}"
        End If
        
        Srpt.FormulaFields(crs_FormulaIndex_Interno(Srpt, "ORDEN_GRUPO1")).Text = GroupOrder1
        Srpt.FormulaFields(crs_FormulaIndex_Interno(Srpt, "ORDEN_GRUPO2")).Text = GroupOrder2
        
        Srpt.RecordSortFields.Add Srpt.Database.Tables(crs_Tableindex_Interno(Srpt, crs_SortTable_Interno(RecordSortFields))).Fields(crs_FieldIndex_Interno(Srpt, crs_SortTable_Interno(RecordSortFields), crs_SortField_Interno(RecordSortFields))), crs_SortDirection_Interno(RecordSortFields)
        
    Next i
        
End Sub


