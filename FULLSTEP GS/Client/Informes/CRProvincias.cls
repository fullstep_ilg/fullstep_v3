VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRProvincias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' <summary>
''' Listado de provincias
''' </summary>
''' <param name="oReport">Report </param>
''' <param name="bTodos">Todas las provibncias de todos los paises </param>
''' <param name="bOrdCod">Ordenacion por codigo</param>
''' <param name="sCodPais">Codigo del pais</param>
''' <returns></returns>
''' <remarks>Llamada desde: frmLstProvi.cmdObtenerClick Tiempo m�ximo: 0,1</remarks>
Public Function ListadoProvincias(oReport As CRAXDRT.Report, Optional ByVal bTodos As Boolean, Optional ByVal bOrdCod As Boolean, Optional ByVal scodPais As String)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim RecordSelectionFormula  As String
    Dim RecordSortFields As String
    Dim GroupOrder(1 To 2, 1 To 1) As String
    
                                                            
                                                            
    If bTodos = True Then
        RecordSelectionFormula = ""
    Else
        RecordSelectionFormula = "{PROVI.PAI}='" & DblQuote(scodPais) & "'"
    End If
                                                            
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    oReport.RecordSelectionFormula = RecordSelectionFormula
    
    
    GroupOrder(2, 1) = "ORDEN_GRUPO1"
    If bOrdCod = True Then
        GroupOrder(1, 1) = "+{PROVI.PAI}"
        RecordSortFields = "+{PROVI.COD}"
    Else
        GroupOrder(1, 1) = "+{PAI_DEN.DEN}"
        RecordSortFields = "+{PROVI_DEN.DEN}"
    End If

    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    
    ' RECORD SORT ORDER
    oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(RecordSortFields))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(RecordSortFields), crs_SortField_Interno(RecordSortFields))), crs_SortDirection_Interno(RecordSortFields)
    
    
    For i = 1 To UBound(GroupOrder, 2)
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, GroupOrder(2, i))).Text = oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(GroupOrder(1, i)))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(GroupOrder(1, i)), crs_SortField_Interno(GroupOrder(1, i)))).Name
    Next i

    
End Function

