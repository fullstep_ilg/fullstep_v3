VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRPaises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' <summary>
''' Listado de paises
''' </summary>
''' <param name="oReport">Report </param>
''' <param name="OrdenarPorDen">Ordenar por denominacion </param>
''' <returns></returns>
''' <remarks>Llamada desde: frmLstPai.cmdObtener_Click; Tiempo m�ximo: 0,1</remarks>
Public Sub Listado(oReport As CRAXDRT.Report, OrdenarPorDen As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim RecordSortFields As String

    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
                                                            
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    If OrdenarPorDen = False Then
        RecordSortFields = "+{PAI.COD}"
    Else
        RecordSortFields = "+{PAI_DEN.DEN}"
    End If
  
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
        
    
    oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(RecordSortFields))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(RecordSortFields), crs_SortField_Interno(RecordSortFields))), crs_SortDirection_Interno(RecordSortFields)
    
    
End Sub

