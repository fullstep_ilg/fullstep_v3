VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRAtributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub Listado(oReport As CRAXDRT.Report, ByVal sOrigen As String, Optional ByVal Cod As Variant, Optional ByVal Den As Variant, _
    Optional ByVal vTipo As Variant, Optional ByVal vIntroducc As Variant, Optional ByVal sGmn1 As String, _
    Optional ByVal sGmn2 As String, Optional ByVal sGmn3 As String, Optional ByVal sGmn4 As String, _
    Optional ByVal sCodArt As String, Optional ByVal bBajasLog As Boolean, Optional ByVal OrdPorCod As Boolean, _
    Optional ByVal sCopGMN1 As String, Optional ByVal sCopGMN2 As String, Optional ByVal sCopGMN3 As String, _
    Optional ByVal sCopGMN4 As String, Optional ByVal sCopCodArt As String, _
    Optional ByVal bATRIBRestMatComp As Boolean, _
    Optional ByVal sEqp As String, _
    Optional ByVal sCodigo As String, Optional ByVal bfiltrarmat As Boolean, Optional ByVal bfiltrarart As Boolean, Optional ByVal bfiltraruon As Boolean, _
    Optional ByRef oUons As CUnidadesOrganizativas, Optional ByVal bNoMostrarDescendientes As Boolean)
    
    Dim sSQLQueryRpt As String
    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim RecordSortFields As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
                                                                
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    'Obtiene la select
    sSQLQueryRpt = ObtenerSQLListadoAtrib(sOrigen, Cod, Den, vTipo, vIntroducc, sGmn1, sGmn2, sGmn3, sGmn4, sCodArt, bBajasLog, _
        OrdPorCod, sCopGMN1, sCopGMN2, sCopGMN3, sCopGMN4, sCopCodArt, bATRIBRestMatComp, _
        sEqp, sCodigo, bfiltrarmat, bfiltrarart, bfiltraruon, oUons, bNoMostrarDescendientes)

    oReport.SQLQueryString = sSQLQueryRpt
    
End Sub

Private Function ObtenerSQLListadoAtrib(ByVal sOrigen As String, _
            Optional ByVal Cod As String, _
            Optional ByVal Den As String, _
            Optional ByVal vTipo As Variant, _
            Optional ByVal udtIntroduccion As Variant, _
            Optional ByVal sGmn1 As String, _
            Optional ByVal sGmn2 As String, _
            Optional ByVal sGmn3 As String, _
            Optional ByVal sGmn4 As String, _
            Optional ByVal sCodArt As String, _
            Optional ByVal bBajasLog As Boolean, _
            Optional ByVal OrdPorCod As Boolean, _
            Optional ByVal sCopGMN1 As String, _
            Optional ByVal sCopGMN2 As String, _
            Optional ByVal sCopGMN3 As String, _
            Optional ByVal sCopGMN4 As String, _
            Optional ByVal sCopCodArt As String, _
            Optional ByVal bATRIBRestMatComp As Boolean, _
            Optional ByVal sEqp As String, _
            Optional ByVal sCodigo As String, _
            Optional ByVal bfiltrarmat As Boolean, _
            Optional ByVal bfiltrarart As Boolean, _
            Optional ByVal bfiltraruon As Boolean, Optional ByRef oUons As CUnidadesOrganizativas, Optional ByVal bNoMostrarDescendientes As Boolean)
    
    Dim sConsulta As String
    
    sConsulta = "SELECT DISTINCT ID,COD,DEN,TIPO,INTRO,MINNUM,MAXNUM,MINFEC,MAXFEC,PREFBAJO,BAJA_LOG,POND,FORMULA,NUM,POND_SI,POND_NO,descr "
    sConsulta = sConsulta & " FROM DEF_ATRIB"
    
    If sCodArt = "" Then
        If bATRIBRestMatComp Then
            sConsulta = sConsulta & " inner join GMN4_ATRIB WITH (NOLOCK) on DEF_ATRIB.ID = GMN4_ATRIB.ATRIB "
            sConsulta = sConsulta & " inner join COM_GMN4 WITH (NOLOCK) on GMN4_ATRIB.GMN1 = COM_GMN4.GMN1 "
            sConsulta = sConsulta & " and GMN4_ATRIB.GMN2 = COM_GMN4.GMN2 "
            sConsulta = sConsulta & " and GMN4_ATRIB.GMN3 = COM_GMN4.GMN3 "
            sConsulta = sConsulta & " and GMN4_ATRIB.GMN4 = COM_GMN4.GMN4 "
            sConsulta = sConsulta & " and COM_GMN4.EQP='" & DblQuote(sEqp) & "' AND COM_GMN4.COM='" & DblQuote(sCodigo) & "'"
        End If
        
        If sGmn1 <> "" Then
            If bATRIBRestMatComp Then
                sConsulta = sConsulta & " AND GMN4_ATRIB.GMN1='" & DblQuote(sGmn1) & "'"
            Else
                sConsulta = sConsulta & " INNER JOIN GMN4_ATRIB WITH (NOLOCK) ON DEF_ATRIB.ID = GMN4_ATRIB.ATRIB AND GMN4_ATRIB.GMN1='" & DblQuote(sGmn1) & "'"
            End If
        End If
        If sGmn2 <> "" And sGmn1 <> "" Then
            sConsulta = sConsulta & " AND GMN4_ATRIB.GMN2='" & DblQuote(sGmn2) & "'"
        End If
        If sGmn3 <> "" And sGmn2 <> "" And sGmn1 <> "" Then
            sConsulta = sConsulta & " AND GMN4_ATRIB.GMN3='" & DblQuote(sGmn3) & "'"
        End If
        If sGmn4 <> "" And sGmn3 <> "" And sGmn2 <> "" And sGmn1 <> "" Then
            sConsulta = sConsulta & " AND GMN4_ATRIB.GMN4='" & DblQuote(sGmn4) & "'"
        End If
    Else
        sConsulta = sConsulta & " INNER JOIN ART4_ATRIB ON DEF_ATRIB.ID = ART4_ATRIB.ATRIB "
        sConsulta = sConsulta & " AND ART4_ATRIB.ART='" & DblQuote(sCodArt) & "'"
        If bATRIBRestMatComp Then
            sConsulta = sConsulta & " INNER JOIN ART4 WITH (NOLOCK) ON ART4_ATRIB.ART= ART4.COD "
            sConsulta = sConsulta & " INNER JOIN COM_GMN4 WITH (NOLOCK) ON ART4.GMN1 = COM_GMN4.GMN1 "
            sConsulta = sConsulta & " AND ART4.GMN2 = COM_GMN4.GMN2 AND ART4.GMN3 = COM_GMN4.GMN3 "
            sConsulta = sConsulta & " AND ART4.GMN4 = COM_GMN4.GMN4 "
            sConsulta = sConsulta & " and COM_GMN4.EQP='" & DblQuote(sEqp) & "' AND COM_GMN4.COM='" & DblQuote(sCodigo) & "'"
        End If
    End If
    
    'filtrado por ambito
    If bfiltrarmat Or bfiltrarart Or bfiltraruon Then
        Dim sConsultaFiltro As String
        Dim sConsultaFiltro2 As String
        sConsultaFiltro2 = ""
        sConsultaFiltro = " INNER JOIN("
        If bfiltraruon Then
            sConsultaFiltro2 = sConsultaFiltro2 & " SELECT ATRIB"
            sConsultaFiltro2 = sConsultaFiltro2 & " FROM UON_ATRIB WITH (NOLOCK)"
        End If
        If bfiltrarmat Then
            If sConsultaFiltro2 <> "" Then
                sConsultaFiltro2 = sConsultaFiltro2 & " UNION"
            End If
            sConsultaFiltro2 = sConsultaFiltro2 & " SELECT ATRIB"
            sConsultaFiltro2 = sConsultaFiltro2 & " FROM GMN4_ATRIB WITH (NOLOCK)"
        End If
        If bfiltrarart Then
            If sConsultaFiltro2 <> "" Then
                sConsultaFiltro2 = sConsultaFiltro2 & " UNION"
            End If
            sConsultaFiltro2 = sConsultaFiltro2 & " SELECT ATRIB"
            sConsultaFiltro2 = sConsultaFiltro2 & " FROM ART4_ATRIB WITH (NOLOCK)"
        End If
        sConsultaFiltro = sConsultaFiltro & sConsultaFiltro2 & " )FILTER ON FILTER.ATRIB=DEF_ATRIB.ID"
    End If
    
    sConsulta = sConsulta & sConsultaFiltro
    
    If sOrigen = "GMN4" Then
        sConsulta = sConsulta & " WHERE DEF_ATRIB.ID NOT IN ( SELECT GMN4_ATRIB.ATRIB FROM GMN4_ATRIB"
        sConsulta = sConsulta & " WHERE GMN4_ATRIB.GMN1='" & DblQuote(sCopGMN1) & "'"
        sConsulta = sConsulta & " AND GMN4_ATRIB.GMN2='" & DblQuote(sCopGMN2) & "'"
        sConsulta = sConsulta & " AND GMN4_ATRIB.GMN3='" & DblQuote(sCopGMN3) & "'"
        sConsulta = sConsulta & " AND GMN4_ATRIB.GMN4='" & DblQuote(sCopGMN4) & "')"
    ElseIf sOrigen = "ART4" Then
        sConsulta = sConsulta & " WHERE DEF_ATRIB.ID NOT IN ( SELECT ART4_ATRIB.ATRIB FROM ART4_ATRIB"
        sConsulta = sConsulta & " WHERE ART4_ATRIB.ART='" & DblQuote(sCopCodArt) & "')"
    Else
        sConsulta = sConsulta & " WHERE 1=1 "
    End If
    
    If Cod <> "" Then
        sConsulta = sConsulta & " AND DEF_ATRIB.COD LIKE '" & DblQuote(Cod) & "%'"
    End If
    
    If Den <> "" Then
        sConsulta = sConsulta & " AND DEF_ATRIB.DEN LIKE '" & DblQuote(Den) & "%'"
    End If
    
    If Not IsNull(vTipo) Then
        sConsulta = sConsulta & " AND DEF_ATRIB.TIPO=" & vTipo
    End If

    If udtIntroduccion = 0 Or udtIntroduccion = 1 Then
       sConsulta = sConsulta & " AND DEF_ATRIB.INTRO =" & udtIntroduccion
    End If
    
    If Not bBajasLog Then
        sConsulta = sConsulta & " AND DEF_ATRIB.BAJA_LOG=0"
    End If
    
    If Not oUons Is Nothing Then
        sConsulta = sConsulta & construirRestriccionPorUonsAtrib(oUons, bNoMostrarDescendientes)
    End If
    
    If OrdPorCod = True Then
        sConsulta = sConsulta & " ORDER BY DEF_ATRIB.COD"
    Else
        sConsulta = sConsulta & " ORDER BY DEF_ATRIB.DEN"
    End If
    
    ObtenerSQLListadoAtrib = sConsulta
End Function

