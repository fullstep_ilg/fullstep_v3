VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstParametros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Parametros generales (Opciones)"
   ClientHeight    =   2055
   ClientLeft      =   1020
   ClientTop       =   1290
   ClientWidth     =   4845
   Icon            =   "frmLstParametros.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2055
   ScaleWidth      =   4845
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   4845
      TabIndex        =   5
      Top             =   1680
      Width           =   4845
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   2
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1650
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   2910
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Orden"
      TabPicture(0)   =   "frmLstParametros.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   1005
         Left            =   120
         TabIndex        =   4
         Top             =   420
         Width           =   4575
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   630
            TabIndex        =   0
            Top             =   450
            Value           =   -1  'True
            Width           =   1515
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2445
            TabIndex        =   1
            Top             =   450
            Width           =   1515
         End
      End
   End
End
Attribute VB_Name = "frmLstParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public FSGSRaiz As CRaiz
Public GestorIdiomas As CGestorIdiomas
Public Mensajes As CMensajes
Public GestorParametros As CGestorParametros
Public Report As CRAXDRT.Report
Public GuardarParametrosIns As Boolean
Public ParamInstRPTPATH As String
Public ParamGenRPTPATH As String
Public Idioma As String
Public Titulo As String  'preview
Public SENT_ORD_CAL1 As Boolean
Public SENT_ORD_CAL2 As Boolean
Public SENT_ORD_CAL3 As Boolean

' Origen llamada a formulario
Public sOrigen As String
'Variables de idioma
Private sIdiTitulo(1 To 9) As String
Private SIdiOpciones As String
Private sIdiDest As String
Private txtPag As String
Private txtDe As String
Private txtCod As String
Private txtDen As String
Private txtDenSpa As String
Private txtDenEng As String
Private txtDenGer As String
Private txtConv As String
Private txtSi As String
Private txtNo As String
Private txtAscen As String
Private txtDescen As String
Private txtIni As String
Private txtFin As String
Private txtComp As String
Private txtAdju As String
Private txtNom As String
Private txtUON As String
Private txtDep As String
Private txtCar As String
Private txtPedido As String

Private m_oIdiomas As CIdiomas

''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmLstParametros.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_LSTPARAMETROS, Idioma)
   
    If Not Ador Is Nothing Then
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.Caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.Caption = Ador(0).Value
        Ador.MoveNext
        SIdiOpciones = Ador(0).Value '5
        Ador.MoveNext
        sIdiDest = Ador(0).Value
        For i = 1 To 9
            Ador.MoveNext
            sIdiTitulo(i) = Ador(0).Value
        Next i
        'Idiomas del RPT
        Ador.MoveNext
        txtPag = Ador(0).Value '200
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtCod = Ador(0).Value
        Ador.MoveNext
        txtDen = Ador(0).Value
        Ador.MoveNext
        txtConv = Ador(0).Value
        Ador.MoveNext
        txtSi = Ador(0).Value  '205
        Ador.MoveNext
        txtNo = Ador(0).Value
        Ador.MoveNext
        txtAscen = Ador(0).Value
        Ador.MoveNext
        txtDescen = Ador(0).Value
        Ador.MoveNext
        txtIni = Ador(0).Value
        Ador.MoveNext
        txtFin = Ador(0).Value '210
        Ador.MoveNext
        txtComp = Ador(0).Value
        Ador.MoveNext
        txtAdju = Ador(0).Value
        Ador.MoveNext
        txtNom = Ador(0).Value
        Ador.MoveNext
        txtUON = Ador(0).Value
        Ador.MoveNext
        txtDep = Ador(0).Value '215
        Ador.MoveNext
        txtCar = Ador(0).Value
        Ador.MoveNext
        txtPedido = Ador(0).Value '217 Admite pedidos
                    
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Public Sub ObtenerListadoCONFGEN()
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim oDestinoST As CDestino
    Dim oCRParametros As CRParametros
    Dim FormulaST As String
    Dim FormulaSTDEN As String
    
    Set oCRParametros = New CRParametros
    oCRParametros.IdiomaInstalacion = Idioma
    
    If ParamInstRPTPATH = "" Then
        If ParamGenRPTPATH = "" Then
            Mensajes.RutaDeRPTNoValida
            Set Report = Nothing
            Exit Sub
        Else
            ParamInstRPTPATH = ParamGenRPTPATH
            GuardarParametrosIns = True
        End If
    End If
    RepPath = ParamInstRPTPATH & "\rptCONFGEN.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Mensajes.RutaDeRPTNoValida
        Set Report = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
           
    'Destino sin transporte
    Set oDestinoST = FSGSRaiz.Generar_CDestino
    oDestinoST.CargarDestinoSinTransporte
    FormulaST = sIdiDest & " (" & oDestinoST.Cod & ")"
    FormulaSTDEN = oDestinoST.Denominaciones.Item(Idioma).Den
    Set oDestinoST = Nothing

    Set Report = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    Screen.MousePointer = vbHourglass
        
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "ST")).Text = """" & FormulaST & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "STDEN")).Text = """" & FormulaSTDEN & """"

    oCRParametros.ListadoCONFGEN Report

    If Report Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Titulo = "Listado de la Configuraci�n General"
    
    Screen.MousePointer = vbNormal
    
    Me.Hide
End Sub

''' <summary>
''' Obtencion de listados
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Boton cmdObtener Tiempo m�ximo: 0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub cmdObtener_Click()
    Dim oCRParametros As CRParametros
    Dim sOri As String
    Dim sTit As String
    Dim RepPath As String
    Dim RptFichero As String
    Dim oFos As FileSystemObject
    Dim Textos() As String
    Dim i As Integer
    Dim oIdi As CIdioma
    Dim j As Integer
    
    If Not bcrs_Connected Then Exit Sub
    
    Set oCRParametros = New CRParametros
    oCRParametros.IdiomaInstalacion = Idioma
    
    If sOrigen = "A5B1C1" Or sOrigen = "frmCONFGEN" Then
        ObtenerListadoCONFGEN
        Exit Sub
    End If

    Select Case sOrigen
        Case "A5B1C2", "frmPARPag"
            sOri = "frmPARPag"
            RptFichero = "rptPARPag"
            sTit = sIdiTitulo(2)
            ReDim Textos(1 To 2, 1 To 3)
            Textos(1, 1) = txtCod: Textos(2, 1) = "txtCod"
            j = 2
            For Each oIdi In m_oIdiomas
                If oIdi.Cod = Idioma Then

                    Textos(1, j) = txtDen & " (" & oIdi.Den & ")": Textos(2, j) = "txtDen"
                    j = j + 1
                    Exit For
                End If
            Next
            Textos(1, 3) = Idioma: Textos(2, 3) = "IDI"
            
        Case "A5B1C3", "frmPARRoles"
            RptFichero = "rptPARRoles"
            sOri = "frmPARRoles"
            sTit = sIdiTitulo(3)
            ReDim Textos(1 To 2, 1 To 5)
            Textos(1, 1) = txtCod: Textos(2, 1) = "txtCod"
            Textos(1, 2) = txtDen: Textos(2, 2) = "txtDen"
            Textos(1, 3) = txtConv: Textos(2, 3) = "txtConv"
            Textos(1, 4) = txtSi: Textos(2, 4) = "txtSi"
            Textos(1, 5) = txtNo: Textos(2, 5) = "txtNo"

        Case "A5B1C4", "frmPARHom"
            RptFichero = "rptPARHom"
            sOri = "frmPARHom"
            sTit = sIdiTitulo(4)
            
            Dim oLiterales As CLiterales
            Set oLiterales = GestorParametros.DevolverLiterales(16, 18, Idioma)
            
            ReDim Textos(1 To 2, 1 To 12)
            Textos(1, 1) = txtCod: Textos(2, 1) = "txtCod"
            Textos(1, 2) = txtDen: Textos(2, 2) = "txtDen"
            Textos(1, 3) = txtAscen: Textos(2, 3) = "txtAscen"
            Textos(1, 4) = txtDescen: Textos(2, 4) = "txtDescen"
            Textos(1, 5) = txtIni: Textos(2, 5) = "txtIni"
            Textos(1, 6) = txtFin: Textos(2, 6) = "txtFin"
            Textos(1, 7) = IIf(SENT_ORD_CAL1, "1", "0"): Textos(2, 7) = "txtSENTORDCAL1"
            Textos(1, 8) = IIf(SENT_ORD_CAL2, "1", "0"): Textos(2, 8) = "txtSENTORDCAL2"
            Textos(1, 9) = IIf(SENT_ORD_CAL3, "1", "0"): Textos(2, 9) = "txtSENTORDCAL3"
            Textos(1, 10) = oLiterales.Item(1).Den: Textos(2, 10) = "txtDENCAL1"
            Textos(1, 11) = oLiterales.Item(2).Den: Textos(2, 11) = "txtDENCAL2"
            Textos(1, 12) = oLiterales.Item(3).Den: Textos(2, 12) = "txtDENCAL3"
            
            
        Case "A5B1C5", "frmPAROfeEst"
            RptFichero = "rptPAROfeEst"
            sOri = "frmPAROfeEst"
            sTit = sIdiTitulo(5)
            ReDim Textos(1 To 2, 1 To 6)
            Textos(1, 1) = txtCod: Textos(2, 1) = "txtCod"
            Textos(1, 2) = txtDen: Textos(2, 2) = "txtDen"
            Textos(1, 3) = txtComp: Textos(2, 3) = "txtComp"
            Textos(1, 4) = txtAdju: Textos(2, 4) = "txtAdju"
            Textos(1, 5) = txtSi: Textos(2, 5) = "txtSi"
            Textos(1, 6) = txtNo: Textos(2, 6) = "txtNo"
            
        Case "A5B1C6", "frmPARFirmas"
            RptFichero = "rptPARFirmas"
            sOri = "frmPARFirmas"
            sTit = sIdiTitulo(6)
            ReDim Textos(1 To 2, 1 To 2)
            Textos(1, 1) = txtCod: Textos(2, 1) = "txtCod"
            Textos(1, 2) = txtDen: Textos(2, 2) = "txtDen"
            
        Case "A5B1C7", "frmPARAsis"
            RptFichero = "rptPARAsis"
            sOri = "frmPARAsis"
            sTit = sIdiTitulo(7)
            ReDim Textos(1 To 2, 1 To 5)
            Textos(1, 1) = txtCod: Textos(2, 1) = "txtCod"
            Textos(1, 2) = txtNom: Textos(2, 2) = "txtNom"
            Textos(1, 3) = txtUON: Textos(2, 3) = "txtUON"
            Textos(1, 4) = txtDep: Textos(2, 4) = "txtDep"
            Textos(1, 5) = txtCar: Textos(2, 5) = "txtCar"
            
        Case "A1B9", "frmPARViaPag"
            sOri = "frmPARViaPag"
            RptFichero = "rptPARViaPag"
            sTit = sIdiTitulo(8)
            ReDim Textos(1 To 2, 1 To 3)
            Textos(1, 1) = txtCod: Textos(2, 1) = "txtCod"
            j = 2
            For Each oIdi In m_oIdiomas
                If oIdi.Cod = Idioma Then
                    Textos(1, j) = txtDen & " (" & oIdi.Den & ")": Textos(2, j) = "txtDen"
                    j = j + 1
                    Exit For
                End If
            Next
            Textos(1, 3) = Idioma: Textos(2, 3) = "IDI"
        
        Case "A1B10", "frmPARTipoRelac"
            RptFichero = "rptPARTipoRelac"
            sOri = "frmPARTipoRelac"
            sTit = sIdiTitulo(9)
            ReDim Textos(1 To 2, 1 To 5)
            Textos(1, 1) = txtCod: Textos(2, 1) = "txtCod"
            j = 2
            For Each oIdi In m_oIdiomas
                If oIdi.Cod = Idioma Then
                    Textos(1, j) = txtDen & " (" & oIdi.Den & ")": Textos(2, j) = "txtDen"
                    j = j + 1
                    Exit For
                End If
            Next
            Textos(1, 3) = txtPedido: Textos(2, 3) = "txtPedido"
            Textos(1, 4) = txtSi: Textos(2, 4) = "txtSi"
            Textos(1, 5) = txtNo: Textos(2, 5) = "txtNo"
        
    End Select

    If ParamInstRPTPATH = "" Then
        If ParamGenRPTPATH = "" Then
            Mensajes.RutaDeRPTNoValida
            Set Report = Nothing
            Exit Sub
        Else
            ParamInstRPTPATH = ParamGenRPTPATH
            GuardarParametrosIns = True
        End If
    End If
    
    RepPath = ParamInstRPTPATH & "\" & RptFichero & ".rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Mensajes.RutaDeRPTNoValida
        Set Report = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set Report = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    'DPD -> Incidencia 16192 / 16219
    ConectarReportInterno Report, scrs_Server, scrs_Database, scrs_User, scrs_Password
    
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtTitulo")).Text = """" & sTit & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtPag")).Text = """" & txtPag & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDe")).Text = """" & txtDe & """"
    For i = 1 To UBound(Textos, 2)
        Report.FormulaFields(crs_FormulaIndex_Interno(Report, Textos(2, i))).Text = """" & Textos(1, i) & """"
    Next i
    
    oCRParametros.ListadosParGen Report, sOri, opOrdDen, Idioma

    If Report Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Titulo = sTit
    Screen.MousePointer = vbNormal
    
    Me.Hide
End Sub

''' <summary>procedimiento de carga inicial del formulario</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1 sec</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub Form_Load()
    Me.Height = 2460
    Me.Width = 4965
    
    Set m_oIdiomas = GestorParametros.DevolverIdiomas(False, False, True)
    
    CargarRecursos
    
    Select Case sOrigen
        Case "A5B1C1", "frmCONFGEN"
            Me.Caption = sIdiTitulo(1) & " (" & SIdiOpciones & ")"
        Case "A5B1C2", "frmPARPag"
            Me.Caption = sIdiTitulo(2) & " (" & SIdiOpciones & ")"
        Case "A5B1C3", "frmPARRoles"
            Me.Caption = sIdiTitulo(3) & " (" & SIdiOpciones & ")"
        Case "A5B1C4", "frmPARHom"
            Me.Caption = sIdiTitulo(4) & " (" & SIdiOpciones & ")"
        Case "A5B1C5", "frmPAROfeEst"
            Me.Caption = sIdiTitulo(5) & " (" & SIdiOpciones & ")"
        Case "A5B1C6", "frmPARFirmas"
            Me.Caption = sIdiTitulo(6) & " (" & SIdiOpciones & ")"
        Case "A5B1C7", "frmPARAsis"
            Me.Caption = sIdiTitulo(7) & " (" & SIdiOpciones & ")"
        Case "A1B9", "frmPARViaPag"
            Me.Caption = sIdiTitulo(8) & " (" & SIdiOpciones & ")"
        Case "A1B10", "frmPARTipoRelac"
            Me.Caption = sIdiTitulo(9) & " (" & SIdiOpciones & ")"
    End Select
End Sub


