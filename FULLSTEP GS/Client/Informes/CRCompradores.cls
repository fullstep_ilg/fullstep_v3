VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRCompradores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub Listado(oReport As CRAXDRT.Report, Optional ByVal OrdenPorDen As Boolean, Optional ByVal Eqp As Variant)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim RecordSelFormula As String
    Dim GroupOrder As String
    Dim RecordSortFields(1 To 2) As String
     
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    If Not IsMissing(Eqp) Then
        If IsEmpty(Eqp) Then
            Eqp = Null
        End If
        If Trim(Eqp) = "" Then
            Eqp = Null
        End If
    Else
        Eqp = Null
    End If
    
    RecordSelFormula = "{PER.BAJALOG}=0"  'Para que no incluya las personas que han sido dadas de baja
    If Not IsNull(Eqp) Then
        RecordSelFormula = RecordSelFormula & "AND {COM.EQP}='" & DblQuote(Eqp) & "'"
    End If
    oReport.RecordSelectionFormula = RecordSelFormula
    
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
        
    If OrdenPorDen Then
        GroupOrder = "{EQP.DEN}"
        RecordSortFields(1) = "+{COM.APE}"
        RecordSortFields(2) = "+{COM.NOM}"
    Else
        GroupOrder = "{COM.EQP}"
        RecordSortFields(1) = "+{COM.COD}"
        RecordSortFields(2) = ""
    End If
    
    For i = 1 To UBound(RecordSortFields)
        If RecordSortFields(i) <> "" Then
            oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(RecordSortFields(i)))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(RecordSortFields(i)), crs_SortField_Interno(RecordSortFields(i)))), crs_SortDirection_Interno(RecordSortFields(i))
        End If
    Next i
    ' formula field para Clasificación grupo
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "ORD_EQP")).Text = GroupOrder
            
    
End Sub




