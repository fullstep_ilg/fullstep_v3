VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CROrganigrama"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public AccesoFSSM As TipoAccesoFSSM 'gParametrosGenerales.gsAccesoFSSM

Public Function Listado(ByRef oGestorInformes As CGestorInformes, oReport As CRAXDRT.Report, Tipo As String, SelecUON() As Variant, Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal CodDep As Variant, Optional ByVal RUO As Boolean = False, Optional ByVal RDep As Boolean, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal OrdenadasPorDen As Boolean, Optional ByVal Compradoras As Integer, Optional ByVal BajaLog As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim j As Integer
    Dim SQLQueryString As String

    If Tipo <> "Estructura" Then

        If bcrs_Connected = False Then
            Set oReport = Nothing
            Exit Function
        End If
        
        For Each Table In oReport.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        
        SQLQueryString = ConstruirSQLListado(Tipo, UON1, UON2, UON3, CodDep, RUO, RDep, sUON1, sUON2, sUON3, OrdenadasPorDen, , Compradoras, BajaLog)
    
        oReport.SQLQueryString = SQLQueryString
            
    Else
    
        Dim Adores As Ador.Recordset
    
        If bcrs_Connected = False Then
            Set oReport = Nothing
            Exit Function
        End If
        
        For Each Table In oReport.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        
        Set Adores = oGestorInformes.ConstruirSQLListadoEstructura(Tipo, UON1, UON2, UON3, CodDep, RUO, RDep, sUON1, sUON2, sUON3, OrdenadasPorDen, , Compradoras, BajaLog)
        If Not Adores Is Nothing Then
            oReport.Database.SetDataSource Adores
            ParametrosListadoEstructura oGestorInformes, oReport, SelecUON, RDep, CodDep, OrdenadasPorDen, Compradoras, BajaLog, sUON1, sUON2, sUON3
        End If
    End If
    
End Function

Public Function ListadoEmpresas(ByRef oGestorInformes As CGestorInformes, oReport As CRAXDRT.Report, Tipo As String, SelecUON() As Variant, Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal CodDep As Variant, Optional ByVal RUO As Boolean = False, Optional ByVal RDep As Boolean, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal OrdenadasPorDen As Boolean, Optional ByVal Compradoras As Integer, Optional ByVal BajaLog As Boolean) As Boolean

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim j As Integer
    Dim SQLQueryString As String
    Dim Adores As Ador.Recordset

    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Function
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    Set Adores = oGestorInformes.ListadoEmpresas(Tipo, UON1, UON2, UON3, CodDep, RUO, RDep, sUON1, sUON2, sUON3, OrdenadasPorDen, , Compradoras, BajaLog)
    If Not Adores Is Nothing Then
        ListadoEmpresas = True
        oReport.Database.SetDataSource Adores
    Else
        ListadoEmpresas = False
    End If
End Function


Private Sub ParametrosListadoEstructura(ByRef oGestorInformes As CGestorInformes, oReport As CRAXDRT.Report, SelecUON() As Variant, Optional ByVal bRDep As Boolean, Optional ByVal Dep As String, Optional ByVal OrdenarPorDen As Boolean, Optional ByVal Compradoras As Integer, Optional ByVal bBajaLog As Boolean, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String)
Dim i As Integer
Dim Srpt As CRAXDRT.Report
Dim Table As CRAXDRT.DatabaseTable
Dim RecordSelFormula As String
Dim Adores As Ador.Recordset
    
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
        
    For i = 0 To UBound(SelecUON)
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "UON" & i & "SEL")).Text = """" & SelecUON(i) & """"
    Next i

    For i = 0 To 3
        Set Srpt = Nothing
        Set Srpt = oReport.OpenSubreport("rptUON" & i & "_DEP_PER")
        For Each Table In Srpt.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
      
        
        Select Case i
        

        Case 0
            RecordSelFormula = " ISNULL({EstrOrgSub0.UON1PER}) AND ISNULL({EstrOrgSub0.UON2PER}) AND ISNULL({EstrOrgSub0.UON3PER})"
        Case 1
            RecordSelFormula = " ISNULL({EstrOrgSub1.UON2PER}) AND ISNULL({EstrOrgSub1.UON3PER})"
        Case 2
            RecordSelFormula = " ISNULL({EstrOrgSub2.UON3PER})"
        Case 3
            RecordSelFormula = "1=1"
        End Select
        
        If bRDep Then RecordSelFormula = RecordSelFormula & " AND {EstrOrgSub" & i & ".DEPCOD}='" & DblQuote(Dep) & "'"
        
        If Compradoras = 1 Then  'Personas compradoras
            RecordSelFormula = RecordSelFormula & " AND NOT ISNULL({EstrOrgSub" & i & ".EQP})"
        ElseIf Compradoras = 2 Then  'Personas no compradoras
            RecordSelFormula = RecordSelFormula & "  AND ISNULL({EstrOrgSub" & i & ".EQP})"
        End If
        
        If Not bBajaLog Then
            RecordSelFormula = RecordSelFormula & "  AND ({EstrOrgSub" & i & ".BAJALOG})=0"
        End If
                
        Set Adores = oGestorInformes.ConstruirSubReportEstructura(i)
        Srpt.Database.SetDataSource Adores

        If RecordSelFormula <> "" Then
            If i > 0 Then
             Srpt.RecordSelectionFormula = Srpt.RecordSelectionFormula & " AND " & RecordSelFormula
            Else
             Srpt.RecordSelectionFormula = RecordSelFormula
            End If
        End If
      
       'Para poder ordenar el grupo por denominación
        If OrdenarPorDen Then
            Srpt.AddGroup 0, Srpt.Database.Tables(crs_Tableindex_Interno(Srpt, crs_SortTable_Interno("+{EstrOrgSub" & i & ".DEN}"))).Fields(crs_FieldIndex_Interno(Srpt, crs_SortTable_Interno("+{EstrOrgSub" & i & ".DEN}"), crs_SortField_Interno("+{EstrOrgSub" & i & ".DEN}"))), crGCAnyValue, crs_SortDirection_Interno("+{EstrOrgSub" & i & ".DEN}")
            Srpt.Sections.Item("GH1").Suppress = True
            Srpt.Sections.Item("GF1").Suppress = True
        End If
        
    Next i
    
    Set Srpt = Nothing
    Set Srpt = oReport.OpenSubreport("rptUON4")
    For Each Table In Srpt.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    If Not bBajaLog Then
        RecordSelFormula = RecordSelFormula & "  AND ({EstrOrgSub4.BAJALOG})=0"
    End If
    
    If AccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
        Set Adores = oGestorInformes.ConstruirSubReportEstructuraUON4(sUON1, sUON2, sUON3, bBajaLog, OrdenarPorDen)
        Srpt.Database.SetDataSource Adores
    Else
        'Como este subreport solo mostrara datos cuando haya acceso a FSSM le paso un recordset vacio.
        Set Adores = Nothing
        Set Adores = New adodb.Recordset
        Adores.Fields.Append "UON1", adVarChar, 3
        Adores.Fields.Append "UON2", adVarChar, 3
        Adores.Fields.Append "UON3", adVarChar, 3
        Adores.Fields.Append "UON4COD", adVarChar, 3
        Adores.Fields.Append "UON4DEN", adVarChar, 100
        Adores.Fields.Append "BAJALOG", adInteger
        Adores.Open
        Adores.AddNew
        Srpt.Database.SetDataSource Adores
    End If
End Sub

Public Function ConstruirSQLListado(TipoListado As String, Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal CodDep As Variant, Optional ByVal RUO As Boolean = False, Optional ByVal RDep As Boolean, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal OrdenadasPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal Compradoras As Integer, Optional ByVal BajaLog As Boolean)
'UON1 -- a partir de que un. del primer nivel
'UON2 -- a partir de que un. del segundo nivel
'UON3 -- a partir de que un. del tercer nivel

Dim sConsultaPER As String
Dim sConsultaUON As String

If Not IsMissing(UON1) Then
    If IsEmpty(UON1) Then
         UON1 = Null
    End If
    If Trim(UON1) = "" Then
         UON1 = Null
    End If
Else
     UON1 = Null
End If

If Not IsMissing(UON2) Then
    If IsEmpty(UON2) Then
         UON2 = Null
    End If
    If Trim(UON2) = "" Then
         UON2 = Null
    End If
Else
     UON2 = Null
End If

If Not IsMissing(UON3) Then
    If IsEmpty(UON3) Then
        UON3 = Null
    End If
    If Trim(UON3) = "" Then
        UON3 = Null
    End If
Else
    UON3 = Null
End If

If Not IsMissing(CodDep) Then
    If IsEmpty(CodDep) Then
        CodDep = Null
    End If
    If Trim(CodDep) = "" Then
        CodDep = Null
    End If
Else
    CodDep = Null
End If

sConsultaUON = "SELECT UON1.COD AS UON1COD, UON1.DEN AS UON1DEN, UON1.BAJALOG AS UON1BAJALOG ,UON2.COD AS UON2COD, UON2.DEN AS UON2DEN, UON2.BAJALOG AS UON2BAJALOG , UON3.COD AS UON3COD, UON3.DEN AS UON3DEN , UON3.BAJALOG  AS UON3BAJALOG"
sConsultaUON = sConsultaUON & ",EMP.NIF AS EMPNIF, EMP.DEN AS EMPDEN, EMP.DIR AS EMPDIR, EMP.CP AS EMPCP, EMP.POB AS EMPPOB ,EMP_2.NIF AS EMP2NIF, "
sConsultaUON = sConsultaUON & " EMP_2.DEN AS EMP2DEN, EMP_2.DIR AS EMP2DIR, EMP_2.CP AS EMP2CP, EMP_2.POB AS EMP2POB,EMP_3.NIF AS EMP3NIF , EMP_3.DEN AS EMP3DEN, EMP_3.DIR AS EMP3DIR, EMP_3.CP AS EMP3CP, EMP_3.POB AS EMP3POB, PAI.DEN AS PAIDEN , PAI2.DEN AS PAI2DEN,  PAI3.DEN AS PAI3DEN, PROVI.DEN AS PROVIDEN, PROVI2.DEN AS PROVI2DEN, PROVI3.DEN AS PROVI3DEN"



sConsultaUON = sConsultaUON & "FROM UON1 UON1 " & vbCrLf
sConsultaUON = sConsultaUON & "      left join (emp left join pai on emp.pai = pai.cod left join provi on emp.pai = provi.pai and emp.provi = provi.cod) on emp.id = uon1.empresa" & vbCrLf

sConsultaUON = sConsultaUON & "      LEFT JOIN (UON2 UON2 " & vbCrLf
sConsultaUON = sConsultaUON & "                      LEFT JOIN (UON3 UON3 " & vbCrLf
sConsultaUON = sConsultaUON & "                                   left join (emp emp_3 " & vbCrLf
sConsultaUON = sConsultaUON & "                                             left join PAI PAI_3 " & vbCrLf
sConsultaUON = sConsultaUON & "                                                    on emp_3.pai = PAI_3.cod " & vbCrLf
sConsultaUON = sConsultaUON & "                                             left join provi provi_3 " & vbCrLf
sConsultaUON = sConsultaUON & "                                                    on emp_3.pai = provi_3.pai " & vbCrLf
sConsultaUON = sConsultaUON & "                                                   and emp_3.provi = provi_3.cod) " & vbCrLf
sConsultaUON = sConsultaUON & "                                          on uon3.empresa = emp_3.id" & vbCrLf
sConsultaUON = sConsultaUON & ")" & vbCrLf
sConsultaUON = sConsultaUON & "                             ON UON2.UON1=UON3.UON1 " & vbCrLf
sConsultaUON = sConsultaUON & "                            AND UON2.COD=UON3.UON2" & vbCrLf

sConsultaUON = sConsultaUON & "                       left join (emp emp_2 " & vbCrLf
sConsultaUON = sConsultaUON & "                                 left join pai pai_2" & vbCrLf
sConsultaUON = sConsultaUON & "                                        on emp_2.pai = pai_2.cod " & vbCrLf
sConsultaUON = sConsultaUON & "                                 left join provi provi_2" & vbCrLf
sConsultaUON = sConsultaUON & "                                        on emp_2.pai = provi_2.pai " & vbCrLf
sConsultaUON = sConsultaUON & "                                       and emp_2.provi = provi_2.cod) " & vbCrLf
sConsultaUON = sConsultaUON & "                              on uon2.empresa = emp_2.id" & vbCrLf
sConsultaUON = sConsultaUON & ")" & vbCrLf
sConsultaUON = sConsultaUON & "             ON UON1.COD=UON2.UON1" & vbCrLf
sConsultaUON = sConsultaUON & ""



sConsultaPER = "SELECT PER.*, UON1.DEN, UON2.DEN, UON3.DEN, DEP.DEN FROM PER LEFT JOIN UON1 ON PER.UON1=UON1.COD"
sConsultaPER = sConsultaPER & " LEFT JOIN UON2 ON PER.UON1=UON2.UON1 AND PER.UON2=UON2.COD "
sConsultaPER = sConsultaPER & " LEFT JOIN UON3 ON PER.UON1=UON3.UON1 AND PER.UON2=UON3.UON2 AND PER.UON3=UON3.COD"
sConsultaPER = sConsultaPER & " LEFT JOIN DEP ON PER.DEP = DEP.COD"
If Not RDep Then
    If RUO Then
    'Restriccion de unidad
       If IsNull(UON1) Then
            ' La persona pertence al nivel 0
            sConsultaUON = sConsultaUON & " WHERE 1=1"
            sConsultaPER = sConsultaPER & " WHERE 1=1"
            
       Else ' Es del nivel 1 o 2 o 3
            If IsNull(UON2) Then
                'La persona pertenece al nivel 1
                sConsultaUON = sConsultaUON & " WHERE UON1.COD = '" & DblQuote(CStr(UON1)) & "'"
                sConsultaPER = sConsultaPER & " WHERE PER.UON1 = '" & DblQuote(CStr(UON1)) & "'"
            Else
                If IsNull(UON3) Then
                    'La persona pertenece al nivel 2
                     sConsultaUON = sConsultaUON & " WHERE UON1.COD ='" & DblQuote(CStr(UON1)) & "' AND UON2.COD='" & DblQuote(CStr(UON2)) & "'"
                     sConsultaPER = sConsultaPER & " WHERE PER.UON1 ='" & DblQuote(CStr(UON1)) & "' AND PER.UON2='" & DblQuote(CStr(UON2)) & "'"
                Else
                ' La persona pertence al nivel 3, Solo cargar la suya
                     sConsultaUON = sConsultaUON & " WHERE UON1.COD ='" & DblQuote(CStr(UON1)) & "'" & _
                        " AND UON2.COD='" & DblQuote(CStr(UON2)) & "' AND UON3.COD='" & DblQuote(CStr(UON3)) & "'"
                     sConsultaPER = sConsultaPER & " WHERE PER.UON1 ='" & DblQuote(CStr(UON1)) & "'" & _
                        " AND PER.UON2='" & DblQuote(CStr(UON2)) & "' AND PER.UON3='" & DblQuote(CStr(UON3)) & "'"
                End If
            End If
        End If
    Else
    ' No hay restricciones
        sConsultaUON = sConsultaUON & " WHERE 1=1"
        sConsultaPER = sConsultaPER & " WHERE 1=1"
    End If
Else 'Hay restricción de Departamento
    sConsultaUON = "SELECT UON1.COD,UON2.COD ,UON3.COD , UON1_DEP.DEP,UON2_DEP.DEP,UON3_DEP.DEP"
    sConsultaUON = sConsultaUON & ",EMP.NIF, EMP.DEN, EMP.DIR, EMP.CP, EMP.POB,EMP_2.NIF, EMP_2.DEN, EMP_2.DIR, EMP_2.CP, EMP_2.POB,EMP_3.NIF, EMP_3.DEN, EMP_3.DIR, EMP_3.CP, EMP_3.POB "
    sConsultaUON = sConsultaUON & " FROM (UON1 LEFT JOIN UON1_DEP ON UON1.COD = UON1_DEP.UON1 AND UON1_DEP.DEP= '" & DblQuote(CStr(CodDep)) & "'"
    sConsultaUON = sConsultaUON & "      left join (emp left join pai on emp.pai = pai.cod left join provi on emp.pai = provi.pai and emp.provi = provi.cod) on emp.id = uon1.empresa" & vbCrLf
    sConsultaUON = sConsultaUON & ")"
    sConsultaUON = sConsultaUON & " LEFT JOIN ((UON2 LEFT JOIN UON2_DEP ON UON2.UON1 = UON2_DEP.UON1 AND"
    sConsultaUON = sConsultaUON & " UON2.COD=UON2_DEP.UON2 AND UON2_DEP.DEP= '" & DblQuote(CStr(CodDep)) & "')"
    sConsultaUON = sConsultaUON & " LEFT JOIN (UON3 INNER JOIN UON3_DEP ON UON3.UON1 = UON3_DEP.UON1 AND UON3.UON2 = UON3_DEP.UON2 AND UON3.COD=UON3_DEP.UON3"
    sConsultaUON = sConsultaUON & " AND UON3_DEP.DEP= '" & DblQuote(CStr(CodDep)) & "'"
    sConsultaUON = sConsultaUON & "                                   left join (emp emp_3 " & vbCrLf
    sConsultaUON = sConsultaUON & "                                             left join PAI PAI_3 " & vbCrLf
    sConsultaUON = sConsultaUON & "                                                    on emp_3.pai = PAI_3.cod " & vbCrLf
    sConsultaUON = sConsultaUON & "                                             left join provi provi_3 " & vbCrLf
    sConsultaUON = sConsultaUON & "                                                    on emp_3.pai = provi_3.pai " & vbCrLf
    sConsultaUON = sConsultaUON & "                                                   and emp_3.provi = provi_3.cod) " & vbCrLf
    sConsultaUON = sConsultaUON & "                                          on uon3.empresa = emp_3.id" & vbCrLf
            
    sConsultaUON = sConsultaUON & ") ON UON2.UON1=UON3.UON1 AND UON2.COD=UON3.UON2"
    sConsultaUON = sConsultaUON & "                       left join (emp emp_2 " & vbCrLf
    sConsultaUON = sConsultaUON & "                                 left join pai pai_2" & vbCrLf
    sConsultaUON = sConsultaUON & "                                        on emp_2.pai = pai_2.cod " & vbCrLf
    sConsultaUON = sConsultaUON & "                                 left join provi provi_2" & vbCrLf
    sConsultaUON = sConsultaUON & "                                        on emp_2.pai = provi_2.pai " & vbCrLf
    sConsultaUON = sConsultaUON & "                                       and emp_2.provi = provi_2.cod) " & vbCrLf
    sConsultaUON = sConsultaUON & "                              on uon2.empresa = emp_2.id" & vbCrLf
    
    sConsultaUON = sConsultaUON & ") ON UON1.COD=UON2.UON1"
    If RUO Then 'Restriccion de unidad
        If IsNull(UON1) Then 'Es de nivel 0
            sConsultaUON = sConsultaUON & " WHERE (UON1_DEP.DEP ='" & DblQuote(CStr(CodDep)) & "' OR UON2_DEP.DEP='" & DblQuote(CStr(CodDep)) & "' OR UON3_DEP.DEP='" & DblQuote(CStr(CodDep)) & "')"
           
            sConsultaPER = sConsultaPER & " WHERE DEP.COD= '" & DblQuote(CStr(CodDep)) & "'"
            
        Else ' Es del nivel 1 o 2 o 3
            If IsNull(UON2) Then
                'La persona pertenece al nivel 1
                
                sConsultaUON = sConsultaUON & " WHERE (UON2_DEP.DEP='" & DblQuote(CStr(CodDep)) & "' OR UON3_DEP.DEP='" & DblQuote(CStr(CodDep)) & "')"
                sConsultaUON = sConsultaUON & " AND UON1.COD = '" & DblQuote(CStr(UON1)) & "'"
            
                sConsultaPER = sConsultaPER & " WHERE PER.UON1 = '" & DblQuote(CStr(UON1)) & "' AND DEP.COD= '" & DblQuote(CStr(CodDep)) & "'"
            Else
                If IsNull(UON3) Then
                    'La persona pertenece al nivel 2
                                        
                    sConsultaUON = sConsultaUON & " WHERE UON3_DEP.DEP='" & DblQuote(CStr(CodDep)) & "'"
                    sConsultaUON = sConsultaUON & " and UON1.COD ='" & DblQuote(CStr(UON1)) & "' AND UON2.COD='" & DblQuote(CStr(UON2)) & "'"
                    
                    sConsultaPER = sConsultaPER & " WHERE PER.UON1 ='" & DblQuote(CStr(UON1)) & "' AND PER.UON2='" & DblQuote(CStr(UON2)) & "' AND DEP.COD= '" & DblQuote(CStr(CodDep)) & "'"
                Else
                ' La persona pertence al nivel 3, Solo cargar la suya
                     sConsultaUON = sConsultaUON & " WHERE UON1.COD ='" & DblQuote(CStr(UON1)) & "'" & _
                        " AND UON2.COD='" & DblQuote(CStr(UON2)) & "' AND UON3.COD='" & DblQuote(CStr(UON3)) & "'"
                     sConsultaPER = sConsultaPER & " WHERE PER.UON1 ='" & DblQuote(CStr(UON1)) & "'" & _
                        " AND PER.UON2='" & DblQuote(CStr(UON2)) & "' AND PER.UON3='" & DblQuote(CStr(UON3)) & "' AND DEP.COD= '" & DblQuote(CStr(CodDep)) & "'"
                End If
            End If
        End If
    Else
            sConsultaUON = sConsultaUON & " WHERE (UON1_DEP.DEP ='" & DblQuote(CStr(CodDep)) & "' OR UON2_DEP.DEP='" & DblQuote(CStr(CodDep)) & "' OR UON3_DEP.DEP='" & DblQuote(CStr(CodDep)) & "')"
           
            sConsultaPER = sConsultaPER & " WHERE DEP.COD= '" & DblQuote(CStr(CodDep)) & "'"
    End If
End If
    'Si han elegido una UO desde la que empezar
If sUON3 <> "" Then
    sConsultaUON = sConsultaUON & " AND UON1.COD = '" & sUON1 & "' AND UON2.COD = '" & sUON2 & "' AND UON3.COD >= '" & sUON3 & "'"
    sConsultaPER = sConsultaPER & " AND PER.UON1 = '" & sUON1 & "' AND PER.UON2 = '" & sUON2 & "' AND PER.UON3 >= '" & sUON3 & "'"
                        
Else
    If sUON2 <> "" Then
            sConsultaUON = sConsultaUON & " AND UON1.COD = '" & sUON1 & "' AND UON2.COD >= '" & sUON2 & "'"
            sConsultaPER = sConsultaPER & " AND PER.UON1 = '" & sUON1 & "' AND PER.UON2 >= '" & sUON2 & "'"
    Else
            If sUON1 <> "" Then
                sConsultaUON = sConsultaUON & " AND UON1.COD = '" & sUON1 & "'"
                sConsultaPER = sConsultaPER & " AND PER.UON1 = '" & sUON1 & "'"

            End If
    End If
End If

If Compradoras = 1 Then  'Personas compradoras
    sConsultaPER = sConsultaPER & " AND PER.EQP IS NOT NULL"
    
ElseIf Compradoras = 2 Then  'Personas no compradoras
    sConsultaPER = sConsultaPER & " AND PER.EQP IS NULL"
End If

If Not BajaLog Then
    sConsultaPER = sConsultaPER & " AND PER.BAJALOG=0"
    sConsultaUON = sConsultaUON & " AND ISNULL(UON1.BAJALOG,0)<>1 AND ISNULL(UON2.BAJALOG,0)<>1 AND ISNULL(UON3.BAJALOG,0)<>1 "
End If

sConsultaUON = sConsultaUON & " AND 1=1"
        
If OrdenadasPorDen Then
    sConsultaUON = sConsultaUON & " ORDER BY UON1.DEN, UON2.DEN, UON3.DEN"
    sConsultaPER = sConsultaPER & " ORDER BY PER.APE, PER.NOM"
Else
    sConsultaUON = sConsultaUON & " ORDER BY UON1.COD, UON2.COD, UON3.COD"
    sConsultaPER = sConsultaPER & " ORDER BY PER.COD"
End If


If TipoListado = "Estructura" Then
    ConstruirSQLListado = sConsultaUON
Else
    ConstruirSQLListado = sConsultaPER
End If
    
End Function


