VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRSolicitudes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub Listado(oReport As CRAXDRT.Report, Id As String, Descr As String, FecDesde As String, FecHasta As String, Pet As String, Estado As Variant, CriterioOrdenacion As TipoOrdenacionSolicitudes, Optional ByVal ResAsig As Boolean, Optional ByVal bResEqp As Boolean, Optional ByVal Comp As String, Optional ByVal Eqp As String, Optional ByVal bResUO As Boolean, Optional ByVal vUON1 As Variant, Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant)
    Dim sSQLQueryRpt As String
    Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSelection As String
    Dim i As Integer
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    'Obtiene la select
    sSQLQueryRpt = ObtenerSQLListadoSolicitud(Id, Descr, Pet, CriterioOrdenacion, ResAsig, bResEqp, Comp, Eqp, bResUO, vUON1, vUON2, vUON3)

    oReport.SQLQueryString = sSQLQueryRpt
    
    'Obtiene el recordselection
    RecordSelection = "1=1"
    If (FecDesde <> "") And (IsDate(FecDesde)) Then
        RecordSelection = RecordSelection & " AND CDate({INSTANCIA.FEC_ALTA}) >= CDate('" & DblQuote(FecDesde) & "')"
    End If

    If (FecHasta <> "") And (IsDate(FecHasta)) Then
        RecordSelection = RecordSelection & " AND CDate({INSTANCIA.FEC_ALTA}) <= CDate('" & DblQuote(FecHasta) & "')"
    End If
    
    If Not IsMissing(Estado) Then
        If UBound(Estado) >= 1 Then
            If Estado(1) <> "" Then
                RecordSelection = RecordSelection & " AND ("
                For i = 1 To UBound(Estado)
                    RecordSelection = RecordSelection & " {INSTANCIA.ESTADO}=" & Estado(i) & " OR"
                Next i
                RecordSelection = Mid(RecordSelection, 1, Len(RecordSelection) - 3)
                RecordSelection = RecordSelection & " )"
            End If
        End If
    End If
    oReport.RecordSelectionFormula = RecordSelection
End Sub


Private Function ObtenerSQLListadoSolicitud(Id As String, Descr As String, _
                                            Pet As String, CriterioOrdenacion As TipoOrdenacionSolicitudes, _
                                            Optional ByVal ResAsig As Boolean, Optional ByVal bResEqp As Boolean, _
                                            Optional ByVal Comp As String, Optional ByVal Eqp As String, _
                                            Optional ByVal bResUO As Boolean, Optional ByVal vUON1 As Variant, _
                                            Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant)
    Dim sConsulta As String
    
    sConsulta = "SELECT DISTINCT INSTANCIA.*,C1.VALOR_TEXT AS DEN,C2.VALOR_FEC AS FECNEC,PER.NOM ,PER.APE ,PER_1.NOM ,PER_1.APE,PER.UON1,PER.UON2,PER.UON3"
    sConsulta = sConsulta & " FROM INSTANCIA "
    sConsulta = sConsulta & " INNER JOIN PER ON INSTANCIA.PETICIONARIO=PER.COD"
    sConsulta = sConsulta & " INNER JOIN PER PER_1 ON INSTANCIA.COMPRADOR=PER_1.COD"
    sConsulta = sConsulta & " INNER JOIN VERSION_INSTANCIA ON INSTANCIA.ID=VERSION_INSTANCIA.INSTANCIA AND NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA=INSTANCIA.ID)"
    sConsulta = sConsulta & " LEFT JOIN"
    sConsulta = sConsulta & " (SELECT VALOR_TEXT,NUM_VERSION,INSTANCIA FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID AND TIPO_CAMPO_GS=" & TipoCampoSC.DescrBreve & ")"
    sConsulta = sConsulta & " C1 ON VERSION_INSTANCIA.INSTANCIA=C1.INSTANCIA AND VERSION_INSTANCIA.NUM_VERSION=C1.NUM_VERSION"
    sConsulta = sConsulta & " LEFT JOIN"
    sConsulta = sConsulta & " (SELECT VALOR_FEC,NUM_VERSION,INSTANCIA FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID AND TIPO_CAMPO_GS=" & TipoCampoSC.FecNecesidad & ")"
    sConsulta = sConsulta & " C2 ON VERSION_INSTANCIA.INSTANCIA=C2.INSTANCIA AND VERSION_INSTANCIA.NUM_VERSION=C2.NUM_VERSION"
    
    If bResEqp = True Then
        sConsulta = sConsulta & " INNER JOIN COM ON INSTANCIA.COMPRADOR=COM.COD"
    Else
        sConsulta = sConsulta & " LEFT JOIN COM ON INSTANCIA.COMPRADOR=COM.COD"
    End If
    sConsulta = sConsulta & " WHERE 1=1"
    
    If Id <> "" And Id <> "0" Then
        sConsulta = sConsulta & " AND INSTANCIA.ID=" & Id
    End If
        
    If Descr <> "" Then
        sConsulta = sConsulta & " AND C1.VALOR_TEXT LIKE '%" & DblQuote(Descr) & "%'"
    End If
    
    If Pet <> "" Then
        sConsulta = sConsulta & " AND INSTANCIA.PETICIONARIO='" & DblQuote(Pet) & "'"
    End If
    
    If ResAsig = True Then
        sConsulta = sConsulta & " AND INSTANCIA.COMPRADOR='" & DblQuote(Comp) & "'"
    End If
    
    'Si hay restricci�n de equipo carga s�lo las solicitudes asignadas a compradores del equipo
    'del usuario
    If bResEqp = True Then
        sConsulta = sConsulta & " AND COM.EQP='" & DblQuote(Eqp) & "'"
    End If
    
    'si hay restricci�n de Unidad Organizativa carga s�lo las solicitudes pedidas por usuarios
    'de la misma unidad organizativa.
    If bResUO = True Then
        If Not IsMissing(vUON1) And vUON1 <> "" Then
            sConsulta = sConsulta & " AND PER.UON1='" & DblQuote(vUON1) & "'"
        End If
        If Not IsMissing(vUON2) And vUON2 <> "" Then
            sConsulta = sConsulta & " AND PER.UON2='" & DblQuote(vUON2) & "'"
        End If
        If Not IsMissing(vUON3) And vUON3 <> "" Then
            sConsulta = sConsulta & " AND PER.UON3='" & DblQuote(vUON3) & "'"
        End If
    End If
    
    Select Case CriterioOrdenacion
            
        Case TipoOrdenacionSolicitudes.OrdSolicPorfecalta
                        
                sConsulta = sConsulta & " ORDER BY INSTANCIA.FEC_ALTA"
                    
        Case TipoOrdenacionSolicitudes.OrdSolicPorFecNec
                
                sConsulta = sConsulta & " ORDER BY C2.VALOR_FEC"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorId
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.ID"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorDescr
                
                sConsulta = sConsulta & " ORDER BY C1.VALOR_TEXT"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorImporte
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.IMPORTE"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorPet
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.PETICIONARIO"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorUO
                
                sConsulta = sConsulta & " ORDER BY PER.UON1,PER.UON2,PER.UON3"
        
        Case TipoOrdenacionSolicitudes.OrdSolicPorEstado
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.ESTADO"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorComp
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.COMPRADOR"
                
    End Select
    
    ObtenerSQLListadoSolicitud = sConsulta
End Function


Public Sub ListadoDetalle(oReport As CRAXDRT.Report, Id As String, Descr As String, FecDesde As String, FecHasta As String, Pet As String, Estado As Variant, CriterioOrdenacion As TipoOrdenacionSolicitudes, Optional ByVal ResAsig As Boolean, Optional ByVal bResEqp As Boolean, Optional ByVal Comp As String, Optional ByVal Eqp As String, Optional ByVal bResUO As Boolean, Optional ByVal vUON1 As Variant, Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant)
    Dim sSQLQueryRpt As String
    Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSelection As String
    Dim i As Integer
    Dim SubListado As CRAXDRT.Report
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    'Obtiene la select
    sSQLQueryRpt = ObtenerSQLListadoSolicitudDetalle(Id, Descr, Pet, CriterioOrdenacion, ResAsig, bResEqp, Comp, Eqp, bResUO, vUON1, vUON2, vUON3)

    oReport.SQLQueryString = sSQLQueryRpt
    
    'Obtiene el recordselection
    RecordSelection = "1=1"
    If (FecDesde <> "") And (IsDate(FecDesde)) Then
        RecordSelection = RecordSelection & " AND CDate({INSTANCIA.FEC_ALTA}) >= CDate('" & DblQuote(FecDesde) & "')"
    End If

    If (FecHasta <> "") And (IsDate(FecHasta)) Then
        RecordSelection = RecordSelection & " AND CDate({INSTANCIA.FEC_ALTA}) <= CDate('" & DblQuote(FecHasta) & "')"
    End If
    
    If Not IsMissing(Estado) Then
        If UBound(Estado) >= 1 Then
            If Estado(1) <> "" Then
                RecordSelection = RecordSelection & " AND ("
                For i = 1 To UBound(Estado)
                    RecordSelection = RecordSelection & " {INSTANCIA.ESTADO}=" & Estado(i) & " OR"
                Next i
                RecordSelection = Mid(RecordSelection, 1, Len(RecordSelection) - 3)
                RecordSelection = RecordSelection & " )"
            End If
        End If
    End If
    
    Set SubListado = oReport.OpenSubreport("rptAdjuntos")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    Set SubListado = oReport.OpenSubreport("rptProcesos")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    Set SubListado = oReport.OpenSubreport("rptPublicaciones")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    Set SubListado = oReport.OpenSubreport("rptPedidosCat")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    Set SubListado = oReport.OpenSubreport("rptPedidosDir")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    oReport.RecordSelectionFormula = RecordSelection
End Sub


Private Function ObtenerSQLListadoSolicitudDetalle(Id As String, Descr As String, _
                                            Pet As String, CriterioOrdenacion As TipoOrdenacionSolicitudes, _
                                            Optional ByVal ResAsig As Boolean, Optional ByVal bResEqp As Boolean, _
                                            Optional ByVal Comp As String, Optional ByVal Eqp As String, _
                                            Optional ByVal bResUO As Boolean, Optional ByVal vUON1 As Variant, _
                                            Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant)
    Dim sConsulta As String
    
    sConsulta = "SELECT DISTINCT INSTANCIA.*,C1.VALOR_TEXT AS DEN,C2.VALOR_FEC AS FECNEC,PER.NOM ,PER.APE ,PER_1.NOM ,PER_1.APE,PER.UON1,PER.UON2,PER.UON3"
    sConsulta = sConsulta & " FROM INSTANCIA "
    sConsulta = sConsulta & " INNER JOIN PER ON INSTANCIA.PETICIONARIO=PER.COD"
    sConsulta = sConsulta & " INNER JOIN PER PER_1 ON INSTANCIA.COMPRADOR=PER_1.COD"
    sConsulta = sConsulta & " INNER JOIN VERSION_INSTANCIA ON INSTANCIA.ID=VERSION_INSTANCIA.INSTANCIA AND NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM VERSION_INSTANCIA WHERE INSTANCIA=INSTANCIA.ID)"
    sConsulta = sConsulta & " LEFT JOIN"
    sConsulta = sConsulta & " (SELECT VALOR_TEXT,NUM_VERSION,INSTANCIA FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID AND TIPO_CAMPO_GS=" & TipoCampoSC.DescrBreve & ")"
    sConsulta = sConsulta & " C1 ON VERSION_INSTANCIA.INSTANCIA=C1.INSTANCIA AND VERSION_INSTANCIA.NUM_VERSION=C1.NUM_VERSION"
    sConsulta = sConsulta & " LEFT JOIN"
    sConsulta = sConsulta & " (SELECT VALOR_FEC,NUM_VERSION,INSTANCIA FROM COPIA_CAMPO INNER JOIN FORM_CAMPO ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID AND TIPO_CAMPO_GS=" & TipoCampoSC.FecNecesidad & ")"
    sConsulta = sConsulta & " C2 ON VERSION_INSTANCIA.INSTANCIA=C2.INSTANCIA AND VERSION_INSTANCIA.NUM_VERSION=C2.NUM_VERSION"
    
    If bResEqp = True Then
        sConsulta = sConsulta & " INNER JOIN COM ON INSTANCIA.COMPRADOR=COM.COD"
    Else
        sConsulta = sConsulta & " LEFT JOIN COM ON INSTANCIA.COMPRADOR=COM.COD"
    End If
    
    sConsulta = sConsulta & " WHERE 1=1"
    
    If Id <> "" And Id <> "0" Then
        sConsulta = sConsulta & " AND INSTANCIA.ID=" & Id
    End If
        
    If Descr <> "" Then
        sConsulta = sConsulta & " AND C1.VALOR_TEXT LIKE '%" & DblQuote(Descr) & "%'"
    End If
    
    If Pet <> "" Then
        sConsulta = sConsulta & " AND INSTANCIA.PETICIONARIO='" & DblQuote(Pet) & "'"
    End If
    
    If ResAsig = True Then
        sConsulta = sConsulta & " AND INSTANCIA.COMPRADOR='" & DblQuote(Comp) & "'"
    End If
    
    'Si hay restricci�n de equipo carga s�lo las solicitudes asignadas a compradores del equipo
    'del usuario
    If bResEqp = True Then
        sConsulta = sConsulta & " AND COM.EQP='" & DblQuote(Eqp) & "'"
    End If
    
    'si hay restricci�n de Unidad Organizativa carga s�lo las solicitudes pedidas por usuarios
    'de la misma unidad organizativa.
    If bResUO = True Then
        If Not IsMissing(vUON1) And vUON1 <> "" Then
            sConsulta = sConsulta & " AND PER.UON1='" & DblQuote(vUON1) & "'"
        End If
        If Not IsMissing(vUON2) And vUON2 <> "" Then
            sConsulta = sConsulta & " AND PER.UON2='" & DblQuote(vUON2) & "'"
        End If
        If Not IsMissing(vUON3) And vUON3 <> "" Then
            sConsulta = sConsulta & " AND PER.UON3='" & DblQuote(vUON3) & "'"
        End If
    End If
    
    Select Case CriterioOrdenacion
            
        Case TipoOrdenacionSolicitudes.OrdSolicPorfecalta
                        
                sConsulta = sConsulta & " ORDER BY INSTANCIA.FEC_ALTA"
                    
        Case TipoOrdenacionSolicitudes.OrdSolicPorFecNec
                
                sConsulta = sConsulta & " ORDER BY C2.VALOR_FEC"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorId
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.ID"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorDescr
                
                sConsulta = sConsulta & " ORDER BY C1.VALOR_TEXT"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorImporte
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.IMPORTE"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorPet
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.PETICIONARIO"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorUO
                
                sConsulta = sConsulta & " ORDER BY PER.UON1,PER.UON2,PER.UON3"
        
        Case TipoOrdenacionSolicitudes.OrdSolicPorEstado
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.ESTADO"
                
        Case TipoOrdenacionSolicitudes.OrdSolicPorComp
                
                sConsulta = sConsulta & " ORDER BY INSTANCIA.COMPRADOR"
                
    End Select
    
    ObtenerSQLListadoSolicitudDetalle = sConsulta
End Function

