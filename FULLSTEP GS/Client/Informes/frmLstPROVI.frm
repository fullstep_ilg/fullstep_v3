VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmLstPROVI 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de provincias (Opciones)"
   ClientHeight    =   2040
   ClientLeft      =   585
   ClientTop       =   2010
   ClientWidth     =   5610
   Icon            =   "frmLstPROVI.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2040
   ScaleWidth      =   5610
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   5610
      TabIndex        =   8
      Top             =   1665
      Width           =   5610
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4260
         TabIndex        =   6
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1635
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   5595
      _ExtentX        =   9869
      _ExtentY        =   2884
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstPROVI.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstPROVI.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame2 
         Height          =   1035
         Left            =   120
         TabIndex        =   7
         Top             =   420
         Width           =   5375
         Begin VB.OptionButton opPais 
            Caption         =   "Pa�s:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   120
            TabIndex        =   1
            Top             =   540
            Width           =   1080
         End
         Begin VB.OptionButton opTodos 
            Caption         =   "Provincias de todos los pa�ses"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   0
            Top             =   240
            Value           =   -1  'True
            Width           =   3165
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
            Height          =   285
            Left            =   1245
            TabIndex        =   2
            Top             =   540
            Width           =   885
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1561
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
            Height          =   285
            Left            =   2145
            TabIndex        =   3
            Top             =   540
            Width           =   3165
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5583
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Height          =   975
         Left            =   -74880
         TabIndex        =   10
         Top             =   420
         Width           =   5350
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   5
            Top             =   600
            Width           =   2475
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   4
            Top             =   240
            Value           =   -1  'True
            Width           =   2475
         End
      End
   End
End
Attribute VB_Name = "frmLstPROVI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Mensajes As CMensajes
Public ParamInstRPTPATH As String
Public ParamGenRPTPATH As String
Public Report As CRAXDRT.Report
Public GestorIdiomas As CGestorIdiomas
Public GuardarParametrosIns As Boolean
Public Titulo As String  'preview
Public Idioma As String
Public Raiz As CRaiz
Public CargaMaximaCombos As Integer
Public gsPais As String
Public gsPAIDEF As String

Private oPaises As CPaises
Private CargarComboDesde As Boolean
Private RespetarCombo As Boolean

'Multilenguaje

Private sIdiTodasProvi As String
Private sIdiProviDe As String
Private sIdiTitulo As String
Private srIdiTitulo As String
Private srIdiSeleccion As String
Private srIdiPais As String
Private srIdiCodigo As String
Private srIdiDenominacion As String
Private srIdiPag As String
Private srIdiDe As String


Private Sub sdbcPaiCod_Change()
    
    opPais.Value = True
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcPaiDen.Text = ""
        RespetarCombo = False
        
        CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcPaiCod_CloseUp()
    
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    RespetarCombo = False
    
    CargarComboDesde = False
        
End Sub

Private Sub sdbcPaiCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    opPais.Value = True
    
    Screen.MousePointer = vbHourglass
    Set oPaises = Nothing
    Set oPaises = Raiz.Generar_CPaises
    
    sdbcPaiCod.RemoveAll
    
    If CargarComboDesde Then
        oPaises.CargarTodosLosPaisesDesde CargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False
    Else
        oPaises.CargarTodosLosPaises , , , , , False
    End If
    
    Codigos = oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesde And Not oPaises.EOF Then
        sdbcPaiCod.AddItem "..."
    End If

    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPaiCod_InitColumnProps()

    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcPaiCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcPaiCod_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = Raiz.Generar_CPaises
    
    If sdbcPaiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    
    Screen.MousePointer = vbHourglass
    oPaises.CargarTodosLosPaises sdbcPaiCod.Text, , True, , False
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiCod.Text = ""
    Else
        RespetarCombo = True
        sdbcPaiCod.Text = oPaises.Item(1).Cod
        sdbcPaiDen.Text = oPaises.Item(1).Den
        sdbcPaiCod.Columns(0).Value = sdbcPaiCod.Text
        sdbcPaiCod.Columns(1).Value = sdbcPaiDen.Text
        
        RespetarCombo = False
        CargarComboDesde = False
    End If
    
    Set oPaises = Nothing
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcPaiDen_Change()
    
    opPais.Value = True
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcPaiCod.Text = ""
        RespetarCombo = False
        CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcPaiDen_CloseUp()
    
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    RespetarCombo = False
    
    CargarComboDesde = False
        
End Sub

Private Sub sdbcPaiDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    opPais.Value = True
    
    Screen.MousePointer = vbHourglass
    Set oPaises = Nothing
    Set oPaises = Raiz.Generar_CPaises
   
    sdbcPaiDen.RemoveAll
    
    If CargarComboDesde Then
        oPaises.CargarTodosLosPaisesDesde CargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    Codigos = oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesde And Not oPaises.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiDen.Refresh
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcPaiDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiDen.Rows - 1
            bm = sdbcPaiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcPaiDen_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = Raiz.Generar_CPaises
    
    If sdbcPaiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    
    Screen.MousePointer = vbHourglass
    oPaises.CargarTodosLosPaises , sdbcPaiDen.Text, True, , , False
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiDen.Text = ""
    Else
        RespetarCombo = True
        sdbcPaiCod.Text = oPaises.Item(1).Cod
        sdbcPaiDen.Text = oPaises.Item(1).Den
        sdbcPaiDen.Columns(0).Value = sdbcPaiDen.Text
        sdbcPaiDen.Columns(1).Value = sdbcPaiCod.Text
        
        RespetarCombo = False
        CargarComboDesde = False
    End If
    
    Set oPaises = Nothing
    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' Obtencion del listado de provincias
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Boton cmdObtener Tiempo m�ximo: 0,1</remarks>

Private Sub cmdObtener_Click()

    ''' * Objetivo: Obtener un listado de provincias
    
    ' ARRAYS para FORMULA FIELDS, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula
    Dim SelectionText(1 To 2, 1 To 9) As String
    Dim oCRProvincias As New CRProvincias
    
    Dim RepPath As String
    Dim oFos  As FileSystemObject
    Dim i As Integer
    
    If bcrs_Connected = False Then
        Exit Sub
    End If
    
    If ParamInstRPTPATH = "" Then
        If ParamGenRPTPATH = "" Then
            Mensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            ParamInstRPTPATH = ParamGenRPTPATH
            GuardarParametrosIns = True
        End If
    End If
    RepPath = ParamInstRPTPATH & "\rptPROVI.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Mensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
        
    Set Report = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    'DPD -> Incidencia 16192 / 16219
    ConectarReportInterno Report, scrs_Server, scrs_Database, scrs_User, scrs_Password
    
    If sdbcPaiCod = "" Then opTodos.Value = True
        
    
    SelectionText(2, 1) = "SEL"
    If opTodos = True Then
        SelectionText(1, 1) = sIdiTodasProvi
    Else
        SelectionText(1, 1) = sIdiProviDe & " " & sdbcPaiDen.Text
    End If
    
    SelectionText(2, 2) = "txtSELECCION"
    SelectionText(1, 2) = srIdiSeleccion
    SelectionText(2, 3) = "txtTITULO"
    SelectionText(1, 3) = srIdiTitulo
    SelectionText(2, 4) = "txtPAIS"
    SelectionText(1, 4) = srIdiPais
    SelectionText(2, 5) = "txtCODIGO"
    SelectionText(1, 5) = srIdiCodigo
    SelectionText(2, 6) = "txtDENOMINACION"
    SelectionText(1, 6) = srIdiDenominacion
    SelectionText(2, 7) = "txtPAG"
    SelectionText(1, 7) = srIdiPag
    SelectionText(2, 8) = "txtDE"
    SelectionText(1, 8) = srIdiDe
    SelectionText(2, 9) = "IDI"
    SelectionText(1, 9) = Idioma
    
    ' FORMULA FIELDS
    For i = 1 To UBound(SelectionText, 2)
        Report.FormulaFields(crs_FormulaIndex_Interno(Report, SelectionText(2, i))).Text = """" & SelectionText(1, i) & """"
    Next i
    
    Screen.MousePointer = vbHourglass
    oCRProvincias.ListadoProvincias Report, opTodos, opOrdCod, sdbcPaiCod.Text
    If Report Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Me.Hide
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Load()
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If gsPais <> "" Then
        sdbcPaiCod.Text = gsPais
    Else
        If gsPAIDEF <> "" Then
            sdbcPaiCod.Text = gsPAIDEF
            gsPais = gsPAIDEF
            GuardarParametrosIns = True
        End If
    End If
    
    sdbcPaiCod_Validate False
    
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_LSTPROVI, Idioma)
    
    If Not Ador Is Nothing Then
        Caption = Ador(0).Value '1
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        opTodos.Caption = Ador(0).Value
        Ador.MoveNext
        opPais.Caption = Ador(0).Value '5
        Ador.MoveNext
        sdbcPaiCod.Columns(0).Caption = Ador(0).Value
        sdbcPaiDen.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        sdbcPaiCod.Columns(1).Caption = Ador(0).Value
        sdbcPaiDen.Columns(0).Caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.Caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.Caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.Caption = Ador(0).Value '10
        Ador.MoveNext
        sIdiTodasProvi = Ador(0).Value
        Ador.MoveNext
        sIdiProviDe = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value
        Ador.MoveNext
        srIdiTitulo = Ador(0).Value
        Ador.MoveNext
        srIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        srIdiPais = Ador(0).Value
        Ador.MoveNext
        srIdiCodigo = Ador(0).Value
        Ador.MoveNext
        srIdiDenominacion = Ador(0).Value
        Ador.MoveNext
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        Ador.Close
    
    End If


   Set Ador = Nothing



End Sub






