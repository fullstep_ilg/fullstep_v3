VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRProveedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public IdiomaInstalacion As String   'basPublic.gParametrosInstalacion.gIdioma
Public AccesoFSIM As TIPOACCESOFSIM     'gParametrosGenerales.gsAccesoFSIM
Public LOGPREBLOQ As Integer     'gParametrosGenerales.giLOGPREBLOQ

Private oFos As FileSystemObject
''' <summary>Obtener listado de proveedores</summary>
''' <param name="oReport">objeto Report</param>
''' <param optional name="scodProve">Cod Proveedor</param></param>
''' <param optional name="sCodGMN1">Cod GMN1</param>
''' <param optional name="sCodGMN2">Cod GMN2</param>
''' <param optional name="sCodGMN3">Cod GMN3</param>
''' <param optional name="sCodGMN4">Cod GMN4</param>
''' <param optional name="sCodEqp">Cod Equipamiento</param>
''' <param optional name="scodPais">Cod pa�s</param>
''' <param optional name="sCodProvi">Cod provincia</param>
''' <param optional name="sCodMon">cod moneda</param>
''' <param optional name="bOrdCod">Boolean</param>
''' <param optional name="bVerContactos">Boolean</param>
''' <param optional name="bVerHomologacion">Boolean</param>
''' <param optional name="bProvPremium">Boolean</param>
''' <param optional name="bPremiumAct">Boolean</param>
''' <param optional name="bSoloWeb">Boolean</param>
''' <param optional name="bBloq">Boolean</param>
''' <param optional name="bDatosWeb">Boolean</param>
''' <param optional name="bCodErp">Boolean</param>
''' <param optional name="bVerProvRelac">Boolean</param>
''' <param optional name="iBusquedaAvanzada">B�squeda avanzada (proveedores relacionados) -> iBusquedaAvanzada = 0 => Todos los proveedores  /  iBusquedaAvanzada = 1 => Proveedores Primarios  /  iBusquedaAvanzada = 2 => Proveedores Subordinados</param>
''' <param optional name="sTipoRel">Tipo de relaci�n</param>
''' <param optional name="bConTipoRel">Si es con tipo de relaci�n</param>
''' <param optional name="sCodProvePriSub">Cod proveedor (principal o relacionado)</param>
''' <returns></returns>
''' <remarks>Llamada desde: frmLstProve.cmdObtener_Click; Tiempo m�ximo: 1 sec</remarks>
''' <revision>JVS 07/09/2011</revision>
Public Function ListadoProveedores(ByRef ADORs As adodb.Recordset, oReport As CRAXDRT.Report, Optional ByVal scodProve As String, Optional ByVal sCodGMN1 As String, Optional ByVal sCodGMN2 As String, Optional ByVal sCodGMN3 As String, Optional ByVal sCodGMN4 As String, _
                                        Optional ByVal sCodEqp As String, Optional ByVal scodPais As String, Optional ByVal sCodProvi As String, Optional ByVal sCodMon As String, Optional ByVal bOrdCod As Boolean, _
                                        Optional ByVal bVerContactos As Boolean, Optional ByVal bVerHomologacion As Boolean, Optional ByVal bProvPremium As Boolean, Optional ByVal bPremiumAct As Boolean, _
                                        Optional ByVal bSoloWeb As Boolean, Optional ByVal bBloq As Boolean, Optional ByVal bDatosWeb As Boolean, Optional ByVal bCodErp As Boolean, Optional ByVal bVerProvRelac As Boolean, _
                                        Optional ByVal iBusquedaAvanzada As Integer, Optional ByVal sTipoRel As String, Optional ByVal bConTipoRel As Boolean, Optional ByVal sCodProvePriSub As String)
                                        
    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim sRepPath As String
    Dim SubListado As CRAXDRT.Report
    Dim sSQLQueryString As String
    Dim oProveedores As CProveedores
    Dim Ador As adodb.Recordset
            
    oReport.Database.SetDataSource ADORs
                  
    If bVerContactos Then
        Set SubListado = oReport.OpenSubreport("rptCON")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
    End If
    
    '_______________________________________________________________________
    If bVerHomologacion Then
        Set SubListado = oReport.OpenSubreport("rptCAL1")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"
        
        Set SubListado = oReport.OpenSubreport("prtCAL2")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"
        
        Set SubListado = oReport.OpenSubreport("rptCAL3")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"
        
    End If
    
    '_______________________________________________________________________
    If bCodErp Then
        Set SubListado = oReport.OpenSubreport("rptProveErp")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
    End If
    
    If bVerProvRelac Then
        Set SubListado = oReport.OpenSubreport("rptPROREL")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        
        Set SubListado = oReport.OpenSubreport("rptPROPRI")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
    End If
    
    
    Set SubListado = Nothing
    
End Function


''' <summary>
''' Monta la instrucci�n SQL necesaria para obtener el listado de proveedores
''' </summary>
''' <param optional name="scodProve">Cod Proveedor</param></param>
''' <param optional name="sCodGMN1">Cod GMN1</param>
''' <param optional name="sCodGMN2">Cod GMN2</param>
''' <param optional name="sCodGMN3">Cod GMN3</param>
''' <param optional name="sCodGMN4">Cod GMN4</param>
''' <param optional name="sCodEqp">Cod Equipamiento</param>
''' <param optional name="scodPais">Cod pa�s</param>
''' <param optional name="sCodProvi">Cod provincia</param>
''' <param optional name="sCodMon">cod moenda</param>
''' <param optional name="bOrdCod">Boolean</param>
''' <param optional name="bProvPremium">Boolean</param>
''' <param optional name="bPremiumAct">Boolean</param>
''' <param optional name="bSoloWeb">Boolean</param>
''' <param optional name="bBloq">Boolean</param>
''' <param optional name="bDatosWeb">Boolean</param>
''' <param optional name="iBusquedaAvanzada">B�squeda avanzada (proveedores relacionados) -> iBusquedaAvanzada = 0 => Todos los proveedores  /  iBusquedaAvanzada = 1 => Proveedores Primarios  /  iBusquedaAvanzada = 2 => Proveedores Subordinados</param>
''' <param optional name="sTipoRel">Tipo de relaci�n</param>
''' <param optional name="bConTipoRel">Con Tipo de relaci�n (s/n)</param>
''' <param optional name="sCodProvePriSub">Cod proveedor relacionado (principal o subordinado)</param>
''' <remarks>Llamada desde: CRProveedores.ListadoProveedores; Tiempo m�ximo: Puede superar los 2 segundos en funcion de la camtidad de datos del proveedor</remarks>
''' <revision>JVS 30/08/2011</revision>
Public Function ObtenerSQLProveedores(Optional ByVal scodProve As String, Optional ByVal sCodGMN1 As String, Optional ByVal sCodGMN2 As String, Optional ByVal sCodGMN3 As String, Optional ByVal sCodGMN4 As String, _
                                        Optional ByVal sCodEqp As String, Optional ByVal scodPais As String, Optional ByVal sCodProvi As String, Optional ByVal sCodMon As String, _
                                        Optional ByVal bOrdCod As Boolean, Optional ByVal bProvPremium As Boolean, Optional ByVal bPremiumAct As Boolean, Optional ByVal bSoloWeb As Boolean, _
                                        Optional ByVal bBloq As Boolean, Optional ByVal bDatosWeb As Boolean, _
                                        Optional ByVal iBusquedaAvanzada As Integer, Optional ByVal sTipoRel As String, Optional ByVal bConTipoRel As Boolean, Optional ByVal sCodProvePriSub As String)
Dim sConsulta As String
Dim iBloqueo As Integer
 
    sConsulta = "SELECT  PROVE.COD,PROVE.DEN AS DEN_PROVE,PROVE.DIR, PROVE.CP, PROVE.POB"
    sConsulta = sConsulta & ",CASE WHEN PROVE.VAL1 IS NULL THEN 0 ELSE PROVE.VAL1 END AS VAL1,CASE WHEN PROVE.VAL2 IS NULL THEN 0 ELSE PROVE.VAL2 END AS VAL2,CASE WHEN PROVE.VAL3 IS NULL THEN 0 ELSE PROVE.VAL3 END AS VAL3 "
    sConsulta = sConsulta & ",CASE WHEN PROVE.CAL1 IS NULL THEN '' ELSE PROVE.CAL1 END AS CAL1,CASE WHEN PROVE.CAL2 IS NULL THEN '' ELSE PROVE.CAL2 END AS CAL2,CASE WHEN PROVE.CAL3 IS NULL THEN '' ELSE PROVE.CAL3 END AS CAL3"
    sConsulta = sConsulta & ",CASE WHEN PROVE.TIPOCAL1 IS NULL THEN 0 ELSE PROVE.TIPOCAL1 END AS TIPOCAL1,CASE WHEN PROVE.TIPOCAL2 IS NULL THEN 0 ELSE PROVE.TIPOCAL2 END AS TIPOCAL2,CASE WHEN PROVE.TIPOCAL3 IS NULL THEN 0 ELSE PROVE.TIPOCAL3 END AS TIPOCAL3"
    sConsulta = sConsulta & ",PROVE.OBS,PROVE.NIF,PROVE.FSP_COD, PROVE.PREMIUM, PROVE.ACTIVO,PROVE.URLPROVE,PROVI_DEN.DEN AS PROVI_DEN,PAI_DEN.DEN AS PAI_DEN,MON_DEN.DEN MON_DEN,PROVE_ESP.NOM AS PROVE_ESP_NOM"
    sConsulta = sConsulta & ", PROVE_ESP.ID AS PROVE_ESP_ID,PROVE_ESP.COM AS PROVE_ESP_COM, USU.COD AS USU_COD, USU.BLOQ AS USU_BLOQ"
    sConsulta = sConsulta & ",VIA_PAG_DEN.DEN AS VIA_PAGDEN "
    sConsulta = sConsulta & ",PAG_DEN.DEN AS PAGDEN"
    If (AccesoFSIM = AccesoFSIM) Then
        sConsulta = sConsulta & ", PROVE.AUTOFACTURA, PROVE.IMPUESTOSFAC"
    Else
        sConsulta = sConsulta & ",0 AUTOFACTURA, 0 IMPUESTOSFAC"
    End If
    sConsulta = sConsulta & " FROM PROVE with(nolock) "
    sConsulta = sConsulta & " LEFT JOIN PROVI_DEN with(nolock) ON PROVI_DEN.PAI=PROVE.PAI AND PROVI_DEN.PROVI = PROVE.PROVI AND PROVI_DEN.IDIOMA='" & IdiomaInstalacion & "'"
    sConsulta = sConsulta & " LEFT JOIN PAI_DEN with(nolock) ON PAI_DEN.PAI=PROVE.PAI AND PAI_DEN.IDIOMA='" & IdiomaInstalacion & "' LEFT JOIN MON_DEN with(nolock) ON MON_DEN.MON = PROVE.MON AND MON_DEN.IDIOMA='" & IdiomaInstalacion & "'"
    sConsulta = sConsulta & " LEFT JOIN PAG_DEN with(nolock) ON PAG_DEN.PAG=PROVE.PAG AND PAG_DEN.IDIOMA='" & IdiomaInstalacion & "'"
    sConsulta = sConsulta & " LEFT JOIN VIA_PAG_DEN with(nolock) ON VIA_PAG_DEN.VIA_PAG=PROVE.VIA_PAG AND VIA_PAG_DEN.IDIOMA='" & IdiomaInstalacion & "'"
    
    If bSoloWeb Then
        sConsulta = sConsulta & " INNER JOIN USU WITH(NOLOCK) ON PROVE.COD=USU.PROVE"
    Else
        sConsulta = sConsulta & " LEFT JOIN USU WITH(NOLOCK) ON PROVE.COD=USU.PROVE"
    End If
    
    sConsulta = sConsulta & " LEFT JOIN PROVE_ESP WITH(NOLOCK) ON PROVE.COD=PROVE_ESP.PROVE"
    
    If iBusquedaAvanzada = 1 Then
        sConsulta = sConsulta & " LEFT JOIN PROVE_REL PR WITH(NOLOCK) ON PR.PROVEP = PROVE.COD"
    ElseIf iBusquedaAvanzada = 2 Then
        sConsulta = sConsulta & " LEFT JOIN PROVE_REL PR WITH(NOLOCK) ON PR.PROVES = PROVE.COD"
    End If
    
    If scodProve <> "" Then
        sConsulta = sConsulta & " WHERE PROVE.COD = '" & DblQuote(scodProve) & "' "
    Else
        If sCodEqp = "" Then
            If sCodGMN1 = "" And sCodGMN2 = "" And sCodGMN3 = "" And sCodGMN4 = "" Then
                sConsulta = sConsulta & " WHERE 1=1"
            Else
                If sCodGMN4 <> "" Then
                    sConsulta = sConsulta & " INNER JOIN PROVE_GMN4 WITH(NOLOCK) ON PROVE_GMN4.PROVE=PROVE.COD AND PROVE_GMN4.GMN1='" & DblQuote(sCodGMN1) & "' AND PROVE_GMN4.GMN2='" & DblQuote(sCodGMN2) & "' AND PROVE_GMN4.GMN3='" & DblQuote(sCodGMN3) & "' AND PROVE_GMN4.GMN4='" & DblQuote(sCodGMN4) & "'"
                Else
                    If sCodGMN3 <> "" Then
                        sConsulta = sConsulta & " INNER JOIN PROVE_GMN4 WITH(NOLOCK) ON PROVE_GMN4.PROVE=PROVE.COD AND PROVE_GMN4.GMN1='" & DblQuote(sCodGMN1) & "' AND PROVE_GMN4.GMN2='" & DblQuote(sCodGMN2) & "' AND PROVE_GMN4.GMN3='" & DblQuote(sCodGMN3) & "'"
                    Else
                        If sCodGMN2 <> "" Then
                            sConsulta = sConsulta & " INNER JOIN PROVE_GMN4 WITH(NOLOCK) ON PROVE_GMN4.PROVE=PROVE.COD AND  PROVE_GMN4.GMN1='" & DblQuote(sCodGMN1) & "' AND PROVE_GMN4.GMN2='" & DblQuote(sCodGMN2) & "' AND PROVE_GMN4.PROVE=PROVE.COD"
                        Else
                            sConsulta = sConsulta & " INNER JOIN PROVE_GMN4 WITH(NOLOCK) ON PROVE_GMN4.PROVE=PROVE.COD AND  PROVE_GMN4.GMN1='" & DblQuote(sCodGMN1) & "'" ' AND PROVE_GMN4.PROVE=PROVE.COD"
                        End If
                    End If
                End If
                
            End If
        Else
             If sCodGMN1 = "" And sCodGMN2 = "" And sCodGMN3 = "" And sCodGMN4 = "" Then
                sConsulta = sConsulta & " INNER JOIN PROVE_EQP with(nolock) ON PROVE.COD = PROVE_EQP.PROVE WHERE PROVE_EQP.EQP='" & DblQuote(sCodEqp) & "'"
            Else
                If sCodGMN4 <> "" Then
                    sConsulta = sConsulta & " INNER JOIN PROVE_EQP with(nolock) ON PROVE.COD = PROVE_EQP.PROVE, PROVE_GMN4 WHERE PROVE_GMN4.GMN1='" & DblQuote(sCodGMN1) & "' AND PROVE_GMN4.GMN2='" & DblQuote(sCodGMN2) & "' AND PROVE_GMN4.GMN3='" & DblQuote(sCodGMN3) & "' AND PROVE_GMN4.GMN4='" & DblQuote(sCodGMN4) & "' AND PROVE_GMN4.PROVE=PROVE.COD AND PROVE_EQP.EQP='" & DblQuote(sCodEqp) & "'"
               Else
                    If sCodGMN3 <> "" Then
                        sConsulta = sConsulta & " INNER JOIN PROVE_EQP with(nolock) ON PROVE.COD = PROVE_EQP.PROVE,PROVE_GMN3 WHERE PROVE_GMN3.GMN1='" & DblQuote(sCodGMN1) & "' AND PROVE_GMN3.GMN2='" & DblQuote(sCodGMN2) & "' AND PROVE_GMN3.GMN3='" & DblQuote(sCodGMN3) & "' AND PROVE_GMN3.PROVE=PROVE.COD AND PROVE_EQP.EQP='" & DblQuote(sCodEqp) & "'"
                    Else
                        If sCodGMN2 <> "" Then
                            sConsulta = sConsulta & " INNER JOIN PROVE_EQP with(nolock) ON PROVE.COD = PROVE_EQP.PROVE,PROVE_GMN2 WHERE PROVE_GMN2.GMN1='" & DblQuote(sCodGMN1) & "' AND PROVE_GMN2.GMN2='" & DblQuote(sCodGMN2) & "' AND PROVE_GMN2.PROVE=PROVE.COD AND PROVE_EQP.EQP='" & DblQuote(sCodEqp) & "'"
                        Else
                            sConsulta = sConsulta & " INNER JOIN PROVE_EQP with(nolock) ON PROVE.COD = PROVE_EQP.PROVE,PROVE_GMN1 WHERE PROVE_GMN1.GMN1='" & DblQuote(sCodGMN1) & "' AND PROVE_GMN1.PROVE=PROVE.COD AND PROVE_EQP.EQP='" & DblQuote(sCodEqp) & "'"
                        End If
                    End If
                End If
            End If
        End If
        If scodPais <> "" Then
            sConsulta = sConsulta & " AND PAI_DEN.PAI = '" & DblQuote(scodPais) & "'"
        End If
        If sCodProvi <> "" Then
            sConsulta = sConsulta & " AND PROVI_DEN.PAI = '" & DblQuote(scodPais) & "' AND PROVI_DEN.PROVI = '" & DblQuote(sCodProvi) & "'"
        End If
        If sCodMon <> "" Then
            sConsulta = sConsulta & " AND MON_DEN.MON = '" & DblQuote(sCodMon) & "'"
        End If
        
        'Premium
        If bProvPremium Then
            sConsulta = sConsulta & " AND PROVE.PREMIUM = 1"
            If bPremiumAct Then
                sConsulta = sConsulta & " AND PROVE.ACTIVO = 1"
            End If
        End If
        
        If bBloq Then
            iBloqueo = LOGPREBLOQ
            sConsulta = sConsulta & "    AND USU.bBloq >= " & iBloqueo
        End If
        
        'Proveedores relacionados
        If iBusquedaAvanzada = 1 Then
            If sTipoRel <> "" And bConTipoRel Then
                sConsulta = sConsulta & " AND PR.PROVETREL = " & sTipoRel
            ElseIf sTipoRel <> "" And Not bConTipoRel Then
                sConsulta = sConsulta & " AND PR.PROVETREL <> " & sTipoRel
            End If
            If sCodProvePriSub <> "" Then
                sConsulta = sConsulta & " AND PR.PROVES = '" & DblQuote(sCodProvePriSub) & "'"
            End If
        ElseIf iBusquedaAvanzada = 2 Then
            If sTipoRel <> "" Then
                sConsulta = sConsulta & " AND PR.PROVETREL = " & sTipoRel
            End If
            If sCodProvePriSub <> "" Then
                sConsulta = sConsulta & " AND PR.PROVEP = '" & DblQuote(sCodProvePriSub) & "'"
            End If
        End If
    
        'orden
        If bOrdCod Then
           sConsulta = sConsulta & " ORDER BY PROVE.COD"
        Else
           sConsulta = sConsulta & " ORDER BY PROVE.DEN"
        End If
   End If
   ObtenerSQLProveedores = sConsulta
        
End Function

