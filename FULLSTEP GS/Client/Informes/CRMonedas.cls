VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRMonedas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public IdiomaInstalacion As String   'basPublic.gParametrosInstalacion.gIdioma

Public Sub Listado(oReport As CRAXDRT.Report, IncluirEqNoAct As Boolean, OrdenarEqNoAct As Boolean, OrdenarPorDen As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim RecordSelectionFormula As String
    Dim RecordSortFields() As String
    Dim BaseOrden As Integer
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
                                                            
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    RecordSelectionFormula = ""
    If IncluirEqNoAct Then
        RecordSelectionFormula = "1=1"
    Else
        RecordSelectionFormula = "{MON.EQ_ACT}=1"
    End If
    
    If OrdenarEqNoAct Then
        ReDim RecordSortFields(2)
        RecordSortFields(1) = "+{MON.EQ_ACT}"
        BaseOrden = 2
    Else
        ReDim RecordSortFields(1)
        BaseOrden = 1
    End If
    
    If OrdenarPorDen = False Then
        RecordSortFields(BaseOrden) = "+{MON.COD}"
    Else
        RecordSortFields(BaseOrden) = "+{MON.DEN_" & IdiomaInstalacion & "}"
    End If
    
    oReport.RecordSelectionFormula = RecordSelectionFormula
    
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
        
    For i = 1 To UBound(RecordSortFields)
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(RecordSortFields(i)))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(RecordSortFields(i)), crs_SortField_Interno(RecordSortFields(i)))), crs_SortDirection_Interno(RecordSortFields(i))
    Next i
    
    
End Sub

