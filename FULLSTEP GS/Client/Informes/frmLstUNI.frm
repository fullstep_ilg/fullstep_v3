VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstUNI 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de unidades (opciones)+"
   ClientHeight    =   2040
   ClientLeft      =   705
   ClientTop       =   2610
   ClientWidth     =   4815
   Icon            =   "frmLstUNI.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2040
   ScaleWidth      =   4815
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   4815
      TabIndex        =   3
      Top             =   1665
      Width           =   4815
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   2
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1635
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   2884
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Orden+"
      TabPicture(0)   =   "frmLstUNI.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   975
         Left            =   120
         TabIndex        =   5
         Top             =   420
         Width           =   4575
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n+"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   1
            Top             =   600
            Width           =   1515
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo+"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   0
            Top             =   240
            Value           =   -1  'True
            Width           =   1515
         End
      End
   End
End
Attribute VB_Name = "frmLstUNI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Idioma As String
Public GestorIdiomas As CGestorIdiomas
Public Mensajes As CMensajes
Public GestorParametros As CGestorParametros
Public ParamInstRPTPATH As String
Public ParamGenRPTPATH As String
Public GuardarParametrosIns As Boolean
Public Report As Object
Public Titulo As String  'preview

Private FormulaRpt(1 To 2, 1 To 6) As String      'formulas textos RPT
Private m_oIdiomas As CIdiomas

''' <summary>
''' Carga las cadenas de idiomas
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_LSTUNI, Idioma)
    
    If Not Ador Is Nothing Then
        cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.Caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.Caption = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        Titulo = Ador(0).Value
        FormulaRpt(1, 1) = Titulo
        Ador.MoveNext
        ' FORMULAS RPT
        For i = 2 To 4
            FormulaRpt(1, i) = Ador(0).Value
            Ador.MoveNext
        Next
        FormulaRpt(1, 5) = Ador(0).Value
        FormulaRpt(1, 6) = Idioma
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

''' <summary>Obtencion de listado de unidades</summary>
''' <returns></returns>
''' <remarks>Llamada desde: Boton cmdObtener Tiempo m�ximo: 0,1</remarks>
Private Sub cmdObtener_Click()
    Dim RecordSortFields(1 To 1) As String
    Dim oCRUnidades As CRUnidades
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim i As Integer
    Dim j As Integer
    Dim oIdi As CIdioma
       
    If Not bcrs_Connected Then Exit Sub
    
    If ParamInstRPTPATH = "" Then
        If ParamGenRPTPATH = "" Then
            Mensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            ParamInstRPTPATH = ParamGenRPTPATH
            GuardarParametrosIns = True
        End If
    End If
    RepPath = ParamInstRPTPATH & "\rptUNI.rpt"
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Mensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Set Report = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    'DPD -> Incidencia 16192 / 16219
    ConectarReportInterno Report, scrs_Server, scrs_Database, scrs_User, scrs_Password
    Set oCRUnidades = New CRUnidades
    
    Screen.MousePointer = vbHourglass
    
        ' FORMULAS TEXTO RPT
    FormulaRpt(2, 1) = "txtTITULO"
    FormulaRpt(2, 2) = "txtPAG"
    FormulaRpt(2, 3) = "txtDE"
    FormulaRpt(2, 4) = "txtCODIGO"
    j = 5
    For Each oIdi In m_oIdiomas
        If oIdi.Cod = Idioma Then
        FormulaRpt(1, j) = FormulaRpt(1, j) & " (" & oIdi.Den & ")"
        FormulaRpt(2, j) = "txtDENOM" '& oIdi.Cod
        j = j + 1
        End If
    Next
    FormulaRpt(2, 6) = "IDI"
    For i = 1 To UBound(FormulaRpt, 2)
        Report.FormulaFields(crs_FormulaIndex_Interno(Report, FormulaRpt(2, i))).Text = """" & FormulaRpt(1, i) & """"
    Next i
    
    oCRUnidades.Listado Report, opOrdCod
    
    Screen.MousePointer = vbNormal
    If Report Is Nothing Then Exit Sub
        
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.Width = 4905
    Me.Height = 2415
    
    Set m_oIdiomas = GestorParametros.DevolverIdiomas(False, False, True)
    CargarRecursos
End Sub
