VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public IdiomaInstalacion As String   'basPublic.gParametrosInstalacion.gIdioma

Public Sub ListadoCONFGEN(oReport As CRAXDRT.Report)
    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim FormulaST As String
    Dim FormulaSTDEN As String

    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
                                                            
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

End Sub

Public Sub ListadoLISPERMant(oReport As CRAXDRT.Report, OrdenarPorDen As Boolean)
    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim sCampoOrden As String
    Dim RecordSortFields() As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    If OrdenarPorDen Then
        sCampoOrden = "DEN}"
    Else
        sCampoOrden = "RPT}"
    End If
    
    ReDim RecordSortFields(1 To 1)
    RecordSortFields(1) = "+{LISPER." & sCampoOrden
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
        
    For i = 1 To UBound(RecordSortFields)
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(RecordSortFields(i)))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(RecordSortFields(i)), crs_SortField_Interno(RecordSortFields(i)))), crs_SortDirection_Interno(RecordSortFields(i))
    Next i
End Sub

''' <summary>
''' Listado de monedas
''' </summary>
''' <param name="oReport">Report </param>
''' <param name="Origen">Desde donde se le llama </param>
''' <param name="OrdenarPorDen">Ordenacion por denominacion</param>
''' <param name="Idioma">idioma en el que se saca el reporte </param>
''' <returns></returns>
''' <remarks>Llamada desde: Varias Partes (Listado de Vias de pago, Formas de pago..) Tiempo m�ximo: 0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Public Sub ListadosParGen(oReport As CRAXDRT.Report, Origen As String, OrdenarPorDen As Boolean, Optional ByVal idioma As String)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim sCampoOrden As String
    Dim RecordSortFields() As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    
    If OrdenarPorDen Then
        sCampoOrden = "DEN}"
    Else
        sCampoOrden = "COD}"
    End If
    Select Case Origen
        
        Case "frmPARPag"
            ReDim RecordSortFields(1 To 1)
            If OrdenarPorDen Then
                RecordSortFields(1) = "+{PAG_DEN.DEN}"
            Else
                RecordSortFields(1) = "+{PAG." & sCampoOrden
            End If
        
        Case "frmPARRoles"
            ReDim RecordSortFields(1 To 1)
            If OrdenarPorDen Then
                RecordSortFields(1) = "+{ROL_IDIOMA." & sCampoOrden
            Else
                RecordSortFields(1) = "+{ROL." & sCampoOrden
            End If
            oReport.RecordSelectionFormula = "{ROL_IDIOMA.IDIOMA}='" & idioma & "'"
        
        Case "frmPARHom"
            ReDim RecordSortFields(1 To 2)
            RecordSortFields(1) = "+{CAL.NUM}"
            If OrdenarPorDen Then
                RecordSortFields(2) = "+{CAL_IDIOMA.DEN}"
            Else
                RecordSortFields(2) = "+{CAL.COD}"
            End If
            
            oReport.SQLQueryString = "SELECT CAL.COD , CAL_IDIOMA.DEN, CAL.INF, CAL.SUP, CAL.NUM FROM CAL CAL, PARGEN_LIT PARGEN_LIT1, PARGEN_LIT PARGEN_LIT2, PARGEN_LIT PARGEN_LIT3, PARGEN_GEST PARGEN_GEST, CAL_IDIOMA CAL_IDIOMA Where CAL.COD= CAL_IDIOMA.COD AND CAL.NUM = CAL_IDIOMA.NUM AND PARGEN_LIT1.IDI='" & idioma & "' AND PARGEN_LIT1.ID=16 AND PARGEN_LIT2.IDI='" & idioma & "' AND PARGEN_LIT2.ID=17 AND PARGEN_LIT3.IDI='" & idioma & "' AND PARGEN_LIT3.ID=18 AND CAL_IDIOMA.IDIOMA = '" & idioma & "'"
            
        Case "frmPAROfeEst"
            ReDim RecordSortFields(1 To 1)
            If OrdenarPorDen Then
                RecordSortFields(1) = "+{OFEEST_IDIOMA." & sCampoOrden
            Else
                RecordSortFields(1) = "+{OFEEST." & sCampoOrden
            End If
            oReport.RecordSelectionFormula = "{OFEEST_IDIOMA.IDIOMA}='" & IdiomaInstalacion & "'"
            
        Case "frmPARFirmas"
            ReDim RecordSortFields(1 To 1)
            RecordSortFields(1) = "+{FIRMAS." & sCampoOrden
            
        Case "frmPARAsis"
            If OrdenarPorDen Then
                ReDim RecordSortFields(1 To 2)
                RecordSortFields(1) = "+{PER.APE}"
                RecordSortFields(2) = "+{PER.NOM}"
            Else
                ReDim RecordSortFields(1 To 1)
                RecordSortFields(1) = "+{FIJ.PER}"
            End If
        Case "frmPARViaPag"
            ReDim RecordSortFields(1 To 1)
            If OrdenarPorDen Then
                RecordSortFields(1) = "+{VIA_PAG_DEN.DEN}"
            Else
                RecordSortFields(1) = "+{VIA_PAG." & sCampoOrden
            End If
        Case "frmPARTipoRelac"
            ReDim RecordSortFields(1 To 1)
            If OrdenarPorDen Then
                RecordSortFields(1) = "+{TREL_IDI.DEN}"
            Else
                RecordSortFields(1) = "+{TREL." & sCampoOrden
            End If
    End Select
    
                                                            
    
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
        
    For i = 1 To UBound(RecordSortFields)
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(RecordSortFields(i)))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(RecordSortFields(i)), crs_SortField_Interno(RecordSortFields(i)))), crs_SortDirection_Interno(RecordSortFields(i))
    Next i
    
    
End Sub

