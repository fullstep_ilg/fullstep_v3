VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRPresPorProy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public DEN_UON0 As String    'gParametrosGenerales.gsDEN_UON0
Public DEN_UON1 As String    'gParametrosGenerales.gsDEN_UON1
Public DEN_UON2 As String    'gParametrosGenerales.gsDEN_UON2
Public DEN_UON3 As String    'gParametrosGenerales.gsDEN_UON3

Public Sub Listado(oReport As CRAXDRT.Report, ByVal Nivel As String, ByVal Anio As Integer, ByVal Proy1 As String, ByVal Proy2 As String, ByVal Proy3 As String, ByVal Proy4 As String, ByVal OrdDen As Boolean, ByVal Ordcod As Boolean, ByVal OrdPres As Boolean, ByVal OrdObj As Boolean, ByVal vUON1 As Variant, ByVal vUON2 As Variant, vUON3 As Variant, ByVal sUODescripcion As String, ByVal bBajaLog As Boolean)
    Dim sSQLQueryString As String
    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim SubListado As CRAXDRT.Report
    Dim sUON1 As String
    Dim sUON2 As String
    Dim sUON3 As String
    Dim sUN_DEN As String
    Dim sUO_CODS As String
    Dim sUO_DEN As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    sUON1 = NullToStr(vUON1)
    sUON2 = NullToStr(vUON2)
    sUON3 = NullToStr(vUON3)
    sUO_CODS = ""
    sUO_DEN = ""
    If sUON1 <> "" Then
        sUN_DEN = DEN_UON1
        sUO_CODS = sUO_CODS & sUON1
        sUO_DEN = sUODescripcion
        If sUON2 <> "" Then
            sUN_DEN = DEN_UON2
            sUO_CODS = sUO_CODS & " - " & sUON2
            If sUON3 <> "" Then
                sUN_DEN = DEN_UON3
                sUO_CODS = sUO_CODS & " - " & sUON3
            End If
        End If
    Else
        sUN_DEN = DEN_UON0
    End If
    Select Case Nivel
        Case "1"
            Set SubListado = oReport.OpenSubreport("prtPROY1")
        Case "2"
            Set SubListado = oReport.OpenSubreport("rptPROY2")
        Case "3"
            Set SubListado = oReport.OpenSubreport("rptPROY3")
        Case "4"
            Set SubListado = oReport.OpenSubreport("rptPROY4")
    End Select
    
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    oReport.FormulaFields.Item(14).Text = """" & sUN_DEN & """"
    oReport.FormulaFields.Item(15).Text = """" & sUO_CODS & """"
    oReport.FormulaFields.Item(16).Text = """" & sUO_DEN & """"
    sSQLQueryString = ConstruirSQL(Nivel, Anio, Proy1, Proy2, Proy3, Proy4, OrdDen, Ordcod, OrdPres, OrdObj, vUON1, vUON2, vUON3, bBajaLog)
    SubListado.SQLQueryString = sSQLQueryString
    
    Set SubListado = Nothing
    
End Sub

Private Function ConstruirSQL(Nivel As String, Anio As Integer, Proy1 As String, Proy2 As String, Proy3 As String, Proy4 As String, OrdDen As Boolean, Ordcod As Boolean, OrdPres As Boolean, OrdObj As Boolean, vUON1 As Variant, vUON2 As Variant, vUON3 As Variant, bBajaLog As Boolean) As String
    Dim sSQLString As String
    Dim sWhereUON As String
    Dim sUON1 As String
    Dim sUON2 As String
    Dim sUON3 As String
    'Dim snivel As String
    'CONSTRUIR LA SQL QUERY A PASAR A Los SUBREPORT
    ' EN FUNCION DE sNivel (nivel de desglose del listado)


    sWhereUON = HacerParteWhereUON(vUON1, vUON2, vUON3)
    
    Select Case Nivel
        Case "1"
            sSQLString = "SELECT PRES1_NIV1.COD , PRES1_NIV1.DEN , PRES1_NIV1.IMP, PRES1_NIV1.OBJ "
            sSQLString = sSQLString & " FROM PRES1_NIV1 "
        Case "2"
            sSQLString = "SELECT PRES1_NIV1.COD , PRES1_NIV1.DEN , PRES1_NIV1.IMP, PRES1_NIV1.OBJ,"
            sSQLString = sSQLString & " PRES1_NIV2.COD, PRES1_NIV2.DEN, PRES1_NIV2.IMP, PRES1_NIV2.OBJ FROM PRES1_NIV1"
            sSQLString = sSQLString & " LEFT JOIN PRES1_NIV2 ON PRES1_NIV2.ANYO = PRES1_NIV1.ANYO AND PRES1_NIV2.PRES1 = PRES1_NIV1.COD" & HacerParteJoinUON(vUON1, vUON2, vUON3, "PRES1_NIV1", "PRES1_NIV2")
            If Not bBajaLog Then sSQLString = sSQLString & " AND PRES1_NIV2.BAJALOG = 0"
        Case "3"
            sSQLString = "SELECT PRES1_NIV1.COD , PRES1_NIV1.DEN , PRES1_NIV1.IMP, PRES1_NIV1.OBJ, "
            sSQLString = sSQLString & " PRES1_NIV2.COD, PRES1_NIV2.DEN, PRES1_NIV2.IMP, PRES1_NIV2.OBJ, "
            sSQLString = sSQLString & " PRES1_NIV3.COD, PRES1_NIV3.DEN, PRES1_NIV3.IMP, PRES1_NIV3.OBJ "
            sSQLString = sSQLString & " FROM PRES1_NIV1"
            sSQLString = sSQLString & " LEFT JOIN PRES1_NIV2 ON PRES1_NIV2.ANYO = PRES1_NIV1.ANYO AND PRES1_NIV2.PRES1 = PRES1_NIV1.COD  AND PRES1_NIV2.PRES1 = PRES1_NIV1.COD" & HacerParteJoinUON(vUON1, vUON2, vUON3, "PRES1_NIV1", "PRES1_NIV2")
            If Not bBajaLog Then sSQLString = sSQLString & " AND PRES1_NIV2.BAJALOG = 0"
            sSQLString = sSQLString & " LEFT JOIN PRES1_NIV3 ON PRES1_NIV3.ANYO = PRES1_NIV1.ANYO AND PRES1_NIV3.PRES1 = PRES1_NIV1.COD AND PRES1_NIV3.PRES2 = PRES1_NIV2.COD" & HacerParteJoinUON(vUON1, vUON2, vUON3, "PRES1_NIV1", "PRES1_NIV3")
            If Not bBajaLog Then sSQLString = sSQLString & " AND PRES1_NIV3.BAJALOG = 0"
        Case "4"
            sSQLString = "SELECT PRES1_NIV1.COD , PRES1_NIV1.DEN , PRES1_NIV1.IMP, PRES1_NIV1.OBJ, "
            sSQLString = sSQLString & " PRES1_NIV2.COD, PRES1_NIV2.DEN, PRES1_NIV2.IMP, PRES1_NIV2.OBJ, "
            sSQLString = sSQLString & " PRES1_NIV3.COD, PRES1_NIV3.DEN, PRES1_NIV3.IMP, PRES1_NIV3.OBJ, "
            sSQLString = sSQLString & " PRES1_NIV4.COD, PRES1_NIV4.DEN, PRES1_NIV4.IMP, PRES1_NIV4.OBJ "
            sSQLString = sSQLString & " FROM PRES1_NIV1 "
            sSQLString = sSQLString & " LEFT JOIN PRES1_NIV2 ON PRES1_NIV2.ANYO = PRES1_NIV1.ANYO AND PRES1_NIV1.COD = PRES1_NIV2.PRES1" & HacerParteJoinUON(vUON1, vUON2, vUON3, "PRES1_NIV1", "PRES1_NIV2")
            If Not bBajaLog Then sSQLString = sSQLString & " AND PRES1_NIV2.BAJALOG = 0"
            sSQLString = sSQLString & " LEFT JOIN PRES1_NIV3 ON PRES1_NIV3.ANYO = PRES1_NIV1.ANYO AND PRES1_NIV3.PRES1 = PRES1_NIV1.COD AND PRES1_NIV3.PRES2 = PRES1_NIV2.COD" & HacerParteJoinUON(vUON1, vUON2, vUON3, "PRES1_NIV1", "PRES1_NIV3")
            If Not bBajaLog Then sSQLString = sSQLString & " AND PRES1_NIV3.BAJALOG = 0"
            sSQLString = sSQLString & " LEFT JOIN PRES1_NIV4 ON PRES1_NIV4.ANYO = PRES1_NIV1.ANYO AND PRES1_NIV4.PRES1 = PRES1_NIV1.COD AND PRES1_NIV4.PRES2 = PRES1_NIV2.COD AND PRES1_NIV4.PRES3 = PRES1_NIV3.COD" & HacerParteJoinUON(vUON1, vUON2, vUON3, "PRES1_NIV1", "PRES1_NIV4")
            If Not bBajaLog Then sSQLString = sSQLString & " AND PRES1_NIV4.BAJALOG = 0"
    End Select
    
    sSQLString = sSQLString & " WHERE PRES1_NIV1.ANYO = " & Anio
    
    If Not bBajaLog Then
        sSQLString = sSQLString & " AND PRES1_NIV1.BAJALOG = 0"
    End If
    
    If Proy4 <> "" Then
        sSQLString = sSQLString & " AND PRES1_NIV1.COD = '" & Proy1 & "' AND PRES1_NIV2.COD = '" & Proy2 & "' AND PRES1_NIV3.COD = '" & Proy3 & "' AND PRES1_NIV4.COD = '" & Proy4 & "'"
    Else
        If Proy3 <> "" Then
            sSQLString = sSQLString & " AND PRES1_NIV1.COD = '" & Proy1 & "' AND PRES1_NIV2.COD = '" & Proy2 & "' AND PRES1_NIV3.COD = '" & Proy3 & "'"
        Else
            If Proy2 <> "" Then
                sSQLString = sSQLString & " AND PRES1_NIV1.COD = '" & Proy1 & "' AND PRES1_NIV2.COD = '" & Proy2 & "'"
            Else
                If Proy1 <> "" Then
                    sSQLString = sSQLString & " AND PRES1_NIV1.COD = '" & Proy1 & "'"
                End If
            End If
        End If
    End If
    
    sSQLString = sSQLString & sWhereUON
    
    If OrdDen Then
    Select Case Nivel
        Case "1"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.DEN"
        Case "2"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.DEN,PRES1_NIV2.DEN "
        Case "3"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.DEN,PRES1_NIV2.DEN,PRES1_NIV3.DEN "
        Case "4"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.DEN,PRES1_NIV2.DEN,PRES1_NIV3.DEN,PRES1_NIV4.DEN "
    End Select
    End If
    
    If Ordcod Then
    Select Case Nivel
        Case "1"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.COD"
        Case "2"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.COD,PRES1_NIV2.COD "
        Case "3"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.COD,PRES1_NIV2.COD,PRES1_NIV3.COD "
        Case "4"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.COD,PRES1_NIV2.COD,PRES1_NIV3.COD,PRES1_NIV4.COD "
    End Select
    End If
    
    If OrdObj Then
    Select Case Nivel
        Case "1"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.OBJ"
        Case "2"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.OBJ,PRES1_NIV2.OBJ "
        Case "3"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.OBJ,PRES1_NIV2.OBJ,PRES1_NIV3.OBJ "
        Case "4"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.OBJ,PRES1_NIV2.OBJ,PRES1_NIV3.OBJ,PRES1_NIV4.OBJ "
    End Select
    End If
    
    If OrdPres Then
    Select Case Nivel
        Case "1"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.IMP"
        Case "2"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.IMP,PRES1_NIV2.IMP "
        Case "3"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.IMP,PRES1_NIV2.IMP,PRES1_NIV3.IMP "
        Case "4"
            sSQLString = sSQLString & " ORDER BY PRES1_NIV1.IMP,PRES1_NIV2.IMP,PRES1_NIV3.IMP,PRES1_NIV4.IMP "
    End Select
    End If
    
    ConstruirSQL = sSQLString
End Function

Private Function HacerParteWhereUON(ByVal vUON1 As Variant, ByVal vUON2 As Variant, ByVal vUON3 As Variant) As String
'************************************************************************************************************
'*** Descripci�n: Compone un string que ser� parte de la clausula Where de una consulta. Ser�n las        ***
'***              condiciones referentes a las unidades organizativas.                                    ***
'***                                                                                                      ***
'*** Par�metros:  vUON1, vUON2, vUON3 ::>> De tipo variant contienen informaci�n sobre cada nivel de una  ***
'***                                       unidad organizativa.                                           ***
'***                                                                                                      ***
'*** Valor que devuelve: Un string con lo ya explicado en la descripci�n de la funci�n.                   ***
'************************************************************************************************************

    Dim sSalida As String


    sSalida = ""
    If NoHayParametro(vUON1) Then
        sSalida = sSalida & " AND PRES1_NIV1.UON1 IS NULL"
    Else
        sSalida = sSalida & " AND PRES1_NIV1.UON1='" & DblQuote(vUON1) & "'"
    End If
    If NoHayParametro(vUON2) Then
        sSalida = sSalida & " AND PRES1_NIV1.UON2 IS NULL"
    Else
        sSalida = sSalida & " AND PRES1_NIV1.UON2='" & DblQuote(vUON2) & "'"
    End If
    If NoHayParametro(vUON3) Then
        sSalida = sSalida & " AND PRES1_NIV1.UON3 IS NULL"
    Else
        sSalida = sSalida & " AND PRES1_NIV1.UON3='" & DblQuote(vUON3) & "'"
    End If
    HacerParteWhereUON = sSalida
End Function

Private Function HacerParteJoinUON(ByVal vUON1 As Variant, ByVal vUON2 As Variant, ByVal vUON3 As Variant, ByVal sTabla1 As String, ByVal sTabla2 As String) As String
'************************************************************************************************************
'*** Descripci�n: Compone un string que ser� parte de la clausula Join de una consulta. Ser�n las         ***
'***              condiciones referentes a las unidades organizativas.                                    ***
'***                                                                                                      ***
'*** Par�metros:  vUON1, vUON2, vUON3 ::>> De tipo variant contienen informaci�n sobre cada nivel de una  ***
'***                                       unidad organizativa.                                           ***
'***              sTabla1, sTabla2    ::>> Son 2 strings que contienen el nombre de las tablas sobre las  ***
'***                                       cuales se har� la Join.                                        ***
'***                                                                                                      ***
'*** Valor que devuelve: Un string con lo ya explicado en la descripci�n de la funci�n.                   ***
'************************************************************************************************************
    Dim sSalida As String
    
    
    If NoHayParametro(vUON1) Then
        sSalida = "AND " & sTabla1 & ".UON1 IS NULL AND " & sTabla2 & ".UON1 IS NULL AND " & sTabla1 & ".UON2 IS NULL AND " & sTabla2 & ".UON2 IS NULL AND " & sTabla1 & ".UON3 IS NULL AND " & sTabla2 & ".UON3 IS NULL"
    ElseIf NoHayParametro(vUON2) Then
        sSalida = "AND " & sTabla1 & ".UON1=" & sTabla2 & ".UON1 AND " & sTabla1 & ".UON2 IS NULL AND " & sTabla2 & ".UON2 IS NULL AND " & sTabla1 & ".UON3 IS NULL AND " & sTabla2 & ".UON3 IS NULL"
    ElseIf NoHayParametro(vUON3) Then
        sSalida = "AND " & sTabla1 & ".UON1=" & sTabla2 & ".UON1 AND " & sTabla1 & ".UON2=" & sTabla2 & ".UON2 AND " & sTabla1 & ".UON3 IS NULL AND " & sTabla2 & ".UON3 IS NULL"
    Else
        sSalida = "AND " & sTabla1 & ".UON1=" & sTabla2 & ".UON1 AND " & sTabla1 & ".UON2=" & sTabla2 & ".UON2 AND " & sTabla1 & ".UON3=" & sTabla2 & ".UON3"
    End If
    HacerParteJoinUON = " " & sSalida & " "
End Function

