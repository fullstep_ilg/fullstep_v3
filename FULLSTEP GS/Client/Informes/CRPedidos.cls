VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public RepPath As String   'RepPath
Public NomCodPersonalizPedido As String  'gParametrosGenerales.gsNomCodPersonalizPedido
Public UsarOrgCompras As Boolean     'gParametrosGenerales.gbUsarOrgCompras
Public AccFSSM As TipoAccesoFSSM  'gParametrosGenerales.gsAccesoFSSM
Public ActivarCodProveErp As Boolean     'gParametrosGenerales.gbActivarCodProveErp
Public CodPersonalizPedido As Boolean    'gParametrosGenerales.gbCodPersonalizPedido
Public CodPersonalizDirecto As Boolean   'gParametrosGenerales.gbCodPersonalizDirecto
Public SolicitudesCompras As Boolean     'gParametrosGenerales.gbSolicitudesCompras
Public FSEPConf As Boolean      'FSEPConf
Public PedidosDirFac As Boolean  'gParametrosGenerales.gbPedidosDirFac
Public AccFSIM As TIPOACCESOFSIM      'gParametrosGenerales.gsAccesoFSIM
Public ParametrosSM  As CParametrosSM    'g_oParametrosSM
Public UsarPres1  As Boolean  'gParametrosGenerales.gbUsarPres1
Public UsarPedPres1 As Boolean   'gParametrosGenerales.gbUsarPedPres1
Public SingPres1 As String   'gParametrosGenerales.gsSingPres1
Public UsarPres2 As Boolean  'gParametrosGenerales.gbUsarPres2
Public UsarPedPres2 As Boolean  'gParametrosGenerales.gbUsarPedPres2
Public SingPres2 As String   'gParametrosGenerales.gsSingPres2
Public UsarPres3 As Boolean  'gParametrosGenerales.gbUsarPres3
Public UsarPedPres3 As Boolean  'gParametrosGenerales.gbUsarPedPres3
Public SingPres3 As String   'gParametrosGenerales.gsSingPres3
Public UsarPres4 As Boolean  'gParametrosGenerales.gbUsarPres4
Public UsarPedPres4 As Boolean  'gParametrosGenerales.gbUsarPedPres4
Public SingPres4 As String   'gParametrosGenerales.gsSingPres4

''' <summary>Imprime el listado de seguimiento de pedidos</summary>
''' <param name="frmOrigen">Formulario origen</param>
''' <param name="sSeleccion">Texto seleccion</param>
''' <param name="arCaptions">array de strings con los captions</param>
''' <param name="sEstados">array de estados</param>
''' <param name="sEspera"></param>
''' <param name="bIncluirObsGen">Incluir observaciones generales</param>
''' <param name="bIncluirObsLin">Incluir observaciones de l�nea</param>
''' <param name="rsDatosPedidos">recordset de pedidos</param>
''' <param name="rsAtributos">recordset de atributos</param>
''' <param name="rsImputaciones">recordset de imputaciones</param>
''' <param name="rsImpuCabe">recordset de imputaciones de cabecera</param>
''' <param name="bIncluirPres">Indica si hay que icluir los pres.</param>
''' <param name="rsPres1">recordset de Pres1</param>
''' <param name="rsPres2">recordset de Pres2</param>
''' <param name="rsPres3">recordset de Pres3</param>
''' <param name="rsPres4">recordset de Pres4</param>
''' <param name="rsAtribLin">recordset de atributos de l�nea</param>
''' <param name="bIncluirPlanes">Indica si hay que incluir los planes de entrega</param>
''' <param name="sPlanes">Captions para planes</param>
''' <param name="rsPlanes">recordset de planes de entrega</param>
''' <param name="bIncluirCDs">Indica si hay que incluir costes/descuentos</param>
''' <param name="rsCDCab">recordset de costes/descuentos de cabecera</param>
''' <param name="rsCDLin">recordset de costes/descuentos de l�nea</param>
''' <param name="sEstadosInt">array de estados de integraci�n</param>
''' <remarks>Llamada desde: frmSeguimiento.cmdImprimir_Click, frmLstPedidos.cmdObtener_Click</remarks>
''' <revision>LTG 07/08/2012</revision>

Public Function InformePedidos(ByRef frmOrigen As Object, ByVal sSeleccion As String, ByVal arCaptions As Variant, ByVal sEstados As Variant, ByVal sEspera As Variant, _
        ByVal bIncluirObsGen As Boolean, ByVal bIncluirObsLin As Boolean, ByVal bIncluirImputaciones As Boolean, _
        ByVal rsDatosPedidos As Ador.Recordset, ByVal rsAtributos As Ador.Recordset, ByVal rsImputaciones As Ador.Recordset, ByVal rsImpuCabe As Ador.Recordset, _
        ByVal bIncluirPres As Boolean, ByVal rsPres1 As Ador.Recordset, ByVal rsPres2 As Ador.Recordset, ByVal rsPres3 As Ador.Recordset, _
        ByVal rsPres4 As Ador.Recordset, ByVal rsAtribLin As Ador.Recordset, ByVal bIncluirPlanes As Boolean, ByVal sPlanes As Variant, _
        ByVal rsPlanes As Ador.Recordset, ByVal bIncluirCDs As Boolean, ByVal rsCDCab As Ador.Recordset, ByVal rsCDLin As Ador.Recordset, ByVal sEstadosInt As Variant) As CRAXDRT.Report
    Dim oReport  As CRAXDRT.Report
    Dim SubListado As Object
    Dim oFos As FileSystemObject
    Dim SelectionText(1 To 2, 1 To 4) As String
    Dim sCodComp As String
    Dim sCodEqp As String
    Dim sOpt As String
    Dim i As Integer
    Dim oMonedas As CMonedas
    Dim sPerSoli As String
    Dim iNumProceCod As Long
    Dim iNumAnyo2 As Integer
    Dim lSolicit As Long
    Dim oParametroSM As CParametroSM
    Dim sPres0 As String
    
    Set oReport = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    ' FORMULA FIELDS A NIVEL DE REPORT
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "TITULO")).Text = """" & arCaptions(0) & """"   'txtTitulo
        
    'Textos de los estados
    For i = 1 To 21
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtEst" & i)).Text = """" & sEstados(i) & """"
    Next i
    For i = 0 To 4
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtEstInt" & i)).Text = """" & sEstadosInt(i) & """"
    Next i
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtEst22")).Text = """" & sEstados(22) & """" 'Borrado
    
    If Len(sSeleccion) > 200 Then
        sSeleccion = Left(sSeleccion, 200)
    End If
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "SEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtPago")).Text = """" & arCaptions(1) & """"    'txtPago
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblMoneda")).Text = """" & arCaptions(2) & """" 'txtMoneda
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblAnyo")).Text = """" & arCaptions(3) & """"    'txtAnyo
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblArticulo")).Text = """" & arCaptions(4) & """"   'txtArt
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblCantidad")).Text = """" & arCaptions(5) & """"   'txtCantidad
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtDe")).Text = """" & arCaptions(6) & """"    'txtDe
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblDestino")).Text = """" & arCaptions(7) & """"    'txtDest
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblEstadoPedido")).Text = """" & arCaptions(8) & """"   'txtEstadoAct
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblEstado")).Text = """" & arCaptions(9) & """" 'txtEstado
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblFecAlta")).Text = """" & arCaptions(61) & """" 'txtFecAlta
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblFecEmi")).Text = """" & arCaptions(10) & """" 'txtFecEmision
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtFecEntProv")).Text = """" & arCaptions(11) & """"   'txtFecEntProv
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtFecEntrega")).Text = """" & arCaptions(12) & """"   'txtFecEntrega
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtFecCambioEst")).Text = """" & arCaptions(13) & """" 'txtFecCambEst
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtNumExterno")).Text = """" & arCaptions(14) & """"   'txtNumExterno
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblPedido")).Text = """" & arCaptions(15) & """"    'txtPed
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtSeleccion")).Text = """" & arCaptions(16) & """"    'txtSeleccion
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblPrecio")).Text = """" & arCaptions(17) & """"    'txtPrecioUP
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtPag")).Text = """" & arCaptions(18) & """"  'txtPag
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblCesta")).Text = """" & arCaptions(19) & """" 'txtNumCesta
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblCodProve")).Text = """" & arCaptions(20) & """"  'txtProveedor
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblUp")).Text = """" & arCaptions(21) & """"    'txtUP
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblSolic")).Text = """" & arCaptions(22) & """" 'stxtSolicit
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblImporte")).Text = """" & arCaptions(23) & """"   'txtImporte
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblPeriodo")).Text = """" & arCaptions(24) & ":"""  'txtPeriodo
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtAbono")).Text = """" & arCaptions(25) & """"    'txtAbono
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblAl")).Text = """" & arCaptions(26) & """"    'txtAl
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblFacturaOriginal")).Text = """" & arCaptions(27) & ":"""  'txtFacOriginal
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtReferencia")).Text = """" & NomCodPersonalizPedido & """"
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtReceptor")).Text = """" & arCaptions(28) & """" 'txtIdiReceptor
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtCodERP")).Text = """" & arCaptions(29) & """"   'txtIdiCodERP
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtEmpresa")).Text = """" & arCaptions(30) & """"  'txtEmpresa
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtAprov")).Text = """" & arCaptions(31) & """"    'txtIdiAprov
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtObs")).Text = """" & arCaptions(32) & """"  'txtObservaciones
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblTipoPedido")).Text = """" & arCaptions(33) & """"    'txtTipoPedido
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtPorCantidad")).Text = """" & arCaptions(53) & """"    'txtPorCantidad
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtPorImporte")).Text = """" & arCaptions(54) & """"    'txtPorImporte
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtFechaInicio")).Text = """" & arCaptions(55) & """"    'txtFechaInicio
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtFechaFin")).Text = """" & arCaptions(56) & """"    'txtFechaFin
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblCantidadAbierta")).Text = """" & arCaptions(57) & """"    'txtlblCantidadAbierta
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblCantidadPedida")).Text = """" & arCaptions(58) & """"    'txtlblCantidadPedida
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblImporteAbierto")).Text = """" & arCaptions(59) & """"    'txtlblImporteAbierto
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblImportePedido")).Text = """" & arCaptions(60) & """"    'txtlblImportePedido
    
    If Not UsarOrgCompras Then
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtCAMPO3")).Text = """" & arCaptions(34) & """"   'txtCampo3
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtMOSTRARCAMPO3")).Text = """" & "S" & """"
        If AccFSSM = AccesoFSSM Then
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtCAMPO4")).Text = """" & arCaptions(35) & """"   'txtCampo4
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtMOSTRARCAMPO4")).Text = """" & "S" & """"
        Else
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtCAMPO4")).Text = """" & "" & """"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtMOSTRARCAMPO4")).Text = """" & "N" & """"
        End If
    Else
        If AccFSSM = AccesoFSSM Then
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtCAMPO3")).Text = """" & arCaptions(34) & """"   'txtCampo3
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtMOSTRARCAMPO3")).Text = """" & "N" & """"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtCAMPO4")).Text = """" & "" & """"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtMOSTRARCAMPO4")).Text = """" & "S" & """"
        Else
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtMOSTRARCAMPO3")).Text = """" & "N" & """"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtMOSTRARCAMPO4")).Text = """" & "N" & """"
        End If
    End If
       
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblFecVistPorv")).Text = """" & arCaptions(36) & """"   'txtVisitadoProve
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtProveAdj")).Text = """" & arCaptions(37) & """" 'lblProAdj.caption
        
    'Indica si el report va a mostrar o no el C�d.ERP
    If ActivarCodProveErp = False Then
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "vERP")).Text = """" & "N" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "vERP")).Text = """" & "S" & """"
    End If
    
    If CodPersonalizPedido Or CodPersonalizDirecto Then
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "vRef")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "vRef")).Text = """" & "N" & """"
    End If

    'Indica al report si se va a mostrar o no las solicitudes
    If SolicitudesCompras And Not FSEPConf Then
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "vMostrarSolicit")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "vMostrarSolicit")).Text = """" & "N" & """"
    End If
    
    'Indicar si se va a mostrar la direcci�n de env�o de factura
    If PedidosDirFac Then
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "vPedDirFac")).Text = """" & "S" & """"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblEnvioFactura")).Text = """" & arCaptions(38) & """"  'txtDirEnvioFactura
    Else
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "vPedDirFac")).Text = """" & "N" & """"
    End If
    
    If bIncluirObsGen Then
        sOpt = "S"
    Else
        sOpt = "N"
    End If
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "ObsGen")).Text = """" & sOpt & """"
    
    If bIncluirObsLin Then
        sOpt = "S"
    Else
        sOpt = "N"
    End If
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "ObsLinea")).Text = """" & sOpt & """"
    
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblGestor")).Text = """" & arCaptions(39) & """"    'txtGestor
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblAutofactura")).Text = """" & arCaptions(40) & """"   'txtAutofactura
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblImporteBruto")).Text = """" & arCaptions(41) & """"  'txtImporteBruto
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblCostes")).Text = """" & arCaptions(42) & """"    'txtCostes
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblDescuentos")).Text = """" & arCaptions(43) & """"    'txtDescuentos
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblImporteLinea")).Text = """" & arCaptions(44) & """"  'txtImporteLinea
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblAlmacen")).Text = """" & arCaptions(45) & """"   'txtAlmacen
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblActivo")).Text = """" & arCaptions(46) & """"    'txtActivo
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtFacAlbaran")).Text = """" & arCaptions(47) & """"   'txtFacTrasRecepAlbaran
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtFacPedido")).Text = """" & arCaptions(48) & """"    'txtFacTrasRecepPedido
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtlblEstInt")).Text = """" & arCaptions(62) & ":"""    'txtlblEstInt
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtAlta")).Text = """" & arCaptions(63) & """"    'txtAlta
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "txtModif")).Text = """" & arCaptions(64) & """"    'txtModif
    
    If (AccFSIM = AccesoFSIM) Then
        sOpt = "S"
    Else
        sOpt = "N"
    End If
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "optFacTrasRecep")).Text = """" & sOpt & """"
                
    If Not rsDatosPedidos Is Nothing Then
        oReport.Database.SetDataSource rsDatosPedidos
    End If
        
    If Not rsAtributos Is Nothing Then
        sOpt = "S"
        Set SubListado = oReport.OpenSubreport("rptAtrOrden")
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtTrue")).Text = """" & arCaptions(49) & """"   'm_sIdiTrue
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtFalse")).Text = """" & arCaptions(50) & """"  'm_sIdiFalse
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptAtrOrden")).Text = """" & sOpt & """"
        SubListado.Database.SetDataSource rsAtributos
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptAtrOrden")).Text = """" & sOpt & """"
    End If
    
    If bIncluirImputaciones Then
        If AccFSSM = AccesoFSSM Then
            If Not ParametrosSM Is Nothing Then
                Set SubListado = oReport.OpenSubreport("rptIMPUTACIONES")
                    
                For Each oParametroSM In ParametrosSM
                     If oParametroSM.ImpModo = TipoModoImputacionSM.NivelCabecera Then
                         sPres0 = oParametroSM.PRES5
                         Exit For
                     End If
                Next
                
                If Not rsImputaciones Is Nothing Then
                    sOpt = "S"
                    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptImpu")).Text = """" & sOpt & """"
                    SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtCentro")).Text = """" & arCaptions(51) & """" 'txtCentro
                    SubListado.Database.SetDataSource rsImputaciones
                Else
                    sOpt = "N"
                    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptImpu")).Text = """" & sOpt & """"
                End If
    
                'Si hay imputaciones de cabecera
                sOpt = "N"
                If sPres0 <> "" Then
                    If Not rsImpuCabe Is Nothing Then
                        sOpt = "S"
                        Set SubListado = oReport.OpenSubreport("rptIMPUCABE")
                        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtCentro")).Text = """" & arCaptions(51) & """" 'txtCentro
                        SubListado.Database.SetDataSource rsImpuCabe
                    End If
                End If
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptImpuCabe")).Text = """" & sOpt & """"
            End If
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptImpu")).Text = """" & sOpt & """"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptImpuCabe")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptImpu")).Text = """" & sOpt & """"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptImpuCabe")).Text = """" & sOpt & """"
    End If
    
    If bIncluirPres Then
        If UsarPres1 And UsarPedPres1 Then
            Set SubListado = oReport.OpenSubreport("rptPRES1")
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtPRES1")).Text = """" & SingPres1 & ":" & """"
            
            If Not rsPres1 Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres1")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource rsPres1
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres1")).Text = """" & sOpt & """"
            End If
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres1")).Text = """" & sOpt & """"
        End If
        
        If UsarPres2 And UsarPedPres2 Then
            Set SubListado = oReport.OpenSubreport("rptPRES2")
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtPRES2")).Text = """" & SingPres2 & ":" & """"
            
            If Not rsPres2 Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres2")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource rsPres2
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres2")).Text = """" & sOpt & """"
            End If
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres2")).Text = """" & sOpt & """"
        End If
        
        If UsarPres3 And UsarPedPres3 Then
            Set SubListado = oReport.OpenSubreport("rptPRES3")
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtPRES3")).Text = """" & SingPres3 & ":" & """"
            
            If Not rsPres3 Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres3")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource rsPres3
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres3")).Text = """" & sOpt & """"
            End If
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres3")).Text = """" & sOpt & """"
        End If
        
        If UsarPres4 And UsarPedPres4 Then
            Set SubListado = oReport.OpenSubreport("rptPRES4")
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtPRES4")).Text = """" & SingPres4 & ":" & """"
            
            If Not rsPres4 Is Nothing Then
                sOpt = "S"
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres4")).Text = """" & sOpt & """"
                SubListado.Database.SetDataSource rsPres4
            Else
                sOpt = "N"
                oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres4")).Text = """" & sOpt & """"
            End If
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres4")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres1")).Text = """" & sOpt & """"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres2")).Text = """" & sOpt & """"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres3")).Text = """" & sOpt & """"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPres4")).Text = """" & sOpt & """"
    End If
    
    'Atributos de la LINEA
    If Not rsAtribLin Is Nothing Then
        sOpt = "S"
        Set SubListado = oReport.OpenSubreport("rptAtrLinea.rpt")
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtTrue")).Text = """" & arCaptions(49) & """"
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtFalse")).Text = """" & arCaptions(50) & """"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptAtrLinea")).Text = """" & sOpt & """"
        SubListado.Database.SetDataSource rsAtribLin
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptAtrLinea")).Text = """" & sOpt & """"
    End If
    
    'Planes de entrega
    If bIncluirPlanes Then
        If Not rsPlanes Is Nothing Then
            Set SubListado = oReport.OpenSubreport("rptPlanes")
    
            sOpt = "S"
            SubListado.Database.SetDataSource rsPlanes
    
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPlanes")).Text = """" & sOpt & """"
    
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtPlanes")).Text = """" & sPlanes(0) & """"
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtFechaPlan")).Text = """" & sPlanes(1) & """"
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtCantidadPlan")).Text = """" & sPlanes(2) & """"
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtAlbaranPlan")).Text = """" & sPlanes(3) & """"
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPlanes")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptPlanes")).Text = """" & sOpt & """"
    End If
    
    'Costes/descuentos
    If bIncluirCDs Then
        'Cabecera
        If Not rsCDCab Is Nothing Then
            Set SubListado = oReport.OpenSubreport("rptCDsCab")
            
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptCDsCab")).Text = """" & sOpt & """"
                        
            SubListado.Database.SetDataSource rsCDCab
    
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtCostesDescuentos")).Text = """" & arCaptions(52) & """"
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptCDsCab")).Text = """" & sOpt & """"
        End If
        
        'Lineas
        If Not rsCDLin Is Nothing Then
            Set SubListado = oReport.OpenSubreport("rptCDsLin")
            
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptCDsLin")).Text = """" & sOpt & """"
                        
            SubListado.Database.SetDataSource rsCDLin
    
            SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtCostesDescuentos")).Text = """" & arCaptions(52) & """"
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptCDsLin")).Text = """" & sOpt & """"
        End If
    Else
        sOpt = "N"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptCDsCab")).Text = """" & sOpt & """"
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OptCDsLin")).Text = """" & sOpt & """"
    End If
    
    Set InformePedidos = oReport
        
    Set oReport = Nothing
    DoEvents
End Function

