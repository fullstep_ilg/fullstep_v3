VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmLstESTRCOMP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de compradores (Opciones)"
   ClientHeight    =   2340
   ClientLeft      =   1275
   ClientTop       =   3300
   ClientWidth     =   5745
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstESTRCOMP.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2340
   ScaleWidth      =   5745
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   5745
      TabIndex        =   9
      Top             =   1965
      Width           =   5745
      Begin VB.CommandButton cmdObtener 
         Caption         =   "D&Obtener"
         Height          =   375
         Left            =   4365
         TabIndex        =   8
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1935
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   5700
      _ExtentX        =   10054
      _ExtentY        =   3413
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DSelecci�n"
      TabPicture(0)   =   "frmLstESTRCOMP.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraOpcComp"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "DOpciones"
      TabPicture(1)   =   "frmLstESTRCOMP.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "DOrden"
      TabPicture(2)   =   "frmLstESTRCOMP.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame1"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame4 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1470
         Left            =   -74880
         TabIndex        =   13
         Top             =   330
         Width           =   5430
         Begin VB.CheckBox chkDetalles 
            Caption         =   "DIncluir detalles"
            Height          =   195
            Left            =   315
            TabIndex        =   5
            Top             =   930
            Width           =   4800
         End
         Begin VB.CheckBox chkComprador 
            Caption         =   "DMostrar compradores"
            Height          =   195
            Left            =   315
            TabIndex        =   4
            Top             =   420
            Value           =   1  'Checked
            Width           =   4770
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1470
         Left            =   -74880
         TabIndex        =   14
         Top             =   330
         Width           =   5430
         Begin VB.OptionButton opOrdCod 
            Caption         =   "DC�digo"
            Height          =   195
            Left            =   330
            TabIndex        =   6
            Top             =   420
            Value           =   -1  'True
            Width           =   3390
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "DDenominaci�n"
            Height          =   195
            Left            =   330
            TabIndex        =   7
            Top             =   930
            Width           =   3240
         End
      End
      Begin VB.Frame fraOpcComp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1470
         Left            =   120
         TabIndex        =   11
         Top             =   330
         Width           =   5430
         Begin VB.OptionButton opTodos 
            Caption         =   "DListado Completo"
            Height          =   195
            Left            =   285
            TabIndex        =   0
            Top             =   360
            Value           =   -1  'True
            Width           =   4950
         End
         Begin VB.OptionButton opEquipo 
            Caption         =   "DEquipo:"
            Height          =   195
            Left            =   285
            TabIndex        =   1
            Top             =   945
            Width           =   900
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
            Height          =   285
            Left            =   1185
            TabIndex        =   2
            Top             =   900
            Width           =   1065
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1296
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5345
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
            Height          =   285
            Left            =   2310
            TabIndex        =   3
            Top             =   900
            Width           =   2985
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1640
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5265
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblEqp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label3"
            Height          =   285
            Left            =   1200
            TabIndex        =   12
            Top             =   900
            Visible         =   0   'False
            Width           =   4065
         End
      End
   End
End
Attribute VB_Name = "frmLstESTRCOMP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Mensajes As CMensajes
Public ParamInstRPTPATH As String
Public ParamGenRPTPATH As String
Public Report As CRAXDRT.Report
Public GestorIdiomas As CGestorIdiomas
Public GuardarParametrosIns As Boolean
Public Titulo As String  'preview
Public Idioma As String
Public Raiz As CRaiz
Public CargaMaximaCombos As Integer
Public oEqpSeleccionado As CEquipo
Public g_sOrigen As String
Public g_bOrdenarPorDen As Boolean
Public gCodEqpUsuario As Variant
Public oUsuarioSummit As CUsuario
' Variables de restricciones
Private bREqp As Boolean

' Variables de la combo
Private CargarComboDesdeEqp As Boolean
Private RespetarCombo As Boolean
Private oEqps As CEquipos

'Variables de idioma
Private sIdiSelEqp As String
Private sIdiSelComp As String
Private sIdiSel As String
Private sIdiTituComp As String
Private sIdiTituEqp As String
Private txtDe As String
Private txtEmail As String
Private txtFax As String
Private txtPag As String
Private txtSeleccion As String
Private txtTfno As String
Private txtCod As String
Private txtDen As String
Private txtNombre As String
Private txtTfno2 As String
Private txtCargo As String
Private Sub chkComprador_Click()
 If chkComprador Then
    chkDetalles.Enabled = True
 Else
    chkDetalles.Enabled = False
 End If
End Sub

Private Sub sdbcEqpCod_Change()
     
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpDen.Text = ""
        RespetarCombo = False
        
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
                    
    End If
    
End Sub
Private Sub sdbcEqpCod_CloseUp()
    
    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpCod.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    opEquipo = True
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(0).Text)
        
    CargarComboDesdeEqp = False
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = Raiz.Generar_CEquipos
    
    sdbcEqpCod.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde CargaMaximaCombos, Trim(sdbcEqpCod.Text), , False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, False, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh

    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcEqpCod_InitColumnProps()

    sdbcEqpCod.DataFieldList = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcEqpCod_Click()
    
    If Not sdbcEqpCod.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpCod.Rows - 1
            bm = sdbcEqpCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = Raiz.Generar_CEquipos
 
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpCod.Text = ""
    Else
        RespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        sdbcEqpCod.Columns(0).Value = sdbcEqpCod.Text
        sdbcEqpCod.Columns(1).Value = sdbcEqpDen.Text
        
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpDen_Change()
      
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpCod.Text = ""
        RespetarCombo = False
        
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
                    
    End If
          
End Sub

Private Sub sdbcEqpDen_Click()
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpDen_CloseUp()
    
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpDen.Value = "" Then Exit Sub
        
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    opEquipo = True
    
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(1).Text)
        
    CargarComboDesdeEqp = False
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = Raiz.Generar_CEquipos
    
    sdbcEqpDen.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde CargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, True, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpDen.Rows - 1
            bm = sdbcEqpDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = Raiz.Generar_CEquipos
 
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    
    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
    Else
        RespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        sdbcEqpDen.Columns(0).Value = sdbcEqpDen.Text
        sdbcEqpDen.Columns(1).Value = sdbcEqpCod.Text
        
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdObtener_Click()

    ObtenerListado
    
End Sub

Private Sub ObtenerListado()
    Dim SelectionText As String
    Dim RepPath As String
    Dim ParamComp As String
    Dim ParamDet As String
    Dim oFos As FileSystemObject
    Dim oCRCompradores As New CRCompradores
    
    ''' * Objetivo: Obtener un listado de la estructura de compradores
    
    If bcrs_Connected = False Then
        Exit Sub
    End If
    
    If ParamInstRPTPATH = "" Then
        If ParamGenRPTPATH = "" Then
            Mensajes.RutaDeRPTNoValida
           Set Report = Nothing
           Exit Sub
        Else
            ParamInstRPTPATH = ParamGenRPTPATH
            GuardarParametrosIns = True
        End If
    End If
    RepPath = ParamInstRPTPATH & "\rptESTRCOMP.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Mensajes.RutaDeRPTNoValida
        Set Report = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    
    If oEqpSeleccionado Is Nothing And Not bREqp Then
        opTodos.Value = True
        opEquipo.Value = False
    Else
        If bREqp Then
            Set oEqpSeleccionado = Nothing
            Set oEqpSeleccionado = Raiz.generar_CEquipo
            oEqpSeleccionado.Cod = gCodEqpUsuario
        End If
        opEquipo.Value = True
        opTodos.Value = False
    End If
    
    Set Report = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)

    SelectionText = ""
    If sdbcEqpCod.Text = "" And Not bREqp Then
        If chkComprador.Value = Unchecked Then
            SelectionText = sIdiSelEqp
        Else
            SelectionText = sIdiSelComp
        End If
        oCRCompradores.Listado Report, opOrdDen
    Else
        SelectionText = sIdiSel & " " & sdbcEqpCod.Text
        oCRCompradores.Listado Report, opOrdDen, sdbcEqpCod.Text
        chkComprador.Value = vbChecked
    End If
    ParamComp = "S": ParamDet = "S"
    If chkDetalles.Value = vbUnchecked Then ParamDet = "N"
    If chkComprador.Value = vbChecked Then
        Titulo = sIdiTituComp
    Else
        ParamComp = "N": ParamDet = "N"
        Titulo = sIdiTituEqp
    End If
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtCod")).Text = """" & txtCod & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDe")).Text = """" & txtDe & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDen")).Text = """" & txtDen & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtEmail")).Text = """" & txtEmail & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtFax")).Text = """" & txtFax & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtNombre")).Text = """" & txtNombre & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtPag")).Text = """" & txtPag & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtSeleccion")).Text = """" & txtSeleccion & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtTfno")).Text = """" & txtTfno & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtTfno2")).Text = """" & txtTfno2 & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtCargo")).Text = """" & txtCargo & """"
    
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "SEL")).Text = """" & SelectionText & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "TITULO")).Text = """" & Titulo & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "VCOMP")).Text = """" & ParamComp & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "VDET")).Text = """" & ParamDet & """"

    If Report Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Me.Hide
    Screen.MousePointer = vbNormal
     
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_LSTESTRCOMP, Idioma)
   
    If Not Ador Is Nothing Then
    
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value     '2
        Ador.MoveNext
        SSTab1.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        chkComprador.Caption = Ador(0).Value '5
        Ador.MoveNext
        chkDetalles.Caption = Ador(0).Value
        Ador.MoveNext
        frmLstESTRCOMP.Caption = Ador(0).Value
        Ador.MoveNext
        opEquipo.Caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.Caption = Ador(0).Value '9 c�digo
        sdbcEqpCod.Columns(0).Caption = Ador(0).Value
        sdbcEqpDen.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.Caption = Ador(0).Value '10 Denominaci�n
        sdbcEqpDen.Columns(0).Caption = Ador(0).Value
        sdbcEqpCod.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        opTodos.Caption = Ador(0).Value
        
        'Idiomas del RPT
        Ador.MoveNext
        sIdiSelEqp = Ador(0).Value '200
        Ador.MoveNext
        sIdiSelComp = Ador(0).Value
        Ador.MoveNext
        sIdiSel = Ador(0).Value
        Ador.MoveNext
        sIdiTituComp = Ador(0).Value
        Ador.MoveNext
        sIdiTituEqp = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value '205
        Ador.MoveNext
        txtEmail = Ador(0).Value
        Ador.MoveNext
        txtFax = Ador(0).Value
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        txtSeleccion = Ador(0).Value
        Ador.MoveNext
        txtTfno = Ador(0).Value '210
        Ador.MoveNext
        txtCod = Ador(0).Value
        Ador.MoveNext
        txtDen = Ador(0).Value
        Ador.MoveNext
        txtNombre = Ador(0).Value
        Ador.MoveNext
        txtTfno2 = Ador(0).Value
        Ador.MoveNext
        txtCargo = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

Private Sub Form_Load()

    Me.Width = 5865
    Me.Height = 2745
    
    CargarRecursos
    PonerFieldSeparator Me
    
    ' Configurar la seguridad
    ConfigurarSeguridad

    If bREqp Then
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp.Caption = gCodEqpUsuario & " - " & oUsuarioSummit.comprador.DenEqp
        sdbcEqpCod.Text = gCodEqpUsuario
        
        opTodos = False
        opEquipo = True
        opTodos.Enabled = False
        
        Set oEqpSeleccionado = Nothing
        Set oEqpSeleccionado = Raiz.generar_CEquipo
  
        oEqpSeleccionado.Cod = gCodEqpUsuario

    End If
    Select Case g_sOrigen
        Case "frmESTRCOMP"
            opOrdDen.Value = g_bOrdenarPorDen
    End Select
End Sub

Public Sub VerificarSeleccion()

    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    opEquipo = True
    RespetarCombo = True
    sdbcEqpCod.Text = oEqpSeleccionado.Cod
    RespetarCombo = False
    sdbcEqpCod_Validate False
    
End Sub

Private Sub ConfigurarSeguridad()
bREqp = False
If Not oUsuarioSummit Is Nothing Then
    If oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPRestEquipo)) Is Nothing) Then
            bREqp = True
        End If
    End If
End If

End Sub

Private Sub opEquipo_Click()
 If opEquipo Then
    chkComprador.Enabled = False
    chkComprador.Value = vbChecked
 End If
End Sub


Private Sub opTodos_Click()
 If opTodos Then
    chkComprador.Enabled = True
    sdbcEqpCod.Text = ""
 End If
End Sub



