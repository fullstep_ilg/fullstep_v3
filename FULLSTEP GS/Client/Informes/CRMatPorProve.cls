VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRMatPorProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Pymes As Boolean 'gParametrosGenerales.gbPymes
Public UsuarioTipo As Integer   'oUsuarioSummit.Tipo
Public UsuarioComprador As CComprador    'oUsuarioSummit.comprador
Public IdiomaInstalacion As String   'basPublic.gParametrosInstalacion.gIdioma

Public Function ListadoMatPorProve(ByRef oGestorInformes As CGestorInformes, oReport As CRAXDRT.Report, Optional ByVal bTodos As Boolean, Optional ByVal bREqp As Boolean, Optional ByVal gCodEqpUsuario As String, Optional ByVal bProve As Boolean, Optional ByVal CodProve As String, Optional ByVal bOrdCod As Boolean, Optional ByVal bOcultarHomologacion As Boolean, Optional ByVal bMostrarArticulos As Boolean, Optional ByVal sGmn1 As String)
    Dim Table As CRAXDRT.DatabaseTable
    Dim SubListado As CRAXDRT.Report
    Dim oConnectionInfo As CRAXDRT.ConnectionProperties
    Dim GroupOrderRpt(1 To 2, 1 To 1) As String
    Dim GroupOrderSubRpt(1 To 2, 1 To 5) As String
    Dim sRecordSelectionFormulaRpt As String
    Dim i As Integer
    Dim adoresDG As Ador.Recordset
    Dim oIMAsig As IMaterialAsignado
    Dim oGruposMN1 As CGruposMatNivel1
    
    If bTodos = True Then
        If bREqp Then
            sRecordSelectionFormulaRpt = "{PROVE.COD} = {PROVE_EQP.PROVE} AND {PROVE_EQP.EQP} = '" & DblQuote(gCodEqpUsuario) & "'"
        End If
        If Pymes And UsuarioTipo <> Administrador Then
            sRecordSelectionFormulaRpt = "{PROVE_GMN4.GMN1} = '" & sGmn1 & "'"
        End If
    Else
        If bProve = True Then
            sRecordSelectionFormulaRpt = "{PROVE.COD} = '" & DblQuote(CodProve) & "'"
        End If
    End If
    
    For Each Table In oReport.Database.Tables
        'Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Set oConnectionInfo = Table.ConnectionProperties
        'Set the OLE DB Provider
        oConnectionInfo.Item("Provider") = "SQLOLEDB"
        'Set the physical server name
        oConnectionInfo.Item("Data Source") = scrs_Server
        'Set the database name
        oConnectionInfo.Item("Initial Catalog") = scrs_Database
        'Set the integrated security
        oConnectionInfo.Item("Integrated Security") = False
        'Set the user name
        oConnectionInfo.Item("User ID") = scrs_User
        'Set the password"
        oConnectionInfo.Item("Password") = scrs_Password
        'Set the fully qualified table name if different from �the original data source
        Table.Location = scrs_Database & ".dbo." & Table.Name
        Set oConnectionInfo = Nothing
    Next
    
    oReport.RecordSelectionFormula = sRecordSelectionFormulaRpt
    
    ' CLASIFICACION REPORT
    GroupOrderRpt(2, 1) = "ORDEN_GRUPO1P"  ' Proveedores
    ' CLASIFICACION SUBREPORT
    GroupOrderSubRpt(2, 1) = "ORDEN_GRUPO1"  ' Nivel material 1
    GroupOrderSubRpt(2, 2) = "ORDEN_GRUPO2"  ' Nivel material 2
    GroupOrderSubRpt(2, 3) = "ORDEN_GRUPO3"  ' Nivel material 3
    GroupOrderSubRpt(2, 4) = "ORDEN_GRUPO4"  ' Nivel material 4
    GroupOrderSubRpt(2, 5) = "ORDEN_GRUPO5"  ' Art�culos
    
    If bOrdCod Then
        GroupOrderRpt(1, 1) = "{PROVE.COD}"
        GroupOrderSubRpt(1, 1) = "{MatPorProve.COD1}"
        GroupOrderSubRpt(1, 2) = "{MatPorProve.COD2}"
        GroupOrderSubRpt(1, 3) = "{MatPorProve.COD3}"
        GroupOrderSubRpt(1, 4) = "{MatPorProve.COD4}"
        GroupOrderSubRpt(1, 5) = "{MatPorProve.COD5}"
    Else
        GroupOrderRpt(1, 1) = "{PROVE.DEN}"
        GroupOrderSubRpt(1, 1) = "{MatPorProve.DEN1}"
        GroupOrderSubRpt(1, 2) = "{MatPorProve.DEN2}"
        GroupOrderSubRpt(1, 3) = "{MatPorProve.DEN3}"
        GroupOrderSubRpt(1, 4) = "{MatPorProve.DEN4}"
        GroupOrderSubRpt(1, 5) = "{MatPorProve.DEN5}"
    End If
    
    For i = 1 To UBound(GroupOrderRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, GroupOrderRpt(2, i))).Text = GroupOrderRpt(1, i)
    Next i
    
          
    'SUBREPORT material
    Set SubListado = oReport.OpenSubreport("rptMAT")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    If Pymes And UsuarioTipo <> Administrador Then
        
        Set oIMAsig = UsuarioComprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        Set adoresDG = oGestorInformes.ListadoMaterialPorProveedor(bMostrarArticulos)
        Set oGruposMN1 = Nothing
        Set oIMAsig = Nothing
    Else
        Set adoresDG = oGestorInformes.ListadoMaterialPorProveedor(bMostrarArticulos)
    End If
    
    If Not adoresDG Is Nothing Then
        SubListado.Database.SetDataSource adoresDG
    End If
    
    If bMostrarArticulos = True Then
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "ARTICULOS")).Text = """" & "S" & """"
    Else
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "ARTICULOS")).Text = """" & "N" & """"
    End If
    
    For i = 1 To UBound(GroupOrderSubRpt, 2)
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, GroupOrderSubRpt(2, i))).Text = GroupOrderSubRpt(1, i)
    Next i
    
    Set SubListado = oReport.OpenSubreport("rptCON")
    
    For Each Table In SubListado.Database.Tables
        Set oConnectionInfo = Table.ConnectionProperties
        oConnectionInfo.Item("Provider") = "SQLOLEDB"
        oConnectionInfo.Item("Data Source") = scrs_Server
        oConnectionInfo.Item("Initial Catalog") = scrs_Database
        oConnectionInfo.Item("Integrated Security") = False
        oConnectionInfo.Item("Password") = scrs_Password
        oConnectionInfo.Item("User ID") = scrs_User
        Table.Location = scrs_Database & ".dbo." & Table.Name
        Set oConnectionInfo = Nothing
    Next
    
    ' SUBREPORTS CALIFICACIONES
    'If bOcultarHomologacion Then
        Set SubListado = oReport.OpenSubreport("rptCAL1")
        SubListado.RecordSelectionFormula = " {CAL.NUM} = {?Pm-PROVE.TIPOCAL1} and {CAL.COD} = {?Pm-PROVE.CAL1} AND {CAL_IDIOMA.IDIOMA}='" & IdiomaInstalacion & "'"
        For Each Table In SubListado.Database.Tables
            Set oConnectionInfo = Table.ConnectionProperties
            oConnectionInfo.Item("Provider") = "SQLOLEDB"
            oConnectionInfo.Item("Data Source") = scrs_Server
            oConnectionInfo.Item("Initial Catalog") = scrs_Database
            oConnectionInfo.Item("Integrated Security") = False
            oConnectionInfo.Item("Password") = scrs_Password
            oConnectionInfo.Item("User ID") = scrs_User
            Table.Location = scrs_Database & ".dbo." & Table.Name
            Set oConnectionInfo = Nothing
        Next
        
        Set SubListado = oReport.OpenSubreport("rptCAL2")
        SubListado.RecordSelectionFormula = " {CAL.NUM} = {?Pm-PROVE.TIPOCAL2} and {CAL.COD} = {?Pm-PROVE.CAL2} AND {CAL_IDIOMA.IDIOMA}='" & IdiomaInstalacion & "'"
        For Each Table In SubListado.Database.Tables
            Set oConnectionInfo = Table.ConnectionProperties
            oConnectionInfo.Item("Provider") = "SQLOLEDB"
            oConnectionInfo.Item("Data Source") = scrs_Server
            oConnectionInfo.Item("Initial Catalog") = scrs_Database
            oConnectionInfo.Item("Integrated Security") = False
            oConnectionInfo.Item("Password") = scrs_Password
            oConnectionInfo.Item("User ID") = scrs_User
            Table.Location = scrs_Database & ".dbo." & Table.Name
            Set oConnectionInfo = Nothing
        Next
        
        Set SubListado = oReport.OpenSubreport("rptCAL3")
        SubListado.RecordSelectionFormula = " {CAL.NUM} = {?Pm-PROVE.TIPOCAL3} and {CAL.COD} = {?Pm-PROVE.CAL3} AND {CAL_IDIOMA.IDIOMA}='" & IdiomaInstalacion & "'"
        For Each Table In SubListado.Database.Tables
            Set oConnectionInfo = Table.ConnectionProperties
            oConnectionInfo.Item("Provider") = "SQLOLEDB"
            oConnectionInfo.Item("Data Source") = scrs_Server
            oConnectionInfo.Item("Initial Catalog") = scrs_Database
            oConnectionInfo.Item("Integrated Security") = False
            oConnectionInfo.Item("Password") = scrs_Password
            oConnectionInfo.Item("User ID") = scrs_User
            Table.Location = scrs_Database & ".dbo." & Table.Name
            Set oConnectionInfo = Nothing
        Next
    'End If
    
    Set SubListado = Nothing
    
End Function

