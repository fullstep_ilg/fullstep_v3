VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRProvePorEqp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public IdiomaInstalacion As String   'basPublic.gParametrosInstalacion.gIdioma

Private oFos As FileSystemObject

''' <summary>
''' Conecta el report a bbdd y establece los datos a mostrar
''' </summary>
''' <param name="oReport">objeto Report</param>
''' <param name="codEqp">equipos a buscar</param>
''' <param name="CodGMN1">GMN1 a buscar</param>
''' <param name="CodGMN2">GMN2 a buscar</param>
''' <param name="CodGMN3">GMN3 a buscar</param>
''' <param name="CodGMN4">GMN4 a buscar</param>
''' <param name="bOrdCod">Ordenacion</param>
''' <param name="bOcultarHomologaciones">ocultar o no las homologaciones</param>
''' <remarks>Llamada desde: frmLstPROVEProvePorEqp/cmdObtener_Click ; Tiempo m�ximo: 0,2</remarks>
Public Function ListadoProvePorEqp(ByRef oGestorInformes As CGestorInformes, oReport As CRAXDRT.Report, ByVal codEqp As String, ByVal CodGMN1 As String, ByVal CodGMN2 As String, ByVal CodGMN3 As String, ByVal codGMN4 As String, ByVal bOrdCod As Boolean, ByVal bOcultarHomologaciones As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim RepPath As String
    Dim SubListado As CRAXDRT.Report
    Dim SQLQueryString As String
    
    Dim m_ArProveEqp As New adodb.Recordset
    Dim g_adoresDG As Ador.Recordset
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    SQLQueryString = ObtenerSQLProvePorEqp(codEqp, CodGMN1, CodGMN2, CodGMN3, codGMN4, bOrdCod)
    
    
    Set g_adoresDG = New adodb.Recordset
    
    Set g_adoresDG = oGestorInformes.ListadoProvePorEqp(SQLQueryString)
    
    If g_adoresDG Is Nothing Then
        Set oReport = Nothing
        Exit Function
    End If
    
    If g_adoresDG.EOF Then
        Set oReport = Nothing
        Exit Function
    End If
    
    Set m_ArProveEqp = New Ador.Recordset
    m_ArProveEqp.Fields.Append "EQPDEN", adVarChar, 100, adFldIsNullable
    m_ArProveEqp.Fields.Append "MONDEN", adVarChar, 100, adFldIsNullable
    m_ArProveEqp.Fields.Append "PAIDEN", adVarChar, 100, adFldIsNullable
    m_ArProveEqp.Fields.Append "COD", adVarChar, 20
    m_ArProveEqp.Fields.Append "PROVEDEN", adVarChar, 100
    m_ArProveEqp.Fields.Append "DIR", adVarChar, 255, adFldIsNullable
    m_ArProveEqp.Fields.Append "CP", adVarChar, 20, adFldIsNullable
    m_ArProveEqp.Fields.Append "POB", adVarChar, 100, adFldIsNullable
    m_ArProveEqp.Fields.Append "VAL1", adDouble, , adFldIsNullable
    m_ArProveEqp.Fields.Append "VAL2", adDouble, , adFldIsNullable
    m_ArProveEqp.Fields.Append "VAL3", adDouble, , adFldIsNullable
    m_ArProveEqp.Fields.Append "TIPOCAL1", adInteger, , adFldIsNullable
    m_ArProveEqp.Fields.Append "CAL1", adVarChar, 3, adFldIsNullable
    m_ArProveEqp.Fields.Append "TIPOCAL2", adInteger, , adFldIsNullable
    m_ArProveEqp.Fields.Append "CAL2", adVarChar, 3, adFldIsNullable
    m_ArProveEqp.Fields.Append "TIPOCAL3", adInteger, , adFldIsNullable
    m_ArProveEqp.Fields.Append "CAL3", adVarChar, 3, adFldIsNullable
    m_ArProveEqp.Fields.Append "NIF", adVarChar, 20, adFldIsNullable
    m_ArProveEqp.Fields.Append "EQP", adVarChar, 3, adFldIsNullable
    m_ArProveEqp.Fields.Append "PROVIDEN", adVarChar, 100, adFldIsNullable
    m_ArProveEqp.Open
    
    While Not g_adoresDG.EOF
        m_ArProveEqp.AddNew
        
        m_ArProveEqp("EQPDEN").Value = g_adoresDG("EQPDEN").Value
        m_ArProveEqp("MONDEN").Value = g_adoresDG("MONDEN").Value
        m_ArProveEqp("PAIDEN").Value = g_adoresDG("PAIDEN").Value
        m_ArProveEqp("COD").Value = g_adoresDG("COD").Value
        m_ArProveEqp("PROVEDEN").Value = g_adoresDG("PROVEDEN").Value
        m_ArProveEqp("DIR").Value = g_adoresDG("DIR").Value
        m_ArProveEqp("CP").Value = g_adoresDG("CP").Value
        m_ArProveEqp("POB").Value = g_adoresDG("POB").Value
        m_ArProveEqp("VAL1").Value = g_adoresDG("VAL1").Value
        m_ArProveEqp("VAL2").Value = g_adoresDG("VAL2").Value
        m_ArProveEqp("VAL3").Value = g_adoresDG("VAL3").Value
        m_ArProveEqp("TIPOCAL1").Value = g_adoresDG("TIPOCAL1").Value
        m_ArProveEqp("CAL1").Value = g_adoresDG("CAL1").Value
        m_ArProveEqp("TIPOCAL2").Value = g_adoresDG("TIPOCAL2").Value
        m_ArProveEqp("CAL2").Value = g_adoresDG("CAL2").Value
        m_ArProveEqp("TIPOCAL3").Value = g_adoresDG("TIPOCAL3").Value
        m_ArProveEqp("CAL3").Value = g_adoresDG("CAL3").Value
        m_ArProveEqp("NIF").Value = g_adoresDG("NIF").Value
        m_ArProveEqp("EQP").Value = g_adoresDG("EQP").Value
        m_ArProveEqp("PROVIDEN").Value = g_adoresDG("PROVIDEN").Value
        
        g_adoresDG.MoveNext
    Wend
    
    oReport.Database.SetDataSource m_ArProveEqp
    Set m_ArProveEqp = Nothing

    ' FORMULA FIELDS REPORT
    
    Set SubListado = oReport.OpenSubreport("rptCON")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
                  
    If bOcultarHomologaciones = False Then
        Set SubListado = oReport.OpenSubreport("rptCAL1")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"
        
        Set SubListado = oReport.OpenSubreport("prtCAL2")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"
        
        Set SubListado = oReport.OpenSubreport("rptCAL3")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"
        
    End If

    
End Function

''' <summary>
''' Crea la Sql con la q obtener los datos
''' </summary>
''' <param name="codEqp">equipos a buscar</param>
''' <param name="CodGMN1">GMN1 a buscar</param>
''' <param name="CodGMN2">GMN2 a buscar</param>
''' <param name="CodGMN3">GMN3 a buscar</param>
''' <param name="CodGMN4">GMN4 a buscar</param>
''' <param name="bOrdCod">Ordenacion</param>
''' <returns>Sql con la q obtener los datos</returns>
''' <remarks>Llamada desde: ListadoProvePorEqp ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 02/11/2012
Private Function ObtenerSQLProvePorEqp(ByVal codEqp As String, ByVal CodGMN1 As String, ByVal CodGMN2 As String, ByVal CodGMN3 As String, ByVal codGMN4 As String, ByVal bOrdCod As Boolean)
    Dim sConsulta As String
    Dim sCampos As String

    sConsulta = "SELECT DISTINCT EQP.DEN AS EQPDEN, MON_DEN.DEN AS MONDEN, PAI_DEN.DEN AS PAIDEN, PROVE.COD AS COD,PROVE.DEN AS PROVEDEN,PROVE.DIR,PROVE.CP,PROVE.POB,PROVE.VAL1,PROVE.VAL2,PROVE.VAL3,PROVE.TIPOCAL1,PROVE.CAL1,PROVE.TIPOCAL2,PROVE.CAL2,PROVE.TIPOCAL3,PROVE.CAL3,PROVE.NIF,PROVE_EQP.EQP AS EQP,PROVI_DEN.DEN AS PROVIDEN FROM"
    sConsulta = sConsulta & "(PROVE_EQP WITH(NOLOCK) INNER JOIN EQP WITH(NOLOCK) ON PROVE_EQP.EQP = EQP.COD) INNER JOIN PROVE WITH(NOLOCK) ON PROVE_EQP.PROVE = PROVE.COD"
    sConsulta = sConsulta & " LEFT JOIN PROVI_DEN WITH(NOLOCK) ON PROVI_DEN.PAI=PROVE.PAI AND PROVI_DEN.PROVI =PROVE.PROVI AND PROVI_DEN.IDIOMA='" & IdiomaInstalacion & "'"
    sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PROVE.PAI AND PAI_DEN.IDIOMA='" & IdiomaInstalacion & "'"
    sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=PROVE.MON AND MON_DEN.IDIOMA='" & IdiomaInstalacion & "'"
    
    If codEqp = "" Then
        If CodGMN1 = "" And CodGMN2 = "" And CodGMN3 = "" And codGMN4 = "" Then
            sConsulta = sConsulta & " WHERE 1=1"
        Else
            If Not codGMN4 = "" Then
                sConsulta = sConsulta & ", PROVE_GMN4 WITH(NOLOCK) WHERE PROVE_GMN4.GMN1='" & DblQuote(CodGMN1) & "' AND PROVE_GMN4.GMN2='" & DblQuote(CodGMN2) & "' AND PROVE_GMN4.GMN3='" & DblQuote(CodGMN3) & "' AND PROVE_GMN4.GMN4='" & DblQuote(codGMN4) & "' AND PROVE_GMN4.PROVE=PROVE_EQP.PROVE"
            Else
                If Not CodGMN3 = "" Then
                    sConsulta = sConsulta & ",PROVE_GMN3 WITH(NOLOCK) WHERE PROVE_GMN3.GMN1='" & DblQuote(CodGMN1) & "' AND PROVE_GMN3.GMN2='" & DblQuote(CodGMN2) & "' AND PROVE_GMN3.GMN3='" & DblQuote(CodGMN3) & "' AND PROVE_GMN3.PROVE=PROVE_EQP.PROVE"
                Else
                    If Not CodGMN2 = "" Then
                        sConsulta = sConsulta & ",PROVE_GMN2 WITH(NOLOCK) WHERE PROVE_GMN2.GMN1='" & DblQuote(CodGMN1) & "' AND PROVE_GMN2.GMN2='" & DblQuote(CodGMN2) & "' AND PROVE_GMN2.PROVE=PROVE_EQP.PROVE"
                    Else
                        sConsulta = sConsulta & ",PROVE_GMN1 WITH(NOLOCK) WHERE PROVE_GMN1.GMN1='" & DblQuote(CodGMN1) & "' AND PROVE_GMN1.PROVE=PROVE_EQP.PROVE"
                    End If
                End If
            End If
            
        End If
    Else
        If CodGMN1 = "" And CodGMN2 = "" And CodGMN3 = "" And codGMN4 = "" Then
            sConsulta = sConsulta & " WHERE PROVE_EQP.EQP='" & DblQuote(codEqp) & "'"
        Else
            If Not codGMN4 = "" Then
                sConsulta = sConsulta & ", PROVE_GMN4 WITH(NOLOCK) WHERE PROVE_GMN4.GMN1='" & DblQuote(CodGMN1) & "' AND PROVE_GMN4.GMN2='" & DblQuote(CodGMN2) & "' AND PROVE_GMN4.GMN3='" & DblQuote(CodGMN3) & "' AND PROVE_GMN4.GMN4='" & DblQuote(codGMN4) & "' AND PROVE_GMN4.PROVE=PROVE_EQP.PROVE AND PROVE_EQP.EQP='" & DblQuote(codEqp) & "'"
           Else
                If Not CodGMN3 = "" Then
                    sConsulta = sConsulta & ",PROVE_GMN3 WITH(NOLOCK) WHERE PROVE_GMN3.GMN1='" & DblQuote(CodGMN1) & "' AND PROVE_GMN3.GMN2='" & DblQuote(CodGMN2) & "' AND PROVE_GMN3.GMN3='" & DblQuote(CodGMN3) & "' AND PROVE_GMN3.PROVE=PROVE_EQP.PROVE AND PROVE_EQP.EQP='" & DblQuote(codEqp) & "'"
                Else
                    If Not CodGMN2 = "" Then
                        sConsulta = sConsulta & ",PROVE_GMN2 WITH(NOLOCK) WHERE PROVE_GMN2.GMN1='" & DblQuote(CodGMN1) & "' AND PROVE_GMN2.GMN2='" & DblQuote(CodGMN2) & "' AND PROVE_GMN2.PROVE=PROVE_EQP.PROVE AND PROVE_EQP.EQP='" & DblQuote(codEqp) & "'"
                    Else
                        sConsulta = sConsulta & ",PROVE_GMN1 WITH(NOLOCK) WHERE PROVE_GMN1.GMN1='" & DblQuote(CodGMN1) & "' AND PROVE_GMN1.PROVE=PROVE_EQP.PROVE AND PROVE_EQP.EQP='" & DblQuote(codEqp) & "'"
                    End If
                End If
            End If
            
        End If
    End If
    'orden
    If bOrdCod Then
       sConsulta = sConsulta & " ORDER BY PROVE_EQP.EQP,PROVE.COD"
    Else
       sConsulta = sConsulta & " ORDER BY EQP.DEN,PROVE.DEN"
    End If

    ObtenerSQLProvePorEqp = sConsulta
        
End Function

