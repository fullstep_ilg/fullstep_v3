Attribute VB_Name = "basPublic"
Option Explicit

Public Declare Function BringWindowToTop Lib "user32" (ByVal hWnd As Long) As Long

Public ocrs_crapp As CRAXDRT.Application
Public scrs_Server As String
Public scrs_Database As String
Public bcrs_Connected As Boolean
Public scrs_User As String
Public scrs_Password As String

Public Function DevolverDenGMN(ByVal bSincronizacionMat As Boolean, ByVal IdiomaPortal As String) As String
    If bSincronizacionMat Then
        DevolverDenGMN = "DEN_" & IdiomaPortal
    Else
        DevolverDenGMN = "DEN"
    End If
End Function

'edu
'nueva funcion que reconencta el origen de datos de un report a la base de datos de explotación
'Soluciona error de migracion de a Crystal Reports 10.
'por el cual los parametros de conexion suministrados desde VB no funcionaban para esta versión de CR.
'fin edu
'Lo de edu como que no tira si intentas cambiar de bbdd o user id o ...
Public Sub ConectarReportInterno(Listado As CRAXDRT.Report, Server As String, Database As String, User As String, Password As String)
    Dim Table As CRAXDRT.DatabaseTable
    Dim CPProperty As CRAXDRT.ConnectionProperty
    Dim SubListado As CRAXDRT.Report
    Dim Section As CRAXDRT.Section
    Dim ReportObject As Object
    Dim FieldObject As CRAXDRT.FieldObject

    For Each Table In Listado.Database.Tables
        On Error GoTo ErrorVario:
        
        Table.SetLogOnInfo Server, Database, User, Password

        Set CPProperty = Table.ConnectionProperties("User ID")
        CPProperty.Value = User
        Set CPProperty = Table.ConnectionProperties("Password")
        CPProperty.Value = Password
    
        If Table.DatabaseType = crSQLDatabase Then 'ODBC o OLEDB
            Set CPProperty = Table.ConnectionProperties("Provider")
            If Not (CPProperty Is Nothing) Then
                If CPProperty.Value = "SQLOLEDB" Then 'Quizas mejor <> "" por si cristal 11 cambio string
                    'conexion por OLEDB.
                    Set CPProperty = Table.ConnectionProperties("Data Source")
                    CPProperty.Value = Server
                    Set CPProperty = Table.ConnectionProperties("Initial Catalog")
                    CPProperty.Value = Database
                Else
                    'conexion por ODBC
                    Set CPProperty = Table.ConnectionProperties("DSN")
                    CPProperty.Value = "ODBCSQL"
                    Set CPProperty = Table.ConnectionProperties("Database")
                    CPProperty.Value = Database
                    Set CPProperty = Table.ConnectionProperties("Server")
                    CPProperty.Value = Server
                    Set CPProperty = Table.ConnectionProperties("Connection String")
                    CPProperty.Value = "DRIVER=SQL Server;UID=" & User & ";PWD=" & Password
                End If
            Else
                'conexion por ODBC
                Set CPProperty = Table.ConnectionProperties("DSN")
                CPProperty.Value = "ODBCSQL"
                Set CPProperty = Table.ConnectionProperties("Database")
                CPProperty.Value = Database
                Set CPProperty = Table.ConnectionProperties("Server")
                CPProperty.Value = Server
                Set CPProperty = Table.ConnectionProperties("Connection String")
                CPProperty.Value = "DRIVER=SQL Server;UID=" & User & ";PWD=" & Password
            End If
        Else
            'ttx
            Exit Sub
        End If
    
        On Error GoTo ErrorTableLocation:
        If Table.Location <> "Command" Then
            Table.Location = scrs_Database & ".dbo." & Table.Location
        End If
    
        For Each Section In Listado.Sections
            For Each ReportObject In Section.ReportObjects
                If ReportObject.Kind = crSubreportObject Then
                    Set SubListado = Listado.OpenSubreport(ReportObject.SubreportName)
                    
                    ConectarReportInterno SubListado, Server, Database, User, Password
                End If
            Next
        Next

        On Error GoTo 0
    
    Next
    
    Exit Sub
ErrorTableLocation:
    'En un mismo report 2 veces ART4 uno name ART4 otro ART4_1  "FSGS_31600_8.dbo.ART4_1" NO ES VALIDO
    'En un mismo report 6 veces COPIA_CAMPO ninguno name COPIA_CAMPO todos COPIA_CAMPO_Algo. "FSGS_31600_8.dbo.COPIA_CAMPO_Algo" NO ES VALIDO
    Resume Next 'Ya no entra nunca
ErrorVario:
    Set CPProperty = Nothing
    Resume Next
End Sub

Public Function crs_FormulaIndex_Interno(rpt As CRAXDRT.Report, formulaname As String)

    Dim i As Integer
    
    crs_FormulaIndex_Interno = 0
    
    For i = 1 To rpt.FormulaFields.Count
        If rpt.FormulaFields(i).FormulaFieldName = formulaname Then
            crs_FormulaIndex_Interno = i
            Exit For
        End If
    Next i
    
End Function

Public Function crs_SortTable_Interno(sortstr As String) As String
    Dim f As Integer
    
    f = InStr(3, sortstr, ".")
    
    crs_SortTable_Interno = Mid(sortstr, 3, f - 3)
End Function

Public Function crs_SortField_Interno(sortstr As String) As String
    Dim i As Integer, f As Integer
    
    i = InStr(3, sortstr, ".") + 1
    f = InStr(i, sortstr, "}")
    
    crs_SortField_Interno = Mid(sortstr, i, f - i)
End Function

Public Function crs_FieldIndex_Interno(rpt As CRAXDRT.Report, tablename As String, fieldname As String)
    Dim i As Integer
    
    Dim Tableindex As Integer
    
    Tableindex = 0
    
    For i = 1 To rpt.Database.Tables.Count
        If rpt.Database.Tables(i).Name = tablename Then
            Tableindex = i
            Exit For
        End If
    Next i
    
    crs_FieldIndex_Interno = 0
    
    For i = 1 To rpt.Database.Tables(Tableindex).Fields.Count
        If rpt.Database.Tables(Tableindex).Fields(i).DatabaseFieldName = fieldname Then
            crs_FieldIndex_Interno = i
            Exit For
        End If
    Next i
End Function

Public Function crs_Tableindex_Interno(rpt As CRAXDRT.Report, tablename As String) As Integer
    Dim i As Integer
    
    crs_Tableindex_Interno = 0
    
    For i = 1 To rpt.Database.Tables.Count
        If rpt.Database.Tables(i).Name = tablename Then
            crs_Tableindex_Interno = i
            Exit For
        End If
    Next i
End Function

Public Function crs_SortDirection_Interno(sortstr As String) As CRSortDirection
    Select Case Left(sortstr, 1)

    Case "+"
        crs_SortDirection_Interno = crAscendingOrder
    Case "-"
        crs_SortDirection_Interno = crDescendingOrder
    
    End Select
End Function

Public Function crs_ParameterIndex_Interno(rpt As CRAXDRT.Report, parametername As String)
    Dim i As Integer
    
    crs_ParameterIndex_Interno = 0
    
    For i = 1 To rpt.ParameterFields.Count
        If rpt.ParameterFields(i).ParameterFieldName = parametername Then
            crs_ParameterIndex_Interno = i
            Exit For
        End If
    Next i
End Function

Public Function construirRestriccionPorUonsAtrib(oUons As CUnidadesOrganizativas, Optional ByVal bNoMostrarDescendientes As Boolean) As String
    'filtrar por un conjunto de uons
    Dim swhereuons As String
    If oUons.Count > 0 Then
    
        swhereuons = " AND ( 1=2 "
        Dim oUON As Variant
        For Each oUON In oUons
            If oUON.Nivel = 3 Then
                'Nivel 3
                swhereuons = swhereuons & " OR DEF_ATRIB.ID IN ("
                swhereuons = swhereuons & " SELECT DISTINCT UA.ATRIB"
                swhereuons = swhereuons & " FROM UON_ATRIB UA WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE UA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND UA.UON2 ='" & oUON.CodUnidadOrgNivel2 & "' AND UA.UON3 ='" & oUON.Cod & "' )"
                
            ElseIf oUON.Nivel = 2 Then
                'NIVEL 2
                swhereuons = swhereuons & " OR DEF_ATRIB.ID IN ("
                swhereuons = swhereuons & " SELECT DISTINCT UA.ATRIB"
                swhereuons = swhereuons & " FROM UON_ATRIB UA WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE UA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND UA.UON2 ='" & oUON.Cod & "' "
                If bNoMostrarDescendientes Then
                    swhereuons = swhereuons & " AND UA.UON3 IS NULL "
                End If
                swhereuons = swhereuons & ")"
                
            ElseIf oUON.Nivel = 1 Then
                swhereuons = swhereuons & " OR DEF_ATRIB.ID IN ("
                swhereuons = swhereuons & " SELECT DISTINCT UA.ATRIB"
                swhereuons = swhereuons & " FROM UON_ATRIB UA WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE UA.UON1='" & oUON.CodUnidadOrgNivel1 & "'"
                If bNoMostrarDescendientes Then
                    swhereuons = swhereuons & " AND UA.UON2 IS NULL "
                End If
                swhereuons = swhereuons & ") "
            End If
            
        Next
        swhereuons = swhereuons & " )"
    End If
    construirRestriccionPorUonsAtrib = swhereuons
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para mantenimiento de materiales</summary>
''' <param name="sUONDepAlias">Alias tabla UON_DEP</param>
''' <param name="iNivel">Nivel</param>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfUONArt(ByVal bRUsuUON As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    
    Dim iNivel As Integer
    iNivel = 3
    
    If UON3 = "" Then
        iNivel = 2
        If UON2 = "" Then
            iNivel = 1
            If UON1 = "" Then
                'nivel 0
                iNivel = 0
            End If
        End If
    End If
       
    If iNivel > 0 Then
        If bRUsuUON Then
            sConsulta1 = "SELECT AU.ART4 "
            sConsulta1 = sConsulta1 & " FROM ART4_UON AU WITH (NOLOCK) "
            sConsulta1 = sConsulta1 & " WHERE AU.UON1='" & DblQuote(UON1) & "'"
            If iNivel > 1 Then
                sConsulta1 = sConsulta1 & " AND AU.UON2='" & DblQuote(UON2) & "'"
                If iNivel = 3 Then
                    sConsulta1 = sConsulta1 & " AND AU.UON3='" & DblQuote(UON3) & "'"
                End If
            End If
        End If
    End If
    
    If bRPerfUON Then
        sConsulta2 = " SELECT A.ART4 "
        sConsulta2 = sConsulta2 & " FROM ART4_UON A WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= A.UON1 AND PU.UON2 IS NULL AND PU.PERF=" & lIdPerfil & " "
        sConsulta2 = sConsulta2 & " Union"
        sConsulta2 = sConsulta2 & " SELECT A.ART4"
        sConsulta2 = sConsulta2 & " FROM ART4_UON A WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= A.UON1 AND  PU.UON2= A.UON2 AND PU.UON3 IS NULL AND PU.PERF=" & lIdPerfil & " "
        sConsulta2 = sConsulta2 & " Union"
        sConsulta2 = sConsulta2 & " SELECT A.ART4"
        sConsulta2 = sConsulta2 & " FROM ART4_UON A WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= A.UON1 AND PU.UON2= A.UON2 AND PU.UON3= A.UON3 AND PU.PERF=" & lIdPerfil & " "
    End If

    If bRUsuUON Then
        sConsulta = sConsulta1
    End If
        
    If bRPerfUON Then
        If bRUsuUON Then
            sConsulta = sConsulta1 & " UNION " & sConsulta2
        Else
            sConsulta = sConsulta2
        End If
    End If
    ConstruirCriterioUONUsuPerfUONArt = sConsulta
End Function

'<summary>filtra por uons en una determinada selección (pertenencia a cualquiera de ellas)</summary>
'<param name="oUons">Colección de uons de cualquier nivel</param>
'<param name="bIncluirAscendientes">Indica si debemos incluir los ascendientes de una uon </param>
Public Function construirRestriccionPorUons(oUons As CUnidadesOrganizativas, Optional ByVal bIncluirAscendientes As Boolean, Optional ByVal bIncluirDescendientes As Boolean) As String
    'filtrar por un conjunto de uons
    Dim swhereuons As String
    Dim oUON As IUon
    If Not oUons Is Nothing Then
    If oUons.Count > 0 Then
    
        swhereuons = " AND ((1=2 "
        
        For Each oUON In oUons
            If oUON.Nivel = 3 Then
                'Nivel 3
                swhereuons = swhereuons & " OR AU.ART4 IN ("
                swhereuons = swhereuons & " SELECT DISTINCT AU2.ART4"
                swhereuons = swhereuons & " FROM ART4_UON AU2 WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE ((AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "' AND AU2.UON2 ='" & DblQuote(oUON.CodUnidadOrgNivel2) & "' AND AU2.UON3 ='" & DblQuote(oUON.CodUnidadOrgNivel3) & "' )"
                If bIncluirAscendientes Then
                    swhereuons = swhereuons & " OR  (AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "' AND AU2.UON2 IS NULL)"
                    swhereuons = swhereuons & " OR  (AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "' AND AU2.UON2 ='" & DblQuote(oUON.CodUnidadOrgNivel2) & "' AND AU2.UON3 IS NULL)"
                End If
                swhereuons = swhereuons & "))"
                
            ElseIf oUON.Nivel = 2 Then
                'NIVEL 2
                swhereuons = swhereuons & " OR AU.ART4 IN ("
                swhereuons = swhereuons & " SELECT DISTINCT AU2.ART4"
                swhereuons = swhereuons & " FROM ART4_UON AU2 WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE ((AU2.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AU2.UON2 ='" & oUON.CodUnidadOrgNivel2 & "' "
                If Not bIncluirDescendientes Then
                    swhereuons = swhereuons & " AND AU2.UON3 IS NULL"
                End If
                swhereuons = swhereuons & ")"
                If bIncluirAscendientes Then
                    swhereuons = swhereuons & " OR (AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "' AND AU2.UON2 IS NULL)"
                End If
                swhereuons = swhereuons & "))"
            ElseIf oUON.Nivel = 1 Then
                swhereuons = swhereuons & " OR AU.ART4 IN ("
                swhereuons = swhereuons & " SELECT DISTINCT AU2.ART4"
                swhereuons = swhereuons & " FROM ART4_UON AU2 WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "'"
                If Not bIncluirDescendientes Then
                    swhereuons = swhereuons & " AND AU2.UON2 IS NULL"
                End If
                swhereuons = swhereuons & ") "
            End If
            
        Next
        swhereuons = swhereuons & " ))"
    End If
    End If
    construirRestriccionPorUons = swhereuons
End Function


Public Function construirFiltroArticulosPorAtributo(ByRef oAtributos As CAtributos, Optional ByRef oUons As CUnidadesOrganizativas, Optional ByVal bIncluirDescendientes As Boolean) As String
    Dim sConsulta As String
    Dim oUON As IUon
    If Not oAtributos Is Nothing Then
        Dim oAtributo As CAtributo
        If oAtributos.Count > 0 Then
            For Each oAtributo In oAtributos
                Dim sWhereValor As String
                Select Case oAtributo.Tipo
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorText) Then
                            sWhereValor = sWhereValor & " AND AA.VALOR_TEXT LIKE '" & DblQuote(oAtributo.ValorText) & "%'"
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorNum) Then
                            sWhereValor = sWhereValor & " AND AA.VALOR_NUM" & oAtributo.Formula & oAtributo.ValorNum
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorFec) Then
                            sWhereValor = sWhereValor & " AND AA.VALOR_FEC=" & DateToSQLTimeDate(oAtributo.ValorFec)
                        End If
                    Case TiposDeAtributos.TipoBoolean
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorBool) Then
                            sWhereValor = sWhereValor & " AND AA.VALOR_BOOL=" & BooleanToSQLBinary(oAtributo.ValorBool)
                        End If
                End Select
                
                If oAtributo.AmbitoAtributo <> TAmbitoUon Then
                    sConsulta = sConsulta & " AND (EXISTS(SELECT AA.ART"
                    sConsulta = sConsulta & " FROM ART4_ATRIB AA WITH (NOLOCK)"
                    sConsulta = sConsulta & " WHERE AA.ART=ART4.COD"
                    sConsulta = sConsulta & " AND AA.ATRIB=" & oAtributo.Id
                    If sWhereValor <> "" Then
                        sConsulta = sConsulta & sWhereValor
                    End If
                    sConsulta = sConsulta & "))"
                End If
            
                'atributos UON
                If oAtributo.AmbitoAtributo = TAmbitoUon Then
                    For Each oUON In oUons
                        sConsulta = sConsulta & " AND ART4.COD IN ( "
                        sConsulta = sConsulta & _
                            " SELECT AA.ART FROM ART4_UON_ATRIB AA WITH(NOLOCK) WHERE AA.ATRIB= " _
                            & oAtributo.Id
                        If oUON.Nivel = 1 Then
                            sConsulta = sConsulta & " AND AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' "
                            If Not bIncluirDescendientes Then
                                sConsulta = sConsulta & " AND AA.UON2 IS NULL"
                            End If
                        End If
                            
                        If oUON.Nivel = 2 Then
                            sConsulta = sConsulta & " AND ((AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2='" & oUON.CodUnidadOrgNivel2 & "' "
                            If Not bIncluirDescendientes Then
                                sConsulta = sConsulta & " AND AA.UON3 IS NULL)"
                            End If
                            sConsulta = sConsulta & " OR (AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2 IS NULL))"
                        End If
                        
                        If oUON.Nivel >= 3 Then
                            sConsulta = sConsulta & " AND (AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2 IS NULL"
                            sConsulta = sConsulta & " OR AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2='" & oUON.CodUnidadOrgNivel2 & "' AND AA.UON3 IS NULL"
                            sConsulta = sConsulta & " OR AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2='" & oUON.CodUnidadOrgNivel2 & "' AND AA.UON3='" & oUON.CodUnidadOrgNivel3 & "')"
                        End If
                        If sWhereValor <> "" Then
                            sConsulta = sConsulta & sWhereValor
                        End If
                        sConsulta = sConsulta & ")"
                    Next
                End If
                
            Next
        End If
    End If
    construirFiltroArticulosPorAtributo = sConsulta
End Function
