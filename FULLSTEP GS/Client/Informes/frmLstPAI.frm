VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPAI 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de pa�ses (Opciones)"
   ClientHeight    =   2040
   ClientLeft      =   1305
   ClientTop       =   3165
   ClientWidth     =   4815
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPAI.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2040
   ScaleWidth      =   4815
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   4815
      TabIndex        =   4
      Top             =   1665
      Width           =   4815
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   3480
         TabIndex        =   3
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1635
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   2884
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Opciones"
      TabPicture(0)   =   "frmLstPAI.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstPAI.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   105
         TabIndex        =   7
         Top             =   420
         Width           =   4575
         Begin VB.CheckBox chkIncluirMoneda 
            Caption         =   "Incluir moneda"
            Height          =   240
            Left            =   390
            TabIndex        =   0
            Top             =   405
            Width           =   4050
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -74895
         TabIndex        =   6
         Top             =   420
         Width           =   4575
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   120
            TabIndex        =   2
            Top             =   600
            Width           =   3750
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   120
            TabIndex        =   1
            Top             =   240
            Value           =   -1  'True
            Width           =   3840
         End
      End
   End
End
Attribute VB_Name = "frmLstPAI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Mensajes As CMensajes
Public ParamInstRPTPATH As String
Public ParamGenRPTPATH As String
Public Report As CRAXDRT.Report
Public GestorIdiomas As CGestorIdiomas
Public GuardarParametrosIns As Boolean
Public Titulo As String  'preview
Public Idioma As String

'Variables de idioma
Private txtPag As String
Private txtDe As String
Private txtCod As String
Private txtDen As String
Private txtMon As String

Private Sub cmdObtener_Click()
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim SelectionText As String
    Dim oCRPaises As New CRPaises
       
    If Not bcrs_Connected Then Exit Sub
    
    If ParamInstRPTPATH = "" Then
        If ParamGenRPTPATH = "" Then
            Mensajes.RutaDeRPTNoValida
            Set Report = Nothing
            Exit Sub
        Else
            ParamInstRPTPATH = ParamGenRPTPATH
            GuardarParametrosIns = True
        End If
    End If
    RepPath = ParamInstRPTPATH & "\rptPAI.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Mensajes.RutaDeRPTNoValida
        Set Report = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set Report = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    ConectarReportInterno Report, scrs_Server, scrs_Database, scrs_User, scrs_Password
    
    SelectionText = "S"
    If chkIncluirMoneda = vbChecked Then SelectionText = "N"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "SEL")).Text = """" & SelectionText & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtTitulo")).Text = """" & Titulo & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtPag")).Text = """" & txtPag & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDe")).Text = """" & txtDe & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtCod")).Text = """" & txtCod & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDen")).Text = """" & txtDen & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtMon")).Text = """" & txtMon & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "IDI")).Text = """" & Idioma & """"
    
    oCRPaises.Listado Report, opOrdDen
    If Report Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Load()
    Me.Width = 4905
    Me.Height = 2415
    
    CargarRecursos
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_LSTPAI, Idioma)
   
    If Not Ador Is Nothing Then
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value     '2
        Ador.MoveNext
        cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        chkIncluirMoneda.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.Caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.Caption = Ador(0).Value
        'Idiomas del RPT
        Ador.MoveNext
        Titulo = Ador(0).Value '200
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtCod = Ador(0).Value
        Ador.MoveNext
        txtDen = Ador(0).Value
        Ador.MoveNext
        txtMon = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub


