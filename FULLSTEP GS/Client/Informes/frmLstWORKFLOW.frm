VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmLstWORKFLOW 
   Caption         =   "Listado de workflows"
   ClientHeight    =   3720
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7230
   Icon            =   "frmLstWORKFLOW.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3720
   ScaleWidth      =   7230
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener+"
      Height          =   375
      Left            =   5865
      TabIndex        =   1
      Top             =   3315
      Width           =   1335
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3270
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7200
      _ExtentX        =   12700
      _ExtentY        =   5768
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstWORKFLOW.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "sdbcBloqueSeguridad"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "sdbcWorkflow"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "optWorkflows"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "optBloques"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      Begin VB.OptionButton optBloques 
         Caption         =   "Bloques de seguridad"
         Height          =   195
         Left            =   240
         TabIndex        =   3
         Top             =   1830
         Width           =   4410
      End
      Begin VB.OptionButton optWorkflows 
         Caption         =   "Workflows"
         Height          =   195
         Left            =   240
         TabIndex        =   2
         Top             =   765
         Value           =   -1  'True
         Width           =   4410
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcWorkflow 
         Height          =   285
         Left            =   2535
         TabIndex        =   4
         Top             =   1275
         Width           =   4020
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2434
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7726
         Columns(1).Caption=   "Denominacion"
         Columns(1).Name =   "Denominacion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Tipo"
         Columns(2).Name =   "Tipo"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   2
         Columns(2).FieldLen=   256
         _ExtentX        =   7091
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcBloqueSeguridad 
         Height          =   285
         Left            =   2535
         TabIndex        =   6
         Top             =   2370
         Width           =   4020
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2434
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7726
         Columns(1).Caption=   "Denominacion"
         Columns(1).Name =   "Denominacion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Tipo"
         Columns(2).Name =   "Tipo"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   2
         Columns(2).FieldLen=   256
         _ExtentX        =   7091
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label Label2 
         Caption         =   "Bloque de seguridad:"
         Height          =   285
         Left            =   795
         TabIndex        =   7
         Top             =   2370
         Width           =   1725
      End
      Begin VB.Label Label1 
         Caption         =   "Workflow:"
         Height          =   285
         Left            =   795
         TabIndex        =   5
         Top             =   1275
         Width           =   1125
      End
   End
End
Attribute VB_Name = "frmLstWORKFLOW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public giOrigen As Integer
Public g_ador  As ADODB.Recordset
Public g_adorSub  As ADODB.Recordset
Public Mensajes As CMensajes
Public ParamInstRPTPATH As String
Public ParamGenRPTPATH As String
Public Report As CRAXDRT.Report
Public GestorIdiomas As CGestorIdiomas
Public GuardarParametrosIns As Boolean
Public Titulo As String  'preview
Public Idioma As String
Public GestorInformes As CGestorInformes
Public Raiz As CRaiz

Dim bCargarComboDesde As Boolean
Dim bRespetarComboWorkflows As Boolean
Dim bRespetarComboBloquesSeguridad As Boolean


    
Private sIdiTxtTitulo As String
Private sIdiTxtSeleccion As String
Private sIdiTxtTodos As String

Private sIdiTxtPag As String
Private sIdiTxtDe As String
Private sIdiTxtDen As String
Private sIdiTxtOrden As String
Private sIdiTxtLimite As String
Private sIdiTxtNombre As String
Private sIdiTxtTituloBloques As String
Private sIdiTxtTodosBloques As String
Private sEspera(1 To 3) As String
Private sIdiTxtNotif As String

    
Private Sub cmdObtener_Click()
    If Me.optWorkflows.Value = True Then
        ObtenerListadoWorkflow
    ElseIf Me.optBloques.Value = True Then
        ObtenerListadoBloques
    End If
End Sub

Private Sub ObtenerListadoWorkflow()
    ''' * Objetivo: Obtener un listado de solicitudes
    
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim SelectionText As String
    Dim SubListado As CRAXDRT.Report
    
    If bcrs_Connected = False Then
        Exit Sub
    End If
    
    If ParamInstRPTPATH = "" Then
        If ParamGenRPTPATH = "" Then
            Mensajes.RutaDeRPTNoValida
            Set Report = Nothing
            Exit Sub
        Else
            ParamInstRPTPATH = ParamGenRPTPATH
            GuardarParametrosIns = True
        End If
    End If
    
    RepPath = ParamInstRPTPATH & "\rptWorkflows.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Mensajes.RutaDeRPTNoValida
        Set Report = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set Report = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    
    If Me.sdbcWorkflow.Value = "" Then
        SelectionText = sIdiTxtTodos
    Else
        SelectionText = Me.sdbcWorkflow.Text
    End If
    
    
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "SEL")).Text = """" & SelectionText & """"
    
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtTitulo")).Text = """" & sIdiTxtTitulo & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtPag")).Text = """" & sIdiTxtPag & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDe")).Text = """" & sIdiTxtDe & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtSeleccion")).Text = """" & sIdiTxtSeleccion & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDen")).Text = """" & sIdiTxtDen & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDenBloque")).Text = """" & sIdiTxtDen & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtOrden")).Text = """" & sIdiTxtOrden & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtLimite")).Text = """" & sIdiTxtLimite & """"
    
    Set g_ador = GestorInformes.ListadoWorkflows(Me.sdbcWorkflow.Columns("ID").Value)
    If g_ador Is Nothing Then
        Screen.MousePointer = vbNormal
        Mensajes.NoHayDatos
        Set Report = Nothing
        Exit Sub
    Else
        Report.Database.SetDataSource g_ador
    End If
        
    'subListado
    Set SubListado = Report.OpenSubreport("rptSubNotifPasos")
    SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, "txtNotif")).Text = """" & sIdiTxtNotif & """"
    Set g_adorSub = GestorInformes.ListadoPasosNotificadores
    If Not g_adorSub Is Nothing Then
        SubListado.Database.SetDataSource g_adorSub
    End If
    Set SubListado = Nothing

    Me.Hide
    Screen.MousePointer = vbNormal

End Sub

Private Sub ObtenerListadoBloques()
    ''' * Objetivo: Obtener un listado de solicitudes
    
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim SelectionText As String
    Dim Report As CRAXDRT.Report
    
    If bcrs_Connected = False Then
        Exit Sub
    End If
    
    If ParamInstRPTPATH = "" Then
        If ParamGenRPTPATH = "" Then
            Mensajes.RutaDeRPTNoValida
            Set Report = Nothing
            Exit Sub
        Else
            ParamInstRPTPATH = ParamGenRPTPATH
            GuardarParametrosIns = True
        End If
    End If
    
    RepPath = ParamInstRPTPATH & "\rptBloquesSeguridad.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Mensajes.RutaDeRPTNoValida
        Set Report = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set Report = ocrs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    
    SelectionText = ""
    
    If Me.sdbcBloqueSeguridad.Value = "" Then
        SelectionText = sIdiTxtTodosBloques
    Else
        SelectionText = Me.sdbcBloqueSeguridad.Text
    End If
    
    
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "SEL")).Text = """" & SelectionText & """"
    
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtTitulo")).Text = """" & sIdiTxtTituloBloques & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtPag")).Text = """" & sIdiTxtPag & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDe")).Text = """" & sIdiTxtDe & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtSeleccion")).Text = """" & sIdiTxtSeleccion & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtDen")).Text = """" & sIdiTxtDen & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtNombre")).Text = """" & sIdiTxtNombre & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtOrden")).Text = """" & sIdiTxtOrden & """"
    Report.FormulaFields(crs_FormulaIndex_Interno(Report, "txtLimite")).Text = """" & sIdiTxtLimite & """"
    
    Set g_ador = GestorInformes.ListadoBloquesSeguridad(Me.sdbcBloqueSeguridad.Columns("ID").Value)
    If g_ador Is Nothing Then
        Screen.MousePointer = vbNormal
        Mensajes.NoHayDatos
        Set Report = Nothing
        Exit Sub
    Else
        Report.Database.SetDataSource g_ador
    End If
        
    Me.Hide
    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Load()
CargarRecursos
Select Case giOrigen
    Case 0
        Me.optBloques.Value = False
        Me.optWorkflows.Value = True
    Case 1
        Me.optWorkflows.Value = False
        Me.optBloques.Value = True
End Select
    
    
End Sub





Private Sub sdbcWorkflow_Change()
    Me.sdbcWorkflow.Columns("ID").Value = Null
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_LST_WORKFLOWS, Idioma)
    
    If Not Ador Is Nothing Then
        
        Me.Caption = Ador(0).Value
        sIdiTxtTitulo = Ador(0).Value
        Ador.MoveNext
        sIdiTxtSeleccion = Ador(0).Value
        Ador.MoveNext
        sIdiTxtTodos = Ador(0).Value
        Ador.MoveNext
        sIdiTxtPag = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDe = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDen = Ador(0).Value
        Ador.MoveNext
        sIdiTxtLimite = Ador(0).Value
        Ador.MoveNext
        sIdiTxtOrden = Ador(0).Value
        Ador.MoveNext
        sIdiTxtNombre = Ador(0).Value
        Ador.MoveNext
        sIdiTxtTituloBloques = Ador(0).Value
        Ador.MoveNext
        sIdiTxtTodosBloques = Ador(0).Value
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        Ador.MoveNext
        optWorkflows.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optBloques.Caption = Ador(0).Value
        Ador.MoveNext
        Label1.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        Label2.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        Me.cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        Me.SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sIdiTxtNotif = Ador(0).Value '21 Notificados
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    

End Sub

Private Sub sdbcWorkflow_Click()
     
     If Not sdbcWorkflow.DroppedDown Then
        sdbcWorkflow = ""
        Me.sdbcWorkflow.Columns("ID").Value = Null
        bCargarComboDesde = False
    End If
End Sub

Private Sub sdbcWorkflow_PositionList(ByVal Text As String)

'    ''' * Objetivo: Posicionarse en el combo segun la seleccion
'
'    Dim i As Long
'    Dim bm As Variant
'
'    On Error Resume Next
'
'    sdbcWorkflow.MoveFirst
'
'    If Text <> "" Then
'        For i = 0 To sdbcWorkflow.Rows - 1
'            bm = sdbcWorkflow.GetBookmark(i)
'            If UCase(Text) = UCase(Mid(sdbcWorkflow.Columns(0).CellText(bm), 1, Len(Text))) Then
'                sdbcWorkflow.Bookmark = bm
'                Exit For
'            End If
'        Next i
'    End If

End Sub

Private Sub sdbcWorkflow_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oRS As ADODB.Recordset
    Dim oWorkflows As CWorkflows
    

    If sdbcWorkflow.Text = "" Then Exit Sub

    Screen.MousePointer = vbHourglass
    Set oWorkflows = Raiz.generar_CWorkflows
    Set oRS = oWorkflows.DevolverWorkflows(, sdbcWorkflow.Text)


    bExiste = Not (oRS.RecordCount = 0)
    
    If bExiste Then bExiste = (UCase(oRS.Fields("DEN").Value) = UCase(sdbcWorkflow.Text))

    Screen.MousePointer = vbNormal

    If Not bExiste Then
        sdbcWorkflow.Text = ""
        Me.sdbcWorkflow.Columns("ID").Value = Null
    Else
        bRespetarComboWorkflows = True
        sdbcWorkflow.Text = oRS.Fields("DEN").Value
        sdbcWorkflow.Columns("ID").Value = oRS.Fields("ID").Value
        bRespetarComboWorkflows = False
    End If
    oRS.Close
    Set oRS = Nothing
    bCargarComboDesde = True
    Set oWorkflows = Nothing
    Me.optWorkflows.Value = True
    
End Sub


Private Sub sdbcWorkflow_CloseUp()
    
    If sdbcWorkflow.Value = "..." Then
        sdbcWorkflow.Text = ""
        Exit Sub
    End If
    
    If sdbcWorkflow.Value = "" Then Exit Sub
    
    bRespetarComboWorkflows = True
    sdbcWorkflow.Text = sdbcWorkflow.Columns(1).Text
    bRespetarComboWorkflows = False
    bCargarComboDesde = False
    DoEvents
        
    Me.optWorkflows.Value = True
End Sub
Private Sub sdbcWorkflow_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim oIasig As IAsignaciones
    Dim sCod As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oWorkflows As CWorkflows
    Dim oRS As ADODB.Recordset
    
    sdbcWorkflow.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oWorkflows = Nothing
    Set oWorkflows = Raiz.generar_CWorkflows
    
    If bCargarComboDesde Then
        sCod = Trim(sdbcWorkflow.Text)
    Else
        sCod = ""
    End If
        
        
    Set oRS = oWorkflows.DevolverWorkflows
    
    While Not oRS.EOF
        sdbcWorkflow.AddItem oRS.Fields("ID").Value & vbTab & oRS.Fields("DEN").Value
        oRS.MoveNext
    Wend
    oRS.Close

    sdbcWorkflow.SelStart = 0
    sdbcWorkflow.SelLength = Len(sdbcWorkflow.Text)
    sdbcWorkflow.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
    
End Sub

Private Sub sdbcWorkflow_InitColumnProps()
    sdbcWorkflow.DataFieldList = "Column 0"
    sdbcWorkflow.DataFieldToDisplay = "Column 1"
End Sub




Private Sub sdbcBloqueSeguridad_Click()
     
     If Not sdbcBloqueSeguridad.DroppedDown Then
        sdbcBloqueSeguridad = ""
        Me.sdbcBloqueSeguridad.Columns("ID").Value = Null
        bCargarComboDesde = False
    End If
End Sub

Private Sub sdbcBloqueSeguridad_PositionList(ByVal Text As String)

'    ''' * Objetivo: Posicionarse en el combo segun la seleccion
'
'    Dim i As Long
'    Dim bm As Variant
'
'    On Error Resume Next
'
'    sdbcBloqueSeguridad.MoveFirst
'
'    If Text <> "" Then
'        For i = 0 To sdbcBloqueSeguridad.Rows - 1
'            bm = sdbcBloqueSeguridad.GetBookmark(i)
'            If UCase(Text) = UCase(Mid(sdbcBloqueSeguridad.Columns(0).CellText(bm), 1, Len(Text))) Then
'                sdbcBloqueSeguridad.Bookmark = bm
'                Exit For
'            End If
'        Next i
'    End If

End Sub

Private Sub sdbcBloqueSeguridad_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oRS As ADODB.Recordset
    Dim oBloquesSeguridad As CBloquesSeguridad
    

    If sdbcBloqueSeguridad.Text = "" Then Exit Sub

    Screen.MousePointer = vbHourglass
    Set oBloquesSeguridad = Raiz.generar_CBloquesSeguridad
    Set oRS = oBloquesSeguridad.DevolverBloquesSeguridad(False, , sdbcBloqueSeguridad.Text)


    bExiste = Not (oRS.RecordCount = 0)
    
    If bExiste Then bExiste = (UCase(oRS.Fields("DEN").Value) = UCase(sdbcBloqueSeguridad.Text))

    Screen.MousePointer = vbNormal

    If Not bExiste Then
        sdbcBloqueSeguridad.Text = ""
        Me.sdbcBloqueSeguridad.Columns("ID").Value = Null
    Else
        bRespetarComboBloquesSeguridad = True
        sdbcBloqueSeguridad.Text = oRS.Fields("DEN").Value
        sdbcBloqueSeguridad.Columns("ID").Value = oRS.Fields("ID").Value
        bRespetarComboBloquesSeguridad = False
    End If
    oRS.Close
    Set oRS = Nothing
    bCargarComboDesde = True
    Set oBloquesSeguridad = Nothing
    Me.optBloques.Value = True
End Sub


Private Sub sdbcBloqueSeguridad_CloseUp()
    
    If sdbcBloqueSeguridad.Value = "..." Then
        sdbcBloqueSeguridad.Text = ""
        Exit Sub
    End If
    
    If sdbcBloqueSeguridad.Value = "" Then Exit Sub
    
    bRespetarComboBloquesSeguridad = True
    sdbcBloqueSeguridad.Text = sdbcBloqueSeguridad.Columns(1).Text
    bRespetarComboBloquesSeguridad = False
    bCargarComboDesde = False
    DoEvents
        
    Me.optBloques.Value = True
End Sub
Private Sub sdbcBloqueSeguridad_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim oIasig As IAsignaciones
    Dim sCod As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oBloquesSeguridad As CBloquesSeguridad
    Dim oRS As ADODB.Recordset
    
    sdbcBloqueSeguridad.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oBloquesSeguridad = Nothing
    Set oBloquesSeguridad = Raiz.generar_CBloquesSeguridad
    
    If bCargarComboDesde Then
        sCod = Trim(sdbcBloqueSeguridad.Text)
    Else
        sCod = ""
    End If
        
        
    Set oRS = oBloquesSeguridad.DevolverBloquesSeguridad(False, , Me.sdbcBloqueSeguridad.Text)
    
    
    While Not oRS.EOF
        sdbcBloqueSeguridad.AddItem oRS.Fields("ID").Value & vbTab & oRS.Fields("DEN").Value
        oRS.MoveNext
    Wend
    oRS.Close

    sdbcBloqueSeguridad.SelStart = 0
    sdbcBloqueSeguridad.SelLength = Len(sdbcBloqueSeguridad.Text)
    sdbcBloqueSeguridad.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
    
End Sub

Private Sub sdbcBloqueSeguridad_InitColumnProps()
    sdbcBloqueSeguridad.DataFieldList = "Column 0"
    sdbcBloqueSeguridad.DataFieldToDisplay = "Column 1"
End Sub



