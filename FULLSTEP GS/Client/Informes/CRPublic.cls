VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRPublic"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Property Get crs_crapp() As CRAXDRT.Application
    Set crs_crapp = ocrs_crapp
End Property
Public Property Set crs_crapp(ByRef oNewValue As CRAXDRT.Application)
    Set ocrs_crapp = oNewValue
End Property

Public Property Get crs_Server() As String
    crs_Server = scrs_Server
End Property
Public Property Let crs_Server(ByRef sNewValue As String)
    scrs_Server = sNewValue
End Property

Public Property Get crs_Database() As String
    crs_Database = scrs_Database
End Property
Public Property Let crs_Database(ByRef sNewValue As String)
    scrs_Database = sNewValue
End Property

Public Property Get crs_User() As String
    crs_User = scrs_User
End Property
Public Property Let crs_User(ByRef sNewValue As String)
    scrs_User = sNewValue
End Property

Public Property Get crs_Password() As String
    crs_Password = scrs_Password
End Property
Public Property Let crs_Password(ByRef sNewValue As String)
    scrs_Password = sNewValue
End Property

Public Property Get crs_Connected() As Boolean
    crs_Connected = bcrs_Connected
End Property
Public Property Let crs_Connected(ByRef bNewValue As Boolean)
    bcrs_Connected = bNewValue
End Property

Public Function Conectar(ByVal sInstancia As String, Optional ByVal sServidor As String, Optional ByVal sBaseDatos As String) As Boolean

On Error GoTo Error:

    Set crs_crapp = New CRAXDRT.Application
    
    If sServidor = "" Or sBaseDatos = "" Then
        scrs_Server = GetStringSetting("FULLSTEP GS", sInstancia, "Conexion", "Servidor")
        scrs_Database = GetStringSetting("FULLSTEP GS", sInstancia, "Conexion", "BaseDeDatos")
    Else
        scrs_Server = sServidor
        scrs_Database = sBaseDatos
    End If
    scrs_User = LICDBLogin(App.Path & "\FSDB.LIC")
    scrs_Password = LICDBContra(App.Path & "\FSDB.LIC")
    'CON CRYSTAL 10 NO HACE FALTA
    'crs_crapp.LogOnServer "p2ssql.dll", scrs_Server, scrs_Database, scrs_User, scrs_Password
        
    Conectar = True
    bcrs_Connected = True
    
    Exit Function

Error:
    
    Conectar = False
    
End Function

Public Function crs_ParameterIndex(rpt As CRAXDRT.Report, parametername As String)
    crs_ParameterIndex = crs_ParameterIndex_Interno(rpt, parametername)
End Function
Public Function crs_FormulaIndex(rpt As CRAXDRT.Report, formulaname As String)
    crs_FormulaIndex = crs_FormulaIndex_Interno(rpt, formulaname)
End Function
Public Function crs_Tableindex(rpt As CRAXDRT.Report, tablename As String) As Integer
    crs_Tableindex = crs_Tableindex_Interno(rpt, tablename)
End Function
Public Function crs_FieldIndex(rpt As CRAXDRT.Report, tablename As String, fieldname As String)
    crs_FieldIndex = crs_FieldIndex_Interno(rpt, tablename, fieldname)
End Function
Public Function crs_SortDirection(sortstr As String) As CRSortDirection
    crs_SortDirection = crs_SortDirection_Interno(sortstr)
End Function
Public Function crs_SortTable(sortstr As String) As String
    crs_SortTable = crs_SortTable_Interno(sortstr)
End Function
Public Function crs_SortField(sortstr As String) As String
    crs_SortField = crs_SortField_Interno(sortstr)
End Function

'edu
'nueva funcion que reconencta el origen de datos de un report a la base de datos de explotación
'Soluciona error de migracion de a Crystal Reports 10.
'por el cual los parametros de conexion suministrados desde VB no funcionaban para esta versión de CR.
'fin edu
'Lo de edu como que no tira si intentas cambiar de bbdd o user id o ...
Public Sub ConectarReport(Listado As CRAXDRT.Report, Server As String, Database As String, User As String, Password As String)
    ConectarReportInterno Listado, Server, Database, User, Password
End Sub

