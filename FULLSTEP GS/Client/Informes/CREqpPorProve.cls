VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CREqpPorProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public IdiomaInstalacion As String   'basPublic.gParametrosInstalacion.gIdioma

Public Function ListadoEqpPorProve(oReport As CRAXDRT.Report, Optional ByVal codGMN4_4 As String, Optional ByVal codGMN3_4 As String, Optional ByVal codGMN2_4 As String, Optional ByVal codGMN1_4 As String, Optional ByVal restEqp As Boolean, Optional ByVal codEqpUsuario As String, Optional ByVal CodProve As String, Optional ByVal bProve As Boolean, Optional ByVal bOrdCod As Boolean, Optional ByVal bOcultarContactos As Boolean, Optional ByVal bOcultarHomologacion As Boolean)
'Private Function ObtenerSQLEqpPorProve(Optional ByVal codGMN4_4 As String, Optional ByVal codGMN3_4 As String, Optional ByVal codGMN2_4 As String, Optional ByVal codGMN1_4 As String, Optional ByVal restEqp As Boolean, Optional ByVal codEqpUsuario As String, Optional ByVal codProve As String, Optional ByVal bProve As Boolean, Optional ByVal bOrdCod As Boolean) As String

    Dim Table As CRAXDRT.DatabaseTable
    Dim sSQL As String
    Dim SubListado As CRAXDRT.Report

    sSQL = ObtenerSQLEqpPorProve(codGMN4_4, codGMN3_4, codGMN2_4, codGMN1_4, restEqp, codEqpUsuario, CodProve, bProve, bOrdCod)
    'edu. Incidencia 6757. conectar a base de datos en curso
    ConectarReportInterno oReport, scrs_Server, scrs_Database, scrs_User, scrs_Password

    oReport.SQLQueryString = sSQL

    ' SUBREPORTs

    If Not bOcultarContactos Then
        ' OCULTAR CONTACTOS = N
        Set SubListado = oReport.OpenSubreport("rptCON")

        ConectarReportInterno SubListado, scrs_Server, scrs_Database, scrs_User, scrs_Password
    End If

    If Not bOcultarHomologacion Then
        ' OCULTAR HOMOLOGACION = N
        Set SubListado = oReport.OpenSubreport("rptCAL1")
        ConectarReportInterno SubListado, scrs_Server, scrs_Database, scrs_User, scrs_Password
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"

        Set SubListado = oReport.OpenSubreport("prtCAL2")
        For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"

        Set SubListado = oReport.OpenSubreport("rptCAL3")
        For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {CAL_IDIOMA.IDIOMA} = '" & IdiomaInstalacion & "'"
    End If

    Set SubListado = oReport.OpenSubreport("rptEQP")

    ConectarReportInterno SubListado, scrs_Server, scrs_Database, scrs_User, scrs_Password


    Set SubListado = Nothing

End Function

Private Function ObtenerSQLEqpPorProve(Optional ByVal codGMN4_4 As String, Optional ByVal codGMN3_4 As String, Optional ByVal codGMN2_4 As String, Optional ByVal codGMN1_4 As String, Optional ByVal restEqp As Boolean, Optional ByVal codEqpUsuario As String, Optional ByVal CodProve As String, Optional ByVal bProve As Boolean, Optional ByVal bOrdCod As Boolean) As String
Dim sSQLString As String
Dim sNivel As String
'CONSTRUIR LA SQL QUERY A PASAR AL REPORT
' EN FUNCION DE sNivel (hasta que nivel de la estructura de materiales se ha seleccionado)
' se aplican las restricciones de material

    If codGMN4_4 <> "" Then
        sNivel = "4"
    Else
        If codGMN3_4 <> "" Then
            sNivel = "3"
        Else
            If codGMN2_4 <> "" Then
                sNivel = "2"
            Else
                If codGMN1_4 <> "" Then
                    sNivel = "1"
                Else
                    sNivel = "5"
                End If
            End If
        End If
    End If

    sSQLString = "SELECT PROVE.COD, PROVE.DEN, PROVE.DIR, PROVE.CP, PROVE.POB, PROVE.NIF,PROVE_EQP.EQP, PAI.DEN, PROVI.DEN,MON.DEN_" & IdiomaInstalacion & " AS DEN,EQP.DEN"
    
    'edu. Incidencia 6757. Incluir en la clausula FROM las tablas de Equipo
    
    sSQLString = sSQLString & " FROM PROVE INNER JOIN PROVE_EQP ON PROVE.COD = PROVE_EQP.PROVE INNER JOIN EQP ON EQP.COD = PROVE_EQP.EQP"
    If sNivel = "1" Then
        sSQLString = sSQLString & " INNER JOIN PROVE_GMN1 ON PROVE_GMN1.PROVE = PROVE.COD AND PROVE_GMN1.GMN1 = '" & DblQuote(codGMN1_4) & "'"
    Else
        If sNivel = "2" Then
            sSQLString = sSQLString & " INNER JOIN PROVE_GMN2 ON PROVE_GMN2.PROVE = PROVE.COD AND PROVE_GMN2.GMN1 = '" & DblQuote(codGMN1_4) & "'" & " AND PROVE_GMN2.GMN2 = '" & DblQuote(codGMN2_4) & "'"
        Else
            If sNivel = "3" Then
                sSQLString = sSQLString & " INNER JOIN PROVE_GMN3 ON PROVE_GMN3.PROVE = PROVE.COD AND PROVE_GMN3.GMN1 = '" & DblQuote(codGMN1_4) & "'" & " AND PROVE_GMN3.GMN2 = '" & DblQuote(codGMN2_4) & "'" & " AND PROVE_GMN3.GMN3 = '" & DblQuote(codGMN3_4) & "'"
            Else
                If sNivel = "4" Then
                    sSQLString = sSQLString & " INNER JOIN PROVE_GMN4 ON PROVE_GMN4.PROVE = PROVE.COD AND PROVE_GMN4.GMN1 = '" & DblQuote(codGMN1_4) & "'" & " AND PROVE_GMN4.GMN2 = '" & DblQuote(codGMN2_4) & "'" & " AND PROVE_GMN4.GMN3 = '" & DblQuote(codGMN3_4) & "'" & " AND PROVE_GMN4.GMN4 = '" & DblQuote(codGMN4_4) & "'"
                End If
            End If
        End If
    End If
    'If chkRestEqp.Visible = True And chkRestEqp.Enabled = True And chkRestEqp.Value = vbUnchecked Then
    'If restEqp Then
    '    sSQLString = sSQLString & " INNER JOIN prove_eqp on prove.cod = prove_eqp.prove AND PROVE_EQP.EQP = '" & codEqpUsuario & "'"
    'End If
    sSQLString = sSQLString & " LEFT JOIN pai on prove.pai = pai.cod"
    sSQLString = sSQLString & " LEFT JOIN provi on prove.pai = provi.pai and prove.provi = provi.cod"
    sSQLString = sSQLString & " LEFT JOIN mon on mon.cod = prove.mon"
    
    sSQLString = sSQLString & " WHERE 1=1 "
    
    If bProve Then
        sSQLString = sSQLString & " AND PROVE.COD = '" & DblQuote(CodProve) & "'"
    End If
    
    If restEqp Then
        sSQLString = sSQLString & " AND PROVE_EQP.EQP = '" & codEqpUsuario & "'"
    End If
    
    If bOrdCod Then
        sSQLString = sSQLString & " ORDER BY PROVE.COD"
    Else
        sSQLString = sSQLString & " ORDER BY PROVE.DEN"
    End If
    
ObtenerSQLEqpPorProve = sSQLString
End Function

