VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmailProce"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public oFSGSRaiz As CRaiz
Public Mensajes As CMensajes
Public oTipoDeUsuario As TIpoDeUsuario
Public UsuarioSummit As CUsuario

Private m_oParametrosGenerales As ParametrosGenerales
Private m_oParametrosInstalacion As ParametrosInstalacion
Private m_oLongitudesDeCodigos As LongitudesDeCodigos

Public Property Get ParametrosGenerales() As ParametrosGenerales
    ParametrosGenerales = m_oParametrosGenerales
End Property

Public Property Let ParametrosGenerales(ByRef vNewValue As ParametrosGenerales)
    m_oParametrosGenerales = vNewValue
End Property

Public Property Get ParametrosInstalacion() As ParametrosInstalacion
    ParametrosInstalacion = m_oParametrosInstalacion
End Property

Public Property Let ParametrosInstalacion(ByRef vNewValue As ParametrosInstalacion)
    m_oParametrosInstalacion = vNewValue
End Property

Public Property Get LongitudesDeCodigos() As LongitudesDeCodigos
    LongitudesDeCodigos = m_oLongitudesDeCodigos
End Property

Public Property Let LongitudesDeCodigos(ByRef vNewValue As LongitudesDeCodigos)
    m_oLongitudesDeCodigos = vNewValue
End Property

''' <summary>Crea el Cuerpo del mail de anulaci�n de proceso</summary>
''' <param name="rsDatos">recordset con los datos para el email</param>
''' <returns>Cuerpo del mail</returns>
''' <remarks>Llamada desde: EnviarCorreosAnulacion</remarks>
Public Function GenerarCuerpoMensajeAnulacion(ByVal rsDatos As ADODB.Recordset, ByRef bDescargarFrm As Boolean, ByRef bActivado As Boolean, ByVal vFecReape As Variant) As String
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim splantilla As String
    Dim bHTML As Boolean
    Dim sIdioma As String
    Dim oFos As Scripting.FileSystemObject
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If bDescargarFrm Then Exit Function
    
    bHTML = (NullToDbl0(rsDatos("TIPOEMAIL")) = 1)
    If NullToStr(rsDatos("IDI")) <> "" Then
        sIdioma = rsDatos("IDI")
    Else
        sIdioma = m_oParametrosGenerales.gIdioma
    End If
        
    If bHTML Then
        splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AvisoAnulacionProceso.htm"
    Else
        splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AvisoAnulacionProceso.txt"
    End If
   
    Set oFos = New Scripting.FileSystemObject
    If oFos.FileExists(splantilla) Then
        Set oFile = oFos.GetFile(splantilla)
        Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
        sCuerpoMensaje = ostream.ReadAll
        ostream.Close
        
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ANYO_PROCE", rsDatos("ANYO"))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CMD_COD", m_oParametrosGenerales.gsabr_GMN1)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@GMN1_PROCE", rsDatos("GMN1"))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_PROCE", rsDatos("PROCE"))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCE", rsDatos("PROCE_DEN"))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROVE", rsDatos("PROVE_DEN"))
        If Not IsNull(vFecReape) And Not IsEmpty(vFecReape) Then
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_ANULACION", Format(vFecReape, rsDatos("DATEFMT")))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@HORA_ANULACION", Hour(vFecReape) & ":" & Format(Minute(vFecReape), "00"))
        Else
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_ANULACION", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@HORA_ANULACION", "")
        End If
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COM", NullToStr(rsDatos("NOM_COM")))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COM", NullToStr(rsDatos("APE_COM")))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAIL_COM", NullToStr(rsDatos("EMAIL_COM")))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1_COM", NullToStr(rsDatos("TFNO_COM")))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2_COM", NullToStr(rsDatos("TFNO2_COM")))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_COM", NullToStr(rsDatos("FAX_COM")))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CAR_COM", NullToStr(rsDatos("CAR_COM")))
    Else
        Mensajes.ArchivoNoEnRuta
    End If
    
    GenerarCuerpoMensajeAnulacion = sCuerpoMensaje
    
Salir:
    Set oFos = Nothing
    Set oFile = Nothing
    Set ostream = Nothing
    Exit Function
Error:
   If Err.Number <> 0 Then
        GenerarCuerpoMensajeAnulacion = ""
        bDescargarFrm = oFSGSRaiz.TratarError("Class Module", "CEmailProceAlta", "GenerarCuerpoMensajeAnulacion", Err, Erl, , bActivado)
   End If
   GoTo Salir
End Function

''' <summary>Crea el Cuerpo del mail</summary>
''' <param name="Persona">Persona a quien se ha de notificar</param>
''' <param name="Nombre">Nombre</param>
''' <param name="Apellido">Apellido</param>
''' <param name="g_oProcesoSeleccionado">Proceso</param>
''' <returns>Cuerpo del mail</returns>
''' <remarks>Llamada desde: cmdValidarProce_click </remarks>
Public Function GenerarCuerpoMensajeExclusionInvitacion(ByVal Persona As CPersona, ByVal nombre As String, ByVal Apellido As String, ByVal bExclusion As Boolean, ByRef bDescargarFrm As Boolean, _
        ByRef bActivado As Boolean, ByRef oProcesoSeleccionado As CProceso) As String
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim splantilla As String
    Dim bHTML As Boolean
    Dim sIdioma As String
    Dim oFos As Scripting.FileSystemObject
    
    '0:txt, 1:htm
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If bDescargarFrm Then Exit Function
    
    If Persona.TipoMail = -1 Then
        bHTML = m_oParametrosGenerales.gbTipoMail
    Else
        bHTML = Persona.TipoMail
    End If
    
    If Persona.Idioma <> "" Then
        sIdioma = Persona.Idioma
    Else
        sIdioma = m_oParametrosGenerales.gIdioma
    End If
    
    If bExclusion Then
        If Not bHTML Then
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AperturaProcesoExclusion." & "txt"
        Else
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AperturaProcesoExclusion." & "htm"
        End If
    Else
        If Not bHTML Then
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AperturaProcesoInvitacion." & "txt"
        Else
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AperturaProcesoInvitacion." & "htm"
        End If
    End If
           
    Set oFos = New Scripting.FileSystemObject
    If oFos.FileExists(splantilla) Then
        Set oFile = oFos.GetFile(splantilla)
        Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)

        sCuerpoMensaje = ostream.ReadAll
    Else
        Mensajes.ArchivoNoEnRuta
    End If
    Set oFos = Nothing
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_CONTACTO", NullToStr(nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_CONTACTO", NullToStr(Apellido))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCESO", oProcesoSeleccionado.Den)
    
    'Si hay comprador responsable asignado
    If Not oProcesoSeleccionado.responsable Is Nothing Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(oProcesoSeleccionado.responsable.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(oProcesoSeleccionado.responsable.Apel))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(oProcesoSeleccionado.responsable.Tfno))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(oProcesoSeleccionado.responsable.mail))
    Else
        'Sino, se asigna a si mismo como responsable, siempre y cuando sea de tipo comprador
        If (oTipoDeUsuario = TIpoDeUsuario.comprador) And (Not UsuarioSummit.comprador Is Nothing) Then
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(UsuarioSummit.comprador.nombre))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(UsuarioSummit.comprador.Apel))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(UsuarioSummit.comprador.Tfno))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(UsuarioSummit.comprador.mail))
        Else
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", "")
        End If
    End If

    GenerarCuerpoMensajeExclusionInvitacion = sCuerpoMensaje

    Exit Function
Error:
   If Err.Number <> 0 Then
      bDescargarFrm = oFSGSRaiz.TratarError("Class Module", "CEmailProceAlta", "GenerarCuerpoMensajeExclusionInvitacion", Err, Erl, , bActivado)
      Exit Function
   End If
End Function

''' <summary>Crea el Cuerpo del mail</summary>
''' <param name="bWeb">Web / txt</param>
''' <param name="oPet">comunicaci�n</param>
''' <param name="oEquipo">Equipo</param>
''' <returns>Cuerpo del mail</returns>
''' <remarks>Llamada desde: cmAdceptar_click ; Tiempo m�ximo: 0,2</remarks>
Public Function GenerarMensajeObjetivo(ByVal bWeb As Boolean, ByVal oPet As CPetOferta, ByVal oEquipo As CEquipo, ByVal splantilla As String, ByVal iAnyo As Integer, ByVal sGmn1 As String, ByVal lProce As Long, _
        ByVal sProceDen As String, ByVal sFecReu As String, ByVal vDatoAPasar As Variant, ByVal sGMN1Cod As String, ByVal vReferencia As Variant) As String
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim bHTML As Boolean
    Dim oFos As Scripting.FileSystemObject
    Dim oProves As CProveedores
    
    bHTML = oPet.AntesDeCierreParcial
    
    If Not bHTML Then splantilla = Left(splantilla, Len(splantilla) - 4) & ".txt"
    
    Set oFos = New Scripting.FileSystemObject
    Set oFile = oFos.GetFile(splantilla)
    Set oFos = Nothing
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.Add oPet.CodProve, oPet.DenProve
    oProves.CargarDatosProveedor oPet.CodProve
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_PROVE", oPet.CodProve)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROVE", oPet.DenProve)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NUM_PROCESO", CStr(iAnyo) & "/" & sGmn1 & "/" & CStr(lProce))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_CONTACTO", NullToStr(oPet.Tfno))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_CONTACTO", NullToStr(oPet.Fax))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_CONTACTO", NullToStr(oPet.nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_CONTACTO", NullToStr(oPet.Apellidos))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCESO", sProceDen & " ")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_REUNION", sFecReu)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_LIMITE", vDatoAPasar)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Apel))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Tfno))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).mail))
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAT1_PROCESO", sGMN1Cod)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@SOLICITUD", NullToStr(vReferencia))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR_PROVE", NullToStr(oProves.Item(1).Direccion))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@POBL_PROVE", NullToStr(oProves.Item(1).Poblacion))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP_PROVE", NullToStr(oProves.Item(1).cP))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PROV_PROVE", NullToStr(oProves.Item(1).DenProvi))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PAIS_PROVE", NullToStr(oProves.Item(1).DenPais))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF_PROVE", NullToStr(oProves.Item(1).NIF))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_CONTACTO", NullToStr(oPet.Email))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Fax))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", NullToStr(m_oParametrosGenerales.gsURLSUMMIT))

    Set oProves = Nothing
    
    GenerarMensajeObjetivo = sCuerpoMensaje
End Function

''' <summary>Genera el mensaje de aviso en HTML</summary>
''' <param name="oProve">Objeto proveedor</param>
''' <param name="oCon">Objeto contacto</param>
''' <param name="sEqp">Equipo</param>
''' <param name="sComp">Comprador</param>
''' <remarks>Llamada desde: EnviarEmails; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 20/04/2011</revision>

Public Function GenerarMensajeAvisoHTML(ByRef oProve As CProveedor, ByRef oCon As CContacto, ByVal sEqp As Variant, ByVal sComp As Variant, ByRef oProcesoSeleccionado As CProceso, _
        ByRef oCompradores As CCompradores) As String
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sFileName As String
    Dim sFile As String
    Dim sFileNew As String
    Dim lPosIni As Long
    Dim lPosFin As Long
    Dim stmpfila As String
    Dim dtFechaCierreTZ As Date
    Dim dtHoraCierreTZ As Date
    Dim sCod As String
    Dim sTZ As String
    Dim oComp As CComprador
    Dim oEmpresas As CEmpresas
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    If InStr(m_oParametrosInstalacion.gsOFENotifAvisoDespubHTML, m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        sCarpeta = oEmpresas.DevolverCarpetaPlantillaProce_o_Ped(oProcesoSeleccionado.Anyo, oProcesoSeleccionado.GMN1Cod, oProcesoSeleccionado.Cod, 0)
        Set oEmpresas = Nothing
    End If
    If sCarpeta = "" And m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    sFileName = Replace(m_oParametrosInstalacion.gsOFENotifAvisoDespubHTML, sMarcaPlantillas, sCarpeta)

    'si el usuario receptor es un usuario del EP,coger� esa plantilla,pero con el idioma del usuario
    Set oFos = New FileSystemObject
    If Not IsNull(oCon.Idioma) Then
        sFile = oFos.GetFileName(sFileName)
        If sFile <> "" Then
            sFileNew = oCon.Idioma & "_" & Right(sFile, Len(sFile) - InStr(1, sFile, "_"))
            sFileName = Replace(sFileName, sFile, sFileNew)
        End If
    End If
        
    If Not oFos.FileExists(sFileName) Then
        Mensajes.PlantillaNoEncontrada sFileName
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeAvisoHTML = ""
        Exit Function
    End If
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    
    ostream.Close
        
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@ANYO_PROCE", NullToStr(oProcesoSeleccionado.Anyo))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_GMN1", NullToStr(oProcesoSeleccionado.GMN1Cod))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_PROCE", NullToStr(oProcesoSeleccionado.Cod))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCE", NullToStr(oProcesoSeleccionado.Den))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_CIA", NullToStr(oProve.Cod))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_CIA", NullToStr(oProve.Den))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_PROVE", NullToStr(oCon.nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_PROVE", NullToStr(oCon.Apellidos))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CAR_PROVE", NullToStr(oCon.Cargo))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1_PROVE", NullToStr(oCon.Tfno))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2_PROVE", NullToStr(oCon.Tfno2))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_PROVE", NullToStr(oCon.Fax))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAIL_PROVE", NullToStr(oCon.mail))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEP_PROVE", NullToStr(oCon.Departamento))
    
    sCod = sEqp & Mid$("                         ", 1, m_oLongitudesDeCodigos.giLongCodeqp - Len(sEqp)) & sComp
    Set oComp = oCompradores.Item(sCod)
    If oComp Is Nothing Then
        lPosIni = InStr(sCuerpoMensaje, "<!--INICIO COMPRADOR-->")
        lPosFin = InStr(sCuerpoMensaje, "<!--FIN COMPRADOR-->") + Len("<!--FIN COMPRADOR-->")
        stmpfila = Mid(sCuerpoMensaje, lPosIni, lPosFin - lPosIni)
        sCuerpoMensaje = Replace(sCuerpoMensaje, stmpfila, "")
                        
        sTZ = NullToStr(m_oParametrosGenerales.gvTimeZone)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COM", NullToStr(oComp.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COM", NullToStr(oComp.Apel))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAIL_COM", NullToStr(oComp.mail))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1_COM", NullToStr(oComp.Tfno))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2_COM", NullToStr(oComp.Tfno2))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_COM", NullToStr(oComp.Fax))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGO_COM", NullToStr(oComp.Cargo))
        
        sTZ = oComp.DevolverTZComprador()
    End If
        
    ConvertirUTCaTZ DateValue(oProcesoSeleccionado.FechaMinimoLimOfertas), TimeValue(oProcesoSeleccionado.FechaMinimoLimOfertas), sTZ, dtFechaCierreTZ, dtHoraCierreTZ
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_CIERRE", NullToStr(Format(dtFechaCierreTZ, "Short Date")))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@HORA_CIERRE", NullToStr(Format(dtHoraCierreTZ, "Short Time")))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_UTC_CIERRE", NullToStr(Format(oProcesoSeleccionado.FechaMinimoLimOfertas, "Short Date")))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@HORA_UTC_CIERRE", NullToStr(Format(oProcesoSeleccionado.FechaMinimoLimOfertas, "Short Time")))
    
    Set ostream = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set oComp = Nothing
    
    GenerarMensajeAvisoHTML = sCuerpoMensaje
End Function

''' <summary>Genera el mensaje de aviso en TXT</summary>
''' <param name="oProve">Objeto proveedor</param>
''' <param name="oCon">Objeto contacto</param>
''' <param name="sEqp">Equipo</param>
''' <param name="sComp">Comprador</param>
''' <remarks>Llamada desde: EnviarEmails; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 20/04/2011</revision>

Public Function GenerarMensajeAvisoTXT(ByRef oProve As CProveedor, ByRef oCon As CContacto, ByVal sEqp As Variant, ByVal sComp As Variant, ByRef oProcesoSeleccionado As CProceso, _
        ByRef oCompradores As CCompradores) As String
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sFileName As String
    Dim sFile As String
    Dim sFileNew As String
    Dim dtFechaCierreTZ As Date
    Dim dtHoraCierreTZ As Date
    Dim lPosIni As Long
    Dim lPosFin As Long
    Dim stmpfila As String
    Dim sCod As String
    Dim sTZ As String
    Dim oComp As CComprador
    Dim oEmpresas As CEmpresas
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    
    If InStr(m_oParametrosInstalacion.gsOFENotifAvisoDespubTXT, m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        sCarpeta = oEmpresas.DevolverCarpetaPlantillaProce_o_Ped(oProcesoSeleccionado.Anyo, oProcesoSeleccionado.GMN1Cod, oProcesoSeleccionado.Cod, 0)
        Set oEmpresas = Nothing
    End If
    If sCarpeta = "" And m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = m_oParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    
    
    'Coge la plantilla de par�metros de la instalaci�n
    sFileName = Replace(m_oParametrosInstalacion.gsOFENotifAvisoDespubTXT, sMarcaPlantillas, sCarpeta)
    
    Set oFos = New FileSystemObject
    If Not IsNull(oCon.Idioma) Then
        sFile = oFos.GetFileName(sFileName)
        If sFile <> "" Then
            sFileNew = oCon.Idioma & "_" & Right(sFile, Len(sFile) - InStr(1, sFile, "_"))
            sFileName = Replace(sFileName, sFile, sFileNew)
        End If
    End If
    
    If Not oFos.FileExists(sFileName) Then
        Mensajes.PlantillaNoEncontrada sFileName
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeAvisoTXT = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    
    sCuerpoMensaje = ostream.ReadAll
    
    ostream.Close
        
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@ANYO_PROCE", NullToStr(oProcesoSeleccionado.Anyo))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_GMN1", NullToStr(oProcesoSeleccionado.GMN1Cod))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_PROCE", NullToStr(oProcesoSeleccionado.Cod))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCE", NullToStr(oProcesoSeleccionado.Den))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_CIA", NullToStr(oProve.Cod))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_CIA", NullToStr(oProve.Den))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_PROVE", NullToStr(oCon.nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_PROVE", NullToStr(oCon.Apellidos))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CAR_PROVE", NullToStr(oCon.Cargo))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1_PROVE", NullToStr(oCon.Tfno))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2_PROVE", NullToStr(oCon.Tfno2))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_PROVE", NullToStr(oCon.Fax))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAIL_PROVE", NullToStr(oCon.mail))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEP_PROVE", NullToStr(oCon.Departamento))
        
    sCod = sEqp & Mid$("                         ", 1, m_oLongitudesDeCodigos.giLongCodeqp - Len(sEqp)) & sComp
    Set oComp = oCompradores.Item(sCod)
    If oComp Is Nothing Then
        lPosIni = InStr(sCuerpoMensaje, "<!--INICIO COMPRADOR-->")
        lPosFin = InStr(sCuerpoMensaje, "<!--FIN COMPRADOR-->") + Len("<!--FIN COMPRADOR-->")
        stmpfila = Mid(sCuerpoMensaje, lPosIni, lPosFin - lPosIni)
        sCuerpoMensaje = Replace(sCuerpoMensaje, stmpfila, "")
                
        sTZ = NullToStr(m_oParametrosGenerales.gvTimeZone)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "<!--INICIO COMPRADOR-->", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "<!--FIN COMPRADOR-->", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COM", NullToStr(oComp.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COM", NullToStr(oComp.Apel))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAIL_COM", NullToStr(oComp.mail))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1_COM", NullToStr(oComp.Tfno))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2_COM", NullToStr(oComp.Tfno2))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_COM", NullToStr(oComp.Fax))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGO_COM", NullToStr(oComp.Cargo))
        
        sTZ = oComp.DevolverTZComprador()
    End If
    
    ConvertirUTCaTZ DateValue(oProcesoSeleccionado.FechaMinimoLimOfertas), TimeValue(oProcesoSeleccionado.FechaMinimoLimOfertas), sTZ, dtFechaCierreTZ, dtHoraCierreTZ
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_CIERRE", NullToStr(Format(dtFechaCierreTZ, "Short Date")))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@HORA_CIERRE", NullToStr(Format(dtHoraCierreTZ, "Short Time")))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_UTC_CIERRE", NullToStr(Format(oProcesoSeleccionado.FechaMinimoLimOfertas, "Short Date")))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@HORA_UTC_CIERRE", NullToStr(Format(oProcesoSeleccionado.FechaMinimoLimOfertas, "Short Time")))
    
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oComp = Nothing
    
    GenerarMensajeAvisoTXT = sCuerpoMensaje
End Function

''' <summary>Crea el Cuerpo del mail</summary>
''' <param name="bWeb">Web / txt</param>
''' <param name="oPet">comunicaci�n</param>
''' <param name="oEquipo">Equipo</param>
''' <returns>Cuerpo del mail</returns>
''' <remarks>Llamada desde: cmAdceptar_click ; Tiempo m�ximo: 0,2</remarks>
Public Function GenerarMensajePeticionOferta(ByVal splantilla As String, ByVal oPet As CPetOferta, ByVal oEquipo As CEquipo, ByVal iAnyo As Integer, ByVal sGmn1 As String, ByVal lProce As Long, _
        ByVal sProceDen As String, ByVal vDatoAPasar As Variant, ByVal sGMN1Cod As String, ByVal vReferencia As Variant, ByVal sFecIniSub As String) As String
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim oFos As Scripting.FileSystemObject
    Dim oProves As CProveedores
    
    If Not oPet.AntesDeCierreParcial Then splantilla = Left(splantilla, Len(splantilla) - 4) & ".txt"
    
    Set oFos = New Scripting.FileSystemObject
    Set oFile = oFos.GetFile(splantilla)
    Set oFos = Nothing
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.Add oPet.CodProve, oPet.DenProve
    oProves.CargarDatosProveedor oPet.CodProve
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_PROVE", oPet.CodProve)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROVE", oPet.DenProve)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NUM_PROCESO", CStr(iAnyo) & "/" & sGmn1 & "/" & CStr(lProce))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_CONTACTO", NullToStr(oPet.Tfno))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_CONTACTO", NullToStr(oPet.Fax))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_CONTACTO", NullToStr(oPet.nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_CONTACTO", NullToStr(oPet.Apellidos))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCESO", sProceDen & " ")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHAINISUB", sFecIniSub)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_LIMITE", vDatoAPasar)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Apel))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Tfno))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).mail))
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAT1_PROCESO", sGMN1Cod)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@SOLICITUD", NullToStr(vReferencia))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR_PROVE", NullToStr(oProves.Item(1).Direccion))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@POBL_PROVE", NullToStr(oProves.Item(1).Poblacion))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP_PROVE", NullToStr(oProves.Item(1).cP))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PROV_PROVE", NullToStr(oProves.Item(1).DenProvi))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PAIS_PROVE", NullToStr(oProves.Item(1).DenPais))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF_PROVE", NullToStr(oProves.Item(1).NIF))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_CONTACTO", NullToStr(oPet.Email))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Fax))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", NullToStr(m_oParametrosGenerales.gsURLSUMMIT))

    Set oProves = Nothing
    
    GenerarMensajePeticionOferta = sCuerpoMensaje
End Function

'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSGS_CambioDeResponsable.txt o                    ***
'***              ENG_FSGS_CambioDeResponsable.txt dependi�ndo del      ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
Public Function GenerarMensajeNotificResponsable(ByVal sIdioma As String, ByVal sTZ As String, ByRef bDescargarFrm As Boolean, ByRef bActivado As Boolean, ByRef oProcesoSeleccionado As CProceso, _
        ByVal sEstados As Variant) As String
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sFileName As String
    Dim bSinResponsableAnt As Boolean
    Dim oPersonas As CPersonas
    Dim sGMN As String
    Dim i As Integer
    Dim dtFecha As Date
    Dim dtHora As Date
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If bDescargarFrm Then Exit Function
   
    Set oFos = New FileSystemObject
    'Coge la plantilla de par�metros de la instalaci�n
    sFileName = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_FSGS_CambioDeResponsable.txt"
    
    If Not oFos.FileExists(sFileName) Then
        Screen.MousePointer = vbNormal
        Mensajes.PlantillaNoEncontrada sFileName
        Set oFos = Nothing
        GenerarMensajeNotificResponsable = ""
        Exit Function
    End If
    
    If oProcesoSeleccionado Is Nothing Then
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeNotificResponsable = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    
    sCuerpoMensaje = ostream.ReadAll
    'Proceso de compras:
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@ANYO", oProcesoSeleccionado.Anyo)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@GMN1_PROCE", oProcesoSeleccionado.GMN1Cod)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PROCE", oProcesoSeleccionado.Cod)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCE", oProcesoSeleccionado.Den)
    
    Select Case oProcesoSeleccionado.Estado
        Case 1:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(1))
        Case 2:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(1))
        Case 3:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(2))
        Case 4:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(2))
        Case 5:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(3))
        Case 6:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(4))
        Case 7:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(4))
        Case 8:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(5))
        Case 9:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(5))
        Case 10:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(6))
        Case 11:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(12))
        Case 12:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(7))
        Case 13:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(8))
        Case 20:
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EST_DEN", sEstados(9))
    End Select
    'Material:
    
    If InStr(1, sCuerpoMensaje, "@MATERIAL", vbTextCompare) = 0 Then
        'Si no existe el marcador @Material se muestra el primer material del proceso
        If Not oProcesoSeleccionado.MaterialProce Is Nothing Then
             If oProcesoSeleccionado.MaterialProce.Count > 0 Then
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@GMN1", NullToStr(oProcesoSeleccionado.MaterialProce.Item(1).GMN1Cod))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_GMN1", NullToStr(oProcesoSeleccionado.MaterialProce.Item(1).GMN1Den))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@GMN2", NullToStr(oProcesoSeleccionado.MaterialProce.Item(1).GMN2Cod))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_GMN2", NullToStr(oProcesoSeleccionado.MaterialProce.Item(1).GMN2Den))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@GMN3", NullToStr(oProcesoSeleccionado.MaterialProce.Item(1).GMN3Cod))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_GMN3", NullToStr(oProcesoSeleccionado.MaterialProce.Item(1).GMN3Den))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@GMN4", NullToStr(oProcesoSeleccionado.MaterialProce.Item(1).Cod))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_GMN4", NullToStr(oProcesoSeleccionado.MaterialProce.Item(1).Den))
            End If
        End If
    Else
        'Si existe el marcador @Material se muestran todos los materiales
        For i = 1 To oProcesoSeleccionado.MaterialProce.Count
            sGMN = sGMN & oProcesoSeleccionado.MaterialProce.Item(i).GMN1Cod & "-" & oProcesoSeleccionado.MaterialProce.Item(i).GMN2Cod & "-" & oProcesoSeleccionado.MaterialProce.Item(i).GMN3Cod & "-" & oProcesoSeleccionado.MaterialProce.Item(i).Cod & "-" & oProcesoSeleccionado.MaterialProce.Item(i).Den & vbCrLf & "             "
        Next
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MATERIAL", sGMN)
    End If
    
    'Fechas:
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_APER", NullToStr(oProcesoSeleccionado.FechaApertura))
    If NullToStr(oProcesoSeleccionado.FechaMinimoLimOfertas) <> vbNullString Then
        ConvertirUTCaTZ DateValue(oProcesoSeleccionado.FechaMinimoLimOfertas), TimeValue(oProcesoSeleccionado.FechaMinimoLimOfertas), sTZ, dtFecha, dtHora
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_LIMOFE", CStr(dtFecha) & " " & CStr(dtHora))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_LIMOFE", vbNullString)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_PRESEN", NullToStr(oProcesoSeleccionado.FechaPresentacion))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_NECE", NullToStr(oProcesoSeleccionado.FechaNecesidad))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_INI", NullToStr(oProcesoSeleccionado.FechaInicioSuministro))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_FIN", NullToStr(oProcesoSeleccionado.FechaFinSuministro))
    
    'Responsable anterior:
    bSinResponsableAnt = False
    If Not oProcesoSeleccionado.responsable Is Nothing Then
        If oProcesoSeleccionado.responsable.Cod <> "" Then
            'Carga los datos del comprador
            Set oPersonas = oFSGSRaiz.Generar_CPersonas
            oPersonas.CargarTodasLasPersonas oProcesoSeleccionado.responsable.Cod, , , True, 10
            If Not oPersonas.Item(1) Is Nothing Then
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_NOM", NullToStr(oPersonas.Item(1).nombre))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_APE", NullToStr(oPersonas.Item(1).Apellidos))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_TFNO2", NullToStr(oPersonas.Item(1).Tfno2))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_TFNO", NullToStr(oPersonas.Item(1).Tfno))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_FAX", NullToStr(oPersonas.Item(1).Fax))
                sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_EMAIL", NullToStr(oPersonas.Item(1).mail))
            Else
                bSinResponsableAnt = True
            End If
            Set oPersonas = Nothing
        Else
            bSinResponsableAnt = True
        End If
    Else
        bSinResponsableAnt = True
    End If
    
    If bSinResponsableAnt Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_NOM", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_APE", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_TFNO2", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_TFNO", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_FAX", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESP_ANT_EMAIL", "")
    End If
    
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing

    GenerarMensajeNotificResponsable = sCuerpoMensaje

    Exit Function
Error:
    If Err.Number <> 0 Then
        bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "GenerarMensajeNotificResponsable", Err, Erl, , bActivado)
        Exit Function
    End If
End Function
