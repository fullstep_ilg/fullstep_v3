VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmailAdj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public oFSGSRaiz As CRaiz
Public Mensajes As CMensajes
Public oTipoDeUsuario As TipoDeUsuario
Public UsuarioSummit As CUsuario

Private m_oParametrosGenerales As ParametrosGenerales
Private m_oParametrosInstalacion As ParametrosInstalacion
Private m_oLongitudesDeCodigos As LongitudesDeCodigos

Public Property Get ParametrosGenerales() As ParametrosGenerales
    ParametrosGenerales = m_oParametrosGenerales
End Property

Public Property Let ParametrosGenerales(ByRef vNewValue As ParametrosGenerales)
    m_oParametrosGenerales = vNewValue
End Property

Public Property Get ParametrosInstalacion() As ParametrosInstalacion
    ParametrosInstalacion = m_oParametrosInstalacion
End Property

Public Property Let ParametrosInstalacion(ByRef vNewValue As ParametrosInstalacion)
    m_oParametrosInstalacion = vNewValue
End Property

Public Property Get LongitudesDeCodigos() As LongitudesDeCodigos
    LongitudesDeCodigos = m_oLongitudesDeCodigos
End Property

Public Property Let LongitudesDeCodigos(ByRef vNewValue As LongitudesDeCodigos)
    m_oLongitudesDeCodigos = vNewValue
End Property

''' <summary>Crea el Cuerpo del mail</summary>
''' <param name="Persona">Persona a quien se ha de notificar</param>
''' <param name="Nombre">Nombre</param>
''' <param name="Apellido">Apellido</param>
''' <param name="g_oProcesoSeleccionado">Proceso</param>
''' <returns>Cuerpo del mail</returns>
''' <remarks>Llamada desde: cmdValidarProce_click </remarks>
Public Function GenerarCuerpoMensajeAdjudicacion(ByVal Persona As CPersona, ByVal nombre As String, ByVal Apellido As String, ByVal bGuardar As Boolean, ByRef bDescargarFrm As Boolean, ByRef bActivado As Boolean, _
        ByRef oProcesoSeleccionado As CProceso, ByRef oProvesAsig As CProveedores, ByRef oAdjs As CAdjsGrupo, ByVal sAbierto As String, ByVal dblAbiertoProc As Double, ByVal sConsumido As String, _
        ByVal dblConsumidoProc As Double, ByVal sAdjudicado As String, ByVal dblAdjudicadoProc As Double, ByVal sAhorrado As String, ByVal dblAhorradoProc As Double, ByVal dblAhorradoPorcenProc As Double, _
        ByVal sCons As String, ByVal sAdj As String, ByVal sAhorr As String) As String
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim splantilla As String
    Dim bHTML As Boolean
    Dim sIdioma As String
    Dim oProve As CProveedor
    Dim dblAdjudicado As Double
    Dim dblConsumido As Double
    Dim dblAhorrado As Double
    Dim dblImporte As Double
    Dim dblAhorroOfe As Double
    Dim oGrupo As CGrupo
    Dim sCod As String
    Dim sBloque As String
    Dim sBloque_aux As String
    Dim sLineas As String
    Dim oFos As Scripting.FileSystemObject
    
    '0:txt, 1:htm
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If bDescargarFrm Then Exit Function
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Persona.TipoMail = -1 Then
        bHTML = m_oParametrosGenerales.gbTipoMail
    Else
        bHTML = Persona.TipoMail
    End If
   
    If Persona.idioma <> "" Then
        sIdioma = Persona.idioma
    Else
        sIdioma = m_oParametrosGenerales.gIdioma
    End If
    
    If bGuardar Then
        If Not bHTML Then
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_PreadjudicacionProceso." & "txt"
        Else
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_PreadjudicacionProceso." & "htm"
        End If
    Else
        If Not bHTML Then
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AdjudicacionProceso." & "txt"
        Else
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AdjudicacionProceso." & "htm"
        End If
    End If
    
    Set oFos = New Scripting.FileSystemObject

    If oFos.FileExists(splantilla) Then
        Set oFile = oFos.GetFile(splantilla)
        Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
        sCuerpoMensaje = ostream.ReadAll
    Else
        Mensajes.ArchivoNoEnRuta
    End If
    Set oFos = Nothing
  
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_CONTACTO", NullToStr(nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_CONTACTO", NullToStr(Apellido))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCESO", oProcesoSeleccionado.Den)
    
    'Tabla general "Resultado de la comparativa del proceso"
    '------------------------------------------------------
    If Not bHTML Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESULTADOCOMPARATIVA", oProcesoSeleccionado.Den & ": " & sAbierto & ":" & Format(dblAbiertoProc, "#,##0.00") & " " & sConsumido & ":" & _
            Format(dblConsumidoProc, "#,##0.00") & " " & sAdjudicado & ":" & Format(dblAdjudicadoProc, "#,##0.00") & " " & sAhorrado & ": " & Format(dblAhorradoProc, "#,##0.00") & " %: " & _
            Format(dblAhorradoPorcenProc, "#,##0.00"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MONEDA", oProcesoSeleccionado.MonCod)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCESO", oProcesoSeleccionado.Den)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ABIERTO", Format(dblAbiertoProc, "#,##0.00"))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CONSUMIDO", Format(dblConsumidoProc, "#,##0.00"))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ADJUDICADO", Format(dblAdjudicadoProc, "#,##0.00"))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@AHORRADO", Format(dblAhorradoProc, "#,##0.00"))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@PORCENTAJE", Format(dblAhorradoPorcenProc, "#,##0.00"))
    End If
    
    'Tabla por cada proveedor
    '------------------------
    sLineas = ""
    For Each oProve In oProvesAsig
        dblAdjudicado = 0
        dblConsumido = 0
        dblAhorrado = 0
        dblImporte = 0
        dblAhorroOfe = 0
    
        'Para cada proveedor, se queda con quien haya hecho ofertas
        If Not oProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
            'Se calcula la suma del proveedor para todos los grupos
            For Each oGrupo In oProcesoSeleccionado.Grupos
                sCod = oGrupo.Codigo & oProve.Cod & Mid$("                         ", 1, m_oLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
    
                If Not oAdjs.Item(sCod) Is Nothing Then
                  If oAdjs.Item(sCod).HayPrecios = True Then
                    dblConsumido = dblConsumido + (oAdjs.Item(sCod).Consumido / oAdjs.Item(sCod).Cambio)
                    dblAdjudicado = dblAdjudicado + (oAdjs.Item(sCod).Adjudicado / oAdjs.Item(sCod).Cambio)
                    dblAhorrado = dblAhorrado + (oAdjs.Item(sCod).Ahorrado / oAdjs.Item(sCod).Cambio)
                  End If
                End If
            Next  'ogrupo
            
            If dblAdjudicado <> 0 Then
                If (InStr(sCuerpoMensaje, "<!--COMIENZO_PROV-->") > 0) And (InStr(sCuerpoMensaje, "<!--FIN_PROV-->") > 0) Then
                sBloque = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--COMIENZO_PROV-->"), (InStr(sCuerpoMensaje, "<!--FIN_PROV-->") + Len("<!--FIN_PROV-->")) - InStr(sCuerpoMensaje, "<!--COMIENZO_PROV-->"))
        
                        sBloque_aux = sBloque
        
                        sBloque_aux = Replace(sBloque_aux, "<!--COMIENZO_PROV-->", "")
                        sBloque_aux = Replace(sBloque_aux, "<!--FIN_PROV-->", "")
        
                        If Not bHTML Then
                            sBloque_aux = Replace(sBloque_aux, "@PROVEEDOR", oProve.Cod & " " & oProve.Den & ":" & " " & sCons & Format(dblConsumido, "#,##0.00") & " " & sAdj & Format(dblAdjudicado, "#,##0.00") & " " & sAhorr & Format(dblAhorrado, "#,##0.00"))
                            'sBloque_aux = Replace(sBloque_aux, "@PROVEEDOR", oProve.Cod & " " & oProve.Den & ":" & " Cons:" & dblConsumido & " Adj:" & dblAdjudicado & " Ahorr." & dblAhorrado)
                        Else
                            sBloque_aux = Replace(sBloque_aux, "@PROVE", oProve.Cod & " " & oProve.Den)
                        
                            sBloque_aux = Replace(sBloque_aux, "@CONS_PROVE", Format(dblConsumido, "#,##0.00"))
                            sBloque_aux = Replace(sBloque_aux, "@ADJ_PROVE", Format(dblAdjudicado, "#,##0.00"))
                            sBloque_aux = Replace(sBloque_aux, "@AHORR_PROVE", Format(dblAhorrado, "#,##0.00"))
                        End If
                    
                        If sLineas = "" Then
                            sLineas = sBloque_aux
                        Else
                            sLineas = sLineas & vbCrLf & sBloque_aux
                        End If
                End If
            End If
        End If
    Next    'Proveedores
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, sBloque, sLineas)
    
    'Si hay comprador responsable asignado
    If Not oProcesoSeleccionado.responsable Is Nothing Then
        'Cargar los datos de la persona
        Dim oPer As CPersona
        Set oPer = oFSGSRaiz.Generar_CPersona
        oPer.Cod = oProcesoSeleccionado.responsable.Cod
        oPer.CargarTodosLosDatos
        
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(oPer.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(oPer.Apellidos))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(oPer.Tfno))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(oPer.mail))
        
        Set oPer = Nothing
    Else
        'Sino, se asigna a si mismo como responsable, siempre y cuando sea de tipo comprador
        If (oTipoDeUsuario = TipoDeUsuario.comprador) And (Not UsuarioSummit.comprador Is Nothing) Then
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(UsuarioSummit.comprador.nombre))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(UsuarioSummit.comprador.Apel))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(UsuarioSummit.comprador.Tfno))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(UsuarioSummit.comprador.mail))
        Else
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", "")
        End If

    End If

    GenerarCuerpoMensajeAdjudicacion = sCuerpoMensaje

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
   If Err.Number <> 0 Then
      bDescargarFrm = oFSGSRaiz.TratarError("Class Module", "CEmailAdj", "GenerarCuerpoMensajeAdjudicacion", Err, Erl, , bActivado)
      Exit Function
   End If
End Function

''' <summary>Crea el Cuerpo del mail</summary>
''' <param name="bEsAdjudicado">Adjudicado / No Adjudicado</param>
''' <param name="oPet">comunicaci�n</param>
''' <param name="oEquipo">Equipo</param>
''' <param name="oOferta">Oferta</param>
''' <returns>Cuerpo del mail</returns>
''' <remarks>Llamada desde: cmAdceptar_click ; Tiempo m�ximo: 0,2</remarks>
Public Function GenerarMensajeAdjudicado(ByVal oPet As CPetOferta, ByVal oEquipo As CEquipo, ByVal oOferta As COferta, ByRef oProceso As CProceso, ByRef bDescargarFrm As Boolean, _
        ByRef bActivado As Boolean, ByVal iAnyo As Integer, ByVal sGmn1 As String, ByVal lProce As Long, ByVal sProceDen As String, ByVal sFechaCierre As String, ByVal splantilla As String) As String
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim bHTML As Boolean
    Dim dtFechaOferta As Date
    Dim dtHoraOferta As Date
    Dim tzInfo As TIME_ZONE_INFO
    Dim TrozoImplicados As String
    Dim TrozoImplicados_Aux As String
    Dim sLineas As String
    Dim IPersonas As IPersonasAsig
    Dim oPersonas As CPersonas
    Dim oPer As CPersona
    Dim oFos As Scripting.FileSystemObject
    Dim oProves As CProveedores
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If bDescargarFrm Then Exit Function
    
    bHTML = oPet.AntesDeCierreParcial
    
    If Not bHTML Then splantilla = Left(splantilla, Len(splantilla) - 4) & ".txt"
    
    Set oFos = New Scripting.FileSystemObject
    Set oFile = oFos.GetFile(splantilla)
    Set oFos = Nothing
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)

    sCuerpoMensaje = ostream.ReadAll
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.Add oPet.CodProve, oPet.DenProve
    oProves.CargarDatosProveedor oPet.CodProve
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_PROVE", oPet.CodProve)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROVE", oPet.DenProve)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NUM_PROCESO", CStr(iAnyo) & "/" & sGmn1 & "/" & CStr(lProce))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_CONTACTO", NullToStr(oPet.Tfno))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_CONTACTO", NullToStr(oPet.Fax))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_CONTACTO", NullToStr(oPet.nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_CONTACTO", NullToStr(oPet.Apellidos))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCESO", sProceDen & " ")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHA_CIERRE", sFechaCierre)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Apel))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Tfno))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).mail))
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAT1_PROCESO", sGmn1)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@SOLICITUD", NullToStr(oProceso.Referencia))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR_PROVE", NullToStr(oProves.Item(1).Direccion))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@POBL_PROVE", NullToStr(oProves.Item(1).Poblacion))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP_PROVE", NullToStr(oProves.Item(1).cP))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PROV_PROVE", NullToStr(oProves.Item(1).DenProvi))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PAIS_PROVE", NullToStr(oProves.Item(1).DenPais))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF_PROVE", NullToStr(oProves.Item(1).NIF))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_CONTACTO", NullToStr(oPet.Email))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Fax))

    'DATOS OFERTA
    If Not oOferta Is Nothing Then
        If oProceso.ModoSubasta Then
            'Pasar la fecha al TZ del usuario
            ConvertirUTCaTZ DateValue(oOferta.FechaRec), TimeValue(oOferta.FechaRec), UsuarioSummit.TimeZone, dtFechaOferta, dtHoraOferta
            tzInfo = GetTimeZoneByKey(UsuarioSummit.TimeZone)
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_OFERTA", CStr(dtFechaOferta + dtHoraOferta) & " " & Mid(tzInfo.DisplayName, 1, 11))
        Else
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_OFERTA", Format(oOferta.FechaRec, "Short Date"))
        End If
    
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NUM_OFERTA", NullToStr(oOferta.Num))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@OBS_OFERTA", NullToStr(oOferta.obs))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FEC_OFERTA", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NUM_OFERTA", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@OBS_OFERTA", "")
    End If
    ' PERSONAS ASOCIADAS
    If (InStr(sCuerpoMensaje, "<!--INICIO IMPLICADOS-->") > 0) And (InStr(sCuerpoMensaje, "<!--FIN IMPLICADOS-->") > 0) Then
        Set IPersonas = oProceso
        Set oPersonas = IPersonas.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorApe, True)
        
        TrozoImplicados = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO IMPLICADOS-->"), (InStr(sCuerpoMensaje, "<!--FIN IMPLICADOS-->") + Len("<!--FIN IMPLICADOS-->")) - InStr(sCuerpoMensaje, "<!--INICIO IMPLICADOS-->"))
        sLineas = ""
        
        If oPersonas.Count = 0 Then
            sCuerpoMensaje = Replace(sCuerpoMensaje, TrozoImplicados, "")
        Else
            For Each oPer In oPersonas
                TrozoImplicados_Aux = TrozoImplicados
                
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "<!--INICIO IMPLICADOS-->", "")
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "<!--FIN IMPLICADOS-->", "")
                
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@COD_IMPLICADO", NullToStr(oPer.Cod))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@NOM_IMPLICADO", NullToStr(oPer.nombre))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@APE_IMPLICADO", NullToStr(oPer.Apellidos))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@ROL_IMPLICADO", NullToStr(oPer.Rol))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@TLFNO_IMPLICADO", NullToStr(oPer.Tfno))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@FAX_IMPLICADO", NullToStr(oPer.Fax))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@MAIL_IMPLICADO", NullToStr(oPer.mail))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@UO1_IMPLICADO", NullToStr(oPer.UON1))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@UO2_IMPLICADO", NullToStr(oPer.UON2))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@UO3_IMPLICADO", NullToStr(oPer.UON3))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@DPTO_IMPLICADO", NullToStr(oPer.CodDep))
                TrozoImplicados_Aux = Replace(TrozoImplicados_Aux, "@CARGO_IMPLICADO", NullToStr(oPer.Cargo))
                
                If sLineas = "" Then
                    sLineas = TrozoImplicados_Aux
                Else
                    sLineas = sLineas & vbCrLf & TrozoImplicados_Aux
                End If
            Next
            sCuerpoMensaje = Replace(sCuerpoMensaje, TrozoImplicados, sLineas)
        End If
    End If
    
    Set oProves = Nothing
    Set IPersonas = Nothing
    Set oPersonas = Nothing
    Set oPer = Nothing
    
    GenerarMensajeAdjudicado = sCuerpoMensaje

    Exit Function
ERROR:
    If Err.Number <> 0 Then
        bDescargarFrm = oFSGSRaiz.TratarError("Class Module", "CEmailAdj", "GenerarMensajeAdjudicado", Err, Erl, , bActivado)
        Exit Function
    End If
End Function

''' <summary>Crea el Cuerpo del mail</summary>
''' <param name="Persona">Persona a quien se ha de notificar</param>
''' <param name="Nombre">Nombre</param>
''' <param name="Apellido">Apellido</param>
''' <param name="g_oProcesoSeleccionado">Proceso</param>
''' <returns>Cuerpo del mail</returns>
''' <remarks>Llamada desde: cmdAceptar_click </remarks>
Public Function GenerarCuerpoMensajeAdjudicacionCierreParc(ByVal Persona As CPersona, ByVal nombre As String, ByVal Apellido As String, ByRef bDescargarFrm As Boolean, ByRef bActivado As Boolean, _
        ByVal bGuardar As Boolean, ByRef oProceso As CProceso, ByRef oProvesAsig As CProveedores, ByRef oAdjsGrupo As CAdjsGrupo, ByRef oAdjs As CAdjudicaciones, ByVal sAbierto As String, ByVal dblAbiertoProc As Double, _
        ByVal sConsumido As String, ByVal dblConsumidoProc As Double, ByVal sAdjudicado As String, ByVal dblAdjudicadoProc As Double, ByVal sAhorrado As String, ByVal dblAhorradoProc As Double, _
        ByVal dblAhorradoPorcenProc As Double, ByVal sCons As String, ByVal sAdj As String, ByVal sAhorr As String) As String
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim splantilla As String
    Dim bHTML As Boolean
    Dim sIdioma As String
    Dim oProve As CProveedor
    Dim dblAdjudicado As Double
    Dim dblAdjudicadoTotal As Double
    Dim dblConsumido As Double
    Dim dblConsumidoTotal As Double
    Dim dblAhorrado As Double
    Dim dblAhorroTotal As Double
    Dim dblPorcenTotal As Double
    Dim dblImporte As Double
    Dim dblAhorroOfe As Double
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim sCod As String
    Dim sBloque As String
    Dim sBloque_aux As String
    Dim sLineas As String
    Dim i As Integer
    Dim bRecienCerrado As Boolean
    Dim bProve As Boolean
    Dim oFos As Scripting.FileSystemObject
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If bDescargarFrm Then Exit Function
  
    '0:txt, 1:htm
    If Persona.TipoMail = -1 Then
        bHTML = m_oParametrosGenerales.gbTipoMail
    Else
        bHTML = Persona.TipoMail
    End If
    
    bHTML = Persona.TipoMail
    
    If Persona.idioma <> "" Then
        sIdioma = Persona.idioma
    Else
        sIdioma = m_oParametrosGenerales.gIdioma
    End If
        
    If bGuardar Then
        If Not bHTML Then
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_PreadjudicacionProceso." & "txt"
        Else
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_PreadjudicacionProceso." & "htm"
        End If
    Else
        If Not bHTML Then
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AdjudicacionProceso." & "txt"
        Else
            splantilla = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdioma & "_AdjudicacionProceso." & "htm"
        End If
    End If
    
    Set oFos = New Scripting.FileSystemObject
    If oFos.FileExists(splantilla) Then
        Set oFile = oFos.GetFile(splantilla)
        Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
        sCuerpoMensaje = ostream.ReadAll
    Else
        Mensajes.ArchivoNoEnRuta
    End If
    Set oFos = Nothing
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_CONTACTO", NullToStr(nombre))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_CONTACTO", NullToStr(Apellido))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCESO", oProceso.Den)
    
    'Tabla general "Resultado de la comparativa del proceso"
    '------------------------------------------------------
    If Not bHTML Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@RESULTADOCOMPARATIVA", oProceso.Den & ": " & sAbierto & ":" & Format(dblAbiertoProc, "#,##0.00") & " " & sConsumido & ":" & _
                         Format(dblConsumidoProc, "#,##0.00") & " " & sAdjudicado & ":" & Format(dblAdjudicadoProc, "#,##0.00") & " " & sAhorrado & ": " & _
                         Format(dblAhorradoProc, "#,##0.00") & " %: " & Format(dblAhorradoPorcenProc, "#,##0.00"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MONEDA", oProceso.MonCod)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEN_PROCESO", oProceso.Den)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ABIERTO", Format(dblAbiertoProc, "#,##0.00"))
    End If
    
    'Tabla por cada proveedor
    '------------------------
    sLineas = ""
    
    dblAdjudicadoTotal = 0
    dblConsumidoTotal = 0
    For Each oProve In oProvesAsig
        dblAdjudicado = 0
        dblConsumido = 0
        dblAhorrado = 0
        dblImporte = 0
        dblAhorroOfe = 0
    
        'Para cada proveedor, se queda con quien haya hecho ofertas
        If Not oProceso.Ofertas.Item(oProve.Cod) Is Nothing Then
            'Se calcula la suma del proveedor para todos los grupos
            For Each oGrupo In oProceso.Grupos
                sCod = oGrupo.Codigo & oProve.Cod & Mid$("                         ", 1, m_oLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                  If oAdjsGrupo.Item(sCod).HayPrecios Then
                    If oAdjsGrupo.Item(sCod).Adjudicado <> 0 Then
                        'Si el grupo est� cerrado cojo el importe adjudicado total del grupo
                        If oGrupo.Cerrado Then
                            dblConsumido = dblConsumido + (oAdjsGrupo.Item(sCod).Consumido / oAdjsGrupo.Item(sCod).Cambio)
                            dblAdjudicado = dblAdjudicado + (oAdjsGrupo.Item(sCod).Adjudicado / oAdjsGrupo.Item(sCod).Cambio)
                            dblAhorrado = dblAhorrado + (oAdjsGrupo.Item(sCod).Ahorrado / oAdjsGrupo.Item(sCod).Cambio)
                        Else
                            'Cojo s�lo los items que ya est�n adjudicados o los que se acaban de adjudicar (a�n no tienen la propiedad cerrado=1)
                            For Each oItem In oAdjsGrupo.Item(sCod).proceso.Grupos.Item(oGrupo.Codigo).Items
                                bProve = False
                                bRecienCerrado = False
                                For i = 1 To oAdjs.Count
                                    If oAdjs.Item(i).Id = oItem.Id Then
                                        bRecienCerrado = True
                                        Exit For
                                    End If
                                Next
                                'Para saber si se ha adjudicado al proveedor para el que estoy sacando la tabla
                                For i = 1 To oAdjsGrupo.Item(sCod).proceso.Grupos.Item(oGrupo.Codigo).Adjudicaciones.Count
                                    If oAdjsGrupo.Item(sCod).proceso.Grupos.Item(oGrupo.Codigo).Adjudicaciones.Item(i).Grupo = oGrupo.Codigo _
                                        And oAdjsGrupo.Item(sCod).proceso.Grupos.Item(oGrupo.Codigo).Adjudicaciones.Item(i).Id = oItem.Id _
                                        And oAdjsGrupo.Item(sCod).proceso.Grupos.Item(oGrupo.Codigo).Adjudicaciones.Item(i).ProveCod = oProve.Cod Then
                                            bProve = True
                                            Exit For
                                    End If
                                Next
                                If (oItem.Cerrado Or bRecienCerrado) And bProve Then
                                    dblAdjudicado = dblAdjudicado + oItem.ImporteAdj
                                    dblConsumido = dblConsumido + oItem.Consumido
                                    dblAhorrado = dblAhorrado + (oItem.Consumido - oItem.ImporteAdj)
                                End If
                            Next
                        End If
                    End If
                  End If
                End If
            Next  'ogrupo
            dblAdjudicadoTotal = dblAdjudicadoTotal + dblAdjudicado
            dblConsumidoTotal = dblConsumidoTotal + dblConsumido
            If dblAdjudicado <> 0 Then
                If (InStr(sCuerpoMensaje, "<!--COMIENZO_PROV-->") > 0) And (InStr(sCuerpoMensaje, "<!--FIN_PROV-->") > 0) Then
                    sBloque = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--COMIENZO_PROV-->"), (InStr(sCuerpoMensaje, "<!--FIN_PROV-->") + Len("<!--FIN_PROV-->")) - InStr(sCuerpoMensaje, "<!--COMIENZO_PROV-->"))
        
                    sBloque_aux = sBloque
    
                    sBloque_aux = Replace(sBloque_aux, "<!--COMIENZO_PROV-->", "")
                    sBloque_aux = Replace(sBloque_aux, "<!--FIN_PROV-->", "")
    
                    If Not bHTML Then
                        sBloque_aux = Replace(sBloque_aux, "@PROVEEDOR", oProve.Cod & " " & oProve.Den & ":" & " " & sCons & Format(dblConsumido, "#,##0.00") & " " & sAdj & Format(dblAdjudicado, "#,##0.00") & " " & sAhorr & Format(dblAhorrado, "#,##0.00"))
                    Else
                        sBloque_aux = Replace(sBloque_aux, "@PROVE", oProve.Cod & " " & oProve.Den)
                        sBloque_aux = Replace(sBloque_aux, "@CONS_PROVE", Format(dblConsumido, "#,##0.00"))
                        sBloque_aux = Replace(sBloque_aux, "@ADJ_PROVE", Format(dblAdjudicado, "#,##0.00"))
                        sBloque_aux = Replace(sBloque_aux, "@AHORR_PROVE", Format(dblAhorrado, "#,##0.00"))
                    End If
                        
                    If sLineas = "" Then
                        sLineas = sBloque_aux
                    Else
                        sLineas = sLineas & vbCrLf & sBloque_aux
                    End If
                End If
            End If
        End If
    Next    'Proveedores
    
    dblAhorroTotal = dblConsumidoTotal - dblAdjudicadoTotal
    dblPorcenTotal = dblAhorroTotal / dblConsumidoTotal * 100
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@ADJUDICADO", Format(dblAdjudicadoTotal, "#,##0.00"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CONSUMIDO", Format(dblConsumidoTotal, "#,##0.00"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@AHORRADO", Format(dblAhorroTotal, "#,##0.00"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PORCENTAJE", Format(dblPorcenTotal, "#,##0.00"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, sBloque, sLineas)
    
    'Si hay comprador responsable asignado
    If Not oProceso.responsable Is Nothing Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(oProceso.responsable.nombre))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(oProceso.responsable.Apel))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(oProceso.responsable.Tfno))
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(oProceso.responsable.mail))
    Else
        'Sino, se asigna a si mismo como responsable, siempre y cuando sea de tipo comprador
        If (oTipoDeUsuario = TipoDeUsuario.comprador) And (Not UsuarioSummit.comprador Is Nothing) Then
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", NullToStr(UsuarioSummit.comprador.nombre))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", NullToStr(UsuarioSummit.comprador.Apel))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", NullToStr(UsuarioSummit.comprador.Tfno))
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", NullToStr(UsuarioSummit.comprador.mail))
        Else
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOM_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@APE_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO_COMPRADOR", "")
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@MAIL_COMPRADOR", "")
        End If
    End If

    GenerarCuerpoMensajeAdjudicacionCierreParc = sCuerpoMensaje

    Exit Function
ERROR:
    If Err.Number <> 0 Then
        bDescargarFrm = oFSGSRaiz.TratarError("Class Module", "CEmailAdj", "GenerarCuerpoMensajeAdjudicacionCierreParc", Err, Erl, , bActivado)
        Exit Function
    End If
End Function

