VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRUnidades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' <summary>
''' Genera el listado de unidades de la aplicacion
''' </summary>
''' <param name="oReport">Report </param>
''' <param name="OrdCod">Ordenacion por codigo o por denominacion</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:frmLstUni.cmdObtenerClick; Tiempo m�ximo:0 </remarks>

Public Sub Listado(oReport As CRAXDRT.Report, Ordcod As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim RecordSortFields As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
                                                                
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
        
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
        
    If Ordcod = True Then
        RecordSortFields = "+{UNI.COD}"
    Else
        RecordSortFields = "+{UNI_DEN.DEN}"
    End If
        
    oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(RecordSortFields))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(RecordSortFields), crs_SortField_Interno(RecordSortFields))), crs_SortDirection_Interno(RecordSortFields)
    
    'oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "SEL")).Text = """" & SelectionText & """"
    
End Sub

