VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmailQA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Mensajes As CMensajes

Private m_oParametrosInstalacion As ParametrosInstalacion
Private m_oLongitudesDeCodigos As LongitudesDeCodigos

Public Property Get LongitudesDeCodigos() As LongitudesDeCodigos
    LongitudesDeCodigos = m_oLongitudesDeCodigos
End Property

Public Property Let LongitudesDeCodigos(ByRef vNewValue As LongitudesDeCodigos)
    m_oLongitudesDeCodigos = vNewValue
End Property

Public Property Get ParametrosInstalacion() As ParametrosInstalacion
    ParametrosInstalacion = m_oParametrosInstalacion
End Property

Public Property Let ParametrosInstalacion(ByRef vNewValue As ParametrosInstalacion)
    m_oParametrosInstalacion = vNewValue
End Property

''' <summary>Genera el cuerpo del mensaje para las notificaciones de paso de un proveedor de QA a real</summary>
''' <param name="sProve">Cod. proveedor</param>
''' <param name="sDenProve">Den. proveedor</param>
''' <param name="sIdi">Idioma</param>
''' <param name="bHTML">Formato email (html o txt)</param>
''' <remarks>Llamada desde: NotificarPasoProveedorQAaReal</remarks>

Public Function GenerarMensajePasoProveedorQAaReal(ByVal sProve As String, sDenProve As String, ByVal sIdi As String, ByVal bHTML As Boolean, ByVal bDesdePedido As Boolean, Optional ByRef oProceso As CProceso, _
        Optional ByVal iOrdenAnyo As Integer, Optional ByVal lNumPedido As Long, Optional ByVal lOrdenNumero As Long, Optional ByRef oAdjs As CAdjsGrupo) As String
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sFileName As String
    Dim sCausa As String
    Dim lPosIni As Long
    Dim lPosFin As Long
    Dim oGrupo As CGrupo
    
    If bHTML Then
        sFileName = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdi & "_FSQA_AvisoPasoProveQAaReal.htm"
    Else
        sFileName = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdi & "_FSQA_AvisoPasoProveQAaReal.txt"
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(sFileName) Then
        Screen.MousePointer = vbNormal
        Mensajes.PlantillaNoEncontrada sFileName
        Set oFos = Nothing
        GenerarMensajePasoProveedorQAaReal = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    
    sCuerpoMensaje = ostream.ReadAll
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVE", sProve)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVE", sDenProve)
    
    If bDesdePedido Then
        'Eliminar mensaje comparativa
        lPosIni = InStr(1, sCuerpoMensaje, "<!--INICIO_COMPARATIVA-->", vbTextCompare)
        lPosFin = InStr(1, sCuerpoMensaje, "<!--FIN_COMPARATIVA-->", vbTextCompare)
        lPosFin = lPosFin + Len("<!--FIN_COMPARATIVA-->")
        If lPosIni > 0 And lPosFin > 0 Then
            sCausa = Mid(sCuerpoMensaje, lPosIni, lPosFin - lPosIni)
            sCuerpoMensaje = Replace(sCuerpoMensaje, sCausa, "")
        End If
        
        sCuerpoMensaje = Replace(sCuerpoMensaje, "<!--INICIO_EMISION-->", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "<!--FIN_EMISION-->", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@PEDIDO", CStr(iOrdenAnyo) & "/" & CStr(lNumPedido) & "/" & CStr(lOrdenNumero))
    Else
        'Eliminar mensaje emisi�n
        lPosIni = InStr(1, sCuerpoMensaje, "<!--INICIO_EMISION-->", vbTextCompare)
        lPosFin = InStr(1, sCuerpoMensaje, "<!--FIN_EMISION-->", vbTextCompare)
        lPosFin = lPosFin + Len("<!--FIN_EMISION-->")
        If lPosIni > 0 And lPosFin > 0 Then
            sCausa = Mid(sCuerpoMensaje, lPosIni, lPosFin - lPosIni)
            sCuerpoMensaje = Replace(sCuerpoMensaje, sCausa, "")
        End If
    
        sCuerpoMensaje = Replace(sCuerpoMensaje, "<!--INICIO_COMPARATIVA-->", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "<!--FIN_COMPARATIVA-->", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@PROCESO", CStr(oProceso.Anyo) & "/" & oProceso.GMN1Cod & "/" & CStr(oProceso.Cod))
        
        lPosIni = InStr(1, sCuerpoMensaje, "@UON3", vbTextCompare)
        If lPosIni > 0 Then
            Dim sUON3Cod As String
            Dim sUON3Den As String
            
            'Obtengo la distribuci�n del primer item adjudicado al proveedor
            Dim oItem As CItem
            Dim sCod As String
            Dim bEncontrado As Boolean
            For Each oGrupo In oProceso.Grupos
                For Each oItem In oGrupo.Items
                    sCod = sProve & Mid$("                         ", 1, m_oLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
                    sCod = CStr(oItem.Id) & sCod
                    
                    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        oItem.cargarDistribuciones
                        If Not oItem.DistsNivel3 Is Nothing Then
                            If oItem.DistsNivel3.Count > 0 Then
                                sUON3Cod = oItem.DistsNivel3.Item(1).CodUON1 & "/" & oItem.DistsNivel3.Item(1).CodUON2 & "/" & oItem.DistsNivel3.Item(1).CodUON3
                                sUON3Den = oItem.DistsNivel3.Item(1).Den
                            End If
                        End If
                        
                        bEncontrado = True
                        Exit For
                    End If
                Next
                
                If bEncontrado Then Exit For
            Next
            Set oGrupo = Nothing
            Set oItem = Nothing
            
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@UON3", sUON3Cod & " - " & sUON3Den)
        End If
        lPosIni = InStr(1, sCuerpoMensaje, "@IMPORTEADJUDICADOALPROVEEDOR", vbTextCompare)
        If lPosIni > 0 Then
            Dim dImporteAdjudicado As Double
            Dim oAdj As CAdjGrupo
            If Not oAdjs Is Nothing Then
                For Each oAdj In oAdjs
                    If oAdj.Prove = sProve Then
                        dImporteAdjudicado = dImporteAdjudicado + oAdj.AdjudicadoMonProce
                    End If
                Next
            End If
            Set oGrupo = Nothing
            Set oAdj = Nothing
            
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@IMPORTEADJUDICADOALPROVEEDOR", Format(dImporteAdjudicado, "#,##0.00") & " (" & oProceso.MonCod & ")")
        End If
    End If
    
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing

    GenerarMensajePasoProveedorQAaReal = sCuerpoMensaje
End Function

'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSQA_NotifAsigMatQAProve.txt o                    ***
'***              ENG_FSQA_NotifAsigMatQAProve.txt o                    ***
'***              GER_FSQA_NotifAsigMatQAProve.txt o                    ***
'***              dependi�ndo del idioma.                               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
Public Function GenerarMensajeNotificarAsigMatQA(ByVal Ador As adodb.Recordset, Optional ByVal sIdi As String = "SPA", Optional ByVal bHTML As Boolean = False) As String
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sFileName As String
    'Variables que me ayudar�n a tener en memoria los distintos Bloques del Cuerpo:
    Dim sBloqueProve As String
    Dim sBloqueAsig As String
    Dim sBloqueDesAsig As String
    Dim sBloqueMatAsig As String
    Dim sBloqueMatDesAsig As String
    'Variables Auxiliares que obtendr�n una copia para modificarla y meterla en el cuerpo
    Dim sBloqueProve_Aux As String
    Dim sBloqueAsig_Aux As String
    Dim sBloqueDesAsig_Aux As String
    Dim sBloqueMatAsig_Aux As String
    Dim sBloqueMatDesAsig_Aux As String
    'Variables que me ayudar�n a recorrer los Datos
    Dim sProveAct As String
    'Variables que ayudaran a meter los distintos Bloques en el Cuerpo
    Dim lCuerpoIndex As Long
    Dim lProveIndex As Long
    Dim lAsigIndex As Long
    Dim lDesAsigIndex As Long
    
    'Defino los nombres de los delimitadores de cada Bloque
    Dim sLimProve As String
    sLimProve = "BLOQUE_PROVEEDOR"
    Dim sLimAsig As String
    sLimAsig = "BLOQUE_ASIGNADOS"
    Dim sLimDesAsig As String
    sLimDesAsig = "BLOQUE_DESASIGNADOS"
    Dim sLimMatAsig As String
    sLimMatAsig = "BLOQUE_MATERIAL_ASIG"
    Dim sLimMatDesAsig As String
    sLimMatDesAsig = "BLOQUE_MATERIAL_DESASIG"
    
    'Coge la plantilla de par�metros de la instalaci�n
    If bHTML Then
        sFileName = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdi & "_FSQA_NotifAsigMatQAProve.html"
    Else
        sFileName = m_oParametrosInstalacion.gsPathSolicitudes & "\" & sIdi & "_FSQA_NotifAsigMatQAProve.txt"
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(sFileName) Then
        Screen.MousePointer = vbNormal
        Mensajes.PlantillaNoEncontrada sFileName
        Set oFos = Nothing
        GenerarMensajeNotificarAsigMatQA = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    
    If (InStr(sCuerpoMensaje, "<!--INICIO " & sLimProve & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--FIN " & sLimProve & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--INICIO " & sLimAsig & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--FIN " & sLimAsig & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--INICIO " & sLimDesAsig & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--FIN " & sLimDesAsig & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--INICIO " & sLimMatAsig & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--FIN " & sLimMatAsig & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--INICIO " & sLimMatDesAsig & "-->") > 0) And _
        (InStr(sCuerpoMensaje, "<!--FIN " & sLimMatDesAsig & "-->") > 0) Then
        
        'Obtengo los Bloques del Cuerpo en Memoria, y los quito del Cuerpo
        sBloqueMatAsig = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO " & sLimMatAsig & "-->"), (InStr(sCuerpoMensaje, "<!--FIN " & sLimMatAsig & "-->") + Len("<!--FIN " & sLimMatAsig & "-->")) - InStr(sCuerpoMensaje, "<!--INICIO " & sLimMatAsig & "-->"))
        sBloqueAsig = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO " & sLimAsig & "-->"), (InStr(sCuerpoMensaje, "<!--FIN " & sLimAsig & "-->") + Len("<!--FIN " & sLimAsig & "-->")) - InStr(sCuerpoMensaje, "<!--INICIO " & sLimAsig & "-->"))
        sBloqueMatDesAsig = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO " & sLimMatDesAsig & "-->"), (InStr(sCuerpoMensaje, "<!--FIN " & sLimMatDesAsig & "-->") + Len("<!--FIN " & sLimMatDesAsig & "-->")) - InStr(sCuerpoMensaje, "<!--INICIO " & sLimMatDesAsig & "-->"))
        sBloqueDesAsig = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO " & sLimDesAsig & "-->"), (InStr(sCuerpoMensaje, "<!--FIN " & sLimDesAsig & "-->") + Len("<!--FIN " & sLimDesAsig & "-->")) - InStr(sCuerpoMensaje, "<!--INICIO " & sLimDesAsig & "-->"))
        sBloqueProve = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO " & sLimProve & "-->"), (InStr(sCuerpoMensaje, "<!--FIN " & sLimProve & "-->") + Len("<!--FIN " & sLimProve & "-->")) - InStr(sCuerpoMensaje, "<!--INICIO " & sLimProve & "-->"))
        lCuerpoIndex = InStr(sCuerpoMensaje, sBloqueProve)
        sCuerpoMensaje = Replace(sCuerpoMensaje, sBloqueProve, "")
        
        Ador.MoveFirst
        While Not Ador.EOF
            'Si es un nuevo Proveedor
            If sProveAct <> NullToStr(Ador("COD_PROVE").Value) Then
                'Meto el Bloque_auxiliar de Asignados dentro del Bloque_auxiliar del Proveedor anterior
                If (sBloqueAsig_Aux <> "") And (sBloqueAsig_Aux <> Replace(Replace(Replace(sBloqueAsig, "<!--INICIO " & sLimAsig & "-->", ""), "<!--FIN " & sLimAsig & "-->", ""), sBloqueMatAsig, "")) Then
                    sBloqueProve_Aux = Left$(sBloqueProve_Aux, lProveIndex) & sBloqueAsig_Aux & Right$(sBloqueProve_Aux, Len(sBloqueProve_Aux) - lProveIndex)
                    lProveIndex = lProveIndex + Len(sBloqueAsig_Aux)
                End If
                'Meto el Bloque_auxiliar de DesAsignados dentro del Bloque_auxiliar del Proveedor anterior
                If (sBloqueDesAsig_Aux <> "") And (sBloqueDesAsig_Aux <> Replace(Replace(Replace(sBloqueDesAsig, "<!--INICIO " & sLimDesAsig & "-->", ""), "<!--FIN " & sLimDesAsig & "-->", ""), sBloqueMatDesAsig, "")) Then
                    sBloqueProve_Aux = Left$(sBloqueProve_Aux, lProveIndex) & sBloqueDesAsig_Aux & Right$(sBloqueProve_Aux, Len(sBloqueProve_Aux) - lProveIndex)
                    lProveIndex = lProveIndex + Len(sBloqueDesAsig_Aux)
                End If
                'Meto el Bloque_auxiliar del Proveedor anterior en el cuerpo del mensaje
                If (sBloqueProve_Aux <> "") Then
                    sCuerpoMensaje = Left$(sCuerpoMensaje, lCuerpoIndex) & sBloqueProve_Aux & Right$(sCuerpoMensaje, Len(sCuerpoMensaje) - lCuerpoIndex)
                    lCuerpoIndex = lCuerpoIndex + Len(sBloqueProve_Aux)
                End If
                
                'Creo un nuevo Bloque_auxiliar de Proveedor (Sin Limitadores)
                sBloqueProve_Aux = Replace(Replace(sBloqueProve, "<!--INICIO " & sLimProve & "-->", ""), "<!--FIN " & sLimProve & "-->", "")
                'Y le doy Valores (COD_PROVE,DEN_PROVE)
                sBloqueProve_Aux = Replace(sBloqueProve_Aux, "@COD_PROVE", NullToStr(Ador("COD_PROVE").Value))
                sBloqueProve_Aux = Replace(sBloqueProve_Aux, "@DEN_PROVE", NullToStr(Ador("DEN_PROVE").Value))
                'Ahora Inicializo el Indice interno
                lProveIndex = InStr(sBloqueProve_Aux, sBloqueAsig)
                'Y le quito los Bloques contenidos en el
                sBloqueProve_Aux = Replace(Replace(sBloqueProve_Aux, sBloqueAsig, ""), sBloqueDesAsig, "")
                
                'Creo un nuevo Bloque_auxiliar de Asignados (Sin Limitadores)
                sBloqueAsig_Aux = Replace(Replace(sBloqueAsig, "<!--INICIO " & sLimAsig & "-->", ""), "<!--FIN " & sLimAsig & "-->", "")
                'Ahora Inicializo el Indice interno
                lAsigIndex = InStr(sBloqueAsig_Aux, sBloqueMatAsig)
                'Y le quito los Bloques contenidos en el
                sBloqueAsig_Aux = Replace(sBloqueAsig_Aux, sBloqueMatAsig, "")
                
                'Creo un nuevo Bloque_auxiliar de DesAsignados (Sin Limitadores)
                sBloqueDesAsig_Aux = Replace(Replace(sBloqueDesAsig, "<!--INICIO " & sLimDesAsig & "-->", ""), "<!--FIN " & sLimDesAsig & "-->", "")
                'Ahora Inicializo el Indice interno
                lDesAsigIndex = InStr(sBloqueDesAsig_Aux, sBloqueMatDesAsig)
                'Y le quito los Bloques contenidos en el
                sBloqueDesAsig_Aux = Replace(sBloqueDesAsig_Aux, sBloqueMatDesAsig, "")
            End If
            
            Dim sSeparadorGMN As String
            If NullToDbl0(Ador("ASIG").Value) = 1 Then
                'Asignado:
                
                'Creo un nuevo Bloque_auxiliar de Material (Sin Limitadores)
                sBloqueMatAsig_Aux = Replace(Replace(sBloqueMatAsig, "<!--INICIO " & sLimMatAsig & "-->", ""), "<!--FIN " & sLimMatAsig & "-->", "")
                'Y le doy Valores (DEN_QA,GMN1,GMN2,GMN3,GMN4,DEN_GS)
                sBloqueMatAsig_Aux = Replace(sBloqueMatAsig_Aux, "@DEN_QA", NullToStr(Ador("DEN_" & sIdi & "_QA").Value))
                
                If NullToStr(Ador("GMN1").Value) <> "" Then
                    sSeparadorGMN = " - "
                Else
                    sSeparadorGMN = ""
                End If
                sBloqueMatAsig_Aux = Replace(sBloqueMatAsig_Aux, "@GMN1", NullToStr(Ador("GMN1").Value) & sSeparadorGMN)
                If NullToStr(Ador("GMN2").Value) <> "" Then
                    sSeparadorGMN = " - "
                Else
                    sSeparadorGMN = ""
                End If
                sBloqueMatAsig_Aux = Replace(sBloqueMatAsig_Aux, "@GMN2", NullToStr(Ador("GMN2").Value) & sSeparadorGMN)
                If NullToStr(Ador("GMN3").Value) <> "" Then
                    sSeparadorGMN = " - "
                Else
                    sSeparadorGMN = ""
                End If
                sBloqueMatAsig_Aux = Replace(sBloqueMatAsig_Aux, "@GMN3", NullToStr(Ador("GMN3").Value) & sSeparadorGMN)
                If NullToStr(Ador("GMN4").Value) <> "" Then
                    sSeparadorGMN = " - "
                Else
                    sSeparadorGMN = ""
                End If
                sBloqueMatAsig_Aux = Replace(sBloqueMatAsig_Aux, "@GMN4", NullToStr(Ador("GMN4").Value) & sSeparadorGMN)
                sBloqueMatAsig_Aux = Replace(sBloqueMatAsig_Aux, "@DEN_GS", NullToStr(Ador("DEN_" & sIdi & "_GS").Value))
                
                'Meto el Bloque_auxilar de Material dentro del Bloque_auxiliar de Asignados
                sBloqueAsig_Aux = Left$(sBloqueAsig_Aux, lAsigIndex) & sBloqueMatAsig_Aux & Right$(sBloqueAsig_Aux, Len(sBloqueAsig_Aux) - lAsigIndex)
                lAsigIndex = lAsigIndex + Len(sBloqueMatAsig_Aux)
            Else
                'Desasignado:
                
                'Creo un nuevo Bloque_auxiliar de Material (Sin Limitadores)
                sBloqueMatDesAsig_Aux = Replace(Replace(sBloqueMatDesAsig, "<!--INICIO " & sLimMatDesAsig & "-->", ""), "<!--FIN " & sLimMatDesAsig & "-->", "")
                'Y le doy Valores (DEN_QA,GMN1,GMN2,GMN3,GMN4,DEN_GS)
                sBloqueMatDesAsig_Aux = Replace(sBloqueMatDesAsig_Aux, "@DEN_QA", NullToStr(Ador("DEN_" & sIdi & "_QA").Value))
                
                If NullToStr(Ador("GMN1").Value) <> "" Then
                    sSeparadorGMN = " - "
                Else
                    sSeparadorGMN = ""
                End If
                sBloqueMatDesAsig_Aux = Replace(sBloqueMatDesAsig_Aux, "@GMN1", NullToStr(Ador("GMN1").Value) & sSeparadorGMN)
                If NullToStr(Ador("GMN2").Value) <> "" Then
                    sSeparadorGMN = " - "
                Else
                    sSeparadorGMN = ""
                End If
                sBloqueMatDesAsig_Aux = Replace(sBloqueMatDesAsig_Aux, "@GMN2", NullToStr(Ador("GMN2").Value) & sSeparadorGMN)
                If NullToStr(Ador("GMN3").Value) <> "" Then
                    sSeparadorGMN = " - "
                Else
                    sSeparadorGMN = ""
                End If
                sBloqueMatDesAsig_Aux = Replace(sBloqueMatDesAsig_Aux, "@GMN3", NullToStr(Ador("GMN3").Value) & sSeparadorGMN)
                If NullToStr(Ador("GMN4").Value) <> "" Then
                    sSeparadorGMN = " - "
                Else
                    sSeparadorGMN = ""
                End If
                sBloqueMatDesAsig_Aux = Replace(sBloqueMatDesAsig_Aux, "@GMN4", NullToStr(Ador("GMN4").Value) & sSeparadorGMN)
                sBloqueMatDesAsig_Aux = Replace(sBloqueMatDesAsig_Aux, "@DEN_GS", NullToStr(Ador("DEN_" & sIdi & "_GS").Value))
                
                'Meto el Bloque_auxilar de Material dentro del Bloque_auxiliar de DesAsignados
                sBloqueDesAsig_Aux = Left$(sBloqueDesAsig_Aux, lDesAsigIndex) & sBloqueMatDesAsig_Aux & Right$(sBloqueDesAsig_Aux, Len(sBloqueDesAsig_Aux) - lDesAsigIndex)
                lDesAsigIndex = lDesAsigIndex + Len(sBloqueMatDesAsig_Aux)
            End If
            sProveAct = NullToStr(Ador("COD_PROVE").Value)
            Ador.MoveNext
        Wend
        'Meto el Bloque_auxiliar de Asignados dentro del Bloque_auxiliar del Proveedor anterior
        If (sBloqueAsig_Aux <> "") And (sBloqueAsig_Aux <> Replace(Replace(Replace(sBloqueAsig, "<!--INICIO " & sLimAsig & "-->", ""), "<!--FIN " & sLimAsig & "-->", ""), sBloqueMatAsig, "")) Then
            sBloqueProve_Aux = Left$(sBloqueProve_Aux, lProveIndex) & sBloqueAsig_Aux & Right$(sBloqueProve_Aux, Len(sBloqueProve_Aux) - lProveIndex)
            lProveIndex = lProveIndex + Len(sBloqueAsig_Aux)
        End If
        'Meto el Bloque_auxiliar de DesAsignados dentro del Bloque_auxiliar del Proveedor anterior
        If (sBloqueDesAsig_Aux <> "") And (sBloqueDesAsig_Aux <> Replace(Replace(Replace(sBloqueDesAsig, "<!--INICIO " & sLimDesAsig & "-->", ""), "<!--FIN " & sLimDesAsig & "-->", ""), sBloqueMatDesAsig, "")) Then
            sBloqueProve_Aux = Left$(sBloqueProve_Aux, lProveIndex) & sBloqueDesAsig_Aux & Right$(sBloqueProve_Aux, Len(sBloqueProve_Aux) - lProveIndex)
        End If
        'Meto el Bloque_auxiliar del Proveedor anterior en el cuerpo del mensaje
        If (sBloqueProve_Aux <> "") Then
            sCuerpoMensaje = Left$(sCuerpoMensaje, lCuerpoIndex) & sBloqueProve_Aux & Right$(sCuerpoMensaje, Len(sCuerpoMensaje) - lCuerpoIndex)
        End If
    Else
        sCuerpoMensaje = ""
    End If
    
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing

    GenerarMensajeNotificarAsigMatQA = sCuerpoMensaje
End Function

