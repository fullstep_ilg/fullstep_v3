VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRPresPorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public SincronizacionMat As Boolean
Public NEM As Integer   'basParametros.gParametrosGenerales.giNEM
Public IdiomaPortal As String   'gParametrosInstalacion.gIdiomaPortal

Public sMatSeleccionado As String

Public Sub Listado(oReport As CRAXDRT.Report, Optional ByVal CodGMN1 As String, Optional ByVal CodGMN2 As String, Optional ByVal CodGMN3 As String, Optional ByVal AnyoSeleccionado As Integer, Optional ByVal bRMat As Boolean, Optional ByVal Eqp As String, Optional ByVal Com As String, Optional ByVal OrdDen As Boolean, Optional ByVal Ordcod As Boolean, Optional ByVal OrdPres As Boolean, Optional ByVal OrdObj As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim SQLQueryString As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    SQLQueryString = ObtenerSQLListado(CodGMN1, CodGMN2, CodGMN3, AnyoSeleccionado, bRMat, Eqp, Com, OrdDen, Ordcod, OrdPres, OrdObj)
    oReport.SQLQueryString = SQLQueryString
    
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "ANIO")).Text = """" & CStr(AnyoSeleccionado) & """"
    
End Sub

Private Function ObtenerSQLListado(CodGMN1 As String, CodGMN2 As String, CodGMN3 As String, AnyoSeleccionado As Variant, bRMat As Boolean, Eqp As String, Com As String, OrdDen As Boolean, Ordcod As Boolean, OrdPres As Boolean, OrdObj As Boolean) As String
Dim sConsulta As String

Select Case NEM

    Case 1
    Case 2
    Case 3
    Case 4 'Cuatro niveles de material
            
        sConsulta = "SELECT PRES_MAT4.ANYO,PRES_MAT4.PRES, PRES_MAT4.OBJ,GMN4.GMN1 ,GMN1." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ",GMN4.GMN2,GMN2." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & " ,GMN4.GMN3,GMN3." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & "  ,GMN4.COD ,GMN4." & DevolverDenGMN(SincronizacionMat, IdiomaPortal)
        'Hay restriccion al material del comprador
        If bRMat Then
            sConsulta = sConsulta & " FROM (GMN4 INNER JOIN COM_GMN4 ON GMN4.GMN1=COM_GMN4.GMN1 AND GMN4.GMN2=COM_GMN4.GMN2 AND GMN4.GMN3=COM_GMN4.GMN3 AND GMN4.COD=COM_GMN4.GMN4 AND COM_GMN4.EQP='" & DblQuote(Eqp) & "' AND COM_GMN4.COM='" & DblQuote(Com) & "' ) LEFT JOIN PRES_MAT4 ON PRES_MAT4.GMN4 = GMN4.COD AND PRES_MAT4.GMN3 = GMN4.GMN3 AND PRES_MAT4.GMN2 = GMN4.GMN2 AND PRES_MAT4.GMN1 = GMN4.GMN1 "
            If Not IsEmpty(AnyoSeleccionado) Then
                sConsulta = sConsulta & " AND ANYO=" & AnyoSeleccionado
            End If
            sConsulta = sConsulta & " INNER JOIN GMN1 ON GMN1.COD=GMN4.GMN1 INNER JOIN GMN2 ON GMN2.GMN1 = GMN4.GMN1 AND GMN2.COD = GMN4.GMN2 "
            sConsulta = sConsulta & " INNER JOIN GMN3 ON GMN3.GMN1 = GMN4.GMN1 AND GMN3.GMN2 = GMN4.GMN2 AND GMN3.COD = GMN4.GMN3"
                                
            sConsulta = sConsulta & " WHERE 1=1 "
              
        Else
            
            sConsulta = sConsulta & " FROM GMN4 LEFT JOIN PRES_MAT4 ON PRES_MAT4.GMN4 = GMN4.COD AND PRES_MAT4.GMN3 = GMN4.GMN3 AND PRES_MAT4.GMN2 = GMN4.GMN2 AND PRES_MAT4.GMN1 = GMN4.GMN1 "
            If Not IsEmpty(AnyoSeleccionado) Then
                sConsulta = sConsulta & " AND ANYO=" & AnyoSeleccionado
            End If
            sConsulta = sConsulta & " INNER JOIN GMN1 ON GMN1.COD=GMN4.GMN1 INNER JOIN GMN2 ON GMN2.GMN1 = GMN4.GMN1 AND GMN2.COD = GMN4.GMN2 "
            sConsulta = sConsulta & " INNER JOIN GMN3 ON GMN3.GMN1 = GMN4.GMN1 AND GMN3.GMN2 = GMN4.GMN2 AND GMN3.COD = GMN4.GMN3"
                
            sConsulta = sConsulta & " WHERE 1=1 "
                
        End If
                
        If Trim(CodGMN1) <> "" Then
            sConsulta = sConsulta & " AND GMN4.GMN1='" & CodGMN1 & "'"
            sMatSeleccionado = Trim(CodGMN1)
        End If
                
        If Trim(CodGMN2) <> "" Then
            sConsulta = sConsulta & " AND GMN4.GMN2='" & CodGMN2 & "'"
            sMatSeleccionado = sMatSeleccionado & " - " & Trim(CodGMN2)
        End If
                
        If Trim(CodGMN3) <> "" Then
            sConsulta = sConsulta & " AND GMN4.GMN3='" & CodGMN3 & "'"
            sMatSeleccionado = sMatSeleccionado & " - " & Trim(CodGMN3)
        End If
                               
        If OrdDen Then
            sConsulta = sConsulta & " ORDER BY GMN1." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ", GMN2." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ", GMN3." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ", GMN4." & DevolverDenGMN(SincronizacionMat, IdiomaPortal)
        End If
         If Ordcod Then
            sConsulta = sConsulta & " ORDER BY GMN4.GMN1, GMN4.GMN2, GMN4.GMN3, GMN4.COD"
        End If
        If OrdPres Then
            sConsulta = sConsulta & " ORDER BY GMN4.GMN1, GMN4.GMN2, GMN4.GMN3, PRES_MAT4.PRES"
        End If
        If OrdObj Then
            sConsulta = sConsulta & " ORDER BY GMN4.GMN1, GMN4.GMN2, GMN4.GMN3, PRES_MAT4.OBJ"
        End If
       
End Select
ObtenerSQLListado = sConsulta

End Function





