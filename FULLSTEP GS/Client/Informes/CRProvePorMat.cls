VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRProvePorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public CodUsuario As String     'basOptimizacion.gvarCodUsuario

Private oFos As FileSystemObject

Public Function ListadoProvePorMat(oReport As CRAXDRT.Report, Optional ByVal sGMN1Cod As String, Optional ByVal sGMN2Cod As String, Optional ByVal sGMN3Cod As String, Optional ByVal sGMN4Cod As String, Optional ByVal bREqp As Boolean, Optional ByVal gCodEqpUsuario As String, Optional ByVal gCodCompradorUsuario As String, Optional ByVal bOrdCod As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim Srpt As CRAXDRT.Report
    Dim RecordSelFormula  As String
    Dim SubProveSelFormula As String
    Dim SubRecordSortField As String
    Dim oConnectionInfo As CRAXDRT.ConnectionProperties
    Dim sOrden As String
    Dim i As Integer
    
    
    'Si se ha elegido una rama concreta de la estructura
    RecordSelFormula = ""
    If sGMN1Cod <> "" Then
        RecordSelFormula = "{MAT_RESTR.COD1}='" & sGMN1Cod & "'"
        If sGMN2Cod <> "" Then
            RecordSelFormula = RecordSelFormula & " and {MAT_RESTR.COD2}='" & sGMN2Cod & "'"
            If sGMN3Cod <> "" Then
                RecordSelFormula = RecordSelFormula & " and {MAT_RESTR.COD3}='" & sGMN3Cod & "'"
                If sGMN4Cod <> "" Then
                     RecordSelFormula = RecordSelFormula & " and {MAT_RESTR.COD4}='" & sGMN4Cod & "'"
                End If
            End If
        End If
    End If
    
    For Each Table In oReport.Database.Tables
        Set oConnectionInfo = Table.ConnectionProperties
        oConnectionInfo.Item("Provider") = "SQLOLEDB"
        oConnectionInfo.Item("Data Source") = scrs_Server
        oConnectionInfo.Item("Initial Catalog") = scrs_Database
        oConnectionInfo.Item("Integrated Security") = False
        oConnectionInfo.Item("Password") = scrs_Password
        oConnectionInfo.Item("User ID") = scrs_User
        Table.Location = scrs_Database & ".dbo." & Table.Name
        Set oConnectionInfo = Nothing
    Next
    
    If bREqp Then SubProveSelFormula = "{PROVE_EQP.EQP}='" & DblQuote(gCodEqpUsuario) & "'"
    
    oReport.ParameterFields(crs_ParameterIndex_Interno(oReport, "@EQP")).SetCurrentValue gCodEqpUsuario, 12
    oReport.ParameterFields(crs_ParameterIndex_Interno(oReport, "@COM")).SetCurrentValue gCodCompradorUsuario, 12
    oReport.ParameterFields(crs_ParameterIndex_Interno(oReport, "@USU")).SetCurrentValue CodUsuario, 12
    
    If bOrdCod Then
        sOrden = "COD"
        SubRecordSortField = "+{PROVE.COD}"
    Else
        sOrden = "DEN"
        SubRecordSortField = "+{PROVE.DEN}"
    End If
    oReport.ParameterFields(crs_ParameterIndex_Interno(oReport, "@ORDEN")).SetCurrentValue sOrden, 12
    
    oReport.EnableParameterPrompting = False
    oReport.RecordSelectionFormula = RecordSelFormula

    ' formula field para Clasificación grupos
    For i = 1 To 3
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "ORD_M" & i)).Text = "{MAT_RESTR." & sOrden & i & "}"
    Next i


    For i = 1 To 4
        Set Srpt = Nothing
        Set Srpt = oReport.OpenSubreport("rptSUBProveMat" & i & ".rpt")
        
        For Each Table In Srpt.Database.Tables
            Set oConnectionInfo = Table.ConnectionProperties
            oConnectionInfo.Item("Provider") = "SQLOLEDB"
            oConnectionInfo.Item("Data Source") = scrs_Server
            oConnectionInfo.Item("Initial Catalog") = scrs_Database
            oConnectionInfo.Item("Integrated Security") = False
            oConnectionInfo.Item("Password") = scrs_Password
            oConnectionInfo.Item("User ID") = scrs_User
            Table.Location = scrs_Database & ".dbo." & Table.Name
            Set oConnectionInfo = Nothing
        Next
        
        While Srpt.RecordSortFields.Count > 0
            Srpt.RecordSortFields.Delete 1
        Wend
        Srpt.RecordSortFields.Add Srpt.Database.Tables(crs_Tableindex_Interno(Srpt, crs_SortTable_Interno(SubRecordSortField))).Fields(crs_FieldIndex_Interno(Srpt, crs_SortTable_Interno(SubRecordSortField), crs_SortField_Interno(SubRecordSortField))), crs_SortDirection_Interno(SubRecordSortField)

        If SubProveSelFormula <> "" Then
             Srpt.RecordSelectionFormula = Srpt.RecordSelectionFormula & " AND " & SubProveSelFormula
        End If
    Next i

End Function



