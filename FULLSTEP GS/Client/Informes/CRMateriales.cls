VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRMateriales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' <summary>Listado de materiales</summary>
''' <param name="oReport">Informe</param>
''' <param name="Comp">Comp</param>
''' <param name="Eqp">Equipo</param>
''' <param name="GMN1Cod">Cod. GMN1</param>
''' <param name="GMN2Cod">Cod. GMN2</param>
''' <param name="GMN3Cod">Cod. GMN3</param>
''' <param name="GMN4Cod">Cod. GMN4</param>
''' <param name="IncluirItems"></param>
''' <param name="OrdenPorDen"></param>
''' <param name="bArtGen">Filtro por gener�co</param>
''' <param name="oAtributos">Atributos para filtrar</param>
''' <returns>Booleano indicando si el listado se ha abierto correctamente</returns>
''' <remarks>Llamada desde: frmLstMAT.cmdObtener_Click  Tiempo m�ximo: 1</remarks>
''' <remarks>Revisado por: LTG  Fecha: 17/08/2011</remarks>

Public Function Listado(ByRef oGestorInformes As CGestorInformes, oReport As CRAXDRT.Report, Comp As String, Eqp As String, GMN1Cod As String, GMN2Cod As String, _
        GMN3Cod As String, GMN4Cod As String, IncluirItems As Boolean, OrdenPorDen As Boolean, Optional ByVal bArtGen As Boolean, Optional ByVal oAtributos As CAtributos, _
         Optional bMostrarSoloCentrales As Boolean, Optional ByVal brRestMatUsu As Boolean, Optional ByVal brRestMatperf As Boolean, _
            Optional ByVal lIdPerfil As Integer, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, _
            Optional ByVal sUON3 As String, Optional ByRef oUons As CUnidadesOrganizativas, Optional ByVal bIncluirAscendientesUon As Boolean, _
            Optional ByVal bIncluirDescendientesUon As Boolean) As Boolean
            
    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim SubListado As CRAXDRT.Report
    Dim GroupOrder(1 To 2, 1 To 4) As String
    ' ARRAY sParametros, Elemento(2, i) = Nombre par�metro en rpt ; Elemento(1, i) = valor par�metro
    Dim sParametros(1 To 3, 1 To 3) As String
    Dim RecordSortFields As String
    Dim sRecordSelectionFormula As String
    Dim oAdoRes As Ador.Recordset
    Dim adoArt As adodb.Recordset
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Function
    End If
    
    If OrdenPorDen = False Then
        If IncluirItems Then
            'RecordSortFields = "+{Mat_Restr.COD}"
        End If
    Else
        If IncluirItems Then
            'RecordSortFields = "+{Mat_Restr.DEN}"
        End If
    End If
       
    ' ORDER BY A SUBREPORT
    'Set SubListado = oReport.OpenSubreport("ITEMS")
    
    'edu pm436
    'Solucionado error de migracion de a Crystal Reports 10 mediante nueva funcion en CRbasPublic
    
    'ConectarReportInterno SubListado, scrs_Server, scrs_Database, scrs_User, scrs_Password
        
    'While SubListado.RecordSortFields.Count > 0
    '    SubListado.RecordSortFields.Delete 1
   ' Wend
        
    'If Not RecordSortFields = "" Then
    '    SubListado.RecordSortFields.Add SubListado.Database.Tables(crs_Tableindex_Interno(SubListado, crs_SortTable_Interno(RecordSortFields))).Fields(crs_FieldIndex_Interno(SubListado, crs_SortTable_Interno(RecordSortFields), crs_SortField_Interno(RecordSortFields))), crs_SortDirection_Interno(RecordSortFields)
    'End If
    
'    Set adoArt = oGestorInformes.ListadoMaterialesArticulos(GMN1Cod, GMN2Cod, GMN3Cod, GMN4Cod, bArtGen, oAtributos, bMostrarSoloCentrales, brRestMatUsu, brRestMatperf, lIdPerfil, sUON1, sUON2, sUON3, oUons, bIncluirAscendientesUon, bIncluirDescendientesUon)
'    If Not adoArt Is Nothing Then
'        If Not adoArt.EOF Then
'            SubListado.Database.SetDataSource adoArt
'        End If
'    Else
'        'Ocultar el subinforme si no hay datos
'        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OcultarItem")).Text = """S"""
'        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "OcultarCampos")).Text = """S"""
'    End If
'
'    Set SubListado = Nothing
End Function
