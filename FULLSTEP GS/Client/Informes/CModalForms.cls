VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CModalForms"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function MostrarFormLstUNI(ByVal oGestorIdiomas As CGestorIdiomas, ByVal Idioma As String, ByRef oMensajes As CMensajes, ByRef oGestorParametros As CGestorParametros, _
        ByRef sParamInstRPTPATH As String, ByVal sParamGenRPTPATH, ByRef bGuardarParametrosIns As Boolean, ByRef sTitulo As String) As Object
    Dim oFrm As frmLstUNI
        
    Set oFrm = New frmLstUNI
    With oFrm
        Set .GestorIdiomas = oGestorIdiomas
        Set .Mensajes = oMensajes
        Set .GestorParametros = oGestorParametros
        .Idioma = Idioma
        .ParamInstRPTPATH = sParamInstRPTPATH
        .ParamGenRPTPATH = sParamGenRPTPATH
        .GuardarParametrosIns = bGuardarParametrosIns
        
        .Show vbModal
        
        bGuardarParametrosIns = .GuardarParametrosIns
        sTitulo = .Titulo
        Set MostrarFormLstUNI = .Report
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Function

Public Function MostrarFormLstMON(ByVal oGestorIdiomas As CGestorIdiomas, ByVal oGestorInformes As CGestorInformes, ByVal Idioma As String, ByRef oMensajes As CMensajes, ByRef oGestorParametros As CGestorParametros, _
        ByRef sParamInstRPTPATH As String, ByVal sParamGenRPTPATH, ByRef bGuardarParametrosIns As Boolean, ByRef sTitulo As String) As Object
    Dim oFrm As frmLstMON
        
    Set oFrm = New frmLstMON
    With oFrm
        Set .Mensajes = oMensajes
        Set .GestorInformes = oGestorInformes
        Set .GestorIdiomas = oGestorIdiomas
        Set .GestorParametros = oGestorParametros
        .ParamInstRPTPATH = sParamInstRPTPATH
        .ParamGenRPTPATH = sParamGenRPTPATH
        .Idioma = Idioma
        .GuardarParametrosIns = bGuardarParametrosIns
        
        .Show vbModal
        
        bGuardarParametrosIns = .GuardarParametrosIns
        sTitulo = .Titulo
        Set MostrarFormLstMON = .Report
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Function

Public Function MostrarFormLstPERFIL(ByVal oGestorIdiomas As CGestorIdiomas, ByVal oGestorInformes As CGestorInformes, ByVal Idioma As String, ByRef oMensajes As CMensajes, ByRef oGestorParametros As CGestorParametros, _
        ByRef sParamInstRPTPATH As String, ByVal sParamGenRPTPATH, ByRef bGuardarParametrosIns As Boolean, ByRef sTitulo As String, ByVal iCargaMaximaCombos As Integer, _
        ByRef oGestorSeguridad As CGestorSeguridad, ByRef oRaiz As CRaiz, ByVal gsPlurPres1 As String, ByVal gsPlurPres2 As String, ByVal gsPlurPres3 As String, _
         ByVal gsPlurPres4 As String, ByVal gbOblProveEqp As Boolean) As Object
    Dim oFrm As frmLstPERFIL
        
    Set oFrm = New frmLstPERFIL
    With oFrm
        Set .Mensajes = oMensajes
        Set .GestorInformes = oGestorInformes
        Set .GestorIdiomas = oGestorIdiomas
        Set .GestorParametros = oGestorParametros
        Set .GestorSeguridad = oGestorSeguridad
        Set .oRaiz = oRaiz
        .ParamInstRPTPATH = sParamInstRPTPATH
        .ParamGenRPTPATH = sParamGenRPTPATH
        .Idioma = Idioma
        .GuardarParametrosIns = bGuardarParametrosIns
        .CargaMaximaCombos = iCargaMaximaCombos
        .gsPlurPres1 = gsPlurPres1
        .gsPlurPres2 = gsPlurPres2
        .gsPlurPres3 = gsPlurPres3
        .gsPlurPres4 = gsPlurPres4
        .gbOblProveEqp = gbOblProveEqp
        .Show vbModal
        
        bGuardarParametrosIns = .GuardarParametrosIns
        sTitulo = .Titulo
        Set MostrarFormLstPERFIL = .Report
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Function

Public Function MostrarFormLstPAI(ByVal oGestorIdiomas As CGestorIdiomas, ByVal Idioma As String, ByRef oMensajes As CMensajes, ByRef sParamInstRPTPATH As String, ByVal sParamGenRPTPATH, _
        ByRef bGuardarParametrosIns As Boolean, ByRef sTitulo As String) As Object
    Dim oFrm As frmLstPAI
        
    Set oFrm = New frmLstPAI
    With oFrm
        Set .Mensajes = oMensajes
        Set .GestorIdiomas = oGestorIdiomas
        .ParamInstRPTPATH = sParamInstRPTPATH
        .ParamGenRPTPATH = sParamGenRPTPATH
        .Idioma = Idioma
        .GuardarParametrosIns = bGuardarParametrosIns
        
        .Show vbModal
        
        bGuardarParametrosIns = .GuardarParametrosIns
        sTitulo = .Titulo
        Set MostrarFormLstPAI = .Report
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Function


Public Function MostrarFormLstPROVI(ByVal oGestorIdiomas As CGestorIdiomas, ByVal Idioma As String, ByRef oMensajes As CMensajes, ByRef sParamInstRPTPATH As String, ByVal sParamGenRPTPATH, _
        ByRef bGuardarParametrosIns As Boolean, ByRef sTitulo As String, ByRef oRaiz As CRaiz, ByVal iCargaMaximaCombos As Integer, ByRef gsPais As String, ByRef gsPAIDEF As String) As Object
    Dim oFrm As frmLstPROVI
        
    Set oFrm = New frmLstPROVI
    With oFrm
        Set .Mensajes = oMensajes
        Set .GestorIdiomas = oGestorIdiomas
        Set .Raiz = oRaiz
        .ParamInstRPTPATH = sParamInstRPTPATH
        .ParamGenRPTPATH = sParamGenRPTPATH
        .Idioma = Idioma
        .GuardarParametrosIns = bGuardarParametrosIns
        .CargaMaximaCombos = iCargaMaximaCombos
        .gsPais = gsPais
        .gsPAIDEF = gsPAIDEF
        .Show vbModal
        
        bGuardarParametrosIns = .GuardarParametrosIns
        sTitulo = .Titulo
        Set MostrarFormLstPROVI = .Report
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Function


Public Function MostrarFormLstESTRCOMP(ByVal oGestorIdiomas As CGestorIdiomas, ByVal Idioma As String, ByRef oMensajes As CMensajes, ByRef sParamInstRPTPATH As String, ByVal sParamGenRPTPATH, _
        ByRef bGuardarParametrosIns As Boolean, ByRef sTitulo As String, ByRef oRaiz As CRaiz, ByVal iCargaMaximaCombos As Integer, _
        ByVal bOrdenarPorDen As Boolean, ByRef oEquipoSeleccionado As CEquipo, ByVal sOrigen As String, ByVal gCodEqpUsuario As Variant, ByRef oUsuarioSummit As CUsuario) As Object
    Dim oFrm As frmLstESTRCOMP
        
    Set oFrm = New frmLstESTRCOMP
    With oFrm
        Set .oUsuarioSummit = oUsuarioSummit
        Set .Mensajes = oMensajes
        Set .GestorIdiomas = oGestorIdiomas
        Set .Raiz = oRaiz
        .ParamInstRPTPATH = sParamInstRPTPATH
        .ParamGenRPTPATH = sParamGenRPTPATH
        .Idioma = Idioma
        .GuardarParametrosIns = bGuardarParametrosIns
        .CargaMaximaCombos = iCargaMaximaCombos
        .g_sOrigen = sOrigen
        .gCodEqpUsuario = gCodEqpUsuario
        If Not oEquipoSeleccionado Is Nothing Then
            Set .oEqpSeleccionado = oEquipoSeleccionado
            .VerificarSeleccion
        End If
        .g_bOrdenarPorDen = bOrdenarPorDen
        
        .Show vbModal
        
        bGuardarParametrosIns = .GuardarParametrosIns
        sTitulo = .Titulo
        Set MostrarFormLstESTRCOMP = .Report
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Function



Public Function MostrarFormLstWORKFLOW(ByVal oGestorIdiomas As CGestorIdiomas, ByVal Idioma As String, ByRef oMensajes As CMensajes, ByRef sParamInstRPTPATH As String, ByVal sParamGenRPTPATH, _
        ByRef bGuardarParametrosIns As Boolean, ByRef sTitulo As String, ByRef oGestorInformes As CGestorInformes, ByRef oRaiz As CRaiz) As Object
    Dim oFrm As frmLstWORKFLOW
        
    Set oFrm = New frmLstWORKFLOW
    With oFrm
        Set .Mensajes = oMensajes
        Set .GestorIdiomas = oGestorIdiomas
        Set .GestorInformes = oGestorInformes
        Set .Raiz = oRaiz
        .ParamInstRPTPATH = sParamInstRPTPATH
        .ParamGenRPTPATH = sParamGenRPTPATH
        .Idioma = Idioma
        .GuardarParametrosIns = bGuardarParametrosIns
        
        .Show vbModal
        
        bGuardarParametrosIns = .GuardarParametrosIns
        sTitulo = .Titulo
        Set MostrarFormLstWORKFLOW = .Report
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Function



Public Function MostrarFormLstParametros(ByVal sOrigen As String, ByRef oFSGSRaiz As CRaiz, ByVal oGestorIdiomas As CGestorIdiomas, ByVal Idioma As String, ByRef oGestorParametros As CGestorParametros, _
        ByRef oMensajes As CMensajes, ByRef sParamInstRPTPATH As String, ByVal sParamGenRPTPATH, ByRef bGuardarParametrosIns As Boolean, ByVal bSENT_ORD_CAL1 As Boolean, bSENT_ORD_CAL2 As Boolean, _
        bSENT_ORD_CAL3 As Boolean, ByRef sTitulo As String, Optional ByVal bOrdDen As Boolean = False) As Object
    Dim oFrm As frmLstParametros
        
    Set oFrm = New frmLstParametros
    With oFrm
        Set .FSGSRaiz = oFSGSRaiz
        Set .Mensajes = oMensajes
        Set .GestorIdiomas = oGestorIdiomas
        Set .GestorParametros = oGestorParametros
        .sOrigen = sOrigen
        .ParamInstRPTPATH = sParamInstRPTPATH
        .ParamGenRPTPATH = sParamGenRPTPATH
        .Idioma = Idioma
        .SENT_ORD_CAL1 = bSENT_ORD_CAL1
        .SENT_ORD_CAL2 = bSENT_ORD_CAL2
        .SENT_ORD_CAL3 = bSENT_ORD_CAL3
        If bOrdDen Then oFrm.opOrdDen.Value = True
    
        .GuardarParametrosIns = bGuardarParametrosIns
        
        .Show vbModal
        
        bGuardarParametrosIns = .GuardarParametrosIns
        sTitulo = .Titulo
        Set MostrarFormLstParametros = .Report
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Function
