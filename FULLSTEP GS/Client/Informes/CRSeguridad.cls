VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRSeguridad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public LOGPREBLOQ As Integer     'gParametrosGenerales.giLOGPREBLOQ

Public Function ListadoPERFIL(oReport As CRAXDRT.Report, ByVal Perf As String, ByVal gbOblProveEqp As Boolean, Optional ByVal OrdenPorDen As Boolean, Optional ByVal VerAcc As Boolean)
    Dim Table As CRAXDRT.DatabaseTable
    Dim SubListado As CRAXDRT.Report
    Dim sOrdenar As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Function
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    If VerAcc Then
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "VACC")).Text = "'S'"
    Else
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "VACC")).Text = "'N'"
    End If

    If Perf <> "" Then
        oReport.RecordSelectionFormula = "{PERF.COD} ='" & DblQuote(Perf) & "'"
    End If
    If OrdenPorDen Then
        sOrdenar = "+{PERF.DEN}"
    Else
        sOrdenar = "+{PERF.COD}"
    End If
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(sOrdenar))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(sOrdenar), crs_SortField_Interno(sOrdenar))), crs_SortDirection_Interno(sOrdenar)
    
    ' SUBREPORT DE MENU-ACCIONES
    If VerAcc Then
        Set SubListado = oReport.OpenSubreport("MENU_PERF")
        
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        If Not gbOblProveEqp Then
            SubListado.RecordSelectionFormula = SubListado.RecordSelectionFormula & " AND {MENU.COD} <> 10705 AND {MENU.COD} <> 10706"
        End If
       Set SubListado = Nothing
    End If
End Function

''' <summary>Listado de usuarios</summary>
''' <param name="oReport">Informe</param>
''' <param name="iTipo">Tipo usuario (comprador...)</param>
''' <param name="Usuario">C�digo del usuario</param>
''' <param name="OrdenPorDen">TRUE si hay que ordenar los datos por denominaci�n. FALSE: Ordena por c�digo</param>
''' <param name="VerDatosPer">TRUE para obtener los datos de la persona relacionada</param>
''' <param name="VerAcc">TRUE para obtener las acciones de seguridad</param>
''' <param name="OblProveEqp"></param>
''' <param name="idioma">Idioma</param>
''' <param name="CuentasUsuario">Cuentas del usuario: Todas, solo activas, solo bloqueadas</param>
''' <param name="ConFSEP">Mostrar �nicamente los usuarios con acceso a EP</param>
''' <param name="ConFSGS">Mostrar �nicamente los usuarios con acceso a GS</param>
''' <param name="ConFSWS">Mostrar �nicamente los usuarios con acceso a PM</param>
''' <param name="ConFSQA">Mostrar �nicamente los usuarios con acceso a QA</param>
''' <param name="ConFSINT">Mostrar �nicamente los usuarios con acceso al visor de integraci�n</param>
''' <param name="ConContratos">Mostrar �nicamente los usuarios con acceso a contratos</param>
''' <param name="ConFSIM">Mostrar �nicamente los usuarios con acceso a FSIM</param>
''' <returns>Booleano indicando si el listado se ha abierto correctamente</returns>
''' <remarks>Llamada desde: frmLstUSUARIOS.cmdObtener_Click  Tiempo m�ximo: 1</remarks>
''' <remarks>Revisado por: NGO  Fecha: 27/12/2011</remarks>
Public Sub ListadoUSUARIOS(oReport As CRAXDRT.Report, ByVal iTipo As Integer, ByVal Usuario As String, Optional ByVal OrdenPorDen As Boolean, _
                    Optional ByVal VerDatosPer As Boolean, Optional ByVal VerAcc As Boolean, Optional ByVal OblProveEqp As Boolean, _
                    Optional ByVal idioma As String, Optional ByVal CuentasUsuario As TipoEstadoCuentasUsuario, Optional ByVal ConFSEP As Boolean, _
                    Optional ByVal ConFSGS As Boolean, Optional ByVal ConFSWS As Boolean, Optional ByVal ConFSQA As Boolean, Optional ByVal ConFSINT As Boolean, _
                    Optional ByVal ConContratos As Boolean, Optional ByVal ConFSIM As Boolean)
                    
    Dim Table As CRAXDRT.DatabaseTable
    Dim SubListado As CRAXDRT.Report
    Dim Acciones As String
    Dim Detalle As String
    Dim RecordSelectionFormula As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
        
    RecordSelectionFormula = "1=1"
    If Usuario <> "" Then
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.COD}='" & DblQuote(Usuario) & "'"
    ElseIf ConFSEP = True Then
        'S�lo con acceso al EP
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.FSEP}=TRUE"
    ElseIf ConFSGS = True Then
        'S�lo con acceso al GS
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.FSGS}=TRUE"
    ElseIf ConFSWS = True Then
        'S�lo con acceso al WS
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.SOL_COMPRA}=TRUE"
    ElseIf ConFSQA = True Then
        'S�lo con acceso al QA
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.FSQA}=TRUE"
    ElseIf ConFSINT = True Then
        'S�lo con acceso al visor de integraci�n
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.FSINT}=TRUE"
    ElseIf ConContratos = True Then
        'S�lo con acceso al visor de integraci�n
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.CONTRATOS}=TRUE"
    ElseIf ConFSIM = True Then
        'S�lo con acceso al visor de integraci�n
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.FSIM}=TRUE"
    ElseIf iTipo <> 0 Then
        RecordSelectionFormula = RecordSelectionFormula & " AND {USU.TIPO}=" & iTipo
    End If
    
    RecordSelectionFormula = RecordSelectionFormula & " AND {USU.BAJA}= 0"
    If LOGPREBLOQ <> 0 Then
        Select Case CuentasUsuario
            Case 0: RecordSelectionFormula = RecordSelectionFormula & " AND {USU.BLOQ}< " & LOGPREBLOQ
            Case 1: RecordSelectionFormula = RecordSelectionFormula & " AND {USU.BLOQ}>= " & LOGPREBLOQ
        End Select
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next
    
    If VerAcc Then
        Acciones = "S"
    Else
        Acciones = "N"
    End If
    If VerDatosPer Then
        Detalle = "S"
    Else
        Detalle = "N"
    End If
    
    oReport.RecordSelectionFormula = RecordSelectionFormula
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "VSEGUR")).Text = """" & Acciones & """"
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "VDETALLE")).Text = """" & Detalle & """"
    oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "IDIOMA")).Text = """" & idioma & """"
            
    If OrdenPorDen Then
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_USU")).Text = oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_NOM")).Text
    Else
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_USU")).Text = oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_COD")).Text
    End If
    
       
    ' SUBREPORTs DE MENU-ACCIONES
    If VerAcc Then
        Set SubListado = oReport.OpenSubreport("MENU_PERF")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        If OblProveEqp = 1 Then
            SubListado.RecordSelectionFormula = " AND {MENU.COD} <> 10705 AND {MENU.COD} <> 10706"
        End If
        Set SubListado = Nothing
        
        Set SubListado = oReport.OpenSubreport("MENU_PERF - 01")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
        Next
        If OblProveEqp = 1 Then
            SubListado.RecordSelectionFormula = " AND {MENU.COD} <> 10705 AND {MENU.COD} <> 10706"
        End If
        Set SubListado = Nothing
    End If
End Sub


Public Function ListadoLISPERPerf(oReport As CRAXDRT.Report, ByVal Perf As String, Optional ByVal OrdenPorDen As Boolean, Optional ByVal bListPerf As Boolean)
    Dim Table As CRAXDRT.DatabaseTable
    Dim sOrdenar As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Function
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    If Perf <> "" Then
        oReport.RecordSelectionFormula = "{PERF.COD} ='" & DblQuote(Perf) & "'"
    End If
    
    'Orden del informe (dependiendo de si es Listados por perfiles o Perfiles por listado)
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    
    If bListPerf Then  'Listados por perfiles
        If OrdenPorDen Then
           oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_PERF")).Text = oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_DEN")).Text
        Else
           oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_PERF")).Text = oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_COD")).Text
        End If
        sOrdenar = "+{LISPER.DEN}"
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(sOrdenar))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(sOrdenar), crs_SortField_Interno(sOrdenar))), crs_SortDirection_Interno(sOrdenar)
        
    Else  'Perfiles por listados
        sOrdenar = "+{LISPER.DEN}"
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(sOrdenar))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(sOrdenar), crs_SortField_Interno(sOrdenar))), crs_SortDirection_Interno(sOrdenar)
        If OrdenPorDen Then
            sOrdenar = "+{PERF.DEN}"
        Else
            sOrdenar = "+{PERF.COD}"
        End If
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(sOrdenar))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(sOrdenar), crs_SortField_Interno(sOrdenar))), crs_SortDirection_Interno(sOrdenar)
    End If
    
End Function

Public Function ListadoLISPERUsu(oReport As CRAXDRT.Report, ByVal Usuario As String, ByVal iTipo As Integer, Optional ByVal OrdenPorDen As Boolean, Optional ByVal bListUsu As Boolean)
    Dim Table As CRAXDRT.DatabaseTable
    Dim sOrdenar As String
    Dim RecordSelectionFormula As String
    
    If bcrs_Connected = False Then
        Set oReport = Nothing
        Exit Function
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo scrs_Server, scrs_Database, scrs_User, scrs_Password
    Next

    If Usuario <> "" Then
        RecordSelectionFormula = " {USU.COD}='" & DblQuote(Usuario) & "'"
    ElseIf iTipo = 2 Then
        RecordSelectionFormula = " {USU.TIPO}=2"
    End If
    oReport.RecordSelectionFormula = RecordSelectionFormula
    
    'Orden del informe dependiendo de si es Listados por usuarios o Usuarios por listados
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    
    If bListUsu Then  'Listados por usuarios
        If OrdenPorDen Then
           oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_USU")).Text = oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_COD")).Text
        Else
           oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_USU")).Text = oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, "GR_COD")).Text
        End If
        sOrdenar = "+{LISPER.DEN}"
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(sOrdenar))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(sOrdenar), crs_SortField_Interno(sOrdenar))), crs_SortDirection_Interno(sOrdenar)
        
    Else 'Usuarios por listados
        sOrdenar = "+{LISPER.DEN}"
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(sOrdenar))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(sOrdenar), crs_SortField_Interno(sOrdenar))), crs_SortDirection_Interno(sOrdenar)
        If OrdenPorDen Then
            sOrdenar = "+{@txtNOMBRE}"
            oReport.RecordSortFields.Add oReport.FormulaFields(9), crs_SortDirection_Interno(sOrdenar)
        Else
            sOrdenar = "+{USU.COD}"
            oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex_Interno(oReport, crs_SortTable_Interno(sOrdenar))).Fields(crs_FieldIndex_Interno(oReport, crs_SortTable_Interno(sOrdenar), crs_SortField_Interno(sOrdenar))), crs_SortDirection_Interno(sOrdenar)
        End If
        
    End If

End Function
