VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmailReuConvo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Mensajes As CMensajes
Public GestorReuniones As CGestorReuniones

Public Function GenerarMensajeConvocatoriaPersona(ByVal sFileName As String, ByVal bHTML As Boolean, ByRef sCuerpo As String, ByVal vFechaReu As Variant, ByVal vReferencia As Variant, ByVal vNombre As Variant, _
        ByVal vApellidos As Variant, ByVal vCodConvValue As Variant, ByVal vCodConvText As String, ByVal vTfno As Variant, ByVal vFax As Variant, ByVal vEmail As Variant, ByVal vCargo As Variant, ByVal vDpto As Variant, _
        ByVal vUON1Conv As Variant, ByVal vUON2Conv As Variant, ByVal vUON3Conv As Variant, ByRef oPers As CPersonas, ByRef oProcesos As CProcesos, ByVal sIdioma As Variant) As Integer
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim ADORs As Ador.Recordset
    Dim sBloque As String
    Dim sBloque_aux As String
    Dim sLineas As String
    Dim bEs As Boolean
    Dim oPer As CPersona
    
    On Error GoTo Error:

    'SI ES UN ASISTENTE FIJO SE RELLENA LOS DATOS Y SE SALE DE LA FUNCION

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(sFileName) Then
        Screen.MousePointer = vbNormal
        Mensajes.PlantillaNoEncontrada sFileName
        GenerarMensajeConvocatoriaPersona = -2
        Set oFos = Nothing
        sCuerpo = ""
        Exit Function
    End If

    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)

    sCuerpoMensaje = ostream.ReadAll

    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHAREU", Format(vFechaReu, "Short Date"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@HORAREU", Format(vFechaReu, "short time"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@REFERENCIA", vReferencia)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMBRE", vNombre)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APELLIDOS", vApellidos)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_CONVOCADO", vCodConvValue)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO", vTfno)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX", vFax)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAIL", vEmail)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGO", vCargo)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DPTO", vDpto)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO1_CONVOCADO", vUON1Conv)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO2_CONVOCADO", vUON2Conv)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO3_CONVOCADO", vUON3Conv)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@EQP_CONVOCADO", "")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TAB", vbTab)
            
    For Each oPer In oPers
        If oPer.Cod = vCodConvText Then
            ' ES UN ASISTENTE FIJO A REUNIONES
            Set ADORs = GestorReuniones.DevolverProcesosReunionParaPersona(vCodConvText, CDate(vFechaReu))

            Dim oProce As CProceso
            If (InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->") > 0) And (InStr(sCuerpoMensaje, "<!--FIN LINEAS-->") > 0) Then
                sBloque = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->"), (InStr(sCuerpoMensaje, "<!--FIN LINEAS-->") + Len("<!--FIN LINEAS-->")) - InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->"))
                sLineas = ""

                For Each oProce In oProcesos
                    If oProce.Estado < conadjudicaciones Then
                        sBloque_aux = sBloque

                        sBloque_aux = Replace(sBloque_aux, "<!--INICIO LINEAS-->", "")
                        sBloque_aux = Replace(sBloque_aux, "<!--FIN LINEAS-->", "")

                        sBloque_aux = Replace(sBloque_aux, "@TAB", vbTab)
                        sBloque_aux = Replace(sBloque_aux, "@ANYO", oProce.Anyo)
                        sBloque_aux = Replace(sBloque_aux, "@GMN", oProce.GMN1Cod)
                        sBloque_aux = Replace(sBloque_aux, "@PROCE", oProce.Cod)
                        If (50 - Len(oProce.Den)) > 0 And (Not bHTML) Then
                            sBloque_aux = Replace(sBloque_aux, "@DENPROCE", oProce.Den & String(50 - Len(oProce.Den), "#") & vbTab)
                        Else
                            sBloque_aux = Replace(sBloque_aux, "@DENPROCE", oProce.Den)
                        End If
                        If oProce.Reudec Then
                            sBloque_aux = Replace(sBloque_aux, "@TIPO", sIdioma(11))
                        Else
                            sBloque_aux = Replace(sBloque_aux, "@TIPO", sIdioma(12))
                        End If
                        If InStr(1, sBloque_aux, "@RESP", vbTextCompare) <> 0 Then
                            ADORs.MoveFirst
                            bEs = False
                            Do While Not ADORs.EOF
                                If ADORs("ANYO").Value = oProce.Anyo And ADORs("GMN1").Value = oProce.GMN1Cod And ADORs("COD").Value = oProce.Cod Then
                                    bEs = True
                                    Exit Do
                                End If
                                ADORs.MoveNext
                            Loop
                            If bEs Then
                                  If NullToStr(ADORs("COM").Value) = vCodConvValue Then
                                      sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(11))
                                  Else
                                      sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(12))
                                  End If
                              Else
                                  sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(12))
                              End If
                        End If
                        sBloque_aux = Replace(sBloque_aux, "@HORAPROCE", Format(oProce.HoraEnReunion, "Short Time"))

                        If sLineas = "" Then
                            sLineas = sBloque_aux
                        Else
                            sLineas = sLineas & vbCrLf & sBloque_aux
                        End If
                    End If
                Next
                sCuerpoMensaje = Replace(sCuerpoMensaje, sBloque, sLineas)
                sCuerpoMensaje = Replace(sCuerpoMensaje, "#", " ")
            End If

            GenerarMensajeConvocatoriaPersona = 1

            ADORs.Close

            Set oFos = Nothing
            Set oFile = Nothing
            Set ostream = Nothing
            Set ADORs = Nothing

            sCuerpo = sCuerpoMensaje
            Exit Function
        End If
    Next
    Set oPer = Nothing

    ' SI NO ES UN ASISTENTE FIJO A REUNIONES
    Set ADORs = GestorReuniones.DevolverProcesosReunionParaPersona(vCodConvText, CDate(vFechaReu))
    If ADORs Is Nothing Or ADORs.RecordCount = 0 Then
        ' Si TODOS SUS procesos est�n cerrados no se saca hoja/mail de convocatoria
        GenerarMensajeConvocatoriaPersona = 0

        ADORs.Close

        Set oFos = Nothing
        Set oFile = Nothing
        Set ostream = Nothing
        Set ADORs = Nothing

        sCuerpo = sCuerpoMensaje
        Exit Function
    End If

    If (InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->") > 0) And (InStr(sCuerpoMensaje, "<!--FIN LINEAS-->") > 0) Then
        sBloque = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->"), (InStr(sCuerpoMensaje, "<!--FIN LINEAS-->") + Len("<!--FIN LINEAS-->")) - InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->"))
        sLineas = ""

        'Rellenamos la tabla para cada proceso
        While Not ADORs.EOF
            sBloque_aux = sBloque

            sBloque_aux = Replace(sBloque_aux, "<!--INICIO LINEAS-->", "")
            sBloque_aux = Replace(sBloque_aux, "<!--FIN LINEAS-->", "")

            sBloque_aux = Replace(sBloque_aux, "@TAB", vbTab)
            sBloque_aux = Replace(sBloque_aux, "@ANYO", ADORs("ANYO").Value)
            sBloque_aux = Replace(sBloque_aux, "@GMN", ADORs("GMN1").Value)
            sBloque_aux = Replace(sBloque_aux, "@PROCE", ADORs("COD").Value)
            If (50 - Len(ADORs("DEN").Value)) > 0 And (Not bHTML) Then
                sBloque_aux = Replace(sBloque_aux, "@DENPROCE", ADORs("DEN").Value & String(50 - Len(ADORs("DEN").Value), "#") & vbTab)
            Else
                sBloque_aux = Replace(sBloque_aux, "@DENPROCE", ADORs("DEN").Value)
            End If
            If Int(ADORs("DEC").Value) = 1 Or Int(ADORs("DEC").Value) = -1 Then
                sBloque_aux = Replace(sBloque_aux, "@TIPO", sIdioma(11))
            Else
                sBloque_aux = Replace(sBloque_aux, "@TIPO", sIdioma(12))
            End If
            If NullToStr(ADORs("COM").Value) = vCodConvValue Then
                sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(11))
            Else
                sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(12))
            End If
            sBloque_aux = Replace(sBloque_aux, "@HORAPROCE", Format(ADORs("HORA").Value, "Short Time"))

            ADORs.MoveNext

            If sLineas = "" Then
                sLineas = sBloque_aux
            Else
                sLineas = sLineas & vbCrLf & sBloque_aux
            End If
        Wend
        
        sCuerpoMensaje = Replace(sCuerpoMensaje, sBloque, sLineas)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "#", " ")

        GenerarMensajeConvocatoriaPersona = 1

        ADORs.Close

        Set oFos = Nothing
        Set oFile = Nothing
        Set ostream = Nothing
        Set ADORs = Nothing

        sCuerpo = sCuerpoMensaje
        Exit Function
    End If

Error:
    Resume Next
End Function

Public Function GenerarMensajeConvocatoriaComprador(ByVal sFileName As String, ByVal bHTML As Boolean, ByRef sCuerpo As String, ByVal vFechaReu As Variant, ByVal vReferencia As Variant, ByVal vNombre As Variant, _
        ByVal vApellidos As Variant, ByVal vCodConvValue As Variant, ByVal vCodConvText As String, ByVal vTfno As Variant, ByVal vFax As Variant, ByVal vEmail As Variant, ByVal vEqp As Variant, _
        ByVal sEqp As String, ByRef oPers As CPersonas, ByRef oProcesos As CProcesos, ByVal sIdioma As Variant) As Integer
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim ADORs As Ador.Recordset
    Dim sBloque As String
    Dim sBloque_aux As String
    Dim sLineas As String
    Dim bEs As Boolean
                  
    On Error GoTo Error:
    
   'SI ES UN ASISTENTE FIJO SE RELLENA LOS DATOS Y SE SALE DE LA FUNCION

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(sFileName) Then
        Mensajes.PlantillaNoEncontrada sFileName
        GenerarMensajeConvocatoriaComprador = -2
        Set oFos = Nothing
        sCuerpo = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)

    sCuerpoMensaje = ostream.ReadAll
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FECHAREU", Format(vFechaReu, "Short Date"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@HORAREU", Format(vFechaReu, "short time"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@REFERENCIA", vReferencia)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMBRE", vNombre)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APELLIDOS", vApellidos)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@COD_CONVOCADO", vCodConvValue)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO", vTfno)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAX", vFax)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAIL", vEmail)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TAB", vbTab)
            
    Dim oPer As CPersona
    For Each oPer In oPers
        If oPer.Cod = vCodConvText Then
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGO", oPer.Cargo)
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@DPTO", oPer.DenDep)
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO1_CONVOCADO", oPer.UON1)
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO2_CONVOCADO", oPer.UON2)
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO3_CONVOCADO", oPer.UON3)
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@EQP_CONVOCADO", "")
            
            ' ES UN ASISTENTE FIJO A REUNIONES
            Set ADORs = GestorReuniones.DevolverProcesosReunionParaPersona(vCodConvText, CDate(vFechaReu))
            
            Dim oProce As CProceso
            If (InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->") > 0) And (InStr(sCuerpoMensaje, "<!--FIN LINEAS-->") > 0) Then
                sBloque = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->"), (InStr(sCuerpoMensaje, "<!--FIN LINEAS-->") + Len("<!--FIN LINEAS-->")) - InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->"))
                sLineas = ""
                
                For Each oProce In oProcesos
                    If oProce.Estado < conadjudicaciones Then
                        sBloque_aux = sBloque
                        sBloque_aux = Replace(sBloque_aux, "<!--INICIO LINEAS-->", "")
                        sBloque_aux = Replace(sBloque_aux, "<!--FIN LINEAS-->", "")
                        sBloque_aux = Replace(sBloque_aux, "@TAB", vbTab)
                        sBloque_aux = Replace(sBloque_aux, "@ANYO", oProce.Anyo)
                        sBloque_aux = Replace(sBloque_aux, "@GMN", oProce.GMN1Cod)
                        sBloque_aux = Replace(sBloque_aux, "@PROCE", oProce.Cod)
                        If (50 - Len(oProce.Den)) > 0 And (Not bHTML) Then
                            sBloque_aux = Replace(sBloque_aux, "@DENPROCE", oProce.Den & String(50 - Len(oProce.Den), "#") & vbTab)
                        Else
                            sBloque_aux = Replace(sBloque_aux, "@DENPROCE", oProce.Den)
                        End If
                        If oProce.Reudec Then
                            sBloque_aux = Replace(sBloque_aux, "@TIPO", sIdioma(11))
                        Else
                            sBloque_aux = Replace(sBloque_aux, "@TIPO", sIdioma(12))
                        End If
                        If InStr(1, sBloque_aux, "@RESP", vbTextCompare) > 0 Then
                            ADORs.MoveFirst
                            bEs = False
                            Do While Not ADORs.EOF
                                If ADORs("ANYO").Value = oProce.Anyo And ADORs("GMN1").Value = oProce.GMN1Cod And ADORs("COD").Value = oProce.Cod Then
                                    bEs = True
                                    Exit Do
                                End If
                                ADORs.MoveNext
                            Loop
                            If bEs Then
                                  If NullToStr(ADORs("COM").Value) = vCodConvValue Then
                                      sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(11))
                                  Else
                                      sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(12))
                                  End If
                              Else
                                  sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(12))
                              End If
                        End If
                        sBloque_aux = Replace(sBloque_aux, "@HORAPROCE", Format(oProce.HoraEnReunion, "Short Time"))
                                                    
                        If sLineas = "" Then
                            sLineas = sBloque_aux
                        Else
                            sLineas = sLineas & vbCrLf & sBloque_aux
                        End If
                    End If
                Next
                sCuerpoMensaje = Replace(sCuerpoMensaje, sBloque, sLineas)
                sCuerpoMensaje = Replace(sCuerpoMensaje, "#", " ")
            End If
            
            GenerarMensajeConvocatoriaComprador = 1
            
            ADORs.Close
            
            Set oFos = Nothing
            Set oFile = Nothing
            Set ostream = Nothing
            Set ADORs = Nothing
                            
            sCuerpo = sCuerpoMensaje
            Exit Function
        End If
    Next
    Set oPer = Nothing
    
    ' SI NO ES UN ASISTENTE FIJO A REUNIONES
    Set ADORs = GestorReuniones.DevolverProcesosReunionParaComprador(sEqp, vCodConvText, CDate(vFechaReu))
    If ADORs Is Nothing Or ADORs.RecordCount = 0 Then
        ' Si TODOS SUS procesos est�n cerrados no se saca hoja/mail de convocatoria
        
        GenerarMensajeConvocatoriaComprador = 0
        
        ADORs.Close
        
        Set oFos = Nothing
        Set oFile = Nothing
        Set ostream = Nothing
        Set ADORs = Nothing
                        
        sCuerpo = ""
        Exit Function
    End If
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGO", "")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DPTO", "")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO1_CONVOCADO", "")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO2_CONVOCADO", "")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@UO3_CONVOCADO", "")
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@EQP_CONVOCADO", vEqp)
    
    If (InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->") > 0) And (InStr(sCuerpoMensaje, "<!--FIN LINEAS-->") > 0) Then
        sBloque = Mid(sCuerpoMensaje, InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->"), (InStr(sCuerpoMensaje, "<!--FIN LINEAS-->") + Len("<!--FIN LINEAS-->")) - InStr(sCuerpoMensaje, "<!--INICIO LINEAS-->"))
        sLineas = ""
        
        'Rellenamos la tabla para cada proceso
        While Not ADORs.EOF
            sBloque_aux = sBloque
            
            sBloque_aux = Replace(sBloque_aux, "<!--INICIO LINEAS-->", "")
            sBloque_aux = Replace(sBloque_aux, "<!--FIN LINEAS-->", "")
            
            sBloque_aux = Replace(sBloque_aux, "@TAB", vbTab)
            sBloque_aux = Replace(sBloque_aux, "@ANYO", ADORs("ANYO").Value)
            sBloque_aux = Replace(sBloque_aux, "@GMN", ADORs("GMN1").Value)
            sBloque_aux = Replace(sBloque_aux, "@PROCE", ADORs("COD").Value)
            If (50 - Len(ADORs("DEN").Value)) > 0 And (Not bHTML) Then
                sBloque_aux = Replace(sBloque_aux, "@DENPROCE", ADORs("DEN").Value & String(50 - Len(ADORs("DEN").Value), "#") & vbTab)
            Else
                sBloque_aux = Replace(sBloque_aux, "@DENPROCE", ADORs("DEN").Value)
            End If
            If Int(ADORs("DEC").Value) = 1 Or Int(ADORs("DEC").Value) = -1 Then
                sBloque_aux = Replace(sBloque_aux, "@TIPO", sIdioma(11))
            Else
                sBloque_aux = Replace(sBloque_aux, "@TIPO", sIdioma(12))
            End If
            If NullToStr(ADORs("EQP").Value) = vEqp _
            And NullToStr(ADORs("COM").Value) = vCodConvValue Then
                sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(11))
            Else
                sBloque_aux = Replace(sBloque_aux, "@RESP", sIdioma(12))
            End If
            sBloque_aux = Replace(sBloque_aux, "@HORAPROCE", Format(ADORs("HORA").Value, "Short Time"))
            
            ADORs.MoveNext
            
            If sLineas = "" Then
                sLineas = sBloque_aux
            Else
                sLineas = sLineas & vbCrLf & sBloque_aux
            End If
        Wend

        sCuerpoMensaje = Replace(sCuerpoMensaje, sBloque, sLineas)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "#", " ")
            
        GenerarMensajeConvocatoriaComprador = 1
            
        ADORs.Close
        
        Set oFos = Nothing
        Set oFile = Nothing
        Set ostream = Nothing
        Set ADORs = Nothing
                        
        sCuerpo = sCuerpoMensaje
        Exit Function
    End If
    
Error:
    Resume Next
End Function
