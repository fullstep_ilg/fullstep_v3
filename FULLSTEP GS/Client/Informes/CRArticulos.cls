VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRArticulos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public SincronizacionMat As Boolean 'gParametrosGenerales.gbSincronizacionMat
Public IdiomaPortal As String   'gParametrosInstalacion.gIdiomaPortal

Private oFos As FileSystemObject

Private Function ObtenerSQLArticulos(ByVal bRMat As Boolean, ByVal sCodComp As String, ByVal sCodEqp As String, ByVal sGMN1Cod As String, _
                                ByVal sGMN2Cod As String, ByVal sGMN3Cod As String, ByVal sGMN4Cod As String, ByVal OrdEst As Boolean, _
                                ByVal Ordcod As Boolean, ByVal OrdDen As Boolean, ByVal Todos As Boolean, Optional ByVal bGenerico As Boolean, _
                                Optional ByVal vConcepto As Variant, Optional ByVal vAlmacen As Variant, Optional ByVal vRecep As Variant, _
                                Optional ByVal bCentral As Boolean, Optional ByVal brRestMatUsu As Boolean, Optional ByVal brRestMatperf As Boolean, _
                                Optional ByVal lIdPerfil As Integer, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, _
                                Optional ByVal sUON3 As String, Optional ByRef oUons As CUnidadesOrganizativas, _
                                Optional ByVal bIncluirAscendientesUon As Boolean, Optional ByVal bIncluirDescendientesUon As Boolean) As String

' Este procedimiento construye la SQL QUERY a pasar
' a los reports principales :
' 1 - todos los articulos
' 2 - A partir de una rama de la estructura
Dim sSQLQueryString As String
Dim sUON As String
Dim swhereuons As String

If OrdEst = False Then
' TODOS LOS ARTICULOS
    sSQLQueryString = "SELECT GMN1." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ", GMN2." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ", GMN3." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ", GMN4.GMN1, GMN4.GMN2, GMN4.GMN3, GMN4." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ", ART4.GMN1 , ART4.GMN2, ART4.GMN3, ART4.GMN4, ART4.COD , ART4.DEN , ART4.UNI, PRES_ART.CANT, ART4.GENERICO, ART4.CONCEPTO CONCEP, ART4.ALMACENAR ALMAC, ART4.RECEPCIONAR RECEP, ART4.CENTRAL"
    sSQLQueryString = sSQLQueryString & " FROM GMN1 WITH (NOLOCK) ,GMN2 WITH (NOLOCK),GMN3 ,GMN4 WITH (NOLOCK), ART4 WITH (NOLOCK),PRES_ART WITH (NOLOCK)"
    If bRMat Then
        sSQLQueryString = sSQLQueryString & ", COM_GMN4"
    End If
    
    'filtrar por un conjunto de uons
    If Not IsEmpty(oUons) And Not oUons Is Nothing Then
        sSQLQueryString = sSQLQueryString & " INNER JOIN ART4_UON AU WITH (NOLOCK) ON AU.ART4=ART4.COD "
        swhereuons = construirRestriccionPorUons(oUons, False) & " "
    End If
    
    'Restricciones por uon de usuario/perfil
    If brRestMatUsu Or brRestMatperf Then
        sUON = ConstruirCriterioUONUsuPerfUONArt(brRestMatUsu, brRestMatperf, lIdPerfil, sUON1, sUON2, sUON3)
    End If
    
    If sUON <> "" Then
        sSQLQueryString = sSQLQueryString & " inner JOIN (" & sUON & ") A3 ON ART4.COD=A3.ART4 "
    End If
    
    sSQLQueryString = sSQLQueryString & " WHERE GMN1.COD = GMN2.GMN1 AND GMN2.GMN1 = GMN3.GMN1 AND GMN2.COD = GMN3.GMN2 AND GMN3.GMN1 = GMN4.GMN1 AND GMN3.GMN2 = GMN4.GMN2 AND GMN3.COD = GMN4.GMN3 AND GMN4.GMN1 = ART4.GMN1 AND GMN4.GMN2 = ART4.GMN2 AND GMN4.GMN3 = ART4.GMN3 AND GMN4.Cod = ART4.GMN4 "
    '******   WHERE (A PARTIR DE UNA RAMA)  *******'
    If Todos = False Then
        If Not sGMN4Cod = "" Then
            sSQLQueryString = sSQLQueryString & " AND GMN1.COD = '" & DblQuote(sGMN1Cod) & "'" & " AND GMN2.COD ='" & DblQuote(sGMN2Cod) & "'" & " AND GMN3.COD = '" & DblQuote(sGMN3Cod) & "'" & " AND GMN4.COD = '" & DblQuote(sGMN4Cod) & "'"
        Else
            If Not sGMN3Cod = "" Then
                sSQLQueryString = sSQLQueryString & " AND GMN1.COD = '" & DblQuote(sGMN1Cod) & "'" & " AND GMN2.COD ='" & DblQuote(sGMN2Cod) & "'" & " AND GMN3.COD = '" & DblQuote(sGMN3Cod) & "'"
            Else
                If Not sGMN2Cod = "" Then
                    sSQLQueryString = sSQLQueryString & " AND GMN1.COD = '" & DblQuote(sGMN1Cod) & "'" & " AND GMN2.COD ='" & DblQuote(sGMN2Cod) & "'"
                Else
                    sSQLQueryString = sSQLQueryString & " AND GMN1.COD = '" & DblQuote(sGMN1Cod) & "'"
                End If
            End If
        End If
    End If
    '******   WHERE ( RESTRICCION DE MATERIAL )*******'
    If bRMat Then
        sSQLQueryString = sSQLQueryString & " and ART4.GMN4 = COM_GMN4.GMN4 AND ART4.GMN3 = COM_GMN4.GMN3 AND ART4.GMN2 = COM_GMN4.GMN2 AND ART4.GMN1 = COM_GMN4.GMN1 and COM_GMN4.EQP = '" & DblQuote(sCodEqp) & "' AND COM_GMN4.COM= '" & DblQuote(sCodComp) & "'"
    End If
    
    sSQLQueryString = sSQLQueryString & " AND ART4.COD*=PRES_ART.COD AND PRES_ART.ANYO=" & Year(Date)
    
    If bGenerico Then
        If Todos = False Then
            sSQLQueryString = sSQLQueryString & " AND ART4.GENERICO=" & BooleanToSQLBinary(True)
        End If
    End If
    
    If bCentral Then
        If Todos = False Then
            sSQLQueryString = sSQLQueryString & " AND ART4.CENTRAL=" & BooleanToSQLBinary(True)
        End If
    End If
    '******   WHERE ( CONCEPTO, ALMACENABLE, RECEPCIONABLE )*******'
    If IsArray(vConcepto) Then
        If vConcepto(0) Or vConcepto(1) Or vConcepto(2) Then
            If vConcepto(0) Then
                sSQLQueryString = sSQLQueryString & " AND (ART4.CONCEPTO=0"
                If vConcepto(1) Then
                    sSQLQueryString = sSQLQueryString & " OR ART4.CONCEPTO=1"
                    If vConcepto(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.CONCEPTO=2"
                    End If
                Else
                    If vConcepto(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.CONCEPTO=2"
                    End If
                End If
                sSQLQueryString = sSQLQueryString & ")"
            Else
                If vConcepto(1) Then
                    sSQLQueryString = sSQLQueryString & " AND (ART4.CONCEPTO=1"
                    If vConcepto(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.CONCEPTO=2"
                    End If
                    sSQLQueryString = sSQLQueryString & ")"
                Else
                    If vConcepto(2) Then
                        sSQLQueryString = sSQLQueryString & " AND ART4.CONCEPTO=2"
                    End If
                End If
            End If
        End If
    End If
    If IsArray(vAlmacen) Then
        If vAlmacen(0) Or vAlmacen(1) Or vAlmacen(2) Then
            If vAlmacen(0) Then
                sSQLQueryString = sSQLQueryString & " AND (ART4.ALMACENAR=0"
                If vAlmacen(1) Then
                    sSQLQueryString = sSQLQueryString & " OR ART4.ALMACENAR=1"
                    If vAlmacen(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.ALMACENAR=2"
                    End If
                Else
                    If vAlmacen(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.ALMACENAR=2"
                    End If
                End If
                sSQLQueryString = sSQLQueryString & ")"
            Else
                If vAlmacen(1) Then
                    sSQLQueryString = sSQLQueryString & " AND (ART4.ALMACENAR=1"
                    If vAlmacen(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.ALMACENAR=2"
                    End If
                    sSQLQueryString = sSQLQueryString & ")"
                Else
                    If vAlmacen(2) Then
                        sSQLQueryString = sSQLQueryString & " AND ART4.ALMACENAR=2"
                    End If
                End If
            End If
        End If
    End If
    If IsArray(vRecep) Then
        If vRecep(0) Or vRecep(1) Or vRecep(2) Then
            If vRecep(0) Then
                sSQLQueryString = sSQLQueryString & " AND (ART4.RECEPCIONAR=0"
                If vRecep(1) Then
                    sSQLQueryString = sSQLQueryString & " OR ART4.RECEPCIONAR=1"
                    If vRecep(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.RECEPCIONAR=2"
                    End If
                Else
                    If vRecep(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.RECEPCIONAR=2"
                    End If
                End If
                sSQLQueryString = sSQLQueryString & ")"
            Else
                If vRecep(1) Then
                    sSQLQueryString = sSQLQueryString & " AND (ART4.RECEPCIONAR=1"
                    If vRecep(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.RECEPCIONAR=2"
                    End If
                    sSQLQueryString = sSQLQueryString & ")"
                Else
                    If vRecep(2) Then
                        sSQLQueryString = sSQLQueryString & " AND ART4.RECEPCIONAR=2"
                    End If
                End If
            End If
        End If
    End If
    
    'restriccion de uons
    If swhereuons <> "" Then
        sSQLQueryString = sSQLQueryString & swhereuons
    End If
    
    '******   ORDER BY   ******'
    If Ordcod = True Then
        sSQLQueryString = sSQLQueryString & " ORDER BY ART4.COD ASC,ART4.GMN1 ASC, ART4.GMN2 ASC, ART4.GMN3 ASC, ART4.GMN4 ASC"
    Else
        If OrdDen = True Then
            sSQLQueryString = sSQLQueryString & " ORDER BY ART4.DEN ASC,GMN1." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & " ASC, GMN2." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & " ASC, GMN3." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & " ASC, GMN4." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & " ASC"
        End If
    End If
Else
' CLASIFICADO POR ESTRUCTURA DE MATERIALES
    sSQLQueryString = "SELECT GMN1.COD,GMN1." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ",GMN2.COD,GMN2." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ",GMN3.COD,GMN3." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ",GMN4.COD,GMN4." & DevolverDenGMN(SincronizacionMat, IdiomaPortal) & ",ART4.COD,ART4.DEN, ART4.GENERICO, ART4.CONCEPTO CONCEP, ART4.ALMACENAR ALMAC, ART4.RECEPCIONAR RECEP FROM GMN1 WITH (NOLOCK)"
    sSQLQueryString = sSQLQueryString & " INNER JOIN GMN2 WITH (NOLOCK) ON GMN1.COD = GMN2.GMN1"
    sSQLQueryString = sSQLQueryString & " INNER JOIN GMN3 WITH (NOLOCK) ON GMN1.COD = GMN3.GMN1 AND GMN2.COD = GMN3.GMN2"
    sSQLQueryString = sSQLQueryString & " INNER JOIN GMN4 WITH (NOLOCK) ON GMN1.COD = GMN4.GMN1 AND GMN2.COD = GMN4.GMN2 AND GMN3.COD = GMN4.GMN3"
    sSQLQueryString = sSQLQueryString & " INNER JOIN ART4 WITH (NOLOCK) ON GMN1.COD = ART4.GMN1 AND GMN2.COD = ART4.GMN2 AND GMN3.COD = ART4.GMN3 AND GMN4.COD = ART4.GMN4"
   
    If bRMat Then
        sSQLQueryString = sSQLQueryString & " INNER JOIN COM_GMN4 WITH (NOLOCK) ON COM_GMN4.GMN1 = GMN4.GMN1 AND COM_GMN4.GMN2 = GMN4.GMN2 AND COM_GMN4.GMN3 = GMN4.GMN3 AND COM_GMN4.GMN4 = GMN4.COD AND COM_GMN4.EQP = '" & DblQuote(sCodEqp) & "' AND COM_GMN4.COM='" & DblQuote(sCodComp) & "'"
    End If
    sSQLQueryString = sSQLQueryString & " LEFT JOIN PRES_ART WITH (NOLOCK) ON ART4.COD = PRES_ART.COD AND PRES_ART.ANYO=" & Year(Date)
        
    
    'filtrar por un conjunto de uons
    If Not IsEmpty(oUons) And Not oUons Is Nothing Then
        sSQLQueryString = sSQLQueryString & " INNER JOIN ART4_UON AU WITH (NOLOCK) ON AU.ART4=ART4.COD "
        swhereuons = construirRestriccionPorUons(oUons, bIncluirAscendientesUon, bIncluirDescendientesUon) & " "
    End If
    
    'Restricciones por uon de usuario/perfil
    If brRestMatUsu Or brRestMatperf Then
        sUON = ConstruirCriterioUONUsuPerfUONArt(brRestMatUsu, brRestMatperf, lIdPerfil, sUON1, sUON2, sUON3)
    End If
    
    If sUON <> "" Then
        sSQLQueryString = sSQLQueryString & " inner JOIN (" & sUON & ") A3 ON ART4.COD=A3.ART4 "
    End If
    
    
    If Todos = False Then
        If Not sGMN4Cod = "" Then
            sSQLQueryString = sSQLQueryString & " WHERE GMN1.COD = '" & DblQuote(sGMN1Cod) & "'" & " AND GMN2.COD ='" & DblQuote(sGMN2Cod) & "'" & " AND GMN3.COD = '" & DblQuote(sGMN3Cod) & "'" & " AND GMN4.COD = '" & DblQuote(sGMN4Cod) & "'"
        Else
            If Not sGMN3Cod = "" Then
                sSQLQueryString = sSQLQueryString & " WHERE GMN1.COD = '" & DblQuote(sGMN1Cod) & "'" & " AND GMN2.COD ='" & DblQuote(sGMN2Cod) & "'" & " AND GMN3.COD = '" & DblQuote(sGMN3Cod) & "'"
            Else
                If Not sGMN2Cod = "" Then
                    sSQLQueryString = sSQLQueryString & " WHERE GMN1.COD = '" & DblQuote(sGMN1Cod) & "'" & " AND GMN2.COD ='" & DblQuote(sGMN2Cod) & "'"
                Else
                    sSQLQueryString = sSQLQueryString & " WHERE GMN1.COD = '" & DblQuote(sGMN1Cod) & "'"
                End If
            End If
        End If
    End If
    If bGenerico Then
        If Todos = False Then
            sSQLQueryString = sSQLQueryString & " AND ART4.GENERICO=" & BooleanToSQLBinary(True)
        End If
    End If
    If bCentral Then
        If Todos = False Then
            sSQLQueryString = sSQLQueryString & " AND ART4.CENTRAL=" & BooleanToSQLBinary(True)
        End If
    End If
    '******   WHERE ( CONCEPTO, ALMACENABLE, RECEPCIONABLE )*******'
    If IsArray(vConcepto) Then
        If vConcepto(0) Or vConcepto(1) Or vConcepto(2) Then
            If vConcepto(0) Then
                sSQLQueryString = sSQLQueryString & " AND (ART4.CONCEPTO=0"
                If vConcepto(1) Then
                    sSQLQueryString = sSQLQueryString & " OR ART4.CONCEPTO=1"
                    If vConcepto(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.CONCEPTO=2"
                    End If
                Else
                    If vConcepto(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.CONCEPTO=2"
                    End If
                End If
                sSQLQueryString = sSQLQueryString & ")"
            Else
                If vConcepto(1) Then
                    sSQLQueryString = sSQLQueryString & " AND (ART4.CONCEPTO=1"
                    If vConcepto(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.CONCEPTO=2"
                    End If
                    sSQLQueryString = sSQLQueryString & ")"
                Else
                    If vConcepto(2) Then
                        sSQLQueryString = sSQLQueryString & " AND ART4.CONCEPTO=2"
                    End If
                End If
            End If
        End If
    End If
    If IsArray(vAlmacen) Then
        If vAlmacen(0) Or vAlmacen(1) Or vAlmacen(2) Then
            If vAlmacen(0) Then
                sSQLQueryString = sSQLQueryString & " AND (ART4.ALMACENAR=0"
                If vAlmacen(1) Then
                    sSQLQueryString = sSQLQueryString & " OR ART4.ALMACENAR=1"
                    If vAlmacen(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.ALMACENAR=2"
                    End If
                Else
                    If vAlmacen(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.ALMACENAR=2"
                    End If
                End If
                sSQLQueryString = sSQLQueryString & ")"
            Else
                If vAlmacen(1) Then
                    sSQLQueryString = sSQLQueryString & " AND (ART4.ALMACENAR=1"
                    If vAlmacen(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.ALMACENAR=2"
                    End If
                    sSQLQueryString = sSQLQueryString & ")"
                Else
                    If vAlmacen(2) Then
                        sSQLQueryString = sSQLQueryString & " AND ART4.ALMACENAR=2"
                    End If
                End If
            End If
        End If
    End If
    If IsArray(vRecep) Then
        If vRecep(0) Or vRecep(1) Or vRecep(2) Then
            If vRecep(0) Then
                sSQLQueryString = sSQLQueryString & " AND (ART4.RECEPCIONAR=0"
                If vRecep(1) Then
                    sSQLQueryString = sSQLQueryString & " OR ART4.RECEPCIONAR=1"
                    If vRecep(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.RECEPCIONAR=2"
                    End If
                Else
                    If vRecep(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.RECEPCIONAR=2"
                    End If
                End If
                sSQLQueryString = sSQLQueryString & ")"
            Else
                If vRecep(1) Then
                    sSQLQueryString = sSQLQueryString & " AND (ART4.RECEPCIONAR=1"
                    If vRecep(2) Then
                        sSQLQueryString = sSQLQueryString & " OR ART4.RECEPCIONAR=2"
                    End If
                    sSQLQueryString = sSQLQueryString & ")"
                Else
                    If vRecep(2) Then
                        sSQLQueryString = sSQLQueryString & " AND ART4.RECEPCIONAR=2"
                    End If
                End If
            End If
        End If
    End If
    
    'restriccion de uons
    If swhereuons <> "" Then
        sSQLQueryString = sSQLQueryString & swhereuons
    End If
    
    sSQLQueryString = sSQLQueryString & " ORDER BY GMN1.COD ASC, GMN2.COD ASC, GMN3.COD ASC, GMN4.COD ASC"
    
End If

ObtenerSQLArticulos = sSQLQueryString

End Function

 Public Sub ListadoArticulos(oReport As CRAXDRT.Report, ByVal bRMat As Boolean, ByVal sCodComp As String, ByVal sCodEqp As String, ByVal sGMN1Cod As String, _
                            ByVal sGMN2Cod As String, ByVal sGMN3Cod As String, ByVal sGMN4Cod As String, ByVal OrdEst As Boolean, ByVal Ordcod As Boolean, _
                            ByVal OrdDen As Boolean, ByVal sGMN1Den As String, ByVal sGMN2Den As String, ByVal sGMN3Den As String, ByVal sGMN4Den As String, _
                            ByVal sMON As String, ByVal Todos As Boolean, ByVal ArtDet As Boolean, ByVal ArtAdj As Boolean, ByVal AdjDet As Boolean, ByVal ArtPos As Boolean, _
                            Optional ByVal vConcepto As Variant, Optional ByVal vAlmacen As Variant, Optional ByVal vRecep As Variant, Optional ByVal ArtCentral As Boolean, _
                            Optional ByVal brUsuUON As Boolean, Optional ByVal brPerfUON As Boolean, _
                            Optional ByVal lIdPerfil As Integer, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, _
                            Optional ByVal sUON3 As String, Optional ByRef oUons As CUnidadesOrganizativas, _
                            Optional ByVal bIncluirAscendientes As Boolean, Optional ByVal bIncluirDescendientes As Boolean)
 
    Dim Table As CRAXDRT.DatabaseTable
    Dim SubListado As CRAXDRT.Report
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula
    Dim SelectionTextRpt(1 To 2, 1 To 7) As String
    ' ARRAY FORMULA FIELDS para SUBREPORT
    Dim SelectionTextSubRpt(1 To 2, 1 To 2) As String
    Dim RecordSortFieldsSubRpt(1 To 1) As String
    Dim i As Integer
    Dim v_concepto As Variant
    Dim v_almacen As Variant
    Dim v_recep As Variant
    
    v_concepto = vConcepto
    v_almacen = vAlmacen
    v_recep = vRecep
    
    If bcrs_Connected = False Then
        Exit Sub
    End If
    
    '*********** FORMULA FIELDS REPORT
    SelectionTextRpt(2, 1) = "OcultarDetallesItem"
    If ArtDet = False Then
        SelectionTextRpt(1, 1) = "S"
    Else
        SelectionTextRpt(1, 1) = "N"
    End If
    SelectionTextRpt(2, 2) = "OcultarAdj"
    If ArtAdj = False Then
        SelectionTextRpt(1, 2) = "S"
    Else
        SelectionTextRpt(1, 2) = "N"
    End If
    'Ocultar Estructura Materiales
    SelectionTextRpt(2, 3) = "OcultarEstMat"
    If ArtPos = False Then
        SelectionTextRpt(1, 3) = "S"
    Else
        SelectionTextRpt(1, 3) = "N"
    End If
    SelectionTextRpt(2, 4) = "GMN1NOM"
    SelectionTextRpt(1, 4) = sGMN1Den & ":"
    SelectionTextRpt(2, 5) = "GMN2NOM"
    SelectionTextRpt(1, 5) = sGMN2Den & ":"
    SelectionTextRpt(2, 6) = "GMN3NOM"
    SelectionTextRpt(1, 6) = sGMN3Den & ":"
    SelectionTextRpt(2, 7) = "GMN4NOM"
    SelectionTextRpt(1, 7) = sGMN4Den & ":"
        
     '*********** FORMULA FIELDS SUBREPORT
    SelectionTextSubRpt(2, 1) = "MON"
    SelectionTextSubRpt(1, 1) = sMON
    'Ocultar Detalles Adjudicacion
    SelectionTextSubRpt(2, 2) = "OcultarDetallesAdj"
    If AdjDet = False Then
        SelectionTextSubRpt(1, 2) = "S"
    Else
        SelectionTextSubRpt(1, 2) = "N"
    End If
        
    If ArtAdj = True Then
        If Ordcod = True Then
            RecordSortFieldsSubRpt(1) = "+{COMART.CODPROVE}"
        Else
            If OrdDen = True Then
                RecordSortFieldsSubRpt(1) = "+{COMART.DENPROVE}"
            End If
        End If
    End If

    'edu pm436
    'Solucionado error de migracion de a Crystal Reports 10 mediante nueva funcion en CRbasPublic
    
    ConectarReportInterno oReport, scrs_Server, scrs_Database, scrs_User, scrs_Password
    
    
    '********* REPORT PRINCIPAL **********'
    ' SQL QUERY
    
    oReport.SQLQueryString = ObtenerSQLArticulos(bRMat, sCodComp, sCodEqp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, OrdEst, Ordcod, OrdDen, Todos, , v_concepto, v_almacen, v_recep, ArtCentral, brUsuUON, brPerfUON, lIdPerfil, sUON1, sUON2, sUON3, oUons)
    
    ' FORMULA FIELDS
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next i
    
    '********* SUBREPORT ADJ *****************'
    Set SubListado = oReport.OpenSubreport("rptARTADJ")

    ConectarReportInterno SubListado, scrs_Server, scrs_Database, scrs_User, scrs_Password
        
    While SubListado.RecordSortFields.Count > 0
        SubListado.RecordSortFields.Delete 1
    Wend
        
    For i = 1 To UBound(RecordSortFieldsSubRpt)
        If Not RecordSortFieldsSubRpt(i) = "" Then
        SubListado.RecordSortFields.Add SubListado.Database.Tables(crs_Tableindex_Interno(SubListado, crs_SortTable_Interno(RecordSortFieldsSubRpt(i)))).Fields(crs_FieldIndex_Interno(SubListado, crs_SortTable_Interno(RecordSortFieldsSubRpt(i)), crs_SortField_Interno(RecordSortFieldsSubRpt(i)))), crs_SortDirection_Interno(RecordSortFieldsSubRpt(i))
        End If
    Next i
    
    ' FORMULA FIELDS subreport
    For i = 1 To UBound(SelectionTextSubRpt, 2)
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, SelectionTextSubRpt(2, i))).Text = """" & SelectionTextSubRpt(1, i) & """"
    Next i
    
    '********* SUBREPORT ATRIB *****************'
    Set SubListado = oReport.OpenSubreport("rtpARTATRIB")
    
    ConectarReportInterno SubListado, scrs_Server, scrs_Database, scrs_User, scrs_Password
    
    While SubListado.RecordSortFields.Count > 0
        SubListado.RecordSortFields.Delete 1
    Wend
        

End Sub
Public Sub ListadoArticulosEst(oReport As CRAXDRT.Report, ByVal bRMat As Boolean, ByVal sCodComp As String, ByVal sCodEqp As String, ByVal sGMN1Cod As String, _
                                ByVal sGMN2Cod As String, ByVal sGMN3Cod As String, ByVal sGMN4Cod As String, ByVal OrdEst As Boolean, ByVal Ordcod As Boolean, _
                                ByVal OrdDen As Boolean, ByVal sGMN1Den As String, ByVal sGMN2Den As String, ByVal sGMN3Den As String, ByVal sGMN4Den As String, _
                                ByVal sMON As String, ByVal Todos As Boolean, ByVal ArtDet As Boolean, ByVal ArtAdj As Boolean, ByVal AdjDet As Boolean, ByVal ArtPos As Boolean, ByVal ArtGen As Boolean, _
                                Optional ByVal vConcepto As Variant, Optional ByVal vAlmacen As Variant, Optional ByVal vRecep As Variant, Optional ByVal ArtCentral As Boolean, _
                                Optional ByVal brUsuUON As Boolean, Optional ByVal brPerfUON As Boolean, _
                                Optional ByVal lIdPerfil As Integer, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, _
                                Optional ByVal sUON3 As String, Optional ByRef oUons As CUnidadesOrganizativas)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim SubListado As CRAXDRT.Report
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula
    Dim SelectionTextRpt(1 To 2, 1 To 8) As String
    ' ARRAY FORMULA FIELDS para SUBREPORT
    Dim SelectionTextSubRpt(1 To 2, 1 To 2) As String
    Dim RecordSortFieldsSubRpt(1 To 1) As String

    
    If bcrs_Connected = False Then
        Exit Sub
    End If
    
    '*********** FORMULA FIELDS REPORT
    SelectionTextRpt(2, 1) = "OcultarDetallesItem"
    If ArtDet = False Then
        SelectionTextRpt(1, 1) = "S"
    Else
        SelectionTextRpt(1, 1) = "N"
    End If
    SelectionTextRpt(2, 2) = "OcultarAdj"
    If ArtAdj = False Then
        SelectionTextRpt(1, 2) = "S"
    Else
        SelectionTextRpt(1, 2) = "N"
    End If
    'Ocultar Estructura Materiales
    SelectionTextRpt(2, 3) = "OcultarEstMat"
    If ArtPos = False Then
        SelectionTextRpt(1, 3) = "S"
    Else
        SelectionTextRpt(1, 3) = "N"
    End If
    SelectionTextRpt(2, 4) = "GMN1NOM"
    SelectionTextRpt(1, 4) = sGMN1Den & ":"
    SelectionTextRpt(2, 5) = "GMN2NOM"
    SelectionTextRpt(1, 5) = sGMN2Den & ":"
    SelectionTextRpt(2, 6) = "GMN3NOM"
    SelectionTextRpt(1, 6) = sGMN3Den & ":"
    SelectionTextRpt(2, 7) = "GMN4NOM"
    SelectionTextRpt(1, 7) = sGMN4Den & ":"
    'Ocultar la columna Gen�rico
    SelectionTextRpt(2, 8) = "OcultarGen"
    If ArtGen Then
        SelectionTextRpt(1, 8) = "S"
    Else
        SelectionTextRpt(1, 8) = "N"
    End If
     '*********** FORMULA FIELDS SUBREPORT ADJ
    SelectionTextSubRpt(2, 1) = "MON"
    SelectionTextSubRpt(1, 1) = sMON
    'Ocultar Detalles Adjudicacion
    SelectionTextSubRpt(2, 2) = "OcultarDetallesAdj"
    If AdjDet = False Then
        SelectionTextSubRpt(1, 2) = "S"
    Else
        SelectionTextSubRpt(1, 2) = "N"
    End If
        
    If ArtAdj = True Then
        If Ordcod = True Then
            RecordSortFieldsSubRpt(1) = "+{COMART.CODPROVE}"
        Else
            If OrdDen = True Then
                RecordSortFieldsSubRpt(1) = "+{COMART.DENPROVE}"
            End If
        End If
    End If

    'edu
    ConectarReportInterno oReport, scrs_Server, scrs_Database, scrs_User, scrs_Password

    '********* REPORT PRINCIPAL **********'
    ' SQL QUERY
    oReport.SQLQueryString = ObtenerSQLArticulos(bRMat, sCodComp, sCodEqp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, OrdEst, Ordcod, OrdDen, Todos, ArtGen, vConcepto, vAlmacen, vRecep, ArtCentral, brUsuUON, brPerfUON, lIdPerfil, sUON1, sUON2, sUON3, oUons)
    ' FORMULA FIELDS
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex_Interno(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next i
    
    '********* SUBREPORT ADJ *****************'
    Set SubListado = oReport.OpenSubreport("rptARTESTADJ")
    
    ConectarReportInterno SubListado, scrs_Server, scrs_Database, scrs_User, scrs_Password
    
    While SubListado.RecordSortFields.Count > 0
        SubListado.RecordSortFields.Delete 1
    Wend
        
    For i = 1 To UBound(RecordSortFieldsSubRpt)
        If Not RecordSortFieldsSubRpt(i) = "" Then
        SubListado.RecordSortFields.Add SubListado.Database.Tables(crs_Tableindex_Interno(SubListado, crs_SortTable_Interno(RecordSortFieldsSubRpt(i)))).Fields(crs_FieldIndex_Interno(SubListado, crs_SortTable_Interno(RecordSortFieldsSubRpt(i)), crs_SortField_Interno(RecordSortFieldsSubRpt(i)))), crs_SortDirection_Interno(RecordSortFieldsSubRpt(i))
        End If
    Next i
    
    ' FORMULA FIELDS subreport
    For i = 1 To UBound(SelectionTextSubRpt, 2)
        SubListado.FormulaFields(crs_FormulaIndex_Interno(SubListado, SelectionTextSubRpt(2, i))).Text = """" & SelectionTextSubRpt(1, i) & """"
    Next i
    
    '********* SUBREPORT ATRIB *****************'
    Set SubListado = oReport.OpenSubreport("rptARTESTATRIB")
    
    ConectarReportInterno SubListado, scrs_Server, scrs_Database, scrs_User, scrs_Password
    
    While SubListado.RecordSortFields.Count > 0
        SubListado.RecordSortFields.Delete 1
    Wend
End Sub


