VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CExcelCatalogo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public oFSGSRaiz As CRaiz




Public Function CargarArticulosExcel(ByRef appexcel As Object, ByVal sFile As String, ByRef sError As String, ByVal sTitulo As String, ByVal ofrmESPERA As Object, _
                                     ByRef iOk As Integer) As Object
Dim iUltimaFila As Integer
Dim iFila As Integer
Dim xlBook As Object
Dim xlSheet As Object
Dim sConsulta As String
Dim sProveCod As String
Dim sArtCod As String
Dim sCodext As String
Dim sArtEsp As String
Dim sArtDEN As String
Dim sPrecio As String
Dim sCatn1 As String
Dim sCatN2 As String
Dim sCatN3 As String
Dim sCatN4 As String
Dim sCatN5 As String
Dim sUniPed As String
Dim sFacConv As String
Dim sMoneda As String
Dim sDestino As String
Dim sPublicar As String
Dim oFos As Scripting.FileSystemObject
Dim sCatn1_Ant As String
Dim sCatn2_Ant As String
Dim sCatn3_Ant As String
Dim sCatn4_Ant As String
Dim sCatn5_Ant As String
Dim lIdCatn1 As Variant
Dim lIdCatn2 As Variant
Dim lIdCatn3 As Variant
Dim lIdCatn4 As Variant
Dim lIdCatn5 As Variant
Dim i As Integer
Dim oLinea As CLineaCatalogo
Dim frmESPERA As Object
Dim bConErrores As Boolean

Set oFos = New Scripting.FileSystemObject
If Not oFos.FileExists(sFile) Then
    sError = "1"
    Exit Function
End If


Set xlBook = appexcel.Workbooks.Open(sFile)
Set xlSheet = xlBook.Sheets.Item(1)
i = 2

If xlSheet.Range("A1") <> "ERRORES" Or _
    xlSheet.Range("M1") <> "CAT.NIVEL 3" Or _
    xlSheet.Range("P1") <> "PUBLICAR" Then
    sError = "2"
    xlBook.Close
    Exit Function
End If

Do While xlSheet.Range("B" & i) <> ""
    i = i + 1
Loop
Set frmESPERA = ofrmESPERA
Load ofrmESPERA
frmESPERA.lblGeneral.Caption = sTitulo
frmESPERA.ProgressBar2.Max = i - 1
frmESPERA.frame3.Visible = False
frmESPERA.Height = frmESPERA.Height - frmESPERA.frame3.Height - 50

frmESPERA.Show

DoEvents
frmESPERA.ProgressBar2.Value = 1
frmESPERA.lblDetalle = ""
xlSheet.Columns("A").Hidden = False
i = 2
iFila = 2

Do While xlSheet.Range("B" & i) <> ""
    On Error GoTo Error
    sArtCod = ""
    sCodext = ""
    sArtDEN = ""
    sProveCod = ""
    sPrecio = ""
    sUniPed = ""
    sFacConv = ""
    sArtEsp = ""
    sCatn1 = ""
    sCatN2 = ""
    sCatN3 = ""
    sCatN4 = ""
    sCatN5 = ""
    sMoneda = ""
    sDestino = ""
    sPublicar = ""
    
    'Columna A se utiliza para dejar el estado de la carga de la l�nea.
    With xlSheet
        'CODIGO ARTICULO FULLSTEP
        If .Range("B" & iFila) <> "" Then
            sArtCod = .Range("B" & iFila)
            sArtCod = Replace(sArtCod, " ", "")
        Else
            Exit Do
        End If
        
        'REFERENCIA PROVEEDOR
        If .Range("C" & iFila) <> "" Then
            sCodext = .Range("C" & iFila)
        End If
        
        'DENOMINACI�N
        If .Range("D" & iFila) <> "" Then
            sArtDEN = .Range("D" & iFila)
        End If
        
        'PROVEEDOR NIF
        If .Range("E" & iFila) <> "" Then
            sProveCod = .Range("E" & iFila)
            sProveCod = Replace(sProveCod, " ", "")
        End If
            
        'DESTINO
        If .Range("F" & iFila) <> "" Then
            sDestino = .Range("F" & iFila)
            sDestino = Replace(sDestino, " ", "")
        End If
        
        'UP_DEF
        If .Range("G" & iFila) <> "" Then
            sUniPed = .Range("G" & iFila)
            sUniPed = Replace(sUniPed, " ", "")
        End If
        
        'PRECIO
        If .Range("H" & iFila) <> "" Then
            sPrecio = .Range("H" & iFila)
            If InStr(1, sPrecio, ",", vbTextCompare) > 0 Then
                sPrecio = Replace(sPrecio, ",", ".")
            End If
        End If
        
        'MONEDA
        If .Range("I" & iFila) <> "" Then
            sMoneda = .Range("I" & iFila)
            sMoneda = Replace(sMoneda, " ", "")
        End If
        
        'ESPECIFICACIONES
        If .Range("J" & iFila) <> "" Then
            sArtEsp = .Range("J" & iFila)
        End If
        
        'CATN1
        If .Range("K" & iFila) <> "" Then
            sCatn1 = .Range("K" & iFila)
            sCatn1 = Replace(sCatn1, " ", "")
        End If
        
        'CATN2
        If .Range("L" & iFila) <> "" Then
            sCatN2 = .Range("L" & iFila)
            sCatN2 = Replace(sCatN2, " ", "")
        End If
        
        'CATN3
        If .Range("M" & iFila) <> "" Then
            sCatN3 = .Range("M" & iFila)
            sCatN3 = Replace(sCatN3, " ", "")
        End If
        
        'CATN4
        If .Range("N" & iFila) <> "" Then
            sCatN4 = .Range("N" & iFila)
            sCatN4 = Replace(sCatN4, " ", "")
        End If
        
        'CATN5
        If .Range("O" & iFila) <> "" Then
            sCatN5 = .Range("O" & iFila)
            sCatN5 = Replace(sCatN5, " ", "")
        End If
        
        'PUBLICAR
        If .Range("P" & iFila) <> "" Then
            sPublicar = .Range("P" & iFila)
        End If
        
        'FC_DEF
        sFacConv = "1"
         
    End With
    
    Set oLinea = oFSGSRaiz.Generar_CLineaCatalogo
    sError = oLinea.CrearCatalogLin(sArtCod, sCodext, sArtDEN, sProveCod, sPrecio, sUniPed, sFacConv, sArtEsp, sCatn1, sCatN2, sCatN3, sCatN4, sCatN5, _
                             sMoneda, sDestino, sCatn1_Ant, sCatn2_Ant, sCatn3_Ant, sCatn4_Ant, sCatn5_Ant, lIdCatn1, lIdCatn2, lIdCatn3, lIdCatn4, lIdCatn5, sPublicar)
    Set oLinea = Nothing
    If sError <> "" Then
        xlSheet.Range("A" & iFila) = sError
        bConErrores = True
    Else
        iOk = iOk + 1
    End If

Siguiente:
    iFila = iFila + 1
    i = i + 1
    frmESPERA.ProgressBar2.Value = i - 1
Loop
If Not bConErrores Then
    xlSheet.Columns("A").Hidden = True
    xlBook.Save
    xlBook.Close
Else
    sError = "ConErrores"
End If

Unload frmESPERA

Exit Function
Error:
    sError = sError & "" & vbCrLf & xlSheet.Range("B" & i) & " - " & xlSheet.Range("D" & i)
    GoTo Siguiente

End Function

'''Descarga la plantilla de excel para cargar art�culos en el cat�logo
Public Function ExcelCargaArticulos(ByRef appexcel As Object, ByVal sPath As String, ByRef iErr As Integer) As Object
Dim xlBook As Object
Dim oFos As Scripting.FileSystemObject
Set oFos = New Scripting.FileSystemObject
If Not oFos.FileExists(sPath & "\CargaArticulosCatalogo.xlt") Then
    iErr = 1
    Exit Function
End If
Set xlBook = appexcel.Workbooks.Add(sPath & "\CargaArticulosCatalogo.xlt")
Set ExcelCargaArticulos = xlBook
Set oFos = Nothing
End Function

'
Public Function ExportarPreciosExcel(ByRef appexcel As Object, ByVal oLineasCatalogo As CLineasCatalogo, ByVal sPath As String) As Object
    Dim xlBook As Object
    Dim xlSheet As Object
    Dim oFos As Scripting.FileSystemObject
    Dim i As Integer
    Dim oLinea As CLineaCatalogo
    Dim oProves As CProveedores
    
    Set oFos = New Scripting.FileSystemObject
    Set xlBook = appexcel.Workbooks.Add(sPath & "\CatalogoPrecios.xlt")
    Set xlSheet = xlBook.Sheets.Item(1)
    xlSheet.Unprotect
    Set oLinea = oFSGSRaiz.Generar_CLineaCatalogo
    Set oProves = oFSGSRaiz.generar_CProveedores
    i = 2
    For Each oLinea In oLineasCatalogo
        'C�digo de L�nea
        xlSheet.Range("A" & i) = oLinea.Id
        'C�digo de Art�culo
        xlSheet.Range("B" & i) = oLinea.ArtCod_Interno
        'C�digo de Art�culo Proveedor
        xlSheet.Range("C" & i) = oLinea.ArtCod_Externo
        'Denominaci�n Art�culo
        xlSheet.Range("D" & i) = oLinea.ArtDen
        'C�digo de Proveedor
        xlSheet.Range("E" & i) = oLinea.ProveCod
        'Proveedor
        oProves.CargarTodosLosProveedoresDesde3 1, oLinea.ProveCod, , True
        xlSheet.Range("F" & i) = oProves.Item(1).Den
        'Unidad base
        xlSheet.Range("G" & i) = oLinea.UnidadCompra
        'Precio
        xlSheet.Range("H" & i) = oLinea.PrecioUC
        'Moneda
        xlSheet.Range("I" & i) = oLinea.mon
        'Unidad de pedido
        xlSheet.Range("J" & i) = oLinea.UnidadPedido
        'Factor de conversi�n
        xlSheet.Range("K" & i) = oLinea.FactorConversion
        'Publicado
        xlSheet.Range("L" & i) = IIf(oLinea.Publicado, 1, 0)
        i = i + 1
    Next
    xlSheet.Protect
    
    xlBook.Sheets.Item(1).Select
    Set oProves = Nothing
    Set oLinea = Nothing
    Set ExportarPreciosExcel = xlBook
    Set oFos = Nothing
End Function

Public Function ImportarPreciosExcel(ByRef appexcel As Object, ByVal sFile As String, ByRef sError As String) As Object
Dim xlBook As Object
Dim xlSheet As Object
Dim oFos As Scripting.FileSystemObject
Dim i As Integer
Dim dPrecio As Double
Dim sUnidadBase As String
Dim sUnidadPedido As String
Dim sPublicado As String
Dim dFactor As Double
Dim tsError As TipoErrorSummit
Dim oLinea As CLineaCatalogo
Dim lID As Long

Set oFos = New Scripting.FileSystemObject
Set xlBook = appexcel.Workbooks.Add(sFile)
Set xlSheet = xlBook.Sheets.Item(1)
i = 2
Set oLinea = oFSGSRaiz.Generar_CLineaCatalogo

Do While xlSheet.Range("A" & i) <> ""
    On Error GoTo Error
    lID = xlSheet.Range("A" & i)
    sUnidadBase = xlSheet.Range("M" & i)
    If Not IsNumeric(xlSheet.Range("N" & i)) Then
        GoTo Error
    End If
    dPrecio = StrToDbl0(xlSheet.Range("N" & i))
    
    sUnidadPedido = xlSheet.Range("O" & i)
    If Not IsNumeric(xlSheet.Range("P" & i)) Then
        GoTo Error
    End If
    dFactor = StrToDbl0(xlSheet.Range("P" & i))
    sPublicado = xlSheet.Range("Q" & i)
    
    'Debe haber algun valor en la l�nea para actualizarla
    If dPrecio > 0 Or dFactor > 0 Or sUnidadBase <> "" Or sUnidadPedido <> "" Or sPublicado <> "" Then
        tsError = oLinea.ActualizarPrecios(lID, sUnidadBase, dPrecio, sUnidadPedido, dFactor, sPublicado)
        If tsError.NumError <> TESnoerror Then
            'Ha habido un problema al actualizar, almacenamos los erroes para mostrarlos despu�s
            sError = sError & "" & vbCrLf & xlSheet.Range("B" & i) & " - " & xlSheet.Range("D" & i)
        End If
    End If
Siguiente:
    i = i + 1
Loop

Set oLinea = Nothing
Exit Function
Error:
    sError = sError & "" & vbCrLf & xlSheet.Range("B" & i) & " - " & xlSheet.Range("D" & i)
    GoTo Siguiente
End Function


