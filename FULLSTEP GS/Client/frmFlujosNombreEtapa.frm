VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosNombreEtapa 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DBloque:"
   ClientHeight    =   2055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5610
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosNombreEtapa.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2055
   ScaleWidth      =   5610
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraBloque 
      BackColor       =   &H00808000&
      Caption         =   "DBloque"
      ForeColor       =   &H00FFFFFF&
      Height          =   1395
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5595
      Begin SSDataWidgets_B.SSDBGrid sdbgDen 
         Height          =   995
         Left            =   1320
         TabIndex        =   2
         Top             =   220
         Width           =   3735
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   1
         BorderStyle     =   0
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD_IDIOMA"
         Columns(0).Name =   "COD_IDIOMA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DEN_IDIOMA"
         Columns(1).Name =   "DEN_IDIOMA"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).ForeColor=   16777215
         Columns(1).BackColor=   8421376
         Columns(2).Width=   3200
         Columns(2).Caption=   "VALOR_IDIOMA"
         Columns(2).Name =   "VALOR_IDIOMA"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   50
         _ExtentX        =   6588
         _ExtentY        =   1755
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblNombre 
         BackColor       =   &H00808000&
         Caption         =   "DNombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   360
         TabIndex        =   1
         Top             =   305
         Width           =   1065
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   1320
      ScaleHeight     =   420
      ScaleWidth      =   2625
      TabIndex        =   5
      Top             =   1560
      Width           =   2625
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   4
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   3
         Top             =   60
         Width           =   1050
      End
   End
End
Attribute VB_Name = "frmFlujosNombreEtapa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public lIdBloque As Long
Public oBloque As CBloque

Private m_sMensajeDenominacion As String
Private oIdiomas As CIdiomas

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    
    If sdbgDen.DataChanged Then
        sdbgDen.Update
    End If
    
    If oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den = "" Then
        oMensajes.NoValido m_sMensajeDenominacion & " " & oGestorParametros.DevolverIdiomas.Item(basPublic.gParametrosInstalacion.gIdioma).Den
        Exit Sub
    End If
    
    Set oIBaseDatos = oBloque
    teserror = oIBaseDatos.FinalizarEdicionModificando
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Set oIBaseDatos = Nothing
        Exit Sub
    Else
        basSeguridad.RegistrarAccion AccionesSummit.ACCBloqueModif, "ID:" & oBloque.Id
    End If
    'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
    frmFlujos.HayCambios
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = frmFlujos.Top + (frmFlujos.Height / 2) - (Me.Height / 2)
    If Me.Top < 0 Then
        Me.Top = 0
    End If
    Me.Left = frmFlujos.Left + (frmFlujos.Width / 2) - (Me.Width / 2)
    If Me.Left < 0 Then
        Me.Left = 0
    End If
    CargarRecursos
    CargarBloque
    Me.caption = Me.caption & oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
End Sub

Private Sub CargarRecursos()
   Dim Ador As Ador.Recordset

    On Error Resume Next
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSNOMBREETAPA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value '1
        Ador.MoveNext
        fraBloque.caption = Ador(0).Value
        Ador.MoveNext
        lblNombre.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '5
        Ador.MoveNext
        m_sMensajeDenominacion = Ador(0).Value
                
        Ador.Close
    End If
End Sub

Private Sub CargarBloque()
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim iNumIdiomas As Integer

    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    iNumIdiomas = oIdiomas.Count
    
    'Se mostrar� scroll vertical solo si hay m�s de 4 idiomas
    sdbgDen.ScrollBars = 0      '0-ssScrollBarsNone
    If iNumIdiomas > 4 Then sdbgDen.ScrollBars = 2      '2-ssScrollBarsVertical
                
    Set oBloque = oFSGSRaiz.Generar_CBloque
    oBloque.Id = lIdBloque
    Set oIBaseDatos = oBloque
    teserror = oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Set oIBaseDatos = Nothing
        Exit Sub
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oBloque = Nothing
End Sub

Private Sub sdbgDen_LostFocus()
    If sdbgDen.DataChanged Then
        sdbgDen.Update
    End If
End Sub

Private Sub sdbgDen_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)
    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim i As Integer
    
    Dim iNumIdiomas As Integer
    Dim m_lDenPointer As Long
        
    '''CargarIdiomas
    If oBloque.Denominaciones Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If
  
    iNumIdiomas = oBloque.Denominaciones.Count
    
    If IsNull(StartLocation) Then                   'If the grid is empty then
        If ReadPriorRows Then                           'If moving backwards through grid then
            m_lDenPointer = iNumIdiomas - 1                 'pointer = # of last grid row
        Else                                            'else
            m_lDenPointer = 1                               'pointer = # of first grid row
        End If
    Else                                            'If the grid already has data in it then
        m_lDenPointer = StartLocation                   'pointer = location just before or after the row where data will be added
        If ReadPriorRows Then                           'If moving backwards through grid then
            m_lDenPointer = m_lDenPointer - 1           'move pointer back one row
        Else                                            'else
            m_lDenPointer = m_lDenPointer + 1           'move pointer ahead one row
        End If
    End If
    
    'The pointer (m_lDenPointer) now points to the row of the grid where you will start adding data.
    For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
        If (m_lDenPointer < 0) Or (m_lDenPointer > iNumIdiomas) Then
            Exit For                                        'If the pointer is outside the grid then stop this
        End If
    
        RowBuf.Value(i, 0) = oBloque.Denominaciones.Item(m_lDenPointer).Cod
        RowBuf.Value(i, 1) = oIdiomas.Item(oBloque.Denominaciones.Item(m_lDenPointer).Cod).Den
        RowBuf.Value(i, 2) = oBloque.Denominaciones.Item(m_lDenPointer).Den
        RowBuf.Bookmark(i) = m_lDenPointer                  'set the value of the bookmark for the current row in the rowbuffer
    
        If ReadPriorRows Then                               'move the pointer forward or backward, depending
            m_lDenPointer = m_lDenPointer - 1                  'on which way it's supposed to move
        Else
            m_lDenPointer = m_lDenPointer + 1
        End If
        r = r + 1                                           'increment the number of rows read
    Next
    
    RowBuf.RowCount = r                                  'set the size of the row buffer to the number of rows read
End Sub

Private Sub sdbgDen_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    If Not (WriteLocation < 1) And Not (WriteLocation > oBloque.Denominaciones.Count) Then
        oBloque.Denominaciones.Item(WriteLocation).Den = RowBuf.Value(0, 2)
    End If
End Sub
