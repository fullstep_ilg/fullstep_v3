VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPROVEEqpPorProve 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de equipos por proveedor (opciones)"
   ClientHeight    =   3315
   ClientLeft      =   540
   ClientTop       =   1425
   ClientWidth     =   8115
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPROVEEqpPorProve.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3315
   ScaleWidth      =   8115
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   8115
      TabIndex        =   11
      Top             =   2940
      Width           =   8115
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener"
         Height          =   375
         Left            =   6750
         TabIndex        =   10
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab sTabGeneral 
      Height          =   2895
      Left            =   0
      TabIndex        =   12
      Top             =   15
      Width           =   8085
      _ExtentX        =   14261
      _ExtentY        =   5106
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstPROVEEqpPorProve.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstPROVEEqpPorProve.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(1)=   "Frame4"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Orden"
      TabPicture(2)   =   "frmLstPROVEEqpPorProve.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame1"
      Tab(2).Control(1)=   "Frame5"
      Tab(2).ControlCount=   2
      Begin VB.Frame Frame3 
         Height          =   1635
         Left            =   -74880
         TabIndex        =   24
         Top             =   360
         Width           =   7875
         Begin VB.CheckBox chkRestEqp 
            Caption         =   "Incluir proveedores no asociados al equipo comprador"
            Height          =   195
            Left            =   780
            TabIndex        =   31
            Top             =   720
            Width           =   6240
         End
         Begin VB.OptionButton optProve 
            Caption         =   "Proveedor"
            Height          =   195
            Left            =   240
            TabIndex        =   27
            Top             =   1200
            Width           =   1335
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Listado completo"
            Height          =   195
            Left            =   240
            TabIndex        =   26
            Top             =   390
            Value           =   -1  'True
            Width           =   2145
         End
         Begin VB.CommandButton cmdBuscar 
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6825
            Picture         =   "frmLstPROVEEqpPorProve.frx":0D06
            Style           =   1  'Graphical
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   1155
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   3000
            TabIndex        =   28
            Top             =   1155
            Width           =   3795
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6694
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   1680
            TabIndex        =   29
            Top             =   1155
            Width           =   1290
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2275
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame Frame4 
         Height          =   765
         Left            =   -74880
         TabIndex        =   20
         Top             =   1950
         Width           =   7875
         Begin VB.CheckBox chkDatosBasicos 
            Caption         =   "Incluir datos b�sicos"
            Height          =   195
            Left            =   75
            TabIndex        =   23
            Top             =   345
            Width           =   2595
         End
         Begin VB.CheckBox chkCalifica 
            Caption         =   "Incluir datos homologaci�n"
            Height          =   195
            Left            =   4965
            TabIndex        =   22
            Top             =   345
            Width           =   2820
         End
         Begin VB.CheckBox chkContactos 
            Caption         =   "Incluir contactos"
            Height          =   195
            Left            =   2700
            TabIndex        =   21
            Top             =   345
            Width           =   2220
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1935
         Left            =   -74550
         TabIndex        =   19
         Top             =   570
         Width           =   6645
         Begin VB.OptionButton optOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   3465
            TabIndex        =   9
            Top             =   900
            Width           =   2685
         End
         Begin VB.OptionButton OptOrdCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   690
            TabIndex        =   8
            Top             =   900
            Value           =   -1  'True
            Width           =   2220
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   5895
         Top             =   -75
      End
      Begin VB.Frame Frame2 
         Caption         =   "Material"
         Height          =   2355
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   7320
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   6480
            Picture         =   "frmLstPROVEEqpPorProve.frx":0D93
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   360
            Width           =   345
         End
         Begin VB.CommandButton cmdSelMat 
            Height          =   315
            Left            =   6870
            Picture         =   "frmLstPROVEEqpPorProve.frx":0E38
            Style           =   1  'Graphical
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   360
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   1815
            TabIndex        =   0
            Top             =   420
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1164
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3889
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
            Height          =   285
            Left            =   1815
            TabIndex        =   2
            Top             =   885
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
            Height          =   285
            Left            =   1815
            TabIndex        =   4
            Top             =   1335
            Width           =   1080
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1905
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
            Height          =   285
            Left            =   1815
            TabIndex        =   6
            Top             =   1800
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
            Height          =   285
            Left            =   2895
            TabIndex        =   1
            Top             =   420
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
            Height          =   285
            Left            =   2895
            TabIndex        =   3
            Top             =   885
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
            Height          =   285
            Left            =   2895
            TabIndex        =   5
            Top             =   1335
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
            Height          =   285
            Left            =   2895
            TabIndex        =   7
            Top             =   1800
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "Grupo:"
            Height          =   225
            Left            =   195
            TabIndex        =   18
            Top             =   1830
            Width           =   1605
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Comodity:"
            Height          =   225
            Left            =   195
            TabIndex        =   17
            Top             =   450
            Width           =   1605
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "Familia:"
            Height          =   225
            Left            =   195
            TabIndex        =   16
            Top             =   915
            Width           =   1590
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "Subfamilia:"
            Height          =   225
            Left            =   195
            TabIndex        =   15
            Top             =   1365
            Width           =   1605
         End
      End
      Begin VB.Frame Frame5 
         Height          =   2355
         Left            =   -74880
         TabIndex        =   30
         Top             =   360
         Width           =   7320
      End
   End
End
Attribute VB_Name = "frmLstPROVEEqpPorProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Variables de seguridad
Private bREqp As Boolean ' Restriccion de equipo
Private bRMat As Boolean ' Restriccion de material
'Private bRMatAsig As Boolean
Public RespetarComboProve As Boolean
Private oProveSeleccionado As CProveedor
Private oProves As CProveedores


'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4

Private oEqps As CEquipos
Private oEqpSeleccionado As CEquipo
Private oIEquiposAsignados As IEquiposAsigAProveedor
Private Accion As accionessummit

'''Combos
Public GMN1RespetarCombo As Boolean
Private GMN1CargarComboDesde As Boolean
Public GMN2RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Public GMN3RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean
Public GMN4RespetarCombo As Boolean
Private GMN4CargarComboDesde As Boolean


'Multilenguaje

Private sIdiMaterial As String
Private sIdiEqp As String
Private sIdiEqpProve As String
Private sIdiGenerando As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private sIdiTitulo As String
Private srIdiTitulo As String
Private srIdiSeleccion As String
Private srIdiProveedor As String
Private srIdiNIF As String
Private srIdiDireccion As String
Private srIdiMoneda As String
Private srIdiHomologacion As String
Private srIdiPag As String
Private srIdiDe As String
Private srIdiContactos As String


Public Sub CargarProveedorConBusqueda()

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
   
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveCod_Validate False
    ProveedorSeleccionado
    
End Sub

Private Sub cmdBorrar_Click()
    sdbcGMN1_4Cod.Text = ""
End Sub

Private Sub optProve_Click()

sdbcProveCod.Enabled = True
sdbcProveDen.Enabled = True
cmdBuscar.Enabled = True
chkRestEqp.Enabled = False
End Sub

Private Sub optTodos_Click()

    sdbcProveCod.Enabled = False
    sdbcProveDen.Enabled = False
    cmdBuscar.Enabled = False
    chkRestEqp.Enabled = True
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN1_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sTabGeneral.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
    
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
    Else
        
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        GMN1RespetarCombo = False
        GMN1Seleccionado
        GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN1_4Den_Click()
     If Not sdbcGMN1_4Den.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN1_4Den_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sTabGeneral.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, , False)
    Else
       
        oGMN1s.CargarTodosLosGruposMat , sdbcGMN1_4Den.Text, True, , False
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Den.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        
        sdbcGMN1_4Den.Columns(0).Value = sdbcGMN1_4Den.Text
        sdbcGMN1_4Den.Columns(1).Value = sdbcGMN1_4Cod.Text
                    
        GMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN2_4Cod_Click()
     
     If Not sdbcGMN2_4Cod.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sTabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN1Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
          
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Den_Click()
     
     If Not sdbcGMN2_4Cod.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sTabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN1Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, , Trim(sdbcGMN2_4Den), True, , False)
        Else
          
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2s.Item(1).Cod
        
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
                    
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcGMN3_4Cod_Click()
    
     If Not sdbcGMN3_4Cod.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sTabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN2Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
           
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
   Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not GMN3RespetarCombo Then

        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Den_Click()
     
     If Not sdbcGMN3_4Cod.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
'    If sdbcGMN3_4Den.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False)
        End If
        
    
    Else
        
        
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sTabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False)
        Else
            
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
   Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3s.Item(1).Cod
        
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
                    
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN4_4Cod_Click()
     
     If Not sdbcGMN4_4Cod.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Private Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sTabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN3Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
           
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN4_4Den_Click()
     If Not sdbcGMN4_4Den.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub
Private Sub sdbcGMN1_4Cod_Change()

    If Not GMN1RespetarCombo Then
    
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub
Private Sub sdbcGMN1_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN1_4Den.RemoveAll

    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
       
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Cod.RemoveAll

    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not GMN2RespetarCombo Then

        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    'If sdbcGMN2_4Den.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , False)
        End If
    
    Else
        
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    Screen.MousePointer = vbNormal

End Sub



Private Sub sdbcGMN3_4Cod_Change()
    
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Cod.RemoveAll

    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not GMN1RespetarCombo Then

        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
        
    If sdbcGMN1_4Den.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
    If Not GMN4RespetarCombo Then
    
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        GMN4RespetarCombo = False
        
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Cod.RemoveAll

    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else
        
        
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not GMN4RespetarCombo Then

        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        GMN4RespetarCombo = False
        
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Den.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False)
        End If
    Else
        
     
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    Screen.MousePointer = vbNormal
    
End Sub




Private Sub Form_Load()

Me.Width = 8235
Me.Height = 3720

If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If


CargarRecursos

    PonerFieldSeparator Me

ConfigurarNombres

ConfigurarSeguridad

Set oProves = oFSGSRaiz.generar_CProveedores

Set oEqps = oFSGSRaiz.Generar_CEquipos
'Me.sTabGeneral.Tab = 0
If bRMat Then cmdObtener.Enabled = False
If bREqp Then
    chkRestEqp.Visible = True
Else
    chkRestEqp.Visible = False
End If

If optTodos.Value = True Then
    sdbcProveCod.Enabled = False
    sdbcProveDen.Enabled = False
    cmdBuscar.Enabled = False
    chkRestEqp.Enabled = True
End If

        
End Sub


Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sTabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN3Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
           
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
                    
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveCod_Click()
     
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    If sdbcProveCod.Text = sdbcProveCod.Columns(0).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
        RespetarComboProve = False
        ProveedorSeleccionado
        Exit Sub
    End If
    
    If sdbcProveCod.Text = sdbcProveDen.Columns(1).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
        RespetarComboProve = False
        ProveedorSeleccionado
        Exit Sub
    End If
    

    ''' Solo continuamos si existe el proveedor

    Screen.MousePointer = vbHourglass
    oProves.BuscarTodosLosProveedoresDesde 1, , Trim(sdbcProveCod.Text), , , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False
        
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        
        RespetarComboProve = False
        ProveedorSeleccionado
    End If
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_Click()
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub sdbcProveDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveDen.Text = "" Then Exit Sub
    
    If sdbcProveDen.Text = sdbcProveDen.Columns(0).Text Then
        RespetarComboProve = True
        sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
        RespetarComboProve = False
        ProveedorSeleccionado
        Exit Sub
    End If
    
    If sdbcProveDen.Text = sdbcProveCod.Columns(1).Text Then
        RespetarComboProve = True
        sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
        RespetarComboProve = False
        ProveedorSeleccionado
        Exit Sub
    End If
    
    If sdbcProveCod.Text <> "" Then
        Exit Sub
    End If

    ''' Solo continuamos si existe el proveedor

    Screen.MousePointer = vbHourglass
    oProves.BuscarTodosLosProveedoresDesde 1, , , Trim(sdbcProveDen.Text), , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , True, , , , False
        
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Den) = UCase(sdbcProveDen.Text))
    If Not bExiste Then
        sdbcProveDen.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        
        sdbcProveDen.Columns(0).Value = sdbcProveDen.Text
        sdbcProveDen.Columns(1).Value = sdbcProveCod.Text
        
        RespetarComboProve = False
        ProveedorSeleccionado
    End If
    Screen.MousePointer = vbNormal

End Sub

Private Sub stabGeneral_Click(PreviousTab As Integer)
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer

If sTabGeneral.Tab = 1 Or sTabGeneral.Tab = 2 Then
    
    If bRMat Then
            If oGMN4Seleccionado Is Nothing And oGMN3Seleccionado Is Nothing And oGMN2Seleccionado Is Nothing And oGMN1Seleccionado Is Nothing Then
                sTabGeneral.Tab = 0
                Exit Sub
            End If
            
            If Not oGMN4Seleccionado Is Nothing Then Exit Sub
            
            If Not oGMN3Seleccionado Is Nothing Then
                Set oICompAsignado = oGMN3Seleccionado
             
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig = 0 Or iNivelAsig > 3 Then
                    sTabGeneral.Tab = 0
                    Exit Sub
                End If
            End If
                
            If Not oGMN2Seleccionado Is Nothing Then
                Set oICompAsignado = oGMN2Seleccionado
              
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig = 0 Or iNivelAsig > 2 Then
                    sTabGeneral.Tab = 0
                    Exit Sub
                End If
                Exit Sub
            End If
            
            If Not oGMN1Seleccionado Is Nothing Then
                Set oICompAsignado = oGMN1Seleccionado
               
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig < 1 Or iNivelAsig > 2 Then
                    sTabGeneral.Tab = 0
                    Exit Sub
                End If
                Exit Sub
            End If
            
        End If
Else
            
        If Accion = accionessummit.ACCEqpPorProveMod Then
            sTabGeneral.Tab = 1
'        Else
'            sdbcProveCod.Text = ""
'            sdbcProveDen.Text = ""
'            Set oProveSeleccionado = Nothing
        End If
        
        
End If
End Sub
Private Sub Form_Unload(Cancel As Integer)

Set oEqps = Nothing
Set oProves = Nothing
Set oProveSeleccionado = Nothing
Set oGMN1Seleccionado = Nothing
Set oGMN2Seleccionado = Nothing
Set oGMN3Seleccionado = Nothing
Set oGMN4Seleccionado = Nothing
Set oGruposMN1 = Nothing
Set oGruposMN2 = Nothing
Set oGruposMN3 = Nothing
Set oGruposMN4 = Nothing
Set oEqpSeleccionado = Nothing
End Sub


Private Sub sdbcProveCod_Change()

    If Not RespetarComboProve Then
    
        RespetarComboProve = True
        sdbcProveDen.Text = ""
        Set oProveSeleccionado = Nothing
        RespetarComboProve = False
               
    End If
    
End Sub

Private Sub sdbcProveDen_Change()
    
    If Not RespetarComboProve Then
    
        RespetarComboProve = True
        sdbcProveCod.Text = ""
        RespetarComboProve = False
        
        Set oProveSeleccionado = Nothing
                
    End If
    
End Sub



Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
     
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado
            
End Sub

Private Sub sdbcProveCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer


    sdbcProveCod.RemoveAll

    Screen.MousePointer = vbHourglass
    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod.Text), Trim(sdbcProveDen), , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False

    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProveDen_CloseUp()

    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado
    
End Sub

Private Sub sdbcProveDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer


    sdbcProveDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod), Trim(sdbcProveDen), , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , True, , , , False

    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If
    
    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub ProveedorSeleccionado()
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
    If Not oProveSeleccionado Is Nothing Then
     
        Set oIEquiposAsignados = oProveSeleccionado
            
    End If
    
End Sub
Public Sub GMN1Seleccionado()
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer

    sdbcGMN2_4Cod.RemoveAll
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN2_4Den.RemoveAll
    sdbcGMN2_4Cod.Text = ""
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcGMN2_4Den.Text = ""
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    If bRMat Then
    Set oICompAsignado = oGMN1Seleccionado
               
    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    If iNivelAsig < 1 Or iNivelAsig > 2 Then
        cmdObtener.Enabled = False
    Else
        cmdObtener.Enabled = True
    End If
    Set oICompAsignado = Nothing
    End If
    
    sTabGeneral.Enabled = True
    
    
End Sub

Public Sub GMN2Seleccionado()
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer

    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    
    If bRMat Then
    Set oICompAsignado = oGMN2Seleccionado
    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    If iNivelAsig = 0 Or iNivelAsig > 2 Then
        cmdObtener.Enabled = False
    Else
        cmdObtener.Enabled = True
    End If
    Set oICompAsignado = Nothing
    End If
    
    sTabGeneral.Enabled = True
    
End Sub
Public Sub GMN3Seleccionado()
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer

    Set oGMN3Seleccionado = Nothing
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    
    If bRMat Then
        Set oICompAsignado = oGMN3Seleccionado
        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        If iNivelAsig = 0 Or iNivelAsig > 3 Then
            cmdObtener.Enabled = False
        Else
            cmdObtener.Enabled = True
        End If
        Set oICompAsignado = Nothing
    End If
    
    sTabGeneral.Enabled = True
    
End Sub
Public Sub GMN4Seleccionado()
    
    Set oGMN4Seleccionado = Nothing
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
    sTabGeneral.Enabled = True
    If bRMat Then cmdObtener.Enabled = True
    
End Sub
Private Sub ConfigurarNombres()

    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"

End Sub

Public Sub PonerMatSeleccionadoEnCombos()
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If
    
           
End Sub

Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> Administrador Then
        
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.EQPPorPROVERestEquipo)) Is Nothing) Then
        bREqp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.EQPPorPROVERestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
End If

End Sub

Private Sub cmdBuscar_Click()


'*********
    frmPROVEBuscar.sOrigen = "LstPROVEEqpPorProve"
    frmPROVEBuscar.CodGMN1 = Trim(sdbcGMN1_4Cod)
    frmPROVEBuscar.CodGMN2 = Trim(sdbcGMN2_4Cod)
    frmPROVEBuscar.CodGMN3 = Trim(sdbcGMN3_4Cod)
    frmPROVEBuscar.codGMN4 = Trim(sdbcGMN4_4Cod)
    If Not oEqpSeleccionado Is Nothing Then
        frmPROVEBuscar.codEqp = oEqpSeleccionado.Cod
    End If
    
    frmPROVEBuscar.Show 1
'*********
        
    
        
End Sub

Private Sub cmdObtener_Click()
   ''' * Objetivo: Obtener un listado de Equipos por Proveedor
     
   ''' * Restricci�n de material se obliga a seleccionar el material asociado, y se pasa seleccion de distintas tablas en la SQLQUERY
   ''' * Restricci�n de EQUIPO, han de salir todos los proveedores asociados al material , no se ven los equipos distintos al del comprador
      
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    Dim SelectionTextRpt(1 To 2, 1 To 12) As String
    Dim sSQLQueryStringRpt As String
    ' ARRAY SelectionTextRpt FORMULA FIELDS para SUBREPORT
    Dim SelectionTextSubRpt(1 To 2, 1 To 5) As String
    Dim oReport As Object
    Dim oCREqpPorProve As CREqpPorProve
    Dim pv As Preview
    Dim i As Integer
    Dim txtEstMat As String
    Dim RepPath As String
    Dim SubListado As CRAXDRT.Report
    Dim oFos As FileSystemObject
    Dim codGMN1_4 As String
    Dim codGMN2_4 As String
    Dim codGMN3_4 As String
    Dim codGMN4_4 As String
    Dim bRestEqp  As Boolean
    Dim bOcultarContactos As Boolean
    Dim bOcultarHomologacion As Boolean
    
    Set oCREqpPorProve = GenerarCREqpPorProve
    
    If crs_Connected = False Then
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass

    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptEQPporProve.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    codGMN1_4 = ""
    codGMN2_4 = ""
    codGMN3_4 = ""
    codGMN4_4 = ""
    SelectionTextRpt(2, 1) = "SEL"
    txtEstMat = ""
    If optTodos = True Then
        If Not oGMN1Seleccionado Is Nothing Then
            txtEstMat = sIdiMaterial & ": " & sdbcGMN1_4Cod
            codGMN1_4 = sdbcGMN1_4Cod
        End If
        If Not oGMN2Seleccionado Is Nothing Then
            txtEstMat = txtEstMat & " - " & sdbcGMN2_4Cod
            codGMN2_4 = sdbcGMN2_4Cod
        End If
        If Not oGMN3Seleccionado Is Nothing Then
            txtEstMat = txtEstMat & " - " & sdbcGMN3_4Cod
            codGMN3_4 = sdbcGMN3_4Cod
        End If
        If Not oGMN4Seleccionado Is Nothing Then
            txtEstMat = txtEstMat & " - " & sdbcGMN4_4Cod
            codGMN4_4 = sdbcGMN4_4Cod
        End If
        If txtEstMat <> "" Then
            SelectionTextRpt(1, 1) = txtEstMat
        End If
    Else
        SelectionTextRpt(1, 1) = sIdiEqpProve & " " & sdbcProveDen.Text
    End If
    
    '*********** FORMULA FIELDS REPORT
        SelectionTextRpt(2, 2) = "OcultarDatos"
        If chkDatosBasicos.Value = vbUnchecked Then
            SelectionTextRpt(1, 2) = "S"
        Else
            SelectionTextRpt(1, 2) = "N"
        End If
        
        SelectionTextRpt(2, 3) = "OcultarContactos"
        If chkContactos.Value = vbUnchecked Then
            bOcultarContactos = True
            SelectionTextRpt(1, 3) = "S"
        Else
            bOcultarContactos = False
            SelectionTextRpt(1, 3) = "N"
        End If
        
        SelectionTextRpt(2, 4) = "OcultarHomologacion"
        If Me.chkCalifica = vbUnchecked Then
            SelectionTextRpt(1, 4) = "S"
            bOcultarHomologacion = True
            
        Else
            SelectionTextRpt(1, 4) = "N"
            bOcultarHomologacion = False
        End If
        
        'SelectionTextRpt(2, 5) = "SELMAT"
        'SelectionTextRpt(1, 5) = txtEstMat
        SelectionTextRpt(2, 5) = "txtTITULO"
        SelectionTextRpt(1, 5) = srIdiTitulo
        SelectionTextRpt(2, 6) = "txtSELECCION"
        SelectionTextRpt(1, 6) = srIdiSeleccion
        SelectionTextRpt(2, 7) = "txtPROVEEDOR"
        SelectionTextRpt(1, 7) = srIdiProveedor
        SelectionTextRpt(2, 8) = "txtNIF"
        SelectionTextRpt(1, 8) = srIdiNIF & ":"
        SelectionTextRpt(2, 9) = "txtDIRECCION"
        SelectionTextRpt(1, 9) = srIdiDireccion & ":"
        SelectionTextRpt(2, 10) = "txtMONEDA"
        SelectionTextRpt(1, 10) = srIdiMoneda & ":"
        SelectionTextRpt(2, 11) = "txtHOMOLOGACION"
        SelectionTextRpt(1, 11) = srIdiHomologacion & ":"
        SelectionTextRpt(2, 12) = "txtPAG"
        SelectionTextRpt(1, 12) = srIdiPag
        
    '*********** FORMULA FIELDS SUBREPORT
        SelectionTextSubRpt(2, 1) = "CAL1NOM"
        SelectionTextSubRpt(1, 1) = gParametrosGenerales.gsDEN_CAL1 & ":"
        SelectionTextSubRpt(2, 2) = "CAL2NOM"
        SelectionTextSubRpt(1, 2) = gParametrosGenerales.gsDEN_CAL2 & ":"
        SelectionTextSubRpt(2, 3) = "CAL3NOM"
        SelectionTextSubRpt(1, 3) = gParametrosGenerales.gsDEN_CAL3 & ":"
        SelectionTextSubRpt(2, 4) = "EQP"
        If bREqp Then
            SelectionTextSubRpt(1, 4) = basOptimizacion.gCodEqpUsuario
        Else
            SelectionTextSubRpt(1, 4) = ""
        End If
        SelectionTextSubRpt(2, 5) = "Equipo"
        SelectionTextSubRpt(1, 5) = sIdiEqp
           
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next i
    If Me.chkCalifica = vbChecked Then
        ' OCULTAR HOMOLOGACION = N
        Set SubListado = oReport.OpenSubreport("rptCAL1")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 1))).Text = """" & SelectionTextSubRpt(1, 1) & """"
        Set SubListado = oReport.OpenSubreport("prtCAL2")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 2))).Text = """" & SelectionTextSubRpt(1, 2) & """"
        Set SubListado = oReport.OpenSubreport("rptCAL3")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 3))).Text = """" & SelectionTextSubRpt(1, 3) & """"
    End If
    
    If Me.chkContactos = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptCON")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCONTACTOS")).Text = """" & srIdiContactos & ":" & """"
    End If
       
    Set SubListado = oReport.OpenSubreport("rptEQP")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 4))).Text = """" & SelectionTextSubRpt(1, 4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 5))).Text = """" & SelectionTextSubRpt(1, 5) & """"
    Set SubListado = Nothing
    
    If chkRestEqp.Visible = True And chkRestEqp.Enabled = True And chkRestEqp.Value = vbUnchecked Then
        bRestEqp = True
    Else
        bRestEqp = False
    End If
    
    oCREqpPorProve.ListadoEqpPorProve oReport, codGMN4_4, codGMN3_4, codGMN2_4, codGMN1_4, bRestEqp, basOptimizacion.gCodEqpUsuario, sdbcProveCod, optProve, OptOrdCod, bOcultarContactos, bOcultarHomologacion
 
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.caption = sIdiTitulo
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oCREqpPorProve = Nothing

    Unload frmESPERA
    Unload Me
    
End Sub



Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmLstPROVEEqpPorProve"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Show 1
End Sub


Private Sub Timer1_Timer()
  If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub


Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPROVE_EQPPORPROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        sTabGeneral.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sTabGeneral.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sTabGeneral.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        chkDatosBasicos.caption = Ador(0).Value '5
        Ador.MoveNext
        chkContactos.caption = Ador(0).Value
        Ador.MoveNext
        chkCalifica.caption = Ador(0).Value
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        optProve.caption = Ador(0).Value
        sIdiEqpProve = Ador(0).Value & ":"
        Ador.MoveNext
        chkRestEqp.caption = Ador(0).Value '10
        Ador.MoveNext
        OptOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        optOrdDen.caption = Ador(0).Value '12
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value '15
        Ador.MoveNext
        sIdiMaterial = Ador(0).Value
        Frame2.caption = Ador(0).Value
        Ador.MoveNext
        sIdiEqp = Ador(0).Value
        'Ador.MoveNext
        'sIdiEqpProve = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value '20
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value
        'idioma del rpt
        Ador.MoveNext
        srIdiTitulo = Ador(0).Value '200
        Ador.MoveNext
        srIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        srIdiProveedor = Ador(0).Value
        Ador.MoveNext
        srIdiNIF = Ador(0).Value
        Ador.MoveNext
        srIdiDireccion = Ador(0).Value
        Ador.MoveNext
        srIdiMoneda = Ador(0).Value '205
        Ador.MoveNext
        srIdiHomologacion = Ador(0).Value
        Ador.MoveNext
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        Ador.MoveNext
        srIdiContactos = Ador(0).Value
        
        Ador.Close
    
    End If


   Set Ador = Nothing



End Sub


