VERSION 5.00
Begin VB.UserControl PedidoSelector 
   BackColor       =   &H80000005&
   BackStyle       =   0  'Transparent
   ClientHeight    =   1365
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   1665
   ScaleHeight     =   1365
   ScaleWidth      =   1665
   Begin VB.CheckBox Check1 
      Caption         =   "S"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   0
      Width           =   315
   End
   Begin VB.Menu mnuSelector 
      Caption         =   "Selector"
      Begin VB.Menu mnuSinPedidos 
         Caption         =   "Sin pedidos"
      End
      Begin VB.Menu mnuConPedidos 
         Caption         =   "Con pedidos"
      End
      Begin VB.Menu mnuExcedidaAdj 
         Caption         =   "Excedida adj"
      End
      Begin VB.Menu mnuTodos 
         Caption         =   "Todos"
      End
   End
End
Attribute VB_Name = "PedidoSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum PESSeleccion
    
    PESSinPedidos = 0
    PESConPedidos = 1
    PESExcedidaAdj = 2
    PESTodos = 3
    
End Enum

Private bShow As Boolean
Private mvarAbrevSinPedidos As String
Private mvarAbrevConPedidos As String
Private mvarAbrevExcedidaAdj As String
Private mvarAbrevTodos As String

Private iSeleccion As PESSeleccion

Public Event Click(ByVal Opcion As PESSeleccion)

Public Property Get Seleccion() As Integer
    Seleccion = iSeleccion
End Property
Public Property Let Seleccion(ByVal iSel As Integer)
    
    Select Case iSel
    
        Case 0
                Check1.Caption = mvarAbrevSinPedidos
                iSeleccion = 0
        Case 1
                Check1.Caption = mvarAbrevConPedidos
                iSeleccion = 1
        Case 2
                Check1.Caption = mvarAbrevExcedidaAdj
                iSeleccion = 2
        Case 3
                Check1.Caption = mvarAbrevTodos
                iSeleccion = 3
    End Select
            

Check1.Value = vbUnchecked
Check1.Refresh
        
End Property

Private Sub Check1_Click()
    
    If bShow Then
        PopupMenu mnuSelector
    End If
    
End Sub
Private Sub mnuSinPedidos_Click()
    bShow = True
    iSeleccion = PESSinPedidos
    Check1.Value = vbUnchecked
    Check1.Caption = mvarAbrevSinPedidos
    bShow = True
    RaiseEvent Click(PESSinPedidos)
End Sub

Private Sub mnuConPedidos_Click()
    bShow = True
    iSeleccion = PESConPedidos
    Check1.Value = vbUnchecked
    Check1.Caption = mvarAbrevConPedidos
    RaiseEvent Click(PESConPedidos)
    
End Sub
Private Sub mnuExcedidaAdj_Click()
    bShow = True
    iSeleccion = PESExcedidaAdj
    Check1.Value = vbUnchecked
    Check1.Caption = mvarAbrevExcedidaAdj
    bShow = True
    RaiseEvent Click(PESExcedidaAdj)
End Sub
Private Sub mnuTodos_Click()
    bShow = True
    iSeleccion = PESTodos
    Check1.Value = vbUnchecked
    Check1.Caption = mvarAbrevTodos
    RaiseEvent Click(PESTodos)
    
End Sub
Private Sub UserControl_Initialize()
    
    bShow = True
    iSeleccion = 0
    
End Sub

Private Sub UserControl_LostFocus()
    
    If Check1.Value = vbChecked Then
        bShow = False
        Check1.Value = vbUnchecked
    End If
    
End Sub
Public Property Let DenParaSinPedidos(ByVal Den As String)
    
    mnuSinPedidos.Caption = Den
    
End Property
Public Property Let AbrevParaSinPedidos(ByVal Den As String)
    
    mvarAbrevSinPedidos = Den
    
End Property


Public Property Let DenParaConPedidos(ByVal Den As String)
    
    mnuConPedidos.Caption = Den
    
End Property
Public Property Let AbrevParaConPedidos(ByVal Den As String)
    
    mvarAbrevConPedidos = Den
    
End Property

Public Property Let DenParaExcedidaAdj(ByVal Den As String)
    
    mnuExcedidaAdj.Caption = Den
    
End Property
Public Property Let AbrevParaExcedidaAdj(ByVal Den As String)
    
    mvarAbrevExcedidaAdj = Den
    
End Property

Public Property Let DenParaTodos(ByVal Den As String)
    
    mnuTodos.Caption = Den
   
    
End Property
Public Property Let AbrevParaTodos(ByVal Den As String)
    
    mvarAbrevTodos = Den
    
End Property
