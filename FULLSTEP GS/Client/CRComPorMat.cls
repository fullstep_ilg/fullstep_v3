VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRComPorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub Listado(oReport As CRAXDRT.Report, Parametros() As String, Optional ByVal OrdenPorDen As Boolean, Optional ByVal GMN1Cod As String, Optional ByVal GMN2Cod As String, Optional ByVal GMN3Cod As String, Optional ByVal GMN4Cod As String, Optional ByVal bREqp As Boolean, Optional ByVal Eqp As String)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim j As Integer
    Dim SubReportOrd() As String
    Dim SubRecordSelFormula As String
    Dim GroupSelFormula As String

    Dim Srpt As CRAXDRT.Report
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    oReport.ParameterFields(crs_ParameterIndex(oReport, "@EQP")).SetCurrentValue Parametros(1), 12
    oReport.ParameterFields(crs_ParameterIndex(oReport, "@COM")).SetCurrentValue Parametros(2), 12
    oReport.ParameterFields(crs_ParameterIndex(oReport, "@ORDEN")).SetCurrentValue Parametros(3), 12
    oReport.EnableParameterPrompting = False
    
    If GMN1Cod <> "" Then
        GroupSelFormula = "{MAT_RESTR;1.COD1}='" & GMN1Cod & "'"
        If GMN2Cod <> "" Then
            GroupSelFormula = GroupSelFormula & " and {MAT_RESTR;1.COD2}='" & GMN2Cod & "'"
            If GMN3Cod <> "" Then
                GroupSelFormula = GroupSelFormula & " and {MAT_RESTR;1.COD3}='" & GMN3Cod & "'"
                If GMN4Cod <> "" Then
                     GroupSelFormula = GroupSelFormula & " and {MAT_RESTR;1.COD4}='" & GMN4Cod & "'"
                End If
            End If
        End If
    End If
    oReport.RecordSelectionFormula = GroupSelFormula

    ' formula field para Clasificación grupos
    For i = 1 To 3
        oReport.FormulaFields(crs_FormulaIndex(oReport, "ORD_M" & i)).Text = "{MAT_RESTR;1." & Parametros(3) & i & "}"
    Next i
    'oreport.RecordSortFields.Add oreport.Database.Tables(crs_Tableindex(oreport, crs_SortTable("+{MAT_RESTR;1." & Parametros(3) & "4}"))).Fields(crs_FieldIndex(oreport, crs_SortTable("+{MAT_RESTR;1." & Parametros(3) & "4}"), crs_SortField("+{MAT_RESTR;1." & Parametros(3) & "4}"))), crs_SortDirection("+{MAT_RESTR;1." & Parametros(3) & "4}")
    
        
    If bREqp Then SubRecordSelFormula = "{COM.EQP}='" & DblQuote(Eqp) & "'"
        
    If Not OrdenPorDen Then
       ReDim SubReportOrd(1 To 2) As String
       SubReportOrd(1) = "{COM.EQP}"
       SubReportOrd(2) = "+{COM.COD}"
    Else
       ReDim SubReportOrd(1 To 3) As String
       SubReportOrd(1) = "{EQP.DEN}"
       SubReportOrd(2) = "+{COM.APE}"
       SubReportOrd(3) = "+{COM.NOM}"
    End If

    For i = 1 To 4
        Set Srpt = Nothing
        Set Srpt = oReport.OpenSubreport("COM_NIVEL" & i)
        
        For Each Table In Srpt.Database.Tables
            Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        Next
        
        While Srpt.RecordSortFields.Count > 0
            Srpt.RecordSortFields.Delete 1
        Wend
             
        If SubRecordSelFormula <> "" Then
             Srpt.RecordSelectionFormula = Srpt.RecordSelectionFormula & " AND " & SubRecordSelFormula
        End If
      
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "ORD_SUB")).Text = SubReportOrd(1)
        For j = 2 To UBound(SubReportOrd)
            Srpt.RecordSortFields.Add Srpt.Database.Tables(crs_Tableindex(Srpt, crs_SortTable(SubReportOrd(j)))).Fields(crs_FieldIndex(Srpt, crs_SortTable(SubReportOrd(j)), crs_SortField(SubReportOrd(j)))), crs_SortDirection(SubReportOrd(j))
        Next j
    Next i
        

End Sub


