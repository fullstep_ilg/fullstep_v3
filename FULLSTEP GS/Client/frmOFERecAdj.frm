VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmOFERecAdj 
   BackColor       =   &H00808000&
   Caption         =   "Archivos adjuntos"
   ClientHeight    =   3570
   ClientLeft      =   705
   ClientTop       =   3975
   ClientWidth     =   7695
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmOFERecAdj.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3570
   ScaleWidth      =   7695
   Begin VB.Frame FrameCargando 
      Caption         =   "DGuardando archivo/s adjunto/s"
      Height          =   2535
      Left            =   870
      TabIndex        =   11
      Top             =   570
      Visible         =   0   'False
      Width           =   6015
      Begin VB.Frame Frame2 
         Caption         =   "Darchivo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   120
         TabIndex        =   15
         Top             =   300
         Width           =   5730
         Begin MSComctlLib.ProgressBar ProgressBar2 
            Height          =   315
            Left            =   120
            TabIndex        =   16
            Top             =   540
            Width           =   5475
            _ExtentX        =   9657
            _ExtentY        =   556
            _Version        =   393216
            Appearance      =   1
         End
         Begin VB.Label lblContacto 
            Appearance      =   0  'Flat
            ForeColor       =   &H00800000&
            Height          =   225
            Left            =   120
            TabIndex        =   17
            Top             =   270
            Width           =   5415
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "DTotal del proceso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   120
         TabIndex        =   12
         Top             =   1305
         Width           =   5730
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   315
            Left            =   120
            TabIndex        =   13
            Top             =   600
            Width           =   5460
            _ExtentX        =   9631
            _ExtentY        =   556
            _Version        =   393216
            Appearance      =   1
            Max             =   12
         End
         Begin VB.Label lblDetalle 
            Appearance      =   0  'Flat
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   120
            TabIndex        =   14
            Top             =   300
            Width           =   5415
         End
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   5220
      ScaleHeight     =   375
      ScaleWidth      =   2430
      TabIndex        =   5
      Top             =   3060
      Width           =   2435
      Begin VB.CommandButton cmdAbrirEsp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   1920
         Picture         =   "frmOFERecAdj.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdSalvarEsp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   1440
         Picture         =   "frmOFERecAdj.frx":0D2E
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdEliminarEsp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   480
         Picture         =   "frmOFERecAdj.frx":0DAF
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdA�adirEsp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   0
         Picture         =   "frmOFERecAdj.frx":0E35
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdModificarEsp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   960
         Picture         =   "frmOFERecAdj.frx":0E96
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
   End
   Begin VB.CommandButton cmdRestaurar 
      Caption         =   "&Restaurar"
      Height          =   345
      Left            =   120
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3120
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.PictureBox picEspGen 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   0
      ScaleHeight     =   1335
      ScaleWidth      =   7575
      TabIndex        =   3
      Top             =   120
      Width           =   7575
      Begin VB.TextBox txtProceEsp 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   1275
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   0
         Top             =   0
         Width           =   7440
      End
   End
   Begin MSComctlLib.ListView lstvwAdjun 
      Height          =   1335
      Left            =   120
      TabIndex        =   1
      Top             =   1680
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   2355
      View            =   3
      Arrange         =   1
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      HotTracking     =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   0
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fichero"
         Object.Width           =   3233
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Tamanyo"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Comentario"
         Object.Width           =   6920
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdAdjun 
      Left            =   120
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   600
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERecAdj.frx":0FE0
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Archivos adjuntos"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   4
      Top             =   1440
      Width           =   1410
   End
End
Attribute VB_Name = "frmOFERecAdj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oPrecio As CPrecioItem
Public oIBaseDatos As IBaseDatos

Public g_bCancelarEsp As Boolean
Public g_bRespetarCombo As Boolean
Public g_sComentario As String

Public g_bModif As Boolean
Public bImagen As Boolean

Private sayFileNames() As String
Private Accion As AccionesSummit

Private m_sIdioma() As String
Private m_skb As String

Private m_sGeneraAdjunto As String
Private m_sGeneraAdjuntos As String

Public Function HabilitarBotonesInvitado()
' funci�n para ocultar/mostrar los botones si se es invitado
    If frmOFERec.sdbcProceCod.Columns("INVI").Value = "True" Or frmOFERec.sdbcProceDen.Columns("INVI").Value = "True" Then
        cmdA�adirEsp.Visible = False
        cmdEliminarEsp.Visible = False
        cmdModificarEsp.Visible = False
        txtProceEsp.Locked = True
        
    Else
        cmdA�adirEsp.Visible = True
        cmdEliminarEsp.Visible = True
        cmdModificarEsp.Visible = True
        txtProceEsp.Locked = False
    End If


End Function


Private Sub cmdAbrirEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
    
On Error GoTo Cancelar:

    Set Item = lstvwAdjun.selectedItem
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    Else
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
    End If

    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing
    
    Set oAdjun = g_oPrecio.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag))
    Set oAdjun.PrecioItem = g_oPrecio
    Set oAdjun.Oferta = frmOFERec.g_oOfertaSeleccionada
    oAdjun.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oAdjun = Nothing
        Exit Sub
    End If
            
    oAdjun.LeerAdjuntoOferta oAdjun.Id, oAdjun.DataSize, sFileName
        
    'Lanzamos la aplicacion
    ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Set oAdjun = Nothing

End Sub
''' <summary>
''' A�ade un adjunto a la oferta
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: depende del tama�o de fichero</remarks>
Private Sub cmdA�adirEsp_Click()
Dim DataFile As Integer
Dim Fl As Long
Dim Chunk() As Byte
Dim lSize As Long
Dim i As Integer
Dim oAdjun As CAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject

Dim dAvanceEspera As Double
Dim bLleva(9) As Boolean

On Error GoTo Cancelar:

    cmmdAdjun.DialogTitle = m_sIdioma(1)
    cmmdAdjun.Filter = m_sIdioma(2) & "|*.*"
    cmmdAdjun.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    cmmdAdjun.filename = ""
    cmmdAdjun.ShowOpen

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle

    If sFileName = "" Then
        Exit Sub
    End If


    arrFileNames = ExtraerFicheros(sFileName)
    
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.chkProcFich.Visible = False
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    frmPROCEComFich.sOrigen = "frmOFERecAdj"
    g_bCancelarEsp = False
    frmPROCEComFich.Show 1

    If Not g_bCancelarEsp Then
        If UBound(arrFileNames) > 1 Then
            FrameCargando.caption = m_sGeneraAdjuntos
        Else
            FrameCargando.caption = m_sGeneraAdjunto
        End If
    
        Me.ProgressBar1.Max = UBound(arrFileNames)
        Me.ProgressBar2.Max = 100
        
        Me.lblDetalle.caption = ""
        
        FrameCargando.Visible = True
        DoEvents
    
        Me.ProgressBar1.Value = 0
        
        Set oFos = New Scripting.FileSystemObject
        For iFile = 1 To UBound(arrFileNames)
            Me.Frame2.caption = arrFileNames(iFile)
            Me.Frame2.Refresh
                                
            Me.ProgressBar2.Value = 0
            
            For i = 0 To 9
                bLleva(i) = False
            Next
            
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            Set oAdjun = oFSGSRaiz.generar_cadjunto
            Set oAdjun.Oferta = frmOFERec.g_oOfertaSeleccionada
            Set oAdjun.PrecioItem = g_oPrecio
            
            oAdjun.nombre = sFileTitle
            oAdjun.Comentario = g_sComentario
    
            DataFile = 1
            'Abrimos el fichero para lectura binaria
            Open sFileName For Binary Access Read As DataFile
            Fl = LOF(DataFile)    ' Length of data in file
            If Fl = 0 Then
                FrameCargando.Visible = False
                Close DataFile
                Set oAdjun = Nothing
                Exit Sub
            End If
    
            If Fl + frmOFERec.g_oOfertaSeleccionada.AdjunSize > gParametrosGenerales.glMAXADJUN * 1024 Then
                FrameCargando.Visible = False
                oMensajes.TamanoMaximoOfertaSuperado
                Close DataFile
                Set oAdjun = Nothing
                Exit Sub
            End If
    
            ' Se lo asignamos en bloques a la especificacion
            If giChunkSize > Fl Then
                lSize = Fl
            Else
                lSize = giChunkSize
            End If
            ReDim Chunk(lSize - 1)
            Get DataFile, , Chunk()
            teserror = oAdjun.ComenzarEscrituraData
            If teserror.NumError <> TESnoerror Then
                FrameCargando.Visible = False
                basErrores.TratarError teserror
                Set oAdjun = Nothing
                Close DataFile
                Exit Sub
            End If
            
            dAvanceEspera = (lSize * 100) / Fl
                
            Dim sAdjunto As String
            Dim ArrayAdjunto() As String
            Dim oFile As File
            Dim bites As Long
                
            Set oFile = oFos.GetFile(sFileName)
            bites = oFile.Size
            oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
            
            sAdjunto = oAdjun.GrabarAdjuntoOferta(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites)
            
            'Creamos un array, cada "substring" se asignar�
            'a un elemento del array
            ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
            oAdjun.Id = ArrayAdjunto(0)
            oAdjun.DataSize = bites
            
            teserror = oAdjun.FinalizarEscrituraData(Fl)
            If teserror.NumError <> TESnoerror Then
                FrameCargando.Visible = False
                basErrores.TratarError teserror
                Set oAdjun = Nothing
                Close DataFile
                Exit Sub
            End If
    
            Close DataFile
    
            basSeguridad.RegistrarAccion AccionesSummit.ACCRecOfeAnyaAdjun, "Anyo:" & g_oPrecio.Oferta.Anyo & "GMN1:" & g_oPrecio.Oferta.GMN1Cod & "Cod:" & g_oPrecio.Oferta.Proce & "Prove:" & g_oPrecio.Oferta.Prove & "Item:" & g_oPrecio.Id & "Ofe:" & g_oPrecio.Oferta.Num & "Archivo:" & sFileTitle
    
            frmOFERec.g_oOfertaSeleccionada.AdjunSize = frmOFERec.g_oOfertaSeleccionada.AdjunSize + Fl
            g_oPrecio.Adjuntos.Add oAdjun.Id, oAdjun.nombre, oAdjun.DataSize, oAdjun.Comentario
            lstvwAdjun.ListItems.Add , "ESP" & CStr(oAdjun.Id), sFileTitle, , "ESP"
            lstvwAdjun.ListItems.Item("ESP" & CStr(oAdjun.Id)).ToolTipText = oAdjun.Comentario
            lstvwAdjun.ListItems.Item("ESP" & CStr(oAdjun.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb
            lstvwAdjun.ListItems.Item("ESP" & CStr(oAdjun.Id)).ListSubItems.Add , "Com", oAdjun.Comentario
            lstvwAdjun.ListItems.Item("ESP" & CStr(oAdjun.Id)).Tag = oAdjun.Id
            
            Me.ProgressBar2.Value = 100
            ProgresoBarra bLleva
        Next

    End If
    lstvwAdjun.Refresh
    Set oAdjun = Nothing
    
    FrameCargando.Visible = False
    Exit Sub

Cancelar:
    On Error Resume Next
    FrameCargando.Visible = False
    If err.Number <> 32755 Then
        Close DataFile
        Set oAdjun = Nothing
    End If
End Sub

Private Sub cmdEliminarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

On Error GoTo Cancelar:
    
    Set Item = lstvwAdjun.selectedItem

    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(3) & lstvwAdjun.selectedItem.Text) 'sIdiElArchivo & " " & lstvwAdjun.SelectedItem.Text)

        If irespuesta = vbNo Then Exit Sub

        Screen.MousePointer = vbHourglass

        Set oAdjun = g_oPrecio.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag))
        Set oAdjun.Oferta = frmOFERec.g_oOfertaSeleccionada
        Set oAdjun.PrecioItem = g_oPrecio
        Set oIBaseDatos = oAdjun

        teserror = oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCRecOfeAdjunEli, "Anyo:" & g_oPrecio.Oferta.Anyo & "GMN1:" & g_oPrecio.Oferta.GMN1Cod & "Cod:" & g_oPrecio.Oferta.Proce & "Prove:" & g_oPrecio.Oferta.Prove & "Item:" & g_oPrecio.Id & "Ofe:" & g_oPrecio.Oferta.Num & "Archivo:" & lstvwAdjun.selectedItem.Tag
            g_oPrecio.Adjuntos.Remove (CStr(lstvwAdjun.selectedItem.Tag))
            lstvwAdjun.ListItems.Remove (CStr(lstvwAdjun.selectedItem.key))
        End If

        Set oAdjun = Nothing
        Set oIBaseDatos = Nothing
    End If

    Screen.MousePointer = vbNormal

Cancelar:

    Set oAdjun = Nothing
    Set oIBaseDatos = Nothing

End Sub

Private Sub cmdModificarEsp_Click()
    Dim teserror As TipoErrorSummit

    Accion = ACCRecOfeAdjunMod

    If lstvwAdjun.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        'Comprobamos que se pueda editar la oferta
        
        Set g_oPrecio.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag)).Oferta = frmOFERec.g_oOfertaSeleccionada
        Set g_oPrecio.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag)).PrecioItem = g_oPrecio
        Set oIBaseDatos = g_oPrecio.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag))
        teserror = oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oIBaseDatos = Nothing
            Exit Sub
        End If
        
        frmPROCEEspMod.g_sOrigen = "frmOFERecAdj"
        Set frmPROCEEspMod.g_oIBaseDatos = oIBaseDatos
        frmPROCEEspMod.Show 1

        
    End If
    Accion = ACCRecOfeCon
End Sub

Private Sub cmdSalvarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit

On Error GoTo Cancelar:

    Set Item = lstvwAdjun.selectedItem

    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        cmmdAdjun.DialogTitle = m_sIdioma(10)
        cmmdAdjun.CancelError = True
        cmmdAdjun.Filter = m_sIdioma(5) & "|*.*"
        cmmdAdjun.filename = Item.Text
        cmmdAdjun.ShowSave

        sFileName = cmmdAdjun.filename
        sFileTitle = cmmdAdjun.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdioma(3)
            Exit Sub
        End If

        ' Cargamos el contenido en la esp.
        Set oAdjun = g_oPrecio.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag))
        Set oAdjun.Oferta = frmOFERec.g_oOfertaSeleccionada
        Set oAdjun.PrecioItem = g_oPrecio
        teserror = oAdjun.ComenzarLecturaData
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oAdjun = Nothing
            Exit Sub
        End If
        
        oAdjun.LeerAdjuntoOferta oAdjun.Id, oAdjun.DataSize, sFileName
    End If

Cancelar:
    On Error Resume Next

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oAdjun = Nothing
    End If
End Sub

Public Sub AnyadirEspsALista()
Dim oAdj As CAdjunto

    lstvwAdjun.ListItems.clear
    
    For Each oAdj In g_oPrecio.Adjuntos
        lstvwAdjun.ListItems.Add , "ESP" & CStr(oAdj.Id), oAdj.nombre, , "ESP"
        lstvwAdjun.ListItems.Item("ESP" & CStr(oAdj.Id)).ToolTipText = NullToStr(oAdj.Comentario)
        lstvwAdjun.ListItems.Item("ESP" & CStr(oAdj.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oAdj.DataSize / 1024) & " " & m_skb
        lstvwAdjun.ListItems.Item("ESP" & CStr(oAdj.Id)).ListSubItems.Add , "Com", NullToStr(oAdj.Comentario)
        lstvwAdjun.ListItems.Item("ESP" & CStr(oAdj.Id)).Tag = oAdj.Id
    Next
    
End Sub
''' <summary>
''' Funci�n que carga los recursos de la ventana, etiquetas...
''' </summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo:0,1</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OFEREC_ADJ, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim m_sIdioma(1 To 11)
        For i = 1 To 11
            m_sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        lstvwAdjun.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        lstvwAdjun.ColumnHeaders(3).Text = Ador(0).Value
        Ador.MoveNext
        lstvwAdjun.ColumnHeaders(2).Text = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        Ador.MoveNext
        m_sGeneraAdjuntos = Ador(0).Value
        Ador.MoveNext
        m_sGeneraAdjunto = Ador(0).Value
        Ador.MoveNext
        Frame3.caption = Ador(0).Value
        
        Ador.Close
        
        Me.caption = Me.caption & ": " & g_oPrecio.ArticuloCod & " " & g_oPrecio.Descr
        cmdA�adirEsp.ToolTipText = m_sIdioma(7)
        cmdEliminarEsp.ToolTipText = m_sIdioma(8)
        cmdModificarEsp.ToolTipText = m_sIdioma(9)
        cmdSalvarEsp.ToolTipText = m_sIdioma(10)
        cmdAbrirEsp.ToolTipText = m_sIdioma(11)
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Activate()

    If Not g_bModif Then
        txtProceEsp.Locked = True
        cmdA�adirEsp.Visible = False
        cmdEliminarEsp.Visible = False
        cmdModificarEsp.Visible = False
        If Me.Visible Then cmdSalvarEsp.SetFocus
    Else
        txtProceEsp.Locked = False
        cmdA�adirEsp.Visible = True
        cmdEliminarEsp.Visible = True
        cmdModificarEsp.Visible = True
        If Me.Visible Then txtProceEsp.SetFocus
    End If
    
   ' HabilitarBotonesInvitado

End Sub

Private Sub Form_Load()
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
    'Array que contendra los ficheros eliminados
    ReDim sayFileNames(0)
    Accion = ACCRecOfeCon
        
End Sub

Private Sub Form_Resize()
    
    On Error Resume Next
    
    If Me.Height < 2800 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    lstvwAdjun.Height = Me.Height * 0.33
    picEspGen.Height = Me.Height - lstvwAdjun.Height - 1240
    txtProceEsp.Height = picEspGen.Height - 60
    Label1.Top = picEspGen.Top + picEspGen.Height
    lstvwAdjun.Top = Label1.Top + 240
    picEspGen.Width = Me.Width - 200
    lstvwAdjun.Width = Me.Width - 345
    txtProceEsp.Width = lstvwAdjun.Width
    
    lstvwAdjun.ColumnHeaders(1).Width = lstvwAdjun.Width * 0.25
    lstvwAdjun.ColumnHeaders(2).Width = lstvwAdjun.Width * 0.15
    lstvwAdjun.ColumnHeaders(3).Width = lstvwAdjun.Width * 0.59
           
    cmdRestaurar.Top = Me.Height - 840
    picEdit.Top = cmdRestaurar.Top
    picEdit.Left = lstvwAdjun.Width - 2235
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim irespuesta As Integer
Dim i As Integer
Dim FOSFile As Scripting.FileSystemObject
Dim bBorrando As Boolean


    If Accion = AccionesSummit.ACCRecOfeAdjunMod Then
                
        txtProceEsp_Validate (irespuesta)
          
    End If
    
    If txtProceEsp.Text <> "" Then
        g_oPrecio.NumAdjuns = g_oPrecio.NumAdjuns + 1
    Else
        If g_oPrecio.Adjuntos.Count > 0 Then
            g_oPrecio.NumAdjuns = g_oPrecio.Adjuntos.Count
        Else
            g_oPrecio.NumAdjuns = 0
        End If
    End If
    Set g_oPrecio = Nothing
    Set oIBaseDatos = Nothing
        
On Error GoTo Error
    Set FOSFile = New Scripting.FileSystemObject
       
    i = 0
    While i < UBound(sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(sayFileNames(i)) Then
            FOSFile.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    Exit Sub

Error:
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
    
End Sub


Private Sub txtProceEsp_Change()
        
    If Not g_bRespetarCombo Then
        If Accion <> ACCRecOfeAdjunMod Then
            Accion = ACCRecOfeAdjunMod
        End If
    End If
    
End Sub

Private Sub txtProceEsp_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit

    If Accion = ACCRecOfeAdjunMod Then
    
        Screen.MousePointer = vbHourglass
        
        If StrComp(NullToStr(g_oPrecio.ObsAdjun), txtProceEsp.Text, vbTextCompare) <> 0 Then
                
            g_oPrecio.ObsAdjun = StrToNull(txtProceEsp.Text)

            teserror = g_oPrecio.ModificarObsAdjunto
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCRecOfeCon
                Exit Sub
            End If
            
            'Registrar accion
            basSeguridad.RegistrarAccion Accion, "Anyo:" & g_oPrecio.Oferta.Anyo & "GMN1:" & g_oPrecio.Oferta.GMN1Cod & "Cod:" & g_oPrecio.Oferta.Proce & "Prove:" & g_oPrecio.Oferta.Prove & "Item:" & g_oPrecio.Id

        End If
        Screen.MousePointer = vbNormal
        Accion = ACCRecOfeCon
    End If

End Sub

''' <summary>
''' Muestra el progreso del total del guardado
''' </summary>
''' <param name="bLleva">array q indica q percentil ya ha sumado progreso</param>
''' <remarks>Llamada desde: cmdA�adirEsp_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub ProgresoBarra(ByRef bLleva() As Boolean)
    Dim inum As Integer
    Dim bFin As Boolean
    Dim i As Integer
        
    inum = 10
    i = 0
    
    While Not bFin
        If (Me.ProgressBar2.Value > inum) Then
            If Not bLleva(i) Then
                If Me.ProgressBar1.Value + 0.1 > Me.ProgressBar1.Max Then
                    Me.ProgressBar1.Value = Me.ProgressBar1.Max
                Else
                    Me.ProgressBar1.Value = Me.ProgressBar1.Value + 0.1
                End If
                bLleva(i) = True
            End If
        Else
            bFin = True
        End If
        
        inum = inum + 10
        i = i + 1
    Wend
End Sub
