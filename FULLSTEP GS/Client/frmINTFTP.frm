VERSION 5.00
Begin VB.Form frmINTFTP 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DConexi�n FTP para X"
   ClientHeight    =   3705
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8640
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmINTFTP.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3705
   ScaleWidth      =   8640
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   3015
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   8415
      Begin VB.TextBox txtRutaLocalRecibos 
         Height          =   285
         Left            =   2640
         MaxLength       =   256
         TabIndex        =   8
         Top             =   2280
         Width           =   5355
      End
      Begin VB.CommandButton cmdRutaLocalRecibos 
         Height          =   285
         Left            =   8040
         Picture         =   "frmINTFTP.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2280
         Width           =   315
      End
      Begin VB.TextBox txtRutaLocalIntermedia 
         Height          =   285
         Left            =   2640
         MaxLength       =   256
         TabIndex        =   6
         Top             =   1800
         Width           =   5355
      End
      Begin VB.CommandButton cmdRutaLocalIntermedia 
         Height          =   285
         Left            =   8040
         Picture         =   "frmINTFTP.frx":0D71
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1800
         Width           =   315
      End
      Begin VB.TextBox txtPuerto 
         Height          =   285
         Left            =   5400
         MaxLength       =   50
         TabIndex        =   1
         Top             =   360
         Width           =   1245
      End
      Begin VB.TextBox txtDirIP 
         Height          =   285
         Left            =   1320
         MaxLength       =   50
         TabIndex        =   0
         Top             =   360
         Width           =   2685
      End
      Begin VB.CommandButton cmdRutaLocal 
         Height          =   285
         Left            =   8040
         Picture         =   "frmINTFTP.frx":0E30
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1320
         Width           =   315
      End
      Begin VB.TextBox txtRutaLocal 
         Height          =   285
         Left            =   2640
         MaxLength       =   256
         TabIndex        =   4
         Top             =   1320
         Width           =   5355
      End
      Begin VB.TextBox txtPwd 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   5400
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   840
         Width           =   2685
      End
      Begin VB.TextBox txtUsu 
         Height          =   285
         Left            =   1320
         MaxLength       =   50
         TabIndex        =   2
         Top             =   840
         Width           =   2685
      End
      Begin VB.Label lblRutaLocalRecibos 
         BackColor       =   &H00808000&
         Caption         =   "DRuta Local recibos x:"
         ForeColor       =   &H8000000E&
         Height          =   600
         Left            =   120
         TabIndex        =   19
         Top             =   2295
         Width           =   2490
      End
      Begin VB.Label lblRutaLocalIntermedia 
         BackColor       =   &H00808000&
         Caption         =   "DRuta Local Intermedia:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   120
         TabIndex        =   18
         Top             =   1815
         Width           =   2490
      End
      Begin VB.Label lblPuerto 
         BackColor       =   &H00808000&
         Caption         =   "DPuerto:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   4320
         TabIndex        =   17
         Top             =   382
         Width           =   930
      End
      Begin VB.Label LblDirIP 
         BackColor       =   &H00808000&
         Caption         =   "DDirecci�n IP:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   120
         TabIndex        =   16
         Top             =   382
         Width           =   1170
      End
      Begin VB.Label lblRutaLocal 
         BackColor       =   &H00808000&
         Caption         =   "DRuta Local:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   120
         TabIndex        =   15
         Top             =   1335
         Width           =   2490
      End
      Begin VB.Label lblPwd 
         BackColor       =   &H00808000&
         Caption         =   "DPassword:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   4320
         TabIndex        =   14
         Top             =   862
         Width           =   1170
      End
      Begin VB.Label lblUsu 
         BackColor       =   &H00808000&
         Caption         =   "DUsuario:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   120
         TabIndex        =   13
         Top             =   862
         Width           =   1170
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   4365
      TabIndex        =   11
      Top             =   3270
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   3240
      TabIndex        =   10
      Top             =   3270
      Width           =   1005
   End
End
Attribute VB_Name = "frmINTFTP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Public m_bConsulta As Boolean
Public m_sEntidad As String
Public m_sfrmLlamada As String
Private m_sSelCarpeta As String
' variables para las etiquetas de recibos
Private m_sRutaLocalRecibosFSGS As String
Private m_sRutaLocalRecibosERP As String
Public m_sSistemaExterno As String
Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next

    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FTP_INTEGRACION, basPublic.gParametrosInstalacion.gIdioma)

    If Not ador Is Nothing Then
        caption = ador(0).Value & " " & m_sEntidad
        ador.MoveNext
        cmdAceptar.caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value
        ador.MoveNext
        LblDirIP.caption = ador(0).Value & ":"
        ador.MoveNext
        lblPuerto.caption = ador(0).Value & ":"
        ador.MoveNext
        lblUsu = ador(0).Value & ":"
        ador.MoveNext
        lblPwd = ador(0).Value & ":"
        ador.MoveNext
        lblRutaLocal = ador(0).Value & ":"
        ador.MoveNext
        m_sRutaLocalRecibosFSGS = ador(0).Value
        ador.MoveNext
        m_sRutaLocalRecibosERP = ador(0).Value
        ador.MoveNext
        lblRutaLocalIntermedia = ador(0).Value & ":"
        ador.MoveNext
        
    End If

    Set ador = Nothing

End Sub


Private Sub cmdAceptar_Click()

Dim teserror As TipoErrorSummit

 If txtDirIP.Text <> "" And txtPwd.Text = "" _
    And txtUsu.Text <> "" And txtRutaLocal.Text <> "" Then
    Exit Sub
 End If
 
 If txtDirIP.Text = "" Then
    oMensajes.FaltanDatos LblDirIP
    If Me.Visible Then txtDirIP.SetFocus
    Exit Sub
 End If
 

 
 If txtUsu.Text = "" Then
    oMensajes.FaltanDatos lblUsu
    If Me.Visible Then txtUsu.SetFocus
    Exit Sub
 End If
 
 If txtPwd.Text = "" Then
    oMensajes.FaltanDatos lblPwd
    If Me.Visible Then txtPwd.SetFocus
    Exit Sub
 End If
 
 
 
  If Not m_bConsulta Then
    If m_sfrmLlamada = "frmINTDestino" Then
        teserror = frmINTDestino.m_oEntidadInt.ModificarFTPDestino(txtDirIP.Text, txtPuerto.Text, txtUsu.Text, txtPwd.Text, txtRutaLocal, txtRutaLocalRecibos)
    Else
        teserror = frmINTOrigen.m_oEntidadInt.ModificarFTPOrigen(txtDirIP.Text, txtPuerto.Text, txtUsu.Text, txtPwd.Text, txtRutaLocal.Text, txtRutaLocalIntermedia, txtRutaLocalRecibos)
    End If
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    If m_sfrmLlamada = "frmINTDestino" Then
        frmCONFIntegracion.m_oEntidadesInt.CargarTodasLasEntidades (frmINTDestino.m_oEntidadInt.CodERP)
    Else
        frmCONFIntegracion.m_oEntidadesInt.CargarTodasLasEntidades (frmINTOrigen.m_oEntidadInt.CodERP)
    End If
 End If

 Unload Me

 
    

End Sub

Private Sub cmdCancelar_Click()

    Unload Me
    
End Sub

Private Sub cmdRutaLocal_Click()
Dim sBuffer As String
Dim sInicio As String

On Error GoTo Error

sInicio = txtRutaLocal.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, m_sSelCarpeta, sInicio)

If sBuffer <> "" Then
    txtRutaLocal.Text = sBuffer
    
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    txtRutaLocal.Text = ""


End Sub

Private Sub cmdRutaLocalIntermedia_Click()
Dim sBuffer As String
Dim sInicio As String

On Error GoTo Error

sInicio = txtRutaLocalIntermedia.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, m_sSelCarpeta, sInicio)

If sBuffer <> "" Then
    txtRutaLocalIntermedia.Text = sBuffer
    
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    txtRutaLocalIntermedia.Text = ""

End Sub

Private Sub cmdRutaLocalRecibos_Click()
Dim sBuffer As String
Dim sInicio As String

On Error GoTo Error

sInicio = txtRutaLocalRecibos.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, m_sSelCarpeta, sInicio)

If sBuffer <> "" Then
    txtRutaLocalRecibos.Text = sBuffer
    
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    txtRutaLocalRecibos.Text = ""

End Sub

Private Sub Form_Load()

    Me.Height = 4080
    
    CargarRecursos
    
    If m_sfrmLlamada = "frmINTDestino" Then
        txtDirIP = NullToStr(frmINTDestino.m_oEntidadInt.FTP_Ip)
        txtPuerto = NullToStr(frmINTDestino.m_oEntidadInt.ftp_puerto)
        txtUsu = NullToStr(frmINTDestino.m_oEntidadInt.ftp_usu)
        txtPwd = NullToStr(frmINTDestino.m_oEntidadInt.ftp_pwd)
        txtRutaLocal = NullToStr(frmINTDestino.m_oEntidadInt.Destino)
        txtRutaLocalRecibos = NullToStr(frmINTDestino.m_oEntidadInt.DestinoRecibosFSGS)
        txtRutaLocalIntermedia.Visible = False
        cmdRutaLocalIntermedia.Visible = False
        lblRutaLocalIntermedia.Visible = False
        lblRutaLocalRecibos.caption = m_sRutaLocalRecibosFSGS & ":"
    Else
        txtDirIP = NullToStr(frmINTOrigen.m_oEntidadInt.FTP_Ip)
        txtPuerto = NullToStr(frmINTOrigen.m_oEntidadInt.ftp_puerto)
        txtUsu = NullToStr(frmINTOrigen.m_oEntidadInt.ftp_usu)
        txtPwd = NullToStr(frmINTOrigen.m_oEntidadInt.ftp_pwd)
        txtRutaLocal = NullToStr(frmINTOrigen.m_oEntidadInt.OrigenEntidad)
        txtRutaLocalRecibos = NullToStr(frmINTOrigen.m_oEntidadInt.OrigenRecibosERP)
        txtRutaLocalIntermedia = NullToStr(frmINTOrigen.m_oEntidadInt.OrigenIntermedia)
        txtRutaLocalIntermedia.Visible = True
        cmdRutaLocalIntermedia.Visible = True
        lblRutaLocalIntermedia.Visible = True
        lblRutaLocalRecibos.caption = m_sRutaLocalRecibosERP & " " & m_sSistemaExterno & ":"
    End If
    
    If m_bConsulta Then ' En modo consulta todo protegido
        txtDirIP.Enabled = False
        txtPuerto.Enabled = False
        txtUsu.Enabled = False
        txtPwd.Enabled = False
        txtRutaLocal.Enabled = False
        cmdRutaLocal.Enabled = False
        txtRutaLocalIntermedia.Enabled = False
        cmdRutaLocalIntermedia.Enabled = False
        txtRutaLocalRecibos.Enabled = False
        cmdRutaLocalRecibos.Enabled = False
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
    Else
        txtDirIP.Enabled = True
        txtPuerto.Enabled = True
        txtUsu.Enabled = True
        txtPwd.Enabled = True
        txtRutaLocal.Enabled = True
        cmdRutaLocal.Enabled = True
        If m_sfrmLlamada = "frmINTOrigen" Then
            If frmINTOrigen.m_oEntidadInt.TablaIntermedia Then
                txtRutaLocalIntermedia.Enabled = True
                cmdRutaLocalIntermedia.Enabled = True
            Else
                txtRutaLocalIntermedia.Enabled = False
                cmdRutaLocalIntermedia.Enabled = False
            End If
        End If
        txtRutaLocalRecibos.Enabled = True
        cmdRutaLocalRecibos.Enabled = True
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
    End If

End Sub
