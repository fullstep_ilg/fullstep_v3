VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPRESCon5Buscar 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "dCuentas Contables (Buscar)"
   ClientHeight    =   3690
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8475
   Icon            =   "frmPRESCon5Buscar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3690
   ScaleWidth      =   8475
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   4200
      TabIndex        =   6
      Top             =   3300
      Width           =   1125
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2850
      TabIndex        =   5
      Top             =   3300
      Width           =   1125
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      Height          =   645
      Left            =   0
      ScaleHeight     =   585
      ScaleWidth      =   7665
      TabIndex        =   2
      Top             =   120
      Width           =   7725
      Begin VB.TextBox txtDen 
         Height          =   285
         Left            =   3720
         MaxLength       =   50
         TabIndex        =   1
         Top             =   120
         Width           =   3705
      End
      Begin VB.TextBox txtCod 
         Height          =   285
         Left            =   855
         MaxLength       =   15
         TabIndex        =   0
         Top             =   120
         Width           =   1440
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   135
         TabIndex        =   8
         Top             =   180
         Width           =   555
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2400
         TabIndex        =   7
         Top             =   180
         Width           =   1200
      End
   End
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7800
      Picture         =   "frmPRESCon5Buscar.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Cargar"
      Top             =   360
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPresupuestos 
      Height          =   2310
      Left            =   0
      TabIndex        =   4
      Top             =   900
      Width           =   8145
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   6
      stylesets.count =   2
      stylesets(0).Name=   "ActiveRow"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8421376
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPRESCon5Buscar.frx":01D5
      stylesets(1).Name=   "ActiveRowBlue"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8388608
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPRESCon5Buscar.frx":01F1
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Columns.Count   =   6
      Columns(0).Width=   1376
      Columns(0).Caption=   "Partida"
      Columns(0).Name =   "PRES0"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1376
      Columns(1).Caption=   "C�digo1"
      Columns(1).Name =   "PRES1"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1376
      Columns(2).Caption=   "C�digo2"
      Columns(2).Name =   "PRES2"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1376
      Columns(3).Caption=   "C�digo3"
      Columns(3).Name =   "PRES3"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1376
      Columns(4).Caption=   "C�digo4"
      Columns(4).Name =   "PRES4"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   7011
      Columns(5).Caption=   "Denominaci�n"
      Columns(5).Name =   "DEN"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   14367
      _ExtentY        =   4075
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPRESCon5Buscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sOrigen As String
Public m_sPres0 As String


Private oPresN1 As CPresConceptos5Nivel1
Private oPresN2 As CPresConceptos5Nivel2
Private oPresN3 As CPresConceptos5Nivel3
Private oPresN4 As CPresConceptos5Nivel4
Private m_bBajaLog As Boolean


Private Sub cmdAceptar_Click()
'Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim j As Integer
On Error GoTo NoSeEncuentra

    If sdbgPresupuestos.Rows = 0 Then Exit Sub

    If Trim(sdbgPresupuestos.Columns(4).Value) <> "" Then
        j = 4
    Else
        If Trim(sdbgPresupuestos.Columns(3).Value) <> "" Then
            j = 3
        Else
            If Trim(sdbgPresupuestos.Columns(2).Value) <> "" Then
                j = 2
            Else
                If Trim(sdbgPresupuestos.Columns(1).Value) <> "" Then
                    j = 1
                End If
            End If
        End If
    End If
    
    Dim sCod As String
    If j > 0 Then
        scod1 = sdbgPresupuestos.Columns(1).Value & String(basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(sdbgPresupuestos.Columns(1).Value), " ")
        sCod = "PRES1" & scod1
    End If
    
    If j > 1 Then
        scod2 = sdbgPresupuestos.Columns(2).Value & String(basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(sdbgPresupuestos.Columns(2).Value), " ")
        sCod = "PRES2" & scod1 & scod2
    End If
    
    If j > 2 Then
        scod3 = sdbgPresupuestos.Columns(3).Value & String(basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(sdbgPresupuestos.Columns(3).Value), " ")
        sCod = "PRES3" & scod1 & scod2 & scod3
    End If
    If j > 3 Then
        scod4 = sdbgPresupuestos.Columns(4).Value & String(basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(sdbgPresupuestos.Columns(4).Value), " ")
        sCod = "PRES4" & scod1 & scod2 & scod3 & scod4
    End If


    frmPresupuestos5.CargarArbolConNodo sdbgPresupuestos.Columns(0).Value, sCod
    Unload Me
    Exit Sub

NoSeEncuentra:
    'oMensajes.Concepto4Nuevo
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub cmdCargar_Click()

    sdbgPresupuestos.RemoveAll


    Set oPresN1 = oFSGSRaiz.Generar_CPresConceptos5Nivel1
    Set oPresN2 = oFSGSRaiz.Generar_CPresConceptos5Nivel2
    Set oPresN3 = oFSGSRaiz.Generar_CPresConceptos5Nivel3
    Set oPresN4 = oFSGSRaiz.Generar_CPresConceptos5Nivel4
    If m_bBajaLog Then
        oPresN1.CargarPresupuestosConceptos5 m_sPres0, Trim(txtCod.Text), Trim(txtDen.Text), False, True
        oPresN2.CargarPresupuestosConceptos5 m_sPres0, , Trim(txtCod.Text), Trim(txtDen.Text), False, True
        oPresN3.CargarPresupuestosConceptos5 m_sPres0, , , Trim(txtCod.Text), Trim(txtDen.Text), False, True
        oPresN4.CargarPresupuestosConceptos5 m_sPres0, , , , Trim(txtCod.Text), Trim(txtDen.Text), False, True
    Else
        'S�lo busco entre los presupuestos que no esten de baja l�gica
        oPresN1.CargarPresupuestosConceptos5 m_sPres0, Trim(txtCod.Text), Trim(txtDen.Text), False, True, m_bBajaLog
        oPresN2.CargarPresupuestosConceptos5 m_sPres0, , Trim(txtCod.Text), Trim(txtDen.Text), False, True, m_bBajaLog
        oPresN3.CargarPresupuestosConceptos5 m_sPres0, , , Trim(txtCod.Text), Trim(txtDen.Text), False, True, m_bBajaLog
        oPresN4.CargarPresupuestosConceptos5 m_sPres0, , , , Trim(txtCod.Text), Trim(txtDen.Text), False, True, m_bBajaLog

    End If
            
    CargarGrid

End Sub

Private Sub Form_Load()

'    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
'    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    CargarRecursos
    
    PonerFieldSeparator Me
    
    m_bBajaLog = frmPresupuestos5.chkBajaLog.Value
    
    ConfigurarGrid

End Sub

Private Sub CargarGrid()
Dim oPRES1 As CPresConcep5Nivel1
Dim oPRES2 As CPresConcep5Nivel2
Dim oPRES3 As CPresConcep5Nivel3
Dim oPRES4 As CPresConcep5Nivel4

    For Each oPRES1 In oPresN1
       
       sdbgPresupuestos.AddItem oPRES1.Pres0 & Chr(m_lSeparador) & oPRES1.Cod & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oPRES1.Den
    Next
    
    Set oPresN1 = Nothing
    

    For Each oPRES2 In oPresN2
        sdbgPresupuestos.AddItem oPRES2.Pres0 & Chr(m_lSeparador) & oPRES2.Pres1 & Chr(m_lSeparador) & oPRES2.Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oPRES2.Den
    Next
    Set oPresN2 = Nothing
    

    For Each oPRES3 In oPresN3
        sdbgPresupuestos.AddItem oPRES3.Pres0 & Chr(m_lSeparador) & oPRES3.Pres1 & Chr(m_lSeparador) & oPRES3.Pres2 & Chr(m_lSeparador) & oPRES3.Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oPRES3.Den
    Next
    
    Set oPresN3 = Nothing
    
    
    For Each oPRES4 In oPresN4
        sdbgPresupuestos.AddItem oPRES4.Pres0 & Chr(m_lSeparador) & oPRES4.Pres1 & Chr(m_lSeparador) & oPRES4.Pres2 & Chr(m_lSeparador) & oPRES4.Pres3 & Chr(m_lSeparador) & oPRES4.Cod & Chr(m_lSeparador) & oPRES4.Den
    Next
    
    Set oPresN4 = Nothing


End Sub


Private Sub ConfigurarGrid()

    sdbgPresupuestos.RemoveAll
    sdbgPresupuestos.Columns(1).Visible = True
    sdbgPresupuestos.Columns(2).Visible = True
    sdbgPresupuestos.Columns(3).Visible = True
    sdbgPresupuestos.Columns(4).Visible = True
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    Set oPresN1 = Nothing
    Set oPresN2 = Nothing
    Set oPresN3 = Nothing
    Set oPresN4 = Nothing
    m_sPres0 = ""
End Sub

Private Sub sdbgPresupuestos_DblClick()
    
    cmdAceptar_Click
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim sTitulo As String
    Dim sDen As String
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESCon5Buscar, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sTitulo = Ador(0).Value
        Ador.MoveNext
        Me.caption = sTitulo & " " & Ador(0).Value
        Ador.MoveNext
        lblCod.caption = Ador(0).Value
        Ador.MoveNext
        sDen = Ador(0).Value
        lblDen.caption = sDen
        Ador.MoveNext
        sdbgPresupuestos.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPresupuestos.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPresupuestos.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPresupuestos.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPresupuestos.Columns(4).caption = Ador(0).Value
        sdbgPresupuestos.Columns(5).caption = sDen
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
    
    End If

    Ador.Close
    Set Ador = Nothing

End Sub







