VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPROCEItemEsp 
   BackColor       =   &H00808000&
   Caption         =   "DEspecificaciones"
   ClientHeight    =   3555
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7680
   Icon            =   "frmPROCEItemEsp.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3555
   ScaleWidth      =   7680
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtProceEsp 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   7440
   End
   Begin VB.PictureBox picEditEspec 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   5220
      ScaleHeight     =   375
      ScaleWidth      =   2355
      TabIndex        =   5
      Top             =   3100
      Width           =   2360
      Begin VB.CommandButton cmdAbrirEsp 
         Height          =   300
         Left            =   1920
         Picture         =   "frmPROCEItemEsp.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdSalvarEsp 
         Height          =   300
         Left            =   1440
         Picture         =   "frmPROCEItemEsp.frx":0D2E
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdEliminarEsp 
         Height          =   300
         Left            =   480
         Picture         =   "frmPROCEItemEsp.frx":0DAF
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdA�adirEsp 
         Height          =   300
         Left            =   0
         Picture         =   "frmPROCEItemEsp.frx":0E32
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdModificarEsp 
         Height          =   300
         Left            =   960
         Picture         =   "frmPROCEItemEsp.frx":0EA4
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
   End
   Begin VB.CommandButton cmdRestaurar 
      Caption         =   "&Restaurar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   120
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3120
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton cmdListado 
      Caption         =   "&Listado"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1290
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   3120
      Visible         =   0   'False
      Width           =   1005
   End
   Begin MSComctlLib.ListView lstvwEsp 
      Height          =   1335
      Left            =   120
      TabIndex        =   1
      Top             =   1680
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   2355
      View            =   3
      Arrange         =   1
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      HotTracking     =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fichero"
         Object.Width           =   3233
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Tamanyo"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Comentario"
         Object.Width           =   6920
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Fecha"
         Object.Width           =   2868
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   120
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   600
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemEsp.frx":0FEE
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Archivos adjuntos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   240
      Left            =   120
      TabIndex        =   4
      Top             =   1440
      Width           =   1410
   End
End
Attribute VB_Name = "frmPROCEItemEsp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oItem As CItem
Public g_oIBaseDatos As IBaseDatos

Public g_bCancelarEsp As Boolean
Public g_bRespetarCombo As Boolean

Public g_sComentario As String
Private m_sayFileNames() As String

Private Accion As accionessummit

Private m_sIdioma() As String

Public g_iEstado As Integer
Public g_bCerrado As Boolean
Public g_bVisualizarTodos As Boolean

Public g_sOrigen As String
Private m_sEsp As String
Private m_iEspecAdj As Integer
Private m_vFecAct As Variant 'Array con FECACT de los items modificados y del proceso

Private m_sIdiCaption As String
Private m_skb As String


Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub cmdAbrirEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oItem As CItem
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        
    Else
        
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
        
        Screen.MousePointer = vbHourglass
        
        ' Cargamos el contenido en la esp.
        If g_sOrigen = "PROCE_MODIFITEMS" Then
            Set oItem = oFSGSRaiz.Generar_CItem
            Set oItem.proceso = frmPROCE.g_oProcesoSeleccionado
            oItem.Id = frmPROCE.g_arItems(1)
            Set oEsp = oFSGSRaiz.generar_CEspecificacion
            Set oEsp.Item = oItem
            oEsp.Id = lstvwEsp.selectedItem.Tag
        
        Else
            Screen.MousePointer = vbHourglass
            Set oEsp = g_oItem.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        End If
        
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspItem)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oItem = Nothing
            Set oEsp = Nothing
            Exit Sub
        End If
        
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspItem, oEsp.DataSize, sFileName
                
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
    
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
   
    Set oEsp = Nothing
    Set oItem = Nothing
    Screen.MousePointer = vbNormal
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number = 70 Then
        Resume Next
   ElseIf err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "cmdAbrirEsp_Click", err, Erl, Me)
      GoTo Cancelar
      Exit Sub
   End If

End Sub

Private Sub cmdA�adirEsp_Click()
Dim DataFile As Integer
Dim i As Integer
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oItems As CItems
Dim maxId As Integer
Dim Item As MSComctlLib.listItem
Dim vErrores As Variant
Dim bErrores As Boolean
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    cmmdEsp.filename = ""
    cmmdEsp.DialogTitle = m_sIdioma(1)
    cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
    cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    
    cmmdEsp.ShowOpen
    
    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileName = "" Then
        frmPROCE.DesbloquearProceso ("IT")
        Exit Sub
    End If
    
    arrFileNames = ExtraerFicheros(sFileName)
    
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.chkProcFich.Visible = False
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    frmPROCEComFich.sOrigen = "frmPROCEItemEsp"
    frmPROCEComFich.Show 1
    
    If g_sOrigen = "PROCE_MODIFITEMS" Then
    
        Screen.MousePointer = vbHourglass
        Set oItems = oFSGSRaiz.Generar_CItems
        teserror = oItems.AnyadirEspecAMultiplesItems(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_arItems, arrFileNames, g_sComentario, FSGSLibrary.DevolverPathFichTemp)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            frmPROCE.DesbloquearProceso ("IT")
            basErrores.TratarError teserror
            Set oItems = Nothing
            Exit Sub
        End If
        
        bErrores = teserror.Arg2
        If bErrores Then
            vErrores = teserror.Arg1
            oMensajes.ImposibleAnyadirEspecAMultiplesItems vErrores, UBound(frmPROCE.g_arItems)
        End If
        
        Screen.MousePointer = vbNormal
        
        'Buscamos el mayor �ndice de las especificaciones de los �tems
        For iFile = 1 To UBound(arrFileNames)
            For i = 1 To lstvwEsp.ListItems.Count
                Set Item = lstvwEsp.ListItems.Item(i)
                If Item.Tag > maxId Then
                    maxId = lstvwEsp.ListItems.Item(i).Tag
                End If
            Next i
            
            maxId = maxId + 1
            
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
        
            lstvwEsp.ListItems.Add , "ESP" & CStr(maxId), sFileTitle, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(maxId)).ToolTipText = g_sComentario
            lstvwEsp.ListItems.Item("ESP" & CStr(maxId)).ListSubItems.Add , "Tamanyo", ""
            lstvwEsp.ListItems.Item("ESP" & CStr(maxId)).ListSubItems.Add , "Com", g_sComentario
            lstvwEsp.ListItems.Item("ESP" & CStr(maxId)).ListSubItems.Add , "Fec", Date & " " & Time
            lstvwEsp.ListItems.Item("ESP" & CStr(maxId)).Tag = maxId
        Next iFile
        For i = 1 To UBound(frmPROCE.g_arItems)
            basSeguridad.RegistrarAccion accionessummit.ACCProceItemMod, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(frmPROCE.g_arItems(i))
        Next
        
        Accion = ACCProceEspMod
        Set oItems = Nothing
        Screen.MousePointer = vbNormal
        frmPROCE.DesbloquearProceso ("IT")
        lstvwEsp.Refresh

        
        Exit Sub
        
    Else
    
        If Not g_bCancelarEsp Then
        
            Set oFos = New Scripting.FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                Set oEsp = oFSGSRaiz.generar_CEspecificacion
                Set oEsp.Item = g_oItem
                oEsp.nombre = sFileTitle
                oEsp.Comentario = g_sComentario
              
                Screen.MousePointer = vbHourglass
                teserror = oEsp.ComenzarEscrituraData(TipoEspecificacion.EspItem)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    frmPROCE.DesbloquearProceso ("IT")
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Exit Sub
                End If
                
                Dim sAdjunto As String
                Dim ArrayAdjunto() As String
                Dim oFile As File
                Dim bites As Long
                
                Set oFile = oFos.GetFile(sFileName)
                bites = oFile.Size
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                sAdjunto = oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspItem)
                
                'Creamos un array, cada "substring" se asignar�
                'a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oEsp.Id = ArrayAdjunto(0)
                oEsp.DataSize = bites
                
                g_oItem.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , g_oItem, oEsp.Comentario, , , , , , , oEsp.DataSize
                g_oItem.EspAdj = 1
                lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
                
                basSeguridad.RegistrarAccion accionessummit.ACCProceItemMod, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(oEsp.Item.Id)
            Next
        End If
        
        lstvwEsp.Refresh
        Set oEsp = Nothing
        frmPROCE.DesbloquearProceso ("IT")
        Screen.MousePointer = vbNormal
        Exit Sub
    
    End If
    Screen.MousePointer = vbNormal
    
Cancelar:
    Screen.MousePointer = vbNormal
    frmPROCE.DesbloquearProceso ("IT")
     
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        MsgBox err.Description
        Close DataFile
        Set oEsp = Nothing
    End If
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "cmdA�adirEsp_Click", err, Erl, Me)
      GoTo Cancelar
      Exit Sub
   End If

End Sub

Private Sub cmdEliminarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim oItems As CItems
Dim i As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
        
    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(3) & ": " & lstvwEsp.selectedItem.Text)
        If irespuesta = vbNo Then
            Exit Sub
        End If
        
        If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
        frmPROCE.m_udtOrigBloqueo = ModificarItems
        
        Screen.MousePointer = vbHourglass
        
        If g_sOrigen = "PROCE_MODIFITEMS" Then
            Set oItems = oFSGSRaiz.Generar_CItems
            teserror = oItems.ElminarEspecMultiplesItems(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_arItems, lstvwEsp.selectedItem.Tag)
            If teserror.NumError <> TESnoerror Then
                frmPROCE.DesbloquearProceso ("IT")
                basErrores.TratarError teserror
                Screen.MousePointer = vbNormal
                Set oItems = Nothing
                Exit Sub
            End If
            
            For i = 1 To UBound(frmPROCE.g_arItems)
                basSeguridad.RegistrarAccion accionessummit.ACCProceItemEli, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(frmPROCE.g_arItems(i)) & " Esp:" & CStr(lstvwEsp.selectedItem.Tag)
            Next
            
            lstvwEsp.ListItems.Remove (CStr(lstvwEsp.selectedItem.key))
            
            Set oItems = Nothing
            frmPROCE.DesbloquearProceso ("IT")
            Screen.MousePointer = vbNormal
            Exit Sub
        
        
        Else
        
            Set oEsp = g_oItem.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            Set g_oIBaseDatos = oEsp
            
            teserror = g_oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                g_oItem.especificaciones.Remove (CStr(lstvwEsp.selectedItem.Tag))
                If g_oItem.especificaciones.Count = 0 Then
                    If txtProceEsp = "" Then
                        g_oItem.EspAdj = 0
                    End If
                End If
                basSeguridad.RegistrarAccion accionessummit.ACCProceItemEli, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(g_oItem.Id) & " Esp:" & CStr(lstvwEsp.selectedItem.Tag)
                lstvwEsp.ListItems.Remove (CStr(lstvwEsp.selectedItem.key))
            End If
            
            Set oEsp = Nothing
            Set g_oIBaseDatos = Nothing
            
        End If
        Screen.MousePointer = vbNormal
        frmPROCE.DesbloquearProceso ("IT")
    End If
    
Cancelar:
    frmPROCE.DesbloquearProceso ("IT")
    Screen.MousePointer = vbNormal
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Set oItems = Nothing
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "cmdEliminarEsp_Click", err, Erl, Me)
      GoTo Cancelar
      Exit Sub
   End If

End Sub

Private Sub cmdModificarEsp_Click()
Dim teserror As TipoErrorSummit
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    If lstvwEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        
        If g_sOrigen = "PROCE_MODIFITEMS" Then
        
            frmPROCEEspMod.g_sOrigen = "PROCE_MODIFITEMS"
            frmPROCEEspMod.Show 1
        
        Else
            Screen.MousePointer = vbHourglass
            Set g_oIBaseDatos = g_oItem.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            teserror = g_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                frmPROCE.DesbloquearProceso ("IT")
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set g_oIBaseDatos = Nothing
                Exit Sub
            End If
                        
            Screen.MousePointer = vbNormal
            Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
            frmPROCEEspMod.g_sOrigen = "frmPROCEItemEsp"
            frmPROCEEspMod.Show 1
        
            basSeguridad.RegistrarAccion accionessummit.ACCProceItemMod, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(lstvwEsp.selectedItem.Tag)
        
        End If
    
    End If
    
    frmPROCE.DesbloquearProceso ("IT")
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "cmdModificarEsp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub





Private Sub cmdSalvarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oItem As CItem
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        cmmdEsp.DialogTitle = m_sIdioma(6)
        cmmdEsp.Filter = m_sIdioma(7) & "|*.*"
    
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdioma(8)
            Exit Sub
        End If
        
        ' Cargamos el contenido en la esp.
        Screen.MousePointer = vbHourglass
        
        If g_sOrigen = "PROCE_MODIFITEMS" Then
            Set oItem = oFSGSRaiz.Generar_CItem
            Set oItem.proceso = frmPROCE.g_oProcesoSeleccionado
            oItem.Id = frmPROCE.g_arItems(1)
            Set oEsp = oFSGSRaiz.generar_CEspecificacion
            Set oEsp.Item = oItem
            oEsp.Id = lstvwEsp.selectedItem.Tag
        Else
            Set oEsp = g_oItem.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        End If
        
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspItem)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oItem = Nothing
            Set oEsp = Nothing
            Exit Sub
        End If
        
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspItem, oEsp.DataSize, sFileName
    End If
    
Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set oItem = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    Screen.MousePointer = vbNormal
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "cmdSalvarEsp_Click", err, Erl, Me)
      Exit Sub
   End If

End Sub

Public Sub AnyadirEspsALista()
Dim oEsp As CEspecificacion

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lstvwEsp.ListItems.clear
    
    For Each oEsp In g_oItem.especificaciones
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
    
    If g_oItem.especificaciones.Count = 0 And txtProceEsp.Locked = True Then
        cmdSalvarEsp.Enabled = False
        cmdAbrirEsp.Enabled = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "AnyadirEspsALista", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCE_ITEMESP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim m_sIdioma(1 To 13)
        For i = 1 To 13
            m_sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        frmPROCEItemEsp.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(3).Text = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(4).Text = Ador(0).Value
        Ador.MoveNext
        m_sIdiCaption = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(2).Text = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next
If Not m_bActivado Then
    m_bActivado = True
End If
    If Me.Visible Then txtProceEsp.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
    'Array que contendra los ficheros eliminados
    ReDim m_sayFileNames(0)
    Accion = ACCProceCon
    
    If g_sOrigen = "PROCE_MODIFITEMS" Then
        m_sEsp = ""
        m_iEspecAdj = 0
        m_vFecAct = Empty
        Me.caption = m_sIdiCaption
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Resize()
    
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 2800 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    lstvwEsp.Width = Me.Width - 345
    txtProceEsp.Width = lstvwEsp.Width
    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.25
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.13
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.4
    lstvwEsp.ColumnHeaders(4).Width = lstvwEsp.Width * 0.21
    
    lstvwEsp.Height = Me.Height * 0.33
    txtProceEsp.Height = Me.Height - lstvwEsp.Height - 1300
    Label1.Top = txtProceEsp.Top + txtProceEsp.Height + 60
    lstvwEsp.Top = Label1.Top + 240
    
    cmdRestaurar.Top = Me.Height - 800
    cmdListado.Top = cmdRestaurar.Top
    picEditEspec.Top = cmdRestaurar.Top - 50
    picEditEspec.Left = lstvwEsp.Left + lstvwEsp.Width - picEditEspec.Width
    cmdA�adirEsp.ToolTipText = m_sIdioma(9)
    cmdEliminarEsp.ToolTipText = m_sIdioma(10)
    cmdModificarEsp.ToolTipText = m_sIdioma(11)
    cmdSalvarEsp.ToolTipText = m_sIdioma(12)
    cmdAbrirEsp.ToolTipText = m_sIdioma(13)
    
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim irespuesta As Integer
Dim i As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bDescargarFrm Then
    Select Case Accion
    
        Case accionessummit.ACCProceEspMod
                
                txtProceEsp_Validate (irespuesta)
                
                'Volcamos en la grid de �tems los nuevos valores
                If g_sOrigen = "PROCE_MODIFITEMS" Then
                    If txtProceEsp.Text <> "" Or lstvwEsp.ListItems.Count > 0 Then
                        m_iEspecAdj = 1
                    End If
                    frmPROCE.g_bUpdate = False
                    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                        frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                        If txtProceEsp.Text <> "" Then
                            frmPROCE.sdbgItems.Columns("COMEN").Value = txtProceEsp.Text
                        End If
                        frmPROCE.sdbgItems.Columns("ESPECADJ").Value = m_iEspecAdj
                        If Not IsEmpty(m_vFecAct) Then
                            frmPROCE.sdbgItems.Columns("FECACT").Value = m_vFecAct(i + 1)
                        End If
                    Next
                    If Not IsEmpty(m_vFecAct) Then
                        frmPROCE.g_oProcesoSeleccionado.FECACT = m_vFecAct(i + 1)
                    End If
                    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                    frmPROCE.sdbgItems.Update
                    frmPROCE.g_bUpdate = True
                End If
    
    End Select
End If
m_bDescargarFrm = False
    
    g_sOrigen = ""
    
    frmPROCE.DesbloquearProceso ("IT")
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Public Sub ConfigurarSeguridad()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not frmPROCE.g_bModifEsp Then
        cmdA�adirEsp.Enabled = False
        cmdEliminarEsp.Enabled = False
        cmdModificarEsp.Enabled = False
        txtProceEsp.Locked = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub txtProceEsp_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bRespetarCombo Then Exit Sub
    
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    g_bRespetarCombo = True
    
    Accion = ACCProceEspMod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "txtProceEsp_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub txtProceEsp_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit
Dim oItems As CItems
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Accion = ACCProceEspMod Then
    
        Screen.MousePointer = vbHourglass
        
        If g_sOrigen = "PROCE_MODIFITEMS" Then
            
            If StrComp(m_sEsp, txtProceEsp.Text, vbTextCompare) <> 0 Then
                    
                m_sEsp = txtProceEsp.Text
                
                Set oItems = oFSGSRaiz.Generar_CItems
                teserror = oItems.ActualizarEspecificacion(frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, frmPROCE.g_arItems, m_sEsp)
                If teserror.NumError <> TESnoerror Then
                    frmPROCE.DesbloquearProceso ("IT")
                    basErrores.TratarError teserror
                    Cancel = True
                    Accion = ACCProceCon
                    Set oItems = Nothing
                    Exit Sub
                End If
                
                m_vFecAct = teserror.Arg1 'Recojemos las fechas
                
                'Registrar accion
                For i = 1 To UBound(frmPROCE.g_arItems)
                    basSeguridad.RegistrarAccion Accion, "Anyo:" & frmPROCE.g_oProcesoSeleccionado.Anyo & " Gmn1:" & frmPROCE.g_oProcesoSeleccionado.GMN1Cod & " Proce:" & frmPROCE.g_oProcesoSeleccionado.Cod & " Item:" & frmPROCE.g_arItems(i) & " Esp:" & Left(m_sEsp, 50)
                Next
                Accion = ACCProceCon
                
                Set oItems = Nothing
            
            End If
    
        Else
        
            If StrComp(NullToStr(g_oItem.esp), txtProceEsp.Text, vbTextCompare) <> 0 Then
                    
                g_oItem.esp = StrToNull(txtProceEsp.Text)
                If txtProceEsp.Text <> "" Then
                    g_oItem.EspAdj = 1
                Else
                    If g_oItem.especificaciones.Count = 0 Then
                        g_oItem.EspAdj = 0
                    End If
                End If
                
                teserror = g_oItem.ModificarEspecificacion
                If teserror.NumError <> TESnoerror Then
                    frmPROCE.DesbloquearProceso ("IT")
                    basErrores.TratarError teserror
                    Cancel = True
                    Accion = ACCProceCon
                    Exit Sub
                End If
                
                'Registrar accion
                basSeguridad.RegistrarAccion Accion, "Anyo:" & g_oItem.proceso.Anyo & " Gmn1:" & g_oItem.proceso.GMN1Cod & " Proce:" & g_oItem.proceso.Cod & " Item:" & g_oItem.Id & " Esp:" & Left(g_oItem.esp, 50)
                Accion = ACCProceCon
            End If
        
        End If
        
        g_bRespetarCombo = False
        Screen.MousePointer = vbNormal
        
    End If

    frmPROCE.DesbloquearProceso ("IT")
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemEsp", "txtProceEsp_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
