VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstSolConfiguracion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de configuracion"
   ClientHeight    =   2865
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5940
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstSolConfiguracion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2865
   ScaleWidth      =   5940
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab ssTabOpciones 
      Height          =   2280
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   4022
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "DSeleccion"
      TabPicture(0)   =   "frmLstSolConfiguracion.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "DOpciones"
      TabPicture(1)   =   "frmLstSolConfiguracion.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "DOrden"
      TabPicture(2)   =   "frmLstSolConfiguracion.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame1"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame3 
         Height          =   1600
         Left            =   120
         TabIndex        =   15
         Top             =   480
         Width           =   5600
         Begin VB.CommandButton cmdBuscarCarpeta 
            Height          =   285
            Left            =   4950
            Picture         =   "frmLstSolConfiguracion.frx":0D06
            Style           =   1  'Graphical
            TabIndex        =   22
            Top             =   280
            Width           =   315
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   21
            Top             =   280
            Width           =   2900
         End
         Begin VB.CommandButton cmdLimpiarCarpeta 
            Height          =   285
            Left            =   4590
            Picture         =   "frmLstSolConfiguracion.frx":1088
            Style           =   1  'Graphical
            TabIndex        =   20
            Top             =   280
            Width           =   315
         End
         Begin VB.TextBox txtCod 
            Height          =   285
            Left            =   1650
            TabIndex        =   2
            Top             =   1135
            Width           =   2175
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcTipo 
            Height          =   285
            Left            =   1650
            TabIndex        =   1
            Top             =   700
            Width           =   3615
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6985
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6376
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblCarpeta 
            Caption         =   "DCarpeta:"
            Height          =   195
            Left            =   120
            TabIndex        =   19
            Top             =   300
            Width           =   840
         End
         Begin VB.Label lblTipo 
            Caption         =   "DTipo de solicitud:"
            Height          =   255
            Left            =   120
            TabIndex        =   17
            Top             =   715
            Width           =   1335
         End
         Begin VB.Label lblCod 
            Caption         =   "DC�digo:"
            Height          =   195
            Left            =   120
            TabIndex        =   16
            Top             =   1165
            Width           =   840
         End
      End
      Begin VB.Frame Frame2 
         Height          =   1250
         Left            =   -74880
         TabIndex        =   11
         Top             =   480
         Width           =   5600
         Begin VB.CheckBox chkConfProve 
            Caption         =   "DConfiguraci�n espec�fica para proveedores"
            Enabled         =   0   'False
            Height          =   375
            Left            =   3000
            TabIndex        =   18
            Top             =   800
            Width           =   2500
         End
         Begin VB.CheckBox chkPeticionarios 
            Caption         =   "DMostrar peticionarios"
            Height          =   375
            Left            =   120
            TabIndex        =   13
            Top             =   500
            Width           =   2500
         End
         Begin VB.CheckBox chkConfiguracion 
            Caption         =   "DMostrar cumplimentaci�n"
            Height          =   375
            Left            =   120
            TabIndex        =   14
            Top             =   800
            Width           =   2500
         End
         Begin VB.CheckBox chkAdjuntos 
            Caption         =   "DMostrar adjuntos"
            Height          =   375
            Left            =   120
            TabIndex        =   12
            Top             =   200
            Width           =   2500
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1250
         Left            =   -74880
         TabIndex        =   4
         Top             =   480
         Width           =   5600
         Begin VB.OptionButton optOrden 
            Caption         =   "DWorkflow"
            Height          =   255
            Index           =   5
            Left            =   2500
            TabIndex        =   10
            Top             =   850
            Width           =   2000
         End
         Begin VB.OptionButton optOrden 
            Caption         =   "DFormulario"
            Height          =   255
            Index           =   4
            Left            =   2500
            TabIndex        =   9
            Top             =   550
            Width           =   2000
         End
         Begin VB.OptionButton optOrden 
            Caption         =   "DGestor"
            Height          =   255
            Index           =   3
            Left            =   2500
            TabIndex        =   8
            Top             =   250
            Width           =   2000
         End
         Begin VB.OptionButton optOrden 
            Caption         =   "DDenominaci�n"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   7
            Top             =   850
            Width           =   2000
         End
         Begin VB.OptionButton optOrden 
            Caption         =   "DC�digo"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   6
            Top             =   550
            Width           =   2000
         End
         Begin VB.OptionButton optOrden 
            Caption         =   "DTipo"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   5
            Top             =   250
            Value           =   -1  'True
            Width           =   2000
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   0
         Top             =   0
      End
   End
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      Height          =   435
      Left            =   4560
      TabIndex        =   3
      Top             =   2350
      Width           =   1350
   End
End
Attribute VB_Name = "frmLstSolConfiguracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_txtTitulo As String
Private m_txtPag As String
Private m_txtDe As String
Private m_stxtTipo As String
Private m_stxtCodigo As String
Private m_stxtDen As String
Private m_stxtGestor As String
Private m_stxtWorkflow As String
Private m_stxtForm As String
Private m_stxtPub As String
Private m_stxtPedidoDir As String
Private m_txtUO As String
Private m_txtDep As String
Private m_txtPer As String
Private m_txtPet As String
Private m_txtSi As String
Private m_txtNo As String
Private m_stxtSolic As String
Private m_stxtProv As String
Private m_stxtSolCompra(2) As String
Private m_stxtSubFicAdjuntos(5) As String
Private m_stxtSubCumpl(7) As String
Private m_sEspera(3) As String
Private m_bRespetarCombo As Boolean
#If VERSION >= 30600 Then
    'Public m_sNodoCarpetaSelKey As String
    Public m_lIDCarpeta As Variant 'Representa a un Long
    Public m_iNivelCarpeta As Variant 'Representa a un Int
#End If


Private Sub ObtenerListado()
    ''' * Objetivo: Obtener un listado de la configuraci�n de solicitudes
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim SelectionText As String
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim oCRSolicitudes As New CRSolicitudes
    Dim CriterioOrdenacion As TipoOrdenacionSolicit
    Dim sPet As String
    Dim vEstado() As Variant
    Dim i As Integer
    Dim sestado As String
    Dim adoresDG As Ador.Recordset
    Dim adoresSubPet As Ador.Recordset
    Dim adoresSubConf As Ador.Recordset
    Dim SubListado As CRAXDRT.Report
    Dim Table As CRAXDRT.DatabaseTable
    Dim vTipo As Variant
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
   If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptSolConfiguracion.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & m_txtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipo")).Text = """" & m_stxtTipo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = """" & m_stxtCodigo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDen")).Text = """" & m_stxtDen & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGestor")).Text = """" & m_stxtGestor & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtForm")).Text = """" & m_stxtForm & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtWorkflow")).Text = """" & m_stxtWorkflow & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPub")).Text = """" & m_stxtPub & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPedidoDir")).Text = """" & m_stxtPedidoDir & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSi")).Text = """" & m_txtSi & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNo")).Text = """" & m_txtNo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtConf")).Text = """" & m_stxtSubCumpl(1) & """"
    
    'orden del report
    If optOrden(0).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicit.OrdSolicPorTipo
    ElseIf optOrden(1).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicit.OrdSolicPorCod
    ElseIf optOrden(2).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicit.OrdSolicPorDen
    ElseIf optOrden(3).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicit.OrdSolicPorGestor
    ElseIf optOrden(4).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicit.OrdSolicPorForm
    ElseIf optOrden(5).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicit.OrdSolicPorWork
    End If
    
    If sdbcTipo.Visible = False Then
        vTipo = TipoSolicitud.SolicitudCompras
    Else
        If Trim(sdbcTipo.Text) <> "" Then
            vTipo = sdbcTipo.Columns("ID").Value
        Else
            vTipo = Null
        End If
    End If
    
    Set adoresDG = oGestorInformes.ListadoSolicConfiguracion(CriterioOrdenacion, vTipo, Trim(txtCod.Text), m_lIDCarpeta, m_iNivelCarpeta)
    
    If Not adoresDG Is Nothing Then
        oReport.Database.SetDataSource adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    'subReport de peticionarios
    If chkPeticionarios.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRARPET")).Text = """" & "S" & """"
        
        Set SubListado = oReport.OpenSubreport("rptSubPeticionarios")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUO")).Text = """" & m_txtUO & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDep")).Text = """" & m_txtDep & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPer")).Text = """" & m_txtPer & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPeticionarios")).Text = """" & m_txtPet & """"
    
        Set adoresSubPet = oGestorInformes.ListadoPeticionarios(vTipo, Trim(txtCod.Text))
    
        If Not adoresSubPet Is Nothing Then
            SubListado.Database.SetDataSource adoresSubPet
        End If
    
        Set SubListado = Nothing
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRARPET")).Text = """" & "N" & """"
    End If
    
    
    'subReport de Ficheros adjuntos
    If chkAdjuntos.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRARADJUN")).Text = """" & "S" & """"
        
        Set SubListado = oReport.OpenSubreport("rptSolConfAdjuntos")
        For Each Table In SubListado.Database.Tables
            Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        Next
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjuntos")).Text = """" & m_stxtSubFicAdjuntos(1) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNombre")).Text = """" & m_stxtSubFicAdjuntos(2) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPersona")).Text = """" & m_stxtSubFicAdjuntos(3) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtComent")).Text = """" & m_stxtSubFicAdjuntos(4) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecha")).Text = """" & m_stxtSubFicAdjuntos(5) & """"
        
        Set SubListado = Nothing
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRARADJUN")).Text = """" & "N" & """"
    End If
    
    
    'SubReports de la configuraci�n:
    If chkConfiguracion.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRARCONF")).Text = """" & "S" & """"
        
        'El subreport de el solicitante:
        Set SubListado = oReport.OpenSubreport("rptSubCumplimentSolic")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSolicitante")).Text = """" & m_stxtSolic & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGrupo")).Text = """" & m_stxtSubCumpl(2) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDato")).Text = """" & m_stxtSubCumpl(3) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVisible")).Text = """" & m_stxtSubCumpl(4) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEscritura")).Text = """" & m_stxtSubCumpl(5) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtObl")).Text = """" & m_stxtSubCumpl(6) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPaso")).Text = """" & m_stxtSubCumpl(7) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_txtSi & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_txtNo & """"
        
        Set adoresSubConf = oGestorInformes.ListadoCumplimentacion(1, vTipo, Trim(txtCod.Text))
        If Not adoresSubConf Is Nothing Then
            SubListado.Database.SetDataSource adoresSubConf
        End If
        Set SubListado = Nothing
        
        'El subreport de los pasos:
        Set SubListado = oReport.OpenSubreport("rptSubCumpliment")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGrupo")).Text = """" & m_stxtSubCumpl(2) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDato")).Text = """" & m_stxtSubCumpl(3) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVisible")).Text = """" & m_stxtSubCumpl(4) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEscritura")).Text = """" & m_stxtSubCumpl(5) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtObl")).Text = """" & m_stxtSubCumpl(6) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPaso")).Text = """" & m_stxtSubCumpl(7) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_txtSi & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_txtNo & """"
        
        Set adoresSubConf = Nothing
        Set adoresSubConf = oGestorInformes.ListadoCumplimentacion(3, vTipo, Trim(txtCod.Text))
        If Not adoresSubConf Is Nothing Then
            SubListado.Database.SetDataSource adoresSubConf
        End If
        Set SubListado = Nothing
        
        'El subreport de la configuraci�n espec�fica para proveedores:
        If chkConfProve.Value = vbChecked Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRAR_CONF_PROV")).Text = """" & "S" & """"
            
            Set SubListado = oReport.OpenSubreport("rptSubCumplimentProv")
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProv")).Text = """" & m_stxtProv & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGrupo")).Text = """" & m_stxtSubCumpl(2) & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDato")).Text = """" & m_stxtSubCumpl(3) & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtVisible")).Text = """" & m_stxtSubCumpl(4) & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEscritura")).Text = """" & m_stxtSubCumpl(5) & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtObl")).Text = """" & m_stxtSubCumpl(6) & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPaso")).Text = """" & m_stxtSubCumpl(7) & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_txtSi & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_txtNo & """"
            
            Set adoresSubConf = Nothing
            Set adoresSubConf = oGestorInformes.ListadoCumplimentacion(2, vTipo, Trim(txtCod.Text))
            If Not adoresSubConf Is Nothing Then
                SubListado.Database.SetDataSource adoresSubConf
            End If
            Set SubListado = Nothing
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRAR_CONF_PROV")).Text = """" & "N" & """"
        End If
        
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRARCONF")).Text = """" & "N" & """"
    End If
    
    
    Set adoresSubConf = Nothing
    Set adoresDG = Nothing
    Set adoresSubPet = Nothing
    
    
    'Muestra el report:
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = m_txtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.lblGeneral.caption = m_sEspera(1) & " " & pv.caption      'Generando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = m_sEspera(2)  'Seleccionando registros ...
    frmESPERA.lblDetalle = m_sEspera(3)  'Visualizando listado ...
    frmESPERA.Show
    
    Timer1.Enabled = True
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LST_CONFIGURACION, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value       '1 Listado de configuraci�n de solicitudes
        m_txtTitulo = Ador(0).Value
        Ador.MoveNext
        
        chkAdjuntos.caption = Ador(0).Value  '2 Mostrar adjuntos
        Ador.MoveNext
        chkConfiguracion.caption = Ador(0).Value  '3 Mostrar configuraci�n
        Ador.MoveNext
        ssTabOpciones.TabCaption(1) = Ador(0).Value '4 Opciones
        Ador.MoveNext
        ssTabOpciones.TabCaption(2) = Ador(0).Value '5 Orden
        Ador.MoveNext
        optOrden.Item(0).caption = Ador(0).Value  '6 Tipo
        m_stxtTipo = Ador(0).Value
        Ador.MoveNext
        optOrden.Item(1).caption = Ador(0).Value  '7 C�digo
        m_stxtCodigo = Ador(0).Value
        Ador.MoveNext
        optOrden.Item(2).caption = Ador(0).Value  '8 Denominaci�n
        m_stxtDen = Ador(0).Value
        Ador.MoveNext
        optOrden.Item(3).caption = Ador(0).Value  '9 Gestor
        m_stxtGestor = Ador(0).Value
        Ador.MoveNext
        optOrden.Item(4).caption = Ador(0).Value  '10 Formulario
        m_stxtForm = Ador(0).Value
        Ador.MoveNext
        optOrden.Item(5).caption = Ador(0).Value  '11 Workflow
        m_stxtWorkflow = Ador(0).Value
        Ador.MoveNext
        m_sEspera(1) = Ador(0).Value '12
        Ador.MoveNext
        m_sEspera(2) = Ador(0).Value '13
        Ador.MoveNext
        m_sEspera(3) = Ador(0).Value '14
        Ador.MoveNext
        m_txtPag = Ador(0).Value  '15 P�g
        Ador.MoveNext
        m_txtDe = Ador(0).Value   '16 De
        Ador.MoveNext
        m_stxtPub = Ador(0).Value  '17 Publicar
        Ador.MoveNext
        m_stxtPedidoDir = Ador(0).Value  '18 Pedido directo
        Ador.MoveNext
        chkPeticionarios.caption = Ador(0).Value  '19 Mostrar peticionarios
        Ador.MoveNext
        m_txtUO = Ador(0).Value '20 Unidad organizativa
        Ador.MoveNext
        m_txtDep = Ador(0).Value '21 Departamento
        Ador.MoveNext
        m_txtPer = Ador(0).Value '22 Persona
        m_stxtSubFicAdjuntos(3) = Ador(0).Value
        Ador.MoveNext
        m_stxtSolCompra(1) = Ador(0).Value  '23 Otras solicitudes
        Ador.MoveNext
        m_stxtSolCompra(2) = Ador(0).Value  '24 Solicitud de compras
        Ador.MoveNext
        m_txtSi = Ador(0).Value '25 S�
        Ador.MoveNext
        m_txtNo = Ador(0).Value '26 No
        Ador.MoveNext
        m_txtPet = Ador(0).Value '27 Peticionarios
        Ador.MoveNext
        m_stxtSubFicAdjuntos(1) = Ador(0).Value '28 Ficheros adjuntos
        Ador.MoveNext
        m_stxtSubFicAdjuntos(2) = Ador(0).Value '29 Fichero
        Ador.MoveNext
        m_stxtSubFicAdjuntos(4) = Ador(0).Value '30 Comentario
        Ador.MoveNext
        m_stxtSubFicAdjuntos(5) = Ador(0).Value '31 Fecha
        For i = 1 To 7
            Ador.MoveNext
            m_stxtSubCumpl(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_stxtSolic = Ador(0).Value  'Solicitante
        Ador.MoveNext
        m_stxtProv = Ador(0).Value  'Proveedor
        
        Ador.MoveNext
        ssTabOpciones.TabCaption(0) = Ador(0).Value '41 Selecci�n
        Ador.MoveNext
        lblCod.caption = Ador(0).Value  ' 42 C�digo
        Ador.MoveNext
        lblTipo.caption = Ador(0).Value ' 43 Tipo de solicitud:
        Ador.MoveNext
        chkConfProve.caption = Ador(0).Value '44 Conf.espec�fica para proveedores
#If VERSION >= 30600 Then
        Ador.MoveNext
        lblCarpeta.caption = Ador(0).Value ' 45 Carpeta:
#End If
    
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub chkConfiguracion_Click()
    If chkConfiguracion.Value = vbChecked Then
        chkConfProve.Enabled = True
    Else
        chkConfProve.Enabled = False
        chkConfProve.Value = vbUnchecked
    End If
End Sub

Private Sub cmdBuscarCarpeta_Click()
    Dim oNodoSeleccionado As MSComctlLib.node
    Dim oCarpetaSeleccionada As Object
                
'    frmCarpetaSolicitArbol.m_sCarpetaPreSelKey = m_sNodoCarpetaSelKey
    frmCarpetaSolicitArbol.Show vbModal
    Set oNodoSeleccionado = frmCarpetaSolicitArbol.oNodoSeleccionado
    Set oCarpetaSeleccionada = frmCarpetaSolicitArbol.oCarpetaSeleccionada
'    frmCarpetaSolicitArbol.m_sCarpetaPreSelKey = ""
    Set frmCarpetaSolicitArbol.oNodoSeleccionado = Nothing
    Set frmCarpetaSolicitArbol.oCarpetaSeleccionada = Nothing
    Unload frmCarpetaSolicitArbol
          
    If Not oNodoSeleccionado Is Nothing Then
        If Not oCarpetaSeleccionada Is Nothing Then
            m_lIDCarpeta = oCarpetaSeleccionada.Id
            m_iNivelCarpeta = oCarpetaSeleccionada.Nivel
            
        Else
            m_lIDCarpeta = 0
            m_iNivelCarpeta = 0
            
        End If
        txtCarpeta.Text = oNodoSeleccionado.Text
    Else
        m_lIDCarpeta = Null
        m_iNivelCarpeta = Null
        txtCarpeta.Text = ""
    End If
End Sub

Private Sub cmdLimpiarCarpeta_Click()
    txtCarpeta.Text = ""
    
    m_lIDCarpeta = Null
    m_iNivelCarpeta = Null
    
End Sub

Private Sub cmdObtener_Click()
    ObtenerListado
End Sub

Private Sub Form_Load()
    Me.Width = 6030
    Me.Height = 3240
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
        lblTipo.Visible = False
        sdbcTipo.Visible = False
        
        lblCod.Top = (lblCod.Top + lblTipo.Top) / 2
        txtCod.Top = (txtCod.Top + sdbcTipo.Top) / 2
    End If
    
    
    
End Sub


Private Sub sdbcTipo_Click()
    If Not sdbcTipo.DroppedDown Then
        sdbcTipo = ""
    End If
End Sub

Private Sub sdbcTipo_CloseUp()
    If sdbcTipo.Value = "..." Or sdbcTipo.Value = "" Then
        sdbcTipo.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcTipo.Text = sdbcTipo.Columns(1).Text
    m_bRespetarCombo = False
End Sub


Private Sub sdbcTipo_DropDown()
    Dim oTipo As CTipoSolicit
    Dim oTipos As CTiposSolicit
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipo.RemoveAll
    
    Set oTipos = oFSGSRaiz.Generar_CTiposSolicit
    oTipos.CargarTodosTiposSolicitud
    
    For Each oTipo In oTipos
        sdbcTipo.AddItem oTipo.Id & Chr(9) & NullToStr(oTipo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
    Next
    
    sdbcTipo.SelStart = 0
    sdbcTipo.SelLength = Len(sdbcTipo.Text)
    sdbcTipo.Refresh

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcTipo_InitColumnProps()
    sdbcTipo.DataFieldList = "Column 1"
    sdbcTipo.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbcTipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipo.Rows - 1
            bm = sdbcTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcTipo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


