VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmBloqueoFlujos 
   BackColor       =   &H00808000&
   Caption         =   "frmBloqueoFlujos"
   ClientHeight    =   5760
   ClientLeft      =   8475
   ClientTop       =   1260
   ClientWidth     =   13065
   Icon            =   "frmBloqueoFlujos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5760
   ScaleWidth      =   13065
   Begin TabDlg.SSTab SSTabBloqueos 
      Height          =   5535
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   11745
      _ExtentX        =   20717
      _ExtentY        =   9763
      _Version        =   393216
      Style           =   1
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      BackColor       =   8421440
      TabCaption(0)   =   "DBloqueoApertura"
      TabPicture(0)   =   "frmBloqueoFlujos.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblCondParticulares(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblEtapas(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "sdbddtipocondicion(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "ListViewEtapas(0)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "ssdbGridParticulares(0)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdAnyadirCondicionParticular(0)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdEliminarCondicionParticular(0)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "DBloqueoAdjudicaciones"
      TabPicture(1)   =   "frmBloqueoFlujos.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdEliminarCondicionParticular(1)"
      Tab(1).Control(1)=   "cmdAnyadirCondicionParticular(1)"
      Tab(1).Control(2)=   "ListViewEtapas(1)"
      Tab(1).Control(3)=   "ssdbGridParticulares(1)"
      Tab(1).Control(4)=   "sdbddtipocondicion(1)"
      Tab(1).Control(5)=   "lblEtapas(1)"
      Tab(1).Control(6)=   "lblCondParticulares(1)"
      Tab(1).ControlCount=   7
      TabCaption(2)   =   "DBloqueoDirectos"
      TabPicture(2)   =   "frmBloqueoFlujos.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "chkPermPedDirecto"
      Tab(2).Control(1)=   "cmdEliminarCondicionParticular(2)"
      Tab(2).Control(2)=   "cmdAnyadirCondicionParticular(2)"
      Tab(2).Control(3)=   "ListViewEtapas(2)"
      Tab(2).Control(4)=   "ssdbGridParticulares(2)"
      Tab(2).Control(5)=   "sdbddtipocondicion(2)"
      Tab(2).Control(6)=   "lblEtapas(2)"
      Tab(2).Control(7)=   "lblCondParticulares(2)"
      Tab(2).ControlCount=   8
      TabCaption(3)   =   "DBloqueoEP"
      TabPicture(3)   =   "frmBloqueoFlujos.frx":0D06
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "cmdEliminarCondicionParticular(3)"
      Tab(3).Control(1)=   "cmdAnyadirCondicionParticular(3)"
      Tab(3).Control(2)=   "ListViewEtapas(3)"
      Tab(3).Control(3)=   "ssdbGridParticulares(3)"
      Tab(3).Control(4)=   "sdbddtipocondicion(3)"
      Tab(3).Control(5)=   "lblEtapas(3)"
      Tab(3).Control(6)=   "lblCondParticulares(3)"
      Tab(3).ControlCount=   7
      Begin VB.CheckBox chkPermPedDirecto 
         Caption         =   "DNo permitir realizar pedidos hasta que haya finalizado el workflow"
         Height          =   215
         Left            =   -74880
         TabIndex        =   10
         Top             =   480
         Visible         =   0   'False
         Width           =   7000
      End
      Begin VB.CommandButton cmdEliminarCondicionParticular 
         Height          =   315
         Index           =   0
         Left            =   11070
         Picture         =   "frmBloqueoFlujos.frx":0D22
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   3480
         Width           =   432
      End
      Begin VB.CommandButton cmdAnyadirCondicionParticular 
         Height          =   312
         Index           =   0
         Left            =   11070
         Picture         =   "frmBloqueoFlujos.frx":0DB4
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   3120
         Width           =   432
      End
      Begin VB.CommandButton cmdEliminarCondicionParticular 
         Height          =   315
         Index           =   3
         Left            =   -63930
         Picture         =   "frmBloqueoFlujos.frx":0E36
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   3480
         Width           =   432
      End
      Begin VB.CommandButton cmdEliminarCondicionParticular 
         Height          =   315
         Index           =   2
         Left            =   -63930
         Picture         =   "frmBloqueoFlujos.frx":0EC8
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   3960
         Width           =   432
      End
      Begin VB.CommandButton cmdEliminarCondicionParticular 
         Height          =   315
         Index           =   1
         Left            =   -63930
         Picture         =   "frmBloqueoFlujos.frx":0F5A
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   3480
         Width           =   432
      End
      Begin VB.CommandButton cmdAnyadirCondicionParticular 
         Height          =   312
         Index           =   3
         Left            =   -63930
         Picture         =   "frmBloqueoFlujos.frx":0FEC
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   3120
         Width           =   432
      End
      Begin VB.CommandButton cmdAnyadirCondicionParticular 
         Height          =   312
         Index           =   2
         Left            =   -63930
         Picture         =   "frmBloqueoFlujos.frx":106E
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   3600
         Width           =   432
      End
      Begin VB.CommandButton cmdAnyadirCondicionParticular 
         Height          =   315
         Index           =   1
         Left            =   -63930
         Picture         =   "frmBloqueoFlujos.frx":10F0
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   3120
         Width           =   432
      End
      Begin SSDataWidgets_B.SSDBGrid ssdbGridParticulares 
         Height          =   2175
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   3120
         Width           =   10905
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   11
         AllowAddNew     =   -1  'True
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   11
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   50
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID_TIPO"
         Columns(2).Name =   "ID_TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "TIPO"
         Columns(3).Name =   "TIPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   3
         Columns(4).Width=   2328
         Columns(4).Caption=   "CONDICIONES"
         Columns(4).Name =   "CONDICIONES"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Style=   4
         Columns(4).ButtonsAlways=   -1  'True
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "MENSAJE_SPA"
         Columns(5).Name =   "MENSAJE_SPA"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   500
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "MENSAJE_ENG"
         Columns(6).Name =   "MENSAJE_ENG"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   500
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "MENSAJE_GER"
         Columns(7).Name =   "MENSAJE_GER"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   500
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "FECACT"
         Columns(8).Name =   "FECACT"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   7
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "FORMULA"
         Columns(9).Name =   "FORMULA"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "MENSAJE_FRA"
         Columns(10).Name=   "MENSAJE_FRA"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         _ExtentX        =   19235
         _ExtentY        =   3836
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ListView ListViewEtapas 
         Height          =   2055
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   11445
         _ExtentX        =   20188
         _ExtentY        =   3625
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView ListViewEtapas 
         Height          =   2055
         Index           =   1
         Left            =   -74850
         TabIndex        =   11
         Top             =   600
         Width           =   11445
         _ExtentX        =   20188
         _ExtentY        =   3625
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView ListViewEtapas 
         Height          =   2055
         Index           =   2
         Left            =   -74880
         TabIndex        =   12
         Top             =   1080
         Width           =   11445
         _ExtentX        =   20188
         _ExtentY        =   3625
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView ListViewEtapas 
         Height          =   2055
         Index           =   3
         Left            =   -74880
         TabIndex        =   13
         Top             =   600
         Width           =   11445
         _ExtentX        =   20188
         _ExtentY        =   3625
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddtipocondicion 
         Height          =   555
         Index           =   0
         Left            =   7200
         TabIndex        =   15
         Top             =   4920
         Width           =   3495
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   6165
         Columns(1).Caption=   "GRUPO"
         Columns(1).Name =   "GRUPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6165
         _ExtentY        =   979
         _StockProps     =   77
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBGrid ssdbGridParticulares 
         Height          =   2175
         Index           =   1
         Left            =   -74880
         TabIndex        =   25
         Top             =   3120
         Width           =   10905
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   11
         AllowAddNew     =   -1  'True
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   11
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID_TIPO"
         Columns(2).Name =   "ID_TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "TIPO"
         Columns(3).Name =   "TIPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   3
         Columns(4).Width=   2328
         Columns(4).Caption=   "CONDICIONES"
         Columns(4).Name =   "CONDICIONES"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Style=   4
         Columns(4).ButtonsAlways=   -1  'True
         Columns(5).Width=   4419
         Columns(5).Caption=   "MENSAJE_SPA"
         Columns(5).Name =   "MENSAJE_SPA"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   4419
         Columns(6).Caption=   "MENSAJE_ENG"
         Columns(6).Name =   "MENSAJE_ENG"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   4419
         Columns(7).Caption=   "MENSAJE_GER"
         Columns(7).Name =   "MENSAJE_GER"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "FECACT"
         Columns(8).Name =   "FECACT"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   7
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "FORMULA"
         Columns(9).Name =   "FORMULA"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   4419
         Columns(10).Caption=   "MENSAJE_FRA"
         Columns(10).Name=   "MENSAJE_FRA"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         _ExtentX        =   19235
         _ExtentY        =   3836
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid ssdbGridParticulares 
         Height          =   1695
         Index           =   2
         Left            =   -74880
         TabIndex        =   26
         Top             =   3600
         Width           =   10905
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   11
         AllowAddNew     =   -1  'True
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   11
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID_TIPO"
         Columns(2).Name =   "ID_TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "TIPO"
         Columns(3).Name =   "TIPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   3
         Columns(4).Width=   2328
         Columns(4).Caption=   "CONDICIONES"
         Columns(4).Name =   "CONDICIONES"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Style=   4
         Columns(4).ButtonsAlways=   -1  'True
         Columns(5).Width=   4419
         Columns(5).Caption=   "MENSAJE_SPA"
         Columns(5).Name =   "MENSAJE_SPA"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   4419
         Columns(6).Caption=   "MENSAJE_ENG"
         Columns(6).Name =   "MENSAJE_ENG"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   4419
         Columns(7).Caption=   "MENSAJE_GER"
         Columns(7).Name =   "MENSAJE_GER"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "FECACT"
         Columns(8).Name =   "FECACT"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   7
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "FORMULA"
         Columns(9).Name =   "FORMULA"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   4419
         Columns(10).Caption=   "MENSAJE_FRA"
         Columns(10).Name=   "MENSAJE_FRA"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         _ExtentX        =   19235
         _ExtentY        =   2990
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid ssdbGridParticulares 
         Height          =   2175
         Index           =   3
         Left            =   -74880
         TabIndex        =   27
         Top             =   3120
         Width           =   10905
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   11
         AllowAddNew     =   -1  'True
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   11
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID_TIPO"
         Columns(2).Name =   "ID_TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "TIPO"
         Columns(3).Name =   "TIPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   3
         Columns(4).Width=   2328
         Columns(4).Caption=   "CONDICIONES"
         Columns(4).Name =   "CONDICIONES"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Style=   4
         Columns(4).ButtonsAlways=   -1  'True
         Columns(5).Width=   4419
         Columns(5).Caption=   "MENSAJE_SPA"
         Columns(5).Name =   "MENSAJE_SPA"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   4419
         Columns(6).Caption=   "MENSAJE_ENG"
         Columns(6).Name =   "MENSAJE_ENG"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   4419
         Columns(7).Caption=   "MENSAJE_GER"
         Columns(7).Name =   "MENSAJE_GER"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "FECACT"
         Columns(8).Name =   "FECACT"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   7
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "FORMULA"
         Columns(9).Name =   "FORMULA"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   4419
         Columns(10).Caption=   "MENSAJE_FRA"
         Columns(10).Name=   "MENSAJE_FRA"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         _ExtentX        =   19235
         _ExtentY        =   3836
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddtipocondicion 
         Height          =   555
         Index           =   1
         Left            =   -67800
         TabIndex        =   22
         Top             =   4920
         Width           =   3495
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   6165
         Columns(1).Caption=   "GRUPO"
         Columns(1).Name =   "GRUPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6165
         _ExtentY        =   979
         _StockProps     =   77
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddtipocondicion 
         Height          =   555
         Index           =   2
         Left            =   -67800
         TabIndex        =   23
         Top             =   4920
         Width           =   3495
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   6165
         Columns(1).Caption=   "GRUPO"
         Columns(1).Name =   "GRUPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6165
         _ExtentY        =   979
         _StockProps     =   77
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddtipocondicion 
         Height          =   555
         Index           =   3
         Left            =   -67800
         TabIndex        =   24
         Top             =   4920
         Width           =   3495
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   6165
         Columns(1).Caption=   "GRUPO"
         Columns(1).Name =   "GRUPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6165
         _ExtentY        =   979
         _StockProps     =   77
         BackColor       =   16777215
      End
      Begin VB.Label lblEtapas 
         Caption         =   "DEtapas"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   8415
      End
      Begin VB.Label lblEtapas 
         Caption         =   "DEtapas"
         Height          =   255
         Index           =   1
         Left            =   -74880
         TabIndex        =   7
         Top             =   360
         Width           =   8415
      End
      Begin VB.Label lblEtapas 
         Caption         =   "DEtapas"
         Height          =   255
         Index           =   2
         Left            =   -74880
         TabIndex        =   6
         Top             =   840
         Width           =   8415
      End
      Begin VB.Label lblEtapas 
         Caption         =   "DEtapas"
         Height          =   255
         Index           =   3
         Left            =   -74880
         TabIndex        =   5
         Top             =   360
         Width           =   8415
      End
      Begin VB.Label lblCondParticulares 
         Caption         =   "DCondParticulares"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   2880
         Width           =   8295
      End
      Begin VB.Label lblCondParticulares 
         Caption         =   "DCondParticulares"
         Height          =   255
         Index           =   1
         Left            =   -74880
         TabIndex        =   3
         Top             =   2880
         Width           =   8295
      End
      Begin VB.Label lblCondParticulares 
         Caption         =   "DCondParticulares"
         Height          =   255
         Index           =   2
         Left            =   -74880
         TabIndex        =   2
         Top             =   3360
         Width           =   8295
      End
      Begin VB.Label lblCondParticulares 
         Caption         =   "DCondParticulares"
         Height          =   255
         Index           =   3
         Left            =   -74880
         TabIndex        =   1
         Top             =   2880
         Width           =   8295
      End
   End
End
Attribute VB_Name = "frmBloqueoFlujos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_lSolicitud As Long
Public m_lIdFlujo As Long
Public m_bModifFlujo As Boolean
Public m_lIdFormulario As Long
Private m_oEtapasBloqueo As CEtapasBloqueo
Private m_scabeceraid As String
Private m_scabeceratipo As String
Private m_scabeceracondiciones As String
Private m_scabeceramensajespa As String
Private m_scabeceramensajeeng As String
Private m_scabeceramensajeger As String
Private m_scabeceramensajefra As String

Private m_arDenTipoCondicion(0 To 2) As String

Public m_lIdCond As Long
Private m_oCondicionesParticulares(0 To 3) As CCondsBloqueo
'Variable Privadas
Private m_oCondicionEnEdicion As CCondBloqueo
Private m_oCondicionAnyadir As CCondBloqueo

Private m_oIBAseDatosEnEdicion As IBaseDatos

Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bError As Boolean

Private m_sMensajesCondicion(1 To 3) As String  'literales para basmensajes
Private m_sErrorFormula(1 To 11) As String
Public m_bErrorConds As Boolean
Private m_lFilaPrevia As Long
Private m_bCierre As Boolean
Public m_bBtnClk As Boolean
Private m_bBorrado As Boolean
Private m_bCerrando As Boolean

Private m_bListaModificada As Boolean
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    On Error Resume Next
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_BLOQUEOFLUJOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        SSTabBloqueos.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTabBloqueos.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        SSTabBloqueos.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        SSTabBloqueos.TabCaption(3) = Ador(0).Value
        Ador.MoveNext
        lblEtapas(0).caption = Ador(0).Value
        Ador.MoveNext
        lblCondParticulares(0).caption = Ador(0).Value
        lblCondParticulares(1).caption = Ador(0).Value
        lblCondParticulares(2).caption = Ador(0).Value
        lblCondParticulares(3).caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        m_scabeceraid = Ador(0).Value
        Ador.MoveNext
        m_scabeceratipo = Ador(0).Value
        Ador.MoveNext
        m_scabeceracondiciones = Ador(0).Value
        Ador.MoveNext
        m_scabeceramensajespa = Ador(0).Value
        Ador.MoveNext
        m_scabeceramensajeeng = Ador(0).Value
        Ador.MoveNext
        m_scabeceramensajeger = Ador(0).Value
        Ador.MoveNext
        m_arDenTipoCondicion(1) = Ador(0).Value
        Ador.MoveNext
        m_arDenTipoCondicion(2) = Ador(0).Value
        Ador.MoveNext
        m_sMensajesCondicion(1) = Ador(0).Value
        Ador.MoveNext
        m_sMensajesCondicion(2) = Ador(0).Value
        For i = 1 To 11
            Ador.MoveNext
            m_sErrorFormula(i) = Ador(0).Value
        Next
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        m_sMensajesCondicion(3) = Ador(0).Value
        Ador.MoveNext
        lblEtapas(1).caption = Ador(0).Value
        Ador.MoveNext
        lblEtapas(2).caption = Ador(0).Value
        Ador.MoveNext
        lblEtapas(3).caption = Ador(0).Value
        Ador.MoveNext
        m_scabeceramensajefra = Ador(0).Value
        Ador.MoveNext
        chkPermPedDirecto.caption = Ador(0).Value  'No permitir realizar pedidos directos hasta que se haya finalizado el workflow.
        'Esta la meto a pi�on es la condicion vacia
        m_arDenTipoCondicion(0) = ""
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub ConfigurarSeguridad()
    Dim i As Integer
    
    If m_bModifFlujo Then
        For i = 0 To ssdbGridParticulares.Count - 1
            sdbddtipocondicion(i).AddItem 0 & Chr(m_lSeparador) & ""
            ssdbGridParticulares(i).Columns("TIPO").DropDownHwnd = sdbddtipocondicion(i).hWnd
            ssdbGridParticulares(i).AllowAddNew = True
            ssdbGridParticulares(i).AllowUpdate = True
            ssdbGridParticulares(i).AllowDelete = True
        Next
    
    Else
        For i = 0 To ListViewEtapas.Count - 1
            ListViewEtapas(i).Enabled = False
        Next
            
        For i = 0 To ssdbGridParticulares.Count - 1
            cmdAnyadirCondicionParticular(i).Enabled = False
            cmdEliminarCondicionParticular(i).Enabled = False
            ssdbGridParticulares(i).AllowAddNew = False
            ssdbGridParticulares(i).AllowUpdate = False
            ssdbGridParticulares(i).AllowDelete = False
        Next
    End If
End Sub

Private Sub CargarGridEtapas()
Dim rs As New ADODB.Recordset
Dim m_oEtapasBloqueo As New CEtapasBloqueo
Dim checkear As Boolean
Dim ielementos As Integer
Dim noexisteenBD As Integer
Dim i As Integer

checkear = False
ielementos = 0

Set m_oEtapasBloqueo = oFSGSRaiz.Generar_CEtapasBloqueo
Set rs = m_oEtapasBloqueo.CargarEtapas(m_lIdFlujo, basPublic.gParametrosInstalacion.gIdioma)
 
    If rs.EOF Then
        rs.Close
        Set rs = Nothing
    Else
        For i = 0 To ListViewEtapas.Count - 1
            ListViewEtapas(i).ColumnHeaders.Add , , "Texto" & i, 2000
            ListViewEtapas(i).ColumnHeaders.Add , , "ExisteBD" & i, 1260
            ListViewEtapas(i).ColumnHeaders.Add , , "IdBloque" & i, 1300
            Me.ListViewEtapas(i).View = lvwReport
            ListViewEtapas(i).ListItems.clear
        Next

        While Not rs.EOF
            checkear = False
            noexisteenBD = 0
            
            If IsNull(rs.Fields("BLOQUEO_APERTURA")) Or IsNull(rs.Fields("BLOQUEO_ADJUDICACION")) Or IsNull(rs.Fields("BLOQUEO_PEDIDOSDIRECTOS")) Or IsNull(rs.Fields("BLOQUEO_EP")) Then
                    noexisteenBD = 1
            End If
            
            For i = 0 To ListViewEtapas.Count - 1
                If i = 0 Then
                'Bloqueo apertura
                    If noexisteenBD = 1 Then
                        If rs.Fields("TIPO") = 2 Then  'si no existe en BD marco en BD la etapa FIN
                            checkear = True
                        End If
                    Else
                        If rs.Fields("BLOQUEO_APERTURA") = 1 Then
                            checkear = True
                        End If
                    End If
                End If
                If i = 1 Then
                'Bloqueo adjudicacion
                    If noexisteenBD = 1 Then
                        If rs.Fields("TIPO") = 2 Then  'si no existe en BD marco en BD la etapa FIN
                            checkear = True
                        End If
                    Else
                        If rs.Fields("BLOQUEO_ADJUDICACION") = 1 Then
                              checkear = True
                        End If
                    End If
                End If
                If i = 2 Then
                'Bloqueo pedidos directos
                    If noexisteenBD = 1 Then
                        If rs.Fields("TIPO") = 2 Then  'si no existe en BD marco en BD la etapa FIN
                            checkear = True
                        End If
                    Else
                        If rs.Fields("BLOQUEO_PEDIDOSDIRECTOS") = 1 Then
                            checkear = True
                        End If
                    End If
                End If
                If i = 3 Then
                'Bloqueo EP
                    If noexisteenBD = 1 Then
                        If rs.Fields("TIPO") = 2 Then  'si no existe en BD marco en BD la etapa FIN
                            checkear = True
                        End If
                    Else
                        If rs.Fields("BLOQUEO_EP") = 1 Then
                            checkear = True
                        End If
                    End If
                End If
                                
                Dim list_item As listItem
                If checkear = True Then
                    Set list_item = ListViewEtapas(i).ListItems.Add(, , rs.Fields("DEN"))
                    list_item.SubItems(1) = noexisteenBD
                    list_item.SubItems(2) = Trim(rs.Fields("BLOQUE"))
                    ListViewEtapas(i).ListItems.Item(ielementos + 1).Checked = True
                    
                Else
                    Set list_item = ListViewEtapas(i).ListItems.Add(, , rs.Fields("DEN"))
                    list_item.SubItems(1) = noexisteenBD
                    list_item.SubItems(2) = Trim(rs.Fields("BLOQUE"))
                End If
                            
                checkear = False
            Next
            
            checkear = False
            ielementos = ielementos + 1
            noexisteenBD = 0
            
            rs.MoveNext
        Wend
        
        For i = 0 To ListViewEtapas.Count - 1
        'Oculto las columnas
            ListViewEtapas(i).ColumnHeaders(1).Width = 9500
            ListViewEtapas(i).ColumnHeaders(2).Width = 0
            ListViewEtapas(i).ColumnHeaders(3).Width = 0
        Next
    
        rs.Close
        Set rs = Nothing
      
    End If

End Sub

Private Sub chkPermPedDirecto_Click()
    Dim teserror As TipoErrorSummit
    Dim oSolicitud As CSolicitud
    
    'Se permitir�n los pedidos directos:
    Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
    oSolicitud.Id = g_lSolicitud
    teserror = oSolicitud.GuardarConfiguracion(bPermPedDirecto:=chkPermPedDirecto.Value <> vbChecked, bSoloPed:=True)
                
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    Else
        frmSOLConfiguracion.m_bRespetar = True
        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("FECACT").Value = oSolicitud.FECACT
        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Update
        frmSOLConfiguracion.m_bRespetar = False
    End If

    Set oSolicitud = Nothing
End Sub

Private Sub cmdAnyadirCondicionParticular_Click(Index As Integer)
    If ssdbGridParticulares(Index).IsAddRow Then
        ssdbGridParticulares(Index).Update
    End If
    ssdbGridParticulares(Index).Scroll 0, ssdbGridParticulares(Index).Rows - ssdbGridParticulares(Index).Row
    
    If ssdbGridParticulares(Index).VisibleRows > 0 Then
        
        If ssdbGridParticulares(Index).VisibleRows >= ssdbGridParticulares(Index).Rows Then
            If ssdbGridParticulares(Index).VisibleRows = ssdbGridParticulares(Index).Rows Then
                ssdbGridParticulares(Index).Row = ssdbGridParticulares(Index).Rows - 1
            Else
                ssdbGridParticulares(Index).Row = ssdbGridParticulares(Index).Rows
            End If
        Else
            ssdbGridParticulares(Index).Row = ssdbGridParticulares(Index).Rows - (ssdbGridParticulares(Index).Rows - ssdbGridParticulares(Index).VisibleRows) - 1
        End If
    End If
    
    If Me.Visible Then ssdbGridParticulares(Index).SetFocus
End Sub

Private Sub cmdEliminarCondicionParticular_Click(Index As Integer)

    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit

    If ssdbGridParticulares(Index).IsAddRow Then
        If ssdbGridParticulares(Index).DataChanged Then
            ssdbGridParticulares(Index).CancelUpdate
        End If
    Else
        If ssdbGridParticulares(Index).Rows = 0 Then Exit Sub
        Screen.MousePointer = vbHourglass
        
        If ssdbGridParticulares(Index).SelBookmarks.Count = 1 Then
            irespuesta = oMensajes.PreguntaEliminar(ssdbGridParticulares(Index).Columns("COD").Value)
            
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set m_oCondicionEnEdicion = m_oCondicionesParticulares(Index).Item(CStr(ssdbGridParticulares(Index).Columns("ID").Value))
            
            Set m_oIBAseDatosEnEdicion = m_oCondicionEnEdicion
            teserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos()
            
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                
                If Me.Visible Then ssdbGridParticulares(Index).SetFocus
                Exit Sub
            
            Else
                m_oCondicionesParticulares(Index).Remove (CStr(ssdbGridParticulares(Index).Columns("ID").Value))
                If ssdbGridParticulares(Index).AddItemRowIndex(ssdbGridParticulares(Index).Bookmark) > -1 Then
                    ssdbGridParticulares(Index).RemoveItem (ssdbGridParticulares(Index).AddItemRowIndex(ssdbGridParticulares(Index).Bookmark))
                Else
                    ssdbGridParticulares(Index).RemoveItem (0)
                End If
                If IsEmpty(ssdbGridParticulares(Index).GetBookmark(0)) Then
                    ssdbGridParticulares(Index).Bookmark = ssdbGridParticulares(Index).GetBookmark(-1)
                Else
                    ssdbGridParticulares(Index).Bookmark = ssdbGridParticulares(Index).GetBookmark(0)
                End If
                basSeguridad.RegistrarAccion accionessummit.ACCCondicionBloqueoElim, "Condicion Particular:" & m_oCondicionEnEdicion.Id
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            End If
            
            Set m_oIBAseDatosEnEdicion = Nothing
            Set m_oCondicionEnEdicion = Nothing
            
        End If
            
        ssdbGridParticulares(Index).SelBookmarks.RemoveAll
    
        m_bErrorConds = False
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub Form_Load()
    m_bErrorConds = False
    m_bCierre = False
    m_bBtnClk = False
    m_bBorrado = False
    m_bCerrando = False
    m_bListaModificada = False
    PonerFieldSeparator Me
    
    CargarRecursos
    ConfigurarSeguridad
    CargarGridEtapas
    CargarGridCondicionesParticulares
    
    'Carga si se permite el pedido directo o no antes de que se acabe el workflow.
    If g_lSolicitud = 0 Then
        chkPermPedDirecto.Visible = False
    Else
        chkPermPedDirecto.Visible = True
                
        Dim oSolicitud As CSolicitud
        Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
        oSolicitud.Id = g_lSolicitud
        oSolicitud.CargarConfiguracion , , , , True
        If oSolicitud.PermitirPedidoDirecto = False Then chkPermPedDirecto.Value = vbChecked
        Set oSolicitud = Nothing
    End If
    
    VisibilidadBotones
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    
    If Me.Width < 11040 Or Me.Height < 5670 Then Exit Sub

    SSTabBloqueos.Width = Me.Width - 225
    SSTabBloqueos.Height = Me.Height - 615

    For i = 0 To 3
        ListViewEtapas(i).Height = SSTabBloqueos.Height * 43.77 / 100
        lblCondParticulares(i).Top = ListViewEtapas(i).Top + ListViewEtapas(i).Height + 30
        ssdbGridParticulares(i).Top = lblCondParticulares(i).Top + 240
        cmdAnyadirCondicionParticular(i).Top = ssdbGridParticulares(i).Top
        cmdEliminarCondicionParticular(i).Top = cmdAnyadirCondicionParticular(i).Top + 360
        ssdbGridParticulares(i).Height = SSTabBloqueos.Height - ssdbGridParticulares(i).Top - 240
        
        ListViewEtapas(i).Width = SSTabBloqueos.Width - 300
        ssdbGridParticulares(i).Width = ListViewEtapas(i).Width - 540
        cmdAnyadirCondicionParticular(i).Left = ssdbGridParticulares(i).Width + 165
        cmdEliminarCondicionParticular(i).Left = cmdAnyadirCondicionParticular(i).Left
        If ssdbGridParticulares(i).Width > 12463.937 Then
            ssdbGridParticulares(i).Columns("COD").Width = ssdbGridParticulares(i).Width * 14.56 / 100
            ssdbGridParticulares(i).Columns("TIPO").Width = ssdbGridParticulares(i).Width * 14.56 / 100
            ssdbGridParticulares(i).Columns("CONDICIONES").Width = ssdbGridParticulares(i).Width * 10.59 / 100
            ssdbGridParticulares(i).Columns("MENSAJE_SPA").Width = ssdbGridParticulares(i).Width * 20.1 / 100
            ssdbGridParticulares(i).Columns("MENSAJE_ENG").Width = ssdbGridParticulares(i).Width * 20.1 / 100
            ssdbGridParticulares(i).Columns("MENSAJE_GER").Width = ssdbGridParticulares(i).Width * 20.1 / 100
            ssdbGridParticulares(i).Columns("MENSAJE_FRA").Width = ssdbGridParticulares(i).Width * 20.1 / 100
        Else
            ssdbGridParticulares(i).Columns("COD").Width = 1814.173
            ssdbGridParticulares(i).Columns("TIPO").Width = 1814.173
            ssdbGridParticulares(i).Columns("CONDICIONES").Width = 1319.811
            ssdbGridParticulares(i).Columns("MENSAJE_SPA").Width = 2505.26
            ssdbGridParticulares(i).Columns("MENSAJE_ENG").Width = 2505.26
            ssdbGridParticulares(i).Columns("MENSAJE_GER").Width = 2505.26
            ssdbGridParticulares(i).Columns("MENSAJE_FRA").Width = 2505.26
        End If
    Next i
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim m_oEtapasBloqueo As New CEtapasBloqueo
Dim etapas As Integer
Dim bloques As Integer
Dim v As Variant
Dim i As Integer
Dim vbm As Variant
Dim j As Integer
Dim oRS As ADODB.Recordset
Dim m_oCondiciones As CCondsBloqueoConds
m_bCerrando = True
'Guarda el tema de las etapas
If m_bModifFlujo And m_bListaModificada Then
    If ListViewEtapas.Count > 0 Then
        etapas = ListViewEtapas.Count 'Numero de pesta�as en la pagina (son 4)
        bloques = ListViewEtapas(1).ListItems.Count 'Numero de bloques en la solicitud (variable)
        If (bloques > 0) Then
            Set m_oEtapasBloqueo = oFSGSRaiz.Generar_CEtapasBloqueo
            For i = 1 To bloques
                m_oEtapasBloqueo.GuardarEtapas ListViewEtapas(0).ListItems(i).SubItems(2), IIf(ListViewEtapas(0).ListItems.Item(i).Checked, 1, 0), IIf(ListViewEtapas(1).ListItems.Item(i).Checked, 1, 0), IIf(ListViewEtapas(2).ListItems.Item(i).Checked, 1, 0), IIf(ListViewEtapas(3).ListItems.Item(i).Checked, 1, 0), ListViewEtapas(0).ListItems(i).SubItems(1)
            Next
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
    m_bListaModificada = False
End If

For i = 0 To 3
    If ssdbGridParticulares(i).DataChanged = True Then
        v = ssdbGridParticulares(i).ActiveCell.Value
        If Me.Visible Then ssdbGridParticulares(i).SetFocus
        ssdbGridParticulares(i).ActiveCell.Value = v
        If (actualizarYSalir(i) = True) Then
            Set m_oCondiciones = Nothing
            Set oRS = Nothing
            Cancel = True
            m_bCerrando = False
            Exit Sub
        End If
    End If
    
    For j = 0 To ssdbGridParticulares(i).Rows - 1
        vbm = ssdbGridParticulares(i).AddItemBookmark(j)
        If Not NoHayParametro(ssdbGridParticulares(i).Columns("ID").CellValue(vbm)) Then
            Set m_oCondiciones = oFSGSRaiz.Generar_CCondsBloqueoConds
            Set oRS = m_oCondiciones.DevolverCondiciones(ssdbGridParticulares(i).Columns("ID").CellValue(vbm))
            If oRS.EOF Then
                m_bCierre = True
                SSTabBloqueos.Tab = i
                Call oMensajes.NoExistenCondicionesBloqueo(SSTabBloqueos.TabCaption(i))
                Set m_oCondiciones = Nothing
                Set oRS = Nothing
                Cancel = True
                m_bCerrando = False
                Exit Sub
            End If
        End If
    Next j
Next i

m_bCerrando = False
Set m_oCondiciones = Nothing
Set oRS = Nothing
End Sub

Public Sub CargarGridCondicionesParticulares()
    Dim oCondicion As CCondBloqueo
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    
    Dim i As Integer
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = oFSGSRaiz.Generar_CGestorParametros
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    For Each oIdi In oIdiomas
        ssdbGridParticulares(i).Columns("MENSAJE_" & oIdi.Cod).Visible = True
    Next
    
    For i = 0 To ssdbGridParticulares.Count - 1
        ssdbGridParticulares(i).RemoveAll
        ssdbGridParticulares(i).Columns("TIPO").caption = m_scabeceratipo
        ssdbGridParticulares(i).Columns("CONDICIONES").caption = m_scabeceracondiciones
        ssdbGridParticulares(i).Columns("MENSAJE_SPA").caption = m_scabeceramensajespa
        ssdbGridParticulares(i).Columns("MENSAJE_ENG").caption = m_scabeceramensajeeng
        ssdbGridParticulares(i).Columns("MENSAJE_GER").caption = m_scabeceramensajeger
        ssdbGridParticulares(i).Columns("MENSAJE_FRA").caption = m_scabeceramensajefra
    
        Dim temp As String
        temp = " "
    
        Set m_oCondicionesParticulares(i) = oFSGSRaiz.Generar_CCondsBloqueo
        m_oCondicionesParticulares(i).CargarCondiciones m_lIdFlujo, i
                   
        For Each oCondicion In m_oCondicionesParticulares(i)
            ssdbGridParticulares(i).AddItem oCondicion.Id & Chr(m_lSeparador) & oCondicion.Cod & Chr(m_lSeparador) & oCondicion.Tipo & Chr(m_lSeparador) & m_arDenTipoCondicion(oCondicion.Tipo) & Chr(m_lSeparador) & temp & Chr(m_lSeparador) & oCondicion.MensajeSpa & Chr(m_lSeparador) & oCondicion.MensajeEng & Chr(m_lSeparador) & oCondicion.MensajeGer & Chr(m_lSeparador) & oCondicion.FECACT & Chr(m_lSeparador) & oCondicion.Formula & Chr(m_lSeparador) & oCondicion.MensajeFra
        Next
    Next
    Set oGestorParametros = Nothing
    Set oIdiomas = Nothing
    Set oIdi = Nothing
End Sub

Private Sub ListViewEtapas_ItemCheck(Index As Integer, ByVal Item As MSComctlLib.listItem)
m_bListaModificada = True
End Sub

Private Sub sdbddtipocondicion_Click(Index As Integer)
    If ssdbGridParticulares(Index).Columns("TIPO").Value = "" Then Exit Sub
    
    ssdbGridParticulares(Index).Columns("ID_TIPO").Value = sdbddtipocondicion(Index).Columns(0).Value
    ssdbGridParticulares(Index).Columns("TIPO").Value = sdbddtipocondicion(Index).Columns(1).Value
End Sub

Private Sub sdbddtipocondicion_CloseUp(Index As Integer)
    If ssdbGridParticulares(Index).Columns("TIPO").Value = "" Then Exit Sub
    
    ssdbGridParticulares(Index).Columns("ID_TIPO").Value = sdbddtipocondicion(Index).Columns(0).Value
    ssdbGridParticulares(Index).Columns("TIPO").Value = sdbddtipocondicion(Index).Columns(1).Value
End Sub

Private Sub sdbddtipocondicion_DropDown(Index As Integer)
    Screen.MousePointer = vbHourglass
    sdbddtipocondicion(Index).RemoveAll
    sdbddtipocondicion(Index).AddItem 1 & Chr(m_lSeparador) & m_arDenTipoCondicion(1)
    sdbddtipocondicion(Index).AddItem 2 & Chr(m_lSeparador) & m_arDenTipoCondicion(2)
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddtipocondicion_InitColumnProps(Index As Integer)
    sdbddtipocondicion(Index).DataFieldList = "Column 0"
    sdbddtipocondicion(Index).DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddtipocondicion_PositionList(Index As Integer, ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddtipocondicion(Index).MoveFirst

    If ssdbGridParticulares(Index).Columns("TIPO").Value <> "" Then
        For i = 0 To sdbddtipocondicion(Index).Rows - 1
            bm = sdbddtipocondicion(Index).GetBookmark(i)
            If UCase(ssdbGridParticulares(Index).Columns("TIPO").Value) = UCase(Mid(sdbddtipocondicion(Index).Columns(1).CellText(bm), 1, Len(ssdbGridParticulares(Index).Columns("TIPO").Value))) Then
                ssdbGridParticulares(Index).Columns("TIPO").Value = Mid(sdbddtipocondicion(Index).Columns(1).CellText(bm), 1, Len(ssdbGridParticulares(Index).Columns("TIPO").Value))
                sdbddtipocondicion(Index).Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub ssdbGridParticulares_BeforeRowColChange(Index As Integer, Cancel As Integer)
Dim oRS As ADODB.Recordset
Dim m_oCondiciones As CCondsBloqueoConds
    
    If Not NoHayParametro(ssdbGridParticulares(Index).Columns("ID").Value) Then
        Set m_oCondiciones = oFSGSRaiz.Generar_CCondsBloqueoConds
        Set oRS = m_oCondiciones.DevolverCondiciones(ssdbGridParticulares(Index).Columns("ID").Value)
        If oRS.EOF Then
            m_lFilaPrevia = ssdbGridParticulares(Index).Row
            m_bErrorConds = True
        Else
            m_bErrorConds = False
        End If
    End If
    
    Set m_oCondiciones = Nothing
    Set oRS = Nothing
End Sub

Private Sub ssdbGridParticulares_BtnClick(Index As Integer)

    If ssdbGridParticulares(Index).Col < 0 Then Exit Sub
    
    If ssdbGridParticulares(Index).Columns(ssdbGridParticulares(Index).Col).Name = "CONDICIONES" Then
        ssdbGridParticulares(Index).Update
        DoEvents
        
        If m_bError Or m_bAnyaError Or m_bModError Then
            Exit Sub
        End If
    
        If ssdbGridParticulares(Index).Columns("ID").Value <> "" Then
            m_bBtnClk = True
            frmBloqueosCondiciones.m_bModifFlujo = m_bModifFlujo
            frmBloqueosCondiciones.m_lIdFlujo = m_lIdFlujo
            frmBloqueosCondiciones.m_lIdCond = ssdbGridParticulares(Index).Columns("ID").Value
            frmBloqueosCondiciones.m_lIdFormulario = m_lIdFormulario
            frmBloqueosCondiciones.m_sFormula = ssdbGridParticulares(Index).Columns("FORMULA").Value
            frmBloqueosCondiciones.Show vbModal
            
            ssdbGridParticulares(Index).Columns("FORMULA").Value = frmBloqueosCondiciones.m_sFormula
            
            If ssdbGridParticulares(Index).DataChanged Then
                ssdbGridParticulares(Index).Update
            End If
        End If
    End If
End Sub

Private Sub ssdbGridParticulares_Change(Index As Integer)
    Dim teserror As TipoErrorSummit
    
    If Not ssdbGridParticulares(Index).IsAddRow Then
        Set m_oCondicionEnEdicion = Nothing
        Set m_oCondicionEnEdicion = m_oCondicionesParticulares(Index).Item(CStr(ssdbGridParticulares(Index).Columns("ID").Value))
        
        Set m_oIBAseDatosEnEdicion = m_oCondicionEnEdicion
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
            
        If teserror.NumError = TESInfModificada Then
            TratarError teserror
            ssdbGridParticulares(Index).DataChanged = False
            
            CargarGridCondicionesParticulares
            teserror.NumError = TESnoerror
        End If
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then ssdbGridParticulares(Index).SetFocus
        End If
        If teserror.NumError = TESnoerror Then
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
End Sub

Private Sub ssdbGridParticulares_AfterDelete(Index As Integer, RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (ssdbGridParticulares(Index).Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(ssdbGridParticulares(Index).GetBookmark(0)) Then
        ssdbGridParticulares(Index).Bookmark = ssdbGridParticulares(Index).GetBookmark(-1)
    Else
        ssdbGridParticulares(Index).Bookmark = ssdbGridParticulares(Index).GetBookmark(0)
    End If
    If Me.Visible Then ssdbGridParticulares((Index)).SetFocus
End Sub

Private Sub ssdbGridParticulares_AfterInsert(Index As Integer, RtnDispErrMsg As Integer)

''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0

    If IsEmpty(ssdbGridParticulares(Index).GetBookmark(0)) Then
        ssdbGridParticulares(Index).Bookmark = ssdbGridParticulares(Index).GetBookmark(-1)
    Else
        ssdbGridParticulares(Index).Bookmark = ssdbGridParticulares(Index).GetBookmark(0)
    End If
End Sub

Private Sub ssdbGridParticulares_AfterUpdate(Index As Integer, RtnDispErrMsg As Integer)
    
    RtnDispErrMsg = 0
    If Not m_oCondicionEnEdicion Is Nothing Then
        ssdbGridParticulares(Index).Columns("FECACT").Value = m_oCondicionEnEdicion.FECACT
    End If
    
    Set m_oCondicionEnEdicion = Nothing
End Sub

Private Sub ssdbGridParticulares_BeforeDelete(Index As Integer, Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub ssdbGridParticulares_BeforeUpdate(Index As Integer, Cancel As Integer)
    Dim teserror As TipoErrorSummit
    
    'Comprobar datos Obligatorios
    If ssdbGridParticulares(Index).Columns("COD").Value = "" Then
        m_bError = True
        oMensajes.NoValido "ID"
        GoTo Salir
    End If
    If ssdbGridParticulares(Index).Columns("TIPO").Value = "" Then
        m_bError = True
        oMensajes.NoValido m_sMensajesCondicion(1) 'Aqui hay que modificar
        GoTo Salir
    End If
    If ssdbGridParticulares(Index).Columns("MENSAJE_" & basPublic.gParametrosInstalacion.gIdioma).Value = "" Then
        m_bError = True
        oMensajes.IntroducirCampo m_sMensajesCondicion(3)
        GoTo Salir
    End If
        
    'Guardar Datos
     If ssdbGridParticulares(Index).IsAddRow Then
        Set m_oCondicionAnyadir = oFSGSRaiz.Generar_CCondBloqueo
        m_oCondicionAnyadir.Cod = ssdbGridParticulares(Index).Columns("COD").Value
        m_oCondicionAnyadir.Tipo = CInt(ssdbGridParticulares(Index).Columns("ID_TIPO").Value)
        m_oCondicionAnyadir.MensajeSpa = ssdbGridParticulares(Index).Columns("MENSAJE_SPA").Value
        m_oCondicionAnyadir.MensajeEng = ssdbGridParticulares(Index).Columns("MENSAJE_ENG").Value
        m_oCondicionAnyadir.MensajeGer = ssdbGridParticulares(Index).Columns("MENSAJE_GER").Value
        m_oCondicionAnyadir.MensajeFra = ssdbGridParticulares(Index).Columns("MENSAJE_FRA").Value
        m_oCondicionAnyadir.TipoBloqueo = Index
        m_oCondicionAnyadir.Workflow = m_lIdFlujo
        m_oCondicionAnyadir.Formula = "" 'sra
                
        Set m_oIBAseDatosEnEdicion = m_oCondicionAnyadir
        teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bAnyaError = True
            If Me.Visible Then ssdbGridParticulares(Index).SetFocus
            ssdbGridParticulares(Index).CancelUpdate
            GoTo Salir
        Else
            basSeguridad.RegistrarAccion accionessummit.ACCCondicionBloqueoAnya, "Condicion Particular:" & m_oCondicionAnyadir.Id
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If

        ssdbGridParticulares(Index).Columns("ID").Value = m_oCondicionAnyadir.Id
        ssdbGridParticulares(Index).Columns("FECACT").Value = m_oCondicionAnyadir.FECACT
        m_oCondicionesParticulares(Index).AddCondicion m_oCondicionAnyadir
        m_bAnyaError = False
    Else
        If Not ssdbGridParticulares(Index).IsAddRow Then
            Set m_oCondicionEnEdicion = Nothing
            Set m_oCondicionEnEdicion = m_oCondicionesParticulares(Index).Item(CStr(ssdbGridParticulares(Index).Columns("ID").Value))
            
            Set m_oIBAseDatosEnEdicion = m_oCondicionEnEdicion
            teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
                
            If teserror.NumError = TESInfModificada Then
                TratarError teserror
                ssdbGridParticulares(Index).DataChanged = False
                
                CargarGridCondicionesParticulares
                teserror.NumError = TESnoerror
            End If
                
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                If Me.Visible Then ssdbGridParticulares(Index).SetFocus
            End If
        End If
    
        m_oCondicionEnEdicion.Cod = ssdbGridParticulares(Index).Columns("COD").Value
        m_oCondicionEnEdicion.Tipo = CInt(ssdbGridParticulares(Index).Columns("ID_TIPO").Value)
        m_oCondicionEnEdicion.MensajeSpa = ssdbGridParticulares(Index).Columns("MENSAJE_SPA").Value
        m_oCondicionEnEdicion.MensajeEng = ssdbGridParticulares(Index).Columns("MENSAJE_ENG").Value
        m_oCondicionEnEdicion.MensajeGer = ssdbGridParticulares(Index).Columns("MENSAJE_GER").Value
        m_oCondicionEnEdicion.MensajeFra = ssdbGridParticulares(Index).Columns("MENSAJE_FRA").Value
        m_oCondicionEnEdicion.TipoBloqueo = Index
        m_oCondicionEnEdicion.Workflow = m_lIdFlujo
        m_oCondicionEnEdicion.Formula = ssdbGridParticulares(Index).Columns("FORMULA").Value
                
        Set m_oIBAseDatosEnEdicion = m_oCondicionEnEdicion
        teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bModError = True
            If Me.Visible Then ssdbGridParticulares(Index).SetFocus
            ssdbGridParticulares(Index).DataChanged = False
            GoTo Salir

        Else
            ''' Registro de acciones
            ssdbGridParticulares(Index).Columns("FECACT").Value = m_oCondicionEnEdicion.FECACT
            basSeguridad.RegistrarAccion accionessummit.ACCCondicionBloqueoModif, "Condicion Particular:" & m_oCondicionEnEdicion.Id
            m_bModError = False
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If

        Set m_oCondicionEnEdicion = Nothing

    End If

    Set m_oCondicionAnyadir = Nothing
    
    m_bError = False
    m_bAnyaError = False
    m_bModError = False
    
    Exit Sub
    
Salir:
    If Me.Visible Then ssdbGridParticulares(Index).SetFocus
    Cancel = True
    Exit Sub
    
Error:
    If err.Number = 13 Then 'Type mismatch
        Cancel = True
        oMensajes.NoValido m_sMensajesCondicion(2)
        If Me.Visible Then ssdbGridParticulares(Index).SetFocus
        m_bError = True
        Exit Sub
    End If
End Sub

Private Sub ssdbGridParticulares_GotFocus(Index As Integer)
    If ssdbGridParticulares(Index).IsAddRow Then
        ssdbGridParticulares(Index).Col = 1
    End If
End Sub

Private Sub ssdbGridParticulares_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete And ssdbGridParticulares(Index).Col = -1 Then
        m_bBorrado = True
        cmdEliminarCondicionParticular_Click (Index)
        m_bBorrado = False
        Exit Sub
    End If
End Sub

Private Sub ssdbGridParticulares_LostFocus(Index As Integer)
    Dim oRS As ADODB.Recordset
    Dim m_oCondiciones As CCondsBloqueoConds
    
    If m_bCierre = True Then
        m_bCierre = False
    Else
        If ssdbGridParticulares(Index).Col <> 4 And Me.ActiveControl.Name <> "cmdEliminarCondicionParticular" And m_bBtnClk = False And m_bBorrado = False Then
            If Not NoHayParametro(ssdbGridParticulares(Index).Columns("ID").Value) Then
                Set m_oCondiciones = oFSGSRaiz.Generar_CCondsBloqueoConds
                Set oRS = m_oCondiciones.DevolverCondiciones(ssdbGridParticulares(Index).Columns("ID").Value)
                If oRS.EOF Then
                    SSTabBloqueos.Tab = Index
                    Call oMensajes.NoExistenCondicionesBloqueo(SSTabBloqueos.TabCaption(Index))
                    If Me.Visible Then ssdbGridParticulares(Index).SetFocus
                End If
            End If
    
            Set m_oCondiciones = Nothing
            Set oRS = Nothing
        End If
    End If
End Sub

Private Sub ssdbGridParticulares_RowColChange(Index As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim bm As Variant

    If m_bCerrando = False Then
        If Not IsNull(LastRow) Then
            If ssdbGridParticulares(Index).Rows > 0 And m_lFilaPrevia <> ssdbGridParticulares(Index).Row Then
                If m_bErrorConds Then
                    bm = ssdbGridParticulares(Index).AddItemBookmark(val(m_lFilaPrevia))
                    Call oMensajes.NoExistenCondicionesBloqueo(SSTabBloqueos.TabCaption(Index))
                    ssdbGridParticulares(Index).Bookmark = bm
                End If
            End If
        End If
        
        If ssdbGridParticulares(Index).IsAddRow Then
            If Not IsNull(LastRow) Then
                If val(LastRow) <> val(ssdbGridParticulares(Index).Row) Then
                    ssdbGridParticulares(Index).Col = 1
                End If
            End If
        End If
    
        Set bm = Nothing
    End If
End Sub

Private Function actualizarYSalir(Index) As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error.
    If ssdbGridParticulares(Index).DataChanged = True Then
        m_bModError = False
        m_bAnyaError = False
        m_bError = False

        If ssdbGridParticulares(Index).Row = 0 Then
            ssdbGridParticulares(Index).MoveNext
            DoEvents
            If m_bModError Or m_bAnyaError Or m_bError Then
                actualizarYSalir = True
                Exit Function
            Else
                ssdbGridParticulares(Index).MovePrevious
            End If
        Else
            ssdbGridParticulares(Index).MovePrevious
            DoEvents
            If m_bModError Or m_bAnyaError Or m_bError Then
                actualizarYSalir = True
                Exit Function
            Else
                ssdbGridParticulares(Index).MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
    
End Function

Private Sub SSTabBloqueos_Click(PreviousTab As Integer)
    VisibilidadBotones
End Sub

''' <summary>Visibilidad de los botones de dcada tab</summary>
''' <remarks>Llamada desde: Form_Load, SSTabBloqueos_Click</remarks>

Private Sub VisibilidadBotones()
    Dim i As Integer
    For i = 0 To SSTabBloqueos.Tabs - 1
        cmdAnyadirCondicionParticular(i).Visible = (i = SSTabBloqueos.Tab)
        cmdEliminarCondicionParticular(i).Visible = (i = SSTabBloqueos.Tab)
    Next
End Sub
