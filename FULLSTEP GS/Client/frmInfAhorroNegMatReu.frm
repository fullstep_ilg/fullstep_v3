VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegMatReu 
   Caption         =   "Informe de ahorros negociados por material en reunión"
   ClientHeight    =   5820
   ClientLeft      =   1545
   ClientTop       =   2700
   ClientWidth     =   10440
   Icon            =   "frmInfAhorroNegMatReu.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5820
   ScaleWidth      =   10440
   Begin VB.Frame fraSel 
      Height          =   675
      Left            =   0
      TabIndex        =   20
      Top             =   0
      Width           =   8985
      Begin VB.CommandButton cmdActualizar 
         Height          =   285
         Left            =   8040
         Picture         =   "frmInfAhorroNegMatReu.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdCalendar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2565
         Picture         =   "frmInfAhorroNegMatReu.frx":0D3D
         Style           =   1  'Graphical
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   6540
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   240
         Width           =   1035
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "Código"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "SSDBCombo1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcFecReu 
         Height          =   285
         Left            =   570
         TabIndex        =   0
         Top             =   240
         Width           =   1965
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Fecha"
         Columns(0).Name =   "FECHA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Fecha"
         Columns(1).Name =   "FECHACORTA"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   5556
         Columns(2).Caption=   "Referencia"
         Columns(2).Name =   "REF"
         Columns(2).DataField=   "Column 1"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   3466
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdGrafico 
         Height          =   285
         Left            =   7650
         Picture         =   "frmInfAhorroNegMatReu.frx":12C7
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         Height          =   285
         Left            =   7650
         Picture         =   "frmInfAhorroNegMatReu.frx":1609
         Style           =   1  'Graphical
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   240
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         Height          =   285
         Left            =   8430
         Picture         =   "frmInfAhorroNegMatReu.frx":1753
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.Label lblFecReu 
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   60
         TabIndex        =   27
         Top             =   270
         Width           =   555
      End
      Begin VB.Label lblrefl 
         Caption         =   "Referencia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2970
         TabIndex        =   26
         Top             =   270
         Width           =   855
      End
      Begin VB.Label lblref 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3840
         TabIndex        =   25
         Top             =   240
         Width           =   2670
      End
   End
   Begin VB.PictureBox picTipoGrafico 
      BorderStyle     =   0  'None
      Height          =   435
      Left            =   5940
      ScaleHeight     =   435
      ScaleWidth      =   1695
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   150
      Visible         =   0   'False
      Width           =   1695
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
         Height          =   285
         Left            =   60
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   90
         Width           =   1575
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Row.Count       =   5
         Row(0)          =   "Barras 2D"
         Row(1)          =   "Barras 3D"
         Row(2)          =   "Lineas 2D"
         Row(3)          =   "Lineas 3D"
         Row(4)          =   "Tarta"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "TIPO"
         Columns(0).Name =   "TIPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2778
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   3675
      Left            =   0
      TabIndex        =   11
      Top             =   1770
      Width           =   10125
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   7
      stylesets.count =   2
      stylesets(0).Name=   "Red"
      stylesets(0).BackColor=   4744445
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegMatReu.frx":1855
      stylesets(1).Name=   "Green"
      stylesets(1).BackColor=   10409634
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegMatReu.frx":1871
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   1111
      Columns(0).Caption=   "Código"
      Columns(0).Name =   "GMN1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   3678
      Columns(1).Caption=   "Denominación"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   1455
      Columns(2).Caption=   "Procesos"
      Columns(2).Name =   "PROCE"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   5
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   12632256
      Columns(3).Width=   2884
      Columns(3).Caption=   "Presupuesto"
      Columns(3).Name =   "PRES"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   15400959
      Columns(4).Width=   2858
      Columns(4).Caption=   "Adjudicado"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   15400959
      Columns(5).Width=   2461
      Columns(5).Caption=   "Ahorro"
      Columns(5).Name =   "AHO"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1323
      Columns(6).Caption=   "%"
      Columns(6).Name =   "PORCEN"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "0.0#\%"
      Columns(6).FieldLen=   256
      _ExtentX        =   17859
      _ExtentY        =   6482
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   8820
      Picture         =   "frmInfAhorroNegMatReu.frx":188D
      ScaleHeight     =   735
      ScaleWidth      =   1635
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   30
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   8820
      Picture         =   "frmInfAhorroNegMatReu.frx":1D87
      ScaleHeight     =   615
      ScaleWidth      =   1635
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   90
      Visible         =   0   'False
      Width           =   1635
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   0
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   5490
      Width           =   10125
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegMatReu.frx":2234
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegMatReu.frx":2250
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegMatReu.frx":226C
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   7
      Columns(0).Width=   2275
      Columns(0).Caption=   "CAPTION"
      Columns(0).Name =   "CAPTION"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "PRESCAPTION"
      Columns(1).Name =   "PRESCAPTION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRES"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,#"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "ADJCAPTION"
      Columns(3).Name =   "ADJCAPTION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ADJ"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,#"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2090
      Columns(5).Caption=   "AHOCAPTION"
      Columns(5).Name =   "AHOCAPTION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "AHO"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "#,#"
      Columns(6).FieldLen=   256
      _ExtentX        =   17859
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraMaterial 
      Caption         =   "Mostrar resultados desde material"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   0
      TabIndex        =   12
      Top             =   720
      Width           =   10125
      Begin VB.CommandButton cmdBorrar 
         Height          =   315
         Left            =   9250
         Picture         =   "frmInfAhorroNegMatReu.frx":2288
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   360
         Width           =   345
      End
      Begin VB.CommandButton cmdSelMat 
         Height          =   315
         Left            =   9650
         Picture         =   "frmInfAhorroNegMatReu.frx":232D
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   345
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   690
         TabIndex        =   3
         Top             =   210
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
         Height          =   285
         Left            =   5220
         TabIndex        =   5
         Top             =   195
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
         Height          =   285
         Left            =   690
         TabIndex        =   7
         Top             =   570
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
         Height          =   285
         Left            =   5220
         TabIndex        =   9
         Top             =   570
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
         Height          =   285
         Left            =   6180
         TabIndex        =   6
         Top             =   195
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
         Height          =   285
         Left            =   1650
         TabIndex        =   8
         Top             =   570
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
         Height          =   285
         Left            =   6180
         TabIndex        =   10
         Top             =   570
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
         Height          =   285
         Left            =   1650
         TabIndex        =   4
         Top             =   210
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblGMN3_4 
         Caption         =   "Subfam.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   17
         Top             =   630
         Width           =   435
      End
      Begin VB.Label lblGMN2_4 
         Caption         =   "Fam.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4800
         TabIndex        =   16
         Top             =   240
         Width           =   540
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Comm:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   15
         Top             =   240
         Width           =   600
      End
      Begin VB.Label lblGMN4_4 
         Caption         =   "Gru.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4800
         TabIndex        =   14
         Top             =   615
         Width           =   390
      End
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3675
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroNegMatReu.frx":2399
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   1770
      Width           =   10125
   End
End
Attribute VB_Name = "frmInfAhorroNegMatReu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ofrmLstAhorroNeg As frmLstINFAhorrosNeg
'Variables para func. combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean

Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oMonedas As CMonedas
Private dequivalencia As Double
Private oMon As CMoneda

'Variables de seguridad
Private bRMat As Boolean

Private ADORs As Ador.Recordset

'Variable de control de flujo de proceso
Private oGestorReuniones As CGestorReuniones
Private oReuniones As CReuniones

Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4

'Variables para caso de bRMat
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

Private oICompAsignado As ICompProveAsignados
Private oIMAsig As IMaterialAsignado


'Variables utilizadas para el multilenguaje
Private sIdiTipoGrafico(5) As String
Private sIdiFecha As String
Private sIdiMat As String
Private sIdiMonCent As String
Private sIdiTotal As String
Private sIdiReu As String
Private sIdiPresup As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiDetResult As String

Private Sub cmdActualizar_Click()
Dim iNivelAsig As Integer

    If sdbcFecReu = "" Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then sdbcFecReu.SetFocus
        Exit Sub
    End If
            
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        If sdbcGMN4_4Cod = "" And sdbcGMN3_4Cod <> "" Then
            Set oICompAsignado = oGMN3Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            'Si el material que tiene asignado el comprador es de nivel 4
            ' no cargamos nada
            If iNivelAsig = 0 Or iNivelAsig > 3 Then
                oMensajes.InfAhorroMatNoValido
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            If sdbcGMN3_4Cod = "" And sdbcGMN2_4Cod <> "" Then
                Set oICompAsignado = oGMN2Seleccionado
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig = 0 Or iNivelAsig > 2 Then
                    oMensajes.InfAhorroMatNoValido
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            Else
                If sdbcGMN2_4Cod = "" And sdbcGMN1_4Cod <> "" Then
                    Set oICompAsignado = oGMN1Seleccionado
                    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                    If iNivelAsig < 1 Or iNivelAsig > 2 Then
                        oMensajes.InfAhorroMatNoValido
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Else
                    If sdbcGMN1_4Cod.Value = "" Then
                        oMensajes.InfAhorroMatNoValido
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
            End If
        End If
    End If
    
    Set ADORs = Nothing
    
    If sdbcGMN4_4Cod <> "" Then
        Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbcFecReu.Columns(0).Value), sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, , , , True, , , , True)
    Else
        If sdbcGMN3_4Cod <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbcFecReu.Columns(0).Value), sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , True, , , , True)
        Else
            If sdbcGMN2_4Cod <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbcFecReu.Columns(0).Value), sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, , , , , , True, , , , True)
            Else
                If sdbcGMN1_4Cod <> "" Then
                    Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbcFecReu.Columns(0).Value), sdbcGMN1_4Cod.Text, , , , , , , True, , , , True)
                Else
                    If bRMat Then
                        Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbcFecReu.Columns(0).Value), , , , , , , , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True)
                    Else
                        Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbcFecReu.Columns(0).Value), , , , , , , , True, , , , True)
                    End If
                End If
            End If
        End If
    End If
    
    Screen.MousePointer = vbNormal
    
    If ADORs Is Nothing Then Exit Sub
    
    CargarGrid
    
    If cmdGrid.Visible = True Then
        MSChart1.Visible = True
        MostrarGrafico sdbcTipoGrafico.Value
    End If
    
End Sub

Private Sub cmdBorrar_Click()
    sdbcGMN1_4Cod = ""
    BorrarDatosTotales
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo máximo=0</remarks>
Private Sub cmdCalendar_Click()
    AbrirFormCalendar Me, sdbcFecReu
End Sub

Private Sub cmdGrafico_Click()
    
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
'       sdbcTipoGrafico.Value = "Barras 3D"
        sdbcTipoGrafico.Value = sIdiTipoGrafico(2)
        MostrarGrafico sdbcTipoGrafico.Value
        picTipoGrafico.Visible = True
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdGrid_Click()
        
        picTipoGrafico.Visible = False
        picLegend.Visible = False
        picLegend2.Visible = False
        
        picLegend.Visible = False
        picLegend2.Visible = False
        
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        sdbgRes.Visible = True
        MSChart1.Visible = False
    
End Sub

Private Sub cmdImprimir_Click()
    Set ofrmLstAhorroNeg = New frmLstINFAhorrosNeg
    ofrmLstAhorroNeg.sOrigen = "frmInfAhorroNegMatReu"
  
    ofrmLstAhorroNeg.WindowState = vbNormal
    
    If sdbcFecReu <> "" Then
        ofrmLstAhorroNeg.sdbcFecReu.Columns(0).Value = sdbcFecReu.Columns(0).Value
        ofrmLstAhorroNeg.sdbcFecReu.Text = sdbcFecReu.Text
    End If
    ofrmLstAhorroNeg.sdbcMon = sdbcMon
    ofrmLstAhorroNeg.sdbcMon_Validate False
    ' hay que crear los objetos oGMNxSeleccionado para las restricciones por material
    ofrmLstAhorroNeg.PonerMatSeleccionado ("frmInfAhorroNegMatReu")
    ofrmLstAhorroNeg.Show 1


End Sub

Private Sub cmdSelMat_Click()
    
    frmSELMAT.sOrigen = "frmInfAhorroNegMatReu"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Hide
    frmSELMAT.Show 1

End Sub

Private Sub Form_Activate()
    sdbgRes.SelBookmarks.RemoveAll
End Sub

Private Sub Form_Load()
    
    Me.Height = 6285
    Me.Width = 10560
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    ConfigurarNombres
        
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
   
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If

    Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & ""
    
End Sub

Private Sub Form_Resize()
    
    If Me.Width > 150 Then
        
        sdbgRes.Width = Me.Width - 120
        
        sdbgRes.Columns(0).Width = sdbgRes.Width * 0.1
        sdbgRes.Columns(1).Width = sdbgRes.Width * 0.25
        sdbgRes.Columns(2).Width = sdbgRes.Width * 0.1
        sdbgRes.Columns(3).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(4).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(5).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.1 - 500
        
        sdbgTotales.Top = Me.Height - 780
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Columns(0).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(1).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(2).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(3).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(4).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(5).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(6).Width = sdbgTotales.Width / 7
        
        MSChart1.Width = Me.Width - 120
        
    End If
    
    If Me.Height > 2600 Then
        sdbgRes.Height = Me.Height - 2600
        MSChart1.Height = Me.Height - 2300
    End If
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oGestorReuniones = Nothing
    Set oReuniones = Nothing
    Set ADORs = Nothing
    Set oMonedas = Nothing
    Set oMon = Nothing
    Me.Visible = False
    
End Sub

Private Sub sdbcFecReu_Change()
 
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        If cmdGrafico.Visible Then
            sdbgRes.RemoveAll
        Else
            MSChart1.Visible = False
        End If
        BorrarDatosTotales
        lblref = ""
    End If

End Sub

Private Sub sdbcFecReu_CloseUp()
    
    If sdbcFecReu.Value = "" Then
        Exit Sub
    End If
    
    MSChart1.Visible = False
    lblref = sdbcFecReu.Columns(2).Text
    sdbcFecReu.Text = sdbcFecReu.Columns(1).Text
    sdbgRes.RemoveAll
    BorrarDatosTotales
    bCargarComboDesde = False
    bRespetarCombo = False
    
End Sub

Private Sub sdbcFecReu_DropDown()
    Dim oReu As CReunion

    Screen.MousePointer = vbHourglass
    sdbcFecReu.RemoveAll
        
    If bCargarComboDesde Then
            
        If IsDate(sdbcFecReu) Then
            Set oReuniones = oGestorReuniones.DevolverReuniones(sdbcFecReu, , , True)
        Else
            Set oReuniones = oGestorReuniones.DevolverReuniones(, , , True)
        End If
    
    Else
        Set oReuniones = oGestorReuniones.DevolverReuniones(, , , True)
        
    End If
    
    For Each oReu In oReuniones
        sdbcFecReu.AddItem oReu.Fecha & Chr(m_lSeparador) & Format(oReu.Fecha, "short date") & " " & Format(oReu.Fecha, "short time") & Chr(m_lSeparador) & oReu.Referencia
    Next
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFecReu_InitColumnProps()
    
    sdbcFecReu.DataFieldList = "Column 0"
    sdbcFecReu.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcFecReu_PositionList(ByVal Text As String)
PositionList sdbcFecReu, Text
End Sub

Private Sub sdbcFecReu_Validate(Cancel As Boolean)

    If Trim(sdbcFecReu.Text = "") Then Exit Sub
    
    If Not IsDate(sdbcFecReu) Then
        oMensajes.NoValido sIdiFecha
        Exit Sub
    End If
    
    If sdbcFecReu.Text = sdbcFecReu.Columns(1).Text Then
        lblref = sdbcFecReu.Columns(2).Text
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    Set oReuniones = oGestorReuniones.DevolverReuniones(sdbcFecReu.Text, sdbcFecReu.Text, , True)
    Screen.MousePointer = vbNormal
    
    If oReuniones.Count = 0 Then
        sdbcFecReu.Text = ""
        oMensajes.NoValido sIdiReu
    Else
        lblref = oReuniones.Item(1).Referencia
    End If
    
End Sub

Private Sub CargarGrid()
    Dim dpres As Double
    Dim dadj As Double

    dpres = 0
    dadj = 0
    
    sdbgRes.RemoveAll
    sdbgTotales.RemoveAll
        
    While Not ADORs.EOF
        
        sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
        dpres = dpres + dequivalencia * ADORs(3).Value
        dadj = dadj + dequivalencia * ADORs(4).Value
        ADORs.MoveNext
        
    Wend
    ADORs.Close
    Set ADORs = Nothing
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & dpres & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & dadj & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & dpres - dadj
   
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    Select Case Tipo
    
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                picLegend.Visible = True
                picLegend2.Visible = False
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar

                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                picLegend.Visible = False
                picLegend2.Visible = True
                
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                picLegend.Visible = True
                picLegend2.Visible = False
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
        
End Sub

Private Sub sdbcGMN1_4Den_Change()
        
    If Not bRespetarCombo Then
    
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        
        bRespetarCombo = True
        sdbcGMN1_4Cod = ""
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
        bRespetarCombo = False
        
        bRespetarCombo = True
        sdbgRes.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
     If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , True, False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
           
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "....."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN4_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        
        Set oGMN4Seleccionado = Nothing
       sdbgRes.RemoveAll
    End If
    
End Sub


Private Sub sdbcGMN4_4Cod_Click()
If sdbcGMN4_4Cod.DroppedDown Then Exit Sub
    
    If sdbcGMN4_4Cod.Value = "" Or sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    sdbcGMN4_4Cod.ListAutoPosition = False
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Value
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Value
    sdbcGMN4_4Cod.ListAutoPosition = True
    DoEvents
    GMN4Seleccionado
End Sub

Private Sub sdbcGMN4_4Cod_CloseUp()
    
    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    bRespetarCombo = False
    GMN4Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN4_4Cod.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oGruposMN3 = Nothing
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
        
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , False, False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    End If
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN4_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Cod_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        Set oGMN4Seleccionado = Nothing
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
          
    End If
End Sub

Private Sub sdbcGMN4_4Den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        sdbgRes.RemoveAll
    End If
    
End Sub


Private Sub sdbcGMN4_4Den_Click()
If sdbcGMN4_4Den.DroppedDown Then Exit Sub
    
    If sdbcGMN4_4Den.Value = "" Or sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    sdbcGMN4_4Den.ListAutoPosition = False
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Value
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Value
    sdbcGMN4_4Den.ListAutoPosition = True
    DoEvents
    GMN4Seleccionado
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()
    
    If sdbcGMN4_4Den.Value = "....." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN4Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN4_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN4_4Den.RemoveAll
    
    Set oGruposMN4 = Nothing
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , Trim(sdbcGMN4_4Den), True, False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN4_4Den), True, False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , True, False)
        Else
             
             oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    End If
    
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "....."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Den_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        Set oGMN4Seleccionado = Nothing
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        
    End If
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgRes_DblClick()
Dim frm As frmInfAhorroNegFechaDetalle
Dim ADORs As Ador.Recordset
        
        If sdbgRes.Rows = 0 Then Exit Sub
        If sdbgRes.Row < 0 Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        If sdbcGMN3_4Cod <> "" Then
            'Sacaremos el detalle de los procesos
            Set ADORs = oGestorInformes.AhorroNegociadoFecha(CDate(sdbcFecReu.Columns(0).Value), sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, sdbgRes.Columns(0).Value, , , , True, , , , , True)
    
            Set frm = New frmInfAhorroNegFechaDetalle
            frm.sDetalle = "GMN4"
            frm.sGmn1 = sdbcGMN1_4Cod.Text
            frm.sGmn2 = sdbcGMN2_4Cod.Text
            frm.sGmn3 = sdbcGMN3_4Cod.Text
            frm.sGmn4 = sdbgRes.Columns(0).Value 'En vez de sdbcGMN4_4Cod.Text pq es parametro de ADORs
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            frm.caption = sIdiDetResult & "     " & sdbcFecReu
            frm.dequivalencia = dequivalencia
            frm.sFecha = sdbcFecReu.Columns(0).Value
            
            If Not ADORs Is Nothing Then
                While Not ADORs.EOF
                    frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                    ADORs.MoveNext
                Wend
            End If
            ADORs.Close
            Set ADORs = Nothing
            frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 1000
            frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm.Show 1
            sdbgRes.SelBookmarks.RemoveAll
        Else
            If sdbcGMN2_4Cod <> "" Then
                bRespetarCombo = True
                sdbcGMN3_4Cod = sdbgRes.Columns(0).Value
                sdbcGMN3_4Den = sdbgRes.Columns(1).Value
                bRespetarCombo = False
                GMN3Seleccionado
                cmdActualizar_Click
                Screen.MousePointer = vbNormal
            Else
                If sdbcGMN1_4Cod <> "" Then
                    bRespetarCombo = True
                    sdbcGMN2_4Cod = sdbgRes.Columns(0).Value
                    sdbcGMN2_4Den = sdbgRes.Columns(1).Value
                    bRespetarCombo = False
                    GMN2Seleccionado
                    cmdActualizar_Click
                    Screen.MousePointer = vbNormal
                Else
                    bRespetarCombo = True
                    sdbcGMN1_4Cod = sdbgRes.Columns(0).Value
                    sdbcGMN1_4Den = sdbgRes.Columns(1).Value
                    bRespetarCombo = False
                    GMN1Seleccionado
                    cmdActualizar_Click
                    Screen.MousePointer = vbNormal
                End If
            End If
        End If
    
        Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
 
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegMatRestMat)) Is Nothing) And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
        bRMat = True
    End If
End Sub
Private Sub ConfigurarNombres()

lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
lblGMN2_4.caption = gParametrosGenerales.gsABR_GMN2 & ":"
lblGMN3_4.caption = gParametrosGenerales.gsabr_GMN3 & ":"
lblGMN4_4.caption = gParametrosGenerales.gsabr_GMN4 & ":"
End Sub

 Public Sub PonerMatSeleccionadoEnCombos()
    
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    
    BorrarDatosTotales
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    If Not oGMN1Seleccionado Is Nothing Then
      
        bRespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Den.Text = oGMN1Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN1_4Den.Text = ""
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    If Not oGMN2Seleccionado Is Nothing Then
       
        bRespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Den.Text = oGMN2Seleccionado.Den
        bRespetarCombo = False
        GMN2Seleccionado
    Else
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN2_4Den.Text = ""
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    If Not oGMN3Seleccionado Is Nothing Then
  
        bRespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Den.Text = oGMN3Seleccionado.Den
        bRespetarCombo = False
        GMN3Seleccionado
    Else
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    If Not oGMN4Seleccionado Is Nothing Then
     
        bRespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Den.Text = oGMN4Seleccionado.Den
        bRespetarCombo = False
        GMN4Seleccionado
    Else
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
    End If
    
           
End Sub


Private Sub sdbcGMN1_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        
        bRespetarCombo = True
        sdbcGMN1_4Den = ""
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
        bRespetarCombo = False
        
        MSChart1.Visible = False
        sdbgRes.RemoveAll
        BorrarDatosTotales
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    
       
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
 
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
           
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
           
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub
Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
        
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
        bRespetarCombo = False
        GMN1Seleccionado
        Exit Sub
    End If
        
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text Then
        bRespetarCombo = True
        sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
        bRespetarCombo = False
        GMN1Seleccionado
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
           
            bCargarComboDesde = False
            
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
       
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
            
            bRespetarCombo = True
            sdbcGMN1_4Den = oGMN1.Den
            bRespetarCombo = False
            bCargarComboDesde = False
            
        End If
        
    End If
    
    Screen.MousePointer = vbNormal
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN2_4Cod_CloseUp()
    
    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN2Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN2_4Cod.RemoveAll
    
    Set oGruposMN2 = Nothing
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
         
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , False, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , False, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    End If
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_InitColumnProps()

    sdbcGMN2_4Cod.DataFieldList = "Column 0"
    sdbcGMN2_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub
Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN2 As CGrupoMatNivel2
    Dim oIMAsig As IMaterialAsignado
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        
        Screen.MousePointer = vbHourglass
        Set oGruposMN2 = Nothing
        Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        If oGruposMN2 Is Nothing Then
            'No existe
            sdbcGMN2_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN2.Item(1) Is Nothing Then
                'No existe
                sdbcGMN2_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN2_4Den.Text = oGruposMN2.Item(1).Den
                bRespetarCombo = False
                Set oGMN2Seleccionado = oGruposMN2.Item(1)
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN2 = Nothing
        Set oGMN2 = oFSGSRaiz.generar_CGrupoMatNivel2
  
        oGMN2.GMN1Cod = sdbcGMN1_4Cod
        oGMN2.Cod = sdbcGMN2_4Cod
        Set oIBaseDatos = oGMN2
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN2_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN2_4Den.Text = oGMN2.Den
            bRespetarCombo = False
            Set oGMN2Seleccionado = oGMN2
            bCargarComboDesde = False
        End If
    
    End If
    
    
    Set oGMN2 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN2 = Nothing
    Set oIMAsig = Nothing
    
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN2_4Den_CloseUp()
    
    If sdbcGMN2_4Den.Value = "....." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN2Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN2_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , Trim(sdbcGMN2_4Den), True, False)
        Else
        
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN2_4Den), True, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , True, False)
        Else
        
             oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    End If
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "....."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()

    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub


Private Sub sdbcGMN3_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        
    End If
    
End Sub

Private Sub sdbcGMN3_4Cod_CloseUp()
    
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN3Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN3_4Cod.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oGruposMN3 = Nothing
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
         
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , False, False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    End If
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Cod_InitColumnProps()

    sdbcGMN3_4Cod.DataFieldList = "Column 0"
    sdbcGMN3_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub
Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN3 As CGrupoMatNivel3
    Dim oIMAsig As IMaterialAsignado
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    If sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    ''' Solo continuamos si existe el grupo
    
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        
        Screen.MousePointer = vbHourglass
        Set oGruposMN3 = Nothing
        Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        If oGruposMN3 Is Nothing Then
            'No existe
            sdbcGMN3_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN2.Item(1) Is Nothing Then
                'No existe
                sdbcGMN3_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN3_4Den.Text = oGruposMN3.Item(1).Den
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN3 = Nothing
        Set oGMN3 = oFSGSRaiz.generar_CGrupoMatNivel3
        
        oGMN3.GMN1Cod = sdbcGMN1_4Cod
        oGMN3.GMN2Cod = sdbcGMN2_4Cod
        oGMN3.Cod = sdbcGMN3_4Cod
        Set oIBaseDatos = oGMN3
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN3_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN3_4Den.Text = oGMN3.Den
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    
    End If
    
    GMN3Seleccionado
    
    Set oGMN3 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN3 = Nothing
    Set oIMAsig = Nothing
    
End Sub
Private Sub sdbcGMN3_4den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
    
    If sdbcGMN3_4Den.Value = "....." Or sdbcGMN3_4Den.Value = "" Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN3Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN3_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN3_4Den.RemoveAll
    
    Set oGruposMN3 = Nothing
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , Trim(sdbcGMN3_4Den), True, False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN3_4Den), True, False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , True, False)
        Else
             
             oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    End If
    
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "....."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()

    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub

Private Sub sdbcGMN4_4Cod_InitColumnProps()

    sdbcGMN4_4Cod.DataFieldList = "Column 0"
    sdbcGMN4_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub
Private Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN4 As CGrupoMatNivel4
    Dim oIMAsig As IMaterialAsignado
    
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    If sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       
        Screen.MousePointer = vbHourglass
        Set oGruposMN4 = Nothing
        Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        If oGruposMN4 Is Nothing Then
            'No existe
            sdbcGMN4_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN4.Item(1) Is Nothing Then
                'No existe
                sdbcGMN4_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN4_4Den.Text = oGruposMN4.Item(1).Den
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN4 = Nothing
        Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
       
        oGMN4.GMN1Cod = sdbcGMN1_4Cod
        oGMN4.GMN2Cod = sdbcGMN2_4Cod
        oGMN4.GMN3Cod = sdbcGMN3_4Cod
        oGMN4.Cod = sdbcGMN4_4Cod
        Set oIBaseDatos = oGMN4
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN4_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN4_4Den.Text = oGMN4.Den
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    
    End If
    
    GMN4Seleccionado
    
    Set oGMN4 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN4 = Nothing
    Set oIMAsig = Nothing
    
End Sub



Private Sub sdbcGMN4_4Den_InitColumnProps()

    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub

Private Sub GMN1Seleccionado()
        
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    sdbcGMN2_4Cod = ""
    sdbcGMN2_4Den = ""
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
    
    sdbgRes.RemoveAll
    BorrarDatosTotales
    MSChart1.Visible = False
    
End Sub

Private Sub GMN2Seleccionado()
        
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod.Text
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    sdbcGMN3_4Cod = ""
    
    sdbgRes.RemoveAll
    BorrarDatosTotales
    MSChart1.Visible = False
   
End Sub
Private Sub GMN3Seleccionado()

    Set oGMN3Seleccionado = Nothing
    
    If sdbcGMN3_4Cod.Text = "" Or sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
  
    sdbcGMN4_4Cod = ""
    
    sdbgRes.RemoveAll
    BorrarDatosTotales
    MSChart1.Visible = False

End Sub

Private Sub GMN4Seleccionado()
    
    Set oGMN4Seleccionado = Nothing
    
    If sdbcGMN4_4Cod.Text = "" Or sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN3_4Cod
   
    sdbgRes.RemoveAll
    BorrarDatosTotales
    MSChart1.Visible = False
        
End Sub
Private Sub BorrarDatosTotales()
    sdbgTotales.RemoveAll
    sdbgTotales.Columns(6).CellStyleSet "Normal"
    sdbgTotales.Refresh
End Sub
Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
    If sdbgTotales.Columns(6).Value = "" Then Exit Sub
    
    If sdbgTotales.Columns(6).Value < 0 Then
        sdbgTotales.Columns(6).CellStyleSet "Red"
    Else
        If sdbgTotales.Columns(6).Value > 0 Then
            sdbgTotales.Columns(6).CellStyleSet "Green"
        End If
    End If
End Sub
Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        bMonCargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
       
End Sub

Private Sub sdbcMon_DropDown()
  
    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)
PositionList sdbcMon, Text
End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
    End If
    
End Sub



Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_NEGMATREU, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        lblFecReu.caption = Ador(0).Value & ":"
        sIdiFecha = Ador(0).Value
        sdbcFecReu.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        lblrefl.caption = Ador(0).Value & ":"
        sdbcFecReu.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value 'Código
        sdbgRes.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value '5 Denominación
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value '10
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        fraMaterial.caption = Ador(0).Value
        sIdiMat = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        sIdiPresup = Ador(0).Value 'Presupuestos
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value
        sIdiAdj = Ador(0).Value 'Adjudicado
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).Value '20
        sIdiAhor = Ador(0).Value 'Ahorro
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value 'Moneda central
        Ador.MoveNext
        sIdiTotal = Ador(0).Value 'Total
        Ador.MoveNext
        sIdiReu = Ador(0).Value 'Presupuestos '25
        Ador.MoveNext
        sIdiDetResult = Ador(0).Value 'Detalle de resultado con fecha
        Ador.Close
    
    End If

    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGMATREU_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGMATREU_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)

    Set Ador = Nothing



End Sub

