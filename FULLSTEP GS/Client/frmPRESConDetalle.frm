VERSION 5.00
Begin VB.Form frmPRESConDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Presupuestos por partidas contables"
   ClientHeight    =   2460
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   Icon            =   "frmPRESConDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2460
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1875
      Left            =   1395
      ScaleHeight     =   1875
      ScaleWidth      =   3210
      TabIndex        =   7
      Top             =   75
      Width           =   3210
      Begin VB.TextBox txtObj 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   3
         Top             =   1500
         Width           =   720
      End
      Begin VB.TextBox txtImp 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   2
         Top             =   1080
         Width           =   1530
      End
      Begin VB.TextBox txtDen 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         MaxLength       =   100
         TabIndex        =   1
         Top             =   660
         Width           =   3165
      End
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   0
         Top             =   210
         Width           =   1530
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   600
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   2040
      Width           =   3825
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1650
         TabIndex        =   5
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   510
         TabIndex        =   4
         Top             =   60
         Width           =   1005
      End
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Objetivo %:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1620
      Width           =   1350
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Importe:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   1200
      Width           =   1350
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   360
      Width           =   1350
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   780
      Width           =   1350
   End
End
Attribute VB_Name = "frmPRESConDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private sIdioma() As String
Private Sub cmdAceptar_Click()

Dim oPRES1 As CPresConNivel1
Dim oPRES2 As CPresconNivel2
Dim oPRES3 As CPresConNivel3
Dim oPRES4 As CPresconNivel4
Dim oIBaseDatos As IBaseDatos
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

txtImp_LostFocus
txtObj_LostFocus

Select Case frmPresupuestos2.Accion
    
    Case accionessummit.ACCPresConNivel1Anya
        
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdioma(1)
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdioma(2)
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdioma(3)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdioma(4)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            Set oPRES1 = oFSGSRaiz.Generar_CPresConNivel1
            oPRES1.Anyo = frmPresupuestos2.sdbcAnyo
            oPRES1.Cod = Trim(txtCod)
            oPRES1.Den = Trim(txtDen)
            oPRES1.importe = StrToDblOrNull(txtImp)
            oPRES1.Objetivo = StrToDblOrNull(txtObj)
            'A�adir UON(s)
            If frmPresupuestos2.m_sUON1 <> "" Then oPRES1.UON1 = frmPresupuestos2.m_sUON1
            If frmPresupuestos2.m_sUON2 <> "" Then oPRES1.UON2 = frmPresupuestos2.m_sUON2
            If frmPresupuestos2.m_sUON3 <> "" Then oPRES1.UON3 = frmPresupuestos2.m_sUON3
          
            Set oIBaseDatos = oPRES1
            
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError = TESnoerror Then
                AnyadirPRES1AEstructura
                RegistrarAccion accionessummit.ACCPresConNivel1Anya, "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                If Me.Visible Then txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set oPRES1 = Nothing
            Set oIBaseDatos = Nothing
           
            
    Case ACCPresConNivel2Anya
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdioma(1)
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdioma(2)
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdioma(3)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdioma(4)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperior Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            Set oPRES2 = oFSGSRaiz.Generar_CPresconNivel2
            oPRES2.Anyo = frmPresupuestos2.sdbcAnyo
            oPRES2.Cod = Trim(txtCod)
            oPRES2.CodPRES1 = frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem)
            oPRES2.Den = Trim(txtDen)
            oPRES2.importe = StrToDblOrNull(txtImp)
            oPRES2.Objetivo = StrToDblOrNull(txtObj)
            'A�adir UON(s)
            If frmPresupuestos2.m_sUON1 <> "" Then oPRES2.UON1 = frmPresupuestos2.m_sUON1
            If frmPresupuestos2.m_sUON2 <> "" Then oPRES2.UON2 = frmPresupuestos2.m_sUON2
            If frmPresupuestos2.m_sUON3 <> "" Then oPRES2.UON3 = frmPresupuestos2.m_sUON3
            
            Set oIBaseDatos = oPRES2
            
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError = TESnoerror Then
                AnyadirPRES2AEstructura
                RegistrarAccion ACCPresConNivel2Anya, "CodPRES1:" & frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem) & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                If Me.Visible Then txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set oPRES2 = Nothing
            Set oIBaseDatos = Nothing
           
    Case accionessummit.ACCPresConNivel3Anya
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdioma(1)
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdioma(2)
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdioma(3)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdioma(4)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperior Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            Set oPRES3 = oFSGSRaiz.Generar_CPresConNivel3
            oPRES3.Anyo = frmPresupuestos2.sdbcAnyo
            oPRES3.CodPRES1 = frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem.Parent)
            oPRES3.CodPRES2 = frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem)
            oPRES3.Cod = Trim(txtCod)
            oPRES3.Den = Trim(txtDen)
            oPRES3.importe = StrToDblOrNull(txtImp)
            oPRES3.Objetivo = StrToDblOrNull(txtObj)
            'A�adir UON(s)
            If frmPresupuestos2.m_sUON1 <> "" Then oPRES3.UON1 = frmPresupuestos2.m_sUON1
            If frmPresupuestos2.m_sUON2 <> "" Then oPRES3.UON2 = frmPresupuestos2.m_sUON2
            If frmPresupuestos2.m_sUON3 <> "" Then oPRES3.UON3 = frmPresupuestos2.m_sUON3
                        
            Set oIBaseDatos = oPRES3
            
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError = TESnoerror Then
                AnyadirPRES3AEstructura
                RegistrarAccion ACCPresConNivel3Anya, "CodPRES1:" & frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem.Parent) & "CodPRES2:" & frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem) & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                If Me.Visible Then txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set oPRES3 = Nothing
            Set oIBaseDatos = Nothing
           
    Case accionessummit.ACCPresConNivel4Anya
            
            '********* Validar datos *********
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdioma(1)
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdioma(2)
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdioma(3)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdioma(4)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperior Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            Set oPRES4 = oFSGSRaiz.Generar_CPresconNivel4
            oPRES4.Anyo = frmPresupuestos2.sdbcAnyo
            oPRES4.CodPRES1 = frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem.Parent.Parent)
            oPRES4.CodPRES2 = frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem.Parent)
            oPRES4.CodPRES3 = frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem)
            oPRES4.Cod = Trim(txtCod)
            oPRES4.Den = Trim(txtDen)
            oPRES4.importe = StrToDblOrNull(txtImp)
            oPRES4.Objetivo = StrToDblOrNull(txtObj)
            'A�adir UON(s)
            If frmPresupuestos2.m_sUON1 <> "" Then oPRES4.UON1 = frmPresupuestos2.m_sUON1
            If frmPresupuestos2.m_sUON2 <> "" Then oPRES4.UON2 = frmPresupuestos2.m_sUON2
            If frmPresupuestos2.m_sUON3 <> "" Then oPRES4.UON3 = frmPresupuestos2.m_sUON3
                     
            Set oIBaseDatos = oPRES4
            
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError = TESnoerror Then
                AnyadirPRES4AEstructura
                RegistrarAccion ACCPresConNivel4Anya, "CodPRES1:" & frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem.Parent.Parent) & "CodPRES2:" & frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem.Parent) & "CodPRES3:" & frmPresupuestos2.DevolverCod(frmPresupuestos2.tvwestrPres.selectedItem) & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                If Me.Visible Then txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set oPRES4 = Nothing
            Set oIBaseDatos = Nothing
           
    
    Case ACCPresConNivel1Mod
            
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdioma(1)
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdioma(2)
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdioma(3)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdioma(4)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperiorMod Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            frmPresupuestos2.oPres1Seleccionado.Den = Trim(txtDen)
            frmPresupuestos2.oPres1Seleccionado.importe = StrToDblOrNull(txtImp)
            frmPresupuestos2.oPres1Seleccionado.Objetivo = StrToDblOrNull(txtObj)
            teserror = frmPresupuestos2.oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError = TESnoerror Then
                ModificarPRESEnEstructura
                RegistrarAccion ACCPresConNivel1Mod, "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
    Case ACCPresConNivel2Mod
            
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdioma(1)
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdioma(2)
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdioma(3)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdioma(4)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperiorMod Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            frmPresupuestos2.oPres2Seleccionado.Den = Trim(txtDen)
            frmPresupuestos2.oPres2Seleccionado.importe = StrToDblOrNull(txtImp)
            frmPresupuestos2.oPres2Seleccionado.Objetivo = StrToDblOrNull(txtObj)
            
            teserror = frmPresupuestos2.oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError = TESnoerror Then
                ModificarPRESEnEstructura
                RegistrarAccion ACCPresConNivel2Mod, "CodPRES1:" & frmPresupuestos2.oPres2Seleccionado.CodPRES1 & "Cod:" & frmPresupuestos2.oPres2Seleccionado.Cod
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
           
    Case ACCPresConNivel3Mod
            
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdioma(1)
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdioma(2)
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdioma(3)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdioma(4)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperiorMod Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            frmPresupuestos2.oPres3Seleccionado.Den = Trim(txtDen)
            frmPresupuestos2.oPres3Seleccionado.importe = StrToDblOrNull(txtImp)
            frmPresupuestos2.oPres3Seleccionado.Objetivo = StrToDblOrNull(txtObj)
            
            teserror = frmPresupuestos2.oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError = TESnoerror Then
                ModificarPRESEnEstructura
                RegistrarAccion ACCPresConNivel3Mod, "CodPRES1:" & frmPresupuestos2.oPres3Seleccionado.CodPRES1 & "CodPRES2:" & frmPresupuestos2.oPres3Seleccionado.CodPRES2 & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                Exit Sub
            End If
            
    Case ACCPresConNivel4MOd
        
            If Trim(txtCod) = "" Then
                oMensajes.NoValido sIdioma(1)
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDen) = "" Then
                oMensajes.NoValida sIdioma(2)
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            
            If Trim(txtImp) <> "" Then
                If Not IsNumeric(txtImp) Then
                    oMensajes.NoValido sIdioma(3)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
            
            If Trim(txtObj) <> "" Then
                If Not IsNumeric(txtObj) Then
                    oMensajes.NoValido sIdioma(4)
                    If Me.Visible Then txtImp.SetFocus
                    Exit Sub
                End If
            End If
        
            'Hay que comprobar que el importe no supere el presupuesto superior
            If Trim(txtImp) <> "" Then
                If EsimporteSuperiorMod Then
                   irespuesta = oMensajes.PreguntaPresImporteSuperior
                   If irespuesta = vbNo Then
                        'cmdCancelar_Click
                        'Unload Me
                        Exit Sub
                    End If
                End If
            End If
            
            
            Screen.MousePointer = vbHourglass
            frmPresupuestos2.oPres4Seleccionado.importe = txtImp
            frmPresupuestos2.oPres4Seleccionado.Den = Trim(txtDen)
            frmPresupuestos2.oPres4Seleccionado.importe = StrToDblOrNull(txtImp)
            frmPresupuestos2.oPres4Seleccionado.Objetivo = StrToDblOrNull(txtObj)
                        
            teserror = frmPresupuestos2.oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError = TESnoerror Then
                ModificarPRESEnEstructura
                RegistrarAccion ACCPresConNivel4MOd, "CodPRES1:" & frmPresupuestos2.oPres4Seleccionado.CodPRES1 & "CodPRES2:" & frmPresupuestos2.oPres4Seleccionado.CodPRES2 & "CodPRES3:" & frmPresupuestos2.oPres4Seleccionado.CodPRES3 & "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
           
End Select

Screen.MousePointer = vbNormal

Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESCON_DETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 4)
        For i = 1 To 4
            sIdioma(i) = Ador(0).value
            Ador.MoveNext
        Next
        Label1.caption = sIdioma(1) & ":"
        Label2.caption = sIdioma(2) & ":"
        Label3.caption = sIdioma(3) & ":"
        Label4.caption = sIdioma(4) & " %:"
        cmdAceptar.caption = Ador(0).value '5
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).value
        'Ador.MoveNext
        'frmPRESConDetalle.Caption = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Activate()

Select Case frmPresupuestos2.Accion

    Case ACCPresConNivel1Eli, ACCPresConNivel2Eli, ACCPresConNivel3Eli, ACCPresConNivel4Eli
            
        picDatos.Enabled = False
    
    Case ACCPresConNivel1Mod, ACCPresConNivel2Mod, ACCPresConNivel3Mod, ACCPresConNivel4MOd
            
        txtCod.Enabled = False
        If Me.Visible Then txtDen.SetFocus
        
    Case ACCPresConnivel1Det, ACCPresConnivel2Det, ACCPresConnivel3Det, ACCPresConnivel4Det
        
        picDatos.Enabled = False
        picEdit.Visible = False
    
    Case Else
        If Me.Visible Then txtCod.SetFocus

End Select
End Sub

Private Sub Form_Load()

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
Select Case frmPresupuestos2.Accion
            
    Case ACCPresConNivel1Anya
                
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESCON1
                            
    Case ACCPresConNivel2Anya
            
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESCON2
    
    Case ACCPresConNivel3Anya
            
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESCON3
    
    Case ACCPresConNivel4Anya
            
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESCON4
            
End Select

End Sub

Private Sub Form_Unload(Cancel As Integer)

Select Case frmPresupuestos2.Accion
            
    Case ACCPresConNivel1Mod, ACCPresConNivel2Mod, ACCPresConNivel3Mod, ACCPresConNivel4MOd, ACCPresConNivel1Eli, ACCPresConNivel2Eli, ACCPresConNivel3Eli, ACCPresConNivel4Eli
    
            frmPresupuestos2.oIBaseDatos.CancelarEdicion
End Select

Set frmPresupuestos2.oIBaseDatos = Nothing
Set frmPresupuestos2.oPres1Seleccionado = Nothing
Set frmPresupuestos2.oPres2Seleccionado = Nothing
Set frmPresupuestos2.oPres3Seleccionado = Nothing
Set frmPresupuestos2.oPres4Seleccionado = Nothing

frmPresupuestos2.Accion = ACCPresConCon

End Sub
Public Function AnyadirPRES1AEstructura()
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String

Set nodx = frmPresupuestos2.tvwestrPres.selectedItem
scod1 = Trim(txtCod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(Trim(txtCod)))
Set nodo = frmPresupuestos2.tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, Trim(txtCod) & " - " & Trim(txtDen), "PRES1")

 
'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
frmPresupuestos2.oPresupuestos.Add frmPresupuestos2.sdbcAnyo, Trim(txtCod), Trim(txtDen), StrToDblOrNull(txtImp), StrToDblOrNull(txtObj)

If frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2 Is Nothing Then
    Set frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2 = oFSGSRaiz.Generar_CPresContablesNivel2
End If

nodo.Tag = "PRES1" & Trim(txtCod)
nodo.Selected = True
nodo.EnsureVisible
frmPresupuestos2.tvwEstrPres_NodeClick nodo
Set nodo = Nothing
Set nodx = Nothing

End Function
Public Function AnyadirPRES2AEstructura()
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String

Set nodx = frmPresupuestos2.tvwestrPres.selectedItem
scod1 = frmPresupuestos2.DevolverCod(nodx)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
scod2 = Trim(txtCod)
scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
Set nodo = frmPresupuestos2.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES2" & scod1 & scod2, Trim(txtCod) & " - " & Trim(txtDen), "PRES2")
nodo.Tag = "PRES2" & Trim(txtCod)

'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2 Is Nothing Then
    Set frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2 = oFSGSRaiz.Generar_CPresContablesNivel2
End If

frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Add frmPresupuestos2.sdbcAnyo, scod1, Trim(txtCod), Trim(txtDen), StrToDblOrNull(txtImp), StrToDblOrNull(txtObj)

If frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3 Is Nothing Then
    Set frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3 = oFSGSRaiz.Generar_CPresContablesNivel3
End If

frmPresupuestos2.tvwEstrPres_NodeClick nodo

nodo.Selected = True
nodo.EnsureVisible
Set nodo = Nothing
Set nodx = Nothing

End Function
Public Function AnyadirPRES3AEstructura()
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String

Set nodx = frmPresupuestos2.tvwestrPres.selectedItem
scod1 = frmPresupuestos2.DevolverCod(nodx.Parent)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
scod2 = frmPresupuestos2.DevolverCod(nodx)
scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
scod3 = Trim(txtCod)
scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))

'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3 Is Nothing Then
    Set frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3 = oFSGSRaiz.Generar_CPresContablesNivel3
End If

frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Add frmPresupuestos2.sdbcAnyo, scod1, scod2, Trim(txtCod), Trim(txtDen), StrToDblOrNull(txtImp), StrToDblOrNull(txtObj)

Set nodo = frmPresupuestos2.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES3" & scod1 & scod2 & scod3, Trim(txtCod) & " - " & Trim(txtDen), "PRES3")

If frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4 Is Nothing Then
    Set frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4 = oFSGSRaiz.Generar_CPresContablesNivel4
End If

nodo.Tag = "PRES3" & Trim(txtCod)
frmPresupuestos2.tvwEstrPres_NodeClick nodo
nodo.Selected = True
nodo.EnsureVisible
Set nodo = Nothing
Set nodx = Nothing

End Function
Public Function AnyadirPRES4AEstructura()
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Set nodx = frmPresupuestos2.tvwestrPres.selectedItem
scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
scod2 = frmPresupuestos2.DevolverCod(nodx.Parent)
scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
scod3 = frmPresupuestos2.DevolverCod(nodx)
scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
scod4 = Trim(txtCod)
scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(scod4))
'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4 Is Nothing Then
    Set frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4 = oFSGSRaiz.Generar_CPresContablesNivel4
End If

frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Add frmPresupuestos2.sdbcAnyo, scod1, scod2, scod3, Trim(txtCod), Trim(txtDen), StrToDblOrNull(txtImp), StrToDblOrNull(txtObj)

Set nodo = frmPresupuestos2.tvwestrPres.Nodes.Add(nodx.key, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, Trim(txtCod) & " - " & Trim(txtDen), "PRES4")
nodo.Tag = "PRES4" & Trim(txtCod)
frmPresupuestos2.tvwEstrPres_NodeClick nodo
nodo.Selected = True
nodo.EnsureVisible
Set nodo = Nothing
Set nodx = Nothing

End Function
Public Function ModificarPRESEnEstructura()

Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Set nodx = frmPresupuestos2.tvwestrPres.selectedItem
nodx.Text = Trim(txtCod) & " - " & Trim(txtDen)

Select Case Left(nodx.Tag, 5)
    
    Case "PRES1"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                    
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            frmPresupuestos2.oPresupuestos.Item(scod1).Den = Trim(txtDen)
            frmPresupuestos2.oPresupuestos.Item(scod1).importe = StrToDblOrNull(txtImp)
            frmPresupuestos2.oPresupuestos.Item(scod1).Objetivo = StrToDblOrNull(txtObj)
            
    Case "PRES2"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Den = Trim(txtDen)
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe = StrToDblOrNull(txtImp)
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Objetivo = StrToDblOrNull(txtObj)
            
    Case "PRES3"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = frmPresupuestos2.DevolverCod(nodx)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Den = Trim(txtDen)
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe = StrToDblOrNull(txtImp)
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Objetivo = StrToDblOrNull(txtObj)
            
    Case "PRES4"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            scod4 = frmPresupuestos2.DevolverCod(nodx)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(scod4))
            
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Den = Trim(txtDen)
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe = StrToDblOrNull(txtImp)
            frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo = StrToDblOrNull(txtObj)
            
End Select

Set nodx = Nothing

frmPresupuestos2.MostrarDatosBarraInf



End Function


Private Function EsimporteSuperior() As Boolean
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim dImporte As Double

Set nodx = frmPresupuestos2.tvwestrPres.selectedItem

Select Case Left(nodx.Tag, 5)
    
    Case "PRES1"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                    
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            If Trim(frmPresupuestos2.oPresupuestos.Item(scod1).importe) = "" Then
                EsimporteSuperior = True
                Exit Function
            Else
                dImporte = SumarImportesHijos(nodx)
                If dImporte > NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).importe) Then
                    EsimporteSuperior = True
                Else
                    EsimporteSuperior = False
                End If
            End If
            
            
    Case "PRES2"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            
            If Trim(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe) = "" Then
                EsimporteSuperior = True
                Exit Function
            Else
                dImporte = SumarImportesHijos(nodx)
                If dImporte > NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe) Then
                    EsimporteSuperior = True
                Else
                    EsimporteSuperior = False
                End If
            End If
            
    Case "PRES3"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = frmPresupuestos2.DevolverCod(nodx)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            
            If Trim(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe) = "" Then
                EsimporteSuperior = True
                Exit Function
            Else
                dImporte = SumarImportesHijos(nodx)
                If dImporte > NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe) Then
                    EsimporteSuperior = True
                Else
                    EsimporteSuperior = False
                End If
            End If
            
    Case "PRES4"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            scod4 = frmPresupuestos2.DevolverCod(nodx)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(scod4))
            
            If Trim(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe) = "" Then
                EsimporteSuperior = True
                Exit Function
            Else
                dImporte = SumarImportesHijos(nodx)
                If dImporte > NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe) Then
                    EsimporteSuperior = True
                Else
                    EsimporteSuperior = False
                End If
            End If
            
End Select

Set nodx = Nothing

End Function
Private Function EsimporteSuperiorMod() As Boolean
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim dImporte As Double

Set nodx = frmPresupuestos2.tvwestrPres.selectedItem

Select Case Left(nodx.Tag, 5)
    
    Case "PRES2"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            
            dImporte = SumarImportesHijos(nodx.Parent) - NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe) '- NullToDbl0(StrToDblOrNull(txtImp))
            
            If dImporte > NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).importe) Then
                EsimporteSuperiorMod = True
            Else
                EsimporteSuperiorMod = False
            End If
            
            
    Case "PRES3"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = frmPresupuestos2.DevolverCod(nodx)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            
            ' La suma de los hijos - su propio valor anterior + el valor actual debe ser inferior al de su padre
            dImporte = SumarImportesHijos(nodx.Parent) - NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe) '+ NullToDbl0(StrToDblOrNull(txtImp))
            
            If dImporte > NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe) Then
                EsimporteSuperiorMod = True
            Else
                EsimporteSuperiorMod = False
            End If
            
    Case "PRES4"
            
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            scod4 = frmPresupuestos2.DevolverCod(nodx)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(scod4))
            
            ' La suma de los hijos - su propio valor anterior + el valor actual debe ser inferior al de su padre
            dImporte = SumarImportesHijos(nodx.Parent) - NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe) '+ NullToDbl0(StrToDblOrNull(txtImp))
            
            If dImporte > NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe) Then
                EsimporteSuperiorMod = True
            Else
                EsimporteSuperiorMod = False
            End If
End Select

Set nodx = Nothing

End Function

Private Function SumarImportesHijos(ByVal nodx As MSComctlLib.node) As Double
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim dImporte As Double
Dim i As Integer
Dim nodSiguiente As MSComctlLib.node


If nodx.Child Is Nothing Then
    dImporte = NullToDbl0(StrToDblOrNull(txtImp))
Else
    
    Select Case Left(nodx.Tag, 5)
        
        Case "PRES1"
                            
            Set nodSiguiente = nodx.Child
    
            While Not (nodSiguiente Is Nothing)
                
                scod1 = frmPresupuestos2.DevolverCod(nodx)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                scod2 = frmPresupuestos2.DevolverCod(nodSiguiente)
                scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
                
                dImporte = dImporte + CDbl(NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe))
                Set nodSiguiente = nodSiguiente.Next
            
            Wend
            
            dImporte = dImporte + CDbl(NullToDbl0(txtImp))
        
        Case "PRES2"
                
            Set nodSiguiente = nodx.Child
    
            While Not (nodSiguiente Is Nothing)
                
                scod1 = frmPresupuestos2.DevolverCod(nodx.Parent)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                scod2 = frmPresupuestos2.DevolverCod(nodx)
                scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
                scod3 = frmPresupuestos2.DevolverCod(nodSiguiente)
                scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
                dImporte = dImporte + NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe)
                Set nodSiguiente = nodSiguiente.Next
            
            Wend
            
            dImporte = dImporte + CDbl(NullToDbl0(txtImp))
                
        Case "PRES3"
                
            Set nodSiguiente = nodx.Child
    
            While Not (nodSiguiente Is Nothing)
                
                scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                scod2 = frmPresupuestos2.DevolverCod(nodx.Parent)
                scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
                scod3 = frmPresupuestos2.DevolverCod(nodx)
                scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
                scod4 = frmPresupuestos2.DevolverCod(nodSiguiente)
                scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(scod4))
                
                dImporte = dImporte + NullToDbl0(frmPresupuestos2.oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe)
                
                Set nodSiguiente = nodSiguiente.Next
            
            Wend
        
            dImporte = dImporte + CDbl(NullToDbl0(txtImp))
                
    End Select

                            
        
End If

Set nodx = Nothing
Set nodSiguiente = Nothing

SumarImportesHijos = dImporte

End Function

Private Sub txtImp_LostFocus()
    txtImp = Format(txtImp, "Standard")
End Sub

Private Sub txtObj_LostFocus()
    txtImp = Format(txtImp, "0.0#")
End Sub
