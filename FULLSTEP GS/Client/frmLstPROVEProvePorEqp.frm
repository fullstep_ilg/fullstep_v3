VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPROVEProvePorEqp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Proveedores por Equipo (Opciones)"
   ClientHeight    =   4575
   ClientLeft      =   1080
   ClientTop       =   2010
   ClientWidth     =   6720
   Icon            =   "frmLstPROVEProvePorEqp.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4575
   ScaleWidth      =   6720
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   0
      ScaleHeight     =   405
      ScaleWidth      =   6720
      TabIndex        =   16
      Top             =   4170
      Width           =   6720
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5310
         TabIndex        =   15
         Top             =   15
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab stabProve 
      Height          =   4095
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   6630
      _ExtentX        =   11695
      _ExtentY        =   7223
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selección"
      TabPicture(0)   =   "frmLstPROVEProvePorEqp.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSelComp"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Timer1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstPROVEProvePorEqp.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(1)=   "Frame2"
      Tab(1).ControlCount=   2
      Begin VB.Frame Frame2 
         Height          =   1815
         Left            =   -74790
         TabIndex        =   29
         Top             =   480
         Width           =   6210
         Begin VB.CheckBox chkContactos 
            Caption         =   "Incluir contactos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   450
            TabIndex        =   12
            Top             =   1185
            Width           =   2925
         End
         Begin VB.CheckBox chkCalifica 
            Caption         =   "Incluir datos homologación"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3090
            TabIndex        =   11
            Top             =   495
            Width           =   3000
         End
         Begin VB.CheckBox chkDatosBasicos 
            Caption         =   "Incluir datos básicos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   450
            TabIndex        =   10
            Top             =   495
            Width           =   2595
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4335
         Top             =   195
      End
      Begin VB.Frame Frame3 
         Height          =   1350
         Left            =   -74775
         TabIndex        =   28
         Top             =   2445
         Width           =   6210
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominación"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   1530
            TabIndex        =   14
            Top             =   855
            Width           =   3390
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "Código"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   1530
            TabIndex        =   13
            Top             =   360
            Width           =   3510
         End
      End
      Begin VB.Frame Frame1 
         Height          =   2715
         Left            =   240
         TabIndex        =   19
         Top             =   1230
         Width           =   6195
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   5075
            Picture         =   "frmLstPROVEProvePorEqp.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   225
            Width           =   345
         End
         Begin VB.CommandButton cmdSelMat 
            Height          =   315
            Left            =   5475
            Picture         =   "frmLstPROVEProvePorEqp.frx":0D8F
            Style           =   1  'Graphical
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   225
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   1245
            TabIndex        =   2
            Top             =   720
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
            Height          =   285
            Left            =   1245
            TabIndex        =   4
            Top             =   1170
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
            Height          =   285
            Left            =   1245
            TabIndex        =   6
            Top             =   1620
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
            Height          =   285
            Left            =   1245
            TabIndex        =   8
            Top             =   2100
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
            Height          =   285
            Left            =   2325
            TabIndex        =   5
            Top             =   1170
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
            Height          =   285
            Left            =   2325
            TabIndex        =   7
            Top             =   1620
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
            Height          =   285
            Left            =   2325
            TabIndex        =   9
            Top             =   2100
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1164
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
            Height          =   285
            Left            =   2325
            TabIndex        =   3
            Top             =   720
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblMat 
            Caption         =   "Material:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   255
            TabIndex        =   27
            Top             =   300
            Width           =   1665
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "Subfamilia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   210
            TabIndex        =   26
            Top             =   1665
            Width           =   975
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "Familia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   210
            TabIndex        =   25
            Top             =   1215
            Width           =   975
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Commodity:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   210
            TabIndex        =   24
            Top             =   750
            Width           =   975
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "Grupo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   210
            TabIndex        =   23
            Top             =   2130
            Width           =   975
         End
      End
      Begin VB.Frame fraSelComp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   240
         TabIndex        =   18
         Top             =   405
         Width           =   6195
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
            Height          =   285
            Left            =   1245
            TabIndex        =   0
            Top             =   270
            Width           =   1065
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1296
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5345
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 3"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
            Height          =   285
            Left            =   2325
            TabIndex        =   1
            Top             =   270
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1640
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblEqpCod 
            Caption         =   "Equipo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   255
            TabIndex        =   21
            Top             =   300
            Width           =   900
         End
         Begin VB.Label lblEqp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label3"
            Height          =   285
            Left            =   1245
            TabIndex        =   20
            Top             =   270
            Visible         =   0   'False
            Width           =   4605
         End
      End
   End
End
Attribute VB_Name = "frmLstPROVEProvePorEqp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Variables para el resize
Private iAltura As Integer
Private iAnchura As Integer

'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

' Variables de seguridad
Private bRMat As Boolean
Public bREqp As Boolean

'Variables para la seleccion en combos
Public oEqpSeleccionado As CEquipo
Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4
Private sMatSeleccionado As String

'variables para los combos
Private oEqps As CEquipos

''' Combos
Private EqpRespetarCombo As Boolean
Private EqpCargarComboDesde As Boolean
Private GMN1RespetarCombo As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN2RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Private GMN3RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean
Private GMN4RespetarCombo As Boolean
Private GMN4CargarComboDesde As Boolean
'Multilenguaje

Private sIdiSuminMat As String
Private sIdiAsignEqp As String
Private sIdiGenerando As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private sIdiTitulo As String
Private srIdiTitulo As String
Private srIdiSeleccion As String
Private srIdiEquipo As String
Private srIdiNIF As String
Private srIdiDireccion As String
Private srIdiMoneda As String
Private srIdiHomologacion As String
Private srIdiPag As String
Private srIdiDe As String
Private srIdiContactos As String


Private Sub cmdBorrar_Click()
    sdbcGMN1_4Cod.Text = ""
End Sub

Private Sub cmdObtener_Click()
    Dim SelectionText As String
    Dim SubProveOrden As String
    Dim sSQL As String
    Dim SelectionTextRpt(1 To 2, 1 To 12) As String
    Dim NombresCAL(1 To 3) As String
    Dim oReport As CRAXDRT.Report
    Dim SubListado As CRAXDRT.Report
    Dim oCRProvePorEqp As CRProvePorEqp
    Dim oFos As FileSystemObject
    Dim pv As Preview
    Dim RepPath As String
    Dim i As Integer
    Dim codEqp As String
    Dim codGMN4 As String
    Dim CodGMN3 As String
    Dim CodGMN2 As String
    Dim CodGMN1 As String
    Dim bOcultarHomologaciones As Boolean
    Dim NombreCAL(3) As String
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRProvePorEqp = GenerarCRProvePorEqp
    
    Screen.MousePointer = vbHourglass
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPROVEPorEqp.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
        
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
  
  
    If oEqpSeleccionado Is Nothing Then
        If oGMN1Seleccionado Is Nothing And oGMN2Seleccionado Is Nothing And oGMN3Seleccionado Is Nothing And oGMN4Seleccionado Is Nothing Then
            sMatSeleccionado = ""
        Else
            If Not oGMN4Seleccionado Is Nothing Then
                sMatSeleccionado = oGMN1Seleccionado.Cod & " - " & oGMN2Seleccionado.Cod & " - " & oGMN3Seleccionado.Cod & " - " & oGMN4Seleccionado.Cod
            Else
                If Not oGMN3Seleccionado Is Nothing Then
                    sMatSeleccionado = oGMN1Seleccionado.Cod & " - " & oGMN2Seleccionado.Cod & " - " & oGMN3Seleccionado.Cod
                Else
                    If Not oGMN2Seleccionado Is Nothing Then
                        sMatSeleccionado = oGMN1Seleccionado.Cod & " - " & oGMN2Seleccionado.Cod
                    Else
                        sMatSeleccionado = oGMN1Seleccionado.Cod
                    End If
                End If
            End If
            
        End If
    Else
        If oGMN1Seleccionado Is Nothing And oGMN2Seleccionado Is Nothing And oGMN3Seleccionado Is Nothing And oGMN4Seleccionado Is Nothing Then
            sMatSeleccionado = ""
        Else
            If Not oGMN4Seleccionado Is Nothing Then
                sMatSeleccionado = oGMN1Seleccionado.Cod & " - " & oGMN2Seleccionado.Cod & " - " & oGMN3Seleccionado.Cod & " - " & oGMN4Seleccionado.Cod
           Else
                If Not oGMN3Seleccionado Is Nothing Then
                    sMatSeleccionado = oGMN1Seleccionado.Cod & " - " & oGMN2Seleccionado.Cod & " - " & oGMN3Seleccionado.Cod
                Else
                    If Not oGMN2Seleccionado Is Nothing Then
                        sMatSeleccionado = oGMN1Seleccionado.Cod & " - " & oGMN2Seleccionado.Cod
                    Else
                        sMatSeleccionado = oGMN1Seleccionado.Cod
                    End If
                End If
            End If
        End If
    End If

  
    'Selección que se lista
    SelectionText = ""
    If Not (oGMN1Seleccionado Is Nothing And oGMN2Seleccionado Is Nothing And oGMN3Seleccionado Is Nothing And oGMN4Seleccionado Is Nothing) Then
        SelectionText = sIdiSuminMat & " " & sMatSeleccionado
    End If
    If Not oEqpSeleccionado Is Nothing Then
        If SelectionText = "" Then
            SelectionText = sIdiAsignEqp & " " & oEqpSeleccionado.Cod
        Else
            SelectionText = SelectionText & ", " & sIdiAsignEqp & " " & oEqpSeleccionado.Cod
        End If
    End If
    
                   'DATOS OPCIONALES
    SelectionTextRpt(2, 1) = "OcultarDatos"
    If chkDatosBasicos.Value = vbUnchecked Then
        SelectionTextRpt(1, 1) = "S"
    Else
        SelectionTextRpt(1, 1) = "N"
    End If
    SelectionTextRpt(2, 2) = "OcultarContactos"
    If chkContactos.Value = vbUnchecked Then
        SelectionTextRpt(1, 2) = "S"
    Else
        SelectionTextRpt(1, 2) = "N"
    End If
    SelectionTextRpt(2, 3) = "OcultarHomologacion"
    If chkCalifica.Value = vbUnchecked Then
        bOcultarHomologaciones = True
        SelectionTextRpt(1, 3) = "S"
    Else
        SelectionTextRpt(1, 3) = "N"
        bOcultarHomologaciones = False
    End If
       
    SelectionTextRpt(2, 4) = "txtTITULO"
    SelectionTextRpt(1, 4) = srIdiTitulo
    SelectionTextRpt(2, 5) = "txtSELECCION"
    SelectionTextRpt(1, 5) = srIdiSeleccion
    SelectionTextRpt(2, 6) = "txtEQUIPO"
    SelectionTextRpt(1, 6) = srIdiEquipo
    SelectionTextRpt(2, 7) = "txtNIF"
    SelectionTextRpt(1, 7) = srIdiNIF & ":"
    SelectionTextRpt(2, 8) = "txtDIRECCION"
    SelectionTextRpt(1, 8) = srIdiDireccion & ":"
    SelectionTextRpt(2, 9) = "txtMONEDA"
    SelectionTextRpt(1, 9) = srIdiMoneda & ":"
    SelectionTextRpt(2, 10) = "txtHOMOLOGACION"
    SelectionTextRpt(1, 10) = srIdiHomologacion & ":"
    SelectionTextRpt(2, 11) = "txtPAG"
    SelectionTextRpt(1, 11) = srIdiPag
    SelectionTextRpt(2, 12) = "txtDE"
    SelectionTextRpt(1, 12) = srIdiDe
       
       ' SelectionTextRpt(2, 5) = "OcultarObs"
       ' If chkObs.Value = vbUnchecked Then
       '     SelectionTextRpt(1, 5) = "S"
 
       ' Else
       '     SelectionTextRpt(1, 5) = "N"
       ' End If
        

    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next i
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IDI")).Text = """" & gParametrosInstalacion.gIdioma & """"
    Set SubListado = oReport.OpenSubreport("rptCON")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCONTACTOS")).Text = """" & srIdiContactos & ":" & """"

    NombresCAL(1) = gParametrosGenerales.gsDEN_CAL1 & ":"
    NombresCAL(2) = gParametrosGenerales.gsDEN_CAL2 & ":"
    NombresCAL(3) = gParametrosGenerales.gsDEN_CAL3 & ":"
    If SelectionTextRpt(1, 3) = "N" Then
        Set SubListado = oReport.OpenSubreport("rptCAL1")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CAL1NOM")).Text = """" & NombreCAL(1) & """"
        Set SubListado = oReport.OpenSubreport("prtCAL2")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CAL2NOM")).Text = """" & NombreCAL(2) & """"
        Set SubListado = oReport.OpenSubreport("rptCAL3")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CAL3NOM")).Text = """" & NombreCAL(3) & """"
    End If

    'Si se ha elegido una rama concreta de la estructura
    If oEqpSeleccionado Is Nothing Then
        codEqp = ""
    Else
        codEqp = oEqpSeleccionado.Cod
    End If
    
    If oGMN4Seleccionado Is Nothing Then
        codGMN4 = ""
    Else
        codGMN4 = oGMN4Seleccionado.Cod
    End If
    If oGMN3Seleccionado Is Nothing Then
        CodGMN3 = ""
    Else
        CodGMN3 = oGMN3Seleccionado.Cod
    End If
    If oGMN2Seleccionado Is Nothing Then
        CodGMN2 = ""
    Else
        CodGMN2 = oGMN2Seleccionado.Cod
    End If
    If oGMN1Seleccionado Is Nothing Then
        CodGMN1 = ""
    Else
        CodGMN1 = oGMN1Seleccionado.Cod
    End If

    
    oCRProvePorEqp.ListadoProvePorEqp oGestorInformes, oReport, codEqp, CodGMN1, CodGMN2, CodGMN3, codGMN4, opOrdCod, bOcultarHomologaciones
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Set pv = New Preview
    pv.caption = sIdiTitulo
    pv.Hide
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    

End Sub

Private Sub Form_Activate()
Me.Width = 6840
Me.Height = 4980

End Sub

Private Sub Form_Unload(Cancel As Integer)

    Set oEqpSeleccionado = Nothing
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing

    Set oGruposMN1 = Nothing
    Set oGruposMN2 = Nothing
    Set oGruposMN3 = Nothing
    Set oGruposMN4 = Nothing
    Set oEqps = Nothing
    sMatSeleccionado = ""
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN1_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
    Else
         oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        GMN1RespetarCombo = False
        GMN1Seleccionado
        GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN1_4Den_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, , False)
    Else
         oGMN1s.CargarTodosLosGruposMat , sdbcGMN1_4Den.Text, True, , False
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Den.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        
        sdbcGMN1_4Den.Columns(0).Value = sdbcGMN1_4Den.Text
        sdbcGMN1_4Den.Columns(1).Value = sdbcGMN1_4Cod.Text
        
        GMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Public Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN1Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
             Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN1Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, , Trim(sdbcGMN2_4Den), True, , False)
        Else
             oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2s.Item(1).Cod
        
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
        
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Public Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN2Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
             oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
    Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not GMN3RespetarCombo Then

        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
         Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False)
        End If
        
    
    Else
        
         If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN2Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
   Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3s.Item(1).Cod
        
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
        
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Public Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN3Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
             oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub
Private Sub sdbcGMN1_4Cod_Change()

    If Not GMN1RespetarCombo Then
    
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub
Private Sub sdbcGMN1_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer


    sdbcGMN1_4Den.RemoveAll

    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Cod.RemoveAll

    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
       If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not GMN2RespetarCombo Then

        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , False)
        End If
    
    Else
        
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcGMN3_4Cod_Change()
    
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Cod.RemoveAll

    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
       If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not GMN1RespetarCombo Then

        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
    If Not GMN4RespetarCombo Then
    
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Cod.RemoveAll

    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else
        
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not GMN4RespetarCombo Then

        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Den.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False)
        End If
    Else
        
       If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    Screen.MousePointer = vbNormal
    
End Sub




Private Sub cmdSelMat_Click()
 
    frmSELMAT.sOrigen = "frmLstPROVEProvePorEqp"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Show 1

End Sub

Private Sub Form_Load()

Me.Width = 6840
Me.Height = 4980
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

CargarRecursos

    PonerFieldSeparator Me
    
cmdObtener.Enabled = False
Timer1.Enabled = False
opOrdCod = True

ConfigurarNombres

ConfigurarSeguridad

If bREqp Then
    'ZZ
    sdbcEqpCod.Visible = False
    sdbcEqpDen.Visible = False
    lblEqp.Visible = True
    lblEqp.caption = basOptimizacion.gCodEqpUsuario & " " & oUsuarioSummit.comprador.DenEqp
    sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
    Set oEqpSeleccionado = Nothing
    Set oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
   oEqpSeleccionado.Cod = basOptimizacion.gCodEqpUsuario
   
End If
'Si no hay restricción de material pueden sacar todo, sino deben meter un mat.
If Not bRMat Then
    cmdObtener.Enabled = True
End If

Set oEqps = oFSGSRaiz.Generar_CEquipos

End Sub
Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then

    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorEQPRestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorEQPRestEquipo)) Is Nothing) Then
        bREqp = True
    End If

End If

End Sub

Private Sub sdbcEqpCod_Change()

    If Not EqpRespetarCombo Then
    
        EqpRespetarCombo = True
        sdbcEqpDen.Text = ""
        EqpRespetarCombo = False

        Set oEqpSeleccionado = Nothing
        EqpCargarComboDesde = True
        
    End If


End Sub
Private Sub sdbcEqpCod_CloseUp()

    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    EqpRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    EqpRespetarCombo = False
    
    EqpCargarComboDesde = False
    
    EquipoSeleccionado
    
End Sub

Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    sdbcEqpCod.RemoveAll

    Screen.MousePointer = vbHourglass
    If EqpCargarComboDesde Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , , False
    Else
        oEqps.CargarTodosLosEquipos , , , , False
    End If
        
    Codigos = oEqps.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If EqpCargarComboDesde And Not oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcEqpCod_InitColumnProps()
    
    sdbcEqpCod.DataField = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub


Private Sub sdbcEqpCod_PositionList(ByVal Text As String)
PositionList sdbcEqpCod, Text
End Sub

Public Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
       
   Screen.MousePointer = vbHourglass
   oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        Set oEqpSeleccionado = Nothing
        sdbcEqpCod.Text = ""
    Else
        EqpRespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        
        sdbcEqpCod.Columns(0).Value = sdbcEqpCod.Text
        sdbcEqpCod.Columns(1).Value = sdbcEqpDen.Text
        
        EquipoSeleccionado
        EqpRespetarCombo = False
        EqpCargarComboDesde = False
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpDen_Change()

    If Not EqpRespetarCombo Then

        EqpRespetarCombo = True
        sdbcEqpCod.Text = ""
        EqpRespetarCombo = False
        Set oEqpSeleccionado = Nothing

        EqpCargarComboDesde = True
        
    End If

    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpDen.RemoveAll
        
    End If
End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub





Private Sub sdbcEqpDen_CloseUp()
   
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    EqpRespetarCombo = True
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    EqpRespetarCombo = False
    
    EqpCargarComboDesde = False
    
    EquipoSeleccionado
    
End Sub

Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    sdbcEqpDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If EqpCargarComboDesde Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), , False
    Else
        oEqps.CargarTodosLosEquipos , , , , False
    End If
    
    Codigos = oEqps.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If EqpCargarComboDesde And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)
PositionList sdbcEqpDen, Text
End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        Set oEqpSeleccionado = Nothing
        sdbcEqpDen.Text = ""
    Else
        EqpRespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        
        sdbcEqpDen.Columns(0).Value = sdbcEqpDen.Text
        sdbcEqpDen.Columns(1).Value = sdbcEqpCod.Text
        
        EquipoSeleccionado
        EqpRespetarCombo = False
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN4_4Den.Text = "" Then Exit Sub
    
     If sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text Then Exit Sub
   
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not oGMN3Seleccionado Is Nothing Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
             oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
        
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Screen.MousePointer = vbNormal

End Sub


Private Sub stabPROVE_Click(PreviousTab As Integer)

If stabProve.Tab = 1 Then
    ComprobarMaterialSeleccionado
End If
End Sub

Public Sub ComprobarMaterialSeleccionado()
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer

    If bRMat Then
        If oGMN4Seleccionado Is Nothing And oGMN3Seleccionado Is Nothing And oGMN2Seleccionado Is Nothing And oGMN1Seleccionado Is Nothing Then
            cmdObtener.Enabled = False
            stabProve.Tab = 0
            Exit Sub
        End If
           
        If Not oGMN4Seleccionado Is Nothing Then
            cmdObtener.Enabled = True
             Exit Sub
        End If
            
        If Not oGMN3Seleccionado Is Nothing Then
            Set oICompAsignado = oGMN3Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivelAsig = 0 Or iNivelAsig > 3 Then
                cmdObtener.Enabled = False
                stabProve.Tab = 0
                Exit Sub
            End If
            cmdObtener.Enabled = True
            Exit Sub
        End If
                
        If Not oGMN2Seleccionado Is Nothing Then
            Set oICompAsignado = oGMN2Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivelAsig = 0 Or iNivelAsig > 2 Then
                cmdObtener.Enabled = False
                stabProve.Tab = 0
                Exit Sub
            End If
            cmdObtener.Enabled = True
            Exit Sub
                
        End If
            
        If Not oGMN1Seleccionado Is Nothing Then
            Set oICompAsignado = oGMN1Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                cmdObtener.Enabled = False
                stabProve.Tab = 0
                Exit Sub
            End If
            cmdObtener.Enabled = True
            Exit Sub
            
        End If

    End If
 
End Sub

Private Sub EquipoSeleccionado()
    
    Set oEqpSeleccionado = Nothing
    
    If Trim(sdbcEqpCod) <> "" Then
        Set oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
        oEqpSeleccionado.Cod = Trim(sdbcEqpCod)
         
    End If
End Sub

Private Sub GMN1Seleccionado()
    
    sdbcGMN2_4Cod.RemoveAll
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN2_4Den.RemoveAll
    sdbcGMN2_4Cod.Text = ""
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcGMN2_4Den.Text = ""
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    If sdbcGMN1_4Cod <> "" Then
        Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
        oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
        ComprobarMaterialSeleccionado
    End If
    
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    
    Set oGMN2Seleccionado = Nothing
    If sdbcGMN2_4Cod.Text <> "" Then
        Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2

        oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
        oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    
        ComprobarMaterialSeleccionado
    End If
    
End Sub
Private Sub GMN3Seleccionado()
    

    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    If sdbcGMN3_4Cod <> "" Then
        Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
        oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
        oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
        oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    
        ComprobarMaterialSeleccionado
    End If
End Sub
Private Sub GMN4Seleccionado()
    

    Set oGMN4Seleccionado = Nothing
    If sdbcGMN4_4Cod <> "" Then
        Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
        oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
        oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
        oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
        ComprobarMaterialSeleccionado
    End If
    
End Sub
Private Sub ConfigurarNombres()

    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsABR_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsABR_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsABR_GMN4 & ":"
    
End Sub

Public Sub PonerMatSeleccionadoEnCombos()
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If
    
    '    ComprobarMaterialSeleccionado
  
End Sub


Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub


Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPROVE_PROVEPOREQP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        stabProve.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        stabProve.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblEqpCod.caption = Ador(0).Value
        sIdiAsignEqp = Ador(0).Value
        Ador.MoveNext
        lblMat.caption = Ador(0).Value '5
        sIdiSuminMat = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        sdbcEqpCod.Columns(0).caption = Ador(0).Value
        sdbcEqpDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbcEqpCod.Columns(1).caption = Ador(0).Value
        sdbcEqpDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        chkDatosBasicos.caption = Ador(0).Value
        Ador.MoveNext
        chkCalifica.caption = Ador(0).Value
        Ador.MoveNext
        chkContactos.caption = Ador(0).Value '10
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        'Ador.MoveNext
        'sIdiProve = Ador(0).Value
        'Ador.MoveNext
        'sIdiSuminMat = Ador(0).Value '15
        'Ador.MoveNext
        'sIdiAsignEqp = Ador(0).Value
        'Ador.MoveNext
        'sIdiPorEqp = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value
        Ador.MoveNext
        srIdiTitulo = Ador(0).Value
        Ador.MoveNext
        srIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        srIdiEquipo = Ador(0).Value
        Ador.MoveNext
        srIdiNIF = Ador(0).Value
        Ador.MoveNext
        srIdiDireccion = Ador(0).Value
        Ador.MoveNext
        srIdiMoneda = Ador(0).Value
        Ador.MoveNext
        srIdiHomologacion = Ador(0).Value
        Ador.MoveNext
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        Ador.MoveNext
        srIdiContactos = Ador(0).Value
        
        Ador.Close
    
    End If


   Set Ador = Nothing



End Sub




