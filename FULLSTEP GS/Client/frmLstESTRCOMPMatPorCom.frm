VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstESTRCOMPMatPorCom 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de material por comprador (Opciones)+"
   ClientHeight    =   2970
   ClientLeft      =   1200
   ClientTop       =   3690
   ClientWidth     =   5775
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstESTRCOMPMatPorCom.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2970
   ScaleWidth      =   5775
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   5775
      TabIndex        =   11
      Top             =   2595
      Width           =   5775
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener+"
         Height          =   375
         Left            =   4440
         TabIndex        =   10
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2535
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   4471
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n+"
      TabPicture(0)   =   "frmLstESTRCOMPMatPorCom.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Timer1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Opciones+"
      TabPicture(1)   =   "frmLstESTRCOMPMatPorCom.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTipoPer"
      Tab(1).ControlCount=   1
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4920
         Top             =   120
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2085
         Left            =   120
         TabIndex        =   14
         Top             =   330
         Width           =   5535
         Begin VB.OptionButton optComp 
            Caption         =   "Por comprador+"
            Height          =   195
            Left            =   165
            TabIndex        =   4
            Top             =   1545
            Width           =   1560
         End
         Begin VB.OptionButton optEqp 
            Caption         =   "Por equipo+"
            Height          =   195
            Left            =   165
            TabIndex        =   1
            Top             =   945
            Width           =   1515
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Listado completo+"
            Height          =   195
            Left            =   165
            TabIndex        =   0
            Top             =   360
            Value           =   -1  'True
            Width           =   2955
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCompCod 
            Height          =   285
            Left            =   1800
            TabIndex        =   5
            Top             =   1500
            Width           =   945
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1111
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5001
            Columns(1).Caption=   "Apellido y nombre"
            Columns(1).Name =   "Ape"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1667
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCompDen 
            Height          =   285
            Left            =   2865
            TabIndex        =   6
            Top             =   1500
            Width           =   2505
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5424
            Columns(0).Caption=   "Apellido y nombre"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1296
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4419
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
            Height          =   285
            Left            =   2880
            TabIndex        =   3
            Top             =   900
            Width           =   2505
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1640
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4419
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
            Height          =   285
            Left            =   1800
            TabIndex        =   2
            Top             =   900
            Width           =   945
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1296
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5345
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1667
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin VB.Label lblEqp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label1"
            Height          =   285
            Left            =   1800
            TabIndex        =   17
            Top             =   900
            Visible         =   0   'False
            Width           =   3585
         End
      End
      Begin VB.Frame fraTipoPer 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2100
         Left            =   -74880
         TabIndex        =   13
         Top             =   315
         Width           =   5535
         Begin VB.OptionButton optOrdCod 
            Caption         =   "C�digo+"
            Height          =   360
            Left            =   600
            TabIndex        =   7
            Top             =   465
            Value           =   -1  'True
            Width           =   1515
         End
         Begin VB.OptionButton optOrdDen 
            Caption         =   "Denominaci�n+"
            Height          =   345
            Left            =   2400
            TabIndex        =   8
            Top             =   465
            Width           =   1515
         End
         Begin VB.Frame Frame1 
            Caption         =   "Orden+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   900
            Left            =   120
            TabIndex        =   15
            Top             =   135
            Width           =   5295
         End
         Begin VB.Frame Frame3 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   900
            Left            =   120
            TabIndex        =   16
            Top             =   1065
            Width           =   5295
            Begin VB.CheckBox chkDetalle 
               Caption         =   "Incluir detalles comprador+"
               Height          =   360
               Left            =   480
               TabIndex        =   9
               Top             =   360
               Width           =   3135
            End
         End
      End
   End
End
Attribute VB_Name = "frmLstESTRCOMPMatPorCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Variables de seguridad
Private bREqp As Boolean 'aplicar restriccion de equipo

Private CargarComboDesdeEqp As Boolean
Private CargarComboDesdeComp As Boolean
Public RespetarCombo As Boolean
Public g_oADORes As Ador.Recordset
Private oEqpSeleccionado As CEquipo
Private oComps As CCompradores
Private oEqps As CEquipos
Private oCompSeleccionado As CComprador
'MULTILENGUAJE
Private sEspera(3) As String         'textos para formulario de espera
Private sTitulo As String
Private FormulaRpt(1 To 2, 1 To 9) As String      'formulas textos RPT
Private sListadoSel As String                  'formula Selecci�n rpt

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTESTRCOMP_MATPORCOMP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        chkDetalle.caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        Frame1.caption = Ador(0).Value
        Ador.MoveNext
        frmLstESTRCOMPMatPorCom.caption = Ador(0).Value
        Ador.MoveNext
        optComp.caption = Ador(0).Value '5
        Ador.MoveNext
        optEqp.caption = Ador(0).Value
        Ador.MoveNext
        OptOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        optOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value '10
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        Ador.MoveNext
        sdbcEqpCod.Columns(0).caption = Ador(0).Value
        sdbcEqpDen.Columns(1).caption = Ador(0).Value
        sdbcCompCod.Columns(0).caption = Ador(0).Value
        sdbcCompDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEqpCod.Columns(1).caption = Ador(0).Value
        sdbcEqpDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCompCod.Columns(1).caption = Ador(0).Value
        sdbcCompDen.Columns(0).caption = Ador(0).Value
        'Ador.MoveNext
        'sTitulo = Ador(0).Value '15
        Ador.MoveNext
        ' FORMULAS RPT
        sTitulo = Ador(0).Value
        For i = 1 To 9
            FormulaRpt(1, i) = Ador(0).Value
            Ador.MoveNext
        Next
        sListadoSel = Ador(0).Value
        'Ador.MoveNext
        'sListadoSel(2) = Ador(0).Value
        'Ador.MoveNext
        'sListadoSel(3) = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub cmdObtener_Click()

   ''' * Objetivo: Obtener un listado de Material por Compradores
        
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    Dim SelectionTextRpt(1 To 2, 1 To 2) As String
    Dim SelectionTextSubRpt(1 To 4, 1 To 4) As String
    
    Dim oReport As Object
    Dim oFos As FileSystemObject
    Dim pv As Preview
    Dim RepPath As String
    Dim sCodComp As String
    Dim sCodEqp As String
    Dim i As Integer
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptMATporCom.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing


    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    ' FORMULAS TEXTO RPT
    FormulaRpt(2, 1) = "txtTITULO"
    FormulaRpt(2, 2) = "txtSEL"
    FormulaRpt(2, 3) = "txtPAG"
    FormulaRpt(2, 4) = "txtDE"
    FormulaRpt(2, 5) = "txtTFNO"
    FormulaRpt(2, 6) = "txtFAX"
    FormulaRpt(2, 7) = "txtEMAIL"
    FormulaRpt(2, 8) = "txtEQP": FormulaRpt(1, 8) = FormulaRpt(1, 8) & ":"
    FormulaRpt(2, 9) = "txtCOM": FormulaRpt(1, 9) = FormulaRpt(1, 9) & ":"
    For i = 1 To UBound(FormulaRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, FormulaRpt(2, i))).Text = """" & FormulaRpt(1, i) & """"
    Next i
    
    '*********** FORMULA FIELDS REPORT
    ' Selecci�n
    SelectionTextRpt(2, 1) = "SEL"
    If optTodos = True Then
        SelectionTextRpt(1, 1) = sListadoSel  'Todos los materiales por comprador
    Else
        If optEqp = True Then
            'Materiales del equipo
            If lblEqp.Visible Then
                SelectionTextRpt(1, 1) = FormulaRpt(1, 8) & " " & lblEqp
            Else
                SelectionTextRpt(1, 1) = FormulaRpt(1, 8) & " " & sdbcEqpCod.Text & " " & sdbcEqpDen.Text
            End If
        Else
            'materiales del comprador
            SelectionTextRpt(1, 1) = FormulaRpt(1, 9) & " " & sdbcCompCod.Text & " " & sdbcCompDen.Text
        End If
    End If
            
        'Ocultar Detalles Items
    SelectionTextRpt(2, 2) = "OcultarDetalles"
    If chkDetalle.Value = vbUnchecked Then
        SelectionTextRpt(1, 2) = "S"
    Else
        SelectionTextRpt(1, 2) = "N"
    End If
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next i
    
    SelectionTextSubRpt(2, 1) = "GMN1NOM"
    SelectionTextSubRpt(1, 1) = gParametrosGenerales.gsDEN_GMN1 & ":"
    SelectionTextSubRpt(2, 2) = "GMN2NOM"
    SelectionTextSubRpt(1, 2) = gParametrosGenerales.gsDEN_GMN2 & ":"
    SelectionTextSubRpt(2, 3) = "GMN3NOM"
    SelectionTextSubRpt(1, 3) = gParametrosGenerales.gsDEN_GMN3 & ":"
    SelectionTextSubRpt(2, 4) = "GMN4NOM"
    SelectionTextSubRpt(1, 4) = gParametrosGenerales.gsDEN_GMN4 & ":"
    For i = 1 To UBound(SelectionTextSubRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextSubRpt(2, i))).Text = """" & SelectionTextSubRpt(1, i) & """"
    Next i
    
    
    
    Screen.MousePointer = vbHourglass
    
    Set g_oADORes = oGestorInformes.ListadoMaterialPorComprador(bREqp, sdbcCompCod.Text, sdbcEqpCod.Text, OptOrdCod, optEqp, optTodos)
    If g_oADORes Is Nothing Then
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        If g_oADORes.EOF Then
            oMensajes.ImposibleMostrarInforme
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    oReport.Database.SetDataSource g_oADORes
        
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sEspera(1) 'Generando listado de material por comprador
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)        'Seleccionando registros ...
    frmESPERA.lblDetalle = sEspera(3)         'Visualizando listado ...
    frmESPERA.Show
    
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.g_sOrigen = "frmLstESTRCOMPMatPorCom"
    
    Timer1.Enabled = True
    pv.caption = sTitulo                      'Listado de material por compradores
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub Form_Load()

    Me.Width = 5895
    Me.Height = 3375
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorCOMPRestEquipo)) Is Nothing) Then
            bREqp = True
        End If
    End If
    
    If bREqp Then
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp.caption = basOptimizacion.gCodEqpUsuario & " - " & oUsuarioSummit.comprador.DenEqp
        sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
        optEqp.Value = True
        Set oEqpSeleccionado = Nothing
        Set oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
  
        oEqpSeleccionado.Cod = basOptimizacion.gCodEqpUsuario
        
    Else
        Set oComps = oFSGSRaiz.generar_CCompradores
        Set oEqps = oFSGSRaiz.Generar_CEquipos
      
    End If


End Sub

Private Sub Form_Unload(Cancel As Integer)

    Set oEqpSeleccionado = Nothing
    Set oComps = Nothing
    Set oCompSeleccionado = Nothing
    Set oEqps = Nothing
End Sub



Private Sub optComp_Click()
If lblEqp.Visible Or Not (oEqpSeleccionado Is Nothing) Then
    sdbcCompCod.Enabled = True
    sdbcCompDen.Enabled = True
    If lblEqp.Visible Then
        lblEqp.Enabled = True
    Else
        sdbcEqpCod.Enabled = True
        sdbcEqpDen.Enabled = True
    End If
Else
    optEqp.Value = True
    'sdbcEqpCod.SetFocus
End If
End Sub

Private Sub optEqp_Click()
    If lblEqp.Visible Then
        lblEqp.Enabled = True
    Else
        sdbcEqpCod.Enabled = True
        sdbcEqpDen.Enabled = True
    End If
    sdbcCompCod.Enabled = False
    sdbcCompDen.Enabled = False

End Sub

Private Sub optTodos_Click()
    If lblEqp.Visible Then
        lblEqp.Enabled = False
    Else
        sdbcEqpCod.Enabled = False
        sdbcEqpDen.Enabled = False
    End If
    sdbcCompCod.Enabled = False
    sdbcCompDen.Enabled = False
    
End Sub
Private Sub sdbcCompCod_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcCompDen.Text = ""
        RespetarCombo = False
        
        CargarComboDesdeComp = True
        Set oCompSeleccionado = Nothing
        'optEqp.Value = True
        
    End If
    
End Sub


Private Sub sdbcCompCod_Click()
    If Not sdbcCompCod.DroppedDown Then
        sdbcCompCod = ""
        sdbcCompDen = ""
    End If
End Sub

Private Sub sdbcCompCod_CloseUp()
    Dim sCod As String
    
    If sdbcCompCod.Value = "..." Then
        sdbcCompCod.Text = ""
        Exit Sub
    End If
    If oEqpSeleccionado Is Nothing Then Exit Sub
    If sdbcCompCod.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcCompDen.Text = sdbcCompCod.Columns(1).Text
    sdbcCompCod.Text = sdbcCompCod.Columns(0).Text
    RespetarCombo = False
    
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    
    Set oCompSeleccionado = oComps.Item(sCod & sdbcCompCod.Columns(0).Text)
        
    CargarComboDesdeComp = False
    
    DoEvents
    
    'CompradorSeleccionado
    
End Sub

Private Sub sdbcCompCod_DropDown()
Dim EqpCod As String

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    Set oComps = Nothing
    Set oComps = oFSGSRaiz.generar_CCompradores
    
  
        
    If CargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcCompCod.Text), , , False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, False, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    'Set oEqpSeleccionado.Compradores = Nothing
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        
                
        If bREqp Then
            
            EqpCod = basOptimizacion.gCodEqpUsuario
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
            Next
        
        Else
            
            EqpCod = sdbcEqpCod.Text
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
            Next
        
        End If
        
        
    
    Else
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcCompCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
    
    End If
    
    
    If CargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompCod.AddItem "..."
    End If

    sdbcCompCod.SelStart = 0
    sdbcCompCod.SelLength = Len(sdbcCompCod.Text)
    sdbcCompCod.Refresh

End Sub


Private Sub sdbcCompCod_InitColumnProps()
    sdbcCompCod.DataFieldList = "Column 0"
    sdbcCompCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCompCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCompCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCompCod.Rows - 1
            bm = sdbcCompCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCompCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCompCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcCompCod_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    If sdbcCompCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el comprador
       
    oEqpSeleccionado.CargarTodosLosCompradores sdbcCompCod.Text, , , True, , , False
    Set oCompradores = oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompCod.Text = ""
    Else
        RespetarCombo = True
        sdbcCompDen.Text = oCompradores.Item(1).Apel & ", " & oCompradores.Item(1).nombre
        sdbcCompCod.Columns(0).Value = sdbcCompCod.Text
        sdbcCompCod.Columns(1).Value = sdbcCompDen.Text
        
        RespetarCombo = False
        Set oCompSeleccionado = oCompradores.Item(1)
        CargarComboDesdeComp = False
        'CompradorSeleccionado
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing

End Sub

Private Sub sdbcCompDen_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcCompCod.Text = ""
        RespetarCombo = False
        
        CargarComboDesdeComp = True
        Set oCompSeleccionado = Nothing
        'optEqp.Value = True
        
    End If
    
End Sub


Private Sub sdbcCompDen_Click()
    If Not sdbcCompDen.DroppedDown Then
        sdbcCompCod = ""
        sdbcCompDen = ""
    End If
End Sub

Private Sub sdbcCompDen_CloseUp()
    Dim sCod As String
    
    If sdbcCompDen.Value = "..." Then
        sdbcCompDen.Text = ""
        Exit Sub
    End If
    
    If sdbcCompDen.Value = "" Then Exit Sub
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    RespetarCombo = True
    sdbcCompDen.Text = sdbcCompDen.Columns(0).Text
    sdbcCompCod.Text = sdbcCompDen.Columns(1).Text
    RespetarCombo = False
    
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    
    Set oCompSeleccionado = oComps.Item(sCod & sdbcCompDen.Columns(1).Text)
        
    CargarComboDesdeComp = False
    
    DoEvents
    
    'CompradorSeleccionado
       
End Sub

Private Sub sdbcCompDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Dim EqpCod As String
    
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
        
    Set oComps = Nothing
    Set oComps = oFSGSRaiz.generar_CCompradores
  
    
    
    If CargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(Apellidos(sdbcEqpDen.Text)), True, False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, True, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    Set oEqpSeleccionado.Compradores = Nothing
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        
                
        If bREqp Then
            
            EqpCod = basOptimizacion.gCodEqpUsuario
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
            Next
            
        Else
            
            EqpCod = sdbcEqpCod.Text
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
            Next
        
        End If
        
        
    
    Else
        
        For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
    
    End If
    
    If CargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompDen.AddItem "..."
    End If

    sdbcCompDen.SelStart = 0
    sdbcCompDen.SelLength = Len(sdbcCompDen.Text)
    sdbcCompDen.Refresh

End Sub

Private Sub sdbcCompDen_InitColumnProps()
    sdbcCompDen.DataFieldList = "Column 0"
    sdbcCompDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcCompDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCompDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCompDen.Rows - 1
            bm = sdbcCompDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCompDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCompDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcCompDen_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    If sdbcCompDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
 
    oEqpSeleccionado.CargarTodosLosCompradores , nombre(sdbcCompDen.Text), Apellidos(sdbcCompDen.Text), True, False, False, False
    Set oCompradores = oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompDen.Text = ""
    Else
        RespetarCombo = True
        sdbcCompCod.Text = oCompradores.Item(1).Cod
        
        sdbcCompDen.Columns(0).Value = sdbcCompDen.Text
        sdbcCompDen.Columns(1).Value = sdbcCompCod.Text
        
        RespetarCombo = False
        Set oCompSeleccionado = oCompradores.Item(1)
        CargarComboDesdeComp = False
        'CompradorSeleccionado
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing

End Sub

Private Sub sdbcEqpCod_Change()
     
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpDen.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        RespetarCombo = False
        
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
        'optTodos.Value = True
                
    End If
    
End Sub
Private Sub sdbcEqpCod_CloseUp()
    
    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpCod.Value = "" Then Exit Sub
    
    sdbcCompCod = ""
    sdbcCompDen = ""
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
    
    'tvwEstrMat.Nodes.Clear
        
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    optEqp.Value = True
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(0).Text)
        
    CargarComboDesdeEqp = False
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEqpCod.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, False, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcEqpCod_InitColumnProps()

    sdbcEqpCod.DataFieldList = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcEqpCod_Click()
    
    If Not sdbcEqpCod.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpCod.Rows - 1
            bm = sdbcEqpCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpCod.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        
    Else
        RespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        sdbcEqpCod.Columns(0).Value = sdbcEqpCod.Text
        sdbcEqpCod.Columns(1).Value = sdbcEqpDen.Text
        
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpDen_Change()
      
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpCod.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        RespetarCombo = False
        
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
        'optTodos.Value = True
    End If
          
End Sub

Private Sub sdbcEqpDen_Click()
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpDen_CloseUp()
    
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpDen.Value = "" Then Exit Sub
    
    sdbcCompCod = ""
    sdbcCompDen = ""
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
        
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(1).Text)
        
    CargarComboDesdeEqp = False
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEqpDen.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, True, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpDen.Rows - 1
            bm = sdbcEqpDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
    Else
        RespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        
        sdbcEqpDen.Columns(0).Value = sdbcEqpDen.Text
        sdbcEqpDen.Columns(1).Value = sdbcEqpCod.Text
        
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub
