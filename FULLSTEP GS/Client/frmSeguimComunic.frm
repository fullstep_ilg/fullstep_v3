VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSeguimComunic 
   BackColor       =   &H00808000&
   Caption         =   "DHist�rico de comunicaci�n con el proveedor"
   ClientHeight    =   4425
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11565
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSeguimComunic.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4425
   ScaleWidth      =   11565
   Begin VB.CommandButton cmdRestaurar 
      Caption         =   "D&Restaurar"
      Height          =   345
      Left            =   2475
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   4020
      Width           =   1005
   End
   Begin VB.CommandButton cmdEliminar 
      Caption         =   "D&Eliminar"
      Height          =   345
      Left            =   1395
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   4020
      Width           =   1005
   End
   Begin VB.CommandButton cmdNuevas 
      Caption         =   "D&Comunicar"
      Height          =   345
      Left            =   75
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   4020
      Width           =   1245
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   75
      ScaleHeight     =   435
      ScaleWidth      =   11340
      TabIndex        =   0
      Top             =   60
      Width           =   11340
      Begin VB.TextBox Text5 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   60
         Width           =   3330
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   555
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   60
         Width           =   750
      End
      Begin VB.TextBox Text2 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   60
         Width           =   1350
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   60
         Width           =   1290
      End
      Begin VB.Label Label4 
         BackColor       =   &H00808000&
         Caption         =   "DOrden de entrega:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   4140
         TabIndex        =   8
         Top             =   120
         Width           =   1515
      End
      Begin VB.Label Label10 
         BackColor       =   &H00808000&
         Caption         =   "DN�mero pedido:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1500
         TabIndex        =   7
         Top             =   120
         Width           =   1155
      End
      Begin VB.Label Label3 
         BackColor       =   &H00808000&
         Caption         =   "DA�o:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   75
         TabIndex        =   6
         Top             =   120
         Width           =   435
      End
      Begin VB.Label Label7 
         BackColor       =   &H00808000&
         Caption         =   "DProveedor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   7020
         TabIndex        =   5
         Top             =   120
         Width           =   825
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPet 
      Height          =   3315
      Left            =   75
      TabIndex        =   9
      Top             =   600
      Width           =   11355
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Row.Count       =   1
      Col.Count       =   70
      Row(0).Col(0)   =   "Gomez Quintero"
      Row(0).Col(1)   =   "Javier"
      Row(0).Col(2)   =   "jago@papyndux.com"
      Row(0).Col(3)   =   "Notificaci�n de emisi�n"
      Row(0).Col(4)   =   "15/4/2001"
      Row(0).Col(6)   =   "-1"
      stylesets.count =   2
      stylesets(0).Name=   "ConPeticiones"
      stylesets(0).BackColor=   13172735
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSeguimComunic.frx":0CB2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSeguimComunic.frx":0CCE
      UseGroups       =   -1  'True
      BeveColorScheme =   1
      BevelColorFace  =   12632256
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   2
      Groups(0).Width =   8387
      Groups(0).Caption=   "Contacto"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   2990
      Groups(0).Columns(0).Caption=   "Apellidos"
      Groups(0).Columns(0).Name=   "CONCOD"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   2011
      Groups(0).Columns(1).Caption=   "Nombre"
      Groups(0).Columns(1).Name=   "CONDEN"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(2).Width=   3387
      Groups(0).Columns(2).Caption=   "Email"
      Groups(0).Columns(2).Name=   "EMAIL"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(1).Width =   10795
      Groups(1).Caption=   "Comunicaci�n"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   9
      Groups(1).Columns(0).Width=   2328
      Groups(1).Columns(0).Caption=   "Tipo "
      Groups(1).Columns(0).Name=   "TIPO"
      Groups(1).Columns(0).DataField=   "Column 3"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(1).Width=   979
      Groups(1).Columns(1).Caption=   "Fecha"
      Groups(1).Columns(1).Name=   "FEC"
      Groups(1).Columns(1).DataField=   "Column 4"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(2).Width=   1720
      Groups(1).Columns(2).Visible=   0   'False
      Groups(1).Columns(2).Caption=   "CODCON"
      Groups(1).Columns(2).Name=   "CODCON"
      Groups(1).Columns(2).DataField=   "Column 5"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(3).Width=   2037
      Groups(1).Columns(3).Caption=   "INCORRECTA"
      Groups(1).Columns(3).Name=   "INCORRECTA"
      Groups(1).Columns(3).DataField=   "Column 6"
      Groups(1).Columns(3).DataType=   8
      Groups(1).Columns(3).FieldLen=   256
      Groups(1).Columns(3).Style=   2
      Groups(1).Columns(4).Width=   2831
      Groups(1).Columns(4).Caption=   "REENVIAR"
      Groups(1).Columns(4).Name=   "REENVIAR"
      Groups(1).Columns(4).DataField=   "Column 7"
      Groups(1).Columns(4).DataType=   8
      Groups(1).Columns(4).FieldLen=   256
      Groups(1).Columns(4).Style=   4
      Groups(1).Columns(4).ButtonsAlways=   -1  'True
      Groups(1).Columns(5).Width=   794
      Groups(1).Columns(5).Caption=   "RWeb"
      Groups(1).Columns(5).Name=   "WEB"
      Groups(1).Columns(5).DataField=   "Column 8"
      Groups(1).Columns(5).DataType=   8
      Groups(1).Columns(5).FieldLen=   256
      Groups(1).Columns(5).Style=   2
      Groups(1).Columns(6).Width=   741
      Groups(1).Columns(6).Caption=   "RMail"
      Groups(1).Columns(6).Name=   "MAIL"
      Groups(1).Columns(6).DataField=   "Column 9"
      Groups(1).Columns(6).DataType=   8
      Groups(1).Columns(6).FieldLen=   256
      Groups(1).Columns(6).Style=   2
      Groups(1).Columns(7).Width=   1085
      Groups(1).Columns(7).Caption=   "RCarta"
      Groups(1).Columns(7).Name=   "CARTA"
      Groups(1).Columns(7).DataField=   "Column 10"
      Groups(1).Columns(7).DataType=   8
      Groups(1).Columns(7).FieldLen=   256
      Groups(1).Columns(7).Style=   2
      Groups(1).Columns(8).Width=   5239
      Groups(1).Columns(8).Visible=   0   'False
      Groups(1).Columns(8).Caption=   "Id"
      Groups(1).Columns(8).Name=   "ID"
      Groups(1).Columns(8).DataField=   "Column 11"
      Groups(1).Columns(8).DataType=   2
      Groups(1).Columns(8).FieldLen=   256
      _ExtentX        =   20029
      _ExtentY        =   5847
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSeguimComunic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmseguimComunic
''' *** Creacion: 07/08/2001


Option Explicit
Public sOrigen As String
Public oOrdenEntrega As COrdenEntrega
Private sIdioma(6) As String
Private sIdiLaNotificacion As String
Private oNotificacion As CNotificacionPedido
Dim m_sId As String     'Id de la orden de comunicaci�n seleccionada en el grid.

Dim bSesionIniciada As Boolean   'Indica si se ha iniciado una sesion de correo
'Iniciando sesi�n de correo ...
Private sIdiInicSesCorreo As String
'Cerrando sesi�n de correo ...
Private sIdiCerrSesCorreo As String

Public g_bCancelarMail As Boolean

Private m_bReenviando As Boolean
'Control de errores
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean

Private m_sMsgError As String
Private Sub cmdEliminar_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Exit Sub
    End If
   
    If sdbgPet.Rows > 0 Then
      
        Screen.MousePointer = vbHourglass
        
        sdbgPet.SelBookmarks.Add sdbgPet.Bookmark
        EliminarNotificacionSeleccionada
        
        Screen.MousePointer = vbNormal
      
    End If
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "cmdEliminar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdNuevas_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Exit Sub
    End If
    
    If oOrdenEntrega.Estado = TipoEstadoOrdenEntrega.Anulado Then
       frmNotificaPedido.TipoComu = 1
    Else
           frmNotificaPedido.TipoComu = 0
    End If
    Set frmNotificaPedido.oOrdenEntrega = oOrdenEntrega
    
    
    
    frmNotificaPedido.CodProve = oOrdenEntrega.ProveCod
    frmNotificaPedido.DenProve = oOrdenEntrega.ProveDen
    frmNotificaPedido.pedido = oOrdenEntrega.idPedido
    frmNotificaPedido.Orden = oOrdenEntrega.Id
    frmNotificaPedido.TipoPedi = oOrdenEntrega.Tipo
    
    MDI.MostrarFormulario frmNotificaPedido, True
    frmNotificaPedido.Show
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "cmdNuevas_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Public Sub cmdRestaurar_Click()
    Dim dentipo As String

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Exit Sub
    End If
    
    sdbgPet.RemoveAll
    
    oOrdenEntrega.CargarNotificacionesPedido
    ' indi = 1
    If oOrdenEntrega.Notificaciones.Count > 0 Then
        For Each oNotificacion In oOrdenEntrega.Notificaciones
            Select Case oNotificacion.Tipo
            Case 0
                dentipo = sIdioma(3)
            Case 1
                dentipo = sIdioma(4)
            Case 2
                dentipo = sIdioma(5)
            Case 3
                dentipo = sIdioma(6)
            End Select
            sdbgPet.AddItem oNotificacion.Apellidos & Chr(m_lSeparador) & oNotificacion.nombre & Chr(m_lSeparador) & oNotificacion.Email & Chr(m_lSeparador) & dentipo & Chr(m_lSeparador) & oNotificacion.Fecha & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(oNotificacion.Fallido) & Chr(m_lSeparador) & Chr(m_lSeparador) & oNotificacion.web & Chr(m_lSeparador) & oNotificacion.mail & Chr(m_lSeparador) & oNotificacion.Carta & Chr(m_lSeparador) & oNotificacion.Id  '& chr(m_lSeparador)& indi
        '    indi = indi + 1
        Next
    End If

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "cmdRestaurar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Unload Me
       Exit Sub
    End If
    
    If Not m_bActivado Then m_bActivado = True
   
    Unload frmNotificaPedido
    
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    Dim oLineaPedido As CLineaPedido
    Dim dentipo As String
    Dim i As Integer

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    
    m_bDescargarFrm = False
    m_bActivado = False

    CargarRecursos
    
    PonerFieldSeparator Me
    
    Me.Width = 11550
    Me.Height = 4845
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    DoEvents
    
    sdbgPet.RemoveAll
    
    'Bloqueamos todas las columnas menos la de reenviar.
    For i = 0 To sdbgPet.Columns.Count - 1
        If UCase(sdbgPet.Columns(i).Name) <> "REENVIAR" And sdbgPet.Columns(i).Visible = True Then
            sdbgPet.Columns(i).Locked = True
        End If
    Next
    
    oOrdenEntrega.CargarNotificacionesPedido

    If oOrdenEntrega.Notificaciones.Count > 0 Then
        For Each oNotificacion In oOrdenEntrega.Notificaciones
            Select Case oNotificacion.Tipo
            Case 0
                dentipo = sIdioma(3)
            Case 1
                dentipo = sIdioma(4)
            Case 2
                dentipo = sIdioma(5)
            Case 3
                dentipo = sIdioma(6)
            End Select
            sdbgPet.AddItem oNotificacion.Apellidos & Chr(m_lSeparador) & oNotificacion.nombre & Chr(m_lSeparador) & oNotificacion.Email & Chr(m_lSeparador) & dentipo & Chr(m_lSeparador) & oNotificacion.Fecha & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(oNotificacion.Fallido) & Chr(m_lSeparador) & Chr(m_lSeparador) & oNotificacion.web & Chr(m_lSeparador) & oNotificacion.mail & Chr(m_lSeparador) & oNotificacion.Carta & Chr(m_lSeparador) & oNotificacion.Id
        Next
    End If

    
    frmSeguimComunic.caption = sIdioma(0) & ": " & oOrdenEntrega.ProveCod & ", " & oOrdenEntrega.ProveDen
    Text1.Text = oOrdenEntrega.Anyo
    Text2.Text = oOrdenEntrega.NumPedido
    Text3.Text = oOrdenEntrega.Numero
    Text5.Text = oOrdenEntrega.ProveDen
    
    If oOrdenEntrega.Tipo = 0 Then
        sdbgPet.Columns("WEB").Visible = False
    Else
        sdbgPet.Columns("WEB").Visible = True
    End If

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub Form_Resize()
On Error Resume Next
   
    ''' * Objetivo: Adecuar los controles
    If Height > 1500 Then
        sdbgPet.Height = Height - 1500
    End If
    If Width >= 360 Then sdbgPet.Width = Width - 360
    sdbgPet.Groups(0).Width = sdbgPet.Width * 47 / 100
    sdbgPet.Groups(1).Width = sdbgPet.Width * 51 / 100
    'sdbgPet.Groups(1).Columns(4).Width = sdbgPet.Groups(1).Columns(3).Width - 250
    cmdRestaurar.Top = sdbgPet.Height + 680
    cmdNuevas.Top = sdbgPet.Height + 680
    cmdEliminar.Top = sdbgPet.Height + 680
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
    
    If sOrigen = "frmSeguimiento" Then
        If sdbgPet.Rows > 0 Then
            oOrdenEntrega.ConComunicaciones = True
            frmSeguimiento.sdbgSeguimiento.Columns("COMUNI").CellStyleSet "Comunic"
        Else
            oOrdenEntrega.ConComunicaciones = False
            frmSeguimiento.sdbgSeguimiento.Columns("COMUNI").CellStyleSet ""
        End If
        frmSeguimiento.sdbgSeguimiento.Refresh
    End If
    
    Me.Visible = False
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

   
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEGUIMCOMUNIC, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sIdioma(0) = Ador(0).Value
        Ador.MoveNext
        frmSeguimComunic.Label3.caption = Ador(0).Value
        Ador.MoveNext
        frmSeguimComunic.Label10.caption = Ador(0).Value
        Ador.MoveNext
        frmSeguimComunic.Label4.caption = Ador(0).Value
        Ador.MoveNext
        frmSeguimComunic.Label7.caption = Ador(0).Value
        Ador.MoveNext
        frmSeguimComunic.cmdNuevas.caption = Ador(0).Value
        Ador.MoveNext
        frmSeguimComunic.cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        frmSeguimComunic.cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
       
        Ador.MoveNext
        sdbgPet.Groups.Item(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups.Item(1).caption = Ador(0).Value
        Ador.MoveNext
        For i = 0 To 4
            sdbgPet.Columns(i).caption = Ador(0).Value
            Ador.MoveNext
        Next
        sdbgPet.Columns("WEB").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("MAIL").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("CARTA").caption = Ador(0).Value
        Ador.MoveNext
        sIdiLaNotificacion = Ador(0).Value
        Ador.MoveNext
        sIdioma(3) = Ador(0).Value
        Ador.MoveNext
        sIdioma(4) = Ador(0).Value
        Ador.MoveNext
        sIdioma(5) = Ador(0).Value
        Ador.MoveNext
        sIdioma(6) = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("INCORRECTA").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("REENVIAR").caption = Ador(0).Value
        Ador.MoveNext
        'Iniciando sesi�n de correo ...
        sIdiInicSesCorreo = Ador(0).Value
        Ador.MoveNext
        'Cerrando sesi�n de correo ...
        sIdiInicSesCorreo = Ador(0).Value

    
        Ador.Close
        
        
    End If
    
    Set Ador = Nothing
    
End Sub
Public Sub EliminarNotificacionSeleccionada()
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminar(sIdiLaNotificacion & " " & sdbgPet.Columns(1).Value & " (" & sdbgPet.Columns(2).Value & ")")

    If irespuesta = vbYes Then
        
        Screen.MousePointer = vbHourglass
        teserror = oOrdenEntrega.EliminarComunicacion(sdbgPet.Columns("ID").Value)
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
        
        basSeguridad.RegistrarAccion accionessummit.ACCSeguimientoPedElimComunic, "Anyo:" & oOrdenEntrega.Anyo & " ORDEN:" & oOrdenEntrega.Anyo & "/" & oOrdenEntrega.NumPedido & "/" & oOrdenEntrega.Numero & " ID:" & oOrdenEntrega.Id & "PEDIDO:" & oOrdenEntrega.idPedido & "NOTIF: " & sdbgPet.Columns(2).Text & sdbgPet.Bookmark
        sdbgPet.SelBookmarks.Add sdbgPet.Bookmark
        sdbgPet.DeleteSelected
        'sdbgPet.RemoveItem (sdbgPet.AddItemRowIndex(sdbgPet.Bookmark))
        
    End If
    
    sdbgPet.SelBookmarks.RemoveAll
    
    If sdbgPet.Rows = 0 Then
        cmdEliminar.Enabled = False
    Else
        cmdEliminar.Enabled = True
    End If
    
    Exit Sub
Error:
   If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "CargarRecursos", err, Erl, , m_bActivado)
        Exit Sub
   End If
End Sub

Private Sub sdbgPet_AfterDelete(RtnDispErrMsg As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Exit Sub
    End If
   
    RtnDispErrMsg = 0
    If Me.Visible Then sdbgPet.SetFocus
    If sdbgPet.Rows > 0 Then
        sdbgPet.Bookmark = sdbgPet.RowBookmark(sdbgPet.Row)
    End If
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "sdbgPet_AfterDelete", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPet_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Exit Sub
    End If
    
    DispPromptMsg = 0
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "sdbgPet_BeforeDelete", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que salta cuando se hace doble click sobre el grid.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Tiempo m�ximo: 0.1seg </remarks>

Private Sub sdbgPet_BtnClick()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Exit Sub
    End If
    
    If oOrdenEntrega.EstaDeBajaLogica Then Exit Sub
    
    'REENVIAR
    If UCase(sdbgPet.Columns(sdbgPet.col).Name) = "REENVIAR" Then
        If Not m_bReenviando Then
            m_bReenviando = True
            
            m_sId = sdbgPet.Columns("ID").Value
        
            'Las comunicaciones via carta no se podr�n reenviar, el bot�n no har� nada.
            If oOrdenEntrega.Notificaciones.Item(m_sId).mail = False Then Exit Sub
            
            'Si la comunicaci�n no tuviese un id registro_email se sacar� un mensaje al usuario
            If oOrdenEntrega.Notificaciones.Item(m_sId).IDRegistroMail = 0 Then
                oMensajes.ImposibleReenviarComunicacion
                Exit Sub
            Else
                ReenviarEMail oOrdenEntrega.Notificaciones.Item(m_sId).IDRegistroMail
            End If
            
            m_bReenviando = False
        End If
    End If
    
    Exit Sub
Error:
    m_bReenviando = False
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSeguimComunic", "sdbgPet_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub


''' <summary>
''' Funci�n que reenvia un mail.
''' </summary>
''' <param name="lIdRegistroEMail">Id del registro de email.</param>
''' <returns></returns>
''' <remarks>Llamada desde: sdbgPet_BtnClick() </remarks>
''' <remarks>Tiempo m�ximo: 0,1seg </remarks>

Private Function ReenviarEMail(ByVal lIdRegistroEMail As Long)
    Dim oEMail As CEMail
    Dim oAdjun As CEspecificacion
    Dim errormail As TipoErrorSummit
    'Guardaremos los adjuntos en un array de adjuntos en temp.
    Dim arrAdjuntos() As String
    Dim i As Integer
    Dim sTemp As String
    Dim teserror As TipoErrorSummit
    Dim fso As Scripting.FileSystemObject

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
       Exit Function
    End If
   
    'Enviamos un mail al proveedor
    If Not bSesionIniciada Or oIdsMail Is Nothing Then
        frmESPERA.ProgressBar1.Value = 2
        frmESPERA.lblDetalle = sIdiInicSesCorreo
        frmESPERA.lblDetalle.Refresh
        DoEvents
        Set oIdsMail = IniciarSesionMail
        bSesionIniciada = True
    End If
    
    'Obtenemos los datos que necesitamos para componer el mail.
    Set oEMail = oFSGSRaiz.Generar_CEMail
    oEMail.Id = lIdRegistroEMail
    If Not oEMail.CargarDatosMail Then
    'No existe ese registro en REGISTRO_EMAIL.
        oMensajes.ComunicacionEliminada
        'Finalizar sesi�n mail.
        If bSesionIniciada Then
            frmESPERA.ProgressBar1.Value = 5
            frmESPERA.lblDetalle = sIdiCerrSesCorreo
            FinalizarSesionMail
        End If
        Exit Function
    End If
    
    'Si no tiene cuerpo mostramos un mensaje al usuario para que emita una nueva comunicaci�n.
    If Trim(oEMail.Cuerpo) = "" Then
        oMensajes.Comunicaci�nSinCuerpo
        'Finalizar sesi�n mail.
        If bSesionIniciada Then
            frmESPERA.ProgressBar1.Value = 5
            frmESPERA.lblDetalle = sIdiCerrSesCorreo
            FinalizarSesionMail
        End If
        Exit Function
    End If
    
    'Guardamos los adjuntos en el array.
    If oEMail.NumAdjun > 0 Then
        oEMail.DevolverAdjuntosMail
        
        ReDim arrAdjuntos(oEMail.NumAdjun - 1)
        
        i = 0
        For Each oAdjun In oEMail.Adjuntos
            arrAdjuntos(i) = oAdjun.nombre
            EscribirEspecificacionADisco oAdjun, EspMailAdjun
            i = i + 1
        Next
    End If
    
    g_bCancelarMail = False
    oOrdenEntrega.Notificaciones.Item(m_sId).Fallido = False
    
    frmESPERA.ProgressBar1.Value = 8
    frmESPERA.lblDetalle = ""
    frmESPERA.lblDetalle.Refresh
    
    'Componemos el mail a enviar.
    If oEMail.NumAdjun > 0 Then
        errormail = ComponerMensaje(oEMail.Para, oEMail.Subject, oEMail.Cuerpo, arrAdjuntos, "frmSeguimComunic", _
                                        entidadNotificacion:=entidadNotificacion.PedDirecto, _
                                        tipoNotificacion:=TipoNotificacionEmail.PedNotificacionProve, _
                                        lIdInstancia:=oOrdenEntrega.Id)
    Else
        errormail = ComponerMensaje(oEMail.Para, oEMail.Subject, oEMail.Cuerpo, , "frmSeguimComunic", _
                                        entidadNotificacion:=entidadNotificacion.PedDirecto, _
                                        tipoNotificacion:=TipoNotificacionEmail.PedNotificacionProve, _
                                        lIdInstancia:=oOrdenEntrega.Id)
    End If
    
    If errormail.NumError <> TESnoerror Then
        TratarError errormail
        oOrdenEntrega.Notificaciones.Item(m_sId).Fallido = True
    End If
    
    If g_bCancelarMail = True Or errormail.NumError <> TESnoerror Then
        'Si se ha cancelado o se ha producido error en el reenv�o no se almacena en BD
    Else
        'Se guarda en la propiedad IdRegistroMail de la notificaci�n el id del nuevo REGISTRO_EMAIL y se actualiza la comunicaci�n (se le mete el REGISTRO_EMAIL y la fecha)
        If g_lIDRegistroEmail <> 0 Then
            oOrdenEntrega.Notificaciones.Item(m_sId).IDRegistroMail = g_lIDRegistroEmail
            teserror = oOrdenEntrega.Notificaciones.Item(m_sId).ActualizarComunicacion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                oOrdenEntrega.Notificado = Notificado
                oOrdenEntrega.ConComunicaciones = True
                'Si la comunicaci�n ha sido reenviada correctamente mostramos un mensaje al usuario.
                oMensajes.ComunicacionReenviada
            End If
        End If
    End If
    DoEvents
    
    'Eliminar los adjuntos de disco
    If oEMail.NumAdjun > 0 Then
        Set fso = New Scripting.FileSystemObject
        sTemp = FSGSLibrary.DevolverPathFichTemp
        For i = 0 To UBound(arrAdjuntos)
            fso.DeleteFile sTemp & arrAdjuntos(i)
        Next
        Set fso = Nothing
    End If
    
    
    'Finalizar sesi�n mail.
    If bSesionIniciada Then
        frmESPERA.ProgressBar1.Value = 5
        frmESPERA.lblDetalle = sIdiCerrSesCorreo
        FinalizarSesionMail
    End If
    
    'Registramos la acci�n.
    If Not g_bCancelarMail = True Then
        basSeguridad.RegistrarAccion accionessummit.ACCSeguimientoPedComunicar, "Orden entrega:" & oOrdenEntrega.Anyo & "/" & oOrdenEntrega.NumPedido & "/" & oOrdenEntrega.Numero & " ID:" & oOrdenEntrega.Id & ", pedido: " & oOrdenEntrega.idPedido & ", proveedor: " & oOrdenEntrega.ProveDen & ",TipoComunicacion: " & oOrdenEntrega.Tipo
    End If
    
    cmdRestaurar_Click
    
    Unload frmESPERA
    Screen.MousePointer = vbNormal

    Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPSeguimComunic", "ReenviarEMail", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

