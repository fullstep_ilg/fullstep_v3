VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFlujosAprobacionPedidos 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11550
   Icon            =   "frmFlujosAprobacionPedidos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6360
   ScaleWidth      =   11550
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab ssTabFlujos 
      Height          =   4815
      Left            =   120
      TabIndex        =   4
      Top             =   960
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   8493
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "DEmpresas"
      TabPicture(0)   =   "frmFlujosAprobacionPedidos.frx":0562
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "sdbgSelEmpresas"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "DTipos de Pedido"
      TabPicture(1)   =   "frmFlujosAprobacionPedidos.frx":057E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sdbgSelTipoPedidos"
      Tab(1).ControlCount=   1
      Begin SSDataWidgets_B.SSDBGrid sdbgSelTipoPedidos 
         Height          =   4050
         Left            =   -74880
         TabIndex        =   5
         Top             =   480
         Width           =   10965
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   7
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmFlujosAprobacionPedidos.frx":059A
         stylesets(1).Name=   "StringTachado"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   -1  'True
         EndProperty
         stylesets(1).Picture=   "frmFlujosAprobacionPedidos.frx":05B6
         stylesets(2).Name=   "Bloqueado"
         stylesets(2).BackColor=   12632256
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmFlujosAprobacionPedidos.frx":05D2
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   1005
         Columns(0).Name =   "SEL"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   1773
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   6244
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "CONCEPTO"
         Columns(3).Name =   "CONCEPTO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Caption=   "RECEPCIONABLE"
         Columns(4).Name =   "RECEPCIONABLE"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   3200
         Columns(5).Caption=   "ALMACENABLE"
         Columns(5).Name =   "ALMACENABLE"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "ID"
         Columns(6).Name =   "ID"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   19341
         _ExtentY        =   7144
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgSelEmpresas 
         Height          =   4050
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   10965
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   10
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmFlujosAprobacionPedidos.frx":05EE
         stylesets(1).Name=   "StringTachado"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   -1  'True
         EndProperty
         stylesets(1).Picture=   "frmFlujosAprobacionPedidos.frx":060A
         stylesets(2).Name=   "Bloqueado"
         stylesets(2).BackColor=   12632256
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmFlujosAprobacionPedidos.frx":0626
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   10
         Columns(0).Width=   873
         Columns(0).Name =   "SEL"
         Columns(0).Alignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "ID"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1720
         Columns(2).Caption=   "NIF"
         Columns(2).Name =   "NIF"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "DENOMINACION"
         Columns(3).Name =   "DENOMINACION"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   2884
         Columns(4).Caption=   "DIRECCION"
         Columns(4).Name =   "DIRECCION"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2699
         Columns(5).Caption=   "POBLACION"
         Columns(5).Name =   "POBLACION"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   2196
         Columns(6).Caption=   "CP"
         Columns(6).Name =   "CP"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   1958
         Columns(7).Caption=   "PAIS"
         Columns(7).Name =   "PAIS"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   2434
         Columns(8).Caption=   "PROVINCIA"
         Columns(8).Name =   "PROVINCIA"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "EN_ROL"
         Columns(9).Name =   "EN_ROL"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   11
         Columns(9).FieldLen=   256
         _ExtentX        =   19341
         _ExtentY        =   7144
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   4320
      TabIndex        =   3
      Top             =   5880
      Width           =   1050
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   5535
      TabIndex        =   2
      Top             =   5880
      Width           =   1050
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcFlujosAprobacion 
      Height          =   285
      Left            =   2400
      TabIndex        =   0
      Top             =   480
      Width           =   4185
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2487
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3625
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "Denominaci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   7382
      _ExtentY        =   503
      _StockProps     =   93
      ForeColor       =   0
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Label lblSeleccione 
      BackColor       =   &H00808000&
      Caption         =   "DSeleccione el flujo de aprobaci�n de pedido para las categorias y las empresas y los tipos de pedido en los que se activa"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   120
      Width           =   11175
   End
   Begin VB.Label lblFlujoDeAprobacion 
      BackColor       =   &H00808000&
      Caption         =   "DFlujo de Aprobacion"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   2055
   End
End
Attribute VB_Name = "frmFlujosAprobacionPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private m_sCadenaTipoPedidos As String 'Cadena con todos los tipos de pedido seleccionados
Private m_sCadenaEmpresas As String 'Cadena con todas las empresas seleccionadas
Private m_sGasto As String
Private m_sInversion As String
Private m_sGastoInversion As String
Private m_sNoAlmacenable As String
Private m_sObligatorioAlmacenar As String
Private m_sOpcionalAlmacenar As String
Private m_sNoRecepcionar As String
Private m_sObligatorioRecepcionar As String
Private m_sOpcionalRecepcionar As String
Private m_sMensajeValidacion As String
Private m_bCambiosRealizados As Boolean

Public lCategoria As Long
Public lNivelCategoria As Long


Private Sub Form_Load()
    PonerFieldSeparator Me
    
    'Carga los textos de la pantalla
    CargarRecursos
    'Cargamos los tipos de pedido en la grid
    CargarTipoPedidos
    'Cargamos las empresas
    CargarEmpresas
    'Cargamos los flujos de aprobacion(solicitudes de tipo Solicitud de pedido)
    CargarSolicitudes
    
    m_sCadenaTipoPedidos = ""
    m_sCadenaEmpresas = ""

End Sub

''' <summary>
''' Carga los textos de la pantalla
''' </summary>
Private Sub CargarRecursos()
   Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSAPROBACIONPEDIDOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1 Flujos de aprobacion
        Me.lblFlujoDeAprobacion.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgSelEmpresas.Columns("NIF").caption = Ador(0).Value '2 nif
        Ador.MoveNext
        Me.sdbgSelEmpresas.Columns("DENOMINACION").caption = Ador(0).Value '3 Denominacion
        Me.sdbgSelTipoPedidos.Columns("DEN").caption = Ador(0).Value '3 Denominacion
        Ador.MoveNext
        Me.sdbgSelEmpresas.Columns("DIRECCION").caption = Ador(0).Value '4 Direccion
        Ador.MoveNext
        Me.sdbgSelEmpresas.Columns("CP").caption = Ador(0).Value '5 Cp
        Ador.MoveNext
        Me.sdbgSelEmpresas.Columns("PAIS").caption = Ador(0).Value '6 Pais
        Ador.MoveNext
        Me.sdbgSelEmpresas.Columns("PROVINCIA").caption = Ador(0).Value '7 Provincia
        Ador.MoveNext
        Me.sdbgSelTipoPedidos.Columns("COD").caption = Ador(0).Value '8 codigo
        Ador.MoveNext
        Me.sdbgSelTipoPedidos.Columns("CONCEPTO").caption = Ador(0).Value '9 Concepto
        Ador.MoveNext
        Me.sdbgSelTipoPedidos.Columns("RECEPCIONABLE").caption = Ador(0).Value '10 Recepcionable
        Ador.MoveNext
        Me.sdbgSelTipoPedidos.Columns("ALMACENABLE").caption = Ador(0).Value '11 Almacenable
        Ador.MoveNext
        Me.cmdAceptar.caption = Ador(0).Value '12 Aceptar
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).Value '13 Cancelar
        Ador.MoveNext
        m_sGasto = Ador(0).Value
        Ador.MoveNext
        m_sInversion = Ador(0).Value
        Ador.MoveNext
        m_sGastoInversion = Ador(0).Value
        Ador.MoveNext
        m_sNoAlmacenable = Ador(0).Value
        Ador.MoveNext
        m_sObligatorioAlmacenar = Ador(0).Value
        Ador.MoveNext
        m_sOpcionalAlmacenar = Ador(0).Value
        Ador.MoveNext
        m_sNoRecepcionar = Ador(0).Value
        Ador.MoveNext
        m_sObligatorioRecepcionar = Ador(0).Value
        Ador.MoveNext
        m_sOpcionalRecepcionar = Ador(0).Value
        Ador.MoveNext
        m_sMensajeValidacion = Ador(0).Value
        Ador.MoveNext
        Me.sdbgSelEmpresas.Columns("POBLACION").caption = Ador(0).Value '24 Poblacion
        Ador.MoveNext
        ssTabFlujos.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        ssTabFlujos.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblSeleccione.caption = Ador(0).Value
        Ador.Close
    End If
End Sub

Public Sub PonerFieldSeparator(ByRef Pantalla As Form)
    Dim sdg As Control
    Dim Name4 As String
    Dim Name5 As String
        
    For Each sdg In Pantalla.Controls
        
        Name4 = LCase(Left(sdg.Name, 4))
        Name5 = LCase(Left(sdg.Name, 5))
        
        If Name4 = "sdbg" Or Name4 = "sdbc" Or Name5 = "sdbdd" Or Name4 = "ssdb" Then
            If sdg.DataMode = ssDataModeAddItem Then
                sdg.FieldSeparator = Chr(m_lSeparador)
            End If
        End If
    Next
End Sub
''' <summary>
''' Carga las solicitudes del tipo Solicitud de pedido para a�adir al combo de flujos de aprobacion
''' </summary>
Private Sub CargarSolicitudes()
    Dim oRes As ADODB.Recordset
    Dim oSolicitudes As CSolicitudes
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    Set oRes = oSolicitudes.DevolverSolicitudesDeTipo(tiposolicitud.SolicitudDePedidoCatalogo)
    While Not oRes.EOF
        sdbcFlujosAprobacion.AddItem oRes("COD").Value & Chr(m_lSeparador) & oRes("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & oRes("ID").Value
        oRes.MoveNext
    Wend
End Sub
''' <summary>
''' Inicializa el combo de solicitudes indicando que columna sera el value y text
''' </summary>
Private Sub sdbcFlujosAprobacion_InitColumnProps()
    sdbcFlujosAprobacion.DataFieldList = "Column 2"
    sdbcFlujosAprobacion.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Carga todos los tipos de pedido que esten asociados a la categoria
''' </summary>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,3seg.</remarks>
Private Sub CargarTipoPedidos()
    Dim oTiposPedido As CTiposPedido
    Dim oTipoPedido As CTipoPedido
    Set oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    
    'Cargo todos los tipos de pedidos
    oTiposPedido.CargarTodosLosTiposPedidosPorCategoria gParametrosInstalacion.gIdioma, lCategoria, lNivelCategoria
    
    For Each oTipoPedido In oTiposPedido
        sdbgSelTipoPedidos.AddItem 0 & Chr(m_lSeparador) & oTipoPedido.Cod & Chr(m_lSeparador) & oTipoPedido.Den & Chr(m_lSeparador) & oTipoPedido.CodConcep & Chr(m_lSeparador) & oTipoPedido.CodRecep & Chr(m_lSeparador) & oTipoPedido.CodAlmac & Chr(m_lSeparador) & oTipoPedido.Id
    Next
    
    Set oTiposPedido = Nothing
End Sub


''' <summary>
''' Carga las empresas en la grid
''' </summary>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,3seg.</remarks>
Private Sub CargarEmpresas()
Dim oEmpresas As CEmpresas
Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
Dim rs As Recordset


'recogemos las empresas
Set rs = oEmpresas.DevolverTodasEmpresas(0)
sdbgSelEmpresas.RemoveAll

'cargamos el grid de empresas
While Not rs.EOF
    sdbgSelEmpresas.AddItem 0 & Chr(m_lSeparador) & rs("ID").Value & Chr(m_lSeparador) & rs("NIF").Value & Chr(m_lSeparador) & rs("DEN").Value & Chr(m_lSeparador) & rs("DIR").Value & Chr(m_lSeparador) & rs("POB").Value & Chr(m_lSeparador) & rs("CP").Value & Chr(m_lSeparador) & rs("PAIS").Value & Chr(m_lSeparador) & rs("PROVINCIA").Value & Chr(m_lSeparador) & 0
    rs.MoveNext
    
Wend
Set rs = Nothing
End Sub


''' <summary>
''' Evento que salta al seleccionar un tipo de pedido en el grid
''' Actualiza la cadena de tipo de pedidos seleccionados
''' </summary>
''' <param name="Cancel">Si ha cancelado el formulario</param>
''' <remarks>Tiempo m�ximo:0,1seg.</remarks>
Private Sub sdbgSelTipoPedidos_Change()
    If sdbgSelTipoPedidos.Col = -1 Then Exit Sub
    
    
    If sdbgSelTipoPedidos.Columns("SEL").Value = "-1" Or sdbgSelTipoPedidos.Columns("SEL").Value = "1" Then
        'Seleccionar Empresa
        If Right(m_sCadenaTipoPedidos, 1) = "," Or m_sCadenaTipoPedidos = "" Then
            m_sCadenaTipoPedidos = m_sCadenaTipoPedidos & sdbgSelTipoPedidos.Columns("ID").Value & ","
        Else
            m_sCadenaTipoPedidos = m_sCadenaTipoPedidos & "," & sdbgSelTipoPedidos.Columns("ID").Value
        End If
        
    Else
        'Deseleccionar Tipo Pedido
        Dim sCadenaABuscar As String
        Dim sCadenaNueva As String
        Dim arrAux() As String
        arrAux = Split(m_sCadenaTipoPedidos, ",")
        sCadenaABuscar = sdbgSelTipoPedidos.Columns("ID").Value

        Dim i As Integer
        For i = 0 To UBound(arrAux)
            If sCadenaABuscar <> arrAux(i) And arrAux(i) <> "" Then
                sCadenaNueva = sCadenaNueva & arrAux(i) & ","
            End If
        Next i
        m_sCadenaTipoPedidos = sCadenaNueva
        
    End If
    sdbgSelTipoPedidos.Update

End Sub

''' <summary>
''' Evento que salta cada vez que se carga una fila en el grid
''' </summary>
''' <param name="Bookmark">Bookmark de la fila que se carga</param>
Private Sub sdbgSelTipoPedidos_RowLoaded(ByVal Bookmark As Variant)
    'Se le ponen los textos a los valores de CONCEPTO, ALMACENABLE,RECEPCIONABLE de los tipos de pedido
    Select Case sdbgSelTipoPedidos.Columns("CONCEPTO").Value
        Case TipoConcepto.Gasto
            sdbgSelTipoPedidos.Columns("CONCEPTO").Value = m_sGasto
        Case TipoConcepto.Inversion
            sdbgSelTipoPedidos.Columns("CONCEPTO").Value = m_sInversion
        Case TipoConcepto.Ambos
            sdbgSelTipoPedidos.Columns("CONCEPTO").Value = m_sGastoInversion
    End Select
    
    Select Case sdbgSelTipoPedidos.Columns("RECEPCIONABLE").Value
        Case TipoArtRecepcionable.NoRececpionar
            sdbgSelTipoPedidos.Columns("RECEPCIONABLE").Value = m_sNoRecepcionar
        Case TipoArtRecepcionable.ObligatorioRececpionar
            sdbgSelTipoPedidos.Columns("RECEPCIONABLE").Value = m_sObligatorioRecepcionar
        Case TipoArtRecepcionable.OpcionalRececpionar
            sdbgSelTipoPedidos.Columns("RECEPCIONABLE").Value = m_sOpcionalRecepcionar
    End Select
    
    Select Case sdbgSelTipoPedidos.Columns("ALMACENABLE").Value
        Case TipoArtAlmacenable.NoAlmacenable
            sdbgSelTipoPedidos.Columns("ALMACENABLE").Value = m_sNoAlmacenable
        Case TipoArtAlmacenable.ObligatorioAlmacenar
            sdbgSelTipoPedidos.Columns("ALMACENABLE").Value = m_sObligatorioAlmacenar
        Case TipoArtAlmacenable.OpcionalAlmacenar
            sdbgSelTipoPedidos.Columns("ALMACENABLE").Value = m_sOpcionalAlmacenar
    End Select
End Sub


''' <summary>
''' Evento que salta al seleccionar una empresa en la grid.
''' Actualiza la cadena de empresas seleccionadas. Si viene de la configuracion de la solicitud almacena en BBDD
''' </summary>
''' <param name="Cancel">Si ha cancelado el formulario</param>
''' <remarks>Tiempo m�ximo:0,1seg.</remarks>
Private Sub sdbgSelEmpresas_Change()

If sdbgSelEmpresas.Col = -1 Then Exit Sub

If sdbgSelEmpresas.Columns("EN_ROL").Value = True Then
    sdbgSelEmpresas.CancelUpdate
    Exit Sub
End If

    m_bCambiosRealizados = True
    
    If sdbgSelEmpresas.Columns("SEL").Value = "-1" Or sdbgSelEmpresas.Columns("SEL").Value = "1" Then
        'Seleccionar Empresa
        If Right(m_sCadenaEmpresas, 1) = "," Or m_sCadenaEmpresas = "" Then
            m_sCadenaEmpresas = m_sCadenaEmpresas & sdbgSelEmpresas.Columns("ID").Value & ","
        Else
            m_sCadenaEmpresas = m_sCadenaEmpresas & "," & sdbgSelEmpresas.Columns("ID").Value
        End If
    Else
        'Deseleccionar Empresa
        Dim sCadenaABuscar As String
        Dim sCadenaNueva As String
        Dim arrAux() As String
        arrAux = Split(m_sCadenaEmpresas, ",")
        sCadenaABuscar = sdbgSelEmpresas.Columns("ID").Value

        Dim i As Integer
        For i = 0 To UBound(arrAux)
            If sCadenaABuscar <> arrAux(i) Then
                sCadenaNueva = sCadenaNueva & arrAux(i) & ","
            End If
        Next i
        m_sCadenaEmpresas = sCadenaNueva
        
    End If
    sdbgSelEmpresas.Update

End Sub
''' <summary>
''' Graba en BD para una categoria, los flujos de aprobacion(solicitud) de una solicitud de pedido por empresa y tipo de pedido
''' </summary>
Private Sub cmdAceptar_Click()
    Dim sSolicitudSeleccionada As String
    Dim arrTiposPedidos() As String
    Dim arrEmpresas() As String
    Dim oFlujoAprobacion As cFlujoAprobacion
    Dim teserror As TipoErrorSummit
    
    
    'Quitamos la coma del final si la hubiese
    If Right(m_sCadenaEmpresas, 1) = "," Then
        m_sCadenaEmpresas = Mid(m_sCadenaEmpresas, 1, Len(m_sCadenaEmpresas) - 1)
    End If
    If Right(m_sCadenaTipoPedidos, 1) = "," Then
        m_sCadenaTipoPedidos = Mid(m_sCadenaTipoPedidos, 1, Len(m_sCadenaTipoPedidos) - 1)
    End If
    
    'Creamos los arrays de tipos de pedidos y empresas
    arrTiposPedidos = Split(m_sCadenaTipoPedidos, ",")
    arrEmpresas = Split(m_sCadenaEmpresas, ",")
    
    sSolicitudSeleccionada = sdbcFlujosAprobacion.Value
    
    'Validaciones
    If sSolicitudSeleccionada = "" Or UBound(arrTiposPedidos) = -1 Or UBound(arrEmpresas) = -1 Then
        MsgBox m_sMensajeValidacion, vbExclamation
        Exit Sub
    End If
    
    Dim i As Integer
    Dim X As Integer
    For i = 0 To UBound(arrTiposPedidos)
        'Recorremos los tipos de pedidos seleccionados
        For X = 0 To UBound(arrEmpresas)
            'Por cada tipo de pedido recorremos las empresas seleccionadas y grabamos cada flujo de aprobacion
            Set oFlujoAprobacion = oFSGSRaiz.Generar_CFlujoAprobacion
            oFlujoAprobacion.Categoria = lCategoria
            oFlujoAprobacion.NivelCat = lNivelCategoria
            oFlujoAprobacion.Emp = arrEmpresas(X)
            oFlujoAprobacion.TipoPedido = arrTiposPedidos(i)
            oFlujoAprobacion.Solicitud = sSolicitudSeleccionada
            teserror = oFlujoAprobacion.A�adirABaseDeDatos
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
            End If
        Next
    Next i
    
    Unload Me
End Sub
''' <summary>
''' Descarga el formulario
''' </summary>
Private Sub cmdCancelar_Click()
    Unload Me
End Sub
