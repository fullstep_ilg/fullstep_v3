VERSION 5.00
Object = "{3C62B3DD-12BE-4941-A787-EA25415DCD27}#10.0#0"; "crviewer.dll"
Begin VB.Form Preview 
   Caption         =   "Preview"
   ClientHeight    =   4770
   ClientLeft      =   915
   ClientTop       =   3930
   ClientWidth     =   8565
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Preview.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4770
   ScaleWidth      =   8565
   Begin CrystalActiveXReportViewerLib10Ctl.CrystalActiveXReportViewer crViewer 
      Height          =   7005
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   8025
      lastProp        =   600
      _cx             =   14155
      _cy             =   12356
      DisplayGroupTree=   -1  'True
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   0   'False
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   0   'False
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
      EnableLogonPrompts=   -1  'True
   End
End
Attribute VB_Name = "Preview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public g_sOrigen As String
Public g_oReport As Object


Private Sub crViewer_PrintButtonClicked(UseDefault As Boolean)
g_oReport.PrinterSetup Me.hWnd
End Sub

Private Sub Form_Resize()

    crViewer.Top = 0
    crViewer.Left = 0
    crViewer.Height = ScaleHeight
    crViewer.Width = ScaleWidth

End Sub

Private Sub Form_Unload(Cancel As Integer)
Set g_oReport = Nothing
Select Case g_sOrigen
    
    Case "frmLstPROCE" 'apertura desde frmListados
        If frmListados.ofrmLstProce1 Is Nothing Then Exit Sub
        
        If Not frmListados.ofrmLstProce1.g_adoresArt Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresArt.Close
            Set frmListados.ofrmLstProce1.g_adoresArt = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresAtrib Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresAtrib.Close
            Set frmListados.ofrmLstProce1.g_adoresAtrib = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresDG Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresDG.Close
            Set frmListados.ofrmLstProce1.g_adoresDG = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresEsp Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresEsp.Close
            Set frmListados.ofrmLstProce1.g_adoresEsp = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresOrg Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresOrg.Close
            Set frmListados.ofrmLstProce1.g_adoresOrg = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresPer Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresPer.Close
            Set frmListados.ofrmLstProce1.g_adoresPer = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresPre1 Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresPre1.Close
            Set frmListados.ofrmLstProce1.g_adoresPre1 = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresPre2 Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresPre2.Close
            Set frmListados.ofrmLstProce1.g_adoresPre2 = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresPre3 Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresPre3.Close
            Set frmListados.ofrmLstProce1.g_adoresPre3 = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresPre4 Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresPre4.Close
            Set frmListados.ofrmLstProce1.g_adoresPre4 = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresPro Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresPro.Close
            Set frmListados.ofrmLstProce1.g_adoresPro = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresAdj Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresAdj.Close
            Set frmListados.ofrmLstProce1.g_adoresAdj = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresAho Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresAho.Close
            Set frmListados.ofrmLstProce1.g_adoresAho = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresComu Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresComu.Close
            Set frmListados.ofrmLstProce1.g_adoresComu = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresReu Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresReu.Close
            Set frmListados.ofrmLstProce1.g_adoresReu = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresOfe Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresOfe.Close
            Set frmListados.ofrmLstProce1.g_adoresOfe = Nothing
        End If
    
    
    Case "frmPROCE" 'apertura
        If frmPROCE.g_ofrmLstPROCE Is Nothing Then Exit Sub
    
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresArt Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresArt.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresArt = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresAtrib Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresAtrib.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresAtrib = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresDG Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresDG.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresDG = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresEsp Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresEsp.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresEsp = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresOrg Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresOrg.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresOrg = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPer Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPer.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPer = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPre1 Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPre1.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPre1 = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPre2 Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPre2.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPre2 = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPre3 Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPre3.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPre3 = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPre4 Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPre4.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPre4 = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPro Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPro.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPro = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresAdj Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresAdj.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresAdj = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresAho Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresAho.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresAho = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresComu Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresComu.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresComu = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresReu Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresReu.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresReu = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresOfe Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresOfe.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresOfe = Nothing
        End If
    
    
    Case "frmLstOFERec" 'Rececpci�n de ofertas desde frmListados
        If frmListados.ofrmLstProce4 Is Nothing Then Exit Sub
        
        If Not frmListados.ofrmLstProce1.g_adoresDG Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresDG.Close
            Set frmListados.ofrmLstProce1.g_adoresDG = Nothing
        End If
        If Not frmListados.ofrmLstProce1.g_adoresAtrib Is Nothing Then
            frmListados.ofrmLstProce1.g_adoresAtrib.Close
            Set frmListados.ofrmLstProce1.g_adoresAtrib = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPre1 Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPre1.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPre1 = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPre2 Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPre2.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPre2 = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPre3 Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPre3.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPre3 = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresPre4 Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresPre4.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresPre4 = Nothing
        End If
        If Not frmPROCE.g_ofrmLstPROCE.g_adoresEsp Is Nothing Then
            frmPROCE.g_ofrmLstPROCE.g_adoresEsp.Close
            Set frmPROCE.g_ofrmLstPROCE.g_adoresEsp = Nothing
        End If
    
    
    Case "frmOFERec" 'Rececpci�n de ofertas
        If frmOFERec.ofrmLstPROCE Is Nothing Then Exit Sub
        
        If Not frmOFERec.ofrmLstPROCE.g_adoresDG Is Nothing Then
            frmOFERec.ofrmLstPROCE.g_adoresDG.Close
            Set frmOFERec.ofrmLstPROCE.g_adoresDG = Nothing
        End If
        If Not frmOFERec.ofrmLstPROCE.g_adoresAtrib Is Nothing Then
            frmOFERec.ofrmLstPROCE.g_adoresAtrib.Close
            Set frmOFERec.ofrmLstPROCE.g_adoresAtrib = Nothing
        End If
        If Not frmOFERec.ofrmLstPROCE.g_adoresPre1 Is Nothing Then
            frmOFERec.ofrmLstPROCE.g_adoresPre1.Close
            Set frmOFERec.ofrmLstPROCE.g_adoresPre1 = Nothing
        End If
        If Not frmOFERec.ofrmLstPROCE.g_adoresPre2 Is Nothing Then
            frmOFERec.ofrmLstPROCE.g_adoresPre2.Close
            Set frmOFERec.ofrmLstPROCE.g_adoresPre2 = Nothing
        End If
        If Not frmOFERec.ofrmLstPROCE.g_adoresPre3 Is Nothing Then
            frmOFERec.ofrmLstPROCE.g_adoresPre3.Close
            Set frmOFERec.ofrmLstPROCE.g_adoresPre3 = Nothing
        End If
        If Not frmOFERec.ofrmLstPROCE.g_adoresPre4 Is Nothing Then
            frmOFERec.ofrmLstPROCE.g_adoresPre4.Close
            Set frmOFERec.ofrmLstPROCE.g_adoresPre4 = Nothing
        End If
        If Not frmOFERec.ofrmLstPROCE.g_adoresEsp Is Nothing Then
            frmOFERec.ofrmLstPROCE.g_adoresEsp.Close
            Set frmOFERec.ofrmLstPROCE.g_adoresEsp = Nothing
        End If
    
    
    Case "frmADJ"
        'Hace que el bot�n pierda el foco
        If frmADJ.Visible Then frmADJ.sstabComparativa.SetFocus

    Case "frmLstPedidos"
        If Not frmLstPedidos.g_ador Is Nothing Then
            frmLstPedidos.g_ador.Close
            Set frmLstPedidos.g_ador = Nothing
        End If
        If Not frmLstPedidos.g_ador1 Is Nothing Then
            frmLstPedidos.g_ador1.Close
            Set frmLstPedidos.g_ador1 = Nothing
        End If
        If Not frmLstPedidos.g_ador2 Is Nothing Then
            frmLstPedidos.g_ador2.Close
            Set frmLstPedidos.g_ador2 = Nothing
        End If
        If Not frmLstPedidos.g_ador3 Is Nothing Then
            frmLstPedidos.g_ador3.Close
            Set frmLstPedidos.g_ador3 = Nothing
        End If
        If Not frmLstPedidos.g_ador4 Is Nothing Then
            frmLstPedidos.g_ador4.Close
            Set frmLstPedidos.g_ador4 = Nothing
        End If
        If Not frmLstPedidos.g_ador5 Is Nothing Then
            frmLstPedidos.g_ador5.Close
            Set frmLstPedidos.g_ador5 = Nothing
        End If
        If Not frmLstPedidos.g_ador6 Is Nothing Then
            frmLstPedidos.g_ador6.Close
            Set frmLstPedidos.g_ador6 = Nothing
        End If
        
    Case "frmSeguimiento"
        If Not frmSeguimiento.g_ador Is Nothing Then
            frmSeguimiento.g_ador.Close
            Set frmSeguimiento.g_ador = Nothing
        End If
        If Not frmSeguimiento.g_ador1 Is Nothing Then
            frmSeguimiento.g_ador1.Close
            Set frmSeguimiento.g_ador1 = Nothing
        End If
        If Not frmSeguimiento.g_ador2 Is Nothing Then
            frmSeguimiento.g_ador2.Close
            Set frmSeguimiento.g_ador2 = Nothing
        End If
        If Not frmSeguimiento.g_ador3 Is Nothing Then
            frmSeguimiento.g_ador3.Close
            Set frmSeguimiento.g_ador3 = Nothing
        End If
        If Not frmSeguimiento.g_ador4 Is Nothing Then
            frmSeguimiento.g_ador4.Close
            Set frmSeguimiento.g_ador4 = Nothing
        End If
        If Not frmSeguimiento.g_ador5 Is Nothing Then
            frmSeguimiento.g_ador5.Close
            Set frmSeguimiento.g_ador5 = Nothing
        End If
        If Not frmSeguimiento.g_ador6 Is Nothing Then
            frmSeguimiento.g_ador6.Close
            Set frmSeguimiento.g_ador6 = Nothing
        End If
        
    Case "frmDatosExt"
        If Not frmCatalogoDatosExt.g_ador Is Nothing Then
            frmCatalogoDatosExt.g_ador.Close
            Set frmCatalogoDatosExt.g_ador = Nothing
        End If
    Case "frmLstCatalogo"
        If Not frmLstCatalogo.g_ador Is Nothing Then
            frmLstCatalogo.g_ador.Close
            Set frmLstCatalogo.g_ador = Nothing
        End If
        
    Case "frmLstCATSeguridad"
        If Not frmLstCATSeguridad.g_ador Is Nothing Then
            frmLstCATSeguridad.g_ador.Close
            Set frmLstCATSeguridad.g_ador = Nothing
        End If
    Case "frmLstESTRCOMPMatPorCom"
        If Not frmLstESTRCOMPMatPorCom.g_oADORes Is Nothing Then
            frmLstESTRCOMPMatPorCom.g_oADORes.Close
            Set frmLstESTRCOMPMatPorCom.g_oADORes = Nothing
        End If
        
    Case "frmLstMAT"
        If Not frmLstMAT.g_oADORes Is Nothing Then
            frmLstMAT.g_oADORes.Close
            Set frmLstMAT.g_oADORes = Nothing
        End If
    
    Case "frmLstESTRCOMPComPorMat"
        If Not frmLstESTRCOMPComPorMat.g_oADORes Is Nothing Then
            frmLstESTRCOMPComPorMat.g_oADORes.Close
            Set frmLstESTRCOMPComPorMat.g_oADORes = Nothing
        End If
        If Not frmLstESTRCOMPComPorMat.g_oADORes1 Is Nothing Then
            frmLstESTRCOMPComPorMat.g_oADORes1.Close
            Set frmLstESTRCOMPComPorMat.g_oADORes1 = Nothing
        End If
        If Not frmLstESTRCOMPComPorMat.g_oADORes2 Is Nothing Then
            frmLstESTRCOMPComPorMat.g_oADORes2.Close
            Set frmLstESTRCOMPComPorMat.g_oADORes2 = Nothing
        End If
        If Not frmLstESTRCOMPComPorMat.g_oADORes3 Is Nothing Then
            frmLstESTRCOMPComPorMat.g_oADORes3.Close
            Set frmLstESTRCOMPComPorMat.g_oADORes3 = Nothing
        End If
        If Not frmLstESTRCOMPComPorMat.g_oADORes4 Is Nothing Then
            frmLstESTRCOMPComPorMat.g_oADORes4.Close
            Set frmLstESTRCOMPComPorMat.g_oADORes4 = Nothing
        End If
    
End Select

Me.Visible = False

End Sub
