VERSION 5.00
Begin VB.Form frmPRESPorMatFiltrar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Presupuestos por material (Filtrar)"
   ClientHeight    =   2490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   Icon            =   "frmPRESPorMatFiltrar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2490
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   30
      TabIndex        =   8
      Top             =   1065
      Width           =   5235
      Begin VB.CheckBox chkIgualDen 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3045
         TabIndex        =   3
         Top             =   420
         Width           =   2130
      End
      Begin VB.TextBox txtDEN 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   100
         TabIndex        =   2
         Top             =   360
         Width           =   2715
      End
      Begin VB.OptionButton optDEN 
         BackColor       =   &H00808000&
         Caption         =   "Por denominaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   -60
         Width           =   2130
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   30
      TabIndex        =   6
      Top             =   45
      Width           =   5235
      Begin VB.CheckBox chkIgualCod 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1140
         TabIndex        =   1
         Top             =   420
         Width           =   2475
      End
      Begin VB.TextBox txtCOD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   3
         TabIndex        =   0
         Top             =   360
         Width           =   795
      End
      Begin VB.OptionButton optCOD 
         BackColor       =   &H00808000&
         Caption         =   "Por c�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   0
         Value           =   -1  'True
         Width           =   2130
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2625
      TabIndex        =   5
      Top             =   2100
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1410
      TabIndex        =   4
      Top             =   2100
      Width           =   1005
   End
End
Attribute VB_Name = "frmPRESPorMatFiltrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPresPorMatFiltrar
''' *** Creacion: 5/2/1999 (Javier Arana)

Option Explicit
Private Sub Form_Load()

    ''' * Objetivo: Situar el formulario respecto al
    ''' * Objetivo: principal de Presupuestos
    
    On Error Resume Next
    
    Me.Left = frmPRESPorMat.Left + 500
    Me.Top = frmPRESPorMat.Top + 1000
    CargarRecursos
    Select Case gParametrosGenerales.giNEM
        
        Case 1
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodGMN1
        Case 2
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodGMN2
        Case 3
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodGMN3
        Case 4
            txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodGMN4
    End Select
    
    
    
End Sub
Private Sub optCOD_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    If optCOD.Value = True Then
        optDEN.Value = False
    End If
        
    txtCod.SetFocus
   
End Sub
Private Sub chkIgualDen_Click()
    
    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    txtDen.SetFocus

End Sub
Private Sub chkIgualCod_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    txtCod.SetFocus
    
End Sub
Private Sub optDEN_Click()

    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    If optDEN.Value = True Then
        optCOD.Value = False
    End If
    
    txtDen.SetFocus
    
End Sub

Private Sub cmdAceptar_Click()

    ''' * Objetivo: Aplicar el filtro y descargar
    ''' * Objetivo: el formulario
    
    Screen.MousePointer = vbHourglass
    
    Set frmPRESPorMat.oPresupuestos = Nothing
    Set frmPRESPorMat.oPresupuestos = oFSGSRaiz.Generar_CPresPorMateriales
    
    frmPRESPorMat.ponerCaption "", 3, False
    
    If optCOD.Value = True And txtCod <> "" Then
        
        If frmPRESPorMat.bRMat Then
            frmPRESPorMat.oPresupuestos.CargarTodosLosPresupuestos frmPRESPorMat.iAnyoSeleccionado, frmPRESPorMat.sdbcGMN1_4Cod, frmPRESPorMat.sdbcGMN2_4Cod, frmPRESPorMat.sdbcGMN3_4Cod, , Trim(txtCod), , (chkIgualCod = vbChecked), , , , , , , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
        Else
            frmPRESPorMat.oPresupuestos.CargarTodosLosPresupuestos frmPRESPorMat.iAnyoSeleccionado, frmPRESPorMat.sdbcGMN1_4Cod, frmPRESPorMat.sdbcGMN2_4Cod, frmPRESPorMat.sdbcGMN3_4Cod, , Trim(txtCod), , (chkIgualCod = vbChecked), , , , , , , , , , , True
        End If
            
        If (chkIgualCod.Value = vbChecked) Then
            frmPRESPorMat.ponerCaption Trim(txtCod), 1, False
        Else
            frmPRESPorMat.ponerCaption Trim(txtCod), 1, True
        End If
    
    Else
    
        If optDEN.Value = True And txtDen <> "" Then
                
            If frmPRESPorMat.bRMat Then
                frmPRESPorMat.oPresupuestos.CargarTodosLosPresupuestos frmPRESPorMat.iAnyoSeleccionado, frmPRESPorMat.sdbcGMN1_4Cod, frmPRESPorMat.sdbcGMN2_4Cod, frmPRESPorMat.sdbcGMN3_4Cod, , , Trim(txtDen), , , , , , , True, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
            Else
                frmPRESPorMat.oPresupuestos.CargarTodosLosPresupuestos frmPRESPorMat.iAnyoSeleccionado, frmPRESPorMat.sdbcGMN1_4Cod, frmPRESPorMat.sdbcGMN2_4Cod, frmPRESPorMat.sdbcGMN3_4Cod, , , Trim(txtDen), , , , , , , True, , , , , True
            End If
                            
            If (chkIgualDen.Value = vbChecked) Then
                frmPRESPorMat.ponerCaption Trim(txtDen), 2, False
            Else
                frmPRESPorMat.ponerCaption Trim(txtDen), 2, True
            End If
        
        End If
        
    End If
        
        
    frmPRESPorMat.sdbgPresupuestos.ReBind
    MDI.MostrarFormulario frmPRESPorMat, True
    frmPRESPorMat.sdbgPresupuestos.MoveFirst
    
    Screen.MousePointer = vbNormal
    
    Unload Me

    frmPRESPorMat.SetFocus
    
End Sub
Private Sub cmdCancelar_Click()

    ''' * Objetivo: Cerrar el formulario
    
    MDI.MostrarFormulario frmPRESPorMat, True
    
    Unload Me
    
    frmPRESPorMat.SetFocus
    
    
    
End Sub
Private Sub Form_Activate()

    ''' * Objetivo: Iniciar el formulario
    
    txtCod.SetFocus
    
End Sub
Private Sub txtCOD_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por codigo

    optCOD.Value = True
    optDEN.Value = False
    
End Sub
Private Sub txtDEN_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por denominacion

    optCOD.Value = False
    optDEN.Value = True
    
End Sub




Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRES_PORMAT_FILTRAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    Caption = Ador(0).Value
    Ador.MoveNext
    optCOD.Caption = Ador(0).Value
    Ador.MoveNext
    chkIgualCod.Caption = Ador(0).Value
    chkIgualDen.Caption = Ador(0).Value
    Ador.MoveNext
    optDEN.Caption = Ador(0).Value
    Ador.MoveNext
    cmdAceptar.Caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.Caption = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub




