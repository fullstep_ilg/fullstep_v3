VERSION 5.00
Begin VB.Form frmESTRCOMPDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle"
   ClientHeight    =   1350
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4695
   Icon            =   "frmESTRCOMPDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1350
   ScaleWidth      =   4695
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   960
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   4
      Top             =   900
      Width           =   3825
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   195
         TabIndex        =   2
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1410
         TabIndex        =   3
         Top             =   60
         Width           =   1050
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1290
      Left            =   1365
      ScaleHeight     =   1290
      ScaleWidth      =   3915
      TabIndex        =   5
      Top             =   0
      Width           =   3915
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   0
         Top             =   120
         Width           =   735
      End
      Begin VB.TextBox txtDen 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         MaxLength       =   100
         TabIndex        =   1
         Top             =   540
         Width           =   3165
      End
   End
   Begin VB.Timer timDet 
      Interval        =   60000
      Left            =   3930
      Top             =   1275
   End
   Begin VB.Label lblDen 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   60
      TabIndex        =   7
      Top             =   600
      Width           =   1380
   End
   Begin VB.Label lblCod 
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   195
      Left            =   60
      TabIndex        =   6
      Top             =   180
      Width           =   1035
   End
End
Attribute VB_Name = "frmESTRCOMPDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'HBA
Private sIdiCod As String
Private sIdiDen As String


Private Sub cmdAceptar_Click()

Dim teserror As TipoErrorSummit

Select Case frmESTRCOMP.Accion
    
    Case ACCEqpAnya
                    
            '******** Validacion de datos **********
            
            If Trim(txtCod) = "" Then
'                oMensajes.NoValido "Codigo"
                oMensajes.NoValido sIdiCod
                If Me.Visible Then txtCod.SetFocus
                Exit Sub
            End If
            
            
            If Trim(txtDen) = "" Then
'                oMensajes.NoValida "Denominacion"
                oMensajes.NoValido sIdiDen
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
            '*****************************************
            
            frmESTRCOMP.oEquipoSeleccionado.Cod = Trim(txtCod)
            frmESTRCOMP.oEquipoSeleccionado.Den = Trim(txtDen)
            
            Set frmESTRCOMP.oIBaseDatos = frmESTRCOMP.oEquipoSeleccionado
                    
            Screen.MousePointer = vbHourglass
            teserror = frmESTRCOMP.oIBaseDatos.AnyadirABaseDatos
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                frmESTRCOMP.AnyadirEquipoAEstructura Trim(txtCod), Trim(txtDen)
                RegistrarAccion ACCEqpAnya, "Cod:" & Trim(txtCod)
            Else
                TratarError teserror
                Exit Sub
            End If
    
    Case ACCEqpMod
         
        '******** Validacion de datos **********
        If Trim(txtDen) = "" Then
'            oMensajes.NoValida "Denominacion"
            oMensajes.NoValido sIdiDen
            If Me.Visible Then txtDen.SetFocus
            Exit Sub
        End If
        '*****************************************
         
        frmESTRCOMP.oEquipoSeleccionado.Den = Trim(txtDen)
         
        Screen.MousePointer = vbHourglass
        teserror = frmESTRCOMP.oIBaseDatos.FinalizarEdicionModificando
        Screen.MousePointer = vbNormal

        If teserror.NumError = TESnoerror Then
            frmESTRCOMP.ModificarEquipoEnEstructura Trim(txtDen)
            RegistrarAccion ACCEqpMod, "Cod:" & Trim(txtCod)
        Else
            TratarError teserror
            Exit Sub
        End If
    
    Case ACCEqpEli
              
        Screen.MousePointer = vbHourglass
        teserror = frmESTRCOMP.oIBaseDatos.FinalizarEdicionEliminando
        Screen.MousePointer = vbNormal
        
        If teserror.NumError = TESnoerror Then
            frmESTRCOMP.EliminarEquipoDeEstructura
            RegistrarAccion ACCEqpEli, "Cod:" & Trim(txtCod)
        Else
            TratarError teserror
            frmESTRCOMP.oIBaseDatos.CancelarEdicion
        End If
        
        
End Select
    
frmESTRCOMP.Accion = ACCCompCon

Set frmESTRCOMP.oCompradorSeleccionado = Nothing
Set frmESTRCOMP.oEquipoSeleccionado = Nothing
Set frmESTRCOMP.oIBaseDatos = Nothing

Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()

Select Case frmESTRCOMP.Accion

    Case ACCEqpEli, ACCCompEli
            
        picDatos.Enabled = False
        
    Case ACCEqpDet, ACCCompDet
        
        picDatos.Enabled = False
        picEdit.Visible = False
        Height = Height - 300
    
    Case ACCEqpMod
        txtCod.Enabled = False
    Case Else
        'txtCod.SetFocus

End Select
CargarRecursos
End Sub

Private Sub Form_Load()

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

Select Case frmESTRCOMP.Accion

Case accionessummit.ACCCompAnya
    
        txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCOM
    
Case accionessummit.ACCEqpAnya
    
        txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodeqp
    
End Select

End Sub

Private Sub Form_Unload(Cancel As Integer)
Select Case frmESTRCOMP.Accion
            
    Case ACCEqpMod, ACCCompMod, ACCCompEli, ACCEqpEli
    
            frmESTRCOMP.oIBaseDatos.CancelarEdicion
End Select

frmESTRCOMP.Accion = ACCCompCon

Set frmESTRCOMP.oCompradorSeleccionado = Nothing
Set frmESTRCOMP.oEquipoSeleccionado = Nothing
Set frmESTRCOMP.oIBaseDatos = Nothing

End Sub

Private Sub timDet_Timer()
Static ThisTime As Integer

If ThisTime = giMinutosDeRetardoEnVentanasDeEdicion - 1 Then
    ThisTime = 1
    Unload Me
Else
    ThisTime = ThisTime + 1
End If

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTR_COMPDETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
'        If frmESTRCOMPDetalle.Caption = "A�adir equipo" Then
'                frmESTRCOMPDetalle.Caption = Ador(0).Value
'        End If
'        Ador.MoveNext
'        If frmESTRCOMPDetalle.Caption = "Modificar equipo" Then
'                frmESTRCOMPDetalle.Caption = Ador(0).Value
'        End If
'        Ador.MoveNext
'        If frmESTRCOMPDetalle.Caption = "Detalle" Then
'                frmESTRCOMPDetalle.Caption = Ador(0).Value
'        End If
'        Ador.MoveNext
        
        lblCod.caption = Ador(0).Value
        Ador.MoveNext
        
        lblDen.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        
        sIdiCod = Ador(0).Value
        Ador.MoveNext
        sIdiDen = Ador(0).Value
    
    End If
    
    Set Ador = Nothing
    
    
        
End Sub



