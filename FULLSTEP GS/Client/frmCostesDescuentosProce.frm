VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCostesDescuentosProce 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   5280
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9090
   Icon            =   "frmCostesDescuentosProce.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   5280
   ScaleWidth      =   9090
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "dCancelar"
      Height          =   375
      Left            =   4800
      TabIndex        =   2
      Top             =   4800
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "dAceptar"
      Height          =   375
      Left            =   3120
      TabIndex        =   1
      Top             =   4800
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCostesDescuentosProce 
      Height          =   4170
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   8775
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   11
      stylesets.count =   4
      stylesets(0).Name=   "ConDescr"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCostesDescuentosProce.frx":0562
      stylesets(1).Name=   "Gris"
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCostesDescuentosProce.frx":0BE4
      stylesets(2).Name=   "Normal"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   16777215
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmCostesDescuentosProce.frx":0C00
      stylesets(3).Name=   "Selected"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   8388608
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmCostesDescuentosProce.frx":0C1C
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   -2147483630
      ForeColorOdd    =   -2147483630
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      Columns.Count   =   11
      Columns(0).Width=   2699
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   5450
      Columns(1).Caption=   "DENOMINACION"
      Columns(1).Name =   "DENOMINACION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Caption=   "OPERACION"
      Columns(2).Name =   "OPERACION"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Caption=   "VALOR"
      Columns(3).Name =   "VALOR"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "TIPO_DATO"
      Columns(4).Name =   "TIPO_DATO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "INTRO"
      Columns(5).Name =   "INTRO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID"
      Columns(6).Name =   "ID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "ATRIB_ID"
      Columns(7).Name =   "ATRIB_ID"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "MIN"
      Columns(8).Name =   "MIN"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "MAX"
      Columns(9).Name =   "MAX"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "DESCR"
      Columns(10).Name=   "DESCR"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      _ExtentX        =   15478
      _ExtentY        =   7355
      _StockProps     =   79
      BackColor       =   -2147483643
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCostes 
      BackColor       =   &H00808000&
      Caption         =   "dSeleccione los costes del proceso a incluir en la linea del catalogo:"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   6135
   End
End
Attribute VB_Name = "frmCostesDescuentosProce"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables privadas
Private m_oAtributos As CAtributos

'Variables publicas
Public g_oLinea         As CLineaCatalogo
Public g_sOrigen       As String

Private Enum TipoAtributo
    Coste = 0
    Descuento = 1
End Enum

Private Sub Form_Load()
    PonerFieldSeparator Me
    CargarRecursos
    Select Case g_sOrigen
    Case "frmCatalogoCostesDescuentos_Costes"
        'Cargamos la grid de costes
        CargarCostesProce
    Case "frmCatalogoCostesDescuentos_Descuentos"
        'Cargamos la grid de costes
        CargarDescuentosProce
    End Select
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdAceptar_Click()
    Dim inum As Integer
    Dim inum2 As Integer
    Dim vbm As Variant
    Dim valorText As Variant
    Dim valorNum As Variant
    Dim valorFec As Variant
    Dim valorBool As Variant
    
    Select Case g_sOrigen
        Case "frmCatalogoCostesDescuentos_Costes"
            If sdbgCostesDescuentosProce.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgCostesDescuentosProce.SelBookmarks.Count
                    With frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo
                        .AddNew
                        sdbgCostesDescuentosProce.Bookmark = sdbgCostesDescuentosProce.SelBookmarks(inum)
                        'Comprobar que no existe ya
                        If .Rows > 0 Then
                            For inum2 = 0 To .Rows - 1
                                vbm = .AddItemBookmark(inum2)
                                If CStr(sdbgCostesDescuentosProce.Columns("ATRIB_ID").Value) = .Columns("ATRIB_ID").CellValue(vbm) Then
                                    .CancelUpdate
                                    GoTo SiguienteAtribProceso_coste
                                End If
                            Next inum2
                        End If
                        '1. A�ADIR AL GRID DE CAMPOS DE PEDIDO DE FRMCATALOGO
                        .Columns("COD").Text = sdbgCostesDescuentosProce.Columns("COD").Value
                        .Columns("NOMBRE").Value = sdbgCostesDescuentosProce.Columns("DENOMINACION").Value
                        .Columns("DESCR").Value = sdbgCostesDescuentosProce.Columns("DESCR").Value
                        .Columns("OPERACION").Text = sdbgCostesDescuentosProce.Columns("OPERACION").Value
                        .Columns("VALOR").Text = sdbgCostesDescuentosProce.Columns("VALOR").Value
                        .Columns("TIPO").Text = ""
                        .Columns("TIPO_HIDDEN").Text = TipoCostesDescuentosLineaCatalogo.Fijos
                        .Columns("GRUPO").Text = ""
                        .Columns("MIN").Value = sdbgCostesDescuentosProce.Columns("MIN").Value
                        .Columns("MAX").Value = sdbgCostesDescuentosProce.Columns("MAX").Value
                        .Columns("INTRO").Value = sdbgCostesDescuentosProce.Columns("INTRO").Value
                        .Columns("ATRIB_ID").Value = sdbgCostesDescuentosProce.Columns("ATRIB_ID").Value
                        valorText = Null
                        valorNum = Null
                        valorFec = Null
                        valorBool = Null
                        Select Case sdbgCostesDescuentosProce.Columns("TIPO_DATO").Value
                            Case TiposDeAtributos.TipoString
                                valorText = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoNumerico
                                valorNum = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoFecha
                                valorFec = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoBoolean
                                valorBool = .Columns("VALOR").Text
                        End Select
                        frmCatalogoCostesDescuentos.g_oCostesA�adir.Add 0, .Columns("COD").Text, .Columns("NOMBRE").Text _
                                    , valor:=.Columns("VALOR").Text, Minimo:=.Columns("MIN").Value, Maximo:=.Columns("MAX").Value, TipoIntroduccion:=.Columns("INTRO").Value, bCoste:=True, Atrib:=.Columns("ATRIB_ID").Value, Tipo:=TipoNumerico, valor_num:=.Columns("VALOR").Text, OperacionCosteDescuento:="+", GrupoCosteDescuento:=""
                        .Update
                        '.Refresh
                    End With
SiguienteAtribProceso_coste:
                    inum = inum + 1
                Wend
           End If
           frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.MoveLast
           frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Columns("VALOR").DropDownHwnd = 0
        Case "frmCatalogoCostesDescuentos_Descuentos"
            If sdbgCostesDescuentosProce.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgCostesDescuentosProce.SelBookmarks.Count
                    With frmCatalogoCostesDescuentos.sdbgDescuentosLineaCatalogo
                        .AddNew
                        sdbgCostesDescuentosProce.Bookmark = sdbgCostesDescuentosProce.SelBookmarks(inum)
                        'Comprobar que no existe ya
                        If .Rows > 0 Then
                            For inum2 = 0 To .Rows - 1
                                vbm = .AddItemBookmark(inum2)
                                If CStr(sdbgCostesDescuentosProce.Columns("ATRIB_ID").Value) = .Columns("ATRIB_ID").CellValue(vbm) Then
                                    .CancelUpdate
                                    GoTo SiguienteAtribProceso_descuento
                                End If
                            Next inum2
                        End If
                        '1. A�ADIR AL GRID DE CAMPOS DE PEDIDO DE FRMCATALOGO
                        .Columns("COD").Text = sdbgCostesDescuentosProce.Columns("COD").Value
                        .Columns("NOMBRE").Value = sdbgCostesDescuentosProce.Columns("DENOMINACION").Value
                        .Columns("DESCR").Value = sdbgCostesDescuentosProce.Columns("DESCR").Value
                        .Columns("OPERACION").Text = sdbgCostesDescuentosProce.Columns("OPERACION").Value
                        .Columns("VALOR").Text = sdbgCostesDescuentosProce.Columns("VALOR").Value
                        .Columns("TIPO").Text = ""
                        .Columns("TIPO_HIDDEN").Text = TipoCostesDescuentosLineaCatalogo.Fijos
                        .Columns("GRUPO").Text = ""
                        .Columns("MIN").Value = sdbgCostesDescuentosProce.Columns("MIN").Value
                        .Columns("MAX").Value = sdbgCostesDescuentosProce.Columns("MAX").Value
                        .Columns("INTRO").Value = sdbgCostesDescuentosProce.Columns("INTRO").Value
                        .Columns("ATRIB_ID").Value = sdbgCostesDescuentosProce.Columns("ATRIB_ID").Value
                        valorText = Null
                        valorNum = Null
                        valorFec = Null
                        valorBool = Null
                        Select Case sdbgCostesDescuentosProce.Columns("TIPO_DATO").Value
                            Case TiposDeAtributos.TipoString
                                valorText = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoNumerico
                                valorNum = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoFecha
                                valorFec = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoBoolean
                                valorBool = .Columns("VALOR").Text
                        End Select
                        frmCatalogoCostesDescuentos.g_oDescuentosA�adir.Add 0, .Columns("COD").Text, .Columns("NOMBRE").Text _
                                    , valor:=.Columns("VALOR").Text, Minimo:=.Columns("MIN").Value, Maximo:=.Columns("MAX").Value, TipoIntroduccion:=.Columns("INTRO").Value, bCoste:=True, Atrib:=.Columns("ATRIB_ID").Value, Tipo:=TipoNumerico, valor_num:=.Columns("VALOR").Text, OperacionCosteDescuento:="+", GrupoCosteDescuento:=""
                        .Update
                        '.Refresh
                    End With
SiguienteAtribProceso_descuento:
                    inum = inum + 1
                Wend
           End If
           frmCatalogoCostesDescuentos.sdbgDescuentosLineaCatalogo.MoveLast
           frmCatalogoCostesDescuentos.sdbgDescuentosLineaCatalogo.Columns("VALOR").DropDownHwnd = 0
    End Select
    
    Unload Me
End Sub


''' <summary>
''' Carga los textos del m�dulo
''' </summary>
''' <remarks>Llamada desde Form_Load; Tiempo m�ximo</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_COSTES_DESCUENTOS_PROCE, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        Me.sdbgCostesDescuentosProce.Columns("COD").caption = Ador(0).Value '1 Codigo
        Ador.MoveNext
        Me.sdbgCostesDescuentosProce.Columns("DENOMINACION").caption = Ador(0).Value '2 Denominacion
        Ador.MoveNext
        Me.sdbgCostesDescuentosProce.Columns("OPERACION").caption = Ador(0).Value '3 operacion
        Ador.MoveNext
        Me.sdbgCostesDescuentosProce.Columns("VALOR").caption = Ador(0).Value '4 valor
        Ador.MoveNext
        Me.cmdAceptar.caption = Ador(0).Value '5 aceptar
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).Value '6 cancelar
        Ador.MoveNext
        Select Case g_sOrigen
        Case "frmCatalogoCostesDescuentos_Costes"
            Ador.MoveNext
            Me.lblCostes.caption = Ador(0).Value '7 Seleccione los costes del proceso a incluir en la linea del catalogo:
        Case "frmCatalogoCostesDescuentos_Descuentos"
            Ador.MoveNext
            Me.lblCostes.caption = Ador(0).Value '8 Seleccione los descuentos del proceso a incluir en la linea del catalogo:
        End Select
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.Close
    End If
End Sub


''' <summary>Carga el objeto de m_oCostesProce con los costes del proceso de la linea de catalogo</summary>
Private Sub CargarCostesProce()
    If g_oLinea Is Nothing Then Unload Me
    
    g_oLinea.CargarCostesProceso
    CargarGridCostesDescuentosProce (Coste)
End Sub

''' <summary>Carga el objeto de m_oDescuentosProce con los descuentos del proceso de la linea de catalogo</summary>
Private Sub CargarDescuentosProce()
    If g_oLinea Is Nothing Then Unload Me
    
    g_oLinea.CargarDescuentosProceso
    CargarGridCostesDescuentosProce (Descuento)
End Sub

''' <summary>Carga en la grid los costes o los descuentos del proceso</summary>
Private Sub CargarGridCostesDescuentosProce(Tipo As TipoAtributo)
    Dim oatrib As CAtributo
    Dim oAtributos As CAtributos
    sdbgCostesDescuentosProce.RemoveAll
    
    Select Case Tipo
        Case TipoAtributo.Coste
            Set oAtributos = g_oLinea.CostesProce
        Case TipoAtributo.Descuento
            Set oAtributos = g_oLinea.DescuentosProce
    End Select
    If Not oAtributos Is Nothing Then
        For Each oatrib In oAtributos
            sdbgCostesDescuentosProce.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.OperacionCosteDescuento & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Descripcion
        Next
    End If
    
End Sub


