VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmListados 
   Caption         =   "Listados"
   ClientHeight    =   4575
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5415
   Icon            =   "frmListados.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4575
   ScaleWidth      =   5415
   Begin VB.PictureBox picSeleccionar 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   5415
      TabIndex        =   1
      Top             =   4200
      Width           =   5415
      Begin VB.CommandButton cmdSeleccionar 
         Caption         =   "Seleccionar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4260
         TabIndex        =   2
         Top             =   0
         Width           =   1155
      End
   End
   Begin MSComctlLib.ImageList imglstListados 
      Left            =   4800
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListados.frx":0CB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListados.frx":0DC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListados.frx":0ED6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwListados 
      Height          =   4155
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   7329
      _Version        =   393217
      Indentation     =   353
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "imglstListados"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmListados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables para listados de procesos
Public ofrmLstProce1 As frmLstPROCE
Public ofrmlstProce2 As frmLstPROCE
Public ofrmLstProce3 As frmLstPROCE
Public ofrmLstProce4 As frmLstPROCE
Public ofrmlstProce5 As frmLstPROCE

'Vbles. para listados Ahorros Neg.
Public ofrmLstNegGenReu As frmLstINFAhorrosNeg
Public ofrmLstNegGenDesde As frmLstINFAhorrosNeg
Public ofrmLstNegMatReu As frmLstINFAhorrosNeg
Public ofrmLstNegMatDesde As frmLstINFAhorrosNeg
Public ofrmLstNegEqpResReu As frmLstINFAhorrosNeg
Public ofrmLstNegEqpResDesde As frmLstINFAhorrosNeg
Public ofrmLstNegEqpReu As frmLstINFAhorrosNeg
Public ofrmLstNegEqpDesde As frmLstINFAhorrosNeg
Public ofrmLstNegConcep3Desde As frmLstINFAhorrosNeg
Public ofrmLstNegConcep4Desde As frmLstINFAhorrosNeg
Public ofrmLstNegConcep3Reu As frmLstINFAhorrosNeg
Public ofrmLstNegConcep4Reu As frmLstINFAhorrosNeg

'Vbles. para listados Ahorros Aplicados
Public ofrmLstApliGen As frmLstINFAhorrosApl
Public ofrmLstApliMat As frmLstINFAhorrosApl
Public ofrmLstApliProy As frmLstINFAhorrosApl
Public ofrmLstApliParCon As frmLstINFAhorrosApl
Public ofrmLstApliUO As frmLstINFAhorrosApl
Public ofrmLstApliConcep3 As frmLstINFAhorrosApl
Public ofrmLstApliConcep4 As frmLstINFAhorrosApl

'Vbles. para listados de Parametros
Public ofrmLstLISPER As frmLstLISPER

'Varbles para listado de ofertas recibidas a traves del WEB
Public ofrmLstProce6 As frmLstPROCE

'Variable para listados de Informes de Evoluci�n de Proyectos y Partidas
Public ofrmLstInfEvolPres3 As frmLstINFEvolPres
Public ofrmLstInfEvolPres4 As frmLstINFEvolPres

'Variables para el multilenguaje
Private sIdiA As String
Private sIdiA1 As String
Private sIdiA1B1 As String
Private sIdiA1B2 As String
Private sIdiA1B3 As String
Private sIdiA1B4 As String
Private sIdiA1B4C1 As String
Private sIdiA1B4C2 As String
Private sIdiA1B5 As String
Private sIdiA1B5C1 As String
Private sIdiA1B5C2 As String
Private sIdiA1B5C3 As String
Private sIdiA1B6 As String
Private sIdiA1B6C1 As String
Private sIdiA1B6C2 As String
Private sIdiA1B6C3 As String
Private sIdiA1B7 As String
Private sIdiA1B7C1 As String
Private sIdiA1B7C3 As String
Private sIdiA1B7C4 As String
Private sIdiA1B7C5 As String
Private sIdiA1B7C6 As String
Private sIdiA1B8 As String
Private sIdiA1B8A As String
Private sIdiA1B8C1 As String
Private sIdiA1B9 As String
Private sIdiA2 As String
Private sIdiA2B1 As String
Private sIdiA2B2 As String
Private sIdiA2B3 As String
Private sIdiA2B5 As String
Private sIdiA3 As String
Private sIdiA3B1 As String
Private sIdiA4 As String
Private sIdiA4B1 As String
Private sIdiA4B1C1 As String
Private sIdiA4B1C1D1 As String
Private sIdiA4B1C1D2 As String
Private sIdiA4B1C2 As String
Private sIdiA4B1C2D1 As String
Private sIdiA4B1C2D2 As String
Private sIdiA4B1C3 As String
Private sIdiA4B1C3D1 As String
Private sIdiA4B1C3D2 As String
Private sIdiA4B1C4 As String
Private sIdiA4B1C4D1 As String
Private sIdiA4B1C4D2 As String
Private sIdiA4B2 As String
Private sIdiA4B2C1 As String
Private sIdiA4B2C2 As String
Private sIdiA4B2C5 As String
Private sIdiA5 As String
Private sIdiA5B1 As String
Private sIdiA5B1C1 As String
Private sIdiA5B1C2 As String
Private sIdiA5B1C3 As String
Private sIdiA5B1C4 As String
Private sIdiA5B1C5 As String
Private sIdiA5B1C6 As String
Private sIdiA5B1C7 As String
Private sIdiA6 As String
Private sIdiA6B1 As String
Private sIdiA6B2 As String
Private sIdiA6B3 As String
Private sIdiA6B3C1 As String
Private sIdiA2B4 As String
Private sIdiA2B4C1 As String
Private sIdiA2B4C2 As String
Private sIdiA5B1C8 As String
Private sIdiA4B3 As String
Private sIdiA4B4 As String
Private sIdiA7 As String
Private sIdiA7B1 As String
Private sIdiA7B2 As String
Private sIdiA7B3 As String
Private sIdiA7B3C1 As String
Private sIdiA7B3C2 As String
Private sIdiA1B5C4 As String
Private sIdiA8 As String
Private sIdiA8B1 As String
Private sIdiA8B2 As String
Private sIdiA8B3 As String
Private sIdiA2B6 As String
Private sIdiA1B8C6 As String
Private sIdiA1B10 As String
Sub Arrange()

    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 2020 Then   'de tama�o de la ventana
            Me.Width = 2220        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 1715 Then  'cuando no se maximiza ni
            Me.Height = 1815       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width
    

    If Me.Width > 120 Then tvwListados.Width = Me.Width - 120
    If Me.Height > 825 Then tvwListados.Height = Me.Height - 825
  
    If Me.Width > (cmdSeleccionar.Width + 120) Then cmdSeleccionar.Left = Me.Width - cmdSeleccionar.Width - 120
End Sub

Private Sub cmdSeleccionar_Click()
    Select Case tvwListados.selectedItem.key
    Case "A1B1"
        AbrirLstMON
    Case "A5B1C2" 'Formas de pago
        AbrirLstParametros "A5B1C2"
    Case "A1B9" ' V�as de pago
        AbrirLstParametros "A1B9"
    Case "A1B2"
        AbrirLstPAI
    Case "A1B3"
        AbrirLstPROVI
    
    ' ESTRUCTURA DE LA ORGANIZACION
    Case "A1B4C1"
        frmLstESTRORG.WindowState = vbNormal
        frmLstESTRORG.Show 1
    Case "A1B4C2"
        frmLstDEST.WindowState = vbNormal
        frmLstDEST.Show 1
    
    ' ESTRUCTURA DE MATERIALES
    Case "A1B5C1"
        frmLstMAT.WindowState = vbNormal
        frmLstMAT.Show 1
    Case "A1B5C2"
        frmLstART.WindowState = vbNormal
        frmLstART.Show 1
    Case "A1B5C3"
        AbrirLstUNI
    Case "A1B5C4"
        frmLstAtributos.WindowState = vbNormal
        frmLstAtributos.Show 1
        
    ' ESTRUCTURA DE COMPRAS
    Case "A1B6C1"
        AbrirLstESTRCOMP
    Case "A1B6C2"
        frmLstESTRCOMPMatPorCom.WindowState = vbNormal
        frmLstESTRCOMPMatPorCom.Show 1
    Case "A1B6C3"
        frmLstESTRCOMPComPorMat.WindowState = vbNormal
        frmLstESTRCOMPComPorMat.Show 1
    
    ' PROVEEDORES
    Case "A1B7C1"
        frmLstPROVE.WindowState = vbNormal
        frmLstPROVE.Show 1
    'Case "A1B7C2"
        'frmLstPROVEPortal.WindowState = vbNormal
        'frmLstPROVEPortal.Show
    Case "A1B7C3"
        frmLstPROVEMatPorProve.WindowState = vbNormal
        frmLstPROVEMatPorProve.Show 1
    Case "A1B7C4"
        frmLstPROVEProvPorMat.WindowState = vbNormal
        frmLstPROVEProvPorMat.Show 1
    Case "A1B7C5"
        frmLstPROVEEqpPorProve.WindowState = vbNormal
        frmLstPROVEEqpPorProve.Show 1
    Case "A1B7C6"
        frmLstPROVEProvePorEqp.WindowState = vbNormal
        frmLstPROVEProvePorEqp.Show 1
        
    ' PRESUPUESTOS
    Case "A1B8C1"
        frmLstPRESPorMat.WindowState = vbNormal
        frmLstPRESPorMat.Show 1
    Case "A1B8C2D1"
        frmLstPRESPorProy.WindowState = vbNormal
        frmLstPRESPorProy.Show 1
    Case "A1B8C2D2"
        frmLstPRESPorParCon.WindowState = vbNormal
        frmLstPRESPorParCon.Show 1
    Case "A1B8C4"
        frmLstPRESPorCon3.WindowState = vbNormal
        frmLstPRESPorCon3.Show 1
    Case "A1B8C5"
        frmLstPRESPorCon4.WindowState = vbNormal
        frmLstPRESPorCon4.Show 1
    Case "A1B8C6"
        frmLstPRESPorCon5.WindowState = vbNormal
        frmLstPRESPorCon5.Show 1


    ' PROCESOS
    Case "A2B1" 'Apertura de procesos
        Set ofrmLstProce1 = New frmLstPROCE
        ofrmLstProce1.sOrigen = "A2B1"
        ofrmLstProce1.g_sOrigen = "frmListados"
        ofrmLstProce1.WindowState = vbNormal
        ofrmLstProce1.Show 1
        
    Case "A2B2"
        Set ofrmlstProce2 = New frmLstPROCE
        ofrmlstProce2.sOrigen = "A2B2"
        ofrmlstProce2.WindowState = vbNormal
        ofrmlstProce2.Show 1
    Case "A2B3"
        Set ofrmLstProce3 = New frmLstPROCE
        ofrmLstProce3.sOrigen = "A2B3"
        ofrmLstProce3.WindowState = vbNormal
        ofrmLstProce3.Show 1
     
    ' Recepci�n de ofertas
    Case "A2B4C1"
        frmLstOFEBuzon.sOrigen = ""
        frmLstOFEBuzon.WindowState = vbNormal
        frmLstOFEBuzon.Show 1
    Case "A2B4C2" 'Recepci�n de ofertas
        Set ofrmLstProce4 = New frmLstPROCE
        ofrmLstProce4.sOrigen = "A2B4C2"
        ofrmLstProce4.g_sOrigen = "frmListados"
        ofrmLstProce4.WindowState = vbNormal
        ofrmLstProce4.Show 1


    Case "A2B5"
        Set ofrmlstProce5 = New frmLstPROCE
        ofrmlstProce5.sOrigen = "A2B5"
        ofrmlstProce5.WindowState = vbNormal
        ofrmlstProce5.Show 1
    
    'REUNIONES
    Case "A3B1"
        frmLstREU.WindowState = vbNormal
        frmLstREU.Show 1
    
    ' INFORMES AHORROS NEGOCIADOS
    Case "A4B1C1D1" 'General Reuni�n
        Set ofrmLstNegGenReu = New frmLstINFAhorrosNeg
        ofrmLstNegGenReu.sOrigen = "A4B1C1D1"
        ofrmLstNegGenReu.WindowState = vbNormal
        ofrmLstNegGenReu.Show 1
    Case "A4B1C1D2" 'General desde/hasta fecha
        Set ofrmLstNegGenDesde = New frmLstINFAhorrosNeg
        ofrmLstNegGenDesde.sOrigen = "A4B1C1D2"
        ofrmLstNegGenDesde.WindowState = vbNormal
        ofrmLstNegGenDesde.Show 1
    Case "A4B1C2D1" 'Por Material Reuni�n
        Set ofrmLstNegMatReu = New frmLstINFAhorrosNeg
        ofrmLstNegMatReu.sOrigen = "A4B1C2D1"
        ofrmLstNegMatReu.WindowState = vbNormal
        ofrmLstNegMatReu.Show 1
    Case "A4B1C2D2" 'Por Material desde/hasta fecha
        Set ofrmLstNegMatDesde = New frmLstINFAhorrosNeg
        ofrmLstNegMatDesde.sOrigen = "A4B1C2D2"
        ofrmLstNegMatDesde.WindowState = vbNormal
        ofrmLstNegMatDesde.Show 1
    Case "A4B1C3D1" 'Por Eqp.Responsables Reuni�n
        Set ofrmLstNegEqpResReu = New frmLstINFAhorrosNeg
        ofrmLstNegEqpResReu.sOrigen = "A4B1C3D1"
        ofrmLstNegEqpResReu.WindowState = vbNormal
        ofrmLstNegEqpResReu.Show 1
    Case "A4B1C3D2" 'Por Eqp.Responsables desde/hasta fecha
        Set ofrmLstNegEqpResDesde = New frmLstINFAhorrosNeg
        ofrmLstNegEqpResDesde.sOrigen = "A4B1C3D2"
        ofrmLstNegEqpResDesde.WindowState = vbNormal
        ofrmLstNegEqpResDesde.Show 1
    Case "A4B1C4D1" 'Por Eqp.Negociadores Reuni�n
        Set ofrmLstNegEqpReu = New frmLstINFAhorrosNeg
        ofrmLstNegEqpReu.sOrigen = "A4B1C4D1"
        ofrmLstNegEqpReu.WindowState = vbNormal
        ofrmLstNegEqpReu.Show 1
    Case "A4B1C4D2" 'Por Eqp.Negociadores desde/hasta fecha
        Set ofrmLstNegEqpDesde = New frmLstINFAhorrosNeg
        ofrmLstNegEqpDesde.sOrigen = "A4B1C4D2"
        ofrmLstNegEqpDesde.WindowState = vbNormal
        ofrmLstNegEqpDesde.Show 1
    Case "A4B1C5D1"  'Presupuesto 3 Reuni�n
        Set ofrmLstNegConcep3Reu = New frmLstINFAhorrosNeg
        ofrmLstNegConcep3Reu.sOrigen = "A4B1C5D1"
        ofrmLstNegConcep3Reu.WindowState = vbNormal
        ofrmLstNegConcep3Reu.Show 1
    Case "A4B1C5D2"  'Presupuesto 3 Desde/hasta
        Set ofrmLstNegConcep3Desde = New frmLstINFAhorrosNeg
        ofrmLstNegConcep3Desde.sOrigen = "A4B1C5D2"
        ofrmLstNegConcep3Desde.WindowState = vbNormal
        ofrmLstNegConcep3Desde.Show 1
    Case "A4B1C6D1"  'Presupuesto 4 Reuni�n
        Set ofrmLstNegConcep4Reu = New frmLstINFAhorrosNeg
        ofrmLstNegConcep4Reu.sOrigen = "A4B1C6D1"
        ofrmLstNegConcep4Reu.WindowState = vbNormal
        ofrmLstNegConcep4Reu.Show 1
    Case "A4B1C6D2"  'Presupuesto 4 Desde/hasta
        Set ofrmLstNegConcep4Desde = New frmLstINFAhorrosNeg
        ofrmLstNegConcep4Desde.sOrigen = "A4B1C6D2"
        ofrmLstNegConcep4Desde.WindowState = vbNormal
        ofrmLstNegConcep4Desde.Show 1
        
    ' INFORMES AHORROS APLICADOS
    Case "A4B2C1" 'General
        Set ofrmLstApliGen = New frmLstINFAhorrosApl
        ofrmLstApliGen.sOrigen = "A4B2C1"
        ofrmLstApliGen.WindowState = vbNormal
        ofrmLstApliGen.Show 1
    Case "A4B2C2" 'Material
        Set ofrmLstApliMat = New frmLstINFAhorrosApl
        ofrmLstApliMat.sOrigen = "A4B2C2"
        ofrmLstApliMat.WindowState = vbNormal
        ofrmLstApliMat.Show 1
    Case "A4B2C3" 'Por Proyectos
        Set ofrmLstApliProy = New frmLstINFAhorrosApl
        ofrmLstApliProy.sOrigen = "A4B2C3"
        ofrmLstApliProy.WindowState = vbNormal
        ofrmLstApliProy.Show 1
    Case "A4B2C4" 'Por Partidas contables
        Set ofrmLstApliParCon = New frmLstINFAhorrosApl
        ofrmLstApliParCon.sOrigen = "A4B2C4"
        ofrmLstApliParCon.WindowState = vbNormal
        ofrmLstApliParCon.Show 1
    Case "A4B2C5" 'Por Unidades organizativas
        Set ofrmLstApliUO = New frmLstINFAhorrosApl
        ofrmLstApliUO.sOrigen = "A4B2C5"
        ofrmLstApliUO.WindowState = vbNormal
        ofrmLstApliUO.Show 1
    Case "A4B2C6" 'Por presupuesto 3
        Set ofrmLstApliConcep3 = New frmLstINFAhorrosApl
        ofrmLstApliConcep3.sOrigen = "A4B2C6"
        ofrmLstApliConcep3.WindowState = vbNormal
        ofrmLstApliConcep3.Show 1
    Case "A4B2C7" 'Por presupuesto 4
        Set ofrmLstApliConcep4 = New frmLstINFAhorrosApl
        ofrmLstApliConcep4.sOrigen = "A4B2C7"
        ofrmLstApliConcep4.WindowState = vbNormal
        ofrmLstApliConcep4.Show 1
        
    ' INFORMES DE EVOLUCION DE PROY1
    Case "A4B3"
        Set ofrmLstInfEvolPres3 = New frmLstINFEvolPres
        ofrmLstInfEvolPres3.iConcepto = 1
        ofrmLstInfEvolPres3.sOrigen = "Listados"
        ofrmLstInfEvolPres3.WindowState = vbNormal
        ofrmLstInfEvolPres3.Show 1
    
    ' INFORMES DE EVOLUCION DE PROY2
    Case "A4B4"
        Set ofrmLstInfEvolPres4 = New frmLstINFEvolPres
        ofrmLstInfEvolPres4.iConcepto = 2
        ofrmLstInfEvolPres4.sOrigen = "Listados"
        ofrmLstInfEvolPres4.WindowState = vbNormal
        ofrmLstInfEvolPres4.Show 1
    
    ' PARAMETROS
    Case "A5B1C3" 'Roles
        AbrirLstParametros "A5B1C3"
    Case "A5B1C4" 'Calidad de proveedores
        AbrirLstParametros "A5B1C4"
    Case "A5B1C5" 'Estado ofertas
        AbrirLstParametros "A5B1C5"
    Case "A5B1C6" 'Firmas
        AbrirLstParametros "A5B1C6"
    Case "A5B1C7" 'Asistentes
        AbrirLstParametros "A5B1C7"
    Case "A5B1C8" 'Listados personalizados
        Set ofrmLstLISPER = New frmLstLISPER
        ofrmLstLISPER.WindowState = vbNormal
        ofrmLstLISPER.Show 1
    'SEGURIDAD
    Case "A6B1"  'Usuarios
        frmLstUSUARIOS.WindowState = vbNormal
        frmLstUSUARIOS.Show 1
    Case "A6B2"  'Perfiles
        AbrirLstPERFIL
    Case "A6B3C1"  'Ofertas recibidas web
        Set ofrmLstProce6 = New frmLstPROCE
        ofrmLstProce6.sOrigen = "A6B3C1"
        ofrmLstProce6.WindowState = vbNormal
        ofrmLstProce6.Show 1
        
    ' PEDIDOS
    Case "A7B3C1"  'Cat�logo, Configuraci�n
        frmLstCatalogo.WindowState = vbNormal
        frmLstCatalogo.Show 1
    Case "A7B1"  'seguimiento
        frmLstPedidos.WindowState = vbNormal
        frmLstPedidos.Show 1
    Case "A7B2"  'recepciones
        frmLstRecepciones.WindowState = vbNormal
        frmLstRecepciones.Show 1
    Case "A7B3C2"  'Cat�logo, Seguridad
        frmLstCATSeguridad.WindowState = vbNormal
        frmLstCATSeguridad.Show 1
        
    'Solicitudes
    Case "A8B1" 'Solicitudes, configuraci�n
        frmLstSolConfiguracion.WindowState = vbNormal
        frmLstSolConfiguracion.Show 1
    Case "A8B2" 'Solicitudes, Formularios
        frmLstFormularios.g_sOrigen = "A8B2"
        frmLstFormularios.WindowState = vbNormal
        frmLstFormularios.Show 1
    Case "A8B3" 'Solicitudes, Workflows
        AbrirLstWORKFLOW
    Case "A2B6"  'Solicitudes de compras
        frmLstSolicitud.g_sOrigen = "A2B6"
        frmLstSolicitud.WindowState = vbNormal
        frmLstSolicitud.Show 1
        
    'Tipos de relaciones
    Case "A1B10" 'tipos de relaci�n
        'Set ofrmLstPARTipoRelac = New frmPARTipoRelac
        'ofrmLstPARTipoRelac.g_sOrigen = "A1B10"
        'ofrmLstPARTipoRelac.WindowState = vbNormal
        'ofrmLstPARTipoRelac.Show 1
    End Select
    
End Sub

Private Sub Form_Load()
         
        
    Me.Height = 5145
    Me.Width = 5600
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
        
    If FSEPConf Then
        GenerarArbolDeListadosFSEP
    Else
        GenerarArbolDeListados
    End If
    
    
        
End Sub
Private Sub Form_Resize()
    Arrange
End Sub



Private Sub Form_Unload(Cancel As Integer)
    Me.Visible = False
End Sub

Private Sub tvwListados_NodeClick(ByVal node As MSComctlLib.node)
    
    Dim nod As MSComctlLib.node

    For Each nod In tvwListados.Nodes
        If nod.Image = 3 Then
            nod.Bold = False
            cmdSeleccionar.Enabled = False
        End If
    Next
    If node.Image = 3 Then
        node.Bold = True
        cmdSeleccionar.Enabled = True
    End If
    
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LISTADOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiA = Ador(0).Value
        Ador.MoveNext
        sIdiA1 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B1 = Ador(0).Value '5
        Ador.MoveNext
        sIdiA1B2 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B3 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B4 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B4C1 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B4C2 = Ador(0).Value '10
        Ador.MoveNext
        sIdiA1B5 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B5C1 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B5C2 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B5C3 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B6 = Ador(0).Value '15
        Ador.MoveNext
        sIdiA1B6C1 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B6C2 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B6C3 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B7 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B7C1 = Ador(0).Value '20
        Ador.MoveNext
        sIdiA1B7C3 = Ador(0).Value '21
        Ador.MoveNext
        sIdiA1B7C4 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B7C5 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B7C6 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B8 = Ador(0).Value '25
        Ador.MoveNext
        sIdiA1B8C1 = Ador(0).Value
        Ador.MoveNext
        sIdiA2 = Ador(0).Value
        Ador.MoveNext
        sIdiA2B1 = Ador(0).Value
        Ador.MoveNext
        sIdiA2B2 = Ador(0).Value '30
        Ador.MoveNext
        sIdiA2B3 = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sIdiA2B5 = Ador(0).Value
        Ador.MoveNext
        sIdiA3 = Ador(0).Value
        Ador.MoveNext
        sIdiA3B1 = Ador(0).Value '35
        Ador.MoveNext
        sIdiA4 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B1 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B1C1 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B1C2 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B1C3 = Ador(0).Value '40
        Ador.MoveNext
        sIdiA4B1C4 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B1C1D1 = Ador(0).Value
        sIdiA4B1C2D1 = Ador(0).Value
        sIdiA4B1C3D1 = Ador(0).Value
        sIdiA4B1C4D1 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B1C1D2 = Ador(0).Value
        sIdiA4B1C2D2 = Ador(0).Value
        sIdiA4B1C3D2 = Ador(0).Value
        sIdiA4B1C4D2 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B2 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B2C1 = Ador(0).Value '45
        Ador.MoveNext
        sIdiA4B2C2 = Ador(0).Value
        Ador.MoveNext
        sIdiA4B2C5 = Ador(0).Value
        Ador.MoveNext
        sIdiA5 = Ador(0).Value
        Ador.MoveNext
        sIdiA5B1 = Ador(0).Value
        Ador.MoveNext
        sIdiA5B1C1 = Ador(0).Value '50
        Ador.MoveNext
        sIdiA5B1C2 = Ador(0).Value
        Ador.MoveNext
        sIdiA5B1C3 = Ador(0).Value
        Ador.MoveNext
        sIdiA5B1C4 = Ador(0).Value
        Ador.MoveNext
        sIdiA5B1C5 = Ador(0).Value
        Ador.MoveNext
        sIdiA5B1C6 = Ador(0).Value '55
        Ador.MoveNext
        sIdiA5B1C7 = Ador(0).Value
        Ador.MoveNext
        sIdiA6 = Ador(0).Value
        Ador.MoveNext
        sIdiA6B1 = Ador(0).Value
        Ador.MoveNext
        sIdiA6B2 = Ador(0).Value
        Ador.MoveNext
        sIdiA6B3 = Ador(0).Value '60
        Ador.MoveNext
        sIdiA6B3C1 = Ador(0).Value
        Ador.MoveNext
        'Recepci�n de ofertas
        sIdiA2B4 = Ador(0).Value
        Ador.MoveNext
        'Buz�n de ofertas
        sIdiA2B4C1 = Ador(0).Value
        Ador.MoveNext
        'Ofertas por proceso
        sIdiA2B4C2 = Ador(0).Value
        Ador.MoveNext
        
        sIdiA5B1C8 = Ador(0).Value
        Ador.MoveNext
        
        'Informe de evoluci�n de PROY1
        If InStr(1, Ador(0).Value, "PROY1") = 1 Then
            sIdiA4B3 = Replace(Ador(0).Value, "PROY1", gParametrosGenerales.gsPlurPres1)
        Else
            sIdiA4B3 = Replace(Ador(0).Value, "PROY1", LCase(gParametrosGenerales.gsPlurPres1))
        End If
        Ador.MoveNext
        'Informe de evoluci�n de PROY2
        If InStr(1, Ador(0).Value, "PROY2") = 1 Then
            sIdiA4B4 = Replace(Ador(0).Value, "PROY2", gParametrosGenerales.gsPlurPres2)
        Else
            sIdiA4B4 = Replace(Ador(0).Value, "PROY2", LCase(gParametrosGenerales.gsPlurPres2))
        End If
        Ador.MoveNext
        sIdiA1B8A = Ador(0).Value
        Ador.MoveNext
        sIdiA7 = Ador(0).Value
        Ador.MoveNext
        sIdiA7B1 = Ador(0).Value
        Ador.MoveNext
        sIdiA7B2 = Ador(0).Value
        Ador.MoveNext
        sIdiA7B3 = Ador(0).Value
        Ador.MoveNext
        sIdiA7B3C1 = Ador(0).Value
        Ador.MoveNext
        sIdiA7B3C2 = Ador(0).Value
        
        Ador.MoveNext
        sIdiA1B5C4 = Ador(0).Value
        Ador.MoveNext
        
        sIdiA8 = Ador(0).Value
        Ador.MoveNext
        sIdiA8B3 = Ador(0).Value
        Ador.MoveNext
        sIdiA8B1 = Ador(0).Value
        Ador.MoveNext
        sIdiA8B2 = Ador(0).Value
        Ador.MoveNext
        sIdiA2B6 = Ador(0).Value
        Ador.MoveNext
        sIdiA1B8C6 = Ador(0).Value '81  "dPartidas de control presupuestario"
        Ador.MoveNext
        sIdiA1B9 = Ador(0).Value '82 "V�as de Pago"
        Ador.MoveNext
        sIdiA1B10 = Ador(0).Value '83 "Tipos de relaci�n"
        Ador.Close
    
    End If


   Set Ador = Nothing



End Sub

''' <summary>
''' Generar Arbol De Listados
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub GenerarArbolDeListados()
Dim bTemp1 As Boolean
Dim bTemp2 As Boolean
Dim bTemp3 As Boolean
Dim bTemp4 As Boolean
Dim bTemp5 As Boolean
Dim bTemp6 As Boolean
Dim bTemp7 As Boolean
Dim bTemp8 As Boolean
Dim bTemp9 As Boolean
Dim bTemp10 As Boolean
Dim bTemp11 As Boolean
Dim bTemp12 As Boolean
Dim bTemp13 As Boolean
Dim bTemp14 As Boolean
Dim bTemp15 As Boolean
Dim bTemp16 As Boolean
Dim bTemp17 As Boolean
Dim bTemp18 As Boolean
Dim bTemp19 As Boolean
Dim btemp20 As Boolean
Dim btemp21 As Boolean
Dim btemp22 As Boolean
Dim bTemp23 As Boolean
Dim bTemp24 As Boolean
Dim bTemp25 As Boolean
Dim bTemp26 As Boolean
Dim bTemp27 As Boolean
Dim bTemp28 As Boolean
Dim bTemp29 As Boolean
Dim bTemp30 As Boolean

''' Generar treeview teniendo en cuenta la seguridad
    
    tvwListados.Indentation = 200
    tvwListados.ImageList = imglstListados
    tvwListados.Nodes.Add , tvwFirst, "A", sIdiA
    tvwListados.Nodes("A").Image = 0
    tvwListados.Nodes("A").ExpandedImage = 2
    tvwListados.Nodes("A").Expanded = True

    'MANTENIMIENTO
    bTemp1 = True: bTemp2 = True: bTemp3 = True: bTemp4 = True: bTemp5 = True
    bTemp6 = True: bTemp7 = True: bTemp8 = True: bTemp9 = True: bTemp10 = True
    bTemp11 = True: bTemp12 = True: bTemp13 = True: bTemp14 = True: bTemp15 = True
    bTemp16 = True: bTemp17 = True: bTemp18 = True: bTemp19 = True: btemp20 = True
    btemp21 = True: btemp22 = True: bTemp27 = True: bTemp28 = True: bTemp29 = True
    bTemp30 = True
    
    If Not gParametrosGenerales.gbUsarPres1 Then
            bTemp18 = False
    End If
    If Not gParametrosGenerales.gbUsarPres2 Then
            bTemp19 = False
    End If
    If Not gParametrosGenerales.gbUsarPres3 Then
            btemp21 = False
    End If
    If Not gParametrosGenerales.gbUsarPres4 Then
            btemp22 = False
    End If
    
    If Not gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
        bTemp28 = False
    End If

    
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        
    Else
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MONConsultar)) Is Nothing) Then bTemp1 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PAIConsultar)) Is Nothing) Then bTemp2 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVIConsultar)) Is Nothing) Then bTemp3 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGConsultar)) Is Nothing) Then bTemp4 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGConsultarDest)) Is Nothing) Then bTemp5 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATConsultar)) Is Nothing) Then bTemp6 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.UNIConsultar)) Is Nothing) Then bTemp7 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPConsultar)) Is Nothing) Then bTemp8 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorCOMPConsultar)) Is Nothing) Then bTemp9 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPPorGRUPConsultar)) Is Nothing) Then bTemp10 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEConsultar)) Is Nothing) Then bTemp11 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.EQPPorPROVEConsultar)) Is Nothing) Then bTemp12 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorEQPConsultar)) Is Nothing) Then bTemp13 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVEConsultar)) Is Nothing) Then bTemp14 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorGRUPConsultar)) Is Nothing) Then bTemp15 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPortalConsultar)) Is Nothing) Then bTemp16 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorMATConsultar)) Is Nothing) Then bTemp17 = False
        If bTemp18 Then
           If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorPROYConsultar)) Is Nothing) Then bTemp18 = False
        End If
        If bTemp19 Then
           If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorParConConsultar)) Is Nothing) Then bTemp19 = False
        End If
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PAGConsultar)) Is Nothing) Then btemp20 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.VIAPAGConsultar)) Is Nothing) Then bTemp29 = False
        If btemp21 Then
           If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESConcepto3Consultar)) Is Nothing) Then btemp21 = False
        End If
        If btemp22 Then
           If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESConcepto4Consultar)) Is Nothing) Then btemp22 = False
        End If
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBConsultar)) Is Nothing) Then bTemp27 = False
        If bTemp28 Then
            If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESConcepto5CrearArboles)) Is Nothing Then bTemp28 = False
        End If
        
    End If
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp6 And Not bTemp7 And Not bTemp8 And Not bTemp9 And Not bTemp10 And Not bTemp11 And Not bTemp12 And Not bTemp13 And Not bTemp14 And Not bTemp15 And Not bTemp16 And Not bTemp17 And Not bTemp18 And Not bTemp19 And Not btemp21 And Not btemp22 And Not bTemp23 And Not bTemp27 And Not bTemp30 Then
    
    Else
        ' ********************** Mantenimiento ***********************
        tvwListados.Nodes.Add "A", tvwChild, "A1", sIdiA1
        tvwListados.Nodes("A1").Image = 1
        tvwListados.Nodes("A1").ExpandedImage = 2

    End If
    
        ''' Monedas

    If bTemp1 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B1", sIdiA1B1
        tvwListados.Nodes("A1B1").Image = 3
    End If

    ''' Pa�ses

    If bTemp2 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B2", sIdiA1B2
        tvwListados.Nodes("A1B2").Image = 3
    End If

    ''' Provincias

    If bTemp3 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B3", sIdiA1B3
        tvwListados.Nodes("A1B3").Image = 3
    End If

    ' Estructura de la organizaci�n

    If bTemp4 Or bTemp5 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B4", sIdiA1B4
        tvwListados.Nodes("A1B4").Image = 1
        tvwListados.Nodes("A1B4").ExpandedImage = 2

        ''' Organigrama
        If bTemp4 Then
            tvwListados.Nodes.Add "A1B4", tvwChild, "A1B4C1", sIdiA1B4C1
            tvwListados.Nodes("A1B4C1").Image = 3
        End If
        ''' Destinos
        If bTemp5 Then
            tvwListados.Nodes.Add "A1B4", tvwChild, "A1B4C2", sIdiA1B4C2
            tvwListados.Nodes("A1B4C2").Image = 3
        End If
    End If

    ' Estructura de Materiales
    If bTemp6 Or bTemp7 Or bTemp27 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B5", sIdiA1B5
        tvwListados.Nodes("A1B5").Image = 1
        tvwListados.Nodes("A1B5").ExpandedImage = 2

        ''' Materiales y articulos
        If bTemp6 Then
            tvwListados.Nodes.Add "A1B5", tvwChild, "A1B5C1", sIdiA1B5C1
            tvwListados.Nodes("A1B5C1").Image = 3
            tvwListados.Nodes.Add "A1B5", tvwChild, "A1B5C2", sIdiA1B5C2
            tvwListados.Nodes("A1B5C2").Image = 3
        End If

        ''' Unidades
        If bTemp7 Then
            tvwListados.Nodes.Add "A1B5", tvwChild, "A1B5C3", sIdiA1B5C3
            tvwListados.Nodes("A1B5C3").Image = 3
        End If
        
        'Atributos
        If bTemp27 Then
            tvwListados.Nodes.Add "A1B5", tvwChild, "A1B5C4", sIdiA1B5C4
            tvwListados.Nodes("A1B5C4").Image = 3
        End If
    End If

    'Estructura de Compras
    If bTemp8 Or bTemp9 Or bTemp10 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B6", sIdiA1B6
        tvwListados.Nodes("A1B6").Image = 1
        tvwListados.Nodes("A1B6").ExpandedImage = 2

        ''' Compradores
        If bTemp8 Then
            tvwListados.Nodes.Add "A1B6", tvwChild, "A1B6C1", sIdiA1B6C1
            tvwListados.Nodes("A1B6C1").Image = 3
        End If
        ''' Material por comprador
        If bTemp9 Then
            tvwListados.Nodes.Add "A1B6", tvwChild, "A1B6C2", sIdiA1B6C2
            tvwListados.Nodes("A1B6C2").Image = 3
        End If
        ''' Compradores por material
        If bTemp10 Then
            tvwListados.Nodes.Add "A1B6", tvwChild, "A1B6C3", sIdiA1B6C3
            tvwListados.Nodes("A1B6C3").Image = 3
        End If
    End If

    'Proveedores
    If bTemp11 Or bTemp12 Or bTemp13 Or bTemp14 Or bTemp15 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B7", sIdiA1B7
        tvwListados.Nodes("A1B7").Image = 1
        tvwListados.Nodes("A1B7").ExpandedImage = 2

        ' Datos B�sicos
        If bTemp11 Then
            tvwListados.Nodes.Add "A1B7", tvwChild, "A1B7C1", sIdiA1B7C1
            tvwListados.Nodes("A1B7C1").Image = 3
        End If
        ' Material por Proveedor
        If bTemp14 Then
            tvwListados.Nodes.Add "A1B7", tvwChild, "A1B7C3", sIdiA1B7C3
            tvwListados.Nodes("A1B7C3").Image = 3
        End If
        ' Proveedores por material
        If bTemp15 Then
            tvwListados.Nodes.Add "A1B7", tvwChild, "A1B7C4", sIdiA1B7C4
            tvwListados.Nodes("A1B7C4").Image = 3
        End If
        
        If basParametros.gParametrosGenerales.gbOblProveEqp = True Then
            ' Equipos por Proveedor
            If bTemp12 Then
                tvwListados.Nodes.Add "A1B7", tvwChild, "A1B7C5", sIdiA1B7C5
                tvwListados.Nodes("A1B7C5").Image = 3
            End If
            ' Proveedores por equipo
            If bTemp13 Then
                tvwListados.Nodes.Add "A1B7", tvwChild, "A1B7C6", sIdiA1B7C6
                tvwListados.Nodes("A1B7C6").Image = 3
            End If
        End If
    End If

        'Presupuestos
    If bTemp17 Or bTemp18 Or bTemp19 Or btemp21 Or btemp22 Or bTemp28 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B8", sIdiA1B8
        tvwListados.Nodes("A1B8").Image = 1
        tvwListados.Nodes("A1B8").ExpandedImage = 2

        ''' Por material
        If bTemp17 Then
            tvwListados.Nodes.Add "A1B8", tvwChild, "A1B8C1", sIdiA1B8C1
            tvwListados.Nodes("A1B8C1").Image = 3
        End If
        
        If bTemp18 Or bTemp19 Then
'
           tvwListados.Nodes.Add "A1B8", tvwChild, "A1B8C2", sIdiA1B8A
           tvwListados.Nodes("A1B8C2").Image = 1
           tvwListados.Nodes("A1B8C2").ExpandedImage = 2
                
'          "Por Proyecto"
           If bTemp18 Then
                tvwListados.Nodes.Add "A1B8C2", tvwChild, "A1B8C2D1", gParametrosGenerales.gsSingPres1
                tvwListados.Nodes("A1B8C2D1").Image = 3
           End If
                
'          "Por Partida Contable"
           If bTemp19 Then
                tvwListados.Nodes.Add "A1B8C2", tvwChild, "A1B8C2D2", gParametrosGenerales.gsSingPres2
                tvwListados.Nodes("A1B8C2D2").Image = 3
           End If
                
      End If
        
        
        ''' Por Concepto3
        If btemp21 Then
            tvwListados.Nodes.Add "A1B8", tvwChild, "A1B8C4", gParametrosGenerales.gsSingPres3
            tvwListados.Nodes("A1B8C4").Image = 3
        End If
        ''' Por Concepto4
        If btemp22 Then
            tvwListados.Nodes.Add "A1B8", tvwChild, "A1B8C5", gParametrosGenerales.gsSingPres4
            tvwListados.Nodes("A1B8C5").Image = 3
        End If
        ''' Por Concepto5 --Partidas de control de aprovisionamiento
        If bTemp28 Then
            tvwListados.Nodes.Add "A1B8", tvwChild, "A1B8C6", sIdiA1B8C6
            tvwListados.Nodes("A1B8C6").Image = 3
        End If
    End If

    ''Formas de pago

    If btemp20 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A5B1C2", sIdiA5B1C2
        tvwListados.Nodes("A5B1C2").Image = 3
    End If
    
    ''V�as de pago
    If bTemp29 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B9", sIdiA1B9
        tvwListados.Nodes("A1B9").Image = 3
    End If
    
    ''tipos de relaci�n
    If bTemp30 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B10", sIdiA1B10
        tvwListados.Nodes("A1B10").Image = 3
    End If
    
    'SOLICITUDES
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.SinAcceso Then
    Else
        bTemp1 = True
        bTemp3 = True
        If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        Else
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFConsultar)) Is Nothing) Then bTemp1 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIOConsultar)) Is Nothing) Then bTemp3 = False
        End If
        
        If bTemp1 Or bTemp3 Then
            tvwListados.Nodes.Add "A", tvwChild, "A8", sIdiA8
            tvwListados.Nodes("A8").Image = 1
            tvwListados.Nodes("A8").ExpandedImage = 2
        End If
        If bTemp1 Then
            tvwListados.Nodes.Add "A8", tvwChild, "A8B1", sIdiA8B1
            tvwListados.Nodes("A8B1").Image = 3
        End If
        If bTemp3 Then
            tvwListados.Nodes.Add "A8", tvwChild, "A8B3", sIdiA8B3
            tvwListados.Nodes("A8B3").Image = 3
        End If
    End If
    
    'PROCESOS
    
    If gParametrosGenerales.gbSolicitudesCompras Then
        bTemp7 = True
    Else
        bTemp7 = False
    End If
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
        bTemp1 = True
        bTemp2 = True
        bTemp3 = True
        bTemp4 = True
        bTemp5 = True
        bTemp6 = True
    Else
        bTemp1 = False
        bTemp2 = False
        bTemp3 = False
        bTemp4 = False
        bTemp5 = False
        bTemp6 = False
    End If
    
    '  ************** Procesos **************
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        
    Else
        If gParametrosGenerales.gbSolicitudesCompras Then
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICConsultar)) Is Nothing) Then bTemp7 = False
        End If
        If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing) Then bTemp1 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEConsultar)) Is Nothing) Then bTemp2 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEConsulta)) Is Nothing) Then bTemp3 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEConsultar)) Is Nothing) Then bTemp4 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEConsultar)) Is Nothing) Then bTemp6 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjConsulta)) Is Nothing) Then bTemp5 = False
        End If
    End If
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp6 And Not bTemp7 Then
    
    Else
        tvwListados.Nodes.Add "A", tvwChild, "A2", sIdiA2  'Procesos
        tvwListados.Nodes("A2").Image = 1
        tvwListados.Nodes("A2").ExpandedImage = 2
    End If
    
    If bTemp7 Then
        tvwListados.Nodes.Add "A2", tvwChild, "A2B6", sIdiA2B6  '"Solicitudes de compra"
        tvwListados.Nodes("A2B6").Image = 3
    End If
    
    If bTemp1 Then
        tvwListados.Nodes.Add "A2", tvwChild, "A2B1", sIdiA2B1  '"Datos de apertura"
        tvwListados.Nodes("A2B1").Image = 3
    End If
    
    If bTemp2 Then
        tvwListados.Nodes.Add "A2", tvwChild, "A2B2", sIdiA2B2  '"Proveedores seleccionados"
        tvwListados.Nodes("A2B2").Image = 3
    End If
    
    If bTemp3 Then
        tvwListados.Nodes.Add "A2", tvwChild, "A2B3", sIdiA2B3  '"Comunicaciones con proveedores"
        tvwListados.Nodes("A2B3").Image = 3
    End If
    
    If bTemp4 Or bTemp6 Then
        'Recepci�n de ofertas
        tvwListados.Nodes.Add "A2", tvwChild, "A2B4", sIdiA2B4
        tvwListados.Nodes("A2B4").Image = 1
        tvwListados.Nodes("A2B4").ExpandedImage = 2
        
        ''' Buz�n de ofertas
        If bTemp6 Then
            tvwListados.Nodes.Add "A2B4", tvwChild, "A2B4C1", sIdiA2B4C1
            tvwListados.Nodes("A2B4C1").Image = 3
        End If
        
        ''' Ofertas recibidas
        If bTemp4 Then
            tvwListados.Nodes.Add "A2B4", tvwChild, "A2B4C2", sIdiA2B4C2
            tvwListados.Nodes("A2B4C2").Image = 3
        End If
        
    End If
        
    If bTemp5 Then
        tvwListados.Nodes.Add "A2", tvwChild, "A2B5", sIdiA2B5  '"Adjudicaciones"
        tvwListados.Nodes("A2B5").Image = 3
    End If
    
    
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
        bTemp1 = True
    Else
        bTemp1 = False
    End If
    bTemp2 = False
    
    
        '  ************** Reuniones **************
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        
    Else
        If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREUConsultar)) Is Nothing) Then bTemp1 = False
        End If
    End If
    
    If Not bTemp1 And Not bTemp2 Then
    
    Else
        tvwListados.Nodes.Add "A", tvwChild, "A3", sIdiA3
        tvwListados.Nodes("A3").Image = 1
        tvwListados.Nodes("A3").ExpandedImage = 2
    End If
    
    If bTemp1 Then
        tvwListados.Nodes.Add "A3", tvwChild, "A3B1", sIdiA3B1
        tvwListados.Nodes("A3B1").Image = 3
    End If
      
    
    '''''  ************** Pedidos  **************
    If Not gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosDirectos Then
        ' no hay pedidos
    Else
        bTemp1 = True
        bTemp2 = True
        bTemp3 = True
        bTemp4 = True
        bTemp5 = True
        If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        Else
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGConsultar)) Is Nothing) Then bTemp1 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECConsultar)) Is Nothing) Then bTemp2 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGConsultar)) Is Nothing) Then bTemp3 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSegurConsultar)) Is Nothing) Then bTemp5 = False
        End If
        
        If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp5 Then
        
        Else
            'Pedidos
            tvwListados.Nodes.Add "A", tvwChild, "A7", sIdiA7
            tvwListados.Nodes("A7").Image = 1
            tvwListados.Nodes("A7").ExpandedImage = 2
        End If
        
        If bTemp3 Then
            If Not gParametrosGenerales.gbPedidosAprov Then bTemp4 = False
            
            If bTemp4 Then
                'Cat�logo
                tvwListados.Nodes.Add "A7", tvwChild, "A7B3", sIdiA7B3
                tvwListados.Nodes("A7B3").Image = 1
                tvwListados.Nodes("A7B3").ExpandedImage = 2
                'Configuraci�n
                tvwListados.Nodes.Add "A7B3", tvwChild, "A7B3C1", sIdiA7B3C1
                tvwListados.Nodes("A7B3C1").Image = 3
                'Seguridad
                If bTemp5 Then
                    tvwListados.Nodes.Add "A7B3", tvwChild, "A7B3C2", sIdiA7B3C2
                    tvwListados.Nodes("A7B3C2").Image = 3
                End If
            End If
        End If
        
        If bTemp1 Then
            tvwListados.Nodes.Add "A7", tvwChild, "A7B1", sIdiA7B1
            tvwListados.Nodes("A7B1").Image = 3
        End If
        
        If bTemp2 Then
            If Not bTemp1 Then
                tvwListados.Nodes.Add "A7", tvwChild, "A7B1", sIdiA7B2
                tvwListados.Nodes("A7B1").Image = 3
            Else
                tvwListados.Nodes.Add "A7", tvwChild, "A7B2", sIdiA7B2
                tvwListados.Nodes("A7B2").Image = 3
            End If
        End If
    End If
    
    
    
    
    '  ************** Informes  **************
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
        bTemp1 = True: bTemp2 = True: bTemp3 = True: bTemp4 = True
        bTemp5 = True: bTemp6 = True: bTemp7 = True: bTemp8 = True: bTemp9 = True
        'emm
        bTemp10 = True: bTemp11 = True
        'emm
        bTemp23 = True: bTemp24 = True: bTemp25 = True: bTemp26 = True
    Else
        bTemp1 = False: bTemp2 = False: bTemp3 = False: bTemp4 = False
        bTemp5 = False: bTemp6 = False: bTemp7 = False: bTemp8 = False: bTemp9 = False
        'emm
        bTemp10 = False: bTemp11 = False
        'emm
        bTemp23 = False: bTemp24 = False: bTemp25 = False: bTemp26 = False
    End If
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        
    Else
        If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegGenConsultar)) Is Nothing) Then bTemp1 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegmatConsultar)) Is Nothing) Then bTemp2 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegEqpResConsultar)) Is Nothing) Then bTemp3 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegEqpConsultar)) Is Nothing) Then bTemp4 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplGenConsultar)) Is Nothing) Then bTemp5 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplMatConsultar)) Is Nothing) Then bTemp6 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplProyConsultar)) Is Nothing) Then bTemp7 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplPartConsultar)) Is Nothing) Then bTemp8 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplUOConsultar)) Is Nothing) Then bTemp9 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto1Consultar)) Is Nothing) Then bTemp10 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto2Consultar)) Is Nothing) Then bTemp11 = False
        
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplCon3Consultar)) Is Nothing) Then bTemp23 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplCon4Consultar)) Is Nothing) Then bTemp24 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegCon3Consultar)) Is Nothing) Then bTemp25 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegCon4Consultar)) Is Nothing) Then bTemp26 = False
        End If
    End If
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp6 And Not bTemp7 And Not bTemp8 And Not bTemp9 And Not bTemp10 And Not bTemp11 And Not bTemp25 And Not bTemp26 Then
    
    Else
    
        'Informes
        tvwListados.Nodes.Add "A", tvwChild, "A4", sIdiA4
        tvwListados.Nodes("A4").Image = 1
        tvwListados.Nodes("A4").ExpandedImage = 2
        'AHORROS NEGOCIADOS
        If bTemp1 Or bTemp2 Or bTemp3 Or bTemp4 Or bTemp25 Or bTemp26 Then
            'ahorros Negociados
            tvwListados.Nodes.Add "A4", tvwChild, "A4B1", sIdiA4B1
            tvwListados.Nodes("A4B1").Image = 1
            tvwListados.Nodes("A4B1").ExpandedImage = 2
            If bTemp1 Then
                'General
                tvwListados.Nodes.Add "A4B1", tvwChild, "A4B1C1", sIdiA4B1C1
                tvwListados.Nodes("A4B1C1").Image = 1
                tvwListados.Nodes("A4B1C1").ExpandedImage = 2
                'En reuni�n
                tvwListados.Nodes.Add "A4B1C1", tvwChild, "A4B1C1D1", sIdiA4B1C1D1
                tvwListados.Nodes("A4B1C1D1").Image = 3
                'Desde/hasta fecha
                tvwListados.Nodes.Add "A4B1C1", tvwChild, "A4B1C1D2", sIdiA4B1C1D2
                tvwListados.Nodes("A4B1C1D2").Image = 3
            End If
            If bTemp2 Then
                'Por material
                tvwListados.Nodes.Add "A4B1", tvwChild, "A4B1C2", sIdiA4B1C2
                tvwListados.Nodes("A4B1C2").Image = 1
                tvwListados.Nodes("A4B1C2").ExpandedImage = 2
                'En reuni�n
                tvwListados.Nodes.Add "A4B1C2", tvwChild, "A4B1C2D1", sIdiA4B1C2D1
                tvwListados.Nodes("A4B1C2D1").Image = 3
                'Desde/hasta fecha
                tvwListados.Nodes.Add "A4B1C2", tvwChild, "A4B1C2D2", sIdiA4B1C2D2
                tvwListados.Nodes("A4B1C2D2").Image = 3
            End If
            If bTemp3 Then
                'Por equipos responsables
                tvwListados.Nodes.Add "A4B1", tvwChild, "A4B1C3", sIdiA4B1C3
                tvwListados.Nodes("A4B1C3").Image = 1
                tvwListados.Nodes("A4B1C3").ExpandedImage = 2
                'En reuni�n
                tvwListados.Nodes.Add "A4B1C3", tvwChild, "A4B1C3D1", sIdiA4B1C3D1
                tvwListados.Nodes("A4B1C3D1").Image = 3
                'Desde/hasta fecha
                tvwListados.Nodes.Add "A4B1C3", tvwChild, "A4B1C3D2", sIdiA4B1C3D2
                tvwListados.Nodes("A4B1C3D2").Image = 3
            End If
            If bTemp4 Then
                'Por equipos negociadores
                tvwListados.Nodes.Add "A4B1", tvwChild, "A4B1C4", sIdiA4B1C4
                tvwListados.Nodes("A4B1C4").Image = 1
                tvwListados.Nodes("A4B1C4").ExpandedImage = 2
                'En reuni�n
                tvwListados.Nodes.Add "A4B1C4", tvwChild, "A4B1C4D1", sIdiA4B1C4D1
                tvwListados.Nodes("A4B1C4D1").Image = 3
                'Desde/hasta fecha
                tvwListados.Nodes.Add "A4B1C4", tvwChild, "A4B1C4D2", sIdiA4B1C4D2
                tvwListados.Nodes("A4B1C4D2").Image = 3
            End If
            
            If bTemp25 And gParametrosGenerales.gbUsarPres3 = True Then
                'Por presupuesto 3
                tvwListados.Nodes.Add "A4B1", tvwChild, "A4B1C5", gParametrosGenerales.gsPlurPres3
                tvwListados.Nodes("A4B1C5").Image = 1
                tvwListados.Nodes("A4B1C5").ExpandedImage = 2
                'En reuni�n
                tvwListados.Nodes.Add "A4B1C5", tvwChild, "A4B1C5D1", sIdiA4B1C4D1
                tvwListados.Nodes("A4B1C5D1").Image = 3
                'Desde/hasta fecha
                tvwListados.Nodes.Add "A4B1C5", tvwChild, "A4B1C5D2", sIdiA4B1C4D2
                tvwListados.Nodes("A4B1C5D2").Image = 3
            End If
            
            If bTemp26 And gParametrosGenerales.gbUsarPres4 = True Then
                'Por presupuesto 4
                tvwListados.Nodes.Add "A4B1", tvwChild, "A4B1C6", gParametrosGenerales.gsPlurPres4
                tvwListados.Nodes("A4B1C6").Image = 1
                tvwListados.Nodes("A4B1C6").ExpandedImage = 2
                'En reuni�n
                tvwListados.Nodes.Add "A4B1C6", tvwChild, "A4B1C6D1", sIdiA4B1C4D1
                tvwListados.Nodes("A4B1C6D1").Image = 3
                'Desde/hasta fecha
                tvwListados.Nodes.Add "A4B1C6", tvwChild, "A4B1C6D2", sIdiA4B1C4D2
                tvwListados.Nodes("A4B1C6D2").Image = 3
            End If
        End If
        
        'Ahorros Aplicados
        If bTemp5 Or bTemp6 Or bTemp7 Or bTemp8 Or bTemp9 Or bTemp23 Or bTemp24 Then
            'Ahorros Aplicados
            tvwListados.Nodes.Add "A4", tvwChild, "A4B2", sIdiA4B2
            tvwListados.Nodes("A4B2").Image = 1
            tvwListados.Nodes("A4B2").ExpandedImage = 2
            If bTemp5 Then
                ' General
                tvwListados.Nodes.Add "A4B2", tvwChild, "A4B2C1", sIdiA4B2C1
                tvwListados.Nodes("A4B2C1").Image = 3
            End If
            If bTemp6 Then
                ' Material
                tvwListados.Nodes.Add "A4B2", tvwChild, "A4B2C2", sIdiA4B2C2
                tvwListados.Nodes("A4B2C2").Image = 3
            End If
            If bTemp7 And gParametrosGenerales.gbUsarPres1 = True Then
                ' Proyectos
                tvwListados.Nodes.Add "A4B2", tvwChild, "A4B2C3", gParametrosGenerales.gsPlurPres1
                tvwListados.Nodes("A4B2C3").Image = 3
            End If
            If bTemp8 And gParametrosGenerales.gbUsarPres2 = True Then
            ' Partidas Contables
                tvwListados.Nodes.Add "A4B2", tvwChild, "A4B2C4", gParametrosGenerales.gsPlurPres2
                tvwListados.Nodes("A4B2C4").Image = 3
            End If
            If bTemp23 And gParametrosGenerales.gbUsarPres3 = True Then
                'Presupuesto 3
                tvwListados.Nodes.Add "A4B2", tvwChild, "A4B2C6", gParametrosGenerales.gsPlurPres3
                tvwListados.Nodes("A4B2C6").Image = 3
            End If
            If bTemp24 And gParametrosGenerales.gbUsarPres4 = True Then 'Presupuesto 4
                tvwListados.Nodes.Add "A4B2", tvwChild, "A4B2C7", gParametrosGenerales.gsPlurPres4
                tvwListados.Nodes("A4B2C7").Image = 3
            End If
            If bTemp9 Then
                ' Unidades organizativas
                tvwListados.Nodes.Add "A4B2", tvwChild, "A4B2C5", sIdiA4B2C5
                tvwListados.Nodes("A4B2C5").Image = 3
            End If
        End If
        
        'emm
        
        If bTemp10 And gParametrosGenerales.gbUsarPres1 = True Then
            ' "Informes de Evoluci�n de PROY1"
            tvwListados.Nodes.Add "A4", tvwChild, "A4B3", sIdiA4B3
            tvwListados.Nodes("A4B3").Image = 3
        End If
        
        If bTemp11 And gParametrosGenerales.gbUsarPres2 = True Then
            ' "Informes de Evoluci�n de PROY2"
            tvwListados.Nodes.Add "A4", tvwChild, "A4B4", sIdiA4B4
            tvwListados.Nodes("A4B4").Image = 3
        End If
        
        'emm
        
    End If
    
    
    '  ************** Parametros  **************
    bTemp1 = True
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        
    Else
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PARGEN)) Is Nothing Then bTemp1 = False
    End If
    If Not bTemp1 Then
    
    Else
    
'        tvwListados.Nodes.Add "A", tvwChild, "A5", "Parametros"
        tvwListados.Nodes.Add "A", tvwChild, "A5", sIdiA5
        tvwListados.Nodes("A5").Image = 1
        tvwListados.Nodes("A5").ExpandedImage = 2
        'Parametros generales
        If bTemp1 Then
            tvwListados.Nodes.Add "A5", tvwChild, "A5B1", sIdiA5B1
            tvwListados.Nodes("A5B1").Image = 1
            tvwListados.Nodes("A5B1").ExpandedImage = 2

            If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
                tvwListados.Nodes.Add "A5B1", tvwChild, "A5B1C3", sIdiA5B1C3
                tvwListados.Nodes("A5B1C3").Image = 3
            End If
            tvwListados.Nodes.Add "A5B1", tvwChild, "A5B1C4", sIdiA5B1C4
            tvwListados.Nodes("A5B1C4").Image = 3
            If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
                tvwListados.Nodes.Add "A5B1", tvwChild, "A5B1C5", sIdiA5B1C5
                tvwListados.Nodes("A5B1C5").Image = 3
            End If
            tvwListados.Nodes.Add "A5B1", tvwChild, "A5B1C6", sIdiA5B1C6
            tvwListados.Nodes("A5B1C6").Image = 3
            If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
                tvwListados.Nodes.Add "A5B1", tvwChild, "A5B1C7", sIdiA5B1C7
                tvwListados.Nodes("A5B1C7").Image = 3
            End If
            tvwListados.Nodes.Add "A5B1", tvwChild, "A5B1C8", sIdiA5B1C8
            tvwListados.Nodes("A5B1C8").Image = 3
        End If
    End If
     '  ************** Seguridad  **************
    
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
    
        tvwListados.Nodes.Add "A", tvwChild, "A6", sIdiA6
        tvwListados.Nodes("A6").Image = 1
        tvwListados.Nodes("A6").ExpandedImage = 2
                    
        tvwListados.Nodes.Add "A6", tvwChild, "A6B1", sIdiA6B1
        tvwListados.Nodes("A6B1").Image = 3
        
        tvwListados.Nodes.Add "A6", tvwChild, "A6B2", sIdiA6B2
        tvwListados.Nodes("A6B2").Image = 3
        If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
            tvwListados.Nodes.Add "A6", tvwChild, "A6B3", sIdiA6B3
            tvwListados.Nodes("A6B3").Image = 1
            tvwListados.Nodes("A6B3").ExpandedImage = 2
            tvwListados.Nodes.Add "A6B3", tvwChild, "A6B3C1", sIdiA6B3C1
            tvwListados.Nodes("A6B3C1").Image = 3
        End If
        
    End If


End Sub

Private Sub GenerarArbolDeListadosFSEP()
Dim bTemp1 As Boolean
Dim bTemp2 As Boolean
Dim bTemp3 As Boolean
Dim bTemp4 As Boolean
Dim bTemp5 As Boolean
Dim bTemp6 As Boolean
Dim bTemp7 As Boolean
Dim bTemp8 As Boolean
Dim bTemp9 As Boolean
Dim bTemp10 As Boolean
Dim bTemp11 As Boolean
Dim bTemp14 As Boolean
Dim bTemp15 As Boolean
Dim btemp20 As Boolean
Dim btemp21 As Boolean
Dim bTemp29 As Boolean

''' Generar treeview teniendo en cuenta la seguridad
    
    tvwListados.Indentation = 200
    tvwListados.ImageList = imglstListados
    tvwListados.Nodes.Add , tvwFirst, "A", sIdiA
    tvwListados.Nodes("A").Image = 0
    tvwListados.Nodes("A").ExpandedImage = 2
    tvwListados.Nodes("A").Expanded = True

    'MANTENIMIENTO
    bTemp1 = True: bTemp2 = True: bTemp3 = True: bTemp4 = True: bTemp5 = True
    bTemp6 = True: bTemp7 = True: bTemp8 = True: bTemp9 = True: bTemp10 = True
    bTemp11 = True: bTemp14 = True: bTemp15 = True: btemp21 = True: btemp20 = True: bTemp29 = True
        
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        
    Else
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MONConsultar)) Is Nothing) Then bTemp1 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PAIConsultar)) Is Nothing) Then bTemp2 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVIConsultar)) Is Nothing) Then bTemp3 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGConsultar)) Is Nothing) Then bTemp4 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGConsultarDest)) Is Nothing) Then bTemp5 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATConsultar)) Is Nothing) Then bTemp6 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.UNIConsultar)) Is Nothing) Then bTemp7 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEConsultar)) Is Nothing) Then bTemp11 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVEConsultar)) Is Nothing) Then bTemp14 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorGRUPConsultar)) Is Nothing) Then bTemp15 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PAGConsultar)) Is Nothing) Then btemp20 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.VIAPAGConsultar)) Is Nothing) Then bTemp29 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBConsultar)) Is Nothing) Then btemp21 = False
    End If
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp6 And Not bTemp7 And Not bTemp11 And Not btemp21 Then
    
    Else
    
        ' ********************** Mantenimiento ***********************
        tvwListados.Nodes.Add "A", tvwChild, "A1", sIdiA1
        tvwListados.Nodes("A1").Image = 1
        tvwListados.Nodes("A1").ExpandedImage = 2

    End If
    
    ''' Monedas

    If bTemp1 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B1", sIdiA1B1
        tvwListados.Nodes("A1B1").Image = 3
    End If

    ''' Pa�ses

    If bTemp2 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B2", sIdiA1B2
        tvwListados.Nodes("A1B2").Image = 3
    End If

    ''' Provincias

    If bTemp3 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B3", sIdiA1B3
        tvwListados.Nodes("A1B3").Image = 3
    End If

    ' Estructura de la organizaci�n

    If bTemp4 Or bTemp5 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B4", sIdiA1B4
        tvwListados.Nodes("A1B4").Image = 1
        tvwListados.Nodes("A1B4").ExpandedImage = 2

        ''' Organigrama
        If bTemp4 Then
            tvwListados.Nodes.Add "A1B4", tvwChild, "A1B4C1", sIdiA1B4C1
            tvwListados.Nodes("A1B4C1").Image = 3
        End If
        ''' Destinos
        If bTemp5 Then
            tvwListados.Nodes.Add "A1B4", tvwChild, "A1B4C2", sIdiA1B4C2
            tvwListados.Nodes("A1B4C2").Image = 3
        End If
    End If

    ' Estructura de Materiales
    If bTemp6 Or bTemp7 Or btemp21 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B5", sIdiA1B5
        tvwListados.Nodes("A1B5").Image = 1
        tvwListados.Nodes("A1B5").ExpandedImage = 2

        ''' Materiales y articulos
        If bTemp6 Then
            tvwListados.Nodes.Add "A1B5", tvwChild, "A1B5C1", sIdiA1B5C1
            tvwListados.Nodes("A1B5C1").Image = 3
            tvwListados.Nodes.Add "A1B5", tvwChild, "A1B5C2", sIdiA1B5C2
            tvwListados.Nodes("A1B5C2").Image = 3
        End If

        ''' Unidades
        If bTemp7 Then
            tvwListados.Nodes.Add "A1B5", tvwChild, "A1B5C3", sIdiA1B5C3
            tvwListados.Nodes("A1B5C3").Image = 3
        End If
        
        '''Atributos
        If btemp21 Then
            tvwListados.Nodes.Add "A1B5", tvwChild, "A1B5C4", sIdiA1B5C4
            tvwListados.Nodes("A1B5C4").Image = 3
        End If
    End If

     'Proveedores
    If bTemp11 Or bTemp14 Or bTemp15 Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B7", sIdiA1B7
        tvwListados.Nodes("A1B7").Image = 1
        tvwListados.Nodes("A1B7").ExpandedImage = 2

        ' Datos B�sicos
        If bTemp11 Then
            tvwListados.Nodes.Add "A1B7", tvwChild, "A1B7C1", sIdiA1B7C1
            tvwListados.Nodes("A1B7C1").Image = 3
        End If
        
        ' Material por Proveedor
        If bTemp14 Then
            tvwListados.Nodes.Add "A1B7", tvwChild, "A1B7C3", sIdiA1B7C3
            tvwListados.Nodes("A1B7C3").Image = 3
        End If
        
        ' Proveedores por material
        If bTemp15 Then
            tvwListados.Nodes.Add "A1B7", tvwChild, "A1B7C4", sIdiA1B7C4
            tvwListados.Nodes("A1B7C4").Image = 3
        End If
            End If

        ''Formas de pago

        If btemp20 Then
            tvwListados.Nodes.Add "A1", tvwChild, "A5B1C2", sIdiA5B1C2
            tvwListados.Nodes("A5B1C2").Image = 3
        End If
        
        ''V�as de pago
        If bTemp29 Then
            tvwListados.Nodes.Add "A1", tvwChild, "A1B9", sIdiA1B9
            tvwListados.Nodes("A1B9").Image = 3
        End If
    
       
     '  ************** Pedidos  **************
        bTemp1 = True
        bTemp2 = True
        bTemp3 = True
        bTemp4 = True
        
        If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        Else
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGConsultar)) Is Nothing) Then bTemp1 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECConsultar)) Is Nothing) Then bTemp2 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGConsultar)) Is Nothing) Then bTemp3 = False
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSegurConsultar)) Is Nothing) Then bTemp4 = False
        End If
        
        If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 Then
        
        Else
            'Pedidos
            tvwListados.Nodes.Add "A", tvwChild, "A7", sIdiA7
            tvwListados.Nodes("A7").Image = 1
            tvwListados.Nodes("A7").ExpandedImage = 2
        End If
        
        If bTemp3 Then
            'Cat�logo
            tvwListados.Nodes.Add "A7", tvwChild, "A7B3", sIdiA7B3
            tvwListados.Nodes("A7B3").Image = 1
            tvwListados.Nodes("A7B3").ExpandedImage = 2
            'Configuraci�n
            tvwListados.Nodes.Add "A7B3", tvwChild, "A7B3C1", sIdiA7B3C1
            tvwListados.Nodes("A7B3C1").Image = 3
            If bTemp4 Then
                'Seguridad catalogo
                tvwListados.Nodes.Add "A7B3", tvwChild, "A7B3C2", sIdiA7B3C2
                tvwListados.Nodes("A7B3C2").Image = 3
            End If

        End If
        
        If bTemp1 Then
            tvwListados.Nodes.Add "A7", tvwChild, "A7B1", sIdiA7B1
            tvwListados.Nodes("A7B1").Image = 3
        End If
        
        If bTemp2 Then
            If Not bTemp1 Then
                tvwListados.Nodes.Add "A7", tvwChild, "A7B1", sIdiA7B2
                tvwListados.Nodes("A7B1").Image = 3
            Else
                tvwListados.Nodes.Add "A7", tvwChild, "A7B2", sIdiA7B2
                tvwListados.Nodes("A7B2").Image = 3
            End If
        End If
     
     
    '  ************** Parametros  **************
    bTemp1 = True
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        
    Else
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PARGEN)) Is Nothing Then bTemp1 = False
    End If
    If Not bTemp1 Then
    
    Else
    
        tvwListados.Nodes.Add "A", tvwChild, "A5", sIdiA5
        tvwListados.Nodes("A5").Image = 1
        tvwListados.Nodes("A5").ExpandedImage = 2
        'Parametros generales
        If bTemp1 Then
            tvwListados.Nodes.Add "A5", tvwChild, "A5B1C4", sIdiA5B1C4
            tvwListados.Nodes("A5B1C4").Image = 3
            tvwListados.Nodes.Add "A5", tvwChild, "A5B1C8", sIdiA5B1C8
            tvwListados.Nodes("A5B1C8").Image = 3
        End If
    End If
     
    '  ************** Seguridad  **************
    
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
    
        tvwListados.Nodes.Add "A", tvwChild, "A6", sIdiA6
        tvwListados.Nodes("A6").Image = 1
        tvwListados.Nodes("A6").ExpandedImage = 2
                    
        tvwListados.Nodes.Add "A6", tvwChild, "A6B1", sIdiA6B1
        tvwListados.Nodes("A6B1").Image = 3
        
        tvwListados.Nodes.Add "A6", tvwChild, "A6B2", sIdiA6B2
        tvwListados.Nodes("A6B2").Image = 3
         
    End If
    
   
    
End Sub
