VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRORGDestAlm 
   BackColor       =   &H00808000&
   Caption         =   "Al"
   ClientHeight    =   4170
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8070
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRORGDestAlm.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4170
   ScaleWidth      =   8070
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdActualizar 
      Caption         =   "Actualizar"
      Height          =   315
      Left            =   1140
      TabIndex        =   3
      Top             =   3780
      Width           =   1005
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "Modificar"
      Default         =   -1  'True
      Height          =   315
      Left            =   90
      TabIndex        =   2
      Top             =   3780
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   3810
      TabIndex        =   1
      Top             =   3780
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Height          =   315
      Left            =   2730
      TabIndex        =   0
      Top             =   3780
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAlmacenes 
      Height          =   3675
      Left            =   60
      TabIndex        =   4
      Top             =   30
      Width           =   7860
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   4
      stylesets.count =   5
      stylesets(0).Name=   "IntOK"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmESTRORGDestAlm.frx":014A
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmESTRORGDestAlm.frx":0166
      stylesets(2).Name=   "IntHeader"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmESTRORGDestAlm.frx":0182
      stylesets(2).AlignmentText=   0
      stylesets(2).AlignmentPicture=   0
      stylesets(3).Name=   "IntKO"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   255
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmESTRORGDestAlm.frx":07AC
      stylesets(3).AlignmentText=   0
      stylesets(4).Name=   "UONInf"
      stylesets(4).BackColor=   13172735
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmESTRORGDestAlm.frx":07C8
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).Alignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   100
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   1667
      Columns(1).Caption=   "Asignado"
      Columns(1).Name =   "ASIG"
      Columns(1).Alignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   2
      Columns(2).Width=   3122
      Columns(2).Caption=   "COD"
      Columns(2).Name =   "COD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   7964
      Columns(3).Caption=   "DEN"
      Columns(3).Name =   "DEN"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      _ExtentX        =   13864
      _ExtentY        =   6482
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmESTRORGDestAlm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnMargen As Long = 60

Public g_sDestino As String
Public g_bModoEdicion As Boolean
Public g_bModif As Boolean

Private m_sIdiConsulta As String
Private m_sIdiEdicion As String
Private m_sIdiTitulo As String

Private m_oAlmacenesDest As CAlmacenes
Private m_oAlmacenes As CAlmacenes
Private m_oAlmacenesSel As CAlmacenes
Private m_oAlmacenesDes As CAlmacenes

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    
    teserror = m_oAlmacenesDest.AsignarAlmacenesADestinos(g_sDestino, m_oAlmacenesSel, m_oAlmacenesDes)
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Exit Sub
    End If
    
    Unload Me
End Sub

Private Sub cmdActualizar_Click()
    CargarGridAlmacenes
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdModificar_Click()
    g_bModoEdicion = True
    cmdModificar.Visible = False
    cmdActualizar.Visible = False
    cmdAceptar.Visible = True
    cmdCancelar.Visible = True
    sdbgAlmacenes.Columns("ASIG").Visible = True
    sdbgAlmacenes.AllowUpdate = True
    sdbgAlmacenes.Columns(1).Width = sdbgAlmacenes.Width * 12 / 100
    sdbgAlmacenes.Columns(2).Width = sdbgAlmacenes.Width * 23 / 100
    sdbgAlmacenes.Columns(3).Width = sdbgAlmacenes.Width * 57 / 100
    Set m_oAlmacenesSel = oFSGSRaiz.Generar_CAlmacenes
    Set m_oAlmacenesDes = oFSGSRaiz.Generar_CAlmacenes
    caption = m_sIdiTitulo & " " & m_sIdiEdicion
    CargarGridAlmacenes
End Sub

Private Sub Form_Load()

Me.Width = 8190
Me.Height = 4680

CargarRecursos

PonerFieldSeparator Me

If g_bModif = False Then
    cmdModificar.Visible = False
    cmdActualizar.Visible = False
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    sdbgAlmacenes.Columns("ASIG").Visible = False
Else
    If g_bModoEdicion Then
        Set m_oAlmacenesSel = oFSGSRaiz.Generar_CAlmacenes
    Set m_oAlmacenesDes = oFSGSRaiz.Generar_CAlmacenes
        cmdModificar.Visible = False
        cmdActualizar.Visible = False
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        sdbgAlmacenes.Columns("ASIG").Visible = True
        sdbgAlmacenes.AllowUpdate = True
    Else
        cmdModificar.Visible = True
        cmdActualizar.Visible = True
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        sdbgAlmacenes.Columns("ASIG").Visible = False
        sdbgAlmacenes.AllowUpdate = True
        
    End If
End If
Set m_oAlmacenesDest = oFSGSRaiz.Generar_CAlmacenes
Set m_oAlmacenes = oFSGSRaiz.Generar_CAlmacenes

CargarGridAlmacenes

End Sub
Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTROG_DESTALM, basPublic.gParametrosInstalacion.gidioma)
    
    If Not ador Is Nothing Then
        caption = ador(0).Value
        ador.MoveNext
        cmdModificar.caption = ador(0).Value
        ador.MoveNext
        cmdActualizar.caption = ador(0).Value
        ador.MoveNext
        cmdAceptar.caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value
        ador.MoveNext
        sdbgAlmacenes.Columns("ASIG").caption = ador(0).Value
        ador.MoveNext
        sdbgAlmacenes.Columns("COD").caption = ador(0).Value
        ador.MoveNext
        sdbgAlmacenes.Columns("DEN").caption = ador(0).Value
        ador.MoveNext
        m_sIdiConsulta = "(" & ador(0).Value & ")"
        ador.MoveNext
        m_sIdiEdicion = "(" & ador(0).Value & ")"
        ador.Close
        
        m_sIdiTitulo = Replace(caption, "@@@", g_sDestino)
        If g_bModoEdicion Then
            caption = m_sIdiTitulo & " " & m_sIdiEdicion
        Else
            caption = m_sIdiTitulo & " " & m_sIdiConsulta
        End If
    End If

    Set ador = Nothing
End Sub

Private Sub CargarGridAlmacenes(Optional ByVal bOrdenarPorCod As Boolean = True)

Dim oAlmacen As CAlmacen
Dim sStr As String
Dim sAsig As String

Screen.MousePointer = vbHourglass

'Ahora hay que cargar los almacenes
sdbgAlmacenes.RemoveAll

m_oAlmacenesDest.CargarAlmacenesDestino (g_sDestino)

If g_bModoEdicion Then
    m_oAlmacenes.CargarAlmacenes
    
    For Each oAlmacen In m_oAlmacenes
        If Not m_oAlmacenesDest.Item(CStr(oAlmacen.Id)) Is Nothing Then
            sAsig = "1"
        Else
            sAsig = "0"
        End If
        sStr = CStr(oAlmacen.Id) & Chr(m_lSeparador) & sAsig & Chr(m_lSeparador) & oAlmacen.Cod & Chr(m_lSeparador) & oAlmacen.Den
        sdbgAlmacenes.AddItem sStr
    Next
Else
    For Each oAlmacen In m_oAlmacenesDest
        sStr = CStr(oAlmacen.Id) & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oAlmacen.Cod & Chr(m_lSeparador) & oAlmacen.Den
        sdbgAlmacenes.AddItem sStr
    Next

End If
Screen.MousePointer = vbNormal

End Sub


Private Sub Form_Resize()
    sdbgAlmacenes.Move cnMargen, cnMargen, Me.ScaleWidth - (2 * cnMargen), Me.ScaleHeight - (3 * cnMargen) - cmdModificar.Height
    
    If sdbgAlmacenes.Columns(1).Visible = False Then
        sdbgAlmacenes.Columns(2).Width = sdbgAlmacenes.Width * 28 / 100
        sdbgAlmacenes.Columns(3).Width = sdbgAlmacenes.Width * 62 / 100
    Else
        sdbgAlmacenes.Columns(1).Width = sdbgAlmacenes.Width * 15 / 100
        sdbgAlmacenes.Columns(2).Width = sdbgAlmacenes.Width * 20 / 100
        sdbgAlmacenes.Columns(3).Width = sdbgAlmacenes.Width * 55 / 100
    End If
        
    cmdModificar.Move cnMargen, sdbgAlmacenes.Top + sdbgAlmacenes.Height + cnMargen
    cmdActualizar.Move cmdModificar.Left + cmdModificar.Width + cnMargen, cmdModificar.Top
    
    cmdAceptar.Move cnMargen, sdbgAlmacenes.Top + sdbgAlmacenes.Height + cnMargen
    cmdCancelar.Move cmdAceptar.Left + cmdAceptar.Width + cnMargen, cmdAceptar.Top
End Sub

Private Sub sdbgAlmacenes_Change()
    If sdbgAlmacenes.Columns("ASIG").Value = "-1" Then
        ' Checked
        If m_oAlmacenesDest.Item(CStr(sdbgAlmacenes.Columns("ID").Value)) Is Nothing Then
            'Si no esta asignado desde antes, lo asignaremos
            m_oAlmacenesSel.Add sdbgAlmacenes.Columns("ID").Value, sdbgAlmacenes.Columns("COD").Value, sdbgAlmacenes.Columns("DEN").Value
        Else
            If Not m_oAlmacenesDes.Item(CStr(sdbgAlmacenes.Columns("ID").Value)) Is Nothing Then
                m_oAlmacenesDes.Remove CStr(sdbgAlmacenes.Columns("ID").Value)
            End If
        End If
    Else
        ' Unchecked
        If Not m_oAlmacenesDest.Item(CStr(sdbgAlmacenes.Columns("ID").Value)) Is Nothing Then
            m_oAlmacenesDes.Add sdbgAlmacenes.Columns("ID").Value, sdbgAlmacenes.Columns("COD").Value, sdbgAlmacenes.Columns("DEN").Value
        Else
            If Not m_oAlmacenesSel.Item(CStr(sdbgAlmacenes.Columns("ID").Value)) Is Nothing Then
                m_oAlmacenesSel.Remove CStr(sdbgAlmacenes.Columns("ID").Value)
            End If
        End If
    End If
   
   sdbgAlmacenes.Update
End Sub

Private Sub sdbgAlmacenes_HeadClick(ByVal ColIndex As Integer)
    If g_bModoEdicion Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If sdbgAlmacenes.Columns(ColIndex).Name = "COD" Then
        CargarGridAlmacenes (True)
    Else
        CargarGridAlmacenes (False)
    End If
    Screen.MousePointer = vbNormal
End Sub
