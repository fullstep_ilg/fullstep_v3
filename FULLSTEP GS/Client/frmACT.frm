VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmACT 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DActa de reuni�n"
   ClientHeight    =   2715
   ClientLeft      =   15
   ClientTop       =   2580
   ClientWidth     =   8370
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmACT.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2715
   ScaleWidth      =   8370
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   415
      Index           =   1
      Left            =   0
      Picture         =   "frmACT.frx":0CB2
      ScaleHeight     =   420
      ScaleWidth      =   420
      TabIndex        =   15
      Top             =   0
      Width           =   415
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   415
      Index           =   0
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   420
      TabIndex        =   14
      Top             =   0
      Width           =   415
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   8040
      Top             =   2280
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   495
      Left            =   6480
      TabIndex        =   13
      Top             =   2160
      Width           =   975
      ExtentX         =   1720
      ExtentY         =   873
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.CheckBox chkProvNoAdj 
      BackColor       =   &H00808000&
      Caption         =   "DIncluir proveedores no adjudicados"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   2040
      Width           =   8055
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Caption         =   $"frmACT.frx":0D2D
      Height          =   855
      Left            =   240
      TabIndex        =   11
      Top             =   1080
      Width           =   8000
      Begin VB.OptionButton optTipo2 
         BackColor       =   &H00808000&
         Caption         =   "DProveedores adjudicados con el detalle de items asignados para cada uno"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   0
         Width           =   6300
      End
      Begin VB.TextBox txtDOT2 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1605
         MaxLength       =   255
         TabIndex        =   5
         Top             =   360
         Width           =   5895
      End
      Begin VB.CommandButton cmdDOT2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7545
         Picture         =   "frmACT.frx":0DB4
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Examinar"
         Top             =   360
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.Label lblUsar2 
         BackColor       =   &H00808000&
         Caption         =   "DUsar la plantilla:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   400
         Width           =   1560
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Caption         =   "                                                                                                                              "
      Height          =   855
      Left            =   240
      TabIndex        =   9
      Top             =   120
      Width           =   8000
      Begin VB.OptionButton optTipo1 
         BackColor       =   &H00808000&
         Caption         =   "DItems del proceso con el detalle de adjudicaciones para cada uno"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   0
         Width           =   6300
      End
      Begin VB.CommandButton cmdDOT 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7545
         Picture         =   "frmACT.frx":10F6
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Examinar"
         Top             =   360
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.TextBox txtDOT 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1605
         MaxLength       =   255
         TabIndex        =   2
         Top             =   360
         Width           =   5895
      End
      Begin VB.Label lblUsar1 
         BackColor       =   &H00808000&
         Caption         =   "DUsar la plantilla:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   400
         Width           =   1560
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Height          =   315
      Left            =   2880
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   2350
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   4020
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   2350
      Width           =   1005
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmACT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public oDestinos As CDestinos
Public oPagos As CPagos
Public oUnidades As CUnidades
Public PermitirAdjReunionNoVig As Boolean
Public FechaVigente As Boolean

Private Type ImportesProceso
    AbiertoProc As Double
    ConsumidoProc As Double
    AbiertoProcReal As Double
    ConsumidoProcReal As Double
    AdjudicadoProc As Double
    AdjudicadoProcReal As Double
    AdjudicadoSinProc As Double
    AdjudicadoSinProcReal As Double
    AhorradoProc As Double
    AhorradoPorcenProc As Double
    TotalProveObjAll As Double
    TotalAhorroProveObjAll As Double
End Type

'Var. para traducci�n
Private sCap(5 To 18) As String

Private oProce As cProceso
Private oProves As CProveedores
Private oAdjsGrupo As CAdjsGrupo
Private oProvesnoAdj As CProveedores

Private m_bCargadoGen As Boolean
Private oFos As Scripting.FileSystemObject
Private oGestorReuniones As CGestorReuniones

Private m_sFormatoNumberGr As String
Private m_sFormatoNumberPorcenGr As String
Private m_sFormatoNumberPrecioGr As String
Private m_sFormatoNumberCantGr As String
Private m_sFormatoNumberProce As String
Private m_sFormatoNumberProcePorcen As String
Private m_oAsigs As CAsignaciones
'Variables para idiomas:
Private m_stxtPrecio As String
Private m_stxtImp As String

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    If gParametrosInstalacion.giActaTipo = ActaWord Then
        cmmdDot.Filter = sCap(5) & " (*.dot)|*.dot"
    Else
        cmmdDot.Filter = sCap(5) & " (*.rpt)|*.rpt"
    End If
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOT = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub

End Sub

Private Sub cmdAceptar_Click()
    Dim bError As Boolean
    
    On Error GoTo ERROR:
    
    bError = False
    'Comprobar las plantillas
    If gParametrosInstalacion.giActaTipo = ActaWord Then
        'Acta en Word
        If optTipo1.Value = True Then
            If Right(txtDOT, 3) <> "dot" Then
                oMensajes.NoValido sCap(6)
                If Me.Visible Then txtDOT.SetFocus
                Exit Sub
            End If
            
            If Not oFos.FileExists(txtDOT) Then
                oMensajes.NoValido sCap(6)
                If Me.Visible Then txtDOT.SetFocus
                Exit Sub
            End If
        
        Else
            If Right(txtDOT2, 3) <> "dot" Then
                oMensajes.NoValido sCap(6)
                If Me.Visible Then txtDOT2.SetFocus
                Exit Sub
            End If
            
            If Not oFos.FileExists(txtDOT2) Then
                oMensajes.NoValido sCap(6)
                If Me.Visible Then txtDOT2.SetFocus
                Exit Sub
            End If
        
        End If
    Else
        'Acta en Crystal
        If optTipo1.Value = True Then
            If Right(txtDOT, 3) <> "rpt" Then
                oMensajes.NoValido sCap(6)
                If Me.Visible Then txtDOT.SetFocus
                Exit Sub
            End If
            
            If Not oFos.FileExists(txtDOT) Then
                oMensajes.NoValido sCap(6)
                If Me.Visible Then txtDOT.SetFocus
                Exit Sub
            End If
        
        Else
            If Right(txtDOT2, 3) <> "rpt" Then
                oMensajes.NoValido sCap(6)
                If Me.Visible Then txtDOT2.SetFocus
                Exit Sub
            End If
            
            If Not oFos.FileExists(txtDOT2) Then
                oMensajes.NoValido sCap(6)
                If Me.Visible Then txtDOT2.SetFocus
                Exit Sub
            End If
        
        End If
    End If

    If frmRESREU.m_oReuSeleccionada Is Nothing Then
        oMensajes.NoValido sCap(7)
        Exit Sub
    End If
         
    Me.Hide
    
    Screen.MousePointer = vbHourglass
    frmESPERA.lblGeneral = sCap(8)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sCap(9)
    
    'Funcion para Crear el acta en formato Word o en crystal
    If gParametrosInstalacion.giActaTipo = ActaWord Then
        CrearActaWord
    Else
        CrearActaCrystal
    End If
    Unload frmESPERA
    
Finalizar:
    On Error Resume Next
      
    Unload Me
    Exit Sub
    
    
ERROR:
    If err.Number = -2147190528 Then
        'subreport no existente
        Resume Next
    End If
    Screen.MousePointer = vbNormal
    
    Unload frmESPERA
    
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar

End Sub

''' <summary>
''' Crear el acta en formato Word
''' </summary>
''' <remarks>Llamada desde:cmdAceptar_Click;
''' Tiempo m�ximo: minutos en Gestamp pq objeto word es lento y sus reuniones son de muchos procesos</remarks>
Private Sub CrearActaWord()
    Dim appword As Object
    Dim docword As Object
    Dim blankword As Object
    Dim rangeword As Object
    Dim rangeword2 As Object
    Dim rangewordres As Object
    Dim oPers As CPersonas
    Dim oPer As CPersona
    Dim iNumProce As Long
    Dim iNumProcesos As Long
    Dim oBook As Object
    Dim oGrupo As CGrupo
    Dim oProve As CProveedor
    Dim oIAdj As IAdjudicaciones
    Dim bMostrar As Boolean
    Dim dblAdjudicadoProc As Double
    Dim dblAhorradoProc As Double
    Dim dblAhorradoPorcenProc As Double
    Dim dblConsumidoProc As Double
    Dim dblAdjudicado As Double
    Dim dblAhorrado As Double
    Dim dblAhorradoPorcen As Double
    Dim dblConsumido As Double
    Dim sCod As String
    Dim oIOfertas As iOfertas
    Dim bMostrarGr As Boolean
    Dim sComentario As String
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim oIAsigs As IAsignaciones
    Dim oIPersAsig As IPersonasAsig
    Dim Comentario As Variant
    Dim ayProxReu As Variant
    Dim bError As Boolean
    Dim i As Integer
    Dim iTiempo As Long 'para ir sumando el tiempo entre reuniones
    Dim sUON As String
    Dim oUON1 As CUnidadesOrgNivel1
    Dim oUON2 As CUnidadesOrgNivel2
    Dim oUON3 As CUnidadesOrgNivel3
    
    On Error GoTo Error_2:

    'Crear aplicaci�n word
    Set appword = CreateObject("Word.Application")
    
    frmESPERA.ProgressBar1.Value = 2

    If optTipo1.Value = True Then
        frmESPERA.lblDetalle = sCap(10) & " " & txtDOT.Text
        Set docword = appword.Documents.Add(txtDOT.Text)
    Else
        frmESPERA.lblDetalle = sCap(10) & " " & txtDOT2.Text
        Set docword = appword.Documents.Add(txtDOT2.Text)
    End If

    If docword.Bookmarks.Exists("PROCESO") = False Then
        oMensajes.NoValido sCap(6)
        If optTipo1.Value = True Then
            If Me.Visible Then txtDOT.SetFocus
        Else
            If Me.Visible Then txtDOT2.SetFocus
        End If
        Screen.MousePointer = vbNormal
        Unload frmESPERA
        docword.Close (False)
        Set docword = Nothing
        Set appword = Nothing
        Exit Sub
    End If
    
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    oUnidades.CargarTodasLasUnidades

    'INumProve = 0
    'ReDim ayObs(0)
    'ReDim ayProv(0)

    'Crear documento en blanco con la configuracion de p�gina que la plantilla
    ' CREAR NUEVO DOCUMENTO A PARTIR DE PLANTILLA 8/6/2000
    If optTipo1.Value = True Then
        Set blankword = appword.Documents.Add(txtDOT.Text)
    Else
        Set blankword = appword.Documents.Add(txtDOT2.Text)
    End If
    
    frmRESREU.m_oReuSeleccionada.CargarProcesos TipoOrdenacionDatosPlanifReu.OrdPorHoraProceso, False
    iNumProce = 0
    iNumProcesos = frmRESREU.m_oReuSeleccionada.Procesos.Count
    
    With blankword
        DatoAWord blankword, "FEC_REUNION", Format(frmRESREU.sdbcFecReu.Columns(0).Value, "short date")
        If .Bookmarks.Exists("HORA_INI_REU") Then
            DatoAWord blankword, "HORA_INI_REU", Format(frmRESREU.sdbcFecReu.Columns(0).Value, "Short Time")
        End If
        
        If .Bookmarks.Exists("HORA_FIN_REU") Then
            'Hora del ultimo proceso a�adiendole el tiempo dedicado por proceso
            i = 1
            iTiempo = 0
            While i < iNumProcesos
                iTiempo = iTiempo + DateDiff("s", frmRESREU.m_oReuSeleccionada.Procesos.Item(i).HoraEnReunion, frmRESREU.m_oReuSeleccionada.Procesos.Item(i + 1).HoraEnReunion)
                i = i + 1
            Wend
            DatoAWord blankword, "HORA_FIN_REU", "- " & Format(DateAdd("S", iTiempo, Format(frmRESREU.sdbcFecReu.Columns(0).Value, "Short Time")), "short time")
        End If
        
        DatoAWord blankword, "REF_REUNION", frmRESREU.lblref
        
        'Asistentes fijos a reuniones:
        If docword.Bookmarks.Exists("ASISTENTE") Then
            Set oPers = oFSGSRaiz.Generar_CPersonas
            oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorCod
            If oPers.Count = 0 Then
                .Bookmarks("ASISTENTES").Range.Delete
            Else
                .Bookmarks("ASISTENTE").Select
                .Application.Selection.Delete
                Set rangeword = docword.Bookmarks("ASISTENTE").Range
                rangeword.Copy
 
                For Each oPer In oPers
                    .Application.Selection.Paste
                    If .Bookmarks.Exists("COD_ASIST") Then
                        .Bookmarks("COD_ASIST").Range.Text = oPer.Cod
                    End If
                    If .Bookmarks.Exists("NOM_ASIST") Then
                        .Bookmarks("NOM_ASIST").Range.Text = NullToStr(oPer.nombre)
                    End If
                    If .Bookmarks.Exists("APE_ASIST") Then
                        .Bookmarks("APE_ASIST").Range.Text = NullToStr(oPer.Apellidos)
                    End If
                    If .Bookmarks.Exists("UO_ASIST") Then
                        .Bookmarks("UO_ASIST").Range.Text = NullToStr(oPer.UON1) & "-" & NullToStr(oPer.UON2) & "-" & NullToStr(oPer.UON3)
                    End If
                    If .Bookmarks.Exists("DEP_ASIST") Then
                        .Bookmarks("DEP_ASIST").Range.Text = NullToStr(oPer.CodDep)
                    End If
                    If .Bookmarks.Exists("CARGO_ASIST") Then
                        .Bookmarks("CARGO_ASIST").Range.Text = NullToStr(oPer.Cargo)
                    End If
                    If .Bookmarks.Exists("TFNO_ASIST") Then
                        .Bookmarks("TFNO_ASIST").Range.Text = NullToStr(oPer.Tfno)
                    End If
                    If .Bookmarks.Exists("TFNO2_ASIST") Then
                        .Bookmarks("TFNO2_ASIST").Range.Text = NullToStr(oPer.Tfno2)
                    End If
                    If .Bookmarks.Exists("FAX_ASIST") Then
                        .Bookmarks("FAX_ASIST").Range.Text = NullToStr(oPer.Fax)
                    End If
                    If .Bookmarks.Exists("MAIL_ASIST") Then
                        .Bookmarks("MAIL_ASIST").Range.Text = NullToStr(oPer.mail)
                    End If
                Next
                .Bookmarks("ASISTENTE").Delete
                .Bookmarks("ASISTENTES").Delete
            End If
        End If
        
        .Bookmarks("PROCESO").Select
        .Application.Selection.Delete
        Set rangeword = docword.Bookmarks("PROCESO").Range
        
'        frmRESREU.m_oReuSeleccionada.CargarProcesos TipoOrdenacionDatosPlanifReu.OrdPorHoraProceso, False
'        iNumProce = 0
'        iNumProcesos = frmRESREU.m_oReuSeleccionada.Procesos.Count
        
        
        For Each oProce In frmRESREU.m_oReuSeleccionada.Procesos
            For Each oBook In .Bookmarks
                oBook.Delete
            Next
            iNumProce = iNumProce + 1
            
            m_bCargadoGen = False
            
            oProce.CargarDatosGeneralesProceso
            
            'Sacamos la linea del proceso
            
            frmESPERA.lblDetalle = sCap(11)
            frmESPERA.lblContacto = sCap(12) & " " & CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod) & " " & oProce.Den
            frmESPERA.ProgressBar2.Value = (iNumProce / iNumProcesos) * 100
             DoEvents
            rangeword.Copy 'el bloque que contiene todos los datos del proceso.
            .Application.Selection.Paste
            
            If .Bookmarks.Exists("NUM_PROCESO") Then
                .Bookmarks("NUM_PROCESO").Range.Text = iNumProce
            End If
            If .Bookmarks.Exists("COD_PROCESO") Then
                .Bookmarks("COD_PROCESO").Range.Text = CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
            End If
            If .Bookmarks.Exists("DEN_PROCESO") Then
                .Bookmarks("DEN_PROCESO").Range.Text = oProce.Den
            End If
            
            sUON = ""
            If .Bookmarks.Exists("UON_COD_PROCESO") Then
                
                If oProce.UON1_Aper <> "" Then
                    sUON = oProce.UON1_Aper
                    If oProce.uon2_aper <> "" Then
                        sUON = sUON & "-" & oProce.uon2_aper
                        If oProce.UON3_Aper <> "" Then
                            sUON = sUON & "-" & oProce.UON3_Aper
                            Set oUON3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
                            .Bookmarks("UON_DEN_PROCESO").Range.Text = "-" & oUON3.DevolverDenominacion(oProce.UON1_Aper, oProce.uon2_aper, oProce.UON3_Aper)
                        Else
                            Set oUON2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
                            .Bookmarks("UON_DEN_PROCESO").Range.Text = "-" & oUON2.DevolverDenominacion(oProce.UON1_Aper, oProce.uon2_aper)
                        End If
                    Else
                        Set oUON1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
                        .Bookmarks("UON_DEN_PROCESO").Range.Text = "-" & oUON1.DevolverDenominacion(oProce.UON1_Aper)
                    End If
                Else
                    sUON = ""
                    .Bookmarks("UON_DEN_PROCESO").Range.Text = ""
                End If
                .Bookmarks("UON_COD_PROCESO").Range.Text = sUON
            End If
            
            If .Bookmarks.Exists("VOL_PROCESO") Then
                .Bookmarks("VOL_PROCESO").Range.Text = NullToDbl0(oProce.PresTotal)
                .Bookmarks("MON_PROCESO").Range.Text = oProce.MonCod
            End If
            If .Bookmarks.Exists("COMENT_PROCESO") Then
                .Bookmarks("COMENT_PROCESO").Range.Text = NullToStr(oProce.esp)
            End If
            
            If .Bookmarks.Exists("MAT1_PROCESO") Then
                .Bookmarks("MAT1_PROCESO").Range.Text = oProce.MaterialProce.Item(1).GMN1Cod
            End If
            If .Bookmarks.Exists("DMAT1_PROCESO") Then
                .Bookmarks("DMAT1_PROCESO").Range.Text = NullToStr(oProce.MaterialProce.Item(1).GMN1Den)
            End If
            If .Bookmarks.Exists("MAT2_PROCESO") Then
                .Bookmarks("MAT2_PROCESO").Range.Text = oProce.MaterialProce.Item(1).GMN2Cod
            End If
            If .Bookmarks.Exists("DMAT2_PROCESO") Then
                .Bookmarks("DMAT2_PROCESO").Range.Text = NullToStr(oProce.MaterialProce.Item(1).GMN2Den)
            End If
            If .Bookmarks.Exists("MAT3_PROCESO") Then
                .Bookmarks("MAT3_PROCESO").Range.Text = oProce.MaterialProce.Item(1).GMN3Cod
            End If
            If .Bookmarks.Exists("DMAT3_PROCESO") Then
                .Bookmarks("DMAT3_PROCESO").Range.Text = NullToStr(oProce.MaterialProce.Item(1).GMN3Den)
            End If
            If .Bookmarks.Exists("MAT4_PROCESO") Then
                .Bookmarks("MAT4_PROCESO").Range.Text = oProce.MaterialProce.Item(1).Cod
            End If
            If .Bookmarks.Exists("DMAT4_PROCESO") Then
                .Bookmarks("DMAT4_PROCESO").Range.Text = NullToStr(oProce.MaterialProce.Item(1).Den)
            End If
            
            If .Bookmarks.Exists("TIPO_REUNION") Then
                If oProce.Reudec Then
                    .Bookmarks("TIPO_REUNION").Range.Text = sCap(13)
                Else
                    .Bookmarks("TIPO_REUNION").Range.Text = sCap(14)
                End If
            End If
            
            If .Bookmarks.Exists("MONEDA") Then
                .Bookmarks("MONEDA").Range.Text = oProce.MonCod
            End If
                
            If oProce.DefDestino = EnProceso Then
                If .Bookmarks.Exists("COD_DEST_PROCE") Then
                    .Bookmarks("COD_DEST_PROCE").Range.Text = NullToStr(oProce.DestCod)
                End If
                If .Bookmarks.Exists("DEN_DEST_PROCE") Then
                    .Bookmarks("DEN_DEST_PROCE").Range.Text = NullToStr(oDestinos.Item(oProce.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                End If
                .Bookmarks("DEST_PROCE").Delete
            Else
                If .Bookmarks.Exists("DEST_PROCE") Then
                    .Bookmarks("DEST_PROCE").Select
                    .Application.Selection.Delete
                End If
            End If
            
            If oProce.DefFechasSum = EnProceso Then
                If .Bookmarks.Exists("FINI_SUM_PROCE") Then
                    .Bookmarks("FINI_SUM_PROCE").Range.Text = Format(NullToStr(oProce.FechaInicioSuministro), "Short Date")
                End If
                If .Bookmarks.Exists("FFIN_SUM_PROCE") Then
                    .Bookmarks("FFIN_SUM_PROCE").Range.Text = Format(NullToStr(oProce.FechaFinSuministro), "Short Date")
                End If
                If .Bookmarks.Exists("FECSUM_PROCE") Then
                    .Bookmarks("FECSUM_PROCE").Delete
                End If
            Else
                If .Bookmarks.Exists("FECSUM_PROCE") Then
                    .Bookmarks("FECSUM_PROCE").Select
                    .Application.Selection.cut
                End If
            End If
                
            If oProce.DefFormaPago = EnProceso Then
                If .Bookmarks.Exists("COD_PAGO_PROCE") Then
                    .Bookmarks("COD_PAGO_PROCE").Range.Text = NullToStr(oProce.PagCod)
                End If
                If .Bookmarks.Exists("DEN_PAGO_PROCE") Then
                    .Bookmarks("DEN_PAGO_PROCE").Range.Text = NullToStr(oPagos.Item(oProce.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                End If
                If .Bookmarks.Exists("PAGO_PROCE") Then
                    .Bookmarks("PAGO_PROCE").Delete
                End If
            Else
                If .Bookmarks.Exists("PAGO_PROCE") Then
                    .Bookmarks("PAGO_PROCE").Select
                    .Application.Selection.cut
                End If
            End If
                
            '@@@@ ESTE BOOKMARK NO EST� EN LA PLANTILLA.
            If .Bookmarks.Exists("CAMBIO") Then
                .Bookmarks("CAMBIO").Range.Text = FormateoNumerico(oProce.Cambio, m_sFormatoNumberProce)
            End If
            
            
            If oProce.responsable Is Nothing Then
                If docword.Bookmarks.Exists("OPC_RESPONSABLE") Then
                    .Bookmarks("OPC_RESPONSABLE").Range.Delete
                Else
                    If .Bookmarks.Exists("COD_RESPONSABLE") Then
                        .Bookmarks("COD_RESPONSABLE").Range.Text = ""
                    End If
                    If .Bookmarks.Exists("NOM_RESPONSABLE") Then
                        .Bookmarks("NOM_RESPONSABLE").Range.Text = ""
                    End If
                    If .Bookmarks.Exists("APE_RESPONSABLE") Then
                        .Bookmarks("APE_RESPONSABLE").Range.Text = ""
                    End If
                    If .Bookmarks.Exists("DEQP_RESPONSABLE") Then
                        .Bookmarks("DEQP_RESPONSABLE").Range.Text = ""
                    End If
                    If .Bookmarks.Exists("EQP_RESPONSABLE") Then
                        .Bookmarks("EQP_RESPONSABLE").Range.Text = ""
                    End If
                    If .Bookmarks.Exists("TLFNO_RESPONSABLE") Then
                        .Bookmarks("TLFNO_RESPONSABLE").Range.Text = ""
                    End If
                    If .Bookmarks.Exists("MAIL_RESPONSABLE") Then
                        .Bookmarks("MAIL_RESPONSABLE").Range.Text = ""
                    End If
                    If .Bookmarks.Exists("FAX_RESPONSABLE") Then
                        .Bookmarks("FAX_RESPONSABLE").Range.Text = ""
                    End If
                End If
            Else
                If .Bookmarks.Exists("COD_RESPONSABLE") Then
                    .Bookmarks("COD_RESPONSABLE").Range.Text = oProce.responsable.Cod
                End If
                If .Bookmarks.Exists("NOM_RESPONSABLE") Then
                    .Bookmarks("NOM_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.nombre)
                End If
                If .Bookmarks.Exists("APE_RESPONSABLE") Then
                    .Bookmarks("APE_RESPONSABLE").Range.Text = oProce.responsable.Apel
                End If
                If .Bookmarks.Exists("DEQP_RESPONSABLE") Then
                    .Bookmarks("DEQP_RESPONSABLE").Range.Text = oProce.responsable.DenEqp
                End If
                If .Bookmarks.Exists("EQP_RESPONSABLE") Then
                    .Bookmarks("EQP_RESPONSABLE").Range.Text = oProce.responsable.codEqp
                End If
                If .Bookmarks.Exists("TLFNO_RESPONSABLE") Then
                    .Bookmarks("TLFNO_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.Tfno)
                End If
                If .Bookmarks.Exists("MAIL_RESPONSABLE") Then
                    .Bookmarks("MAIL_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.mail)
                End If
                If .Bookmarks.Exists("FAX_RESPONSABLE") Then
                    .Bookmarks("FAX_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.Fax)
                End If
                .Bookmarks("OPC_RESPONSABLE").Delete
            End If

            If docword.Bookmarks.Exists("IMPLICADO") Then
                Set oIPersAsig = oProce
                Set oPers = oIPersAsig.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorCod, True)
            
                If oPers.Count = 0 Then
                    .Bookmarks("IMPLICADOS").Range.Delete
                Else
                    .Bookmarks("IMPLICADO").Select
                    .Application.Selection.Delete
                    Set rangeword2 = docword.Bookmarks("IMPLICADO").Range
                    rangeword2.Copy
                    For Each oPer In oPers
                        .Application.Selection.Paste
                        If .Bookmarks.Exists("COD_IMPLICADO") Then
                            .Bookmarks("COD_IMPLICADO").Range.Text = oPer.Cod
                        End If
                        If .Bookmarks.Exists("NOM_IMPLICADO") Then
                            .Bookmarks("NOM_IMPLICADO").Range.Text = NullToStr(oPer.nombre)
                        End If
                        If .Bookmarks.Exists("APE_IMPLICADO") Then
                            .Bookmarks("APE_IMPLICADO").Range.Text = oPer.Apellidos
                        End If
                        If .Bookmarks.Exists("UO1_IMPLICADO") Then
                            .Bookmarks("UO1_IMPLICADO").Range.Text = NullToStr(oPer.UON1)
                        End If
                        If .Bookmarks.Exists("UO2_IMPLICADO") Then
                            .Bookmarks("UO2_IMPLICADO").Range.Text = NullToStr(oPer.UON2)
                        End If
                        If .Bookmarks.Exists("UO3_IMPLICADO") Then
                            .Bookmarks("UO3_IMPLICADO").Range.Text = NullToStr(oPer.UON3)
                        End If
                        If .Bookmarks.Exists("CARGO_IMPLICADO") Then
                            .Bookmarks("CARGO_IMPLICADO").Range.Text = NullToStr(oPer.Cargo)
                        End If
                        If .Bookmarks.Exists("ROL_IMPLICADO") Then
                            .Bookmarks("ROL_IMPLICADO").Range.Text = oPer.Rol
                        End If
                        If .Bookmarks.Exists("TLFNO_IMPLICADO") Then
                            .Bookmarks("TLFNO_IMPLICADO").Range.Text = NullToStr(oPer.Tfno)
                        End If
                        If .Bookmarks.Exists("FAX_IMPLICADO") Then
                            .Bookmarks("FAX_IMPLICADO").Range.Text = NullToStr(oPer.Fax)
                        End If
                        If .Bookmarks.Exists("MAIL_IMPLICADO") Then
                            .Bookmarks("MAIL_IMPLICADO").Range.Text = NullToStr(oPer.mail)
                        End If
                        If .Bookmarks.Exists("DPTO_IMPLICADO") Then
                            .Bookmarks("DPTO_IMPLICADO").Range.Text = NullToStr(oPer.CodDep)
                        End If
                    Next
                    .Bookmarks("IMPLICADO").Delete
                    .Bookmarks("IMPLICADOS").Delete
                End If
            End If
            
            ' Comentario
            frmESPERA.ProgressBar1.Value = 3
            frmESPERA.lblContacto = sCap(15)
            

            
            If docword.Bookmarks.Exists("COMENT") Then
                Comentario = frmRESREU.m_oReuSeleccionada.DevolverComentario(oProce.Anyo, oProce.GMN1Cod, oProce.Cod)
                If IsNull(Comentario) Then
                    If docword.Bookmarks.Exists("COMENT") Then
                        .Bookmarks("OPC_COMENTARIO").Range.Delete
                    Else
                        .Bookmarks("COMENT").Range.Text = ""
                    End If
                Else
                    .Bookmarks("COMENT").Range.Text = NullToStr(Comentario)
                    .Bookmarks("OPC_COMENTARIO").Delete
                End If
            End If
    
            ' Pr�xima reuni�n
            If docword.Bookmarks.Exists("FEC_PROXIMAR") Then
                ayProxReu = oGestorReuniones.DevolverFechaProximaReunion(oProce.Anyo, oProce.GMN1Cod, oProce.Cod, CDate(frmRESREU.sdbcFecReu.Columns(0).Value))
                If Not IsNull(ayProxReu(0)) Then
                    frmESPERA.ProgressBar1.Value = 4
                    frmESPERA.lblContacto = sCap(16)
                    .Bookmarks("FEC_PROXIMAR").Range.Text = NullToStr(ayProxReu(0))
                    If ayProxReu(1) Then
                        .Bookmarks("TIPO_PROXIMAR").Range.Text = " " & sCap(13)
                    Else
                        .Bookmarks("TIPO_PROXIMAR").Range.Text = " " & sCap(14)
                    End If
                    .Bookmarks("OPC_PROXIMA_REUNION").Delete
                Else
                    If docword.Bookmarks.Exists("OPC_PROXIMA_REUNION") Then
                        .Bookmarks("OPC_PROXIMA_REUNION").Range.Delete
                    Else
                        .Bookmarks("FEC_PROXIMAR").Range.Text = ""
                        .Bookmarks("TIPO_PROXIMAR").Range.Text = ""
                    End If
                End If
            End If
    
            'Carga las adjudicaciones para las �ltimas ofertas
            m_bCargadoGen = True
            Set oAdjsGrupo = oFSGSRaiz.Generar_CAdjsGrupo
            oAdjsGrupo.CargarAdjudicaciones oProce, , basPublic.gParametrosInstalacion.gIdioma, frmRESREU.sdbcFecReu.Columns(0).Value
            
            'Carga las �ltimas ofertas para el proceso
            Set oIOfertas = oProce
            oIOfertas.DevolverUltimasOfertasDelProceso basPublic.gParametrosInstalacion.gIdioma
            oProce.CargarUltimasOfertasGrupos basPublic.gParametrosInstalacion.gIdioma
            
            'Carga los atributos aplicables al precio
            oProce.CargarAtributosAplicables
            
            'Carga de las adjudicaciones para el tema de los proves-grupos
            If gParametrosGenerales.gbProveGrupos Then
                Set oIAsigs = oProce
                Set m_oAsigs = oIAsigs.DevolverAsignaciones(OrdAsigPorCodProve, , , , , True)
                Set oIAsigs = Nothing
            End If
            
            If Not oProce.Atributos Is Nothing Then
                If oProce.Atributos.Count > 0 Then
                    oProce.Ofertas.CargarAtributosProcesoOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, frmRESREU.sdbcFecReu.Columns(0).Value 'de proceso
                    oProce.Ofertas.CargarAtributosGrupoOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, frmRESREU.sdbcFecReu.Columns(0).Value  'de grupo
                End If
            End If
            
            'RESUMEN ECON�MICO:
            dblAdjudicadoProc = 0
            dblAhorradoProc = 0
            dblAhorradoPorcenProc = 0
            dblConsumidoProc = 0
            
            Set oIAdj = oProce
            Set oProves = oIAdj.DevolverProveedoresCompConAdjudicaciones(CDate(frmRESREU.sdbcFecReu.Columns(0).Value))
    
            If docword.Bookmarks.Exists("OPT_RESUMEN") Then
                If docword.Bookmarks.Exists("RESUMEN_GR") Then
                    'OBTIENE PROVEEDORES ADJUDICADOS Y DATOS DE RES�MENES:
                                
                    If oProves.Count > 0 Then
                    
                        .Bookmarks("RESUMEN_GR").Select
                        .Application.Selection.Delete
                        Set rangewordres = docword.Bookmarks("RESUMEN_GR").Range
                        rangewordres.Copy
                        For Each oGrupo In oProce.Grupos
                            bMostrarGr = True
                            If oProce.AdminPublica = True Then
                                If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                    bMostrarGr = False
                                End If
                            End If
                
                            If bMostrarGr = True Then
                                .Application.Selection.Paste
                                dblAdjudicado = 0
                                dblAhorrado = 0
                                dblAhorradoPorcen = 0
                                dblConsumido = 0
                                'Calcula el resumen de grupos
                                For Each oProve In oProves
                                    bCargar = True
                                    If gParametrosGenerales.gbProveGrupos Then
                                        scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                        If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                            bCargar = False
                                        End If
                                    End If
                                    If bCargar Then
                                    
                                        bMostrar = True
                                        If oProce.AdminPublica = True Then
                                            If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                                bMostrar = False
                                            End If
                                        End If
                                        If bMostrar = True Then
                                            sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                        
                                            If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                                dblAdjudicadoProc = dblAdjudicadoProc + (NullToDbl0(oAdjsGrupo.Item(sCod).Adjudicado) / oAdjsGrupo.Item(sCod).Cambio)
                                                dblConsumidoProc = dblConsumidoProc + (NullToDbl0(oAdjsGrupo.Item(sCod).Consumido) / oAdjsGrupo.Item(sCod).Cambio)
                                                
                                                dblAdjudicado = dblAdjudicado + (NullToDbl0(oAdjsGrupo.Item(sCod).Adjudicado) / oAdjsGrupo.Item(sCod).Cambio)
                                                dblConsumido = dblConsumido + (NullToDbl0(oAdjsGrupo.Item(sCod).Consumido) / oAdjsGrupo.Item(sCod).Cambio)
                                                dblAhorrado = dblAhorrado + (NullToDbl0(oAdjsGrupo.Item(sCod).Ahorrado) / oAdjsGrupo.Item(sCod).Cambio)
                                            End If
                                        End If
                                    End If
                                Next
                            
                                If dblConsumido = 0 Then
                                    dblAhorradoPorcen = 0
                                Else
                                    dblAhorradoPorcen = (dblAhorrado / dblConsumido) * 100
                                End If
                            
                                If .Bookmarks.Exists("GR_COD_RES") Then
                                    .Bookmarks("GR_COD_RES").Range.Text = oGrupo.Codigo
                                End If
                                If .Bookmarks.Exists("GR_DEN_RES") Then
                                    .Bookmarks("GR_DEN_RES").Range.Text = oGrupo.Den
                                End If
                                If .Bookmarks.Exists("PRES_GR") Then
                                    .Bookmarks("PRES_GR").Range.Text = FormateoNumerico(dblConsumido, m_sFormatoNumberProce)
                                End If
                                If .Bookmarks.Exists("ADJ_GR") Then
                                    sComentario = ""
                                    sComentario = RPT_FormulaDeAtributosDeGrupoProveedor(oGrupo.Codigo)
                                    If sComentario <> "" Then
                                        .Bookmarks("ADJ_GR").Range.Comments.Add Range:=.Bookmarks("ADJ_GR").Range, Text:=sComentario
                                    End If
                                    .Bookmarks("ADJ_GR").Range.Text = FormateoNumerico(dblAdjudicado, m_sFormatoNumberProce)
                                End If
                                If .Bookmarks.Exists("AHORRO_GR") Then
                                    .Bookmarks("AHORRO_GR").Range.Text = FormateoNumerico(dblAhorrado, m_sFormatoNumberProce)
                                End If
                                If .Bookmarks.Exists("PORCEN_GR_RES") Then
                                    .Bookmarks("PORCEN_GR_RES").Range.Text = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberProcePorcen) & "%"
                                End If
                            End If
                        Next
                        
                        dblAdjudicadoProc = AplicarAtributosDeProceso(dblAdjudicadoProc)
                        dblAhorradoProc = dblConsumidoProc - dblAdjudicadoProc
                        If dblConsumidoProc = 0 Then
                            dblAhorradoPorcenProc = 0
                        Else
                            dblAhorradoPorcenProc = (dblAhorradoProc / dblConsumidoProc) * 100
                        End If
                        
                        If .Bookmarks.Exists("PRES_GEN") Then
                            .Bookmarks("PRES_GEN").Range.Text = FormateoNumerico(dblConsumidoProc, m_sFormatoNumberProce)
                        End If
                        If .Bookmarks.Exists("ADJ_GEN") Then
                            sComentario = ""
                            sComentario = RPT_FormulaDeAtributosProcesoProveedor()
                            If sComentario <> "" Then
                                .Bookmarks("ADJ_GEN").Range.Select
                                .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                                .Application.Selection.TypeText Text:=sComentario
                                .Application.activewindow.ActivePane.Close
                            End If
                            .Bookmarks("ADJ_GEN").Range.Text = FormateoNumerico(dblAdjudicadoProc, m_sFormatoNumberProce)
                        End If
                        If .Bookmarks.Exists("AHORRO_GEN") Then
                            .Bookmarks("AHORRO_GEN").Range.Text = FormateoNumerico(dblAhorradoProc, m_sFormatoNumberProce)
                        End If
                        If .Bookmarks.Exists("PORCEN_GEN") Then
                            .Bookmarks("PORCEN_GEN").Range.Text = FormateoNumerico(dblAhorradoPorcenProc, m_sFormatoNumberProcePorcen) & "%"
                        End If
        
                        .Bookmarks("RESUMEN_GR").Delete
                        .Bookmarks("OPT_RESUMEN").Delete
                        
                    Else
                        .Bookmarks("OPT_RESUMEN").Range.Delete
                    End If
                End If
            End If
                
            'RESUMEN PROVEDORES ADJUDICADOS:
            If docword.Bookmarks.Exists("OPT_RESUMEN_PROV") Then
                If oProves.Count > 0 Then
                    If .Bookmarks.Exists("PRES_TOT") Then
                        .Bookmarks("PRES_TOT").Range.Text = FormateoNumerico(dblConsumidoProc, m_sFormatoNumberProce)
                    End If
                    If .Bookmarks.Exists("OFE_TOT") Then
                        sComentario = ""
                        sComentario = RPT_FormulaDeAtributosProcesoProveedor()
                        If sComentario <> "" Then
                            .Bookmarks("OFE_TOT").Range.Comments.Add Range:=.Bookmarks("OFE_TOT").Range, Text:=sComentario
                        End If
                        .Bookmarks("OFE_TOT").Range.Text = FormateoNumerico(dblAdjudicadoProc, m_sFormatoNumberProce)
                    End If
                    If .Bookmarks.Exists("AHORRO_TOT") Then
                        .Bookmarks("AHORRO_TOT").Range.Text = FormateoNumerico(dblAhorradoProc, m_sFormatoNumberProce)
                    End If
                    If .Bookmarks.Exists("PORCEN_TOT") Then
                        .Bookmarks("PORCEN_TOT").Range.Text = FormateoNumerico(dblAhorradoPorcenProc, m_sFormatoNumberProcePorcen) & "%"
                    End If
                    If docword.Bookmarks.Exists("RESUMEN_PROV") Then
                        .Bookmarks("RESUMEN_PROV").Select
                        .Application.Selection.Delete
                        Set rangewordres = docword.Bookmarks("RESUMEN_PROV").Range
                        rangewordres.Copy
                        For Each oProve In oProves
                            .Application.Selection.Paste
                            
                            dblAdjudicado = 0
                            dblAhorrado = 0
                            dblAhorradoPorcen = 0
                            dblConsumido = 0
                            
                            For Each oGrupo In oProce.Grupos
                                bCargar = True
                                If gParametrosGenerales.gbProveGrupos Then
                                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                    If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                        bCargar = False
                                    End If
                                End If
                                If bCargar Then
                            
                                    bMostrar = True
                                    If oProce.AdminPublica = True Then
                                        If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                            bMostrar = False
                                        End If
                                    End If
                                    If bMostrar = True Then
                                        sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                    
                                        If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                            dblAdjudicado = dblAdjudicado + (NullToDbl0(oAdjsGrupo.Item(sCod).Adjudicado) / oAdjsGrupo.Item(sCod).Cambio)
                                            dblConsumido = dblConsumido + (NullToDbl0(oAdjsGrupo.Item(sCod).Consumido) / oAdjsGrupo.Item(sCod).Cambio)
                                            dblAhorrado = dblAhorrado + (NullToDbl0(oAdjsGrupo.Item(sCod).Ahorrado) / oAdjsGrupo.Item(sCod).Cambio)
                                        End If
                                    End If
                                End If
                            Next
                            
                            dblAdjudicado = AplicarAtributosDeProceso(dblAdjudicado)
                            dblAhorrado = dblConsumido - dblAdjudicado
                            If dblConsumido = 0 Then
                                dblAhorradoPorcen = 0
                            Else
                                dblAhorradoPorcen = (dblAhorrado / dblConsumido) * 100
                            End If
                        
                            If .Bookmarks.Exists("PROV_COD_RES") Then
                                .Bookmarks("PROV_COD_RES").Range.Text = oProve.Cod
                            End If
                            If .Bookmarks.Exists("PROV_DEN_RES") Then
                                .Bookmarks("PROV_DEN_RES").Range.Text = oProve.Den
                            End If
                            If .Bookmarks.Exists("PRES_TOT_PROV") Then
                                .Bookmarks("PRES_TOT_PROV").Range.Text = FormateoNumerico(dblConsumido, m_sFormatoNumberProce)
                            End If
                            If .Bookmarks.Exists("OFE_TOT_PROV") Then
                                .Bookmarks("OFE_TOT_PROV").Range.Text = FormateoNumerico(dblAdjudicado, m_sFormatoNumberProce)
                            End If
                            If .Bookmarks.Exists("AHORRO_TOT_PROV") Then
                                .Bookmarks("AHORRO_TOT_PROV").Range.Text = FormateoNumerico(dblAhorrado, m_sFormatoNumberProce)
                            End If
                            If .Bookmarks.Exists("PORCEN_TOT_PROV") Then
                                .Bookmarks("PORCEN_TOT_PROV").Range.Text = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberProcePorcen) & "%"
                            End If
                        Next
                        .Bookmarks("RESUMEN_PROV").Delete
                        .Bookmarks("OPT_RESUMEN_PROV").Delete
                    End If
                Else
                    .Bookmarks("OPT_RESUMEN_PROV").Range.Delete
                End If
            End If
            
            'RESUMEN PROVEDORES NO ADJUDICADOS:
            If docword.Bookmarks.Exists("OPT_RESUMEN_PROV_NOADJ") Then
                If chkProvNoAdj.Value = vbChecked Then
                     If docword.Bookmarks.Exists("RESUMEN_PROV_NOAJ") Then
                        Set oProvesnoAdj = oIAdj.DevolverProveedoresSinAdjudicaciones(, , , CDate(frmRESREU.sdbcFecReu.Columns(0).Value))
                        If oProvesnoAdj.Count > 0 Then
                            .Bookmarks("RESUMEN_PROV_NOAJ").Select
                            .Application.Selection.Delete
                            Set rangewordres = docword.Bookmarks("RESUMEN_PROV_NOAJ").Range
                            rangewordres.Copy
            
                            For Each oProve In oProvesnoAdj
                                .Application.Selection.Paste
                                
                                dblConsumido = 0
                                dblAdjudicado = 0
                                dblAhorrado = 0
                                dblAhorradoPorcen = 0
                                
                                For Each oGrupo In oProce.Grupos
                                    bCargar = True
                                    If gParametrosGenerales.gbProveGrupos Then
                                        scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                        If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                            bCargar = False
                                        End If
                                    End If
                                    If bCargar Then
                                        bMostrar = True
                                        If oProce.AdminPublica = True Then
                                            If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                                bMostrar = False
                                            End If
                                        End If
                                        If bMostrar = True Then
                                            sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                        
                                            If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                                dblAdjudicado = dblAdjudicado + (NullToDbl0(oAdjsGrupo.Item(sCod).importe) / oAdjsGrupo.Item(sCod).Cambio)
                                                If oAdjsGrupo.Item(sCod).importe <> 0 Then
                                                    dblConsumido = dblConsumido + (NullToDbl0(oAdjsGrupo.Item(sCod).Abierto) / oAdjsGrupo.Item(sCod).Cambio)
                                                End If
                                                dblAhorrado = dblAhorrado + (NullToDbl0(oAdjsGrupo.Item(sCod).AhorroOfe) / oAdjsGrupo.Item(sCod).Cambio)
                                            End If
                                        End If
                                    End If
                                Next
                                
                                dblAdjudicado = AplicarAtributosDeProcesoNoAdj(dblAdjudicado, oProve.Cod)
                                dblAhorrado = dblConsumido - dblAdjudicado
                                If dblConsumido = 0 Then
                                    dblAhorradoPorcen = 0
                                Else
                                    dblAhorradoPorcen = (dblAhorrado / dblConsumido) * 100
                                End If
                                If .Bookmarks.Exists("PROV_COD_RES_NOADJ") Then
                                    .Bookmarks("PROV_COD_RES_NOADJ").Range.Text = oProve.Cod
                                End If
                                If .Bookmarks.Exists("PROV_DEN_RES_NOADJ") Then
                                    .Bookmarks("PROV_DEN_RES_NOADJ").Range.Text = oProve.Den
                                End If
                                
                                If .Bookmarks.Exists("PRES_TOT_PROV_NOADJ") Then
                                    .Bookmarks("PRES_TOT_PROV_NOADJ").Range.Text = FormateoNumerico(dblConsumido, m_sFormatoNumberProce)
                                End If
                                If .Bookmarks.Exists("OFE_TOT_PROV_NOADJ") Then
                                    sComentario = ""
                                    sComentario = RPT_FormulaDeAtributosProcesoProveedor()
                                    If sComentario <> "" Then
                                        .Bookmarks("OFE_TOT_PROV_NOADJ").Range.Comments.Add Range:=.Bookmarks("OFE_TOT_PROV_NOADJ").Range, Text:=sComentario
                                    End If
                                    .Bookmarks("OFE_TOT_PROV_NOADJ").Range.Text = FormateoNumerico(dblAdjudicado, m_sFormatoNumberProce)
                                End If
                                If .Bookmarks.Exists("AHORRO_TOT_PROV_NOADJ") Then
                                    .Bookmarks("AHORRO_TOT_PROV_NOADJ").Range.Text = FormateoNumerico(dblAhorrado, m_sFormatoNumberProce)
                                End If
                                If .Bookmarks.Exists("PORCEN_TOT_PROV_NOADJ") Then
                                    .Bookmarks("PORCEN_TOT_PROV_NOADJ").Range.Text = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberProcePorcen) & "%"
                                End If
                            Next
                            .Bookmarks("RESUMEN_PROV_NOAJ").Delete
                            .Bookmarks("OPT_RESUMEN_PROV_NOADJ").Delete
                        Else
                            .Bookmarks("OPT_RESUMEN_PROV_NOADJ").Range.Delete
                        End If
                    End If
                Else
                    .Bookmarks("OPT_RESUMEN_PROV_NOADJ").Range.Delete
                End If
            End If
            
            If oProves.Count = 0 Then
                If optTipo1.Value = True Then
                    .Bookmarks("GRUPO").Range.Delete
                Else
                    .Bookmarks("PROVEEDOR").Range.Delete
                    .Bookmarks("PROVEEDOR_NOADJ").Range.Delete
                End If
            Else
                If optTipo1.Value = True Then
                    'TIPO 1:
                    If TipoActa1(docword, blankword) = False Then GoTo Finalizar
                Else
                    'TIPO2:
                    If TipoActa2(docword, blankword) = False Then GoTo Finalizar
                End If
            End If
            
            .Bookmarks("ESPACIO").Select
            .Application.Selection.cut
       
       Next 'PROCESO
    
    End With
                    
    ' cerrar documento word
    docword.Close (False)
    appword.Visible = True
    Screen.MousePointer = vbNormal
    basSeguridad.RegistrarAccion AccionesSummit.ACCResReuObtenerActa, "Proceso: " & CStr(Format(frmRESREU.sdbcFecReu.Columns(0).Value, "Short Date"))



Finalizar:
    On Error Resume Next
    
    If bError Then
        docword.Close (False)
        blankword.Close (False)
        If appword.Visible = False Then
            appword.Quit
        End If
    End If
    
    
    Set docword = Nothing
    Set appword = Nothing
    Set blankword = Nothing
    Set rangeword = Nothing
    Set rangeword2 = Nothing
    Set rangewordres = Nothing
    Set oPers = Nothing
    Set oPer = Nothing
    Set oBook = Nothing
    Set oGrupo = Nothing
    Set oProve = Nothing
    Set oIAdj = Nothing
    Set oIOfertas = Nothing
    Set oIAsigs = Nothing
    Set oIPersAsig = Nothing
    Exit Sub
    
Error_2:
    
    'Si el error es que no encuentra un formato o bookmark continuamos
    If err.Number = 5834 Or err.Number = 5941 Or err.Number = 9 Or err.Number = 91 Or err.Number = 4605 Then
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    
    Unload frmESPERA
    
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar
    
End Sub

''' <summary>
''' Crear el acta en formato crystal
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click;
''' Tiempo m�ximo: minutos en Gestamp pq objeto word es lento y sus reuniones son de muchos procesos</remarks>
Private Sub CrearActaCrystal()
    Dim oPers As CPersonas
    Dim oPer As CPersona
    Dim iNumProce As Long
    Dim iNumProcesos As Long
    Dim oGrupo As CGrupo
    Dim oProve As CProveedor
    Dim oIAdj As IAdjudicaciones
    Dim bMostrar As Boolean
    Dim dblAdjudicadoProc As Double
    Dim dblAhorradoProc As Double
    Dim dblAhorradoPorcenProc As Double
    Dim dblConsumidoProc As Double
    Dim dblAdjudicado As Double
    Dim dblAhorrado As Double
    Dim dblAhorradoPorcen As Double
    Dim dblConsumido As Double
    Dim oIOfertas As iOfertas
    Dim bMostrarGr As Boolean
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim oIAsigs As IAsignaciones
    Dim oIPersAsig As IPersonasAsig
    Dim sCod As String
    Dim Comentario As Variant
    Dim ayProxReu As Variant
    Dim bError As Boolean
    Dim ADORs As ADODB.Recordset
    Dim adoAsist As ADODB.Recordset
    Dim adoAsistReu As ADODB.Recordset
    Dim adoResumen As ADODB.Recordset
    Dim adoProvAdj As ADODB.Recordset
    Dim adoProvNoAdj As ADODB.Recordset
    Dim adoProv As ADODB.Recordset
    Dim adoProvNoADJ2 As ADODB.Recordset
    Dim adoItems As ADODB.Recordset
    Dim RepPath As String
    Dim oReport As CRAXDRT.Report
    Dim SubListadoAsistentes As CRAXDRT.Report
    Dim subListadoResumen As CRAXDRT.Report
    Dim subListadoProvAdj As CRAXDRT.Report
    Dim subListadoProvNoAdj As CRAXDRT.Report
    Dim subListadoProveedorAdjudicado As CRAXDRT.Report
    Dim subListadoProveedorNoAdjudicado As CRAXDRT.Report
    Dim subListadoItems As CRAXDRT.Report
    Dim pv As Preview
    Dim i As Integer
    Dim iTiempo As Long
    Dim sUON As String
    Dim oUON1 As CUnidadesOrgNivel1
    Dim oUON2 As CUnidadesOrgNivel2
    Dim oUON3 As CUnidadesOrgNivel3
    
    On Error GoTo Error_2:
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    frmESPERA.ProgressBar1.Value = 2

    If optTipo1.Value = True Then
        frmESPERA.lblDetalle = sCap(10) & " " & txtDOT.Text
        RepPath = txtDOT.Text
    Else
        frmESPERA.lblDetalle = sCap(10) & " " & txtDOT2.Text
        RepPath = txtDOT2.Text
    End If
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
       
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    oUnidades.CargarTodasLasUnidades
    
    'Se crea un recordset dinamicamente
    Set ADORs = Nothing
    Set ADORs = New ADODB.Recordset
    
    ADORs.Fields.Append "FEC_REUNION", adVarChar, 25
    ADORs.Fields.Append "REF_REUNION", adVarChar, 100
    ADORs.Fields.Append "NUM_PROCESO", adInteger
    ADORs.Fields.Append "COD_PROCESO", adVarChar, 50
    ADORs.Fields.Append "DEN_PROCESO", adVarChar, 100
    ADORs.Fields.Append "TIPO_REUNION", adVarChar, 100
    ADORs.Fields.Append "MONEDA", adVarChar, 50
    ADORs.Fields.Append "COD_DEST_PROCE", adVarChar, 50
    ADORs.Fields.Append "DEN_DEST_PROCE", adVarChar, 100
    ADORs.Fields.Append "FINI_SUM_PROCE", adVarChar, 25
    ADORs.Fields.Append "FFIN_SUM_PROCE", adVarChar, 25
    ADORs.Fields.Append "COD_PAGO_PROCE", adVarChar, 50
    ADORs.Fields.Append "DEN_PAGO_PROCE", adVarChar, 100
    ADORs.Fields.Append "NOM_RESPONSABLE", adVarChar, 100
    ADORs.Fields.Append "APE_RESPONSABLE", adVarChar, 100
    ADORs.Fields.Append "FEC_PROXIMAR", adVarChar, 25
    ADORs.Fields.Append "TIPO_PROXIMAR", adVarChar, 100
    ADORs.Fields.Append "COMENT", adVarChar, 4000
    ADORs.Fields.Append "PRES_GEN", adDouble
    ADORs.Fields.Append "ADJ_GEN", adDouble
    ADORs.Fields.Append "AHORRO_GEN", adDouble
    ADORs.Fields.Append "PORCEN_GEN", adVarChar, 25
    ADORs.Fields.Append "HORA_INI_REU", adVarChar, 10
    ADORs.Fields.Append "HORA_FIN_REU", adVarChar, 10
    ADORs.Fields.Append "UON_COD_PROCESO", adVarChar, 25
    ADORs.Fields.Append "UON_DEN_PROCESO", adVarChar, 100
    ADORs.Fields.Append "VOL_PROCESO", adDouble
    ADORs.Fields.Append "MON_PROCESO", adVarChar, 50
    ADORs.Open
    
    '''''SUBREPORT ASISTENTES
    
    Set oPers = oFSGSRaiz.Generar_CPersonas
    oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorCod
    If oPers.Count > 0 Then
        Set SubListadoAsistentes = oReport.OpenSubreport("Asistentes")
        
        If Not SubListadoAsistentes Is Nothing Then
             Set adoAsist = New ADODB.Recordset
             
             adoAsist.Fields.Append "COD_ASIST", adVarChar, 50
             adoAsist.Fields.Append "NOM_ASIST", adVarChar, 100
             adoAsist.Fields.Append "APE_ASIST", adVarChar, 100
             adoAsist.Fields.Append "UO_ASIST", adVarChar, 50
             adoAsist.Fields.Append "DEP_ASIST", adVarChar, 100
             adoAsist.Fields.Append "CARGO_ASIST", adVarChar, 100
             adoAsist.Fields.Append "TFNO_ASIST", adVarChar, 20
             adoAsist.Fields.Append "TFNO2_ASIST", adVarChar, 20
             adoAsist.Fields.Append "FAX_ASIST", adVarChar, 20
             adoAsist.Fields.Append "MAIL_ASIST", adVarChar, 100
             adoAsist.Open
            
             For Each oPer In oPers
                 adoAsist.AddNew
                 adoAsist("COD_ASIST").Value = oPer.Cod
                 adoAsist("NOM_ASIST").Value = NullToStr(oPer.nombre)
                 adoAsist("APE_ASIST").Value = NullToStr(oPer.Apellidos)
                 adoAsist("UO_ASIST").Value = NullToStr(oPer.UON1) & "-" & NullToStr(oPer.UON2) & "-" & NullToStr(oPer.UON3)
                 adoAsist("DEP_ASIST").Value = NullToStr(oPer.CodDep)
                 adoAsist("CARGO_ASIST").Value = NullToStr(oPer.Cargo)
                 adoAsist("TFNO_ASIST").Value = IIf(NullToStr(oPer.Tfno) = "", " ", NullToStr(oPer.Tfno))
                 adoAsist("TFNO2_ASIST").Value = IIf(NullToStr(oPer.Tfno2) = "", " ", NullToStr(oPer.Tfno2))
                 adoAsist("FAX_ASIST").Value = IIf(NullToStr(oPer.Fax) = "", " ", NullToStr(oPer.Fax))
                 adoAsist("MAIL_ASIST").Value = IIf(NullToStr(oPer.mail) = "", " ", NullToStr(oPer.mail))
             Next
        End If
    End If
        
    'SUBREPORT ASISTENTES
    If Not SubListadoAsistentes Is Nothing Then
        'Asistentes fijos a reuniones
        If Not adoAsist Is Nothing Then
            SubListadoAsistentes.Database.SetDataSource adoAsist
        Else
            oMensajes.ImposibleMostrarInforme
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    Set SubListadoAsistentes = oReport.OpenSubreport("AsistentesReunion")
    If Not SubListadoAsistentes Is Nothing Then
        Set adoAsistReu = New ADODB.Recordset
        adoAsistReu.Fields.Append "NOM_IMPLICADO", adVarChar, 100
        adoAsistReu.Fields.Append "APE_IMPLICADO", adVarChar, 100
        adoAsistReu.Fields.Append "NUM_PROCESO", adInteger
        adoAsistReu.Open
    End If
    
    frmRESREU.m_oReuSeleccionada.CargarProcesos TipoOrdenacionDatosPlanifReu.OrdPorHoraProceso, False
    iNumProce = 0
    iNumProcesos = frmRESREU.m_oReuSeleccionada.Procesos.Count
    
    Set subListadoResumen = oReport.OpenSubreport("ResumenEconomico")
    If Not subListadoResumen Is Nothing Then
        Set adoResumen = New ADODB.Recordset
        adoResumen.Fields.Append "COD_PROCESO", adVarChar, 50
        adoResumen.Fields.Append "GR_COD_RES", adVarChar, 50
        adoResumen.Fields.Append "GR_DEN_RES", adVarChar, 100
        adoResumen.Fields.Append "PRES_GR", adDouble
        adoResumen.Fields.Append "ADJ_GR", adDouble
        adoResumen.Fields.Append "AHORRO_GR", adDouble
        adoResumen.Fields.Append "PORCEN_GR_RES", adVarChar, 25
        adoResumen.Fields.Append "ADJ_GEN", adDouble
        adoResumen.Fields.Append "PRES_GEN", adDouble
        adoResumen.Fields.Append "AHORRO_GEN", adDouble
        adoResumen.Fields.Append "PORCEN_GEN", adVarChar, 25
        adoResumen.Open
    End If
    
    'Proveedores Adjudicados
    Set subListadoProvAdj = oReport.OpenSubreport("Proveedores")
    If Not subListadoProvAdj Is Nothing Then
        Set adoProvAdj = New ADODB.Recordset
        adoProvAdj.Fields.Append "AHORRO_TOT", adDouble
        adoProvAdj.Fields.Append "AHORRO_TOT_PROV", adDouble
        adoProvAdj.Fields.Append "AHORRO_TOT_PROV_NOADJ", adDouble
        adoProvAdj.Fields.Append "OFE_TOT", adDouble
        adoProvAdj.Fields.Append "OFE_TOT_PROV", adDouble
        adoProvAdj.Fields.Append "OFE_TOT_PROV_NOADJ", adDouble
        adoProvAdj.Fields.Append "PORCEN_TOT", adVarChar, 25
        adoProvAdj.Fields.Append "PORCEN_TOT_PROV", adVarChar, 25
        adoProvAdj.Fields.Append "PORCEN_TOT_PROV_NOADJ", adVarChar, 25
        adoProvAdj.Fields.Append "PRES_TOT_PROV", adDouble
        adoProvAdj.Fields.Append "PRES_TOT_PROV_NOADJ", adDouble
        adoProvAdj.Fields.Append "PRES_TOT", adDouble
        adoProvAdj.Fields.Append "PROV_COD_RES", adVarChar, 50
        adoProvAdj.Fields.Append "PROV_DEN_RES", adVarChar, 100
        adoProvAdj.Fields.Append "PROV_COD_RES_NOADJ", adVarChar, 50
        adoProvAdj.Fields.Append "PROV_DEN_RES_NOADJ", adVarChar, 100
        adoProvAdj.Fields.Append "NUM_PROCESO", adInteger
        adoProvAdj.Open
    End If
    
    'Proveedores no adjudicados
    Set subListadoProvNoAdj = oReport.OpenSubreport("ProveedoresNoAdj")
    If Not subListadoProvNoAdj Is Nothing Then
        Set adoProvNoAdj = New ADODB.Recordset
        adoProvNoAdj.Fields.Append "PRES_TOT", adDouble
        adoProvNoAdj.Fields.Append "OFE_TOT", adDouble
        adoProvNoAdj.Fields.Append "AHORRO_TOT", adDouble
        adoProvNoAdj.Fields.Append "PORCEN_TOT", adVarChar, 25
        adoProvNoAdj.Fields.Append "PROV_COD_RES", adVarChar, 50
        adoProvNoAdj.Fields.Append "PROV_DEN_RES", adVarChar, 100
        adoProvNoAdj.Fields.Append "PRES_TOT_PROV", adDouble
        adoProvNoAdj.Fields.Append "OFE_TOT_PROV", adDouble
        adoProvNoAdj.Fields.Append "AHORRO_TOT_PROV", adDouble
        adoProvNoAdj.Fields.Append "PORCEN_TOT_PROV", adVarChar, 25
        adoProvNoAdj.Fields.Append "NUM_PROCESO", adInteger
        adoProvNoAdj.Fields.Append "AHORRO_TOT_PROV_NOADJ", adDouble
        adoProvNoAdj.Fields.Append "OFE_TOT_PROV_NOADJ", adDouble
        adoProvNoAdj.Fields.Append "PORCEN_TOT_PROV_NOADJ", adVarChar, 25
        adoProvNoAdj.Fields.Append "PRES_TOT_PROV_NOADJ", adDouble
        adoProvNoAdj.Fields.Append "PROV_COD_RES_NOADJ", adVarChar, 50
        adoProvNoAdj.Fields.Append "PROV_DEN_RES_NOADJ", adVarChar, 100
        adoProvNoAdj.Open
    End If
    
    'Proveedores adjudicados Acta tipo 2
    Set subListadoProveedorAdjudicado = oReport.OpenSubreport("ProveedorAdjudicado")
    If Not subListadoProveedorAdjudicado Is Nothing Then
        Set adoProv = New ADODB.Recordset
        adoProv.Fields.Append "COD_PROVEEDOR", adVarChar, 50
        adoProv.Fields.Append "NOM_PROVEEDOR", adVarChar, 100
        adoProv.Fields.Append "PRECIO_PROVEEDOR", adDouble
        adoProv.Fields.Append "ADJ_PROV", adDouble
        adoProv.Fields.Append "AHORRO_PROV", adDouble
        adoProv.Fields.Append "PORCEN_PROV", adVarChar, 25
        adoProv.Fields.Append "OBS_PROVEEDOR", adVarChar, 4000
        adoProv.Fields.Append "COD_GRUPO", adVarChar, 50
        adoProv.Fields.Append "DEN_GRUPO", adVarChar, 100
        adoProv.Fields.Append "COD_DEST_GR", adVarChar, 10
        adoProv.Fields.Append "DEN_DEST_GR", adVarChar, 100
        adoProv.Fields.Append "FINI_SUM_GR", adVarChar, 25
        adoProv.Fields.Append "FFIN_SUM_GR", adVarChar, 25
        adoProv.Fields.Append "COD_PAGO_GR", adVarChar, 50
        adoProv.Fields.Append "DEN_PAGO_GR", adVarChar, 100
        adoProv.Fields.Append "PRES_AHORR", adDouble
        adoProv.Fields.Append "ADJ_AHORR", adDouble
        adoProv.Fields.Append "AHORR_GR", adDouble
        adoProv.Fields.Append "PORCEN_GR", adVarChar, 25
        adoProv.Fields.Append "COD_ITEM", adVarChar, 50
        adoProv.Fields.Append "DEN_ITEM", adVarChar, 200
        adoProv.Fields.Append "COD_DEST_ITEM", adVarChar, 50
        adoProv.Fields.Append "DEN_DEST_ITEM", adVarChar, 100
        adoProv.Fields.Append "FINI_SUM_ITEM", adVarChar, 25
        adoProv.Fields.Append "FFIN_SUM_ITEM", adVarChar, 25
        adoProv.Fields.Append "CANTIDAD", adDouble
        adoProv.Fields.Append "COD_UNI", adVarChar, 50
        adoProv.Fields.Append "PREC_OFE", adDouble
        adoProv.Fields.Append "IMPORTE", adDouble
        adoProv.Fields.Append "AHORRO", adDouble
        adoProv.Fields.Append "AHORROPORCEN", adVarChar, 25
        adoProv.Fields.Append "COD_PROCESO", adVarChar, 50
        adoProv.Fields.Append "ID_ITEM", adDouble
        adoProv.Open
    End If
    
    Set subListadoProveedorNoAdjudicado = oReport.OpenSubreport("ProveedorNoAdjudicado")
    If Not subListadoProveedorNoAdjudicado Is Nothing Then
        Set adoProvNoADJ2 = New ADODB.Recordset
        adoProvNoADJ2.Fields.Append "COD_PROVEEDOR_NOADJ", adVarChar, 50
        adoProvNoADJ2.Fields.Append "NOM_PROVEEDOR_NOADJ", adVarChar, 100
        adoProvNoADJ2.Fields.Append "PRESUPUESTO_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "IMPORTE_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "AHORRO_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "PORCEN_NOADJ", adVarChar, 25
        adoProvNoADJ2.Fields.Append "OBS_PROVEEDOR_NOADJ", adVarChar, 4000
        adoProvNoADJ2.Fields.Append "COD_GRUPO_NOADJ", adVarChar, 50
        adoProvNoADJ2.Fields.Append "DEN_GRUPO_NOADJ", adVarChar, 100
        adoProvNoADJ2.Fields.Append "COD_DEST_GR_NOADJ", adVarChar, 50
        adoProvNoADJ2.Fields.Append "DEN_DEST_GR_NOADJ", adVarChar, 100
        adoProvNoADJ2.Fields.Append "FINI_SUM_GR_NOADJ", adVarChar, 25
        adoProvNoADJ2.Fields.Append "FFIN_SUM_GR_NOADJ", adVarChar, 25
        adoProvNoADJ2.Fields.Append "COD_PAGO_GR_NOADJ", adVarChar, 50
        adoProvNoADJ2.Fields.Append "DEN_PAGO_GR_NOADJ", adVarChar, 100
        adoProvNoADJ2.Fields.Append "PRES_AHORR_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "IMPORTE_OFERTA_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "AHORR_GR_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "PORCEN_GR_NOADJ", adVarChar, 25
        adoProvNoADJ2.Fields.Append "COD_ITEM_NOADJ", adVarChar, 50
        adoProvNoADJ2.Fields.Append "DEN_ITEM_NOADJ", adVarChar, 200
        adoProvNoADJ2.Fields.Append "COD_DEST_ITEM_NOADJ", adVarChar, 50
        adoProvNoADJ2.Fields.Append "DEN_DEST_ITEM_NOADJ", adVarChar, 100
        adoProvNoADJ2.Fields.Append "FINI_SUM_ITEM_NOADJ", adVarChar, 25
        adoProvNoADJ2.Fields.Append "FFIN_SUM_ITEM_NOADJ", adVarChar, 25
        adoProvNoADJ2.Fields.Append "CANTIDAD_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "COD_UNI_NOADJ", adVarChar, 50
        adoProvNoADJ2.Fields.Append "PRECOFE_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "IMPORTE_ITEM_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "AHORRO_ITEM_NOADJ", adDouble
        adoProvNoADJ2.Fields.Append "AHORROPORCEN_NOADJ", adVarChar, 25
        adoProvNoADJ2.Fields.Append "COD_PROCESO", adVarChar, 50
        adoProvNoADJ2.Fields.Append "ID_ITEM", adDouble
        adoProvNoADJ2.Open
    End If
    
    Set subListadoItems = oReport.OpenSubreport("Items")
    If Not subListadoItems Is Nothing Then
        Set adoItems = New ADODB.Recordset
        adoItems.Fields.Append "ADJ_AHORR", adDouble
        adoItems.Fields.Append "ADJ_PROV ", adDouble
        adoItems.Fields.Append "AHORRO", adDouble
        adoItems.Fields.Append "AHORROPORCEN", adVarChar, 10
        adoItems.Fields.Append "AHORR_GR", adDouble
        adoItems.Fields.Append "AHORRO_PROV", adDouble
        adoItems.Fields.Append "CANTIDAD", adDouble
        adoItems.Fields.Append "CANT_PROVEEDOR", adInteger
        adoItems.Fields.Append "COD_DEST_ITEM", adVarChar, 50
        adoItems.Fields.Append "COD_DEST_GR", adVarChar, 50
        adoItems.Fields.Append "COD_GRUPO", adVarChar, 50
        adoItems.Fields.Append "COD_ITEM", adVarChar, 50
        adoItems.Fields.Append "COD_ITEM_NOADJ", adVarChar, 50
        adoItems.Fields.Append "COD_PAGO_GR", adVarChar, 50
        adoItems.Fields.Append "COD_PAGO_ITEM", adVarChar, 50
        adoItems.Fields.Append "COD_PROVEEDOR", adVarChar, 50
        adoItems.Fields.Append "COD_PROVEEDOR_NOADJ", adVarChar, 50
        adoItems.Fields.Append "COD_UNI", adVarChar, 50
        adoItems.Fields.Append "COD_UNI_PROVEEDOR", adVarChar, 50
        adoItems.Fields.Append "COD_UNI_PROVEEDOR_NOADJ", adVarChar, 50
        adoItems.Fields.Append "DEN_DEST_ITEM", adVarChar, 100
        adoItems.Fields.Append "DEN_DEST_GR", adVarChar, 100
        adoItems.Fields.Append "DEN_DEST_GR_NOADJ", adVarChar, 100
        adoItems.Fields.Append "DEN_GRUPO", adVarChar, 100
        adoItems.Fields.Append "DEN_ITEM", adVarChar, 200
        adoItems.Fields.Append "DEN_PAGO_GR", adVarChar, 100
        adoItems.Fields.Append "DEN_PAGO_ITEM", adVarChar, 100
        adoItems.Fields.Append "DEN_UNI_PROVEEDOR", adVarChar, 100
        adoItems.Fields.Append "FFIN_SUM_GR", adVarChar, 25
        adoItems.Fields.Append "FFIN_SUM_ITEM", adVarChar, 25
        adoItems.Fields.Append "FINI_SUM_GR", adVarChar, 25
        adoItems.Fields.Append "FINI_SUM_ITEM", adVarChar, 25
        adoItems.Fields.Append "IMPORTE", adDouble
        adoItems.Fields.Append "NOM_PROVEEDOR", adVarChar, 100
        adoItems.Fields.Append "NOM_PROVEEDOR_NOADJ", adVarChar, 100
        adoItems.Fields.Append "OBJETIVO", adDouble
        adoItems.Fields.Append "OBS_PROVEEDOR", adVarChar, 4000
        adoItems.Fields.Append "OBS_PROVEEDOR_NOADJ", adVarChar, 4000
        adoItems.Fields.Append "PRECIO_PROVEEDOR", adDouble
        adoItems.Fields.Append "PRECIO_PROVEEDOR_NOADJ", adDouble
        adoItems.Fields.Append "PREC_OFE", adDouble
        adoItems.Fields.Append "PROVEEDOR_NOADJ", adVarChar, 100
        adoItems.Fields.Append "PORCEN_GR", adVarBinary, 10
        adoItems.Fields.Append "PORCEN_PROV", adVarChar, 10
        adoItems.Fields.Append "TOTAL_PROVEEDOR", adDouble
        adoItems.Fields.Append "PRES_AHORR", adDouble
        adoItems.Fields.Append "COD_PROCESO", adVarChar, 50
        adoItems.Fields.Append "NUM_PROCESO", adDouble
        adoItems.Fields.Append "ID_ITEM", adDouble
        adoItems.Open
    End If
    
    For Each oProce In frmRESREU.m_oReuSeleccionada.Procesos
        iNumProce = iNumProce + 1
        
        m_bCargadoGen = False
        
        oProce.CargarDatosGeneralesProceso
     
        'Sacamos la linea del proceso
        frmESPERA.lblDetalle = sCap(11)
        frmESPERA.lblContacto = sCap(12) & " " & CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod) & " " & oProce.Den
        frmESPERA.ProgressBar2.Value = (iNumProce / iNumProcesos) * 100
        
        ADORs.AddNew
        ADORs("FEC_REUNION").Value = Format(frmRESREU.sdbcFecReu.Columns(0).Value, "short date")
        ADORs("HORA_INI_REU").Value = Format(frmRESREU.sdbcFecReu.Columns(0).Value, "short time")
              
        i = 1
        iTiempo = 0
        While i < frmRESREU.m_oReuSeleccionada.Procesos.Count
                iTiempo = iTiempo + DateDiff("s", frmRESREU.m_oReuSeleccionada.Procesos.Item(i).HoraEnReunion, frmRESREU.m_oReuSeleccionada.Procesos.Item(i + 1).HoraEnReunion)
                i = i + 1
        Wend
        ADORs("HORA_FIN_REU").Value = "- " & Format(DateAdd("S", iTiempo, Format(frmRESREU.sdbcFecReu.Columns(0).Value, "Short Time")), "short time")
        If oProce.UON1_Aper <> "" Then
            sUON = oProce.UON1_Aper
            If oProce.uon2_aper <> "" Then
                sUON = sUON & "-" & oProce.uon2_aper
                If oProce.UON3_Aper <> "" Then
                    sUON = sUON & "-" & oProce.UON3_Aper
                    Set oUON3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
                    ADORs("UON_DEN_PROCESO").Value = "-" & oUON3.DevolverDenominacion(oProce.UON1_Aper, oProce.uon2_aper, oProce.UON3_Aper)
                Else
                    Set oUON2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
                    ADORs("UON_DEN_PROCESO").Value = "-" & oUON2.DevolverDenominacion(oProce.UON1_Aper, oProce.uon2_aper)
                End If
            Else
                Set oUON1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
                ADORs("UON_DEN_PROCESO").Value = "-" & oUON1.DevolverDenominacion(oProce.UON1_Aper)
            End If
        End If
        ADORs("UON_COD_PROCESO").Value = sUON
        
        ADORs("VOL_PROCESO").Value = NullToDbl0(oProce.PresTotal)
        ADORs("MON_PROCESO").Value = oProce.MonCod
         
        ADORs("REF_REUNION").Value = frmRESREU.lblref
        ADORs("NUM_PROCESO").Value = iNumProce
        ADORs("COD_PROCESO").Value = CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
        ADORs("DEN_PROCESO").Value = oProce.Den
        
        If oProce.Reudec Then
            ADORs("TIPO_REUNION").Value = sCap(13)
        Else
            ADORs("TIPO_REUNION").Value = sCap(14)
        End If
        ADORs("MONEDA").Value = oProce.MonCod
           
        If oProce.DefDestino = EnProceso Then
            ADORs("COD_DEST_PROCE").Value = NullToStr(oProce.DestCod)
            ADORs("DEN_DEST_PROCE").Value = NullToStr(oDestinos.Item(oProce.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        Else
            ADORs("COD_DEST_PROCE").Value = ""
            ADORs("DEN_DEST_PROCE").Value = ""
        End If
        
        If oProce.DefFechasSum = EnProceso Then
            ADORs("FINI_SUM_PROCE").Value = Format(NullToStr(oProce.FechaInicioSuministro), "Short Date")
            ADORs("FFIN_SUM_PROCE").Value = Format(NullToStr(oProce.FechaFinSuministro), "Short Date")
        Else
            ADORs("FINI_SUM_PROCE").Value = ""
            ADORs("FFIN_SUM_PROCE").Value = ""
        End If
            
        If oProce.DefFormaPago = EnProceso Then
            ADORs("COD_PAGO_PROCE").Value = NullToStr(oProce.PagCod)
            ADORs("DEN_PAGO_PROCE").Value = NullToStr(oPagos.Item(oProce.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        Else
            ADORs("COD_PAGO_PROCE").Value = ""
            ADORs("DEN_PAGO_PROCE").Value = ""
        End If
                                
        If Not oProce.responsable Is Nothing Then
            ADORs("NOM_RESPONSABLE").Value = NullToStr(oProce.responsable.nombre)
            ADORs("APE_RESPONSABLE").Value = NullToStr(oProce.responsable.Apel)
        Else
            ADORs("NOM_RESPONSABLE").Value = ""
            ADORs("APE_RESPONSABLE").Value = ""
        End If

        'Subreport Asistentes implicados
        Set oIPersAsig = oProce
        Set oPers = oIPersAsig.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorCod, True)
        If Not SubListadoAsistentes Is Nothing Then
            If oPers.Count > 0 Then
                  For Each oPer In oPers
                    adoAsistReu.AddNew
                    adoAsistReu("NOM_IMPLICADO").Value = NullToStr(oPer.nombre)
                    adoAsistReu("APE_IMPLICADO").Value = oPer.Apellidos
                    adoAsistReu("NUM_PROCESO").Value = iNumProce
                Next
            Else
                adoAsistReu.AddNew
                adoAsistReu("NOM_IMPLICADO").Value = ""
                adoAsistReu("APE_IMPLICADO").Value = ""
                adoAsistReu("NUM_PROCESO").Value = iNumProce
            End If
            
            If Not adoAsistReu Is Nothing Then
                SubListadoAsistentes.Database.SetDataSource adoAsistReu
            Else
                oMensajes.ImposibleMostrarInforme
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            'Fin subreport Asistentes reunion
        End If
        
        ' Comentario
        frmESPERA.ProgressBar1.Value = 3
        frmESPERA.lblContacto = sCap(15)
                   
        Comentario = frmRESREU.m_oReuSeleccionada.DevolverComentario(oProce.Anyo, oProce.GMN1Cod, oProce.Cod)
        ADORs("COMENT").Value = NullToStr(Comentario)
    
        ' Pr�xima reuni�n
        ayProxReu = oGestorReuniones.DevolverFechaProximaReunion(oProce.Anyo, oProce.GMN1Cod, oProce.Cod, CDate(frmRESREU.sdbcFecReu.Columns(0).Value))
        If Not IsNull(ayProxReu(0)) Then
            frmESPERA.ProgressBar1.Value = 4
            frmESPERA.lblContacto = sCap(16)
            ADORs("FEC_PROXIMAR").Value = NullToStr(ayProxReu(0))
            If ayProxReu(1) Then
                ADORs("TIPO_PROXIMAR").Value = " " & sCap(13)
            Else
                ADORs("TIPO_PROXIMAR").Value = " " & sCap(14)
            End If
        Else
            ADORs("FEC_PROXIMAR").Value = ""
            ADORs("TIPO_PROXIMAR").Value = ""
        End If
        
        'Carga las adjudicaciones para las �ltimas ofertas
        m_bCargadoGen = True
        Set oAdjsGrupo = oFSGSRaiz.Generar_CAdjsGrupo
        oAdjsGrupo.CargarAdjudicaciones oProce, , basPublic.gParametrosInstalacion.gIdioma, frmRESREU.sdbcFecReu.Columns(0).Value
        
        'Carga las �ltimas ofertas para el proceso
        Set oIOfertas = oProce
        oIOfertas.DevolverUltimasOfertasDelProceso basPublic.gParametrosInstalacion.gIdioma
        
        Dim bEscalados As Boolean
        For Each oGrupo In oProce.Grupos
            If oGrupo.UsarEscalados = 1 Then
                bEscalados = True
                oGrupo.CargarEscalados
            End If
        Next
        oProce.CargarAdjudicacionesGrupos frmRESREU.sdbcFecReu.Columns(0).Value, , , , bEscalados
        If FechaVigente Or PermitirAdjReunionNoVig Then
            'Carga los proveedores con sus �ltimas ofertas
            oProce.CargarUltimasOfertasGrupos basPublic.gParametrosInstalacion.gIdioma
            'Carga los precios
            oProce.CargarPreciosOfertaGrupos , bEscalados
        Else
            'Carga los proveedores con sus �ltimas ofertas
            oProce.CargarOfertasReunionGrupos frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma
            'Carga los precios
            oProce.CargarPreciosOfertaGrupos frmRESREU.sdbcFecReu.Columns(0).Value, bEscalados
        End If
                        
        'Cargar items
        If FechaVigente Or PermitirAdjReunionNoVig Then
            oProce.CargarTodosLosItemsGrupos , , , bEscalados, oUsuarioSummit.idioma, bActivoSM:=(Not g_oParametrosSM Is Nothing)
        Else
            oProce.CargarTodosLosItemsGrupos , , Fecreu:=frmRESREU.sdbcFecReu.Columns(0).Value, bCargarPresescalados:=bEscalados, sIdioma:=oUsuarioSummit.idioma, bActivoSM:=(Not g_oParametrosSM Is Nothing)
        End If
        
        'Carga los atributos aplicables al precio
        oProce.CargarAtributosAplicables
        
        'Carga de las adjudicaciones para el tema de los proves-grupos
        If gParametrosGenerales.gbProveGrupos Then
            Set oIAsigs = oProce
            Set m_oAsigs = oIAsigs.DevolverAsignaciones(OrdAsigPorCodProve, , , , , True)
            Set oIAsigs = Nothing
        End If
        
        If Not oProce.Atributos Is Nothing Then
            If oProce.Atributos.Count > 0 Then
                oProce.Ofertas.CargarAtributosProcesoOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, frmRESREU.sdbcFecReu.Columns(0).Value 'de proceso
                oProce.Ofertas.CargarAtributosGrupoOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, frmRESREU.sdbcFecReu.Columns(0).Value  'de grupo
            End If
        End If
        
        Set oIAdj = oProce
        Set oProves = oIAdj.DevolverProveedoresCompConAdjudicaciones(CDate(frmRESREU.sdbcFecReu.Columns(0).Value))
        
        CalcularPrecioConFormulas oProce, oProves, oAdjsGrupo
        
        'RESUMEN ECON�MICO:
        dblAdjudicadoProc = 0
        dblAhorradoProc = 0
        dblAhorradoPorcenProc = 0
        dblConsumidoProc = 0
        If Not subListadoResumen Is Nothing Then
            If oProves.Count > 0 Then
                For Each oGrupo In oProce.Grupos
                    bMostrarGr = True
                    If oProce.AdminPublica Then
                        If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                            bMostrarGr = False
                        End If
                    End If
        
                    If bMostrarGr Then
                        dblAdjudicado = 0
                        dblAhorrado = 0
                        dblAhorradoPorcen = 0
                        dblConsumido = 0
                        
                        'Calcula el resumen de grupos
                        For Each oProve In oProves
                            bCargar = True
                            If gParametrosGenerales.gbProveGrupos Then
                                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                    bCargar = False
                                End If
                            End If
                            If bCargar Then
                            
                                bMostrar = True
                                If oProce.AdminPublica Then
                                    If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then bMostrar = False
                                End If
                                If bMostrar Then
                                    sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                
                                    If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                        dblAdjudicadoProc = dblAdjudicadoProc + NullToDbl0(oAdjsGrupo.Item(sCod).AdjudicadoMonProce)
                                        dblConsumidoProc = dblConsumidoProc + NullToDbl0(oAdjsGrupo.Item(sCod).ConsumidoMonProce)
                                        
                                        dblAdjudicado = dblAdjudicado + NullToDbl0(oAdjsGrupo.Item(sCod).AdjudicadoMonProce)
                                        dblConsumido = dblConsumido + NullToDbl0(oAdjsGrupo.Item(sCod).ConsumidoMonProce)
                                        dblAhorrado = dblAhorrado + NullToDbl0(oAdjsGrupo.Item(sCod).AhorradoMonProce)
                                    End If
                                End If
                            End If
                        Next
                    
                        If dblConsumido = 0 Then
                            dblAhorradoPorcen = 0
                        Else
                            dblAhorradoPorcen = (dblAhorrado / dblConsumido) * 100
                        End If
                        adoResumen.AddNew
    
                        adoResumen.Fields("COD_PROCESO") = CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
                        adoResumen.Fields("GR_COD_RES").Value = oGrupo.Codigo
                        adoResumen.Fields("GR_DEN_RES").Value = oGrupo.Den
                        adoResumen.Fields("PRES_GR").Value = dblConsumido
                        adoResumen.Fields("ADJ_GR").Value = dblAdjudicado
                        adoResumen.Fields("AHORRO_GR").Value = dblAhorrado
                        adoResumen.Fields("PORCEN_GR_RES").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberProcePorcen) & "%"
                        
                        dblAdjudicadoProc = AplicarAtributosDeProceso(dblAdjudicadoProc)
                        dblAhorradoProc = dblConsumidoProc - dblAdjudicadoProc
                        If dblConsumidoProc = 0 Then
                            dblAhorradoPorcenProc = 0
                        Else
                            dblAhorradoPorcenProc = (dblAhorradoProc / dblConsumidoProc) * 100
                        End If
                        
                        adoResumen.Fields("PRES_GEN").Value = dblConsumidoProc
                        adoResumen.Fields("ADJ_GEN").Value = dblAdjudicadoProc
                        adoResumen.Fields("AHORRO_GEN").Value = dblAhorradoProc
                        adoResumen.Fields("PORCEN_GEN").Value = FormateoNumerico(dblAhorradoPorcenProc, m_sFormatoNumberProcePorcen) & "%"
                        
                    End If
                Next
            End If
        
            'RESUMEN ECONOMICO
            If Not adoResumen Is Nothing Then
                subListadoResumen.Database.SetDataSource adoResumen
            Else
                oMensajes.ImposibleMostrarInforme
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If 'not subListadoResumen is nothing
        
        'RESUMEN PROVEDORES ADJUDICADOS:
        If Not subListadoProvAdj Is Nothing Then
            If oProves.Count > 0 Then
                For Each oProve In oProves
                    dblAdjudicado = 0
                    dblAhorrado = 0
                    dblAhorradoPorcen = 0
                    dblConsumido = 0
                    
                    For Each oGrupo In oProce.Grupos
                        bCargar = True
                        If gParametrosGenerales.gbProveGrupos Then
                            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                bCargar = False
                            End If
                        End If
                        If bCargar Then
                    
                            bMostrar = True
                            If oProce.AdminPublica = True Then
                                If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then bMostrar = False
                            End If
                            If bMostrar = True Then
                                sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            
                                If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                    dblAdjudicado = dblAdjudicado + NullToDbl0(oAdjsGrupo.Item(sCod).AdjudicadoMonProce)
                                    dblConsumido = dblConsumido + NullToDbl0(oAdjsGrupo.Item(sCod).ConsumidoMonProce)
                                    dblAhorrado = dblAhorrado + NullToDbl0(oAdjsGrupo.Item(sCod).AhorradoMonProce)
                                End If
                            End If
                        End If
                    Next
                    
                    dblAdjudicado = AplicarAtributosDeProceso(dblAdjudicado)
                    dblAhorrado = dblConsumido - dblAdjudicado
                    If dblConsumido = 0 Then
                        dblAhorradoPorcen = 0
                    Else
                        dblAhorradoPorcen = (dblAhorrado / dblConsumido) * 100
                    End If
                    adoProvAdj.AddNew
                    adoProvAdj.Fields("PRES_TOT").Value = dblConsumidoProc
                    adoProvAdj.Fields("OFE_TOT").Value = dblAdjudicadoProc
                    adoProvAdj.Fields("AHORRO_TOT").Value = dblAhorradoProc
                    adoProvAdj.Fields("PORCEN_TOT").Value = FormateoNumerico(dblAhorradoPorcenProc, m_sFormatoNumberProcePorcen) & "%"
                    
                    adoProvAdj.Fields("PROV_COD_RES").Value = oProve.Cod
                    adoProvAdj.Fields("PROV_DEN_RES").Value = oProve.Den
                    adoProvAdj.Fields("PRES_TOT_PROV").Value = dblConsumido
                    adoProvAdj.Fields("OFE_TOT_PROV").Value = dblAdjudicado
                    adoProvAdj.Fields("AHORRO_TOT_PROV").Value = dblAhorrado
                    adoProvAdj.Fields("PORCEN_TOT_PROV").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberProcePorcen) & "%"
                    adoProvAdj.Fields("NUM_PROCESO").Value = iNumProce
                Next
            Else
                adoProvAdj.AddNew
                adoProvAdj("AHORRO_TOT") = 0
                adoProvAdj("AHORRO_TOT_PROV") = 0
                adoProvAdj("AHORRO_TOT_PROV_NOADJ") = 0
                adoProvAdj("OFE_TOT") = 0
                adoProvAdj("OFE_TOT_PROV") = 0
                adoProvAdj("OFE_TOT_PROV_NOADJ") = 0
                adoProvAdj("PORCEN_TOT") = ""
                adoProvAdj("PORCEN_TOT_PROV") = ""
                adoProvAdj("PORCEN_TOT_PROV_NOADJ") = ""
                adoProvAdj("PRES_TOT_PROV") = 0
                adoProvAdj("PRES_TOT_PROV_NOADJ") = 0
                adoProvAdj("PRES_TOT") = 0
                adoProvAdj("PROV_COD_RES") = ""
                adoProvAdj("PROV_DEN_RES") = ""
                adoProvAdj("PROV_COD_RES_NOADJ") = ""
                adoProvAdj("PROV_DEN_RES_NOADJ") = ""
                adoProvAdj("NUM_PROCESO") = 0
            End If
        End If 'not subListadoProvAdj is nothing
        
        If Not subListadoProvNoAdj Is Nothing Then
            If chkProvNoAdj.Value = vbChecked Then
                Set oProvesnoAdj = oIAdj.DevolverProveedoresSinAdjudicaciones(, , , CDate(frmRESREU.sdbcFecReu.Columns(0).Value))
                If oProvesnoAdj.Count > 0 Then
                    For Each oProve In oProvesnoAdj
                        dblConsumido = 0
                        dblAdjudicado = 0
                        dblAhorrado = 0
                        dblAhorradoPorcen = 0
                        Dim oGrupo2 As CGrupo
                        For Each oGrupo2 In oProce.Grupos
                            bCargar = True
                            If gParametrosGenerales.gbProveGrupos Then
                                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo2.Codigo) Is Nothing Then
                                    bCargar = False
                                End If
                            End If
                            If bCargar Then
                                bMostrar = True
                                If oProce.AdminPublica Then
                                    If oProce.Sobres.Item(CStr(oGrupo2.Sobre)).Estado = 0 Then bMostrar = False
                                End If
                                If bMostrar Then
                                    sCod = CStr(oGrupo2.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
        
                                    If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                        dblAdjudicado = dblAdjudicado + NullToDbl0(oAdjsGrupo.Item(sCod).ImporteMonProce)
                                        If oAdjsGrupo.Item(sCod).importe <> 0 Then
                                            dblConsumido = dblConsumido + NullToDbl0(oAdjsGrupo.Item(sCod).AbiertoMonProce)
                                        End If
                                        dblAhorrado = dblAhorrado + NullToDbl0(oAdjsGrupo.Item(sCod).AhorroOfeMonProce)
                                    End If
                                End If
                            End If
                        Next
        
                        dblAdjudicado = AplicarAtributosDeProcesoNoAdj(dblAdjudicado, oProve.Cod)
                        dblAhorrado = dblConsumido - dblAdjudicado
                        If dblConsumido = 0 Then
                            dblAhorradoPorcen = 0
                        Else
                            dblAhorradoPorcen = (dblAhorrado / dblConsumido) * 100
                        End If
        
                        adoProvNoAdj.AddNew
                        adoProvNoAdj("PROV_COD_RES_NOADJ").Value = oProve.Cod
                        adoProvNoAdj("PROV_DEN_RES_NOADJ").Value = oProve.Den
                        adoProvNoAdj("PRES_TOT_PROV_NOADJ").Value = dblConsumido
                        adoProvNoAdj("OFE_TOT_PROV_NOADJ").Value = dblAdjudicado
                        adoProvNoAdj("AHORRO_TOT_PROV_NOADJ").Value = dblAhorrado
                        adoProvNoAdj("PORCEN_TOT_PROV_NOADJ").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberProcePorcen) & "%"
                        adoProvNoAdj("NUM_PROCESO").Value = iNumProce
                    Next
                End If
            Else
                adoProvNoAdj.AddNew
                adoProvNoAdj("AHORRO_TOT") = 0
                adoProvNoAdj("AHORRO_TOT_PROV") = 0
                adoProvNoAdj("AHORRO_TOT_PROV_NOADJ") = 0
                adoProvNoAdj("OFE_TOT") = 0
                adoProvNoAdj("OFE_TOT_PROV") = 0
                adoProvNoAdj("OFE_TOT_PROV_NOADJ") = 0
                adoProvNoAdj("PORCEN_TOT") = ""
                adoProvNoAdj("PORCEN_TOT_PROV") = ""
                adoProvNoAdj("PORCEN_TOT_PROV_NOADJ") = ""
                adoProvNoAdj("PRES_TOT_PROV") = 0
                adoProvNoAdj("PRES_TOT_PROV_NOADJ") = 0
                adoProvNoAdj("PRES_TOT") = 0
                adoProvNoAdj("PROV_COD_RES") = ""
                adoProvNoAdj("PROV_DEN_RES") = ""
                adoProvNoAdj("PROV_COD_RES_NOADJ") = ""
                adoProvNoAdj("PROV_DEN_RES_NOADJ") = ""
                adoProvNoAdj("NUM_PROCESO") = 0
            End If
        
            If Not adoProvNoAdj Is Nothing Then
                subListadoProvNoAdj.Database.SetDataSource adoProvNoAdj
            Else
                oMensajes.ImposibleMostrarInforme
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If  'not subListadoProvNoAdj is nothing
    
        dblAdjudicadoProc = AplicarAtributosDeProceso(dblAdjudicadoProc)
        dblAhorradoProc = dblConsumidoProc - dblAdjudicadoProc
        If dblConsumidoProc = 0 Then
            dblAhorradoPorcenProc = 0
        Else
            dblAhorradoPorcenProc = (dblAhorradoProc / dblConsumidoProc) * 100
        End If
                            
        ADORs("PRES_GEN").Value = dblConsumidoProc
        ADORs("ADJ_GEN").Value = dblAdjudicadoProc
        ADORs("AHORRO_GEN").Value = dblAhorradoProc
        ADORs("PORCEN_GEN").Value = FormateoNumerico(dblAhorradoPorcenProc, m_sFormatoNumberProcePorcen) & "%"
          
        If oProves.Count > 0 Then
            If optTipo1.Value = True Then
                'TIPO 1:
                If Not subListadoItems Is Nothing Then
                    Set adoItems = TipoActa1Crystal(iNumProce, adoItems)
                    If adoItems Is Nothing Then GoTo Finalizar
                End If
            Else
                'TIPO2:
                If Not adoProv Is Nothing Or Not adoProvNoAdj Is Nothing Then
                    If TipoActa2Crystal(adoProv, adoProvNoADJ2) = False Then GoTo Finalizar
                End If
            End If
       
        End If
    Next 'Proceso
    
    Screen.MousePointer = vbNormal
    
    If optTipo2.Value = True Then
        'Set SubListado = oReport.OpenSubreport("ProveedorAdjudicado")
        If Not subListadoProveedorAdjudicado Is Nothing Then
            If Not adoProv Is Nothing Then
                subListadoProveedorAdjudicado.Database.SetDataSource adoProv
            Else
                oMensajes.ImposibleMostrarInforme
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
        
        If Not subListadoProveedorNoAdjudicado Is Nothing Then
            If Not adoProvNoADJ2 Is Nothing Then
                subListadoProveedorNoAdjudicado.Database.SetDataSource adoProvNoADJ2
            Else
                oMensajes.ImposibleMostrarInforme
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
    End If
    
   'PROVEEDORES ADJUDICADOS
   If Not subListadoProvAdj Is Nothing Then
        If Not adoProvAdj Is Nothing Then
            subListadoProvAdj.Database.SetDataSource adoProvAdj
        Else
            oMensajes.ImposibleMostrarInforme
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    'Acta Tipo 1 Subreport Items
    If optTipo1.Value = True Then
        If Not subListadoItems Is Nothing Then
            If Not adoItems Is Nothing Then
                subListadoItems.Database.SetDataSource adoItems
            Else
                oMensajes.ImposibleMostrarInforme
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
    End If
    
    If Not ADORs Is Nothing Then
        oReport.Database.SetDataSource ADORs
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = Me.caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
        
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    pv.Show
        
    basSeguridad.RegistrarAccion AccionesSummit.ACCResReuObtenerActa, "Proceso: " & CStr(Format(frmRESREU.sdbcFecReu.Columns(0).Value, "Short Date"))
Finalizar:
    On Error Resume Next
    
    If bError Then
        Set oReport = Nothing
        oReport.Close
    End If
    
    Set oPers = Nothing
    Set oPer = Nothing
    Set oGrupo = Nothing
    Set oProve = Nothing
    Set oIAdj = Nothing
    Set oIOfertas = Nothing
    Set oIAsigs = Nothing
    Set oIPersAsig = Nothing
    Set ADORs = Nothing
    Set adoAsist = Nothing
    Set adoAsistReu = Nothing
    Set adoResumen = Nothing
    Set adoProvNoAdj = Nothing
    Set adoProvAdj = Nothing
    Set adoProvNoADJ2 = Nothing
    Set oReport = Nothing
    Set SubListadoAsistentes = Nothing
    Set subListadoResumen = Nothing
    Set subListadoProvAdj = Nothing
    Set subListadoProvNoAdj = Nothing
    Set subListadoProveedorAdjudicado = Nothing
    Set subListadoProveedorNoAdjudicado = Nothing
    Set subListadoItems = Nothing

    Exit Sub
    
Error_2:
    Screen.MousePointer = vbNormal
    
    If err.Number = -2147190528 Then
        'Subreport no existente
        Resume Next
    End If
    
    Unload frmESPERA
    
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar
End Sub

Private Function CargarAtributos(ByRef oProce As cProceso) As CAtributos
    Dim oatrib As CAtributo
    Dim oAtribTotalGrupo As CAtribTotalGrupo
    Dim oAtribsFormulas As CAtributos
    
    'carga todos los atributos para el proceso seleccionado
    'Carga los atributos de proceso (�mbito=1)
    On Error GoTo ERROR
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oProce.CargarAtributos ambito:=proceso

    'Carga los atributos de grupo (�mbito=2)
    oProce.CargarAtributos ambito:=AmbGrupo
    'Carga los atributos de item (�mbito=3)
    oProce.CargarAtributos ambito:=AmbItem
    'ahora cargo en la clase m_oAtribsFormulas los atributos que tienen f�rmulas a aplicar.Esta clase la uso para que
    'sea m�s f�cil aplicar las f�rmulas de los atributos,por el orden.Lo hago as� para evitar otro acceso
    'a BD.
        
    Set oAtribsFormulas = oFSGSRaiz.Generar_CAtributos

    'a�ado a la clase los atributos de �mbito item
    If Not oProce.AtributosItem Is Nothing Then
        For Each oatrib In oProce.AtributosItem
            If Not IsNull(oatrib.PrecioAplicarA) Then
                oAtribsFormulas.Add oatrib.Id, oatrib.Cod, oatrib.Den, oatrib.Tipo, oatrib.PreferenciaValorBajo, oatrib.GMN1, oatrib.GMN2, oatrib.GMN3, oatrib.GMN4, oatrib.Heredado, oatrib.Articulo, oatrib.indice, oatrib.valor, oatrib.FECACT, oatrib.IDPlantilla, oatrib.AnyoProce, oatrib.CodProce, oatrib.codgrupo, oatrib.idAtribProce, oatrib.TipoIntroduccion, oatrib.Minimo, oatrib.Maximo, oatrib.BajaLogica, oatrib.interno, oatrib.ambito, oatrib.Obligatorio, oatrib.PrecioFormula, oatrib.PrecioAplicarA, oatrib.PrecioDefecto, oatrib.TipoPonderacion, , oatrib.Formula, oatrib.Numero, oatrib.ValorPondSi, oatrib.ValorPondNo, True, oatrib.UsarPrec, oatrib.Orden
                oAtribsFormulas.Item(CStr(oatrib.idAtribProce)).FlagAplicar = True
            End If
        Next
    End If

    'a�ado a la clase los atributos de �mbito grupo
    If Not oProce.AtributosGrupo Is Nothing Then
        For Each oatrib In oProce.AtributosGrupo
            If Not IsNull(oatrib.PrecioAplicarA) Then
                oAtribsFormulas.Add oatrib.Id, oatrib.Cod, oatrib.Den, oatrib.Tipo, oatrib.PreferenciaValorBajo, oatrib.GMN1, oatrib.GMN2, oatrib.GMN3, oatrib.GMN4, oatrib.Heredado, oatrib.Articulo, oatrib.indice, oatrib.valor, oatrib.FECACT, oatrib.IDPlantilla, oatrib.AnyoProce, oatrib.CodProce, oatrib.codgrupo, oatrib.idAtribProce, oatrib.TipoIntroduccion, oatrib.Minimo, oatrib.Maximo, oatrib.BajaLogica, oatrib.interno, oatrib.ambito, oatrib.Obligatorio, oatrib.PrecioFormula, oatrib.PrecioAplicarA, oatrib.PrecioDefecto, oatrib.TipoPonderacion, , oatrib.Formula, oatrib.Numero, oatrib.ValorPondSi, oatrib.ValorPondNo, True, oatrib.UsarPrec, oatrib.Orden

                If oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalGrupo Then    'si se aplica al total de la oferta
                    oAtribsFormulas.Item(CStr(oatrib.idAtribProce)).CargarUsarPrecTotalGrupo
                Else
                    oAtribsFormulas.Item(CStr(oatrib.idAtribProce)).FlagAplicar = True
                End If
            End If
        Next
    End If

    'a�ado a la clase los atributos de �mbito proceso
    If Not oProce.Atributos Is Nothing Then
        For Each oatrib In oProce.Atributos
            If Not IsNull(oatrib.PrecioAplicarA) Then
                oAtribsFormulas.Add oatrib.Id, oatrib.Cod, oatrib.Den, oatrib.Tipo, oatrib.PreferenciaValorBajo, oatrib.GMN1, oatrib.GMN2, oatrib.GMN3, oatrib.GMN4, oatrib.Heredado, oatrib.Articulo, oatrib.indice, oatrib.valor, oatrib.FECACT, oatrib.IDPlantilla, oatrib.AnyoProce, oatrib.CodProce, oatrib.codgrupo, oatrib.idAtribProce, oatrib.TipoIntroduccion, oatrib.Minimo, oatrib.Maximo, oatrib.BajaLogica, oatrib.interno, oatrib.ambito, oatrib.Obligatorio, oatrib.PrecioFormula, oatrib.PrecioAplicarA, oatrib.PrecioDefecto, oatrib.TipoPonderacion, , oatrib.Formula, oatrib.Numero, oatrib.ValorPondSi, oatrib.ValorPondNo, True, oatrib.UsarPrec, oatrib.Orden

                If oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalGrupo Then    'si se aplica al total de la oferta
                    oAtribsFormulas.Item(CStr(oatrib.idAtribProce)).CargarUsarPrecTotalGrupo
                Else
                    oAtribsFormulas.Item(CStr(oatrib.idAtribProce)).FlagAplicar = True
                End If
            End If
        Next
    End If
    
    Set CargarAtributos = oAtribsFormulas
        
    Exit Function
ERROR:
    If err.Number <> 0 Then
        MsgBox err.Description, vbCritical, "Fullstep"
        Exit Function
    End If
End Function

''' <summary>Realiza los c�lculo para los grids de la pantalla</summary>
''' <remarks>Llamada desde:  CrearActaCrystal</remarks>

Private Sub CalcularPrecioConFormulas(ByRef oProce As cProceso, ByRef oProves As CProveedores, ByRef oAdjsGrupo As CAdjsGrupo)
    Dim oGrupo As CGrupo
    Dim oProve As CProveedor
    Dim bMostrar As Boolean
    Dim oAtribsFormulas As CAtributos
    Dim ImportesProc As ImportesProceso
    Dim oAdjsProveEsc As CAdjudicaciones
    
    'Calcula el precio v�lido seg�n las f�rmulas a aplicar
    'Se va recorriendo cada grupo y calculado el precio v�lido de las �ltimas ofertas
    'para el grupo aplic�ndole las f�rmulas correspondientes.
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR

    Set oAtribsFormulas = CargarAtributos(oProce)

    ImportesProc.AbiertoProc = 0
    ImportesProc.ConsumidoProc = 0
    ImportesProc.AbiertoProcReal = 0
    ImportesProc.ConsumidoProcReal = 0
    ImportesProc.AdjudicadoProc = 0
    ImportesProc.AdjudicadoProcReal = 0
    ImportesProc.AdjudicadoSinProc = 0
    ImportesProc.AdjudicadoSinProcReal = 0
    ImportesProc.AhorradoProc = 0
    ImportesProc.AhorradoPorcenProc = 0
    ImportesProc.TotalProveObjAll = 0
    ImportesProc.TotalAhorroProveObjAll = 0

    For Each oProve In oProves
        If Not oProce.Ofertas.Item(oProve.Cod) Is Nothing Then
            oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProc = 0
            oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcMonProce = 0
            oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcReal = 0
            oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcRealMonProce = 0
        End If
    Next

    Set oAdjsProveEsc = CalcularAdjudicacionesGruposEscalados(oProce, oProce.Grupos, oProves, oAtribsFormulas)

    For Each oGrupo In oProce.Grupos
        bMostrar = True
        'Si es un proceso se administraci�n p�blica comprueba que el grupo est� asignado a un sobre visible
        If oProce.AdminPublica Then
            If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then bMostrar = False
        End If

        If bMostrar Then
            If oGrupo.UsarEscalados Then
                CalcularPrecioConFormulasGrupoEsc oProce, oGrupo, oAtribsFormulas, oAdjsGrupo, ImportesProc, oProves, oAdjsProveEsc
            Else
                CalcularPrecioConFormulasGrupo oProce, oGrupo, oAtribsFormulas, oAdjsGrupo, ImportesProc, oProves
            End If
        End If
    Next  'oGrupo

    'Ahora aplica los atributos para el total del proceso
    ImportesProc.AdjudicadoProc = ImportesProc.AdjudicadoSinProc
    ImportesProc.AdjudicadoProcReal = ImportesProc.AdjudicadoSinProcReal

    AplicarAtributosNivelProceso oProce, oProves, oAdjsGrupo, oAtribsFormulas, ImportesProc
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If err.Number <> 0 Then
        MsgBox err.Description, vbCritical, "Fullstep"
        Exit Sub
    End If
End Sub

''' <summary>Realiza los c�lculos de pantalla para un grupo</summary>
''' <param name="oGrupo">Grupo para el que se est�n llevando a cabo los c�lculos</param>
''' <remarks>Llamada desde: CalcularPrecioConFormulas</remarks>
Private Sub CalcularPrecioConFormulasGrupo(ByRef oProce As cProceso, ByVal oGrupo As CGrupo, ByRef oAtribsFormulas As CAtributos, ByRef oAdjsGrupo As CAdjsGrupo, ByRef ImportesProc As ImportesProceso, ByRef oProves As CProveedores)
    Dim oProve As CProveedor
    Dim oItem As CItem
    Dim oPrecItem As CPrecioItem
    Dim oOfertaGr As COferta
    Dim oAdj As CAdjudicacion
    Dim dAdjudicadoTotalGrupo As Double
    Dim dAdjudicadoGrupoReal As Double
    Dim dAdjudicadoTotalGrupoReal As Double
    Dim dConsumidoGrupo As Double
    Dim dObjetivoGrupo As Double
    Dim dObjetivoAhoGrupo As Double
    Dim dPrecioAplicFormula As Variant
    Dim bAplicarAtribsGrupo As Boolean
    Dim bAdjudicado As Boolean
    Dim dAdjudicado As Double
    Dim dAdjudicadoMonProce As Double
    Dim dAdjudicadoGrupo As Double    'Para los totales de grupo de CGrupo
    Dim dAdjudicadoReal As Double
    Dim dAdjudicadoRealMonProce As Double
    Dim dAdjudicadoItems As Double
    Dim dAdjudicadoItemsMonProce As Double
    Dim dConsumido As Double
    Dim dConsumidoMonProce As Double
    Dim dConsumidoReal As Double
    Dim dConsumidoRealMonProce As Double
    Dim dImporte As Double
    Dim dImporteMonProce As Double
    Dim dAhorroOfeAb As Double
    Dim dAhorroOfeAbMonProce As Double
    Dim bHayPrecios As Boolean
    Dim sCod As String
    Dim bDistintos As Boolean
    Dim dblCambio As Double
    Dim dblImporteAux As Double
    
    'Para los totales de grupo de CGrupo
    On Error GoTo ERROR

    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dAdjudicadoGrupo = 0
    dAdjudicadoTotalGrupo = 0
    dAdjudicadoGrupoReal = 0
    dAdjudicadoTotalGrupoReal = 0
    dConsumidoGrupo = 0
    dObjetivoGrupo = 0
    dObjetivoAhoGrupo = 0
    
    'Ahora empieza el c�lculo del precio unitario
    For Each oProve In oProves
        For Each oItem In oGrupo.Items
            If oItem.Confirmado And (Not oItem.Cerrado) Then
                dPrecioAplicFormula = 0

                If Not oGrupo.UltimasOfertas.Item(oProve.Cod) Is Nothing Then
                    Set oPrecItem = oGrupo.UltimasOfertas.Item(oProve.Cod).Lineas.Item(CStr(oItem.Id))

                    If Not oPrecItem Is Nothing Then
                        'Comprueba si se va a usar el precio 1,2 o 3
                        Select Case oPrecItem.Usar
                        Case 1
                            dPrecioAplicFormula = oPrecItem.Precio
                        Case 2
                            dPrecioAplicFormula = oPrecItem.Precio2
                        Case 3
                            dPrecioAplicFormula = oPrecItem.Precio3
                        End Select

                        dPrecioAplicFormula = AplicarAtributos(oProce, oAtribsFormulas, dPrecioAplicFormula, TipoAplicarAPrecio.UnitarioItem, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, oItem.Id)

                        'Guarda el precio v�lido obtenido en la colecci�n, en moneda de la oferta
                        oPrecItem.PrecioOferta = dPrecioAplicFormula
                    End If
                End If
            End If
        Next    'oItem
    Next oProve

    '**** APLICA LOS ATRIBUTOS A APLICAR AL TOTAL DEL ITEM ****
    AplicarAtributosNivelItem oProce, oGrupo.Codigo, oProves, ImportesProc, oAtribsFormulas

    bAplicarAtribsGrupo = True
    If Not oAtribsFormulas Is Nothing Then bAplicarAtribsGrupo = oAtribsFormulas.ComprobarAplicarAtribsGrupo(oGrupo.Codigo)

    'Calculamos consumido y adjudicado
    For Each oProve In oProves
        'Todo en moneda de la ofeerta
        bAdjudicado = False
        dAdjudicado = 0    'Adjudicado del grupo para un proveedor - con atribs
        dAdjudicadoMonProce = 0
        dAdjudicadoReal = 0    'Adjudicado del grupo para un proveedor - con atribs con todos los cerrados
        dAdjudicadoRealMonProce = 0
        dAdjudicadoItems = 0    'Suma del Adjudicado de los items del grupo para un proveedor - sin atribs de total item
        dAdjudicadoItemsMonProce = 0
        dConsumido = 0  'Consumido del grupo para un proveedor
        dConsumidoMonProce = 0
        dConsumidoReal = 0
        dConsumidoRealMonProce = 0
        dImporte = 0    'Adjudicado hipotetico del grupo para un proveedor - con atribs
        dImporteMonProce = 0
        dAhorroOfeAb = 0    'Abierto del grupo (suma de los items
        dAhorroOfeAbMonProce = 0
        bHayPrecios = False

        Set oOfertaGr = oGrupo.UltimasOfertas.Item(oProve.Cod)

        For Each oItem In oGrupo.Items
            With oItem
                If .Confirmado Then
                    If Not oOfertaGr Is Nothing Then
                        If Not oOfertaGr.Lineas.Item(CStr(.Id)) Is Nothing Then
                            sCod = oOfertaGr.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oOfertaGr.Prove))
                            sCod = CStr(.Id) & sCod
                            Set oAdj = oGrupo.Adjudicaciones.Item(sCod)

                            dblCambio = .CambioComparativa(oOfertaGr.Prove)

                            'Va recalculando los valores de consumido y adjudicado para el grupo
                            If Not oAdj Is Nothing Then
                                dAdjudicado = dAdjudicado + VarToDbl0(oAdj.ImporteAdjTot)
                                dAdjudicadoMonProce = dAdjudicadoMonProce + (VarToDbl0(oAdj.ImporteAdjTot) / dblCambio)
                                dAdjudicadoItems = dAdjudicadoItems + VarToDbl0(oAdj.ImporteAdj)
                                dAdjudicadoItemsMonProce = dAdjudicadoItemsMonProce + (VarToDbl0(oAdj.ImporteAdj) / dblCambio)
                                dImporte = dImporte + VarToDbl0(oAdj.importe)
                                dImporteMonProce = dImporteMonProce + (VarToDbl0(oAdj.importe) / dblCambio)
                                dConsumido = dConsumido + ((NullToDbl0(.Precio) * dblCambio) * (.Cantidad * (oAdj.Porcentaje / 100)))
                                dConsumidoMonProce = dConsumidoMonProce + (NullToDbl0(.Precio) * (.Cantidad * (oAdj.Porcentaje / 100)))

                                'En moneda del proceso acumulo el consumido y adjudicado del grupo
                                dConsumidoGrupo = dConsumidoGrupo + (NullToDbl0(.Precio) * .Cantidad * (oAdj.Porcentaje / 100))
                                dAdjudicadoGrupo = dAdjudicadoGrupo + (VarToDbl0(oAdj.ImporteAdjTot) / dblCambio)

                                'Calculo el total objetivos para ALL, el de grupo se calcular� al ir a cada pesta�a
                                If Not IsNull(.Objetivo) Then
                                    dObjetivoGrupo = dObjetivoGrupo + (.Cantidad * (oAdj.Porcentaje / 100) * NullToDbl0(.Objetivo))
                                    dObjetivoAhoGrupo = dObjetivoAhoGrupo + (NullToDbl0(.Precio) * .Cantidad * (oAdj.Porcentaje / 100)) - (NullToDbl0(.Objetivo) * .Cantidad * (oAdj.Porcentaje / 100))
                                    'Importe (Objetivo * Cantidad)
                                    ImportesProc.TotalProveObjAll = ImportesProc.TotalProveObjAll + (.Cantidad * (oAdj.Porcentaje / 100) * NullToDbl0(.Objetivo))
                                    'Ahorro ((Presupuesto*Cantidad) - (Objetivo * Cantidad))
                                    ImportesProc.TotalAhorroProveObjAll = ImportesProc.TotalAhorroProveObjAll + (NullToDbl0(.Precio) * .Cantidad * (oAdj.Porcentaje / 100)) - (NullToDbl0(.Objetivo) * .Cantidad * (oAdj.Porcentaje / 100))
                                End If
                            Else
                                dImporte = dImporte + (NullToDbl0(oOfertaGr.Lineas.Item(CStr(oItem.Id)).importe))
                                dImporteMonProce = dImporteMonProce + (NullToDbl0(oOfertaGr.Lineas.Item(CStr(oItem.Id)).importe) / dblCambio)
                            End If
                            If Not IsNull(oOfertaGr.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                                dAhorroOfeAb = dAhorroOfeAb + (NullToDbl0(.Cantidad) * NullToDbl0(.Precio) * dblCambio)
                                dAhorroOfeAbMonProce = dAhorroOfeAbMonProce + (NullToDbl0(.Cantidad) * NullToDbl0(.Precio))
                                bHayPrecios = True
                            End If
                            If Not oAdj Is Nothing Then    'Esto es para el Adjudicado real del proceso
                                dAdjudicadoReal = dAdjudicadoReal + VarToDbl0(oAdj.ImporteAdjTot)
                                dAdjudicadoRealMonProce = dAdjudicadoRealMonProce + (VarToDbl0(oAdj.ImporteAdjTot) / dblCambio)
                                dConsumidoReal = dConsumidoReal + ((NullToDbl0(.Precio) * dblCambio) * (.Cantidad * (oAdj.Porcentaje / 100)))
                                dConsumidoRealMonProce = dConsumidoRealMonProce + (NullToDbl0(.Precio) * .Cantidad * (oAdj.Porcentaje / 100))

                                'En moneda de proceso acumulo para el grupo
                                dAdjudicadoGrupoReal = dAdjudicadoGrupoReal + (VarToDbl0(oAdj.ImporteAdjTot) / dblCambio)

                                bAdjudicado = True
                            End If
                        End If
                    End If  'si hay ofertas
                End If
            End With
        Next  'oItem

        Set oOfertaGr = Nothing

        '**** APLICA LOS ATRIBUTOS A APLICAR AL TOTAL DEL GRUPO ****
        Set oOfertaGr = oProce.Ofertas.Item(oProve.Cod)
        sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
        If Not oOfertaGr Is Nothing Then
            'Ahora aplica las operaciones para los atributos de grupo.Aplica las f�rmulas al importe obtenido

            If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                'Guardo los valores antes de aplicar los atributos
                oAdjsGrupo.Item(sCod).AdjudicadoSin = dAdjudicado
                oAdjsGrupo.Item(sCod).AdjudicadoSinMonProce = dAdjudicadoMonProce
                oAdjsGrupo.Item(sCod).AdjudicadoSinReal = dAdjudicadoReal
                oAdjsGrupo.Item(sCod).AdjudicadoSinRealMonProce = dAdjudicadoRealMonProce
                oAdjsGrupo.Item(sCod).AdjudicadoItems = dAdjudicadoItems
                oAdjsGrupo.Item(sCod).AdjudicadoItemsMonproce = dAdjudicadoItemsMonProce
                oAdjsGrupo.Item(sCod).AdjudicadoOfe = dImporte
                oAdjsGrupo.Item(sCod).AdjudicadoOfeMonProce = dImporteMonProce

                'Aplica los atributos DE GRUPO
                If bAplicarAtribsGrupo Then
                    bDistintos = False
                    If dAdjudicadoReal <> dAdjudicado Then
                        If bAdjudicado Then    'Si hay algo adjudicado, sino no se aplica
                            dblImporteAux = AplicarAtributos(oProce, oAtribsFormulas, dAdjudicadoReal, TotalGrupo, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                            If dblImporteAux <> 0 Then dAdjudicadoReal = NullToDbl0(dblImporteAux)

                            'En moneda de proceso
                            dblImporteAux = AplicarAtributos(oProce, oAtribsFormulas, dAdjudicadoRealMonProce, TotalGrupo, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                               oGrupo.Codigo, , , oGrupo.CambioComparativa(oProve.Cod))
                            If dblImporteAux <> 0 Then dAdjudicadoRealMonProce = dblImporteAux
                        End If
                        bDistintos = True
                    End If
                    dblImporteAux = 0
                    If bAdjudicado Then dblImporteAux = dAdjudicado
                    If bHayPrecios Then dImporte = AplicarAtributos(oProce, oAtribsFormulas, dImporte, TotalGrupo, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)

                    If bAdjudicado Then
                        dAdjudicado = dblImporteAux
                        If Not bDistintos Then dAdjudicadoReal = dblImporteAux
                    End If

                    'En moneda de proceso
                    dblImporteAux = 0
                    If bAdjudicado Then dblImporteAux = dAdjudicadoMonProce
                    If bHayPrecios Then dImporteMonProce = AplicarAtributos(oProce, oAtribsFormulas, dImporteMonProce, TotalGrupo, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                                            oGrupo.Codigo, , , oGrupo.CambioComparativa(oProve.Cod))
                    If bAdjudicado Then
                        dAdjudicadoMonProce = dblImporteAux
                        If Not bDistintos Then dAdjudicadoRealMonProce = dblImporteAux
                    End If
                End If
            End If
        End If
        Set oOfertaGr = Nothing

        If dAdjudicado <> 0 Then dAdjudicadoTotalGrupo = dAdjudicadoTotalGrupo + (dAdjudicadoMonProce)
        If dAdjudicadoReal <> 0 Then dAdjudicadoTotalGrupoReal = dAdjudicadoTotalGrupoReal + (dAdjudicadoRealMonProce)

        'Almacena el importe,adjudicado,consumido,ahorrado,ahorradoporcen,ahorro ofe,... para el proveedor
        If Not oAdjsGrupo.Item(sCod) Is Nothing Then
            With oAdjsGrupo.Item(sCod)
                'Todo en moneda de la oferta
                .Adjudicado = dAdjudicado    'Adjudicado del grupo para un proveedor - con atribs
                .AdjudicadoMonProce = dAdjudicadoMonProce
                .AdjudicadoReal = dAdjudicadoReal
                .AdjudicadoRealMonProce = dAdjudicadoRealMonProce
                .Consumido = dConsumido   'Consumido del grupo para un proveedor
                .ConsumidoMonProce = dConsumidoMonProce
                .ConsumidoReal = dConsumidoReal
                .ConsumidoRealMonProce = dConsumidoRealMonProce
                .Ahorrado = (dConsumido - dAdjudicado)  'Ahorro del grupo para un proveedor
                .AhorradoMonProce = (dConsumidoMonProce - dAdjudicadoMonProce)
                If dConsumido = 0 Then
                    .AhorradoPorcen = 0
                Else
                    .AhorradoPorcen = ((dConsumidoMonProce - dAdjudicadoMonProce) / Abs(dConsumidoMonProce)) * 100
                End If

                .importe = dImporte                     'Adjudicado hipotetico del grupo para un proveedor - con atribs
                .ImporteMonProce = dImporteMonProce
                .AbiertoOfe = dAhorroOfeAb              'Consumido hipotetico (abierto) del grupo para un prove
                .AbiertoOfeMonProce = dAhorroOfeAbMonProce
                .AhorroOfe = dAhorroOfeAb - dImporte    'Ahorro hipotetico del grupo para un prove
                .AhorroOfeMonProce = dAhorroOfeAbMonProce - dImporteMonProce
                If dAhorroOfeAb = 0 Then
                    .AhorroOfePorcen = 0
                Else
                    .AhorroOfePorcen = ((dAhorroOfeAbMonProce - dImporteMonProce) / Abs(dAhorroOfeAbMonProce)) * 100
                End If

                .HayPrecios = bHayPrecios

                'Calcula los totales proceso en moneda del proceso
                'Si el grupo est� cerrado no se muestra en los resultados
                ImportesProc.ConsumidoProc = ImportesProc.ConsumidoProc + .ConsumidoMonProce
                ImportesProc.AdjudicadoSinProc = ImportesProc.AdjudicadoSinProc + .AdjudicadoMonProce
                oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProc = oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProc + .Adjudicado
                oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcMonProce = oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcMonProce + .AdjudicadoMonProce
                ImportesProc.AdjudicadoSinProcReal = ImportesProc.AdjudicadoSinProcReal + .AdjudicadoRealMonProce
                oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcReal = oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcReal + .Adjudicado
                oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcRealMonProce = oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcRealMonProce + .AdjudicadoMonProce
            End With
        End If
    Next  'oProve

    With oGrupo
        .Consumido = dConsumidoGrupo
        .Adjudicado = dAdjudicadoGrupo
        .AdjudicadoTotal = dAdjudicadoTotalGrupo
        .AdjudicadoReal = dAdjudicadoGrupoReal
        .AdjudicadoTotalReal = dAdjudicadoTotalGrupoReal
        .ObjetivoImp = dObjetivoGrupo
        .ObjetivoAhorro = dObjetivoAhoGrupo

        ImportesProc.AbiertoProc = ImportesProc.AbiertoProc + .Abierto
    End With
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If err.Number <> 0 Then
        MsgBox err.Description, vbCritical, "Fullstep"
        Exit Sub
    End If
End Sub

''' <summary>Calcula el importe adj. aplicando las f�rmulas de total del item</summary>
''' <param name="sGrupo">Codigo del grupo seleccionado</param>
''' <remarks>Llamada desde: CalcularGrupoConFormulas    CalcularPrecioConFormulas; Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 03/10/2011</revision>

Private Sub AplicarAtributosNivelItem(ByRef oProce As cProceso, ByVal sGrupo As String, ByRef oProves As CProveedores, ByRef ImportesProc As ImportesProceso, ByRef oAtribsFormulas As CAtributos, _
        Optional ByRef oAdjsProveEsc As CAdjudicaciones)
    Dim oProv As CProveedor
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim sCod As String
    Dim oOfertaGr As COferta
    Dim oAdj As CAdjudicacion
    Dim bAplicTotalItem As Boolean
    Dim dImporteAdj As Double
    Dim dCantidad As Double
    Dim dCantidadProve As Double
    Dim dPorcen As Double
    Dim dImporteAdjProve As Double
    Dim dImporteProve As Double
    Dim dAbiertoGrupo As Double
    Dim vPrecioItem As Variant
    Dim vObjetivoItem As Variant
    Dim lIdEscalado As Long
    Dim oAdjTotProveEsc As CAdjudicacion
    Dim dConsumidoItemEsc As Double
    Dim vCons As Variant
    Dim dblImporteAux As Double
    
    'Calcula el importe adj. aplicando las f�rmulas de total del item
    On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGrupo = oProce.Grupos.Item(sGrupo)
    
    'esto esta mirado en CalcularPrecio con f�rmulas que llama a esta, quito ese codigo:
    'si es un proceso de administraci�n p�blica,y el sobre asociado al grupo no est� visible no se
    'le aplican los atributos,
    dAbiertoGrupo = 0
    For Each oItem In oGrupo.Items
        With oItem
            If .Confirmado Then
                vPrecioItem = .Precio
                vObjetivoItem = .Objetivo
                If oGrupo.UsarEscalados Then
                    'Si el grupo tiene escalados y el item no tiene precio se calcula en funci�n de las adjudicaciones
                    PresupuestoYObjetivoItemConEscalados oGrupo, oItem, oProves, vPrecioItem, vObjetivoItem
                End If
                                
                dAbiertoGrupo = dAbiertoGrupo + (NullToDbl0(.Cantidad) * NullToDbl0(vPrecioItem))
                ImportesProc.AbiertoProcReal = ImportesProc.AbiertoProcReal + (NullToDbl0(.Cantidad) * NullToDbl0(vPrecioItem))
                
                dImporteAdj = 0
                dCantidad = 0 'Para calcular el consumido del �tem
                dPorcen = 0 'Para calcular el consumido del �tem
                dImporteAdjProve = 0
                dImporteProve = 0
                dConsumidoItemEsc = 0
                    
                bAplicTotalItem = ComprobarAplicarAtribsItem(oAtribsFormulas, CStr(.Id), .grupoCod)
                
                For Each oProv In oProves
                    dImporteAdjProve = 0
                    dImporteProve = 0
                    dCantidadProve = 0
                    
                    sCod = oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
                    sCod = CStr(.Id) & sCod
                
                    Set oOfertaGr = oGrupo.UltimasOfertas.Item(oProv.Cod)
                    If oGrupo.UsarEscalados Then
                        Set oAdj = oAdjsProveEsc.Item(sCod)
                    Else
                        Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
                    End If
                    If Not oOfertaGr Is Nothing Then
                        If Not oAdj Is Nothing Then
                            'Importe adjudicado al proveedor sin atributos
                            If oGrupo.UsarEscalados Then
                                dImporteAdjProve = oAdj.ImporteAdj
                                dCantidad = dCantidad + oAdj.Adjudicado
                                dCantidadProve = dCantidadProve + oAdj.Adjudicado
                                dPorcen = dPorcen + oAdj.Porcentaje
                            Else
                                dImporteAdjProve = NullToDbl0(oOfertaGr.Lineas.Item(CStr(.Id)).PrecioOferta) * (.Cantidad * oAdj.Porcentaje / 100)
                                dCantidad = dCantidad + (.Cantidad * oAdj.Porcentaje / 100)
                                dPorcen = dPorcen + oAdj.Porcentaje
                                dConsumidoItemEsc = dConsumidoItemEsc + (.Cantidad * oAdj.Porcentaje / 100) * vPrecioItem 'Moneda proceso
                            End If
                        End If
                        'Importe hipotetico si adjudicas todo el item al proveedor
                        If oGrupo.UsarEscalados Then
                            'Si el item tiene cantidad se calcula el importe �ptimo
                            If NullToDbl0(oItem.Cantidad) > 0 Then
                                ImporteOptimoItemEsc oProv.Cod, oItem, oOfertaGr, lIdEscalado, oProce, oAtribsFormulas, dblImporteAux, dImporteProve
                            Else
                                Set oAdjTotProveEsc = CalcularAdjTotalProveedorEsc(oProv.Cod, oItem.Id, oProce, oGrupo, oOfertaGr, oProves, oAtribsFormulas)
                                If Not oAdjTotProveEsc Is Nothing Then
                                    dImporteProve = oAdjTotProveEsc.ImporteAdj
                                End If
                            End If
                        Else
                            dImporteProve = (NullToDbl0(oOfertaGr.Lineas.Item(CStr(.Id)).PrecioOferta) * NullToDbl0(.Cantidad))
                        End If
                    
                        'Si el grupo tiene escalados los atributos de total de item ya est�n incluidos en oAdj.ImporteAdj
                        If bAplicTotalItem And oGrupo.UsarEscalados = 0 Then
                            If Not IsNull(oOfertaGr.Lineas.Item(CStr(.Id)).PrecioOferta) Then
                                dblImporteAux = dImporteAdjProve 'Uso esa vble global
                                dImporteProve = AplicarAtributos(oProce, oAtribsFormulas, dImporteProve, TotalItem, oProv.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, .Id)
                                If Not oAdj Is Nothing Then dImporteAdjProve = dblImporteAux
                            End If
                        End If
                        
                        'Importe adjudicado de todo el item
                        dImporteAdj = dImporteAdj + (dImporteAdjProve / .CambioComparativa(oProv.Cod))
                        
                        'Va recalculando los valores de importe adj. para ese proveedor
                        If Not oAdj Is Nothing Then
                            If Not oOfertaGr Is Nothing Then
                                'Guarda el importe adjudicado obtenido en la colecci�n (sin atributos de total de �tem)
                                If oGrupo.UsarEscalados Then
                                    oAdj.ImporteAdj = ImporteAdjudicadoProveedor(oProce, oGrupo.Adjudicaciones, oAtribsFormulas, oGrupo, oOfertaGr, oProv.Cod, .Id, oAdj.Adjudicado)
                                Else
                                    oAdj.ImporteAdj = NullToDbl0(oOfertaGr.Lineas.Item(CStr(.Id)).PrecioOferta) * NullToDbl0((.Cantidad * oAdj.Porcentaje / 100))
                                End If
                            End If
                            oAdj.ImporteAdjTot = dImporteAdjProve   'Importe adjudicado con atributos de total de �tem
                            oAdj.importe = dImporteProve
                        End If
                        oOfertaGr.Lineas.Item(CStr(.Id)).importe = dImporteProve    'TOTAL DE ITEM CON ATRIBUTOS
                    End If
                    If oGrupo.UsarEscalados Then
                        vCons = ConsumidoProveedor(oGrupo, oProv.Cod, oItem.Id, dCantidadProve)
                        If Not oAdj Is Nothing Then
                            oAdj.ConsumidoItemProve = CDec(NullToDbl0(vCons) * .CambioComparativa(oProv.Cod))
                        End If
                        dConsumidoItemEsc = dConsumidoItemEsc + NullToDbl0(vCons)
                    End If
                    
                    Set oOfertaGr = Nothing
                    Set oAdj = Nothing
                    
                Next 'oProv
                
                .ImporteAdj = dImporteAdj 'Importe adjudicado de todo el item
                .CantidadCons = dCantidad
                .PorcenCons = dPorcen
                .Consumido = dConsumidoItemEsc
                
                ImportesProc.ConsumidoProcReal = ImportesProc.ConsumidoProcReal + (NullToDbl0(.Consumido))
            End If
        End With
    Next 'oItem
    oGrupo.Abierto = dAbiertoGrupo
    
    Set oGrupo = Nothing
    Set oAdjTotProveEsc = Nothing
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
   If err.Number <> 0 Then
      MsgBox err.Description, vbCritical, "Fullstep"
      Exit Sub
   End If
End Sub

''' <summary>Realiza los c�lculos de pantalla para un grupo con escalados</summary>
''' <param name="oGrupo">Grupo para el que se est�n llevando a cabo los c�lculos</param>
''' <remarks>Llamada desde: CalcularPrecioConFormulas</remarks>

Private Sub CalcularPrecioConFormulasGrupoEsc(ByRef oProce As cProceso, ByVal oGrupo As CGrupo, ByRef oAtribsFormulas As CAtributos, ByRef oAdjsGrupo As CAdjsGrupo, ByRef ImportesProc As ImportesProceso, _
        ByRef oProves As CProveedores, ByRef oAdjsProveEsc As CAdjudicaciones)
    Dim oProve As CProveedor
    Dim oItem As CItem
    Dim oPrecItem As CPrecioItem
    Dim oOfertaGr As COferta
    Dim oAdj As CAdjudicacion
    Dim oEscalado As CEscalado
    Dim dAdjudicadoTotalGrupo As Double
    Dim dAdjudicadoGrupoReal As Double
    Dim dAdjudicadoTotalGrupoReal As Double
    Dim dConsumidoGrupo As Double
    Dim dObjetivoGrupo As Double
    Dim dObjetivoAhoGrupo As Double
    Dim dPrecioAplicFormula As Variant
    Dim bAplicarAtribsGrupo As Boolean
    Dim bAdjudicado As Boolean
    Dim dAdjudicado As Double
    Dim dAdjudicadoMonProce As Double
    Dim dAdjudicadoGrupo As Double    'Para los totales de grupo de CGrupo
    Dim dAdjudicadoReal As Double
    Dim dAdjudicadoRealMonProce As Double
    Dim dAdjudicadoItems As Double
    Dim dAdjudicadoItemsMonProce As Double
    Dim dConsumido As Double
    Dim dConsumidoMonProce As Double
    Dim dConsumidoReal As Double
    Dim dConsumidoRealMonProce As Double
    Dim dImporte As Double
    Dim dImporteMonProce As Double
    Dim dAhorroOfeAb As Double
    Dim dAhorroOfeAbMonProce As Double
    Dim bHayPrecios As Boolean
    Dim sCod As String
    Dim bDistintos As Boolean
    Dim vPrecioItem As Variant
    Dim vCantidadItem As Variant
    Dim vObjetivoItem As Variant
    Dim dblCambio As Double
    Dim dblImporteAux As Double
    
    'Para los totales de grupo de CGrupo
    On Error GoTo ERROR

    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dAdjudicadoGrupo = 0
    dAdjudicadoTotalGrupo = 0
    dAdjudicadoGrupoReal = 0
    dAdjudicadoTotalGrupoReal = 0
    dConsumidoGrupo = 0
    dObjetivoGrupo = 0
    dObjetivoAhoGrupo = 0
    
    'Ahora empieza el c�lculo del precio unitario
    For Each oProve In oProves
        For Each oItem In oGrupo.Items
            If oItem.Confirmado And (Not oItem.Cerrado) Then
                If Not oGrupo.UltimasOfertas.Item(oProve.Cod) Is Nothing Then
                    Set oPrecItem = oGrupo.UltimasOfertas.Item(oProve.Cod).Lineas.Item(CStr(oItem.Id))
                    If Not oPrecItem Is Nothing Then
                        If Not oPrecItem.Escalados Is Nothing Then
                            For Each oEscalado In oPrecItem.Escalados
                                dPrecioAplicFormula = oEscalado.Precio
                                dPrecioAplicFormula = AplicarAtributos(oProce, oAtribsFormulas, dPrecioAplicFormula, TipoAplicarAPrecio.UnitarioItem, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, oItem.Id, oEscalado.Id)

                                'Guarda el precio v�lido obtenido en la colecci�n, en moneda de la oferta
                                oEscalado.PrecioOferta = dPrecioAplicFormula
                            Next
                        End If
                    End If
                End If
            End If
        Next    'oItem
    Next oProve

    '**** APLICA LOS ATRIBUTOS A APLICAR AL TOTAL DEL ITEM ****
    AplicarAtributosNivelItem oProce, oGrupo.Codigo, oProves, ImportesProc, oAtribsFormulas, oAdjsProveEsc

    bAplicarAtribsGrupo = True
    If Not oAtribsFormulas Is Nothing Then bAplicarAtribsGrupo = oAtribsFormulas.ComprobarAplicarAtribsGrupo(oGrupo.Codigo)

    'Calculamos consumido y adjudicado
    For Each oProve In oProves
        'Todo en moneda de la ofeerta
        bAdjudicado = False
        dAdjudicado = 0    'Adjudicado del grupo para un proveedor - con atribs
        dAdjudicadoMonProce = 0
        dAdjudicadoReal = 0    'Adjudicado del grupo para un proveedor - con atribs con todos los cerrados
        dAdjudicadoRealMonProce = 0
        dAdjudicadoItems = 0    'Suma del Adjudicado de los items del grupo para un proveedor - sin atribs de total item
        dAdjudicadoItemsMonProce = 0
        dConsumido = 0  'Consumido del grupo para un proveedor
        dConsumidoMonProce = 0
        dConsumidoReal = 0
        dImporte = 0    'Adjudicado hipotetico del grupo para un proveedor - con atribs
        dAhorroOfeAb = 0    'Abierto del grupo (suma de los items
        dAhorroOfeAbMonProce = 0
        bHayPrecios = False

        Set oOfertaGr = oGrupo.UltimasOfertas.Item(oProve.Cod)

        For Each oItem In oGrupo.Items
            With oItem
                If .Confirmado Then
                    If Not oOfertaGr Is Nothing Then
                        If Not oOfertaGr.Lineas.Item(CStr(.Id)) Is Nothing Then
                            sCod = oOfertaGr.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oOfertaGr.Prove))
                            sCod = CStr(.Id) & sCod
                            Set oAdj = oAdjsProveEsc.Item(sCod)

                            dblCambio = .CambioComparativa(oOfertaGr.Prove)

                            'C�culo del precio, objetivo y cantidad del item
                            PresupuestoYObjetivoItemConEscalados oGrupo, oItem, oProves, vPrecioItem, vObjetivoItem
                            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) Then vCantidadItem = .Cantidad

                            'Va recalculando los valores de consumido y adjudicado para el grupo
                            If Not oAdj Is Nothing Then
                                dAdjudicado = dAdjudicado + VarToDbl0(oAdj.ImporteAdjTot)
                                dAdjudicadoMonProce = dAdjudicadoMonProce + (VarToDbl0(oAdj.ImporteAdjTot) / dblCambio)
                                dAdjudicadoItems = dAdjudicadoItems + VarToDbl0(oAdj.ImporteAdj)
                                dAdjudicadoItemsMonProce = dAdjudicadoItemsMonProce + (VarToDbl0(oAdj.ImporteAdj) / dblCambio)
                                dImporte = dImporte + VarToDbl0(oAdj.importe)
                                dImporteMonProce = dImporteMonProce + (VarToDbl0(oAdj.importe) / dblCambio)
                                oAdj.ConsumidoItemProve = CDec(ConsumidoProveedor(oGrupo, oProve.Cod, oItem.Id, oAdj.Adjudicado) * dblCambio)
                                dConsumido = dConsumido + oAdj.ConsumidoItemProve
                                dConsumidoMonProce = dConsumidoMonProce + CDec(ConsumidoProveedor(oGrupo, oProve.Cod, oItem.Id, oAdj.Adjudicado))

                                'Calculo el total objetivos para ALL, el de grupo se calcular� al ir a cada pesta�a
                                If Not IsNull(vObjetivoItem) Then
                                    dObjetivoGrupo = dObjetivoGrupo + (vCantidadItem * (oAdj.Porcentaje / 100) * NullToDbl0(vObjetivoItem))
                                    dObjetivoAhoGrupo = dObjetivoAhoGrupo + (NullToDbl0(vPrecioItem) * vCantidadItem * (oAdj.Porcentaje / 100)) - (NullToDbl0(vObjetivoItem) * vCantidadItem * (oAdj.Porcentaje / 100))
                                    'Importe (Objetivo * Cantidad)
                                    ImportesProc.TotalProveObjAll = ImportesProc.TotalProveObjAll + (vCantidadItem * (oAdj.Porcentaje / 100) * NullToDbl0(vObjetivoItem))
                                    'Ahorro ((Presupuesto*Cantidad) - (Objetivo * Cantidad))
                                    ImportesProc.TotalAhorroProveObjAll = ImportesProc.TotalAhorroProveObjAll + (NullToDbl0(vPrecioItem) * vCantidadItem * (oAdj.Porcentaje / 100)) - (NullToDbl0(vObjetivoItem) * vCantidadItem * (oAdj.Porcentaje / 100))
                                End If
                            Else
                                dImporte = dImporte + (NullToDbl0(oOfertaGr.Lineas.Item(CStr(oItem.Id)).importe))
                            End If
                            If Not IsNull(oOfertaGr.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                                dAhorroOfeAb = dAhorroOfeAb + (NullToDbl0(vCantidadItem) * NullToDbl0(vPrecioItem) * dblCambio)
                                dAhorroOfeAbMonProce = dAhorroOfeAbMonProce + (NullToDbl0(vCantidadItem) * NullToDbl0(vPrecioItem))
                                bHayPrecios = True
                            End If
                            If Not oAdj Is Nothing Then    'Esto es para el Adjudicado real del proceso
                                dAdjudicadoReal = dAdjudicadoReal + VarToDbl0(oAdj.ImporteAdjTot)
                                dConsumidoReal = dConsumidoReal + ((NullToDbl0(vPrecioItem) * oOfertaGr.Cambio) * (vCantidadItem * (oAdj.Porcentaje / 100)))
                                dConsumidoRealMonProce = dConsumidoRealMonProce + ((NullToDbl0(vPrecioItem)) * (vCantidadItem * (oAdj.Porcentaje / 100)))
                                bAdjudicado = True
                            End If
                        End If
                    End If  'si hay ofertas
                End If
            End With
        Next  'oItem

        '**** APLICA LOS ATRIBUTOS A APLICAR AL TOTAL DEL GRUPO ****
        Set oOfertaGr = oProce.Ofertas.Item(oProve.Cod)
        sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
        If Not oOfertaGr Is Nothing Then
            'Ahora aplica las operaciones para los atributos de grupo.Aplica las f�rmulas al importe obtenido

            If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                'Guardo los valores antes de aplicar los atributos
                oAdjsGrupo.Item(sCod).AdjudicadoSin = dAdjudicado
                oAdjsGrupo.Item(sCod).AdjudicadoSinMonProce = dAdjudicadoMonProce
                oAdjsGrupo.Item(sCod).AdjudicadoSinReal = dAdjudicadoReal
                oAdjsGrupo.Item(sCod).AdjudicadoSinRealMonProce = dAdjudicadoRealMonProce
                oAdjsGrupo.Item(sCod).AdjudicadoItems = dAdjudicadoItems
                oAdjsGrupo.Item(sCod).AdjudicadoItemsMonproce = dAdjudicadoItemsMonProce
                oAdjsGrupo.Item(sCod).AdjudicadoOfe = dImporte
                oAdjsGrupo.Item(sCod).AdjudicadoOfeMonProce = dImporteMonProce

                'Aplica los atributos DE GRUPO
                If bAplicarAtribsGrupo Then
                    bDistintos = False
                    If dAdjudicadoReal <> dAdjudicado Then
                        If bAdjudicado Then    'Si hay algo adjudicado, sino no se aplica
                            dblImporteAux = AplicarAtributos(oProce, oAtribsFormulas, dAdjudicadoReal, TotalGrupo, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                            If dblImporteAux <> 0 Then dAdjudicadoReal = dblImporteAux
                        End If
                        bDistintos = True
                    End If
                    dblImporteAux = 0
                    If bAdjudicado Then dblImporteAux = dAdjudicado
                    If bHayPrecios Then dImporte = AplicarAtributos(oProce, oAtribsFormulas, dImporte, TotalGrupo, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                    If bAdjudicado Then
                        dAdjudicado = dblImporteAux
                        If Not bDistintos Then dAdjudicadoReal = dblImporteAux
                    End If

                    'En moneda de proceso
                    dblImporteAux = 0
                    If bAdjudicado Then dblImporteAux = dAdjudicadoMonProce
                    If bHayPrecios Then dImporteMonProce = AplicarAtributos(oProce, oAtribsFormulas, dImporteMonProce, TotalGrupo, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                                            oGrupo.Codigo, , , oGrupo.CambioComparativa(oProve.Cod))
                    If bAdjudicado Then
                        dAdjudicadoMonProce = dblImporteAux
                        If Not bDistintos Then dAdjudicadoRealMonProce = dblImporteAux
                    End If
                End If
            End If
        End If
        Set oOfertaGr = Nothing

        If dAdjudicado <> 0 Then dAdjudicadoTotalGrupo = dAdjudicadoTotalGrupo + dAdjudicadoMonProce
        If dAdjudicadoReal <> 0 Then dAdjudicadoTotalGrupoReal = dAdjudicadoTotalGrupoReal + dAdjudicadoRealMonProce

        'Almacena el importe,adjudicado,consumido,ahorrado,ahorradoporcen,ahorro ofe,... para el proveedor
        If Not oAdjsGrupo.Item(sCod) Is Nothing Then
            With oAdjsGrupo.Item(sCod)
                'Todo en moneda de la oferta
                .Adjudicado = dAdjudicado    'Adjudicado del grupo para un proveedor - con atribs
                .AdjudicadoMonProce = dAdjudicadoMonProce
                .AdjudicadoReal = dAdjudicadoReal
                .AdjudicadoRealMonProce = dAdjudicadoRealMonProce
                .Consumido = dConsumido   'Consumido del grupo para un proveedor
                .ConsumidoMonProce = dConsumidoMonProce
                .ConsumidoReal = dConsumidoReal
                .ConsumidoRealMonProce = dConsumidoRealMonProce
                .Ahorrado = (dConsumido - dAdjudicado)  'Ahorro del grupo para un proveedor
                If dConsumido = 0 Then
                    .AhorradoPorcen = 0
                Else
                    .AhorradoPorcen = ((dConsumidoMonProce - dAdjudicadoMonProce) / Abs(dConsumidoMonProce)) * 100
                End If

                .importe = dImporte                     'Adjudicado hipotetico del grupo para un proveedor - con atribs  'TOTAL DE GRUPO CON ATRIBUTOS DE GRUPO
                .AbiertoOfe = dAhorroOfeAb              'Consumido hipotetico (abierto) del grupo para un prove
                .AbiertoOfeMonProce = dAhorroOfeAbMonProce
                .AhorroOfe = dAhorroOfeAb - dImporte    'Ahorro hipotetico del grupo para un prove
                .AhorroOfeMonProce = dAhorroOfeAbMonProce - dImporteMonProce
                If dAhorroOfeAb = 0 Then
                    .AhorroOfePorcen = 0
                Else
                    .AhorroOfePorcen = ((dAhorroOfeAbMonProce - dImporteMonProce) / Abs(dAhorroOfeAbMonProce)) * 100
                End If

                .HayPrecios = bHayPrecios

                'Calcula los totales proceso en moneda del proceso
                'Si el grupo est� cerrado no se muestra en los resultados
                ImportesProc.ConsumidoProc = ImportesProc.ConsumidoProc + CDec(.ConsumidoMonProce)
                ImportesProc.AdjudicadoSinProc = ImportesProc.AdjudicadoSinProc + CDec(.AdjudicadoMonProce)
                oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProc = oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProc + .Adjudicado
                oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcMonProce = oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcMonProce + .AdjudicadoMonProce
                ImportesProc.AdjudicadoSinProcReal = ImportesProc.AdjudicadoSinProcReal + CDec(.AdjudicadoRealMonProce)
                oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcReal = oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcReal + .Adjudicado
                oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcRealMonProce = oProce.Ofertas.Item(oProve.Cod).AdjudicadoSinProcRealMonProce + .AdjudicadoMonProce
            End With
        End If
    Next  'oProve

    With oGrupo
        .Consumido = dConsumidoGrupo
        .Adjudicado = dAdjudicadoGrupo
        .AdjudicadoTotal = dAdjudicadoTotalGrupo
        .AdjudicadoReal = dAdjudicadoGrupoReal
        .AdjudicadoTotalReal = dAdjudicadoTotalGrupoReal
        .ObjetivoImp = dObjetivoGrupo
        .ObjetivoAhorro = dObjetivoAhoGrupo

        ImportesProc.AbiertoProc = ImportesProc.AbiertoProc + .Abierto
    End With
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If err.Number <> 0 Then
        MsgBox err.Description, vbCritical, "Fullstep"
        Exit Sub
    End If
End Sub

''' <summary>Aplica atributos de proceos</summary>
''' <remarks>Llamada desde: ctlADJEscalado.Recalcular; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG  13/10/2011</revision>
Private Sub AplicarAtributosNivelProceso(ByRef oProce As cProceso, ByRef oProves As CProveedores, ByRef oAdjsGrupo As CAdjsGrupo, ByRef oAtribsFormulas As CAtributos, ByRef ImportesProc As ImportesProceso)
    Dim oProve As CProveedor
    Dim oGrupo As CGrupo
    Dim oOferta As COferta
    Dim sCod As String
    Dim dblImporteOfertaSinAtrib As Double
    Dim dblImporteOfertaConAtrib As Double
    Dim dblAdjudicadoMonProcAux As Double
    Dim dblImporteAux As Double

    'Calculamos el total de oferta con y sin atributos de total de oferta(es el total hipotetico como si aplicaramos el 100%).
    On Error GoTo ERROR

    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dblAdjudicadoMonProcAux = 0
    For Each oProve In oProves
        dblImporteOfertaSinAtrib = 0
        dblImporteOfertaConAtrib = 0
        Set oOferta = oProce.Ofertas.Item(oProve.Cod)
        For Each oGrupo In oProce.Grupos
            sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                If Not oOferta Is Nothing Then
                    dblImporteOfertaSinAtrib = dblImporteOfertaSinAtrib + oAdjsGrupo.Item(sCod).importe  'Sumamos los totales de grupo con los atributos aplicados para cada proveedor.
                End If
            End If
        Next
        If Not oOferta Is Nothing Then
            dblImporteOfertaConAtrib = AplicarAtributos(oProce, oAtribsFormulas, dblImporteOfertaSinAtrib, TotalOferta, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE)
            oProce.Ofertas.Item(oProve.Cod).ImporteOfertaSinAtrib = dblImporteOfertaSinAtrib
            oProce.Ofertas.Item(oProve.Cod).ImporteOfertaConAtrib = dblImporteOfertaConAtrib

            'Adjudicado
            If oOferta.AdjudicadoSinProc <> 0 Then
                dblAdjudicadoMonProcAux = dblAdjudicadoMonProcAux + (AplicarAtributos(oProce, oAtribsFormulas, oOferta.AdjudicadoSinProcMonProce, TotalOferta, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE))
            End If
        End If
        Set oOferta = Nothing
    Next

    ImportesProc.AdjudicadoProc = dblAdjudicadoMonProcAux
    ImportesProc.AdjudicadoProcReal = dblAdjudicadoMonProcAux

    'Muestra los resultados totales del proceso en la grid de resultados
    ImportesProc.AhorradoProc = ImportesProc.ConsumidoProc - ImportesProc.AdjudicadoProc
    If ImportesProc.ConsumidoProc = 0 Then
        ImportesProc.AhorradoPorcenProc = 0
    Else
        ImportesProc.AhorradoPorcenProc = (ImportesProc.AhorradoProc / Abs(ImportesProc.ConsumidoProc)) * 100
    End If
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If err.Number <> 0 Then
        MsgBox err.Description, vbCritical, "Fullstep"
        Exit Sub
    End If
End Sub

''' <summary>
''' Rellenar un recordset con la informaci�n de todos los items de todos los grupos de todos los procesos de una reuni�n
''' </summary>
''' <param name="oReport">Objeto report q esta siendo imprimido</param>
''' <param name="iNumProceso">Numero de proceso dentro de la reuni�n</param>
''' <param name="adoItems">Recordset a rellenar</param>
''' <returns>Recordset con la informaci�n de todos los items de todos los grupos de todos los procesos de una reuni�n</returns>
''' <remarks>Llamada desde: CrearActaCrystal; Tiempo m�ximo:0,4</remarks>
Private Function TipoActa1Crystal(ByVal iNumProceso As Long, ByVal adoItems As ADODB.Recordset) As ADODB.Recordset  ' As Boolean
    Dim oProveedores As CProveedores
    Dim oProve As CProveedor
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oItem As CItem
    Dim oIAdj As IAdjudicaciones
    Dim dCantProve As Double
    Dim oOferta As COferta
    Dim oGrupo As CGrupo
    Dim bMostrarGr As Boolean
    Dim bError As Boolean
    Dim oOfertaNoAdj As COferta
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim dImporte As Double
    Dim sCod As String
    Dim bCargadoAtribOferta As Boolean
    
    On Error GoTo ERROR:
     
    bError = False
    
    If oProce.Grupos.Count > 0 Then

'        oProce.CargarPreciosOfertaGrupos
'        oProce.CargarAdjudicacionesGrupos frmRESREU.sdbcFecReu.Columns(0).Value

        For Each oGrupo In oProce.Grupos

            bMostrarGr = True
            
            bCargadoAtribOferta = False
            
            If oProce.AdminPublica = True Then
                If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                    bMostrarGr = False
                End If
            End If

            If bMostrarGr = True Then
                oGrupo.CargarTodosLosItems OrdItemPorOrden
                If oGrupo.Items.Count > 0 Then
                    For Each oItem In oGrupo.Items

                        Set oIAdj = oProce
                        Set oAdjs = oIAdj.DevolverAdjudicacionesDeItemConFecha(oItem.Id, CDate(frmRESREU.sdbcFecReu.Columns(0).Value))

                        'Adjudicados, si est� adjudicado estar� en ese grupo
                        If oAdjs.Count > 0 Then
                            For Each oAdj In oAdjs
                                sCod = oAdj.ProveCod
                                Set oProve = oProves.Item(sCod)

                                Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oAdj.ProveCod))
                                If Not oOferta Is Nothing Then
                                    bCargar = True
                                    If gParametrosGenerales.gbProveGrupos Then
                                        scodProve = oAdj.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdj.ProveCod))
                                        If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                            bCargar = False
                                        End If
                                    End If
                                    If bCargar Then
                                        adoItems.AddNew
                                        Set adoItems = CargarDatosGrupo(adoItems, oGrupo, oItem, bCargadoAtribOferta)
                                        
                                        bCargadoAtribOferta = True
                                        
                                        adoItems("NUM_PROCESO").Value = iNumProceso
                                        adoItems("COD_PROCESO").Value = CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
                                        adoItems("COD_GRUPO") = oGrupo.Codigo
                                        adoItems("DEN_GRUPO") = oGrupo.Den

                                        adoItems("COD_PROVEEDOR").Value = oAdj.ProveCod
                                        adoItems("NOM_PROVEEDOR").Value = oProve.Den

                                        adoItems("PRECIO_PROVEEDOR").Value = oAdj.Precio
                                        adoItems("CANT_PROVEEDOR").Value = (oAdj.Porcentaje / 100) * oItem.Cantidad

                                        dCantProve = ((oAdj.Porcentaje / 100) * oItem.Cantidad)
                                        adoItems("COD_UNI_PROVEEDOR").Value = oItem.UniCod

                                        If Not oUnidades.Item(oItem.UniCod) Is Nothing Then
                                            adoItems("DEN_UNI_PROVEEDOR").Value = oUnidades.Item(oItem.UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                        End If

                                        If Not HayAplicar(TotalItem) Then
                                            adoItems("TOTAL_PROVEEDOR").Value = NullToDbl0(dCantProve) * NullToDbl0(oAdj.Precio) 'aplicados los atributos de total de item
                                        Else
                                            'Aplica los atributos de total de item:
                                            dImporte = AplicarAtributosTotalItem(oGrupo.Codigo, oItem.Id, oAdj.ProveCod)
                                            adoItems("TOTAL_PROVEEDOR").Value = dImporte 'aplicados los atributos de total de item
                                        End If
                                        adoItems("OBS_PROVEEDOR").Value = adoItems("OBS_PROVEEDOR").Value & NullToStr(oProce.Ofertas.Item(CStr(oAdj.ProveCod)).obs)

                                        adoItems("COD_PROVEEDOR").Value = oAdj.ProveCod
                                        If IsNull(oProve.DenPais) Or IsEmpty(oProve.DenPais) Or oProve.DenPais = "" Then
                                            oProves.CargarDatosProveedor oAdj.ProveCod
                                        End If

'                                        If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
'                                            adoItems("PRECIO_PROVEEDOR").Value = oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta
'                                        End If
                                    End If 'bCargar
                                End If 'oferta
                                Set oOferta = Nothing
                            Next
                        End If

                        If chkProvNoAdj.Value = vbChecked Then
                            For Each oProve In oProvesnoAdj
                                Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                If Not oOferta Is Nothing Then
                                    bCargar = True
                                    If gParametrosGenerales.gbProveGrupos Then
                                        scodProve = oOfertaNoAdj.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oOfertaNoAdj.Prove))
                                        If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                            bCargar = False
                                        End If
                                    End If
                                    If bCargar Then
                                        adoItems.AddNew
                                        Set adoItems = CargarDatosGrupo(adoItems, oGrupo, oItem, bCargadoAtribOferta)
                                        
                                        bCargadoAtribOferta = True
                                        
                                        adoItems("NUM_PROCESO").Value = iNumProceso
                                        adoItems("COD_PROCESO").Value = CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
                                        adoItems("COD_ITEM").Value = NullToStr(oItem.ArticuloCod)
                                        adoItems("ID_ITEM").Value = oItem.Id
                                        adoItems("COD_PROVEEDOR_NOADJ").Value = oProve.Cod
                                        adoItems("NOM_PROVEEDOR_NOADJ").Value = oProve.Den
                                        If IsNull(oProve.DenPais) Or IsEmpty(oProve.DenPais) Or oProve.DenPais = "" Then
                                            oProvesnoAdj.CargarDatosProveedor oProve.Cod
                                        End If

                                        If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                                            adoItems("PRECIO_PROVEEDOR_NOADJ").Value = (oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta / oOferta.Cambio) ' FormateoNumerico(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta, m_sFormatoNumberPrecioGr)
                                        End If

                                        adoItems("OBS_PROVEEDOR_NOADJ").Value = adoItems("OBS_PROVEEDOR_NOADJ").Value & NullToStr(oOferta.obs)
                                    End If 'bCargar
                                End If 'oferta
                                Set oOferta = Nothing
                            Next
                        End If
                Next 'items
            End If 'items.count
        End If 'MostrarGr
    Next 'GRUPO
End If
            
Finalizar:
    On Error Resume Next
    
    If bError Then
        Set TipoActa1Crystal = Nothing
    Else
        Set TipoActa1Crystal = adoItems
    End If
    
    Set oProveedores = Nothing
    Set oAdjs = Nothing
    Set oAdj = Nothing
    Set oItem = Nothing
    Set oIAdj = Nothing
    Set oOferta = Nothing
    Set oGrupo = Nothing
    Set adoItems = Nothing
    
    Exit Function
    
ERROR:
    
    'Si el error es que no encuentra un formato
    If err.Number = 5834 Or err.Number = 5941 Or err.Number = 9 Or err.Number = 91 Or err.Number = 4605 Then
        Resume Next
    End If
    
    If err.Number = 11 Then
    'Division by 0
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    
    Unload frmESPERA
    
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar

End Function

''' <summary>
''' Crea acta cristal de no adjudicados
''' </summary>
''' <param name="oReport">Objeto report q esta siendo imprimido</param>
''' <param name="adoProv">proveedor</param>
''' <param name="adoProvNoAdj">proveedores no adjudicados</param>
''' <returns>Recordset con la informaci�n de todos los items de todos los grupos de todos los procesos de una reuni�n</returns>
''' <remarks>Llamada desde: CrearActaCrystal; Tiempo m�ximo:0,4</remarks>
Private Function TipoActa2Crystal(ByRef adoProv As ADODB.Recordset, ByRef adoProvNoAdj As ADODB.Recordset) As Boolean
    Dim vConsumido As Variant
    Dim vAdjudicado As Variant
    Dim oProve As CProveedor
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oItem As CItem
    Dim oIAdj As IAdjudicaciones
    Dim sCod As String
    Dim oiasignaciones As IAsignaciones
    Dim oAsignaciones As CAsignaciones
    Dim oEquipo As CEquipo
    Dim oOferta As COferta
    Dim oGrupo As CGrupo
    Dim bMostrarGr As Boolean
    Dim bError As Boolean
    Dim dblConsumido As Double
    Dim dblAdjudicado As Double
    Dim dblAhorrado As Double
    Dim dblAhorradoPorcen As Double
    Dim dblPresupuesto As Double
    Dim dblImporte As Double
    Dim dImporteOfe As Double
    Dim dAhorroOfe As Double
    Dim dAhorroOfePorcen As Double
    Dim dPrecioOfe As Double
    Dim bDestino As Boolean
    Dim bPago As Boolean
    Dim bFecha As Boolean
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim bItems As Boolean
    Dim dblPresAhorr As Double
    Dim dblAdjAhorr As Double
    Dim dblAhorrGR As Double
    Dim strPorcenGR As String
    Dim strObs As String
    
    On Error GoTo ERROR:
    
    bError = False
    
    'Proveedores asignados con el detalle de items asignados por cada uno:
    If oProves.Count > 0 Then
        bItems = False
        
        For Each oProve In oProves
            If IsNull(oProve.DenProvi) Then
                oProves.CargarDatosProveedor oProve.Cod
            End If
                
            dblConsumido = 0
            dblAdjudicado = 0
            dblAhorrado = 0
            dblAhorradoPorcen = 0
            For Each oGrupo In oProce.Grupos
                'Si no est� asignado al grupo no se pone
                bCargar = True
                If gParametrosGenerales.gbProveGrupos Then
                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                        bCargar = False
                    End If
                End If
                If bCargar Then
                    bMostrarGr = True
                    If oProce.AdminPublica = True Then
                        If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                            bMostrarGr = False
                        End If
                    End If
                    If bMostrarGr = True Then
                        sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    
                        If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                            dblConsumido = dblConsumido + (oAdjsGrupo.Item(sCod).Consumido / oAdjsGrupo.Item(sCod).Cambio)
                            dblAdjudicado = dblAdjudicado + (oAdjsGrupo.Item(sCod).Adjudicado / oAdjsGrupo.Item(sCod).Cambio)
                            dblAhorrado = dblAhorrado + (oAdjsGrupo.Item(sCod).Ahorrado / oAdjsGrupo.Item(sCod).Cambio)
                        End If
                    End If
                End If
            Next
            dblAdjudicado = AplicarAtributosDeProceso(dblAdjudicado, oProve.Cod)
            dblAhorrado = dblConsumido - dblAdjudicado
            If dblConsumido = 0 Then
                dblAhorradoPorcen = 0
            Else
                dblAhorradoPorcen = (dblAhorrado / dblConsumido) * 100
            End If
            
            Set oOferta = oProce.DevolverUltimaOfertaProveedor(oProve.Cod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)
            
            'GRUPOS
            If oProce.Grupos.Count > 0 Then
                If Not oAdjsGrupo Is Nothing Then
                
                    For Each oGrupo In oProce.Grupos
                        bCargar = True
                        If gParametrosGenerales.gbProveGrupos Then
                            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                bCargar = False
                            End If
                        End If
                        If bCargar Then
                            bMostrarGr = True
                            If oProce.AdminPublica = True Then
                                If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                    bMostrarGr = False
                                End If
                            End If
                            'Si se muestra el grupo comprueba si tiene adjudicaciones para el proveedor:
                            If bMostrarGr = True Then

                                sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                                                  
                                    dblPresAhorr = oAdjsGrupo.Item(sCod).Consumido
                                    dblAdjAhorr = oAdjsGrupo.Item(sCod).Adjudicado
                                    dblAhorrGR = oAdjsGrupo.Item(sCod).Ahorrado
                                    strPorcenGR = FormateoNumerico(oAdjsGrupo.Item(sCod).AhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                                    
                                   'ITEMS
                                    oGrupo.CargarTodosLosItems OrdItemPorOrden
                                    bItems = True
                                    If HayAplicar(TotalItem) Then
                                        oProce.Ofertas.CargarAtributosItemOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, oGrupo.Id, frmRESREU.sdbcFecReu.Columns(0).Value   'de �tem
                                    End If
                                    
                                    If (oProce.DefDestino = EnItem) Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                                        bDestino = True
                                    Else
                                        bDestino = False
                                    End If
                                    If oProce.DefFormaPago = EnItem Or (oProce.DefFormaPago = EnGrupo And Not oGrupo.DefFormaPago) Then
                                        bPago = True
                                    Else
                                        bPago = False
                                    End If
                                    If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                                        bFecha = True
                                    Else
                                        bFecha = False
                                    End If
                                    If oGrupo.Items.Count > 0 Then
                                    
                                        For Each oItem In oGrupo.Items
                                           
                                            adoProv.AddNew
                                            
                                            adoProv("COD_PROCESO").Value = CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
                                            adoProv("COD_PROVEEDOR").Value = oProve.Cod
                                            adoProv("NOM_PROVEEDOR").Value = oProve.Den
                                            adoProv("PRECIO_PROVEEDOR").Value = dblConsumido
                                            adoProv("ADJ_PROV").Value = dblAdjudicado
                                            adoProv("AHORRO_PROV").Value = dblAhorrado
                                            adoProv("PORCEN_PROV").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                                            adoProv("OBS_PROVEEDOR").Value = adoProv("OBS_PROVEEDOR").Value & NullToStr(oProce.Ofertas.Item(oProve.Cod).obs)
                        
                                            If IsNull(oGrupo.Sobre) Then
                                                adoProv("COD_GRUPO").Value = NullToStr(oGrupo.Codigo)
                                            Else
                                                adoProv("COD_GRUPO").Value = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
                                            End If
                                            adoProv("DEN_GRUPO").Value = NullToStr(oGrupo.Den)
                                            
                                            If oGrupo.DefDestino Then
                                                adoProv("COD_DEST_GR").Value = NullToStr(oGrupo.DestCod)
                                                adoProv("DEN_DEST_GR").Value = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                            End If
    
                                            If oGrupo.DefFechasSum Then
                                                adoProv("FINI_SUM_GR").Value = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
                                                adoProv("FFIN_SUM_GR").Value = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                                            End If
    
                                            If oGrupo.DefFormaPago Then
                                                adoProv("COD_PAGO_GR").Value = NullToStr(oGrupo.PagCod)
                                                adoProv("DEN_PAGO_GR").Value = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                            End If
                                            
                                            adoProv("PRES_AHORR").Value = dblPresAhorr
                                            adoProv("ADJ_AHORR").Value = dblAdjAhorr
                                            adoProv("AHORR_GR").Value = dblAhorrGR
                                            adoProv("PORCEN_GR").Value = strPorcenGR
                                                                                   
                                            Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                            sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                            sCod = CStr(oItem.Id) & sCod
                                            Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
                                            
                                            If Not oAdj Is Nothing Then
                                                adoProv("COD_ITEM").Value = NullToStr(oItem.ArticuloCod)
                                                adoProv("DEN_ITEM").Value = NullToStr(oItem.Descr)
                                                adoProv("ID_ITEM").Value = oItem.Id
                                                If oProce.DefDestino = EnItem Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                                                    adoProv("COD_DEST_ITEM").Value = NullToStr(oItem.DestCod)
                                                    adoProv("DEN_DEST_ITEM").Value = NullToStr(oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                End If

                                                If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                                                    adoProv("FINI_SUM_ITEM").Value = Format(NullToStr(oItem.FechaInicioSuministro), "Short Date")
                                                    adoProv("FFIN_SUM_ITEM").Value = Format(NullToStr(oItem.FechaFinSuministro), "Short Date")
                                                End If
                                                adoProv("CANTIDAD").Value = oItem.Cantidad * (oAdj.Porcentaje / 100)
                                                adoProv("COD_UNI").Value = oItem.UniCod
                                                adoProv("PREC_OFE").Value = VarToDbl0(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta)

                                                vConsumido = oItem.Cantidad * oItem.Precio * (oAdj.Porcentaje / 100)
                                                If Not HayAplicar(TotalItem) Then
                                                    vAdjudicado = NullToDbl0(oItem.Cantidad) * NullToDbl0(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) * (oAdj.Porcentaje / 100)
                                                Else
                                                    vAdjudicado = AplicarAtributosTotalItem(oGrupo.Codigo, oItem.Id, oProve.Cod)
                                                End If
                                                adoProv("IMPORTE").Value = vAdjudicado  'aplicados los atributos de total de item
                                                adoProv("AHORRO").Value = vConsumido - vAdjudicado
                                                If vConsumido <> 0 Then
                                                    adoProv("AHORROPORCEN").Value = FormateoNumerico(((vConsumido - vAdjudicado) / vConsumido) * 100, m_sFormatoNumberPorcenGr) & "%"
                                                Else
                                                    adoProv("AHORROPORCEN").Value = ""
                                                End If
                                            Else
                                                adoProv("COD_ITEM").Value = ""
                                                adoProv("DEN_ITEM").Value = ""
                                                adoProv("ID_ITEM").Value = 0
                                            End If
                                            Set oOferta = Nothing
                                            Set oAdj = Nothing
                                        Next
                                    Else
                                        adoProv.AddNew
                                        adoProv("COD_PROVEEDOR").Value = oProve.Cod
                                        adoProv("NOM_PROVEEDOR").Value = oProve.Den
                                        adoProv("PRECIO_PROVEEDOR").Value = dblConsumido
                                        adoProv("ADJ_PROV").Value = dblAdjudicado
                                        adoProv("AHORRO_PROV").Value = dblAhorrado
                                        adoProv("PORCEN_PROV").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                                        adoProv("OBS_PROVEEDOR").Value = adoProv("OBS_PROVEEDOR").Value & NullToStr(oProce.Ofertas.Item(oProve.Cod).obs)
                    
                                        If IsNull(oGrupo.Sobre) Then
                                            adoProv("COD_GRUPO").Value = NullToStr(oGrupo.Codigo)
                                        Else
                                            adoProv("COD_GRUPO").Value = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
                                        End If
                                        adoProv("DEN_GRUPO").Value = NullToStr(oGrupo.Den)
                                        
                                        If oGrupo.DefDestino Then
                                            adoProv("COD_DEST_GR").Value = NullToStr(oGrupo.DestCod)
                                            adoProv("DEN_DEST_GR").Value = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                        End If

                                        If oGrupo.DefFechasSum Then
                                            adoProv("FINI_SUM_GR").Value = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
                                            adoProv("FFIN_SUM_GR").Value = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                                        End If

                                        If oGrupo.DefFormaPago Then
                                            adoProv("COD_PAGO_GR").Value = NullToStr(oGrupo.PagCod)
                                            adoProv("DEN_PAGO_GR").Value = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                        End If
                                        
                                        adoProv("PRES_AHORR").Value = dblPresAhorr
                                        adoProv("ADJ_AHORR").Value = dblAdjAhorr
                                        adoProv("AHORR_GR").Value = dblAhorrGR
                                        adoProv("PORCEN_GR").Value = strPorcenGR ' FormateoNumerico(oAdjsGrupo.Item(sCod).AhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                                                                                
                                    End If
                                End If
                            End If
                        Else
                            adoProv.AddNew
                                            
                            adoProv("COD_PROVEEDOR").Value = oProve.Cod
                            adoProv("NOM_PROVEEDOR").Value = oProve.Den
                            adoProv("PRECIO_PROVEEDOR").Value = dblConsumido
                            adoProv("ADJ_PROV").Value = dblAdjudicado
                            adoProv("AHORRO_PROV").Value = dblAhorrado
                            adoProv("PORCEN_PROV").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                            adoProv("OBS_PROVEEDOR").Value = adoProv("OBS_PROVEEDOR").Value & NullToStr(oProce.Ofertas.Item(oProve.Cod).obs)
                            
                        End If 'bCargar
                    Next  'oGrupo
                End If
            Else
                adoProv.AddNew
                adoProv("COD_PROVEEDOR").Value = oProve.Cod
                adoProv("NOM_PROVEEDOR").Value = oProve.Den
                adoProv("PRECIO_PROVEEDOR").Value = dblConsumido
                adoProv("ADJ_PROV").Value = dblAdjudicado
                adoProv("AHORRO_PROV").Value = dblAhorrado
                adoProv("PORCEN_PROV").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                adoProv("OBS_PROVEEDOR").Value = adoProv("OBS_PROVEEDOR").Value & NullToStr(oProce.Ofertas.Item(oProve.Cod).obs)
            End If   'Grupos
        Next
    End If
        
    If chkProvNoAdj.Value = vbChecked Then

        If oProvesnoAdj.Count > 0 Then
            For Each oProve In oProvesnoAdj
                oProvesnoAdj.CargarDatosProveedor oProve.Cod

                dblPresupuesto = 0
                dblImporte = 0
                dblAhorrado = 0
                dblAhorradoPorcen = 0
                For Each oGrupo In oProce.Grupos
                    'Si no est� asignado al grupo no se pone
                    bCargar = True
                    If gParametrosGenerales.gbProveGrupos Then
                        scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                        If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                            bCargar = False
                        End If
                    End If
                    If bCargar Then
                        bMostrarGr = True
                        If oProce.AdminPublica = True Then
                            If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                bMostrarGr = False
                            End If
                        End If
                        If bMostrarGr = True Then
                            sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))

                            If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                If oAdjsGrupo.Item(sCod).importe <> 0 Then
                                    dblPresupuesto = dblPresupuesto + (oAdjsGrupo.Item(sCod).Abierto / oAdjsGrupo.Item(sCod).Cambio)
                                End If
                                dblImporte = dblImporte + (oAdjsGrupo.Item(sCod).importe / oAdjsGrupo.Item(sCod).Cambio)
                                dblAhorrado = dblAhorrado + (oAdjsGrupo.Item(sCod).AhorroOfe / oAdjsGrupo.Item(sCod).Cambio)
                            End If
                        End If
                    End If
                Next
                dblImporte = AplicarAtributosDeProcesoNoAdj(dblImporte, oProve.Cod)
                dblAhorrado = dblPresupuesto - dblImporte
                If dblPresupuesto = 0 Then
                    dblAhorradoPorcen = 0
                Else
                    dblAhorradoPorcen = (dblAhorrado / Abs(dblPresupuesto)) * 100
                End If

                Set oOferta = oProce.DevolverUltimaOfertaProveedor(oProve.Cod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)

                If Not oOferta Is Nothing Then
                    strObs = NullToStr(oOferta.obs)
                Else
                    strObs = ""
                End If
                Set oOferta = oProce.DevolverUltimaOfertaProveedor(oProve.Cod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)

                'GRUPOS
                If oProce.Grupos.Count > 0 Then
                    If Not oAdjsGrupo Is Nothing Then
                        For Each oGrupo In oProce.Grupos
                            'Si no est� asignado al grupo no se pone
                            bCargar = True
                            If gParametrosGenerales.gbProveGrupos Then
                                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                    bCargar = False
                                End If
                            End If
                            If bCargar Then
                                bMostrarGr = True
                                If oProce.AdminPublica = True Then
                                    If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                        bMostrarGr = False
                                    End If
                                End If

                                If bMostrarGr = True Then
                                    sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                    If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                        If Not bItems Then
                                            oGrupo.CargarTodosLosItems OrdItemPorOrden
                                            If HayAplicar(TotalItem) Then
                                                oProce.Ofertas.CargarAtributosItemOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, oGrupo.Id, frmRESREU.sdbcFecReu.Columns(0).Value   'de �tem
                                            End If
                                        End If

                                            For Each oItem In oGrupo.Items
                                                adoProvNoAdj.AddNew
                                                adoProvNoAdj("COD_PROCESO").Value = CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
                                                adoProvNoAdj("COD_PROVEEDOR_NOADJ").Value = oProve.Cod
                                                adoProvNoAdj("NOM_PROVEEDOR_NOADJ").Value = oProve.Den
                                                adoProvNoAdj("OBS_PROVEEDOR_NOADJ").Value = adoProvNoAdj("OBS_PROVEEDOR_NOADJ").Value & NullToStr(strObs)

                                                adoProvNoAdj("PRESUPUESTO_NOADJ").Value = dblPresupuesto
                                                adoProvNoAdj("IMPORTE_NOADJ").Value = dblImporte
                                                adoProvNoAdj("AHORRO_NOADJ").Value = dblAhorrado
                                                adoProvNoAdj("PORCEN_NOADJ").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"

                                                If IsNull(oGrupo.Sobre) Then
                                                    adoProvNoAdj("COD_GRUPO_NOADJ").Value = NullToStr(oGrupo.Codigo)
                                                Else
                                                    adoProvNoAdj("COD_GRUPO_NOADJ").Value = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
                                                End If
                                                adoProvNoAdj("DEN_GRUPO_NOADJ").Value = oGrupo.Den
                                                If oGrupo.DefDestino Then
                                                    adoProvNoAdj("COD_DEST_GR_NOADJ").Value = NullToStr(oGrupo.DestCod)
                                                    adoProvNoAdj("DEN_DEST_GR_NOADJ").Value = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                End If

                                                If oGrupo.DefFechasSum Then
                                                    adoProvNoAdj("FINI_SUM_GR_NOADJ").Value = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
                                                    adoProvNoAdj("FFIN_SUM_GR_NOADJ").Value = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                                                End If

                                                If oGrupo.DefFormaPago Then
                                                    adoProvNoAdj("COD_PAGO_GR_NOADJ").Value = NullToStr(oGrupo.PagCod)
                                                    adoProvNoAdj("DEN_PAGO_GR_NOADJ").Value = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                End If
                                                
                                                If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                                    adoProvNoAdj("PRES_AHORR_NOADJ").Value = oAdjsGrupo.Item(sCod).Abierto
                                                    adoProvNoAdj("IMPORTE_OFERTA_NOADJ").Value = oAdjsGrupo.Item(sCod).importe
                                                    adoProvNoAdj("AHORR_GR_NOADJ").Value = oAdjsGrupo.Item(sCod).AhorroOfe
                                                    adoProvNoAdj("PORCEN_GR_NOADJ").Value = FormateoNumerico(oAdjsGrupo.Item(sCod).AhorroOfePorcen, m_sFormatoNumberPorcenGr) & "%"
                                                Else
                                                    adoProvNoAdj("PRES_AHORR_NOADJ").Value = 0
                                                    adoProvNoAdj("IMPORTE_OFERTA_NOADJ").Value = 0
                                                    adoProvNoAdj("AHORR_GR_NOADJ").Value = 0
                                                    adoProvNoAdj("PORCEN_GR_NOADJ").Value = ""
                                                End If
                                                
                                                Set oOferta = oGrupo.UltimasOfertas.Item(Trim(CStr(oProve.Cod)))
                                                adoProvNoAdj("COD_ITEM_NOADJ").Value = NullToStr(oItem.ArticuloCod)
                                                adoProvNoAdj("DEN_ITEM_NOADJ").Value = NullToStr(oItem.Descr)
                                                adoProvNoAdj("ID_ITEM").Value = oItem.Id
                                                If oProce.DefDestino = EnItem Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                                                    adoProvNoAdj("COD_DEST_ITEM_NOADJ").Value = NullToStr(oItem.DestCod)
                                                    adoProvNoAdj("DEN_DEST_ITEM_NOADJ").Value = NullToStr(oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                End If

                                                If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                                                    adoProvNoAdj("FINI_SUM_ITEM_NOADJ").Value = Format(NullToStr(oItem.FechaInicioSuministro), "Short Date")
                                                    adoProvNoAdj("FFIN_SUM_ITEM_NOADJ").Value = Format(NullToStr(oItem.FechaFinSuministro), "Short Date")
                                                End If

                                                adoProvNoAdj("CANTIDAD_NOADJ").Value = oItem.Cantidad

                                                adoProvNoAdj("COD_UNI_NOADJ").Value = oItem.UniCod

                                                If Not oOferta Is Nothing Then
                                                  If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                                                    dPrecioOfe = (oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) / oOferta.Cambio
                                                    adoProvNoAdj("PRECOFE_NOADJ").Value = dPrecioOfe

                                                    sCod = CStr(oItem.Id) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                                    Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
                                                    If Not oAdj Is Nothing Then
                                                        If Not IsNull(oAdj.importe) Then
                                                            dImporteOfe = oAdj.importe
                                                        Else
                                                            dImporteOfe = NullToDbl0(oItem.Cantidad * dPrecioOfe)
                                                        End If
                                                    Else
                                                        dImporteOfe = NullToDbl0(oItem.Cantidad * dPrecioOfe)
                                                    End If
                                                    dAhorroOfe = (oItem.Cantidad * NullToDbl0(oItem.Precio)) - dImporteOfe
                                                    If (oItem.Cantidad * NullToDbl0(oItem.Precio)) = 0 Then
                                                        dAhorroOfePorcen = 0
                                                    Else
                                                        dAhorroOfePorcen = (dAhorroOfe / (oItem.Cantidad * NullToDbl0(oItem.Precio))) * 100
                                                    End If

                                                    adoProvNoAdj("IMPORTE_ITEM_NOADJ").Value = dImporteOfe 'aplicados los atributos de total de item
                                                    adoProvNoAdj("AHORRO_ITEM_NOADJ").Value = dAhorroOfe
                                                    adoProvNoAdj("AHORROPORCEN_NOADJ").Value = FormateoNumerico(dAhorroOfePorcen, m_sFormatoNumberPorcenGr) & "%"

                                                  Else
                                                    adoProvNoAdj("PRECOFE_NOADJ").Value = 0
                                                    adoProvNoAdj("IMPORTE_ITEM_NOADJ").Value = 0
                                                    adoProvNoAdj("AHORRO_ITEM_NOADJ").Value = 0
                                                    adoProvNoAdj("AHORROPORCEN_NOADJ").Value = ""
                                                  End If
                                                Else
                                                    adoProvNoAdj("PRECOFE_NOADJ").Value = 0
                                                    adoProvNoAdj("IMPORTE_ITEM_NOADJ").Value = 0
                                                    adoProvNoAdj("AHORRO_ITEM_NOADJ").Value = 0
                                                    adoProvNoAdj("AHORROPORCEN_NOADJ").Value = ""
                                                End If
                                            Next
                                        Else
                                            adoProvNoAdj.AddNew
                                            adoProvNoAdj("COD_PROCESO").Value = CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
                                            adoProvNoAdj("COD_PROVEEDOR_NOADJ").Value = oProve.Cod
                                            adoProvNoAdj("NOM_PROVEEDOR_NOADJ").Value = oProve.Den
                                            adoProvNoAdj("OBS_PROVEEDOR_NOADJ").Value = adoProvNoAdj("OBS_PROVEEDOR_NOADJ").Value & NullToStr(strObs)

                                            adoProvNoAdj("PRESUPUESTO_NOADJ").Value = dblPresupuesto
                                            adoProvNoAdj("IMPORTE_NOADJ").Value = dblImporte
                                            adoProvNoAdj("AHORRO_NOADJ").Value = dblAhorrado
                                            adoProvNoAdj("PORCEN_NOADJ").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"

                                            If IsNull(oGrupo.Sobre) Then
                                                adoProvNoAdj("COD_GRUPO_NOADJ").Value = NullToStr(oGrupo.Codigo)
                                            Else
                                                adoProvNoAdj("COD_GRUPO_NOADJ").Value = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
                                            End If
                                            adoProvNoAdj("DEN_GRUPO_NOADJ").Value = oGrupo.Den
                                            If oGrupo.DefDestino Then
                                                adoProvNoAdj("COD_DEST_GR_NOADJ").Value = NullToStr(oGrupo.DestCod)
                                                adoProvNoAdj("DEN_DEST_GR_NOADJ").Value = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                            End If

                                            If oGrupo.DefFechasSum Then
                                                adoProvNoAdj("FINI_SUM_GR_NOADJ").Value = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
                                                adoProvNoAdj("FFIN_SUM_GR_NOADJ").Value = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                                            End If

                                            If oGrupo.DefFormaPago Then
                                                adoProvNoAdj("COD_PAGO_GR_NOADJ").Value = NullToStr(oGrupo.PagCod)
                                                adoProvNoAdj("DEN_PAGO_GR_NOADJ").Value = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                            End If

                                            adoProvNoAdj("PRES_AHORR_NOADJ").Value = dblPresupuesto 'oAdjsGrupo.Item(sCod).Abierto
                                            adoProvNoAdj("IMPORTE_OFERTA_NOADJ").Value = dblImporte ' oAdjsGrupo.Item(sCod).Importe
                                            adoProvNoAdj("AHORR_GR_NOADJ").Value = dblAhorrado ' oAdjsGrupo.Item(sCod).AhorroOfe
                                            adoProvNoAdj("PORCEN_GR_NOADJ").Value = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%" ' FormateoNumerico(oAdjsGrupo.Item(sCod).AhorroOfePorcen, m_sFormatoNumberPorcenGr) & "%"
                                            
                                        End If
                                    End If
                                End If
                        Next 'oGrupo
                    End If
                End If
            Next
       End If
    End If

Finalizar:
    On Error Resume Next
    
    If bError Then
        TipoActa2Crystal = False
    Else
        TipoActa2Crystal = True
    End If
    
    Set oProves = Nothing
    Set oAdjs = Nothing
    Set oAdj = Nothing
    Set oItem = Nothing
    Set oIAdj = Nothing
    Set oiasignaciones = Nothing
    Set oAsignaciones = Nothing
    Set oEquipo = Nothing
    Set oOferta = Nothing
    Set oGrupo = Nothing
    Set oProve = Nothing
    Exit Function
    
ERROR:
    
    'Si el error es que no encuentra un formato o bookmark continuamos
    If err.Number = 5834 Or err.Number = 5941 Or err.Number = 9 Or err.Number = 91 Or err.Number = 4605 Then
        Resume Next
    End If
    
    If err.Number = 11 Then
    'Division by 0
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    Unload frmESPERA
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar
End Function



Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
      
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ACT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        caption = Ador(0).Value
        Ador.MoveNext
        lblUsar1.caption = Ador(0).Value
        lblUsar2.caption = Ador(0).Value
        Ador.MoveNext
        sCap(5) = Ador(0).Value
        Ador.MoveNext
        sCap(6) = Ador(0).Value
        Ador.MoveNext
        sCap(7) = Ador(0).Value
        Ador.MoveNext
        sCap(8) = Ador(0).Value
        Ador.MoveNext
        sCap(9) = Ador(0).Value
        Ador.MoveNext
        sCap(10) = Ador(0).Value
        Ador.MoveNext
        sCap(11) = Ador(0).Value
        Ador.MoveNext
        sCap(12) = Ador(0).Value
        Ador.MoveNext
        sCap(13) = Ador(0).Value
        Ador.MoveNext
        sCap(14) = Ador(0).Value
        Ador.MoveNext
        sCap(15) = Ador(0).Value
        Ador.MoveNext
        sCap(16) = Ador(0).Value
        Ador.MoveNext
        sCap(17) = Ador(0).Value
        Ador.MoveNext
        sCap(18) = Ador(0).Value
        Ador.MoveNext
        optTipo1.caption = Ador(0).Value
        Ador.MoveNext
        optTipo2.caption = Ador(0).Value
        Ador.MoveNext
        chkProvNoAdj.caption = Ador(0).Value
        
        Ador.MoveNext
        m_stxtPrecio = Ador(0).Value
        Ador.MoveNext
        m_stxtImp = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub


Private Sub cmdDOT2_Click()
    On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    If gParametrosInstalacion.giActaTipo = ActaWord Then
        cmmdDot.Filter = sCap(5) & " (*.dot)|*.dot"
    Else
        cmmdDot.Filter = sCap(5) & " (*.rpt)|*.rpt"
    End If
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOT2 = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oGrupo As CGrupo
    Dim oVistasProc As CConfVistasProce
    Dim oVistasGr As CConfVistasGrupo
    Dim sCod, sCodGrupo As String
        
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    CargarRecursos
    If gParametrosInstalacion.giActaTipo = ActaWord Then
        Dim Y, m, d, h, min, Seg As Integer
        Y = Year(frmRESREU.m_oReuSeleccionada.Fecha)
        m = Month(frmRESREU.m_oReuSeleccionada.Fecha)
        d = Day(frmRESREU.m_oReuSeleccionada.Fecha)
        h = Hour(frmRESREU.m_oReuSeleccionada.Fecha)
        min = Minute(frmRESREU.m_oReuSeleccionada.Fecha)
        Seg = Second(frmRESREU.m_oReuSeleccionada.Fecha)
        
        Me.Width = 10005
        Me.Height = 6000
        Picture1(0).Width = 10005
        Picture1(0).Height = 6000
        Picture1(1).Left = (Picture1(0).Width - Picture1(1).Width) / 2
        Picture1(1).Top = (Picture1(0).Height - Picture1(1).Height) / 2
        Timer1.Enabled = True
        With WebBrowser1
            .Visible = True
            .Left = 0
            .Top = 0
            .Width = 10005
            .Height = 6000
            Dim strSessionId As String
            ' Se crea el id de sesion de fullstep web
            strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
            Dim sURL As String
            sURL = gParametrosGenerales.gsRutasFullstepWeb & "/App_Pages/GS/Reuniones/SelectorPlantilla.aspx"
            sURL = sURL & "?desdeGS=1&SessionId=" & strSessionId & "&tipoDocumento=2&a=" & Y & "&m=" & m & "&d=" & d & "&h=" & h & "&mn=" & min & "&s=" & Seg
            .Navigate2 sURL, 4
        End With
    Else
        WebBrowser1.Visible = False
        Picture1(0).Visible = False
        Picture1(1).Visible = False
    
        Set oDestinos = oFSGSRaiz.Generar_CDestinos
      
        Set oUnidades = oFSGSRaiz.Generar_CUnidades
      
        Set oPagos = oFSGSRaiz.Generar_CPagos
        
        oDestinos.CargarTodosLosDestinos , , , , , , , , , , True, True
        oUnidades.CargarTodasLasUnidades
        oPagos.CargarTodosLosPagos
            
        'WordVersion = GetWordVersion
        Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
        If gParametrosInstalacion.gsActa = "" And gParametrosGenerales.gsACTADOT <> "" Then
            gParametrosInstalacion.gsActa = gParametrosGenerales.gsACTADOT
            g_GuardarParametrosIns = True
        End If
        
        Set oFos = New Scripting.FileSystemObject
        
        If gParametrosInstalacion.gbTipoActa = True Then
            optTipo1.Value = False
            optTipo2.Value = True
        Else
            optTipo2.Value = False
            optTipo1.Value = True
        End If
        
        txtDOT = gParametrosInstalacion.gsActa
        txtDOT2 = gParametrosInstalacion.gsActa2
        
        '***** Obtener formatos num�ricos.Si no est�n cargadas las vistas iniciales las carga *************
        'ConfVistasProce.CargarVistaInicial pone como usuario a pelo INI. La clave es vista+tipovista+usuario.
        sCod = "INI" & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUSU - Len("INI"))
        sCod = "00" & sCod
        If frmRESREU.m_oProcesoSeleccionado Is Nothing Then
            Set oVistasProc = oFSGSRaiz.Generar_CConfVistasProce
            oVistasProc.CargarVistaInicial oUsuarioSummit.Cod, Nothing
            If Not oVistasProc.Item(sCod) Is Nothing Then
                m_sFormatoNumberProce = FormateoNumericoComp(oVistasProc.Item(sCod).DecResult)
                m_sFormatoNumberProcePorcen = FormateoNumericoComp(oVistasProc.Item(sCod).DecPorcen)
            Else
                m_sFormatoNumberProce = FormateoNumericoComp(2)
                m_sFormatoNumberProcePorcen = FormateoNumericoComp(2)
            End If
            Set oVistasProc = Nothing
            
            Set oVistasGr = oFSGSRaiz.Generar_CConfVistasGrupo
            oVistasGr.CargarVistaInicial oUsuarioSummit.Cod, Nothing
            'oVistasGr.CargarVistaInicial otra vez clave vista+tipovista+usuario. Pero esta vez el usuario es "".
            sCodGrupo = "" & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUSU - Len(""))
            sCodGrupo = "00" & sCodGrupo
            If Not oVistasGr.Item(sCodGrupo) Is Nothing Then
                m_sFormatoNumberGr = FormateoNumericoComp(oVistasGr.Item(sCodGrupo).DecResult)
                m_sFormatoNumberPorcenGr = FormateoNumericoComp(oVistasGr.Item(sCodGrupo).DecPorcen)
                m_sFormatoNumberPrecioGr = FormateoNumericoComp(oVistasGr.Item(sCodGrupo).DecPrecios)
                m_sFormatoNumberCantGr = FormateoNumericoComp(oVistasGr.Item(sCodGrupo).DecCant)
            Else
                m_sFormatoNumberGr = FormateoNumericoComp(2)
                m_sFormatoNumberPorcenGr = FormateoNumericoComp(2)
                m_sFormatoNumberPrecioGr = FormateoNumericoComp(2)
                m_sFormatoNumberCantGr = FormateoNumericoComp(2)
            End If
            Set oVistasGr = Nothing
            
        Else
            'vista inicial de proceso:
            If frmRESREU.m_oConfVistasProce.Item(sCod) Is Nothing Then
                frmRESREU.m_oConfVistasProce.CargarVistaInicial oUsuarioSummit.Cod, frmRESREU.m_oProcesoSeleccionado
            End If
            If Not frmRESREU.m_oConfVistasProce.Item(sCod) Is Nothing Then
                m_sFormatoNumberProce = FormateoNumericoComp(frmRESREU.m_oConfVistasProce.Item(sCod).DecResult)
                m_sFormatoNumberProcePorcen = FormateoNumericoComp(frmRESREU.m_oConfVistasProce.Item(sCod).DecPorcen)
            Else
                m_sFormatoNumberProce = FormateoNumericoComp(2)
                m_sFormatoNumberProcePorcen = FormateoNumericoComp(2)
            End If
            'vista inicial de grupo:
            If Not frmRESREU.m_oProcesoSeleccionado.Grupos Is Nothing Then
                Set oGrupo = frmRESREU.m_oProcesoSeleccionado.Grupos.Item(1)
                'Grupo.CargarVistaInicial otra vez clave vista+tipovista+usuario.
                'Pero esta vez el usuario es INI.
                sCodGrupo = "INI" & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUSU - Len("INI"))
                sCodGrupo = "00" & sCodGrupo
                If Not oGrupo.ConfVistas Is Nothing Then
                    If oGrupo.ConfVistas.Item(sCodGrupo) Is Nothing Then
                        oGrupo.CargarVistaInicial oUsuarioSummit.Cod
                    End If
                Else
                    oGrupo.CargarVistaInicial oUsuarioSummit.Cod
                End If
                
                If Not oGrupo.ConfVistas Is Nothing Then
                    If Not oGrupo.ConfVistas.Item(sCodGrupo) Is Nothing Then
                        m_sFormatoNumberGr = FormateoNumericoComp(oGrupo.ConfVistas.Item(sCodGrupo).DecResult)
                        m_sFormatoNumberPorcenGr = FormateoNumericoComp(oGrupo.ConfVistas.Item(sCodGrupo).DecPorcen)
                        m_sFormatoNumberPrecioGr = FormateoNumericoComp(oGrupo.ConfVistas.Item(sCodGrupo).DecPrecios)
                        m_sFormatoNumberCantGr = FormateoNumericoComp(oGrupo.ConfVistas.Item(sCodGrupo).DecCant)
                    Else
                        m_sFormatoNumberGr = FormateoNumericoComp(2)
                        m_sFormatoNumberPorcenGr = FormateoNumericoComp(2)
                        m_sFormatoNumberPrecioGr = FormateoNumericoComp(2)
                        m_sFormatoNumberCantGr = FormateoNumericoComp(2)
                    End If
                Else
                    m_sFormatoNumberGr = FormateoNumericoComp(2)
                    m_sFormatoNumberPorcenGr = FormateoNumericoComp(2)
                    m_sFormatoNumberPrecioGr = FormateoNumericoComp(2)
                    m_sFormatoNumberCantGr = FormateoNumericoComp(2)
                End If
                Set oGrupo = Nothing
            End If
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oFos = Nothing
    Set oDestinos = Nothing
    Set oUnidades = Nothing
    Set oPagos = Nothing
    
End Sub

 

Private Sub optTipo1_Click()
    If optTipo1.Value = True Then
        optTipo2.Value = False
    End If
End Sub


Private Sub optTipo2_Click()
    If optTipo2.Value = True Then
        optTipo1.Value = False
    End If
End Sub
'
''' <summary>
''' Muestra en un word los items del proceso con el detalle de adjudicaciones para cada uno.
''' Esta es la parte del grupo/items/etc
''' </summary>
''' <param name="docword">Word de donde se copia para crear el acta</param>
''' <param name="blankword">Word q se esta creando con el acta</param>
''' <returns>Si se ido bien o mal</returns>
''' <remarks>Llamada desde: CrearActaWord ; Tiempo m�ximo: 0,2</remarks>
Private Function TipoActa1(ByVal docword As Object, ByVal blankword As Object) As Boolean
    Dim rangeword2 As Object
    Dim rangeword3 As Object
    Dim rangeword4 As Object
    Dim oProveedores As CProveedores
    Dim oProve As CProveedor
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oItem As CItem
    Dim oIAdj As IAdjudicaciones
    Dim dCantProve As Double
    Dim oOferta As COferta
    Dim oGrupo As CGrupo
    Dim bMostrarGr As Boolean
    Dim cont As Integer
    Dim bError As Boolean
    Dim oBook As Object
    Dim oOfertaNoAdj As COferta
    Dim sComentario As String
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim dImporte As Double
    Dim sCod As String
    
    On Error GoTo ERROR:

    bError = False

    With blankword
        If .Bookmarks.Exists("GRUPO") = True Then
    
            .Bookmarks("GRUPO").Select
            Set rangeword4 = docword.Bookmarks("GRUPO").Range
            
            
            .Bookmarks("GRUPO").Select
            .Application.Selection.Delete
                                    
            If oProce.Grupos.Count = 0 Then
            Else
                If docword.Bookmarks.Exists("ITEM_TOD") Or docword.Bookmarks.Exists("PROVEEDOR") Then
                    oProce.CargarPreciosOfertaGrupos
                    oProce.CargarAdjudicacionesGrupos frmRESREU.sdbcFecReu.Columns(0).Value
                End If
                
                For Each oGrupo In oProce.Grupos
                    bMostrarGr = True
                    If oProce.AdminPublica = True Then
                        If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                            bMostrarGr = False
                        End If
                    End If
        
                    If bMostrarGr = True Then
                        For Each oBook In .Bookmarks
                            oBook.Delete
                        Next
    
                        rangeword4.Copy
                        .Application.Selection.Paste
                        If docword.Bookmarks.Exists("ITEM_TOD") Or docword.Bookmarks.Exists("ITEM") Then
                            oGrupo.CargarTodosLosItems OrdItemPorOrden, , , , , , , , CDate(frmRESREU.sdbcFecReu.Columns(0).Value), , , , , , True
                        End If
                        If .Bookmarks.Exists("COD_GRUPO") Then
                            .Bookmarks("COD_GRUPO").Select
                            If IsNull(oGrupo.Sobre) Then
                                .Bookmarks("COD_GRUPO").Range.Text = NullToStr(oGrupo.Codigo)
                            Else
                                .Bookmarks("COD_GRUPO").Range.Text = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
                            End If
                        End If
                        If .Bookmarks.Exists("DEN_GRUPO") Then
                            .Bookmarks("DEN_GRUPO").Select
                            .Bookmarks("DEN_GRUPO").Range.Text = NullToStr(oGrupo.Den)
                        End If
                        If oGrupo.DefDestino Then
                            If .Bookmarks.Exists("COD_DEST_GR") Then
                                .Bookmarks("COD_DEST_GR").Range.Text = NullToStr(oGrupo.DestCod)
                            End If
                            If .Bookmarks.Exists("DEN_DEST_GR") Then
                                .Bookmarks("DEN_DEST_GR").Range.Text = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                            End If
                            .Bookmarks("GR_DEST").Delete
                        Else
                            If .Bookmarks.Exists("GR_DEST") Then
                               .Bookmarks("GR_DEST").Select
                               .Application.Selection.cut
            
                            End If
                        End If
        
                        If oGrupo.DefFechasSum Then
                            If .Bookmarks.Exists("FINI_SUM_GR") Then
                                .Bookmarks("FINI_SUM_GR").Range.Text = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
            
                            End If
                            If .Bookmarks.Exists("FFIN_SUM_GR") Then
                                .Bookmarks("FFIN_SUM_GR").Range.Text = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                            End If
                            .Bookmarks("GR_FECSUM").Delete
                        Else
            
                            If .Bookmarks.Exists("GR_FECSUM") Then
                                .Bookmarks("GR_FECSUM").Select
                                .Application.Selection.cut
                            End If
                        End If
        
                        If oGrupo.DefFormaPago Then
                            If .Bookmarks.Exists("COD_PAGO_GR") Then
                                .Bookmarks("COD_PAGO_GR").Range.Text = NullToStr(oGrupo.PagCod)
            
                            End If
                            If .Bookmarks.Exists("DEN_PAGO_GR") Then
                               .Bookmarks("DEN_PAGO_GR").Range.Text = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                            End If
                            .Bookmarks("GR_PAGO").Delete
                        Else
                            If .Bookmarks.Exists("GR_PAGO") Then
                               .Bookmarks("GR_PAGO").Select
                               .Application.Selection.cut
                            End If
                        End If
                                                                    
                        .Bookmarks("ITEM_TOD").Select
                        .Application.Selection.cut
                        Set rangeword2 = docword.Bookmarks("ITEM_TOD").Range
                        cont = 0
                        If oGrupo.Items.Count = 0 Then
            
                            .Bookmarks("PROVEEDOR").Select
                            .Application.Selection.Delete
                        Else
    
                            If docword.Bookmarks.Exists("PROVEEDOR") = True Then
                                If HayAplicar(TotalItem) Then
                                    oProce.Ofertas.CargarAtributosItemOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, oGrupo.Id, frmRESREU.sdbcFecReu.Columns(0).Value   'de �tem
                                End If
                            End If
                            
                            For Each oItem In oGrupo.Items
                                For Each oBook In .Bookmarks
                                    oBook.Delete
                                Next
                                rangeword2.Copy
                                .Application.Selection.Paste
        
                                If .Bookmarks.Exists("COD_ITEM") Then
                                    .Bookmarks("COD_ITEM").Range.Text = NullToStr(oItem.ArticuloCod)
                                End If
                                If .Bookmarks.Exists("DEN_ITEM") Then
                                    .Bookmarks("DEN_ITEM").Range.Text = NullToStr(oItem.Descr)
                                End If
                                If .Bookmarks.Exists("CANTIDAD") Then
                                    .Bookmarks("CANTIDAD").Range.Text = FormateoNumerico(oItem.Cantidad, m_sFormatoNumberCantGr)
                                End If
                                If .Bookmarks.Exists("COD_UNI") Then
                                    .Bookmarks("COD_UNI").Range.Text = oItem.UniCod
                                End If
                                If .Bookmarks.Exists("DEN_UNI") Then
                                    If Not oUnidades.Item(oItem.UniCod) Is Nothing Then
                                        .Bookmarks("DEN_UNI").Range.Text = oUnidades.Item(oItem.UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                    End If
                                End If
        
                                If .Bookmarks.Exists("PRECIO") Then
                                    .Bookmarks("PRECIO").Range.Text = FormateoNumerico(VarToDbl0(oItem.Precio), m_sFormatoNumberPrecioGr)
                                End If
                                If .Bookmarks.Exists("OBJETIVO") Then
                                    If IsNumeric(oItem.Objetivo) Then
                                        .Bookmarks("OBJETIVO").Range.Text = FormateoNumerico(oItem.Objetivo, m_sFormatoNumberPrecioGr)
                                    Else
                                        .Bookmarks("OBJETIVO").Range.Text = ""
                                    End If
                                End If
                                If oProce.DefDestino <> EnItem And oProce.DefFechasSum <> EnItem And oProce.DefFormaPago <> EnItem Then
                                    If .Bookmarks.Exists("OPT_ITEM") Then
                                        .Bookmarks("OPT_ITEM").Select
                                        .Application.Selection.cut
                                        .Bookmarks("OPT_ITEM").Delete
                                    End If
                                Else
                                    If oProce.DefDestino = EnItem Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
            
                                        If .Bookmarks.Exists("COD_DEST_ITEM") Then
                                            .Bookmarks("COD_DEST_ITEM").Range.Text = NullToStr(oItem.DestCod)
                                        End If
                                        If .Bookmarks.Exists("DEN_DEST_ITEM") Then
                                            .Bookmarks("DEN_DEST_ITEM").Range.Text = NullToStr(oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                        End If
                                    Else
                                        If .Bookmarks.Exists("IT_DEST") Then
                                            .Bookmarks("IT_DEST").Select
                                            .Application.Selection.cut
                                        End If
                                    End If
            
                                    If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                                        If .Bookmarks.Exists("FINI_SUM_ITEM") Then
                                            .Bookmarks("FINI_SUM_ITEM").Range.Text = Format(NullToStr(oItem.FechaInicioSuministro), "Short Date")
                                        End If
                                        If .Bookmarks.Exists("FFIN_SUM_ITEM") Then
                                            .Bookmarks("FFIN_SUM_ITEM").Range.Text = Format(NullToStr(oItem.FechaFinSuministro), "Short Date")
                                        End If
                                    Else
                                        If .Bookmarks.Exists("IT_FECSUM") Then
                                            .Bookmarks("IT_FECSUM").Select
                                            .Application.Selection.cut
                                        End If
                                    End If
                                    If oProce.DefFormaPago = EnItem Or (oProce.DefFormaPago = EnGrupo And Not oGrupo.DefFormaPago) Then
            
                                        If .Bookmarks.Exists("COD_PAGO_ITEM") Then
                                            .Bookmarks("COD_PAGO_ITEM").Range.Text = NullToStr(oItem.PagCod)
                                        End If
                                        If .Bookmarks.Exists("DEN_PAGO_ITEM") Then
                                            .Bookmarks("DEN_PAGO_ITEM").Range.Text = NullToStr(oPagos.Item(oItem.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                        End If
                                    Else
                                        If .Bookmarks.Exists("IT_PAGO") Then
                                            .Bookmarks("IT_PAGO").Select
                                            .Application.Selection.cut
                                        End If
                                    End If
                                End If
                    
            
                                If .Bookmarks.Exists("PROVEEDOR") = True Then
                                    Set oIAdj = oProce
                                    Set oAdjs = oIAdj.DevolverAdjudicacionesDeItemConFecha(oItem.Id, CDate(frmRESREU.sdbcFecReu.Columns(0).Value))
                                    
                                    .Bookmarks("PROVEEDOR").Select
                                    .Application.Selection.Delete
                                    Set rangeword3 = docword.Bookmarks("PROVEEDOR").Range
                                    rangeword3.Copy
                                    'Adjudicados, si est� adjudicado estar� en ese grupo
                                    If oAdjs.Count > 0 Then
                                        For Each oAdj In oAdjs
                                            sCod = oAdj.ProveCod
                                            Set oProve = oProves.Item(sCod)
                                            
                                            .Application.Selection.Paste
                                            
                                            If .Bookmarks.Exists("COD_PROVEEDOR") Then
                                                .Bookmarks("COD_PROVEEDOR").Range.Text = oAdj.ProveCod
                                            End If
                                            If .Bookmarks.Exists("NOM_PROVEEDOR") Then
                                                .Bookmarks("NOM_PROVEEDOR").Range.Text = oProve.Den
                                            End If
                                            If .Bookmarks.Exists("PROV_PROVEEDOR") Then
                                                .Bookmarks("PROV_PROVEEDOR").Range.Text = oProve.DenProvi
                                            End If
                                            If .Bookmarks.Exists("PAIS_PROVEEDOR") Then
                                                .Bookmarks("PAIS_PROVEEDOR").Range.Text = oProve.DenPais
                                            End If
                                            If .Bookmarks.Exists("PRECIO_PROVEEDOR") Then
                                                sComentario = ""
                                                sComentario = RPT_FormulaDeAtributosUnitarioProveedor(oGrupo.Codigo, oAdj.ProveCod, oAdj.Id)
                                                If sComentario <> "" Then
                                                    .Bookmarks("PRECIO_PROVEEDOR").Range.Comments.Add Range:=.Bookmarks("PRECIO_PROVEEDOR").Range, Text:=sComentario
                                                End If
                                
                                                .Bookmarks("PRECIO_PROVEEDOR").Range.Text = FormateoNumerico(oAdj.Precio, m_sFormatoNumberPrecioGr)
                                            End If
                                            If .Bookmarks.Exists("CANT_PROVEEDOR") Then
                                                .Bookmarks("CANT_PROVEEDOR").Range.Text = FormateoNumerico((oAdj.Porcentaje / 100) * oItem.Cantidad, m_sFormatoNumberCantGr)
                                            End If
                                            dCantProve = ((oAdj.Porcentaje / 100) * oItem.Cantidad)
                                            If .Bookmarks.Exists("COD_UNI_PROVEEDOR") Then
                                                .Bookmarks("COD_UNI_PROVEEDOR").Range.Text = oItem.UniCod
                                            End If
                                            If .Bookmarks.Exists("DEN_UNI_PROVEEDOR") Then
                                                If Not oUnidades.Item(oItem.UniCod) Is Nothing Then
                                                    .Bookmarks("DEN_UNI_PROVEEDOR").Range.Text = oUnidades.Item(oItem.UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                                End If
                                            End If
            
                                            If .Bookmarks.Exists("TOTAL_PROVEEDOR") Then
                                                sComentario = ""
                                                sComentario = RPT_FormulaDeAtributosDeItemProveedor(oGrupo.Codigo, oAdj.ProveCod, oItem.Id)
                                                If sComentario <> "" Then
                                                    .Bookmarks("TOTAL_PROVEEDOR").Range.Comments.Add Range:=.Bookmarks("TOTAL_PROVEEDOR").Range, Text:=sComentario
                                                End If
                                                If Not HayAplicar(TotalItem) Then
                                                    .Bookmarks("TOTAL_PROVEEDOR").Range.Text = FormateoNumerico(NullToDbl0(dCantProve) * NullToDbl0(oAdj.Precio), m_sFormatoNumberGr)  'aplicados los atributos de total de item
                                                Else
                                                    'Aplica los atributos de total de item:
                                                    dImporte = AplicarAtributosTotalItem(oGrupo.Codigo, oItem.Id, oAdj.ProveCod)
                                                    .Bookmarks("TOTAL_PROVEEDOR").Range.Text = FormateoNumerico(dImporte, m_sFormatoNumberGr)  'aplicados los atributos de total de item
                                                End If
                                            End If
    
                                            If .Bookmarks.Exists("APE_COMPRADOR") Then
                                                 .Bookmarks("APE_COMPRADOR").Range.Text = oProve.cP 'Lo he cargado ah�
                                            End If
                                            If .Bookmarks.Exists("NOM_COMPRADOR") Then
                                                 .Bookmarks("NOM_COMPRADOR").Range.Text = oProve.Direccion 'Lo he cargado ah�
                                            End If
                                            If .Bookmarks.Exists("OBS_PROVEEDOR") Then
                                                'Set oOferta = oproce.DevolverUltimaOfertaProveedor(oAdj.ProveCod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)
                                                .Bookmarks("OBS_PROVEEDOR").Range.Text = .Bookmarks("OBS_PROVEEDOR").Range.Text & NullToStr(oProce.Ofertas.Item(CStr(oAdj.ProveCod)).obs)
                                            End If
    '                                        If .Bookmarks.Exists("OBSERVACION") Then
    '                                            Set oOferta = oproce.DevolverUltimaOfertaProveedor(oAdj.ProveCod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)
    '                                            If Not IsNull(oOferta.obs) Then
    '                                                'ayObs(INumProve) = oOferta.obs
    '                                                'ayProv(INumProve) = oProve.Den
    '                                                'INumProve = INumProve + 1
    '                                                'ReDim Preserve ayObs(UBound(ayObs) + 1)
    '                                                'ReDim Preserve ayProv(UBound(ayObs) + 1)
    '                                            End If
    '                                        End If
                                            Set oProve = Nothing
                                        Next
                                        .Bookmarks("PROVEEDOR").Delete
                                    Else
                                        If .Bookmarks.Exists("ITEM_TOD") Then
                                            .Bookmarks("ITEM_TOD").Select
                                            .Application.Selection.cut
                                        End If
                                        cont = cont + 1
                                    End If
            
                               End If
                                
                               If .Bookmarks.Exists("PROVEEDOR_NOADJ") = True Then
                                    If chkProvNoAdj.Value = vbChecked Then
                                        .Bookmarks("PROVEEDOR_NOADJ").Select
                                        .Application.Selection.Delete
                                        Set rangeword3 = docword.Bookmarks("PROVEEDOR_NOADJ").Range
                                        rangeword3.Copy
                                         For Each oProve In oProvesnoAdj
                                            Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                            If Not oOferta Is Nothing Then
                                         
                                                bCargar = True
                                                If gParametrosGenerales.gbProveGrupos Then
                                                    scodProve = oOfertaNoAdj.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oOfertaNoAdj.Prove))
                                                    If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                                        bCargar = False
                                                    End If
                                                End If
                                                If bCargar Then
                                                    .Application.Selection.Paste
                                                    If .Bookmarks.Exists("COD_PROVEEDOR_NOADJ") Then
                                                        .Bookmarks("COD_PROVEEDOR_NOADJ").Range.Text = oProve.Cod
                                                    End If
                                                    If .Bookmarks.Exists("NOM_PROVEEDOR_NOADJ") Then
                                                        .Bookmarks("NOM_PROVEEDOR_NOADJ").Range.Text = oProve.Den
                                                    End If
                                                    If .Bookmarks.Exists("PROV_PROVEEDOR_NOADJ") Or .Bookmarks.Exists("PAIS_PROVEEDOR_NOADJ") Then
                                                        If IsNull(oProve.DenPais) Or IsEmpty(oProve.DenPais) Or oProve.DenPais = "" Then
                                                            oProvesnoAdj.CargarDatosProveedor oProve.Cod
                                                        End If
                                                    End If
                                                    If .Bookmarks.Exists("PROV_PROVEEDOR_NOADJ") Then
                                                        .Bookmarks("PROV_PROVEEDOR_NOADJ").Range.Text = oProve.DenProvi
                                                    End If
                                                    If .Bookmarks.Exists("PAIS_PROVEEDOR_NOADJ") Then
                                                        .Bookmarks("PAIS_PROVEEDOR_NOADJ").Range.Text = oProve.DenPais
                                                    End If
                                                    
                                                    If .Bookmarks.Exists("PRECIO_PROVEEDOR_NOADJ") Then
                                                        If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                                                            sComentario = ""
                                                            sComentario = RPT_FormulaDeAtributosUnitarioProveedor(oGrupo.Codigo, oProve.Cod, oItem.Id)
                                                            If sComentario <> "" Then
                                                                .Bookmarks("PRECIO_PROVEEDOR_NOADJ").Range.Comments.Add Range:=.Bookmarks("PRECIO_PROVEEDOR_NOADJ").Range, Text:=sComentario
                                                            End If
                                                    
                                                            .Bookmarks("PRECIO_PROVEEDOR_NOADJ").Range.Text = FormateoNumerico(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta, m_sFormatoNumberPrecioGr)
                                                        Else
                                                            .Bookmarks("PRECIO_PROVEEDOR_NOADJ").Range.Text = ""
                                                        End If
                                                    End If
                                                    If .Bookmarks.Exists("COD_UNI_PROVEEDOR_NOADJ") Then
                                                        .Bookmarks("COD_UNI_PROVEEDOR_NOADJ").Range.Text = oItem.UniCod
                                                    End If
                                                    If .Bookmarks.Exists("DEN_UNI_PROVEEDOR_NOADJ") Then
                                                        If Not oUnidades.Item(oItem.UniCod) Is Nothing Then
                                                            .Bookmarks("DEN_UNI_PROVEEDOR_NOADJ").Range.Text = oUnidades.Item(oItem.UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                                        End If
                                                    End If
                                                
                                                    If .Bookmarks.Exists("OBS_PROVEEDOR_NOADJ") Then
                                                        .Bookmarks("OBS_PROVEEDOR_NOADJ").Range.Text = .Bookmarks("OBS_PROVEEDOR_NOADJ").Range.Text & NullToStr(oOferta.obs)
                                                    End If
                                                End If 'bCargar
                                            End If 'oferta
                                            Set oOferta = Nothing
                                        Next
                                    Else
    '                                    .Bookmarks("PROVEEDOR_NOADJ").Range.Delete
                                        .Bookmarks("PROVEEDOR_NOADJ").Select
                                        .Application.Selection.Delete
                                    End If
                               End If
                               
                            Next 'items
                        End If 'items.count
                    End If 'MostrarGr
                Next 'GRUPO
            End If
                 
            .Bookmarks("GRUPO").Delete
            .Bookmarks("GR_DEST").Delete
            .Bookmarks("GR_FECSUM").Delete
            .Bookmarks("GR_PAGO").Delete
            .Bookmarks("OPT_ITEM").Delete
        End If
    End With
    
Finalizar:
    On Error Resume Next
    
    If bError Then
        TipoActa1 = False
    Else
        TipoActa1 = True
    End If
    
    Set rangeword2 = Nothing
    Set rangeword3 = Nothing
    Set rangeword4 = Nothing
    Set oProveedores = Nothing
    Set oAdjs = Nothing
    Set oAdj = Nothing
    Set oItem = Nothing
    Set oIAdj = Nothing
    Set oOferta = Nothing
    Set oGrupo = Nothing

    Exit Function
    
    
ERROR:
    
    'Si el error es que no encuentra un formato o bookmark continuamos
    If err.Number = 5834 Or err.Number = 5941 Or err.Number = 9 Or err.Number = 91 Or err.Number = 4605 Then
        Resume Next
    End If
    
    If err.Number = 11 Then
    'Division by 0
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    
    Unload frmESPERA
    
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar
    

End Function

''' <summary>
''' Crear Acta Word de no adjudicados
''' </summary>
''' <param name="docword">Word de donde se copia para crear el acta</param>
''' <param name="blankword">Word q se esta creando con el acta</param>
''' <returns>Si se imprimio</returns>
''' <remarks>Llamada desde: CrearActaWord ; Tiempo m�ximo: 0,2</remarks>
Private Function TipoActa2(ByVal docword As Object, ByVal blankword As Object) As Boolean
    Dim rangeword2 As Object
    Dim rangeword3 As Object
    Dim rangeword4 As Object
    Dim vConsumido As Variant
    Dim vAdjudicado As Variant
    Dim oProve As CProveedor
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oItem As CItem
    Dim oIAdj As IAdjudicaciones
    Dim sCod As String
    Dim oiasignaciones As IAsignaciones
    Dim oAsignaciones As CAsignaciones
    Dim oEquipo As CEquipo
    Dim oOferta As COferta
    Dim oGrupo As CGrupo
    Dim bMostrarGr As Boolean
    Dim bError As Boolean
    Dim oBook As Object
    Dim dblConsumido As Double
    Dim dblAdjudicado As Double
    Dim dblAhorrado As Double
    Dim dblAhorradoPorcen As Double
    Dim rangeword5 As Object
    Dim dblPresupuesto As Double
    Dim dblImporte As Double
    Dim dImporteOfe As Double
    Dim dAhorroOfe As Double
    Dim dAhorroOfePorcen As Double
    Dim dPrecioOfe As Double
    Dim sComentario As String
    Dim bDestino As Boolean
    Dim bPago As Boolean
    Dim bFecha As Boolean
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim bItems As Boolean
    
    On Error GoTo ERROR:
    bError = False
               
    'Proveedores asignados con el detalle de items asignados por cada uno:
    With blankword
        If .Bookmarks.Exists("PROVEEDOR") = True Then
            If oProves.Count > 0 Then
                bItems = False
                If docword.Bookmarks.Exists("ITEM_TOD") Or docword.Bookmarks.Exists("ADJ_AHORR") Then
                    oProce.CargarPreciosOfertaGrupos
                    oProce.CargarAdjudicacionesGrupos frmRESREU.sdbcFecReu.Columns(0).Value
                End If
                
                .Bookmarks("PROVEEDOR").Select
                Set rangeword3 = docword.Bookmarks("PROVEEDOR").Range
                .Bookmarks("PROVEEDOR").Select
                .Application.Selection.Delete
                                            
                For Each oProve In oProves
                    
                    For Each oBook In .Bookmarks
                        If Right(oBook.Name, 6) <> "_NOADJ" Then
                            oBook.Delete
                        End If
                    Next
                    
                    rangeword3.Copy
                    .Application.Selection.Paste
                    If IsNull(oProve.DenProvi) Then
                        oProves.CargarDatosProveedor oProve.Cod
                    End If
                    If .Bookmarks.Exists("COD_PROVEEDOR") Then
                        .Bookmarks("COD_PROVEEDOR").Range.Text = oProve.Cod
                    End If
                    If .Bookmarks.Exists("NOM_PROVEEDOR") Then
                        .Bookmarks("NOM_PROVEEDOR").Range.Text = oProve.Den
                    End If
                    If .Bookmarks.Exists("PROV_PROVEEDOR") Then
                        .Bookmarks("PROV_PROVEEDOR").Range.Text = oProve.DenProvi
                    End If
                    If .Bookmarks.Exists("PAIS_PROVEEDOR") Then
                        .Bookmarks("PAIS_PROVEEDOR").Range.Text = oProve.DenPais
                    End If

                    If .Bookmarks.Exists("APE_COMPRADOR") Then
                         .Bookmarks("APE_COMPRADOR").Range.Text = oProve.cP 'Lo he cargado ah� para ahorrar cargas
                    End If
                    If .Bookmarks.Exists("NOM_COMPRADOR") Then
                         .Bookmarks("NOM_COMPRADOR").Range.Text = oProve.Direccion 'Lo he cargado ah� para ahorrar cargas
                    End If
                    
                    dblConsumido = 0
                    dblAdjudicado = 0
                    dblAhorrado = 0
                    dblAhorradoPorcen = 0
                    For Each oGrupo In oProce.Grupos
                        'Si no est� asignado al grupo no se pone
                        bCargar = True
                        If gParametrosGenerales.gbProveGrupos Then
                            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                bCargar = False
                            End If
                        End If
                        If bCargar Then
                            bMostrarGr = True
                            If oProce.AdminPublica = True Then
                                If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                    bMostrarGr = False
                                End If
                            End If
                            If bMostrarGr = True Then
                                sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            
                                If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                    dblConsumido = dblConsumido + (oAdjsGrupo.Item(sCod).Consumido / oAdjsGrupo.Item(sCod).Cambio)
                                    dblAdjudicado = dblAdjudicado + (oAdjsGrupo.Item(sCod).Adjudicado / oAdjsGrupo.Item(sCod).Cambio)
                                    dblAhorrado = dblAhorrado + (oAdjsGrupo.Item(sCod).Ahorrado / oAdjsGrupo.Item(sCod).Cambio)
                                End If
                            End If
                        End If
                    Next
                    dblAdjudicado = AplicarAtributosDeProceso(dblAdjudicado, oProve.Cod)
                    dblAhorrado = dblConsumido - dblAdjudicado
                    If dblConsumido = 0 Then
                        dblAhorradoPorcen = 0
                    Else
                        dblAhorradoPorcen = (dblAhorrado / dblConsumido) * 100
                    End If
                    
                    If .Bookmarks.Exists("PRECIO_PROVEEDOR") Then
                        .Bookmarks("PRECIO_PROVEEDOR").Range.Text = FormateoNumerico(dblConsumido, m_sFormatoNumberPrecioGr)
                    End If
                    If .Bookmarks.Exists("ADJ_PROV") Then
                        sComentario = ""
                        sComentario = RPT_FormulaDeAtributosProcesoProveedor(oProve.Cod)
                        If sComentario <> "" Then
                            .Bookmarks("ADJ_PROV").Range.Comments.Add Range:=.Bookmarks("ADJ_PROV").Range, Text:=sComentario
                        End If
                        .Bookmarks("ADJ_PROV").Range.Text = FormateoNumerico(dblAdjudicado, m_sFormatoNumberGr)
                    End If
                    If .Bookmarks.Exists("AHORR_PROV") Then
                        .Bookmarks("AHORR_PROV").Range.Text = FormateoNumerico(dblAhorrado, m_sFormatoNumberGr)
                    End If
                    If .Bookmarks.Exists("PORCEN_PROV") Then
                        .Bookmarks("PORCEN_PROV").Range.Text = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                    End If
                        
                    If .Bookmarks.Exists("OBS_PROVEEDOR") Then
                        Set oOferta = oProce.DevolverUltimaOfertaProveedor(oProve.Cod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)
                        .Bookmarks("OBS_PROVEEDOR").Range.Text = .Bookmarks("OBS_PROVEEDOR").Range.Text & NullToStr(oProce.Ofertas.Item(oProve.Cod).obs)
                    End If
'                    If .Bookmarks.Exists("OBSERVACION") Then
'                        Set oOferta = oproce.DevolverUltimaOfertaProveedor(oProve.Cod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)
'                    End If
                        
                    'GRUPOS
                    .Bookmarks("GRUPO").Select
                    Set rangeword4 = docword.Bookmarks("GRUPO").Range
                    .Bookmarks("GRUPO").Select
                    .Application.Selection.Delete
                
                    If oProce.Grupos.Count = 0 Then
                    Else
                        If Not oAdjsGrupo Is Nothing Then
                            For Each oGrupo In oProce.Grupos
                                'Si no est� asignado al grupo no se pone
                                bCargar = True
                                If gParametrosGenerales.gbProveGrupos Then
                                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                    If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                        bCargar = False
                                    End If
                                End If
                                If bCargar Then
                            
                                    bMostrarGr = True
                                    If oProce.AdminPublica = True Then
                                        If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                            bMostrarGr = False
                                        End If
                                    End If
                                    'Si se muestra el grupo comprueba si tiene adjudicaciones para el proveedor:
                                    If bMostrarGr = True Then
    
                                        sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                        If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                            For Each oBook In .Bookmarks
                                                If Right(oBook.Name, 6) <> "_NOADJ" Then
                                                    oBook.Delete
                                                End If
                                            Next
                                            rangeword4.Copy
                                            .Application.Selection.Paste
    
                                            If .Bookmarks.Exists("COD_GRUPO") Then
                                                .Bookmarks("COD_GRUPO").Select
                                                If IsNull(oGrupo.Sobre) Then
                                                    .Bookmarks("COD_GRUPO").Range.Text = NullToStr(oGrupo.Codigo)
                                                Else
                                                    .Bookmarks("COD_GRUPO").Range.Text = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
                                                End If
                                            End If
                                            If .Bookmarks.Exists("DEN_GRUPO") Then
                                                .Bookmarks("DEN_GRUPO").Select
                                                .Bookmarks("DEN_GRUPO").Range.Text = NullToStr(oGrupo.Den)
                                            End If
                                            If oGrupo.DefDestino Then
                                                If .Bookmarks.Exists("COD_DEST_GR") Then
                                                    .Bookmarks("COD_DEST_GR").Range.Text = NullToStr(oGrupo.DestCod)
                                                End If
                                                If .Bookmarks.Exists("DEN_DEST_GR") Then
                                                    .Bookmarks("DEN_DEST_GR").Range.Text = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                End If
                                                .Bookmarks("GR_DEST").Delete
                                            Else
                                                If .Bookmarks.Exists("GR_DEST") Then
                                                   .Bookmarks("GR_DEST").Select
                                                   .Application.Selection.cut
                                                End If
                                            End If
    
                                            If oGrupo.DefFechasSum Then
                                                If .Bookmarks.Exists("FINI_SUM_GR") Then
                                                    .Bookmarks("FINI_SUM_GR").Range.Text = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
    
                                                End If
                                                If .Bookmarks.Exists("FFIN_SUM_GR") Then
                                                    .Bookmarks("FFIN_SUM_GR").Range.Text = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                                                End If
                                                .Bookmarks("GR_FECSUM").Delete
                                            Else
                                                If .Bookmarks.Exists("GR_FECSUM") Then
                                                    .Bookmarks("GR_FECSUM").Select
                                                    .Application.Selection.cut
                                                End If
                                            End If
    
                                            If oGrupo.DefFormaPago Then
                                                If .Bookmarks.Exists("COD_PAGO_GR") Then
                                                    .Bookmarks("COD_PAGO_GR").Range.Text = NullToStr(oGrupo.PagCod)
                                                End If
                                                If .Bookmarks.Exists("DEN_PAGO_GR") Then
                                                   .Bookmarks("DEN_PAGO_GR").Range.Text = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                End If
                                                .Bookmarks("GR_PAGO").Delete
                                            Else
                                                If .Bookmarks.Exists("GR_PAGO") Then
                                                   .Bookmarks("GR_PAGO").Select
                                                   .Application.Selection.cut
                                                End If
                                            End If
                                            
                                            If .Bookmarks.Exists("PRES_AHORR") Then
                                                .Bookmarks("PRES_AHORR").Select
                                                .Bookmarks("PRES_AHORR").Range.Text = FormateoNumerico(oAdjsGrupo.Item(sCod).Consumido, m_sFormatoNumberGr)
                                            End If
                                            If .Bookmarks.Exists("ADJ_AHORR") Then
                                                sComentario = ""
                                                sComentario = RPT_FormulaDeAtributosDeGrupoProveedor(oGrupo.Codigo, oProve.Cod)
                                                If sComentario <> "" Then
                                                    .Bookmarks("ADJ_AHORR").Range.Comments.Add Range:=.Bookmarks("ADJ_AHORR").Range, Text:=sComentario
                                                End If
                                                            
                                                .Bookmarks("ADJ_AHORR").Select
                                                .Bookmarks("ADJ_AHORR").Range.Text = FormateoNumerico(oAdjsGrupo.Item(sCod).Adjudicado, m_sFormatoNumberGr)
                                            End If
                                            If .Bookmarks.Exists("AHORR_GR") Then
                                                .Bookmarks("AHORR_GR").Select
                                                .Bookmarks("AHORR_GR").Range.Text = FormateoNumerico(oAdjsGrupo.Item(sCod).Ahorrado, m_sFormatoNumberGr)
                                            End If
                                            If .Bookmarks.Exists("PORCEN_GR") Then
                                                .Bookmarks("PORCEN_GR").Select
                                                .Bookmarks("PORCEN_GR").Range.Text = FormateoNumerico(oAdjsGrupo.Item(sCod).AhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                                            End If
                                            
    
                                            'ITEMS
                                            If docword.Bookmarks.Exists("ITEM") Or docword.Bookmarks.Exists("ITEM_TOD") Then
                                                oGrupo.CargarTodosLosItems OrdItemPorOrden, , , , , , , , frmRESREU.sdbcFecReu.Columns(0).Value, , , , , , True
                                                bItems = True
                                                If HayAplicar(TotalItem) Then
                                                    oProce.Ofertas.CargarAtributosItemOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, oGrupo.Codigo, frmRESREU.sdbcFecReu.Columns(0).Value   'de �tem
                                                End If
                                            End If
                                            If oGrupo.Items.Count = 0 Then
                                                .Bookmarks("ITEM_TOD").Select
                                                .Bookmarks("ITEM_TOD").Range.Delete
                                            ElseIf ExistenItemsConPrecios(oGrupo, oProve) = False Then
                                                .Bookmarks("ITEM_TOD").Select
                                                .Bookmarks.Item("ITEM_TOD").Range.Delete
                                            Else
                                                If docword.Bookmarks.Exists("ITEM") Then
                                                    If (oProce.DefDestino = EnItem) Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                                                        bDestino = True
                                                    Else
                                                        bDestino = False
                                                    End If
                                                    If oProce.DefFormaPago = EnItem Or (oProce.DefFormaPago = EnGrupo And Not oGrupo.DefFormaPago) Then
                                                        bPago = True
                                                    Else
                                                        bPago = False
                                                    End If
                                                    If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                                                        bFecha = True
                                                    Else
                                                        bFecha = False
                                                    End If
                                                    'basUtilidades.DatosConfigurablesAWord blankword, "IT_DEST", "IT_PAGO", "IT_FECSUM", bDestino, bPago, bFecha
                                                    
                                                    .Bookmarks("ITEM").Select
                                                    .Application.Selection.Delete
                                                    Set rangeword2 = docword.Bookmarks("ITEM").Range
                                                    
                                                    rangeword2.Copy
                                                    For Each oItem In oGrupo.Items
                                                        Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                                        sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                                        sCod = CStr(oItem.Id) & sCod
                                                        Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
                                                        
                                                        If Not oAdj Is Nothing Then
                                                            .Application.Selection.Paste
                                                            
                                                            If .Bookmarks.Exists("COD_ITEM") Then
                                                                .Bookmarks("COD_ITEM").Range.Text = NullToStr(oItem.ArticuloCod)
                                                            End If
                                                            If .Bookmarks.Exists("DEN_ITEM") Then
                                                                .Bookmarks("DEN_ITEM").Range.Text = NullToStr(oItem.Descr)
                                                            End If
                                                            
    
                                                            If oProce.DefDestino = EnItem Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                                                                If .Bookmarks.Exists("COD_DEST_ITEM") Then
                                                                    .Bookmarks("COD_DEST_ITEM").Range.Text = NullToStr(oItem.DestCod)
                                                                End If
                                                                If .Bookmarks.Exists("DEN_DEST_ITEM") Then
                                                                    .Bookmarks("DEN_DEST_ITEM").Range.Text = NullToStr(oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                                End If
                                                            End If
    
                                                            If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                                                                If .Bookmarks.Exists("FINI_SUM_ITEM") Then
                                                                    .Bookmarks("FINI_SUM_ITEM").Range.Text = Format(NullToStr(oItem.FechaInicioSuministro), "Short Date")
                                                                End If
                                                                If .Bookmarks.Exists("FFIN_SUM_ITEM") Then
                                                                    .Bookmarks("FFIN_SUM_ITEM").Range.Text = Format(NullToStr(oItem.FechaFinSuministro), "Short Date")
                                                                End If
    
                                                            End If
    
                                                            If .Bookmarks.Exists("CANTIDAD") Then
                                                                .Bookmarks("CANTIDAD").Range.Text = FormateoNumerico(oItem.Cantidad * (oAdj.Porcentaje / 100), m_sFormatoNumberCantGr)
                                                            End If
                                                            If .Bookmarks.Exists("COD_UNI") Then
                                                                .Bookmarks("COD_UNI").Range.Text = oItem.UniCod
                                                            End If
                                                            If .Bookmarks.Exists("DEN_UNI") Then
                                                                If Not oUnidades.Item(oItem.UniCod) Is Nothing Then
                                                                    .Bookmarks("DEN_UNI").Range.Text = oUnidades.Item(oItem.UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                                                End If
                                                            End If
                                                            If .Bookmarks.Exists("PRECOFE") Then
                                                                sComentario = ""
                                                                sComentario = RPT_FormulaDeAtributosUnitarioProveedor(oGrupo.Codigo, oProve.Cod, oItem.Id)
                                                                If sComentario <> "" Then
                                                                    .Bookmarks("PRECOFE").Range.Comments.Add Range:=.Bookmarks("PRECOFE").Range, Text:=sComentario
                                                                End If
                                                                .Bookmarks("PRECOFE").Range.Text = FormateoNumerico(VarToDbl0(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta), m_sFormatoNumberPrecioGr)
                                                            End If
                                                            If .Bookmarks.Exists("OBJETIVO") Then
                                                                If IsNumeric(oItem.Objetivo) Then
                                                                    .Bookmarks("OBJETIVO").Range.Text = FormateoNumerico(oItem.Objetivo, m_sFormatoNumberPrecioGr)
                                                                Else
                                                                    .Bookmarks("OBJETIVO").Range.Text = ""
                                                                End If
                                                            End If
                                                            
                                                            vConsumido = oItem.Cantidad * oItem.Precio * (oAdj.Porcentaje / 100)
                                                            If Not HayAplicar(TotalItem) Then
                                                                vAdjudicado = NullToDbl0(oItem.Cantidad) * NullToDbl0(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) * (oAdj.Porcentaje / 100)
                                                            Else
                                                                vAdjudicado = AplicarAtributosTotalItem(oGrupo.Codigo, oItem.Id, oProve.Cod)
                                                            End If
                                                            If .Bookmarks.Exists("IMPORTE") Then
                                                                sComentario = ""
                                                                sComentario = RPT_FormulaDeAtributosDeItemProveedor(oGrupo.Codigo, oProve.Cod, oItem.Id)
                                                                If sComentario <> "" Then
                                                                    .Bookmarks("IMPORTE").Range.Comments.Add Range:=.Bookmarks("IMPORTE").Range, Text:=sComentario
                                                                End If
                                                                .Bookmarks("IMPORTE").Range.Text = FormateoNumerico(vAdjudicado, m_sFormatoNumberGr) 'aplicados los atributos de total de item
                                                            End If
                                                            If .Bookmarks.Exists("AHORRO") Then
                                                                .Bookmarks("AHORRO").Range.Text = FormateoNumerico(vConsumido - vAdjudicado, m_sFormatoNumberGr)
                                                            End If
                                                            If .Bookmarks.Exists("AHORROPORCEN") And vConsumido <> 0 Then
                                                                .Bookmarks("AHORROPORCEN").Range.Text = FormateoNumerico(((vConsumido - vAdjudicado) / vConsumido) * 100, m_sFormatoNumberPorcenGr) & "%"
                                                            End If
                                                        End If
                                                        Set oOferta = Nothing
                                                        Set oAdj = Nothing
                                                    Next
                                                    .Bookmarks("ITEM").Delete
                                                    .Bookmarks("ITEM_TOD").Delete
                                                End If
                                            End If
                                        End If
                                    End If
                                End If 'bCargar
                            Next  'oGrupo
                        End If
                    End If   'Grupos
                 Next
             Else
                 If .Bookmarks.Exists("PROVEEDOR") Then
                     .Bookmarks("PROVEEDOR").Select
                     .Application.Selection.cut
                 End If
             End If
                 
            .Bookmarks("PROVEEDOR").Delete
            .Bookmarks("GRUPO").Delete
            .Bookmarks("GR_DEST").Delete
            .Bookmarks("GR_FECSUM").Delete
            .Bookmarks("GR_PAGO").Delete

        End If

        If docword.Bookmarks.Exists("PROVEEDOR_NOADJ") Then
            If chkProvNoAdj.Value = vbChecked Then
                If oProvesnoAdj.Count > 0 Then
                    .Bookmarks("PROVEEDOR_NOADJ").Select
                    .Application.Selection.Delete
                    Set rangeword5 = docword.Bookmarks("PROVEEDOR_NOADJ").Range
                    
                    For Each oProve In oProvesnoAdj
                        For Each oBook In .Bookmarks
                            oBook.Delete
                        Next
                    
                        'Muestra el proveedor con su precio:
                        rangeword5.Copy
                        .Application.Selection.Paste
                        If .Bookmarks.Exists("PROV_PROVEEDOR_NOADJ") Or .Bookmarks.Exists("PAIS_PROVEEDOR_NOADJ") Then
                            oProvesnoAdj.CargarDatosProveedor oProve.Cod
                        End If
                        If .Bookmarks.Exists("COD_PROVEEDOR_NOADJ") Then
                            .Bookmarks("COD_PROVEEDOR_NOADJ").Range.Text = oProve.Cod
                        End If
                        If .Bookmarks.Exists("NOM_PROVEEDOR_NOADJ") Then
                            .Bookmarks("NOM_PROVEEDOR_NOADJ").Range.Text = oProve.Den
                        End If
                        If .Bookmarks.Exists("PROV_PROVEEDOR_NOADJ") Then
                            .Bookmarks("PROV_PROVEEDOR_NOADJ").Range.Text = oProve.DenProvi
                        End If
                        If .Bookmarks.Exists("PAIS_PROVEEDOR_NOADJ") Then
                            .Bookmarks("PAIS_PROVEEDOR_NOADJ").Range.Text = oProve.DenPais
                        End If
                        dblPresupuesto = 0
                        dblImporte = 0
                        dblAhorrado = 0
                        dblAhorradoPorcen = 0
                        For Each oGrupo In oProce.Grupos
                            'Si no est� asignado al grupo no se pone
                            bCargar = True
                            If gParametrosGenerales.gbProveGrupos Then
                                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                    bCargar = False
                                End If
                            End If
                            If bCargar Then
                                bMostrarGr = True
                                If oProce.AdminPublica = True Then
                                    If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                        bMostrarGr = False
                                    End If
                                End If
                                If bMostrarGr = True Then
                                    sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                
                                    If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                        If oAdjsGrupo.Item(sCod).importe <> 0 Then
                                            dblPresupuesto = dblPresupuesto + (oAdjsGrupo.Item(sCod).Abierto / oAdjsGrupo.Item(sCod).Cambio)
                                        End If
                                        dblImporte = dblImporte + (oAdjsGrupo.Item(sCod).importe / oAdjsGrupo.Item(sCod).Cambio)
                                        dblAhorrado = dblAhorrado + (oAdjsGrupo.Item(sCod).AhorroOfe / oAdjsGrupo.Item(sCod).Cambio)
                                    End If
                                End If
                            End If
                        Next
                        dblImporte = AplicarAtributosDeProcesoNoAdj(dblImporte, oProve.Cod)
                        dblAhorrado = dblPresupuesto - dblImporte
                        If dblPresupuesto = 0 Then
                            dblAhorradoPorcen = 0
                        Else
                            dblAhorradoPorcen = (dblAhorrado / Abs(dblPresupuesto)) * 100
                        End If
                        If .Bookmarks.Exists("PRESUPUESTO_NOADJ") Then
                            .Bookmarks("PRESUPUESTO_NOADJ").Range.Text = FormateoNumerico(dblPresupuesto, m_sFormatoNumberGr)
                        End If
                        If .Bookmarks.Exists("IMPORTE_NOADJ") Then
                            sComentario = ""
                            sComentario = RPT_FormulaDeAtributosProcesoProveedor(oProve.Cod)
                            If sComentario <> "" Then
                                .Bookmarks("IMPORTE_NOADJ").Range.Select
                                .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                                .Application.Selection.TypeText Text:=sComentario
                                .Application.activewindow.ActivePane.Close
                            End If
                            .Bookmarks("IMPORTE_NOADJ").Range.Text = FormateoNumerico(dblImporte, m_sFormatoNumberGr)
                        End If
                        If .Bookmarks.Exists("AHORRO_NOADJ") Then
                            .Bookmarks("AHORRO_NOADJ").Range.Text = FormateoNumerico(dblAhorrado, m_sFormatoNumberGr)
                        End If
                        If .Bookmarks.Exists("PORCEN_NOADJ") Then
                            .Bookmarks("PORCEN_NOADJ").Range.Text = FormateoNumerico(dblAhorradoPorcen, m_sFormatoNumberPorcenGr) & "%"
                        End If
                        If .Bookmarks.Exists("OBS_PROVEEDOR_NO_ADJ") Then
                            Set oOferta = oProce.DevolverUltimaOfertaProveedor(oProve.Cod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)
                            .Bookmarks("OBS_PROVEEDOR_NO_ADJ").Range.Text = .Bookmarks("OBS_PROVEEDOR").Range.Text & NullToStr(oOferta.obs)
                        End If
                        If .Bookmarks.Exists("OBSERVACION_NOADJ") Then
                            Set oOferta = oProce.DevolverUltimaOfertaProveedor(oProve.Cod, , True, frmRESREU.sdbcFecReu.Columns(0).Value, basPublic.gParametrosInstalacion.gIdioma)
                        End If
                            
                        'GRUPOS
                        .Bookmarks("GRUPO_NOADJ").Select
                        Set rangeword4 = docword.Bookmarks("GRUPO_NOADJ").Range
                        .Bookmarks("GRUPO_NOADJ").Select
                        .Application.Selection.Delete
    
                        If oProce.Grupos.Count = 0 Then
                        Else
                            If Not oAdjsGrupo Is Nothing Then
                                For Each oGrupo In oProce.Grupos
                                    'Si no est� asignado al grupo no se pone
                                    bCargar = True
                                    If gParametrosGenerales.gbProveGrupos Then
                                        scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                        If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                            bCargar = False
                                        End If
                                    End If
                                    If bCargar Then
                                
                                        bMostrarGr = True
                                        If oProce.AdminPublica = True Then
                                            If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                                                bMostrarGr = False
                                            End If
                                        End If
                                        If bMostrarGr = True Then
                                            sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                            If Not oAdjsGrupo.Item(sCod) Is Nothing Then
                                                For Each oBook In .Bookmarks
                                                    oBook.Delete
                                                Next
                                                rangeword4.Copy
                                                .Application.Selection.Paste
                                                
                                                If .Bookmarks.Exists("COD_GRUPO_NOADJ") Then
                                                    .Bookmarks("COD_GRUPO_NOADJ").Select
                                                    If IsNull(oGrupo.Sobre) Then
                                                        .Bookmarks("COD_GRUPO_NOADJ").Range.Text = NullToStr(oGrupo.Codigo)
                                                    Else
                                                        .Bookmarks("COD_GRUPO_NOADJ").Range.Text = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
                                                    End If
                                                End If
                                                If .Bookmarks.Exists("DEN_GRUPO_NOADJ") Then
                                                    .Bookmarks("DEN_GRUPO_NOADJ").Select
                                                    .Bookmarks("DEN_GRUPO_NOADJ").Range.Text = NullToStr(oGrupo.Den)
                                                End If
                                                If oGrupo.DefDestino Then
                                                    If .Bookmarks.Exists("COD_DEST_GR_NOADJ") Then
                                                        .Bookmarks("COD_DEST_GR_NOADJ").Range.Text = NullToStr(oGrupo.DestCod)
                                                    End If
                                                    If .Bookmarks.Exists("DEN_DEST_GR_NOADJ") Then
                                                        .Bookmarks("DEN_DEST_GR_NOADJ").Range.Text = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                    End If
                                                    .Bookmarks("GR_DEST_NOADJ").Delete
                                                Else
                                                    If .Bookmarks.Exists("GR_DEST_NOADJ") Then
                                                       .Bookmarks("GR_DEST_NOADJ").Select
                                                       .Application.Selection.cut
                                                    End If
                                                End If
        
                                                If oGrupo.DefFechasSum Then
                                                    If .Bookmarks.Exists("FINI_SUM_GR_NOADJ") Then
                                                        .Bookmarks("FINI_SUM_GR_NOADJ").Range.Text = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
        
                                                    End If
                                                    If .Bookmarks.Exists("FFIN_SUM_GR_NOADJ") Then
                                                        .Bookmarks("FFIN_SUM_GR_NOADJ").Range.Text = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                                                    End If
                                                    .Bookmarks("GR_FECSUM_NOADJ").Delete
                                                Else
                                                    If .Bookmarks.Exists("GR_FECSUM_NOADJ") Then
                                                        .Bookmarks("GR_FECSUM_NOADJ").Select
                                                        .Application.Selection.cut
                                                    End If
                                                End If
        
                                                If oGrupo.DefFormaPago Then
                                                    If .Bookmarks.Exists("COD_PAGO_GR_NOADJ") Then
                                                        .Bookmarks("COD_PAGO_GR_NOADJ").Range.Text = NullToStr(oGrupo.PagCod)
                                                    End If
                                                    If .Bookmarks.Exists("DEN_PAGO_GR_NOADJ") Then
                                                       .Bookmarks("DEN_PAGO_GR_NOADJ").Range.Text = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                    End If
                                                    .Bookmarks("GR_PAGO_NOADJ").Delete
                                                Else
                                                    If .Bookmarks.Exists("GR_PAGO_NOADJ") Then
                                                       .Bookmarks("GR_PAGO_NOADJ").Select
                                                       .Application.Selection.cut
                                                    End If
                                                End If
                                                
                                                If .Bookmarks.Exists("PRES_AHORR_NOADJ") Then
                                                    .Bookmarks("PRES_AHORR_NOADJ").Select
                                                    .Bookmarks("PRES_AHORR_NOADJ").Range.Text = FormateoNumerico(oAdjsGrupo.Item(sCod).Abierto, m_sFormatoNumberGr)
                                                End If
                                                If .Bookmarks.Exists("IMPORTE_OFERTA_NOADJ") Then
                                                    sComentario = ""
                                                    sComentario = RPT_FormulaDeAtributosDeGrupoProveedor(oGrupo.Codigo, oProve.Cod)
                                                    If sComentario <> "" Then
                                                        .Bookmarks("IMPORTE_OFERTA_NOADJ").Range.Comments.Add Range:=.Bookmarks("IMPORTE_OFERTA_NOADJ").Range, Text:=sComentario
                                                    End If
                                                
                                                    .Bookmarks("IMPORTE_OFERTA_NOADJ").Select
                                                    .Bookmarks("IMPORTE_OFERTA_NOADJ").Range.Text = FormateoNumerico(oAdjsGrupo.Item(sCod).importe, m_sFormatoNumberGr)
                                                End If
                                                If .Bookmarks.Exists("AHORR_GR_NOADJ") Then
                                                    .Bookmarks("AHORR_GR_NOADJ").Select
                                                    .Bookmarks("AHORR_GR_NOADJ").Range.Text = FormateoNumerico(oAdjsGrupo.Item(sCod).AhorroOfe, m_sFormatoNumberGr)
                                                End If
                                                If .Bookmarks.Exists("PORCEN_GR_NOADJ") Then
                                                    .Bookmarks("PORCEN_GR_NOADJ").Select
                                                    .Bookmarks("PORCEN_GR_NOADJ").Range.Text = FormateoNumerico(oAdjsGrupo.Item(sCod).AhorroOfePorcen, m_sFormatoNumberPorcenGr) & "%"
                                                End If
                                                If Not bItems And docword.Bookmarks.Exists("ITEM_NOADJ") Then
                                                    oGrupo.CargarTodosLosItems OrdItemPorOrden
                                                    If HayAplicar(TotalItem) Then
                                                        oProce.Ofertas.CargarAtributosItemOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, oGrupo.Codigo, frmRESREU.sdbcFecReu.Columns(0).Value   'de �tem
                                                    End If
                                                End If
                                                
                                                If oGrupo.Items.Count = 0 Then
                                                    .Bookmarks("ITEM_TOD_NOADJ").Select
                                                    .Bookmarks("ITEM_TOD_NOAJD").Range.Delete
                                                ElseIf ExistenItemsConPrecios(oGrupo, oProve) = False Then
                                                    .Bookmarks("ITEM_TOD_NOADJ").Select
                                                    .Bookmarks.Item("ITEM_TOD_NOADJ").Range.Delete
                                                Else
                                                    If docword.Bookmarks.Exists("ITEM_NOADJ") Then
    
                                                        .Bookmarks("ITEM_NOADJ").Select
                                                        .Application.Selection.Delete
                                                        Set rangeword2 = docword.Bookmarks("ITEM_NOADJ").Range
                                                        For Each oItem In oGrupo.Items
                                                          Set oOferta = oGrupo.UltimasOfertas.Item(Trim(CStr(oProve.Cod)))
                                                            
                                                            rangeword2.Copy
                                                            .Application.Selection.Paste
                                                            If .Bookmarks.Exists("COD_ITEM_NOADJ") Then
                                                                .Bookmarks("COD_ITEM_NOADJ").Range.Text = NullToStr(oItem.ArticuloCod)
                                                            End If
                                                            If .Bookmarks.Exists("DEN_ITEM_NOADJ") Then
                                                                .Bookmarks("DEN_ITEM_NOADJ").Range.Text = NullToStr(oItem.Descr)
                                                            End If
    
                                                            If oProce.DefDestino = EnItem Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                                                                If .Bookmarks.Exists("COD_DEST_ITEM_NOADJ") Then
                                                                    .Bookmarks("COD_DEST_ITEM_NOADJ").Range.Text = NullToStr(oItem.DestCod)
                                                                End If
                                                                If .Bookmarks.Exists("DEN_DEST_ITEM_NOADJ") Then
                                                                    .Bookmarks("DEN_DEST_ITEM_NOADJ").Range.Text = NullToStr(oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                                End If
                                                            End If
        
                                                            If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                                                                If .Bookmarks.Exists("FINI_SUM_ITEM_NOADJ") Then
                                                                    .Bookmarks("FINI_SUM_ITEM_NOADJ").Range.Text = Format(NullToStr(oItem.FechaInicioSuministro), "Short Date")
                                                                End If
                                                                If .Bookmarks.Exists("FFIN_SUM_ITEM_NOADJ") Then
                                                                    .Bookmarks("FFIN_SUM_ITEM_NOADJ").Range.Text = Format(NullToStr(oItem.FechaFinSuministro), "Short Date")
                                                                End If
                                                            End If
    
                                                            If .Bookmarks.Exists("CANTIDAD_NOADJ") Then
                                                                .Bookmarks("CANTIDAD_NOADJ").Range.Text = FormateoNumerico(oItem.Cantidad, m_sFormatoNumberCantGr)
                                                            End If
                                                            If .Bookmarks.Exists("COD_UNI_NOADJ") Then
                                                                .Bookmarks("COD_UNI_NOADJ").Range.Text = oItem.UniCod
                                                            End If
                                                            If .Bookmarks.Exists("DEN_UNI_NOADJ") Then
                                                                If Not oUnidades.Item(oItem.UniCod) Is Nothing Then
                                                                    .Bookmarks("DEN_UNI_NOADJ").Range.Text = oUnidades.Item(oItem.UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                                                End If
                                                            End If
                                                            If .Bookmarks.Exists("OBJETIVO_NOADJ") Then
                                                                If IsNumeric(oItem.Objetivo) Then
                                                                    .Bookmarks("OBJETIVO_NOADJ").Range.Text = FormateoNumerico(oItem.Objetivo, m_sFormatoNumberPrecioGr)
                                                                Else
                                                                    .Bookmarks("OBJETIVO_NOADJ").Range.Text = ""
                                                                End If
                                                            End If
                                                            If Not oOferta Is Nothing Then
                                                              If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                                                                
                                                                If .Bookmarks.Exists("PRECOFE_NOADJ") Then
                                                                    sComentario = ""
                                                                    sComentario = RPT_FormulaDeAtributosUnitarioProveedor(oGrupo.Codigo, oProve.Cod, oItem.Id)
                                                                    If sComentario <> "" Then
                                                                        .Bookmarks("PRECOFE_NOADJ").Range.Comments.Add Range:=.Bookmarks("PRECOFE_NOADJ").Range, Text:=sComentario
                                                                    End If
                                                            
                                                                    dPrecioOfe = (oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) / oOferta.Cambio
                                                                    .Bookmarks("PRECOFE_NOADJ").Range.Text = FormateoNumerico(dPrecioOfe, m_sFormatoNumberPrecioGr)
                                                                End If
                                                                
                                                                sCod = CStr(oItem.Id) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                                                Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
                                                                If Not IsNull(oAdj.importe) Then
                                                                    dImporteOfe = oAdj.importe
                                                                Else
                                                                    dImporteOfe = NullToDbl0(oItem.Cantidad * dPrecioOfe)
                                                                End If
                                                                dAhorroOfe = (oItem.Cantidad * NullToDbl0(oItem.Precio)) - dImporteOfe
                                                                If (oItem.Cantidad * NullToDbl0(oItem.Precio)) = 0 Then
                                                                    dAhorroOfePorcen = 0
                                                                Else
                                                                    dAhorroOfePorcen = (dAhorroOfe / (oItem.Cantidad * NullToDbl0(oItem.Precio))) * 100
                                                                End If
                                                                
                                                                If .Bookmarks.Exists("IMPORTE_ITEM_NOADJ") Then
                                                                    sComentario = ""
                                                                    sComentario = RPT_FormulaDeAtributosDeItemProveedor(oGrupo.Codigo, oProve.Cod, oItem.Id, True)
                                                                    If sComentario <> "" Then
                                                                        .Bookmarks("IMPORTE_ITEM_NOADJ").Range.Comments.Add Range:=.Bookmarks("IMPORTE_ITEM_NOADJ").Range, Text:=sComentario
                                                                    End If
                                                                    .Bookmarks("IMPORTE_ITEM_NOADJ").Range.Text = FormateoNumerico(dImporteOfe, m_sFormatoNumberGr) 'aplicados los atributos de total de item
                                                                End If
                                                                If .Bookmarks.Exists("AHORRO_ITEM_NOADJ") Then
                                                                    .Bookmarks("AHORRO_ITEM_NOADJ").Range.Text = FormateoNumerico(dAhorroOfe, m_sFormatoNumberGr)
                                                                End If
                                                                If .Bookmarks.Exists("AHORROPORCEN_NOADJ") Then
                                                                    .Bookmarks("AHORROPORCEN_NOADJ").Range.Text = FormateoNumerico(dAhorroOfePorcen, m_sFormatoNumberPorcenGr) & "%"
                                                                End If
                                                                
                                                              Else
                                                                .Bookmarks("PRECOFE_NOADJ").Range.Text = ""
                                                                .Bookmarks("IMPORTE_ITEM_NOADJ").Range.Text = ""
                                                                .Bookmarks("AHORRO_ITEM_NOADJ").Range.Text = ""
                                                                .Bookmarks("AHORROPORCEN_NOADJ").Range.Text = ""
                                                              End If
                                                            Else
                                                                .Bookmarks("PRECOFE_NOADJ").Range.Text = ""
                                                                .Bookmarks("IMPORTE_ITEM_NOADJ").Range.Text = ""
                                                                .Bookmarks("AHORRO_ITEM_NOADJ").Range.Text = ""
                                                                .Bookmarks("AHORROPORCEN_NOADJ").Range.Text = ""
                                                            End If
                                                        Next
                                                        .Bookmarks.Item("ITEM_NOADJ").Delete
                                                        .Bookmarks.Item("ITEM_TOD_NOADJ").Delete
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If 'bCargar
                                Next 'oGrupo
                            End If
                        End If
                    
                    Next
                
                    .Bookmarks("PROVEEDOR_NOADJ").Delete
                    .Bookmarks("GRUPO_NOADJ").Delete
                    .Bookmarks("GR_DEST_NOADJ").Delete
                    .Bookmarks("GR_FECSUM_NOADJ").Delete
                    .Bookmarks("GR_PAGO_NOADJ").Delete
                
                Else
                    .Bookmarks("PROVEEDOR_NOADJ").Range.Delete
                End If
            Else
                .Bookmarks("PROVEEDOR_NOADJ").Range.Delete
            End If
        End If
        
    End With
    
Finalizar:
    On Error Resume Next
    
    If bError Then
        TipoActa2 = False
    Else
        TipoActa2 = True
    End If
    
    Set rangeword2 = Nothing
    Set rangeword3 = Nothing
    Set rangeword4 = Nothing
    Set oProves = Nothing
    Set oAdjs = Nothing
    Set oAdj = Nothing
    Set oItem = Nothing
    Set oIAdj = Nothing
    Set oiasignaciones = Nothing
    Set oAsignaciones = Nothing
    Set oEquipo = Nothing
    Set oOferta = Nothing
    Set oGrupo = Nothing
    Set oProve = Nothing
     
    Exit Function
    
ERROR:
    
    'Si el error es que no encuentra un formato o bookmark continuamos
    If err.Number = 5834 Or err.Number = 5941 Or err.Number = 9 Or err.Number = 91 Or err.Number = 4605 Then
        Resume Next
    End If
    
    If err.Number = 11 Then
    'Division by 0
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    Unload frmESPERA
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar

End Function

''' <summary>
''' Aplica los atributos de total de item
''' </summary>
''' <param name="sGrupo">Cod Grupo</param>
''' <param name="sItem">Id Item</param>
''' <param name="sProve">Cod Prove</param>
''' <returns>total de item</returns>
''' <remarks>Llamada desde: TipoActa1Crystal    TipoActa2Crystal; Tiempo m�ximo: 0,2</remarks>
Private Function AplicarAtributosTotalItem(ByVal sGrupo As String, ByVal sItem As Long, ByVal sProve As String) As Double
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim oOferta As COferta
    Dim sCod As String
    Dim scod1 As String
    Dim bSiguiente As Boolean
    Dim lngMinimo As Long
    Dim oatrib As CAtributo
    Dim iOrden As Integer
    Dim dValorAtrib As Double
    Dim lAtribAnterior As Long
    Dim dImporteAdj As Double
    Dim dImporteAdjProve As Double
    
    'Calcula el importe adj. aplicando las f�rmulas de total del item
    
    'si es un proceso de administraci�n p�blica,y el sobre asociado al grupo no est� visible no se
    'le aplican los atributos
    Set oGrupo = oProce.Grupos.Item(sGrupo)
    If oGrupo.proceso.AdminPublica = True Then
        If Not (oGrupo.proceso.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 1) Then
            Exit Function
        End If
    End If
    
    Set oItem = oGrupo.Items.Item(CStr(sItem))
    If oItem.Confirmado = True And oItem.Cerrado = False Then
        dImporteAdj = 0
        dImporteAdjProve = 0
                            
        sCod = sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
        sCod = CStr(oItem.Id) & sCod
            
        Set oOferta = oGrupo.UltimasOfertas.Item(Trim(sProve))
        If Not oOferta Is Nothing Then
            If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                'Importe adjudicado al proveedor sin atributos
                dImporteAdjProve = NullToDbl0(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) * (oItem.Cantidad * oGrupo.Adjudicaciones.Item(sCod).Porcentaje / 100)
            
                If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                    lAtribAnterior = 0
                    bSiguiente = False
                    iOrden = 1
                    
                    While bSiguiente = False
                        lngMinimo = 0
                        For Each oatrib In oProce.Atributos
                            'si el atributo aplica la f�rmula al total del item
                            If oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalItem And oatrib.UsarPrec = 1 Then
                                If oatrib.Orden = iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                    lngMinimo = oatrib.idAtribProce
                                    Exit For
                                Else
                                    If oatrib.Orden > iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                        If lngMinimo = 0 Then
                                            lngMinimo = oatrib.idAtribProce
                                        ElseIf (oatrib.Orden < oProce.Atributos.Item(CStr(lngMinimo)).Orden) Then
                                            lngMinimo = oatrib.idAtribProce
                                        End If
                                    End If
                                End If
                            End If
                        Next
                        If (lngMinimo = 0) Then
                            bSiguiente = True
                        ElseIf (oProce.Atributos.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                            bSiguiente = True
                        Else
                            dValorAtrib = 0
                            Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
                            lAtribAnterior = oatrib.idAtribProce
                            iOrden = oatrib.Orden
                            Select Case oatrib.ambito
                                Case 1
                                    'Proceso
                                     If Not oProce.Ofertas.Item(sProve).AtribProcOfertados Is Nothing Then
                                        If Not oProce.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                            dValorAtrib = NullToDbl0(oProce.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                        End If
                                      End If
                                    
                                Case 2
                                    'Grupo
                                    'aplica las formulas que tengan efecto sobre el precio total del item:
                                    If oGrupo.Codigo = oatrib.codgrupo Or IsNull(oatrib.codgrupo) Then
                                        scod1 = oGrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(oGrupo.Codigo))
                                        scod1 = scod1 & CStr(oatrib.idAtribProce)
                                        If Not oProce.Ofertas.Item(sProve).AtribGrOfertados.Item(scod1) Is Nothing Then
                                            dValorAtrib = NullToDbl0(oProce.Ofertas.Item(sProve).AtribGrOfertados.Item(scod1).valorNum)
                                        End If
                                    End If
                        
                                Case 3  'Item
                                    If Not oProce.Ofertas.Item(sProve).AtribItemOfertados Is Nothing Then
                                        If Not oProce.Ofertas.Item(sProve).AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                            dValorAtrib = NullToDbl0(oProce.Ofertas.Item(sProve).AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)).valorNum)
                                        End If
                                    End If
                            End Select
                            If oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-" Then
                                dValorAtrib = dValorAtrib / oOferta.Cambio
                            End If
                            
                            If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                Select Case oatrib.PrecioFormula
                                    Case "+"
                                        dImporteAdjProve = dImporteAdjProve + dValorAtrib
                                        
                                    Case "-"
                                        dImporteAdjProve = dImporteAdjProve - dValorAtrib
                                        
                                    Case "/"
                                        dImporteAdjProve = dImporteAdjProve / dValorAtrib
                                        
                                    Case "*"
                                        dImporteAdjProve = dImporteAdjProve * dValorAtrib
                                        
                                    Case "+%"
                                        dImporteAdjProve = dImporteAdjProve + (dImporteAdjProve * (dValorAtrib / 100))
                                        
                                    Case "-%"
                                        dImporteAdjProve = dImporteAdjProve - (dImporteAdjProve * (dValorAtrib / 100))
                                End Select
                            End If
                            
                            Set oatrib = Nothing
                            bSiguiente = False
                            iOrden = iOrden + 1
                            
                        End If
                    Wend
                End If
            End If
                
            If dImporteAdjProve <> 0 Then
                'Importe adjudicado de todo el item
                dImporteAdj = dImporteAdj + (dImporteAdjProve / oOferta.Cambio)
            End If
                
        End If
    End If
    Set oItem = Nothing
    Set oGrupo = Nothing
    Set oOferta = Nothing
    
    AplicarAtributosTotalItem = dImporteAdj
End Function


                                                                 
Private Function AplicarAtributosDeProceso(ByVal dAdjudicadoAtrib As Double, Optional ByVal sProve As String) As Double
    Dim oatrib As CAtributo
    Dim sProv As String
    Dim bSiguiente As Boolean
    Dim lngMinimo As Long
    Dim dValorAtrib As Double
    Dim iOrden As Integer
    Dim lAtribAnterior As Long
    Dim oOferta As COferta
    
    'Comprueba si existen atributos aplicables al total del proceso y en ese caso los aplica.
    If oProce.Atributos Is Nothing Then
        AplicarAtributosDeProceso = dAdjudicadoAtrib
        Exit Function
    End If
    If oProce.Atributos.Count = 0 Then
        AplicarAtributosDeProceso = dAdjudicadoAtrib
        Exit Function
    End If
    
    If Not HayAplicar(TipoAplicarAPrecio.TotalOferta) Then
        AplicarAtributosDeProceso = dAdjudicadoAtrib
        Exit Function
    End If
    
        
    'comprueba si se aplica o no atributos para �ste proveedor.S�lo se aplicar� si los items
    'de los grupos est�n todos asignados al mismo proveedor.
    If sProve = "" Then
        sProv = ComprobarAplicarAtribsProceso
    Else
        sProv = ComprobarAplicarAtribsProceso
        If sProv <> sProve Then
            AplicarAtributosDeProceso = dAdjudicadoAtrib
            Exit Function
        End If
    End If
    
    Set oOferta = oProce.Ofertas.Item(sProv)
    
    'Aplica los atributos
    lAtribAnterior = 0
    iOrden = 1
    bSiguiente = False
        
    While bSiguiente = False
        lngMinimo = 0
        For Each oatrib In oProce.Atributos
            If Not IsNull(oatrib.PrecioFormula) And oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalOferta And oatrib.UsarPrec = 1 Then
                If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                    lngMinimo = oatrib.idAtribProce
                    Exit For
                Else
                    If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                        If lngMinimo = 0 Then
                            lngMinimo = oatrib.idAtribProce
                        ElseIf (oatrib.Orden < oProce.Atributos.Item(CStr(lngMinimo)).Orden) Then
                            lngMinimo = oatrib.idAtribProce
                        ElseIf oatrib.idAtribProce = lngMinimo Then
                            bSiguiente = True
                            Exit For
                        End If
                    End If
                End If
            End If
        Next
        
        If (lngMinimo = 0) Then
            bSiguiente = True
        Else
            dValorAtrib = 0
            Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
            lAtribAnterior = oatrib.idAtribProce
            iOrden = oatrib.Orden

            If Not oOferta Is Nothing Then
                If Not oOferta.AtribProcOfertados Is Nothing Then
                    If Not oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                        dValorAtrib = NullToDbl0(oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                        If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                            ''''bAplicaAtrib = True
                            Select Case oatrib.PrecioFormula
                                Case "+"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib + (dValorAtrib / oOferta.Cambio)
                                Case "-"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib - (dValorAtrib / oOferta.Cambio)
                                Case "/"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib / dValorAtrib
                                Case "*"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib * dValorAtrib
                                Case "+%"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib + ((dValorAtrib / 100) * dAdjudicadoAtrib)
                                Case "-%"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib - ((dValorAtrib / 100) * dAdjudicadoAtrib)
                            End Select
                        End If
                    End If
                End If
            End If
            
            Set oatrib = Nothing
            bSiguiente = False
            iOrden = iOrden + 1
        End If
    Wend

    AplicarAtributosDeProceso = dAdjudicadoAtrib   ''''/ oProce.Ofertas.Item(sProv).Cambio
            
End Function


Private Function ComprobarAplicarAtribsProceso() As String
    Dim sProvAnt As String
    Dim bAdj As Boolean
    Dim sProv As String
    Dim oAdj As CAdjGrupo
    
    'ahora emmpieza a comprobar si se deshabilitar�n los atributos que afectan al total del proceso
    bAdj = True
    
    sProvAnt = ""
    For Each oAdj In oAdjsGrupo
        If oAdj.Adjudicado <> 0 Or oAdj.Consumido <> 0 Then
            sProv = oAdj.Prove
            If sProvAnt = "" Then sProvAnt = sProv
            If sProv <> sProvAnt Then
                bAdj = False
                Exit For
            End If
        End If
    Next
        
    If bAdj = False Then
        ComprobarAplicarAtribsProceso = ""
    Else
        ComprobarAplicarAtribsProceso = sProvAnt
    End If
    
End Function

''' <summary>
''' Construye un comentario con los precios
''' </summary>
''' <param name="sCodGrupo">Cod Grupo</param>
''' <param name="scodProve">Cod Prove</param>
''' <param name="IdItem">Id Item</param>
''' <returns>formula de los precios</returns>
''' <remarks>Llamada desde: TipoActa1  TipoActa2; Tiempo m�ximo: 0,2</remarks>
Private Function RPT_FormulaDeAtributosUnitarioProveedor(ByVal sCodGrupo As String, ByVal scodProve As String, ByVal IdItem As Long) As String
    Dim oGrupo As CGrupo
    Dim sCod As String
    Dim scod1 As String
    Dim bSiguiente As Boolean
    Dim oAsig As COferta
    Dim lngMinimo As Long
    Dim oatrib As CAtributo
    Dim iOrden As Integer
    Dim dValorAtrib As Double
    Dim lAtribAnterior As Long
    Dim sComentarioRPT As String

    Set oGrupo = oProce.Grupos.Item(sCodGrupo)
    
    'si el grupo est� cerrado no se aplican lo atributos
    If oGrupo.Cerrado = 1 Then
        RPT_FormulaDeAtributosUnitarioProveedor = ""
        Exit Function
    End If
        
    If oProce.Atributos Is Nothing Then
        RPT_FormulaDeAtributosUnitarioProveedor = ""
        Exit Function
    End If
    
    If oProce.Atributos.Count = 0 Then
        RPT_FormulaDeAtributosUnitarioProveedor = ""
        Exit Function
    End If
    
    If Not HayAplicar(TipoAplicarAPrecio.UnitarioItem) Then
        RPT_FormulaDeAtributosUnitarioProveedor = ""
        Exit Function
    End If
    
    sComentarioRPT = ""
    
    sCod = scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
    sCod = CStr(IdItem) & sCod
    Set oAsig = oGrupo.UltimasOfertas.Item(Trim(scodProve))
    
            lAtribAnterior = 0
             bSiguiente = False
             iOrden = 1
                
                While bSiguiente = False
                    lngMinimo = 0
                    For Each oatrib In oProce.Atributos
                        'si el atributo aplica la f�rmula al total del item
                        If oatrib.PrecioAplicarA = TipoAplicarAPrecio.UnitarioItem And oatrib.UsarPrec = 1 And (oatrib.codgrupo = sCodGrupo Or IsNull(oatrib.codgrupo)) Then
                            If oatrib.Orden = iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                lngMinimo = oatrib.idAtribProce
                                Exit For
                            Else
                                If oatrib.Orden > iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                    If lngMinimo = 0 Then
                                        lngMinimo = oatrib.idAtribProce
                                       
                                    ElseIf (oatrib.Orden < oProce.Atributos.Item(CStr(lngMinimo)).Orden) Then
                                        lngMinimo = oatrib.idAtribProce
                                    
                                    End If
                                End If
                            End If
                        End If
                    Next
                        
                    If (lngMinimo = 0) Then
                        bSiguiente = True
                    ElseIf (oProce.Atributos.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                        bSiguiente = True
                    Else
                        
                        dValorAtrib = 0
                        Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
                        lAtribAnterior = oatrib.idAtribProce
                        iOrden = oatrib.Orden
                        Select Case oatrib.ambito
                            Case 1
                                'Proceso
                                If Not oProce.Ofertas.Item(scodProve) Is Nothing Then
                                 If Not oProce.Ofertas.Item(scodProve).AtribProcOfertados Is Nothing Then
                                    If Not oProce.Ofertas.Item(scodProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                        dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                        If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                            Select Case oatrib.PrecioFormula
                                                Case "+"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " + " & dValorAtrib / oAsig.Cambio
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " + " & dValorAtrib / oAsig.Cambio
                                                    End If
                                                     
                                                Case "-"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " - " & dValorAtrib / oAsig.Cambio
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " - " & dValorAtrib / oAsig.Cambio
                                                    End If
                                                    
                                                Case "/"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " / " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " / " & dValorAtrib
                                                    End If
       
                                                Case "*"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " * " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " * " & dValorAtrib
                                                    End If

                                                Case "+%"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " +% " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " +% " & dValorAtrib
                                                    End If

                                                Case "-%"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " -% " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " -% " & dValorAtrib
                                                    End If

                                            End Select
                                        End If
                                    End If
                                  End If
                                End If
                                
                            Case 2
                                'Grupo
                                If Not oProce.Ofertas.Item(scodProve).AtribGrOfertados Is Nothing Then
                                    'aplica las formulas que tengan efecto sobre el precio total del item:
                                    If oGrupo.Codigo = oatrib.codgrupo Or IsNull(oatrib.codgrupo) Then
                                        scod1 = oGrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(oGrupo.Codigo))
                                        scod1 = scod1 & CStr(oatrib.idAtribProce)
                                        If Not oProce.Ofertas.Item(scodProve).AtribGrOfertados.Item(scod1) Is Nothing Then
                                            dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribGrOfertados.Item(scod1).valorNum)
                                            If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                            Select Case oatrib.PrecioFormula
                                                Case "+"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " + " & dValorAtrib / oAsig.Cambio
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " + " & dValorAtrib / oAsig.Cambio
                                                    End If
                                                    
                                                Case "-"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " - " & dValorAtrib / oAsig.Cambio
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " - " & dValorAtrib / oAsig.Cambio
                                                    End If
                                                    
                                                Case "/"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " / " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " / " & dValorAtrib
                                                    End If
                                                    
                                                Case "*"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " * " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " * " & dValorAtrib
                                                    End If

                                                Case "+%"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " +% " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " +% " & dValorAtrib
                                                    End If

                                                Case "-%"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " -% " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " -% " & dValorAtrib
                                                    End If
                                            End Select

                                            End If
                                        End If
                                    End If
                                End If
                    
                            Case 3  'Item
                                If Not oProce.Ofertas.Item(scodProve).AtribItemOfertados Is Nothing Then
                                    If Not oProce.Ofertas.Item(scodProve).AtribItemOfertados.Item(CStr(IdItem) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                        dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribItemOfertados.Item(CStr(IdItem) & "$" & CStr(oatrib.idAtribProce)).valorNum)
                                        If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                            Select Case oatrib.PrecioFormula
                                                Case "+"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " + " & dValorAtrib / oAsig.Cambio
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " + " & dValorAtrib / oAsig.Cambio
                                                    End If
                                                    
                                                Case "-"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " - " & dValorAtrib / oAsig.Cambio
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " - " & dValorAtrib / oAsig.Cambio
                                                    End If
                                                    
                                                Case "/"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " / " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " / " & dValorAtrib
                                                    End If
                                                    
                                                Case "*"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " * " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " * " & dValorAtrib
                                                    End If
                                                    
                                                Case "+%"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " +% " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " +% " & dValorAtrib
                                                    End If
                                                    
                                                Case "-%"
                                                    If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtPrecio & " -% " & dValorAtrib
                                                    Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtPrecio & " -% " & dValorAtrib
                                                    End If
       
                                            End Select
                                        End If
                                    End If
                                End If
                        End Select
                        Set oatrib = Nothing
                        bSiguiente = False
                        iOrden = iOrden + 1
                        
                    End If
                Wend
                
           
    Set oGrupo = Nothing
    
    
    RPT_FormulaDeAtributosUnitarioProveedor = sComentarioRPT
    

End Function

Private Function RPT_FormulaDeAtributosProcesoProveedor(Optional ByVal scodProve As String) As String

Dim oatrib As CAtributo
Dim bSiguiente As Boolean
Dim lngMinimo As Long
Dim dValorAtrib As Double
Dim iOrden As Integer
Dim lAtribAnterior As Long
Dim sAtrib As String
Dim sComentarioRPT As String
    
    If oProce.Atributos Is Nothing Then
        RPT_FormulaDeAtributosProcesoProveedor = ""
        Exit Function
    End If
    
    If oProce.Atributos.Count = 0 Then
        RPT_FormulaDeAtributosProcesoProveedor = ""
        Exit Function
    End If
    
    sAtrib = ""
    sComentarioRPT = ""
    
    If Not oProce.Atributos Is Nothing Then
        lAtribAnterior = 0
        iOrden = 1
        bSiguiente = False
        While bSiguiente = False
            lngMinimo = 0
            For Each oatrib In oProce.Atributos
                If Not IsNull(oatrib.PrecioFormula) And oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalOferta And oatrib.UsarPrec = 1 Then
                    If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                        lngMinimo = oatrib.idAtribProce
                        Exit For
                    Else
                        If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                            If lngMinimo = 0 Then
                                lngMinimo = oatrib.idAtribProce
                            ElseIf (oatrib.Orden < oProce.Atributos.Item(CStr(lngMinimo)).Orden) Then
                                lngMinimo = oatrib.idAtribProce
                            ElseIf oatrib.idAtribProce = lngMinimo Then
                                bSiguiente = True
                                Exit For
                            End If
                        End If
                    End If
                End If
            Next
                    
            If (lngMinimo = 0) Then
                bSiguiente = True
            Else
                If scodProve = "" Then
                    Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
                    lAtribAnterior = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                    Select Case oatrib.PrecioFormula
                        Case "+"
                            If sComentarioRPT <> "" Then
                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (+) "
                            Else
                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (+) "
                            End If

                        Case "-"
                            
                            If sComentarioRPT <> "" Then
                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (-) "
                            Else
                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (-) "
                            End If
                        Case "/"
                            
                            If sComentarioRPT <> "" Then
                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (/) "
                            Else
                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (/) "
                            End If
                        Case "*"
                            
                            If sComentarioRPT <> "" Then
                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (*) "
                            Else
                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (*) "
                            End If
                        Case "+%"
                        
                            If sComentarioRPT <> "" Then
                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (+%) "
                            Else
                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (+%) "
                            End If
                        Case "-%"
                            
                            If sComentarioRPT <> "" Then
                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (-%) "
                            Else
                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (-%) "
                            End If
                    End Select
                                    
                Else
                    dValorAtrib = 0
                    Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
                    lAtribAnterior = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                    If Not oProce.Ofertas.Item(scodProve) Is Nothing Then
                        If Not oProce.Ofertas.Item(scodProve).AtribProcOfertados Is Nothing Then
                            If Not oProce.Ofertas.Item(scodProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                        
                                    Select Case oatrib.PrecioFormula
                                        Case "+"
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                            End If
    
                                        Case "-"
                                            
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                            End If
                                        Case "/"
                                            
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                            End If
                                        Case "*"
                                            
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                            End If
                                        Case "+%"
                                        
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": m_stxtImp +% " & dValorAtrib
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                            End If
                                        Case "-%"
                                            
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                            End If
                                    End Select
                                End If
                            End If
                        End If
                    End If
                End If
                Set oatrib = Nothing
                bSiguiente = False
                iOrden = iOrden + 1
            End If
        Wend
    End If
    
    RPT_FormulaDeAtributosProcesoProveedor = sComentarioRPT
    
End Function

Private Function RPT_FormulaDeAtributosDeGrupoProveedor(ByVal sCodGrupo As String, Optional ByVal scodProve As String) As String
    Dim oatrib As CAtributo
    Dim bSiguiente As Boolean
    Dim lngMinimo As Long
    Dim dValorAtrib As Double
    Dim iOrden As Integer
    Dim lAtribAnterior As Long
    Dim sCod As String
    Dim scod1 As String
    Dim sComentarioRPT As String

    If oProce.Atributos Is Nothing Then
        RPT_FormulaDeAtributosDeGrupoProveedor = ""
        Exit Function
    End If
    
    If oProce.Atributos.Count = 0 Then
        RPT_FormulaDeAtributosDeGrupoProveedor = ""
        Exit Function
    End If
    
    sComentarioRPT = ""
        
    If Not oProce.Ofertas.Item(scodProve) Is Nothing Or scodProve = "" Then
        sCod = CStr(sCodGrupo) & scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
        
        lAtribAnterior = 0
        bSiguiente = False
        
         While bSiguiente = False
             lngMinimo = 0
             For Each oatrib In oProce.Atributos
                 'si el atributo aplica la f�rmula al grupo
                 If oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalGrupo Then
                     If Not oatrib.AtribTotalGrupo.Item(sCodGrupo) Is Nothing Then
                         If oatrib.AtribTotalGrupo.Item(sCodGrupo).UsarPrec = 1 Then
                            If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                lngMinimo = oatrib.idAtribProce
                                Exit For
                            Else
                                If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                    If lngMinimo = 0 Then
                                        lngMinimo = oatrib.idAtribProce
                                    ElseIf (oatrib.Orden < oProce.Atributos.Item(CStr(lngMinimo)).Orden) Then
                                        lngMinimo = oatrib.idAtribProce
                                    End If
                                End If
                            End If
                         End If
                     End If
                 End If
             Next
             
             If (lngMinimo = 0) Then
                 bSiguiente = True
             Else
                 If (oProce.Atributos.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                     bSiguiente = True
                 Else
                    If scodProve = "" Then
                        Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
                        lAtribAnterior = oatrib.idAtribProce
                        iOrden = oatrib.Orden
                        Select Case oatrib.ambito
                            Case 1  'Proceso
                                 'Comprueba los atributos a aplicar al precio unitario de �mbito proceso
                                    Select Case oatrib.PrecioFormula
                                        Case "+"
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (+) "
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (+) "
                                            End If
                                        Case "-"
                                            
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (-) "
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (-) "
                                            End If
                                        Case "/"
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (/) "
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (/) "
                                            End If
                                        Case "*"
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (*) "
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (*) "
                                            End If
                                       Case "+%"
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (+%) "
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (+%) "
                                            End If
                                       Case "-%"
                                            If sComentarioRPT <> "" Then
                                                sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (-%) "
                                            Else
                                                sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (-%) "
                                            End If
                                    End Select
                                        
                                 
                            Case 2 'Grupo
                                        
                                Select Case oatrib.PrecioFormula
                                     Case "+"
                                         If sComentarioRPT <> "" Then
                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (+) "
                                         Else
                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (+) "
                                         End If
                                     Case "-"
                                         
                                         If sComentarioRPT <> "" Then
                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (-) "
                                         Else
                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " /-) "
                                         End If
                                     Case "/"
                                         If sComentarioRPT <> "" Then
                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (/) "
                                         Else
                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (/) "
                                         End If
                                     Case "*"
                                         If sComentarioRPT <> "" Then
                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (*) "
                                         Else
                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (*) "
                                         End If
                                         
                                     Case "+%"
                                         If sComentarioRPT <> "" Then
                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (+%) "
                                         Else
                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (+%) "
                                         End If
                                     
                                     Case "-%"
                                         If sComentarioRPT <> "" Then
                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " (-%) "
                                         Else
                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " (-%) "
                                         End If
                                                                                                 
                                    End Select
                            End Select
                            
                    Else
                        dValorAtrib = 0
                        Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
                        lAtribAnterior = oatrib.idAtribProce
                        iOrden = oatrib.Orden
                        'si el atributo aplica la f�rmula al precio unitario del item
                        Select Case oatrib.ambito
                            Case 1  'Proceso
                                 'Comprueba los atributos a aplicar al precio unitario de �mbito proceso
                                 If Not oProce.Ofertas.Item(scodProve).AtribProcOfertados Is Nothing Then
                                     If Not oProce.Ofertas.Item(scodProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                         dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                         If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                             Select Case oatrib.PrecioFormula
                                                 Case "+"
                                                     If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                                     Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                                     End If
                                                 Case "-"
                                                     
                                                     If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                                     Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                                     End If
                                                 Case "/"
                                                     If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                     Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                     End If
                                                 Case "*"
                                                     If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                     Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                     End If
                                                Case "+%"
                                                     If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                     Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                     End If
                                                Case "-%"
                                                     If sComentarioRPT <> "" Then
                                                         sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                     Else
                                                         sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                     End If
                                             End Select
                                         End If
                                     End If
                                 End If
                                 
                            Case 2 'Grupo
                                        If oatrib.codgrupo = sCodGrupo Or IsNull(oatrib.codgrupo) Then
                                            If Not oProce.Ofertas.Item(scodProve).AtribGrOfertados Is Nothing Then
                                                scod1 = sCodGrupo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(sCodGrupo))
                                                scod1 = scod1 & CStr(oatrib.idAtribProce)
                                                If Not oProce.Ofertas.Item(scodProve).AtribGrOfertados.Item(scod1) Is Nothing Then
                                                    dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribGrOfertados.Item(scod1).valorNum)
                                                    If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                                        Select Case oatrib.PrecioFormula
                                                     Case "+"
                                                         If sComentarioRPT <> "" Then
                                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                                         Else
                                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                                         End If
                                                     Case "-"
                                                         
                                                         If sComentarioRPT <> "" Then
                                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                                         Else
                                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oProce.Ofertas.Item(scodProve).Cambio
                                                         End If
                                                     Case "/"
                                                         If sComentarioRPT <> "" Then
                                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                         Else
                                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                         End If
                                                     Case "*"
                                                         If sComentarioRPT <> "" Then
                                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                         Else
                                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                         End If
                                                         
                                                     Case "+%"
                                                         If sComentarioRPT <> "" Then
                                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                         Else
                                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                         End If
                                                     
                                                     Case "-%"
                                                         If sComentarioRPT <> "" Then
                                                             sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                         Else
                                                             sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                         End If
                                                                                                                 
                                                     End Select
                                                    End If
                                                End If
                                            End If
                                        End If
                            End Select
                        End If
                        Set oatrib = Nothing
                        bSiguiente = False
                        iOrden = iOrden + 1
                 End If
             End If
        Wend
    End If
    
    RPT_FormulaDeAtributosDeGrupoProveedor = sComentarioRPT
    

End Function

''' <summary>
''' Construye un comentario con la formula de los atributos
''' </summary>
''' <param name="sCodGrupo">Cod Grupo</param>
''' <param name="scodProve">Cod Prove</param>
''' <param name="IdItem">Id Item</param>
''' <returns>formula de los atributos</returns>
''' <remarks>Llamada desde: cmdImprimir_Click  ; Tiempo m�ximo: 0,2</remarks>
Private Function RPT_FormulaDeAtributosDeItemProveedor(ByVal sCodGrupo As String, ByVal scodProve As String, ByVal IdItem As Long, Optional ByVal bNoAdj As Boolean) As String
    Dim oGrupo As CGrupo
    Dim sCod As String
    Dim scod1 As String
    Dim bSiguiente As Boolean
    Dim oAsig As COferta
    Dim lngMinimo As Long
    Dim oatrib As CAtributo
    Dim iOrden As Integer
    Dim dValorAtrib As Double
    Dim lAtribAnterior As Long
    Dim sComentarioRPT As String

    If oProce.Atributos Is Nothing Then
        RPT_FormulaDeAtributosDeItemProveedor = ""
        Exit Function
    End If
    
    If oProce.Atributos.Count = 0 Then
        RPT_FormulaDeAtributosDeItemProveedor = ""
        Exit Function
    End If
    
    'Calcula el importe adj. aplicando las f�rmulas de total del item
    Set oGrupo = oProce.Grupos.Item(sCodGrupo)
    
    'si el grupo est� cerrado no se aplican lo atributos
    If oGrupo.Cerrado = 1 Then
        sComentarioRPT = ""
        Exit Function
    End If
        
    sComentarioRPT = ""
    
    sCod = scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
    sCod = CStr(IdItem) & sCod
    Set oAsig = oGrupo.UltimasOfertas.Item(Trim(scodProve))
    
    lAtribAnterior = 0
    bSiguiente = False
    iOrden = 1
                
    While bSiguiente = False
        lngMinimo = 0
        For Each oatrib In oProce.Atributos
            'si el atributo aplica la f�rmula al total del item
            If oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalItem And oatrib.UsarPrec = 1 And (oatrib.codgrupo = sCodGrupo Or IsNull(oatrib.codgrupo)) Then
                If oatrib.Orden = iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                    lngMinimo = oatrib.idAtribProce
                    Exit For
                Else
                    If oatrib.Orden > iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                        If lngMinimo = 0 Then
                            lngMinimo = oatrib.idAtribProce
                        ElseIf (oatrib.Orden < oProce.Atributos.Item(CStr(lngMinimo)).Orden) Then
                            lngMinimo = oatrib.idAtribProce
                        End If
                    End If
                End If
            End If
        Next
            
        If (lngMinimo = 0) Then
            bSiguiente = True
        ElseIf (oProce.Atributos.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
            bSiguiente = True
        Else
            
            dValorAtrib = 0
            Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
            lAtribAnterior = oatrib.idAtribProce
            iOrden = oatrib.Orden
            Select Case oatrib.ambito
                Case 1
                    'Proceso
                    If Not oProce.Ofertas.Item(scodProve) Is Nothing Then
                     If Not oProce.Ofertas.Item(scodProve).AtribProcOfertados Is Nothing Then
                        If Not oProce.Ofertas.Item(scodProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                            dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                            If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                Select Case oatrib.PrecioFormula
                                    Case "+"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oAsig.Cambio
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oAsig.Cambio
                                                 End If
                                            End If
                                        End If
                                    Case "-"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oAsig.Cambio
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oAsig.Cambio
                                                 End If
                                            End If
                                        End If
                                    Case "/"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "*"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "+%"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "-%"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                        
                                End Select
                            End If
                        End If
                      End If
                    End If
                    
                Case 2
                    'Grupo
                    If Not oProce.Ofertas.Item(scodProve).AtribGrOfertados Is Nothing Then
                        'aplica las formulas que tengan efecto sobre el precio total del item:
                        If oGrupo.Codigo = oatrib.codgrupo Or IsNull(oatrib.codgrupo) Then
                            scod1 = oGrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(oGrupo.Codigo))
                            scod1 = scod1 & CStr(oatrib.idAtribProce)
                            If Not oProce.Ofertas.Item(scodProve).AtribGrOfertados.Item(scod1) Is Nothing Then
                                dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribGrOfertados.Item(scod1).valorNum)
                                If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                Select Case oatrib.PrecioFormula
                                    Case "+"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oAsig.Cambio
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oAsig.Cambio
                                                 End If
                                                
                                            End If
                                        End If
                                    Case "-"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oAsig.Cambio
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oAsig.Cambio
                                                 End If
                                            End If
                                        End If
                                    Case "/"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "*"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "+%"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "-%"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                        
                                End Select

                                End If
                            End If
                        End If
                    End If
        
                Case 3  'Item
                    If Not oProce.Ofertas.Item(scodProve).AtribItemOfertados Is Nothing Then
                        If Not oProce.Ofertas.Item(scodProve).AtribItemOfertados.Item(CStr(IdItem) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                            dValorAtrib = NullToDbl0(oProce.Ofertas.Item(scodProve).AtribItemOfertados.Item(CStr(IdItem) & "$" & CStr(oatrib.idAtribProce)).valorNum)
                            If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                Select Case oatrib.PrecioFormula
                                    Case "+"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oAsig.Cambio
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / oAsig.Cambio
                                                 End If
                                            End If
                                        End If
                                    Case "-"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oAsig.Cambio
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / oAsig.Cambio
                                                 End If
                                            End If
                                        End If
                                    Case "/"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "*"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "+%"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                    Case "-%"
                                        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Or bNoAdj = True Then
                                            If oGrupo.Adjudicaciones.Item(sCod).Porcentaje > 0 Or bNoAdj = True Then
                                                If sComentarioRPT <> "" Then
                                                     sComentarioRPT = sComentarioRPT & vbLf & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                 Else
                                                     sComentarioRPT = sComentarioRPT & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib
                                                 End If
                                            End If
                                        End If
                                        
                                End Select
                            End If
                        End If
                    End If
            End Select
            Set oatrib = Nothing
            bSiguiente = False
            iOrden = iOrden + 1
            
        End If
    Wend
                
           
    Set oGrupo = Nothing
    
    
    RPT_FormulaDeAtributosDeItemProveedor = sComentarioRPT
    


End Function




Private Function AplicarAtributosDeProcesoNoAdj(ByVal dAdjudicadoAtrib As Double, ByVal sProv As String) As Double
    Dim oatrib As CAtributo
    Dim bSiguiente As Boolean
    Dim lngMinimo As Long
    Dim dValorAtrib As Double
    Dim iOrden As Integer
    Dim lAtribAnterior As Long
    Dim oOferta As COferta
    
    'Comprueba si existen atributos aplicables al total del proceso y en ese caso los aplica.
    If oProce.Atributos Is Nothing Then
        AplicarAtributosDeProcesoNoAdj = dAdjudicadoAtrib
        Exit Function
    End If
    If oProce.Atributos.Count = 0 Then
        AplicarAtributosDeProcesoNoAdj = dAdjudicadoAtrib
        Exit Function
    End If
    
    If Not HayAplicar(TotalOferta) Then
        AplicarAtributosDeProcesoNoAdj = dAdjudicadoAtrib
        Exit Function
    End If
    
    'Aplica los atributos
    Set oOferta = oProce.Ofertas.Item(sProv)
    If Not oOferta Is Nothing Then
       
        lAtribAnterior = 0
        iOrden = 1
        bSiguiente = False
        
        'Los aplico siempre porque son hipoterticos
        While bSiguiente = False
            lngMinimo = 0
            For Each oatrib In oProce.Atributos
                If Not IsNull(oatrib.PrecioFormula) And oatrib.PrecioAplicarA = TipoAplicarAPrecio.TotalOferta And oatrib.UsarPrec = 1 Then
                    If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                        lngMinimo = oatrib.idAtribProce
                        Exit For
                    Else
                        If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                            If lngMinimo = 0 Then
                                lngMinimo = oatrib.idAtribProce
                            ElseIf (oatrib.Orden < oProce.Atributos.Item(CStr(lngMinimo)).Orden) Then
                                lngMinimo = oatrib.idAtribProce
                            ElseIf oatrib.idAtribProce = lngMinimo Then
                                bSiguiente = True
                                Exit For
                            End If
                        End If
                    End If
                End If
            Next
            
            If (lngMinimo = 0) Then
                bSiguiente = True
            Else
                dValorAtrib = 0
                Set oatrib = oProce.Atributos.Item(CStr(lngMinimo))
                lAtribAnterior = oatrib.idAtribProce
                iOrden = oatrib.Orden
                    
                If Not oOferta.AtribProcOfertados Is Nothing Then
                    If Not oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                        dValorAtrib = NullToDbl0(oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                        If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                            Select Case oatrib.PrecioFormula
                                Case "+"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib + (dValorAtrib / oOferta.Cambio)
                                Case "-"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib - (dValorAtrib / oOferta.Cambio)
                                Case "/"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib / dValorAtrib
                                Case "*"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib * dValorAtrib
                                Case "+%"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib + ((dValorAtrib / 100) * dAdjudicadoAtrib)
                                Case "-%"
                                    dAdjudicadoAtrib = dAdjudicadoAtrib - ((dValorAtrib / 100) * dAdjudicadoAtrib)
                            End Select
                        End If
                    End If
                End If
                
                Set oatrib = Nothing
                bSiguiente = False
                iOrden = iOrden + 1
            End If
        Wend
            
    End If

    AplicarAtributosDeProcesoNoAdj = dAdjudicadoAtrib
    
End Function

''' <summary>
''' Determina Si Existen Items Con Precios
''' </summary>
''' <param name="oGrupo">Grupo</param>
''' <param name="oProve">Prove</param>
''' <returns>Si Existen Items Con Precios</returns>
''' <remarks>Llamada desde: TipoActa2 ; Tiempo m�ximo: 0,2</remarks>
Private Function ExistenItemsConPrecios(ByVal oGrupo As CGrupo, ByVal oProve As CProveedor) As Boolean
    Dim oItem As CItem
    Dim bExistenPrecios As Boolean
    
    bExistenPrecios = False
    
    If Not oGrupo.UltimasOfertas.Item(Trim(CStr(oProve.Cod))) Is Nothing Then
        For Each oItem In oGrupo.Items
            If Not oGrupo.UltimasOfertas.Item(Trim(CStr(oProve.Cod))).Lineas Is Nothing Then
                If Not IsNull(oGrupo.UltimasOfertas.Item(Trim(CStr(oProve.Cod))).Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                    bExistenPrecios = True
                    Exit For
                End If
            End If
        Next
    End If
    
    ExistenItemsConPrecios = bExistenPrecios
End Function

Private Function HayAplicar(ByVal udtTipo As TipoAplicarAPrecio) As Boolean
Dim oatrib As CAtributo
Dim bBool As Boolean
    
    bBool = False
    If oProce.Atributos Is Nothing Then
        HayAplicar = False
        Exit Function
    End If
        
    For Each oatrib In oProce.Atributos
        If oatrib.PrecioAplicarA = udtTipo Then
            bBool = True
            Exit For
        End If
    Next

    HayAplicar = bBool
End Function

''' <summary>
''' Rellenar un recordset con la informaci�n del grupo e item q estan siendon tratados. Aparte si es necesario
''' carga la colecci�n de atributos ofertados de los items del grupo.
''' </summary>
''' <param name="RS">Recordset a rellenar</param>
''' <param name="oGrupo">Grupo q esta siendo tratado</param>
''' <param name="oItem">Item q esta siendo tratado</param>
''' <param name="bCargadoYa">La colecci�n de atributos ofertados de los items del grupo esta cargada o no</param>
''' <returns>Recordset con informaci�n del grupo e item q esta siendo tratado</returns>
''' <remarks>Llamada desde: TipoActa1Crystal; Tiempo m�ximo: 0,2</remarks>
Private Function CargarDatosGrupo(ByVal rs As ADODB.Recordset, ByVal oGrupo As CGrupo, ByVal oItem As CItem, ByVal bCargadoYa As Boolean) As ADODB.Recordset
    If IsNull(oGrupo.Sobre) Then
        rs("COD_GRUPO").Value = NullToStr(oGrupo.Codigo)
    Else
        rs("COD_GRUPO").Value = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
    End If
    rs("DEN_GRUPO").Value = oGrupo.Den
    If oGrupo.DefDestino Then
        rs("COD_DEST_GR").Value = NullToStr(oGrupo.DestCod)
        rs("DEN_DEST_GR").Value = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
    End If

    If oGrupo.DefFechasSum Then
        rs("FINI_SUM_GR").Value = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
        rs("FFIN_SUM_GR").Value = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
    End If

    If oGrupo.DefFormaPago Then
        rs("COD_PAGO_GR").Value = NullToStr(oGrupo.PagCod)
        rs("DEN_PAGO_GR").Value = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
    End If
                        
    If HayAplicar(TotalItem) Then
        'CargarAtributosItemOfertados carga todos los atributos del grupo, no hace ningun caso a oItem
        'Entonces si en un grupo resulta q 2 � + items aplican al total se intenta cargar 2 o + veces
        'la misma colecci�n.
        If Not bCargadoYa Then
            oProce.Ofertas.CargarAtributosItemOfertados oProce.Anyo, oProce.GMN1Cod, oProce.Cod, oGrupo.Id, frmRESREU.sdbcFecReu.Columns(0).Value   'de �tem
        End If
    End If
    rs("ID_ITEM").Value = oItem.Id
    rs("COD_ITEM").Value = NullToStr(oItem.ArticuloCod)
    rs("DEN_ITEM").Value = NullToStr(oItem.Descr)
    rs("CANTIDAD").Value = oItem.Cantidad
    rs("COD_UNI").Value = oItem.UniCod
    rs("IMPORTE").Value = oItem.Precio
    
    If IsNumeric(oItem.Objetivo) Then
        rs("OBJETIVO").Value = oItem.Objetivo
    Else
         rs("OBJETIVO").Value = 0
    End If

    If oProce.DefDestino = EnItem Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
        rs("COD_DEST_ITEM").Value = NullToStr(oItem.DestCod)
        rs("DEN_DEST_ITEM").Value = NullToStr(oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
    End If

    If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
        rs("FINI_SUM_ITEM").Value = Format(NullToStr(oItem.FechaInicioSuministro), "Short Date")
        rs("FFIN_SUM_ITEM").Value = Format(NullToStr(oItem.FechaFinSuministro), "Short Date")
    End If
    If oProce.DefFormaPago = EnItem Or (oProce.DefFormaPago = EnGrupo And Not oGrupo.DefFormaPago) Then
        rs("COD_PAGO_ITEM").Value = NullToStr(oItem.PagCod)
        rs("DEN_PAGO_ITEM").Value = NullToStr(oPagos.Item(oItem.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
    End If
    Set CargarDatosGrupo = rs
    Set rs = Nothing

End Function

Private Sub Timer1_Timer()
    If WebBrowser1.ReadyState = READYSTATE_COMPLETE Then
        Picture1(0).Visible = False
        Picture1(1).Visible = False
        Timer1.Enabled = False
    End If
End Sub

Private Sub WebBrowser1_WindowClosing(ByVal IsChildWindow As Boolean, Cancel As Boolean)
    Unload frmACT
End Sub
