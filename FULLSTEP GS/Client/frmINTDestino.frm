VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmINTDestino 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "dDestino "
   ClientHeight    =   3030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmINTDestino.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   7680
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdFTP 
      Caption         =   "..."
      Height          =   285
      Left            =   3180
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   480
      Width           =   315
   End
   Begin VB.CommandButton cmdNTFSRecibos 
      Height          =   285
      Left            =   7260
      Picture         =   "frmINTDestino.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   1680
      Width           =   315
   End
   Begin VB.TextBox txtDestinoRecibosFSGS 
      Height          =   285
      Left            =   1680
      TabIndex        =   2
      Top             =   1680
      Width           =   5565
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   2640
      TabIndex        =   3
      Top             =   2310
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   3765
      TabIndex        =   4
      Top             =   2310
      Width           =   1005
   End
   Begin VB.TextBox txtDestino 
      Height          =   285
      Left            =   1680
      TabIndex        =   1
      Top             =   1080
      Width           =   5565
   End
   Begin VB.CommandButton cmdNTFS 
      Height          =   285
      Left            =   7260
      Picture         =   "frmINTDestino.frx":0D71
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1080
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcTipo 
      Height          =   285
      Left            =   1680
      TabIndex        =   0
      Top             =   480
      Width           =   1485
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2249
      Columns(0).Caption=   "Tipo"
      Columns(0).Name =   "DEN"
      Columns(0).CaptionAlignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "COD"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2619
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin VB.Label lblDestinoRecibos 
      BackColor       =   &H00808000&
      Caption         =   "DRuta:"
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   120
      TabIndex        =   9
      Top             =   1680
      Width           =   1530
   End
   Begin VB.Label lblTipo 
      BackColor       =   &H00808000&
      Caption         =   "DTipo :"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   930
   End
   Begin VB.Label LblDestino 
      BackColor       =   &H00808000&
      Caption         =   "DRuta:"
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   120
      TabIndex        =   8
      Top             =   1080
      Width           =   1530
   End
End
Attribute VB_Name = "frmINTDestino"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
''' Objeto para utilizar el m�todo BrowseForFolder que visualiza una dialog box para seleccionar carpetas
Public m_oEntidadInt As CEntidadInt
Public m_oERPsIntDestino As CERPsInt
Private m_iTipo As Integer
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Public m_bConsulta As Boolean
Public m_sEntidad As String
Public m_sCodERP As String
Public m_sTabla As String
Private m_oEntidadesIntegracion As CEntidadesInt
'''
''' Variables para idiomas
'''
Private m_sTipoDestinoNTFS As String
Private m_sTipoDestinoFTP As String
Private m_sTipoDestinoWEB As String
Private m_sTipoDestinoWCF As String
Private m_sConexionDestinoWCF As String

Private m_aDestinoEntidad(1 To 3) As String
Private m_aDestinoReciboFSGS(1 To 3) As String
Private m_sSelCarpeta As String
Private m_sConfFTP As String
Private m_sConfWEB As String

''' <summary>
''' Cargar las variables de pantalla multiidioma
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo:0 </remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DEST_INTEGRACION, basPublic.gParametrosInstalacion.gIdioma)
    '''@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    '''TextoProvisional
    
    If Not Ador Is Nothing Then
        caption = m_sEntidad & ": " & Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblTipo.caption = Ador(0).Value
        Ador.MoveNext
        m_sTipoDestinoNTFS = Ador(0).Value
        LblDestino.caption = Ador(0).Value
        Ador.MoveNext
        m_aDestinoEntidad(1) = Ador(0).Value
        m_aDestinoEntidad(2) = Ador(0).Value
        m_aDestinoEntidad(3) = Ador(0).Value
        Ador.MoveNext
        'm_sSelCarpeta = Ador(0).Value
        Ador.MoveNext
        'lblFormato = Ador(0).Value
        Ador.MoveNext
        'optXML.Caption = ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        'optTXT.Caption = ador(0).Value
        m_aDestinoReciboFSGS(1) = Ador(0).Value
        m_aDestinoReciboFSGS(2) = Ador(0).Value
        m_aDestinoReciboFSGS(3) = Ador(0).Value
        Ador.MoveNext
        m_sTipoDestinoFTP = Ador(0).Value
        Ador.MoveNext
        m_sConfFTP = Ador(0).Value
        Ador.MoveNext
        m_sTipoDestinoWEB = Ador(0).Value
        Ador.MoveNext
        m_sConfWEB = Ador(0).Value
        Ador.MoveNext
        m_sTipoDestinoWCF = Ador(0).Value
        'm_sTipoDestinoWCF = "Servicio WCF"
        Ador.MoveNext
        m_sConexionDestinoWCF = Ador(0).Value
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

''' <summary>
''' Responde a la pulsaci�n del bot�n Aceptar. Graba en bbdd los datos del destino.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1 </remarks>
Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim formato_Export As FormatoExportacionIntegracion
If sdbcTipo.Text <> m_sTipoDestinoWCF Then
    If txtDestino.Text = "" And txtDestinoRecibosFSGS = "" Then
       oMensajes.NoValido LblDestino.caption
       Exit Sub
    End If
 End If
 
 If sdbcTipo.Text = "" Then
    oMensajes.NoValido lblTipo.caption
    Exit Sub
 End If
 If m_iTipo = 0 Then
     oMensajes.NoValido lblTipo.caption
    Exit Sub
 End If
 
 If Not m_bConsulta Then
     If sdbcTipo.Text = m_sTipoDestinoFTP Then
        If NullToStr(m_oEntidadInt.FTP_Ip) = "" Then
            oMensajes.FaltanDatos (m_sConfFTP)
            Exit Sub
        End If
    End If
    If UCase(sdbcTipo.Text) = m_sTipoDestinoWEB Then
        If NullToStr(m_oEntidadInt.WEB_Dest_Url) = "" Then
            oMensajes.FaltanDatos (m_sConfWEB)
            Exit Sub
        End If
    End If

    teserror = m_oEntidadInt.ModificarDestino(m_iTipo, txtDestino.Text, txtDestinoRecibosFSGS.Text)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    frmCONFIntegracion.m_oEntidadesInt.CargarTodasLasEntidades (m_sCodERP)
 End If

 Unload Me
End Sub

Private Sub cmdCancelar_Click()
 Unload Me
End Sub

''' <summary>
''' Nos muestra la configuraci�n Ftp � WebService dependiendo del tipo seleccionado
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
Private Sub cmdFTP_Click()

    If UCase(sdbcTipo.Value) = m_sTipoDestinoWEB Then
        frmINTWebService.m_tipodestino = WEBSERVICE
        frmINTWebService.m_bConsulta = m_bConsulta
        frmINTWebService.m_sEntidad = m_sEntidad
        frmINTWebService.m_sfrmLlamada = "frmINTDestino"
        frmINTWebService.Left = Me.Left + 100
        frmINTWebService.Top = Me.Top + 1500
        
        Load frmINTWebService
        
        frmINTWebService.Show 1
    ElseIf sdbcTipo.Value = m_sTipoDestinoWCF Then
        frmINTWebService.m_tipodestino = WCF
        frmINTWebService.m_sfrmLlamada = "frmINTDestino"
        frmINTWebService.m_bConsulta = m_bConsulta
        frmINTWebService.m_sEntidad = m_sEntidad
        frmINTWebService.Left = Me.Left + 100
        frmINTWebService.Top = Me.Top + 1500
        
        frmINTWebService.caption = m_sConexionDestinoWCF & " " & m_sEntidad
        Load frmINTWebService
        
        frmINTWebService.Show 1
        
    Else
        frmINTFTP.m_bConsulta = m_bConsulta
        frmINTFTP.m_sEntidad = m_sEntidad
        frmINTFTP.m_sfrmLlamada = "frmINTDestino"
        frmINTFTP.Left = Me.Left + 100
        frmINTFTP.Top = Me.Top + 1500
        Load frmINTFTP
        frmINTFTP.Show 1
        
    End If
End Sub

Private Sub cmdNTFS_Click()
Dim sBuffer As String
Dim sInicio As String

On Error GoTo Error

sInicio = txtDestino.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, m_sSelCarpeta, sInicio)

If sBuffer <> "" Then
    txtDestino.Text = sBuffer
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    txtDestino.Text = ""
End Sub

Private Sub cmdNTFSRecibos_Click()
Dim sBuffer As String
Dim sInicio As String

On Error GoTo Error

sInicio = txtDestinoRecibosFSGS.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, m_sSelCarpeta, sInicio)

If sBuffer <> "" Then
    txtDestinoRecibosFSGS.Text = sBuffer
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    txtDestinoRecibosFSGS.Text = ""
    
End Sub

''' <summary>
''' Carga e inicializa la pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
Private Sub Form_Load()

Dim oERPInt As CERPInt

    Me.Height = 3500 ''2900
    
    CargarRecursos
    
    ''MultiERP:Se cargan los diferentes ERP
    Set m_oERPsIntDestino = Nothing
    Set m_oERPsIntDestino = oFSGSRaiz.generar_CERPsInt
    m_oERPsIntDestino.CargarTodosLosERPs
    
    sdbcTipo.AddItem m_sTipoDestinoNTFS & Chr(9) & CStr(tipodestinointegracion.NTFS)
    sdbcTipo.AddItem m_sTipoDestinoFTP & Chr(9) & CStr(tipodestinointegracion.FTP)
    sdbcTipo.AddItem m_sTipoDestinoWEB & Chr(9) & CStr(tipodestinointegracion.WEBSERVICE)
    sdbcTipo.AddItem m_sTipoDestinoWCF & Chr(9) & CStr(tipodestinointegracion.WCF)
    
' QUITAR COMENTARIO CUANDO EST� CODIFICADO EL FORMATO TXT
'    If m_oEntidadInt.FormatoExportacion = xml Then
'        optXML.Value = True
'    Else
'        optTXT.Value = True
'    End If
    
    m_sTabla = m_oEntidadInt.Entidad
    
    ''Se cargan las rutas destino
    txtDestino = NullToStr(m_oEntidadInt.Destino)
    If Not IsNull(m_oEntidadInt.DestinoTipo) Then
        m_iTipo = m_oEntidadInt.DestinoTipo
    End If
    If m_iTipo = tipodestinointegracion.FTP Then
        txtDestino = NullToStr(m_oEntidadInt.FTP_Destino)
        txtDestinoRecibosFSGS = NullToStr(m_oEntidadInt.FTP_RecibosFSGS)
    Else
        txtDestino = NullToStr(m_oEntidadInt.Destino)
        txtDestinoRecibosFSGS = NullToStr(m_oEntidadInt.DestinoRecibosFSGS)
    End If
    
    If m_oEntidadInt.DestinoTipo = tipodestinointegracion.FTP Or m_oEntidadInt.DestinoTipo = tipodestinointegracion.WEBSERVICE Or m_oEntidadInt.DestinoTipo = tipodestinointegracion.WCF Then
        cmdFTP.Visible = True
    Else
        cmdFTP.Visible = False
    End If
        
    If m_bConsulta Then ' En modo consulta todo protegido
        sdbcTipo.Enabled = False
        txtDestino.Enabled = False
        txtDestinoRecibosFSGS.Enabled = False
        cmdNTFS.Enabled = False
        cmdNTFSRecibos.Enabled = False
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
    Else
        sdbcTipo.Enabled = True
        txtDestino.Enabled = True
        txtDestinoRecibosFSGS.Enabled = True
        cmdNTFS.Enabled = True
        cmdNTFSRecibos.Enabled = True
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
    End If


    Select Case m_oEntidadInt.DestinoTipo ' la label toma una caption u otra en funci�n del tipo de destino
    Case tipodestinointegracion.NTFS
        LblDestino.caption = m_aDestinoEntidad(1)
        lblDestinoRecibos.caption = m_aDestinoReciboFSGS(1)
    Case tipodestinointegracion.FTP
        LblDestino.caption = m_aDestinoEntidad(2)
        lblDestinoRecibos.caption = m_aDestinoReciboFSGS(2)
    Case tipodestinointegracion.WCF
        'lblDestino.caption = "Url" '''@@@@@@@@@@@@@@@@@@@texto de base de datos
        'lblDestinoRecibos.caption = "Borrar"
    Case Else
        LblDestino.caption = m_aDestinoEntidad(3)
        lblDestinoRecibos.caption = m_aDestinoReciboFSGS(3)
    End Select
    
    cmdNTFS.Visible = True
    cmdNTFSRecibos.Visible = True
    
    mostrarOcultarDestinos
End Sub


Private Sub Form_Unload(Cancel As Integer)

Set m_oEntidadInt = Nothing
Set m_oERPsIntDestino = Nothing
Set m_oEntidadesIntegracion = Nothing

End Sub

Private Sub sdbcTipo_Change()
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        'm_iTipo = 0
    End If
    
End Sub

''' <summary>
''' Tras seleccionar un tipo, establece a que rutas se tiene acceso
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
Private Sub sdbcTipo_CloseUp()

    bRespetarCombo = True
    sdbcTipo.Text = sdbcTipo.Columns("DEN").Text
    bRespetarCombo = False
    
    m_iTipo = sdbcTipo.Columns("COD").Value
    
    cmdFTP.Visible = False
    cmdNTFS.Enabled = True
    cmdNTFSRecibos.Enabled = True
    
    If m_iTipo = tipodestinointegracion.NTFS Then
        LblDestino.caption = m_aDestinoEntidad(1)
        lblDestinoRecibos.caption = m_aDestinoReciboFSGS(1)
    ElseIf m_iTipo = tipodestinointegracion.FTP Then
        LblDestino.caption = m_aDestinoEntidad(2)
        lblDestinoRecibos.caption = m_aDestinoReciboFSGS(2)
        
        cmdFTP.Visible = True
    ElseIf m_iTipo = tipodestinointegracion.WCF Then
    
        cmdFTP.Visible = True
    Else 'web
        LblDestino.caption = m_aDestinoEntidad(3)
        lblDestinoRecibos.caption = m_aDestinoReciboFSGS(3)
        
        cmdFTP.Visible = True
    End If
    
    mostrarOcultarDestinos
    
    txtDestino.Text = ""
    txtDestinoRecibosFSGS.Text = ""
    bCargarComboDesde = False

End Sub

Private Sub mostrarOcultarDestinos()
    If m_iTipo = tipodestinointegracion.WCF Then
       Me.LblDestino.Visible = False
       Me.lblDestinoRecibos.Visible = False
       Me.txtDestino.Visible = False
       Me.txtDestinoRecibosFSGS.Visible = False
       Me.cmdNTFS.Visible = False
       Me.cmdNTFSRecibos.Visible = False
    Else
       Me.LblDestino.Visible = True
       Me.lblDestinoRecibos.Visible = True
       Me.txtDestino.Visible = True
       Me.txtDestinoRecibosFSGS.Visible = True
       Me.cmdNTFS.Visible = True
       Me.cmdNTFSRecibos.Visible = True
    End If
End Sub
Private Sub sdbcTipo_InitColumnProps()
    sdbcTipo.DataFieldList = "Column 0"
    sdbcTipo.DataFieldToDisplay = "Column 0"
End Sub

Public Sub sdbcTipo_Validate(Cancel As Boolean)
    If sdbcTipo.Text = "" Then Exit Sub
   ' m_iTipo = sdbcTipo.Columns("COD").Value
    bCargarComboDesde = False
End Sub


    




