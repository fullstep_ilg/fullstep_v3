VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSolicitudDetalle 
   Caption         =   "DSolicitud"
   ClientHeight    =   7380
   ClientLeft      =   60
   ClientTop       =   705
   ClientWidth     =   13995
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudDetalle.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7380
   ScaleWidth      =   13995
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   4995
      Left            =   195
      TabIndex        =   14
      Top             =   1680
      Width           =   13455
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   14
      stylesets.count =   10
      stylesets(0).Name=   "CalculadoSucio"
      stylesets(0).ForeColor=   255
      stylesets(0).BackColor=   16766421
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSolicitudDetalle.frx":0CB2
      stylesets(1).Name=   "Calculado"
      stylesets(1).BackColor=   16766421
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSolicitudDetalle.frx":0CCE
      stylesets(1).AlignmentPicture=   1
      stylesets(2).Name=   "Sucio"
      stylesets(2).ForeColor=   255
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSolicitudDetalle.frx":0CEA
      stylesets(3).Name=   "BajaLogica"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(3).Picture=   "frmSolicitudDetalle.frx":0D06
      stylesets(4).Name=   "Gris"
      stylesets(4).BackColor=   -2147483633
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSolicitudDetalle.frx":0D22
      stylesets(5).Name=   "Amarillo"
      stylesets(5).BackColor=   12648447
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmSolicitudDetalle.frx":0D3E
      stylesets(6).Name=   "RiesgoBajo"
      stylesets(6).ForeColor=   32768
      stylesets(6).BackColor=   16777215
      stylesets(6).Picture=   "frmSolicitudDetalle.frx":0D5A
      stylesets(7).Name=   "CalculadoValor"
      stylesets(7).BackColor=   16766421
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmSolicitudDetalle.frx":0D76
      stylesets(8).Name=   "RiesgoAlto"
      stylesets(8).ForeColor=   202
      stylesets(8).BackColor=   16777215
      stylesets(8).Picture=   "frmSolicitudDetalle.frx":0D92
      stylesets(9).Name=   "RiesgoMedio"
      stylesets(9).ForeColor=   4227327
      stylesets(9).BackColor=   16777215
      stylesets(9).Picture=   "frmSolicitudDetalle.frx":0DAE
      DividerType     =   0
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   14
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "CAMPO"
      Columns(1).Name =   "CAMPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).Style=   1
      Columns(1).ButtonsAlways=   -1  'True
      Columns(2).Width=   5001
      Columns(2).Caption=   "VALOR"
      Columns(2).Name =   "VALOR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1588
      Columns(3).Caption=   "AYUDA"
      Columns(3).Name =   "AYUDA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   4
      Columns(3).ButtonsAlways=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "TIPO"
      Columns(4).Name =   "TIPO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "SUBTIPO"
      Columns(5).Name =   "SUBTIPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "INTRO"
      Columns(6).Name =   "INTRO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "CAMPO_GS"
      Columns(7).Name =   "CAMPO_GS"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "COD_VALOR"
      Columns(8).Name =   "COD_VALOR"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "BAJALOGICA"
      Columns(9).Name =   "BAJALOGICA"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   11
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "GENERICO"
      Columns(10).Name=   "GENERICO"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "RIESGO"
      Columns(11).Name=   "RIESGO"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "TABLA_EXTERNA"
      Columns(12).Name=   "TABLA_EXTERNA"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   2
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "ID_TABLA_EXTERNA"
      Columns(13).Name=   "ID_TABLA_EXTERNA"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      _ExtentX        =   23733
      _ExtentY        =   8811
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdInfProcesosPed 
      Height          =   285
      Left            =   10185
      Picture         =   "frmSolicitudDetalle.frx":0DCA
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "Detalle"
      Top             =   810
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcVersion 
      Height          =   285
      Left            =   10185
      TabIndex        =   3
      Top             =   120
      Width           =   3450
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   16777215
      ForeColorOdd    =   16777215
      BackColorEven   =   -2147483647
      BackColorOdd    =   -2147483647
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "NUM"
      Columns(0).Name =   "NUM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3016
      Columns(1).Caption=   "FECHA"
      Columns(1).Name =   "FECHA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasHeadForeColor=   -1  'True
      Columns(2).Width=   3519
      Columns(2).Caption=   "USU"
      Columns(2).Name =   "USU"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasHeadForeColor=   -1  'True
      _ExtentX        =   6085
      _ExtentY        =   503
      _StockProps     =   93
      ForeColor       =   16777215
      BackColor       =   -2147483647
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddArticulos 
      Height          =   1575
      Left            =   840
      TabIndex        =   35
      Top             =   3240
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   2461
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   5530
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "GENERICO"
      Columns(2).Name =   "GENERICO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   11
      Columns(2).FieldLen=   10
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "PREC_ULT_ADJ"
      Columns(3).Name =   "PREC_ULT_ADJ"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "CODPROV_ULT_ADJ"
      Columns(4).Name =   "CODPROV_ULT_ADJ"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "DENPROV_ULT_ADJ"
      Columns(5).Name =   "DENPROV_ULT_ADJ"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "UNI"
      Columns(6).Name =   "UNI"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin VB.PictureBox picControl 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   13995
      TabIndex        =   36
      Top             =   7005
      Width           =   13995
      Begin VB.CommandButton cmdEnviarPed 
         Caption         =   "En&viar a pedido"
         Height          =   345
         Left            =   11960
         TabIndex        =   26
         Top             =   0
         Width           =   1320
      End
      Begin VB.CommandButton cmdPedidoDir 
         Caption         =   "Pedido directo"
         Height          =   345
         Left            =   10730
         TabIndex        =   25
         Top             =   0
         Width           =   1200
      End
      Begin VB.CommandButton cmdEnviarProc 
         Caption         =   "Enviar a proceso"
         Height          =   345
         Left            =   9330
         TabIndex        =   24
         Top             =   0
         Width           =   1400
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "&Imprimir"
         Height          =   345
         Left            =   7200
         TabIndex        =   22
         Top             =   0
         Width           =   855
      End
      Begin VB.CommandButton cmdReabrir 
         Caption         =   "R&eabrir"
         Height          =   345
         Left            =   4470
         TabIndex        =   19
         Top             =   0
         Width           =   930
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   6300
         TabIndex        =   21
         Top             =   0
         Width           =   850
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "&Cerrar"
         Height          =   345
         Left            =   3510
         TabIndex        =   18
         Top             =   0
         Width           =   930
      End
      Begin VB.CommandButton cmdAbrirProc 
         Caption         =   "Abrir &proceso"
         Height          =   345
         Left            =   8100
         TabIndex        =   23
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnular 
         Caption         =   "&Anular"
         Height          =   345
         Left            =   5430
         TabIndex        =   20
         Top             =   0
         Width           =   850
      End
      Begin VB.CommandButton cmdRechazar 
         Caption         =   "&Rechazar"
         Height          =   345
         Left            =   1005
         TabIndex        =   16
         Top             =   0
         Width           =   930
      End
      Begin VB.CommandButton cmdAprobar 
         Caption         =   "&Aprobar"
         Height          =   345
         Left            =   40
         TabIndex        =   15
         Top             =   0
         Width           =   930
      End
      Begin VB.CommandButton cmdAsignar 
         Caption         =   "A&signar comprador"
         Height          =   345
         Left            =   1965
         TabIndex        =   17
         Top             =   0
         Width           =   1515
      End
   End
   Begin VB.CommandButton cmdDetalleProce 
      Height          =   285
      Index           =   0
      Left            =   8595
      Picture         =   "frmSolicitudDetalle.frx":110C
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Detalle"
      Top             =   810
      Width           =   315
   End
   Begin VB.CommandButton cmdDetalleProce 
      Height          =   285
      Index           =   2
      Left            =   8595
      Picture         =   "frmSolicitudDetalle.frx":144E
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Detalle"
      Top             =   465
      Width           =   315
   End
   Begin VB.CommandButton cmdDetalleProce 
      Height          =   285
      Index           =   1
      Left            =   13320
      Picture         =   "frmSolicitudDetalle.frx":1790
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Detalle"
      Top             =   465
      Width           =   315
   End
   Begin VB.CommandButton cmdComentarios 
      Height          =   285
      Left            =   8595
      Picture         =   "frmSolicitudDetalle.frx":1AD2
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Comentarios"
      Top             =   120
      Width           =   315
   End
   Begin VB.PictureBox picEdicion 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   13995
      TabIndex        =   37
      Top             =   6630
      Visible         =   0   'False
      Width           =   13995
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer cambios"
         Height          =   345
         Left            =   1655
         TabIndex        =   28
         Top             =   0
         Width           =   1515
      End
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "&Guardar cambios"
         Height          =   345
         Left            =   40
         TabIndex        =   27
         Top             =   0
         Width           =   1515
      End
   End
   Begin MSComctlLib.TabStrip ssTabGrupos 
      Height          =   5775
      Left            =   60
      TabIndex        =   13
      Top             =   1200
      Width           =   13785
      _ExtentX        =   24315
      _ExtentY        =   10186
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddFecha 
      Height          =   500
      Left            =   0
      TabIndex        =   40
      Top             =   3000
      Width           =   3000
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5636
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5292
      _ExtentY        =   882
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
      Height          =   915
      Left            =   0
      TabIndex        =   41
      Top             =   3000
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSolicitudDetalle.frx":1BE3
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5318
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddDestinos 
      Height          =   1260
      Left            =   0
      TabIndex        =   42
      Top             =   3000
      Width           =   7350
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSolicitudDetalle.frx":1BFF
      stylesets(1).Name=   "Espec"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSolicitudDetalle.frx":1C1B
      BevelColorFace  =   12632256
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   1958
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadBackColor=   -2147483633
      Columns(0).BackColor=   16777215
      Columns(1).Width=   7038
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HeadBackColor=   -2147483633
      Columns(2).Width=   3200
      Columns(2).Caption=   "Direcci�n"
      Columns(2).Name =   "DIR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HeadBackColor=   -2147483633
      Columns(3).Width=   2090
      Columns(3).Caption=   "Poblaci�n"
      Columns(3).Name =   "POB"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasHeadBackColor=   -1  'True
      Columns(3).HeadBackColor=   -2147483633
      Columns(4).Width=   1138
      Columns(4).Caption=   "CP"
      Columns(4).Name =   "CP"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasHeadBackColor=   -1  'True
      Columns(4).HeadBackColor=   -2147483633
      Columns(5).Width=   926
      Columns(5).Caption=   "Pa�s"
      Columns(5).Name =   "PAI"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).HasHeadBackColor=   -1  'True
      Columns(5).HeadBackColor=   -2147483633
      Columns(6).Width=   1402
      Columns(6).Caption=   "Provincia"
      Columns(6).Name =   "PROVI"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).HasHeadBackColor=   -1  'True
      Columns(6).HeadBackColor=   -2147483633
      _ExtentX        =   12965
      _ExtentY        =   2222
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddPagos 
      Height          =   1575
      Left            =   0
      TabIndex        =   43
      Top             =   3000
      Width           =   2895
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1085
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4868
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddOtrosCombos 
      Height          =   1575
      Left            =   0
      TabIndex        =   44
      Top             =   1200
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2461
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   5530
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "GENERICO"
      Columns(2).Name =   "GENERICO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   11
      Columns(2).FieldLen=   10
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddAlmacenes 
      Height          =   1575
      Left            =   6120
      TabIndex        =   45
      Top             =   1680
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "dID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2461
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   3
      Columns(2).Width=   5530
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   50
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddEmpresas 
      Height          =   1575
      Left            =   0
      TabIndex        =   47
      Top             =   0
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "dID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2461
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   3
      Columns(2).Width=   5530
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   50
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00FFFFFF&
      Height          =   1095
      Left            =   60
      Top             =   60
      Width           =   13755
   End
   Begin VB.Label lblInf 
      Caption         =   "DInformaci�n de Procesos/Pedidos"
      Height          =   255
      Left            =   10545
      TabIndex        =   39
      Top             =   840
      Width           =   3105
   End
   Begin VB.Label lblVersion 
      Caption         =   "DVersi�n:"
      Height          =   255
      Left            =   9135
      TabIndex        =   38
      Top             =   150
      Width           =   1005
   End
   Begin VB.Label lblFecAlta 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1845
      TabIndex        =   4
      Top             =   465
      Width           =   1770
   End
   Begin VB.Label lblEstado 
      Alignment       =   2  'Center
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5070
      TabIndex        =   1
      Top             =   120
      Width           =   3495
   End
   Begin VB.Label lblPeticionario 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5070
      TabIndex        =   9
      Top             =   465
      Width           =   3495
   End
   Begin VB.Label lblLitId 
      Caption         =   "DIdentificador:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   140
      TabIndex        =   46
      Top             =   150
      Width           =   1200
   End
   Begin VB.Label lblIdentificador 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1845
      TabIndex        =   0
      Top             =   120
      Width           =   1770
   End
   Begin VB.Label lblLitEstado 
      Caption         =   "DEstado:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3855
      TabIndex        =   34
      Top             =   150
      Width           =   1215
   End
   Begin VB.Label lblLitComprador 
      Caption         =   "DComprador:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9135
      TabIndex        =   33
      Top             =   495
      Width           =   1005
   End
   Begin VB.Label lblLitFecAlta 
      Caption         =   "DFecha alta:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   140
      TabIndex        =   32
      Top             =   495
      Width           =   1200
   End
   Begin VB.Label lblLitPeticionario 
      Caption         =   "DPeticionario:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3855
      TabIndex        =   31
      Top             =   495
      Width           =   1200
   End
   Begin VB.Label lblLitFecAprob 
      Caption         =   "DFecha aprobaci�n:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   140
      TabIndex        =   30
      Top             =   840
      Width           =   1530
   End
   Begin VB.Label lblFecAprobacion 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1845
      TabIndex        =   11
      Top             =   810
      Width           =   1770
   End
   Begin VB.Label lblComprador 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   10185
      TabIndex        =   7
      Top             =   465
      Width           =   3075
   End
   Begin VB.Label lblAprobador 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5070
      TabIndex        =   5
      Top             =   810
      Width           =   3495
   End
   Begin VB.Label lblLitAprobador 
      Caption         =   "DAprobador:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3855
      TabIndex        =   29
      Top             =   840
      Width           =   1215
   End
End
Attribute VB_Name = "frmSolicitudDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas:
Public g_oSolicitudSeleccionada As CInstancia
Public g_sOrigen As String
Public g_oGrupoSeleccionado As CFormGrupo
Public m_dicFrmDesgloseValores As Dictionary
Public g_ofrmDesgloseValores As frmSolicitudDesglose    'El formulario de desglose activo en cada momento
Public g_bSoloLectura As Boolean
Public m_lIDSOLPadre As Long

Public m_sUON1Sel As String
Public m_sUON2Sel As String
Public m_sUON3Sel As String
Public m_sUODescrip As String

Public g_strPK As String
'Variables privadas:
Private m_strPK_old As String
Private m_oIBaseDatos As IBaseDatos
Private m_oPagos As CPagos
Private m_oDestinos As CDestinos
Private m_oPaises As CPaises
Private m_oUnidades As CUnidades
Private g_ofrmDesglose As frmDesglose
Private m_oGruposBD As CFormGrupos
Private m_bUpdate As Boolean

'Variables de idiomas:
Private m_sPendiente As String
Private m_sAprobada As String
Private m_sRechazada As String
Private m_sGenRechazada As String
Private m_sAnulada As String
Private m_sCerrada As String
Private m_sEnCurso As String
Private m_sDatosGen As String
Private m_sIdiTipoFecha(17) As String
Private m_sMensaje(1 To 7) As String
Private m_sIdiDesglose As String
Private m_sIdiFalse As String
Private m_sIdiTrue As String
Private m_sIdiKB As String
Private m_sIdiErrorEvaluacion(2) As String

'Seguridad
Private m_bRuo As Boolean
Private m_bRDep As Boolean
Private m_bREquipo As Boolean
Private m_bRAsig As Boolean
Private m_bAprobRechazoCierre As Boolean
Private m_bAnular As Boolean
Private m_bEliminar As Boolean
Private m_bAsignar As Boolean
Private m_bReabrir As Boolean
Private m_bModificar As Boolean
Private m_bAbrirProc As Boolean
Private m_bEnviarProc As Boolean
Private m_bPedidoDirecto As Boolean
Private m_bModifUltimaVersion As Boolean
Private m_bRestrDest As Boolean
Private m_bRestrMat As Boolean
Private m_bRestrProve As Boolean
Private m_bAbrirProcEnCurso As Boolean
Private m_bRestrPresUO As Boolean
Private m_bEnviarPedido As Boolean

Private m_bPresBajaLog As Boolean
Private m_bPermProcMultimaterial As Boolean  'Permitir abrir procesos multimaterial

Private m_intTabla As Integer                           'paso de vbles entre Click y BeforeUpdate
Private m_blnExisteArticulo As Boolean
Private m_blnExisteTablaExternaParaArticulo As Boolean
Private m_oTablaExterna As CTablaExterna            'clase que almacena funciones

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String

Public oFSIS As CFSISService

'GS. Detalle de solicitud. Aplicar la cumplimentaci�n de los campos del formulario.
Private m_oCumplimentacionesGS As CSolCumplimentacionesGs
Private m_sLinea As String

Private Sub cmdAbrirProc_Click()
    Dim Ador As Ador.Recordset
    Dim iResp As Integer
        
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    If m_bPermProcMultimaterial Then
        Screen.MousePointer = vbHourglass
    
        Set Ador = g_oSolicitudSeleccionada.DevolverMaterialesInstancia_Campo
        If Ador.RecordCount > 1 Then
            iResp = oMensajes.MensajeYesNo(948)
            If iResp = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Set Ador = g_oSolicitudSeleccionada.DevolverMaterialesInstancia_Desglose
            If Ador.RecordCount > 1 Then
                iResp = oMensajes.MensajeYesNo(948)
                If iResp = vbNo Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
        End If
    End If
    
    Dim arSolicitudes(0) As CInstancia
    Set arSolicitudes(0) = g_oSolicitudSeleccionada
        
    frmSOLAbrirProc.g_arSolicitudes = arSolicitudes
    frmSOLAbrirProc.m_iGenerar = 0
    Screen.MousePointer = vbNormal
    frmSOLAbrirProc.Show vbModal
    Set Ador = Nothing

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdAbrirProc_Click", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub cmdAnular_Click()
    'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudDetalle"
    frmSolicitudGestionar.g_sOrigen2 = g_sOrigen
    frmSolicitudGestionar.g_iAccion = 1
    Set frmSolicitudGestionar.g_oSolicitud = g_oSolicitudSeleccionada
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdAnular_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdAprobar_Click()
    'Llama al formulario para aprobar la solicitud
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    frmSolicitudAprobar.g_sOrigen = "frmSolicitudDetalle"
    frmSolicitudAprobar.g_sOrigen2 = g_sOrigen
    Set frmSolicitudAprobar.g_oSolicitud = g_oSolicitudSeleccionada
    frmSolicitudAprobar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdAprobar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdAsignar_Click()
    'Llama al formulario para aprobar la solicitud
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    frmSolicitudAsignarComp.g_sOrigen = "frmSolicitudDetalle"
    frmSolicitudAsignarComp.g_sOrigen2 = g_sOrigen
    frmSolicitudAsignarComp.g_bRestricEquipo = m_bREquipo
    Set frmSolicitudAsignarComp.g_oSolicitud = g_oSolicitudSeleccionada
    frmSolicitudAsignarComp.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdAsignar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCerrar_Click()
    'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudDetalle"
    frmSolicitudGestionar.g_sOrigen2 = g_sOrigen
    frmSolicitudGestionar.g_iAccion = 2
    Set frmSolicitudGestionar.g_oSolicitud = g_oSolicitudSeleccionada
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdCerrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdComentarios_Click()

    'Muestra un formulario con los comentarios
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set frmSolicitudComentarios.g_oSolicitudSeleccionada = g_oSolicitudSeleccionada
    frmSolicitudComentarios.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdComentarios_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdDeshacer_Click()
    Dim oGrupo As CFormGrupo

    'Limpia todos los campos y carga el actual
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each oGrupo In g_oSolicitudSeleccionada.Grupos
        Set oGrupo.CAMPOS = Nothing
        Set oGrupo.CAMPOS = m_oGruposBD.Item(CStr(oGrupo.Id)).CAMPOS
    Next
    
    ''Carga otra vez el grupo actual
    GrupoSeleccionado
    
    picControl.Visible = True
    picEdicion.Visible = False
    
    'Si est� abierta la pantalla de desglose
    CerrarFormulariosDesglose
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdDeshacer_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Devuelve un formulario de desglose</summary>
''' <returns>Formulario desglose</returns>
''' <remarks>Llamada desde: sdbgCampos_BtnClick</remarks>
Private Function ObtenerFormularioDesglose(ByVal sKey As String) As frmSolicitudDesglose
    Dim oFrmDesgloseValores As frmSolicitudDesglose
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
    
    If m_dicFrmDesgloseValores Is Nothing Then Set m_dicFrmDesgloseValores = New Dictionary
    
    If Not m_dicFrmDesgloseValores.Exists(sKey) Then
        Set oFrmDesgloseValores = New frmSolicitudDesglose
        m_dicFrmDesgloseValores.Add sKey, oFrmDesgloseValores
    Else
        Set oFrmDesgloseValores = m_dicFrmDesgloseValores.Item(sKey)
    End If
    
    Set ObtenerFormularioDesglose = oFrmDesgloseValores
    
Salir:
    Set oFrmDesgloseValores = Nothing
    Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ObtenerFormularioDesglose", err, Erl, Me, m_bActivado, m_sMsgError)
      Resume Salir
   End If
End Function

''' <summary>Cierra los formularios de desglose que est�n abiertos</summary>
''' <remarks>Llamada desde: cmdDeshacer_Click</remarks>
Private Sub CerrarFormulariosDesglose()
    Dim i As Integer
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not m_dicFrmDesgloseValores Is Nothing Then
        For i = 0 To m_dicFrmDesgloseValores.Count - 1
            Unload m_dicFrmDesgloseValores.Items(i)
            Set m_dicFrmDesgloseValores.Items(i) = Nothing
        Next
        
        Set m_dicFrmDesgloseValores = Nothing
    End If
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "CerrarFormulariosDesglose", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdDetalleProce_Click(Index As Integer)
    'Muestra el detalle del comprador,del peticionario o del aprobador:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Index
        Case 0
            'Aprobador
            If g_oSolicitudSeleccionada.Aprobador Is Nothing Then Exit Sub
            frmDetallePersona.g_sPersona = g_oSolicitudSeleccionada.Aprobador.Cod
            
        Case 1
            'comprador
            If g_oSolicitudSeleccionada.comprador Is Nothing Then Exit Sub
            frmDetallePersona.g_sPersona = g_oSolicitudSeleccionada.comprador.Cod
            
        Case 2
            'Peticionario
            frmDetallePersona.g_sPersona = g_oSolicitudSeleccionada.Peticionario.Cod
            
    End Select
    
    frmDetallePersona.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdDetalleProce_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdEliminar_Click()
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim Ador As Ador.Recordset

    'Elimina la solicitud que est� seleccionada en la grid
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    irespuesta = oMensajes.PreguntaEliminarSolicitudes(CStr(g_oSolicitudSeleccionada.Id))

    If irespuesta = vbNo Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    g_oSolicitudSeleccionada.Usuario = basOptimizacion.gvarCodUsuario
    
    Set Ador = g_oSolicitudSeleccionada.DevolverProcesosRelacionados
    If Not Ador Is Nothing Then
        If Ador.RecordCount > 0 Then
            oMensajes.MensajeOKOnly 931, Exclamation  'Imposible eliminar la solicitud. Existen procesos asociados
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    Set m_oIBaseDatos = g_oSolicitudSeleccionada
    
    teserror = m_oIBaseDatos.EliminarDeBaseDatos

    If teserror.NumError <> TESnoerror Then
        Set m_oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        
    Else
        'Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCSolicEli, "Id:" & g_oSolicitudSeleccionada.Id

        Set g_oSolicitudSeleccionada = Nothing
        
        'Elimina la solicitud de la grid y de la colecci�n en el formulario de solicitudes
        frmSolicitudes.ActualizarGridEliminar
        
        Set m_oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
    
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdEliminar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdEnviarPed_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirWizardCrearPedido False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdEnviarPed_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdEnviarProc_Click()
    Dim vbm As Variant
    
    'Env�a la solicitud a un proceso ya existente:
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
        
    frmSOLEnviarAProc.g_lNumVersion = CLng(sdbcVersion.Columns("NUM").CellValue(vbm))
    frmSOLEnviarAProc.g_lIdSOL = g_oSolicitudSeleccionada.Id
    frmSOLEnviarAProc.Show vbModal

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdEnviarProc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdGuardar_Click()
    Dim teserror As TipoErrorSummit
    Dim oCampo As CFormItem
    Dim oGrupo As CFormGrupo
    Dim bGuardar As Boolean
    Dim oLinea As CLineaDesglose
    Dim rsCabecera As ADODB.Recordset
    Dim rsLinea As ADODB.Recordset
    Dim rsInfo As ADODB.Recordset
    Dim oColOK As Collection
    Dim oColKO As Collection
    Dim iDispSC As Integer
    Dim oColLineas As Collection
    Dim oColCompleta As Collection
    Dim iImpSC As Integer
    Dim sListaOblig As String
    Dim oSubCampo As CFormItem
    Dim LineasDesgl() As String
    Dim LineasDesglAux As String
    Dim Indice As Integer
    Dim Encontrado As Boolean
    Dim LaLinea As Integer
    
    'Guarda los cambios realizados a la solicitud:
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
    End If
        
    'Comprueba si se ha cambiado algo en las colecciones.Si no se ha cambiado nada no guarda
    For Each oGrupo In g_oSolicitudSeleccionada.Grupos
        If Not oGrupo.CAMPOS Is Nothing Then
            For Each oCampo In oGrupo.CAMPOS
                If oCampo.modificado = True Then
                    bGuardar = True
                    Exit For
                End If
                
                If Not oCampo.LineasDesglose Is Nothing Then
                    For Each oLinea In oCampo.LineasDesglose
                        If oLinea.modificado = True Or oLinea.eliminado = True Or oLinea.Anyadido = True Then
                            bGuardar = True
                            Exit For
                        End If
                    Next
                End If
            Next
        End If
    Next
   
    If bGuardar = False Then Exit Sub
            
    'Ahora guarda en la base de datos:
    Screen.MousePointer = vbHourglass
    
    'Carga las colecciones de desglose que no estuviesen cargadas:
    For Each oGrupo In g_oSolicitudSeleccionada.Grupos
        If Not oGrupo.CAMPOS Is Nothing Then
            For Each oCampo In oGrupo.CAMPOS
                If oCampo.CampoGS = TipoCampoGS.Desglose Or oCampo.Tipo = TiposDeAtributos.TipoDesglose Then
                    If oCampo.Desglose Is Nothing Then
                        oCampo.CargarDesglose
                    End If
                End If
            Next
        Else
            oGrupo.CargarTodosLosCampos
            For Each oCampo In oGrupo.CAMPOS
                If oCampo.CampoGS = TipoCampoGS.Desglose Or oCampo.Tipo = TiposDeAtributos.TipoDesglose Then
                    If oCampo.Desglose Is Nothing Then
                        oCampo.CargarDesglose
                    End If
                End If
            Next
        End If
    Next
    
    If g_sOrigen = "frmSolicitudes" Then
        sListaOblig = ""
        For Each oGrupo In g_oSolicitudSeleccionada.Grupos
            If Not oGrupo.CAMPOS Is Nothing Then
                For Each oCampo In oGrupo.CAMPOS
                    If oCampo.Tipo = TiposDeAtributos.TipoDesglose Then
                        If oCampo.LineasDesglose Is Nothing Then
                            oCampo.DevolverLineasDesglose m_sIdiTrue, m_sIdiFalse
                        End If
                        
                        If oCampo.LineasDesglose.Count = 0 Then
                        Else
                            LineasDesglAux = ""
                            ReDim LineasDesgl(0)
                            For Each oLinea In oCampo.LineasDesglose
                                If Not oLinea.eliminado And InStr(LineasDesglAux, "@@" & CStr(oLinea.Linea)) = 0 Then
                                    LineasDesglAux = LineasDesglAux & "@@" & CStr(oLinea.Linea)
                                    
                                    ReDim Preserve LineasDesgl(UBound(LineasDesgl) + 1)
                                    LineasDesgl(UBound(LineasDesgl)) = CStr(oLinea.Linea)
                                End If
                            Next
                                For Each oSubCampo In oCampo.Desglose
                                    If Not NullToDbl0(oSubCampo.CampoGS) = idsficticios.numLinea Then
                                        If m_oCumplimentacionesGS.Item(CStr(oSubCampo.IdCampoDef)).Obligatorio Then
                                            For Indice = 1 To UBound(LineasDesgl)
                                                Encontrado = False
                                                LaLinea = LineasDesgl(Indice)
                                                For Each oLinea In oCampo.LineasDesglose 'lo recorres
                                                    If oSubCampo.IdCampoDef = oLinea.CampoHijo.IdCampoDef And oLinea.Linea = LaLinea Then
                                                        Encontrado = True
                                                
                                                        Select Case oLinea.CampoHijo.Tipo
                                                        Case TiposDeAtributos.TipoFecha
                                                            If IsNull(oLinea.valorFec) Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            ElseIf Not IsDate(oLinea.valorFec) Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            End If
                                                        Case TiposDeAtributos.TipoNumerico
                                                            If IsNull(oLinea.valorNum) Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            ElseIf Not IsNumeric(oLinea.valorNum) Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            End If
                                                        Case TiposDeAtributos.TipoArchivo
                                                            If IsNull(oLinea.valorText) Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            ElseIf oLinea.valorText = "" Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            End If
                                                        Case TiposDeAtributos.TipoBoolean
                                                            If IsNull(oLinea.valorBool) Then sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                        Case TiposDeAtributos.TipoEditor
                                                            If IsNull(oLinea.valorText) Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            ElseIf oLinea.valorText = "" Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            End If
                                                        Case TiposDeAtributos.TipoEnlace
                                                            'No deber�a ocurrir
                                                        Case Else 'Texto
                                                            If IsNull(oLinea.valorText) Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            ElseIf oLinea.valorText = "" Then
                                                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                                & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                                & oLinea.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & oLinea.Linea & ")"
                                                            End If
                                                        End Select
                                                        
                                                        Exit For
                                                    End If
                                                Next
                                            
                                                If Encontrado = False Then
                                                    sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " - " _
                                                    & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " _
                                                    & oSubCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & " (" & m_sLinea & " " & CStr(LaLinea) & ")"
                                                End If
                                            Next
                                        End If
                                    End If
                                Next
                            End If
                        
                    End If
                
                    If m_oCumplimentacionesGS.Item(CStr(oCampo.IdCampoDef)).Obligatorio Then
                        Select Case oCampo.Tipo
                        Case TiposDeAtributos.TipoFecha
                            If IsNull(oCampo.valorFec) Then
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            ElseIf Not IsDate(oCampo.valorFec) Then
                                'Nota. Los fecha al limpiar valor dejan oCampo.valorFec = "". No es fecha
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            End If
                        Case TiposDeAtributos.TipoNumerico
                            If IsNull(oCampo.valorNum) Then
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            ElseIf Not IsNumeric(oCampo.valorNum) Then
                                'Nota. Los numeros al limpiar valor dejan oCampo.valorNum = "". No es numero
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            End If
                        Case TiposDeAtributos.TipoArchivo
                            If IsNull(oCampo.valorText) Then
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            ElseIf oCampo.valorText = "" Then
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            End If
                        Case TiposDeAtributos.TipoBoolean
                            If IsNull(oCampo.valorBool) Then sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        Case TiposDeAtributos.TipoDesglose
                            If oCampo.LineasDesglose.Count = 0 Then
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            End If
                        Case TiposDeAtributos.TipoEditor
                            If IsNull(oCampo.valorText) Then
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            ElseIf oCampo.valorText = "" Then
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            End If
                        Case TiposDeAtributos.TipoEnlace
                            'No deber�a ocurrir
                        Case Else 'Texto
                            If IsNull(oCampo.valorText) Then
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            ElseIf oCampo.valorText = "" Then
                                'Nota. Los Pres1/Combo al limpiar valor dejan oCampo.valorText = ""
                                sListaOblig = sListaOblig & "#@@#" & "(" & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ") " & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            End If
                        End Select
                    End If
                Next
            End If
        Next
            
        If Not sListaOblig = "" Then
            Screen.MousePointer = vbNormal
            Call oMensajes.HasDeRellenar(sListaOblig)
            Exit Sub
        End If
    End If
        
    Set rsInfo = g_oSolicitudSeleccionada.obtenerInforInstancia(g_oSolicitudSeleccionada)
    Set rsCabecera = g_oSolicitudSeleccionada.obtenerCabeceraInstancia(g_oSolicitudSeleccionada)
    Set rsLinea = g_oSolicitudSeleccionada.obtenerLineaInstancia(g_oSolicitudSeleccionada)

    
    If oFSIS Is Nothing Then
       Set oFSIS = New CFSISService
    End If
    
    Dim iServiceBindingType As Integer
    Dim iServiceSecurityMode As Integer
    Dim iClientCredentialType As Integer
    Dim iProxyCredentialType As Integer
    Dim sUserName As String
    Dim sUserPassword As String
    Dim strRutaServicio As String
    Dim WCF As Boolean
    Dim sError As String
     
    WCF = g_oSolicitudSeleccionada.ObtenerDatosConexionWS(EntidadIntegracion.SolicitudPM, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sUserName, sUserPassword, strRutaServicio)
    
    sError = ""
    
    If Not WCF Then
        ''Se llama a la mapper para hacer las validaciones
        ''ESTO VA CON UN STORED
        
        Dim Maper As Object
        Dim sMapper As String
        Dim iNumError As Integer
        Dim TextosGS As cTextosGS
        Dim Modulo As Integer
        
        sMapper = g_oSolicitudSeleccionada.ObtenerNombreMaper(g_oSolicitudSeleccionada.Id)
    
        If sMapper <> "" Then
            On Error Resume Next
            Set Maper = CreateObject(sMapper & ".clsInPedidosDirectos")
            On Error GoTo 0
        
            If Not Maper Is Nothing Then
                On Error GoTo NoTratarSolicitud
                
                If Not Maper.TratarLineaSolicitudGS(basPublic.gInstancia, iNumError, sError, rsInfo, rsCabecera, rsLinea) Then
                
                    Screen.MousePointer = vbNormal
                    
                    If iNumError = -1000 Then
                        'bbdd mal
                        Exit Sub
                    End If
                    
                    Set TextosGS = oFSGSRaiz.Generar_CTextosGS
                            
                    Modulo = MapperModuloMensaje.PedidoDirecto
                    
                    Call oMensajes.MapperTextosGS(TextosGS.MensajeError(Modulo, iNumError, basPublic.gParametrosInstalacion.gIdioma, sError))
                    
                    Exit Sub
                End If
NoTratarSolicitud:
            End If
        End If
    Else
        ''Se hacen las validaciones contra FSIS
        sError = oFSIS.ValidacionFSIS("ValidacionesSolicitudGS", EntidadIntegracion.SolicitudPM, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sUserName, sUserPassword, strRutaServicio, , rsInfo, rsCabecera, rsLinea)
        
        If sError <> "" Then
            Call oMensajes.MapperTextosGS(sError)
            Exit Sub
        End If
    End If
    
    'Validaci�n de las imputaciones SM
    teserror = g_oSolicitudSeleccionada.ComprobarSolicitado(oColOK, oColKO, iDispSC, oColLineas, oColCompleta, True, iImpSC)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    'Si hay control de disponible
    If iImpSC > 0 Then
        If oColKO.Count > 0 And iDispSC > 0 Then
            'Llamada a la ventana de aviso pasando la colecci�n
            If Not FSGSForm.MostrarFormAvisoDisponible_SM(oGestorIdiomas, gParametrosInstalacion.gIdioma, oColKO, iDispSC) Then
                g_oSolicitudSeleccionada.CancelarSolicitado oColOK
                Set oColKO = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                'Primero cancelamos lo solicitado porque depues pasaremos todas las partidas, si no cancelamos, las que estuvieran bien antes del mensaje las sumaria 2 veces
                g_oSolicitudSeleccionada.CancelarSolicitado oColOK
                'Llamamos al Stored sin tener en cuenta el control de disponible ya que hemos decidido continuar despues del aviso
                teserror = g_oSolicitudSeleccionada.ComprobarSolicitado(oColOK, oColKO, iDispSC, oColLineas, oColCompleta, False, iImpSC)
            End If
        End If
        g_oSolicitudSeleccionada.ActualizarSolicitado oColCompleta, oColLineas
    End If
    Set oColKO = Nothing
    
    'Almacena en BD:
    teserror.NumError = TESnoerror
    teserror = g_oSolicitudSeleccionada.ModificarValores(oUsuarioSummit.Persona.Cod, basOptimizacion.gvarCodUsuario, g_oSolicitudSeleccionada.Estado)


    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If

    Screen.MousePointer = vbNormal

    'Registrar accion
    RegistrarAccion accionessummit.ACCSolicMod, "Id:" & g_oSolicitudSeleccionada.Id

    'Leer otra vez de BD para que traiga los �ltimos valores de los campos
    'Actualizar la combo de versiones
    sdbcVersion.Text = CDate(Date & " " & Time) & " " & oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
    sdbcVersion.Columns("NUM").Value = g_oSolicitudSeleccionada.NumVersion
    
    For Each oGrupo In g_oSolicitudSeleccionada.Grupos 'Lee de BD
        Set oGrupo.CAMPOS = Nothing
        
        Set m_oGruposBD.Item(CStr(oGrupo.Id)).CAMPOS = Nothing
    Next
    GrupoSeleccionado
        
    CerrarFormulariosDesglose
    
    picControl.Visible = True
    picEdicion.Visible = False
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdGuardar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdImprimir_Click()
    ''' * Objetivo: Obtener un listado de detalle de solicitud
    '''   Muestra el formulario para imprimir el listado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmLstSolicitud.g_sOrigen = "frmSolicitudDetalle"
    frmLstSolicitud.g_sOrigen2 = g_sOrigen
    frmLstSolicitud.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdImprimir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdInfProcesosPed_Click()
    'Muestra la pantalla de procesos,pedidos, cat�logo...
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set frmSolicitudInformacion.g_oSolicitudSeleccionada = g_oSolicitudSeleccionada
    frmSolicitudInformacion.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdInfProcesosPed_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Crear pedido directo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPedidoDir_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirWizardCrearPedido True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdPedidoDir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Abre el wizard de creaci�n de pedidos</summary>
''' <remarks>Llamada desde: cmdPedidoDir_Click</remarks>
Private Sub AbrirWizardCrearPedido(ByVal bNuevoPedido As Boolean)
    Dim irespuesta As Integer
    Dim bBloqueoEtapa As Boolean
    Dim bBloqueoCondiciones As Boolean
    Dim Ador As Ador.Recordset
    Dim iResp As Integer
    
    'Si el estado de la Instancia es distinta que aprobada, miramos si su solicitud permite los pedidos directos:
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Screen.MousePointer = vbHourglass
   
    If g_oSolicitudSeleccionada.Estado < EstadoSolicitud.Pendiente Then
        g_oSolicitudSeleccionada.Solicitud.CargarConfiguracion , , , , True
        If Not g_oSolicitudSeleccionada.Solicitud.PermitirPedidoDirecto Then
            Screen.MousePointer = vbNormal
            oMensajes.NoPermitidoPedido
            Exit Sub
        End If
    End If
        
    If bNuevoPedido Then
        'Si el usuario no tiene permisos para emitir pedidos directos le sale un mensaje indic�ndoselo:
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIREmitir)) Is Nothing Then
            Screen.MousePointer = vbNormal
            irespuesta = oMensajes.PreguntaGenerarPedDirecto
            If irespuesta = vbNo Then Exit Sub
            Screen.MousePointer = vbHourglass
        End If
    End If
    
    If gParametrosGenerales.gbSolicitudesCompras Then
        bBloqueoEtapa = False
        bBloqueoCondiciones = False
        
        'Se comprueban las condiciones de bloqueo por etapa
        bBloqueoEtapa = BloqueoEtapa(g_oSolicitudSeleccionada.Id)
        If bBloqueoEtapa Then
            oMensajes.MensajeOKOnly 904, Exclamation  'La solicitud se encuentra en una etapa en la que no se permite la emisi�n de pedidos directos
        Else
            'Se comprueban las condiciones de bloqueo por f�rmula
            bBloqueoCondiciones = BloqueoCondiciones(g_oSolicitudSeleccionada.Id, TipoBloqueo.PedidosDirectos, m_sIdiErrorEvaluacion(1), m_sIdiErrorEvaluacion(2))
        End If

        If bBloqueoEtapa Or bBloqueoCondiciones Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        Screen.MousePointer = vbHourglass
    End If
    
    If m_bPermProcMultimaterial Then
        Set Ador = g_oSolicitudSeleccionada.DevolverMaterialesInstancia_Campo
        If Ador.RecordCount > 1 Then
            Screen.MousePointer = vbNormal
            iResp = oMensajes.MensajeYesNo(948)
            If iResp = vbNo Then
                Exit Sub
            End If
            Screen.MousePointer = vbHourglass
        Else
            Set Ador = g_oSolicitudSeleccionada.DevolverMaterialesInstancia_Desglose
            If Ador.RecordCount > 1 Then
                Screen.MousePointer = vbNormal
                iResp = oMensajes.MensajeYesNo(948)
                If iResp = vbNo Then
                    Exit Sub
                End If
                Screen.MousePointer = vbHourglass
            End If
        End If
        
    End If
    
       
    Dim arSolicitudes(0) As CInstancia
    Set arSolicitudes(0) = g_oSolicitudSeleccionada
        
    frmSOLAbrirProc.g_arSolicitudes = arSolicitudes
    frmSOLAbrirProc.g_bGenerarPedido = bNuevoPedido
    frmSOLAbrirProc.g_bPedidoExistente = Not bNuevoPedido
    If bNuevoPedido Then
        frmSOLAbrirProc.m_iGenerar = 3
    Else
        frmSOLAbrirProc.m_iGenerar = 4
    End If
    Screen.MousePointer = vbNormal
    frmSOLAbrirProc.Show vbModal
 
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "AbrirWizardCrearPedido", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub cmdReabrir_Click()
    'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudDetalle"
    frmSolicitudGestionar.g_sOrigen2 = g_sOrigen
    frmSolicitudGestionar.g_iAccion = 3
    Set frmSolicitudGestionar.g_oSolicitud = g_oSolicitudSeleccionada
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdReabrir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdRechazar_Click()
    'Llama al formulario para anular la solicitud
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    frmSolicitudGestionar.g_sOrigen = "frmSolicitudDetalle"
    frmSolicitudGestionar.g_sOrigen2 = g_sOrigen
    frmSolicitudGestionar.g_iAccion = 4
    Set frmSolicitudGestionar.g_oSolicitud = g_oSolicitudSeleccionada
    frmSolicitudGestionar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "cmdRechazar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Screen.MousePointer = vbHourglass
        
    Set m_oTablaExterna = oFSGSRaiz.Generar_CTablaExterna
    'fin Tarea 811
    
    Me.Width = 13000
    Me.Height = 7845
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    'Configura la seguridad
    ConfigurarSeguridad
    
    Me.caption = Me.caption & " " & NullToStr(g_oSolicitudSeleccionada.DescrBreve)
    
    'Carga los datos de la solicitud
    CargarDatosSolicitud
    
    If g_sOrigen = "frmSolicitudes" Then
        Set m_oCumplimentacionesGS = oFSGSRaiz.Generar_CSolCumplimentacionesGs
        m_oCumplimentacionesGS.CargarCumplimentacion g_oSolicitudSeleccionada.Solicitud.Id, True
    End If
    
    'Habilita los botones seg�n el estado de la solicitud seleccionada
    HabilitarBotones
    
    sdbddDestinos.AddItem ""
    sdbddPagos.AddItem ""
    sdbddValor.AddItem ""
    sdbddOtrosCombos.AddItem ""
    sdbddAlmacenes.AddItem ""
    sdbddArticulos.AddItem ""
    sdbddEmpresas.AddItem ""
    
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.generar_CPagos
    
    Set m_oDestinos = Nothing
    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    
    Set m_oUnidades = Nothing
    Set m_oUnidades = oFSGSRaiz.Generar_CUnidades
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLIC_DETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value        '1 Detalle de solicitud
        Ador.MoveNext
        lblLitId.caption = Ador(0).Value  '2 Identificador
        Ador.MoveNext
        lblLitEstado.caption = Ador(0).Value  '3 Estado:
        Ador.MoveNext
        lblLitPeticionario.caption = Ador(0).Value  '4 Peticionario:
        Ador.MoveNext
        lblLitAprobador.caption = Ador(0).Value  '5  Aprobador:
        Ador.MoveNext
        lblLitFecAlta.caption = Ador(0).Value  '6  Fecha alta:
        Ador.MoveNext
        lblLitFecAprob.caption = Ador(0).Value  '7  Fecha aprobaci�n:
        Ador.MoveNext
        lblLitComprador.caption = Ador(0).Value & ":"  '8  Comprador:
        Ador.MoveNext
        lblVersion.caption = Ador(0).Value & ":" '9 Versi�n
        Ador.MoveNext
        lblInf.caption = Ador(0).Value '10 Informaci�n de Procesos/Pedidos
        Ador.MoveNext
        ssTabGrupos.Tabs(1).caption = Ador(0).Value '11 Datos generales
        m_sDatosGen = Ador(0).Value
        Ador.MoveNext
        sdbgCampos.Columns("CAMPO").caption = Ador(0).Value '12 Dato
        Ador.MoveNext
        sdbgCampos.Columns("VALOR").caption = Ador(0).Value '13 Valor
        Ador.MoveNext
        sdbgCampos.Columns("AYUDA").caption = Ador(0).Value '14 ayuda
        Ador.MoveNext
        
        cmdEnviarProc.caption = Ador(0).Value  '15 &Enviar a proceso existente
        Ador.MoveNext
        cmdAprobar.caption = Ador(0).Value  '16 &Aprobar
        Ador.MoveNext
        cmdRechazar.caption = Ador(0).Value  '17 &Rechazar
        Ador.MoveNext
        cmdAnular.caption = Ador(0).Value  '18  Anu&lar
        Ador.MoveNext
        cmdAbrirProc.caption = Ador(0).Value  '19  Abrir &proceso
        Ador.MoveNext
        cmdImprimir.caption = Ador(0).Value  '20 &Imprimir
    
        Ador.MoveNext
        m_sAnulada = Ador(0).Value  '  21  Anulada
        Ador.MoveNext
        m_sAprobada = Ador(0).Value '22  Aprobada
        Ador.MoveNext
        m_sCerrada = Ador(0).Value  '23  Cerrada
        Ador.MoveNext
        m_sPendiente = Ador(0).Value '24  Pendiente
        Ador.MoveNext
        m_sRechazada = Ador(0).Value  '25  Rechazada
        
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value '26 Eliminar
        Ador.MoveNext
        cmdReabrir.caption = Ador(0).Value '27 Reabrir
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value  '28 Cerrar
        Ador.MoveNext
        cmdAsignar.caption = Ador(0).Value '29 Asignar comprador
        Ador.MoveNext
        cmdGuardar.caption = Ador(0).Value   '30 &Guardar cambios
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value  '31 &Deshacer cambios
        Ador.MoveNext
        cmdPedidoDir.caption = Ador(0).Value  '32 Pedido direc&to
        
        Ador.MoveNext
        sdbcVersion.Columns("FECHA").caption = Ador(0).Value  '33 Fecha
        Ador.MoveNext
        sdbcVersion.Columns("USU").caption = Ador(0).Value   '34 Usuario
        Ador.MoveNext
        m_sEnCurso = Ador(0).Value  '35 En curso de aprobaci�n
        
        Ador.MoveNext
        m_sIdiDesglose = Ador(0).Value  '36  (Doble click o pulse sobre el bot�n para ver el desglose)
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value  '37  No
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value  '38  S�

        For i = 1 To 17
            Ador.MoveNext
            m_sIdiTipoFecha(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sIdiKB = Ador(0).Value  '56 KB
        Ador.MoveNext
        sdbddPagos.Columns("COD").caption = Ador(0).Value  '57 C�digo
        sdbddDestinos.Columns("COD").caption = Ador(0).Value
        sdbddOtrosCombos.Columns("COD").caption = Ador(0).Value
        sdbddAlmacenes.Columns("COD").caption = Ador(0).Value
        sdbddEmpresas.Columns("COD").caption = Ador(0).Value
        sdbddArticulos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbddPagos.Columns("DEN").caption = Ador(0).Value   '58 Denominaci�n
        sdbddDestinos.Columns("DEN").caption = Ador(0).Value
        sdbddOtrosCombos.Columns("DEN").caption = Ador(0).Value
        sdbddAlmacenes.Columns("DEN").caption = Ador(0).Value
        sdbddArticulos.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbddDestinos.Columns("DIR").caption = Ador(0).Value  ' 59 Direcci�n
        Ador.MoveNext
        sdbddDestinos.Columns("POB").caption = Ador(0).Value  ' 60 Poblaci�n
        Ador.MoveNext
        sdbddDestinos.Columns("CP").caption = Ador(0).Value  ' 61 C�digo postal
        Ador.MoveNext
        sdbddDestinos.Columns("PAI").caption = Ador(0).Value  ' 62 Pais
        Ador.MoveNext
        sdbddDestinos.Columns("PROVI").caption = Ador(0).Value  ' 63 Provincia
        
        For i = 1 To 7
            Ador.MoveNext
            m_sMensaje(i) = Ador(0).Value
        Next i

        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '71 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '72 Error al realizar el c�lculo:Valores incorrectos.
        For i = 1 To 11
            Ador.MoveNext
        Next
        cmdEnviarPed.caption = Ador(0).Value 'Enviar a pedido
        Ador.MoveNext
        m_sGenRechazada = Ador(0).Value
        Ador.MoveNext
        sdbddEmpresas.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        m_sLinea = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Resize()
Dim dblWGrid As Double

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 500 Then Exit Sub
    If Me.Width < 2500 Then Exit Sub
    
    ssTabGrupos.Height = Me.Height - 2150
    ssTabGrupos.Width = Me.Width - 230
    
    sdbgCampos.Height = ssTabGrupos.Height - 580
    sdbgCampos.Width = ssTabGrupos.Width - 270
    
    sdbgCampos.Columns("AYUDA").Width = sdbgCampos.Width / 11
    sdbgCampos.Columns("VALOR").Width = sdbgCampos.Width / 2.35
    
    dblWGrid = sdbgCampos.Width - sdbgCampos.Columns("VALOR").Width - sdbgCampos.Columns("AYUDA").Width - 600
    
    sdbgCampos.Columns("CAMPO").Width = dblWGrid
    
    sdbddValor.Columns(0).Width = sdbgCampos.Columns("VALOR").Width
    sdbddFecha.Columns("DEN").Width = sdbgCampos.Columns("VALOR").Width
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If Not g_ofrmDesglose Is Nothing Then
        Unload g_ofrmDesglose
        Set g_ofrmDesglose = Nothing
    End If
    
    CerrarFormulariosDesglose
    Set g_oGrupoSeleccionado = Nothing
                        
    'jpa Tatrea 811
    Set m_oTablaExterna = Nothing
    'fin jpa Tatrea 811
                        
    Set m_oGruposBD = Nothing
    Set g_oGrupoSeleccionado = Nothing
    Set g_oSolicitudSeleccionada = Nothing
    Set m_oPagos = Nothing
    Set m_oDestinos = Nothing
    Set m_oIBaseDatos = Nothing
    Set m_oPaises = Nothing
    Set m_oUnidades = Nothing
    
    Set m_oCumplimentacionesGS = Nothing
    
    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarDatosSolicitud()
    Dim iTab As Integer
    Dim oGrupo As CFormGrupo
    Dim ADORs As Ador.Recordset
    Dim sCadena As String

    'Carga los datos de la solicitud seleccionada
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblIdentificador.caption = g_oSolicitudSeleccionada.Id
    lblFecAlta.caption = Format(g_oSolicitudSeleccionada.FecAlta, "General date")
    lblPeticionario.caption = g_oSolicitudSeleccionada.Peticionario.Cod & "-" & NullToStr(g_oSolicitudSeleccionada.Peticionario.nombre) & " " & NullToStr(g_oSolicitudSeleccionada.Peticionario.Apellidos)
    If Not g_oSolicitudSeleccionada.comprador Is Nothing Then
        lblComprador.caption = g_oSolicitudSeleccionada.comprador.Cod & "-" & NullToStr(g_oSolicitudSeleccionada.comprador.nombre) & " " & NullToStr(g_oSolicitudSeleccionada.comprador.Apel)
    Else
        lblComprador.caption = ""
    End If
        
    If m_bAbrirProc Then cmdAbrirProc.Enabled = True
    If m_bEnviarProc Then cmdEnviarProc.Enabled = True
    If m_bPedidoDirecto Then cmdPedidoDir.Enabled = True
    
    cmdEnviarPed.Enabled = m_bEnviarPedido
            
    Select Case g_oSolicitudSeleccionada.Estado
        Case EstadoSolicitud.Anulada
            lblEstado.caption = m_sAnulada
            cmdAbrirProc.Enabled = False
            cmdEnviarProc.Enabled = False
            cmdPedidoDir.Enabled = False
            cmdEnviarPed.Enabled = False
        Case EstadoSolicitud.Aprobada
            lblEstado.caption = m_sAprobada
        Case EstadoSolicitud.Cerrada
            lblEstado.caption = m_sCerrada
            cmdAbrirProc.Enabled = False
            cmdEnviarProc.Enabled = False
            cmdPedidoDir.Enabled = False
            cmdEnviarPed.Enabled = False
        Case EstadoSolicitud.Pendiente
            lblEstado.caption = m_sPendiente
        Case EstadoSolicitud.Rechazada
            lblEstado.caption = m_sRechazada
            cmdAbrirProc.Enabled = False
            cmdEnviarProc.Enabled = False
            cmdPedidoDir.Enabled = False
            cmdEnviarPed.Enabled = False
        Case EstadoSolicitud.GenRechazada
            lblEstado.caption = m_sGenRechazada
            cmdAbrirProc.Enabled = False
            cmdEnviarProc.Enabled = False
            cmdPedidoDir.Enabled = False
            cmdEnviarPed.Enabled = False
        Case Else  'En curso de aprobaci�n
            lblEstado.caption = m_sEnCurso
            If Not m_bAbrirProcEnCurso Then
                cmdAbrirProc.Enabled = False
                cmdEnviarProc.Enabled = False
                cmdPedidoDir.Enabled = False
                cmdEnviarPed.Enabled = False
            End If
    End Select
    
    If g_oSolicitudSeleccionada.Estado >= EstadoSolicitud.Aprobada Then
        g_oSolicitudSeleccionada.ObtenerAprobador
        lblFecAprobacion.caption = Format(g_oSolicitudSeleccionada.FecAprob, "General date")
        If Not g_oSolicitudSeleccionada.Aprobador Is Nothing Then
            lblAprobador.caption = g_oSolicitudSeleccionada.Aprobador.Cod & "-" & NullToStr(g_oSolicitudSeleccionada.Aprobador.nombre) & " " & NullToStr(g_oSolicitudSeleccionada.Aprobador.Apellidos)
        End If
    Else
        lblFecAprobacion.caption = ""
        lblAprobador.caption = ""
    End If
    
    'si no hay aprobador el bot�n de detalle se inhabilita
    If lblAprobador.caption = "" Then
        cmdDetalleProce(0).Enabled = False
    Else
        cmdDetalleProce(0).Enabled = True
    End If
    
    'Versi�n de la instancia:
    Set ADORs = g_oSolicitudSeleccionada.CargarVersiones(True)
    If Not ADORs.EOF Then
        sCadena = ADORs("NUM_VERSION").Value & Chr(m_lSeparador) & Format(ADORs("FECHA_MODIF").Value, "General Date")
        If Not IsNull(ADORs("PER_MODIF").Value) Then
            sCadena = sCadena & Chr(m_lSeparador) & NullToStr(ADORs("NOM").Value) & " " & NullToStr(ADORs("APE").Value)
        ElseIf Not IsNull(ADORs("PROVE").Value) Then 'Proveedor
            sCadena = sCadena & Chr(m_lSeparador) & NullToStr(ADORs("COD_PROVE").Value) & " " & NullToStr(ADORs("DEN_PROVE").Value)
        Else
            sCadena = sCadena & Chr(m_lSeparador) & ""
        End If
        sdbcVersion.AddItem sCadena
        sdbcVersion.Text = sdbcVersion.Columns("FECHA").Value & " " & sdbcVersion.Columns("USU").Value
        g_oSolicitudSeleccionada.NumVersion = ADORs("NUM_VERSION").Value
    End If
    ADORs.Close
    Set ADORs = Nothing

    If m_bModificar Then m_bModifUltimaVersion = True
    
    'Carga el tab con los grupos
    g_oSolicitudSeleccionada.CargarGrupos
        
    Set m_oGruposBD = oFSGSRaiz.Generar_CFormGrupos
    For Each oGrupo In g_oSolicitudSeleccionada.Grupos
        m_oGruposBD.Add oGrupo.Id, oGrupo.Denominaciones, oGrupo.FECACT, , oGrupo.Instancia
    Next
    
    'A�ade un tab para cada grupo:
    ssTabGrupos.Tabs.clear
    iTab = 1
    For Each oGrupo In g_oSolicitudSeleccionada.Grupos
        ssTabGrupos.Tabs.Add iTab, "A" & oGrupo.Id, oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den      'si no es multiidioma ser� la misma para todos
        ssTabGrupos.Tabs(iTab).Tag = CStr(oGrupo.Id)
        iTab = iTab + 1
    Next
    If ssTabGrupos.Tabs.Count > 0 Then ssTabGrupos.Tabs(1).Selected = True
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "CargarDatosSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Establece a que tiene acceso a q no.
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub ConfigurarSeguridad()
    'Modificar datos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICModificarDatos)) Is Nothing) Then
        m_bModificar = True
    End If
    
    'Aprobar/rechazar/cierre
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAprobRechazoCierre)) Is Nothing) Then
        m_bAprobRechazoCierre = True
    End If
    
    'Asignar comprador
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAsignar)) Is Nothing) Then
        m_bAsignar = True
    End If

    'Reabrir
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICReabrir)) Is Nothing) Then
        m_bReabrir = True
    End If
    
    'Anular
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAnular)) Is Nothing) Then
        m_bAnular = True
    End If
    
    'Eliminar
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICEliminar)) Is Nothing) Then
        m_bEliminar = True
    End If
    
    'Restricci�n de equipo
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestEquipo)) Is Nothing) Then
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
            'S�lo le afectar� la restricci�n si el usuario es un comprador.
            m_bREquipo = True
        End If
    End If
    
    'Restricci�n de asignaci�n
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestAsig)) Is Nothing) Then
        m_bRAsig = True
    End If
    
    'Restricci�n de unidad organizativa
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestUO)) Is Nothing) Then
        m_bRuo = True
    End If
    
    'Restricci�n de Departamento
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrDpto)) Is Nothing) Then
        m_bRDep = True
    End If
    
    'Abrir proceso
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAbrirProc)) Is Nothing) Then
        m_bAbrirProc = True
    End If
    
    'Enviar a proceso
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICEnviarProc)) Is Nothing) Then
        m_bEnviarProc = True
    End If
    
    'Pedido directo
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICPedidoDirecto)) Is Nothing) Then
        m_bPedidoDirecto = True
    End If
    
    'Enviar a pedido existente
    m_bEnviarPedido = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICEnviarPedExistente)) Is Nothing)
    
    'Abrir proceso proceso desde solicitudes en curso
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAbrirProcEnCurso)) Is Nothing) Then
        m_bAbrirProcEnCurso = True
    End If
    
    'Restricciones de destino,material y proveedor:
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrDest)) Is Nothing) Then
        m_bRestrDest = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing) Then
        m_bRestrMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrProve)) Is Nothing) Then
        m_bRestrProve = True
    End If
    
    'Abrir procesos multimaterial
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
        If gParametrosGenerales.gbMultiMaterial Then
            m_bPermProcMultimaterial = True
        Else
            m_bPermProcMultimaterial = False
        End If
    Else
        m_bPermProcMultimaterial = False
    End If
    
    'Si no es el administrador comprueba los datos del usuario
    Dim oUsuario As CUsuario
    Set oUsuario = oFSGSRaiz.generar_cusuario
    oUsuario.Cod = basOptimizacion.gvarCodUsuario
    oUsuario.ExpandirUsuario
    If oUsuario.FSWSPresUO = True Then
        m_bRestrPresUO = True
    End If
    Set oUsuario = Nothing
   
    'Si la solicitud est� cerrada no se podr� modificar
    If m_bModificar Then
        If g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Cerrada Or g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Anulada Or g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Rechazada Then
            m_bModificar = False
        ElseIf g_bSoloLectura Or g_oSolicitudSeleccionada.Estado = EstadoSolicitud.GenEnviada Or g_oSolicitudSeleccionada.Estado = EstadoSolicitud.GenAprobada Or _
            g_oSolicitudSeleccionada.Estado = EstadoSolicitud.GenPendiente Or g_oSolicitudSeleccionada.Estado = EstadoSolicitud.GenGuardada Then
            m_bModificar = False   'si la solicitud est� en curso de aprobaci�n del WS que no se pueda modificar
        End If
    End If
    
    If m_bModificar = False Then
        sdbgCampos.Columns("VALOR").Locked = True
    End If
    
    On Error Resume Next
    'Aprobar/rechazar/cierre
    If Not m_bAprobRechazoCierre Then
        cmdAprobar.Visible = False
        cmdRechazar.Visible = False
        cmdCerrar.Visible = False
        cmdAsignar.Left = cmdAprobar.Left
        cmdReabrir.Left = cmdAsignar.Left + cmdAsignar.Width + 70
        cmdAnular.Left = cmdReabrir.Left + cmdReabrir.Width + 70
        cmdEliminar.Left = cmdAnular.Left + cmdAnular.Width + 70
        cmdImprimir.Left = cmdEliminar.Left + cmdEliminar.Width + 70
        cmdAbrirProc.Left = cmdImprimir.Left + cmdImprimir.Width + 70
        cmdEnviarProc.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 70
        cmdPedidoDir.Left = cmdEnviarProc.Left + cmdEnviarProc.Width + 70
        cmdEnviarPed.Left = cmdPedidoDir.Left + cmdPedidoDir.Width + 70
    End If
    
    'Asignar comprador
    If Not m_bAsignar Then
        cmdAsignar.Visible = False
        If m_bAprobRechazoCierre = True Then
            cmdCerrar.Left = cmdAsignar.Left + cmdAsignar.Width + 70
            cmdReabrir.Left = cmdCerrar.Left + cmdCerrar.Width + 70
        Else
            cmdReabrir.Left = cmdAsignar.Left + cmdAsignar.Width + 70
        End If
        cmdAnular.Left = cmdReabrir.Left + cmdReabrir.Width + 70
        cmdEliminar.Left = cmdAnular.Left + cmdAnular.Width + 70
        cmdImprimir.Left = cmdEliminar.Left + cmdEliminar.Width + 70
        cmdAbrirProc.Left = cmdImprimir.Left + cmdImprimir.Width + 70
        cmdEnviarProc.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 70
        cmdPedidoDir.Left = cmdEnviarProc.Left + cmdEnviarProc.Width + 70
        cmdEnviarPed.Left = cmdPedidoDir.Left + cmdPedidoDir.Width + 70
    End If
    
    'Reabrir
    If Not m_bReabrir Then
        cmdReabrir.Visible = False
        cmdAnular.Left = cmdReabrir.Left
        cmdEliminar.Left = cmdAnular.Left + cmdAnular.Width + 70
        cmdImprimir.Left = cmdEliminar.Left + cmdEliminar.Width + 70
        cmdAbrirProc.Left = cmdImprimir.Left + cmdImprimir.Width + 70
        cmdEnviarProc.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 70
        cmdPedidoDir.Left = cmdEnviarProc.Left + cmdEnviarProc.Width + 70
        cmdEnviarPed.Left = cmdPedidoDir.Left + cmdPedidoDir.Width + 70
    End If
    
    'Anular
    If Not m_bAnular Then
        cmdAnular.Visible = False
        cmdEliminar.Left = cmdAnular.Left
        cmdImprimir.Left = cmdEliminar.Left + cmdEliminar.Width + 70
        cmdAbrirProc.Left = cmdImprimir.Left + cmdImprimir.Width + 70
        cmdEnviarProc.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 70
        cmdPedidoDir.Left = cmdEnviarProc.Left + cmdEnviarProc.Width + 70
        cmdEnviarPed.Left = cmdPedidoDir.Left + cmdPedidoDir.Width + 70
    End If
    
    'Eliminar
    If Not m_bEliminar Then
        cmdEliminar.Visible = False
        cmdImprimir.Left = cmdEliminar.Left
        cmdAbrirProc.Left = cmdImprimir.Left + cmdImprimir.Width + 70
        cmdEnviarProc.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 70
        cmdPedidoDir.Left = cmdEnviarProc.Left + cmdEnviarProc.Width + 70
        cmdEnviarPed.Left = cmdPedidoDir.Left + cmdPedidoDir.Width + 70
    End If
    
    'abrir proceso
    If Not m_bAbrirProc Then
        cmdAbrirProc.Visible = False
        cmdEnviarProc.Left = cmdAbrirProc.Left
        cmdPedidoDir.Left = cmdEnviarProc.Left + cmdEnviarProc.Width + 70
        cmdEnviarPed.Left = cmdPedidoDir.Left + cmdPedidoDir.Width + 70
    End If
        
    'enviar a proceso
    If Not m_bEnviarProc Then
        cmdEnviarProc.Visible = False
        cmdPedidoDir.Left = cmdEnviarProc.Left
        cmdEnviarPed.Left = cmdPedidoDir.Left + cmdPedidoDir.Width + 70
    End If
    
    'Pedido directo
    If Not m_bPedidoDirecto Then
        cmdPedidoDir.Visible = False
        cmdEnviarPed.Left = cmdPedidoDir.Left
    End If
    
    cmdEnviarPed.Visible = m_bEnviarPedido
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub



Private Sub HabilitarBotones()
    'Habilita los men�s seg�n el estado de la solicitud seleccionada
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_oSolicitudSeleccionada.Estado
        Case EstadoSolicitud.Anulada
            cmdAnular.Enabled = False
            cmdAprobar.Enabled = False
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdAsignar.Enabled = False
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True
            
        Case EstadoSolicitud.Aprobada
            cmdAnular.Enabled = True
            cmdAprobar.Enabled = False
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = True
            cmdAsignar.Enabled = True
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True
            
        Case EstadoSolicitud.Cerrada
            cmdAnular.Enabled = False
            cmdAprobar.Enabled = False
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdAsignar.Enabled = False
            cmdReabrir.Enabled = True
            cmdEliminar.Enabled = True
            
        Case EstadoSolicitud.Pendiente
            cmdAnular.Enabled = True
            cmdAprobar.Enabled = True
            cmdRechazar.Enabled = True
            cmdCerrar.Enabled = False
            cmdAsignar.Enabled = True
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True
            
        Case EstadoSolicitud.Rechazada
            cmdAnular.Enabled = False
            cmdAprobar.Enabled = False
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdAsignar.Enabled = False
            cmdReabrir.Enabled = False
            cmdEliminar.Enabled = True
            
        Case Else  'en curso de aprobaci�n
            cmdAprobar.Enabled = False
            cmdRechazar.Enabled = False
            cmdCerrar.Enabled = False
            cmdAsignar.Enabled = True
            cmdReabrir.Enabled = False
            If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Administrador Then
                cmdEliminar.Enabled = True
                cmdAnular.Enabled = True
            Else
                cmdEliminar.Enabled = False
                cmdAnular.Enabled = False
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "HabilitarBotones", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Sub ActualizarEstadoSolicitud(ByVal iEstado As Integer, Optional ByVal oAprob As Variant, Optional ByVal vFecha As Variant)
    'Actualiza los datos del formulario  y la colecci�n con el nuevo estado para la solicitud seleccionada
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case iEstado
        Case EstadoSolicitud.Anulada
            g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Anulada
            lblEstado.caption = m_sAnulada
            
        Case EstadoSolicitud.Aprobada
            g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Aprobada
            lblEstado.caption = m_sAprobada
            
            If IsMissing(vFecha) Then
                lblFecAprobacion.caption = ""
                g_oSolicitudSeleccionada.FecAprob = Null
            Else
                lblFecAprobacion.caption = Format(vFecha, "Short date")
                g_oSolicitudSeleccionada.FecAprob = CDate(vFecha)
            End If
            
            If IsMissing(oAprob) Or IsNull(oAprob) Then
                lblAprobador.caption = ""
                Set g_oSolicitudSeleccionada.Aprobador = Nothing
            Else
                lblAprobador.caption = oAprob.Cod & "-" & NullToStr(oAprob.nombre) & " " & NullToStr(oAprob.Apellidos)
                Set g_oSolicitudSeleccionada.Aprobador = Nothing
                Set g_oSolicitudSeleccionada.Aprobador = oAprob
                cmdDetalleProce(0).Enabled = True
            End If
            
        Case EstadoSolicitud.Rechazada
            g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Rechazada
            lblEstado.caption = m_sRechazada
            
        Case EstadoSolicitud.Cerrada
            g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Cerrada
            lblEstado.caption = m_sCerrada
    End Select
    
    
    HabilitarBotones
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ActualizarEstadoSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Public Sub ActualizarCompradorEnGrid(ByVal oComp As CComprador)
    'Actualiza el form. y la colecci�n con el nuevo comprador que se le ha asignado
    'a la solicitud seleccionada
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oSolicitudSeleccionada.comprador = oComp
    
    lblComprador.caption = oComp.Cod & "-" & NullToStr(oComp.nombre) & " " & NullToStr(oComp.Apel)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ActualizarCompradorEnGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcVersion_CloseUp()
Dim oGrupo As CFormGrupo
Dim vbm As Variant
Dim i As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcVersion.Value = "" Then Exit Sub
    
    sdbcVersion.Text = sdbcVersion.Columns("FECHA").Value & " " & sdbcVersion.Columns("USU").Value
    
    g_oSolicitudSeleccionada.NumVersion = sdbcVersion.Columns("NUM").Value
    
    'Si no es la �ltima versi�n no se podr�n modificar sus datos:
    If m_bModificar = True Then
        m_bModifUltimaVersion = True
        sdbgCampos.Columns("VALOR").Locked = False
        
        For i = 0 To sdbcVersion.Rows - 1
            vbm = sdbcVersion.AddItemBookmark(i)
            If CLng(sdbcVersion.Columns("NUM").CellValue(vbm)) > CLng(g_oSolicitudSeleccionada.NumVersion) Then
                m_bModifUltimaVersion = False
                sdbgCampos.Columns("VALOR").Locked = True
                Exit For
            End If
        Next i
    End If
    
    For Each oGrupo In g_oSolicitudSeleccionada.Grupos
        Set oGrupo.CAMPOS = Nothing
    Next
    GrupoSeleccionado
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbcVersion_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcVersion_DropDown()
    Dim ADORs As Ador.Recordset
    Dim sCadena As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcVersion.RemoveAll
    
    Set ADORs = g_oSolicitudSeleccionada.CargarVersiones
    
    If ADORs Is Nothing Then
        sdbcVersion.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcVersion.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sCadena = ADORs("NUM_VERSION").Value & Chr(m_lSeparador) & Format(ADORs("FECHA_MODIF").Value, "General Date")
        
        If Not IsNull(ADORs("PER_MODIF").Value) Then
            sCadena = sCadena & Chr(m_lSeparador) & NullToStr(ADORs("NOM").Value) & " " & NullToStr(ADORs("APE").Value)
        ElseIf Not IsNull(ADORs("PROVE").Value) Then 'Proveedor
            sCadena = sCadena & Chr(m_lSeparador) & NullToStr(ADORs("COD_PROVE").Value) & " " & NullToStr(ADORs("DEN_PROVE").Value)
        Else
            sCadena = sCadena & Chr(m_lSeparador) & ""
        End If
        sdbcVersion.AddItem sCadena
        ADORs.MoveNext
    Wend
    
    If sdbcVersion.Rows = 0 Then
        sdbcVersion.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbcVersion_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbddDestinos_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgCampos.Columns("COD_VALOR").Value = sdbddDestinos.Columns("COD").Value
    sdbgCampos.Columns("VALOR").Value = sdbddDestinos.Columns("COD").Value & " - " & sdbddDestinos.Columns("DEN").Value
    sdbgCampos.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddDestinos_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddDestinos_DropDown()
Dim ADORs As Ador.Recordset
    
    ''' * Objetivo: Abrir el combo de destinos de la forma adecuada
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bModificar Or Not m_bModifUltimaVersion Then
        sdbddDestinos.DroppedDown = False
        Exit Sub
    End If
    
    sdbddDestinos.RemoveAll
    
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest)
    End If
    
    If ADORs Is Nothing Then
        sdbddDestinos.RemoveAll
        Screen.MousePointer = vbNormal
        sdbddDestinos.AddItem ""
        sdbgCampos.ActiveCell.SelStart = 0
        sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbddDestinos.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbddDestinos.Rows = 0 Then
        sdbddDestinos.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddDestinos_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddDestinos_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddDestinos.DataFieldList = "Column 0"
    sdbddDestinos.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddDestinos_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddDestinos_PositionList(ByVal Text As String)
PositionList sdbddDestinos, Text
End Sub

Private Sub sdbddFecha_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgCampos.Columns("COD_VALOR").Value = sdbddFecha.Columns("ID").Value
    sdbgCampos.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddFecha_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddFecha_InitColumnProps()
''' * Objetivo: Definir que columna es la de busqueda y seleccion,
''' * Objetivo: y cual llevaremos a la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddFecha.DataFieldList = "Column 1"
    sdbddFecha.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddFecha_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddFecha_PositionList(ByVal Text As String)
PositionList sdbddFecha, Text, 1
End Sub

Private Sub sdbddPagos_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgCampos.Columns("COD_VALOR").Value = sdbddPagos.Columns("COD").Value
    sdbgCampos.Columns("VALOR").Value = sdbddPagos.Columns("COD").Value & " - " & sdbddPagos.Columns("DEN").Value
    sdbgCampos.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddPagos_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddPagos_DropDown()
    Dim oPag As CPago

    ''' * Objetivo: Cargar combo con la coleccion de formas de pago
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bModificar Or Not m_bModifUltimaVersion Then
        sdbddPagos.DroppedDown = False
        Exit Sub
    End If

    m_oPagos.CargarTodosLosPagos
    
    sdbddPagos.RemoveAll

    For Each oPag In m_oPagos
        sdbddPagos.AddItem oPag.Cod & Chr(m_lSeparador) & oPag.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next

    If sdbddPagos.Rows = 0 Then
        sdbddPagos.AddItem ""
    End If
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Value)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddPagos_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddPagos_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddPagos.DataFieldList = "Column 0"
    sdbddPagos.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddPagos_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddPagos_PositionList(ByVal Text As String)
PositionList sdbddPagos, Text
End Sub

Private Sub sdbddValor_CloseUp()
    'Si es un campo de tipo lista y de tipo texto
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    If sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec Then
        If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or _
            sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Then
            sdbgCampos.Columns("COD_VALOR").Value = sdbddValor.Columns("ID").Value
            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = sdbgCampos.Columns("COD_VALOR").Value
            
            LimpiarValorListaHija sdbgCampos.Columns("ID").Value
        End If
    End If

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddValor_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Limpia los valores de las listas hijas del campo material pasado como par�metro</summary>
''' <param name="lIdCampo">Id del campo material padre</param>
''' <remarks>Llamada desde: sdbddValor_CloseUp, sdbgCampos_Change</remarks>
Private Sub LimpiarValorListaHijaMaterial(ByVal lIdCampo As Long)
    Dim lIdListaHijaDef As Long
    Dim lIdListaHija As Long
    Dim oForm As CFormulario
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim bEncontrado As Boolean
    Dim iRow As Integer
    Dim i As Integer
    Dim vbm As Variant
    
    'Si el campo est� como campo padre de alguna lista se quita el valor de esa lista
    Set oForm = oFSGSRaiz.Generar_CFormulario
    lIdListaHijaDef = oForm.DevolverIdCampoListaHija(lIdCampo)
    Set oForm = Nothing
    If lIdListaHijaDef > 0 Then
        'Si est� cargado el la colecci�n se actualiza el valor del objeto
        For Each oGrupo In g_oSolicitudSeleccionada.Grupos
            For Each oCampo In oGrupo.CAMPOS
                If oCampo.IdCampoDef = lIdListaHijaDef Then
                    bEncontrado = True
                    
                    lIdListaHija = oCampo.Id
                    
                    oCampo.valorBool = Null
                    oCampo.valorFec = Null
                    oCampo.valorNum = Null
                    oCampo.valorText = Null
                    
                    Exit For
                End If
            Next
            
            If bEncontrado Then Exit For
        Next
        Set oGrupo = Nothing
        Set oCampo = Nothing
        
        'Si est� en el mismo grupo se actualiza el valor en el grid (el valor en BD se actualizar� al hacer el Update del grid)
        'Si no est� en el mismo grupo se actualiza directamente en BD
        bEncontrado = False
        iRow = sdbgCampos.Row
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.GetBookmark(i)
            If sdbgCampos.Columns("ID").CellValue(vbm) = lIdListaHija Then
                bEncontrado = True
                sdbgCampos.Bookmark = vbm
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                
                g_oGrupoSeleccionado.CAMPOS.Item(CStr(lIdListaHija)).ResetearValores True
                
                If g_oGrupoSeleccionado.CAMPOS.Item(CStr(lIdListaHija)).TipoIntroduccion = Introselec Then LimpiarValorListaHija lIdListaHija
                
                Exit For
            End If
        Next
        sdbgCampos.Row = iRow
        If Not bEncontrado Then
            Set oCampo = oFSGSRaiz.Generar_CFormCampo
            oCampo.Id = lIdListaHija
            oCampo.ResetearValores True
            
            oCampo.CargarDatosFormCampo
            If oCampo.TipoIntroduccion = Introselec Then LimpiarValorListaHija lIdListaHija
        End If
    End If
End Sub

''' <summary>Limpia los valores de las listas hijas del campo pasado como par�metro</summary>
''' <param name="lIdCampo">Id del campo de la lista padre</param>
''' <remarks>Llamada desde: sdbddValor_CloseUp, sdbgCampos_Change</remarks>
Private Sub LimpiarValorListaHija(ByVal lIdCampo As Long)
    Dim iRow As Integer
    Dim lIdListaHija As Long
    Dim lIdListaHijaDef As Long
    Dim i As Integer
    Dim vbm As Variant
    Dim oCampo As CFormItem
    Dim oForm As CFormulario
        
    'Si el campo est� como campo padre de alguna lista se quita el valor de esa lista
    Set oForm = oFSGSRaiz.Generar_CFormulario
    lIdListaHijaDef = oForm.DevolverIdCampoListaHija(g_oGrupoSeleccionado.CAMPOS.Item(CStr(lIdCampo)).IdCampoDef)
    Set oForm = Nothing
    For Each oCampo In g_oGrupoSeleccionado.CAMPOS
        If oCampo.IdCampoDef = lIdListaHijaDef Then
            lIdListaHija = oCampo.Id
            Exit For
        End If
    Next
    Set oCampo = Nothing
               
    'Si el campo est� como campo padre de alguna lista se quita el valor de esa lista
    If lIdListaHija > 0 Then
        iRow = sdbgCampos.Row
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.GetBookmark(i)
            If sdbgCampos.Columns("ID").CellValue(vbm) = lIdListaHija Then
                sdbgCampos.Bookmark = vbm
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                
                g_oGrupoSeleccionado.CAMPOS.Item(CStr(lIdListaHija)).ResetearValores
                
                If sdbgCampos.Columns("INTRO").CellValue(vbm) = TAtributoIntroduccion.Introselec Then LimpiarValorListaHija lIdListaHija
                
                Exit For
            End If
        Next
        sdbgCampos.Row = iRow
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oLista As CCampoValorListas
    Dim oElem As CCampoValorLista
    Dim iOrdenPadre As Integer
    Dim lConCampoPadre As Long
    Dim rs As Recordset
    Dim sGMN As String
    Dim arGMN As Variant
    Dim oCampo As CFormItem
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
  
    If Not m_bModificar Or Not m_bModifUltimaVersion Then
        sdbddValor.Enabled = False
        Exit Sub
    End If

    sdbddValor.RemoveAll
    
    If sdbgCampos.Columns("INTRO").Value = "1" Then
        lConCampoPadre = 0
        'Mirar si la lista tiene campoPadre seleccionado
        If g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre > 0 Then
            lConCampoPadre = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre
            
            If Not g_oGrupoSeleccionado.CAMPOS.Item(CStr(lConCampoPadre)) Is Nothing Then
                If g_oGrupoSeleccionado.CAMPOS.Item(CStr(lConCampoPadre)).CampoGS = TipoCampoGS.material Then
                    arGMN = DevolverMaterial(Trim(g_oGrupoSeleccionado.CAMPOS.Item(CStr(lConCampoPadre)).valorText))
                    sGMN = arGMN(1) & IIf(arGMN(2) = "", "", "|" & arGMN(2) & IIf(arGMN(3) = "", "", "|" & arGMN(3) & IIf(arGMN(4) = "", "", "|" & arGMN(4))))
                Else
                    If IsNumeric(g_oGrupoSeleccionado.CAMPOS.Item(CStr(lConCampoPadre)).valorNum) Then
                        iOrdenPadre = g_oGrupoSeleccionado.CAMPOS.Item(CStr(lConCampoPadre)).valorNum
                    End If
                End If
            Else
                'Se trata de una lista con padre en otro grupo. S�lo un campo material puede ser padre de una lista de otro grupo
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                oCampo.Id = lConCampoPadre
                sGMN = oCampo.DevolverValorCampo
                If sGMN <> "" Then
                    arGMN = DevolverMaterial(sGMN)
                    sGMN = arGMN(1) & IIf(arGMN(2) = "", "", "|" & arGMN(2) & IIf(arGMN(3) = "", "", "|" & arGMN(3) & IIf(arGMN(4) = "", "", "|" & arGMN(4))))
                End If
                Set oCampo = Nothing
            End If
        End If
        If (lConCampoPadre = 0) Or (lConCampoPadre > 0 And iOrdenPadre > 0) Or sGMN <> "" Then
            'Selecci�n mediante lista de valores:
            If sGMN <> "" Then
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                Set rs = oCampo.DevolverValoresListasCampoPadre(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).IdCampoDef, 0, g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo, sGMN)
                Set oCampo = Nothing
                
                While Not rs.EOF
                    sdbddValor.AddItem rs("TEXTO").Value & Chr(m_lSeparador) & rs("ORDEN").Value
                    rs.MoveNext
                Wend
            Else
                g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).CargarValoresLista , iOrdenPadre
                
                Set oLista = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).ValoresLista
                
                For Each oElem In oLista
                    Select Case sdbgCampos.Columns("SUBTIPO").Value
                        Case TiposDeAtributos.TipoFecha
                            sdbddValor.AddItem oElem.valorFec
                        Case TiposDeAtributos.TipoNumerico
                            sdbddValor.AddItem oElem.valorNum
                        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                            sdbddValor.AddItem oElem.valorText.Item(CStr(gParametrosInstalacion.gIdioma)).Den & Chr(m_lSeparador) & oElem.Orden
                    End Select
                Next
                Set oLista = Nothing
            End If
        End If
    Else
        'Campo de tipo boolean:
        If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoBoolean Then
            sdbddValor.AddItem m_sIdiTrue
            sdbddValor.AddItem m_sIdiFalse
        End If
    End If

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddValor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddValor_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddValor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgCampos.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddValor_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgCampos_AfterUpdate(RtnDispErrMsg As Integer)
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim sVariables() As String
Dim dValues() As Double
Dim i As Integer

Dim vbm As Variant
Dim j As Integer
Dim Adores As Ador.Recordset

Dim RowPrec, RowProv, rowInit As Integer
Dim bCargarUltADJ As Boolean

Dim sCodArt As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
RowPrec = -1
RowProv = -1

    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
        rowInit = sdbgCampos.Row - 1
        'Buscar si hay Precio y Proveedor y mirar si tiene el checkBox de cargar el ultimo Precio/proveedor adjucicado
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.GetBookmark(i)
            If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.PrecioUnitario Then
                If g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).CargarUltADJ = True Then
                    RowPrec = i + 1
                End If
            End If
            If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Proveedor Then
                If g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).CargarUltADJ = True Then
                    RowProv = i + 1
                End If
            End If
        Next i
        
        If RowPrec <> -1 Or RowProv <> -1 Then
            bCargarUltADJ = True
        End If
    
        
        ''Si el Codigo articulo introducido es un articulo codificado introducir la denominacion
        Dim oArticulos As CArticulos
        Set oArticulos = oFSGSRaiz.Generar_CArticulos

        If sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1)) <> "" Then
            Dim arrMat As Variant
            sCodArt = sdbgCampos.Columns("VALOR").Value
            
            arrMat = DevolverMaterial(sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1)))
            oArticulos.CargarTodosLosArticulos sCodArt, , True, , , , arrMat(1), arrMat(2), arrMat(3), arrMat(4)
            
            If oArticulos.Count = 1 Then
                
                sdbgCampos.Row = sdbgCampos.Row + 1
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                    sdbgCampos_Change
                    sdbgCampos.Columns("COD_VALOR").Value = oArticulos.Item(1).Den
                    sdbgCampos.Columns("VALOR").Value = oArticulos.Item(1).Den
                End If
                If bCargarUltADJ And Not oArticulos.Item(1).Generico Then
                    If RowPrec <> -1 Then
                        Dim sMoneda As String
                        Dim dPrecio As Double
                        If Not g_oSolicitudSeleccionada Is Nothing Then
                            sMoneda = g_oSolicitudSeleccionada.Moneda
                        End If
                        dPrecio = oArticulos.cargarUltPrecioAdj(sCodArt, sMoneda)
                        
                        sdbgCampos.Row = RowPrec
                        sdbgCampos.Columns("COD_VALOR").Value = dPrecio
                        sdbgCampos.Columns("VALOR").Value = dPrecio
                        
                    End If
                    If RowProv <> -1 Then
                        Dim vProveedor As Variant
                        vProveedor = oArticulos.cargarUltProveedorAdj(sCodArt)
                        sdbgCampos.Row = RowProv
                        If Not IsNull(vProveedor) Then
                            sdbgCampos.Columns("COD_VALOR").Value = vProveedor(1)
                            sdbgCampos.Columns("VALOR").Value = vProveedor(1) & " - " & vProveedor(2)
                        Else
                            sdbgCampos.Columns("COD_VALOR").Value = Null
                            sdbgCampos.Columns("VALOR").Value = ""
                        
                        End If
                    End If
                End If
                sdbgCampos.Row = rowInit
            End If
            Set oArticulos = Nothing
        End If
    End If



'''    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo And sdbgCampos.Columns("COD_VALOR").Value <> "" Then
'''        ''Si el Codigo articulo introducido es un articulo codificado introducir la denominacion
'''        Dim arrMat As Variant
'''
'''        Dim oArticulos As CArticulos
'''        Set oArticulos = oFSGSRaiz.Generar_CArticulos
'''
'''        If sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1)) <> "" Then
'''            arrMat = DevolverMaterial(sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1)))
'''            oArticulos.CargarTodosLosArticulos sdbgCampos.Columns("COD_VALOR").Value, , True, , , , arrMat(1), arrMat(2), arrMat(3), arrMat(4)
'''
'''            If oArticulos.Count = 1 Then
'''                sdbgCampos.Row = sdbgCampos.Row + 1
'''                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
'''                    sdbgCampos.Columns("COD_VALOR").Value = oArticulos.Item(1).Den
'''                    sdbgCampos.Columns("VALOR").Value = oArticulos.Item(1).Den
'''                End If
'''                sdbgCampos.Row = sdbgCampos.Row - 1
'''            End If
'''            Set oArticulos = Nothing
'''        End If
'''    End If


    'Si se ha modificado un campo num�rico y hay f�rmulas se calculan sus valores:
    If sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoNumerico Then Exit Sub
    If basParametros.gParametrosGenerales.gsAccesoFSWS <> AccesoFSWSCompleto Then Exit Sub
            
    Set Adores = g_oSolicitudSeleccionada.CargarCamposCalculados(False)
    If Adores Is Nothing Then Exit Sub
    
    Set iEq = New USPExpression
    i = 1
    'Almacena las variables en un array:
    ReDim sVariables(Adores.RecordCount)
    ReDim dValues(Adores.RecordCount)
    
    While Not Adores.EOF
        sVariables(i) = Adores.Fields("ID_CALCULO").Value
        
        If g_oSolicitudSeleccionada.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).CAMPOS Is Nothing Then
            g_oSolicitudSeleccionada.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).CargarTodosLosCampos
        End If
        
        If g_oSolicitudSeleccionada.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).CAMPOS.Item(CStr(Adores.Fields("ID_CAMPO").Value)).valorNum <> "" Then
            dValues(i) = CDbl(NullToDbl0(g_oSolicitudSeleccionada.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).CAMPOS.Item(CStr(Adores.Fields("ID_CAMPO").Value)).valorNum))
        End If
        
        i = i + 1
        Adores.MoveNext
    Wend
                
    'Ahora va recalculando todas las f�rmulas:
    Adores.MoveFirst
    i = 1
    While Not Adores.EOF
        If Adores.Fields("TIPO").Value = TipoCampoPredefinido.Calculado And Not IsNull(Adores.Fields("FORMULA").Value) Then
            If IsNull(Adores.Fields("ORIGEN_CALC_DESGLOSE").Value) Then
                lIndex = iEq.Parse(Adores.Fields("FORMULA").Value, sVariables, lErrCode)
                If lErrCode = USPEX_NO_ERROR Then
                    On Error GoTo EvaluationError
                    dValues(i) = iEq.Evaluate(dValues)
                    'almacena en la colecci�n:
                    If g_oSolicitudSeleccionada.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).CAMPOS.Item(CStr(Adores.Fields("ID_CAMPO").Value)).valorNum <> dValues(i) Then
                        g_oSolicitudSeleccionada.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).CAMPOS.Item(CStr(Adores.Fields("ID_CAMPO").Value)).modificado = True
                        g_oSolicitudSeleccionada.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).CAMPOS.Item(CStr(Adores.Fields("ID_CAMPO").Value)).valorNum = dValues(i)
                        
                        'Modifica la grid
                        If Adores.Fields("GRUPO").Value = g_oGrupoSeleccionado.Id Then
                            For j = 0 To sdbgCampos.Rows - 1
                                vbm = sdbgCampos.AddItemBookmark(j)
                                If CLng(sdbgCampos.Columns("ID").CellValue(vbm)) = CLng(Adores.Fields("ID_CAMPO").Value) Then
                                    sdbgCampos.Bookmark = vbm
                                    sdbgCampos.Columns("VALOR").Value = dValues(i)
                                    sdbgCampos.Update
                                    Exit For
                                End If
                            Next j
                        End If
                    End If
                    
                Else  'Ha habido error:
                    oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                    Exit Sub
                End If
            End If
        End If
        
        i = i + 1
        Adores.MoveNext
    Wend
 
    Adores.Close
    Set Adores = Nothing
    
    Set iEq = Nothing
    Exit Sub
    
EvaluationError:
    oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_AfterUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' ******** REALIZA LAS COMPROBACIONES CORRESPONDIENTES ***************
''' </summary>
''' <param name="Cancel">Cancelar el update</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgCampos_BeforeUpdate(Cancel As Integer)
    Dim vMaxLength As Variant

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    If sdbgCampos.Columns("VALOR").Value <> "" Then
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
            'cambia la descripcion
            
        ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
            'Si son los campos de GS importe o cantidad:
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.importe Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.Cantidad Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitario Then
                If Not IsNumeric(sdbgCampos.Columns("VALOR").Value) Then
                    Screen.MousePointer = vbNormal
                    oMensajes.NoValido sdbgCampos.Columns("CAMPO").Value & " " & m_sMensaje(1)
                    Cancel = True
                    Exit Sub
                End If
            End If
        ElseIf sdbgCampos.Columns("INTRO").Value <> TAtributoIntroduccion.Introselec Then  'Introducci�n libre
            Select Case sdbgCampos.Columns("SUBTIPO").Value
                Case TiposDeAtributos.TipoFecha
                    If Not IsDate(sdbgCampos.Columns("VALOR").Value) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido sdbgCampos.Columns("CAMPO").Value & " " & m_sMensaje(2)
                        Cancel = True
                        Exit Sub
                    End If
                    
                    'comprueba que el valor existente est� dentro de los valores de m�ximo y m�nimo
                    If Not IsNull(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Maximo) Then
                        If CDate(sdbgCampos.Columns("VALOR").Value) > CDate(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Maximo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido sdbgCampos.Columns("CAMPO").Value & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    If Not IsNull(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Minimo) Then
                        If CDate(sdbgCampos.Columns("VALOR").Value) < CDate(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Minimo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido sdbgCampos.Columns("CAMPO").Value & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    
                Case TiposDeAtributos.TipoNumerico
                    If Not IsNumeric(sdbgCampos.Columns("VALOR").Value) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido sdbgCampos.Columns("CAMPO").Value & " " & m_sMensaje(1)
                        Cancel = True
                        Exit Sub
                    End If
                    
                    'comprueba que el valor existente est� dentro de los valores de m�ximo y m�nimo
                    If Not IsNull(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Maximo) Then
                        If CDbl(sdbgCampos.Columns("VALOR").Value) > CDbl(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Maximo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido sdbgCampos.Columns("CAMPO").Value & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    If Not IsNull(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Minimo) Then
                        If CDbl(sdbgCampos.Columns("VALOR").Value) < CDbl(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Minimo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido sdbgCampos.Columns("CAMPO").Value & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                
                
                Case TiposDeAtributos.TipoBoolean
                    If UCase(sdbgCampos.Columns("VALOR").Value) <> UCase(m_sIdiTrue) And UCase(sdbgCampos.Columns("VALOR").Value) <> UCase(m_sIdiFalse) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido sdbgCampos.Columns("CAMPO").Value & " " & m_sMensaje(3)
                        Cancel = True
                        Exit Sub
                    End If
                    
                 Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                    If IsNull(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength) Or g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength = "" Then
                        'Para el caso de las solicitudes anteriores a la creaci�n del campo MAX_LENGTH
                        If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Then
                            vMaxLength = 100
                        ElseIf sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Then
                            vMaxLength = 800
                        Else
                            vMaxLength = -1
                        End If
                    Else
                        vMaxLength = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength
                    End If
                    
                    If CInt(vMaxLength) > 0 And Len(sdbgCampos.Columns("VALOR").Value) > CInt(vMaxLength) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.LongitudMaxima (vMaxLength)
                        Cancel = True
                        Exit Sub
                    End If
            End Select
        End If
    End If

    '''******* Actualiza las colecciones ****************************
    'Le pone la propiedad modificado en la colecci�n (si ha habido cambios, sino se deja como est�):
    g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = True
        
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.externo
            Dim intTabla As Integer
            Dim strNombreTabla As String, strPK As String
            Dim vTipoDeDatoPK As TiposDeAtributos
            intTabla = CInt(sdbgCampos.Columns(11).Value)
            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).TablaExterna = intTabla
            strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
            strPK = m_oTablaExterna.SacarPK(intTabla)
            vTipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
            With g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
                If Trim(sdbgCampos.Columns("VALOR").Value = "") Then
                   'Trim(frmSolicitudDetalle.g_strPK) = ""
                    .valorFec = Null: .valorBool = Null: .valorNum = Null: .valorText = Null
                Else
                    Select Case vTipoDeDatoPK
                        Case TipoFecha
                            .Tipo = TipoFecha
                            .valorFec = CDate(frmSolicitudDetalle.g_strPK)
                            .valorBool = Null: .valorNum = Null: .valorText = Null
                        Case TipoNumerico
                            .Tipo = TipoNumerico
                            .valorNum = CLng(frmSolicitudDetalle.g_strPK)
                            .valorBool = Null: .valorFec = Null: .valorText = Null
                        Case TipoTextoMedio
                            .Tipo = TipoTextoMedio
                            .valorText = frmSolicitudDetalle.g_strPK
                            .valorBool = Null: .valorFec = Null: .valorNum = Null
                    End Select
                End If
            End With
            '--------------------------------------------------------------------------------------------
        Else
            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
                Select Case sdbgCampos.Columns("CAMPO_GS").Value
                    Case TipoCampoSC.DescrBreve, TipoCampoSC.DescrDetallada
                        If NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = sdbgCampos.Columns("VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                        
                    Case TipoCampoGS.Dest, TipoCampoGS.FormaPago, TipoCampoGS.Proveedor, TipoCampoGS.Pais, TipoCampoGS.provincia, TipoCampoGS.material, TipoCampoGS.CodArticulo, TipoCampoGS.Unidad, TipoCampoGS.Moneda, TipoCampoGS.CampoPersona
                    
                        If ((NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("COD_VALOR").Value)) Or (NullToStr(sdbgCampos.Columns("COD_VALOR").Value) = "")) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = StrToNull(sdbgCampos.Columns("COD_VALOR").Value)
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                        
                    Case TipoCampoGS.NuevoCodArticulo
                        If NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("COD_VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = sdbgCampos.Columns("COD_VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                        
                    Case TipoCampoGS.DenArticulo
                        If NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = sdbgCampos.Columns("VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
        
                    Case TipoCampoSC.importe, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario
                        If NullToDbl0(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum) <> NullToDbl0(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = sdbgCampos.Columns("VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                        
                    Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                        If NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorFec) <> NullToStr(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorFec = sdbgCampos.Columns("VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
        
                    Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                        If NullToDbl0(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToDbl0(sdbgCampos.Columns("COD_VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = sdbgCampos.Columns("COD_VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
          
                    Case TipoCampoSC.ArchivoEspecific
                        If NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = sdbgCampos.Columns("VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                        
                    Case TipoCampoGS.UnidadOrganizativa
                        If ((NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("VALOR").Value)) Or (NullToStr(sdbgCampos.Columns("VALOR").Value) = "")) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = StrToNull(sdbgCampos.Columns("VALOR").Value)
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                    Case TipoCampoGS.Departamento
                        If ((NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("VALOR").Value)) Or (NullToStr(sdbgCampos.Columns("VALOR").Value) = "")) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = StrToNull(sdbgCampos.Columns("COD_VALOR").Value)
                            sdbgCampos.Columns("BAJALOGICA").Value = False
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                        
                    Case TipoCampoGS.OrganizacionCompras, TipoCampoGS.Centro, TipoCampoGS.ProveedorERP
                        If ((NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("COD_VALOR").Value)) Or (NullToStr(sdbgCampos.Columns("COD_VALOR").Value) = "")) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = StrToNull(sdbgCampos.Columns("COD_VALOR").Value)
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                    Case TipoCampoGS.Almacen
                        If ((NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum) <> NullToStr(sdbgCampos.Columns("COD_VALOR").Value)) Or (NullToStr(sdbgCampos.Columns("COD_VALOR").Value) = "")) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = StrToNull(sdbgCampos.Columns("COD_VALOR").Value)
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                    Case TipoCampoGS.Empresa
                        If ((NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum) <> NullToStr(sdbgCampos.Columns("COD_VALOR").Value)) Or (NullToStr(sdbgCampos.Columns("COD_VALOR").Value) = "")) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = StrToNull(sdbgCampos.Columns("COD_VALOR").Value)
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                End Select
            Else
                Select Case sdbgCampos.Columns("SUBTIPO").Value
                    Case TiposDeAtributos.TipoFecha
                        If NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorFec) <> NullToStr(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorFec = sdbgCampos.Columns("VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
        
                    Case TiposDeAtributos.TipoNumerico
                        If NullToDbl0(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum) <> NullToDbl0(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = sdbgCampos.Columns("VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
        
                    Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                        If NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = sdbgCampos.Columns("VALOR").Value
                            If sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec Then  'Si es un campo de tipo lista guardamos el �ndice en valor num
                                g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = sdbgCampos.Columns("COD_VALOR").Value
                            End If
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
        
                    Case TiposDeAtributos.TipoBoolean
                        Select Case UCase(sdbgCampos.Columns("VALOR").Value)
                            Case UCase(m_sIdiTrue)
                                If m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool <> 1 Then
                                    g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool = 1
                                ElseIf IsNull(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool) Then
                                    g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool = 1
                                Else
                                    g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                                End If
                            Case UCase(m_sIdiFalse)
                                If m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool <> 0 Then
                                    g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool = 0
                                ElseIf IsNull(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool) Then
                                    g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool = 0
                                Else
                                    g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                                End If
                            Case Else
                                g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool = Null
                        End Select
        
                    Case TiposDeAtributos.TipoArchivo, TiposDeAtributos.TipoEditor
                        If NullToStr(m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText) <> NullToStr(sdbgCampos.Columns("VALOR").Value) Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = sdbgCampos.Columns("VALOR").Value
                        Else
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = False
                        End If
                End Select
            End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
''' <summary>
''' Evento de grid cuando se pulsa algun boton en el grid
''' </summary>
''' <remarks>Llamada desde: Evento de grid; Tiempo m�ximo: instantes</remarks>
Private Sub sdbgCampos_BtnClick()
    Dim TipoDeDatoPK As TiposDeAtributos
    Dim intTabla As Integer
    Dim strNombreTabla As String, strPK As String, strDenoTabla As String, strPrimerCampoNoPK As String
    Dim valor As Variant
    Dim sObs As String
    Dim lMaxLength As Long
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgCampos.col < 0 Then Exit Sub
    
    If g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)) Is Nothing Then Exit Sub
    
    Select Case sdbgCampos.Columns(sdbgCampos.col).Name
        Case "AYUDA"
            'Muestra el formulario con la ayuda:
            MostrarFormFormCampoAyuda oGestorIdiomas, gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, oGestorSeguridad, oUsuarioSummit.Cod, gParametrosGenerales, _
                FormCampoAyudaTipoAyuda.CampoInstancia, False, g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
        
        Case "VALOR"
            
            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                If m_bModificar = True And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef) Then
                    '_______________________________________________________________________
                    intTabla = CInt(sdbgCampos.Columns(11).Value)
                    m_intTabla = intTabla   'paso de vbles entre Click y BeforeUpdate
                    m_blnExisteTablaExternaParaArticulo = IIf(m_oTablaExterna.SacarTablaTieneART(intTabla), True, False)
                    strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
                    strDenoTabla = m_oTablaExterna.SacarDenominacionTabla(intTabla)
                    strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(intTabla)
                    strPK = m_oTablaExterna.SacarPK(intTabla)
                    TipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
                    '_______________________________________________________________________
                    If sdbgCampos.Columns("VALOR").Value <> "" Then
                        valor = sdbgCampos.Columns(12).Value
                        Select Case TipoDeDatoPK
                            Case TipoFecha
                                valor = CDate(valor)
                            Case TipoNumerico
                                valor = CLng(valor)
                        End Select
                    End If
                    
                    Dim sIdART As String
                    If ExisteAtributoArticulo() Then
                        sIdART = DevuelveIdArticulo()
                        m_blnExisteArticulo = True
                    Else
                        sIdART = ""
                        m_blnExisteArticulo = False
                    End If

                    m_strPK_old = g_strPK
                    
                    FSGSForm.MostrarFormTablaExterna oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, g_strPK, valor, _
                                         strDenoTabla, strNombreTabla, strPK, sIdART, intTabla, "frmSolicitudDetalle"
                                         
                    'aqui devuelve la primary key del registro en g_strPK
                    'hay que sacar la primary key + el primer campo no primary key .lo buscamos:
                    'colocamos en la columna la clave y el primer valor que no sea clave
                    '_______________________________________________________________________
                    If frmSolicitudDetalle.g_strPK = "vbFormControlMenu" Then
                        'lo dejo como esta
                        g_strPK = m_strPK_old
                    ElseIf frmSolicitudDetalle.g_strPK <> "" Then
                        sdbgCampos.Columns("VALOR").Value = frmSolicitudDetalle.g_strPK & " " & m_oTablaExterna.ValorCampo(frmSolicitudDetalle.g_strPK, strPK, strPrimerCampoNoPK, strNombreTabla, TipoDeDatoPK)
                        sdbgCampos.Columns(12).Value = frmSolicitudDetalle.g_strPK
                    Else    '?
                        sdbgCampos.Columns("VALOR").Value = ""
                        sdbgCampos.Columns(12).Value = ""
                    End If
                    sdbgCampos_Change
                    sdbgCampos.Update
                '�����������������������������������������������������������������������
                End If
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Servicio Then
                Exit Sub
            Else
                'Dependiendo del tipo de campo de GS que sea mostrar� las pantallas para seleccionar los valores
                If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
                    'Si son campos de GS:
                    Select Case sdbgCampos.Columns("CAMPO_GS").Value
                        Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro
                        ''miramos si puede modificar
                            If m_bModificar = True And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef) Then
                                Set frmCalendar.frmDestination = frmSolicitudes.g_ofrmDetalleSolic
                                Set frmCalendar.ctrDestination = Nothing
                                frmCalendar.addtotop = 900 + 360
                                frmCalendar.addtoleft = 180
                                If sdbgCampos.Columns("VALOR").Value <> "" Then
                                    frmCalendar.Calendar.Value = sdbgCampos.Columns("VALOR").Value
                                Else
                                    frmCalendar.Calendar.Value = Date
                                End If
                                frmCalendar.Show vbModal
                            End If
                            
                        Case TipoCampoSC.DescrDetallada
                            sObs = NullToStr(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText)
                            If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgCampos.Columns("CAMPO").Value, _
                            IIf(m_bModificar, m_bModifUltimaVersion And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef), m_bModificar), -1, sObs) Then
                                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = sObs
                                frmSolicitudes.g_ofrmDetalleSolic.g_oGrupoSeleccionado.CAMPOS.Item(CStr(frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("ID").Value)).valorText = sObs
                                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Update
                            End If
                            
                        Case TipoCampoGS.Proveedor
                            frmPROVEBuscar.sOrigen = "frmSolicitudDetalle"
                            If m_bRestrProve = True Then
                                frmPROVEBuscar.codEqp = basOptimizacion.gCodEqpUsuario
                            End If
                            frmPROVEBuscar.Show vbModal
                            
                        Case TipoCampoGS.Pres1
                            frmPRESAsig.g_sOrigen = "frmSolicitudDetalle"
                            
                            frmPRESAsig.g_dblAsignado = ObtenerPresupAsignado(sdbgCampos.Columns("COD_VALOR").Value)
                            frmPRESAsig.g_bHayPres = (frmPRESAsig.g_dblAsignado > 0)
                            frmPRESAsig.g_bHayPresBajaLog = False
                            frmPRESAsig.g_iTipoPres = 1
                            frmPRESAsig.g_bModif = m_bModificar And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef)
                            frmPRESAsig.g_bSinPermisos = False
                            frmPRESAsig.g_bRUO = m_bRestrPresUO
                            frmPRESAsig.g_sValorPresFormulario = sdbgCampos.Columns("COD_VALOR").Value
                            frmPRESAsig.Show vbModal
                                                        
                        Case TipoCampoGS.Pres2
                            frmPRESAsig.g_dblAsignado = ObtenerPresupAsignado(sdbgCampos.Columns("COD_VALOR").Value)
                            frmPRESAsig.g_bHayPres = (frmPRESAsig.g_dblAsignado > 0)
                            frmPRESAsig.g_bHayPresBajaLog = False
                            frmPRESAsig.g_sOrigen = "frmSolicitudDetalle"
                            frmPRESAsig.g_iTipoPres = 2
                            frmPRESAsig.g_bModif = m_bModificar And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef)
                            frmPRESAsig.g_bSinPermisos = False
                            frmPRESAsig.g_bRUO = m_bRestrPresUO
                            frmPRESAsig.g_sValorPresFormulario = sdbgCampos.Columns("COD_VALOR").Value
                            frmPRESAsig.Show vbModal
            
                        Case TipoCampoGS.Pres3
                            frmPRESAsig.g_dblAsignado = ObtenerPresupAsignado(sdbgCampos.Columns("COD_VALOR").Value)
                            frmPRESAsig.g_bHayPres = (frmPRESAsig.g_dblAsignado > 0)
                            frmPRESAsig.g_bHayPresBajaLog = False
                            frmPRESAsig.g_sOrigen = "frmSolicitudDetalle"
                            frmPRESAsig.g_iTipoPres = 3
                            frmPRESAsig.g_bModif = m_bModificar And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef)
                            frmPRESAsig.g_bSinPermisos = False
                            frmPRESAsig.g_bRUO = m_bRestrPresUO
                            frmPRESAsig.g_sValorPresFormulario = sdbgCampos.Columns("COD_VALOR").Value
                            frmPRESAsig.Show vbModal
        
                        Case TipoCampoGS.Pres4
                            frmPRESAsig.g_dblAsignado = ObtenerPresupAsignado(sdbgCampos.Columns("COD_VALOR").Value)
                            frmPRESAsig.g_bHayPres = (frmPRESAsig.g_dblAsignado > 0)
                            frmPRESAsig.g_bHayPresBajaLog = False
                            frmPRESAsig.g_sOrigen = "frmSolicitudDetalle"
                            frmPRESAsig.g_iTipoPres = 4
                            frmPRESAsig.g_bModif = m_bModificar And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef)
                            frmPRESAsig.g_bSinPermisos = False
                            frmPRESAsig.g_bRUO = m_bRestrPresUO
                            frmPRESAsig.g_sValorPresFormulario = sdbgCampos.Columns("COD_VALOR").Value
                            frmPRESAsig.Show vbModal
            
                        Case TipoCampoSC.ArchivoEspecific
                            frmFormAdjuntos.g_sOrigen = "frmSolicitudDetalle"
                            Set frmFormAdjuntos.g_oCampoSeleccionado = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
                            Set frmFormAdjuntos.g_oInstanciaSeleccionada = g_oSolicitudSeleccionada
                            If m_bModificar = False Then
                                frmFormAdjuntos.g_bModif = False
                            Else
                                frmFormAdjuntos.g_bModif = m_bModifUltimaVersion And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef)
                            End If
                            frmFormAdjuntos.Show vbModal
                            
                        Case TipoCampoGS.Desglose
                            'Valores del desglose:
                            Set g_ofrmDesgloseValores = ObtenerFormularioDesglose(CStr(sdbgCampos.Columns("ID").Value))
                            If m_bModificar = False Then
                                g_ofrmDesgloseValores.g_bModif = False
                            Else
                                g_ofrmDesgloseValores.g_bModif = m_bModifUltimaVersion
                            End If
                            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.OrganizacionCompras Then
                                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                                g_ofrmDesgloseValores.g_sCodCentro = "-1"
                            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Centro Then
                                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-2))
                                g_ofrmDesgloseValores.g_sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                            Else
                                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = "-1"
                            End If
                            Set g_ofrmDesgloseValores.g_oCampoDesglose = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
                            Set g_ofrmDesgloseValores.g_oInstanciaSeleccionada = g_oSolicitudSeleccionada
                            g_ofrmDesgloseValores.g_sOrigen = g_sOrigen
                            Set g_ofrmDesgloseValores.g_oCumplimentacionesGS = m_oCumplimentacionesGS
                                                        
                            g_ofrmDesgloseValores.WindowState = vbNormal
                            g_ofrmDesgloseValores.Show vbModal
                        
                        Case TipoCampoGS.material
                            frmSELMAT.sOrigen = "frmSolicitudDetalle"
                            frmSELMAT.bRComprador = m_bRestrMat
                            frmSELMAT.Hide
                            frmSELMAT.nivelSeleccion = CampoSeleccionado.nivelSeleccion
                            frmSELMAT.Show vbModal
                            
                         Case TipoCampoGS.CampoPersona
                            frmSOLSelPersona.g_sOrigen = "frmSolicitudDetalle"
                            frmSOLSelPersona.bAllowSelUON = False
                            frmSOLSelPersona.bRDep = m_bRDep
                            frmSOLSelPersona.bRuo = m_bRuo
                            frmSOLSelPersona.Show vbModal
                            
                        Case TipoCampoGS.UnidadOrganizativa
                            frmSELUO.sOrigen = "frmSolicitudDetalle"
                            frmSELUO.Show vbModal
                            MostrarUOSeleccionada
                        
                        Case TipoCampoGS.RefSolicitud
                            frmSolicitudDetalle.m_lIDSOLPadre = 0
                            frmSolicitudesBusquedaSOLPadre.g_lIdRefSol = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).IDRefSolicitud
                            frmSolicitudesBusquedaSOLPadre.Show vbModal
                            MostrarSolPadreSeleccionada
                        Case TipoCampoGS.CodComprador
                            Exit Sub
                    End Select
                    
                Else
                    'Si es un campo de tipo archivo
                    Select Case sdbgCampos.Columns("SUBTIPO").Value
                        Case TiposDeAtributos.TipoArchivo
                            frmFormAdjuntos.g_sOrigen = "frmSolicitudDetalle"
                            Set frmFormAdjuntos.g_oCampoSeleccionado = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
                            Set frmFormAdjuntos.g_oInstanciaSeleccionada = g_oSolicitudSeleccionada
                            If m_bModificar = False Then
                                frmFormAdjuntos.g_bModif = False
                            Else
                                frmFormAdjuntos.g_bModif = m_bModifUltimaVersion And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef)
                            End If
                            frmFormAdjuntos.Show vbModal
                        
                        Case TiposDeAtributos.TipoFecha    'Muestra el calendario
                            Set frmCalendar.frmDestination = frmSolicitudes.g_ofrmDetalleSolic
                            Set frmCalendar.ctrDestination = Nothing
                            frmCalendar.addtotop = 900 + 360
                            frmCalendar.addtoleft = 180
                            If sdbgCampos.Columns("VALOR").Value <> "" Then
                                frmCalendar.Calendar.Value = sdbgCampos.Columns("VALOR").Value
                            Else
                                frmCalendar.Calendar.Value = Date
                            End If
                            frmCalendar.Show vbModal
                            
                        Case TiposDeAtributos.TipoTextoMedio
                            If (IsNull(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength)) Or (g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength = "") Then
                                lMaxLength = 800
                            Else
                                lMaxLength = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength
                            End If
                            sObs = NullToStr(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText)
                            If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgCampos.Columns("CAMPO").Value, _
                            IIf(m_bModificar, m_bModifUltimaVersion And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef), m_bModificar), lMaxLength, sObs) Then
                                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = sObs
                                frmSolicitudes.g_ofrmDetalleSolic.g_oGrupoSeleccionado.CAMPOS.Item(CStr(frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("ID").Value)).valorText = sObs
                                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Update
                            End If
                        
                        Case TiposDeAtributos.TipoTextoLargo
                            If (IsNull(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength)) Or (g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength = "") Then
                                lMaxLength = -1
                            Else
                                lMaxLength = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength
                            End If
                            sObs = NullToStr(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText)
                            If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgCampos.Columns("CAMPO").Value, _
                            IIf(m_bModificar, m_bModifUltimaVersion And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef), m_bModificar), lMaxLength, sObs) Then
                                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = sObs
                                frmSolicitudes.g_ofrmDetalleSolic.g_oGrupoSeleccionado.CAMPOS.Item(CStr(frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("ID").Value)).valorText = sObs
                                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Update
                            End If
                            
                        Case TiposDeAtributos.TipoDesglose  'valores de un campo de tipo desglose:
                            Set g_ofrmDesgloseValores = ObtenerFormularioDesglose(CStr(sdbgCampos.Columns("ID").Value))
                            If m_bModificar = False Then
                                g_ofrmDesgloseValores.g_bModif = False
                            Else
                                g_ofrmDesgloseValores.g_bModif = m_bModifUltimaVersion
                            End If
                            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.OrganizacionCompras Then
                                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                                g_ofrmDesgloseValores.g_sCodCentro = "-1"
                            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Centro Then
                                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-2))
                                g_ofrmDesgloseValores.g_sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                            Else
                                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = "-1"
                            End If
                            Set g_ofrmDesgloseValores.g_oCampoDesglose = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
                            Set g_ofrmDesgloseValores.g_oInstanciaSeleccionada = g_oSolicitudSeleccionada
                            If g_sOrigen = "frmSolicitudes" Then
                                g_ofrmDesgloseValores.g_sOrigen = g_sOrigen
                                Set g_ofrmDesgloseValores.g_oCumplimentacionesGS = m_oCumplimentacionesGS
                            End If
                            
                            g_ofrmDesgloseValores.WindowState = vbNormal
                            g_ofrmDesgloseValores.Show vbModal
                            
                        Case TiposDeAtributos.TipoEditor
                            Dim params As String
                            Dim strSessionId As String
                            Dim bReadOnly As Boolean
                            
                            strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
                            params = "?desdeGS=1&sessionId=" & strSessionId & "&copia_campo_id=" & sdbgCampos.Columns("ID").Value
                            
                            If m_bModificar = False Then
                                params = params & "&readOnly=1"
                                bReadOnly = True
                            Else
                                If m_bModifUltimaVersion And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef) Then
                                    params = params & "&readOnly=0"
                                    bReadOnly = False
                                Else
                                    params = params & "&readOnly=1"
                                    bReadOnly = True
                                End If
                            End If
                                                        
                            With frmEditor
                                .caption = sdbgCampos.Columns("CAMPO").Value
                                .g_sOrigen = "frmSolicitudDetalle"
                                .g_sRuta = gParametrosGenerales.gsURLCampoEditor & params
                                .g_bReadOnly = bReadOnly
                                .Show vbModal
                            End With
                    End Select
                End If
            End If
            
        Case Else   'Detalle de campo:
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or (sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.DesgloseActividad) Then
                'Desglose de material:
                If Not g_ofrmDesglose Is Nothing Then
                    Unload g_ofrmDesglose
                    Set g_ofrmDesglose = Nothing
                End If
                Set g_ofrmDesglose = New frmDesglose
                Set g_ofrmDesglose.g_oCampoDesglose = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
                g_ofrmDesglose.g_sOrigen = "frmSolicitudDetalle"
                g_ofrmDesglose.g_bModif = False
                g_ofrmDesglose.g_Instancia = g_oSolicitudSeleccionada.Id
                Screen.MousePointer = vbHourglass
                MDI.MostrarFormulario g_ofrmDesglose
                Screen.MousePointer = vbNormal
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Servicio Then
                'Muestra la pantalla frmServicio pero sin opci�n a editar el contenido:
                frmServicio.g_sOrigen = "frmSolicitudDetalle"
                frmServicio.g_multiidioma = False
                frmServicio.iServAntiguo = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Servicio
                frmServicio.lCampoForm = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).campoOrigen
                frmServicio.g_Instancia = g_oSolicitudSeleccionada.Id
                frmServicio.Show vbModal
            
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                'Muestra la pantalla frmDetalleCampoExterno:
                intTabla = CInt(sdbgCampos.Columns(11).Value)
                m_intTabla = intTabla   'paso de vbles entre Click y BeforeUpdate FALTA necesario?????
                strDenoTabla = m_oTablaExterna.SacarDenominacionTabla(intTabla)
                
                MostrarFormDetalleCampoExterno oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, strDenoTabla, sdbgCampos.Columns("SUBTIPO").Value
            
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                'Muestra la pantalla de los campos calculados:
                frmFormularioCampoCalculado.g_bModif = False
                Select Case g_sOrigen
                    Case "frmPROCE", "frmPedidos", "frmSeguimiento"
                        frmFormularioCampoCalculado.g_sOrigen = g_sOrigen
                    Case Else
                        frmFormularioCampoCalculado.g_sOrigen = "frmSolicitudDetalle"
                End Select
                frmFormularioCampoCalculado.Show vbModal
            
            ElseIf (sdbgCampos.Columns("CAMPO_GS").Value <> "") And (g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef <> Normal) Then
                frmDetalleCampoPredef.g_sDenCampo = ObtenerNombreCampo(g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoGS, g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef, g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo, g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).PRES5)
                frmDetalleCampoPredef.g_iTipo = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo
                frmDetalleCampoPredef.g_iTipoCampo = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoGS
                frmDetalleCampoPredef.g_iTipoSolicit = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef
                frmDetalleCampoPredef.g_lCampoId = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
                frmDetalleCampoPredef.g_sOrigen = "frmSolicitudDetalle"
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material Then
                    frmDetalleCampoPredef.permitirCambiarNivelSeleccion = False
                    g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).CargarNivelSeleccion 1
                    frmDetalleCampoPredef.nivelSeleccion = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).nivelSeleccion
                End If
                frmDetalleCampoPredef.Show vbModal
            
            Else  'Detalle del campo normal
                frmDetalleCampos.g_sOrigen = "frmSolicitudDetalle"
                frmDetalleCampos.g_bModif = False
                Set frmDetalleCampos.g_oCampo = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
                frmDetalleCampos.Show vbModal
            End If
            
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgCampos_Change()
    Dim vMaxLength As Variant
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    picControl.Visible = False
    picEdicion.Visible = True
    
    If m_bUpdate Then
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
            sdbgCampos.Columns("GENERICO").Value = True
        End If
        
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
            sdbgCampos.Columns("COD_VALOR").Value = sdbgCampos.Columns("VALOR").Value
        End If
        
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
            If Len(sdbgCampos.Columns("VALOR").Value) + 1 > basParametros.gLongitudesDeCodigos.giLongCodDENART Then
                sdbgCampos.Columns("VALOR").Value = Left(sdbgCampos.Columns("VALOR").Value, basParametros.gLongitudesDeCodigos.giLongCodDENART)
            End If
        End If
        
        'Si es un campo de tipo lista y de tipo texto y elimino el valor elimino el valor de las listas hijas
        If sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec And sdbgCampos.Columns("VALOR").Value = "" Then
            sdbgCampos.Columns("COD_VALOR").Value = ""
            LimpiarValorListaHija sdbgCampos.Columns("ID").Value
        End If
    End If
    
    'Controlar los tipos TipoTextoCorto, TipoTextoMedio, TipoTextoLargo para que no superen el maximo permitido cuando se hace copy/paste
    If (sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.DenArticulo) And (sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo) Then
        vMaxLength = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength
        If IsNull(vMaxLength) Or vMaxLength = "" Then
            If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Then
               vMaxLength = 100
            ElseIf sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Then
                vMaxLength = 800
            Else
                vMaxLength = -1
            End If
        End If
        If CInt(vMaxLength) > 0 And Len(sdbgCampos.Columns("VALOR").Value) > CInt(vMaxLength) Then
            sdbgCampos.Columns("VALOR").Value = Left(sdbgCampos.Columns("VALOR").Value, CInt(vMaxLength))
        End If
    End If

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Muestra las l�neas del desglose de material
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgCampos_DblClick()
    'Muestra las l�neas del desglose de material
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgCampos.col < 0 Then Exit Sub
    If sdbgCampos.Columns(sdbgCampos.col).Name = "VALOR" Then
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or (sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.DesgloseActividad) Then
            'Valores del desglose:
            Set g_ofrmDesgloseValores = ObtenerFormularioDesglose(CStr(sdbgCampos.Columns("ID").Value))
            If Not m_bModificar Then
                g_ofrmDesgloseValores.g_bModif = False
            Else
                g_ofrmDesgloseValores.g_bModif = m_bModifUltimaVersion
            End If
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.OrganizacionCompras Then
                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                g_ofrmDesgloseValores.g_sCodCentro = "-1"
            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Centro Then
                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-2))
                g_ofrmDesgloseValores.g_sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
            Else
                g_ofrmDesgloseValores.g_sCodOrganizacionCompras = "-1"
            End If
            Set g_ofrmDesgloseValores.g_oCampoDesglose = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
            Set g_ofrmDesgloseValores.g_oInstanciaSeleccionada = g_oSolicitudSeleccionada
            g_ofrmDesgloseValores.g_sOrigen = g_sOrigen
            Set g_ofrmDesgloseValores.g_oCumplimentacionesGS = m_oCumplimentacionesGS
                        
            g_ofrmDesgloseValores.WindowState = vbNormal
            g_ofrmDesgloseValores.Show vbModal
        End If
    End If
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgCampos_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim arrMat As Variant
    Dim lRowInit As Long
    Dim i As Integer
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   

    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgCampos
        If .Columns("VALOR").Value = "" Then Exit Sub
        If .col < 0 Then Exit Sub
        If .Columns(.col).Name <> "VALOR" Then Exit Sub
        If .Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Then Exit Sub
        If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoArchivo Then Exit Sub
        If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoEditor Then Exit Sub
        If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then Exit Sub
        If .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then Exit Sub
        If .Columns("CAMPO_GS").Value = TipoCampoGS.Moneda Or .Columns("CAMPO_GS").Value = TipoCampoGS.AnyoImputacion Then Exit Sub
        m_bUpdate = True
        If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
            If (.Columns("VALOR").Locked And .Columns("CAMPO_GS").Value = TipoCampoGS.CodComprador) Or _
                (.Columns("VALOR").Locked And .Columns("TIPO").Value = TipoCampoPredefinido.Atributo And .Columns("INTRO").Value = TAtributoIntroduccion.IntroListaExterna) Then
                Exit Sub
            End If
            If .Columns("VALOR").Locked And m_bModificar = True And m_bModifUltimaVersion And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef) Then
                If .Columns("CAMPO_GS").Value = TipoCampoGS.material Then
                    arrMat = DevolverMaterial(.Columns("COD_VALOR").Value)
                End If
                .Columns("VALOR").Value = ""
                .Columns("COD_VALOR").Value = ""
                
                'Si hemos quitado el valor del material quitamos tambi�n el del art�culo
                If .Columns("CAMPO_GS").Value = TipoCampoGS.material Then
                    .MoveNext
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                        .Columns("COD_VALOR").Value = ""
                        .Columns("VALOR").Value = ""
                        .Update
                    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
                        Dim oArticulos As CArticulos
                        Set oArticulos = oFSGSRaiz.Generar_CArticulos
                        oArticulos.CargarTodosLosArticulos .Columns("COD_VALOR").Value, , True, , , , arrMat(1), arrMat(2), arrMat(3), arrMat(4)
    
                        If oArticulos.Count > 0 Then
                            .Columns("COD_VALOR").Value = ""
                            .Columns("VALOR").Value = ""
                            .Update
                            
                            .MoveNext
                            If .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                                .Columns("COD_VALOR").Value = ""
                                .Columns("VALOR").Value = ""
                                .Update
                                .MovePrevious
                            End If
                            
                        End If
                        Set oArticulos = Nothing
                    End If
                    .MovePrevious
                    
                    'Si es un campo material con listas hijas y elimino el valor elimino el valor de las listas hijas
                    LimpiarValorListaHijaMaterial CampoSeleccionado.IdCampoDef
                End If
                
                If .Columns("CAMPO_GS").Value = TipoCampoGS.UnidadOrganizativa Then
                    .MoveNext
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.Departamento Then
                        .Columns("COD_VALOR").Value = ""
                        .Columns("VALOR").Value = ""
                    End If
                    .MovePrevious
                    .Update
                ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras Then
                     .MoveNext
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
                        .Columns("COD_VALOR").Value = ""
                        .Columns("VALOR").Value = ""
                        .MoveNext
                        If .Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
                            .Columns("COD_VALOR").Value = ""
                            .Columns("VALOR").Value = ""
                        End If
                        .MovePrevious
                        
                        'Borrar el articulo
                        If .Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
                            lRowInit = .Row
                            For i = lRowInit To .Rows - 1
                                .Row = i
                                If .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                                    .Columns("VALOR").Value = ""
                                    .Columns("COD_VALOR").Value = ""
                                    .Row = i + 1
                                    If .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                                        .Columns("VALOR").Value = ""
                                        .Columns("COD_VALOR").Value = ""
                                    End If
                                    Exit For
                                End If
                            Next i
                            .Row = lRowInit
                        End If
                    ElseIf (.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose) Or (.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose) Then
                        g_oGrupoSeleccionado.CAMPOS.Item(CStr(.Columns("ID").Value)).EliminarArticulosEnDesglose
                    End If
                    .MovePrevious
                    .Update
                
                ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    .MoveNext
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
                        .Columns("COD_VALOR").Value = ""
                        .Columns("VALOR").Value = ""
                    End If
                                    
                    'Borrar el articulo
                    If .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras Then
                        
                        If .Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
                            g_oGrupoSeleccionado.CAMPOS.Item(CStr(.Columns("ID").Value)).EliminarArticulosEnDesglose
                        
                        Else
                            lRowInit = .Row
                            For i = lRowInit To .Rows - 1
                                .Row = i
                                If .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                                    .Columns("VALOR").Value = ""
                                    .Columns("COD_VALOR").Value = ""
                                    .Row = i + 1
                                    If .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                                        .Columns("VALOR").Value = ""
                                        .Columns("COD_VALOR").Value = ""
                                    End If
                                    Exit For
                                End If
                            Next i
                            .Row = lRowInit
                        End If
                    End If
                    .MovePrevious
                        
                ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                        'Si hemos quitado el valor de la Denominacion quitar tambien el codigo
                            .MovePrevious
                            If .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
                                .Columns("COD_VALOR").Value = ""
                                .Columns("VALOR").Value = ""
                            End If
                            .MoveNext
                End If
        
                '------------------------------------------------------------------------------------------------------------------
                
                'Si hemos quitado el valor del material quitamos tambi�n el del art�culo
                'y si habia una campo tabla con articulo relacionada tb quitamos ese valor
                'AXIOMA: solo puede existir un articulo por formulario
                'ha desaparecido el articulo --> quitamos el valor para la columna de la tabla externa
                QuitarValorTablaExterna
                m_blnExisteTablaExternaParaArticulo = False
                m_blnExisteArticulo = False
                
                '------------------------------------------------------------------------------------------------------------------
        
                'Si hemos quitado el valor del pa�s quitamos tambi�n el de la provincia
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Pais Then
                    .MoveNext
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.provincia Then
                        .Columns("COD_VALOR").Value = ""
                        .Columns("VALOR").Value = ""
                        .Update
                    End If
                    .MovePrevious
                End If
                
                'Si hemos quitado el valor del proveedor y existe el campoGs Proveedor ERP eliminaremos su valor
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Proveedor Then
                    AccionSobreCampoGsProveedorERP (AccionSobreCampoProveedorERP.EliminarValor)
                End If
                
            End If
        End If
    End With

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
Dim vMaxLength As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyBack Then Exit Sub
    
    'Control para que los tipos TipoTextoCorto, TipoTextoMedio y TipoTextoLargo no superen el m�ximo permitido
    If (sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.DenArticulo) Then
        If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Then
            vMaxLength = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength
            If IsNull(vMaxLength) Or vMaxLength = "" Then
                If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Then
                    vMaxLength = 100
                ElseIf sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Then
                    vMaxLength = 800
                Else
                    vMaxLength = -1
                End If
            End If
            If CInt(vMaxLength) > 0 And (Len(sdbgCampos.Columns("VALOR").Value) >= CInt(vMaxLength)) And sdbgCampos.ActiveCell.SelLength = 0 Then
                KeyAscii = 0
            End If
        End If
    End If
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
        If Len(sdbgCampos.Columns("VALOR").Value) + 1 > basParametros.gLongitudesDeCodigos.giLongCodDENART And sdbgCampos.ActiveCell.SelLength = 0 Then
            KeyAscii = 0
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   

    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgCampos
        If .col < 0 Then Exit Sub
    
        .Columns("VALOR").DropDownHwnd = 0
        
        If m_bModificar And m_bModifUltimaVersion And EscrituraParaDetalleSolicGS(CampoSeleccionado.IdCampoDef) Then
            .Columns("VALOR").Locked = False
            
            If .Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                .Columns(1).Locked = False
                .Columns(1).Style = ssStyleEditButton
                .Columns("VALOR").Locked = True
                .Columns("VALOR").Style = ssStyleEditButton
            ElseIf .Columns("TIPO").Value = TipoCampoPredefinido.Servicio Then
                .Columns("VALOR").Locked = True
                .Columns("VALOR").Style = ssStyleEditButton
            ElseIf .Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then    'Es un campo de GS
                Select Case .Columns("CAMPO_GS").Value
                    Case TipoCampoSC.ArchivoEspecific
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                    
                    Case TipoCampoSC.DescrBreve, TipoCampoSC.Cantidad, TipoCampoSC.importe, TipoCampoSC.PrecioUnitario
                        .Columns("VALOR").Style = ssStyleEdit
                        .Columns("VALOR").Locked = False
        
                    Case TipoCampoSC.DescrDetallada, TipoCampoGS.Proveedor, TipoCampoGS.Desglose, TipoCampoGS.CampoPersona
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoGS.Dest
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddDestinos.hWnd
                        .Columns("VALOR").Locked = True
                    Case TipoCampoGS.ProveedorERP
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddOtrosCombos.hWnd
                        .Columns("VALOR").Locked = True
                    Case TipoCampoGS.FormaPago
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddPagos.hWnd
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro, TipoCampoGS.InicioAbono, TipoCampoGS.FinAbono
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoGS.Unidad, TipoCampoGS.Pais, TipoCampoGS.provincia
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddOtrosCombos.hWnd
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoGS.material
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoGS.CodArticulo
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddArticulos.hWnd
                        .Columns("VALOR").Locked = False  'deja modificar sin seleccionar del combo para introducir art�culos no codificados
                        
                    Case TipoCampoGS.NuevoCodArticulo
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddArticulos.hWnd
                        .Columns("VALOR").Locked = Not (g_oGrupoSeleccionado.CAMPOS.Item(CStr(.Columns("ID").Value)).AnyadirArt) 'deja modificar sin seleccionar del combo para introducir art�culos no codificados
                        
                    Case TipoCampoGS.DenArticulo
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddArticulos.hWnd
                        .Columns("VALOR").Locked = Not (esGenerico(.Columns("COD_VALOR").CellValue(.GetBookmark(-1)), g_oGrupoSeleccionado.CAMPOS.Item(CStr(.Columns("ID").CellValue(.GetBookmark(-1)))).AnyadirArt))
                        
                    Case TipoCampoGS.UnidadOrganizativa
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoGS.Departamento
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddOtrosCombos.hWnd
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoGS.OrganizacionCompras, TipoCampoGS.Centro
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddOtrosCombos.hWnd
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoGS.Almacen
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddAlmacenes.hWnd
                        .Columns("VALOR").Locked = True
                    
                    Case TipoCampoGS.Empresa
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddEmpresas.hWnd
                        .Columns("VALOR").Locked = True
                    
                    Case TipoCampoGS.RefSolicitud
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                    
                    Case TipoCampoGS.ImporteSolicitudesVinculadas, TipoCampoGS.Activo, TipoCampoGS.CentroCoste, TipoCampoGS.Factura, TipoCampoGS.AnyoImputacion
                        .Columns("VALOR").Locked = True
                        
                    Case TipoCampoSC.PrecioUnitarioAdj, TipoCampoSC.ProveedorAdj, TipoCampoSC.CantidadAdj, TipoCampoGS.DesgloseActividad, TipoCampoGS.Moneda
                        .Columns("VALOR").Style = ssStyleEdit
                        .Columns("VALOR").Locked = True
                    Case TipoCampoGS.CodComprador
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                End Select
        
            ElseIf .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                'Si es un campo calculado no se podr� modificar su valor:
                .Columns("VALOR").Locked = True
    
            Else   'Es un atributo o campo normal:
                If .Columns("INTRO").Value = TAtributoIntroduccion.IntroLibre Then
                    Select Case .Columns("SUBTIPO").Value
                        Case TiposDeAtributos.TipoFecha
                            .Columns("VALOR").Style = ssStyleEditButton
                            .Columns("VALOR").Locked = False
                            
                        Case TiposDeAtributos.TipoBoolean
                            .Columns("VALOR").Style = ssStyleComboBox
                            sdbddValor.RemoveAll
                            sdbddValor.AddItem ""
                            sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                            .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                            .Columns("VALOR").Locked = False
                            
                        Case TiposDeAtributos.TipoArchivo, TiposDeAtributos.TipoEditor
                            .Columns("VALOR").Style = ssStyleEditButton
                            .Columns("VALOR").Locked = True
                            
                        Case TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                            .Columns("VALOR").Style = ssStyleEditButton
                            .Columns("VALOR").Locked = False
                            
                        Case TiposDeAtributos.TipoDesglose
                            .Columns("VALOR").Style = ssStyleEditButton
                            .Columns("VALOR").Locked = True
                        
                        Case Else
                            .Columns("VALOR").Style = ssStyleEdit
                            .Columns("VALOR").Locked = False
                    End Select
                ElseIf .Columns("INTRO").Value = TAtributoIntroduccion.Introselec Then
                    'Selecci�n mediante una lista
                    .Columns("VALOR").Style = ssStyleComboBox
                    sdbddValor.RemoveAll
                    sdbddValor.AddItem ""
                    If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoNumerico Then
                        sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
                    Else
                        sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                    End If
                    .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                    .Columns("VALOR").Locked = True
                Else
                    'Lista externa
                    .Columns("VALOR").Style = ssStyleEdit
                    .Columns("VALOR").Locked = True
                End If
            End If
        Else
            'si no hay permisos de modificaci�n solo podr� ver los botones de archivos y descripci�n detallada:
            .Columns("VALOR").Locked = True
            If .Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                .Columns("VALOR").Style = ssStyleEdit
            ElseIf .Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
                If .Columns("CAMPO_GS").Value = TipoCampoSC.ArchivoEspecific Or .Columns("CAMPO_GS").Value = TipoCampoSC.DescrDetallada Or .Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Then
                    .Columns("VALOR").Style = ssStyleEditButton
                Else
                    .Columns("VALOR").Style = ssStyleEdit
                End If
            Else
                If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoArchivo Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoEditor Then
                    .Columns("VALOR").Style = ssStyleEditButton
                Else
                    .Columns("VALOR").Style = ssStyleEdit
                End If
            End If
        End If
    End With
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.externo Then
    'aqui iria CellStyleSet
Else
    If sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgCampos.Columns("CAMPO").CellStyleSet "Amarillo"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        sdbgCampos.Columns("CAMPO").CellStyleSet "Calculado"
    Else
        sdbgCampos.Columns("CAMPO").CellStyleSet ""
    End If
    
    If sdbgCampos.Columns("BAJALOGICA").Value Then
        sdbgCampos.Columns("VALOR").CellStyleSet "BajaLogica"
        Exit Sub
    Else
        sdbgCampos.Columns("VALOR").CellStyleSet ""
    End If
    
    If sdbgCampos.Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.Desglose Or sdbgCampos.Columns("SUBTIPO").CellValue(Bookmark) = TiposDeAtributos.TipoDesglose Then
        sdbgCampos.Columns("VALOR").CellStyleSet "Gris"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        If g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Sucio = True Then
            sdbgCampos.Columns("VALOR").CellStyleSet "CalculadoSucio"
        Else
            sdbgCampos.Columns("VALOR").CellStyleSet "CalculadoValor"
        End If
    ElseIf Not g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)) Is Nothing Then
        If g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).Sucio = True Then
            sdbgCampos.Columns("VALOR").CellStyleSet "Sucio"
        Else
            sdbgCampos.Columns("VALOR").CellStyleSet ""
        End If
    Else
        sdbgCampos.Columns("VALOR").CellStyleSet ""
    End If
    If sdbgCampos.Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.material Then
        Select Case sdbgCampos.Columns("RIESGO").Value
            Case 1
                sdbgCampos.Columns("VALOR").CellStyleSet "RiesgoAlto"
            Case 2
                sdbgCampos.Columns("VALOR").CellStyleSet "RiesgoMedio"
            Case 3
                sdbgCampos.Columns("VALOR").CellStyleSet "RiesgoBajo"
'            Case Else
'                sdbgCampos.Columns("VALOR").CellStyleSet ""
        End Select
    End If
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbgCampos_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub SSTabGrupos_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgCampos.Update

    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    GrupoSeleccionado
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "SSTabGrupos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub GrupoSeleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ssTabGrupos.selectedItem.Tag <> "" Then
        Set g_oGrupoSeleccionado = g_oSolicitudSeleccionada.Grupos.Item(CStr(ssTabGrupos.selectedItem.Tag))
    End If

    If g_oGrupoSeleccionado Is Nothing Then Exit Sub

    If g_oGrupoSeleccionado.CAMPOS Is Nothing Then
        g_oGrupoSeleccionado.CargarTodosLosCampos
    End If
    
    If m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CAMPOS Is Nothing Then
        m_oGruposBD.Item(CStr(g_oGrupoSeleccionado.Id)).CargarTodosLosCampos
    End If

    sdbgCampos.RemoveAll
    CargarGridCampos
    sdbgCampos.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "GrupoSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Carga la grid de solicitudes de compra
''' </summary>
''' <remarks>Llamada desde: GrupoSeleccionado ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridCampos()
Dim sValor As String
Dim oCampo As CFormItem
Dim sCadena As String
Dim sCodValor As String
Dim oProveedores As CProveedores
Dim oDestinos As CDestinos
Dim oPagos As CPagos

Dim oPais As CPais
Dim oPaises As CPaises
Dim sPaisAnterior As String
Dim oArticulos As CArticulos
Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN1s As CGruposMatNivel1
Dim oGMN4 As CGrupoMatNivel4
Dim sMatAnterior As String
Dim arrMat As Variant
Dim oUnidades As CUnidades
Dim oMonedas As CMonedas
Dim oPersona As CPersona
Dim bGenerico As Boolean
Dim sCodIdioma As String
Dim sValorCompleto As String
Dim textDen() As String
Dim textValor() As String
Dim aDen() As String
Dim i As Integer
Dim iRiesgo As Integer
Dim strPrimerCampoNoPK As String
Dim intTabla As Integer
Dim strNombreTabla As String, strPK As String
Dim Texto As String
Dim TextoDen As String

    Dim iNumCampo As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iNumCampo = 0
    
    For Each oCampo In g_oGrupoSeleccionado.CAMPOS
        iRiesgo = 0
        sValor = Chr(m_lSeparador) & ""
        If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
            '----------------------------------------------------------------------------------------------------
            sCodValor = Chr(m_lSeparador) & ""
            intTabla = CInt(NullToStr(oCampo.TablaExterna))
            strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
            strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(intTabla)
            strPK = m_oTablaExterna.SacarPK(intTabla)
            Select Case oCampo.Tipo
                Case TipoFecha
                    If IsNull(oCampo.valorFec) Then
                        sValor = Chr(m_lSeparador) 'sValor = chr(m_lSeparador) & oCampo.ValorFec
                    Else
                        sValor = Chr(m_lSeparador) & Format(oCampo.valorFec, "Short date") & " " & m_oTablaExterna.ValorCampo(oCampo.valorFec, strPK, strPrimerCampoNoPK, strNombreTabla, oCampo.Tipo)
                    End If
                    
                Case TipoNumerico
                    If IsNull(oCampo.valorNum) Then
                        sValor = Chr(m_lSeparador) 'sValor = chr(m_lSeparador) & oCampo.ValorNum
                    Else
                        'sValor = chr(m_lSeparador) & Format(oCampo.ValorNum, "Standard") & " " & m_oTablaExterna.ValorCampo(oCampo.ValorNum, strPK, strPrimerCampoNoPK, strNombreTabla, oCampo.Tipo)
                        sValor = Chr(m_lSeparador) & CStr(oCampo.valorNum) & " " & m_oTablaExterna.ValorCampo(oCampo.valorNum, strPK, strPrimerCampoNoPK, strNombreTabla, oCampo.Tipo)
                    End If
                    
                Case TipoTextoMedio
                    If IsNull(oCampo.valorText) Then
                        sValor = Chr(m_lSeparador)     'sValor = chr(m_lSeparador) & oCampo.ValorText
                    Else
                        sValor = Chr(m_lSeparador) & oCampo.valorText & " " & m_oTablaExterna.ValorCampo(oCampo.valorText, strPK, strPrimerCampoNoPK, strNombreTabla, oCampo.Tipo)
                    End If
            End Select
            '----------------------------------------------------------------------------------------------------
        ElseIf oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                'Campos predefinidos de GS:
                sCodValor = Chr(m_lSeparador) & ""
                
                Select Case oCampo.CampoGS
                    Case TipoCampoSC.ArchivoEspecific
                        oCampo.CargarAdjuntos
                        sValor = Chr(m_lSeparador) & LiteralAdjuntos(oCampo)
                        
                    Case TipoCampoSC.Cantidad, TipoCampoSC.importe, TipoCampoSC.PrecioUnitario, TipoCampoSC.PrecioUnitarioAdj, TipoCampoSC.CantidadAdj, TipoCampoGS.RetencionEnGarantia
                        If IsNull(oCampo.valorNum) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorNum
                        Else
                            sValor = Chr(m_lSeparador) & Format(oCampo.valorNum, "Standard")
                        End If
                        
                    Case TipoCampoSC.DescrBreve, TipoCampoSC.DescrDetallada, TipoCampoGS.Factura
                        sValor = Chr(m_lSeparador) & EliminarTab(NullToStr(oCampo.valorText))
                    
                    Case TipoCampoGS.Desglose
                        sValor = Chr(m_lSeparador) & m_sIdiDesglose
                    
                    Case TipoCampoGS.Dest
                        If Not IsNull(oCampo.valorText) Then
                            Set oDestinos = oFSGSRaiz.Generar_CDestinos
                            oDestinos.CargarTodosLosDestinosDesde 1, oCampo.valorText, , True
                            If Not oDestinos.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oDestinos.Item(1).Cod & " - " & oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oDestinos = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.FormaPago
                        If Not IsNull(oCampo.valorText) Then
                            Set oPagos = oFSGSRaiz.generar_CPagos
                            oPagos.CargarTodosLosPagos oCampo.valorText, , True
                            If Not oPagos.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oPagos.Item(1).Cod & " - " & oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oPagos = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.Proveedor, TipoCampoSC.ProveedorAdj
                        If Not IsNull(oCampo.valorText) Then
                            Set oProveedores = oFSGSRaiz.generar_CProveedores
                            oProveedores.CargarTodosLosProveedoresDesde3 1, oCampo.valorText, , True
                            If Not oProveedores.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oProveedores.Item(1).Cod & " - " & oProveedores.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oProveedores = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro, TipoCampoGS.InicioAbono, TipoCampoGS.FinAbono
                        If IsNull(oCampo.valorFec) Then
                            sValor = Chr(m_lSeparador) & ""
                        Else
                            sValor = Chr(m_lSeparador) & Format(oCampo.valorFec, "Short Date")
                        End If
    
                    Case TipoCampoGS.Unidad
                        If Not IsNull(oCampo.valorText) Then
                            Set oUnidades = oFSGSRaiz.Generar_CUnidades
                            oUnidades.CargarTodasLasUnidades oCampo.valorText, , True
                            If Not oUnidades.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oUnidades.Item(1).Cod & " - " & oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oUnidades = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.Pais
                        If Not IsNull(oCampo.valorText) Then
                            Set oPaises = oFSGSRaiz.Generar_CPaises
                            oPaises.CargarTodosLosPaises oCampo.valorText, , True
                            If Not oPaises.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oPaises.Item(1).Cod & " - " & oPaises.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oPaises = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        sPaisAnterior = NullToStr(oCampo.valorText)
                        
                    Case TipoCampoGS.provincia
                        If sPaisAnterior <> "" Then
                            If Not IsNull(oCampo.valorText) Then
                                Set oPais = oFSGSRaiz.generar_CPais
                                oPais.Cod = sPaisAnterior
                                oPais.CargarTodasLasProvincias oCampo.valorText, , True
                                If Not oPais.Provincias.Item(1) Is Nothing Then
                                    sValor = Chr(m_lSeparador) & oPais.Provincias.Item(1).Cod & " - " & oPais.Provincias.Item(1).Den
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                Else
                                    sValor = Chr(m_lSeparador) & ""
                                    sCodValor = Chr(m_lSeparador) & ""
                                End If
                                Set oPais = Nothing
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.Moneda
                        If Not IsNull(oCampo.valorText) Then
                            Set oMonedas = oFSGSRaiz.Generar_CMonedas
                            oMonedas.CargarTodasLasMonedas oCampo.valorText, , True
                            If Not oMonedas.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oMonedas.Item(1).Cod & " - " & oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oMonedas = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.material
                        If Not IsNull(oCampo.valorText) Then
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            
                            arrMat = DevolverMaterial(oCampo.valorText)
                            
                            If arrMat(4) <> "" Then
                                'Sacamos el riesgo
                                Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
                                oGMN4.GMN1Cod = arrMat(1)
                                oGMN4.GMN2Cod = arrMat(2)
                                oGMN4.GMN3Cod = arrMat(3)
                                oGMN4.Cod = arrMat(4)
                                iRiesgo = oGMN4.DevolverRiesgo
                                Set oGMN4 = Nothing
                                Set oGMN3 = oFSGSRaiz.generar_CGrupoMatNivel3
                                oGMN3.GMN1Cod = arrMat(1)
                                oGMN3.GMN2Cod = arrMat(2)
                                oGMN3.Cod = arrMat(3)
                                oGMN3.DevolverTodosLosGruposMatDesde 1, arrMat(4), , True
                                sValor = Chr(m_lSeparador) & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) & " - " & oGMN3.GruposMatNivel4.Item(1).Den
                                Set oGMN3 = Nothing
                            ElseIf arrMat(3) <> "" Then
                                Set oGMN2 = oFSGSRaiz.generar_CGrupoMatNivel2
                                oGMN2.GMN1Cod = arrMat(1)
                                oGMN2.Cod = arrMat(2)
                                oGMN2.DevolverTodosLosGruposMatDesde 1, arrMat(3), , True
                                sValor = Chr(m_lSeparador) & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & oGMN2.GruposMatNivel3.Item(1).Den
                                Set oGMN2 = Nothing
                            ElseIf arrMat(2) <> "" Then
                                Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
                                oGMN1.Cod = arrMat(1)
                                oGMN1.DevolverTodosLosGruposMatDesde 1, arrMat(2), , True
                                sValor = Chr(m_lSeparador) & arrMat(1) & " - " & arrMat(2) & " - " & oGMN1.GruposMatNivel2.Item(1).Den
                                Set oGMN1 = Nothing
                            ElseIf arrMat(1) <> "" Then
                                Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
                                oGMN1s.DevolverTodosLosGruposMatDesde arrMat(1), , , True
                                sValor = Chr(m_lSeparador) & arrMat(1) & " - " & oGMN1s.Item(1).Den
                                Set oGMN1s = Nothing
                            End If
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        sMatAnterior = NullToStr(oCampo.valorText)
                    Case TipoCampoGS.TipoPedido
                        If Not IsNull(oCampo.valorText) Then
                            sCodIdioma = gParametrosInstalacion.gIdioma
                            textDen = Split(oCampo.ValorTextDen, "#")
                
                           
                            Texto = oCampo.valorText
                            For i = 0 To UBound(textDen)
                                aDen = Split(textDen(i), "|")
                                If UBound(aDen) > 0 Then
                                    If aDen(0) = sCodIdioma Then
                                        If Len(aDen(1)) > 0 Then
                                            Texto = oCampo.valorText & " - " & aDen(1)
                                        End If
                                        Exit For
                                    End If
                                End If
                            Next
                                                
                            sValor = Chr(m_lSeparador) & Texto
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.ProveedorERP
                        If Not IsNull(oCampo.valorText) Then
                            If NullToStr(oCampo.ValorTextDen) <> "" Then
                                'Si ya tiene la denominacion no la buscaremos
                                TextoDen = oCampo.ValorTextDen
                            Else
                                'Si no tenemos la denominacion la buscaremos
                                
                                Dim sOrgCompras As String
                                'Miramos el valor de organizacion de compras a nivel del formulario
                                sOrgCompras = ComprobarOrgCompras(True)
                                
                                Dim oCampoProve As CFormItem
                                Dim sCodProveedor As String
                                For Each oCampoProve In g_oGrupoSeleccionado.CAMPOS
                                    Select Case oCampoProve.CampoGS
                                        Case TipoCampoGS.Proveedor
                                            sCodProveedor = NullToStr(oCampoProve.valorText)
                                            Exit For
                                    End Select
                                Next
                                
                                If sCodProveedor <> "" And sOrgCompras <> "" Then
                                    Dim oProveERPs As CProveERPs
                                    Set oProveERPs = oFSGSRaiz.Generar_CProveERPs
                                    Dim oProveERP As CProveERP
                                    oProveERPs.CargarProveedoresERP , sCodProveedor, NullToStr(oCampo.valorText), , sOrgCompras, True
                                    For Each oProveERP In oProveERPs
                                        TextoDen = oProveERP.Den
                                        Exit For
                                    Next
                                    Set oProveERP = Nothing
                                    Set oProveERPs = Nothing
                                End If
                            End If
                            
                            
                            Texto = oCampo.valorText
                            Texto = oCampo.valorText & " - " & TextoDen
                            sValor = Chr(m_lSeparador) & Texto
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.CodArticulo
                        If sMatAnterior <> "" Then
                            If Not IsNull(oCampo.valorText) Then
                                arrMat = DevolverMaterial(sMatAnterior)
                
                                Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
                                oGMN4.GMN1Cod = arrMat(1)
                                oGMN4.GMN2Cod = arrMat(2)
                                oGMN4.GMN3Cod = arrMat(3)
                                oGMN4.Cod = arrMat(4)
                                oGMN4.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, oCampo.valorText, , True
                                Set oArticulos = oGMN4.ARTICULOS
                                Set oGMN4 = Nothing
                            
                                If Not oArticulos.Item(1) Is Nothing Then
                                    sValor = Chr(m_lSeparador) & oArticulos.Item(1).Cod & " - " & oArticulos.Item(1).Den
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                Else
                                    sValor = Chr(m_lSeparador) & oCampo.valorText
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                End If
                                Set oArticulos = Nothing
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.NuevoCodArticulo
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            bGenerico = esGenerico(oCampo.valorText)
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.DenArticulo
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                        If Not IsNull(oCampo.valorText) Then
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Select Case oCampo.CampoGS
                                Case TipoCampoGS.Pres1
                                    sValor = Chr(m_lSeparador) & GenerarDenominacionPresupuesto(oCampo.valorText, 1, m_bPresBajaLog)
                                
                                Case TipoCampoGS.Pres2
                                    sValor = Chr(m_lSeparador) & GenerarDenominacionPresupuesto(oCampo.valorText, 2, m_bPresBajaLog)
                                
                                Case TipoCampoGS.Pres3
                                    sValor = Chr(m_lSeparador) & GenerarDenominacionPresupuesto(oCampo.valorText, 3, m_bPresBajaLog)
                                
                                Case TipoCampoGS.Pres4
                                    sValor = Chr(m_lSeparador) & GenerarDenominacionPresupuesto(oCampo.valorText, 4, m_bPresBajaLog)
    
                            End Select
                            sCodValor = sCodValor & Chr(m_lSeparador) & BooleanToSQLBinary(m_bPresBajaLog)
                            m_bPresBajaLog = False
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.CampoPersona
                        If Not IsNull(oCampo.valorText) Then
                            Set oPersona = oFSGSRaiz.Generar_CPersona
                            oPersona.Cod = oCampo.valorText
                            oPersona.CargarTodosLosDatos
                            sValor = Chr(m_lSeparador) & oPersona.nombre & " " & oPersona.Apellidos & " (" & oPersona.mail & ")"
                            sCodValor = Chr(m_lSeparador) & oPersona.Cod
                            Set oPersona = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.CodComprador
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.ValorTextDen
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.UnidadOrganizativa
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            If PonerEstiloUON(oCampo.valorText) Then
                                sCodValor = sCodValor & Chr(m_lSeparador) & "1"
                            End If
                            
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.Departamento
                        If Not IsNull(oCampo.valorText) Then
                        
                            Dim vbm As Variant
                            vbm = sdbgCampos.GetBookmark(iNumCampo - 1)
    
                            Dim oDepartamentos As CDepartamentos
                            Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
                            
                            oDepartamentos.CargarTodosLosDepartamentos oCampo.valorText, , True
                                                    
                            If Not oDepartamentos.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oDepartamentos.Item(1).Cod & " - " & oDepartamentos.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & oCampo.valorText
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            End If
                            
                            If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.UnidadOrganizativa) Then
                                If (sdbgCampos.Columns("BAJALOGICA").CellValue(vbm)) Then
                                    sCodValor = sCodValor & Chr(m_lSeparador) & "1"
                                End If
                            Else
                                If (PonerEstiloDep(oCampo.valorText)) Then
                                    sCodValor = sCodValor & Chr(m_lSeparador) & "1"
                                End If
                                
                            End If
    
                            Set oDepartamentos = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    
                    Case TipoCampoGS.OrganizacionCompras
                        If Not IsNull(oCampo.valorText) Then
                            Dim oOrganizacionesCompras As COrganizacionesCompras
                            Set oOrganizacionesCompras = oFSGSRaiz.Generar_COrganizacionesCompras
                            
                            oOrganizacionesCompras.CargarOrganizacionesCompra oCampo.valorText
                            If Not oOrganizacionesCompras.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oOrganizacionesCompras.Item(1).Cod & " - " & oOrganizacionesCompras.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                         Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        Set oOrganizacionesCompras = Nothing
                        
                    Case TipoCampoGS.Centro
                        If Not IsNull(oCampo.valorText) Then
                            Dim oCentros As CCentros
                            Set oCentros = oFSGSRaiz.Generar_CCentros
                            
                            oCentros.CargarTodosLosCentros oCampo.valorText
                            If Not oCentros.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oCentros.Item(1).Cod & " - " & oCentros.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                         Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        Set oCentros = Nothing
                        
                    Case TipoCampoGS.Almacen
                        If Not IsNull(oCampo.valorNum) Then
                            Dim oAlmacenes As CAlmacenes
                            Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
                            
                            oAlmacenes.CargarTodosLosAlmacenes , , , , oCampo.valorNum
                            If Not oAlmacenes.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oAlmacenes.Item(1).Cod & " - " & oAlmacenes.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorNum
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                         Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        Set oAlmacenes = Nothing
                    Case TipoCampoGS.Empresa
                        If Not IsNull(oCampo.valorNum) Then
                            Dim oEmpresas As CEmpresas
                            Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
                            
                            oEmpresas.CargarTodasLasEmpresasDesde lID:=oCampo.valorNum
                            If oEmpresas.Count = 0 Then
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            Else
                                sValor = Chr(m_lSeparador) & oEmpresas.Item(1).NIF & " - " & oEmpresas.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorNum
                            End If
                         Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        Set oEmpresas = Nothing
                    Case TipoCampoGS.NumSolicitERP
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                     
                     Case TipoCampoGS.RefSolicitud
                        Dim sTitulo As String
                        If Not IsNull(oCampo.valorNum) Then
                            sTitulo = oCampo.DevolverTitulo(oCampo.valorNum)
                            If sTitulo <> "" Then
                                sValor = Chr(m_lSeparador) & oCampo.valorNum & " - " & sTitulo
                            Else
                                sValor = Chr(m_lSeparador) & oCampo.valorNum
                            End If
                            sCodValor = Chr(m_lSeparador) & oCampo.valorNum
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.ImporteSolicitudesVinculadas
                        If ((g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Pendiente) Or (g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Aprobada) Or (g_oSolicitudSeleccionada.Estado = EstadoSolicitud.Cerrada)) Then
                            sValor = Chr(m_lSeparador) & Format(oCampo.DevolverImporteSOLVinculadas(g_oSolicitudSeleccionada.Id, g_oSolicitudSeleccionada.Moneda), "Standard")
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    
                    Case TipoCampoGS.Activo
                        If Not IsNull(oCampo.valorText) Then
                            sCodIdioma = gParametrosInstalacion.gIdioma
                            textDen = Split(oCampo.ValorTextDen, "#")
                            
                            Texto = oCampo.valorText
                            For i = 0 To UBound(textDen)
                                aDen = Split(textDen(i), "|")
                                If UBound(aDen) > 0 Then
                                    If aDen(0) = sCodIdioma Then
                                        If Len(aDen(1)) > 0 Then
                                            Texto = oCampo.valorText & " - " & aDen(1)
                                        End If
                                        Exit For
                                    End If
                                End If
                            Next
                                                
                            sValor = Chr(m_lSeparador) & Texto
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.CentroCoste
                        If Not IsNull(oCampo.valorText) Then
                                                   
                            sCodIdioma = gParametrosInstalacion.gIdioma

                            textDen = Split(oCampo.ValorTextDen, "#")
                            textValor = Split(oCampo.valorText, "#")
                            If UBound(textValor) + 1 > 0 Then
                                sValorCompleto = textValor(UBound(textValor))
                            End If

                            For i = 0 To UBound(textDen)
                                aDen = Split(textDen(i), "|")
                                If UBound(aDen) > 0 Then
                                    If aDen(0) = sCodIdioma Then
                                        If Len(aDen(1)) > 0 Then
                                            sValorCompleto = sValorCompleto & " - " & aDen(1)
                                        End If
                                        Exit For
                                    End If
                                End If
                            Next
                
                            If UBound(textValor) > 1 Then
                                sValorCompleto = sValorCompleto & " ("
                                For i = 0 To UBound(textValor) - 1
                                    sValorCompleto = sValorCompleto & textValor(i) & "-"
                                Next
                    
                                sValorCompleto = Left(sValorCompleto, Len(sValorCompleto) - 1) & ")"
                            End If
                                        
                            sValor = Chr(m_lSeparador) & sValorCompleto
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    
                    Case TipoCampoGS.PartidaPresupuestaria
                        If Not IsNull(oCampo.valorText) Then
                            sCodIdioma = gParametrosInstalacion.gIdioma

                            textDen = Split(oCampo.ValorTextDen, "#")
                            textValor = Split(oCampo.valorText, "#")
                            If UBound(textValor) + 1 > 0 Then
                                sValorCompleto = textValor(UBound(textValor))
                                sValorCompleto = Replace(sValorCompleto, "|", "-")
                            End If
        
                            For i = 0 To UBound(textDen)
                                aDen = Split(textDen(i), "|")
                                If UBound(aDen) > 0 Then
                                    If aDen(0) = sCodIdioma Then
                                        If Len(aDen(1)) > 0 Then
                                            sValorCompleto = sValorCompleto & " - " & aDen(1)
                                        End If
                                        Exit For
                                    End If
                                End If
                            Next
                        
                            sValor = Chr(m_lSeparador) & sValorCompleto
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                    Case TipoCampoGS.AnyoImputacion
                        If Not IsNull(oCampo.valorNum) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorNum
                            sCodValor = Chr(m_lSeparador) & oCampo.valorNum
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    
                End Select
            '----------------------------------------------------------------------------------------------------
            ElseIf oCampo.TipoPredef = TipoCampoPredefinido.Calculado Then
                'Campo calculado
                sCodValor = Chr(m_lSeparador) & ""
                If IsNull(oCampo.valorNum) Then
                    sValor = Chr(m_lSeparador) & oCampo.valorNum
                Else
                    sValor = Chr(m_lSeparador) & Format(oCampo.valorNum, "Standard")
                End If
            '----------------------------------------------------------------------------------------------------
            Else
                'Campos normales o de atributos
                sCodValor = Chr(m_lSeparador) & ""
                
                Select Case oCampo.Tipo
                    Case TiposDeAtributos.TipoArchivo
                        oCampo.CargarAdjuntos
                        sValor = Chr(m_lSeparador) & LiteralAdjuntos(oCampo)
                        
                    Case TiposDeAtributos.TipoBoolean
                        Select Case oCampo.valorBool
                            Case 0, False
                                sValor = Chr(m_lSeparador) & m_sIdiFalse
                            Case 1, True
                                sValor = Chr(m_lSeparador) & m_sIdiTrue
                            Case Else
                                sValor = Chr(m_lSeparador) & ""
                        End Select
            
                    Case TiposDeAtributos.TipoFecha
                        If IsNull(oCampo.valorFec) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorFec
                        Else
                            sValor = Chr(m_lSeparador) & Format(oCampo.valorFec, "Short date")
                        End If
                        
                    Case TiposDeAtributos.TipoNumerico
                        If IsNull(oCampo.valorNum) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorNum
                        Else
                            sValor = Chr(m_lSeparador) & Format(oCampo.valorNum, "Standard")
                        End If
                        
                    Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoEditor
                        sValor = Chr(m_lSeparador) & EliminarTab(NullToStr(oCampo.valorText))
                        
                    Case TiposDeAtributos.TipoDesglose
                        sValor = Chr(m_lSeparador) & m_sIdiDesglose
                End Select
            End If
            
        
        sCadena = oCampo.Id
        
        'Denominaci�n del campo en el idioma de la aplicaci�n:
        sCadena = sCadena & Chr(m_lSeparador) & oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
        sCadena = sCadena & sValor & Chr(m_lSeparador) & " " & Chr(m_lSeparador) & oCampo.TipoPredef & Chr(m_lSeparador) & oCampo.Tipo & Chr(m_lSeparador) & oCampo.TipoIntroduccion & Chr(m_lSeparador) & oCampo.CampoGS & sCodValor
        
        sCadena = sCadena & Chr(m_lSeparador) 'Baja Logica
        
        sCadena = sCadena & Chr(m_lSeparador) & bGenerico & Chr(m_lSeparador) & iRiesgo
        bGenerico = False
        
        
        If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
            sCadena = sCadena & Chr(m_lSeparador) & CInt(NullToStr(oCampo.TablaExterna))
            '-------------------------------------------------------------------------------------
            intTabla = CInt(NullToStr(oCampo.TablaExterna))
            strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
            strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(intTabla)
            strPK = m_oTablaExterna.SacarPK(intTabla)
            Select Case oCampo.Tipo
                Case TipoFecha
                     sValor = Chr(m_lSeparador) & oCampo.valorFec
                Case TipoNumerico
                    sValor = Chr(m_lSeparador) & oCampo.valorNum
                Case TipoTextoMedio
                    sValor = Chr(m_lSeparador) & oCampo.valorText
            End Select
            '-------------------------------------------------------------------------------------
            sCadena = sCadena & sValor
        End If
        
        'a�ade la cadena a la grid:
        sdbgCampos.AddItem sCadena
        iNumCampo = iNumCampo + 1
    Next
    'sdbgCampos.Columns("VALOR").FieldLen = 4000
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "CargarGridCampos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub CargarProveedorConBusqueda()
    Dim oProves As CProveedores
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing

    sdbgCampos.Columns("VALOR").Value = oProves.Item(1).Cod & " - " & oProves.Item(1).Den
    sdbgCampos.Columns("COD_VALOR").Value = oProves.Item(1).Cod
    sdbgCampos.Update
    
    Set oProves = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "CargarProveedorConBusqueda", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function LiteralAdjuntos(ByVal oCampo As CFormItem) As String
Dim sAdjuntos As String
Dim oAdjunto As CCampoAdjunto

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAdjuntos = ""
    For Each oAdjunto In oCampo.Adjuntos
        sAdjuntos = sAdjuntos & oAdjunto.nombre & " (" & TamanyoAdjuntos(oAdjunto.DataSize / 1024) & m_sIdiKB & ");"
    Next
    If sAdjuntos <> "" Then
        sAdjuntos = Mid(sAdjuntos, 1, Len(sAdjuntos) - 1)
    End If

    LiteralAdjuntos = sAdjuntos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "LiteralAdjuntos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Function ModificarAdjuntos(ByVal oCampo As CFormItem)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bModificar = False Then Exit Function
    If Not EscrituraParaDetalleSolicGS(oCampo.IdCampoDef) Then Exit Function
    sdbgCampos.Columns("VALOR").Value = LiteralAdjuntos(oCampo)
    sdbgCampos.Update
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ModificarAdjuntos", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Sub sdbddOtrosCombos_CloseUp()
    Dim bCambiado As Boolean
    Dim lRowInit As Long
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bCambiado = Not (sdbgCampos.Columns("COD_VALOR").Value = sdbddOtrosCombos.Columns("COD").Value)
    sdbgCampos.Columns("COD_VALOR").Value = sdbddOtrosCombos.Columns("COD").Value
    
    If sdbddOtrosCombos.Columns("COD").Value <> "" Or sdbddOtrosCombos.Columns("DEN").Value <> "" Then
        sdbgCampos.Columns("VALOR").Value = sdbddOtrosCombos.Columns("COD").Value & " - " & sdbddOtrosCombos.Columns("DEN").Value
    End If
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras Then
        If bCambiado Then
            sdbgCampos.MoveNext
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                sdbgCampos.MoveNext
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
                    sdbgCampos.Columns("COD_VALOR").Value = ""
                    sdbgCampos.Columns("VALOR").Value = ""
                End If
                sdbgCampos.MovePrevious
            End If
             'Borrar el articulo
            lRowInit = sdbgCampos.Row
            For i = lRowInit To sdbgCampos.Rows - 1
                sdbgCampos.Row = i
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                    sdbgCampos.Columns("VALOR").Value = ""
                    sdbgCampos.Columns("COD_VALOR").Value = ""
                    sdbgCampos.Row = i + 1
                    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                        sdbgCampos.Columns("VALOR").Value = ""
                        sdbgCampos.Columns("COD_VALOR").Value = ""
                    End If
                    Exit For
                End If
            Next i
            sdbgCampos.Row = lRowInit
            sdbgCampos.MovePrevious
            'Quitamos el valor del campoGS ProveedorERP si estuviese en el grupo
            AccionSobreCampoGsProveedorERP AccionSobreCampoProveedorERP.EliminarValor, TipoCampoGS.OrganizacionCompras
        End If
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
        If bCambiado Then
            sdbgCampos.MoveNext
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
            End If
             'Borrar el articulo
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras Then
                lRowInit = sdbgCampos.Row
                For i = lRowInit To sdbgCampos.Rows - 1
                    sdbgCampos.Row = i
                    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                        sdbgCampos.Columns("VALOR").Value = ""
                        sdbgCampos.Columns("COD_VALOR").Value = ""
                        sdbgCampos.Row = i + 1
                        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                            sdbgCampos.Columns("VALOR").Value = ""
                            sdbgCampos.Columns("COD_VALOR").Value = ""
                        End If
                        Exit For
                    End If
                Next i
                sdbgCampos.Row = lRowInit
            End If
            sdbgCampos.MovePrevious
        End If
    End If
      
    sdbgCampos.Update
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais Then
        sdbgCampos.MoveNext
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia Then
            sdbgCampos.Columns("COD_VALOR").Value = ""
            sdbgCampos.Columns("VALOR").Value = ""
        End If
        sdbgCampos.MovePrevious
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddOtrosCombos_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddOtrosCombos_DropDown()
Dim oPais As CPais
Dim oUnidad As CUnidad
Dim oProvi As CProvincia
Dim vbm As Variant
Dim oMonedas As CMonedas
Dim oMoneda As CMoneda
Dim oDepartamentos As CDepartamentos
Dim oDepartamento As CDepartamento
Dim oOrganizacionCompras As COrganizacionCompras
Dim oCentro As CCentro
Dim oUON As IUon

    ''' * Objetivo: Abrir el combo de destinos de la forma adecuada
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bModificar Or Not m_bModifUltimaVersion Then
        sdbddOtrosCombos.DroppedDown = False
        Exit Sub
    End If
    
    sdbddOtrosCombos.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    Select Case sdbgCampos.Columns("CAMPO_GS").Value
        Case TipoCampoGS.Pais
            m_oPaises.CargarTodosLosPaises
            
            For Each oPais In m_oPaises
                sdbddOtrosCombos.AddItem oPais.Cod & Chr(m_lSeparador) & oPais.Den
            Next
            
        Case TipoCampoGS.Unidad
            m_oUnidades.CargarTodasLasUnidades
            
            For Each oUnidad In m_oUnidades
                sdbddOtrosCombos.AddItem oUnidad.Cod & Chr(m_lSeparador) & oUnidad.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            Next
            
        Case TipoCampoGS.provincia
            'Obtiene el pa�s seleccionado en la fila anterior.si no ha seleccionado pa�s no muestra las provincias.
            vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
            If sdbgCampos.Columns("COD_VALOR").CellValue(vbm) <> "" Then
                Set oPais = oFSGSRaiz.generar_CPais
                oPais.Cod = sdbgCampos.Columns("COD_VALOR").CellValue(vbm)
                oPais.CargarTodasLasProvincias
                For Each oProvi In oPais.Provincias
                    sdbddOtrosCombos.AddItem oProvi.Cod & Chr(m_lSeparador) & oProvi.Den
                Next
                Set oPais = Nothing
            End If
            
        Case TipoCampoGS.Moneda
            Set oMonedas = oFSGSRaiz.Generar_CMonedas
            oMonedas.CargarTodasLasMonedas
            For Each oMoneda In oMonedas
                sdbddOtrosCombos.AddItem oMoneda.Cod & Chr(m_lSeparador) & oMoneda.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            Next
            Set oMonedas = Nothing
            
        Case TipoCampoGS.Departamento
            vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
            If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) <> TipoCampoGS.UnidadOrganizativa Then
                'El campo Departamento es independiente a las unidades Organizativas
                Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
                oDepartamentos.CargarTodosLosDepartamentos
                For Each oDepartamento In oDepartamentos
                    sdbddOtrosCombos.AddItem oDepartamento.Cod & Chr(m_lSeparador) & oDepartamento.Den
                Next
                Set oDepartamentos = Nothing
            Else
                'El campo Departamento est� relacionado a las unidades Organizativas
                Dim vUnidadOrganizativa As Variant
                vUnidadOrganizativa = Split(sdbgCampos.Columns("valor").CellValue(vbm), " - ")
                ReDim Preserve vUnidadOrganizativa(UBound(vUnidadOrganizativa) - 1)
                ReDim Preserve vUnidadOrganizativa(3)
                Set oUON = createUon(vUnidadOrganizativa(0), vUnidadOrganizativa(1), vUnidadOrganizativa(2))
                oUON.CargarTodosLosDepartamentos
                For Each oDepartamento In oUON.Departamentos
                    sdbddOtrosCombos.AddItem oDepartamento.Cod & Chr(m_lSeparador) & oDepartamento.Den
                Next
                Set oUON = Nothing
            End If
            
        Case TipoCampoGS.OrganizacionCompras
            Dim oOrganizacionesCompras As COrganizacionesCompras
            Set oOrganizacionesCompras = oFSGSRaiz.Generar_COrganizacionesCompras
                
            oOrganizacionesCompras.CargarOrganizacionesCompra
            For Each oOrganizacionCompras In oOrganizacionesCompras.OrganizacionesCompras
                sdbddOtrosCombos.AddItem oOrganizacionCompras.Cod & Chr(m_lSeparador) & oOrganizacionCompras.Den
            Next
            Set oOrganizacionesCompras = Nothing
                
        Case TipoCampoGS.Centro
            Dim oCentros As CCentros
            Set oCentros = oFSGSRaiz.Generar_CCentros
            Dim sCodOrganizacion As String
            vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
            If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.OrganizacionCompras Then
                sCodOrganizacion = sdbgCampos.Columns("COD_VALOR").CellValue(vbm)
                oCentros.CargarTodosLosCentros , sCodOrganizacion
            Else
                oCentros.CargarTodosLosCentros
            End If
               
            For Each oCentro In oCentros.Centros
                sdbddOtrosCombos.AddItem oCentro.Cod & Chr(m_lSeparador) & oCentro.Den
            Next
            Set oCentros = Nothing
        Case TipoCampoGS.ProveedorERP
            Dim oCampo As CFormItem
            Dim sOrgCompras As String
            Dim sCodProveedor As String
            For Each oCampo In g_oGrupoSeleccionado.CAMPOS
                Select Case oCampo.CampoGS
                    Case TipoCampoGS.Proveedor
                        sCodProveedor = NullToStr(oCampo.valorText)
                        Exit For
                End Select
            Next
            'Miramos el valor de organizacion de compras a nivel del formulario
            sOrgCompras = ComprobarOrgCompras(True)
            If sCodProveedor <> "" And sOrgCompras <> "" Then
                Dim oProveERPs As CProveERPs
                Dim oProveERP As CProveERP
                Set oProveERPs = oFSGSRaiz.Generar_CProveERPs
                oProveERPs.CargarProveedoresERP , sCodProveedor, , , sOrgCompras, True
                For Each oProveERP In oProveERPs
                    sdbddOtrosCombos.AddItem oProveERP.Cod & Chr(m_lSeparador) & oProveERP.Den
                Next
            End If
    End Select
    
    If sdbddOtrosCombos.Rows = 0 Then
        sdbddOtrosCombos.AddItem ""
    End If
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddOtrosCombos_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbddOtrosCombos_InitColumnProps()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddOtrosCombos.DataFieldList = "Column 0"
    sdbddOtrosCombos.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddOtrosCombos_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddOtrosCombos_PositionList(ByVal Text As String)
PositionList sdbddOtrosCombos, Text
End Sub

Public Sub PonerMatSeleccionado()
    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    Dim sNuevoCod1 As String
    Dim sNuevoCod2 As String
    Dim sNuevoCod3 As String
    Dim sNuevoCod4 As String
    Dim sMat As String
    Dim iResp As Integer
    Dim arrMat As Variant
    Dim teserror As TipoErrorSummit
    Dim lIdCampoMatDef As Long
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub

    lIdCampoMatDef = CampoSeleccionado.IdCampoDef
    
    teserror.NumError = TESnoerror
    'Si el nivel de selecci�n es obligatorio, Comprobamos que haya habido cambios de familia de material
    If CampoSeleccionado.nivelSeleccion <> 0 Then
        If (CampoSeleccionado.valorText <> "") Then
            arrMat = DevolverMaterial(CampoSeleccionado.valorText)
            
            If (arrMat(1) <> frmSELMAT.GMNSeleccionado.GMN1Cod Or arrMat(2) <> frmSELMAT.GMNSeleccionado.GMN2Cod _
                Or arrMat(3) <> frmSELMAT.GMNSeleccionado.GMN3Cod Or arrMat(4) <> frmSELMAT.GMNSeleccionado.GMN4Cod) _
                And g_oSolicitudSeleccionada.existeLineaDesglose Then
                    iResp = oMensajes.MensajeYesNo(1450)
                    If iResp = vbNo Then
                        Exit Sub
                    Else
                        teserror = g_oSolicitudSeleccionada.EliminarArticulosEnTodosDesglosesDeInstancia
                        If teserror.NumError <> TESnoerror Then
                            Exit Sub
                        End If
                    End If
            End If
        End If
    End If
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then sNuevoCod1 = oGMN1Seleccionado.Cod
    If Not oGMN2Seleccionado Is Nothing Then sNuevoCod2 = oGMN2Seleccionado.Cod
    If Not oGMN3Seleccionado Is Nothing Then sNuevoCod3 = oGMN3Seleccionado.Cod
    If Not oGMN4Seleccionado Is Nothing Then sNuevoCod4 = oGMN4Seleccionado.Cod
            
    If Not oGMN1Seleccionado Is Nothing Then
        sdbgCampos.Columns("VALOR").Value = sNuevoCod1 & " - " & oGMN1Seleccionado.Den
        sMat = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sNuevoCod1)) & sNuevoCod1
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sdbgCampos.Columns("VALOR").Value = sNuevoCod1 & " - " & sNuevoCod2 & " - " & oGMN2Seleccionado.Den
        sMat = sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(sNuevoCod2)) & sNuevoCod2
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sdbgCampos.Columns("VALOR").Value = sNuevoCod1 & " - " & sNuevoCod2 & " - " & sNuevoCod3 & " - " & oGMN3Seleccionado.Den
        sMat = sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(sNuevoCod3)) & sNuevoCod3
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sdbgCampos.Columns("VALOR").Value = sNuevoCod1 & " - " & sNuevoCod2 & " - " & sNuevoCod3 & " - " & sNuevoCod4 & " - " & oGMN4Seleccionado.Den
        sMat = sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(sNuevoCod4)) & sNuevoCod4
    End If
    
    sdbgCampos.Columns("COD_VALOR").Value = sMat
    Dim iRiesgo As Integer
    iRiesgo = oGMN4Seleccionado.DevolverRiesgo
    sdbgCampos.Columns("RIESGO").Value = iRiesgo
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    sdbgCampos.Update
    
      'Quita el valor del art�culo
    sdbgCampos.Row = sdbgCampos.Row + 1
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
        sdbgCampos.Columns("COD_VALOR").Value = ""
        sdbgCampos.Columns("VALOR").Value = ""
        sdbgCampos.Update
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo And sdbgCampos.Columns("COD_VALOR").Value <> "" Then
        Dim oArticulos As CArticulos
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
    
        oArticulos.CargarTodosLosArticulos sdbgCampos.Columns("COD_VALOR").Value, , True
       
        If oArticulos.Count > 0 Then
        
            sdbgCampos.Columns("COD_VALOR").Value = ""
            sdbgCampos.Columns("VALOR").Value = ""
            sdbgCampos.Update
    
        'Quita el valor de la denominaci�n del art�culo
            sdbgCampos.Row = sdbgCampos.Row + 1
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                sdbgCampos.Update
            End If
            sdbgCampos.Row = sdbgCampos.Row - 1
        End If
    
        Set oArticulos = Nothing
    End If
    
    LimpiarValorListaHijaMaterial lIdCampoMatDef
    
    sdbgCampos.Update

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "PonerMatSeleccionado", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
Public Sub MostrarPresSeleccionado(ByVal sValor As String, ByVal iTipo As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sdbgCampos.Columns("VALOR").Value = GenerarDenominacionPresupuesto(sValor, iTipo)
sdbgCampos.Columns("COD_VALOR").Value = sValor
sdbgCampos.Update

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "MostrarPresSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function ObtenerPresupAsignado(ByVal sValor As Variant) As Double
Dim arrPresupuestos() As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
arrPresupuestos = Split(sValor, "#")

Dim oPresup As Variant
Dim arrPresup As Variant
Dim dPorcent As Double

For Each oPresup In arrPresupuestos
    arrPresup = Split(oPresup, "_")
    dPorcent = dPorcent + Numero(arrPresup(2), ".")
Next

ObtenerPresupAsignado = dPorcent * 100
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ObtenerPresupAsignado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Sub MostrarUOSeleccionada()
    Dim valorUO As String
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
valorUO = sdbgCampos.Columns("VALOR").Value

    If frmSolicitudDetalle.m_sUON3Sel <> "" Then
        sdbgCampos.Columns("VALOR").Value = frmSolicitudDetalle.m_sUON1Sel & " - " & frmSolicitudDetalle.m_sUON2Sel & " - " & frmSolicitudDetalle.m_sUON3Sel & " - " & frmSolicitudDetalle.m_sUODescrip
    ElseIf frmSolicitudDetalle.m_sUON2Sel <> "" Then
        sdbgCampos.Columns("VALOR").Value = frmSolicitudDetalle.m_sUON1Sel & " - " & frmSolicitudDetalle.m_sUON2Sel & " - " & frmSolicitudDetalle.m_sUODescrip
    ElseIf frmSolicitudDetalle.m_sUON1Sel <> "" Then
        sdbgCampos.Columns("VALOR").Value = frmSolicitudDetalle.m_sUON1Sel & " - " & frmSolicitudDetalle.m_sUODescrip
    ElseIf frmSolicitudDetalle.m_sUODescrip <> "" Then
        sdbgCampos.Columns("VALOR").Value = frmSolicitudDetalle.m_sUODescrip
    End If
    
    If valorUO <> sdbgCampos.Columns("VALOR").Value Then
        sdbgCampos.Columns("BAJALOGICA").Value = False
        sdbgCampos.Columns("VALOR").CellStyleSet ""
    End If

    sdbgCampos.MoveNext
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Departamento Then
        sdbgCampos.Columns("COD_VALOR").Value = ""
        sdbgCampos.Columns("VALOR").Value = ""
        g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = ""
        sdbgCampos.Columns("BAJALOGICA").Value = False
        sdbgCampos.Columns("VALOR").CellStyleSet ""
        sdbgCampos.Bookmark = sdbgCampos.GetBookmark(0) 'Esto es necesario para refrescar el estilo
    End If
    sdbgCampos.MovePrevious
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "MostrarUOSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub sdbddAlmacenes_CloseUp()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgCampos.Columns("COD_VALOR").Value = sdbddAlmacenes.Columns("ID").Value
      
    If sdbddAlmacenes.Columns("COD").Value <> "" Or sdbddAlmacenes.Columns("DEN").Value <> "" Then
        sdbgCampos.Columns("VALOR").Value = sdbddAlmacenes.Columns("COD").Value & " - " & sdbddAlmacenes.Columns("DEN").Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddAlmacenes_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbddAlmacenes_DropDown()
    Dim vbm As Variant
    Dim oAlmacen As CAlmacen
        
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddAlmacenes.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
        Dim oAlmacenes As CAlmacenes
        Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
        Dim sCodCentro As String

        vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Centro Then
            sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(vbm)
            oAlmacenes.CargarTodosLosAlmacenes , sCodCentro
        Else
            oAlmacenes.CargarTodosLosAlmacenes
        End If

        For Each oAlmacen In oAlmacenes
            sdbddAlmacenes.AddItem oAlmacen.Id & Chr(m_lSeparador) & oAlmacen.Cod & Chr(m_lSeparador) & oAlmacen.Den
        Next
        Set oAlmacenes = Nothing
    End If
    
    If sdbddAlmacenes.Rows = 0 Then
        sdbddAlmacenes.AddItem ""
    End If
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddAlmacenes_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddAlmacenes_InitColumnProps()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddAlmacenes.DataFieldList = "Column 0"
    sdbddAlmacenes.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddAlmacenes_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddAlmacenes_PositionList(ByVal Text As String)
    PositionList sdbddAlmacenes, Text
End Sub

Private Sub sdbddEmpresas_CloseUp()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgCampos.Columns("COD_VALOR").Value = sdbddEmpresas.Columns("ID").Value
      
    If sdbddEmpresas.Columns("COD").Value <> "" Or sdbddEmpresas.Columns("DEN").Value <> "" Then
        sdbgCampos.Columns("VALOR").Value = sdbddEmpresas.Columns("COD").Value & " - " & sdbddEmpresas.Columns("DEN").Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddEmpresas_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbddEmpresas_DropDown()
    Dim vbm As Variant
        
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddEmpresas.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Empresa Then
        Dim oEmpresas As CEmpresas
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        
        vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
        
        Dim rs As Recordset
        Set rs = oEmpresas.DevolverTodasEmpresas(0)

        While Not rs.EOF
            sdbddEmpresas.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("NIF").Value & Chr(m_lSeparador) & rs("DEN").Value
            rs.MoveNext
        Wend
    
        Set oEmpresas = Nothing
    End If
    
    If sdbddEmpresas.Rows = 0 Then
        sdbddEmpresas.AddItem ""
    End If
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddEmpresas_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddEmpresas_InitColumnProps()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddEmpresas.DataFieldList = "Column 0"
    sdbddEmpresas.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddEmpresas_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddEmpresas_PositionList(ByVal Text As String)
    PositionList sdbddEmpresas, Text
End Sub

'*************************************************************
'Funcion que devuelve el Nombre del CampoGS, certificado o No Conformidad
'*************************************************************
Private Function ObtenerNombreCampo(ByVal lCampoGS As Long, ByVal iTipo As Integer, ByVal iTipoPredef As Integer, Optional ByVal sPRES5 As String) As String
    Dim sNombre As String
    Dim oParametros As CLiterales
    Dim Ador As Ador.Recordset
    Dim oPres5Niv0 As cPresConceptos5Nivel0

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case iTipo
    Case TipoCampoPredefinido.CampoGS
            Select Case lCampoGS
                Case TipoCampoGS.Pres1
                    Set oParametros = oGestorParametros.DevolverLiterales(20, 20, basPublic.gParametrosInstalacion.gIdioma)
                    sNombre = oParametros.Item(1).Den
                Case TipoCampoGS.Pres2
                    Set oParametros = oGestorParametros.DevolverLiterales(21, 21, basPublic.gParametrosInstalacion.gIdioma)
                    sNombre = oParametros.Item(1).Den
                Case TipoCampoGS.Pres3
                    Set oParametros = oGestorParametros.DevolverLiterales(27, 27, basPublic.gParametrosInstalacion.gIdioma)
                    sNombre = oParametros.Item(1).Den
                Case TipoCampoGS.Pres4
                    Set oParametros = oGestorParametros.DevolverLiterales(28, 28, basPublic.gParametrosInstalacion.gIdioma)
                    sNombre = oParametros.Item(1).Den
                Case Else
                    Select Case lCampoGS
                        Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 5)
                        Case TipoCampoGS.DenArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 36)
                        Case TipoCampoGS.Desglose
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 7)
                        Case TipoCampoGS.Dest
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 10)
                        Case TipoCampoGS.FormaPago
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 2)
                        Case TipoCampoGS.material
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 4)
                        Case TipoCampoGS.Moneda
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 3)
                        Case TipoCampoGS.Pais
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 8)
                        Case TipoCampoGS.Proveedor
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 1)
                        Case TipoCampoGS.provincia
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 9)
                        Case TipoCampoGS.Unidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 6)
                        Case TipoCampoGS.CampoPersona
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 35)
                        Case TipoCampoSC.DescrBreve
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 26)
                        Case TipoCampoSC.DescrDetallada
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 27)
                        Case TipoCampoSC.importe
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 28)
                        Case TipoCampoSC.Cantidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 29)
                        Case TipoCampoSC.FecNecesidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 30)
                        Case TipoCampoSC.IniSuministro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 31)
                        Case TipoCampoSC.FinSuministro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 32)
                        Case TipoCampoSC.ArchivoEspecific
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 33)
                        Case TipoCampoSC.PrecioUnitario
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 34)
                        Case TipoCampoGS.NumSolicitERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 37)
                        Case TipoCampoGS.UnidadOrganizativa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 38)
                        Case TipoCampoGS.Departamento
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 39)
                        Case TipoCampoGS.OrganizacionCompras
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 40)
                        Case TipoCampoGS.Centro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 41)
                        Case TipoCampoGS.Almacen
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 42)
                        Case TipoCampoGS.ImporteSolicitudesVinculadas
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 43)
                        Case TipoCampoGS.RefSolicitud
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 44)
                        Case TipoCampoSC.PrecioUnitarioAdj
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 45)
                        Case TipoCampoSC.ProveedorAdj
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 46)
                        Case TipoCampoSC.CantidadAdj
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 47)
                        Case TipoCampoGS.CentroCoste
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 48)
                        Case TipoCampoGS.Activo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 49)
                        Case TipoCampoGS.TipoPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 50)
                        Case TipoCampoGS.DesgloseActividad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 51)
                        Case TipoCampoGS.Factura
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 53)
                        Case TipoCampoGS.InicioAbono
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 54)
                        Case TipoCampoGS.FinAbono
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 55)
                        Case TipoCampoGS.ProveedorERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 58)
                        Case TipoCampoGS.PartidaPresupuestaria
                            Set oPres5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
                            Set Ador = oPres5Niv0.DevolverDenominacion(basPublic.gParametrosInstalacion.gIdioma, sPRES5)
                            Set oPres5Niv0 = Nothing
                        Case TipoCampoGS.CodComprador
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 59)
                        Case TipoCampoGS.Empresa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma, 65)
                    End Select
                    sNombre = Ador(0).Value
                    Ador.Close
            End Select

            Set oParametros = Nothing

    Case TipoCampoPredefinido.Certificado, TipoCampoPredefinido.NoConformidad

        Dim oTipoSolic As CTipoSolicit
        
        Set oTipoSolic = oFSGSRaiz.Generar_CTipoSolicit
        oTipoSolic.CargarTipoSolicitud lCampoGS, iTipo, iTipoPredef
        
        While oTipoSolic.CAMPOS.Item(1).Denominaciones.Count > 0
            If oTipoSolic.CAMPOS.Item(1).Denominaciones.Item(1).Cod = gParametrosInstalacion.gIdioma Then
                sNombre = oTipoSolic.CAMPOS.Item(1).Denominaciones.Item(1).Den
            End If
            oTipoSolic.CAMPOS.Item(1).Denominaciones.Remove (oTipoSolic.CAMPOS.Item(1).Denominaciones.Item(1).Cod)
        Wend
        
        Set oTipoSolic = Nothing
End Select

ObtenerNombreCampo = sNombre

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ObtenerNombreCampo", err, Erl, , m_bActivado)
      Exit Function
   End If
        
End Function

Private Function esGenerico(sCodArt As String, Optional bAnyadirArt As Boolean = False) As Boolean

    Dim oArticulos As CArticulos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticulos = oFSGSRaiz.Generar_CArticulos

    If sCodArt <> "" Then
        oArticulos.CargarTodosLosArticulos sCodArt, , True
        
        If oArticulos.Count > 0 Then
            esGenerico = CBool(oArticulos.Item(1).Generico)
        Else
            If bAnyadirArt Then
                esGenerico = True
            Else
                esGenerico = False
            End If
        End If
    Else
        esGenerico = True
    End If
    
    Set oArticulos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "esGenerico", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Sub MostrarSolPadreSeleccionada()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmSolicitudDetalle.m_lIDSOLPadre <> 0 Then
        Dim oCampo As CFormItem
        Dim sTitulo As String
        
        sdbgCampos.Columns("COD_VALOR").Value = frmSolicitudDetalle.m_lIDSOLPadre
        
        Set oCampo = oFSGSRaiz.Generar_CFormCampo
        
        sTitulo = oCampo.DevolverTitulo(frmSolicitudDetalle.m_lIDSOLPadre)
        
        If sTitulo <> "" Then
            sdbgCampos.Columns("VALOR").Value = frmSolicitudDetalle.m_lIDSOLPadre & " - " & sTitulo
        Else
            sdbgCampos.Columns("VALOR").Value = frmSolicitudDetalle.m_lIDSOLPadre
        End If
        
        Set oCampo = Nothing
        'Provocar el evento change de sdbgCampos
        sdbgCampos_Change
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "MostrarSolPadreSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Function PonerEstiloUON(ByVal sValor As String) As Boolean
Dim arrUon() As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
arrUon = Split(sValor, " - ")

Dim ilong As Integer
ilong = UBound(arrUon)

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

Set oUON1 = oFSGSRaiz.generar_CUnidadOrgNivel1
Set oUON2 = oFSGSRaiz.generar_CUnidadOrgNivel2
Set oUON3 = oFSGSRaiz.generar_CUnidadOrgNivel3

Dim Estado As Boolean

Select Case ilong
Case 1
        Estado = oUON1.EstadeBajaUON1(arrUon(0))
Case 2
        Estado = oUON2.EstadeBajaUON2(arrUon(0), arrUon(1))
Case 3
        Estado = oUON3.EstadeBajaUON3(arrUon(0), arrUon(1), arrUon(2))
End Select

Set oUON1 = Nothing
Set oUON2 = Nothing
Set oUON3 = Nothing

PonerEstiloUON = Estado
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "PonerEstiloUON", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Sub EstiloUON()
'''Escribimos el nuevo presupuesto en la grid
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_sUON3Sel <> "" Then
        sdbgCampos.Columns("VALOR").Value = m_sUON1Sel & " - " & m_sUON2Sel & " - " & m_sUON3Sel & " - " & m_sUODescrip
    ElseIf m_sUON2Sel <> "" Then
        sdbgCampos.Columns("VALOR").Value = m_sUON1Sel & " - " & m_sUON2Sel & " - " & m_sUODescrip
    ElseIf m_sUON1Sel <> "" Then
        sdbgCampos.Columns("VALOR").Value = m_sUON1Sel & " - " & m_sUODescrip
    ElseIf m_sUODescrip <> "" Then
        sdbgCampos.Columns("VALOR").Value = m_sUODescrip
    End If
    sdbgCampos.Columns("BAJALOGICA").Value = 0
    sdbgCampos.Columns("VALOR").CellStyleSet ""
    
    sdbgCampos.MoveNext
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Departamento Then
        sdbgCampos.Columns("COD_VALOR").Value = ""
        sdbgCampos.Columns("VALOR").Value = ""
    End If
    sdbgCampos.MovePrevious
    sdbgCampos.Update

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "EstiloUON", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Function PonerEstiloDep(ByVal sValor As String) As Boolean

Dim arrDep() As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
arrDep = Split(sValor, " - ")

Dim ilong As Integer
ilong = UBound(arrDep)

Dim oDep As CDepartamentos
Set oDep = oFSGSRaiz.Generar_CDepartamentos
PonerEstiloDep = oDep.EstadeBajaDep(arrDep(0))
Set oDep = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "PonerEstiloDep", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Sub sdbddArticulos_CloseUp()
Dim i As Long
Dim bm As Variant
Dim oUnidades As CUnidades
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("COD").Value
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then

        sdbgCampos.Columns("GENERICO").Value = sdbddArticulos.Columns("GENERICO").Value
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
            sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("COD").Value & " - " & sdbddArticulos.Columns("DEN").Value
        Else
            sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("COD").Value
        End If
        
        sdbgCampos.MoveNext
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
            sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("DEN").Value
            sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("DEN").Value
            sdbgCampos.MovePrevious
        End If
        QuitarValorTablaExterna
        
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
        sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("DEN").Value
        sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("DEN").Value
        sdbgCampos.MovePrevious
        sdbgCampos.Columns("GENERICO").Value = sdbddArticulos.Columns("GENERICO").Value
        sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("COD").Value
        sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("COD").Value
        
        sdbgCampos.MoveNext
        
    Else
        If sdbddArticulos.Columns("COD").Value <> "" Or sdbddArticulos.Columns("DEN").Value <> "" Then
            sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("COD").Value & " - " & sdbddArticulos.Columns("DEN").Value
        End If
    End If
    
    'Buscar si hay Precio y Proveedor y mirar si tiene el checkBox de cargar el ultimo Precio/proveedor adjucicado
    'Cargar tambien la UNIDAD
    If sdbgCampos.Columns("COD_VALOR").Value <> "" Then
        Dim lRowInit As Long
        lRowInit = sdbgCampos.Row
        sdbgCampos.Row = 0
        For i = 0 To sdbgCampos.Rows - 1
            bm = sdbgCampos.GetBookmark(i)
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoSC.PrecioUnitario Then
                If g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").CellValue(bm))).CargarUltADJ = True And Not sdbddArticulos.Columns("GENERICO").Value Then
                    sdbgCampos.Row = i
                    sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("PREC_ULT_ADJ").Value
                    sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("PREC_ULT_ADJ").Value
                   
                    sdbgCampos.Row = 0
                End If
            End If
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoGS.Proveedor And sdbddArticulos.Columns("CODPROV_ULT_ADJ").Value <> "" Then
                If g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").CellValue(bm))).CargarUltADJ = True And Not sdbddArticulos.Columns("GENERICO").Value Then
                    sdbgCampos.Row = i
                    sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("CODPROV_ULT_ADJ").Value
                    sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("CODPROV_ULT_ADJ").Value & " - " & sdbddArticulos.Columns("DENPROV_ULT_ADJ").Value
                   
                    sdbgCampos.Row = 0
                End If
            End If
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoGS.Unidad Then
                sdbgCampos.Row = i
                If Not IsNull(sdbddArticulos.Columns("UNI").Value) Then
                    Set oUnidades = oFSGSRaiz.Generar_CUnidades
                    oUnidades.CargarTodasLasUnidades sdbddArticulos.Columns("UNI").Value, , True
                    
                    If Not oUnidades.Item(1) Is Nothing Then
                        sdbgCampos.Columns("VALOR").Value = oUnidades.Item(1).Cod & " - " & oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        sdbgCampos.Columns("COD_VALOR").Value = oUnidades.Item(1).Cod
                    Else
                        sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("UNI").Value
                        sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("UNI").Value
                    End If
                    Set oUnidades = Nothing
                End If
                sdbgCampos.Row = 0
            End If
            
        Next i
        sdbgCampos.Row = lRowInit
    End If
  
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddArticulos_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddArticulos_DropDown()
Dim oArt As CArticulo
Dim vbm As Variant
Dim arrMat As Variant
Dim oGMN4 As CGrupoMatNivel4
Dim sCodOrgCompras, sCodCentro As String
Dim bUsarOrgCompras As Boolean
Dim i As Integer

Dim oArticulos As CArticulos
    ''' * Objetivo: Abrir el combo de destinos de la forma adecuada
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddArticulos.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    Select Case sdbgCampos.Columns("CAMPO_GS").Value
        Case TipoCampoGS.CodArticulo, TipoCampoGS.DenArticulo, TipoCampoGS.NuevoCodArticulo
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
                vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
            Else
                vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 2)
            End If

            If sdbgCampos.Columns("COD_VALOR").CellValue(vbm) <> "" Then
                If gParametrosGenerales.gbUsarOrgCompras = True Then
                    Dim vbmOrgCompras, vbmCentro As Variant
                    'Buscar la Organizacion de Compras y Centros
                    i = 0
                    While i < sdbgCampos.Row
                         vbmOrgCompras = sdbgCampos.AddItemBookmark(i)
                         If sdbgCampos.Columns("CAMPO_GS").CellValue(vbmOrgCompras) = TipoCampoGS.OrganizacionCompras Then
                             vbmCentro = sdbgCampos.AddItemBookmark(i + 1)
                             sCodOrgCompras = sdbgCampos.Columns("COD_VALOR").CellValue(vbmOrgCompras)
                             If sdbgCampos.Columns("CAMPO_GS").CellValue(vbmCentro) = TipoCampoGS.Centro Then
                                 sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(vbmCentro)
                                 bUsarOrgCompras = True
                             End If
                         End If
                         i = i + 1
                     Wend
                     
                End If
                
                arrMat = DevolverMaterial(sdbgCampos.Columns("COD_VALOR").CellValue(vbm))
        
                Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
                oGMN4.GMN1Cod = arrMat(1)
                oGMN4.GMN2Cod = arrMat(2)
                oGMN4.GMN3Cod = arrMat(3)
                oGMN4.Cod = arrMat(4)
                Dim sMoneda As String
                If Not g_oSolicitudSeleccionada Is Nothing Then
                    sMoneda = g_oSolicitudSeleccionada.Moneda
                End If
                
                If bUsarOrgCompras Then
                    oGMN4.CargarTodosLosArticulos , , , , , , False, , , , , , True, sMoneda, sCodOrgCompras, sCodCentro
                Else
                    oGMN4.CargarTodosLosArticulos , , , , , , False, , , , , , True, sMoneda
                End If
                
                
                Set oArticulos = oGMN4.ARTICULOS
                Set oGMN4 = Nothing
                
                For Each oArt In oArticulos
                    sdbddArticulos.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den & Chr(m_lSeparador) & oArt.Generico & Chr(m_lSeparador) & oArt.PREC_UltADJ & Chr(m_lSeparador) & oArt.CODPROV_UltADJ & Chr(m_lSeparador) & oArt.DENPROV_UltADJ & Chr(m_lSeparador) & oArt.CodigoUnidad
                Next
                Set oArticulos = Nothing
            End If
    End Select
    If sdbddArticulos.Rows = 0 Then
        sdbddArticulos.AddItem ""
    End If
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddArticulos_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbddArticulos_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddArticulos.DataFieldList = "Column 0"
    sdbddArticulos.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "sdbddArticulos_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddArticulos_PositionList(ByVal Text As String)
PositionList sdbddArticulos, Text
End Sub


Private Function ExisteAtributoArticulo() As Boolean
    Dim i As Integer
    Dim vbm As Variant
    Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bExiste = False
    
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
    
        If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo _
        Or sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo) Then
            If sdbgCampos.Columns("VALOR").CellValue(vbm) <> "" And sdbgCampos.Row <> i Then
                bExiste = True
                Exit For
            End If
        End If
    Next
    
    ExisteAtributoArticulo = bExiste
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ExisteAtributoArticulo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function DevuelveIdArticulo() As String
Dim i As Integer
Dim vbm As Variant
Dim tArray() As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Then
            If sdbgCampos.Columns("VALOR").CellValue(vbm) <> "" And sdbgCampos.Row <> i Then
                tArray = Split(sdbgCampos.Columns("VALOR").CellValue(vbm), "-")
                DevuelveIdArticulo = Trim(tArray(0))
                Exit For
            End If
        End If
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo Then
            If sdbgCampos.Columns("VALOR").CellValue(vbm) <> "" And sdbgCampos.Row <> i Then
                DevuelveIdArticulo = Trim(sdbgCampos.Columns("VALOR").CellValue(vbm))
                Exit For
            End If
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "DevuelveIdArticulo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub QuitarValorTablaExterna()

Dim i As Integer
Dim vbm As Variant
Dim intTabla As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        If sdbgCampos.Columns("TIPO").CellValue(vbm) = TipoCampoPredefinido.externo Then
            sdbgCampos.Bookmark = vbm
            intTabla = CInt(sdbgCampos.Columns(11).Value)
            If m_oTablaExterna.SacarTablaTieneART(intTabla) Then
                sdbgCampos.Columns("VALOR").Value = ""
                '---------------------------------------------------------------------------------------------------------------------
                'hay que actualizar el campo
                g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).modificado = True
                g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = Null
                g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorBool = Null
                g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorFec = Null
                g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = Null
                sdbgCampos.Update
                '---------------------------------------------------------------------------------------------------------------------
            End If
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "QuitarValorTablaExterna", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
''' <summary>
''' Comprueba si existe a nivel de formulario un campo GS Organizacion de compras, o ademas de comprobarlo devolvera su valor
''' </summary>
''' <param name="DevolverValor">True si se quiere que devuelva el valor del campo si existe, si es false se devolvera true si existe el campo</param>
''' <returns>Devuelve true si existe el campo Org.compras</returns>
''' <remarks>Llamada desde: cmdElimItem_Click; Tiempo m�ximo=0,6 seg.</remarks>
Private Function ComprobarOrgCompras(Optional ByVal DevolverValor As Boolean) As Variant
    Dim g As CFormGrupo
    Dim c As CFormItem
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oSolicitudSeleccionada Is Nothing Then
        If g_oSolicitudSeleccionada.Grupos Is Nothing Then
            g_oSolicitudSeleccionada.CargarGrupos
        End If
        For Each g In g_oSolicitudSeleccionada.Grupos
            If g.CAMPOS Is Nothing Then
                g.CargarTodosLosCampos
            End If
            
            If Not g.CAMPOS Is Nothing Then
                For Each c In g.CAMPOS
                    If c.CampoGS = TipoCampoGS.OrganizacionCompras Then
                        If DevolverValor Then
                            ComprobarOrgCompras = c.valorText
                        Else
                            ComprobarOrgCompras = True
                        End If
                        Exit Function
                    End If
                Next
            End If
        Next
    End If
    ComprobarOrgCompras = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "ComprobarOrgCompras", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

'Funcion que dependiendo de la accion hara diferentes acciones sobre el campoGs Proveedor ERP
'Accion: 0: Elimina el valor del campo;1: Devuelve true si el campo existe en el formulario y false si no
Private Function AccionSobreCampoGsProveedorERP(ByVal Accion As Byte, Optional ByVal CampoGsOrigen As TipoCampoGS) As Boolean
Dim vbm As Variant
Dim i As Integer
Dim oCampo As CFormItem
Dim oSubCampo As CFormItem
Dim oLineaDesglose As CLineaDesglose

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.ProveedorERP Then
            sdbgCampos.Bookmark = vbm
            Select Case Accion
            Case 0 'Eliminamos el valor del campo
                sdbgCampos.Columns("VALOR").Value = ""
                sdbgCampos.Columns("COD_VALOR").Value = ""
            Case 1 'Devolvemos que el campo existe
                AccionSobreCampoGsProveedorERP = True
                Exit Function
            End Select
        End If
    Next
    Select Case Accion
        Case 0
            'Si se pretende eliminar el valor del campo proveedor Erp por haber modificado otro campo
            'habra que mirar en los desgloses(si los hubiera) por si hay campos proveedor ERP
            
            Select Case CampoGsOrigen
                Case TipoCampoGS.OrganizacionCompras
                    'Miramos los campos de ese formulario buscando campos de desglose
                    For Each oCampo In g_oGrupoSeleccionado.CAMPOS
                        If oCampo.Tipo = TipoDesglose Then
                            Dim bExisteOrgComprasEnDesglose As Boolean
                            Dim bExisteProveedorErpEnDesglose As Boolean
                            'Recorremos una primera vez los campos del desglose porque solo se eliminara el valor
                            'del proveedor Erp cuando la organizacion de compras no este tambien en el desglose
                            oCampo.CargarDesglose
                            Dim rs As ADODB.Recordset
                            Set rs = oCampo.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse)
                            For Each oSubCampo In oCampo.Desglose
                                If oSubCampo.CampoGS = TipoCampoGS.OrganizacionCompras Then
                                    bExisteOrgComprasEnDesglose = True
                                ElseIf oSubCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                                    bExisteProveedorErpEnDesglose = True
                                End If
                            Next
                            If bExisteOrgComprasEnDesglose = False And bExisteProveedorErpEnDesglose = True Then
                                'Si la organizacion de compras no esta en el desglose borraremos el valor del campoGs Proveedor ERP en todas las lineas de desglose
                                For Each oLineaDesglose In oCampo.LineasDesglose
                                    If oLineaDesglose.CampoHijo.CampoGS = TipoCampoGS.ProveedorERP Then
                                        'Borramos el valor del campo Proveedor ERP
                                        oLineaDesglose.valorText = Null
                                        Exit For
                                    End If
                                Next
                            End If
                        End If
                    Next
                    
                    'Se busca en el resto de grupos
                    Dim g As CFormGrupo
                    Dim c As CFormItem
                    
                    If Not g_oSolicitudSeleccionada Is Nothing Then
                        If g_oSolicitudSeleccionada.Grupos Is Nothing Then
                            g_oSolicitudSeleccionada.CargarGrupos
                        End If
                        For Each g In g_oSolicitudSeleccionada.Grupos
                            If g.CAMPOS Is Nothing Then
                                g.CargarTodosLosCampos
                            End If
                            
                            If Not g.CAMPOS Is Nothing And Not g.Id = g_oGrupoSeleccionado.Id Then
                                For Each c In g.CAMPOS
                                    If c.CampoGS = TipoCampoGS.ProveedorERP Then
                                        c.valorText = Null
                                    ElseIf c.Tipo = TipoDesglose Then
                                        bExisteOrgComprasEnDesglose = False
                                        bExisteProveedorErpEnDesglose = False
                                        c.CargarDesglose
                                        Set rs = c.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse)
                                        For Each oSubCampo In c.Desglose
                                            If oSubCampo.CampoGS = TipoCampoGS.OrganizacionCompras Then
                                                bExisteOrgComprasEnDesglose = True
                                            ElseIf oSubCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                                                bExisteProveedorErpEnDesglose = True
                                            End If
                                        Next
                                        If bExisteOrgComprasEnDesglose = False And bExisteProveedorErpEnDesglose = True Then
                                            'Si la organizacion de compras no esta en el desglose borraremos el valor del campoGs Proveedor ERP en todas las lineas de desglose
                                            For Each oLineaDesglose In c.LineasDesglose
                                                If oLineaDesglose.CampoHijo.CampoGS = TipoCampoGS.ProveedorERP Then
                                                    'Borramos el valor del campo Proveedor ERP
                                                    oLineaDesglose.valorText = Null
                                                End If
                                            Next
                                        End If
                                    End If
                                Next
                            End If
                        Next
                    End If
                    
            End Select
            
        Case 1 'El campo proveedor ERP no existe en el formulario
            AccionSobreCampoGsProveedorERP = False
            Exit Function
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDetalle", "AccionSobreCampoGsProveedorERP", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function CampoSeleccionado() As CFormItem
    Set CampoSeleccionado = g_oGrupoSeleccionado.CAMPOS.Item(CStr(sdbgCampos.Columns("ID").Value))
End Function

Private Function EscrituraParaDetalleSolicGS(ByVal lID As Long) As Boolean
    If g_sOrigen = "frmSolicitudes" Then
        EscrituraParaDetalleSolicGS = m_oCumplimentacionesGS.Item(CStr(lID)).ESCRITURA
    Else
        'Por ejemplo: Pedidos. El mantenimiento 13462 (GS. Detalle de solicitud. Aplicar la cumplimentaci�n de los campos del formulario.) no aplica
        EscrituraParaDetalleSolicGS = True
    End If
End Function
