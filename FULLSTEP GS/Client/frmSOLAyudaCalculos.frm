VERSION 5.00
Object = "{DE8CE233-DD83-481D-844C-C07B96589D3A}#1.1#0"; "vbalSGrid6.ocx"
Begin VB.Form frmSOLAyudaCalculos 
   Caption         =   "DFunciones y operadores disponibles"
   ClientHeight    =   6795
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10875
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLAyudaCalculos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6795
   ScaleWidth      =   10875
   StartUpPosition =   1  'CenterOwner
   Begin vbAcceleratorSGrid6.vbalGrid vbAyuda 
      Height          =   6760
      Left            =   20
      TabIndex        =   0
      Top             =   20
      Width           =   10840
      _ExtentX        =   19129
      _ExtentY        =   11933
      BackgroundPictureHeight=   0
      BackgroundPictureWidth=   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Header          =   0   'False
      DisableIcons    =   -1  'True
   End
End
Attribute VB_Name = "frmSOLAyudaCalculos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sAritmeticos(7) As String
Private m_sBooleanos(8) As String
Private m_sLogicos(7) As String
Private m_sBit(5) As String
Private m_sCondicionales(3) As String
Private m_sFunciones(31) As String

Private m_sDescrAritmeticos(7) As String
Private m_sDescrBooleanos(8) As String
Private m_sDescrLogicos(7) As String
Private m_sDescrBit(5) As String
Private m_sDescrCondicionales(3) As String
Private m_sDescrFunciones(31) As String

Private m_sOperadores As String
Private m_sFunc As String

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_AYUDA_CALCULOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.Caption = Ador(0).Value   '1 Funciones y operadores disponibles
        Ador.MoveNext
        m_sOperadores = Ador(0).Value  '2 Operadores
        
        For i = 1 To 7   'textos de los operadores aritm�ticos
            Ador.MoveNext
            m_sDescrAritmeticos(i) = Ador(0).Value
        Next i
        
        For i = 1 To 8   'textos de los operadores booleanos
            Ador.MoveNext
            m_sDescrBooleanos(i) = Ador(0).Value
        Next i
        
        For i = 1 To 7  'textos de los operadores l�gicos
            Ador.MoveNext
            m_sDescrLogicos(i) = Ador(0).Value
        Next i
        
        For i = 1 To 5  'textos de los operadores a nivel de bit
            Ador.MoveNext
            m_sDescrBit(i) = Ador(0).Value
        Next i
        
        For i = 1 To 3  'textos de los operadores condicionales
            Ador.MoveNext
            m_sDescrCondicionales(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sFunc = Ador(0).Value  '33 Funciones
        
        For i = 1 To 31  'textos de las descripciones de las funciones
            Ador.MoveNext
            m_sDescrFunciones(i) = Ador(0).Value
        Next i
        
        For i = 1 To 31  'textos de las funciones
            Ador.MoveNext
            m_sFunciones(i) = Ador(0).Value
        Next i
        
        Ador.Close
    
    End If

    Set Ador = Nothing
    
    
    'Operadores aritm�ticos:
    m_sAritmeticos(2) = "+"
    m_sAritmeticos(3) = "-"
    m_sAritmeticos(4) = "*"
    m_sAritmeticos(5) = "/"
    m_sAritmeticos(6) = "%"
    m_sAritmeticos(7) = "^"
    
    'Operadores booleanos:
    m_sBooleanos(2) = "<"
    m_sBooleanos(3) = ">"
    m_sBooleanos(4) = "=="
    m_sBooleanos(5) = "!="
    m_sBooleanos(6) = "<>"
    m_sBooleanos(7) = "<="
    m_sBooleanos(8) = ">="
    
    'Operadores l�gicos:
    m_sLogicos(2) = "&&"
    m_sLogicos(3) = "||"
    m_sLogicos(4) = "!"
    m_sLogicos(5) = "AND"
    m_sLogicos(6) = "OR"
    m_sLogicos(7) = "NOT"
    
    'Operadores a nivel de bit
    m_sBit(2) = "&"
    m_sBit(3) = "|"
    m_sBit(4) = "!&"
    m_sBit(5) = "~"
    
    'Operadores condicionales:
    m_sCondicionales(2) = ":"
    m_sCondicionales(3) = "?"


End Sub


Private Sub Form_Load()

    Me.Height = 7500
    Me.Width = 12000
    
    CargarRecursos
    
    CargarAyuda
End Sub

Private Sub CargarAyuda()
Dim cabecera As New StdFont
Dim i As Integer
Dim iFila As Integer
Dim iOperador As Integer

    iFila = 1
    iOperador = 1
    
    'A�ade un estilo para las cabeceras:
    cabecera.Bold = True
    cabecera.Name = "Verdana"
    cabecera.Size = "10"

    'Inserta las columnas:
    vbAyuda.AddColumn "Numero"
    vbAyuda.AddColumn "Operador"
    vbAyuda.ColumnWidth(2) = 150
    vbAyuda.AddColumn "Descripci�n"
    vbAyuda.ColumnWidth(3) = 540
    
    'OPERADORES:
    vbAyuda.AddRow
    vbAyuda.CellText(iFila, 3) = m_sOperadores
    vbAyuda.CellFont(iFila, 3) = cabecera
    vbAyuda.Cell(iFila, 2).TextAlign = DT_EXTERNALLEADING
    iFila = iFila + 1
    
    'Operadores aritm�ticos:
    vbAyuda.AddRow
    vbAyuda.CellText(iFila, 3) = m_sDescrAritmeticos(1)
    vbAyuda.CellFont(iFila, 3) = cabecera
    iFila = iFila + 1
    
    For i = 2 To UBound(m_sAritmeticos)
        vbAyuda.AddRow
        vbAyuda.CellText(iFila, 1) = iOperador
        vbAyuda.CellText(iFila, 2) = m_sAritmeticos(i)
        vbAyuda.CellText(iFila, 3) = m_sDescrAritmeticos(i)
        iFila = iFila + 1
        iOperador = iOperador + 1
    Next i
    
    
    'Operadores booleanos:
    vbAyuda.AddRow
    vbAyuda.CellText(iFila, 1) = ""
    vbAyuda.CellText(iFila, 3) = m_sDescrBooleanos(1)
    vbAyuda.CellFont(iFila, 3) = cabecera
    iFila = iFila + 1
    
    For i = 2 To UBound(m_sBooleanos)
        vbAyuda.AddRow
        vbAyuda.CellText(iFila, 1) = iOperador
        vbAyuda.CellText(iFila, 2) = m_sBooleanos(i)
        vbAyuda.CellText(iFila, 3) = m_sDescrBooleanos(i)
        iFila = iFila + 1
        iOperador = iOperador + 1
    Next i
    

    'Operadores l�gicos:
    vbAyuda.AddRow
    vbAyuda.CellText(iFila, 1) = ""
    vbAyuda.CellText(iFila, 3) = m_sDescrLogicos(1)
    vbAyuda.CellFont(iFila, 3) = cabecera
    iFila = iFila + 1
    
    For i = 2 To UBound(m_sLogicos)
        vbAyuda.AddRow
        vbAyuda.CellText(iFila, 1) = iOperador
        vbAyuda.CellText(iFila, 2) = m_sLogicos(i)
        vbAyuda.CellText(iFila, 3) = m_sDescrLogicos(i)
        iFila = iFila + 1
        iOperador = iOperador + 1
    Next i
    

    'Operadores a nivel de bit
    vbAyuda.AddRow
    vbAyuda.CellText(iFila, 1) = ""
    vbAyuda.CellText(iFila, 3) = m_sDescrBit(1)
    vbAyuda.CellFont(iFila, 3) = cabecera
    iFila = iFila + 1
    
    For i = 2 To UBound(m_sBit)
        vbAyuda.AddRow
        vbAyuda.CellText(iFila, 1) = iOperador
        vbAyuda.CellText(iFila, 2) = m_sBit(i)
        vbAyuda.CellText(iFila, 3) = m_sDescrBit(i)
        iFila = iFila + 1
        iOperador = iOperador + 1
    Next i
    
    
    'Operadores condicionales:
    vbAyuda.AddRow
    vbAyuda.CellText(iFila, 1) = ""
    vbAyuda.CellText(iFila, 3) = m_sDescrCondicionales(1)
    vbAyuda.CellFont(iFila, 3) = cabecera
    iFila = iFila + 1
    
    For i = 2 To UBound(m_sCondicionales)
        vbAyuda.AddRow
        vbAyuda.CellText(iFila, 1) = iOperador
        vbAyuda.CellText(iFila, 2) = m_sCondicionales(i)
        vbAyuda.CellText(iFila, 3) = m_sDescrCondicionales(i)
        iFila = iFila + 1
        iOperador = iOperador + 1
    Next i

    
    
    'FUNCIONES:
    vbAyuda.AddRow
    vbAyuda.CellText(iFila, 3) = m_sFunc
    vbAyuda.CellFont(iFila, 3) = cabecera
    vbAyuda.Cell(iFila, 2).TextAlign = DT_EXTERNALLEADING
    iFila = iFila + 1
    iOperador = 1
    
    For i = 1 To UBound(m_sFunciones)
        vbAyuda.AddRow
        vbAyuda.CellText(iFila, 1) = iOperador
        vbAyuda.CellText(iFila, 2) = m_sFunciones(i)
        vbAyuda.CellText(iFila, 3) = m_sDescrFunciones(i)
        iFila = iFila + 1
        iOperador = iOperador + 1
    Next i
    
End Sub

Private Sub Form_Resize()
    vbAyuda.Width = Me.Width - 150
    vbAyuda.Height = Me.Height - 540
End Sub


