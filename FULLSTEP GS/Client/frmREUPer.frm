VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmREUPer 
   BackColor       =   &H00808000&
   Caption         =   "Personas implicadas"
   ClientHeight    =   4215
   ClientLeft      =   30
   ClientTop       =   1395
   ClientWidth     =   9480
   Icon            =   "frmREUPer.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4215
   ScaleWidth      =   9480
   Begin SSDataWidgets_B.SSDBDropDown sdbddRoles 
      Height          =   1515
      Left            =   780
      TabIndex        =   7
      Top             =   720
      Width           =   3855
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1085
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   4868
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      _ExtentX        =   6800
      _ExtentY        =   2672
      _StockProps     =   77
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigatePersona 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   450
      Left            =   0
      ScaleHeight     =   450
      ScaleWidth      =   9480
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3765
      Width           =   9480
      Begin VB.CommandButton cmdListadoPersona 
         Caption         =   "&Listado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4680
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   90
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adirPersona 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminarPersona 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdCambiarRol 
         Caption         =   "&Cambiar rol"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   90
         Width           =   1065
      End
      Begin VB.CommandButton cmdRestaurarPersona 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3540
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPersonas 
      Height          =   3615
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   10080
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   11
      stylesets.count =   1
      stylesets(0).Name=   "BajaLog"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(0).Picture=   "frmREUPer.frx":014A
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      BalloonHelp     =   0   'False
      MaxSelectedRows =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterPos     =   3
      SplitterVisible =   -1  'True
      Columns.Count   =   11
      Columns(0).Width=   1191
      Columns(0).Caption=   "Rol"
      Columns(0).Name =   "REL"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1746
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   3
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3122
      Columns(2).Caption=   "Apellidos"
      Columns(2).Name =   "APE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2514
      Columns(3).Caption=   "Nombre"
      Columns(3).Name =   "DEN"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   50
      Columns(4).Width=   2805
      Columns(4).Caption=   "Unidad organizativa"
      Columns(4).Name =   "UON"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   2037
      Columns(5).Caption=   "Departamento"
      Columns(5).Name =   "DEP"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   2064
      Columns(6).Caption=   "Cargo"
      Columns(6).Name =   "CAR"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   1799
      Columns(7).Caption=   "Tel�fono"
      Columns(7).Name =   "TFNO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   1693
      Columns(8).Caption=   "Fax"
      Columns(8).Name =   "FAX"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   2990
      Columns(9).Caption=   "Mail"
      Columns(9).Name =   "MAIL"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "BAJALOG"
      Columns(10).Name=   "BAJALOG"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   11
      Columns(10).FieldLen=   256
      _ExtentX        =   17780
      _ExtentY        =   6376
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmREUPer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oRoles As CRoles
Private bModif As Boolean
Private sRolViejo As String
Private sIdiPer As String

Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModificarPers)) Is Nothing) Then
        bModif = True
    End If
Else
    bModif = True
End If

If Not bModif Then
    cmdA�adirPersona.Visible = False
    cmdEliminarPersona.Visible = False
    cmdCambiarRol.Visible = False
    cmdRestaurarPersona.Left = cmdA�adirPersona.Left
    cmdListadoPersona.Left = cmdEliminarPersona.Left
End If
    
End Sub

Private Sub cmdEliminarPersona_Click()
    
    If sdbgPersonas.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If Not IsNull(sdbgPersonas.Bookmark) Then
        sdbgPersonas.SelBookmarks.Add sdbgPersonas.Bookmark
        EliminarPersonaSeleccionada
    End If
    Screen.MousePointer = vbNormal
    
End Sub
Public Sub CargarGridConPersonas()

Dim oPer As CPersona
Dim oPers As CPersonas
Dim oIPersAsig As IPersonasAsig

    sdbgPersonas.RemoveAll
        
    Set oIPersAsig = frmREU.oProcesoSeleccionado
    Set oPers = oIPersAsig.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorCod, True)
    
    For Each oPer In oPers
        sdbgPersonas.AddItem oPer.Rol & Chr(m_lSeparador) & oPer.Cod & Chr(m_lSeparador) & oPer.Apellidos & Chr(m_lSeparador) & oPer.nombre & Chr(m_lSeparador) & oPer.UON1 & CStr(" - " & oPer.UON2) & CStr(" - " & oPer.UON3) & Chr(m_lSeparador) & oPer.CodDep & Chr(m_lSeparador) & oPer.Cargo & Chr(m_lSeparador) & oPer.Tfno & Chr(m_lSeparador) & oPer.Fax & Chr(m_lSeparador) & oPer.mail & Chr(m_lSeparador) & oPer.BajaLog
    Next
    
    Set oPers = Nothing
    Set oIPersAsig = Nothing
    

End Sub

Private Sub cmdA�adirPersona_Click()
 
    frmSELPer.sOrigen = "frmREUPer"
    If gParametrosGenerales.gsROLDEF <> "" Then
        frmSELPer.sdbcRolCod = gParametrosGenerales.gsROLDEF
        frmSELPer.sdbcRolCod_Validate False
    End If
    frmSELPer.Show 1
    
End Sub

Public Sub EliminarPersonaSeleccionada()
Dim oIPersAsig As IPersonasAsig
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

    
    irespuesta = oMensajes.PreguntaEliminar(sIdiPer & " " & sdbgPersonas.Columns(1).value & " (" & sdbgPersonas.Columns(2).value & ")")
    If irespuesta = vbYes Then
        
        Screen.MousePointer = vbHourglass
        Set oIPersAsig = frmREU.oProcesoSeleccionado
        teserror = oIPersAsig.DesAsignarPersona(sdbgPersonas.Columns(1).value, sdbgPersonas.Columns(0).value)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oIPersAsig = Nothing
            Exit Sub
        End If
        
        sdbgPersonas.RemoveItem (sdbgPersonas.AddItemRowIndex(sdbgPersonas.AddItemBookmark(sdbgPersonas.Row)))
        
        basSeguridad.RegistrarAccion accionessummit.ACCProcePerEli, "Anyo:" & CStr(frmREU.oProcesoSeleccionado.Anyo) & "GMN1:" & CStr(frmREU.oProcesoSeleccionado.GMN1Cod) & "Proce:" & CStr(frmREU.oProcesoSeleccionado.Cod) & "Per:" & sdbgPersonas.Columns(1).Text
        
        Set oIPersAsig = Nothing
        Screen.MousePointer = vbNormal
    
    End If
    
    sdbgPersonas.SelBookmarks.RemoveAll
    
End Sub
Private Sub cmdCambiarRol_Click()
        
        Screen.MousePointer = vbHourglass
        If sdbgPersonas.Rows > 0 Then
            'Creamos las variables para las dropdowns
            Set oRoles = Nothing
            Set oRoles = oFSGSRaiz.generar_CRoles
            
            oRoles.CargarTodosLosRoles , , , , , , basPublic.gParametrosInstalacion.gIdioma
            sdbgPersonas.AllowUpdate = True
            
            CargarGridConRoles
            
            If Me.Visible Then sdbgPersonas.SetFocus
            sdbgPersonas.Col = 0
            
            picNavigatePersona.Enabled = False
             
        End If
        Screen.MousePointer = vbNormal
            
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_REUPER, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sIdiPer = Ador(0).value
        Ador.MoveNext
        cmdA�adirPersona.caption = Ador(0).value '2
        Ador.MoveNext
        cmdCambiarRol.caption = Ador(0).value
        Ador.MoveNext
        cmdEliminarPersona.caption = Ador(0).value '2
        Ador.MoveNext
        cmdListadoPersona.caption = Ador(0).value
        Ador.MoveNext
        cmdRestaurarPersona.caption = Ador(0).value
        Ador.MoveNext
        frmREUPer.caption = Ador(0).value '7
        Ador.MoveNext
        sdbddRoles.Columns(0).caption = Ador(0).value '8 Cod
        Ador.MoveNext
        sdbddRoles.Columns(1).caption = Ador(0).value '9 Denominacion
        Ador.MoveNext
        sdbgPersonas.Columns(0).caption = Ador(0).value '10
        Ador.MoveNext
        sdbgPersonas.Columns(1).caption = Ador(0).value
        Ador.MoveNext
        sdbgPersonas.Columns(2).caption = Ador(0).value
        Ador.MoveNext
        sdbgPersonas.Columns(3).caption = Ador(0).value
        Ador.MoveNext
        sdbgPersonas.Columns(4).caption = Ador(0).value
        Ador.MoveNext
        sdbgPersonas.Columns(5).caption = Ador(0).value
        Ador.MoveNext
        sdbgPersonas.Columns(6).caption = Ador(0).value
        Ador.MoveNext
        sdbgPersonas.Columns(7).caption = Ador(0).value
        Ador.MoveNext
        sdbgPersonas.Columns(8).caption = Ador(0).value
        Ador.MoveNext
        sdbgPersonas.Columns(9).caption = Ador(0).value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub




Private Sub Form_Load()
    
    Me.Height = 4620
    Me.Width = 10290
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    CargarRecursos

    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    
    'Inicializamos las dropdown
    sdbddRoles.AddItem ""

    'Enlazamos las dropdown
    sdbgPersonas.Columns(0).DropDownHwnd = sdbddRoles.hWnd
    
    
    
End Sub
Public Sub AnyadirPersonasSeleccionadaReu()
Dim oIPersAsig As IPersonasAsig
Dim teserror As TipoErrorSummit
Dim oPers As CPersonas
Dim i As Integer
Dim oRoles As CRoles
    
    Set oPers = oFSGSRaiz.Generar_CPersonas
    Set oRoles = oFSGSRaiz.generar_CRoles

    oRoles.CargarTodosLosRoles , , , , , , basPublic.gParametrosInstalacion.gIdioma
    
    For i = 1 To frmSELPer.lstvwPer.ListItems.Count
        If Not oRoles.Item(frmSELPer.lstvwPer.ListItems(i).ListSubItems(1).Text) Is Nothing Then
            oPers.Add frmSELPer.lstvwPer.ListItems(i).Tag, "", "", , , , , , , , , i, frmSELPer.lstvwPer.ListItems(i).ListSubItems(1).Text, , , , , , , oRoles.Item(frmSELPer.lstvwPer.ListItems(i).ListSubItems(1).Text).Invi
        End If
    Next
    
    Set oRoles = Nothing
    
'    For i = 1 To frmSELPer.lstvwPer.ListItems.count
'
'        oPers.Add frmSELPer.lstvwPer.ListItems(i).Tag, "", "", , , , , , , , , i, frmSELPer.lstvwPer.ListItems(i).ListSubItems(1).Text
'    Next
    
    Set oIPersAsig = frmREU.oProcesoSeleccionado
    
    
    teserror = oIPersAsig.AsignarPersonas(oPers)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oIPersAsig = Nothing
        Exit Sub
    End If
    
    Dim sPersona As String
    Dim oPer As CPersona
    sPersona = ""
    For Each oPer In oPers
        sPersona = sPersona & oPer.Cod & " "
    Next
    RegistrarAccion ACCPlaReuMod, "Reunion:" & frmREU.oReuSeleccionada.Fecha & " Personas a�adidas:" & sPersona
    
    Set oIPersAsig = Nothing
    Set oPers = Nothing
    
    CargarGridConPersonas
    
    
End Sub

Private Sub CargarGridConRoles()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oRol As CRol
    
    sdbddRoles.RemoveAll
    
    For Each oRol In oRoles
        sdbddRoles.AddItem oRol.Cod & Chr(m_lSeparador) & oRol.Den
    Next
    
    
End Sub


Private Sub Form_Resize()
    
    If Me.Height < 1000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    sdbgPersonas.Height = Me.Height - 900
    sdbgPersonas.Width = Me.Width - 300
    
End Sub

Private Sub sdbddRoles_CloseUp()
Dim oIPersAsig As IPersonasAsig
Dim teserror As TipoErrorSummit

    Screen.MousePointer = vbHourglass
    Set oIPersAsig = frmREU.oProcesoSeleccionado
    teserror = oIPersAsig.CambiarRol(sdbgPersonas.Columns(1).value, sdbddRoles.Columns(0).value, sRolViejo)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        sdbgPersonas.DataChanged = False
    Else
        sdbgPersonas.Update
        basSeguridad.RegistrarAccion accionessummit.ACCProcePerMod, "Anyo:" & CStr(frmREU.oProcesoSeleccionado.Anyo) & "GMN1:" & CStr(frmREU.oProcesoSeleccionado.GMN1Cod) & "Proce:" & CStr(frmREU.oProcesoSeleccionado.Cod) & "Persona:" & sdbgPersonas.Columns(1).value
    End If
    
    Set oIPersAsig = Nothing
    
    sdbgPersonas.AllowUpdate = False
    sdbgPersonas.Refresh
    picNavigatePersona.Enabled = True
    Screen.MousePointer = vbNormal
            
End Sub

Private Sub sdbddRoles_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

    sdbddRoles.RemoveAll
    
    sRolViejo = sdbgPersonas.Columns(0).value
    
    Screen.MousePointer = vbHourglass
    Codigos = oRoles.DevolverLosCodigos
    Screen.MousePointer = vbNormal
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbddRoles.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
        
End Sub

Private Sub sdbddRoles_InitColumnProps()
      
    sdbddRoles.DataFieldList = "Column 0"
    sdbddRoles.DataFieldToDisplay = "Column 0"
    
End Sub


Private Sub sdbgPersonas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
  
    If sdbgPersonas.Col = 0 And sdbgPersonas.AllowUpdate = True Then
        sdbgPersonas.DroppedDown = True
        sdbddRoles.MoveFirst
    End If
    
End Sub

Private Sub sdbgPersonas_RowLoaded(ByVal Bookmark As Variant)
    If sdbgPersonas.Columns("BAJALOG").value Then
        Dim i As Integer
        For i = 0 To sdbgPersonas.Columns.Count - 1
            sdbgPersonas.Columns(i).CellStyleSet "BajaLog"
        Next
    End If
End Sub
