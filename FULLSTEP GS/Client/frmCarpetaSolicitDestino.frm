VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCarpetaSolicitArbol 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccionar Carpeta Destino"
   ClientHeight    =   5955
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7830
   Icon            =   "frmCarpetaSolicitDestino.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5955
   ScaleWidth      =   7830
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   2520
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   0
      Top             =   5400
      Width           =   3825
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   2
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   1
         Top             =   60
         Width           =   1050
      End
   End
   Begin MSComctlLib.TreeView tvwCarpetasSolicit 
      Height          =   5175
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   7560
      _ExtentX        =   13335
      _ExtentY        =   9128
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCarpetaSolicitDestino.frx":0CB2
            Key             =   "CSN4"
            Object.Tag             =   "CSN4"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCarpetaSolicitDestino.frx":0D62
            Key             =   "CSN0"
            Object.Tag             =   "CSN0"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCarpetaSolicitDestino.frx":11B4
            Key             =   "CSN1"
            Object.Tag             =   "CSN1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCarpetaSolicitDestino.frx":1264
            Key             =   "CSN2"
            Object.Tag             =   "CSN2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCarpetaSolicitDestino.frx":1314
            Key             =   "CSN3"
            Object.Tag             =   "CSN3"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCarpetaSolicitArbol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public oNodoSeleccionado As MSComctlLib.Node
Public oCarpetaSeleccionada As Object
'Public m_sCarpetaPreSelKey As String
'Public iNivelCarpeta As Integer

Private Sub cmdAceptar_Click()
    Dim Teserror As TipoErrorSummit
               
    Set oNodoSeleccionado = tvwCarpetasSolicit.SelectedItem

    If Not oNodoSeleccionado Is Nothing Then
         Set oCarpetaSeleccionada = frmSOLConfiguracion.DevolverCarpeta(Me.tvwCarpetasSolicit)
         'iNivelCarpeta = frmSOLConfiguracion.iNivel
         Me.Hide 'Lo descargo en el Formulario que lo ha llamado
     End If
End Sub

Private Sub cmdCancelar_Click()
    Me.Hide 'Lo descargo en el Formulario que lo ha llamado
End Sub

Private Sub Form_Load()
        
    Me.Top = frmSOLConfiguracion.Top + (frmSOLConfiguracion.Height / 2) - (Me.Height / 2)
    If Me.Top < 0 Then
        Me.Top = 0
    End If
    Me.Left = frmSOLConfiguracion.Left + (frmSOLConfiguracion.Width / 2) - (Me.Width / 2)
    If Me.Left < 0 Then
        Me.Left = 0
    End If
    
    CargarRecursos
        
    'ConfigurarSeguridad

    frmSOLConfiguracion.GenerarEstructuraCarpetas Me.tvwCarpetasSolicit, False, True
    
    'Preseleccionamos una carpeta:
'    If m_sCarpetaPreSelKey <> "" Then
'        tvwCarpetasSolicit.Nodes(m_sCarpetaPreSelKey).Selected = True
'    End If

    'Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CARPETASOLICITARBOL, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.Caption = Ador(0).Value '1
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
                
        Ador.Close
    End If
End Sub
