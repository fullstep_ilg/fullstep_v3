VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSolicitudesBusquedaSOLPadre 
   BackColor       =   &H00808000&
   Caption         =   "DBusqueda de solicitudes"
   ClientHeight    =   6120
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10845
   Icon            =   "frmSolicitudesBusquedaSOLPadre.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6120
   ScaleWidth      =   10845
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtFecDesde 
      Height          =   285
      Left            =   4995
      TabIndex        =   4
      Top             =   1005
      Width           =   990
   End
   Begin VB.TextBox txtFecHasta 
      Height          =   285
      Left            =   7005
      TabIndex        =   6
      Top             =   1005
      Width           =   990
   End
   Begin VB.CommandButton cmdCalFecDesde 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6015
      Picture         =   "frmSolicitudesBusquedaSOLPadre.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1005
      Width           =   315
   End
   Begin VB.CommandButton cmdCalFecHasta 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8025
      Picture         =   "frmSolicitudesBusquedaSOLPadre.frx":06D4
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1005
      Width           =   315
   End
   Begin VB.TextBox txtId 
      Height          =   285
      Left            =   1620
      TabIndex        =   1
      Top             =   630
      Width           =   1300
   End
   Begin VB.TextBox txtDescr 
      Height          =   285
      Left            =   4320
      TabIndex        =   2
      Top             =   630
      Width           =   4230
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   5760
      TabIndex        =   11
      Top             =   5760
      Width           =   1215
   End
   Begin VB.CommandButton cmdSeleccionar 
      Caption         =   "D&Seleccionar"
      Height          =   315
      Left            =   4320
      TabIndex        =   9
      Top             =   5760
      Width           =   1215
   End
   Begin VB.CommandButton cmdBuscar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9120
      Picture         =   "frmSolicitudesBusquedaSOLPadre.frx":0C5E
      Style           =   1  'Graphical
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Busqueda"
      Top             =   120
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgSolicitudes 
      Height          =   4095
      Left            =   120
      TabIndex        =   10
      Top             =   1560
      Width           =   10635
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   13
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).BackColor=   16777215
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSolicitudesBusquedaSOLPadre.frx":0FA0
      stylesets(1).Name=   "Header"
      stylesets(1).ForeColor=   0
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSolicitudesBusquedaSOLPadre.frx":0FBC
      BeveColorScheme =   0
      BevelColorFrame =   12632256
      BevelColorHighlight=   8421504
      BevelColorShadow=   128
      BevelColorFace  =   12632256
      CheckBox3D      =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      CellNavigation  =   1
      MaxSelectedRows =   1
      HeadStyleSet    =   "Header"
      ForeColorEven   =   0
      BackColorEven   =   12648447
      BackColorOdd    =   12648447
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   13
      Columns(0).Width=   2752
      Columns(0).Caption=   "TIPO"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   1
      Columns(0).ButtonsAlways=   -1  'True
      Columns(1).Width=   1667
      Columns(1).Caption=   "Alta"
      Columns(1).Name =   "ALTA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1667
      Columns(2).Caption=   "Necesidad"
      Columns(2).Name =   "NECESIDAD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1058
      Columns(3).Caption=   "Ident."
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   2910
      Columns(4).Caption=   "Denominaci�n"
      Columns(4).Name =   "DESCR"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1773
      Columns(5).Caption=   "Importe"
      Columns(5).Name =   "IMPORTE"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "standard"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2831
      Columns(6).Caption=   "Peticionario"
      Columns(6).Name =   "PET"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   1
      Columns(6).ButtonsAlways=   -1  'True
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Estado"
      Columns(7).Name =   "ESTADO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   2831
      Columns(8).Caption=   "Comprador"
      Columns(8).Name =   "COMP"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(8).Style=   1
      Columns(8).ButtonsAlways=   -1  'True
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID_ESTADO"
      Columns(9).Name =   "ID_ESTADO"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "COD_PER"
      Columns(10).Name=   "COD_PER"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "COD_COMP"
      Columns(11).Name=   "COD_COMP"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "DESTINATARIO_PROV"
      Columns(12).Name=   "DESTINATARIO_PROV"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      _ExtentX        =   18759
      _ExtentY        =   7223
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcTipoSolicit 
      Height          =   285
      Left            =   1620
      TabIndex        =   0
      Top             =   240
      Width           =   6975
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2249
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   9419
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   12294
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcPeticionario 
      Height          =   285
      Left            =   1620
      TabIndex        =   3
      Top             =   1005
      Width           =   2595
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4895
      Columns(1).Caption=   "PET"
      Columns(1).Name =   "PET"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4586
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblTipo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "DTipoSolicitud"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   1680
      TabIndex        =   18
      Top             =   240
      Width           =   1035
   End
   Begin VB.Label lblTipoSolicit 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "Dtipo de solicitud:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   240
      TabIndex        =   17
      Top             =   270
      Width           =   1260
   End
   Begin VB.Label lblFecDesde 
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "DDesde:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   4395
      TabIndex        =   16
      Top             =   1035
      Width           =   660
   End
   Begin VB.Label lblFecHasta 
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "DHasta:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   6405
      TabIndex        =   15
      Top             =   1035
      Width           =   645
   End
   Begin VB.Label lblIdentificador 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "DIdentificador:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   240
      TabIndex        =   14
      Top             =   660
      Width           =   1035
   End
   Begin VB.Label lblPeticionario 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "DPeticionario : "
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   240
      TabIndex        =   13
      Top             =   1035
      Width           =   1080
   End
   Begin VB.Label lblDescripcion 
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "DDescripci�n: "
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   3120
      TabIndex        =   12
      Top             =   660
      Width           =   1080
   End
   Begin VB.Shape ShapeSelecc 
      Height          =   1275
      Left            =   120
      Top             =   120
      Width           =   8595
   End
End
Attribute VB_Name = "frmSolicitudesBusquedaSOLPadre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Varibles Publicas
Public g_lIdRefSol As Long

'Variables Privadas
Private m_oInstancias As CInstancias

Private Sub cmdBuscar_Click()
    Dim oInstancia As CInstancia
    Dim sPet As String
    Dim vEstado() As Variant
    Dim iOrden As TipoOrdenacionSolicitudes
    Dim vFecNec As Variant
    Dim lTipo As Long
    Dim sAsignadoA As String
    Dim sCodAsignadoA As String

    sdbgSolicitudes.RemoveAll
    If g_lIdRefSol > 0 Then
        lTipo = g_lIdRefSol
    Else
        If sdbcTipoSolicit.Text <> "" Then
            lTipo = sdbcTipoSolicit.Columns("ID").Value
        End If
    End If
    
    If sdbcPeticionario.Text <> "" Then
        sPet = sdbcPeticionario.Columns(0).Value
    End If
    
   ReDim Preserve vEstado(1)
   vEstado(1) = EstadoSolicitud.Pendiente
    
    
    Set m_oInstancias = oFSGSRaiz.Generar_CInstancias
    iOrden = OrdSolicPorfecalta
    m_oInstancias.BuscarSolicitudes iOrden, lTipo, txtId.Text, txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text
    
    For Each oInstancia In m_oInstancias
         If IsNull(oInstancia.FecNecesidad) Then
             vFecNec = ""
         Else
             vFecNec = Format(oInstancia.FecNecesidad, "Short date")
         End If
         
         sAsignadoA = ""
         sCodAsignadoA = ""
        
         If Not oInstancia.comprador Is Nothing Then
             sAsignadoA = oInstancia.comprador.Cod & " - " & NullToStr(oInstancia.comprador.nombre) & " " & NullToStr(oInstancia.comprador.Apel)
             sCodAsignadoA = oInstancia.comprador.Cod
        
         ElseIf Not oInstancia.Solicitud.Gestor Is Nothing Then
             sAsignadoA = oInstancia.Solicitud.Gestor.Cod & " - " & NullToStr(oInstancia.Solicitud.Gestor.nombre) & " " & NullToStr(oInstancia.Solicitud.Gestor.Apellidos)
             sCodAsignadoA = oInstancia.Solicitud.Gestor.Cod
         End If
         
         sdbgSolicitudes.AddItem NullToStr(oInstancia.Solicitud.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & Format(oInstancia.FecAlta, "Short date") & Chr(m_lSeparador) & vFecNec & Chr(m_lSeparador) & oInstancia.Id & Chr(m_lSeparador) & oInstancia.DescrBreve & Chr(m_lSeparador) & oInstancia.importe / oInstancia.Cambio & Chr(m_lSeparador) & oInstancia.Peticionario.Cod & " - " & NullToStr(oInstancia.Peticionario.nombre) & " " & NullToStr(oInstancia.Peticionario.Apellidos) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & sAsignadoA & Chr(m_lSeparador) & oInstancia.Estado & Chr(m_lSeparador) & oInstancia.Peticionario.Cod & Chr(m_lSeparador) & sCodAsignadoA & Chr(m_lSeparador) & ""
    Next
    
    sdbgSolicitudes.MoveFirst
    
    Set oInstancia = Nothing

    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdSeleccionar_Click()
    If sdbgSolicitudes.Columns("ID").Value <> "" Then
        frmSolicitudDetalle.m_lIDSOLPadre = sdbgSolicitudes.Columns("ID").Value
        Unload Me
    End If
End Sub

Private Sub Form_Load()

    CargarRecursos
    
    PonerFieldSeparator Me

    If g_lIdRefSol > 0 Then
        Dim oSolicitud As CSolicitud
        
        Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
        
        oSolicitud.Id = g_lIdRefSol
        oSolicitud.CargarDatosSolicitud
        If Not oSolicitud.Denominaciones.EOF Then
            lblTipo.caption = oSolicitud.Codigo & " - " & oSolicitud.Denominaciones.Item(gParametrosGenerales.gIdioma).Den
        Else
            lblTipo.caption = g_lIdRefSol
        End If
        
        sdbcTipoSolicit.Visible = False
        lblTipo.Visible = True
    Else
        lblTipo.Visible = False
        sdbcTipoSolicit.Visible = True
    End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_BUSQUEDASOLICITUDESPADRE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value        '1 B�squeda de solicitudes
        Ador.MoveNext
        lblTipoSolicit.caption = Ador(0).Value  '2 Tipo de solicitud:
        Ador.MoveNext
        lblIdentificador.caption = Ador(0).Value  '3 Identificador:
        Ador.MoveNext
        lblDescripcion.caption = Ador(0).Value  '4 Descripci�n:
        Ador.MoveNext
        lblPeticionario.caption = Ador(0).Value  '5  Peticionario:
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value  '6  Desde:
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value  '7  Hasta:
        
        'Columnas GRID
        Ador.MoveNext
        sdbgSolicitudes.Columns("TIPO").caption = Ador(0).Value '8  Tipo
        Ador.MoveNext
        sdbgSolicitudes.Columns("ALTA").caption = Ador(0).Value '9 Alta
        Ador.MoveNext
        sdbgSolicitudes.Columns("NECESIDAD").caption = Ador(0).Value '10 Necesidad
        Ador.MoveNext
        sdbgSolicitudes.Columns("ID").caption = Ador(0).Value '11 Ident.
        Ador.MoveNext
        sdbgSolicitudes.Columns("DESCR").caption = Ador(0).Value '12 Descripci�n breve
        Ador.MoveNext
        sdbgSolicitudes.Columns("IMPORTE").caption = Ador(0).Value '13 Importe
        Ador.MoveNext
        sdbgSolicitudes.Columns("PET").caption = Ador(0).Value '14 Peticionario
        Ador.MoveNext
        sdbgSolicitudes.Columns("COMP").caption = Ador(0).Value  'Comprador
        
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value  '16 &Seleccionar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value  '17 &Cancelar
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_lIdRefSol = 0
    Set m_oInstancias = Nothing
End Sub

Private Sub sdbcTipoSolicit_Click()
    If Not sdbcTipoSolicit.DroppedDown Then
        sdbcTipoSolicit = ""
    End If
End Sub

Private Sub sdbcTipoSolicit_CloseUp()
    If sdbcTipoSolicit.Value = "..." Or sdbcTipoSolicit.Value = "" Then
        sdbcTipoSolicit.Text = ""
        Exit Sub
    End If
    sdbcTipoSolicit.Text = sdbcTipoSolicit.Columns(1).Text
End Sub

Private Sub sdbcTipoSolicit_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
    Dim oSolic As CSolicitud
    Dim oSolicitudes As CSolicitudes
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoSolicit.RemoveAll
    
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    oSolicitudes.CargarTodasSolicitudesDeCompras

    For Each oSolic In oSolicitudes
        sdbcTipoSolicit.AddItem oSolic.Codigo & Chr(m_lSeparador) & NullToStr(oSolic.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & oSolic.Id
    Next
        
    Set oSolicitudes = Nothing
    
    sdbcTipoSolicit.SelStart = 0
    sdbcTipoSolicit.SelLength = Len(sdbcTipoSolicit.Text)
    sdbcTipoSolicit.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcTipoSolicit_InitColumnProps()
    sdbcTipoSolicit.DataFieldList = "Column 1"
    sdbcTipoSolicit.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcTipoSolicit_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoSolicit.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoSolicit.Rows - 1
            bm = sdbcTipoSolicit.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcTipoSolicit.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcTipoSolicit.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcPeticionario_Click()
    If Not sdbcPeticionario.DroppedDown Then
        sdbcPeticionario = ""
    End If
End Sub

Private Sub sdbcPeticionario_CloseUp()
    If sdbcPeticionario.Value = "..." Or sdbcPeticionario = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
    sdbcPeticionario.Text = sdbcPeticionario.Columns(1).Text
End Sub


Private Sub sdbcPeticionario_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
    Dim oPersona As CPersona
    Dim oPersonas As CPersonas
    
    Screen.MousePointer = vbHourglass
    
    sdbcPeticionario.RemoveAll
    
    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    oPersonas.CargarPeticionariosSolCompra (True)

    For Each oPersona In oPersonas
        sdbcPeticionario.AddItem oPersona.Cod & Chr(m_lSeparador) & oPersona.Cod & " - " & NullToStr(oPersona.nombre) & " " & oPersona.Apellidos
    Next
        
    Set oPersonas = Nothing
    
    sdbcPeticionario.SelStart = 0
    sdbcPeticionario.SelLength = Len(sdbcPeticionario.Text)
    sdbcPeticionario.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPeticionario_InitColumnProps()
    sdbcPeticionario.DataFieldList = "Column 1"
    sdbcPeticionario.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcPeticionario_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPeticionario.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPeticionario.Rows - 1
            bm = sdbcPeticionario.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPeticionario.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPeticionario.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgSolicitudes_BtnClick()
    If sdbgSolicitudes.Col = -1 Then Exit Sub
    
    Select Case sdbgSolicitudes.Columns(sdbgSolicitudes.Col).Name
        Case "TIPO"
            If Not m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Solicitud Is Nothing Then
                Set frmDetTipoSolicitud.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Solicitud
                frmDetTipoSolicitud.Show vbModal
            End If
            
        Case "PET"
            frmDetallePersona.g_sPersona = sdbgSolicitudes.Columns("COD_PER").Value
            frmDetallePersona.Show vbModal
            
        Case "COMP"
            frmDetallePersona.g_sPersona = sdbgSolicitudes.Columns("COD_COMP").Value
            frmDetallePersona.Show vbModal
    End Select
End Sub

Private Sub txtFecDesde_LostFocus()
    If txtFecDesde.Text <> "" Then
        If Not IsDate(txtFecDesde.Text) Then
            oMensajes.NoValido txtFecDesde.Text
            If Me.Visible Then txtFecDesde.SetFocus
            Exit Sub
        End If
    End If
End Sub

Private Sub txtFecHasta_LostFocus()
    If txtFecHasta.Text <> "" Then
        If Not IsDate(txtFecHasta.Text) Then
            oMensajes.NoValido txtFecHasta.Text
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
End Sub
