VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{14ACBB92-9C4A-4C45-AFD2-7AE60E71E5B3}#4.0#0"; "IGSplitter40.ocx"
Begin VB.Form frmSolicitudBuscar 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "B�squeda de solicitudes"
   ClientHeight    =   7845
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12975
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7845
   ScaleWidth      =   12975
   StartUpPosition =   1  'CenterOwner
   Begin SSSplitter.SSSplitter splitterMain 
      Height          =   7140
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   14355
      _ExtentX        =   25321
      _ExtentY        =   12594
      _Version        =   262144
      BackColor       =   8421376
      PaneTree        =   "frmSolicitudBuscar.frx":014A
      Begin VB.PictureBox picBottom 
         BackColor       =   &H00808000&
         Height          =   3360
         Left            =   30
         ScaleHeight     =   3300
         ScaleWidth      =   14235
         TabIndex        =   5
         Top             =   3750
         Width           =   14295
         Begin VB.PictureBox picGridToolbar 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   0
            ScaleHeight     =   375
            ScaleWidth      =   13875
            TabIndex        =   60
            Top             =   0
            Width           =   13875
            Begin VB.CommandButton cmdFirst 
               Height          =   285
               Left            =   30
               Picture         =   "frmSolicitudBuscar.frx":019C
               Style           =   1  'Graphical
               TabIndex        =   64
               Top             =   45
               Width           =   315
            End
            Begin VB.CommandButton cmdPrevious 
               Height          =   285
               Left            =   375
               Picture         =   "frmSolicitudBuscar.frx":0248
               Style           =   1  'Graphical
               TabIndex        =   63
               Top             =   45
               Width           =   315
            End
            Begin VB.CommandButton cmdNext 
               Height          =   285
               Left            =   2475
               Picture         =   "frmSolicitudBuscar.frx":02EA
               Style           =   1  'Graphical
               TabIndex        =   62
               Top             =   45
               Width           =   315
            End
            Begin VB.CommandButton cmdLast 
               Height          =   285
               Left            =   2820
               Picture         =   "frmSolicitudBuscar.frx":038C
               Style           =   1  'Graphical
               TabIndex        =   61
               Top             =   45
               Width           =   315
            End
            Begin VB.Label lblPageNumber 
               Alignment       =   2  'Center
               BackColor       =   &H00808000&
               Caption         =   "DPagina {n} de {m}"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   735
               TabIndex        =   65
               Top             =   90
               Width           =   1680
            End
         End
         Begin VB.PictureBox picGrid 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   3210
            Left            =   0
            ScaleHeight     =   3210
            ScaleWidth      =   13635
            TabIndex        =   6
            Top             =   375
            Width           =   13635
            Begin SSDataWidgets_B.SSDBGrid sdbgSolicitudes 
               Height          =   1920
               Left            =   0
               TabIndex        =   8
               Top             =   0
               Width           =   11055
               ScrollBars      =   2
               _Version        =   196617
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   13
               stylesets.count =   9
               stylesets(0).Name=   "Normal"
               stylesets(0).BackColor=   16777215
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmSolicitudBuscar.frx":0439
               stylesets(1).Name=   "Seleccion"
               stylesets(1).ForeColor=   16777215
               stylesets(1).BackColor=   8388608
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmSolicitudBuscar.frx":0455
               stylesets(2).Name=   "Curso"
               stylesets(2).BackColor=   12648447
               stylesets(2).HasFont=   -1  'True
               BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(2).Picture=   "frmSolicitudBuscar.frx":0471
               stylesets(3).Name=   "Anulada"
               stylesets(3).ForeColor=   0
               stylesets(3).BackColor=   4744445
               stylesets(3).HasFont=   -1  'True
               BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(3).Picture=   "frmSolicitudBuscar.frx":048D
               stylesets(4).Name=   "Cerrada"
               stylesets(4).ForeColor=   0
               stylesets(4).BackColor=   10079487
               stylesets(4).HasFont=   -1  'True
               BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(4).Picture=   "frmSolicitudBuscar.frx":04A9
               stylesets(5).Name=   "Pendiente"
               stylesets(5).ForeColor=   0
               stylesets(5).BackColor=   16777215
               stylesets(5).HasFont=   -1  'True
               BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(5).Picture=   "frmSolicitudBuscar.frx":04C5
               stylesets(6).Name=   "Header"
               stylesets(6).ForeColor=   0
               stylesets(6).BackColor=   -2147483633
               stylesets(6).HasFont=   -1  'True
               BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(6).Picture=   "frmSolicitudBuscar.frx":04E1
               stylesets(7).Name=   "Aprobada"
               stylesets(7).ForeColor=   0
               stylesets(7).BackColor=   10409635
               stylesets(7).HasFont=   -1  'True
               BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(7).Picture=   "frmSolicitudBuscar.frx":04FD
               stylesets(8).Name=   "Rechazada"
               stylesets(8).ForeColor=   0
               stylesets(8).BackColor=   12632256
               stylesets(8).HasFont=   -1  'True
               BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(8).Picture=   "frmSolicitudBuscar.frx":0519
               BeveColorScheme =   0
               BevelColorFrame =   12632256
               BevelColorHighlight=   8421504
               BevelColorShadow=   128
               BevelColorFace  =   12632256
               CheckBox3D      =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               CellNavigation  =   1
               MaxSelectedRows =   1
               HeadStyleSet    =   "Header"
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               ExtraHeight     =   26
               Columns.Count   =   13
               Columns(0).Width=   2143
               Columns(0).Caption=   "TIPO"
               Columns(0).Name =   "TIPO"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(0).Style=   1
               Columns(0).ButtonsAlways=   -1  'True
               Columns(1).Width=   1667
               Columns(1).Caption=   "Alta"
               Columns(1).Name =   "ALTA"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(1).Locked=   -1  'True
               Columns(2).Width=   1667
               Columns(2).Caption=   "Necesidad"
               Columns(2).Name =   "NECESIDAD"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).Locked=   -1  'True
               Columns(3).Width=   1058
               Columns(3).Caption=   "Ident."
               Columns(3).Name =   "ID"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(3).Locked=   -1  'True
               Columns(4).Width=   2910
               Columns(4).Caption=   "Descripci�n breve"
               Columns(4).Name =   "DESCR"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(4).Locked=   -1  'True
               Columns(5).Width=   1773
               Columns(5).Caption=   "Importe"
               Columns(5).Name =   "IMPORTE"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).NumberFormat=   "standard"
               Columns(5).FieldLen=   256
               Columns(5).Locked=   -1  'True
               Columns(6).Width=   2328
               Columns(6).Caption=   "Peticionario"
               Columns(6).Name =   "PET"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               Columns(6).Locked=   -1  'True
               Columns(6).Style=   1
               Columns(6).ButtonsAlways=   -1  'True
               Columns(7).Width=   1826
               Columns(7).Caption=   "Estado"
               Columns(7).Name =   "ESTADO"
               Columns(7).DataField=   "Column 7"
               Columns(7).DataType=   8
               Columns(7).FieldLen=   256
               Columns(8).Width=   2540
               Columns(8).Caption=   "Comprador"
               Columns(8).Name =   "COMP"
               Columns(8).DataField=   "Column 8"
               Columns(8).DataType=   8
               Columns(8).FieldLen=   256
               Columns(8).Locked=   -1  'True
               Columns(8).Style=   1
               Columns(8).ButtonsAlways=   -1  'True
               Columns(9).Width=   3200
               Columns(9).Visible=   0   'False
               Columns(9).Caption=   "ID_ESTADO"
               Columns(9).Name =   "ID_ESTADO"
               Columns(9).DataField=   "Column 9"
               Columns(9).DataType=   8
               Columns(9).FieldLen=   256
               Columns(10).Width=   3200
               Columns(10).Visible=   0   'False
               Columns(10).Caption=   "COD_PER"
               Columns(10).Name=   "COD_PER"
               Columns(10).DataField=   "Column 10"
               Columns(10).DataType=   8
               Columns(10).FieldLen=   256
               Columns(11).Width=   3200
               Columns(11).Visible=   0   'False
               Columns(11).Caption=   "COD_COMP"
               Columns(11).Name=   "COD_COMP"
               Columns(11).DataField=   "Column 11"
               Columns(11).DataType=   8
               Columns(11).FieldLen=   256
               Columns(12).Width=   3200
               Columns(12).Visible=   0   'False
               Columns(12).Caption=   "DESTINATARIO_PROV"
               Columns(12).Name=   "DESTINATARIO_PROV"
               Columns(12).DataField=   "Column 12"
               Columns(12).DataType=   8
               Columns(12).FieldLen=   256
               _ExtentX        =   19500
               _ExtentY        =   3387
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
      Begin VB.PictureBox picTop 
         BackColor       =   &H00808000&
         Height          =   3630
         Left            =   30
         ScaleHeight     =   3570
         ScaleWidth      =   14235
         TabIndex        =   4
         Top             =   30
         Width           =   14295
         Begin VB.TextBox txtFecDesde 
            Height          =   285
            Left            =   4635
            TabIndex        =   49
            Top             =   960
            Width           =   1110
         End
         Begin VB.TextBox txtFecHasta 
            Height          =   285
            Left            =   6675
            TabIndex        =   48
            Top             =   960
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5775
            Picture         =   "frmSolicitudBuscar.frx":0535
            Style           =   1  'Graphical
            TabIndex        =   47
            TabStop         =   0   'False
            Top             =   975
            Width           =   315
         End
         Begin VB.CommandButton cmdCalFecHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7815
            Picture         =   "frmSolicitudBuscar.frx":0ABF
            Style           =   1  'Graphical
            TabIndex        =   46
            TabStop         =   0   'False
            Top             =   975
            Width           =   315
         End
         Begin VB.TextBox txtId 
            Height          =   285
            Left            =   1425
            TabIndex        =   45
            Top             =   585
            Width           =   1300
         End
         Begin VB.TextBox txtDescr 
            Height          =   285
            Left            =   3915
            TabIndex        =   44
            Top             =   585
            Width           =   3840
         End
         Begin VB.CommandButton cmdBuscar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   12585
            Picture         =   "frmSolicitudBuscar.frx":1049
            Style           =   1  'Graphical
            TabIndex        =   43
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   30
            Width           =   315
         End
         Begin VB.PictureBox picEstado 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   1245
            Left            =   9960
            ScaleHeight     =   1245
            ScaleWidth      =   2475
            TabIndex        =   37
            Top             =   75
            Width           =   2475
            Begin VB.CheckBox chkEstado 
               BackColor       =   &H00808000&
               Caption         =   "DCerradas"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   3
               Left            =   0
               TabIndex        =   42
               Top             =   945
               Width           =   1215
            End
            Begin VB.CheckBox chkEstado 
               BackColor       =   &H00808000&
               Caption         =   "DAnuladas"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   2
               Left            =   0
               TabIndex        =   41
               Top             =   705
               Width           =   1335
            End
            Begin VB.CheckBox chkEstado 
               BackColor       =   &H00808000&
               Caption         =   "DAprobadas"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   1
               Left            =   0
               TabIndex        =   40
               Top             =   480
               Value           =   1  'Checked
               Width           =   1335
            End
            Begin VB.CheckBox chkEstado 
               BackColor       =   &H00808000&
               Caption         =   "DEn curso de aprobaci�n"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   4
               Left            =   0
               TabIndex        =   39
               Top             =   0
               Width           =   2415
            End
            Begin VB.CheckBox chkEstado 
               BackColor       =   &H00808000&
               Caption         =   "DPendientes"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   5
               Left            =   0
               TabIndex        =   38
               Top             =   240
               Width           =   2115
            End
         End
         Begin VB.PictureBox picBusquedaAvanzada 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   2025
            Left            =   30
            ScaleHeight     =   2025
            ScaleWidth      =   13920
            TabIndex        =   7
            Top             =   1455
            Visible         =   0   'False
            Width           =   13920
            Begin VB.CommandButton cmdLimpiarContrato 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   5400
               Picture         =   "frmSolicitudBuscar.frx":138B
               Style           =   1  'Graphical
               TabIndex        =   36
               Top             =   1275
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarContrato 
               Height          =   285
               Index           =   0
               Left            =   5745
               Picture         =   "frmSolicitudBuscar.frx":1430
               Style           =   1  'Graphical
               TabIndex        =   35
               Top             =   1275
               Width           =   315
            End
            Begin VB.ListBox lstContrato 
               Height          =   285
               IntegralHeight  =   0   'False
               Left            =   1725
               TabIndex        =   34
               Top             =   1470
               Visible         =   0   'False
               Width           =   3960
            End
            Begin VB.ListBox lstCentroCoste 
               Height          =   285
               IntegralHeight  =   0   'False
               Left            =   1770
               TabIndex        =   33
               Top             =   1005
               Visible         =   0   'False
               Width           =   3960
            End
            Begin VB.TextBox txtMateriales 
               BackColor       =   &H80000018&
               Height          =   285
               Left            =   1410
               Locked          =   -1  'True
               TabIndex        =   18
               Top             =   90
               Width           =   3960
            End
            Begin VB.CommandButton cmdLimpiarMateriales 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5400
               Picture         =   "frmSolicitudBuscar.frx":17B2
               Style           =   1  'Graphical
               TabIndex        =   17
               Top             =   105
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarMateriales 
               Height          =   285
               Left            =   5745
               Picture         =   "frmSolicitudBuscar.frx":1857
               Style           =   1  'Graphical
               TabIndex        =   16
               Top             =   105
               Width           =   315
            End
            Begin VB.TextBox txtCentroCoste 
               Height          =   285
               Left            =   1410
               TabIndex        =   15
               Top             =   870
               Width           =   3960
            End
            Begin VB.TextBox txtContrato 
               Height          =   285
               Index           =   0
               Left            =   1410
               TabIndex        =   14
               Top             =   1260
               Width           =   3960
            End
            Begin VB.TextBox txtImporteHasta 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   10455
               TabIndex        =   13
               Top             =   1260
               Width           =   1290
            End
            Begin VB.TextBox txtImporteDesde 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   7785
               TabIndex        =   12
               Top             =   1260
               Width           =   1290
            End
            Begin VB.TextBox txtDenArticulo 
               Height          =   285
               Left            =   3180
               TabIndex        =   11
               Top             =   480
               Width           =   2865
            End
            Begin VB.CommandButton cmdLimpiarCentroCoste 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5400
               Picture         =   "frmSolicitudBuscar.frx":1BD9
               Style           =   1  'Graphical
               TabIndex        =   10
               Top             =   885
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarCentroCoste 
               Height          =   285
               Left            =   5745
               Picture         =   "frmSolicitudBuscar.frx":1C7E
               Style           =   1  'Graphical
               TabIndex        =   9
               Top             =   885
               Width           =   315
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEmpresa 
               Height          =   285
               Left            =   7785
               TabIndex        =   19
               Top             =   90
               Width           =   4665
               DataFieldList   =   "Column 1"
               ListAutoValidate=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2593
               Columns(1).Caption=   "NIF"
               Columns(1).Name =   "NIF"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   5477
               Columns(2).Caption=   "DEN"
               Columns(2).Name =   "DEN"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "NIF_DEN"
               Columns(3).Name =   "NIF_DEN"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   8229
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               DataFieldToDisplay=   "Column 3"
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcComprador 
               Height          =   285
               Left            =   7785
               TabIndex        =   20
               Top             =   870
               Width           =   4665
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               HeadLines       =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8229
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   8229
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcDepartamento 
               Height          =   285
               Left            =   7785
               TabIndex        =   21
               Top             =   480
               Width           =   4665
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2619
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   5477
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   8229
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcArticulo 
               Height          =   285
               Left            =   1410
               TabIndex        =   22
               Top             =   480
               Width           =   1740
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2408
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4630
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3069
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBGrid sdbgPartidas 
               Height          =   855
               Left            =   1410
               TabIndex        =   66
               Top             =   1380
               Visible         =   0   'False
               Width           =   4695
               _Version        =   196617
               DataMode        =   2
               GroupHeaders    =   0   'False
               ColumnHeaders   =   0   'False
               Col.Count       =   11
               stylesets.count =   2
               stylesets(0).Name=   "Rojo"
               stylesets(0).BackColor=   8421631
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmSolicitudBuscar.frx":2000
               stylesets(0).AlignmentText=   0
               stylesets(1).Name=   "Verde"
               stylesets(1).BackColor=   8454016
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmSolicitudBuscar.frx":201C
               stylesets(1).AlignmentText=   0
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowColumnSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               ExtraHeight     =   265
               Columns.Count   =   11
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(1).HasBackColor=   -1  'True
               Columns(1).BackColor=   12632256
               Columns(2).Width=   3200
               Columns(2).Caption=   "VALOR"
               Columns(2).Name =   "VALOR"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).Style=   1
               Columns(2).ButtonsAlways=   -1  'True
               Columns(2).HasBackColor=   -1  'True
               Columns(2).BackColor=   16777215
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "NIVEL"
               Columns(3).Name =   "NIVEL"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   3200
               Columns(4).Visible=   0   'False
               Columns(4).Caption=   "PRES5"
               Columns(4).Name =   "PRES5NIV1"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(5).Width=   3200
               Columns(5).Visible=   0   'False
               Columns(5).Caption=   "PRES5UON"
               Columns(5).Name =   "PRES5UON"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(6).Width=   3200
               Columns(6).Visible=   0   'False
               Columns(6).Caption=   "VALIDADO"
               Columns(6).Name =   "VALIDADO"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               Columns(7).Width=   3200
               Columns(7).Visible=   0   'False
               Columns(7).Caption=   "PRES5NIV2"
               Columns(7).Name =   "PRES5NIV2"
               Columns(7).DataField=   "Column 7"
               Columns(7).DataType=   8
               Columns(7).FieldLen=   256
               Columns(8).Width=   3200
               Columns(8).Visible=   0   'False
               Columns(8).Caption=   "PRES5NIV3"
               Columns(8).Name =   "PRES5NIV3"
               Columns(8).DataField=   "Column 8"
               Columns(8).DataType=   8
               Columns(8).FieldLen=   256
               Columns(9).Width=   3200
               Columns(9).Visible=   0   'False
               Columns(9).Caption=   "PRES5NIV4"
               Columns(9).Name =   "PRES5NIV4"
               Columns(9).DataField=   "Column 9"
               Columns(9).DataType=   8
               Columns(9).FieldLen=   256
               Columns(10).Width=   3200
               Columns(10).Visible=   0   'False
               Columns(10).Caption=   "TAG"
               Columns(10).Name=   "TAG"
               Columns(10).DataField=   "Column 10"
               Columns(10).DataType=   8
               Columns(10).FieldLen=   256
               _ExtentX        =   8281
               _ExtentY        =   1508
               _StockProps     =   79
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Shape shapeBusquedaAvanzada 
               Height          =   1935
               Left            =   15
               Top             =   15
               Width           =   12495
            End
            Begin VB.Label lblOcultarBusquedaAvanzada 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DOcultar"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   10845
               MouseIcon       =   "frmSolicitudBuscar.frx":2038
               MousePointer    =   99  'Custom
               TabIndex        =   59
               Top             =   1650
               Width           =   630
            End
            Begin VB.Label lblMateriales 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DMateriales:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   90
               TabIndex        =   32
               Top             =   135
               Width           =   900
            End
            Begin VB.Label lblEmpresa 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DEmpresa:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   6465
               TabIndex        =   31
               Top             =   135
               Width           =   780
            End
            Begin VB.Label lblMon 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "MONCEN"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   11820
               TabIndex        =   30
               Top             =   1290
               Width           =   645
            End
            Begin VB.Label lblArticulo 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DArticulo:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   90
               TabIndex        =   29
               Top             =   555
               Width           =   705
            End
            Begin VB.Label lblImporteDesde 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DImporte desde:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   6465
               TabIndex        =   28
               Top             =   1290
               Width           =   1215
            End
            Begin VB.Label lblCentroCoste 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DCentro coste:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   90
               TabIndex        =   27
               Top             =   915
               Width           =   1095
            End
            Begin VB.Label lblImporteHasta 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DImporte hasta:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   9225
               TabIndex        =   26
               Top             =   1290
               Width           =   1185
            End
            Begin VB.Label lblDepartamento 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DDepartamento:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   6465
               TabIndex        =   25
               Top             =   555
               Width           =   1200
            End
            Begin VB.Label lblComprador 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DComprador:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   6465
               TabIndex        =   24
               Top             =   915
               Width           =   960
            End
            Begin VB.Label lblContrato 
               BackColor       =   &H00808000&
               Caption         =   "DContrato:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   0
               Left            =   90
               TabIndex        =   23
               Top             =   1290
               Width           =   1185
            End
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPeticionario 
            Height          =   285
            Left            =   1425
            TabIndex        =   50
            Top             =   960
            Width           =   2475
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4895
            Columns(1).Caption=   "PET"
            Columns(1).Name =   "PET"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4366
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoSolicit 
            Height          =   285
            Left            =   1425
            TabIndex        =   51
            Top             =   195
            Width           =   6345
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2249
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   9419
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "ID"
            Columns(2).Name =   "ID"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   11192
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblMostrarBusquedaAvanzada 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "DBusqueda avanzada"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   7950
            MouseIcon       =   "frmSolicitudBuscar.frx":218A
            MousePointer    =   99  'Custom
            TabIndex        =   58
            Top             =   645
            Width           =   1575
         End
         Begin VB.Label lblFecDesde 
            BackColor       =   &H00808000&
            Caption         =   "DDesde:"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   3990
            TabIndex        =   57
            Top             =   990
            Width           =   660
         End
         Begin VB.Label lblFecHasta 
            BackColor       =   &H00808000&
            Caption         =   "Hasta:"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   6135
            TabIndex        =   56
            Top             =   990
            Width           =   645
         End
         Begin VB.Label lblIdentificador 
            BackColor       =   &H00808000&
            Caption         =   "DIdentificador: : "
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   90
            TabIndex        =   55
            Top             =   615
            Width           =   1305
         End
         Begin VB.Label lblPeticionario 
            BackColor       =   &H00808000&
            Caption         =   "DPeticionario : "
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   90
            TabIndex        =   54
            Top             =   990
            Width           =   1305
         End
         Begin VB.Label lblDescripcion 
            BackColor       =   &H00808000&
            Caption         =   "DDescripci�n: "
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   2865
            TabIndex        =   53
            Top             =   615
            Width           =   900
         End
         Begin VB.Shape ShapeSelecc 
            Height          =   1410
            Left            =   30
            Top             =   15
            Width           =   12510
         End
         Begin VB.Label lblTipoSolicit 
            BackColor       =   &H00808000&
            Caption         =   "Dtipo de solicitud:"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   90
            TabIndex        =   52
            Top             =   225
            Width           =   1305
         End
      End
   End
   Begin VB.PictureBox picSeleccion 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   570
      Left            =   0
      ScaleHeight     =   570
      ScaleWidth      =   12975
      TabIndex        =   0
      Top             =   7275
      Width           =   12975
      Begin VB.CommandButton cmdSeleccionar 
         Caption         =   "&Seleccionar"
         Height          =   315
         Left            =   5235
         TabIndex        =   2
         Top             =   135
         Width           =   1215
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   6555
         TabIndex        =   1
         Top             =   135
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmSolicitudBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sIncrementoAltura As Single

Private Const PANE0_MIN_HEIGHT As Integer = 3850
Private Const PANE0_INIT_HEIGHT As Integer = 1650

'Variables p�blicas
Public g_sOrigen As String
Public g_iCol As Integer
Public g_iLstGrupoItem As Integer
Public g_lId As Long

Public g_oCenCos As CCentroCoste
Public g_oPartida As CContratopres

'Variables privadas
Private m_oInstancias As CInstancias

Private m_lColumnaOrden As Long

'Seguridad
Private m_bRuo As Boolean
Private m_bREquipo As Boolean
Private m_bRAsig As Boolean
Private m_bAbrirProcEnCurso As Boolean
Private m_bRestrMatComp As Boolean

'Variables de idiomas:
Private m_sPendiente As String 'sra
Private m_sRechazada As String 'sra
Private m_sAprobada As String
Private m_sAnulada As String
Private m_sCerrada As String
Private sId As String
Private sImporteHasta As String
Private sImporteDesde As String
Private sFecDesde As String
Private sFecHasta As String
Private sOrdenando As String
Private m_sProveedor As String
Private m_sPageNumber As String

Private m_bRespetarCombo As Boolean

'filtro de materiales
Private m_sGMN1Cod As String
Private m_sGMN2Cod As String
Private m_sGMN3Cod As String
Private m_sGMN4Cod As String

'Para el filtro por materiales/articulos
Private m_bEditandoArticulo As Boolean
Private m_bEditandoArticuloConDropDown As Boolean
Private m_bEditandoArticuloDen As Boolean
Private m_oGMN4Seleccionado As CGrupoMatNivel4

'Para el filtro de centros de centro coste
Private m_sUON1_CC As String
Private m_sUON2_CC As String
Private m_sUON3_CC As String
Private m_sUON4_CC As String
Private m_sUON_CC As String
Private m_bCentroCosteValidado As Boolean
Private m_oCentrosCoste As CCentrosCoste

'Para el filtro de partidas
Private m_asPres5Niv0() As String
Private m_aiNivelImp() As Integer
Private m_asPres5UON() As String
Private m_aPres5() As TipoConfigVisorSolPP
Private m_bPres5Validado As Boolean
Private m_oContratosPres As CContratosPres

'Tarea 3369
Private m_bRSolicPerfUON As Boolean
Private m_bRSolicDep As Boolean
Private m_lIdPerfil As Long
Private g_oConfVisorSol As CConfVistaVisorSol
Private g_oConfVisorSolPPs As CConfVistasVisorSolPP
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub chkEstado_Click(Index As Integer)
    Dim i As Integer
    Dim bCancelar As Boolean
    
    'cuando se deschequea se comprueba si existe alg�n check m�s marcado.En caso contrario
    'no permite quitar el check
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkEstado(Index).Value = vbUnchecked Then
        bCancelar = True
        For i = 1 To chkEstado.Count
            If chkEstado(i).Value = vbChecked Then
                bCancelar = False
                Exit For
            End If
        Next i
        If bCancelar = True Then
            chkEstado(Index).Value = vbChecked
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "chkEstado_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_lColumnaOrden = 1
    m_oInstancias.PaginaAMostrar = 1
    m_oInstancias.PaginaActual = 0
    CargarGridSolicitudes m_lColumnaOrden
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdBuscar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscarCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCenCoste.g_sOrigen = "frmSolicitudBuscar"
    frmSelCenCoste.g_bSaltarComprobacionArbol = True
    frmSelCenCoste.Show vbModal
    If Not g_oCenCos Is Nothing Then
        If Len(g_oCenCos.Cod) > 0 Then
            If Len(g_oCenCos.CodConcat) > 0 Then
                txtCentroCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
            Else
                txtCentroCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            End If
            txtCentroCoste.Tag = g_oCenCos.Cod
            m_bCentroCosteValidado = True
            
            m_sUON1_CC = g_oCenCos.UON1
            m_sUON2_CC = g_oCenCos.UON2
            m_sUON3_CC = g_oCenCos.UON3
            m_sUON4_CC = g_oCenCos.UON4
            
            m_sUON_CC = FormatearUON(g_oCenCos)
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdBuscarCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Function FormatearUON(ByVal CentroCoste As CCentroCoste)
    Dim sUON As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CentroCoste.UON1 <> "" Then
        sUON = sUON & "#" & CentroCoste.UON1
        If CentroCoste.UON2 <> "" Then
            sUON = sUON & "#" & CentroCoste.UON2
            If CentroCoste.UON3 <> "" Then
                sUON = sUON & "#" & CentroCoste.UON3
                If CentroCoste.UON4 <> "" Then
                    sUON = sUON & "#" & CentroCoste.UON4
                End If
            End If
        End If
    End If
    FormatearUON = sUON
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "FormatearUON", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function FormatearPres5o(ByVal sPres5Niv0 As String, ByVal Partida As CContratopres) As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    FormatearPres5o = FormatearPres5s(sPres5Niv0, Partida.Pres1, Partida.Pres2, Partida.Pres3, Partida.Pres4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "FormatearPres5o", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function FormatearPres5s(ByVal sPres5Niv0 As String, ByVal sPres5Niv1 As String, ByVal sPres5Niv2 As String, ByVal sPres5Niv3 As String, ByVal sPres5Niv4 As String) As String
    Dim sPRES5 As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sPRES5 = sPres5Niv0
    If sPres5Niv1 <> "" Then
        sPRES5 = sPRES5 & "@" & sPres5Niv1
        If sPres5Niv2 <> "" Then
            sPRES5 = sPRES5 & "|" & sPres5Niv2
            If sPres5Niv3 <> "" Then
                sPRES5 = sPRES5 & "|" & sPres5Niv3
                If sPres5Niv4 <> "" Then
                    sPRES5 = sPRES5 & "|" & sPres5Niv4
                End If
            End If
        End If
    End If
    FormatearPres5s = sPRES5
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "FormatearPres5s", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub cmdBuscarContrato_Click(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCContables.g_sOrigen = "frmSolicitudBuscar"
    frmSelCContables.g_sPres0 = m_asPres5Niv0(Index)
    frmSelCContables.g_iNivel = m_aiNivelImp(Index)
    frmSelCContables.Show vbModal
    If Not g_oPartida Is Nothing Then
        If Len(g_oPartida.Cod) > 0 Then
                       
            txtContrato(Index).Text = g_oPartida.Cod & " - " & g_oPartida.Den
            txtContrato(Index).Tag = g_oPartida.Cod
            m_asPres5UON(Index) = FormatearPres5o(m_asPres5Niv0(Index), g_oPartida)
                        
            m_aPres5(Index).sPres5_Nivel0 = m_asPres5Niv0(Index)
            m_aPres5(Index).sPres5_Nivel1 = g_oPartida.Pres1
            m_aPres5(Index).sPres5_Nivel2 = g_oPartida.Pres2
            m_aPres5(Index).sPres5_Nivel3 = g_oPartida.Pres3
            m_aPres5(Index).sPres5_Nivel4 = g_oPartida.Pres4
                        
            m_bPres5Validado = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdBuscarContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscarMateriales_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELMAT.sOrigen = Me.Name
    frmSELMAT.bRComprador = m_bRestrMatComp
    frmSELMAT.m_PYME = oUsuarioSummit.pyme
    frmSELMAT.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdBuscarMateriales_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdFirst_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = 1
    cmdFirst.Enabled = False
    cmdPrevious.Enabled = False
    cmdNext.Enabled = True
    cmdLast.Enabled = True
    CargarGridSolicitudes m_lColumnaOrden
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdFirst_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdLast_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = m_oInstancias.TotalPaginas
    cmdLast.Enabled = False
    cmdNext.Enabled = False
    cmdFirst.Enabled = True
    cmdPrevious.Enabled = True
    CargarGridSolicitudes m_lColumnaOrden
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdLast_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdLimpiarCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtCentroCoste.Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdLimpiarCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdLimpiarContrato_Click(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtContrato(Index).Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdLimpiarContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdLimpiarMateriales_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtMateriales.Text = ""
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
    Set m_oGMN4Seleccionado = Nothing
    sdbcArticulo.Value = ""
    txtDenArticulo.Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdLimpiarMateriales_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub cmdCancelar_Click()
Dim iDef As Integer
Dim udtTeserror As TipoErrorSummit
Dim oGrupo As CGrupo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_sOrigen = "frmPROCEModifConfig" Then 'Modificaci�n de la configuraci�n de un proceso existente

            If g_iCol = 2 Then 'Pasa a PROCESO
                iDef = frmPROCE.g_oProcesoSeleccionado.DefSolicitud
                frmPROCE.g_oProcesoSeleccionado.DefSolicitud = EnProceso
                frmPROCE.g_oProcesoSeleccionado.SolicitudId = Null
                frmPROCE.g_oProcesoSeleccionado.SolicitudDen = Null
                udtTeserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(TipoDeDatoConfigurable.Solicitud)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    frmPROCE.g_oProcesoSeleccionado.DefSolicitud = iDef
                    frmPROCE.MostrarConfiguracion
                    Unload Me
                    Exit Sub
                End If
                frmPROCE.sdbcGrupo.Columns(0).Value = ""
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefSolicitud Then
                        oGrupo.DefSolicitud = False
                    End If
                Next
            End If

            If g_iCol = 3 Then 'Pasa a GRUPO
                frmPROCE.g_oProcesoSeleccionado.DefSolicitud = EnGrupo
                Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag))
                If oGrupo Is Nothing Then
                    Screen.MousePointer = vbNormal
                    Set oGrupo = Nothing
                    Exit Sub
                End If

                oGrupo.DefSolicitud = True
                oGrupo.SolicitudId = Null
                oGrupo.SolicitudDen = Null
            End If
    End If
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdNext_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = m_oInstancias.PaginaActual + 1
    If m_oInstancias.TotalPaginas = m_oInstancias.PaginaAMostrar Then
        cmdLast.Enabled = False
        cmdNext.Enabled = False
    End If
    cmdFirst.Enabled = True
    cmdPrevious.Enabled = True
    CargarGridSolicitudes m_lColumnaOrden
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdNext_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdPrevious_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_oInstancias.PaginaAMostrar = m_oInstancias.PaginaActual - 1
    If m_oInstancias.PaginaAMostrar = 1 Then
        cmdFirst.Enabled = False
        cmdPrevious.Enabled = False
    End If
    cmdNext.Enabled = True
    cmdLast.Enabled = True
    CargarGridSolicitudes m_lColumnaOrden
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdPrevious_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Selecciona una solicitud y la carga en el formulario correspondiente
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdSeleccionar_Click()
    Dim oSolic As CInstancia
    Dim oGrupo As CGrupo
    Dim iDef As Integer
    Dim udtTeserror As TipoErrorSummit
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSolicitudes.Rows = 0 Then
        Unload Me
        Exit Sub
    End If
    
    If m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))) Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oSolic = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark)))
                         
    Select Case g_sOrigen
    
        Case "frmPROCE"
            frmPROCE.PonerSolicitudSeleccionada oSolic, 1
            frmPROCE.sdbcGrupo.Columns(0).Value = ""
        
        Case "frmCatalogo"
            frmCatalogo.PonerSolicitudSeleccionada oSolic
         
        Case "frmCATLINBuscar"
            frmCATLINBuscar.PonerSolicitudSeleccionada oSolic
         
        Case "frmPROCEBuscar"
            frmPROCEBuscar.PonerSolicitudSeleccionada oSolic
            
        Case "frmPROCEItem"
            frmPROCE.PonerSolicitudSeleccionada oSolic, 3
        
        Case "frmPROCEGrupo"
            frmPROCE.PonerSolicitudSeleccionada oSolic, 2
            frmPROCE.sdbcGrupo.Columns(0).Value = ""
            
        Case "frmItemModificarValores"
            frmItemModificarValores.PonerSolicitudSeleccionada oSolic
            
        Case "frmItemsWizard2"
            frmItemsWizard2.PonerSolicitudSeleccionada oSolic
            
        Case "frmPROCEModifConfig" 'Modificaci�n de la configuraci�n de un proceso existente
    
            If g_iCol = 2 Then 'Pasa a PROCESO
                iDef = frmPROCE.g_oProcesoSeleccionado.DefSolicitud
                frmPROCE.g_oProcesoSeleccionado.DefSolicitud = EnProceso
                frmPROCE.g_oProcesoSeleccionado.SolicitudId = oSolic.Id
                frmPROCE.g_oProcesoSeleccionado.SolicitudDen = oSolic.DescrBreve
                udtTeserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(TipoDeDatoConfigurable.Solicitud)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    frmPROCE.g_oProcesoSeleccionado.DefSolicitud = iDef
                    frmPROCE.MostrarConfiguracion
                    Unload Me
                    Set oGrupo = Nothing
                    Exit Sub
                End If
                frmPROCE.sdbcGrupo.Columns(0).Value = ""
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefSolicitud Then
                        oGrupo.DefSolicitud = False
                    End If
                Next
            End If
            
            If g_iCol = 3 Then 'Pasa a GRUPO
                frmPROCE.g_oProcesoSeleccionado.DefSolicitud = EnGrupo
                Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag))
                If oGrupo Is Nothing Then
                    Screen.MousePointer = vbNormal
                    Set oGrupo = Nothing
                    Exit Sub
                End If
                
                oGrupo.DefSolicitud = True
                oGrupo.SolicitudId = oSolic.Id
                oGrupo.SolicitudDen = oSolic.DescrBreve
            End If
        
        Case "SOLICITUD_ITEMS"
            ModifSolicitudItems
                        
        Case "frmLstPROCEA2B1"
            frmListados.ofrmLstProce1.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstPROCEA2B2"
            frmListados.ofrmlstProce2.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstPROCEA2B3"
            frmListados.ofrmLstProce3.PonerSolicitudSeleccionada oSolic
        
        Case "frmLstPROCEA2B4C2"
            frmListados.ofrmLstProce4.PonerSolicitudSeleccionada oSolic
        
        Case "frmLstPROCEA2B5"
            frmListados.ofrmlstProce5.PonerSolicitudSeleccionada oSolic
        
        Case "frmLstPROCEA6B3C1"
            frmListados.ofrmLstProce6.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstPROCEfrmPROCE"
            frmPROCE.g_ofrmLstPROCE.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstPROCEfrmSELPROVE"
            frmSELPROVE.ofrmLstPROCE.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstPROCEfrmOFEPet"
            frmOFEPet.ofrmLstPROCE.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstPROCEfrmOFERec"
            frmOFERec.ofrmLstPROCE.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstPROCEfrmOFEHistWeb"
            frmOFEHistWeb.ofrmLstPROCE.PonerSolicitudSeleccionada oSolic
        
        Case "frmSeguimiento"
            frmSeguimiento.PonerSolicitudSeleccionada oSolic
            
        Case "frmLstSeguimiento"
            frmLstPedidos.PonerSolicitudSeleccionada oSolic
    End Select
    
    Set oSolic = Nothing
    
    Screen.MousePointer = vbNormal
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "cmdSeleccionar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    m_bActivado = False
    
    PonerFieldSeparator Me

    Set m_oInstancias = oFSGSRaiz.Generar_CInstancias
    m_oInstancias.BusquedaConPaginacion = True
    
    Set m_oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
    Set m_oContratosPres = oFSGSRaiz.Generar_CContratosPres
    
    m_lColumnaOrden = 1
    
    lblMon.caption = gParametrosGenerales.gsMONCEN
    
    splitterMain.Panes(0).Height = PANE0_INIT_HEIGHT
    splitterMain.Height = Me.Height - picSeleccion.Height - 530
    
    sdbgPartidas.RowHeight = 285
    
    Me.Width = 13065
    Me.Height = 8325
    splitterMain.Width = Me.Width
    
    ReDim m_asPres5Niv0(0)
    ReDim m_aiNivelImp(0)
    ReDim m_asPres5UON(0)
    ReDim m_aPres5(0)
    
    CargarPartidas
    
    lstCentroCoste.Left = txtCentroCoste.Left
    lstCentroCoste.Top = txtCentroCoste.Top + txtCentroCoste.Height
    lstCentroCoste.Height = 570
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    ConfigurarPantalla
    
    If Left(g_sOrigen, 8) = "frmPROCE" Or g_sOrigen = "SOLICITUD_ITEMS" Or g_sOrigen = "frmItemModificarValores" Then
        
        CargarTiposSolicitud
        CargarPersonas
        CargarArticulos
        CargarEmpresas
        CargarDepartamentos
        CargarCompradores
    
        CargarConfiguracionVisorSol
        CargarConfiguracionVisorSolPP
        
        ComprobarYAplicarFiltros
    End If
     
    If g_sOrigen <> "frmLstSeguimiento" And g_sOrigen <> "frmSeguimiento" Then
        If g_lId <> 0 Then
            txtId.Text = g_lId
        End If
    
        'Carga la grid con las solicitudes:
        CargarGridSolicitudes m_lColumnaOrden
    Else
        OcultarPaginacion
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub MostrarBusquedaAvanzada()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblMostrarBusquedaAvanzada.Visible = False
    picBusquedaAvanzada.Visible = True
    If splitterMain.Panes(0).Height < PANE0_MIN_HEIGHT + m_sIncrementoAltura Then
        splitterMain.Panes(0).Height = PANE0_MIN_HEIGHT + m_sIncrementoAltura
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "MostrarBusquedaAvanzada", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub OcultarBusquedaAvanzada()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblMostrarBusquedaAvanzada.Visible = True
    picBusquedaAvanzada.Visible = False
    splitterMain.Panes(0).Height = PANE0_INIT_HEIGHT
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "OcultarBusquedaAvanzada", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga los idiomas del formulario.
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo=0,2</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_BUSCAR_SOLICITUDES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value       '1 b�squeda de solicitudes de compra
        Ador.MoveNext
        sId = Ador(0).Value  'Identificador
        lblIdentificador.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblDescripcion.caption = Ador(0).Value   'descripci�n
        Ador.MoveNext
        lblPeticionario.caption = Ador(0).Value  'peticionario
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value & ":"    'desde
        sFecDesde = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value & ":"  'hasta
        sFecHasta = Ador(0).Value
        Ador.MoveNext
        
        sdbgSolicitudes.Columns("ALTA").caption = Ador(0).Value  'alta
        Ador.MoveNext
        sdbgSolicitudes.Columns("NECESIDAD").caption = Ador(0).Value  'necesidad
        Ador.MoveNext
        sdbgSolicitudes.Columns("ID").caption = Ador(0).Value  'identificador
        Ador.MoveNext
        sdbgSolicitudes.Columns("DESCR").caption = Ador(0).Value  'descripci�n breve
        Ador.MoveNext
        sdbgSolicitudes.Columns("IMPORTE").caption = Ador(0).Value  'importe
        Ador.MoveNext
        sdbgSolicitudes.Columns("PET").caption = Ador(0).Value  'peticionario
        Ador.MoveNext
        sdbgSolicitudes.Columns("COMP").caption = Ador(0).Value  'comprador
        
        Ador.MoveNext
        sOrdenando = Ador(0).Value 'Ordenando...
        
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value 'Seleccionar
        
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value 'Cancelar
        
        Ador.MoveNext
        m_sAprobada = Ador(0).Value    'Aprobada
        Ador.MoveNext
        m_sAnulada = Ador(0).Value     'Anulada
        Ador.MoveNext
        m_sCerrada = Ador(0).Value     'Cerrada
        
        Ador.MoveNext
        sdbgSolicitudes.Columns("ESTADO").caption = Ador(0).Value  'estado
        
        Ador.MoveNext
        chkEstado(1).caption = Ador(0).Value   'aprobadas
        Ador.MoveNext
        chkEstado(2).caption = Ador(0).Value   'anuladas
        Ador.MoveNext
        chkEstado(3).caption = Ador(0).Value   'cerradas
        
        Ador.MoveNext
        lblTipoSolicit.caption = Ador(0).Value '24 tipo de solicitud
        Ador.MoveNext
        sdbcTipoSolicit.Columns("COD").caption = Ador(0).Value '25 C�digo
        sdbcArticulo.Columns("COD").caption = "" & Ador(0).Value
        sdbcDepartamento.Columns("COD").caption = "" & Ador(0).Value
        Ador.MoveNext
        sdbcTipoSolicit.Columns("DEN").caption = Ador(0).Value '26 Denominaci�n
        sdbcArticulo.Columns("DEN").caption = "" & Ador(0).Value
        sdbcDepartamento.Columns("DEN").caption = "" & Ador(0).Value
        sdbcEmpresa.Columns("DEN").caption = "" & Ador(0).Value
        Ador.MoveNext
        sdbgSolicitudes.Columns("TIPO").caption = Ador(0).Value '27 Tipo
        Ador.MoveNext
        chkEstado(4).caption = Ador(0).Value 'En curso de aprobaci�n
        Ador.MoveNext
        chkEstado(5).caption = Ador(0).Value 'Pendiente
        m_sPendiente = Ador(0).Value
        Ador.MoveNext
        m_sProveedor = Ador(0).Value 'Proveedor
        'Ador.MoveNext
        'm_sPendiente = Ador(0).Value   'pendiente
        'Ador.MoveNext
        'm_sRechazada = Ador(0).Value   'rechazada
        
        Ador.MoveNext
        lblMostrarBusquedaAvanzada.caption = Ador(0).Value
        Ador.MoveNext
        lblMateriales.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblArticulo.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblCentroCoste.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblEmpresa.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblDepartamento.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblComprador.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblImporteDesde.caption = Ador(0).Value & ":"
        sImporteDesde = "" & Ador(0).Value
        Ador.MoveNext
        lblImporteHasta.caption = Ador(0).Value & ":"
        sImporteHasta = "" & Ador(0).Value
        Ador.MoveNext
        lblOcultarBusquedaAvanzada.caption = Ador(0).Value
        Ador.MoveNext
        m_sPageNumber = "" & Ador(0).Value  '72
        Ador.MoveNext
        If g_sOrigen = "frmPROCEItem" Then cmdSeleccionar.caption = Ador(0).Value 'Siguiente
        
        Ador.Close
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
           
End Sub

''' <summary>
''' Descarga las variables de modulo
''' </summary>
''' <param name="Cancel">Cancelar el cierre</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oInstancias = Nothing
    g_iCol = 0
    g_sOrigen = ""
    g_lId = 0
    
    m_bRSolicPerfUON = False
    m_bRSolicDep = False
    m_lIdPerfil = 0
    
    Set m_oGMN4Seleccionado = Nothing
    Set m_oCentrosCoste = Nothing
    Set m_oContratosPres = Nothing
    
    Set g_oConfVisorSolPPs = Nothing
    Set g_oConfVisorSol = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub lblMostrarBusquedaAvanzada_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    MostrarBusquedaAvanzada
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "lblMostrarBusquedaAvanzada_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub lblOcultarBusquedaAvanzada_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    OcultarBusquedaAvanzada
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "lblOcultarBusquedaAvanzada_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub lstCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtCentroCoste.Text = lstCentroCoste.List(lstCentroCoste.ListIndex)
    lstCentroCoste.Visible = False
    m_bCentroCosteValidado = True
    txtCentroCoste.BackColor = &HC0FFC0
    
    Dim selectedItem As CCentroCoste
    Set selectedItem = m_oCentrosCoste.Item(lstCentroCoste.ListIndex + 1)
    If Not selectedItem Is Nothing Then
        m_sUON1_CC = selectedItem.UON1
        m_sUON2_CC = selectedItem.UON2
        m_sUON3_CC = selectedItem.UON3
        m_sUON4_CC = selectedItem.UON4
        m_sUON_CC = FormatearUON(selectedItem)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "lstCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub lstContrato_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtContrato(0).Text = lstContrato.List(lstContrato.ListIndex)
    lstContrato.Visible = False
    m_bPres5Validado = True
    txtContrato(0).BackColor = &HC0FFC0

    Dim selectedItem As CContratopres
    Set selectedItem = m_oContratosPres.Item(lstContrato.ListIndex + 1)
    If Not selectedItem Is Nothing Then
        txtContrato(0).Tag = selectedItem.Cod
        m_asPres5UON(0) = FormatearPres5o(m_asPres5Niv0(0), selectedItem)
        m_aPres5(0).sPres5_Nivel0 = m_asPres5Niv0(0)
        m_aPres5(0).sPres5_Nivel1 = selectedItem.Pres1
        m_aPres5(0).sPres5_Nivel2 = selectedItem.Pres2
        m_aPres5(0).sPres5_Nivel3 = selectedItem.Pres3
        m_aPres5(0).sPres5_Nivel4 = selectedItem.Pres4
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "lstContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcArticulo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticulo = True
    m_bEditandoArticuloConDropDown = False
    If Not m_bEditandoArticuloDen Then
        txtDenArticulo.Text = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcArticulo_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Len(sdbcArticulo.Value) <> 0 Then
        txtDenArticulo.Text = sdbcArticulo.Columns(1).Text
    End If
    m_bEditandoArticuloConDropDown = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcArticulo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcArticulo_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticulo = False
    m_bEditandoArticuloConDropDown = True
    sdbcArticulo.RemoveAll
    
    CargarArticulos

    sdbcArticulo.SelStart = 0
    sdbcArticulo.SelLength = Len(sdbcArticulo.Text)
    sdbcArticulo.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcArticulo_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarArticulos()
    Dim oArticulos As CArticulos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
        
    If m_bRestrMatComp Then
        If Not m_oGMN4Seleccionado Is Nothing Or sdbcArticulo.Text <> "" Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod, False, Trim(sdbcArticulo), , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
        End If
    Else
        If m_oGMN4Seleccionado Is Nothing Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, , False, Trim(sdbcArticulo.Text), , , , , , , , , , , , , oUsuarioSummit.pyme
        Else
            m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcArticulo.Text), , , , , , False
            Set oArticulos = m_oGMN4Seleccionado.ARTICULOS
        End If
    End If

    Dim oArt As CArticulo
    For Each oArt In oArticulos
        sdbcArticulo.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
    Next
    
    Set oArticulos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarArticulos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcArticulo_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bEditandoArticulo And Len(sdbcArticulo.Text) <> 0 Then
        Dim oArticulos As CArticulos
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        If m_bRestrMatComp Then
            If Not m_oGMN4Seleccionado Is Nothing Or sdbcArticulo.Text <> "" Then
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod, True, Trim(sdbcArticulo), , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            End If
        Else
            If m_oGMN4Seleccionado Is Nothing Then
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, , True, Trim(sdbcArticulo.Text), , True, , , , , , , , , , , oUsuarioSummit.pyme
            Else
                m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcArticulo.Text), , True, , , , True
                Set oArticulos = m_oGMN4Seleccionado.ARTICULOS
            End If
        End If
        If oArticulos.Count = 0 Then
            oMensajes.NoValido 21
            sdbcArticulo.Text = ""
            txtDenArticulo.Text = ""
        Else
            sdbcArticulo.Columns(0).Value = oArticulos.Item(1).Cod
            sdbcArticulo.Columns(1).Value = oArticulos.Item(1).Den
            txtDenArticulo.Text = oArticulos.Item(1).Den
        End If
        Set oArticulos = Nothing
    End If
    m_bEditandoArticulo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcArticulo_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcComprador_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcComprador.RemoveAll
    
    CargarCompradores

    sdbcComprador.SelStart = 0
    sdbcComprador.SelLength = Len(sdbcComprador.Text)
    sdbcComprador.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcComprador_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cargar la combo con todos los Compradores con acceso al EP
''' </summary>
''' <remarks>Llamada desde: Form_load  sdbcComprador_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarCompradores()
    Dim oCompradores As CCompradores
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If m_bRuo Then
        oCompradores.CargarTodosLosCompradoresPorUON , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, sdbcDepartamento.Value, True, "NOM", , , m_bRSolicPerfUON, m_lIdPerfil
    Else
        oCompradores.CargarTodosLosCompradoresPorUON , , , , , , sdbcDepartamento.Value, True, "NOM", , oUsuarioSummit.pyme, m_bRSolicPerfUON, m_lIdPerfil
    End If

    sdbcComprador.AddItem vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oComprador As CComprador
    For Each oComprador In oCompradores
        sdbcComprador.AddItem oComprador.Cod & Chr(m_lSeparador) & oComprador.Cod & " - " & oComprador.nombre & " " & oComprador.Apel
    Next

    Set oCompradores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarCompradores", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcComprador_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcComprador.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcComprador_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDepartamento_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDepartamento.RemoveAll

    CargarDepartamentos
    
    sdbcDepartamento.SelStart = 0
    sdbcDepartamento.SelLength = Len(sdbcDepartamento.Text)
    sdbcDepartamento.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcDepartamento_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Cargar la combo con todos los Departamentoss con acceso al EP
''' </summary>
''' <remarks>Llamada desde: Form_load  sdbcDepartamento_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarDepartamentos()
    Dim oDepartamentos As CDepartamentos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
        
    If m_bRuo Then
        oDepartamentos.CargarTodosLosDepartamentosPorUON , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , True, , , m_bRSolicPerfUON, m_lIdPerfil
    Else
        oDepartamentos.CargarTodosLosDepartamentos , , , True, , , oUsuarioSummit.pyme, m_bRSolicPerfUON, m_lIdPerfil
    End If
    
    sdbcDepartamento.AddItem vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oDepartamento As CDepartamento
    For Each oDepartamento In oDepartamentos
        sdbcDepartamento.AddItem oDepartamento.Cod & Chr(m_lSeparador) & oDepartamento.Den
    Next

    Set oDepartamentos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarDepartamentos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcDepartamento_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcDepartamento.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcDepartamento_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcEmpresa_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    sdbcEmpresa.RemoveAll
        
    CargarEmpresas
    
    sdbcEmpresa.SelStart = 0
    sdbcEmpresa.SelLength = Len(sdbcEmpresa.Text)
    sdbcEmpresa.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcEmpresa_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Cargar la combo con todos las Empresas con acceso al EP
''' </summary>
''' <remarks>Llamada desde: Form_load  sdbcEmpresa_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarEmpresas()
    Dim oEmpresas As CEmpresas
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    If m_bRuo Then
        oEmpresas.CargarTodasLasEmpresasDesde , , , , True, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , , , m_bRSolicPerfUON, m_lIdPerfil
    Else
        oEmpresas.CargarTodasLasEmpresasDesde , , , , True, , , , , , , , oUsuarioSummit.pyme, m_bRSolicPerfUON, m_lIdPerfil
    End If
    
    sdbcEmpresa.AddItem 0 & Chr(m_lSeparador) & vbNullString & Chr(m_lSeparador) & vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oEmpresa As CEmpresa
    For Each oEmpresa In oEmpresas
        sdbcEmpresa.AddItem oEmpresa.Id & Chr(m_lSeparador) & oEmpresa.NIF & Chr(m_lSeparador) & oEmpresa.Den & Chr(m_lSeparador) & oEmpresa.NIF & " - " & oEmpresa.Den
    Next
    
    Set oEmpresas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarEmpresas", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcEmpresa_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcEmpresa.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcEmpresa_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcPeticionario_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPeticionario.DroppedDown Then
        sdbcPeticionario = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcPeticionario_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPeticionario_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If sdbcPeticionario.Value = "..." Or sdbcPeticionario = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
    
    
    m_bRespetarCombo = True
    sdbcPeticionario.Text = sdbcPeticionario.Columns(1).Text
    m_bRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcPeticionario_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Cargar la combo con todos los usuarios con acceso al EP
''' </summary>
''' <remarks>Llamada desde: sdbcTipoSolicit_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcPeticionario_DropDown()
    

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcPeticionario.RemoveAll
    
    CargarPersonas
    
    
    
    sdbcPeticionario.SelStart = 0
    sdbcPeticionario.SelLength = Len(sdbcPeticionario.Text)
    sdbcPeticionario.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcPeticionario_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPeticionario_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPeticionario.DataFieldList = "Column 1"
    sdbcPeticionario.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcPeticionario_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPeticionario_PositionList(ByVal Text As String)
PositionList sdbcPeticionario, Text, 1
End Sub


Private Sub sdbcPeticionario_Validate(Cancel As Boolean)
     Dim oPersonas As CPersonas
    Dim bExiste As Boolean
    Dim sbuscar As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPeticionario.Text = "" Then
        sdbcPeticionario.Columns(0).Value = ""
        Exit Sub
    End If
    
    If sdbcPeticionario.Text = sdbcPeticionario.Columns(0).Text Then Exit Sub

    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    
    If sdbcPeticionario.Columns(0).Value = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el peticionario
    
    Screen.MousePointer = vbHourglass
    
    If sdbcPeticionario.Columns(0).Text = "" Then
        sbuscar = "NULL"
    Else
        sbuscar = sdbcPeticionario.Columns(0).Text
    End If
    
    If m_bRuo Then
        oPersonas.CargarPeticionariosSolCompra True, sbuscar, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
    Else
        oPersonas.CargarPeticionariosSolCompra True, sbuscar
    End If
    
    bExiste = Not (oPersonas.Count = 0)
    
    If Not bExiste Then
        sdbcPeticionario.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcPeticionario.Text = oPersonas.Item(1).Cod & " - " & NullToStr(oPersonas.Item(1).nombre) & " " & NullToStr(oPersonas.Item(1).Apellidos)
        sdbcPeticionario.Columns(0).Value = oPersonas.Item(1).Cod
        sdbcPeticionario.Columns(1).Value = oPersonas.Item(1).Cod & "-" & oPersonas.Item(1).nombre & " " & NullToStr(oPersonas.Item(1).Apellidos)
        
        m_bRespetarCombo = False
        
    End If
    
    Set oPersonas = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcPeticionario_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Posicionarse en el combo seg�n la selecci�n
''' </summary>
''' <param name="Id">Valor seleccionado</param>
''' <returns>Posici�n del valor seleccionado</returns>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros; Tiempo m�ximo: 0,1 sg</remarks>
Private Function sdbcArticulo_PositionListId(ByVal Id As String) As String
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcArticulo.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcArticulo.Rows - 1
            bm = sdbcArticulo.GetBookmark(i)
            If Id = sdbcArticulo.Columns(0).CellText(bm) Then
                sdbcArticulo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcArticulo_PositionListId = sdbcArticulo.Columns(0).CellText(bm)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcArticulo_PositionListId", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Posicionarse en el combo seg�n la selecci�n
''' </summary>
''' <param name="Id">Valor seleccionado</param>
''' <returns>Posici�n del valor seleccionado</returns>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros; Tiempo m�ximo: 0,1 sg</remarks>
Private Function sdbcEmpresa_PositionListId(ByVal Id As String) As String
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcArticulo.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcEmpresa.Rows - 1
            bm = sdbcEmpresa.GetBookmark(i)
            If Id = sdbcEmpresa.Columns(0).CellText(bm) Then
                sdbcEmpresa.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcEmpresa_PositionListId = sdbcEmpresa.Columns(1).CellText(bm) & " - " & sdbcEmpresa.Columns(2).CellText(bm)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcEmpresa_PositionListId", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Posicionarse en el combo seg�n la selecci�n
''' </summary>
''' <param name="Id">Valor seleccionado</param>
''' <returns>Posici�n del valor seleccionado</returns>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros; Tiempo m�ximo: 0,1 sg</remarks>
Private Function sdbcDepartamento_PositionListId(ByVal Id As String) As String
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcDepartamento.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcDepartamento.Rows - 1
            bm = sdbcDepartamento.GetBookmark(i)
            If Id = sdbcDepartamento.Columns(0).CellText(bm) Then
                sdbcDepartamento.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcDepartamento_PositionListId = sdbcDepartamento.Columns(1).CellText(bm)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcDepartamento_PositionListId", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Posicionarse en el combo seg�n la selecci�n
''' </summary>
''' <param name="Id">Valor seleccionado</param>
''' <returns>Posici�n del valor seleccionado</returns>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros; Tiempo m�ximo: 0,1 sg</remarks>
Private Function sdbcComprador_PositionListId(ByVal Id As String) As String
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcComprador.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcComprador.Rows - 1
            bm = sdbcComprador.GetBookmark(i)
            If Id = sdbcComprador.Columns(0).CellText(bm) Then
                sdbcComprador.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcComprador_PositionListId = sdbcComprador.Columns(1).CellText(bm)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcComprador_PositionListId", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub sdbgPartidas_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgPartidas.Columns(ColIndex).Name = "VALOR" And sdbgPartidas.Columns("VALIDADO").Value = 0 Then
        sdbgPartidas.Columns("PRES5NIV1").Value = ""
        sdbgPartidas.Columns("PRES5NIV2").Value = ""
        sdbgPartidas.Columns("PRES5NIV3").Value = ""
        sdbgPartidas.Columns("PRES5NIV4").Value = ""
        sdbgPartidas.Columns("TAG").Value = ""
        sdbgPartidas.Columns("PRES5UON").Value = ""
        
        
        If sdbgPartidas.Columns(ColIndex).Value <> "" Then
        
        
            Dim pos As Integer
            pos = InStr(1, sdbgPartidas.Columns("VALOR").Value, " - ", vbTextCompare)
            If pos > 0 Then
                sdbgPartidas.Columns("TAG").Value = Trim(Mid(sdbgPartidas.Columns("VALOR").Value, 1, pos))
            Else
                sdbgPartidas.Columns("TAG").Value = ""
            End If
            
            Dim sValue As String
            If Len(sdbgPartidas.Columns("TAG").Value) > 0 Then
                sValue = CStr(sdbgPartidas.Columns("TAG").Value)
            Else
                sValue = sdbgPartidas.Columns(ColIndex).Value
            End If
            
                        
            If m_bRuo Then
                m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , sValue
            Else
                If oUsuarioSummit.pyme <> 0 Then
                    m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, , , , sValue
                Else
                    m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, , , , , sValue
                End If
            End If

            If m_oContratosPres.Count > 0 Then
'                sdbgPartidas.Columns(ColIndex).Style = ssStyleComboBox
'                sdbgPartidas.Columns(ColIndex).RemoveAll
'                Dim sItemContrato As String
'                Dim i As Integer
'                For i = 1 To m_oContratosPres.Count
'                    sItemContrato = m_oContratosPres.Item(i).cod & " - " & m_oContratosPres.Item(i).Den
'                    sdbgPartidas.Columns(ColIndex).AddItem sItemContrato
'                Next
                
                'Se coge la primera partida
                sdbgPartidas.Columns("VALOR").Value = m_oContratosPres.Item(1).Cod & " - " & m_oContratosPres.Item(1).Den
                sdbgPartidas.Columns("TAG").Value = m_oContratosPres.Item(1).Cod
                sdbgPartidas.Columns("PRES5UON").Value = FormatearPres5o(sdbgPartidas.Columns("COD").Value, m_oContratosPres.Item(1))
                sdbgPartidas.Columns("VALIDADO").Value = 1
                sdbgPartidas.Columns("PRES5NIV1").Value = m_oContratosPres.Item(1).Pres1
                sdbgPartidas.Columns("PRES5NIV2").Value = m_oContratosPres.Item(1).Pres2
                sdbgPartidas.Columns("PRES5NIV3").Value = m_oContratosPres.Item(1).Pres3
                sdbgPartidas.Columns("PRES5NIV4").Value = m_oContratosPres.Item(1).Pres4
            Else
                sdbgPartidas.Columns("VALOR").Value = sValue
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbgPartidas_BeforeColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPartidas_BtnClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCContables.g_sOrigen = "frmSolicitudBuscar"
    frmSelCContables.g_sPres0 = sdbgPartidas.Columns("COD").Value
    frmSelCContables.g_iNivel = sdbgPartidas.Columns("NIVEL").Value
    frmSelCContables.Show vbModal
    If Not g_oPartida Is Nothing Then
        If Len(g_oPartida.Cod) > 0 Then
                        
            sdbgPartidas.Columns("VALOR").Value = g_oPartida.Cod & " - " & g_oPartida.Den
            sdbgPartidas.Columns("TAG").Value = g_oPartida.Cod
            sdbgPartidas.Columns("PRES5UON").Value = FormatearPres5o(sdbgPartidas.Columns("COD").Value, g_oPartida)
            sdbgPartidas.Columns("VALIDADO").Value = 2
            sdbgPartidas.Columns("PRES5NIV1").Value = g_oPartida.Pres1
            sdbgPartidas.Columns("PRES5NIV2").Value = g_oPartida.Pres2
            sdbgPartidas.Columns("PRES5NIV3").Value = g_oPartida.Pres3
            sdbgPartidas.Columns("PRES5NIV4").Value = g_oPartida.Pres4
            
            sdbgPartidas.Update
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbgPartidas_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPartidas_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgPartidas.Columns("VALIDADO").Value = 0
    sdbgPartidas.Columns("VALOR").CellStyleSet ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbgPartidas_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPartidas_KeyPress(KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        If sdbgPartidas.Row + 1 = sdbgPartidas.Rows Then
            sdbgPartidas.MoveFirst
        Else
            sdbgPartidas.MoveNext
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbgPartidas_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPartidas_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgPartidas.Columns("VALOR").CellValue(Bookmark) = "" Then
        sdbgPartidas.Columns("VALOR").CellStyleSet ""
    Else
        If sdbgPartidas.Columns("VALIDADO").CellValue(Bookmark) = 1 Then
            sdbgPartidas.Columns("VALOR").CellStyleSet "Verde"
        ElseIf sdbgPartidas.Columns("VALIDADO").CellValue(Bookmark) = 0 Then
            sdbgPartidas.Columns("VALOR").CellStyleSet "Rojo"
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbgPartidas_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgSolicitudes_BtnClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgSolicitudes.Col = -1 Then Exit Sub
    
    Select Case sdbgSolicitudes.Columns(sdbgSolicitudes.Col).Name
        Case "TIPO"
            If Not m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Solicitud Is Nothing Then
                Set frmDetTipoSolicitud.g_oSolicitud = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Solicitud
                frmDetTipoSolicitud.Show vbModal
            End If
            
        Case "PET"
            frmDetallePersona.g_sPersona = sdbgSolicitudes.Columns("COD_PER").Value
            frmDetallePersona.Show vbModal
            
        Case "COMP"
            If sdbgSolicitudes.Columns("DESTINATARIO_PROV").Value <> "" Then
                MostrarDetalleProveedor (sdbgSolicitudes.Columns("DESTINATARIO_PROV").Value)
            ElseIf sdbgSolicitudes.Columns("COD_COMP").Value <> "" Then
                frmDetallePersona.g_sPersona = sdbgSolicitudes.Columns("COD_COMP").Value
                frmDetallePersona.Show vbModal
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbgSolicitudes_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbgSolicitudes_HeadClick(ByVal ColIndex As Integer)
    Dim sHeadCaption As String

    ''' * Objetivo: Ordenar el grid segun la columna

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ColIndex > 11 Then Exit Sub

    Screen.MousePointer = vbHourglass

    sHeadCaption = sdbgSolicitudes.Columns(ColIndex).caption
    sdbgSolicitudes.Columns(ColIndex).caption = sOrdenando
    
    m_lColumnaOrden = ColIndex
    m_oInstancias.PaginaAMostrar = 1
    m_oInstancias.PaginaActual = 0
    CargarGridSolicitudes ColIndex
        
    sdbgSolicitudes.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbgSolicitudes_HeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbgSolicitudes_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case sdbgSolicitudes.Columns("ID_ESTADO").CellValue(Bookmark)
        Case EstadoSolicitud.Pendiente
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Pendiente"
            Next i

        Case EstadoSolicitud.Aprobada
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Aprobada"
            Next i
            
        Case EstadoSolicitud.Rechazada
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Rechazada"
            Next i
    
        Case EstadoSolicitud.Anulada
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Anulada"
            Next i
            
        Case EstadoSolicitud.Cerrada
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Cerrada"
            Next i
    
        Case Else   'En curso
            For i = 0 To sdbgSolicitudes.Columns.Count - 1
                sdbgSolicitudes.Columns(i).CellStyleSet "Curso"
            Next i
            
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbgSolicitudes_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cargar Grid Solicitudes
''' </summary>
''' <param name="iCol">columna por la q ordena</param>
''' <remarks>Llamada desde: cmdBuscar_Click     cmdFirst_Click      cmdLast_Click       cmdNext_Click       cmdPrevious_Click       Form_Load
''' sdbgSolicitudes_HeadClick; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridSolicitudes(ByVal iCol As Integer)
    Dim sPet As String
    Dim vEstado() As Variant
    Dim iOrden As TipoOrdenacionSolicitudes
    Dim vFecNec As Variant
    Dim i As Integer
    Dim sestado As String
    Dim oInstancia As CInstancia
    Dim lTipo As Long
    Dim sAsignadoA As String
    Dim sCodAsignadoA As String
    Dim sTrasladadaProv As String
    
    Dim sMateriales As String
    Dim sCodArticulo As String
    Dim lEmpresa As Long
    Dim sDepartamento As String
    Dim sComprador As String
    Dim sPartidas As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgSolicitudes.RemoveAll
    
    'Hace las comprobaciones necesarias:
    If txtId.Text <> "" Then
        If Not IsNumeric(txtId.Text) Then
            oMensajes.NoValido sId
            If Me.Visible Then txtId.SetFocus
            Exit Sub
        End If
    End If
    
    If txtFecDesde.Text <> "" Then
        If Not IsDate(txtFecDesde.Text) Then
            oMensajes.NoValido sFecDesde
            If Me.Visible Then txtFecDesde.SetFocus
            Exit Sub
        End If
    End If
    
    If txtFecHasta.Text <> "" Then
        If Not IsDate(txtFecHasta.Text) Then
            oMensajes.NoValido sFecHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(txtImporteDesde.Text) <> "" Then
        If Not IsNumeric(txtImporteDesde.Text) Then
            oMensajes.NoValido sImporteDesde
            If Me.Visible Then txtImporteDesde.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(txtImporteHasta.Text) <> "" Then
        If Not IsNumeric(txtImporteHasta.Text) Then
            oMensajes.NoValido sImporteHasta
            If Me.Visible Then txtImporteHasta.SetFocus
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
         
    If sdbcTipoSolicit.Text <> "" Then
        lTipo = sdbcTipoSolicit.Columns("ID").Value
    End If
    
    If sdbcPeticionario.Text <> "" Then
        sPet = sdbcPeticionario.Columns(0).Value
    End If
    
    'Obtiene los estados
    If g_sOrigen = "frmPROCEBuscar" Or g_sOrigen = "frmLstSeguimiento" Or g_sOrigen = "frmSeguimiento" Then
        'Obtiene los estados
        i = 1
        If Trim(txtId.Text) = "" Then
            'S�lo busca por los estados cuando no se ha introducido el identificador
            If chkEstado(1).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Aprobada
                i = i + 1
            End If
            If chkEstado(2).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Anulada
                i = i + 1
            End If
            If chkEstado(3).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Cerrada
                i = i + 1
            End If
            'En curso de aprobaci�n
            If chkEstado(4).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.GenPendiente
                i = i + 1
                
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.GenAprobada
                i = i + 1
            End If
            'Pendiente
            If chkEstado(5).Value = vbChecked Then
                ReDim Preserve vEstado(i)
                vEstado(i) = EstadoSolicitud.Pendiente
                i = i + 1
            End If
            
        Else
            ReDim Preserve vEstado(i)
            vEstado(i) = ""
        End If
    Else
        i = 1
        ReDim vEstado(i)
        vEstado(i) = EstadoSolicitud.Aprobada
        i = i + 1
        ReDim Preserve vEstado(i)
        vEstado(i) = EstadoSolicitud.Pendiente
        i = i + 1
        If (Not m_bAbrirProcEnCurso) And (Not chkEstado(1).Visible) Then
        Else
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.GenPendiente
            i = i + 1
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.GenAprobada
        End If
    End If
    
    If m_sGMN1Cod <> "" Then
        sMateriales = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(m_sGMN1Cod)) & m_sGMN1Cod
        If m_sGMN2Cod <> "" Then
            sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(m_sGMN2Cod)) & m_sGMN2Cod
            If m_sGMN3Cod <> "" Then
                sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(m_sGMN3Cod)) & m_sGMN3Cod
                If m_sGMN4Cod <> "" Then
                    sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(m_sGMN4Cod)) & m_sGMN4Cod
                End If
            End If
        End If
    End If
            
    If sdbcArticulo.Text <> "" Then
        sCodArticulo = sdbcArticulo.Columns(0).Value
    End If
    
    If sdbcEmpresa.Text <> "" Then
        lEmpresa = sdbcEmpresa.Columns(0).Value
    End If
    
    If sdbcDepartamento.Text <> "" Then
        sDepartamento = sdbcDepartamento.Columns(0).Value
    End If
    
    If sdbcComprador.Text <> "" Then
        sComprador = sdbcComprador.Columns(0).Value
    End If
    
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 1 Then
            Dim vbm As Variant
            Dim Row As Integer
            Row = sdbgPartidas.Rows - 1
            While Row >= 0
                vbm = sdbgPartidas.AddItemBookmark(Row)
                If Len(sdbgPartidas.Columns("VALOR").CellValue(vbm)) > 0 Then
                    If Len(sPartidas) = 0 Then
                        sPartidas = sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                    Else
                        sPartidas = sPartidas & ";" & sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                    End If
                End If
                Row = Row - 1
            Wend
        Else
            sPartidas = m_asPres5UON(0)
        End If
    End If
    
    Select Case sdbgSolicitudes.Columns(iCol).Name
        Case "TIPO"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorTipo
        Case "ALTA"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorfecalta
        Case "NECESIDAD"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorFecNec
        Case "ID"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorId
        Case "DESCR"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorDescr
        Case "IMPORTE"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorImporte
        Case "PET"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorPet
        Case "ESTADO"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorEstado
        Case "COMP"
            iOrden = TipoOrdenacionSolicitudes.OrdSolicPorComp
    End Select
    
    If m_bRuo Then
        m_oInstancias.BuscarSolicitudes iOrden, lTipo, txtId.Text, txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text, m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRSolicDep, basOptimizacion.gCodDepUsuario, basOptimizacion.gCodPersonaUsuario, , sMateriales, sCodArticulo, txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), m_sUON_CC, lEmpresa, sDepartamento, sComprador, sPartidas, , , , , m_bRSolicPerfUON, m_lIdPerfil
    Else
        m_oInstancias.BuscarSolicitudes iOrden, lTipo, txtId.Text, txtDescr.Text, sPet, vEstado, txtFecDesde.Text, txtFecHasta.Text, m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, , , , m_bRSolicDep, basOptimizacion.gCodDepUsuario, basOptimizacion.gCodPersonaUsuario, , sMateriales, sCodArticulo, txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), m_sUON_CC, lEmpresa, sDepartamento, sComprador, sPartidas, , , , , m_bRSolicPerfUON, m_lIdPerfil
    End If
    
    For Each oInstancia In m_oInstancias
        Select Case oInstancia.Estado
            Case EstadoSolicitud.Anulada
                sestado = m_sAnulada
            Case EstadoSolicitud.Aprobada
                sestado = m_sAprobada
            Case EstadoSolicitud.Cerrada
                sestado = m_sCerrada
            Case EstadoSolicitud.GenEnviada, EstadoSolicitud.GenAprobada
                sestado = m_sPendiente
            Case EstadoSolicitud.Rechazada
                sestado = m_sRechazada
        End Select
        
        If IsNull(oInstancia.FecNecesidad) Then
            vFecNec = ""
        Else
            vFecNec = Format(oInstancia.FecNecesidad, "Short date")
        End If
            
        sAsignadoA = ""
        sCodAsignadoA = ""
        sTrasladadaProv = ""
        If Not oInstancia.comprador Is Nothing Then
            sAsignadoA = oInstancia.comprador.Cod & " - " & NullToStr(oInstancia.comprador.nombre) & " " & NullToStr(oInstancia.comprador.Apel)
            sCodAsignadoA = oInstancia.comprador.Cod
        ElseIf oInstancia.Estado < EstadoSolicitud.Pendiente Then  'Si est� dentro del workflow
            If oInstancia.Trasladada = True Then  'Si est� trasladada muestra el proveedor o usuario al que se le ha trasladado
                If Not oInstancia.Destinatario Is Nothing Then
                    sAsignadoA = oInstancia.Destinatario.Cod & " - " & NullToStr(oInstancia.Destinatario.nombre) & " " & NullToStr(oInstancia.Destinatario.Apellidos)
                    sCodAsignadoA = oInstancia.Destinatario.Cod
                ElseIf Not oInstancia.DestinatarioProv Is Nothing Then
                    sAsignadoA = oInstancia.DestinatarioProv.Cod & " - " & NullToStr(oInstancia.DestinatarioProv.Den)
                    sCodAsignadoA = oInstancia.DestinatarioProv.Cod
                    sTrasladadaProv = oInstancia.DestinatarioProv.Cod
                End If
            ElseIf Not oInstancia.Aprobador Is Nothing Then  'muestra el aprobador actual
                sAsignadoA = oInstancia.Aprobador.Cod & " - " & NullToStr(oInstancia.Aprobador.nombre) & " " & NullToStr(oInstancia.Aprobador.Apellidos)
                sCodAsignadoA = oInstancia.Aprobador.Cod
            End If
        ElseIf Not oInstancia.Solicitud.Gestor Is Nothing Then  'si no tiene ning�n comprador asignado saca el gestor de la solicitud
            sAsignadoA = oInstancia.Solicitud.Gestor.Cod & " - " & NullToStr(oInstancia.Solicitud.Gestor.nombre) & " " & NullToStr(oInstancia.Solicitud.Gestor.Apellidos)
            sCodAsignadoA = oInstancia.Solicitud.Gestor.Cod
        End If
        
        sdbgSolicitudes.AddItem NullToStr(oInstancia.Solicitud.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & Format(oInstancia.FecAlta, "Short date") & Chr(m_lSeparador) & vFecNec & Chr(m_lSeparador) & oInstancia.Id & Chr(m_lSeparador) & oInstancia.DescrBreve & Chr(m_lSeparador) & oInstancia.importe / oInstancia.Cambio & Chr(m_lSeparador) & oInstancia.Peticionario.Cod & " - " & NullToStr(oInstancia.Peticionario.nombre) & " " & NullToStr(oInstancia.Peticionario.Apellidos) & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & sAsignadoA & Chr(m_lSeparador) & oInstancia.Estado & Chr(m_lSeparador) & oInstancia.Peticionario.Cod & Chr(m_lSeparador) & sCodAsignadoA & Chr(m_lSeparador) & sTrasladadaProv
        
    Next
    
    sdbgSolicitudes.MoveFirst
    
    If m_oInstancias.TotalPaginas < 2 Then
        OcultarPaginacion
    Else
        MostrarPaginacion
        lblPageNumber.caption = Replace(Replace(m_sPageNumber, "{n}", m_oInstancias.PaginaActual), "{m}", m_oInstancias.TotalPaginas)
        If m_oInstancias.PaginaActual = 1 Then
            cmdLast.Enabled = True
            cmdNext.Enabled = True
            cmdFirst.Enabled = False
            cmdPrevious.Enabled = False
        End If
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarGridSolicitudes", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub OcultarPaginacion()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picGridToolbar.Visible = False
    picGrid.Top = 0
    ResizeGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "OcultarPaginacion", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub MostrarPaginacion()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picGridToolbar.Visible = True
    picGrid.Top = picGridToolbar.Height
    ResizeGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "MostrarPaginacion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Configuraci�n de la seguridad de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub ConfigurarSeguridad()
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        chkEstado(1).Visible = True
        chkEstado(2).Visible = True
        chkEstado(3).Visible = True
        chkEstado(4).Visible = True
        chkEstado(5).Visible = True
        
        Select Case g_sOrigen
            Case "frmPROCE", "frmPROCEGrupo", "frmPROCEItem", "frmPROCEModifConfig", "SOLICITUD_ITEMS", "frmPROCEBuscar", "frmItemModificarValores"
                'Restricci�n de equipo
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APESolicRestEquipo)) Is Nothing) Then
                    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
                        'S�lo le afectar� la restricci�n si el usuario es un comprador.
                        m_bREquipo = True
                    End If
                End If
                
                'Restricci�n de asignaci�n
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APESolicRestAsig)) Is Nothing) Then
                    m_bRAsig = True
                End If
                
                'Restricci�n de unidad organizativa
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APESolicRestUO)) Is Nothing) Then
                    m_bRuo = True
                End If
                
                'Todas ellas sacadas de frmSolictudes
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestPerfUON)) Is Nothing) Then
                    If Not oUsuarioSummit.Perfil Is Nothing Then m_lIdPerfil = oUsuarioSummit.Perfil.Id
                    m_bRSolicPerfUON = True
                End If
                               
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrDpto)) Is Nothing) Then
                    m_bRSolicDep = True
                End If
                
                'SOLICEnCurso = 20715
                
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAbrirProcEnCurso)) Is Nothing) Then
                    m_bAbrirProcEnCurso = True
                End If
                
                'Buscar saca estados fijos: 100, 101, 2 y 7
                chkEstado(1).Visible = False
                chkEstado(2).Visible = False
                chkEstado(3).Visible = False
                chkEstado(4).Visible = False
                chkEstado(5).Visible = False
                
            Case "frmCatalogo", "frmCATLINBuscar"
                'Restricci�n de equipo
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicRestEquipo)) Is Nothing) Then
                    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
                        'S�lo le afectar� la restricci�n si el usuario es un comprador.
                        m_bREquipo = True
                    End If
                End If
                
                'Restricci�n de asignaci�n
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicRestAsig)) Is Nothing) Then
                    m_bRAsig = True
                End If
                
                'Restricci�n de unidad organizativa
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicRestUO)) Is Nothing) Then
                    m_bRuo = True
                End If
        Case Else
            'Restricci�n de equipo
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegSolicRestEquipo)) Is Nothing) Then
                If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
                    'S�lo le afectar� la restricci�n si el usuario es un comprador.
                    m_bREquipo = True
                End If
            End If
            
            'Restricci�n de asignaci�n
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegSolicRestAsig)) Is Nothing) Then
                m_bRAsig = True
            End If
            
            'Restricci�n de unidad organizativa
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegSolicRestUO)) Is Nothing) Then
                m_bRuo = True
            End If
            
            'Restriccion material usuario comprador
            If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing Then
                m_bRestrMatComp = True
            End If
        End Select
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


''' <summary>
''' Modifica la solicitud de un item.
''' Si se supera el VolumenAdjDir se mira si se puede ignorar la restricci�n
''' </summary>
''' <remarks>Llamada desde: cmdSeleccionar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub ModifSolicitudItems()
Dim oItem As CItem
Dim oItems As CItems
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim bErrores As Boolean
Dim vErrores As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set oItems = oFSGSRaiz.Generar_CItems
        Set oItem = oFSGSRaiz.Generar_CItem
        
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            
            With oItem
            
                .SolicitudId = m_oInstancias.Item(CStr(sdbgSolicitudes.Columns("ID").CellValue(sdbgSolicitudes.Bookmark))).Id
                .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                    Case EnItem
                        .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                            .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                        Else
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                    Case EnProceso
                        .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
                End Select
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                    Case EnItem
                        .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                            .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                        Else
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                    Case EnProceso
                        .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
                End Select
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                    Case EnItem
                        .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                    Case EnGrupo
                        .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                    Case EnProceso
                        .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
                End Select
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                    Case EnItem
                        .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                            .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                            .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                        Else
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                    Case EnProceso
                        .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                        .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
                End Select
                
                If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .ArticuloCod = Null
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                
                If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .Descr = Null
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                
                .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
            End With
            
            oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , , oItem.FECACT, oItem.SolicitudId
        
        Next

        Screen.MousePointer = vbHourglass
        teserror = oItems.ModificarValores(, , , , , , , , , , , True, , basOptimizacion.gvarCodUsuario)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            If teserror.NumError <> TESVolumenAdjDirSuperado Then
                basErrores.TratarError teserror
                Set oItems = Nothing
                Set oItem = Nothing
                Unload Me
                frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                Exit Sub
            Else
                If g_sOrigen = "SOLICITUD_ITEMS" Then
                    If frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then basErrores.TratarError teserror
                Else
                    basErrores.TratarError teserror
                End If
            End If
        Else
            bErrores = teserror.Arg2
            vErrores = teserror.Arg1
            If bErrores Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
                Screen.MousePointer = vbHourglass
            End If
        End If
        
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                frmPROCE.sdbgItems.Columns("SOLICITUD").Value = oItems.Item(i + 1).SolicitudId
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            Next
        Else
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                If vErrores(1, i) = 0 Then 'Ning�n error
                    frmPROCE.sdbgItems.Columns("SOLICITUD").Value = oItems.Item(i + 1).SolicitudId
                    frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
                End If
            Next
        End If
        
        frmPROCE.sdbgItems.SelBookmarks.RemoveAll
        frmPROCE.sdbgItems.Update
        frmPROCE.g_bUpdate = True
        Screen.MousePointer = vbNormal
        
        Set oItem = Nothing
        Set oItems = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "ModifSolicitudItems", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub ConfigurarPantalla()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_sOrigen = "frmPROCEBuscar" Then Exit Sub
    
    If g_sOrigen = "frmLstSeguimiento" Or g_sOrigen = "frmSeguimiento" Then
        chkEstado(2).Visible = False
        chkEstado(4).Visible = False
        chkEstado(5).Visible = False
        Exit Sub
    End If
    
    'todo revisar 1580
    
'    'Si se le llama desde un formulario que no sea el de b�squeda de procesos
'    'configura la pantalla ocultando los estados
'    picEstado.Visible = False
'    ShapeSelecc.Width = 9475
'    cmdBuscar.Left = 9700
'    sdbgSolicitudes.Columns("ESTADO").Visible = False
'    sdbgSolicitudes.Columns("PET").Width = 1844.142
'    sdbgSolicitudes.Columns("COMP").Width = 1844.882
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "ConfigurarPantalla", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcTipoSolicit_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcTipoSolicit.DroppedDown Then
        sdbcTipoSolicit = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcTipoSolicit_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoSolicit_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcTipoSolicit.Value = "..." Or sdbcTipoSolicit.Value = "" Then
        sdbcTipoSolicit.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcTipoSolicit.Text = sdbcTipoSolicit.Columns(1).Text
    m_bRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcTipoSolicit_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cargar la combo con todos los Tipos Solicitud con acceso al EP
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcTipoSolicit_DropDown()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcTipoSolicit.RemoveAll
    
    CargarTiposSolicitud
        
    sdbcTipoSolicit.SelStart = 0
    sdbcTipoSolicit.SelLength = Len(sdbcTipoSolicit.Text)
    sdbcTipoSolicit.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcTipoSolicit_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoSolicit_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcTipoSolicit.DataFieldList = "Column 1"
    sdbcTipoSolicit.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcTipoSolicit_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoSolicit_PositionList(ByVal Text As String)
PositionList sdbcTipoSolicit, Text
End Sub

Private Sub MostrarDetalleProveedor(ByVal CodProve As String)
Dim oProves As CProveedores
Dim oProve As CProveedor
Dim oCon As CContacto
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado m_sProveedor
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    
    Set frmProveDetalle.g_oProveSeleccionado = oProve
    frmProveDetalle.caption = CodProve & "  " & oProve.Den
    frmProveDetalle.lblCodPortProve = NullToStr(oProve.CodPortal)
    frmProveDetalle.g_bPremium = oProve.EsPremium
    frmProveDetalle.g_bActivo = oProve.EsPremiumActivo
    frmProveDetalle.lblDIR = NullToStr(oProve.Direccion)
    frmProveDetalle.lblCP = NullToStr(oProve.cP)
    frmProveDetalle.lblPOB = NullToStr(oProve.Poblacion)
    frmProveDetalle.lblPaiCod = NullToStr(oProve.CodPais)
    frmProveDetalle.lblPaiDen = NullToStr(oProve.DenPais)
    frmProveDetalle.lblMonCod = NullToStr(oProve.CodMon)
    frmProveDetalle.lblMonDen = NullToStr(oProve.DenMon)
    frmProveDetalle.lblProviCod = NullToStr(oProve.CodProvi)
    frmProveDetalle.lblProviDen = NullToStr(oProve.DenProvi)
    frmProveDetalle.lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    frmProveDetalle.lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    frmProveDetalle.lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then
        frmProveDetalle.lblCal1Den = oProve.Calif1
    End If
    
    If Trim(oProve.Calif2) <> "" Then
        frmProveDetalle.lblcal2den = oProve.Calif2
    End If
    
    If Trim(oProve.Calif3) <> "" Then
        frmProveDetalle.lblcal3den = oProve.Calif3
    End If
    
    frmProveDetalle.lblCal1Val = DblToStr(oProve.Val1)
    frmProveDetalle.lblCal2Val = DblToStr(oProve.Val2)
    frmProveDetalle.lblCal3Val = DblToStr(oProve.Val3)

    frmProveDetalle.lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        
        frmProveDetalle.sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    
    Next
        
    Set oProves = Nothing
    Set oProve = Nothing
    
    
    frmProveDetalle.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "MostrarDetalleProveedor", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub splitterMain_Resize(ByVal BorderPanes As SSSplitter.Panes)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ResizeGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "splitterMain_Resize", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ResizeGrid()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    picGridToolbar.Width = picBottom.Width
    picGrid.Width = picBottom.Width
    If picGridToolbar.Visible = True Or (sdbgSolicitudes.Visible = False And picGridToolbar.Visible = False) Then
        picGrid.Height = picBottom.Height - picGridToolbar.Height
    Else
        picGrid.Height = picBottom.Height
    End If
    sdbgSolicitudes.Width = picGrid.Width - 120
    sdbgSolicitudes.Height = picGrid.Height - 120
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "ResizeGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub txtCentroCoste_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCentroCosteValidado = False
    lstCentroCoste.Visible = False
    txtCentroCoste.BackColor = &H80000005
    
    m_sUON1_CC = ""
    m_sUON2_CC = ""
    m_sUON3_CC = ""
    m_sUON4_CC = ""
    m_sUON_CC = ""
    
    Dim pos As Integer
    pos = InStr(1, txtCentroCoste.Text, " - ", vbTextCompare)
    If pos > 0 Then
        txtCentroCoste.Tag = Trim(Mid(txtCentroCoste.Text, 1, pos))
    Else
        txtCentroCoste.Tag = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "txtCentroCoste_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtCentroCoste_KeyPress(KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        txtCentroCoste_Validate True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "txtCentroCoste_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtCentroCoste_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bCentroCosteValidado And txtCentroCoste.Text <> "" Then
        Dim sCod As String
        If Len(txtCentroCoste.Tag) > 0 Then
            sCod = CStr(txtCentroCoste.Tag)
        Else
            sCod = txtCentroCoste.Text
        End If
        
        If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
            CargarCentroCostePorCentroSM sCod
        Else
            If m_bRuo Then
               CargarCentroCostePorUON sCod, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, oUsuarioSummit.Cod
            Else
                If oUsuarioSummit.pyme <> 0 Then
                    CargarCentroCostePorUON sCod, basOptimizacion.gUON1Usuario, , , oUsuarioSummit.Cod
                Else
                    CargarCentroCostePorCentroSM sCod, oUsuarioSummit.Cod
                End If
            End If
        End If
  
        If m_oCentrosCoste.Count > 0 Then
            If m_oCentrosCoste.Count > 1 Then
                lstCentroCoste.clear
                Dim sItemCentro As String
                Dim i As Integer
                For i = 1 To m_oCentrosCoste.Count
                    sItemCentro = m_oCentrosCoste.Item(i).CodConcat
                    If m_oCentrosCoste.Item(1).UON1 <> "" Then
                        sItemCentro = sItemCentro & " (" & m_oCentrosCoste.Item(1).UON1
                        If m_oCentrosCoste.Item(1).UON2 <> "" Then
                            sItemCentro = sItemCentro & "-" & m_oCentrosCoste.Item(1).UON2
                            If m_oCentrosCoste.Item(1).UON3 <> "" Then
                                sItemCentro = sItemCentro & "-" & m_oCentrosCoste.Item(1).UON3
                            End If
                        End If
                        sItemCentro = sItemCentro & ")"
                    End If
                    lstCentroCoste.AddItem sItemCentro
                Next
                lstCentroCoste.Visible = True
                If Me.Visible Then lstCentroCoste.SetFocus
            ElseIf m_oCentrosCoste.Count = 1 Then
                txtCentroCoste.Text = m_oCentrosCoste.Item(1).CodConcat
                If m_oCentrosCoste.Item(1).UON1 <> "" Then
                    txtCentroCoste.Text = txtCentroCoste.Text & " (" & m_oCentrosCoste.Item(1).UON1
                    If m_oCentrosCoste.Item(1).UON2 <> "" Then
                        txtCentroCoste.Text = txtCentroCoste.Text & "-" & m_oCentrosCoste.Item(1).UON2
                        If m_oCentrosCoste.Item(1).UON3 <> "" Then
                            txtCentroCoste.Text = txtCentroCoste.Text & "-" & m_oCentrosCoste.Item(1).UON3
                        End If
                    End If
                    txtCentroCoste.Text = txtCentroCoste.Text & ")"
                End If
                txtCentroCoste.Tag = m_oCentrosCoste.Item(1).COD_SM
                txtCentroCoste.BackColor = &HC0FFC0
    
                m_sUON1_CC = m_oCentrosCoste.Item(1).UON1
                m_sUON2_CC = m_oCentrosCoste.Item(1).UON2
                m_sUON3_CC = m_oCentrosCoste.Item(1).UON3
                m_sUON4_CC = m_oCentrosCoste.Item(1).UON4
                
                m_sUON_CC = FormatearUON(m_oCentrosCoste.Item(1))
            End If
        Else
            txtCentroCoste.Tag = ""
            txtCentroCoste.BackColor = &H8080FF
        End If
        m_bCentroCosteValidado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "txtCentroCoste_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarCentroCostePorCentroSM(ByVal sCod As String, Optional ByVal sUsu As String)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Len(sCod) > 0 Then
        '(Ver txtCentro_Validate de frmSelActivo)
        If Len(sUsu) > 0 Then
            m_oCentrosCoste.CargarCentrosDeCoste sCod, , , , , , sUsu
            If m_oCentrosCoste.Count = 0 Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod, , , , , sUsu
                If m_oCentrosCoste.Count = 0 Then
                    m_oCentrosCoste.CargarCentrosDeCoste , , sCod, , , , sUsu
                    If m_oCentrosCoste.Count = 0 Then
                        m_oCentrosCoste.CargarCentrosDeCoste , , , sCod, , , sUsu
                        If m_oCentrosCoste.Count = 0 Then
                            m_oCentrosCoste.CargarCentrosDeCoste , , , , sCod, , sUsu
                            If m_oCentrosCoste.Count = 0 Then
                                m_oCentrosCoste.CargarCentrosDeCoste , , , , , sCod, sUsu
                            End If
                        End If
                    End If
                End If
            End If
        Else
            m_oCentrosCoste.CargarCentrosDeCoste sCod
            If m_oCentrosCoste.Count = 0 Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod
                If m_oCentrosCoste.Count = 0 Then
                    m_oCentrosCoste.CargarCentrosDeCoste , , sCod
                    If m_oCentrosCoste.Count = 0 Then
                        m_oCentrosCoste.CargarCentrosDeCoste , , , sCod
                        If m_oCentrosCoste.Count = 0 Then
                            m_oCentrosCoste.CargarCentrosDeCoste , , , , sCod
                            If m_oCentrosCoste.Count = 0 Then
                                m_oCentrosCoste.CargarCentrosDeCoste , , , , , sCod
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarCentroCostePorCentroSM", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CargarCentroCostePorUON(ByVal sCod As String, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal sUsu As String)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Len(sCod) > 0 Then
        If Len(sUsu) > 0 Then
            If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sUON3, sCod, , sUsu
            ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sCod, , , sUsu
            ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sCod, , , , sUsu
            ElseIf sUON1 = "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod, , , , , sUsu
            End If
        Else
            If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sUON3, sCod
            ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sCod
            ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sCod
            ElseIf sUON1 = "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarCentroCostePorUON", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub txtContrato_Change(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bPres5Validado = False
    lstContrato.Visible = False
    txtContrato(Index).BackColor = &H80000005
    
    m_asPres5UON(Index) = ""
    m_aPres5(Index).sPres5_Nivel0 = ""
    m_aPres5(Index).sPres5_Nivel1 = ""
    m_aPres5(Index).sPres5_Nivel2 = ""
    m_aPres5(Index).sPres5_Nivel3 = ""
    m_aPres5(Index).sPres5_Nivel4 = ""
    
    Dim pos As Integer
    pos = InStr(1, txtContrato(Index).Text, " - ", vbTextCompare)
    If pos > 0 Then
        txtContrato(Index).Tag = Trim(Mid(txtContrato(Index).Text, 1, pos))
    Else
        txtContrato(Index).Tag = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "txtContrato_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtContrato_KeyPress(Index As Integer, KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        txtContrato_Validate Index, True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "txtContrato_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtContrato_Validate(Index As Integer, Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPres5Validado And txtContrato(Index).Text <> "" Then
        Dim sCod As String
        If Len(txtContrato(Index).Tag) > 0 Then
            sCod = CStr(txtContrato(Index).Tag)
        Else
            sCod = txtContrato(Index).Text
        End If
        
        If m_bRuo Then
            m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , sCod
        Else
            If oUsuarioSummit.pyme <> 0 Then
                m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, , , , sCod
            Else
                m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, , , , , sCod
            End If
        End If
        
        If m_oContratosPres.Count > 0 Then
            If m_oContratosPres.Count > 1 Then
                lstContrato.clear
                Dim sItemContrato As String
                Dim i As Integer
                For i = 1 To m_oContratosPres.Count
                    sItemContrato = m_oContratosPres.Item(i).Cod & " - " & m_oContratosPres.Item(i).Den
                    lstContrato.AddItem sItemContrato
                Next
                lstContrato.Left = txtContrato(Index).Left
                lstContrato.Top = txtContrato(Index).Top + txtContrato(Index).Height
                lstContrato.Height = 570
                lstContrato.Visible = True
                If Me.Visible Then lstContrato.SetFocus
            ElseIf m_oContratosPres.Count = 1 Then
                txtContrato(Index).Text = m_oContratosPres.Item(1).Cod & " - " & m_oContratosPres.Item(1).Den
                txtContrato(Index).Tag = m_oContratosPres.Item(1).Cod
                txtContrato(Index).BackColor = &HC0FFC0
                
                m_asPres5UON(Index) = FormatearPres5o(m_asPres5Niv0(Index), m_oContratosPres.Item(1))
                m_aPres5(Index).sPres5_Nivel0 = m_asPres5Niv0(Index)
                m_aPres5(Index).sPres5_Nivel1 = m_oContratosPres.Item(1).Pres1
                m_aPres5(Index).sPres5_Nivel2 = m_oContratosPres.Item(1).Pres2
                m_aPres5(Index).sPres5_Nivel3 = m_oContratosPres.Item(1).Pres3
                m_aPres5(Index).sPres5_Nivel4 = m_oContratosPres.Item(1).Pres4
            End If
        Else
            txtContrato(Index).Tag = ""
            txtContrato(Index).BackColor = &H8080FF
        End If
        m_bPres5Validado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "txtContrato_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtDenArticulo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticuloDen = True
    If Not m_bEditandoArticulo And Not m_bEditandoArticuloConDropDown Then
        sdbcArticulo.Value = ""
    End If
    m_bEditandoArticuloDen = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "txtDenArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtMateriales_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         cmdLimpiarMateriales_Click
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "txtMateriales_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Public Sub PonerMatSeleccionado()
    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    Dim sNuevoCod1 As String
    Dim sNuevoCod2 As String
    Dim sNuevoCod3 As String
    Dim sNuevoCod4 As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    Set m_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sNuevoCod1 = oGMN1Seleccionado.Cod
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sNuevoCod2 = oGMN2Seleccionado.Cod
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sNuevoCod3 = oGMN3Seleccionado.Cod
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sNuevoCod4 = oGMN4Seleccionado.Cod
    End If
    If m_sGMN1Cod = sNuevoCod1 And m_sGMN2Cod = sNuevoCod2 And m_sGMN3Cod = sNuevoCod3 And m_sGMN4Cod = sNuevoCod4 Then
        Exit Sub
    End If
    
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
        
    If Not oGMN1Seleccionado Is Nothing Then
        m_sGMN1Cod = oGMN1Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & oGMN1Seleccionado.Den
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        m_sGMN2Cod = oGMN2Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & oGMN2Seleccionado.Den
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        m_sGMN3Cod = oGMN3Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & oGMN3Seleccionado.Den
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        m_sGMN4Cod = oGMN4Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & m_sGMN4Cod & " - " & oGMN4Seleccionado.Den
    End If
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "PonerMatSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub PonerDenominaciondeMaterial()
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4

    Dim strCadena As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_sGMN4Cod <> "" Then
        Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
        strCadena = oGMN4.ObtenerDenGMN4(m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod, gParametrosInstalacion.gIdioma)
        If strCadena = "" Then
            txtMateriales.Text = ""
        Else
            txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & m_sGMN4Cod & " - " & strCadena
        End If
    ElseIf m_sGMN3Cod <> "" Then
        Set oGMN3 = oFSGSRaiz.generar_CGrupoMatNivel3
        strCadena = oGMN3.ObtenerDenGMN3(m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, gParametrosInstalacion.gIdioma)
        If strCadena = "" Then
            txtMateriales.Text = ""
        Else
            txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & strCadena
        End If
    ElseIf m_sGMN2Cod <> "" Then
        Set oGMN2 = oFSGSRaiz.generar_CGrupoMatNivel2
        strCadena = oGMN2.ObtenerDenGMN2(m_sGMN1Cod, m_sGMN2Cod, gParametrosInstalacion.gIdioma)
        If strCadena = "" Then
            txtMateriales.Text = ""
        Else
            txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & strCadena
        End If
    ElseIf m_sGMN1Cod <> "" Then
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
        strCadena = oGMN1.ObtenerDenGMN1(m_sGMN1Cod, gParametrosInstalacion.gIdioma)
        If strCadena = "" Then
            txtMateriales.Text = ""
        Else
            txtMateriales.Text = m_sGMN1Cod & " - " & strCadena
        End If
    End If
    
    Set oGMN1 = Nothing
    Set oGMN2 = Nothing
    Set oGMN3 = Nothing
    Set oGMN4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "PonerDenominaciondeMaterial", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Poner Denominacion Centro Coste
''' </summary>
''' <param name="sUON1">UON1</param>
''' <param name="sUON2">UON2</param>
''' <param name="sUON3">UON3</param>
''' <param name="sUON4">UON4</param>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros ; Tiempo m�ximo: 0,2</remarks>
Private Sub PonerDenominacionCentroCoste(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String, ByVal sUON4 As String)
    Dim oCentroCoste As CCentroCoste
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCentroCoste = oFSGSRaiz.Generar_CCentroCoste
    Dim sDen As String
    sDen = oCentroCoste.DevolverDenominacion(sUON1, sUON2, sUON3, sUON4)
    
    oCentroCoste.UON1 = m_sUON1_CC
    oCentroCoste.UON2 = m_sUON2_CC
    oCentroCoste.UON3 = m_sUON3_CC
    oCentroCoste.UON4 = m_sUON4_CC
    
    If sUON4 <> "" Then
        txtCentroCoste.Text = sUON4 & " - " & sDen & " (" & sUON1 & "-" & sUON2 & "-" & sUON3 & ")"
    ElseIf sUON3 <> "" Then
        txtCentroCoste.Text = sUON3 & " - " & sDen & " (" & sUON1 & "-" & sUON2 & ")"
    ElseIf sUON2 <> "" Then
        txtCentroCoste.Text = sUON2 & " - " & sDen & " (" & sUON1 & ")"
    ElseIf sUON1 <> "" Then
        txtCentroCoste.Text = sUON1 & " - " & sDen
    End If
    
    m_sUON_CC = FormatearUON(oCentroCoste)
    
    Set oCentroCoste = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "PonerDenominacionCentroCoste", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CargarPartidas()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 0 Then
            If g_oParametrosSM.Count > 1 Then
                Dim pres As CParametroSM
                For Each pres In g_oParametrosSM
                    
                    sdbgPartidas.AddItem pres.PRES5 & Chr(m_lSeparador) & _
                                        pres.NomNivelImputacion & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        pres.impnivel & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        0 & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        ""
                Next
    
                lblContrato(0).Visible = False
                txtContrato(0).Visible = False
                cmdBuscarContrato(0).Visible = False
                cmdLimpiarContrato(0).Visible = False
                sdbgPartidas.Visible = True
    
                sdbgPartidas.Top = lblContrato(0).Top
                sdbgPartidas.Left = lblContrato(0).Left
                sdbgPartidas.Width = (cmdBuscarContrato(0).Left - lblContrato(0).Left) + cmdBuscarContrato(0).Width
                
                Dim cota As Integer
                cota = 3
                If g_oParametrosSM.Count > 2 And g_oParametrosSM.Count < cota Then
                    sdbgPartidas.Height = sdbgPartidas.Rows * (sdbgPartidas.RowHeight + 10)
                    m_sIncrementoAltura = (sdbgPartidas.Rows - 3) * (sdbgPartidas.RowHeight + 10) + 250
                    picBusquedaAvanzada.Height = picBusquedaAvanzada.Height + m_sIncrementoAltura
                    shapeBusquedaAvanzada.Height = shapeBusquedaAvanzada.Height + m_sIncrementoAltura
                    sdbgPartidas.Columns(1).Width = sdbgPartidas.Width * 0.19
                    sdbgPartidas.Columns(2).Width = sdbgPartidas.Width * 0.75
                Else
                    sdbgPartidas.ScrollBars = ssScrollBarsVertical
                    sdbgPartidas.Height = cota * (sdbgPartidas.RowHeight + 10)
                    m_sIncrementoAltura = (cota - 3) * (sdbgPartidas.RowHeight + 10) + 250
                    picBusquedaAvanzada.Height = picBusquedaAvanzada.Height + m_sIncrementoAltura
                    shapeBusquedaAvanzada.Height = shapeBusquedaAvanzada.Height + m_sIncrementoAltura
                    sdbgPartidas.Columns(1).Width = sdbgPartidas.Width * 0.19
                    sdbgPartidas.Columns(2).Width = sdbgPartidas.Width * 0.71
                End If
    
                lblOcultarBusquedaAvanzada.Top = lblOcultarBusquedaAvanzada.Top + m_sIncrementoAltura
            Else
                lblContrato(0).caption = g_oParametrosSM.Item(1).NomNivelImputacion & ":"
                m_asPres5Niv0(0) = g_oParametrosSM.Item(1).PRES5
                m_aiNivelImp(0) = g_oParametrosSM.Item(1).impnivel
            End If
                    
    '        lblContrato(0).caption = g_oParametrosSM.Item(1).NomNivelImputacion & ":"
    '        m_asPres5Niv0(0) = g_oParametrosSM.Item(1).pres5
    '        m_aiNivelImp(0) = g_oParametrosSM.Item(1).impnivel
    
        End If
    Else
        lblContrato(0).Visible = False
        txtContrato(0).Visible = False
        cmdBuscarContrato(0).Visible = False
        cmdLimpiarContrato(0).Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarPartidas", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga la configuraci�n de los campos,orden y filtro para el usuario de la aplicaci�n
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarConfiguracionVisorSolPP()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oConfVisorSolPPs = oFSGSRaiz.Generar_CConfVistasVisorSolPP
    g_oConfVisorSolPPs.CargarConfVisorSolPP oUsuarioSummit.Cod, True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarConfiguracionVisorSolPP", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga la configuraci�n de los campos,orden y filtro para el usuario de la aplicaci�n
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarConfiguracionVisorSol()
    Dim oConfVisoresSol As CConfVistasVisorSol
    Dim ConfigVisorSol As TipoConfigVisorSol
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oConfVisoresSol = oFSGSRaiz.Generar_CConfVistasVisorSol
    
    oConfVisoresSol.CargarConfVisorSol oUsuarioSummit.Cod
    
    If oConfVisoresSol.Item(1) Is Nothing Then
        'Carga la configuraci�n por defecto
        ConfigVisorSol.dblTipoWidth = 2294.92
        ConfigVisorSol.dblAltaWidth = 945
        ConfigVisorSol.dblNecesidadWidth = 945
        ConfigVisorSol.dblIdentWidth = 600
        ConfigVisorSol.dblDenominacionWidth = 1650
        ConfigVisorSol.dblImporteWidth = 1005
        ConfigVisorSol.dblPeticionarioWidth = 2940
        ConfigVisorSol.dblUONWidth = 1600
        ConfigVisorSol.dblEstadoWidth = 1100
        ConfigVisorSol.dblAsignadoAWidth = 1600
        ConfigVisorSol.dblDepartamentoWidth = 1400
        ConfigVisorSol.dblSinProcesoWidth = 300

        ConfigVisorSol.vTipo = Null
        ConfigVisorSol.vIdentificador = Null
        ConfigVisorSol.vDescripcion = Null
        ConfigVisorSol.vPeticionario = Null
        ConfigVisorSol.vDesde = Null
        ConfigVisorSol.vHasta = Null
        ConfigVisorSol.vUON1 = Null
        ConfigVisorSol.vUON2 = Null
        ConfigVisorSol.vUON3 = Null
        ConfigVisorSol.vEnCursoAprobacion = False
        ConfigVisorSol.vPendientes = False
        ConfigVisorSol.vAprobadas = False
        ConfigVisorSol.vRechazadas = False
        ConfigVisorSol.vAnuladas = False
        ConfigVisorSol.vCerradas = False
        ConfigVisorSol.sGMN1 = ""
        ConfigVisorSol.sGMN2 = ""
        ConfigVisorSol.sGMN3 = ""
        ConfigVisorSol.sGMN4 = ""
        ConfigVisorSol.sCodArticulo = ""
        ConfigVisorSol.sDenArticulo = ""
        ConfigVisorSol.dImporteDesde = 0
        ConfigVisorSol.dImporteHasta = 0
        ConfigVisorSol.lEmpresa = 0
        ConfigVisorSol.sDepartamento = ""
        ConfigVisorSol.sComprador = ""
        ConfigVisorSol.sUON1_CC = ""
        ConfigVisorSol.sUON2_CC = ""
        ConfigVisorSol.sUON3_CC = ""
        ConfigVisorSol.sUON4_CC = ""
        ConfigVisorSol.Proveedor = ""
        ConfigVisorSol.bAlertaSinProceso = False
        ConfigVisorSol.bAlertaSinPedido = False
        ConfigVisorSol.bAlertaIntegracion = False
        
        oConfVisoresSol.Add oUsuarioSummit.Cod, ConfigVisorSol
    End If
    
    Set g_oConfVisorSol = oConfVisoresSol.Item(1)
    
    Set oConfVisoresSol = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarConfiguracionVisorSol", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Comprueba que los filtros sean correctos y los aplica
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub ComprobarYAplicarFiltros()
    Dim bMostrarBusquedaAvanzada As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bMostrarBusquedaAvanzada = False

    '******************** Filtro Tipo de solicitud ***************************
    If Not IsNull(g_oConfVisorSol.Tipo) Then
        sdbcTipoSolicit_PositionListId (g_oConfVisorSol.Tipo)
        sdbcTipoSolicit.Text = g_oConfVisorSol.Tipo
    Else
        sdbcTipoSolicit.Text = ""
    End If
    
    '******************** Filtro Identificador ***************************
    txtId.Text = NullToStr(g_oConfVisorSol.Identificador)
    
    '******************** Filtro Descripci�n ***************************
    txtDescr.Text = NullToStr(g_oConfVisorSol.Descripcion)
     
    '******************** Filtro Desde  ***************************
    txtFecDesde.Text = NullToStr(g_oConfVisorSol.Desde)
    
    '******************** Filtro Hasta ***************************
    txtFecHasta.Text = NullToStr(g_oConfVisorSol.Hasta)
    
    '******************** Filtro Peticionario ***************************
    If Not IsNull(g_oConfVisorSol.Peticionario) Then
        sdbcPeticionario.Text = sdbcPeticionario_PositionListId(g_oConfVisorSol.Peticionario)
    Else
        sdbcPeticionario.Text = ""
    End If
                
    '******************** Materiales ***************************
    m_sGMN1Cod = NullToStr(g_oConfVisorSol.GMN1)
    m_sGMN2Cod = NullToStr(g_oConfVisorSol.GMN2)
    m_sGMN3Cod = NullToStr(g_oConfVisorSol.GMN3)
    m_sGMN4Cod = NullToStr(g_oConfVisorSol.GMN4)
    PonerDenominaciondeMaterial
    If m_sGMN1Cod <> "" Then
        bMostrarBusquedaAvanzada = True
    End If
    
    '******************** C�digo Art�culo ***************************
    If g_oConfVisorSol.CodArticulo <> "" Then
        sdbcArticulo.Text = sdbcArticulo_PositionListId(g_oConfVisorSol.CodArticulo)
        bMostrarBusquedaAvanzada = True
    Else
        sdbcArticulo.Text = ""
    End If
    
    '******************** Denominaci�n Art�culo ***************************
    If NullToStr(g_oConfVisorSol.DenArticulo) <> "" Then
        bMostrarBusquedaAvanzada = True
    End If
    txtDenArticulo.Text = NullToStr(g_oConfVisorSol.DenArticulo)
        
    '******************** Importe Desde  ***************************
    If NullToStr(g_oConfVisorSol.ImporteDesde) <> "0" Then
        txtImporteDesde.Text = NullToStr(g_oConfVisorSol.ImporteDesde)
        bMostrarBusquedaAvanzada = True
    Else
        txtImporteDesde.Text = ""
    End If
    
    '******************** Importe Hasta ***************************
    If NullToStr(g_oConfVisorSol.ImporteHasta) <> "0" Then
        txtImporteHasta.Text = NullToStr(g_oConfVisorSol.ImporteHasta)
        bMostrarBusquedaAvanzada = True
    Else
        txtImporteHasta.Text = ""
    End If
    
    '******************** Empresa ***************************
    If g_oConfVisorSol.Empresa <> 0 Then
        sdbcEmpresa.Text = sdbcEmpresa_PositionListId(g_oConfVisorSol.Empresa)
        bMostrarBusquedaAvanzada = True
    Else
        sdbcEmpresa.Text = ""
    End If
    
    '******************** Departamento ***************************
    If g_oConfVisorSol.Departamento <> "" Then
        sdbcDepartamento.Text = sdbcDepartamento_PositionListId(g_oConfVisorSol.Departamento)
        bMostrarBusquedaAvanzada = True
    Else
        sdbcDepartamento.Text = ""
    End If
    
    '******************** Comprador ***************************
    If g_oConfVisorSol.comprador <> "" Then
        sdbcComprador.Text = sdbcComprador_PositionListId(g_oConfVisorSol.comprador)
        bMostrarBusquedaAvanzada = True
    Else
        sdbcComprador.Text = ""
    End If
    
    If g_oConfVisorSol.UON1_CC <> "" Then
        m_sUON1_CC = g_oConfVisorSol.UON1_CC
        If g_oConfVisorSol.UON2_CC <> "" Then
            m_sUON2_CC = g_oConfVisorSol.UON2_CC
            If g_oConfVisorSol.UON3_CC <> "" Then
                m_sUON3_CC = g_oConfVisorSol.UON3_CC
                If g_oConfVisorSol.UON4_CC <> "" Then
                    m_sUON4_CC = g_oConfVisorSol.UON4_CC
                End If
            End If
        End If
        bMostrarBusquedaAvanzada = True
    End If
    PonerDenominacionCentroCoste m_sUON1_CC, m_sUON2_CC, m_sUON3_CC, m_sUON4_CC

    If Not g_oConfVisorSolPPs Is Nothing Then
        If g_oConfVisorSolPPs.Count > 0 Then
            If g_oParametrosSM.Count > 1 Then
                Dim i As Long
                Dim Row As Long
                Dim vbm As Variant
                For i = 1 To g_oConfVisorSolPPs.Count
                    Row = sdbgPartidas.Rows - 1
                    While Row >= 0
                        vbm = sdbgPartidas.AddItemBookmark(Row)
                        sdbgPartidas.Bookmark = vbm
                        If sdbgPartidas.Columns("COD").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel0 Then
                            If Len(g_oConfVisorSolPPs.Item(i).Pres5_Nivel1) > 0 Then
                                sdbgPartidas.Columns("PRES5NIV1").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel1
                                sdbgPartidas.Columns("TAG").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel1
                                If Len(g_oConfVisorSolPPs.Item(i).Pres5_Nivel2) > 0 Then
                                    sdbgPartidas.Columns("PRES5NIV2").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel2
                                    sdbgPartidas.Columns("TAG").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel2
                                    If Len(g_oConfVisorSolPPs.Item(i).Pres5_Nivel3) > 0 Then
                                        sdbgPartidas.Columns("PRES5NIV3").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel3
                                        sdbgPartidas.Columns("TAG").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel3
                                        If Len(g_oConfVisorSolPPs.Item(i).Pres5_Nivel4) > 0 Then
                                            sdbgPartidas.Columns("PRES5NIV4").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel4
                                            sdbgPartidas.Columns("TAG").Value = g_oConfVisorSolPPs.Item(i).Pres5_Nivel4
                                        End If
                                    End If
                                End If
                            End If
                            If Len(sdbgPartidas.Columns("TAG").Value) > 0 Then
                                sdbgPartidas.Columns("VALOR").Value = sdbgPartidas.Columns("TAG").Value & " - " & m_oContratosPres.DevolverDenominacion(g_oConfVisorSolPPs.Item(i).Pres5_Nivel0, g_oConfVisorSolPPs.Item(i).Pres5_Nivel1, g_oConfVisorSolPPs.Item(i).Pres5_Nivel2, g_oConfVisorSolPPs.Item(i).Pres5_Nivel3, g_oConfVisorSolPPs.Item(i).Pres5_Nivel4)
                                sdbgPartidas.Columns("PRES5UON").Value = FormatearPres5s(g_oConfVisorSolPPs.Item(i).Pres5_Nivel0, g_oConfVisorSolPPs.Item(i).Pres5_Nivel1, g_oConfVisorSolPPs.Item(i).Pres5_Nivel2, g_oConfVisorSolPPs.Item(i).Pres5_Nivel3, g_oConfVisorSolPPs.Item(i).Pres5_Nivel4)
                                sdbgPartidas.Columns("VALIDADO").Value = 2
                            End If
                        End If
                        Row = Row - 1
                    Wend
                Next
                sdbgPartidas.Update
            Else
                Dim pres5Niv0 As String
                Dim pres5Niv1 As String
                Dim pres5Niv2 As String
                Dim pres5Niv3 As String
                Dim pres5Niv4 As String
                
                pres5Niv0 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel0
                If Len(g_oConfVisorSolPPs.Item(1).Pres5_Nivel1) > 0 Then
                    pres5Niv1 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel1
                    txtContrato(0).Tag = g_oConfVisorSolPPs.Item(1).Pres5_Nivel1
                    If Len(g_oConfVisorSolPPs.Item(1).Pres5_Nivel2) > 0 Then
                        pres5Niv2 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel2
                        txtContrato(0).Tag = g_oConfVisorSolPPs.Item(1).Pres5_Nivel2
                        If Len(g_oConfVisorSolPPs.Item(1).Pres5_Nivel3) > 0 Then
                            pres5Niv3 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel3
                            txtContrato(0).Tag = g_oConfVisorSolPPs.Item(1).Pres5_Nivel3
                            If Len(g_oConfVisorSolPPs.Item(1).Pres5_Nivel4) > 0 Then
                                pres5Niv4 = g_oConfVisorSolPPs.Item(1).Pres5_Nivel4
                                txtContrato(0).Tag = g_oConfVisorSolPPs.Item(1).Pres5_Nivel4
                            End If
                        End If
                    End If
                End If
                
                txtContrato(0).Text = txtContrato(0).Tag & " - " & m_oContratosPres.DevolverDenominacion(pres5Niv0, pres5Niv1, pres5Niv2, pres5Niv3, pres5Niv4)
            
                m_aPres5(0).sPres5_Nivel0 = pres5Niv0
                m_aPres5(0).sPres5_Nivel1 = pres5Niv1
                m_aPres5(0).sPres5_Nivel2 = pres5Niv2
                m_aPres5(0).sPres5_Nivel3 = pres5Niv3
                m_aPres5(0).sPres5_Nivel4 = pres5Niv4
                          
                m_asPres5UON(0) = FormatearPres5s(g_oConfVisorSolPPs.Item(1).Pres5_Nivel0, g_oConfVisorSolPPs.Item(1).Pres5_Nivel1, g_oConfVisorSolPPs.Item(1).Pres5_Nivel2, g_oConfVisorSolPPs.Item(1).Pres5_Nivel3, g_oConfVisorSolPPs.Item(1).Pres5_Nivel4)
                m_bPres5Validado = True
            End If
            
            For i = g_oConfVisorSolPPs.Count To 1 Step -1
                g_oConfVisorSolPPs.Remove i
            Next
            
            bMostrarBusquedaAvanzada = True
        End If
    End If
    
    If bMostrarBusquedaAvanzada Then
        MostrarBusquedaAvanzada
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "ComprobarYAplicarFiltros", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' * Objetivo: Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Id">TipoSolicit</param>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcTipoSolicit_PositionListId(ByVal Id As String)
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcTipoSolicit.MoveFirst
    
    If Id <> 0 Then
        For i = 0 To sdbcTipoSolicit.Rows - 1
            bm = sdbcTipoSolicit.GetBookmark(i)
            If Id = sdbcTipoSolicit.Columns(1).CellText(bm) Then
                sdbcTipoSolicit.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcTipoSolicit_PositionListId", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' * Objetivo: Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Peticionario">Peticionario</param>
''' <remarks>Llamada desde: ComprobarYAplicarFiltros ; Tiempo m�ximo: 0,2</remarks>
Private Function sdbcPeticionario_PositionListId(ByVal Id As String) As String
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcPeticionario.MoveFirst
    
    If Id <> "" Then
        For i = 0 To sdbcPeticionario.Rows - 1
            bm = sdbcPeticionario.GetBookmark(i)
            If Id = sdbcPeticionario.Columns(0).CellText(bm) Then
                sdbcPeticionario.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    sdbcPeticionario_PositionListId = sdbcPeticionario.Columns(1).CellText(bm)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "sdbcPeticionario_PositionListId", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Cargar la combo con todos los Tipos Solicitud con acceso al EP
''' </summary>
''' <remarks>Llamada desde: sdbcTipoSolicit_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarTiposSolicitud()
    Dim oSolic As CSolicitud
    Dim oSolicitudes As CSolicitudes
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    
    If m_bRuo Then
        oSolicitudes.CargarTodasSolicitudesDeCompras , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRSolicPerfUON, m_lIdPerfil
    Else
        oSolicitudes.CargarTodasSolicitudesDeCompras , , , , , , , m_bRSolicPerfUON, m_lIdPerfil
    End If


    For Each oSolic In oSolicitudes
        sdbcTipoSolicit.AddItem oSolic.Codigo & Chr(m_lSeparador) & NullToStr(oSolic.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & oSolic.Id
    Next

    Set oSolicitudes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarTiposSolicitud", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cargar la combo con todos los usuarios con acceso al EP
''' </summary>
''' <remarks>Llamada desde: sdbcTipoSolicit_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarPersonas()
    Dim oPersona As CPersona
    Dim oPersonas As CPersonas
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPersonas = oFSGSRaiz.Generar_CPersonas
        
    If m_bRuo Then
        oPersonas.CargarPeticionariosSolCompra True, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRSolicPerfUON, m_lIdPerfil
    Else
        oPersonas.CargarPeticionariosSolCompra (True), , , , , m_bRSolicPerfUON, m_lIdPerfil
    End If

    For Each oPersona In oPersonas
        sdbcPeticionario.AddItem oPersona.Cod & Chr(m_lSeparador) & oPersona.Cod & " - " & NullToStr(oPersona.nombre) & " " & oPersona.Apellidos
    Next
        
    Set oPersonas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudBuscar", "CargarPersonas", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
