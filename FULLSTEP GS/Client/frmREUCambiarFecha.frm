VERSION 5.00
Begin VB.Form frmREUCambiarFecha 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cambiar fecha+"
   ClientHeight    =   2445
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5700
   Icon            =   "frmREUCambiarFecha.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2445
   ScaleWidth      =   5700
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtHoraNue 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4665
      TabIndex        =   14
      Top             =   1080
      Width           =   855
   End
   Begin VB.TextBox txtRef 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2025
      MaxLength       =   100
      TabIndex        =   7
      Top             =   1545
      Width           =   3495
   End
   Begin VB.CommandButton cmdCalendar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3495
      Picture         =   "frmREUCambiarFecha.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1080
      Width           =   315
   End
   Begin VB.TextBox txtFecNue 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2025
      TabIndex        =   0
      Top             =   1080
      Width           =   1440
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1800
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2040
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2940
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   2040
      Width           =   1005
   End
   Begin VB.Label Label7 
      BackColor       =   &H00808000&
      Caption         =   "Hora:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   3915
      TabIndex        =   13
      Top             =   1110
      Width           =   660
   End
   Begin VB.Label Label6 
      BackColor       =   &H00808000&
      Caption         =   "Hora:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   3915
      TabIndex        =   12
      Top             =   210
      Width           =   660
   End
   Begin VB.Label lblHora 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4665
      TabIndex        =   11
      Top             =   180
      Width           =   855
   End
   Begin VB.Label Label5 
      BackColor       =   &H00808000&
      Caption         =   "Referencia nueva:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   1560
      Width           =   1905
   End
   Begin VB.Label Label4 
      BackColor       =   &H00808000&
      Caption         =   "Referencia actual:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   120
      TabIndex        =   9
      Top             =   660
      Width           =   1905
   End
   Begin VB.Label lblRef 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2025
      TabIndex        =   8
      Top             =   630
      Width           =   3495
   End
   Begin VB.Label lblFecActual 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2025
      TabIndex        =   6
      Top             =   180
      Width           =   1485
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Fecha actual:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   120
      TabIndex        =   4
      Top             =   210
      Width           =   1905
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Fecha nueva:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   120
      TabIndex        =   3
      Top             =   1110
      Width           =   1905
   End
End
Attribute VB_Name = "frmREUCambiarFecha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private sIdiFecha As String

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit

    Screen.MousePointer = vbHourglass
    If Not IsDate(txtFecNue) Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then txtFecNue.SetFocus
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    If Trim(txtRef) <> Trim(lblRef) Then
        teserror = frmREU.oReuSeleccionada.CambiarReferencia(txtRef.Text)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If
        RegistrarAccion ACCPlaReuMod, "Reunion:" & frmREU.oReuSeleccionada.Fecha & " Nueva referencia:" & txtRef.Text
    End If
            
    If CDate(txtFecNue) <> CDate(lblFecActual) Or CDate(txtHoraNue.Text) <> CDate(lblHora.caption) Then
        teserror = frmREU.oReuSeleccionada.CambiarFecha(CDate(txtFecNue & " " & txtHoraNue.Text))
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If
        RegistrarAccion ACCPlaReuMod, "Reunion:" & frmREU.oReuSeleccionada.Fecha & " Nueva fecha:" & txtFecNue.Text
    End If
    
    frmREU.sdbcFecReu.Columns(0).Text = frmREU.oReuSeleccionada.Fecha
    frmREU.sdbcFecReu.Text = txtFecNue & " " & txtHoraNue.Text
    frmREU.lblRef = txtRef
    frmREU.sdbcFecReu_Validate False
    Screen.MousePointer = vbNormal
    Unload Me
    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendar_Click()
    AbrirFormCalendar Me, txtFecNue
End Sub

Private Sub cmdCancelar_Click()
    
    Unload Me
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_REUCAMBIARFECHA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sIdiFecha = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmREUCambiarFecha.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Ador.MoveNext
        Label4.caption = Ador(0).Value
        Ador.MoveNext
        Label5.caption = Ador(0).Value
        Ador.MoveNext
        Label6.caption = Ador(0).Value
        Label7.caption = Ador(0).Value
        
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
    
    lblFecActual = Format(frmREU.sdbcFecReu, "short date")
    lblHora.caption = Format(frmREU.sdbcFecReu, "hh:nn")
    txtFecNue = lblFecActual
    txtHoraNue = lblHora
    lblRef = frmREU.lblRef
    txtRef = lblRef
    
End Sub
