VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSELGrupo 
   BackColor       =   &H00808000&
   Caption         =   "Selección de grupo destino en proceso"
   ClientHeight    =   3870
   ClientLeft      =   120
   ClientTop       =   405
   ClientWidth     =   8250
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELGrupo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3870
   ScaleWidth      =   8250
   Begin VB.TextBox txtDenominacion 
      Height          =   285
      Left            =   1560
      MaxLength       =   100
      TabIndex        =   2
      Top             =   1560
      Width           =   4095
   End
   Begin VB.TextBox txtCodigo 
      Height          =   285
      Left            =   1560
      MaxLength       =   100
      TabIndex        =   3
      Top             =   1080
      Width           =   4095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   3600
      TabIndex        =   10
      Top             =   3360
      Width           =   1050
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2160
      TabIndex        =   9
      Top             =   3360
      Width           =   1050
   End
   Begin VB.OptionButton optTodasLasLineasSolicitud 
      BackColor       =   &H0080800F&
      Caption         =   "Trasladar todas las lineas de la solicitud al grupo seleccionado"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Index           =   2
      Left            =   240
      TabIndex        =   8
      Top             =   2880
      Width           =   7500
   End
   Begin VB.OptionButton optTodasLasLineasPestaña 
      BackColor       =   &H0080800F&
      Caption         =   "Trasladar todas las lineas de la pestaña ""xxx"" al grupo seleccionado"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Index           =   0
      Left            =   240
      TabIndex        =   7
      Top             =   2520
      Width           =   7500
   End
   Begin VB.OptionButton optTrasladarUnaLinea 
      BackColor       =   &H0080800F&
      Caption         =   "Trasladar la linea xxx al grupo seleccionado"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Index           =   1
      Left            =   240
      TabIndex        =   6
      Top             =   2160
      Width           =   7500
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcGruposDestino 
      Height          =   285
      Left            =   185
      TabIndex        =   5
      Top             =   520
      Width           =   5500
      DataFieldList   =   "Column 0"
      ListAutoPosition=   0   'False
      ListWidthAutoSize=   0   'False
      ListWidth       =   9701
      _Version        =   196617
      DataMode        =   2
      Cols            =   1
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "PLANTILLAS"
      Columns(0).Name =   "PLANTILLAS"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "COD_GRUPO"
      Columns(1).Name =   "COD_GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID_GRUPO"
      Columns(2).Name =   "ID_GRUPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "DEN_GRUPO"
      Columns(3).Name =   "DEN_GRUPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   9710
      Columns(4).Caption=   "COD-DEN"
      Columns(4).Name =   "COD-DEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   9701
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblDenominacion 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominación:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   1560
      Width           =   1305
   End
   Begin VB.Label lblCodigo 
      BackStyle       =   0  'Transparent
      Caption         =   "Código:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   1080
      Width           =   1305
   End
   Begin VB.Label lblDesplegable 
      BackStyle       =   0  'Transparent
      Caption         =   "Grupo destino en proceso:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Index           =   0
      Left            =   185
      TabIndex        =   4
      Top             =   180
      Width           =   6500
   End
End
Attribute VB_Name = "frmSELGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public PermisoModificar As Boolean
Public VisibleOpt3 As Boolean
Public Linea As Integer
Public GrupoSeleccionado As String
Public txtLabelCod As String
Public txtLabelDen As String
Public ComboValue As String
Public txtGrupoNuevo As String
Public FormOrigen As String

Private txtMsgBox1 As String
Private txtMsgBox2 As String


Private Sub cmdAceptar_Click()
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim i As Long
    Dim k As Long
    Dim sGrupo As String
    Dim Linea2 As Integer
    Dim bExisteGrupoAnt As Boolean
    Dim n As Long
    
    bExisteGrupoAnt = False

    'Comprobación datos metidos para Grupo Nuevo
    If txtCodigo.Locked = False And PermisoModificar Then
        'Comprobación de que se haya introducido denominación
        If txtDenominacion.Text = "" Then
            MsgBox txtMsgBox2, vbInformation + vbOKOnly, "FULLSTEP"
            Exit Sub
        End If
    
        'No es posible introducir un código ya existente
        If FormOrigen = "frmSOLAbrirProc" Then
            For k = 1 To frmSOLAbrirProc.oProc.Grupos.Count
                If txtCodigo.Text = frmSOLAbrirProc.oProc.Grupos.Item(k).Codigo Then
                    If txtDenominacion.Text <> frmSOLAbrirProc.oProc.Grupos.Item(k).Den Then
                        MsgBox txtMsgBox1, vbInformation + vbOKOnly, "FULLSTEP"
                        Exit Sub
                    End If
                End If
            Next k
        End If
        

        If FormOrigen = "frmSOLEnviarAProc" Then
            'Comprobamos que no coincida con otro grupo nuevo previamente introducido
            'y que el Grupo Nuevo no se haya dejado con el código y nombre del grupo de la solicitud que se le da por defecto
            For k = 1 To frmSOLEnviarAProc.ProcNuevo.Grupos.Count
                If (txtCodigo.Text = frmSOLEnviarAProc.ProcNuevo.Grupos.Item(k).Codigo) And (txtCodigo.Text <> txtLabelCod) Then
                    'If txtDenominacion.Text <> frmSOLEnviarAProc.ProcNuevo.Grupos.Item(k).Den Then
                        MsgBox txtMsgBox1, vbInformation + vbOKOnly, "FULLSTEP"
                        Exit Sub
                    'End If
                End If
             Next

            'Comprobamos que no coincida con otro grupo ya cargado en el desplegable
            'y que el Grupo Nuevo no se haya dejado con el código y nombre del grupo de la solicitud que se le da por defecto
            For n = 1 To frmSOLEnviarAProc.ProcSeleccionado.Grupos.Count
                If (txtCodigo.Text = frmSOLEnviarAProc.ProcSeleccionado.Grupos.Item(n).Codigo) And (txtCodigo.Text <> txtLabelCod) Then
                    MsgBox txtMsgBox1, vbInformation + vbOKOnly, "FULLSTEP"
                    Exit Sub
                End If
            Next
        
        End If
    End If

    '****Origen: frmSOLAbrirProc ****
    If FormOrigen = "frmSOLAbrirProc" Then
        frmSOLAbrirProc.sdbgLineas.Columns("GR_DESTINO_PROC").Value = sdbcGruposDestino.Value

        'Para poder mantener valores introducidos si se va cambiando de pestañas, filas..
        sGrupo = Mid(frmSOLAbrirProc.ssTabGrupos.Tabs(frmSOLAbrirProc.ssTabGrupos.selectedItem.Index).key, 2)
    End If
                
    '****Origen: frmSOLEnviarAProc ****
    If FormOrigen = "frmSOLEnviarAProc" Then
        frmSOLEnviarAProc.sdbgLineas.Columns("GR_DESTINO_PROC").Value = sdbcGruposDestino.Value
    
        sGrupo = Mid(frmSOLEnviarAProc.ssTabGrupos.Tabs(frmSOLEnviarAProc.ssTabGrupos.selectedItem.Index).key, 2)
        
        
        If txtCodigo.Text <> "" Then
            'Si se ha rellenado txtCodigo, es porque se ha elegido Grupo Nuevo.
            'Que en el grid de frmSOLEnviarAProc, aparezca 'Cod-Den' y no 'Grupo Nuevo'
            frmSOLEnviarAProc.sdbgLineas.Columns("GR_DESTINO_PROC").Value = txtCodigo.Text & " - " & txtDenominacion.Text
            
            'Añadimos el grupo nuevo con el código y denominación nuevos introducidos
            'Comprobamos que no se haya dejado el Grupo Nuevo con el cod-den que se da por defecto, en cuyo caso, no habría que añadirle el grupo
            If txtCodigo.Text <> txtLabelCod Then
                frmSOLEnviarAProc.ProcNuevo.Grupos.Add frmSOLEnviarAProc.ProcNuevo, txtCodigo.Text, txtDenominacion.Text
            End If
        End If
        
    End If


    '****Opción 1****
    If optTrasladarUnaLinea(1) Then
    
        '****Origen: frmSOLEnviarAProc ****
        If FormOrigen = "frmSOLEnviarAProc" Then
            If txtCodigo.Text = "" Then
                If Linea = 0 Then
                    Linea2 = Linea + 1
                    frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(Linea2).grupoCod = sdbcGruposDestino.Columns("COD_GRUPO").Value
                Else
                    frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(Linea).grupoCod = sdbcGruposDestino.Columns("COD_GRUPO").Value
                End If
            Else
                If Linea = 0 Then
                    Linea2 = Linea + 1
                    Linea2 = CInt(Linea2)
                    frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(Linea2).grupoCod = txtCodigo.Text
                    frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(Linea2).GrupoDen = txtDenominacion.Text
                Else
                    frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(Linea).grupoCod = txtCodigo.Text
                    frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(Linea).GrupoDen = txtDenominacion.Text
                End If
            End If
            
            If sdbcGruposDestino.Columns("ID_GRUPO").Value <> "" Then
                If Linea = 0 Then
                    Linea2 = Linea + 1
                    frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(Linea2).GrupoID = sdbcGruposDestino.Columns("ID_GRUPO").Value
                Else
                    frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items.Item(Linea).GrupoID = sdbcGruposDestino.Columns("ID_GRUPO").Value
                End If
            End If
        
        '****Origen: frmSOLAbrirProc ****
        Else
            If FormOrigen = "frmSOLAbrirProc" Then
                If txtCodigo.Text = "" Then
                    If Linea = 0 Then
                        Linea2 = Linea + 1
                        frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items.Item(Linea2).grupoCod = sdbcGruposDestino.Columns("COD_GRUPO").Value
                    Else
                        frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items.Item(Linea).grupoCod = sdbcGruposDestino.Columns("COD_GRUPO").Value
                    End If
                Else
                    If Linea = 0 Then
                        Linea2 = Linea + 1
                        frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items.Item(Linea2).grupoCod = txtCodigo.Text
                        frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items.Item(Linea2).GrupoDen = txtDenominacion.Text
                    Else
                        frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items.Item(Linea).grupoCod = txtCodigo.Text
                        frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items.Item(Linea).GrupoDen = txtDenominacion.Text
                    End If
                End If
                
                If sdbcGruposDestino.Columns("ID_GRUPO").Value <> "" Then
                    If Linea = 0 Then
                        Linea2 = Linea + 1
                        frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items.Item(Linea2).GrupoID = sdbcGruposDestino.Columns("ID_GRUPO").Value
                    Else
                        frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items.Item(Linea).GrupoID = sdbcGruposDestino.Columns("ID_GRUPO").Value
                    End If
                End If
            End If
        End If
    Else
    
    '****Opción 2****
        If optTodasLasLineasPestaña(0) Then
        
            '****Origen: frmSOLEnviarAProc ****
            If FormOrigen = "frmSOLEnviarAProc" Then
                    For Each oItem In frmSOLEnviarAProc.ProcNuevo.Grupos.Item(CStr(sGrupo)).Items
                        If txtCodigo.Text = "" Then
                            oItem.grupoCod = sdbcGruposDestino.Columns("COD_GRUPO").Value
                        Else
                            oItem.grupoCod = txtCodigo.Text
                            oItem.GrupoDen = txtDenominacion.Text
                        End If
                        
                        If sdbcGruposDestino.Columns("ID_GRUPO").Value <> "" Then
                            oItem.GrupoID = sdbcGruposDestino.Columns("ID_GRUPO").Value
                        End If
                    Next
                                    
                    'Se rellena la columna ("GR_DESTINO_PROC") entera
                    frmSOLEnviarAProc.sdbgLineas.MoveFirst

                    For i = 0 To frmSOLEnviarAProc.sdbgLineas.Rows - 1
                        If txtCodigo.Text = "" Then
                            frmSOLEnviarAProc.sdbgLineas.Columns("GR_DESTINO_PROC").Value = sdbcGruposDestino.Value
                            frmSOLEnviarAProc.sdbgLineas.MoveNext
                        Else
                            frmSOLEnviarAProc.sdbgLineas.Columns("GR_DESTINO_PROC").Value = txtCodigo.Text & " - " & txtDenominacion.Text
                            frmSOLEnviarAProc.sdbgLineas.MoveNext
                        End If
                        
                    Next i

                    
            '****Origen: frmSOLAbrirProc ****
            Else
                If FormOrigen = "frmSOLAbrirProc" Then
                    For Each oItem In frmSOLAbrirProc.oProc.Grupos.Item(CStr(sGrupo)).Items
                        If txtCodigo.Text = "" Then
                            oItem.grupoCod = sdbcGruposDestino.Columns("COD_GRUPO").Value
                        Else
                            oItem.grupoCod = txtCodigo.Text
                            oItem.GrupoDen = txtDenominacion.Text
                        End If
                        
                        If sdbcGruposDestino.Columns("ID_GRUPO").Value <> "" Then
                            oItem.GrupoID = sdbcGruposDestino.Columns("ID_GRUPO").Value
                        End If
                    Next
                    
                    'Se rellena la columna ("GR_DESTINO_PROC") entera
                    'frmSOLAbrirProc.sdbgLineas.MoveFirst
                    
                    For i = 0 To frmSOLAbrirProc.sdbgLineas.Rows - 1
                        frmSOLAbrirProc.sdbgLineas.Columns("GR_DESTINO_PROC").Value = sdbcGruposDestino.Value
                        'frmSOLAbrirProc.sdbgLineas.MoveNext
                    Next i
                End If
            End If
        
        Else
        
            '****Opción 3****
            If optTodasLasLineasSolicitud(2) Then
            
                '****Origen: frmSOLEnviarAProc ****
                If FormOrigen = "frmSOLEnviarAProc" Then
                    For Each oGrupo In frmSOLEnviarAProc.ProcNuevo.Grupos
                        If Not oGrupo.Items Is Nothing Then
                            For Each oItem In oGrupo.Items
                                If txtCodigo.Text = "" Then
                                    oItem.grupoCod = sdbcGruposDestino.Columns("COD_GRUPO").Value
                                Else
                                    oItem.grupoCod = txtCodigo.Text
                                    oItem.GrupoDen = txtDenominacion.Text
                                End If
                            
                                If sdbcGruposDestino.Columns("ID_GRUPO").Value <> "" Then
                                    oItem.GrupoID = sdbcGruposDestino.Columns("ID_GRUPO").Value
                                End If
                            Next
                        End If
                    Next
                    
                    'Se rellena la columna ("GR_DESTINO_PROC") entera
                    frmSOLEnviarAProc.sdbgLineas.MoveFirst

                    For i = 0 To frmSOLEnviarAProc.sdbgLineas.Rows - 1
                        frmSOLEnviarAProc.sdbgLineas.Columns("GR_DESTINO_PROC").Value = sdbcGruposDestino.Value
                        frmSOLEnviarAProc.sdbgLineas.MoveNext
                    Next i

                
                '****Origen: frmSOLAbrirProc ****
                Else
                    If FormOrigen = "frmSOLAbrirProc" Then
                        For Each oGrupo In frmSOLAbrirProc.oProc.Grupos
                            If Not oGrupo.Items Is Nothing Then
                                For Each oItem In oGrupo.Items
                                    If txtCodigo.Text = "" Then
                                        oItem.grupoCod = sdbcGruposDestino.Columns("COD_GRUPO").Value
                                    Else
                                        oItem.grupoCod = txtCodigo.Text
                                        oItem.GrupoDen = txtDenominacion.Text
                                    End If
                                    
                                    If sdbcGruposDestino.Columns("ID_GRUPO").Value <> "" Then
                                        oItem.GrupoID = sdbcGruposDestino.Columns("ID_GRUPO").Value
                                    End If
                                Next
                            End If
                        Next
                        
                        'Se rellena la columna ("GR_DESTINO_PROC") entera
                        frmSOLAbrirProc.sdbgLineas.MoveFirst
                        
                        For i = 0 To frmSOLAbrirProc.sdbgLineas.Rows - 1
                            frmSOLAbrirProc.sdbgLineas.Columns("GR_DESTINO_PROC").Value = sdbcGruposDestino.Value
                            frmSOLAbrirProc.sdbgLineas.MoveNext
                        Next i
                        
                    End If
                End If
            End If
        End If
    End If
        
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
    
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    PonerFieldSeparator Me
    ConfigurarFormulario

    'Primera opción marcada por defecto
    optTrasladarUnaLinea(1).Value = True
    
    txtCodigo.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE
    
    'Permiso Apertura de Procesos: "Permitir modificar la estructura de un proceso creado desde plantilla"
    'Entonces, opción de crear un nuevo grupo
    If Not PermisoModificar Then
        lblCodigo.Visible = False
        txtCodigo.Visible = False
        lblDenominacion(0).Visible = False
        txtDenominacion.Visible = False
        
        optTrasladarUnaLinea(1).Top = 1080
        optTodasLasLineasPestaña(0).Top = 1440
        optTodasLasLineasSolicitud(2).Top = 1800
        
        cmdAceptar.Top = 2160
        cmdCancelar.Top = 2160
        
        frmSELGrupo.Height = frmSELGrupo.Height - (2 * txtCodigo.Height) - (2 * txtDenominacion.Height)
    Else
        txtCodigo.Locked = True
        txtDenominacion.Locked = True
    End If
    

    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Configura el número de opciones a mostrar en función de los grupos que tenga la solicitud
''' </summary>
''' <remarks>Llamada: Form_load</remarks>
Private Sub ConfigurarFormulario()

    'Si la solicitud tiene mas de un grupo, se muestra una tercera opción, sino no
    If FormOrigen = "frmSOLAbrirProc" Then
        If frmSOLAbrirProc.ssTabGrupos.Tabs.Count > 1 Then
            optTodasLasLineasSolicitud(2).Visible = True
        Else
            optTodasLasLineasSolicitud(2).Visible = False
        End If
    Else
        If FormOrigen = "frmSOLEnviarAProc" Then
            If frmSOLEnviarAProc.ssTabGrupos.Tabs.Count > 1 Then
                optTodasLasLineasSolicitud(2).Visible = True
            Else
                optTodasLasLineasSolicitud(2).Visible = False
            End If
        End If
    End If
    

End Sub

''' <summary>
''' Carga los idiomas
''' </summary>
''' <remarks>Llamada: Form_load</remarks>
Private Sub CargarRecursos()
   Dim Ador As Ador.Recordset

    On Error Resume Next

    'Modulo 504, TEXTOS
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELGRUPO, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then

        cmdAceptar.caption = Ador(0).Value                       'Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value                      'Cancelar
        Ador.MoveNext
        Me.caption = Ador(0).Value                               'Selección de grupo destino en proceso
        Ador.MoveNext
        lblDesplegable(0).caption = Ador(0).Value                'Grupo destino en proceso:
        Ador.MoveNext
        optTrasladarUnaLinea(1).caption = Ador(0).Value          'Trasladar la linea xxx al grupo seleccionado
        Ador.MoveNext
        optTodasLasLineasPestaña(0).caption = Ador(0).Value      'Trasladar todas las lineas de la pestaña "xxx" al grupo seleccionado
        Ador.MoveNext
        optTodasLasLineasSolicitud(2).caption = Ador(0).Value    'Trasladar todas las lineas de la solicitud al grupo seleccionado
        Ador.MoveNext
        lblCodigo.caption = Ador(0).Value                        'Código:
        Ador.MoveNext
        lblDenominacion(0).caption = Ador(0).Value               'Denominación:
        Ador.MoveNext
        txtGrupoNuevo = Ador(0).Value                            'Grupo Nuevo
        Ador.MoveNext
        txtMsgBox1 = Ador(0).Value                               'El código introducido ya existe. Por favor, introduzca uno nuevo
        Ador.MoveNext
        txtMsgBox2 = Ador(0).Value                               'Por favor, introduzca la denominación
        Ador.Close
        
    End If
End Sub

Private Sub sdbcGruposDestino_CloseUp()

    Dim i As Long
    Dim vble As String

    sdbcGruposDestino.Value = sdbcGruposDestino.Columns(4).Text
    
    For i = 0 To sdbcGruposDestino.Rows - 1
            vble = sdbcGruposDestino.Columns(4).CellText(i)
    Next i

    'Si se tiene el permiso "Permitir modificar la estructura de un proceso creado desde plantilla", se puede crear un nuevo grupo
    'Grupo Nuevo: txtCodigo y txtDenominacion visibles
    If PermisoModificar Then
        If UCase(sdbcGruposDestino.Text) = UCase(vble) Then
            txtCodigo.Text = txtLabelCod
            txtDenominacion.Text = txtLabelDen
            txtCodigo.Locked = False
            txtDenominacion.Locked = False
        Else
            txtCodigo.Text = ""
            txtDenominacion.Text = ""
            txtCodigo.Locked = True
            txtDenominacion.Locked = True
        End If
    End If

End Sub

Private Sub sdbcGruposDestino_InitColumnProps()

    sdbcGruposDestino.DataFieldList = "Column 0"
    sdbcGruposDestino.DataFieldToDisplay = "Column 0"
        
End Sub

Private Sub sdbcGruposDestino_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGruposDestino.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGruposDestino.Rows - 1
            bm = sdbcGruposDestino.GetBookmark(i)
            If UCase(sdbcGruposDestino.Text) = UCase(sdbcGruposDestino.Columns(4).CellText(bm)) Then
                sdbcGruposDestino.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    PermisoModificar = 0
    VisibleOpt3 = 0
    Linea = 0
    GrupoSeleccionado = ""
    ComboValue = ""
    sdbcGruposDestino.RemoveAll
End Sub

