VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.UserControl ctlAdjunPedidos 
   BackColor       =   &H00808000&
   ClientHeight    =   3390
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7680
   ScaleHeight     =   3390
   ScaleWidth      =   7680
   Begin VB.PictureBox picEspGen 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1335
      Left            =   0
      ScaleHeight     =   1335
      ScaleWidth      =   7575
      TabIndex        =   7
      Top             =   120
      Width           =   7575
      Begin VB.TextBox txtProceEsp 
         Height          =   1275
         Left            =   60
         MaxLength       =   2000
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   0
         Width           =   7440
      End
   End
   Begin VB.PictureBox picBotones 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   340
      Left            =   5200
      ScaleHeight     =   345
      ScaleWidth      =   2370
      TabIndex        =   0
      Top             =   3080
      Width           =   2375
      Begin VB.CommandButton cmdAbrirEsp 
         Height          =   300
         Left            =   1920
         Picture         =   "ctlAdjunPedidos.ctx":0000
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdSalvarEsp 
         Height          =   300
         Left            =   1440
         Picture         =   "ctlAdjunPedidos.ctx":007C
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdEliminarEsp 
         Height          =   300
         Left            =   480
         Picture         =   "ctlAdjunPedidos.ctx":00FD
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdA�adirEsp 
         Height          =   300
         Left            =   0
         Picture         =   "ctlAdjunPedidos.ctx":0180
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdModificarEsp 
         Height          =   300
         Left            =   960
         Picture         =   "ctlAdjunPedidos.ctx":01F2
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   420
      End
   End
   Begin MSComctlLib.ListView lstvwEsp 
      Height          =   1335
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   2355
      View            =   3
      Arrange         =   1
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fichero"
         Object.Width           =   3233
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Tamanyo"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Comentario"
         Object.Width           =   6920
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   120
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   600
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ctlAdjunPedidos.ctx":033C
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Archivos adjuntos"
      ForeColor       =   &H80000005&
      Height          =   240
      Left            =   120
      TabIndex        =   9
      Top             =   1440
      Width           =   1410
   End
End
Attribute VB_Name = "ctlAdjunPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'VARIABLES P�BLICAS:
Public g_oLinea As CLineaPedido
Public g_oOrden As COrdenEntrega
Public g_oIBaseDatos As IBaseDatos
Public g_bCancelarEsp As Boolean
Public g_bRespetarCombo As Boolean
Public g_sComentario As String
Public g_sOrigen As String
Public g_lTamanyoMax As Long
Public g_bModificar As Boolean
Public g_sProceEsp As String
Public g_sSelectedItem As String
Public g_sNombre As String
'VARIABLES PRIVADAS:
Private m_sayFileNames() As String
Private Accion As AccionesSummit

'Variables para los idiomas
Private m_sIdioma() As String
Private m_sIdiElArchivo As String
Private m_sIdiGuardar As String
Private m_sIdiTipoOrig As String

Private m_skb As String
Private m_bCargarRecursos As Boolean

Public Sub AnyadirEspsALista()
Dim oAdjun As CAdjunto
If Not m_bCargarRecursos Then CargarRecursos

lstvwEsp.ListItems.clear
If Not g_oLinea Is Nothing Then
    'ADJUNTOS DE LA L�NEA
    If Not g_oLinea.Adjuntos Is Nothing Then
        For Each oAdjun In g_oLinea.Adjuntos
            lstvwEsp.ListItems.Add , "ESP" & CStr(oAdjun.Id), oAdjun.nombre, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ToolTipText = NullToStr(oAdjun.Comentario)
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ListSubItems.Add , "Com", NullToStr(oAdjun.Comentario)
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).Tag = oAdjun.Id
        Next
    End If
Else
    'ADJUNTOS DE LA ORDEN
    If Not g_oOrden.Adjuntos Is Nothing Then
        For Each oAdjun In g_oOrden.Adjuntos
            lstvwEsp.ListItems.Add , "ESP" & CStr(oAdjun.Id), oAdjun.nombre, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ToolTipText = NullToStr(oAdjun.Comentario)
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ListSubItems.Add , "Com", NullToStr(oAdjun.Comentario)
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).Tag = oAdjun.Id
        Next
    End If
End If
End Sub

Public Sub Arrange()
If UserControl.Height > 2000 And UserControl.Width > 2000 Then
    If Not g_oOrden Is Nothing Then
        lstvwEsp.Top = 0
        lstvwEsp.Height = UserControl.Height - 810
    Else
        lstvwEsp.Height = UserControl.Height * 0.33
        picEspGen.Height = UserControl.Height - lstvwEsp.Height - 1240
        txtProceEsp.Height = picEspGen.Height - 30
        Label1.Top = picEspGen.Top + picEspGen.Height
        lstvwEsp.Top = Label1.Top + 240
    End If
    
    picEspGen.Width = UserControl.Width - 200
    lstvwEsp.Width = UserControl.Width - 345
    txtProceEsp.Width = lstvwEsp.Width
    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.287
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.15
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.55
    
    picBotones.Left = lstvwEsp.Width - 2255
    picBotones.Top = UserControl.Height - 745
End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRImeR LUGAR SE CARGAN ELEmeNTOS DEL FORMULARIO
On Error Resume Next
If m_bCargarRecursos Then Exit Sub

Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEGUIMIENTO_ADJUN, basPublic.gParametrosInstalacion.gIdioma)
If Not Ador Is Nothing Then
    ReDim m_sIdioma(1 To 7)
    For i = 1 To 7
        m_sIdioma(i) = Ador(0).Value
        Ador.MoveNext
    Next
    Label1.caption = Ador(0).Value
    Ador.MoveNext
    lstvwEsp.ColumnHeaders(1).Text = Ador(0).Value
    m_sIdiElArchivo = Ador(0).Value
    Ador.MoveNext
    lstvwEsp.ColumnHeaders(3).Text = Ador(0).Value
    Ador.MoveNext
    m_sIdiGuardar = Ador(0).Value
    Ador.MoveNext
    m_sIdiTipoOrig = Ador(0).Value
    Ador.MoveNext
    lstvwEsp.ColumnHeaders(2).Text = Ador(0).Value
    Ador.MoveNext
    m_skb = Ador(0).Value
    Ador.Close
End If
Set Ador = Nothing
cmdA�adirEsp.ToolTipText = m_sIdioma(3)
cmdEliminarEsp.ToolTipText = m_sIdioma(4)
cmdModificarEsp.ToolTipText = m_sIdioma(5)
cmdSalvarEsp.ToolTipText = m_sIdioma(6)
cmdAbrirEsp.ToolTipText = m_sIdioma(7)
m_bCargarRecursos = True
End Sub

Private Sub LeerFichero(ByRef sFileName As String)
Dim oAdjun As CAdjunto
Set oAdjun = oFSGSRaiz.generar_cadjunto
If Not g_oLinea Is Nothing Then
    Set oAdjun = g_oLinea.Adjuntos.Item(CStr(lstvwEsp.selectedItem.Tag))
    Set oAdjun.Linea = g_oLinea
    If oAdjun.EnLOG Then
        oAdjun.Tipo = TipoAdjunto.Log_Linea_Pedido
    Else
        oAdjun.Tipo = TipoAdjunto.Linea_Pedido
    End If
Else
    Set oAdjun = g_oOrden.Adjuntos.Item(CStr(lstvwEsp.selectedItem.Tag))
    Set oAdjun.Orden = g_oOrden
    If oAdjun.EnLOG Then
        oAdjun.Tipo = TipoAdjunto.Log_Orden_Entrega
    Else
        oAdjun.Tipo = TipoAdjunto.Orden_Entrega
    End If
End If
If oAdjun.DataSize = 0 Then
    oMensajes.NoValido m_sIdiElArchivo & " " & oAdjun.nombre
    Set oAdjun = Nothing
    Exit Sub
End If
If g_sOrigen = "frmPEDIDOS" Then
    sFileName = oAdjun.Ruta & sFileName
Else
    oAdjun.LeerAdjunto sFileName
End If
End Sub

Private Sub cmdAbrirEsp_Click()
Dim Item As MSComctlLib.listItem
Dim sFileName As String
Dim oAdjun As CAdjunto
Set Item = lstvwEsp.selectedItem
If Item Is Nothing Then
    oMensajes.SeleccioneFichero
    Exit Sub
End If
If g_sOrigen = "frmPEDIDOS" Then
    sFileName = Item.Text
Else
    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & Item.Text
End If
LeerFichero sFileName
m_sayFileNames(UBound(m_sayFileNames)) = sFileName
ReDim Preserve m_sayFileNames(UBound(m_sayFileNames) + 1)

'Lanzamos la aplicaci�n
ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
End Sub
Private Sub cmdA�adirEsp_Click()
Dim DataFile As Integer
Dim oAdjun As CAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim arrFileNames As Variant
Dim iFile As Integer
Dim sPathTemp As String
Dim sPath As String
Dim sAdjunto As String
Dim ArrayAdjunto() As String
Dim idLogOrden As Long
Dim sERP As String
Dim teserror As TipoErrorSummit
Dim lIdLogLineasPedido As Long
Dim oFos As Scripting.FileSystemObject
On Error GoTo Cancelar:

    cmmdEsp.DialogTitle = m_sIdioma(1)
    cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
    cmmdEsp.filename = ""
    cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    cmmdEsp.ShowOpen

    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle

    If sFileName = "" Then
        Exit Sub
    End If

    arrFileNames = ExtraerFicheros(sFileName)
       
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.chkProcFich.Visible = False
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    If g_sOrigen = "frmPEDIDOS" Then
        If Not g_oLinea Is Nothing Then
            frmPROCEComFich.sOrigen = "frmPEDIDOSLin"
        Else
            frmPROCEComFich.sOrigen = "frmPEDIDOS"
        End If
    Else
        frmPROCEComFich.sOrigen = "frmSeguimientoEsp"
    End If
    g_bCancelarEsp = False
    frmPROCEComFich.Show 1

    If Not g_bCancelarEsp Then
        sPathTemp = FSGSLibrary.DevolverPathFichTemp
        For iFile = 1 To UBound(arrFileNames)
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            Set oAdjun = oFSGSRaiz.generar_cadjunto
            If g_oLinea Is Nothing Then
                Set oAdjun.Orden = g_oOrden
                oAdjun.Tipo = TipoAdjunto.Orden_Entrega
            Else
                Set oAdjun.Linea = g_oLinea
                oAdjun.Tipo = TipoAdjunto.Linea_Pedido
            End If
            oAdjun.nombre = sFileTitle
            oAdjun.Comentario = g_sComentario
            If g_sOrigen = "frmPEDIDOS" Then
                'Generamos una carpeta temporal para copiar ahi el fichero y guardamos la ruta del fichero
                Set oFos = New Scripting.FileSystemObject
                sPath = Replace(sPathTemp & oFos.GetTempName(), ".tmp", "") & "\"
                Do While oFos.FolderExists(sPath)
                    sPath = Replace(sPathTemp & oFos.GetTempName(), ".tmp", "") & "\"
                Loop
                oFos.CreateFolder sPath
                oFos.CopyFile sFileName, sPath & "\"
                oAdjun.Ruta = sPath
                Open sPath & sFileTitle For Binary Access Read As 1
                oAdjun.DataSize = LOF(1)
                Close 1
            Else
                If Not g_oLinea Is Nothing Then
                    sAdjunto = oAdjun.GrabarAdjunto(arrFileNames(0) & "\", sPathTemp, CStr(g_oLinea.Id))
                Else
                    sAdjunto = oAdjun.GrabarAdjunto(arrFileNames(0) & "\", sPathTemp, CStr(g_oOrden.Id))
                End If
                 'Creamos un array, cada "substring" se asignar� a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oAdjun.Id = ArrayAdjunto(0)
                
                'Grabamos el LOG______________
                If g_oLinea Is Nothing Then
                    g_oOrden.TipoLogPedido = Adjuntos_Cabecera
                    g_oOrden.GrabarPedidoEnLog "U", , , oAdjun.Id
                Else
                    If g_oLinea.GrabarEnLog Then
                        g_oLinea.ObjetoOrden.TipoLogPedido = Adjuntos_Linea
                        
                        teserror = g_oLinea.ObjetoOrden.GrabarLogOrdenEntrega(idLogOrden, sERP, "R")
                        If teserror.NumError <> TESnoerror Then
                            Set oAdjun = Nothing
                            Exit Sub
                        End If
                    
                        teserror = g_oLinea.GrabarLogLineaPedido(idLogOrden, lIdLogLineasPedido, "U", sERP)
                        If teserror.NumError <> TESnoerror Then
                            Set oAdjun = Nothing
                            Exit Sub
                        End If
                        g_oLinea.IdCod_Adjun = oAdjun.Id
                        teserror = g_oLinea.GrabarLogResto(idLogOrden, lIdLogLineasPedido)
                        If teserror.NumError <> TESnoerror Then
                            Set oAdjun = Nothing
                            Exit Sub
                        End If
                    End If
                End If
            End If
            If Not g_oLinea Is Nothing Then
                If g_sOrigen <> "frmPEDIDOS" Then basSeguridad.RegistrarAccion AccionesSummit.ACCSeguimientoAnyaAdjun, "Orden entrega:" & g_oLinea.ObjetoOrden.Anyo & "/" & g_oLinea.ObjetoOrden.NumPedido & "/" & g_oLinea.ObjetoOrden.Numero & " Linea:" & g_oLinea.Anyo & g_oLinea.Id & "Archivo:" & sFileTitle
                
                If g_oLinea.Adjuntos Is Nothing Then
                    Set g_oLinea.Adjuntos = oFSGSRaiz.Generar_CAdjuntos
                End If
                'Como todavia no hay ID porque no se va a grabar en la BBDD hay que generar un id para la KEY del objeto
                If g_sOrigen = "frmPEDIDOS" Then oAdjun.Id = g_oLinea.Adjuntos.Count + 1
                g_oLinea.Adjuntos.Add oAdjun.Id, oAdjun.nombre, oAdjun.DataSize, oAdjun.Comentario, , , oAdjun.Ruta, oAdjun.Tipo
                g_oLinea.NumAdjuntos = g_oLinea.NumAdjuntos + 1
            Else
                If g_sOrigen <> "frmPEDIDOS" Then basSeguridad.RegistrarAccion AccionesSummit.ACCSeguimientoAnyaAdjun, "Orden entrega:" & g_oOrden.Anyo & "/" & g_oOrden.NumPedido & "/" & g_oOrden.Numero & " ID:" & g_oOrden.Id & "Archivo:" & sFileTitle
                If g_oOrden.Adjuntos Is Nothing Then
                    Set g_oOrden.Adjuntos = oFSGSRaiz.Generar_CAdjuntos
                End If
                If g_sOrigen = "frmPEDIDOS" Then oAdjun.Id = g_oOrden.Adjuntos.Count + 1
                g_oOrden.Adjuntos.Add oAdjun.Id, oAdjun.nombre, oAdjun.DataSize, oAdjun.Comentario, , , oAdjun.Ruta, oAdjun.Tipo
                g_oOrden.NumAdjuntos = g_oOrden.NumAdjuntos + 1
            End If
            
            lstvwEsp.ListItems.Add , "ESP" & CStr(oAdjun.Id), sFileTitle, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ToolTipText = oAdjun.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).ListSubItems.Add , "Com", oAdjun.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oAdjun.Id)).Tag = oAdjun.Id
        Next iFile
        lstvwEsp.Refresh
    End If

    g_lTamanyoMax = g_lTamanyoMax + oAdjun.DataSize
    
    Set oAdjun = Nothing
    Exit Sub

Cancelar:
    On Error Resume Next
    If err.Number <> 32755 Then
        Close DataFile
        Set oAdjun = Nothing
    End If
End Sub

Private Sub cmdEliminarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

On Error GoTo Cancelar:
    Set Item = lstvwEsp.selectedItem
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        irespuesta = oMensajes.PreguntaEliminar(m_sIdiElArchivo & ": " & lstvwEsp.selectedItem.Text)
        If irespuesta = vbNo Then Exit Sub
        Screen.MousePointer = vbHourglass
        If Not g_oLinea Is Nothing Then
            Set oAdjun = g_oLinea.Adjuntos.Item(CStr(lstvwEsp.selectedItem.Tag))
            Set oAdjun.Linea = g_oLinea
        Else
            Set oAdjun = g_oOrden.Adjuntos.Item(CStr(lstvwEsp.selectedItem.Tag))
            Set oAdjun.Orden = g_oOrden
        End If
        If g_sOrigen <> "frmPEDIDOS" Then
            Set g_oIBaseDatos = oAdjun
            teserror = g_oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                GoTo Cancelar
            End If
        End If
        If Not g_oLinea Is Nothing Then
            basSeguridad.RegistrarAccion AccionesSummit.ACCSeguimientoAdjunEli, "Orden entrega:" & g_oLinea.ObjetoOrden.Anyo & "/" & g_oLinea.ObjetoOrden.NumPedido & "/" & g_oLinea.ObjetoOrden.Numero & " Linea:" & g_oLinea.Id & "Adjun:" & oAdjun.Id & "Archivo:" & lstvwEsp.selectedItem.Text
            g_oLinea.Adjuntos.Remove (CStr(lstvwEsp.selectedItem.Tag))
            g_oLinea.NumAdjuntos = g_oLinea.NumAdjuntos - 1
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCSeguimientoAdjunEli, "Orden entrega:" & g_oOrden.Anyo & "/" & g_oOrden.NumPedido & "/" & g_oOrden.Numero & " ID:" & g_oOrden.Id & "Adjun:" & oAdjun.Id & "Archivo:" & lstvwEsp.selectedItem.Text
            g_oOrden.Adjuntos.Remove (CStr(lstvwEsp.selectedItem.Tag))
            g_oOrden.NumAdjuntos = g_oOrden.NumAdjuntos - 1
        End If
        lstvwEsp.ListItems.Remove (CStr(lstvwEsp.selectedItem.key))
        
        Set oAdjun = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    Screen.MousePointer = vbNormal
Cancelar:
    Set oAdjun = Nothing
    Set g_oIBaseDatos = Nothing
End Sub

Private Sub cmdModificarEsp_Click()
Dim teserror As TipoErrorSummit
Dim oAdjun As CAdjunto
Dim s As String
Dim sNombre As String
Dim oFos As Scripting.FileSystemObject
    If lstvwEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        If Not g_oLinea Is Nothing Then
            Set oAdjun = g_oLinea.Adjuntos.Item(CStr(lstvwEsp.selectedItem.Tag))
            Set oAdjun.Linea = g_oLinea
        Else
            Set oAdjun = g_oOrden.Adjuntos.Item(CStr(lstvwEsp.selectedItem.Tag))
            Set oAdjun.Orden = g_oOrden
        End If
        If g_sOrigen <> "frmPEDIDOS" Then
            Set g_oIBaseDatos = oAdjun
            teserror = g_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set g_oIBaseDatos = Nothing
                Exit Sub
            End If
        End If
        s = g_sComentario
        sNombre = lstvwEsp.selectedItem.Text
        g_sNombre = lstvwEsp.selectedItem.Text
        g_sComentario = lstvwEsp.selectedItem.ListSubItems.Item(2).Text
        If g_sOrigen <> "frmPEDIDOS" Then
            frmPROCEEspMod.g_sOrigen = "frmSeguimientoEsp"
        Else
            If Not g_oLinea Is Nothing Then
                frmPROCEEspMod.g_sOrigen = "frmPEDIDOSLin"
            Else
                frmPROCEEspMod.g_sOrigen = "frmPEDIDOS"
            End If
        End If
        g_sSelectedItem = lstvwEsp.selectedItem.Tag
        Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
        frmPROCEEspMod.Show 1
        If g_sOrigen <> "frmPEDIDOS" Then Accion = ACCSeguimientoAdjunCon
        g_sSelectedItem = ""
        lstvwEsp.selectedItem.ListSubItems.Item(2).Text = g_sComentario
        g_sComentario = s
        lstvwEsp.selectedItem.Text = g_sNombre
        If g_sNombre <> sNombre Then
            Set oFos = New Scripting.FileSystemObject
            If Not oFos.FileExists(oAdjun.Ruta & g_sNombre) Then
                oFos.MoveFile oAdjun.Ruta & sNombre, oAdjun.Ruta & g_sNombre
            End If
            Set oFos = Nothing
        End If
    End If
    
    Set oAdjun = Nothing
    Set g_oIBaseDatos = Nothing

End Sub

Private Sub cmdSalvarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim sFileName As String
Dim oFos As Scripting.FileSystemObject
On Error GoTo Cancelar

Set Item = lstvwEsp.selectedItem
If Item Is Nothing Then
    oMensajes.SeleccioneFichero
    Exit Sub
End If
cmmdEsp.DialogTitle = m_sIdiGuardar
cmmdEsp.CancelError = True
cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
cmmdEsp.filename = Item.Text
cmmdEsp.ShowSave
sFileName = cmmdEsp.filename
If g_sOrigen = "frmPEDIDOS" Then
    Set oFos = New Scripting.FileSystemObject
    sFileName = oFos.GetFileName(sFileName)
    LeerFichero sFileName
    oFos.CopyFile sFileName, cmmdEsp.filename
Else
    LeerFichero sFileName
End If
Cancelar:
    Set Item = Nothing
End Sub


Private Sub txtProceEsp_Change()
    If g_bRespetarCombo Then Exit Sub
    g_bRespetarCombo = True
    Accion = ACCSeguimientoAdjunMod
End Sub

Private Sub txtProceEsp_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit
If Accion = ACCSeguimientoAdjunMod Then
    Screen.MousePointer = vbHourglass
        If StrComp(NullToStr(g_oLinea.ObsAdjun), txtProceEsp.Text, vbTextCompare) <> 0 Then
            g_oLinea.ObsAdjun = StrToNull(txtProceEsp.Text)
            If txtProceEsp.Text <> "" Then
                g_oLinea.NumAdjuntos = 1
            Else
                If g_oLinea.Adjuntos Is Nothing Then
                    g_oLinea.NumAdjuntos = 0
                Else
                    If g_oLinea.Adjuntos.Count = 0 Then
                        g_oLinea.NumAdjuntos = 0
                    End If
                End If
            End If
            If g_sOrigen <> "frmPEDIDOS" Then
                teserror = g_oLinea.ModificarObsAdjun
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Cancel = True
                    Accion = ACCSeguimientoAdjunCon
                    Exit Sub
                End If

                'Registrar accion
                basSeguridad.RegistrarAccion Accion, "Orden entrega:" & g_oLinea.ObjetoOrden.Anyo & "/" & g_oLinea.ObjetoOrden.NumPedido & "/" & g_oLinea.ObjetoOrden.Numero & " Linea:" & g_oLinea.Id & " Esp:" & Left(g_oLinea.ObsAdjun, 50)
                Accion = ACCSeguimientoAdjunCon
            End If
        End If
    g_bRespetarCombo = False
    Screen.MousePointer = vbNormal
End If
    
End Sub


Public Sub Initialize(Optional ByVal Backcolor As Long = 0, Optional ByVal Forecolor As Long = 0)
If Not g_oOrden Is Nothing Then
    picEspGen.Visible = False
End If

CargarRecursos

'Array que contendra los ficheros eliminados
ReDim m_sayFileNames(0)
Accion = ACCSeguimientoAdjunCon
If Backcolor <> 0 Then
    UserControl.Backcolor = Backcolor
    Label1.Backcolor = Backcolor
    picBotones.Backcolor = Backcolor
    picEspGen.Backcolor = Backcolor
End If
If Forecolor <> 0 Then
    Label1.Forecolor = Forecolor
End If
If g_bModificar = False Then
    txtProceEsp.Locked = True
    cmdA�adirEsp.Enabled = False
    cmdEliminarEsp.Enabled = False
    cmdModificarEsp.Enabled = False
End If
txtProceEsp.Text = g_sProceEsp
cmmdEsp.FLAGS = cdlOFNHideReadOnly
If Not g_oOrden Is Nothing Then
    If txtProceEsp.Visible Then txtProceEsp.SetFocus
End If
End Sub

Private Sub UserControl_Resize()
Arrange
End Sub

Private Sub UserControl_Terminate()
Dim irespuesta As Integer
 Select Case Accion
    Case AccionesSummit.ACCSeguimientoAdjunMod
        txtProceEsp_Validate (irespuesta)
End Select

g_sOrigen = ""

Set g_oLinea = Nothing
Set g_oOrden = Nothing
End Sub
