VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmOFEPetAnya 
   BackColor       =   &H00808000&
   Caption         =   "Realizar peticiones de ofertas"
   ClientHeight    =   6075
   ClientLeft      =   1425
   ClientTop       =   1950
   ClientWidth     =   9270
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOFEPetAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6075
   ScaleWidth      =   9270
   Begin VB.TextBox txtForm 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   2445
      MaxLength       =   255
      TabIndex        =   8
      Top             =   1620
      Width           =   4530
   End
   Begin VB.CommandButton cmdForm 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7020
      Picture         =   "frmOFEPetAnya.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1620
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.CheckBox chkExcel 
      BackColor       =   &H00808000&
      Caption         =   "DIncluir hoja excel del petici�n de ofertas"
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   7560
      TabIndex        =   19
      Top             =   1810
      Width           =   1575
   End
   Begin VB.TextBox txtHoraLimite 
      Height          =   300
      Left            =   4680
      TabIndex        =   16
      Top             =   2955
      Width           =   1125
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPet 
      Height          =   2175
      Left            =   60
      TabIndex        =   20
      Top             =   3360
      Width           =   16830
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   10
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOFEPetAnya.frx":0FF4
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOFEPetAnya.frx":1010
      UseGroups       =   -1  'True
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   4
      Groups(0).Width =   6641
      Groups(0).Caption=   "Proveedores"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   2
      Groups(0).Columns(0).Width=   3281
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "CODPROVE"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   3360
      Groups(0).Columns(1).Caption=   "Denominaci�n"
      Groups(0).Columns(1).Name=   "DENPROVE"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(1).Width =   5689
      Groups(1).Caption=   "Contactos"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns(0).Width=   5689
      Groups(1).Columns(0).Caption=   "Apellidos"
      Groups(1).Columns(0).Name=   "APE"
      Groups(1).Columns(0).DataField=   "Column 2"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(2).Width =   2831
      Groups(2).Caption=   "Via de notificaci�n"
      Groups(2).HasHeadForeColor=   -1  'True
      Groups(2).HasHeadBackColor=   -1  'True
      Groups(2).HeadForeColor=   16777215
      Groups(2).HeadBackColor=   8421504
      Groups(2).Columns.Count=   3
      Groups(2).Columns(0).Width=   900
      Groups(2).Columns(0).Caption=   "Web"
      Groups(2).Columns(0).Name=   "WEB"
      Groups(2).Columns(0).DataField=   "Column 3"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Style=   2
      Groups(2).Columns(1).Width=   900
      Groups(2).Columns(1).Caption=   "Mail"
      Groups(2).Columns(1).Name=   "MAIL"
      Groups(2).Columns(1).DataField=   "Column 4"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(1).Style=   2
      Groups(2).Columns(2).Width=   1032
      Groups(2).Columns(2).Caption=   "Carta"
      Groups(2).Columns(2).Name=   "IMP"
      Groups(2).Columns(2).DataField=   "Column 5"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(2).Style=   2
      Groups(3).Width =   5583
      Groups(3).Visible=   0   'False
      Groups(3).Caption=   "Ocultas"
      Groups(3).Columns.Count=   4
      Groups(3).Columns(0).Width=   1085
      Groups(3).Columns(0).Caption=   "CODCON"
      Groups(3).Columns(0).Name=   "CODCON"
      Groups(3).Columns(0).DataField=   "Column 6"
      Groups(3).Columns(0).DataType=   8
      Groups(3).Columns(0).FieldLen=   256
      Groups(3).Columns(1).Width=   1296
      Groups(3).Columns(1).Caption=   "OFERTADOWEB"
      Groups(3).Columns(1).Name=   "OFERTADOWEB"
      Groups(3).Columns(1).DataField=   "Column 7"
      Groups(3).Columns(1).DataType=   8
      Groups(3).Columns(1).FieldLen=   256
      Groups(3).Columns(2).Width=   1191
      Groups(3).Columns(2).Caption=   "PORTALCON"
      Groups(3).Columns(2).Name=   "PORTALCON"
      Groups(3).Columns(2).DataField=   "Column 8"
      Groups(3).Columns(2).DataType=   8
      Groups(3).Columns(2).FieldLen=   256
      Groups(3).Columns(3).Width=   -3572
      Groups(3).Columns(3).Caption=   "TIPOEMAIL"
      Groups(3).Columns(3).Name=   "TIPOEMAIL"
      Groups(3).Columns(3).DataField=   "Column 9"
      Groups(3).Columns(3).DataType=   8
      Groups(3).Columns(3).FieldLen=   256
      _ExtentX        =   29686
      _ExtentY        =   3836
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   9270
      TabIndex        =   22
      Top             =   5580
      Width           =   9270
      Begin VB.CommandButton cmdAceptar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   3300
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "D&Cancelar"
         Height          =   315
         Left            =   4530
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveDen 
      Height          =   1575
      Left            =   1620
      TabIndex        =   24
      Top             =   3450
      Width           =   5055
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOFEPetAnya.frx":102C
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOFEPetAnya.frx":1048
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5477
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2990
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8916
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConApe 
      Height          =   1515
      Left            =   1140
      TabIndex        =   21
      Top             =   3510
      Width           =   5715
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOFEPetAnya.frx":1064
      stylesets(1).Name=   "Tan"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOFEPetAnya.frx":1080
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   4022
      Columns(0).Caption=   "Apellidos"
      Columns(0).Name =   "APE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3889
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "NOM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3757
      Columns(2).Caption=   "Mail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "PORT"
      Columns(4).Name =   "PORT"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "TIPOEMAIL"
      Columns(5).Name =   "TIPOEMAIL"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   10081
      _ExtentY        =   2672
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveCod 
      Height          =   1635
      Left            =   900
      TabIndex        =   23
      Top             =   3510
      Width           =   5175
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOFEPetAnya.frx":109C
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOFEPetAnya.frx":10B8
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2725
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6006
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9128
      _ExtentY        =   2884
      _StockProps     =   77
   End
   Begin VB.CommandButton cmdWebDot 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7005
      Picture         =   "frmOFEPetAnya.frx":10D4
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1005
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.CommandButton cmdMailDot 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7005
      Picture         =   "frmOFEPetAnya.frx":1416
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   585
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.TextBox txtWebDot 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   2430
      MaxLength       =   255
      TabIndex        =   6
      Top             =   1005
      Width           =   4530
   End
   Begin VB.TextBox txtMailDot 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   2430
      MaxLength       =   255
      TabIndex        =   4
      Top             =   585
      Width           =   4530
   End
   Begin VB.CheckBox chkFich 
      BackColor       =   &H00808000&
      Caption         =   "DIncluir archivos adjuntos"
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   7560
      TabIndex        =   18
      Top             =   980
      Width           =   1575
   End
   Begin VB.CheckBox chkEsp 
      BackColor       =   &H00808000&
      Caption         =   "DIncluir especificaciones de �tems"
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   7560
      TabIndex        =   17
      Top             =   150
      Width           =   1575
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   8700
      Top             =   3360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox txtDOT 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   2430
      MaxLength       =   255
      TabIndex        =   2
      Top             =   180
      Width           =   4530
   End
   Begin VB.CommandButton cmdDOT 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7005
      Picture         =   "frmOFEPetAnya.frx":1758
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   165
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.CommandButton cmdCalendar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3735
      Picture         =   "frmOFEPetAnya.frx":1A9A
      Style           =   1  'Graphical
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2970
      Width           =   315
   End
   Begin VB.TextBox txtFecLimOfe 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   2430
      TabIndex        =   14
      Top             =   2955
      Width           =   1275
   End
   Begin VB.Label lblTZHoraFinSub 
      BackColor       =   &H00808000&
      Caption         =   "Label11"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6000
      TabIndex        =   39
      Top             =   3000
      Width           =   1335
   End
   Begin VB.Label lblTZHoraIniSub 
      BackColor       =   &H00808000&
      Caption         =   "Label10"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6000
      TabIndex        =   38
      Top             =   2565
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label lblFecLimit 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   2430
      TabIndex        =   12
      Top             =   2520
      Width           =   1275
   End
   Begin VB.Label lblForm 
      BackColor       =   &H00808000&
      Caption         =   "Formulario de petici�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   34
      Top             =   1665
      Width           =   2235
   End
   Begin VB.Shape Shape4 
      BorderColor     =   &H00FFFFFF&
      Height          =   480
      Left            =   90
      Top             =   1530
      Width           =   7350
   End
   Begin VB.Label Label8 
      BackColor       =   &H00808000&
      Caption         =   "DNueva fecha l�mite:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   165
      TabIndex        =   33
      Top             =   2970
      Width           =   2370
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808000&
      Caption         =   "Hora"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4080
      TabIndex        =   32
      Top             =   2550
      Width           =   540
   End
   Begin VB.Shape Shape3 
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00FFFFFF&
      Height          =   3270
      Left            =   7515
      Top             =   60
      Width           =   1710
   End
   Begin VB.Shape Shape2 
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00FFFFFF&
      Height          =   1245
      Left            =   90
      Top             =   2085
      Width           =   7350
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808000&
      Caption         =   "Hora"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4080
      TabIndex        =   31
      Top             =   2985
      Width           =   540
   End
   Begin VB.Label Label3 
      BackColor       =   &H00808000&
      Caption         =   "Plantilla para web:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   165
      TabIndex        =   30
      Top             =   1050
      Width           =   2370
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Plantilla para e-mail:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   165
      TabIndex        =   29
      Top             =   645
      Width           =   2370
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00FFFFFF&
      Height          =   1380
      Left            =   90
      Top             =   60
      Width           =   7350
   End
   Begin VB.Label Label7 
      BackColor       =   &H00808000&
      Caption         =   "Plantilla para carta:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   165
      TabIndex        =   28
      Top             =   240
      Width           =   2370
   End
   Begin VB.Label lblFecPresBox 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   5985
      TabIndex        =   11
      Top             =   2130
      Width           =   1290
   End
   Begin VB.Label lblFecPres 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808000&
      Caption         =   "Fecha de presentaci�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3735
      TabIndex        =   26
      Top             =   2190
      Width           =   2190
   End
   Begin VB.Label lblFecNecBox 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   2430
      TabIndex        =   10
      Top             =   2130
      Width           =   1290
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Fecha de necesidad:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   165
      TabIndex        =   25
      Top             =   2190
      Width           =   2370
   End
   Begin VB.Label lblFecInicioSubasta 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   2430
      TabIndex        =   35
      Top             =   2520
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label lblHoraLimite 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   300
      Left            =   4680
      TabIndex        =   13
      Top             =   2520
      Width           =   1125
   End
   Begin VB.Label lblHoraInicioSubasta 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   300
      Left            =   4680
      TabIndex        =   36
      Top             =   2520
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.Label Label4 
      BackColor       =   &H00808000&
      Caption         =   "Fecha l�mite anterior:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   165
      TabIndex        =   27
      Top             =   2610
      Width           =   2370
   End
   Begin VB.Label Label9 
      BackColor       =   &H00808000&
      Caption         =   "Inicio de subasta:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   165
      TabIndex        =   37
      Top             =   2610
      Visible         =   0   'False
      Width           =   2370
   End
End
Attribute VB_Name = "frmOFEPetAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oFos As Scripting.FileSystemObject
Private oProves As CProveedores
Private oProve As CProveedor
Private oContactos As CContactos
Private oIasig As IAsignaciones  ' Para cargar las combos de proveedores asignables
Private m_oAsigs As CAsignaciones
Private WordVersion As Integer
Private bCargarComboDesdeDD As Boolean
'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private sayFileNames() As String
Private Nombres() As String
Public iNumProves As String 'Contendr� el n�mero de proveedores seleccionados, para el FUP
'Variable de idioma
Private sIdioma() As String
Private sParte() As String
Private m_sIdiImpreso As String
Private appword  As Object
Private m_iWordVer As Integer

Public bModificarFecLimit As Boolean  'Variable para saber si se puede modificar o no la fecha l�mite de ofertas
Public bModSubasta As Boolean 'Variable para saber si se puede modificar o no la fecha l�mite en subastas
Public g_bCancelarMail As Boolean
Public bPubMatProve As Boolean
Public Anyo As Integer
Public GMN1 As String
Public Cod As Long
Public vTZHora As Variant

Private m_stxtTexto As String
Private m_stxtNumero As String
Private m_stxtFecha As String
Private m_stxtSi As String
Private m_stxtOferta As String
Private m_stxtGrupo As String
Private m_stxtItem As String
Private m_stxtUniItem As String
Private m_stxtMinimo As String
Private m_stxtMaximo As String
Private m_sHoraLim As String
Private m_sSi As String
Private m_sNo As String
Private m_sIniSub As String
Private m_sCierreSub As String


Private oProvesAsignados As CProveedores
Private m_oOfertasProceso As COfertas

Private m_sProceso As String
Private m_sGrupo As String
Private m_sItem As String

Private m_vDatoAPasar As Variant
Private m_sFecIniSub As String

Private Sub chkEsp_Click()
    If chkEsp.Value = vbUnchecked Then chkFich.Value = vbUnchecked
End Sub

Private Sub chkFich_Click()
    If chkEsp.Value = vbUnchecked Then chkFich.Value = vbUnchecked
End Sub
''' <summary>
''' Valida los cambios
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema (bt cmdAceptar); Tiempo m�ximo: 0</remarks>
Private Sub cmdAceptar_Click()
    Dim oPet As CPetOferta
    Dim oPets As CPetOfertas
    Dim oIPet As IPeticiones
    Dim oEsp As CEspecificacion
    Dim oItem As CItem
    Dim oGrupo As CGrupo
    Dim oAsignaciones As CAsignaciones
    Dim oEquipo As CEquipo
    Dim oProves As CProveedores
    Dim NoComunicados() As Variant
    Dim NoEmail() As Variant
    Dim NoC As Variant
    Dim NoE As Variant
    Dim EsAutorizado As Variant
    Dim iDes As Integer
    Dim iMai As Integer
    Dim bNoComunicar As Boolean
    Dim Section As Object
    Dim docword As Object
    Dim docPetWord As Object
    Dim sTempNombreWord  As String
    Dim rangeword As Object
    Dim bVisibleWord As Boolean
    Dim sVersion As String
    Dim splantilla As String
    Dim splantillaCarta As String
    Dim sPlantillaWeb As String
    Dim sPlantillaMail As String
    Dim bSesionIniciada As Boolean   'Indica si se ha iniciado una sesion Mapi
    Dim i As Integer
    Dim bWeb As Boolean
    Dim bEMail As Boolean
    Dim bImp As Boolean
    Dim sTemp As String
    Dim bAdjuntarImpreso As Boolean
    Dim iNumPet As Integer
    Dim iNumPets As Integer
    Dim bSalvar As Boolean
    Dim errormail As TipoErrorSummit
    Dim teserror As TipoErrorSummit
    Dim iNumWeb As Integer
    Dim dFechaLimite As Variant
    Dim bPublicar As Boolean
    Dim vbm As Variant
    Dim dFechaSobres As Date
    Dim sCuerpo As String
    Dim oProve As CProveedor
    Dim scodProve As String
    Dim sIdCon As String
    Dim sTarget As String
    Dim oAtribsEspec As CAtributosEspecificacion
    Dim oAtribEsp As CAtributoEspecificacion
    Dim sCadena As String
    Dim sCadenaTmp As String
    Dim oregla As CReglaSubasta
    Dim dtFechaLimiteUTC As Date
    Dim dtHoraLimiteUTC As Date
    Dim sMailSubject As String
    Dim sTempCarta As String
    Dim ResulGrabaError As TipoErrorSummit
    Dim bSiguiente As Boolean
    Dim bEsHTML As Boolean
    Dim oError As TipoErrorSummit
    Dim bComunicando As Boolean
    Dim oCEmailProce As CEmailProce
    
    On Error GoTo Error:
    
    If bModificarFecLimit Then
        If txtFecLimOfe.Text = "" Then
            dFechaLimite = Null
        Else
            If Not IsDate(txtFecLimOfe.Text) Then
                oMensajes.NoValida sIdioma(1)
                Exit Sub
            End If
            If Trim(txtHoraLimite.Text) <> "" Then
                If Not IsTime(txtHoraLimite.Text) Then
                    oMensajes.NoValida m_sHoraLim
                    Exit Sub
                End If
            Else
                oMensajes.NoValida m_sHoraLim
                Exit Sub
            End If
            If CDate(txtFecLimOfe.Text) < frmOFEPet.oProcesoSeleccionado.FechaApertura Then
                oMensajes.FechaLimOfeMenorQueApertura IIf(frmOFEPet.oProcesoSeleccionado.ModoSubasta, 2, 0), frmOFEPet.oProcesoSeleccionado.FechaApertura
                Exit Sub
            End If

            dFechaLimite = CDate(FormatDateTime(txtFecLimOfe.Text, vbShortDate) & " " & FormatDateTime(CDate(txtHoraLimite.Text), vbShortTime))
            
            ConvertirTZaUTC oUsuarioSummit.TimeZone, DateValue(dFechaLimite), TimeValue(dFechaLimite), dtFechaLimiteUTC, dtHoraLimiteUTC
            dtFechaLimiteUTC = dtFechaLimiteUTC + dtHoraLimiteUTC
            If dtFechaLimiteUTC < ObtenerFechaUTC(Now) Then
                oMensajes.FechaLimiteCaducada
                Exit Sub
            End If
        End If
        'Comprobar la fecha con los sobres
        If frmOFEPet.oProcesoSeleccionado.AdminPublica Then
            If IsNull(dFechaLimite) Then
                oMensajes.FaltanDatos 35 'Fecha limite
                Exit Sub
            Else
                dFechaSobres = frmOFEPet.FechaMinimaDeSobre
                If CDate(dFechaLimite) > dFechaSobres Then
                    oMensajes.FechaSobreMenorQueLimOfer dFechaSobres, 128
                    Exit Sub
                End If
            End If
        End If
    Else
        dFechaLimite = frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas
        dtFechaLimiteUTC = dFechaLimite
    End If
        
    'Comprobaci�n de si se va a publicar
    bPublicar = False
    For i = 0 To sdbgPet.Rows - 1
        vbm = sdbgPet.AddItemBookmark(i)
        If sdbgPet.Columns("WEB").CellValue(vbm) <> "0" Then
            bPublicar = True
            Exit For
        End If
    Next
    
    If gParametrosGenerales.giINSTWEB <> SinWeb And Not bModificarFecLimit Then
        If IsDate(dFechaLimite) Then
            If CDate(Format(dtFechaLimiteUTC, "short date")) < DateValue(ObtenerFechaUTC(Date)) And bPublicar Then
                oMensajes.ImposiblePublicarCaducado
                Exit Sub
            End If
        End If
    End If
    
    If IsDate(dFechaLimite) Then
        If IsDate(frmOFEPet.oProcesoSeleccionado.FechaPresentacion) Then
            If CDate(Format(frmOFEPet.oProcesoSeleccionado.FechaPresentacion, "short date")) < CDate(Format(dFechaLimite, "short date")) Then
                oMensajes.FechaMayorQuePresentacion (Not frmOFEPet.oProcesoSeleccionado.PermitirAdjDirecta)
                Exit Sub
            End If
        End If
    
        If bModificarFecLimit Then
            If CDate(dtFechaLimiteUTC) < ObtenerFechaUTC(Now) Then
                If frmOFEPet.oProcesoSeleccionado.AdminPublica Then
                    oMensajes.ImposiblePublicarCaducado (Not bPublicar)
                    Exit Sub
                Else
                    i = oMensajes.PreguntaEliminarFecLimite((Not bPublicar))
                    If i = vbYes Then
                        dFechaLimite = Null
                        dtFechaLimiteUTC = Null
                    Else
                        Exit Sub
                    End If
                End If
            End If
        End If
    End If

    'Si se va a publicar y es una subasta la fecha de fin y de inicio no puedes ser nulas
    If bPublicar And frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
        If IsNull(frmOFEPet.oProcesoSeleccionado.FechaInicioSubasta) Then
            oMensajes.FechaInicioSubastaObligatoria
            Exit Sub
        End If
        
        If IsNull(dFechaLimite) Then
            oMensajes.FechaFinSubastaObligatoria
            Exit Sub
        End If
    End If
    
    'Comprobamos que las plantillas de word son v�lida
    'Petici�n de ofertas via carta
    If Trim(txtDOT.Text) = "" Then
        oMensajes.NoValido sIdioma(2)
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    Else
        If Right(txtDOT.Text, 3) <> "dot" Then
            oMensajes.NoValido sIdioma(2)
            If Me.Visible Then txtDOT.SetFocus
            Exit Sub
        End If
    End If
    
    If Not oFos.FileExists(txtDOT.Text) Then
        oMensajes.PlantillaNoEncontrada txtDOT.Text
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    End If
    
    splantillaCarta = txtDOT.Text
 
'   Peticion de ofertas via web.
    If gParametrosGenerales.giINSTWEB = ConPortal Or gParametrosGenerales.giINSTWEB = conweb Then
        If Trim(txtWebDot.Text) = "" Then
            oMensajes.NoValido sIdioma(2)
            If Me.Visible Then txtWebDot.SetFocus
            Exit Sub
        Else
            If LCase(Right(txtWebDot.Text, 3)) <> "htm" Then
                oMensajes.NoValido sIdioma(2)
                If Me.Visible Then txtWebDot.SetFocus
                Exit Sub
            End If
        End If
        
        If Not oFos.FileExists(txtWebDot.Text) Then
            oMensajes.PlantillaNoEncontrada txtWebDot.Text
            If Me.Visible Then txtWebDot.SetFocus
            Exit Sub
        End If
        splantilla = Left(txtWebDot.Text, Len(txtWebDot.Text) - 4) & ".txt"
        If Not oFos.FileExists(splantilla) Then
            oMensajes.PlantillaNoEncontrada splantilla
            If Me.Visible Then txtWebDot.SetFocus
            Exit Sub
        End If
    End If
 
  '  Peticion de ofertas via Mail
    If gParametrosGenerales.giMail <> 0 Then
        
        sPlantillaWeb = txtWebDot.Text
    
        If Trim(txtMailDot.Text) = "" Then
            oMensajes.NoValido sIdioma(2)
            If Me.Visible Then txtMailDot.SetFocus
            Exit Sub
        Else
            If LCase(Right(txtMailDot.Text, 3)) <> "htm" Then
                oMensajes.NoValido sIdioma(2)
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
            If Not oFos.FileExists(txtMailDot.Text) Then
                oMensajes.PlantillaNoEncontrada txtMailDot.Text
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
            splantilla = Left(txtMailDot.Text, Len(txtMailDot.Text) - 4) & ".txt"
            If Not oFos.FileExists(splantilla) Then
                oMensajes.PlantillaNoEncontrada splantilla
                If Me.Visible Then txtWebDot.SetFocus
                Exit Sub
            End If
        End If
        
        sPlantillaMail = txtMailDot.Text
    End If
    
    Screen.MousePointer = vbHourglass
    
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sIdioma(10)
    
    'Creamos las peticiones de ofertas
    Set oPets = Nothing
    Set oPets = oFSGSRaiz.generar_CPetOfertas
    
    'Generamos las peticiones correspondientes
    Set oProves = oFSGSRaiz.generar_CProveedores
    sdbgPet.Update
    iNumPet = 0
    For i = 0 To sdbgPet.Rows - 1
        vbm = sdbgPet.AddItemBookmark(i)
        
        Set oProve = Nothing
        
        If sdbgPet.Columns("CODPROVE").CellValue(vbm) <> "" And (sdbgPet.Columns("WEB").CellValue(vbm) <> "0" Or sdbgPet.Columns("MAIL").CellValue(vbm) <> "0" Or sdbgPet.Columns("IMP").CellValue(vbm) <> "0") Then
            If (sdbgPet.Columns("APE").CellValue(vbm) <> "" And (sdbgPet.Columns("WEB").CellValue(vbm) <> "0" Or sdbgPet.Columns("MAIL").CellValue(vbm) <> "0")) Or sdbgPet.Columns("IMP").CellValue(vbm) <> "0" Then
                Set oProve = oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").CellValue(vbm))
                sIdCon = CStr(sdbgPet.Columns("CODCON").CellValue(vbm))
                bEsHTML = (sdbgPet.Columns("TIPOEMAIL").CellValue(vbm) = 1)
                
                If oProve.Contactos Is Nothing Then
                    oProve.CargarTodosLosContactos
                Else
                    If oProve.Contactos.Item(sIdCon) Is Nothing Then
                        oProve.CargarTodosLosContactos
                    End If
                End If
                bWeb = False
                bEMail = False
                bImp = False
                
                If sdbgPet.Columns("WEB").CellValue(vbm) <> "0" Then
                    bWeb = True
                    iNumWeb = iNumWeb + 1
                Else
                    If sdbgPet.Columns("MAIL").CellValue(vbm) <> "0" Then
                        bEMail = True
                    Else
                        bImp = True
                    End If
                End If
                'Comprobar que el email existe en los usuarios del portal
                bNoComunicar = False
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    If bWeb Then
                        EsAutorizado = oProve.ComprobarAutorizacionPortal
                        If Not IsNull(EsAutorizado) Then
                            ReDim Preserve NoComunicados(2, iDes)
                            NoComunicados(0, iDes) = EsAutorizado(0)
                            NoComunicados(1, iDes) = EsAutorizado(1)
                            NoComunicados(2, iDes) = EsAutorizado(2)
                            iDes = iDes + 1
                            bNoComunicar = True
                        Else
                            If NullToStr(oProve.Contactos.Item(sIdCon).mail) <> "" And oProve.Contactos.Item(sIdCon).Port = True Then
                                If Not oProves.ComprobarEmailEnPortal(NullToStr(oProve.CodPortal), NullToStr(oProve.Contactos.Item(sIdCon).mail)) Then
                                    ReDim Preserve NoEmail(2, iMai)
                                    NoEmail(0, iMai) = oProve.Cod
                                    NoEmail(1, iMai) = oProve.Contactos.Item(sIdCon).mail
                                    NoEmail(2, iMai) = "EMAIL"
                                    iMai = iMai + 1
                                    bNoComunicar = True
                                End If
                            End If
                        End If
                    End If
                End If
                
                If Not bNoComunicar Then
                    'Se reusa AntesDeCierreParcial para guardar si es html � txt
                    oPets.Add oProve.Cod, oProve.Den, bWeb, bEMail, bImp, Date, Int(sIdCon), oProve.Contactos.Item(sIdCon).Apellidos, oProve.Contactos.Item(sIdCon).nombre, oProve.Contactos.Item(sIdCon).mail, iNumPet, oProve.Contactos.Item(sIdCon).Tfno, oProve.Contactos.Item(sIdCon).Tfno2, oProve.Contactos.Item(sIdCon).Fax, , , oProve.Contactos.Item(sIdCon).tfnomovil, bEsHTML
                    iNumPet = iNumPet + 1
                End If
            End If
        End If
    Next
    Set oProves = Nothing
    If iDes > 0 Then NoC = NoComunicados
    If iMai > 0 Then NoE = NoEmail
    If iDes > 0 Or iMai > 0 Then
        oMensajes.ImposiblePublicarPremiums NoC, NoE
    End If
    
    
    If bPublicar And Not frmOFEPet.oProcesoSeleccionado.AtributosEspValidados Then
        'Comprobacion de atributo de epecificacion obligatorios con validacion en la publicacion
        Set oAtribsEspec = frmOFEPet.oProcesoSeleccionado.ValidarAtributosEspObligatorios(TValidacionAtrib.Publicacion)
        If frmOFEPet.oProcesoSeleccionado.TError.NumError <> TESnoerror Then
            teserror = frmOFEPet.oProcesoSeleccionado.TError
            frmOFEPet.oProcesoSeleccionado.TError.NumError = TESnoerror
            GoTo Error
        End If
        If Not oAtribsEspec.Count = 0 Then
            sCadena = ""
            sCadenaTmp = ""
            For Each oAtribEsp In oAtribsEspec
                sCadenaTmp = oAtribEsp.Den
                Select Case oAtribEsp.ambito
                    Case TipoAmbitoProceso.AmbProceso
                        sCadenaTmp = sCadenaTmp & " " & m_sProceso & vbCrLf
                    Case TipoAmbitoProceso.AmbGrupo
                        sCadenaTmp = sCadenaTmp & " " & m_sGrupo & vbCrLf
                    Case TipoAmbitoProceso.AmbItem
                        sCadenaTmp = sCadenaTmp & " " & m_sItem & vbCrLf
                End Select
                If InStr(1, sCadena, sCadenaTmp) = 0 Then
                    sCadena = sCadena & sCadenaTmp
                End If
            Next
            oMensajes.AtributosEspObl sCadena, TValidacionAtrib.Publicacion
            Screen.MousePointer = vbNormal
            Unload frmESPERA
            Exit Sub
        End If
    
        teserror = frmOFEPet.oProcesoSeleccionado.ActualizarValidacionAtribEsp(True)
        If teserror.NumError <> TESnoerror Then GoTo Error
        frmOFEPet.oProcesoSeleccionado.AtributosEspValidados = True
        
    End If
    
    
    If chkExcel.Value = vbChecked Then
        Dim oIOfertas As iOfertas
        Set oIOfertas = frmOFEPet.oProcesoSeleccionado
        Set m_oOfertasProceso = oIOfertas.CargarOfertasDelProceso
        If frmOFEPet.oProcesoSeleccionado.TError.NumError <> TESnoerror Then
            teserror = frmOFEPet.oProcesoSeleccionado.TError
            frmOFEPet.oProcesoSeleccionado.TError.NumError = TESnoerror
            GoTo Error
        End If
        
    End If
    
    ' Recorremos las peticiones para enviar un mail u obtener un impreso
    iNumPet = 0
    iNumPets = oPets.Count
    sTemp = DevolverPathFichTemp
    
    If frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
        m_vDatoAPasar = lblFecLimit.caption & " " & Trim(lblHoraLimite.caption)
        m_sFecIniSub = Trim(lblFecInicioSubasta.caption) & " " & Trim(lblHoraInicioSubasta.caption)
    Else
        m_vDatoAPasar = txtFecLimOfe.Text & " " & Trim(txtHoraLimite.Text)
        m_sFecIniSub = ""
    End If
    
    For Each oPet In oPets
        sCuerpo = ""
        Set docword = Nothing
            
        g_bCancelarMail = False
        
        ReDim Nombres(0)
        iNumPet = iNumPet + 1
        
        frmESPERA.lblContacto = Mid(sIdioma(11) & " " & oPet.nombre & " " & oPet.Apellidos, 1, 100)
        
        frmESPERA.lblContacto.Refresh
        DoEvents
        frmESPERA.ProgressBar2.Value = val((iNumPet / iNumPets) * 100)
        frmESPERA.ProgressBar1.Max = 11
        frmESPERA.ProgressBar1.Value = 1
        frmESPERA.lblDetalle = sIdioma(12)
        frmESPERA.lblDetalle.Refresh
        
        ' DATOS de COMPRADORES
        Set oAsignaciones = oIasig.DevolverAsignaciones(, , , oPet.CodProve)
        If frmOFEPet.oProcesoSeleccionado.TError.NumError <> TESnoerror Then
            teserror = frmOFEPet.oProcesoSeleccionado.TError
            frmOFEPet.oProcesoSeleccionado.TError.NumError = TESnoerror
            GoTo Error
        End If
        Set oEquipo = Nothing
        Set oEquipo = oFSGSRaiz.generar_CEquipo
        oEquipo.Cod = oAsignaciones.Item(1).codEqp
        oEquipo.CargarTodosLosCompradoresDesde 1, oAsignaciones.Item(1).CodComp
        If oEquipo.TError.NumError <> TESnoerror Then
            teserror = oEquipo.TError
            oEquipo.TError.NumError = TESnoerror
            GoTo Error
        End If
                
        DoEvents
               
        If frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
            sMailSubject = gParametrosInstalacion.gsPETSUBMAILSUBJECT
        Else
            sMailSubject = gParametrosInstalacion.gsPetOfeMailSubject
        End If
               
        '********** PETICIONES VIA WEB ***************
                        
        If oPet.ViaWeb = True And oPet.Email <> "" Then
                
            ' Enviamos un mail al proveedor
               
            If Not bSesionIniciada Or oIdsMail Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sIdioma(12)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set oIdsMail = IniciarSesionMail
                bSesionIniciada = True
            End If
            
            sParte = DividirFrase(sIdioma(3))
            frmESPERA.ProgressBar1.Value = 3
            frmESPERA.lblDetalle = Mid(sParte(1) & SoloFichero(sPlantillaWeb) & sParte(2), 1, 50)
            frmESPERA.lblDetalle.Refresh
            DoEvents
            
            'Se reusa AntesDeCierreParcial para saber si es html � txt
            Set oCEmailProce = GenerarCEmailProce(oFSGSRaiz, gParametrosGenerales, basPublic.gParametrosInstalacion, oMensajes, basOptimizacion.gTipoDeUsuario, oUsuarioSummit, basParametros.gLongitudesDeCodigos)
            sCuerpo = oCEmailProce.GenerarMensajePeticionOferta(txtWebDot.Text, oPet, oEquipo, frmOFEPet.sdbcAnyo.Text, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.sdbcProceCod.Text, _
                            frmOFEPet.sdbcProceDen.Text, m_vDatoAPasar, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.oProcesoSeleccionado.Referencia, m_sFecIniSub)
            Set oCEmailProce = Nothing
            
            frmESPERA.ProgressBar1.Value = 4
            frmESPERA.lblDetalle = sIdioma(14)
            frmESPERA.lblDetalle.Refresh
            DoEvents
            
            frmESPERA.ProgressBar1.Value = 5
            frmESPERA.lblDetalle = sIdioma(15)
            frmESPERA.lblDetalle.Refresh
            DoEvents
            
            'Si hay que adjuntar las especificaciones
            If chkFich.Value = vbChecked Then
                AdjuntarEspecificaciones sTemp, oPet.CodProve
            End If
            
            'Si hay que adjuntar la hoja excel de petici�n de ofertas:
            If chkExcel.Value = vbChecked Then AdjuntarExcel oPet
            
            frmESPERA.ProgressBar1.Value = 11
            frmESPERA.lblDetalle = sIdioma(15)
            frmESPERA.lblDetalle.Refresh
            DoEvents
                    
            ' enviar mensaje
            sMailSubject = ComponerAsuntoEMail(sMailSubject, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, frmOFEPet.oProcesoSeleccionado.Den)
            bComunicando = True
            errormail = ComponerMensaje(oPet.Email, sMailSubject, sCuerpo, Nombres, "frmOfePetAnya", oPet.AntesDeCierreParcial, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, _
                                            entidadNotificacion:=ProcesoCompra, _
                                            tipoNotificacion:=PetOfertaWeb, _
                                            Anyo:=frmOFEPet.oProcesoSeleccionado.Anyo, _
                                            GMN1:=frmOFEPet.oProcesoSeleccionado.GMN1Cod, _
                                            Proce:=frmOFEPet.oProcesoSeleccionado.Cod, _
                                            sToName:=oPet.nombre & " " & oPet.Apellidos)
            If errormail.NumError <> TESnoerror Then
                oPet.MailCancel = True
                GoTo Error
            ElseIf g_bCancelarMail = True Then
                oPet.MailCancel = True
                
            End If
        
        End If
        ' Env�o del mail al proveedor con las especificaciones adjuntas del proceso y de los items
        If oPet.viaEMail And oPet.Email <> "" Then
            
            If Not bSesionIniciada Or oIdsMail Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sIdioma(12)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set oIdsMail = IniciarSesionMail
                bSesionIniciada = True
            End If
            
        
            'Realizamos el envio de mensajes desde el cliente
            ' Enviamos un mail al proveedor
            
            If appword Is Nothing Then
                frmESPERA.ProgressBar1.Value = 3
                frmESPERA.lblDetalle = sIdioma(13)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set appword = CreateObject("Word.Application")
                'establecer el caption de la aplicaci�n word para despu�s poder encontrar su ventana y traerla al frente
                appword.caption = appword.caption & " " & CStr(Rnd)
                bSalvar = appword.Options.SavePropertiesPrompt
                appword.Options.SavePropertiesPrompt = False
                bVisibleWord = appword.Visible
                appword.WindowState = 2
                sVersion = appword.VERSION
                If InStr(1, sVersion, ".") > 1 Then
                    sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                    m_iWordVer = CInt(sVersion)
                Else
                    m_iWordVer = 9
                End If
            Else
                sVersion = appword.VERSION
            End If
                
            frmESPERA.ProgressBar1.Value = 4
            frmESPERA.lblDetalle = sIdioma(9) & " " & sPlantillaMail & " ..."
            frmESPERA.lblDetalle.Refresh
            DoEvents
                            
            'Se reusa AntesDeCierreParcial para saber si es html � txt
            Set oCEmailProce = GenerarCEmailProce(oFSGSRaiz, gParametrosGenerales, basPublic.gParametrosInstalacion, oMensajes, basOptimizacion.gTipoDeUsuario, oUsuarioSummit, basParametros.gLongitudesDeCodigos)
            sCuerpo = oCEmailProce.GenerarMensajePeticionOferta(txtMailDot.Text, oPet, oEquipo, frmOFEPet.sdbcAnyo.Text, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.sdbcProceCod.Text, _
                            frmOFEPet.sdbcProceDen.Text, m_vDatoAPasar, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.oProcesoSeleccionado.Referencia, m_sFecIniSub)
            Set oCEmailProce = Nothing
                                            
            ' Primero adjunto el documento con el impreso a rellenar.
            frmESPERA.ProgressBar1.Value = 5
            frmESPERA.lblDetalle = sIdioma(16)
            frmESPERA.lblDetalle.Refresh
            DoEvents
            Set docPetWord = ImpresoOferta(oPet)
            If Not docPetWord Is Nothing Then
                sTemp = DevolverPathFichTemp
                sTempNombreWord = DevolverNombreArchivo(m_sIdiImpreso, sTemp, "doc")
                docPetWord.SaveAs sTemp & sTempNombreWord
                docPetWord.Close
              
                sayFileNames(UBound(sayFileNames)) = sTemp & sTempNombreWord
                ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                Nombres(UBound(Nombres)) = sTempNombreWord
                ReDim Preserve Nombres(UBound(Nombres) + 1)
                
            End If
            'Si hay que adjuntar las especificaciones
            If chkFich.Value = vbChecked Then
                AdjuntarEspecificaciones sTemp, oPet.CodProve
            End If
            'Si hay que adjuntar la hoja excel de petici�n de ofertas:
            If chkExcel.Value = vbChecked Then
                AdjuntarExcel oPet
            End If
                            
            frmESPERA.ProgressBar1.Value = 10
            frmESPERA.lblDetalle = sIdioma(15)
            frmESPERA.lblDetalle.Refresh
            DoEvents
            
            sMailSubject = ComponerAsuntoEMail(sMailSubject, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, frmOFEPet.oProcesoSeleccionado.Den)
            bComunicando = True
            errormail = ComponerMensaje(oPet.Email, sMailSubject, sCuerpo, Nombres, "frmOfePetAnya", _
                                            oPet.AntesDeCierreParcial, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, _
                                            Anyo:=frmOFEPet.oProcesoSeleccionado.Anyo, _
                                            GMN1:=frmOFEPet.oProcesoSeleccionado.GMN1Cod, _
                                            Proce:=frmOFEPet.oProcesoSeleccionado.Cod, _
                                            sToName:=oPet.nombre & " " & oPet.Apellidos)
            If errormail.NumError <> TESnoerror Then
                oPet.MailCancel = True
                GoTo Error
            ElseIf g_bCancelarMail = True Then
                oPet.MailCancel = True
            End If
        End If
                
        ' Notificaciones impresas
        'Generamos un documento de notificaci�n impresa para cada contacto
        
        ' *************** PETICIONES VIA IMPRESO **********************
        If oPet.ViaImp Then
            
            bAdjuntarImpreso = True
            
            oPet.MailCancel = False
            
            If appword Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sIdioma(13)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set appword = CreateObject("Word.Application")
                'establecer el caption de la aplicaci�n word para despu�s poder encontrar su ventana y traerla al frente
                appword.caption = appword.caption & " " & CStr(Rnd)
                bSalvar = appword.Options.SavePropertiesPrompt
                appword.Options.SavePropertiesPrompt = False
                bVisibleWord = appword.Visible
                appword.WindowState = 2
                sVersion = appword.VERSION
                If InStr(1, sVersion, ".") > 1 Then
                    sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                    m_iWordVer = CInt(sVersion)
                Else
                    m_iWordVer = 9
                End If
            Else
                sVersion = appword.VERSION
            End If
            
            sParte = DividirFrase(sIdioma(3))
            frmESPERA.ProgressBar1.Value = 6
            frmESPERA.lblDetalle = sParte(1) & SoloFichero(splantillaCarta) & sParte(2)
            frmESPERA.lblDetalle.Refresh
            DoEvents
                
            Set docword = CartaImpreso(splantillaCarta, oPet, oEquipo)
            
            'Generamos el impreso
            frmESPERA.ProgressBar1.Value = 10
            frmESPERA.lblDetalle = sIdioma(16)
            frmESPERA.lblDetalle.Refresh
            DoEvents
            Set docPetWord = ImpresoOferta(oPet)
            If Not docPetWord Is Nothing Then
                ' Tengo que pegar este �ltimo documento en las peticiones anteriores
                docPetWord.Range.Select
                docPetWord.Range.Copy
                docPetWord.Close False

                Set rangeword = docword.Range
                rangeword.Collapse 0 'wdCollapseEnd
                Set Section = docword.Sections.Add(rangeword, 0)
                docword.Sections(2).Range.Paste
            End If
            

        End If
                
Siguiente:

        bComunicando = False
        If oPet.MailCancel = False Then
            basSeguridad.RegistrarAccion AccionesSummit.ACCPetOfeAnya, "Anyo:" & CStr(frmOFEPet.sdbcAnyo.Value) & "GMN1:" & CStr(frmOFEPet.sdbcGMN1_4Cod.Value) & "Proce:" & Int(frmOFEPet.sdbcProceCod.Value) & "Prove:" & oPet.CodProve & "Con:" & oPet.Apellidos
        End If

    Next
    
    bComunicando = False
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sIdioma(20)
        
    'Modificamos la fecha l�mite de ofertas del proceso
    Set oIPet = frmOFEPet.oProcesoSeleccionado
    
    If bModificarFecLimit Then
        Dim iCancel As Integer
        iCancel = 0
        For Each oPet In oPets
            If oPet.MailCancel = True Then
                iCancel = iCancel + 1
            End If
        Next
        If iCancel < oPets.Count Then
            If (Not IsNull(dFechaLimite) And Not IsNull(frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas) And dFechaLimite <> frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas) Or _
                (Not IsNull(dFechaLimite) And IsNull(frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas)) Or _
                (IsNull(dFechaLimite) And Not IsNull(frmOFEPet.oProcesoSeleccionado.FechaMinimoLimOfertas)) Then
                teserror = oIPet.ModificarFechaLimiteDeOfertas(dtFechaLimiteUTC)
                If teserror.NumError <> TESnoerror Then
                    GoTo Error
                Else
                    basSeguridad.RegistrarAccion AccionesSummit.ACCPetOfeModFecLimit, "Anyo:" & CStr(frmOFEPet.sdbcAnyo.Value) & "GMN1:" & CStr(frmOFEPet.sdbcGMN1_4Cod.Value) & "Proce:" & Int(frmOFEPet.sdbcProceCod.Value)
                End If
            End If
        End If
    End If
    
    ' Almacenamos las peticiones en la BD ' FALTA MOVER A PETICION WEB
    If gParametrosGenerales.giINSTWEB = ConPortal And iNumWeb > 0 Then
        frmEsperaPortal.Show
        DoEvents
    End If
    NoC = Null
    teserror = oIPet.RealizarPeticiones(oPets)
    If gParametrosGenerales.giINSTWEB = ConPortal And iNumWeb > 0 Then
        Unload frmEsperaPortal
    End If
    
    If teserror.NumError <> TESnoerror Then
        GoTo Error
    Else
        If IsArray(teserror.Arg1) Then
            NoC = teserror.Arg1
        End If
    End If
    
    If bSesionIniciada Then
        frmESPERA.ProgressBar1.Value = 5
        frmESPERA.lblDetalle = sIdioma(18)
        FinalizarSesionMail
        bSesionIniciada = False
    End If
    
    
    If Not appword Is Nothing Then appword.Options.SavePropertiesPrompt = bSalvar
    
    If bAdjuntarImpreso Then
        frmESPERA.ProgressBar1.Value = 10
        frmESPERA.lblDetalle = Mid(sIdioma(4) & SoloFichero(sPlantillaWeb) & " ...", 1, 100)
        frmESPERA.lblDetalle.Refresh
        DoEvents
        If Not appword Is Nothing Then
            appword.Selection.HomeKey Unit:=6
            appword.WindowState = 1
            appword.Visible = True
        End If
        bVisibleWord = True
    End If
                
    If chkExcel.Value = vbChecked Then
        'Si se ha seleccionado la check de petici�n de ofertas comprueba si hab�a generado
        'la hoja para el caso de que no haya ofertas,y si es as� la borra.
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If sTemp = "" Then
            sTemp = DevolverPathFichTemp
        End If
        sTarget = sTemp & frmOFEPet.oProcesoSeleccionado.Anyo & "_" & frmOFEPet.oProcesoSeleccionado.GMN1Cod & "_" & frmOFEPet.oProcesoSeleccionado.Cod & ".xls"
        If FOSFile.FileExists(sTarget) Then
            FOSFile.DeleteFile sTarget, True
        End If
        Set FOSFile = Nothing
        Set oIOfertas = Nothing
        Set m_oOfertasProceso = Nothing
    End If
    If IsArray(NoC) Then
        oMensajes.ImposiblePublicarPremiums (NoC)
    End If
    
FinalControlado:

    If bSesionIniciada Then
        FinalizarSesionMail
    End If

    Unload frmESPERA

    frmOFEPet.cmdRestaurar_Click
    
    Screen.MousePointer = vbNormal
On Error Resume Next
    Set oProve = Nothing
    If bVisibleWord = False And Not appword Is Nothing Then
        ' cuando se realizan solo peticiones via web, queda una tarea
        ' de word abierta que no se destruye con appword = nothing
        appword.Quit
    End If
    Set docword = Nothing
    Set docPetWord = Nothing
    Set Section = Nothing
    Set rangeword = Nothing
    Set oEquipo = Nothing
    Set oAsignaciones = Nothing
    Set oEsp = Nothing
    Set oItem = Nothing
    Set oPets = Nothing
    Set oIPet = Nothing
    Set oContactos = Nothing
    Set oregla = Nothing
    
    Screen.MousePointer = vbNormal
    
    Unload Me
    
    If bAdjuntarImpreso And Not appword Is Nothing Then
        'Traer el word al frente. Se hace despu�s del Unload porque si se hace antes se vuelve a quedar por detr�s
        BringWindowToTop FindWindow(0&, appword.caption)
    End If
    Set appword = Nothing
          
    Exit Sub
                    
Error:

    oError.Arg1 = err.Number
    oError.Arg2 = err.Description
'Resume Next
    If bComunicando And errormail.NumError <> TESnoerror Then
        'viene de un error en componermensaje con lo que el error ya se ha guardado en BD, muestro un mensaje y siguiente
        If Right(CStr(errormail.Arg2), 3) = "$$4" Then
            oMensajes.ErrorComunicacionMail_MalMail False, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, Left(CStr(errormail.Arg2), Len(CStr(errormail.Arg2)) - 3)
        ElseIf Right(CStr(errormail.Arg2), 3) = "$$5" Then
            oMensajes.ErrorComunicacionMail_MalMail True, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre, Left(CStr(errormail.Arg2), Len(CStr(errormail.Arg2)) - 3), errormail.Arg1
        Else
            oMensajes.ErrorComunicacionMail errormail.Arg1, oPet.CodProve, oPet.Apellidos & ", " & oPet.nombre
        End If
        bSiguiente = True
    ElseIf teserror.NumError <> TESnoerror Then
        TratarError teserror
        bSiguiente = False
    Else
        If oError.Arg1 = 462 Then
            ResulGrabaError = basErrores.GrabarError("frmOFEPetAnya.cmdAceptar", CStr(oError.Arg1), "Descr:" & oError.Arg2)
            ' the remote server machine... " han cerrado el word"
            Set appword = Nothing
            DoEvents
            Set appword = CreateObject("Word.Application")
            'establecer el caption de la aplicaci�n word para despu�s poder encontrar su ventana y traerla al frente
            appword.caption = appword.caption & " " & CStr(Rnd)
            bSalvar = appword.Options.SavePropertiesPrompt
            appword.Options.SavePropertiesPrompt = False
            bVisibleWord = appword.Visible
            appword.WindowState = 2
            sVersion = appword.VERSION
            If InStr(1, sVersion, ".") > 1 Then
                sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
                m_iWordVer = CInt(sVersion)
            Else
                m_iWordVer = 9
            End If
            Resume Next
        Else
            If oPet Is Nothing Then
                ResulGrabaError = basErrores.GrabarError("frmOFEPetAnya.cmdAceptar", CStr(oError.Arg1), "Descr:" & oError.Arg2)
            Else
                ResulGrabaError = basErrores.GrabarError("frmOFEPetAnya.cmdAceptar", CStr(oError.Arg1), "Prove:" & oPet.CodProve & " Con:" & oPet.Apellidos & ", " & oPet.nombre & " Descr:" & oError.Arg2)
            End If
            ResulGrabaError.NumError = TESErrorCodigo
            TratarError ResulGrabaError
            bSiguiente = False
        End If
    End If


            
    If bSiguiente Then
        GoTo Siguiente
    Else
        GoTo FinalControlado
    End If
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdCalendar_Click()
    AbrirFormCalendar Me, txtFecLimOfe
End Sub

Private Sub cmdCancelar_Click()
    
    Unload Me
    
End Sub

Private Sub cmdDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdioma(5) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOT.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub

Private Sub cmdForm_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdioma(5) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtForm.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub


End Sub

''' <summary>
''' Elegir la plantilla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdMailDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdioma(5) & " (*.htm)|*.htm"
    cmmdDot.DefaultExt = "htm"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtMailDot.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub

End Sub

''' <summary>
''' Elegir la plantilla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdWebDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdioma(5) & " (*.htm)|*.htm"
    cmmdDot.DefaultExt = "htm"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtWebDot.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub

End Sub



''' <summary>
''' Carga los recursos de la pagina
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;Load de la pagina Tiempo m�ximo 0</remarks>

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OFEPETANYA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 22)
        For i = 1 To 19
            sIdioma(i) = Ador(0).Value '1
            Ador.MoveNext
        Next
        chkEsp.caption = Ador(0).Value '20
        Ador.MoveNext
        chkFich.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmOFEPetAnya.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value '25
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Ador.MoveNext
        Label3.caption = Ador(0).Value
        Ador.MoveNext
        Label4.caption = Ador(0).Value
        Ador.MoveNext
        Label7.caption = Ador(0).Value
        Ador.MoveNext
        lblFecPres.caption = Ador(0).Value '30
        Ador.MoveNext
        sdbddConApe.Columns("APE").caption = Ador(0).Value
        'sdbddConNom.Columns(1).Caption = Ador(0).Value

        Ador.MoveNext
        sdbddConApe.Columns("NOM").caption = Ador(0).Value
        'sdbddConNom.Columns(0).Caption = Ador(0).Value
        'sdbgPet.Groups(1).Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        sdbddConApe.Columns("EMAIL").caption = Ador(0).Value
        'sdbddConNom.Columns(2).Caption = Ador(0).Value
        'sdbgPet.Groups(2).Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        sdbddProveCod.Columns("COD").caption = Ador(0).Value '34 Codigo
        sdbddProveDen.Columns("COD").caption = Ador(0).Value
        sdbgPet.Columns("CODPROVE").caption = Ador(0).Value
        Ador.MoveNext
        sdbddProveCod.Columns("DEN").caption = Ador(0).Value '35 Denominaci�n
        sdbddProveDen.Columns("DEN").caption = Ador(0).Value
        sdbgPet.Columns("DENPROVE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(0).caption = Ador(0).Value '
        Ador.MoveNext
        sdbgPet.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(2).Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(2).Columns(2).caption = Ador(0).Value '40
        Ador.MoveNext
        m_stxtOferta = Ador(0).Value
        Ador.MoveNext
        m_stxtGrupo = Ador(0).Value
        Ador.MoveNext
        m_stxtItem = Ador(0).Value
        Ador.MoveNext
        m_stxtUniItem = Ador(0).Value
        Ador.MoveNext
        m_stxtTexto = Ador(0).Value
        Ador.MoveNext
        m_stxtNumero = Ador(0).Value
        Ador.MoveNext
        m_stxtFecha = Ador(0).Value
        Ador.MoveNext
        m_stxtSi = Ador(0).Value
        Ador.MoveNext
        m_stxtMinimo = Ador(0).Value
        Ador.MoveNext
        m_stxtMaximo = Ador(0).Value
        Ador.MoveNext
        m_sHoraLim = Ador(0).Value
        Ador.MoveNext
        Label5.caption = Ador(0).Value
        Label6.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("APE").caption = Ador(0).Value
        Ador.MoveNext
        chkExcel.caption = Ador(0).Value '54 Incluir hoja excel de petici�n de ofertas
        Ador.MoveNext
        sIdioma(20) = Ador(0).Value  '55 Almacenando peticiones en base de datos...
        Ador.MoveNext
        sIdioma(21) = Ador(0).Value  '56 Generando hoja excel de petici�n de ofertas...
        Ador.MoveNext
        m_sIdiImpreso = Ador(0).Value
        Ador.MoveNext
        Me.Label8.caption = Ador(0).Value
        Ador.MoveNext
        lblForm.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        m_sSi = Ador(0).Value 'S�
        Ador.MoveNext
        m_sNo = Ador(0).Value 'No
        
        Ador.MoveNext
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        m_sGrupo = Ador(0).Value
        Ador.MoveNext
        m_sItem = Ador(0).Value
        Ador.MoveNext
        sIdioma(22) = Ador(0).Value
        Ador.MoveNext
        m_sIniSub = Ador(0).Value
        Ador.MoveNext
        m_sCierreSub = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Activate()
    Dim dcListaZonasHorarias As Dictionary
    Dim oProceso As CProceso
    Dim dtFechaIniSub As Date
    Dim dtHoraIniSub As Date
    Dim dtFechaFinSub As Date
    Dim dtHoraFinSub As Date
    Dim tzInfo As TIME_ZONE_INFO
    
    'Es posible que la zona horaria haya cambiado en otra pantalla, actualizarla
    If oUsuarioSummit.TimeZone <> vTZHora Then
        vTZHora = oUsuarioSummit.TimeZone
        Set dcListaZonasHorarias = ObtenerZonasHorarias
        Set oProceso = oFSGSRaiz.Generar_CProceso
        oProceso.Anyo = Anyo
        oProceso.GMN1Cod = GMN1
        oProceso.Cod = Cod
        oProceso.CargarDatosGeneralesProceso bSinpres:=True, bSinGRupos:=True, sUsu:=oUsuarioSummit.Cod
            
        If Not oProceso Is Nothing Then
            tzInfo = GetTimeZoneByKey(vTZHora)
            lblTZHoraFinSub = Mid(tzInfo.DisplayName, 2, 9)
            
            'Fecha l�mite ofertas
            If IsNull(oProceso.FechaMinimoLimOfertas) Then
                lblFecLimit = vbNullString
                lblHoraLimite = vbNullString
                    
                If Not oProceso.ModoSubasta Then
                    txtFecLimOfe = ""
                    txtHoraLimite.Text = ""
                End If
            Else
                ConvertirUTCaTZ DateValue(oProceso.FechaMinimoLimOfertas), TimeValue(oProceso.FechaMinimoLimOfertas), vTZHora, dtFechaFinSub, dtHoraFinSub
                    
                lblFecLimit = Format(dtFechaFinSub, "short date")
                lblHoraLimite = Format(dtHoraFinSub, "short time")
                
                If Not oProceso.ModoSubasta Then
                    ConvertirUTCaTZ DateValue(oProceso.FechaMinimoLimOfertas), TimeValue(oProceso.FechaMinimoLimOfertas), vTZHora, dtFechaFinSub, dtHoraFinSub
                    
                    txtFecLimOfe = Format(dtFechaFinSub, "short date")
                    txtHoraLimite.Text = Format(dtHoraFinSub, "short time")
                End If
            End If
            
            'Fecha inicio subasta
            If oProceso.ModoSubasta Then
                ConvertirUTCaTZ DateValue(oProceso.FechaInicioSubasta), TimeValue(oProceso.FechaInicioSubasta), vTZHora, dtFechaIniSub, dtHoraIniSub
                
                lblFecInicioSubasta = Format(dtFechaIniSub, "short date")
                lblHoraInicioSubasta = Format(dtHoraIniSub, "short time")
                lblTZHoraIniSub = Mid(tzInfo.DisplayName, 2, 9)
            End If
        End If
        
        Set oProceso = Nothing
        Set dcListaZonasHorarias = Nothing
    End If
End Sub

Private Sub Form_Load()
    
    On Error GoTo Error:
    
    Me.Width = 9450
    Me.Height = 6480
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
        
    PonerFieldSeparator Me
            
    If frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
        bModificarFecLimit = False
        cmdCalendar.Visible = False
        
        Label9.Visible = True
        lblFecInicioSubasta.Visible = True
        lblHoraInicioSubasta.Visible = True
        lblTZHoraIniSub.Visible = True
        'lblTZHoraFinSub.Visible = True
        
        'Colocar los labels de la Fec. L�mite en la posici�n del textbox
        Label8.Visible = False
        txtFecLimOfe.Visible = False
        txtHoraLimite.Visible = False
        Label4.Top = Label8.Top
        lblFecLimit.Top = txtFecLimOfe.Top
        lblHoraLimite.Top = txtHoraLimite.Top
        
        Label9.caption = m_sIniSub
        Label4.caption = m_sCierreSub
    End If
    
    If bModificarFecLimit Then
        txtFecLimOfe.Enabled = True
        cmdCalendar.Visible = True
        txtHoraLimite.Locked = False
    Else
        txtFecLimOfe.Enabled = False
        cmdCalendar.Visible = False
        txtHoraLimite.Locked = True
    End If
    
    'Inicializar el array de ficheros
    ReDim sayFileNames(0)
    ReDim Nombres(0)
    
    Set oFos = New Scripting.FileSystemObject
    
    Set oProve = oFSGSRaiz.generar_CProveedor
    Set oIasig = frmOFEPet.oProcesoSeleccionado
    iNumProves = ContarProveedoresAsignados
    
    Set oProvesAsignados = oFSGSRaiz.generar_CProveedores
    If frmOFEPet.bComunAsigComp Then
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , frmOFEPet.oProcesoSeleccionado.ModoSubasta)
    ElseIf frmOFEPet.bComunAsigEqp Then
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, gCodEqpUsuario, , , , , , , , , , frmOFEPet.oProcesoSeleccionado.ModoSubasta)
    Else
        Set oProvesAsignados = oIasig.DevolverProveedoresDesde("", "", , OrdAsigPorCodProve, , , , , , , , , , , frmOFEPet.oProcesoSeleccionado.ModoSubasta)
    End If
    
    If gParametrosGenerales.gbProveGrupos Then
        Set m_oAsigs = oIasig.DevolverAsignaciones(OrdAsigPorCodProve, , , , , True)
    End If
    
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        txtWebDot.Enabled = False
        cmdWebDot.Enabled = False
    End If
    
    If gParametrosGenerales.giMail = 0 Then
        txtMailDot.Enabled = False
        cmdMailDot.Enabled = False
        txtWebDot.Enabled = False
        cmdWebDot.Enabled = False
        txtForm.Enabled = False
        cmdForm.Enabled = False
    End If
    
    sdbgPet.Columns("CODPROVE").DropDownHwnd = sdbddProveCod.hWnd
    sdbgPet.Columns("DENPROVE").DropDownHwnd = sdbddProveDen.hWnd
    sdbgPet.Columns("APE").DropDownHwnd = sdbddConApe.hWnd
    
    sdbddConApe.AddItem ""
    sdbddProveCod.AddItem ""
    sdbddProveDen.AddItem ""
        
    If frmOFEPet.oProcesoSeleccionado.PermitirAdjDirecta Then
        lblFecPres.Visible = False
        lblFecPresBox.Visible = False
    End If
    
    If frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
        If gParametrosInstalacion.gsPETSUBCARTADOT = "" Then
            If gParametrosGenerales.gsPETSUBCARTADOT = "" Then
                oMensajes.NoValido sIdioma(6)
            Else
                gParametrosInstalacion.gsPETSUBCARTADOT = gParametrosGenerales.gsPETSUBCARTADOT
                g_GuardarParametrosIns = True
            End If
        End If
        
        If gParametrosInstalacion.gsPETSUBDOT = "" Then
            If gParametrosGenerales.gsPETSUBDOT = "" Then
                oMensajes.NoValido sIdioma(8)
            Else
                gParametrosInstalacion.gsPETSUBDOT = gParametrosGenerales.gsPETSUBDOT
                g_GuardarParametrosIns = True
            End If
        End If
        
        If gParametrosGenerales.giMail <> 0 Then
            If gParametrosInstalacion.gsPETSUBMAILDOT = "" Then
                If gParametrosGenerales.gsPETSUBMAILDOT = "" Then
                    oMensajes.NoValido sIdioma(7)
                Else
                    gParametrosInstalacion.gsPETSUBMAILDOT = gParametrosGenerales.gsPETSUBMAILDOT
                    g_GuardarParametrosIns = True
                End If
            End If
        End If
        
        If gParametrosGenerales.giINSTWEB = ConPortal Or gParametrosGenerales.giINSTWEB = conweb Then
            If gParametrosInstalacion.gsPETSUBWEBDOT = "" Then
                If gParametrosGenerales.gsPETSUBWEBDOT = "" Then
                    oMensajes.NoValido sIdioma(6)
                Else
                    gParametrosInstalacion.gsPETSUBWEBDOT = gParametrosGenerales.gsPETSUBWEBDOT
                    g_GuardarParametrosIns = True
                End If
            End If
        End If
           
    Else
        If gParametrosInstalacion.gsPetOfeCarta = "" Then
            If gParametrosGenerales.gsPETOFECARTADOT = "" Then
                oMensajes.NoValido sIdioma(6)
            Else
                gParametrosInstalacion.gsPetOfeCarta = gParametrosGenerales.gsPETOFECARTADOT
                g_GuardarParametrosIns = True
            End If
        End If
        
        If gParametrosInstalacion.gsPetOfe = "" Then
            
            If gParametrosGenerales.gsPETOFEDOT = "" Then
                oMensajes.NoValido sIdioma(8)
            Else
                gParametrosInstalacion.gsPetOfe = gParametrosGenerales.gsPETOFEDOT
                g_GuardarParametrosIns = True
            End If
        
        End If
        
        If gParametrosGenerales.giMail <> 0 Then
        
            If gParametrosInstalacion.gsPetOfeMail = "" Then
                If gParametrosGenerales.gsPETOFEMAILDOT = "" Then
                    oMensajes.NoValido sIdioma(7)
                Else
                    gParametrosInstalacion.gsPetOfeMail = gParametrosGenerales.gsPETOFEMAILDOT
                    g_GuardarParametrosIns = True
                End If
            End If
            
        End If
        
        If gParametrosGenerales.giINSTWEB = ConPortal Or gParametrosGenerales.giINSTWEB = conweb Then
            If gParametrosInstalacion.gsPetOfeWeb = "" Then
                If gParametrosGenerales.gsPETOFEWEBDOT = "" Then
                    oMensajes.NoValido sIdioma(6)
                Else
                    gParametrosInstalacion.gsPetOfeWeb = gParametrosGenerales.gsPETOFEWEBDOT
                    g_GuardarParametrosIns = True
                End If
            End If
        End If
    
    End If
    Dim oEmpresas As CEmpresas
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    If InStr(gParametrosInstalacion.gsPetOfeCarta, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsPetOfeMail, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsPetOfeWeb, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsPetOfe, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsPETSUBCARTADOT, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsPETSUBMAILDOT, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsPETSUBWEBDOT, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
        InStr(gParametrosInstalacion.gsPETSUBDOT, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
        'Buscamos la empresa CARPETA_PLANTILLAS
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        sCarpeta = oEmpresas.DevolverCarpetaPlantillaProce_o_Ped(frmOFEPet.sdbcAnyo.Text, frmOFEPet.sdbcGMN1_4Cod.Text, frmOFEPet.sdbcProceCod.Text, 0)
        Set oEmpresas = Nothing
    End If
    If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    
    If Not frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
        txtDOT.Text = Replace(gParametrosInstalacion.gsPetOfeCarta, sMarcaPlantillas, sCarpeta)
        txtMailDot.Text = Replace(gParametrosInstalacion.gsPetOfeMail, sMarcaPlantillas, sCarpeta)
        txtWebDot.Text = Replace(gParametrosInstalacion.gsPetOfeWeb, sMarcaPlantillas, sCarpeta)
        txtForm.Text = Replace(gParametrosInstalacion.gsPetOfe, sMarcaPlantillas, sCarpeta)
    Else
        txtDOT.Text = Replace(gParametrosInstalacion.gsPETSUBCARTADOT, sMarcaPlantillas, sCarpeta)
        txtMailDot.Text = Replace(gParametrosInstalacion.gsPETSUBMAILDOT, sMarcaPlantillas, sCarpeta)
        txtWebDot.Text = Replace(gParametrosInstalacion.gsPETSUBWEBDOT, sMarcaPlantillas, sCarpeta)
        txtForm.Text = Replace(gParametrosInstalacion.gsPETSUBDOT, sMarcaPlantillas, sCarpeta)
    
    
    End If
    WordVersion = GetWordVersion
    Exit Sub

Error:
    
    MsgBox err.Description
    
    
End Sub
Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    On Error Resume Next
    
    If Me.Height - Shape1.Height - Shape2.Height - Shape4.Height - picEdit.Height - 650 > 10 Then
        sdbgPet.Height = Me.Height - Shape1.Height - Shape2.Height - Shape4.Height - picEdit.Height - 650
    End If
    If Width >= 360 Then sdbgPet.Width = Width - 360
    sdbgPet.Groups(0).Width = sdbgPet.Width * 0.4
    sdbgPet.Groups(1).Width = sdbgPet.Width * 0.35
    sdbgPet.Groups(2).Width = sdbgPet.Width * 0.205
    
    cmdAceptar.Left = Me.Width / 2 - 1400
    cmdCancelar.Left = cmdAceptar.Left + 1200

End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim irespuesta As Integer
Dim i As Integer


On Error GoTo Error:
    
    'Borramos los archivos temporales que hayamos creado
    
    g_bCancelarMail = False
    Set m_oOfertasProceso = Nothing
    Set oProvesAsignados = Nothing
    
    i = 0
    While i < UBound(sayFileNames)
        
        If oFos.FileExists(sayFileNames(i)) Then
            oFos.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    
    Set oFos = Nothing
    Me.Visible = False
    Exit Sub

Error:
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
End Sub

''' <summary>
''' CLose up del combo del apellido de contacto
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbddConApe_CloseUp()
   
    If sdbgPet.Columns("CODCON").Value = sdbddConApe.Columns("ID").Value Then
        'Se ha seleccionado el mismo contacto que ya teniamos
        Exit Sub
    End If
    
    If sdbddConApe.Columns("APE").Value <> "" Then
        
        Screen.MousePointer = vbHourglass
        
        If sdbddConApe.Columns("NOM").Value <> "" Then
            sdbgPet.Columns("APE").Value = sdbddConApe.Columns("APE").Value & ", " & sdbddConApe.Columns("NOM").Value
            sdbgPet.Columns("APE").Text = sdbddConApe.Columns("APE").Value & ", " & sdbddConApe.Columns("NOM").Value
        Else
            sdbgPet.Columns("APE").Value = sdbddConApe.Columns("APE").Value
            sdbgPet.Columns("APE").Text = sdbddConApe.Columns("APE").Value
        End If
        sdbgPet.Columns("CODCON").Value = sdbddConApe.Columns("ID").Value
        sdbgPet.Columns("PORTALCON").Value = sdbddConApe.Columns("PORT").Value
        sdbgPet.Columns("TIPOEMAIL").Value = sdbddConApe.Columns("TIPOEMAIL").Value
        
        If sdbddConApe.Columns("APE").Value <> "" Then
        ' contacto con e-mail
            If gParametrosGenerales.giMail = 0 Then
                ' instalacion SIN clte mail
                sdbgPet.Columns("WEB").Value = 0
                sdbgPet.Columns("MAIL").Value = 0
                sdbgPet.Columns("IMP").Value = 1
            Else
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                ' peticion via web para proveedores del portal
                    If NullToStr(oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value).CodPortal) = "" Then
                        sdbgPet.Columns("WEB").Value = 0
                        sdbgPet.Columns("MAIL").Value = 1
                        sdbgPet.Columns("IMP").Value = 0
                    Else
                        If sdbddConApe.Columns("PORT").Value = "1" Then
                            sdbgPet.Columns("WEB").Value = 1
                            sdbgPet.Columns("MAIL").Value = 0
                            sdbgPet.Columns("IMP").Value = 0
                        Else
                            sdbgPet.Columns("WEB").Value = 0
                            sdbgPet.Columns("MAIL").Value = 1
                            sdbgPet.Columns("IMP").Value = 0
                        End If
                    End If
                Else
                    If gParametrosGenerales.giINSTWEB = conweb Then
                        sdbgPet.Columns("WEB").Value = 1
                        sdbgPet.Columns("MAIL").Value = 0
                        sdbgPet.Columns("IMP").Value = 0
                    Else
                        sdbgPet.Columns("WEB").Value = 0
                        sdbgPet.Columns("MAIL").Value = 1
                        sdbgPet.Columns("IMP").Value = 0
                    End If
                End If
            End If
        Else
            sdbgPet.Columns("WEB").Value = 0
            sdbgPet.Columns("MAIL").Value = 0
            sdbgPet.Columns("IMP").Value = 1
        End If
'        sdbgPet.Update
        Screen.MousePointer = vbNormal

    End If
    
End Sub

Private Sub sdbddConApe_DropDown()
Dim scodProve As String

    sdbddConApe.RemoveAll
    
    If sdbgPet.Columns("CODPROVE").Value = "" Then
        sdbddConApe.RemoveAll
        sdbddConApe.AddItem ""
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    
    scodProve = sdbgPet.Columns("CODPROVE").Value
    
    Set oContactos = Nothing
    If sdbgPet.Columns("OFERTADOWEB").Value = "0" And gParametrosGenerales.giINSTWEB = ConPortal And _
       NullToStr(oProvesAsignados.Item(scodProve).CodPortal) <> "" And gParametrosGenerales.gbFUP And (iNumProves > 1) Then    'No HA OFERTADO por Web
        'Cargo solo los del portal
        Set oContactos = oProvesAsignados.Item(scodProve).CargarTodosLosContactos(, , , , True, , , , , , , , , True)
    Else
        Set oContactos = oProvesAsignados.Item(scodProve).CargarTodosLosContactos(, , , , True)
    End If
    
    Screen.MousePointer = vbNormal
        
    CargarGridConContactosApe
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
End Sub


Private Sub sdbddConApe_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddConApe.DataFieldList = "Column 0"
    sdbddConApe.DataFieldToDisplay = "Column 0"
    
End Sub
    


Private Sub CargarGridConProveCod()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oProve As CProveedor
    
    sdbddProveCod.RemoveAll
    
    For Each oProve In oProvesAsignados
        sdbddProveCod.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den & Chr(m_lSeparador) & oProve.CodPortal & Chr(m_lSeparador) & IIf(oProve.EsPremium, "1", "0") & Chr(m_lSeparador) & IIf(oProve.EsPremiumActivo, "1", "0")
    Next
    
    If sdbddProveCod.Rows = 0 Then
        sdbddProveCod.AddItem ""
    End If
    
    sdbddProveCod.MoveFirst
    
End Sub
Private Sub CargarGridConProveDen()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oProve As CProveedor
    
    sdbddProveDen.RemoveAll
    
    For Each oProve In oProvesAsignados
        sdbddProveDen.AddItem oProve.Den & Chr(m_lSeparador) & oProve.Cod & Chr(m_lSeparador) & oProve.CodPortal & Chr(m_lSeparador) & IIf(oProve.EsPremium, "1", "0") & Chr(m_lSeparador) & IIf(oProve.EsPremiumActivo, "1", "0")
    Next
    
    If sdbddProveDen.Rows = 0 Then
        sdbddProveDen.AddItem ""
    End If
    
    sdbddProveDen.MoveFirst
    
End Sub

''' <summary>
''' * Objetivo: Cargar combo con la coleccion de Contactos
''' </summary>
''' <remarks>Llamada desde: sdbddConApe_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConContactosApe()
    
    Dim oCon As CContacto
    
    sdbddConApe.RemoveAll
    
    For Each oCon In oContactos
        sdbddConApe.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & IIf(oCon.Port, "1", "0") & Chr(m_lSeparador) & oCon.TipoEmail
    Next
    
    If sdbddConApe.Rows = 0 Then
        sdbddConApe.AddItem ""
    End If
    
    sdbddConApe.MoveFirst
    
End Sub



Private Sub sdbddConApe_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    Dim sContacto As String

    On Error Resume Next
    sContacto = ""
    sdbddConApe.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddConApe.Rows - 1
            bm = sdbddConApe.GetBookmark(i)
            If sdbddConApe.Columns(1).CellText(bm) = "" Then
                sContacto = sdbddConApe.Columns(0).CellText(bm)
            Else
                sContacto = sdbddConApe.Columns(0).CellText(bm) & ", " & sdbddConApe.Columns(1).CellText(bm)
            End If
            If UCase(Text) = UCase(Mid(sContacto, 1, Len(Text))) Then
                sdbddConApe.Bookmark = bm
                Exit For
            End If
        Next i
    End If


End Sub


Private Sub sdbddConApe_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
    If sdbddConApe.Columns("PORT").Value = "1" Then
        For i = 0 To sdbddConApe.Cols - 1
            sdbddConApe.Columns(i).CellStyleSet "Tan"
        Next
    End If
End Sub


Private Sub sdbddProveCod_CloseUp()
    
    If sdbddProveCod.Columns("COD").Value <> "" Then
        sdbgPet.Columns("CODPROVE").Value = sdbddProveCod.Columns("COD").Value
        sdbgPet.Columns("CODPROVE").Text = sdbddProveCod.Columns("COD").Value
        sdbgPet.Columns("DENPROVE").Value = sdbddProveCod.Columns("DEN").Value
        sdbgPet.Columns("DENPROVE").Text = sdbddProveCod.Columns("DEN").Value
        sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(sdbddProveCod.Columns("COD").Value), "1", "0")
    End If
   
End Sub

Private Sub sdbddProveCod_RowLoaded(ByVal Bookmark As Variant)
    If oProvesAsignados Is Nothing Then Exit Sub
    If oProvesAsignados.Item(sdbddProveCod.Columns("COD").Value) Is Nothing Then Exit Sub
    If NullToStr(oProvesAsignados.Item(sdbddProveCod.Columns("COD").Value).CodPortal) <> "" And _
      gParametrosGenerales.giINSTWEB = ConPortal Then
        sdbddProveCod.Columns("COD").CellStyleSet "ProvPortal"
        sdbddProveCod.Columns("DEN").CellStyleSet "ProvPortal"
    Else
        sdbddProveCod.Columns("COD").CellStyleSet "Normal"
        sdbddProveCod.Columns("DEN").CellStyleSet "Normal"
    End If
End Sub

Private Sub sdbddProveDen_CloseUp()
    
    If sdbddProveDen.Columns("DEN").Value <> "" Then
        sdbgPet.Columns("CODPROVE").Value = sdbddProveDen.Columns("COD").Value
        sdbgPet.Columns("CODPROVE").Text = sdbddProveDen.Columns("COD").Value
        sdbgPet.Columns("DENPROVE").Value = sdbddProveDen.Columns("DEN").Value
        sdbgPet.Columns("DENPROVE").Text = sdbddProveDen.Columns("DEN").Value
        sdbgPet_Change
        sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(sdbddProveDen.Columns("COD").Value), "1", "0")

    End If
   
End Sub

Private Sub sdbddProveDen_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveDen.DataFieldList = "Column 0"
    sdbddProveDen.DataFieldToDisplay = "Column 0"
End Sub



Private Sub sdbddProveDen_RowLoaded(ByVal Bookmark As Variant)
    If oProvesAsignados Is Nothing Then Exit Sub
    If oProvesAsignados.Item(sdbddProveDen.Columns("COD").Value) Is Nothing Then Exit Sub
    If NullToStr(oProvesAsignados.Item(sdbddProveDen.Columns("COD").Value).CodPortal) <> "" And _
      gParametrosGenerales.giINSTWEB = ConPortal Then
        sdbddProveDen.Columns("COD").CellStyleSet "ProvPortal"
        sdbddProveDen.Columns("DEN").CellStyleSet "ProvPortal"
    Else
        sdbddProveDen.Columns("COD").CellStyleSet "Normal"
        sdbddProveDen.Columns("DEN").CellStyleSet "Normal"
    End If
End Sub

Private Sub sdbgPet_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
Dim vbm As Variant
Dim oPets As CPetOfertas
Dim oIPet As IPeticiones
Dim inumitem As Integer
Dim oProve As CProveedor


    DispPromptMsg = 0
    
        'Si es premium activo y no se ha comunicado, no borrar la fila
    If sdbgPet.Columns("CODPROVE").Value <> "" Then
        Set oProve = oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value)
        If gParametrosGenerales.giINSTWEB = ConPortal And NullToStr(oProve.CodPortal) <> "" And oProve.EsPremium = True And oProve.EsPremiumActivo = True Then
            'Miramos si se ha hecho alguna comunicacion
            Set oIPet = frmOFEPet.oProcesoSeleccionado
            Set oPets = oIPet.DevolverPeticiones(OrdPorEMail, , , , sdbgPet.Columns("CODPROVE").Value)
            If oPets.Count = 0 Then
            ''Comprobamos si hay otra fila del mismo prove
                inumitem = 0
                Do While inumitem < sdbgPet.Rows
                    vbm = sdbgPet.AddItemBookmark(inumitem)
                    If sdbgPet.Columns("CODPROVE").CellValue(vbm) = oProve.Cod And sdbgPet.Row <> inumitem Then
                        Exit Do
                    End If
                    inumitem = inumitem + 1
                Loop
                If inumitem = sdbgPet.Rows Or (inumitem < sdbgPet.Rows And sdbgPet.Columns("CODPROVE").CellValue(vbm) <> oProve.Cod) Then
                   oMensajes.PremiumComunicacionObligatoria
                   Set oPets = Nothing
                   Set oIPet = Nothing
                   Set oProve = Nothing
                   Cancel = True
                   Exit Sub
                End If
            End If
            Set oPets = Nothing
            Set oIPet = Nothing
        End If
        Set oProve = Nothing
    End If
End Sub


Private Sub sdbgPet_Change()
Dim oPets As CPetOfertas
Dim oIPet As IPeticiones
Dim inumitem As Integer
Dim vbm As Variant
Dim oProve As CProveedor
'Dim oProves As CProveedores

    If sdbgPet.Columns("CODPROVE").Value = "" Then
        Exit Sub
    Else
        Set oProve = oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value)
        If oProve Is Nothing Then Exit Sub
        If oProve.Contactos Is Nothing Then
            oProve.CargarTodosLosContactos
        End If
    End If
    
    Select Case sdbgPet.col
    
    Case 0, 1
        'Si es premium activo y no se ha comunicado, no cambiar la fila
        If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And NullToStr(oProve.CodPortal) <> "" And oProve.EsPremium = True And oProve.EsPremiumActivo = True Then
        
            'Miramos si se ha hecho alguna comunicacion
            Set oIPet = frmOFEPet.oProcesoSeleccionado
            Set oPets = oIPet.DevolverPeticiones(OrdPorEMail, , , , oProve.Cod)
            If oPets.Count = 0 Then
            ''Comprobamos si hay otra fila del mismo prove
                
                inumitem = 0
                Do While inumitem < sdbgPet.Rows
                    vbm = sdbgPet.AddItemBookmark(inumitem)
                    If sdbgPet.Columns("CODPROVE").CellValue(vbm) = oProve.Cod And sdbgPet.Row <> inumitem Then
                        Exit Do
                    End If
                    inumitem = inumitem + 1
                Loop
                If inumitem = sdbgPet.Rows And sdbgPet.Columns("CODPROVE").CellValue(vbm) <> oProve.Cod Then
                   oMensajes.PremiumComunicacionObligatoria
                   Set oPets = Nothing
                   Set oIPet = Nothing
                   Set oProve = Nothing
                   sdbgPet.CancelUpdate
                   sdbgPet.DataChanged = False
                   Exit Sub
                End If
            End If
            Set oPets = Nothing
            Set oIPet = Nothing
        End If
        
        sdbgPet.Columns("APE").Value = ""
        sdbgPet.Columns("WEB").Value = 0
        sdbgPet.Columns("MAIL").Value = 0
        sdbgPet.Columns("IMP").Value = 0
        sdbgPet.Columns("CODCON").Value = ""
        sdbgPet.Columns("PORTALCON").Value = ""
        
    Case 3
        

        If sdbgPet.Columns("WEB").Value = "1" Or sdbgPet.Columns("WEB").Value = "-1" Then
            If Not bModificarFecLimit And IsDate(lblFecLimit) Then
                If CDate(lblFecLimit) < Date Then
                    oMensajes.NoValida sIdioma(1)
                    sdbgPet.CancelUpdate
                    sdbgPet.DataChanged = False
                    Set oProve = Nothing
                    Exit Sub
                End If
            End If
            If Not oProve.Contactos.Item(sdbgPet.Columns("CODCON").Value) Is Nothing Then
                If NullToStr(oProve.Contactos.Item(sdbgPet.Columns("CODCON").Value).mail) = "" Then
                    oMensajes.NoValido sIdioma(19)
                    sdbgPet.CancelUpdate
                    sdbgPet.DataChanged = False
                    Set oProve = Nothing
                    Exit Sub
                End If
            End If
            sdbgPet.Columns("MAIL").Value = 0
            sdbgPet.Columns("IMP").Value = 0
            
        Else
            If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbFUP And (iNumProves > 1) And NullToStr(oProve.CodPortal) <> "" And sdbgPet.Columns("OFERTADOWEB").Value = "0" Then
                oMensajes.FUPComunicacionObligatoria
                sdbgPet.CancelUpdate
                sdbgPet.DataChanged = False
                Set oProve = Nothing
                Exit Sub
            End If
        End If

    Case 4
        
        
        If sdbgPet.Columns("MAIL").Value = "-1" Or sdbgPet.Columns("MAIL").Value = "1" Then
            If sdbgPet.Columns("WEB").Value <> 0 Then
                If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbFUP And (iNumProves > 1) And NullToStr(oProve.CodPortal) <> "" And sdbgPet.Columns("OFERTADOWEB").Value = "0" Then
                    oMensajes.FUPComunicacionObligatoria
                    sdbgPet.CancelUpdate
                    sdbgPet.DataChanged = False
                    Set oProve = Nothing
                    Exit Sub
                End If
            End If
            If Not oProve.Contactos.Item("CODCON") Is Nothing Then
                If NullToStr(oProve.Contactos.Item("CODCON").mail) = "" Then
                    oMensajes.NoValido sIdioma(19)
                    sdbgPet.CancelUpdate
                    sdbgPet.DataChanged = False
                    Set oProve = Nothing
                    Exit Sub
                End If
            End If
            sdbgPet.Columns("WEB").Value = 0
            sdbgPet.Columns("IMP").Value = 0
        End If

    Case 5
        
        
        If sdbgPet.Columns("IMP").Value = "1" Or sdbgPet.Columns("IMP").Value = "-1" Then
            
            If sdbgPet.Columns("WEB").Value <> 0 Then
                If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbFUP And (iNumProves > 1) And NullToStr(oProve.CodPortal) <> "" And sdbgPet.Columns("OFERTADOWEB").Value = "0" Then
                    oMensajes.FUPComunicacionObligatoria
                    sdbgPet.CancelUpdate
                    sdbgPet.DataChanged = False
                    Set oProve = Nothing
                    Exit Sub
                End If
            End If
            sdbgPet.Columns("MAIL").Value = 0
            sdbgPet.Columns("WEB").Value = 0
        End If
        

    End Select
    
    'Comprobar el contacto de portal y su email si comunicacion web
    If sdbgPet.Columns("APE").Value <> "" Then

        If gParametrosGenerales.giINSTWEB = ConPortal And sdbgPet.Columns("WEB").Value <> 0 Then
            If NullToStr(oProve.CodPortal) = "" Then
                oMensajes.ProvePortalDesautorizadoEnCia
                sdbgPet.CancelUpdate
                sdbgPet.DataChanged = False
                Set oProve = Nothing
                Exit Sub
            End If
            If Not oProve.Contactos.Item(sdbgPet.Columns("CODCON").Value) Is Nothing Then
                If oProve.Contactos.Item(sdbgPet.Columns("CODCON").Value).Port = False Then
                    oMensajes.ContactoWebNoValido
                    sdbgPet.CancelUpdate
                    sdbgPet.DataChanged = False
                    Set oProve = Nothing
                    Exit Sub
                End If
            End If
        End If
    End If
    Set oProve = Nothing
   
End Sub

Private Sub sdbgPet_InitColumnProps()
    
    sdbgPet.Columns("APE").Locked = True
    'sdbgPet.Columns(3).Locked = True
    
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        sdbgPet.Columns("WEB").Locked = True
    End If
    
    If gParametrosGenerales.giMail = 0 Then
        sdbgPet.Columns("WEB").Locked = True
        sdbgPet.Columns("MAIL").Locked = True
    End If
    
End Sub
Private Sub sdbddProveCod_DropDown()

    Screen.MousePointer = vbHourglass
    
    
    If frmOFEPet.bComunAsigComp Then
    
        If bCargarComboDesdeDD Then
            Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            Set oProves = oProvesAsignados
        End If
    Else
    
        If frmOFEPet.bComunAsigEqp Then
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", , OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oProvesAsignados
            End If
        Else
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "")
            Else
                Set oProves = oProvesAsignados
            End If
        End If
    
    End If
        
    CargarGridConProveCod
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveCod_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveCod.DataFieldList = "Column 0"
    sdbddProveCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddProveCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveCod.Rows - 1
            bm = sdbddProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddProveCod_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        sdbgPet.Columns("CODPROVE").Value = ""
        sdbgPet.Columns("CODPROVE").Text = ""
        sdbgPet.Columns("DENPROVE").Value = ""
        sdbgPet.Columns("DENPROVE").Text = ""
        sdbgPet.Columns("OFERTADOWEB").Value = ""
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddProveCod.Columns("COD").Value = Text Then
            RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = sdbddProveCod.Columns("COD").Value
            sdbgPet.Columns("CODPROVE").Text = sdbddProveCod.Columns("COD").Value
            sdbgPet.Columns("DENPROVE").Value = sdbddProveCod.Columns("DEN").Value
            sdbgPet.Columns("DENPROVE").Text = sdbddProveCod.Columns("DEN").Value
            sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(sdbddProveCod.Columns("COD").Value), "1", "0")
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        If frmOFEPet.bComunAsigComp Then
            Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If frmOFEPet.bComunAsigEqp Then
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde(sdbgPet.ActiveCell.Value, "", True)
            End If
        End If
        
        If Not oProves.Item(1) Is Nothing Then
            RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = oProves.Item(1).Cod
            sdbgPet.Columns("CODPROVE").Text = oProves.Item(1).Cod
            sdbgPet.Columns("DENPROVE").Value = oProves.Item(1).Den
            sdbgPet.Columns("DENPROVE").Text = oProves.Item(1).Den
            sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(oProves.Item(1).Cod), "1", "0")
        Else
            sdbgPet.Columns("CODPROVE").Value = ""
            sdbgPet.Columns("CODPROVE").Text = ""
            sdbgPet.Columns("DENPROVE").Value = ""
            sdbgPet.Columns("DENPROVE").Text = ""
            sdbgPet.Columns("OFERTADOWEB").Value = ""
        End If
    
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveDen_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    
    Screen.MousePointer = vbHourglass
    
    If frmOFEPet.bComunAsigComp Then
    
        If bCargarComboDesdeDD Then
            Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            Set oProves = oProvesAsignados
        End If
    Else
    
        If frmOFEPet.bComunAsigEqp Then
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oProvesAsignados
            End If
        Else
            If bCargarComboDesdeDD Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, , OrdAsigPorDenProve)
            Else
                Set oProves = oProvesAsignados
            End If
        End If
    
    End If
        
    CargarGridConProveDen
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveDen.Rows - 1
            bm = sdbddProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddProveDen_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = ""
            sdbgPet.Columns("CODPROVE").Text = ""
            sdbgPet.Columns("DENPROVE").Value = ""
            sdbgPet.Columns("DENPROVE").Text = ""
            sdbgPet.Columns("OFERTADOWEB").Value = ""
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddProveDen.Columns(0).Value = Text Then
            RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = sdbddProveDen.Columns("COD").Value
            sdbgPet.Columns("CODPROVE").Value = sdbddProveDen.Columns("COD").Value
            sdbgPet.Columns("DENPROVE").Value = sdbddProveDen.Columns("DEN").Value
            sdbgPet.Columns("DENPROVE").Value = sdbddProveDen.Columns("DEN").Value
            sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(sdbddProveDen.Columns("COD").Value), "1", "0")

            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        If frmOFEPet.bComunAsigComp Then
            Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        Else
            If frmOFEPet.bComunAsigEqp Then
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProves = oIasig.DevolverProveedoresDesde("", sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve)
            End If
        End If
        
        If Not oProves.Item(1) Is Nothing Then
            RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = oProves.Item(1).Cod
            sdbgPet.Columns("CODPROVE").Text = oProves.Item(1).Cod
            sdbgPet.Columns("DENPROVE").Value = oProves.Item(1).Den
            sdbgPet.Columns("DENPROVE").Text = oProves.Item(1).Den
            sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(oProves.Item(1).Cod), "1", "0")
        Else
            sdbgPet.Columns("CODPROVE").Value = ""
            sdbgPet.Columns("CODPROVE").Text = ""
            sdbgPet.Columns("DENPROVE").Value = ""
            sdbgPet.Columns("DENPROVE").Text = ""
            sdbgPet.Columns("OFERTADOWEB").Value = ""
        End If
    
    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Genera el documento word para la comunicaci�n de ofertas</summary>
''' <param name="oPet">Oferta</param>
''' <param name="appword">Objeto aplicaci�n word</param>
''' <returns>Objeto documento word</returns>
''' <remarks>Llamada desde;cmdAceptar_Click Tiempo m�ximo 0</remarks>
''' <revision>LTG 29/11/2011</revision>

Private Function ImpresoOferta(ByVal oPet As CPetOferta) As Object
    Dim i As Integer
    Dim docword As Object
    Dim blankword As Object
    Dim rangeword As Object
    Dim rangewordGR As Object
    Dim rangewordAtr As Object
    Dim rangewordAtrGR As Object
    Dim rangewordIT  As Object
    Dim rangewordArt As Object
    Dim rangewordITAtr  As Object
    Dim rangewordITEsp As Object
    Dim rangewordFic As Object
    Dim rangewordOptIt As Object
    Dim oItem As CItem
    Dim iNumEsp As Integer
    Dim iNumPago As Integer
    Dim INUMDEST As Integer
    Dim ayDest() As String
    Dim ayPago() As String
    Dim iNumFich As Integer
    Dim oEsp As CEspecificacion
    Dim oatrib As CAtributo
    Dim oGrupo As CGrupo
    Dim oProce As CProceso
    Dim oEsc As CEscalado
    Dim iNumAtr As Integer
    Dim cont As Integer
    Dim vEspec() As Variant
    Dim vFic() As Variant
    Dim iNumFic As Integer
    Dim j As Integer
    Dim Destino As Boolean
    Dim Pago As Boolean
    Dim Fecha As Boolean
    Dim numItem As Integer
    Dim oElem As CValorPond
    Dim var1 As Variant
    Dim var2 As Variant
    Dim iTabla As Integer
    Dim oProves As CProveedores
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim intcontItems As Integer
    Dim sDenGrupo As String
    Dim rangewordAtrEspePRO As Object
    Dim rangewordAtrEspeGR As Object
    Dim rangewordITEspeAtr As Object
    
        
    If txtForm.Text = "" Then
        oMensajes.NoValido sIdioma(8)
        Set ImpresoOferta = Nothing
        Exit Function
    End If
            
    If Right(txtForm.Text, 3) <> "dot" Then
        oMensajes.NoValido sIdioma(2) & ": " & txtForm.Text
        Set ImpresoOferta = Nothing
        Exit Function
    End If
        
    If Not oFos.FileExists(txtForm.Text) Then
        oMensajes.PlantillaNoEncontrada txtForm.Text
        Set ImpresoOferta = Nothing
        Exit Function
    End If
    
    ' CARGAR DATOS GENERALES DE PROVEEDOR
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.Add oPet.CodProve, oPet.DenProve
    oProves.CargarDatosProveedor oPet.CodProve
    
            
    Set docword = appword.Documents.Add(txtForm.Text)
    'appword.Visible = True
    
    If m_iWordVer >= 9 Then
        'appword.Windows(docword.Name).WindowState = 2
        If appword.Visible Then docword.activewindow.WindowState = 2
    Else
        appword.Visible = True
        appword.Windows(docword.Name).WindowState = 1
    End If
             
    INUMDEST = 0
    ReDim ayDest(0)
    iNumPago = 0
    ReDim ayPago(0)
    
    Set blankword = appword.Documents.Add(txtForm.Text) '''@@@jul
    If m_iWordVer >= 9 Then
        If appword.Visible Then docword.activewindow.WindowState = 2
    Else
        appword.Windows(blankword.Name).WindowState = 1
    End If
    
    'Set rangeword = midocword.Range
    'rangeword.Copy
    
    With docword
        '.Bookmarks("\StartOfDoc").Select
        '.Application.Selection.Paste
        ' DATOS PROVEEDORES
        If .Bookmarks.Exists("COD_PROVE") Then
            DatoAWord docword, "COD_PROVE", oProves.Item(1).Cod
        End If
        If .Bookmarks.Exists("DEN_PROVE") Then
            DatoAWord docword, "DEN_PROVE", oProves.Item(1).Den
        End If
        If .Bookmarks.Exists("DIR_PROVE") Then
            DatoAWord docword, "DIR_PROVE", oProves.Item(1).Direccion
        End If
        If .Bookmarks.Exists("POBL_PROVE") Then
            DatoAWord docword, "POBL_PROVE", oProves.Item(1).Poblacion
        End If
        If .Bookmarks.Exists("CP_PROVE") Then
            DatoAWord docword, "CP_PROVE", oProves.Item(1).cP
        End If
        If .Bookmarks.Exists("PROV_PROVE") Then
            DatoAWord docword, "PROV_PROVE", oProves.Item(1).DenProvi
        End If
        If .Bookmarks.Exists("PAIS_PROVE") Then
            DatoAWord docword, "PAIS_PROVE", oProves.Item(1).DenPais
        End If
        If .Bookmarks.Exists("NIF_PROVE") Then
            DatoAWord docword, "NIF_PROVE", NullToStr(oProves.Item(1).NIF)
        End If
        ' DATOS PERSONA CONTACTO
        If .Bookmarks.Exists("NOM_CONTACTO") Then
            DatoAWord docword, "NOM_CONTACTO", oPet.nombre
        End If
        If .Bookmarks.Exists("APE_CONTACTO") Then
            DatoAWord docword, "APE_CONTACTO", oPet.Apellidos
        End If
        If .Bookmarks.Exists("TFNO_CONTACTO") Then
            DatoAWord docword, "TFNO_CONTACTO", NullToStr(oPet.Tfno)
        End If
        If .Bookmarks.Exists("FAX_CONTACTO") Then
            DatoAWord docword, "FAX_CONTACTO", NullToStr(oPet.Fax)
        End If
        If .Bookmarks.Exists("MAIL_CONTACTO") Then
            DatoAWord docword, "MAIL_CONTACTO", NullToStr(oPet.Email)
        End If
        If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
            DatoAWord docword, "TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil)
        End If
        'DATOS PROCESO
        If .Bookmarks.Exists("NUM_PROCESO") Then
            DatoAWord docword, "NUM_PROCESO", frmOFEPet.sdbcAnyo.Text & "/" & frmOFEPet.sdbcGMN1_4Cod.Text & "/" & frmOFEPet.sdbcProceCod.Text
        End If
        If .Bookmarks.Exists("DEN_PROCESO") Then
            DatoAWord docword, "DEN_PROCESO", frmOFEPet.sdbcProceDen.Text & " "
        End If
        If .Bookmarks.Exists("MAT1_PROCESO") Then
            DatoAWord docword, "MAT1_PROCESO", frmOFEPet.oProcesoSeleccionado.GMN1Cod
        End If
        If .Bookmarks.Exists("FEC_LIMITE") Then
            'DatoAWord docword, "FEC_LIMITE", txtFecLimOfe.Text & " " & Trim(txtHoraLimite.Text)
            DatoAWord docword, "FEC_LIMITE", m_vDatoAPasar
        End If
        If .Bookmarks.Exists("FECHAINISUB") Then
            DatoAWord docword, "FECHAINISUB", m_sFecIniSub
        End If
        
        'DESTINO DE PROCESO
        If frmOFEPet.oProcesoSeleccionado.DefDestino = EnProceso Then
            ayDest(INUMDEST) = frmOFEPet.oProcesoSeleccionado.DestCod
            If .Bookmarks.Exists("COD_DEST_PROCE") Then
                .Bookmarks("COD_DEST_PROCE").Range.Text = ayDest(INUMDEST)
            End If
            If .Bookmarks.Exists("DEN_DEST_PROCE") Then
                .Bookmarks("DEN_DEST_PROCE").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            If .Bookmarks.Exists("DESTINO") Then
                If .Bookmarks.Exists("DEST_COD") Then .Bookmarks("DEST_COD").Range.Text = ayDest(INUMDEST)
                If .Bookmarks.Exists("DEST_DIR") Then .Bookmarks("DEST_DIR").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).dir)
                If .Bookmarks.Exists("DEST_POB") Then .Bookmarks("DEST_POB").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).POB)
                If .Bookmarks.Exists("DEST_CP") Then .Bookmarks("DEST_CP").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).cP)
                If .Bookmarks.Exists("DEST_DEN") Then .Bookmarks("DEST_DEN").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                If .Bookmarks.Exists("DEST_PROV") Then .Bookmarks("DEST_PROV").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Provi)
                If .Bookmarks.Exists("DEST_PAIS") Then .Bookmarks("DEST_PAIS").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Pais)
                .Bookmarks("DESTINO").Delete
            End If
            If .Bookmarks.Exists("PROCE_DEST") Then .Bookmarks("PROCE_DEST").Delete
        Else
            If .Bookmarks.Exists("PROCE_DEST") Then
                .Bookmarks("PROCE_DEST").Select
                .Application.Selection.Delete
            End If
        End If
        'FORMA PAGO EN PROCESO
        If frmOFEPet.oProcesoSeleccionado.DefFormaPago = EnProceso Then
            If .Bookmarks.Exists("COD_PAGO_PROCE") Then
                .Bookmarks("COD_PAGO_PROCE").Range.Text = frmOFEPet.oProcesoSeleccionado.PagCod
            End If
            If .Bookmarks.Exists("DEN_PAGO_PROCE") Then
                .Bookmarks("DEN_PAGO_PROCE").Range.Text = NullToStr(frmOFEPet.oPagos.Item(frmOFEPet.oProcesoSeleccionado.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            If .Bookmarks.Exists("PAGO") Then
                .Bookmarks("PAGO").Select
                .Application.Selection.cut
            End If
            If .Bookmarks.Exists("PROCE_PAGO") Then .Bookmarks("PROCE_PAGO").Delete
        Else
            If .Bookmarks.Exists("PROCE_PAGO") Then
                .Bookmarks("PROCE_PAGO").Select
                .Application.Selection.cut
            End If
        End If
        'FECHAS SUM EN PROCESO
        If frmOFEPet.oProcesoSeleccionado.DefFechasSum = EnProceso Then
            If .Bookmarks.Exists("FINI_SUM_PROCE") Then
                .Bookmarks("FINI_SUM_PROCE").Range.Text = Format(NullToStr(frmOFEPet.oProcesoSeleccionado.FechaInicioSuministro), "Short Date")
            End If
            If .Bookmarks.Exists("FFIN_SUM_PROCE") Then
                .Bookmarks("FFIN_SUM_PROCE").Range.Text = Format(NullToStr(frmOFEPet.oProcesoSeleccionado.FechaFinSuministro), "Short Date")
            End If
            If .Bookmarks.Exists("PROCE_FECSUM") Then .Bookmarks("PROCE_FECSUM").Delete
        Else
            If .Bookmarks.Exists("PROCE_FECSUM") Then
                .Bookmarks("PROCE_FECSUM").Select
                .Application.Selection.cut
            End If
        End If
                
        
        frmOFEPet.oProcesoSeleccionado.CargarAtributos frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, True, AmbProceso, , True
        'ATRIBUTOS DE PROCESO
        If docword.Bookmarks.Exists("ATRIBUTO") Then
            
            If frmOFEPet.oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
                If .Bookmarks.Exists("PROCE_ATRIBUTOS") Then
                    .Bookmarks("PROCE_ATRIBUTOS").Select
                    .Application.Selection.cut
                End If
            Else
                docword.Bookmarks("ATRIBUTO").Select
                
                Set rangewordAtr = blankword.Bookmarks("ATRIBUTO").Range
                rangewordAtr.Copy
                docword.Application.Selection.cut
            
            
                For Each oatrib In frmOFEPet.oProcesoSeleccionado.ATRIBUTOS
    
                    docword.Application.Selection.Paste
                    If .Bookmarks.Exists("COD_ATR_PROCE") Then
                        .Bookmarks("COD_ATR_PROCE").Range.Text = NullToStr(oatrib.Cod)
                    End If
                    If .Bookmarks.Exists("DEN_ATR_PROCE") Then
                        .Bookmarks("DEN_ATR_PROCE").Range.Text = NullToStr(oatrib.Den)
                    End If
                    If .Bookmarks.Exists("TIPO_ATR_PROCE") Then
                        Select Case oatrib.Tipo
                            Case 1:
                                .Bookmarks("TIPO_ATR_PROCE").Range.Text = NullToStr(m_stxtTexto)
                            Case 2:
                                .Bookmarks("TIPO_ATR_PROCE").Range.Text = NullToStr(m_stxtNumero)
                            Case 3:
                                .Bookmarks("TIPO_ATR_PROCE").Range.Text = NullToStr(m_stxtFecha)
                            Case 4:
                                .Bookmarks("TIPO_ATR_PROCE").Range.Text = NullToStr(m_stxtSi)
    
                        End Select
    
                    End If
                    If .Bookmarks.Exists("VALOR_ATR_PROCE") Then
                        
                        If oatrib.TipoIntroduccion = IntroLibre Then
                            If Not IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
                                var1 = m_stxtMinimo & " : " & oatrib.Minimo & vbCrLf & m_stxtMaximo & " : " & oatrib.Maximo
                                .Bookmarks("VALOR_ATR_PROCE").Range.Text = var1
                            ElseIf Not IsNull(oatrib.Minimo) Then
                                var1 = m_stxtMinimo & " : " & oatrib.Minimo
                                .Bookmarks("VALOR_ATR_PROCE").Range.Text = var1
                            ElseIf Not IsNull(oatrib.Maximo) Then
                                var1 = m_stxtMaximo & " : " & oatrib.Maximo
                                .Bookmarks("VALOR_ATR_PROCE").Range.Text = var1
                            End If
                        End If
                        If oatrib.TipoIntroduccion = Introselec Then
                            var1 = ""
                            For Each oElem In oatrib.ListaPonderacion
                                var1 = var1 & "#@@# " & CStr(oElem.ValorLista) & vbCrLf
                            Next
                            If var1 <> "" Then
                                .Bookmarks("VALOR_ATR_PROCE").Range.Text = var1
                            End If
                        End If
                        If .Bookmarks.Exists("VALOR_ATR_PROCE") Then .Bookmarks("VALOR_ATR_PROCE").Delete
                    
                    End If
                    
                    If .Bookmarks.Exists("OP_ATR_PROCE") Then
                        .Bookmarks("OP_ATR_PROCE").Range.Text = NullToStr(oatrib.PrecioFormula)
                        If .Bookmarks.Exists("OP_ATR_PROCE") Then .Bookmarks("OP_ATR_PROCE").Delete
                    End If
                    If .Bookmarks.Exists("APL_ATR_PROCE") Then
                        Select Case oatrib.PrecioAplicarA
                        Case 0:
                            .Bookmarks("APL_ATR_PROCE").Range.Text = NullToStr(m_stxtOferta)
                        Case 1:
                            .Bookmarks("APL_ATR_PROCE").Range.Text = NullToStr(m_stxtGrupo)
                        Case 2:
                            .Bookmarks("APL_ATR_PROCE").Range.Text = NullToStr(m_stxtItem)
                        Case 3:
                            .Bookmarks("APL_ATR_PROCE").Range.Text = NullToStr(m_stxtUniItem)
                        End Select
                        If .Bookmarks.Exists("APL_ATR_PROCE") Then .Bookmarks("APL_ATR_PROCE").Delete
                    End If
                    
                Next
    
            End If
        End If 'Atributo
        
        'GRUPOS
        numItem = 1
        If .Bookmarks.Exists("GRUPO") Then
            .Bookmarks("GRUPO").Select
            Set rangewordGR = blankword.Bookmarks("GRUPO").Range
        End If
    
        If frmOFEPet.oProcesoSeleccionado.Grupos.Count = 0 Then
            If .Bookmarks.Exists("GRUPO") Then
                .Bookmarks("GRUPO").Select
                .Application.Selection.cut
            End If
        Else
            scodProve = oProves.Item(1).Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProves.Item(1).Cod))
            For Each oGrupo In frmOFEPet.oProcesoSeleccionado.Grupos
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                
                If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
                If oGrupo.UsarEscalados Then oGrupo.CargarEscalados
                
                If gParametrosGenerales.gbMultiMaterial And bPubMatProve Then
                    oGrupo.CargarTodosLosItems OrdItemPorOrden, bPubMatProve:=bPubMatProve, CodProvePet:=oPet.CodProve
                Else
                    oGrupo.CargarTodosLosItems OrdItemPorOrden
                End If
                rangewordGR.Copy
                .Application.Selection.Paste
                If .Bookmarks.Exists("COD_GRUPO") Then
                    If IsNull(oGrupo.Sobre) Then
                        .Bookmarks("COD_GRUPO").Range.Text = NullToStr(oGrupo.Codigo)
                    Else
                        .Bookmarks("COD_GRUPO").Range.Text = "(" & oGrupo.Sobre & ") " & NullToStr(oGrupo.Codigo)
                    End If
                End If
                If .Bookmarks.Exists("DEN_GRUPO") Then
                    .Bookmarks("DEN_GRUPO").Range.Text = NullToStr(oGrupo.Den)
                End If
                
                If oGrupo.DefDestino Then
                    If .Bookmarks.Exists("COD_DEST_GR") Then
                        .Bookmarks("COD_DEST_GR").Range.Text = NullToStr(oGrupo.DestCod)
                    End If
                    If .Bookmarks.Exists("DEN_DEST_GR") Then
                        .Bookmarks("DEN_DEST_GR").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                    End If
                    If .Bookmarks.Exists("DESTINO") Then
                        If Not BuscarEnArray(ayDest, oGrupo.DestCod) Then
                            ayDest(INUMDEST) = oGrupo.DestCod
                            INUMDEST = INUMDEST + 1
                            ReDim Preserve ayDest(UBound(ayDest) + 1)
                        End If
                    End If
                    If .Bookmarks.Exists("GR_DEST") Then .Bookmarks("GR_DEST").Delete
                Else
                    If .Bookmarks.Exists("GR_DEST") Then
                        .Bookmarks("GR_DEST").Select
                        .Application.Selection.cut
                    End If
                End If
                If oGrupo.DefFormaPago Then
                    If .Bookmarks.Exists("COD_PAGO_GR") Then
                        .Bookmarks("COD_PAGO_GR").Range.Text = NullToStr(oGrupo.PagCod)
                    End If
                    If .Bookmarks.Exists("DEN_PAGO_GR") Then
                        .Bookmarks("DEN_PAGO_GR").Range.Text = NullToStr(frmOFEPet.oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                    End If
                    If .Bookmarks.Exists("PAGO") Then
                        If Not BuscarEnArray(ayPago, oGrupo.PagCod) Then
                            ayPago(iNumPago) = oGrupo.PagCod
                            iNumPago = iNumPago + 1
                            ReDim Preserve ayPago(UBound(ayPago) + 1)
                        End If
                    End If
                    If .Bookmarks.Exists("GR_PAGO") Then .Bookmarks("GR_PAGO").Delete
                Else
                    If .Bookmarks.Exists("GR_PAGO") Then
                        .Bookmarks("GR_PAGO").Select
                        .Application.Selection.cut
                    End If
                End If
                If oGrupo.DefFechasSum Then
                    If .Bookmarks.Exists("FINI_SUM_GR") Then
                        .Bookmarks("FINI_SUM_GR").Range.Text = Format(NullToStr(oGrupo.FechaInicioSuministro), "Short Date")
                    End If
                    If .Bookmarks.Exists("FFIN_SUM_GR") Then
                        .Bookmarks("FFIN_SUM_GR").Range.Text = Format(NullToStr(oGrupo.FechaFinSuministro), "Short Date")
                    End If
                    If .Bookmarks.Exists("GR_FECSUM") Then .Bookmarks("GR_FECSUM").Delete
                Else
                    If .Bookmarks.Exists("GR_FECSUM") Then
                        .Bookmarks("GR_FECSUM").Select
                        .Application.Selection.cut
                    End If
                End If
                oGrupo.CargarAtributos oGrupo.Codigo, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, True, AmbGrupo, True
                'ATRIBUTOS DE GRUPO
                If .Bookmarks.Exists("ATRIBUTO_LIN") Then
                    .Bookmarks("ATRIBUTO_LIN").Select
                    Set rangewordAtrGR = blankword.Bookmarks("ATRIBUTO_LIN").Range
                    rangewordAtrGR.Copy
    
                    .Application.Selection.cut
                    If oGrupo.ATRIBUTOS.Count = 0 Then
                        If .Bookmarks.Exists("GRUPO_ATRIBUTOS") Then
                            .Bookmarks("GRUPO_ATRIBUTOS").Select
                            .Application.Selection.cut
                            .Application.Selection.Delete
                        End If
                    Else
                    
                        For Each oatrib In oGrupo.ATRIBUTOS
                            rangewordAtrGR.Copy
                            
                            .Application.Selection.Paste
                                                
                            If .Bookmarks.Exists("COD_ATR_GRUPO") Then
                                .Bookmarks("COD_ATR_GRUPO").Range.Text = NullToStr(oatrib.Cod)
                            End If
                            If .Bookmarks.Exists("DEN_ATR_GRUPO") Then
                                .Bookmarks("DEN_ATR_GRUPO").Range.Text = NullToStr(oatrib.Den)
                            End If
                            If .Bookmarks.Exists("TIPO_ATR_GRUPO") Then
                                Select Case oatrib.Tipo
                                    Case 1:
                                        .Bookmarks("TIPO_ATR_GRUPO").Range.Text = NullToStr(m_stxtTexto)
                                    Case 2:
                                        .Bookmarks("TIPO_ATR_GRUPO").Range.Text = NullToStr(m_stxtNumero)
                                    Case 3:
                                        .Bookmarks("TIPO_ATR_GRUPO").Range.Text = NullToStr(m_stxtFecha)
                                    Case 4:
                                        .Bookmarks("TIPO_ATR_GRUPO").Range.Text = NullToStr(m_stxtSi)
                                End Select
                            
                            End If
                            If .Bookmarks.Exists("VALOR_ATR_GRUPO") Then
                                
                                If oatrib.TipoIntroduccion = IntroLibre Then
                                    If Not IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
               
                                        var1 = m_stxtMinimo & " : " & oatrib.Minimo & vbCrLf & m_stxtMaximo & " : " & oatrib.Maximo
                                        .Bookmarks("VALOR_ATR_GRUPO").Range.Text = var1
                                    End If
                                    If Not IsNull(oatrib.Minimo) And IsNull(oatrib.Maximo) Then
                                        
                                        var1 = m_stxtMinimo & " : " & oatrib.Minimo
                                        .Bookmarks("VALOR_ATR_GRUPO").Range.Text = var1
                                    
                                    End If
                                    If IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
                                        
                                        var1 = m_stxtMaximo & " : " & oatrib.Maximo
                                        .Bookmarks("VALOR_ATR_GRUPO").Range.Text = var1
                                    
                                    End If
                                End If
                                
                                If oatrib.TipoIntroduccion = Introselec Then
                                 
                                    var1 = ""
                                    var2 = "#@@#"
                                    For Each oElem In oatrib.ListaPonderacion
                                        var1 = var1 & var2 & " " & CStr(oElem.ValorLista) & vbCrLf
                                    Next
                                    If var1 <> "" Then
                                        .Bookmarks("VALOR_ATR_GRUPO").Range.Text = var1
                                    End If
                                End If
                                If .Bookmarks.Exists("VALOR_ATR_GRUPO") Then .Bookmarks("VALOR_ATR_GRUPO").Delete
                            
                            End If
                            
                            If .Bookmarks.Exists("OP_ATR_GRUPO") Then
                                .Bookmarks("OP_ATR_GRUPO").Range.Text = NullToStr(oatrib.PrecioFormula)
                                If .Bookmarks.Exists("OP_ATR_GRUPO") Then .Bookmarks("OP_ATR_GRUPO").Delete
                            End If
                            If .Bookmarks.Exists("APL_ATR_GRUPO") Then
                                Select Case oatrib.PrecioAplicarA
                                    Case 0:
                                        .Bookmarks("APL_ATR_GRUPO").Range.Text = NullToStr(m_stxtOferta)
                                    Case 1:
                                        .Bookmarks("APL_ATR_GRUPO").Range.Text = NullToStr(m_stxtGrupo)
                                    Case 2:
                                        .Bookmarks("APL_ATR_GRUPO").Range.Text = NullToStr(m_stxtItem)
                                    Case 3:
                                        .Bookmarks("APL_ATR_GRUPO").Range.Text = NullToStr(m_stxtUniItem)
                                End Select
                                If .Bookmarks.Exists("APL_ATR_GRUPO") Then .Bookmarks("APL_ATR_GRUPO").Delete
                            End If
                            
                        Next
                        If .Bookmarks.Exists("GRUPO_ATRIBUTOS") Then .Bookmarks("GRUPO_ATRIBUTOS").Delete
                        If .Bookmarks.Exists("ATRIBUTO_LIN") Then .Bookmarks("ATRIBUTO_LIN").Delete
                    
                    End If
                End If
                If (frmOFEPet.oProcesoSeleccionado.DefDestino = EnItem) Or (frmOFEPet.oProcesoSeleccionado.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                    Destino = True
                Else
                    Destino = False
                End If
                If frmOFEPet.oProcesoSeleccionado.DefFormaPago = EnItem Or (frmOFEPet.oProcesoSeleccionado.DefFormaPago = EnGrupo And Not oGrupo.DefFormaPago) Then
                    Pago = True
                Else
                    Pago = False
                End If
                If frmOFEPet.oProcesoSeleccionado.DefFechasSum = EnItem Or (frmOFEPet.oProcesoSeleccionado.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                    Fecha = True
                Else
                    Fecha = False
                End If
                oGrupo.CargarAtributos oGrupo.Codigo, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, True, AmbItem, True
                
                If .Bookmarks.Exists("ITEM_TOD") Then
                    .Bookmarks("ITEM_TOD").Select
                    .Application.Selection.cut
                    Set rangewordIT = blankword.Bookmarks("ITEM_TOD").Range
                    
                    If oGrupo.Items.Count = 0 Then
                        If .Bookmarks.Exists("ITEM_TOD") Then .Bookmarks("ITEM_TOD").Delete
                        
                    Else
                        iNumEsp = 1
                        'ITEMS
                        For Each oItem In oGrupo.Items
                            rangewordIT.Copy
                            .Application.Selection.Paste
                            
                            If .Bookmarks.Exists("ARTICULO") Then
                                If numItem > 1 Then
                                    .Bookmarks("ARTICULO").Select
                                    .Application.Selection.cut
                                Else
                                    .Bookmarks("ARTICULO").Delete
                                End If
                            End If
                            
                            FSGSLibrary.DatosConfigurablesAWord docword, "ITEM_DEST", "ITEM_PAGO", "ITEM_FECSUM", Destino, Pago, Fecha, "ESCALADO", (oGrupo.UsarEscalados = 1)
                            If .Bookmarks.Exists("ITEM") Then
                                Set rangewordArt = docword.Bookmarks("ITEM").Range
                                .Bookmarks("ITEM").Select
                                'rangewordArt.Select
                                '.Application.Selection.cut
                                '.Bookmarks("ITEM").cut
                                    
                                If oGrupo.UsarEscalados Then
                                    For Each oEsc In oGrupo.Escalados
                                        ImpresoOfertaArticulo docword, rangewordArt, iNumEsp, oItem, Destino, Pago, Fecha, ayDest, INUMDEST, ayPago, iNumPago, oEsc, oGrupo.TipoEscalados
                                    Next
                               Else
                                    ImpresoOfertaArticulo docword, rangewordArt, iNumEsp, oItem, Destino, Pago, Fecha, ayDest, INUMDEST, ayPago, iNumPago
                                
                                End If
                            End If
                            If .Bookmarks.Exists("ITEM") Then .Bookmarks("ITEM").Delete
                            
                            numItem = numItem + 1
                            'ATRIBUTOS DE ITEM
                            If .Bookmarks.Exists("ITEM_ATR") Then
                                If oGrupo.UsarEscalados Then
                                    MostrarMarcador docword, "ITEM_ATR_ESC", True
                                Else
                                    MostrarMarcador docword, "ITEM_ATR_ESC", False
                                End If
                                    
                                .Bookmarks("ITEM_ATR").Select
                                Set rangewordITAtr = blankword.Bookmarks("ITEM_ATR").Range
                                rangewordITAtr.Copy
                                .Application.Selection.cut
                                'oGrupo.CargarAtributos oGrupo.Codigo, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, True, AmbItem
                           
             
                                If oGrupo.AtributosItem.Count = 0 Then
                                    If .Bookmarks.Exists("ITEM_ATRIBUTOS") Then
                                        .Bookmarks("ITEM_ATRIBUTOS").Select
                                        .Application.Selection.cut
                                    End If
                                Else
                                    iNumAtr = 1
                                    For Each oatrib In oGrupo.AtributosItem
                                        If oatrib.ambito = AmbItem Then
                                            If oGrupo.UsarEscalados Then
                                                For Each oEsc In oGrupo.Escalados
                                                    ImpresoOfertaAtributo docword, iNumAtr, oatrib, oEsc, oGrupo.TipoEscalados
                                                Next
                                                Set oEsc = Nothing
                                            Else
                                                ImpresoOfertaAtributo docword, iNumAtr, oatrib
                                            End If
                                            
                                            iNumAtr = iNumAtr + 1
                                        Else
                                            If .Bookmarks.Exists("ITEM_ATR") Then .Bookmarks("ITEM_ATR").Delete
                                            If .Bookmarks.Exists("ITEM_ATRIBUTOS") Then .Bookmarks("ITEM_ATRIBUTOS").Delete
                                        End If
                                        If .Bookmarks.Exists("ITEM_ATR") Then .Bookmarks("ITEM_ATR").Delete
                                        If .Bookmarks.Exists("ITEM_ATRIBUTOS") Then .Bookmarks("ITEM_ATRIBUTOS").Delete
                                    Next
                                End If
                            End If
                            
                            'ATRIBUTOS DE ESPECIFICACION DE ITEM
                            If .Bookmarks.Exists("ATRIB_I_ESP") Then
                                .Bookmarks("ATRIB_I_ESP").Select
                                Set rangewordITAtr = .Bookmarks("ATRIB_I_ESP").Range
                                rangewordITAtr.Copy
                                .Application.Selection.cut
                                oItem.CargarTodosLosAtributosEspecificacion
                                If oItem.AtributosEspecificacion.Count = 0 Then
                                    If .Bookmarks.Exists("ITEM_ATRIB_ESP") Then
                                        .Bookmarks("ITEM_ATRIB_ESP").Select
                                        .Application.Selection.cut
                                    End If
                                Else
                                    iNumAtr = 0
                                    For Each oatrib In oItem.AtributosEspecificacion
                                        If Not oatrib.interno And _
                                            (oatrib.Tipo = TiposDeAtributos.TipoString And Not IsNull(oatrib.valorText) Or oatrib.Tipo = TiposDeAtributos.TipoNumerico And Not IsNull(oatrib.valorNum) Or oatrib.Tipo = TiposDeAtributos.TipoFecha And Not IsNull(oatrib.valorFec) Or oatrib.Tipo = TiposDeAtributos.TipoBoolean And Not IsNull(oatrib.valorBool)) Then
                                            .Application.Selection.Paste
                                            If .Bookmarks.Exists("COD_ATR_I_ESP") Then _
                                                .Bookmarks("COD_ATR_I_ESP").Range.Text = oatrib.Cod
                                            If .Bookmarks.Exists("DEN_ATR_I_ESP") Then _
                                                .Bookmarks("DEN_ATR_I_ESP").Range.Text = oatrib.Den
                                            If .Bookmarks.Exists("VALOR_ATR_I_ESP") Then
                                                Select Case oatrib.Tipo
                                                    Case TiposDeAtributos.TipoString:
                                                        .Bookmarks("VALOR_ATR_I_ESP").Range.Text = oatrib.valorText
                                                    Case TiposDeAtributos.TipoNumerico:
                                                        .Bookmarks("VALOR_ATR_I_ESP").Range.Text = oatrib.valorNum
                                                    Case TiposDeAtributos.TipoFecha:
                                                        .Bookmarks("VALOR_ATR_I_ESP").Range.Text = oatrib.valorFec
                                                    Case TiposDeAtributos.TipoBoolean:
                                                        .Bookmarks("VALOR_ATR_I_ESP").Range.Text = IIf(oatrib.valorBool, m_sSi, m_sNo)
                                                End Select
                                            End If
                                            iNumAtr = iNumAtr + 1
                                            If .Bookmarks.Exists("ATRIB_I_ESP") Then .Bookmarks("ATRIB_I_ESP").Delete
                                            If .Bookmarks.Exists("ITEM_ATRIB_ESP") Then .Bookmarks("ITEM_ATRIB_ESP").Delete
                                        End If
                                    Next
                                    If iNumAtr = 0 Then
                                        '.Application.Selection.Paste
                                        If .Bookmarks.Exists("ITEM_ATRIB_ESP") Then
                                            .Bookmarks("ITEM_ATRIB_ESP").Select
                                            .Application.Selection.cut
                                        End If
                                    End If
                                End If
                            End If
                                                                        
                            iNumEsp = iNumEsp + 1
                            If .Bookmarks.Exists("ITEM_TOD") Then .Bookmarks("ITEM_TOD").Delete
                        Next
                        If .Bookmarks.Exists("ITEM") Then .Bookmarks("ITEM").Delete
                    End If
                End If
    'ESPECIFICACIONES A NIVEL DE ITEM
                
                If frmOFEPet.oProcesoSeleccionado.DefEspItems = True Then
                    If chkEsp.Value = vbChecked Then
                        If .Bookmarks.Exists("ITEM_ESP") Then
                            .Bookmarks("ITEM_ESP").Select
                            Set rangeword = .Bookmarks("ITEM_ESP").Range
                            rangeword.Copy
                            .Application.Selection.cut
                            iNumFich = 0
                            iNumEsp = 1
                            For Each oItem In oGrupo.Items
                                 If oItem.CargarEspGeneral <> "" Then
                                     .Application.Selection.Paste
                                    If .Bookmarks.Exists("DEN_ESP_ITEM") Then .Bookmarks("DEN_ESP_ITEM").Range.Text = NullToStr(oItem.esp)
                                    If .Bookmarks.Exists("NUM_ESP_ITEM") Then .Bookmarks("NUM_ESP_ITEM").Range.Text = iNumEsp
                                    If .Bookmarks.Exists("ITEM_ESP") Then .Bookmarks("ITEM_ESP").Delete
                                    iNumFich = iNumFich + 1
                                End If
                                iNumEsp = iNumEsp + 1
                            Next
                            If iNumFich = 0 Then
                                If .Bookmarks.Exists("ITEM_ESPEC") Then
                                    .Bookmarks("ITEM_ESPEC").Range.cut
                                End If
                            Else
                                If .Bookmarks.Exists("ITEM_ESPEC") Then .Bookmarks("ITEM_ESPEC").Delete
                            End If
                        End If
    
                    Else
                        If .Bookmarks.Exists("ITEM_ESPEC") Then
                            .Bookmarks("ITEM_ESPEC").Range.cut
                        End If
                    End If
    
                    iNumEsp = 1
                    'FICHEROS ADJUNTOS
                    If chkFich.Value = vbChecked Then
                        If .Bookmarks.Exists("ITEM_ADJ") Then
                            .Bookmarks("ITEM_ADJ").Select
                            Set rangeword = .Bookmarks("ITEM_ADJ").Range
                            rangeword.Copy
                            .Application.Selection.cut
                            iNumFich = 0
                            For Each oItem In oGrupo.Items
                                oItem.CargarTodasLasEspecificaciones
                                If oItem.especificaciones.Count <> 0 Then
                                    For Each oEsp In oItem.especificaciones
                                            .Application.Selection.Paste
                                            If .Bookmarks.Exists("NUM_ADJ_ITEM") Then .Bookmarks("NUM_ADJ_ITEM").Range.Text = iNumEsp
                                            If .Bookmarks.Exists("NOM_ADJ_ITEM") Then .Bookmarks("NOM_ADJ_ITEM").Range.Text = CStr(oEsp.nombre)
                                            If .Bookmarks.Exists("FEC_ADJ_ITEM") Then .Bookmarks("FEC_ADJ_ITEM").Range.Text = Format(oEsp.Fecha, "Short Date")
                                            If .Bookmarks.Exists("DEN_ADJ_ITEM") Then .Bookmarks("DEN_ADJ_ITEM").Range.Text = NullToStr(oEsp.Comentario)
                                            If .Bookmarks.Exists("ITEM_ADJ") Then .Bookmarks("ITEM_ADJ").Delete
                                    Next
                                    iNumFich = iNumFich + 1
                                End If
                                iNumEsp = iNumEsp + 1
                            Next
                            If iNumFich = 0 Then
                                If .Bookmarks.Exists("ITEM_ADJ") Then
                                    .Bookmarks("ITEM_ADJ").Range.cut
                                End If
                                If .Bookmarks.Exists("ITEM_ADJUNTOS") Then
                                    .Bookmarks("ITEM_ADJUNTOS").Range.cut
                                End If
                            Else
                                If .Bookmarks.Exists("ITEM_ADJUNTOS") Then .Bookmarks("ITEM_ADJUNTOS").Delete
                            End If
        
                        Else
                            If .Bookmarks.Exists("ITEM_ADJ") Then
                                .Bookmarks("ITEM_ADJ").Range.cut
                            End If
                            If .Bookmarks.Exists("ITEM_ADJUNTOS") Then
                                .Bookmarks("ITEM_ADJUNTOS").Range.cut
                            End If
                        End If
                    Else
                        If .Bookmarks.Exists("ITEM_ADJ") Then
                            .Bookmarks("ITEM_ADJ").Range.cut
                        End If
                        If .Bookmarks.Exists("ITEM_ADJUNTOS") Then
                            .Bookmarks("ITEM_ADJUNTOS").Range.cut
                        End If
                    End If
        
                Else
                    If .Bookmarks.Exists("ITEM_ADJUNTOS") Then
                        .Bookmarks("ITEM_ADJUNTOS").Range.cut
                    End If
                    If .Bookmarks.Exists("ITEM_ESPEC") Then
                        .Bookmarks("ITEM_ESPEC").Range.cut
                    End If
                End If
                
                'ATRIBUTOS DE ESPECIFICACION DE GRUPO
                If .Bookmarks.Exists("ATRIB_G_ESP") Then
                    .Bookmarks("ATRIB_G_ESP").Select
                    Set rangewordITAtr = .Bookmarks("ATRIB_G_ESP").Range
                    rangewordITAtr.Copy
                    .Application.Selection.cut
                    oGrupo.CargarAEspecificacionesG
                    If oGrupo.AtributosEspecificacion.Count = 0 Then
                        .Application.Selection.Paste
                        If .Bookmarks.Exists("GRUPO_ATRIB_ESP") Then
                            .Bookmarks("GRUPO_ATRIB_ESP").Select
                            .Application.Selection.cut
                        End If
                    Else
                        iNumAtr = 0
                        For Each oatrib In oGrupo.AtributosEspecificacion
                            If Not oatrib.interno Then
                                .Application.Selection.Paste
                                If .Bookmarks.Exists("COD_ATR_G_ESP") Then _
                                    .Bookmarks("COD_ATR_G_ESP").Range.Text = oatrib.Cod
                                If .Bookmarks.Exists("DEN_ATR_G_ESP") Then _
                                    .Bookmarks("DEN_ATR_G_ESP").Range.Text = oatrib.Den
                                If .Bookmarks.Exists("VALOR_ATR_G_ESP") Then
                                    Select Case oatrib.Tipo
                                        Case TiposDeAtributos.TipoString:
                                            .Bookmarks("VALOR_ATR_G_ESP").Range.Text = oatrib.valorText
                                        Case TiposDeAtributos.TipoNumerico:
                                            .Bookmarks("VALOR_ATR_G_ESP").Range.Text = oatrib.valorNum
                                        Case TiposDeAtributos.TipoFecha:
                                            .Bookmarks("VALOR_ATR_G_ESP").Range.Text = oatrib.valorFec
                                        Case TiposDeAtributos.TipoBoolean:
                                            .Bookmarks("VALOR_ATR_G_ESP").Range.Text = IIf(oatrib.valorBool, m_sSi, m_sNo)
                                    End Select
                                End If
                                iNumAtr = iNumAtr + 1
                                If .Bookmarks.Exists("ATRIB_G_ESP") Then .Bookmarks("ATRIB_G_ESP").Delete
                                If .Bookmarks.Exists("GRUPO_ATRIB_ESP") Then .Bookmarks("GRUPO_ATRIB_ESP").Delete
                            End If
                        Next
                        If iNumAtr = 0 Then
                            If .Bookmarks.Exists("GRUPO_ATRIB_ESP") Then
                                .Bookmarks("GRUPO_ATRIB_ESP").Select
                                .Application.Selection.cut
                            End If
                        End If
                    End If
                End If
                                        
                If .Bookmarks.Exists("GRUPO") Then .Bookmarks("GRUPO").Delete
            End If 'bCargar
            Next 'GRUPO
    
        End If
          
              
        If .Bookmarks.Exists("DESTINO") Then
             Set rangeword = blankword.Bookmarks("DESTINO").Range
            .Bookmarks("DESTINO").Select
            .Application.Selection.cut
             For INUMDEST = 0 To UBound(ayDest) - 1
                 rangeword.Copy
                 .Application.Selection.Paste
                 If .Bookmarks.Exists("DEST_COD") Then .Bookmarks("DEST_COD").Range.Text = ayDest(INUMDEST)
                 If .Bookmarks.Exists("DEST_DIR") Then .Bookmarks("DEST_DIR").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).dir)
                 If .Bookmarks.Exists("DEST_POB") Then .Bookmarks("DEST_POB").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).POB)
                 If .Bookmarks.Exists("DEST_CP") Then .Bookmarks("DEST_CP").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).cP)
                 If .Bookmarks.Exists("DEST_DEN") Then .Bookmarks("DEST_DEN").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                 If .Bookmarks.Exists("DEST_PROV") Then .Bookmarks("DEST_PROV").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Provi)
                 If .Bookmarks.Exists("DEST_PAIS") Then .Bookmarks("DEST_PAIS").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(ayDest(INUMDEST)).Pais)
             Next
         End If
         
         If .Bookmarks.Exists("PAGO") Then
                .Bookmarks("PAGO").Select
                .Application.Selection.cut
                Set rangeword = blankword.Bookmarks("PAGO").Range
                For iNumPago = 0 To UBound(ayPago) - 1
                     rangeword.Copy
                     .Application.Selection.Paste
                     If .Bookmarks.Exists("PAGO_COD") Then .Bookmarks("PAGO_COD").Range.Text = ayPago(iNumPago)
                     If .Bookmarks.Exists("PAGO_DEN") Then .Bookmarks("PAGO_DEN").Range.Text = NullToStr(frmOFEPet.oPagos.Item(ayPago(iNumPago)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                 Next
         End If
       
        'Especificaciones a nivel de grupo
        If frmOFEPet.oProcesoSeleccionado.DefEspGrupos = True Then
            If chkEsp.Value = vbChecked Then
                If .Bookmarks.Exists("GRUPO_ESP") Then
                    .Bookmarks("GRUPO_ESP").Select
                    Set rangeword = blankword.Bookmarks("GRUPO_ESP").Range
                    rangeword.Copy
                    .Application.Selection.cut
                    iNumFich = 0
                    For Each oGrupo In frmOFEPet.oProcesoSeleccionado.Grupos
                         If oGrupo.esp <> "" Then
                            .Application.Selection.Paste
                            
                            If .Bookmarks.Exists("NUM_ESP_GRUPO") Then .Bookmarks("NUM_ESP_GRUPO").Range.Text = NullToStr(oGrupo.Codigo)
                            If .Bookmarks.Exists("DEN_ESP_GRUPO") Then .Bookmarks("DEN_ESP_GRUPO").Range.Text = NullToStr(oGrupo.esp)
                            If .Bookmarks.Exists("GRUPO_ESP") Then .Bookmarks("GRUPO_ESP").Delete
                            iNumFich = iNumFich + 1
                        End If
                    Next
                    If iNumFich = 0 Then
                        If .Bookmarks.Exists("GRUPO_ESPEC") Then
                            .Bookmarks("GRUPO_ESPEC").Range.cut
                        End If
                    Else
                        If .Bookmarks.Exists("GRUPO_ESPEC") Then .Bookmarks("GRUPO_ESPEC").Delete
                    End If
                
                End If
            Else
                If .Bookmarks.Exists("GRUPO_ESPEC") Then
                    .Bookmarks("GRUPO_ESPEC").Range.cut
                End If
            End If
                    
            'FICHEROS ADJUNTOS
            If chkFich.Value = vbChecked Then
                If .Bookmarks.Exists("GRUPO_ADJ") Then
                    .Bookmarks("GRUPO_ADJ").Select
                    Set rangeword = blankword.Bookmarks("GRUPO_ADJ").Range
                    rangeword.Copy
                    .Application.Selection.cut
                    iNumFich = 0
                    For Each oGrupo In frmOFEPet.oProcesoSeleccionado.Grupos
                        oGrupo.CargarTodasLasEspecificaciones
                        If oGrupo.especificaciones.Count <> 0 Then
                            For Each oEsp In oGrupo.especificaciones
                                    .Application.Selection.Paste
                                    If .Bookmarks.Exists("NUM_ADJ_GRUPO") Then .Bookmarks("NUM_ADJ_GRUPO").Range.Text = oGrupo.Codigo
                                    If .Bookmarks.Exists("NOM_ADJ_GRUPO") Then .Bookmarks("NOM_ADJ_GRUPO").Range.Text = CStr(oEsp.nombre)
                                    If .Bookmarks.Exists("DEN_ADJ_GRUPO") Then .Bookmarks("DEN_ADJ_GRUPO").Range.Text = NullToStr(oEsp.Comentario)
                                    If .Bookmarks.Exists("FEC_ADJ_GRUPO") Then .Bookmarks("FEC_ADJ_GRUPO").Range.Text = Format(oEsp.Fecha, "Short Date")
                                    If .Bookmarks.Exists("GRUPO_ADJ") Then .Bookmarks("GRUPO_ADJ").Delete
                            Next
                            iNumFich = iNumFich + 1
                        End If
                    Next
                    If iNumFich = 0 Then
                        If .Bookmarks.Exists("GRUPO_ADJ") Then
                            .Bookmarks("GRUPO_ADJ").Range.cut
                        End If
                        If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then
                            .Bookmarks("GRUPO_ADJUNTOS").Range.cut
                        End If
                    Else
                        If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then .Bookmarks("GRUPO_ADJUNTOS").Delete
                    End If
                
                Else
                    If .Bookmarks.Exists("GRUPO_ADJ") Then
                        .Bookmarks("GRUPO_ADJ").Range.cut
                    End If
                    If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then
                        .Bookmarks("GRUPO_ADJUNTOS").Range.cut
                    End If
                End If
            Else
                If .Bookmarks.Exists("GRUPO_ADJ") Then
                    .Bookmarks("GRUPO_ADJ").Range.cut
                End If
                If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then
                    .Bookmarks("GRUPO_ADJUNTOS").Range.cut
                End If
            End If
        Else
            If .Bookmarks.Exists("GRUPO_ADJUNTOS") Then
                .Bookmarks("GRUPO_ADJUNTOS").Range.cut
            End If
            If .Bookmarks.Exists("GRUPO_ESPEC") Then
                .Bookmarks("GRUPO_ESPEC").Range.cut
            End If
        End If
        
        ' ESPECIFICACIONES DE PROCESO
        If frmOFEPet.oProcesoSeleccionado.DefEspecificaciones = True Then
            If Not frmOFEPet.oProcesoSeleccionado.esp = "" Then
                If .Bookmarks.Exists("ESPEC_PROCESO") Then .Bookmarks("ESPEC_PROCESO").Range.Text = frmOFEPet.oProcesoSeleccionado.esp
                If .Bookmarks.Exists("OPC_ESP_PROCESO") Then .Bookmarks("OPC_ESP_PROCESO").Delete
            Else
                If .Bookmarks.Exists("OPC_ESP_PROCESO") Then
                    .Bookmarks("OPC_ESP_PROCESO").Range.cut
                Else
                    If .Bookmarks.Exists("ESPEC_PROCESO") Then .Bookmarks("ESPEC_PROCESO").Range.Text = ""
                End If
            End If
            'FICHEROS ADJUNTOS
            If chkFich.Value = vbChecked Then
                frmOFEPet.oProcesoSeleccionado.CargarTodasLasEspecificaciones
                If frmOFEPet.oProcesoSeleccionado.especificaciones.Count > 0 Then
                    If .Bookmarks.Exists("PROC_ADJ") Then
                        .Bookmarks("PROC_ADJ").Select
                        Set rangeword = blankword.Bookmarks("PROC_ADJ").Range
                        rangeword.Copy
                        .Application.Selection.cut
                        iNumFich = 0
                        For Each oEsp In frmOFEPet.oProcesoSeleccionado.especificaciones
                            .Application.Selection.Paste
    '                        .bookmarks("NUM_ADJ_PROC").Range.Text = CStr(iNumFich)
                            If .Bookmarks.Exists("NOM_ADJ_PROC") Then .Bookmarks("NOM_ADJ_PROC").Range.Text = oEsp.nombre
                            If .Bookmarks.Exists("FEC_ADJ_PROC") Then .Bookmarks("FEC_ADJ_PROC").Range.Text = Format(oEsp.Fecha, "Short Date")
                            If .Bookmarks.Exists("DEN_ADJ_PROC") Then .Bookmarks("DEN_ADJ_PROC").Range.Text = NullToStr(oEsp.Comentario)
                            If .Bookmarks.Exists("PROC_ADJ") Then .Bookmarks("PROC_ADJ").Delete
                            iNumFich = iNumFich + 1
                        Next
                        If iNumFich = 0 Then
                            If .Bookmarks.Exists("PROCESO_ADJ") Then
                                .Bookmarks("PROCESO_ADJ").Range.cut
                            End If
                        Else
                            If .Bookmarks.Exists("PROCESO_ADJ") Then .Bookmarks("PROCESO_ADJ").Delete
                        End If
                    End If
                Else
                    If .Bookmarks.Exists("PROCESO_ADJ") Then
                        .Bookmarks("PROCESO_ADJ").Range.cut
                    End If
                End If
            Else
                If .Bookmarks.Exists("PROCESO_ADJ") Then
                    .Bookmarks("PROCESO_ADJ").Range.cut
                End If
            End If
        Else
            If .Bookmarks.Exists("OPC_ESP_PROCESO") Then
                .Bookmarks("OPC_ESP_PROCESO").Range.cut
            End If
            If .Bookmarks.Exists("PROCESO_ADJ") Then
                .Bookmarks("PROCESO_ADJ").Range.cut
            End If
        End If
       
       'ATRIBUTOS DE ESPECIFICACION DEL PROCESO
        frmOFEPet.oProcesoSeleccionado.CargarAEspecificacionesP
        
        If .Bookmarks.Exists("ATRIB_P_ESP") Then
            .Bookmarks("ATRIB_P_ESP").Select
            .Application.Selection.cut
    
            If frmOFEPet.oProcesoSeleccionado.AtributosEspecificacion.Count = 0 Then
                If .Bookmarks.Exists("PROCE_ATRIB_ESP") Then
                    .Bookmarks("PROCE_ATRIB_ESP").Select
                    .Application.Selection.cut
                    .Application.Selection.Delete
                End If
            Else
                For Each oatrib In frmOFEPet.oProcesoSeleccionado.AtributosEspecificacion
                    .Application.Selection.Paste
    
                    If .Bookmarks.Exists("COD_ATR_P_ESP") Then .Bookmarks("COD_ATR_P_ESP").Range.Text = NullToStr(oatrib.Cod)
                    If .Bookmarks.Exists("DEN_ATR_P_ESP") Then .Bookmarks("DEN_ATR_P_ESP").Range.Text = NullToStr(oatrib.Den)
                    If .Bookmarks.Exists("VALOR_ATR_P_ESP") Then
                        Select Case oatrib.Tipo
                            Case 1:
                                .Bookmarks("VALOR_ATR_P_ESP").Range.Text = NullToStr(oatrib.valorText)
                            Case 2:
                                .Bookmarks("VALOR_ATR_P_ESP").Range.Text = NullToStr(oatrib.valorNum)
                            Case 3:
                                .Bookmarks("VALOR_ATR_P_ESP").Range.Text = NullToStr(oatrib.valorFec)
                            Case 4:
                                If NullToStr(oatrib.valorBool) = "False" Then
                                    .Bookmarks("VALOR_ATR_P_ESP").Range.Text = m_sNo
                                ElseIf NullToStr(oatrib.valorBool) = "True" Then
                                    .Bookmarks("VALOR_ATR_P_ESP").Range.Text = m_sSi
                                End If
                                
                        End Select
                    End If
                Next
                If .Bookmarks.Exists("ATRIB_P_ESP") Then .Bookmarks("ATRIB_P_ESP").Delete
            End If
        End If
        
        .Application.Selection.Find.ClearFormatting
        .Application.Selection.Find.Text = "#@@#"
        .Application.Selection.Find.Replacement.Text = ""
        .Application.Selection.Find.Forward = True
        .Application.Selection.Find.Wrap = 1
        .Application.Selection.Find.Execute
        While .Application.Selection.Find.Found = True
            .Application.Selection.InsertSymbol CharacterNumber:=9633, Unicode:=True
            .Application.Selection.Find.Execute
        Wend
                        
    End With
                    
    Set ImpresoOferta = docword
    Set oEsp = Nothing
    Set oProves = Nothing
    blankword.Close
    Set blankword = Nothing
    Set rangewordArt = Nothing
        
End Function

''' <summary>Crea una nueva l�nea de atributo de art�culo en el impreso de oferta</summary>
''' <param name="docword">Documento word</param>
''' <param name="iNumAtr">n� de atributo</param>
''' <param name="oatrib">objeto atributo</param>
''' <param name="TipoEscalado">Tipo de escalados</param>
''' <returns></returns>
''' <remarks>Llamada desde;ImpresoOferta</remarks>

Private Sub ImpresoOfertaAtributo(ByRef docword As Object, ByVal iNumAtr As Integer, ByVal oatrib As CAtributo, Optional ByVal oEsc As CEscalado, _
        Optional ByVal TipoEscalado As TModoEscalado)
    Dim var1 As Variant
    Dim var2 As Variant
    Dim oElem As CValorPond
    'Provisional
    On Error Resume Next
    With docword
        ' rangewordITAtr.Copy
         .Application.Selection.Paste
         
        If .Bookmarks.Exists("ITEM_ATR_ESC_RANGO") Then
           If Not oEsc Is Nothing Then
               If TipoEscalado = ModoDirecto Then
                   .Bookmarks("ITEM_ATR_ESC_RANGO").Range.Text = oEsc.Inicial
               Else
                   .Bookmarks("ITEM_ATR_ESC_RANGO").Range.Text = oEsc.Inicial & " - " & oEsc.final
               End If
           End If
        End If
         
        If .Bookmarks.Exists("NUM_ATR_ITEM") Then
            .Bookmarks("NUM_ATR_ITEM").Range.Text = NullToStr(iNumAtr)
        End If
        If .Bookmarks.Exists("COD_ATR_ITEM") Then
            .Bookmarks("COD_ATR_ITEM").Range.Text = NullToStr(oatrib.Cod)
        
        End If
        If .Bookmarks.Exists("DEN_ATR_ITEM") Then
            .Bookmarks("DEN_ATR_ITEM").Range.Text = NullToStr(oatrib.Den)
        End If
        If .Bookmarks.Exists("TIPO_ATR_ITEM") Then
            Select Case oatrib.Tipo
                Case 1:
                    .Bookmarks("TIPO_ATR_ITEM").Range.Text = NullToStr(m_stxtTexto)
                Case 2:
                    .Bookmarks("TIPO_ATR_ITEM").Range.Text = NullToStr(m_stxtNumero)
                Case 3:
                    .Bookmarks("TIPO_ATR_ITEM").Range.Text = NullToStr(m_stxtFecha)
                Case 4:
                    .Bookmarks("TIPO_ATR_ITEM").Range.Text = NullToStr(m_stxtSi)
            End Select
        
        End If
        If .Bookmarks.Exists("OP_ATR_ITEM") Then
            .Bookmarks("OP_ATR_ITEM").Range.Text = NullToStr(oatrib.PrecioFormula)
            If .Bookmarks.Exists("OP_ATR_ITEM") Then .Bookmarks("OP_ATR_ITEM").Delete
        End If
        
        If .Bookmarks.Exists("VALOR_ATR_ITEM") Then
            
            If oatrib.TipoIntroduccion = IntroLibre Then
                If Not IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
        
                    var1 = m_stxtMinimo & " : " & oatrib.Minimo & vbCrLf & m_stxtMaximo & " : " & oatrib.Maximo
                    .Bookmarks("VALOR_ATR_ITEM").Range.Text = var1
                End If
                If Not IsNull(oatrib.Minimo) And IsNull(oatrib.Maximo) Then
                    
                    var1 = m_stxtMinimo & " : " & oatrib.Minimo
                    .Bookmarks("VALOR_ATR_ITEM").Range.Text = var1
                
                End If
                If IsNull(oatrib.Minimo) And Not IsNull(oatrib.Maximo) Then
                    
                    var1 = m_stxtMaximo & " : " & oatrib.Maximo
                    .Bookmarks("VALOR_ATR_ITEM").Range.Text = var1
                
                End If
            End If
            
            If oatrib.TipoIntroduccion = Introselec Then
        
                var1 = ""
                var2 = "#@@#"
                For Each oElem In oatrib.ListaPonderacion
                    var1 = var1 & var2 & " " & CStr(oElem.ValorLista) & vbCrLf
                Next
                Set oElem = Nothing
                If var1 <> "" Then
                    .Bookmarks("VALOR_ATR_ITEM").Range.Text = var1
                End If
            End If
            If .Bookmarks.Exists("VALOR_ATR_ITEM") Then .Bookmarks("VALOR_ATR_ITEM").Delete
        
        End If
            
        If .Bookmarks.Exists("APL_ATR_ITEM") Then
            Select Case oatrib.PrecioAplicarA
                Case 0:
                    .Bookmarks("APL_ATR_ITEM").Range.Text = NullToStr(m_stxtOferta)
                Case 1:
                    .Bookmarks("APL_ATR_ITEM").Range.Text = NullToStr(m_stxtGrupo)
                Case 2:
                    .Bookmarks("APL_ATR_ITEM").Range.Text = NullToStr(m_stxtItem)
                Case 3:
                    .Bookmarks("APL_ATR_ITEM").Range.Text = NullToStr(m_stxtUniItem)
        
            End Select
            If .Bookmarks.Exists("APL_ATR_ITEM") Then .Bookmarks("APL_ATR_ITEM").Delete
        End If
    End With
    
    
End Sub

''' <summary>Crea una nueva l�nea de art�culo en el impreso de oferta</summary>
''' <param name="docword">Documento word</param>
''' <param name="rangewordArt">Rango con las columnas para datos del art�culo</param>
''' <param name="iNumEsp">N� de art�culo</param>
''' <param name="oItem">Objeto item</param>
''' <param name="Destino">Indica si se muestra la columna Destino</param>
''' <param name="Pago">Indica sise muestra la columna F. de Pago</param>
''' <param name="Fecha">Indica si se muestra la columna F. suministro</param>
''' <param name="ayDest">array de destinos</param>
''' <param name="INUMDEST">n� de destinos</param>
''' <param name="ayPago">array de formas de pago</param>
''' <param name="iNumPago">n� de formas de pago</param>
''' <param name="oEsc">Objeto escalado si hay</param>
''' <param name="TipoEscalado">Tipo de escalados</param>
''' <returns></returns>
''' <remarks>Llamada desde;ImpresoOferta</remarks>

Private Sub ImpresoOfertaArticulo(ByRef docword As Object, ByRef rangewordArt As Object, ByVal iNumEsp As Integer, ByVal oItem As CItem, _
        ByVal Destino As Boolean, ByVal Pago As Boolean, ByVal Fecha As Boolean, ByRef ayDest As Variant, ByRef INUMDEST As Integer, _
        ByRef ayPago As Variant, ByRef iNumPago As Integer, Optional ByVal oEsc As CEscalado, Optional ByVal TipoEscalado As TModoEscalado)
'Provisional
On Error Resume Next

    With docword
        rangewordArt.Copy
        .Application.Selection.Paste
    
        If .Bookmarks.Exists("ESC_RANGO") Then
            If Not oEsc Is Nothing Then
                If TipoEscalado = ModoDirecto Then
                    .Bookmarks("ESC_RANGO").Range.Text = oEsc.Inicial
                Else
                    .Bookmarks("ESC_RANGO").Range.Text = oEsc.Inicial & " - " & oEsc.final
                End If
            End If
        End If
        
        If .Bookmarks.Exists("NUM_ART") Then .Bookmarks("NUM_ART").Range.Text = iNumEsp
        If .Bookmarks.Exists("COD_ART") Then .Bookmarks("COD_ART").Range.Text = NullToStr(oItem.ArticuloCod)
        If .Bookmarks.Exists("DEN_ART") Then .Bookmarks("DEN_ART").Range.Text = NullToStr(oItem.Descr)
        If .Bookmarks.Exists("CANTIDAD") Then .Bookmarks("CANTIDAD").Range.Text = FormateoNumerico(oItem.Cantidad)
        If .Bookmarks.Exists("COD_UNI") Then .Bookmarks("COD_UNI").Range.Text = frmOFEPet.oUnidades.Item(oItem.UniCod).Cod
            
        If Destino Then
            If .Bookmarks.Exists("COD_DEST") Then
                .Bookmarks("COD_DEST").Range.Text = NullToStr(oItem.DestCod)
            End If
    
            If .Bookmarks.Exists("DEN_DEST") Then
                .Bookmarks("DEN_DEST").Range.Text = NullToStr(frmOFEPet.oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            
            If .Bookmarks.Exists("DESTINO") Then
                If Not BuscarEnArray(ayDest, oItem.DestCod) Then
                    ayDest(INUMDEST) = oItem.DestCod
                    INUMDEST = INUMDEST + 1
                    ReDim Preserve ayDest(UBound(ayDest) + 1)
                End If
            End If
        End If
                
        If Pago Then
            If .Bookmarks.Exists("COD_PAGO") Then
                .Bookmarks("COD_PAGO").Range.Text = NullToStr(oItem.PagCod)
            End If
    
            If .Bookmarks.Exists("DEN_PAGO") Then
                .Bookmarks("DEN_PAGO").Range.Text = NullToStr(frmOFEPet.oPagos.Item(oItem.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            
            If .Bookmarks.Exists("PAGO") Then
                If Not BuscarEnArray(ayPago, oItem.PagCod) Then
                    ayPago(iNumPago) = oItem.PagCod
                    iNumPago = iNumPago + 1
                    ReDim Preserve ayPago(UBound(ayPago) + 1)
                End If
            End If
        End If
        If Fecha Then
            If .Bookmarks.Exists("FINI_SUMINISTRO") Then
                .Bookmarks("FINI_SUMINISTRO").Range.Text = Format(NullToStr(oItem.FechaInicioSuministro), "Short Date")
            End If
    
            If .Bookmarks.Exists("FFIN_SUMINISTRO") Then
                .Bookmarks("FFIN_SUMINISTRO").Range.Text = Format(NullToStr(oItem.FechaFinSuministro), "Short Date")
            End If
        End If
    End With
    
End Sub

''' <summary>
''' Crear el word para el cuerpo de la notificaci�n
''' </summary>
''' <param name="appword">objeto word con el q trabajamos</param>
''' <param name="splantilla">plantilla de la notificacion</param>
''' <param name="oPet">prove notificado</param>
''' <param name="oEquipo">Equipo del notificado</param>
''' <returns>fichero generado</returns>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Function CartaImpreso(splantilla As String, ByVal oPet As CPetOferta, oEquipo As CEquipo) As Object
Dim docword As Object

' CARGAR DATOS GENERALES DE PROVEEDOR
        Set oProves = Nothing
        Set oProves = oFSGSRaiz.generar_CProveedores
        oProves.Add oPet.CodProve, oPet.DenProve
        oProves.CargarDatosProveedor oPet.CodProve
       
    Set docword = appword.Documents.Add(splantilla)
    'appword.Visible = True
    
    If m_iWordVer >= 9 Then
        'appword.Windows(docword.Name).WindowState = 2
        If appword.Visible Then docword.activewindow.WindowState = 2
    Else
        appword.Visible = True
        appword.Windows(docword.Name).WindowState = 1
    End If


With docword
    If .Bookmarks.Exists("NUM_PROCESO") Then
        DatoAWord docword, "NUM_PROCESO", frmOFEPet.sdbcAnyo.Text & "/" & frmOFEPet.sdbcGMN1_4Cod.Text & "/" & frmOFEPet.sdbcProceCod.Text
    End If
    If .Bookmarks.Exists("DEN_PROCESO") Then
        DatoAWord docword, "DEN_PROCESO", frmOFEPet.sdbcProceDen.Text & " "
    End If
    If .Bookmarks.Exists("MAT1_PROCESO") Then
        DatoAWord docword, "MAT1_PROCESO", frmOFEPet.sdbcGMN1_4Cod.Text
    End If
    If .Bookmarks.Exists("FECHA_LIMITE") Then
        DatoAWord docword, "FECHA_LIMITE", m_vDatoAPasar
    End If
    If .Bookmarks.Exists("FECHAINISUB") Then
        DatoAWord docword, "FECHAINISUB", m_sFecIniSub
    End If
    If .Bookmarks.Exists("SOLICITUD") Then
        DatoAWord docword, "SOLICITUD", NullToStr(frmOFEPet.oProcesoSeleccionado.Referencia)
    End If
    ' DATOS PROVEEDORES
    'With docword
    If .Bookmarks.Exists("COD_PROVE") Then
        DatoAWord docword, "COD_PROVE", oPet.CodProve
    End If
    If .Bookmarks.Exists("DEN_PROVE") Then
        DatoAWord docword, "DEN_PROVE", oPet.DenProve
    End If
    If .Bookmarks.Exists("DIR_PROVE") Then
        DatoAWord docword, "DIR_PROVE", NullToStr(oProves.Item(1).Direccion)
    End If
    If .Bookmarks.Exists("POBL_PROVE") Then
        DatoAWord docword, "POBL_PROVE", NullToStr(oProves.Item(1).Poblacion)
    End If
    If .Bookmarks.Exists("CP_PROVE") Then
        DatoAWord docword, "CP_PROVE", NullToStr(oProves.Item(1).cP)
    End If
    If .Bookmarks.Exists("PROV_PROVE") Then
        DatoAWord docword, "PROV_PROVE", NullToStr(oProves.Item(1).DenProvi)
    End If
    If .Bookmarks.Exists("PAIS_PROVE") Then
        DatoAWord docword, "PAIS_PROVE", NullToStr(oProves.Item(1).DenPais)
    End If
    If .Bookmarks.Exists("NIF_PROVE") Then
        DatoAWord docword, "NIF_PROVE", NullToStr(oProves.Item(1).NIF)
    End If
    ' DATOS PERSONA CONTACTO
    If .Bookmarks.Exists("NOM_CONTACTO") Then
        DatoAWord docword, "NOM_CONTACTO", NullToStr(oPet.nombre)
    End If
    If .Bookmarks.Exists("APE_CONTACTO") Then
        DatoAWord docword, "APE_CONTACTO", NullToStr(oPet.Apellidos)
    End If
    If .Bookmarks.Exists("TFNO_CONTACTO") Then
        DatoAWord docword, "TFNO_CONTACTO", NullToStr(oPet.Tfno)
    End If
    If .Bookmarks.Exists("FAX_CONTACTO") Then
        DatoAWord docword, "FAX_CONTACTO", NullToStr(oPet.Fax)
    End If
    If .Bookmarks.Exists("MAIL_CONTACTO") Then
        DatoAWord docword, "MAIL_CONTACTO", NullToStr(oPet.Email)
    End If
    If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
        DatoAWord docword, "TFNO_MOVIL_CONTACTO", NullToStr(oPet.tfnomovil)
    End If

    
    ' DATOS COMPRADOR
    If .Bookmarks.Exists("APE_COMPRADOR") Then
        DatoAWord docword, "APE_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Apel)
    End If
    If .Bookmarks.Exists("NOM_COMPRADOR") Then
        DatoAWord docword, "NOM_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).nombre)
    End If
    If .Bookmarks.Exists("TFNO_COMPRADOR") Then
        DatoAWord docword, "TFNO_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Tfno)
    End If
    If .Bookmarks.Exists("FAX_COMPRADOR") Then
        DatoAWord docword, "FAX_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Fax)
    End If
    If .Bookmarks.Exists("MAIL_COMPRADOR") Then
        DatoAWord docword, "MAIL_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).mail)
    End If
End With


Set oProves = Nothing


Set CartaImpreso = docword
Set docword = Nothing
  
End Function

Private Function ContarProveedoresAsignados() As Integer
Dim oAsigs As CAsignaciones

If oIasig Is Nothing Then Exit Function

Set oAsigs = oIasig.DevolverAsignaciones

ContarProveedoresAsignados = oAsigs.Count

Set oAsigs = Nothing

End Function
Private Function SoloFichero(ByVal sPath As String) As String
Dim vFile As Variant

    If sPath <> "" Then
        vFile = Split(sPath, "\")
        If UBound(vFile) > 0 Then
            SoloFichero = vFile(UBound(vFile))
        Else
            SoloFichero = ""
        End If
    Else
        SoloFichero = ""
    End If
    
End Function


Private Sub sdbgPet_RowLoaded(ByVal Bookmark As Variant)
    If Not oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value) Is Nothing Then
        If NullToStr(oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value).CodPortal) <> "" And _
          gParametrosGenerales.giINSTWEB = ConPortal Then
            sdbgPet.Columns("CODPROVE").CellStyleSet "ProvPortal"
            sdbgPet.Columns("DENPROVE").CellStyleSet "ProvPortal"
        Else
            sdbgPet.Columns("CODPROVE").CellStyleSet "Normal"
            sdbgPet.Columns("DENPROVE").CellStyleSet "Normal"
        End If
    End If
    
    If sdbgPet.Columns("PORTALCON").Value = "1" Then
        sdbgPet.Columns("APE").CellStyleSet "ProvPortal"
    Else
        sdbgPet.Columns("APE").CellStyleSet "Normal"
    End If
End Sub


''' <summary>
''' Genera el excel de la oferta
''' </summary>
''' <param name="oPet">Oferta</param>
''' <param name="sTarget">Path en el que se copia el excel</param>
''' <returns></returns>
''' <remarks>Llamada desde;cmdAceptar_Click Tiempo m�ximo 0</remarks>
Private Function GenerarExcel(ByVal oPet As CPetOferta, ByVal sTarget As String) As Object
Dim sConnect As String
Dim oExcelAdoConn As ADODB.Connection
Dim oExcelAdoRS As ADODB.Recordset
Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter
Dim SQL As String
Dim xls() As String
Dim iNumCols As Integer
Dim oGrupo As CGrupo
Dim oOfeGrupo As COfertaGrupo
Dim oatrib As CAtributo
Dim oItem As CItem
Dim oEsp As CEspecificacion
Dim oValor As CValorPond
Dim oAtribOfe As CAtributoOfertado
Dim oAtribsOfe As CAtributosOfertados
Dim i As Integer
Dim oMon As CMoneda
Dim iFila As Integer
Dim iFilaInicio As Integer
Dim iNumFilasPorAtrib As Integer
Dim oAtribEsp As CAtributo
Dim lCountAtrEspeGrupo  As Long

iNumFilasPorAtrib = 13

Dim oProceso As CProceso
Dim oOferta As COferta
Dim oPrecioItem As CPrecioItem
Dim lCountEspecsGrupo  As Long
Dim sPathXls As String
Dim sCod As String
Dim oFSO As Object
Dim oFile As Scripting.File
Dim Ador As Ador.Recordset
Dim oOfe As COferta
Dim j As Integer
Dim bGenerarHoja As Boolean
Dim bSinOfertas As Boolean
Dim scodProve As String
Dim bCargar As Boolean
Dim lNumGrupos As Long
Dim sTexto As String

On Error GoTo Error

Set oProceso = frmOFEPet.oProcesoSeleccionado

Screen.MousePointer = vbHourglass

bGenerarHoja = True
    
Set oOferta = oProceso.CargarUltimaOfertaProveedor(oPet.CodProve)

If oOferta Is Nothing Then
    Set oOferta = oFSGSRaiz.Generar_COferta
    oOferta.Anyo = oProceso.Anyo
    oOferta.GMN1Cod = oProceso.GMN1Cod
    oOferta.Proce = oProceso.Cod
    oOferta.Prove = oPet.CodProve
    oOferta.Num = 1
    oOferta.CodMon = oProceso.MonCod
        
    bSinOfertas = True
    'comprueba si se ha generado ya la hoja excel: si se usa prove-grupos no se puede usar la misma excel, se genera siempre
    'si hay multimaterial y el proveedor solo debe ver los items de su material. No vale la vieja excel.
    If gParametrosGenerales.gbProveGrupos = False _
    And Not (gParametrosGenerales.gbMultiMaterial And bPubMatProve) Then
        Set oFSO = CreateObject("Scripting.filesystemobject")
        If oFSO.FileExists(DevolverPathFichTemp & ValidFilename(oProceso.Anyo & "_" & oProceso.GMN1Cod & "_" & oProceso.Cod & ".xls")) Then
            bGenerarHoja = False
        End If
        Set oFSO = Nothing
    End If
Else
    sCod = oPet.CodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oPet.CodProve))
    sCod = sCod & CStr(oOferta.Num)
    Set oOferta = m_oOfertasProceso.Item(sCod)
End If

If bGenerarHoja = True Then
    'Genera la excel
    Dim dcListaZonasHorarias As Dictionary
    Set dcListaZonasHorarias = ObtenerZonasHorarias
    
    If IsNull(oUsuarioSummit.TimeZone) Or IsEmpty(oUsuarioSummit.TimeZone) Then
        oUsuarioSummit.TimeZone = GetTimeZone.key
    End If
    
    Dim oExcel As CExcelOfertas
    Set oExcel = New CExcelOfertas
    If oExcel.FSXLSOfertaOffline(oProceso, oPet.CodProve, m_oAsigs, sTarget, basParametros.gLongitudesDeCodigos.giLongCodPROVE, oFSGSRaiz, basPublic.gParametrosInstalacion.gIdioma, gParametrosGenerales.gbProveGrupos, _
            oGestorIdiomas, gParametrosInstalacion.giCargaMaximaCombos, oProceso.MostrarAtribExpExcelOfe, gParametrosInstalacion.giAnyadirEspecArticulo, , Mid(dcListaZonasHorarias.Item(oUsuarioSummit.TimeZone), 2, 9)) Then
        Set oFSO = CreateObject("Scripting.filesystemobject")
        Set oFile = oFSO.GetFile(sTarget)
    
        'Copia la hoja y la renombra sin el prov al temporal.Se mantiene ah� para el siguiente prov. sin ofertas:
        If bSinOfertas = True Then
            sPathXls = DevolverPathFichTemp & oProceso.Anyo & "_" & oProceso.GMN1Cod & "_" & oProceso.Cod & ".xls"
            Set oFSO = CreateObject("Scripting.filesystemobject")
            oFSO.CopyFile sTarget, sPathXls
            Set oFSO = Nothing
        End If
    End If
    Set dcListaZonasHorarias = Nothing
    Set oExcel = Nothing
Else
    'Copia la excel y la renombra
    sPathXls = DevolverPathFichTemp & oProceso.Anyo & "_" & oProceso.GMN1Cod & "_" & oProceso.Cod & ".xls"
    Set oFSO = CreateObject("Scripting.filesystemobject")
    oFSO.CopyFile sPathXls, sTarget
    Set oFile = oFSO.GetFile(sTarget)
    Set oFSO = Nothing
End If

Set oOferta = Nothing

Set GenerarExcel = oFile

Screen.MousePointer = vbNormal

fin:
Exit Function

Error:
If err.Number = 9 Then
    ReDim Preserve xls(0 To UBound(xls, 1), 0 To UBound(xls, 2) + 10) As String
    Resume 0
ElseIf err.Number = 32755 Then
    Resume fin
Else
    Resume Next
    Resume 0
End If

End Function


Private Sub txtFecLimOfe_LostFocus()
    If txtFecLimOfe.Text = "" Then
        txtHoraLimite.Text = ""
    Else
        If txtHoraLimite.Text = "" Then txtHoraLimite.Text = TimeValue("23:59")
    End If
End Sub

''' <summary>
''' Adjuntar Especificaciones
''' </summary>
''' <param name="sTemp">ruta archivos</param>
''' <param name="scodProve">Proveedor</param>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Sub AdjuntarEspecificaciones(ByVal sTemp As String, ByVal scodProve As String)
Dim oEsp As CEspecificacion
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim oregla As CReglaSubasta
Dim bCargar As Boolean
Dim sProve As String

    'Primero adjuntamos las del proceso
    If frmOFEPet.oProcesoSeleccionado.especificaciones Is Nothing Then frmOFEPet.oProcesoSeleccionado.CargarTodasLasEspecificaciones
    If frmOFEPet.oProcesoSeleccionado.especificaciones.Count > 0 Then
        For Each oEsp In frmOFEPet.oProcesoSeleccionado.especificaciones
            frmESPERA.ProgressBar1.Value = 6
            frmESPERA.lblDetalle = sIdioma(17) & " " & oEsp.nombre
            frmESPERA.lblDetalle.Refresh
            DoEvents
            EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspProceso

            sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
            ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
            Nombres(UBound(Nombres)) = oEsp.nombre
            ReDim Preserve Nombres(UBound(Nombres) + 1)
        Next
    End If
    'Ahora adjuntamos las de los items del proceso
    If frmOFEPet.oProcesoSeleccionado.Grupos Is Nothing Then frmOFEPet.oProcesoSeleccionado.CargarGrupos bSoloAbiertos:=True
    sProve = scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
    For Each oGrupo In frmOFEPet.oProcesoSeleccionado.Grupos
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            If m_oAsigs.Item(sProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                bCargar = False
            End If
        End If
        If bCargar Then
            If oGrupo.especificaciones Is Nothing Then
                oGrupo.CargarTodasLasEspecificaciones
            ElseIf oGrupo.especificaciones.Count = 0 Then
                oGrupo.CargarTodasLasEspecificaciones
            End If
            If oGrupo.especificaciones.Count > 0 Then
                For Each oEsp In oGrupo.especificaciones
                    frmESPERA.ProgressBar1.Value = 7
                    frmESPERA.lblDetalle = sIdioma(17) & " " & oEsp.nombre
                    frmESPERA.lblDetalle.Refresh
                    DoEvents
                    EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspGrupo
    
                    sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
                    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                    Nombres(UBound(Nombres)) = oEsp.nombre
                    ReDim Preserve Nombres(UBound(Nombres) + 1)
                Next
            End If
            If gParametrosGenerales.gbMultiMaterial And bPubMatProve Then
                oGrupo.CargarTodosLosItems udtcriterioOrdenacion:=OrdItemPorOrden, bSoloConfirmados:=True, TipoComunicacion:=1, bPubMatProve:=bPubMatProve, CodProvePet:=scodProve
            Else
                If oGrupo.Items Is Nothing Then oGrupo.CargarTodosLosItems udtcriterioOrdenacion:=OrdItemPorOrden, bSoloConfirmados:=True, TipoComunicacion:=1
            End If
            For Each oItem In oGrupo.Items
                If oItem.especificaciones Is Nothing Then
                    oItem.CargarTodasLasEspecificaciones
                ElseIf oItem.especificaciones.Count = 0 Then
                    oItem.CargarTodasLasEspecificaciones
                End If
                If oItem.especificaciones.Count > 0 Then
                    For Each oEsp In oItem.especificaciones
                        frmESPERA.ProgressBar1.Value = 8
                        frmESPERA.lblDetalle = sIdioma(17) & " " & oEsp.nombre
                        frmESPERA.lblDetalle.Refresh
                        DoEvents
                        EscribirEspecificacionADisco oEsp, TipoEspecificacion.EspItem
    
                        sayFileNames(UBound(sayFileNames)) = sTemp & oEsp.nombre
                        ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                        Nombres(UBound(Nombres)) = oEsp.nombre
                        ReDim Preserve Nombres(UBound(Nombres) + 1)
    
                    Next
                End If
            Next
        End If
    Next
    
    'Si es un proceso en modo subasta adjuntar las reglas
    If frmOFEPet.oProcesoSeleccionado.ModoSubasta Then
        If frmOFEPet.oProcesoSeleccionado.ReglasSubasta Is Nothing Then frmOFEPet.oProcesoSeleccionado.CargarReglasSubasta
        For Each oregla In frmOFEPet.oProcesoSeleccionado.ReglasSubasta
            frmESPERA.ProgressBar1.Value = 10
            frmESPERA.lblDetalle = sIdioma(22) & " " & oregla.Nom
            frmESPERA.lblDetalle.Refresh
            DoEvents
            EscribirReglaADisco oregla

            sayFileNames(UBound(sayFileNames)) = sTemp & oregla.Nom
            ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
            Nombres(UBound(Nombres)) = oregla.Nom
            ReDim Preserve Nombres(UBound(Nombres) + 1)
        Next
    End If

End Sub

''' <summary>Adjuntar Excel</summary>
''' <param name="oPet">Comunicaci�n</param>
''' <remarks>Llamada desde: cmdAceptar_click ; Tiempo m�ximo: 0,2</remarks>
Private Sub AdjuntarExcel(ByVal oPet As CPetOferta)
    Dim sTemp As String
    Dim sTarget As String
    Dim docPetExcel As Object
    
    frmESPERA.ProgressBar1.Value = 9
    frmESPERA.lblDetalle = sIdioma(21)
    frmESPERA.lblDetalle.Refresh
    DoEvents
    
    sTemp = DevolverPathFichTemp
    sTarget = frmOFEPet.oProcesoSeleccionado.Anyo & "_" & frmOFEPet.oProcesoSeleccionado.GMN1Cod & "_" & frmOFEPet.oProcesoSeleccionado.Cod & "_" & oPet.CodProve & ".xls"
    Set docPetExcel = GenerarExcel(oPet, sTemp & sTarget)
    If Not docPetExcel Is Nothing Then
        sayFileNames(UBound(sayFileNames)) = sTemp & sTarget
        ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
        Nombres(UBound(Nombres)) = sTarget
        ReDim Preserve Nombres(UBound(Nombres) + 1)
    End If
    Set docPetExcel = Nothing
End Sub

