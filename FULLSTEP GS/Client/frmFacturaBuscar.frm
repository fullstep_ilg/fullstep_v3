VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFacturaBuscar 
   BackColor       =   &H00808000&
   Caption         =   "DBuscar factura"
   ClientHeight    =   5940
   ClientLeft      =   4470
   ClientTop       =   3180
   ClientWidth     =   9780
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFacturaBuscar.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5940
   ScaleWidth      =   9780
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab sstabFacturas 
      Height          =   5805
      Left            =   60
      TabIndex        =   13
      Top             =   90
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   10239
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BackColor       =   8421376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DDatos de busqueda"
      TabPicture(0)   =   "frmFacturaBuscar.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraCriterios"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "DFacturas"
      TabPicture(1)   =   "frmFacturaBuscar.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdCancelar"
      Tab(1).Control(1)=   "cmdSel"
      Tab(1).Control(2)=   "sdbgFacturas"
      Tab(1).ControlCount=   3
      Begin VB.Frame fraCriterios 
         Height          =   5265
         Left            =   150
         TabIndex        =   17
         Top             =   390
         Width           =   9345
         Begin VB.Frame fraCriterios2 
            BorderStyle     =   0  'None
            Height          =   3345
            Left            =   150
            TabIndex        =   27
            Top             =   1770
            Width           =   8955
            Begin VB.CommandButton cmdArticulo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   8250
               Picture         =   "frmFacturaBuscar.frx":0182
               Style           =   1  'Graphical
               TabIndex        =   41
               Top             =   1260
               Width           =   315
            End
            Begin VB.CommandButton cmdLimpiarCCoste 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   8250
               Picture         =   "frmFacturaBuscar.frx":020F
               Style           =   1  'Graphical
               TabIndex        =   40
               Top             =   1650
               Width           =   315
            End
            Begin VB.CommandButton cmdLimpiarContrato 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   8250
               Picture         =   "frmFacturaBuscar.frx":02B4
               Style           =   1  'Graphical
               TabIndex        =   39
               Top             =   2040
               Width           =   315
            End
            Begin VB.CommandButton cmdCCoste 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   8610
               Picture         =   "frmFacturaBuscar.frx":0359
               Style           =   1  'Graphical
               TabIndex        =   38
               Top             =   1650
               Width           =   315
            End
            Begin VB.CommandButton cmdContrato 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   8610
               Picture         =   "frmFacturaBuscar.frx":03C5
               Style           =   1  'Graphical
               TabIndex        =   37
               Top             =   2040
               Width           =   315
            End
            Begin VB.TextBox txtImpDesde 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   4740
               TabIndex        =   36
               Top             =   90
               Width           =   1275
            End
            Begin VB.TextBox txtImpHasta 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   7290
               TabIndex        =   35
               Top             =   90
               Width           =   1275
            End
            Begin VB.TextBox txtAlbaran 
               Height          =   285
               Left            =   1290
               TabIndex        =   34
               Top             =   480
               Width           =   1815
            End
            Begin VB.TextBox txtNumERP 
               Height          =   285
               Left            =   6270
               TabIndex        =   33
               Top             =   870
               Width           =   2295
            End
            Begin VB.TextBox txtArticulo 
               Height          =   285
               Left            =   1290
               TabIndex        =   32
               Top             =   1260
               Width           =   6885
            End
            Begin VB.TextBox txtCCoste 
               Height          =   285
               Left            =   1290
               TabIndex        =   31
               Top             =   1650
               Width           =   6885
            End
            Begin VB.TextBox txtContrato 
               Height          =   285
               Left            =   1290
               TabIndex        =   30
               Top             =   2040
               Width           =   6885
            End
            Begin VB.TextBox txtCesta 
               Height          =   285
               Left            =   2220
               TabIndex        =   29
               Top             =   870
               Width           =   915
            End
            Begin VB.TextBox txtPedido 
               Height          =   285
               Left            =   3165
               TabIndex        =   28
               Top             =   870
               Width           =   915
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEstado 
               Height          =   285
               Left            =   1290
               TabIndex        =   42
               Top             =   90
               Width           =   1815
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGestor 
               Height          =   285
               Left            =   4740
               TabIndex        =   43
               Top             =   480
               Width           =   3825
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6747
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
               Height          =   285
               Left            =   1290
               TabIndex        =   44
               Top             =   870
               Width           =   915
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   1693
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   1614
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBGrid sdbgPartidas 
               Height          =   855
               Left            =   1290
               TabIndex        =   45
               Top             =   2460
               Visible         =   0   'False
               Width           =   7260
               _Version        =   196617
               DataMode        =   2
               RecordSelectors =   0   'False
               GroupHeaders    =   0   'False
               ColumnHeaders   =   0   'False
               Col.Count       =   5
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowColumnSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               ForeColorEven   =   0
               BackColorEven   =   14811135
               BackColorOdd    =   14811135
               RowHeight       =   423
               Columns.Count   =   5
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(0).Locked=   -1  'True
               Columns(1).Width=   2381
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(1).Locked=   -1  'True
               Columns(1).HasBackColor=   -1  'True
               Columns(1).BackColor=   12632256
               Columns(2).Width=   9895
               Columns(2).Caption=   "VALOR"
               Columns(2).Name =   "VALOR"
               Columns(2).AllowSizing=   0   'False
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).Locked=   -1  'True
               Columns(2).Style=   1
               Columns(2).ButtonsAlways=   -1  'True
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "POSICION"
               Columns(3).Name =   "POSICION"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   3200
               Columns(4).Visible=   0   'False
               Columns(4).Caption=   "NIVEL_IMPUTACION"
               Columns(4).Name =   "NIVEL_IMPUTACION"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               _ExtentX        =   12806
               _ExtentY        =   1508
               _StockProps     =   79
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblEstado 
               Caption         =   "Destado:"
               Height          =   255
               Left            =   30
               TabIndex        =   55
               Top             =   90
               Width           =   1500
            End
            Begin VB.Label lblAlbaran 
               Caption         =   "DAlbar�n:"
               Height          =   255
               Left            =   30
               TabIndex        =   54
               Top             =   480
               Width           =   1500
            End
            Begin VB.Label lblNPedido 
               Caption         =   "DPedido:"
               Height          =   255
               Left            =   30
               TabIndex        =   53
               Top             =   870
               Width           =   1500
            End
            Begin VB.Label lblArticulo 
               Caption         =   "DArt�culo:"
               Height          =   255
               Left            =   30
               TabIndex        =   52
               Top             =   1260
               Width           =   1500
            End
            Begin VB.Label lblCCoste 
               Caption         =   "DC. coste:"
               Height          =   255
               Left            =   30
               TabIndex        =   51
               Top             =   1650
               Width           =   1500
            End
            Begin VB.Label lblContrato 
               Caption         =   "DContrato:"
               Height          =   255
               Left            =   30
               TabIndex        =   50
               Top             =   2040
               Width           =   1500
            End
            Begin VB.Label lblImpdesde 
               Caption         =   "DImporte desde:"
               Height          =   255
               Left            =   3300
               TabIndex        =   49
               Top             =   90
               Width           =   1500
            End
            Begin VB.Label lblImpHasta 
               Caption         =   "DHasta:"
               Height          =   255
               Left            =   6600
               TabIndex        =   48
               Top             =   90
               Width           =   990
            End
            Begin VB.Label lblGestor 
               Caption         =   "DGestor:"
               Height          =   255
               Left            =   3300
               TabIndex        =   47
               Top             =   480
               Width           =   1500
            End
            Begin VB.Label lblNERP 
               Caption         =   "DRef. o Num ERP:"
               Height          =   255
               Left            =   4740
               TabIndex        =   46
               Top             =   870
               Width           =   1500
            End
         End
         Begin VB.Frame fraFactERP 
            BorderStyle     =   0  'None
            Height          =   435
            Left            =   150
            TabIndex        =   18
            Top             =   1380
            Width           =   8985
            Begin VB.CommandButton cmdFContaDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6060
               Picture         =   "frmFacturaBuscar.frx":0431
               Style           =   1  'Graphical
               TabIndex        =   23
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   90
               Width           =   315
            End
            Begin VB.CommandButton cmdFContaHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   8610
               Picture         =   "frmFacturaBuscar.frx":09BB
               Style           =   1  'Graphical
               TabIndex        =   22
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   90
               Width           =   315
            End
            Begin VB.TextBox txtFactERP 
               Height          =   285
               Left            =   1290
               TabIndex        =   21
               Top             =   90
               Width           =   1815
            End
            Begin VB.TextBox txtFContaDesde 
               Height          =   285
               Left            =   4740
               TabIndex        =   20
               Top             =   90
               Width           =   1275
            End
            Begin VB.TextBox txtFContaHasta 
               Height          =   285
               Left            =   7290
               TabIndex        =   19
               Top             =   90
               Width           =   1275
            End
            Begin VB.Label lblFactERP 
               Caption         =   "DFactura ERP:"
               Height          =   255
               Left            =   30
               TabIndex        =   26
               Top             =   120
               Width           =   1500
            End
            Begin VB.Label lblFContaDesde 
               Caption         =   "DF. contabilizaci�n:"
               Height          =   255
               Left            =   3300
               TabIndex        =   25
               Top             =   120
               Width           =   1500
            End
            Begin VB.Label lblFContaHasta 
               Caption         =   "DHasta:"
               Height          =   255
               Left            =   6600
               TabIndex        =   24
               Top             =   90
               Width           =   1080
            End
         End
         Begin VB.CommandButton cmdFFactHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8760
            Picture         =   "frmFacturaBuscar.frx":0F45
            Style           =   1  'Graphical
            TabIndex        =   12
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   1080
            Width           =   315
         End
         Begin VB.TextBox txtFFactDesde 
            Height          =   285
            Left            =   4890
            TabIndex        =   8
            Top             =   1080
            Width           =   1275
         End
         Begin VB.TextBox txtFFactHasta 
            Height          =   285
            Left            =   7440
            TabIndex        =   11
            Top             =   1080
            Width           =   1275
         End
         Begin VB.TextBox txtNFactura 
            Height          =   285
            Left            =   1440
            TabIndex        =   6
            Top             =   1080
            Width           =   1815
         End
         Begin VB.CommandButton cmdAyuda 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   8940
            Picture         =   "frmFacturaBuscar.frx":14CF
            Style           =   1  'Graphical
            TabIndex        =   2
            Top             =   240
            Width           =   225
         End
         Begin VB.CommandButton cmdFFactDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6210
            Picture         =   "frmFacturaBuscar.frx":1700
            Style           =   1  'Graphical
            TabIndex        =   9
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   1080
            Width           =   315
         End
         Begin VB.TextBox txtEmpresa 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   4
            Top             =   570
            Width           =   7275
         End
         Begin VB.TextBox txtProveedor 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   1
            Top             =   240
            Width           =   7275
         End
         Begin VB.Label lblFFactHasta 
            Caption         =   "DHasta:"
            Height          =   255
            Left            =   6750
            TabIndex        =   10
            Top             =   1110
            Width           =   960
         End
         Begin VB.Label lblFFactDesde 
            Caption         =   "DFecha factura:"
            Height          =   255
            Left            =   3450
            TabIndex        =   7
            Top             =   1110
            Width           =   1500
         End
         Begin VB.Label lblNFactura 
            Caption         =   "DN�m factura:"
            Height          =   255
            Left            =   180
            TabIndex        =   5
            Top             =   1110
            Width           =   1500
         End
         Begin VB.Label lblEmpresa 
            Caption         =   "DEmpresa:"
            Height          =   255
            Left            =   180
            TabIndex        =   3
            Top             =   600
            Width           =   1500
         End
         Begin VB.Label lblProveedor 
            Caption         =   "DProveedor:"
            Height          =   255
            Left            =   180
            TabIndex        =   0
            Top             =   270
            Width           =   1230
         End
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cerrar"
         Height          =   345
         Left            =   -70020
         TabIndex        =   14
         Top             =   4530
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdSel 
         Caption         =   "&Seleccionar"
         Height          =   345
         Left            =   -71205
         TabIndex        =   15
         Top             =   4530
         Visible         =   0   'False
         Width           =   1005
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgFacturas 
         Height          =   3885
         Left            =   -74880
         TabIndex        =   16
         Top             =   510
         Width           =   9420
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   6
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmFacturaBuscar.frx":1C8A
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmFacturaBuscar.frx":1CA6
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   3200
         Columns(0).Caption=   "DN�mero de factura"
         Columns(0).Name =   "NUMFAC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DFecha factura"
         Columns(1).Name =   "FECFAC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "DContabilizaci�n"
         Columns(2).Name =   "CONTA"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "DImporte"
         Columns(3).Name =   "IMP"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Caption=   "DEstado"
         Columns(4).Name =   "EST"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "IDFAC"
         Columns(5).Name =   "IDFAC"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         _ExtentX        =   16616
         _ExtentY        =   6853
         _StockProps     =   79
         BackColor       =   12632256
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmFacturaBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnSep As Integer = 60
Private Const cnMinWidth As Long = 9900
Private Const cnMinHeight As Long = 4200

Public g_sProveCod As String
Public g_sProveDen As String
Public g_sEmpresaCod As String
Public g_lEmpresaId As String
Public g_sEmpresaDen As String
Public g_bRMat As Boolean
Public g_oCenCos As CCentroCoste
Public g_oPartida As CContratopres
Public g_sNumFac As String
Public g_lIDFac As Long

Private m_sArticulo As String
Private m_sFFactDesde As String
Private m_sFContaDesde As String
Private m_sHasta As String
Private m_sEstado As String
Private m_sImporte As String
Private m_sImpDesde As String
Private m_sGestor As String
Private m_sPedido As String

Private m_lMinHeight As Long
Private m_bMostrarPartidas As Boolean
Private m_bAccesoFSSM As Boolean
Private m_bSaltarCCoste_Validate As Boolean
Private m_bControlPartidaFiltro As Boolean
Private m_iNivelImputacion As Integer
Private m_arrPartidas() As String

Private Sub cmdArticulo_Click()
    Dim oArt As CArticulo
    
    frmPROCEBuscarArticulo.sOrigen = "frmFacturaBuscar"
    frmPROCEBuscarArticulo.bModoEdicion = True
    frmPROCEBuscarArticulo.g_bPermProcMultimaterial = False
    
    'Se accede en modo consulta
    frmPROCEBuscarArticulo.GMN1Cod = ""
    frmPROCEBuscarArticulo.GMN2Cod = ""
    frmPROCEBuscarArticulo.GMN3Cod = ""
    frmPROCEBuscarArticulo.GMN4Cod = ""
    frmPROCEBuscarArticulo.GMN4Den = ""
    frmPROCEBuscarArticulo.CodArticulo = ""
    frmPROCEBuscarArticulo.DenArticulo = ""
    
    'Cambiar el BorderStyle. Reasignar la propiedad camption obliga a que se repinte el borde
    frmPROCEBuscarArticulo.BorderStyle = 2
    frmPROCEBuscarArticulo.caption = frmPROCEBuscarArticulo.caption
    frmPROCEBuscarArticulo.Show 1
    
    If frmPROCEBuscarArticulo.g_bSeleccionar Then
        Set oArt = frmPROCEBuscarArticulo.g_oArticuloSeleccionado
        txtArticulo.Text = oArt.Cod & " - " & oArt.Den
        Set oArt = Nothing
    End If
    
    Unload frmPROCEBuscarArticulo
End Sub

Private Sub cmdAyuda_Click()
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub cmdCCoste_Click()
    Dim sCod As String
    
    frmSelCenCoste.g_sOrigen = "frmFacturaBuscar"
    frmSelCenCoste.g_bCentrosSM = False
    frmSelCenCoste.g_lIdEmpresa = g_lEmpresaId
    frmSelCenCoste.g_bSaltarComprobacionArbol = True
    Set frmSelCenCoste.g_oOrigen = Me
    frmSelCenCoste.Show 1

    m_bSaltarCCoste_Validate = True

    If Not g_oCenCos Is Nothing Then
        If Not NoHayParametro(g_oCenCos.UON4) Then
            sCod = g_oCenCos.UON4
        ElseIf Not NoHayParametro(g_oCenCos.UON3) Then
            sCod = g_oCenCos.UON3
        ElseIf Not NoHayParametro(g_oCenCos.UON2) Then
            sCod = g_oCenCos.UON2
        ElseIf Not NoHayParametro(g_oCenCos.UON1) Then
            sCod = g_oCenCos.UON1
        End If
        If sCod <> "" Then
            If g_oCenCos.CodConcat <> "" Then
                txtCCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
            Else
                txtCCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            End If
        End If
    Else
        txtCCoste.Text = ""
    End If
    
    m_bSaltarCCoste_Validate = False
End Sub

Private Sub cmdContrato_Click()
    Dim sPres0 As String
    
    sPres0 = cmdContrato.Tag
    
    Set frmSelCContables.g_oPartida = Nothing
    Set frmSelCContables.g_oCenCos = Nothing
    frmSelCContables.g_sPres0 = sPres0
    frmSelCContables.g_iNivel = g_oParametrosSM.Item(sPres0).impnivel
    
    If Not g_oCenCos Is Nothing Then
        If txtCCoste.Text <> "" Then
            Set frmSelCContables.g_oCenCos = g_oCenCos
        End If
    End If
    
    Set frmSelCContables.g_oOrigen = Me
    frmSelCContables.g_sOrigen = "frmFacturaBuscar"
    frmSelCContables.Show vbModal
    
    If Not g_oCenCos Is Nothing Then
        If g_oCenCos.UON1 <> "" And g_oCenCos.Cod <> "" Then
            If g_oCenCos.UON4 <> "" Then
                g_oCenCos.CodConcat = g_oCenCos.UON1 & "-" & g_oCenCos.UON2 & "-" & g_oCenCos.UON3
            ElseIf g_oCenCos.UON3 <> "" Then
                g_oCenCos.CodConcat = g_oCenCos.UON1 & "-" & g_oCenCos.UON2
            ElseIf g_oCenCos.UON2 <> "" Then
                g_oCenCos.CodConcat = g_oCenCos.UON1
            End If
            
            If g_oCenCos.CodConcat <> "" Then
                txtCCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & "(" & g_oCenCos.CodConcat & ")"
            Else
                txtCCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            End If
        End If
        
        Set frmSelCContables.g_oCenCos = Nothing
    End If
    
    If Not g_oPartida Is Nothing Then
        If g_oPartida.Cod <> "" Then
            txtContrato.Text = g_oPartida.Cod & " - " & g_oPartida.Den
            m_bControlPartidaFiltro = True
            txtContrato_Validate True
        End If
    End If
End Sub

Private Sub cmdFContaDesde_Click()
    AbrirSeleccionFecha txtFContaDesde
End Sub

Private Sub cmdFContaHasta_Click()
    AbrirSeleccionFecha txtFContaHasta
End Sub

Private Sub cmdFFactDesde_Click()
    AbrirSeleccionFecha txtFFactDesde
End Sub

''' <summary>Abre la pantalla de selecci�n de fecha</summary>
''' <remarks>Llamada desde: cmdFFactDesde_Click</remarks>
''' <revision>LTG 26/06/2012</revision>

Private Sub AbrirSeleccionFecha(ByRef txtDestination As TextBox)
    AbrirFormCalendar Me, txtDestination
End Sub

Private Sub cmdFFactHasta_Click()
    AbrirSeleccionFecha txtFFactHasta
End Sub

Private Sub cmdLimpiarCCoste_Click()
    txtCCoste.Text = ""
    Set g_oCenCos = Nothing
End Sub

Private Sub cmdLimpiarContrato_Click()
    txtContrato.Text = ""
    m_arrPartidas(0) = ""
End Sub

Private Sub cmdSel_Click()
    With sdbgFacturas
        If .SelBookmarks.Count > 0 Then
            g_sNumFac = .Columns("NUMFAC").Value
            g_lIDFac = .Columns("IDFAC").Value
            
            Me.Hide
        End If
    End With
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>Funci�n de carga</summary>
''' <remarks>Llamada desde: frmFacturaBuscar.sstabProve_Click; Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 26/06/2012</revision>

Private Sub Cargar()
    Dim oFacturas As CFacturas
    Dim oFactura As cFactura
    Dim sImporte As String
    Dim sArtCod As String
    Dim lPos As Long
    Dim sUONCC As String
    
    Set oFacturas = oFSGSRaiz.Generar_CFacturas
    
    With sdbgFacturas
        .RemoveAll
        
        If txtArticulo.Text <> "" Then
            lPos = InStr(1, txtArticulo.Text, " - ")
            If lPos <> 0 Then
                sArtCod = Left(txtArticulo.Text, lPos - 1)
            Else
                sArtCod = txtArticulo.Text
            End If
        End If
        
        If Not g_oCenCos Is Nothing Then
            sUONCC = g_oCenCos.UON1 & "-" & g_oCenCos.UON2 & "-" & g_oCenCos.UON3 & "-" & g_oCenCos.UON4
        End If
        
        oFacturas.CargarFacturas basPublic.gParametrosInstalacion.gIdioma, txtNFactura.Text, txtFFactDesde.Text, txtFFactHasta.Text, txtFContaDesde.Text, _
            txtFContaHasta.Text, txtFactERP.Text, sdbcEstado.Value, txtImpDesde.Text, txtImpHasta.Text, txtAlbaran.Text, sdbcGestor.Value, sdbcAnyo.Value, _
            txtCesta.Text, txtPedido.Text, txtNumERP.Text, sArtCod, sUONCC, m_arrPartidas
        If oFacturas.Count > 0 Then
            For Each oFactura In oFacturas
                If NullToStr(oFactura.importe) <> "" Then
                    sImporte = NullToStr(oFactura.importe) & " " & NullToStr(oFactura.Moneda)
                Else
                    sImporte = ""
                End If
                
                .AddItem oFactura.Numero & Chr(m_lSeparador) & oFactura.Fecha & Chr(m_lSeparador) & oFactura.FechaConta & Chr(m_lSeparador) & sImporte & _
                    Chr(m_lSeparador) & oFactura.EstadoDen & Chr(m_lSeparador) & oFactura.IdFactura
            Next
        End If
    End With
    
    Set oFacturas = Nothing
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmFacturaBuscar.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FACTURA_BUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        With Ador
            Me.caption = Ador(0).Value
            .MoveNext
            sstabFacturas.TabCaption(0) = Ador(0).Value
            .MoveNext
            sstabFacturas.TabCaption(1) = Ador(0).Value
            .MoveNext
            lblProveedor.caption = Ador(0).Value & ":"
            .MoveNext
            lblEmpresa.caption = Ador(0).Value & ":"
            .MoveNext
            lblNFactura.caption = Ador(0).Value & ":"
            .MoveNext
            lblFFactDesde.caption = Ador(0).Value & ":"
            sdbgFacturas.Columns("FECFAC").caption = Ador(0).Value
            m_sFFactDesde = Ador(0).Value
            .MoveNext
            lblFFactHasta.caption = Ador(0).Value & ":"
            lblFContaHasta.caption = Ador(0).Value & ":"
            m_sHasta = Ador(0).Value
            lblImpHasta.caption = Ador(0).Value & ":"
            .MoveNext
            lblFactERP.caption = Ador(0).Value & ":"
            .MoveNext
            lblFContaDesde.caption = Ador(0).Value & ":"
            m_sFContaDesde = Ador(0).Value
            .MoveNext
            lblEstado.caption = Ador(0).Value & ":"
            sdbgFacturas.Columns("EST").caption = Ador(0).Value
            m_sEstado = Ador(0).Value
            .MoveNext
            lblImpdesde.caption = Ador(0).Value & ":"
            m_sImpDesde = Ador(0).Value
            .MoveNext
            lblAlbaran.caption = Ador(0).Value & ":"
            .MoveNext
            lblGestor.caption = Ador(0).Value & ":"
            m_sGestor = Ador(0).Value
            .MoveNext
            lblNPedido.caption = Ador(0).Value & ":"
            m_sPedido = Ador(0).Value
            .MoveNext
            lblNERP.caption = Ador(0).Value & ":"
            .MoveNext
            lblArticulo.caption = Ador(0).Value & ":"
            m_sArticulo = Ador(0).Value
            .MoveNext
            lblCCoste.caption = Ador(0).Value & ":"
            .MoveNext
            lblContrato.caption = Ador(0).Value & ":"
            .MoveNext
            cmdSel.caption = Ador(0).Value
            .MoveNext
            cmdCancelar.caption = Ador(0).Value
            .MoveNext
            sdbgFacturas.Columns("NUMFAC").caption = Ador(0).Value
            .MoveNext
            sdbgFacturas.Columns("CONTA").caption = Ador(0).Value
            .MoveNext
            sdbgFacturas.Columns("IMP").caption = Ador(0).Value
            m_sImporte = Ador(0).Value
            
            .Close
        End With
    End If
    
    Set Ador = Nothing
End Sub
        
Private Sub Form_Load()
    m_bAccesoFSSM = (gParametrosGenerales.gsAccesoFSSM = AccesoFSSM)
    
    CargarRecursos
    PonerFieldSeparator Me
    
    CargarAnyos
    CargarEstados
    CargarGestores
    
    ConfiguracionBusqueda
    If m_bMostrarPartidas Then
        CargarPartidas
    Else
        ReDim m_arrPartidas(0)
    End If
    
    Me.Height = m_lMinHeight
    
    txtProveedor.Text = g_sProveCod & " - " & g_sProveDen
    txtEmpresa.Text = g_sEmpresaCod & " - " & g_sEmpresaDen
    
    sstabFacturas.Tab = 0
End Sub

''' <summary>Carga el combo de a�os</summary>
''' <remarks>Llamada desde=Form_load</remarks>
''' <revision>LTG 26/06/2012</revision>

Private Sub CargarAnyos()
    Dim iAnyoActual As Integer
    Dim i As Integer

    iAnyoActual = Year(Date)
    sdbcAnyo.AddItem ""
    For i = iAnyoActual - 10 To iAnyoActual + 10
        sdbcAnyo.AddItem i
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
End Sub

''' <summary>Carga el combo de estados de factura</summary>
''' <remarks>Llamada desde=Form_load</remarks>
''' <revision>LTG 26/06/2012</revision>

Private Sub CargarEstados()
    Dim rsEstados As Recordset
    Dim oFacturas As CFacturas
    
    Set oFacturas = oFSGSRaiz.Generar_CFacturas
    Set rsEstados = oFacturas.devolverTipoEstados(oUsuarioSummit.idioma)
    Set oFacturas = Nothing
    If Not rsEstados Is Nothing Then
        If rsEstados.RecordCount > 0 Then
            While Not rsEstados.EOF
                sdbcEstado.AddItem rsEstados("ID") & Chr(m_lSeparador) & rsEstados("DEN")
                rsEstados.MoveNext
            Wend
        End If
    End If
    Set rsEstados = Nothing
End Sub

''' <summary>Carga el combo de gestores con los gestores de la empresa del pedido</summary>
''' <remarks>Llamada desde=Form_load</remarks>
''' <revision>LTG 29/06/2012</revision>

Private Sub CargarGestores()
    Dim oEmpresa As CEmpresa
    Dim oGestores As CPersonas
    Dim oGestor As CPersona

    With sdbcGestor
        .RemoveAll
                
        'Obtener gestores
        Set oEmpresa = oFSGSRaiz.Generar_CEmpresa
        oEmpresa.Id = g_lEmpresaId
        Set oGestores = oEmpresa.DevolverGestoresAsociados
        Set oEmpresa = Nothing
        
        If Not oGestores Is Nothing Then
            If oGestores.Count > 0 Then
                For Each oGestor In oGestores
                    .AddItem oGestor.Cod & Chr(m_lSeparador) & oGestor.nombre & " " & oGestor.Apellidos
                Next
                Set oGestor = Nothing
                Set oGestores = Nothing
            End If
        End If
        
        .SelStart = 0
        .SelLength = Len(.Text)
        .Refresh
    End With
End Sub

''' <summary>Comprueba si hay que mostrar el grid de partidas presupuestarias</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: sstabFacturas_Click</remarks>

Private Sub ConfiguracionBusqueda()
    m_bMostrarPartidas = False
    
    lblGestor.Visible = m_bAccesoFSSM
    sdbcGestor.Visible = m_bAccesoFSSM
    lblCCoste.Visible = m_bAccesoFSSM
    txtCCoste.Visible = m_bAccesoFSSM
    cmdLimpiarCCoste.Visible = m_bAccesoFSSM
    cmdCCoste.Visible = m_bAccesoFSSM
    
    lblContrato.Visible = False
    txtContrato.Visible = False
    cmdLimpiarContrato.Visible = False
    cmdContrato.Visible = False
    sdbgPartidas.Visible = False
        
    m_lMinHeight = cnMinHeight
    
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Facturas) Then
        fraFactERP.Visible = True
        
        m_lMinHeight = m_lMinHeight + txtFactERP.Height + (2 * cnSep)
    Else
        fraFactERP.Visible = False
        fraCriterios2.Top = fraFactERP.Top
    End If
    
    If gParametrosGenerales.gbCodPersonalizDirecto Or gParametrosGenerales.gbCodPersonalizPedido Then
        lblNERP.Visible = True
        txtNumERP.Visible = True
    Else
        lblNERP.Visible = False
        txtNumERP.Visible = False
    End If
    
    If m_bAccesoFSSM Then
        lblCCoste.Visible = True
        txtCCoste.Visible = True
        cmdLimpiarCCoste.Visible = True
        cmdCCoste.Visible = True
        
        m_lMinHeight = m_lMinHeight + txtCCoste.Height + (2 * cnSep)
        
        If Not g_oParametrosSM Is Nothing Then
            If g_oParametrosSM.Count > 0 Then
                If g_oParametrosSM.Count > 1 Then
                    m_bMostrarPartidas = True
                    
                    lblContrato.Visible = False
                    txtContrato.Visible = False
                    cmdLimpiarContrato.Visible = False
                    cmdContrato.Visible = False
                    sdbgPartidas.Visible = True
                    
                    sdbgPartidas.Top = txtContrato.Top
                    
                    fraCriterios2.Height = sdbgPartidas.Top + sdbgPartidas.Height + (2 * cnSep)
                    m_lMinHeight = m_lMinHeight + sdbgPartidas.Height + (2 * cnSep)
                Else
                    lblContrato.Visible = True
                    txtContrato.Visible = True
                    cmdLimpiarContrato.Visible = True
                    cmdContrato.Visible = True
                    sdbgPartidas.Visible = False
                    
                    fraCriterios2.Height = txtContrato.Top + txtContrato.Height + (2 * cnSep)
                    m_lMinHeight = m_lMinHeight + txtContrato.Height + (2 * cnSep)
                End If
                
                fraCriterios.Height = fraCriterios2.Top + fraCriterios2.Height + (2 * cnSep)
            End If
        End If
    Else
        lblCCoste.Visible = False
        txtCCoste.Visible = False
        cmdLimpiarCCoste.Visible = False
        cmdCCoste.Visible = False
        
        fraCriterios2.Height = txtArticulo.Top + txtArticulo.Height + (2 * cnSep)
        fraCriterios.Height = fraCriterios2.Top + fraCriterios2.Height + (2 * cnSep)
    End If
End Sub

''' <summary>Carga las partidas que hay en el sistema</summary>
''' <remarks>Llamada desde=Form_load</remarks>
''' <revision>LTG 26/06/2012</revision>

Private Sub CargarPartidas()
    Dim oParamSM As CParametroSM
    Dim opres5 As cPresConceptos5Nivel0
    Dim sUsu As String
    Dim iPosicion As Integer

    Set opres5 = oFSGSRaiz.Generar_CPresConceptos5Nivel0

    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        sUsu = basOptimizacion.gvarCodUsuario
    End If

    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 0 Then
            ReDim m_arrPartidas(g_oParametrosSM.Count)
            
            If g_oParametrosSM.Count > 1 Then
                cmdContrato.Tag = ""
                                
                For Each oParamSM In g_oParametrosSM
                    sdbgPartidas.AddItem oParamSM.PRES5 & Chr(m_lSeparador) & oParamSM.NomNivelImputacion & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & iPosicion & Chr(m_lSeparador) & oParamSM.impnivel
                    iPosicion = iPosicion + 1
                Next
                Set oParamSM = Nothing
'                ConfiguracionMasDe1Partida
            Else
                txtContrato.Text = g_oParametrosSM.Item(1).NomNivelImputacion & ":"
                m_iNivelImputacion = g_oParametrosSM.Item(1).impnivel
                
                cmdContrato.Tag = g_oParametrosSM.Item(1).PRES5
            End If
        End If
    End If

    Set opres5 = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_sProveCod = ""
    g_sProveDen = ""
    g_sEmpresaCod = ""
    g_lEmpresaId = 0
    g_sEmpresaDen = ""
    
    Set g_oCenCos = Nothing

    sdbgFacturas.SelBookmarks.RemoveAll
End Sub

Private Sub sdbcEstado_Change()
    If sdbcEstado.Text = "" Then sdbcEstado.Value = ""
End Sub

Private Sub sdbcEstado_CloseUp()
    If Trim(sdbcEstado.Value) = "" Then
        sdbcEstado.Text = ""
        Exit Sub
    End If
        
    sdbcEstado.Text = sdbcEstado.Columns("DEN").Text
End Sub

Private Sub sdbcEstado_InitColumnProps()
    sdbcEstado.DataFieldList = "Column 0"
    sdbcEstado.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcEstado_PositionList(ByVal Text As String)
    Dim i As Long
    Dim vbm As Variant

    On Error Resume Next
    
    With sdbcEstado
        .MoveFirst
        
        If Text <> "" Then
            For i = 0 To .Rows - 1
                vbm = .GetBookmark(i)
                If UCase(.Text) = UCase(Mid(.Columns("DEN").CellText(vbm), 1, Len(.Text))) Then
                    .Bookmark = vbm
                    Exit For
                End If
            Next i
        Else
            .Value = ""
        End If
    End With
End Sub

Private Sub sdbcEstado_Validate(Cancel As Boolean)
    ValidarEstado
End Sub

''' <summary>Valida el valor del estado</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: sdbcEstado_Validate</remarks>

Private Function ValidarEstado() As Boolean
    Dim i As Integer
    Dim vbm As Variant
    
    ValidarEstado = True
    
    With sdbcEstado
        If .Text <> "" Then
            For i = 0 To .Rows - 1
                vbm = .GetBookmark(i)
                If UCase(.Text) = UCase(Mid(.Columns("DEN").CellText(vbm), 1, Len(.Text))) Or UCase(.Text) = UCase(.Columns("COD").CellText(vbm)) Then
                    .Bookmark = vbm
                    .Value = .Columns("COD").CellText(vbm)
                    .Text = .Columns("DEN").CellText(vbm)
                    Exit Function
                End If
            Next
            
            ValidarEstado = False
            oMensajes.NoValida " " & m_sEstado
            .Text = ""
        Else
            .Value = ""
        End If
    End With
End Function

Private Sub sdbcGestor_InitColumnProps()
    sdbcGestor.DataFieldList = "Column 0"
    sdbcGestor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcGestor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim vbm As Variant

    On Error Resume Next
    
    With sdbcGestor
        .MoveFirst
        
        If Text <> "" Then
            For i = 0 To .Rows - 1
                vbm = .GetBookmark(i)
                If UCase(.Text) = UCase(Mid(.Columns("DEN").CellText(vbm), 1, Len(.Text))) Then
                    .Bookmark = vbm
                    Exit For
                End If
            Next i
        Else
            .Value = ""
        End If
    End With
End Sub

Private Sub sdbcGestor_Validate(Cancel As Boolean)
    Cancel = Not ValidarGestor
End Sub

''' <summary>Valida el gestor introducido</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: sdbcGestor_Validate</remarks>
''' <revision>LTG 04/07/2012</revision>

Private Function ValidarGestor() As Boolean
    Dim i As Integer
    Dim vbm As Variant
    
    ValidarGestor = True

    With sdbcGestor
        If .Text <> "" Then
            For i = 0 To .Rows - 1
                vbm = .GetBookmark(i)
                If UCase(.Text) = UCase(Mid(.Columns("DEN").CellText(vbm), 1, Len(.Text))) Or UCase(.Text) = UCase(.Columns("COD").CellText(vbm)) Then
                    .Bookmark = vbm
                    .Value = .Columns("COD").CellText(vbm)
                    .Text = .Columns("DEN").CellText(vbm)
                    Exit Function
                End If
            Next
            
            ValidarGestor = False
            oMensajes.NoValida " " & m_sGestor
            .Text = ""
        Else
            .Value = ""
        End If
    End With
End Function

Private Sub sdbgFacturas_DblClick()
    sdbgFacturas.SelBookmarks.Add sdbgFacturas.AddItemBookmark(sdbgFacturas.Row)
    cmdSel_Click
End Sub

Private Sub sdbgPartidas_BeforeRowColChange(Cancel As Integer)
    If Not m_bControlPartidaFiltro Then Exit Sub
    
    With sdbgPartidas
        .Columns("VALOR").Value = ValidarPartida(.Columns("COD").Value, .Columns("VALOR").Value, .Columns("NIVEL_IMPUTACION").Value, .Columns("POSICION").Value)
    End With
    
    m_bControlPartidaFiltro = False
End Sub

''' <summary>Valida lla partida introducida</summary>
''' <param name="sPres0">Pres0</param>
''' <param name="sContrato">Contrato</param>
''' <param name="iNivelImputacion">Nivel de imputaci�n</param>
''' <param name="iIndice">Indice en m_arrPartidas</param>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: sstabFacturas_Click</remarks>
''' <revision>LTG 03/07/2012</revision>

Private Function ValidarPartida(ByVal sPres0 As String, ByVal sContrato As String, ByVal iNivelImputacion As Integer, ByVal iIndice As Integer) As String
    Dim oContratosPres As CContratosPres
    Dim oCentrosCoste As CCentrosCoste
    Dim bEncontrado As Boolean
    Dim arrCC() As String
    Dim sUsu As String
    Dim sCod As String
    Dim sAux() As String
    
    sAux = Split(sContrato, " - ")
    If UBound(sAux) > 0 Then
        sCod = sAux(0)
    Else
        sCod = sContrato
    End If
    
    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        sUsu = basOptimizacion.gvarCodUsuario
    End If
    
    Set oContratosPres = oFSGSRaiz.Generar_CContratosPres
        
    ReDim arrCC(4)
    
    oContratosPres.CargarTodosLosContratosCentros sPres0, iNivelImputacion, sUsu, gParametrosInstalacion.gIdioma, arrCC(0), arrCC(1), arrCC(2), arrCC(3), sCod, , True
    If oContratosPres.Count > 0 Then
        bEncontrado = True
    End If
        
    If bEncontrado Then
        If g_oCenCos Is Nothing Then
            Set oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
            oCentrosCoste.CargarCentrosDeCoste , oContratosPres.Item(1).UON1, oContratosPres.Item(1).UON2, oContratosPres.Item(1).UON3, oContratosPres.Item(1).UON4
            Set g_oCenCos = oCentrosCoste.Item(1)
            
            If oContratosPres.Count > 0 Then
                If Not NoHayParametro(oContratosPres.Item(1).UON4) Then
                    sCod = oContratosPres.Item(1).UON4
                ElseIf Not NoHayParametro(oContratosPres.Item(1).UON3) Then
                    sCod = oContratosPres.Item(1).UON3
                ElseIf Not NoHayParametro(oContratosPres.Item(1).UON2) Then
                    sCod = oContratosPres.Item(1).UON2
                Else
                    sCod = oContratosPres.Item(1).UON1
                End If
                
                txtCCoste.Text = sCod & " - " & oContratosPres.Item(1).DenUON
            End If
        End If
        
        m_arrPartidas(iIndice) = sPres0 & "-" & NullToStr(oContratosPres.Item(1).Pres1) & "-" & NullToStr(oContratosPres.Item(1).Pres2) & "-" & NullToStr(oContratosPres.Item(1).Pres3) & "-" & NullToStr(oContratosPres.Item(1).Pres4)
        ValidarPartida = oContratosPres.Item(1).Cod & " - " & oContratosPres.Item(1).Den
    Else
        m_arrPartidas(iIndice) = ""
        ValidarPartida = ""
    End If
    
    Set oContratosPres = Nothing
End Function

Private Sub sdbgPartidas_BtnClick()
    Dim sPres0 As String
    
    sPres0 = sdbgPartidas.Columns("COD").Value
        
    Set frmSelCContables.g_oPartida = Nothing
    Set frmSelCContables.g_oCenCos = Nothing
    frmSelCContables.g_sPres0 = sPres0
    frmSelCContables.g_iNivel = g_oParametrosSM.Item(sPres0).impnivel
    If Not g_oCenCos Is Nothing Then
        If txtCCoste.Text <> "" Then
            Set frmSelCContables.g_oCenCos = g_oCenCos
        End If
    End If
    Set frmSelCContables.g_oOrigen = Me
    frmSelCContables.g_sOrigen = "frmFacturaBuscar"
    frmSelCContables.Show vbModal

    If Not g_oCenCos Is Nothing Then
        If g_oCenCos.UON1 <> "" And g_oCenCos.Cod <> "" Then
            If g_oCenCos.UON4 <> "" Then
                g_oCenCos.CodConcat = g_oCenCos.UON1 & "-" & g_oCenCos.UON2 & "-" & g_oCenCos.UON3
            ElseIf g_oCenCos.UON3 <> "" Then
                g_oCenCos.CodConcat = g_oCenCos.UON1 & "-" & g_oCenCos.UON2
            ElseIf g_oCenCos.UON2 <> "" Then
                g_oCenCos.CodConcat = g_oCenCos.UON1
            End If
           
            If g_oCenCos.CodConcat <> "" Then
                txtCCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & "(" & g_oCenCos.CodConcat & ")"
            Else
                txtCCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            End If
        End If
        
        Set frmSelCContables.g_oCenCos = Nothing
    End If
    If Not g_oPartida Is Nothing Then
        If g_oPartida.Cod <> "" Then
            sdbgPartidas.Columns("VALOR").Value = g_oPartida.Cod & " - " & g_oPartida.Den
            m_bControlPartidaFiltro = True
        End If
    End If
End Sub

Private Sub sdbgPartidas_Change()
    m_bControlPartidaFiltro = True
End Sub

Private Sub sdbgPartidas_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyBack Then
        sdbgPartidas.Columns("VALOR").Value = ""
        m_arrPartidas(sdbgPartidas.Columns("POSICION").Value) = ""
    End If
End Sub

Private Sub sstabFacturas_Click(PreviousTab As Integer)
    Screen.MousePointer = vbNormal
    
    If sstabFacturas.Tab = 0 Then
        cmdSel.Visible = False
        cmdCancelar.Visible = False
    Else
        cmdSel.Visible = True
        cmdCancelar.Visible = True
    End If
    
    If PreviousTab = 0 And sstabFacturas.Tab = 1 Then
        If Not HayFiltro Then
            sstabFacturas.Tab = 0
            
            oMensajes.FacturaSeleccioneAlgo
        ElseIf Not ValidarCriterios Then
            sstabFacturas.Tab = 0
        Else
            Screen.MousePointer = vbHourglass
            Cargar
            Screen.MousePointer = vbNormal
        End If
    End If
End Sub

''' <summary>Valida los criterios de filtrado</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: sstabFacturas_Click</remarks>
''' <revision>LTG 03/07/2012</revision>

Private Function ValidarCriterios() As Boolean
    ValidarCriterios = True
    
    If Not ValidarEstado Then
        ValidarCriterios = False
    ElseIf Not ValidarImpDesde Then
        ValidarCriterios = False
    ElseIf Not ValidarImpHasta Then
        ValidarCriterios = False
    ElseIf Not ValidarGestor Then
        ValidarCriterios = False
    ElseIf Not ValidarPedido Then
        ValidarCriterios = False
    ElseIf Not ValidarCesta Then
        ValidarCriterios = False
    End If
End Function

''' <summary>Indica si se ha seleccionado alg�n filtro</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: sstabFacturas_Click</remarks>
''' <revision>LTG 03/07/2012</revision>

Private Function HayFiltro() As Boolean
    HayFiltro = True
End Function

''' <summary>Posici�n y tama�o de los controles del formulario</summary>
''' <remarks>Llamada desde: frmFacturaBuscar.Form_Load</remarks>
''' <revision>JLTG 26/06/2012</revision>

Private Sub Arrange()
    If Me.WindowState <> vbMinimized Then
        If Me.Width >= cnMinWidth Then
            sstabFacturas.Width = Me.ScaleWidth - (2 * cnSep)
            'fraCriterios.Width = sstabFacturas.Width - (4 * cnSep)
            
            With sdbgFacturas
                .Width = sstabFacturas.Width - (4 * cnSep)
                
                .Columns("NUMFAC").Width = .Width * 0.22
                .Columns("FECFAC").Width = .Width * 0.15
                .Columns("CONTA").Width = .Width * 0.15
                .Columns("IMP").Width = .Width * 0.15
                .Columns("EST").Width = .Width * 0.27
            End With
            
            cmdSel.Left = (sdbgFacturas.Width - cmdSel.Width - cmdCancelar.Width - (5 * cnSep)) / 2
            cmdCancelar.Left = cmdSel.Left + cmdSel.Width + (3 * cnSep)
        Else
            Me.Width = cnMinWidth
        End If
        
        If Me.Height >= m_lMinHeight Then
            sstabFacturas.Height = Me.ScaleHeight - (3 * cnSep)
                        
            sdbgFacturas.Height = sstabFacturas.Height - cmdSel.Height - (11 * cnSep)
            
            cmdSel.Top = sdbgFacturas.Top + sdbgFacturas.Height + cnSep
            cmdCancelar.Top = cmdSel.Top
        Else
            Me.Height = m_lMinHeight
        End If
    End If
End Sub

Private Sub txtCCoste_Validate(Cancel As Boolean)
    Dim oCentrosCoste As CCentrosCoste
    Dim bExiste As Boolean
    Dim oParametroSM As CParametroSM
    Dim sAux() As String
    Dim sCod As String
    Dim sValor As String
    
    If m_bSaltarCCoste_Validate Then Exit Sub
    If txtCCoste.Text = "" Then Exit Sub
        
    Set oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
    
    sAux = Split(txtCCoste.Text, " - ")
    If UBound(sAux) > 0 Then
        sValor = sAux(0)
    Else
        sValor = txtCCoste.Text
    End If
    
    bExiste = False
    For Each oParametroSM In g_oParametrosSM
        oCentrosCoste.CargarTodosLosCentrosCoste oParametroSM.PRES5, oParametroSM.impnivel, basOptimizacion.gTipoDeUsuario, basOptimizacion.gvarCodUsuario, gParametrosInstalacion.gIdioma, , , , , , Trim(txtCCoste.Text)
        If oCentrosCoste.Count > 0 Then
            bExiste = True
            Exit For
        End If
    Next
      
    If bExiste Then
        Set g_oCenCos = oCentrosCoste.Item(1)
        
        If oCentrosCoste.Count > 0 Then
            If Not NoHayParametro(g_oCenCos.UON4) Then
                sCod = g_oCenCos.UON4
            ElseIf Not NoHayParametro(g_oCenCos.UON3) Then
                sCod = g_oCenCos.UON3
            ElseIf Not NoHayParametro(g_oCenCos.UON2) Then
                sCod = g_oCenCos.UON2
            Else
                sCod = g_oCenCos.UON1
            End If
                            
            txtCCoste.Text = sCod & " - " & DevolverDenCC(g_oCenCos.UON1, g_oCenCos.UON2, g_oCenCos.UON3, g_oCenCos.UON4, g_oCenCos.Den, g_oCenCos.Den, g_oCenCos.Den, g_oCenCos.Den)
        End If

    Else
        txtCCoste.Text = ""
    End If
        
    Set oCentrosCoste = Nothing
End Sub

Private Sub txtCesta_Validate(Cancel As Boolean)
    Cancel = Not ValidarCesta
End Sub

''' <summary>Valida el valor del campo cesta</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: txtCesta_Validate</remarks>
''' <revision>LTG 05/07/2012</revision>

Private Function ValidarCesta() As Boolean
    ValidarCesta = True
    
    If txtCesta.Text <> "" Then
        If Not IsNumeric(txtCesta.Text) Then
            ValidarCesta = False
            oMensajes.NoValido m_sPedido
            txtCesta.Text = ""
            If Me.Visible Then txtCesta.SetFocus
        End If
    End If
End Function

Private Sub txtContrato_Change()
    m_bControlPartidaFiltro = True
End Sub

Private Sub txtContrato_Validate(Cancel As Boolean)
    If Not m_bControlPartidaFiltro Then Exit Sub
    
    If txtContrato.Text = "" Then Exit Sub
    
    txtContrato.Text = ValidarPartida(cmdContrato.Tag, txtContrato.Text, m_iNivelImputacion, 0)
        
    m_bControlPartidaFiltro = False
End Sub

Private Sub txtFContaDesde_Validate(Cancel As Boolean)
    Cancel = Not FechaValida(txtFContaDesde, m_sFContaDesde)
End Sub

Private Sub txtFContaHasta_Validate(Cancel As Boolean)
    Cancel = Not FechaValida(txtFContaHasta, m_sFContaDesde & " " & m_sHasta)
End Sub

Private Sub txtFFactDesde_Validate(Cancel As Boolean)
    Cancel = Not FechaValida(txtFFactDesde, m_sFFactDesde)
End Sub

Private Sub txtFFactHasta_Validate(Cancel As Boolean)
    Cancel = Not FechaValida(txtFFactHasta, m_sFFactDesde & " " & m_sHasta)
End Sub

Private Function FechaValida(ByRef oControlFecha As TextBox, ByVal sMensaje As String) As Boolean
    FechaValida = True
    
    If oControlFecha.Text <> "" Then
        If Not IsDate(oControlFecha.Text) Then
            FechaValida = False
            
            oMensajes.NoValida sMensaje
            oControlFecha.Text = ""
            If Me.Visible Then oControlFecha.SetFocus
        End If
    End If
End Function

Private Sub txtImpDesde_Validate(Cancel As Boolean)
    Cancel = Not ValidarImpDesde
End Sub

''' <summary>Valida el importe desde</summary>
''' <remarks>Llamada desde: txtImpDesde_Validate</remarks>
''' <revision>LTG 03/07/2012</revision>

Private Function ValidarImpDesde() As Boolean
    ValidarImpDesde = True
    
    If txtImpDesde.Text <> "" Then
        If IsNumeric(txtImpDesde.Text) Then
            txtImpDesde.Text = Format(txtImpDesde.Text, "standard")
        Else
            ValidarImpDesde = False
            oMensajes.NoValido m_sImpDesde
            txtImpDesde.Text = ""
        End If
    End If
End Function

Private Sub txtImpHasta_Validate(Cancel As Boolean)
    Cancel = Not ValidarImpHasta
End Sub

''' <summary>Valida el importe hasta</summary>
''' <remarks>Llamada desde: txtImpDesde_Validate</remarks>
''' <revision>LTG 03/07/2012</revision>

Private Function ValidarImpHasta() As Boolean
    ValidarImpHasta = True
    
    If txtImpHasta.Text <> "" Then
        If IsNumeric(txtImpHasta.Text) Then
            txtImpHasta.Text = Format(txtImpHasta.Text, "standard")
        Else
            ValidarImpHasta = False
            oMensajes.NoValido m_sImporte & " " & m_sHasta
            txtImpHasta.Text = ""
        End If
    End If
End Function

Private Sub txtPedido_Validate(Cancel As Boolean)
    Cancel = Not ValidarPedido
End Sub

''' <summary>Valida el campo pedido</summary>
''' <returns>Booleano</returns>
''' <remarks>Llamada desde: txtPedido_Validate</remarks>
''' <revision>LTG 05/07/2012</revision>

Private Function ValidarPedido() As Boolean
    ValidarPedido = True
    
    If txtPedido.Text <> "" Then
        If Not IsNumeric(txtPedido.Text) Then
            ValidarPedido = False
            oMensajes.NoValido m_sPedido
            txtPedido.Text = ""
            If Me.Visible Then txtPedido.SetFocus
        End If
    End If
End Function
