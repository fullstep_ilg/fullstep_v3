VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSelEmpresas 
   Caption         =   "Form1"
   ClientHeight    =   4845
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11220
   Icon            =   "frmSelEmpresas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4845
   ScaleWidth      =   11220
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   4320
      TabIndex        =   2
      Top             =   4410
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "DCancelar"
      Height          =   345
      Left            =   5520
      TabIndex        =   1
      Top             =   4410
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgSelEmpresas 
      Height          =   4170
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10845
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   10
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSelEmpresas.frx":014A
      stylesets(1).Name=   "StringTachado"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(1).Picture=   "frmSelEmpresas.frx":0166
      stylesets(2).Name=   "Bloqueado"
      stylesets(2).BackColor=   12632256
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSelEmpresas.frx":0182
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   10
      Columns(0).Width=   873
      Columns(0).Name =   "SEL"
      Columns(0).Alignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1720
      Columns(2).Caption=   "NIF"
      Columns(2).Name =   "NIF"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Caption=   "DENOMINACION"
      Columns(3).Name =   "DENOMINACION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   2884
      Columns(4).Caption=   "DIRECCION"
      Columns(4).Name =   "DIRECCION"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   2699
      Columns(5).Caption=   "POBLACION"
      Columns(5).Name =   "POBLACION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2196
      Columns(6).Caption=   "CP"
      Columns(6).Name =   "CP"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   1958
      Columns(7).Caption=   "PAIS"
      Columns(7).Name =   "PAIS"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   2434
      Columns(8).Caption=   "PROVINCIA"
      Columns(8).Name =   "PROVINCIA"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "EN_ROL"
      Columns(9).Name =   "EN_ROL"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   11
      Columns(9).FieldLen=   256
      _ExtentX        =   19129
      _ExtentY        =   7355
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelEmpresas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oSolicitud As CSolicitud
Public g_sEmpresas As String
Public g_TipoSolicitud As Integer
Public g_lSolicitud As Long
'Public g_sCadenaEmpresasSolicitud As String

Private m_sCadenaEmpresas As String
Private iDesde As Integer
Private m_bCambiosRealizados As Boolean

Private m_sLitSinEmpresaSeleccionada As String
Private Sub cmdAceptar_Click()
Dim sMensaje As String
If Not g_oSolicitud Is Nothing Then
    If m_sCadenaEmpresas = "" Then
        oMensajes.NoValida m_sLitSinEmpresaSeleccionada
        Exit Sub
    End If
    sMensaje = g_oSolicitud.ValidarEmpresas(m_sCadenaEmpresas, oUsuarioSummit.idioma, g_TipoSolicitud)
    If sMensaje <> "" Then
        oMensajes.CfgSolicitudEmpresas sMensaje
        Exit Sub
    End If
    If Not m_bCambiosRealizados Then
        m_sCadenaEmpresas = ""
    Else
        g_oSolicitud.ActualizarAprobadores m_sCadenaEmpresas
    End If
    
    g_oSolicitud.CadenaEmpresas = m_sCadenaEmpresas
    Set g_oSolicitud = Nothing
Else
    g_sEmpresas = m_sCadenaEmpresas
End If
m_sCadenaEmpresas = ""

Unload Me

End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub


''' <summary>
''' Carga el formulario
''' </summary>
''' <remarks>Tiempo m�ximo:0,3seg.</remarks>
Private Sub Form_Load()
    CargarRecursos
    PonerFieldSeparator Me
    CargarEmpresas
    m_bCambiosRealizados = False
End Sub

''' <summary>
''' Carga los idiomas
''' </summary>
''' <remarks>Llamada: Form_load; Tiempo m�ximo:0,1seg.</remarks>
Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SelEmpresas, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value '1 Selecci�n de empresas
        Ador.MoveNext
        sdbgSelEmpresas.Columns("NIF").caption = Ador(0).Value '"NIF"
        Ador.MoveNext
        sdbgSelEmpresas.Columns("DENOMINACION").caption = Ador(0).Value '"Denominaci�n"
        Ador.MoveNext
        sdbgSelEmpresas.Columns("DIRECCION").caption = Ador(0).Value '"Direcci�n"
        Ador.MoveNext
        sdbgSelEmpresas.Columns("POBLACION").caption = Ador(0).Value '"Poblaci�n"
        Ador.MoveNext
        sdbgSelEmpresas.Columns("CP").caption = Ador(0).Value '"C.P."
        Ador.MoveNext
        sdbgSelEmpresas.Columns("PAIS").caption = Ador(0).Value '"Pais"
        Ador.MoveNext
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '"Aceptar"
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value  '"Cancelar"
        Ador.MoveNext
        m_sLitSinEmpresaSeleccionada = Ador(0).Value
        Ador.Close
    End If
End Sub

''' <summary>
''' Carga las empresas en la grid
''' </summary>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,3seg.</remarks>
Private Sub CargarEmpresas()
Dim oEmpresas As CEmpresas
Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
Dim bAsignado As Boolean
Dim rs As Recordset
Dim lSolicitud As Long
Dim bRestriccion As Boolean
Dim oSolicitud As CSolicitud
Dim bConCambiosSinGuardar As Boolean
Dim sCadenaEmpresas As String

Set oSolicitud = oFSGSRaiz.Generar_CSolicitud

lSolicitud = 0
bRestriccion = False
m_sCadenaEmpresas = ""
bConCambiosSinGuardar = False

If Not g_oSolicitud Is Nothing Then
    lSolicitud = g_oSolicitud.Id
    iDesde = 0
    If g_sEmpresas <> "" Then
        bConCambiosSinGuardar = True
        sCadenaEmpresas = g_sEmpresas
    Else
         sCadenaEmpresas = g_oSolicitud.CadenaEmpresas
    End If
   
ElseIf g_lSolicitud > 0 Then
    sCadenaEmpresas = g_sEmpresas
    iDesde = 1
    lSolicitud = g_lSolicitud
    oSolicitud.Id = lSolicitud
    Dim iNumEmpresas As Integer
    iNumEmpresas = oSolicitud.NumeroDeEmpresas
    bRestriccion = (iNumEmpresas > 0)
    
End If


Set rs = oEmpresas.DevolverTodasEmpresas(iDesde, lSolicitud, bRestriccion)
sdbgSelEmpresas.RemoveAll

Dim bEnRol As Boolean

    Dim i As Integer
    Dim arrAux() As String
    arrAux = Split(sCadenaEmpresas, ",")
    Dim bAsignadoBBDD As Boolean
    While Not rs.EOF
        bAsignado = False
        If Not bConCambiosSinGuardar Then
            bAsignadoBBDD = rs("ASIGNADO").Value
        End If
        bEnRol = rs("EN_ROL").Value
        
        If bEnRol Then
            bAsignado = True
        Else
            For i = 0 To UBound(arrAux)
                If rs("ID").Value = arrAux(i) Then
                    bAsignado = True
                    Exit For
                End If
            Next i
        End If
                
        If bAsignado Or bAsignadoBBDD Then
            m_sCadenaEmpresas = m_sCadenaEmpresas & rs("ID").Value & ","
            bAsignado = True
        End If
        
        
        
        sdbgSelEmpresas.AddItem BooleanToSQLBinary(bAsignado) & Chr(m_lSeparador) & rs("ID").Value & Chr(m_lSeparador) & rs("NIF").Value & Chr(m_lSeparador) & rs("DEN").Value & Chr(m_lSeparador) & rs("DIR").Value & Chr(m_lSeparador) & rs("POB").Value & Chr(m_lSeparador) & rs("CP").Value & Chr(m_lSeparador) & rs("PAIS").Value & Chr(m_lSeparador) & rs("PROVINCIA").Value & Chr(m_lSeparador) & bEnRol
        rs.MoveNext
        
    Wend
Set rs = Nothing
End Sub


''' <summary>
''' Evento que salta al seleccionar una empresa en la grid.
''' Actualiza la cadena de empresas seleccionadas. Si viene de la configuracion de la solicitud almacena en BBDD
''' </summary>
''' <param name="Cancel">Si ha cancelado el formulario</param>
''' <remarks>Tiempo m�ximo:0,1seg.</remarks>
Private Sub sdbgSelEmpresas_Change()

If sdbgSelEmpresas.Col = -1 Then Exit Sub

If sdbgSelEmpresas.Columns("EN_ROL").Value = True Then
    sdbgSelEmpresas.CancelUpdate
    Exit Sub
End If

    m_bCambiosRealizados = True
    
    If sdbgSelEmpresas.Columns("SEL").Value = "-1" Or sdbgSelEmpresas.Columns("SEL").Value = "1" Then
        'Seleccionar Empresa
        m_sCadenaEmpresas = m_sCadenaEmpresas & sdbgSelEmpresas.Columns("ID").Value & ","
    Else
        'Deseleccionar Empresa
        Dim sCadenaABuscar As String
        Dim sCadenaNueva As String
        Dim arrAux() As String
        arrAux = Split(m_sCadenaEmpresas, ",")
        sCadenaABuscar = sdbgSelEmpresas.Columns("ID").Value

        Dim i As Integer
        For i = 0 To UBound(arrAux)
            If sCadenaABuscar <> arrAux(i) And arrAux(i) <> "" Then
                sCadenaNueva = sCadenaNueva & arrAux(i) & ","
            End If
        Next i
        m_sCadenaEmpresas = sCadenaNueva
        
    End If
    sdbgSelEmpresas.Update

End Sub


