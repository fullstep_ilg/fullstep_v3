VERSION 5.00
Begin VB.Form frmEsperaPortal 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FullStep Portal+"
   ClientHeight    =   660
   ClientLeft      =   3120
   ClientTop       =   5670
   ClientWidth     =   5895
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   660
   ScaleWidth      =   5895
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer timer 
      Interval        =   1
      Left            =   4320
      Top             =   240
   End
   Begin VB.Line lineprog 
      BorderColor     =   &H000000C0&
      BorderWidth     =   2
      X1              =   300
      X2              =   840
      Y1              =   540
      Y2              =   540
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   30
      Picture         =   "frmEsperaPortal.frx":0000
      Top             =   60
      Width           =   480
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Comunicando con el portal, por favor espere+++"
      Height          =   420
      Left            =   600
      TabIndex        =   0
      Top             =   60
      Width           =   5265
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00C00000&
      BorderWidth     =   2
      X1              =   300
      X2              =   4200
      Y1              =   540
      Y2              =   540
   End
End
Attribute VB_Name = "frmEsperaPortal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public s_Origen As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub Arrange()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If s_Origen = "frmRESREU" Or s_Origen = "frmVARCalidad" Then
    Me.Width = 6000
Else
    Me.Width = 4335
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEsperaPortal", "Arrange", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEsperaPortal", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    CargarRecursos
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEsperaPortal", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

oFSGSRaiz.pg_sFrmCargado Me.Name, False

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEsperaPortal", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub timer_Timer()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
lineprog.X1 = lineprog.X1 + 100
lineprog.X2 = lineprog.X2 + 100
Refresh
If lineprog.X1 > 3660 Then
    lineprog.X1 = 300
    lineprog.X2 = 840
    Refresh
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEsperaPortal", "timer_Timer", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESPERA_PORTAL, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Select Case s_Origen
        Case "frmRESREU"
            Ador.MoveNext
            Ador.MoveNext
            frmEsperaPortal.caption = Ador(0).Value
            Ador.MoveNext
            Label1.caption = Ador(0).Value
            Ador.Close
        Case "frmVARCalidad"
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            frmEsperaPortal.caption = Ador(0).Value
            Ador.MoveNext
            Label1.caption = Ador(0).Value
            Ador.Close
        
        Case Else
            frmEsperaPortal.caption = Ador(0).Value
            Ador.MoveNext
            Label1.caption = Ador(0).Value
            Ador.Close
        End Select
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmEsperaPortal", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
            
End Sub

