VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCONTRWizardGru 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de grupos del proceso:"
   ClientHeight    =   3105
   ClientLeft      =   1665
   ClientTop       =   1395
   ClientWidth     =   9045
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONTRWizardGru.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3105
   ScaleWidth      =   9045
   Begin VB.CommandButton cmdVolver 
      Caption         =   "<<             "
      Height          =   315
      Left            =   3930
      TabIndex        =   4
      Top             =   2760
      Width           =   1095
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "&Finalizar"
      Default         =   -1  'True
      Height          =   315
      Left            =   5100
      TabIndex        =   3
      Top             =   2760
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2760
      TabIndex        =   2
      Top             =   2760
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgContrato 
      Height          =   2265
      Left            =   60
      TabIndex        =   0
      Top             =   450
      Width           =   8925
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   9
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCONTRWizardGru.frx":0CB2
      stylesets(1).Name=   "ITEMCERRADO"
      stylesets(1).ForeColor=   0
      stylesets(1).BackColor=   16777152
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCONTRWizardGru.frx":0CCE
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   53
      ActiveRowStyleSet=   "Normal"
      Columns.Count   =   9
      Columns(0).Width=   873
      Columns(0).Name =   "INC"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   1535
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   4710
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777215
      Columns(3).Width=   1984
      Columns(3).Caption=   "Importe"
      Columns(3).Name =   "IMP"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   820
      Columns(4).Caption=   "Dest"
      Columns(4).Name =   "DEST"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16777215
      Columns(5).Width=   1138
      Columns(5).Caption=   "F.Pago"
      Columns(5).Name =   "PAG"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1852
      Columns(6).Caption=   "Fec.Ini.Sum."
      Columns(6).Name =   "FINI"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   1852
      Columns(7).Caption=   "Fec.Fin Sum."
      Columns(7).Name =   "FFIN"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "ID"
      Columns(8).Name =   "ID"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   3
      Columns(8).FieldLen=   256
      _ExtentX        =   15743
      _ExtentY        =   3995
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblSel 
      Caption         =   "Seleccione los grupos que desea incluir en el nuevo contrato:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   90
      TabIndex        =   1
      Top             =   90
      Width           =   6225
   End
End
Attribute VB_Name = "frmCONTRWizardGru"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_sFormatoNumber As String
Private m_sIdiMensaje  As String
Private m_sIdiCaption  As String
Private m_bVolver As Boolean


Public g_sCodProve As String

Private Sub cmdCancelar_Click()

    Unload frmCONTRWizard
    Unload Me
End Sub

''' <summary>
''' Envia los datos para la generacion del contrato
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo:0,1seg.</remarks>
Private Sub cmdContinuar_Click()
Dim i As Integer
Dim vb As Variant
Dim oGrupos As CGrupos

Set oGrupos = oFSGSRaiz.Generar_CGrupos

For i = 0 To sdbgContrato.Rows - 1
    vb = sdbgContrato.AddItemBookmark(i)
    If sdbgContrato.Columns(0).CellValue(vb) = "1" Or sdbgContrato.Columns(0).CellValue(vb) = "-1" Then
        oGrupos.Add oproce:=frmCONTRWizard.m_oProcesoSeleccionado, sCod:=sdbgContrato.Columns(1).CellValue(vb), sDen:=sdbgContrato.Columns(2).CellValue(vb), lID:=CLng(sdbgContrato.Columns(8).CellValue(vb)), vFechaInicioSuministro:=sdbgContrato.Columns(6).CellValue(vb), vFechaFinSuministro:=sdbgContrato.Columns(7).CellValue(vb)
    End If
Next

If oGrupos.Count = 0 Then
    MsgBox m_sIdiMensaje, vbExclamation, "FULLSTEP"
    Exit Sub
End If

Set frmCONTRWizard.g_oGruposProce = oGrupos
Set oGrupos = Nothing

frmCONTRWizard.WizardContrato
Unload Me

End Sub

Private Sub cmdVolver_Click()
    m_bVolver = True
    
    Unload Me
    frmCONTRWizard.Show

End Sub
''' <summary>
''' Carga los datos del formulario
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo:0,2seg.</remarks>
Private Sub Form_Load()
Dim i As Integer

    Me.Height = 3510
    Me.Width = 9165
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    'Obtiene el n� de decimales a mostrar en esta pantalla
    m_sFormatoNumber = "#,##0."
    For i = 1 To gParametrosInstalacion.giNumDecimalesComp
        m_sFormatoNumber = m_sFormatoNumber & "0"
    Next i
    
    m_bVolver = False
    
    CargarRecursos
    
    
    Me.caption = m_sIdiCaption

    PonerFieldSeparator Me

    frmCONTRWizard.m_oProcesoSeleccionado.CargarTodosLosGrupos bSinpres:=True

    CargarGrid
    
End Sub
''' <summary>
''' Carga la grid
''' </summary>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,2</remarks>
Private Sub CargarGrid()
Dim oGrupos As CGrupos
Dim Ador As Ador.Recordset
Dim bDest As Boolean
Dim bPago As Boolean
Dim bSum As Boolean


sdbgContrato.Columns("DEST").Visible = False
sdbgContrato.Columns("PAG").Visible = False
sdbgContrato.Columns("FINI").Visible = False
sdbgContrato.Columns("FFIN").Visible = False

Set oGrupos = oFSGSRaiz.Generar_CGrupos
Set Ador = oGrupos.DevolverGruposConAdjudicaciones(frmCONTRWizard.m_oProcesoSeleccionado.Anyo, frmCONTRWizard.m_oProcesoSeleccionado.GMN1Cod, frmCONTRWizard.m_oProcesoSeleccionado.Cod, g_sCodProve)
While Not Ador.EOF
    With frmCONTRWizard.m_oProcesoSeleccionado.Grupos.Item(Ador(0).Value)
        If .DefDestino Then bDest = True:
        If .DefFormaPago Then bPago = True
        If .DefFechasSum Then bSum = True
        sdbgContrato.AddItem "0" & Chr(m_lSeparador) & Ador(0).Value & Chr(m_lSeparador) & Ador(1).Value & Chr(m_lSeparador) & FormateoNumerico(StrToDbl0(Ador(2).Value), m_sFormatoNumber) & Chr(m_lSeparador) & NullToStr(.DestCod) & Chr(m_lSeparador) & NullToStr(.PagCod) & Chr(m_lSeparador) & NullToStr(.FechaInicioSuministro) & Chr(m_lSeparador) & NullToStr(.FechaFinSuministro) & Chr(m_lSeparador) & .Id
    End With
    Ador.MoveNext
Wend
Ador.Close
Set Ador = Nothing
Set oGrupos = Nothing

sdbgContrato.Columns("DEST").Visible = bDest
sdbgContrato.Columns("PAG").Visible = bPago
sdbgContrato.Columns("FINI").Visible = bSum
sdbgContrato.Columns("FFIN").Visible = bSum
sdbgContrato.Refresh
RedimensionarGrid

End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not m_bVolver Then
    Unload frmCONTRWizard
End If
End Sub

Private Sub sdbgContrato_change()
sdbgContrato.Update
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONTR_WIZARDGRU, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sIdiCaption = Ador(0).Value
        Ador.MoveNext
        lblSel.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdContinuar.caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiMensaje = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing

    cmdVolver.caption = " <<      "
End Sub


Private Sub RedimensionarGrid()
'*********************************************************************
'Redimensiona la grid seg�n el n�mero de columnas visibles.
'*********************************************************************

Dim i As Integer
Dim dblAncho As Double
Dim m_iTotalCols As Integer
 
    dblAncho = 0
    m_iTotalCols = 0
    For i = 0 To sdbgContrato.Cols - 1
        If sdbgContrato.Columns(i).Visible = True Then
            m_iTotalCols = m_iTotalCols + 1
            dblAncho = dblAncho + sdbgContrato.Columns(i).Width
        End If
    Next
    
    If dblAncho <= sdbgContrato.Width Then
        dblAncho = sdbgContrato.Columns("INC").Width
        For i = 0 To sdbgContrato.Cols - 1
            With sdbgContrato.Columns(i)
                If .Visible Then
                    Select Case sdbgContrato.Columns(i).Name
                    Case "COD"
                        .Width = (sdbgContrato.Width / m_iTotalCols) - 300
                        dblAncho = dblAncho + .Width
                    Case "IMP"
                        .Width = (sdbgContrato.Width / m_iTotalCols) - 45
                        dblAncho = dblAncho + .Width
                    Case "DEST"
                        .Width = (sdbgContrato.Width / m_iTotalCols) / 2 - 120
                        dblAncho = dblAncho + .Width
                    Case "PAG"
                        .Width = (sdbgContrato.Width / m_iTotalCols) / 2 + 60
                        dblAncho = dblAncho + .Width
                    Case "FINI", "FFIN"
                        .Width = (sdbgContrato.Width / m_iTotalCols) - 120
                        dblAncho = dblAncho + .Width
                    End Select
                End If
            End With
        Next

        sdbgContrato.Columns("DEN").Width = (sdbgContrato.Width - dblAncho) - 450
    
    End If

End Sub

