VERSION 5.00
Begin VB.Form frmDetallePrecios 
   Caption         =   "Detalle de precio de adjudicación"
   ClientHeight    =   3825
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10125
   Icon            =   "frmDetallePrecios.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3825
   ScaleWidth      =   10125
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      Height          =   3800
      Left            =   0
      ScaleHeight     =   3735
      ScaleWidth      =   10035
      TabIndex        =   0
      Top             =   0
      Width           =   10095
      Begin VB.TextBox txtMonOfe 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         TabIndex        =   11
         Top             =   360
         Width           =   3180
      End
      Begin VB.TextBox txtOferta 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   10
         Top             =   1500
         Width           =   1620
      End
      Begin VB.TextBox txtProce 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4050
         TabIndex        =   9
         Top             =   1500
         Width           =   1620
      End
      Begin VB.TextBox txtCentral 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7980
         TabIndex        =   8
         Top             =   1500
         Width           =   1620
      End
      Begin VB.TextBox txtMonProce 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         MaxLength       =   20
         TabIndex        =   7
         Top             =   720
         Width           =   3180
      End
      Begin VB.TextBox txtPrecioCat 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3400
         TabIndex        =   6
         Top             =   3320
         Width           =   2900
      End
      Begin VB.CommandButton cmdDetalleProce 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5340
         TabIndex        =   5
         Top             =   1200
         Width           =   315
      End
      Begin VB.TextBox txtAdj 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3400
         TabIndex        =   4
         Top             =   2600
         Width           =   2900
      End
      Begin VB.TextBox txtAhorroAdj 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6600
         TabIndex        =   3
         Top             =   2600
         Width           =   2900
      End
      Begin VB.TextBox txtPres 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Top             =   2600
         Width           =   2900
      End
      Begin VB.TextBox txtAhorroCat 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6600
         TabIndex        =   1
         Top             =   3320
         Width           =   2900
      End
      Begin VB.Label lblCentral2 
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Central (EUR)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   180
         TabIndex        =   26
         Top             =   2000
         Width           =   1140
      End
      Begin VB.Label lblCambioOfe 
         BackColor       =   &H8000000B&
         Caption         =   "Equivale a 1 EUR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   4980
         TabIndex        =   25
         Top             =   420
         Width           =   1300
      End
      Begin VB.Label lblMonOfe 
         BackColor       =   &H8000000B&
         Caption         =   "Moneda de oferta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   120
         TabIndex        =   24
         Top             =   420
         Width           =   1500
      End
      Begin VB.Label lblCat 
         BackColor       =   &H8000000B&
         Caption         =   "Precio de catálogo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   3400
         TabIndex        =   23
         Top             =   3020
         Width           =   1665
      End
      Begin VB.Label lblOferta 
         BackColor       =   &H8000000B&
         Caption         =   "Oferta (USD) "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   120
         TabIndex        =   22
         Top             =   1260
         Width           =   1260
      End
      Begin VB.Label lblProce 
         BackColor       =   &H8000000B&
         Caption         =   "Proceso (ESP)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   4020
         TabIndex        =   21
         Top             =   1260
         Width           =   1380
      End
      Begin VB.Label lblCentral 
         BackColor       =   &H8000000B&
         Caption         =   "Central"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   7980
         TabIndex        =   20
         Top             =   1200
         Width           =   1140
      End
      Begin VB.Label lblCambioOfeProce 
         BackColor       =   &H8000000B&
         Caption         =   "--> (*1/0,00536...) -->"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   2040
         TabIndex        =   19
         Top             =   1560
         Width           =   1860
      End
      Begin VB.Label lblCambioProceCentral 
         BackColor       =   &H8000000B&
         Caption         =   "-->(*1/166,386)--->"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   6120
         TabIndex        =   18
         Top             =   1560
         Width           =   1740
      End
      Begin VB.Label lblMonProce 
         BackColor       =   &H8000000B&
         Caption         =   "Moneda de proceso:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   120
         TabIndex        =   17
         Top             =   780
         Width           =   1500
      End
      Begin VB.Label lblCambioProce 
         BackColor       =   &H8000000B&
         Caption         =   "Equivale a 1 EUR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   4980
         TabIndex        =   16
         Top             =   780
         Width           =   1300
      End
      Begin VB.Label lblAdj 
         BackColor       =   &H8000000B&
         Caption         =   "Adjudicado:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   3400
         TabIndex        =   15
         Top             =   2300
         Width           =   1500
      End
      Begin VB.Label lblAhorroAdj 
         BackColor       =   &H8000000B&
         Caption         =   "Ahorro adjudicación"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   6600
         TabIndex        =   14
         Top             =   2300
         Width           =   1620
      End
      Begin VB.Label lblPres 
         BackColor       =   &H8000000B&
         Caption         =   "Presupuesto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   180
         TabIndex        =   13
         Top             =   2300
         Width           =   1080
      End
      Begin VB.Line Line1 
         X1              =   120
         X2              =   9900
         Y1              =   1920
         Y2              =   1920
      End
      Begin VB.Label lblAhorroCat 
         BackColor       =   &H8000000B&
         Caption         =   "Ahorro catalogo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   6600
         TabIndex        =   12
         Top             =   3020
         Width           =   1620
      End
   End
End
Attribute VB_Name = "frmDetallePrecios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Idiomas
Public sIdiEquivale As String
Public sIdiOferta As String
Public sIdiProceso As String
Public sIdiCentral As String
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DETALLEPRECIO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        lblMonOfe.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiEquivale = Ador(0).Value
        Ador.MoveNext
        lblMonProce.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiOferta = Ador(0).Value
        Ador.MoveNext
        sIdiProceso = Ador(0).Value
        Ador.MoveNext
        #If VERSION < 30600 Then
            sIdiCentral = Ador(0).Value
        #End If
        Ador.MoveNext
        lblPres.Caption = Ador(0).Value
        Ador.MoveNext
        lblAdj.Caption = Ador(0).Value
        Ador.MoveNext
        lblAhorroAdj.Caption = Ador(0).Value
        Ador.MoveNext
        lblCat.Caption = Ador(0).Value
        Ador.MoveNext
        lblAhorroCat.Caption = Ador(0).Value
        Ador.MoveNext
        #If VERSION >= 30600 Then
            sIdiCentral = Ador(0).Value
        #End If
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

Private Sub cmdDetalleProce_Click()
    frmCatalogo.MostrarDetalleProceso frmCatalogo.sdbgAdjudicaciones.Columns("ANYO").Value, frmCatalogo.sdbgAdjudicaciones.Columns("GMN1").Value, frmCatalogo.sdbgAdjudicaciones.Columns("PROCECOD").Value
    Unload Me
End Sub

Private Sub Form_Load()

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Me.Top = 2000
    Me.Left = 1000
    
    CargarRecursos

End Sub

