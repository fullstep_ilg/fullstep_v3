VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPROVE 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de proveedores (Opciones)"
   ClientHeight    =   11040
   ClientLeft      =   3225
   ClientTop       =   1935
   ClientWidth     =   6690
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPROVE.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11040
   ScaleWidth      =   6690
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   0
      ScaleHeight     =   405
      ScaleWidth      =   6690
      TabIndex        =   23
      Top             =   10635
      Width           =   6690
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   5295
         TabIndex        =   22
         Top             =   0
         Width           =   1350
      End
   End
   Begin TabDlg.SSTab stabProve 
      Height          =   11055
      Left            =   0
      TabIndex        =   24
      Top             =   0
      Width           =   6630
      _ExtentX        =   11695
      _ExtentY        =   19500
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstPROVE.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblBusqAvanzada"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "framePremium"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Timer1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "fraSelComp"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "fraBusqAvanzada"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstPROVE.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame2"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame3"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      Begin VB.Frame fraBusqAvanzada 
         Caption         =   "DProveedores relacionados"
         Height          =   3975
         Left            =   120
         TabIndex        =   52
         Top             =   6600
         Width           =   6315
         Begin VB.OptionButton optProveedores 
            Caption         =   "DMostrar todos los proveedores"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   69
            Top             =   3600
            Value           =   -1  'True
            Width           =   3105
         End
         Begin VB.OptionButton optProveedores 
            Caption         =   "DMostrar proveedores subordinados"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   68
            Top             =   240
            Width           =   2985
         End
         Begin VB.OptionButton optProveedores 
            Caption         =   "DMostrar proveedores principales"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   67
            Top             =   1710
            Width           =   2865
         End
         Begin VB.Frame fraProveSub 
            BorderStyle     =   0  'None
            Caption         =   "Frame2"
            Height          =   1215
            Left            =   360
            TabIndex        =   61
            Top             =   480
            Width           =   5655
            Begin SSDataWidgets_B.SSDBCombo sdbcTipoRelacSub 
               Height          =   285
               Left            =   1560
               TabIndex        =   62
               Top             =   120
               Width           =   3765
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3201
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   7038
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "ID"
               Columns(2).Name =   "ID"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   6641
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProveSubCod 
               Height          =   285
               Left            =   1560
               TabIndex        =   63
               Top             =   840
               Width           =   1020
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "C�d."
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1799
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProveSubDen 
               Height          =   285
               Left            =   2595
               TabIndex        =   64
               Top             =   840
               Width           =   2730
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "C�d."
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4815
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblProveRelac 
               Caption         =   "DTipo de relaci�n:"
               Height          =   225
               Index           =   0
               Left            =   0
               TabIndex        =   66
               Top             =   120
               Width           =   1320
            End
            Begin VB.Label lblProveRelac 
               Caption         =   "DMostrar proveedores subordinados al siguiente proveedor:"
               Height          =   225
               Index           =   1
               Left            =   0
               TabIndex        =   65
               Top             =   540
               Width           =   4560
            End
         End
         Begin VB.Frame fraProvePri 
            BorderStyle     =   0  'None
            Caption         =   "Frame2"
            Height          =   1695
            Left            =   360
            TabIndex        =   53
            Top             =   1920
            Width           =   5775
            Begin VB.OptionButton optTRel 
               Caption         =   "DCon tipo de relaci�n"
               Height          =   255
               Index           =   0
               Left            =   840
               TabIndex        =   55
               Top             =   120
               Value           =   -1  'True
               Width           =   2115
            End
            Begin VB.OptionButton optTRel 
               Caption         =   "DSin tipo de relaci�n"
               Height          =   255
               Index           =   1
               Left            =   3120
               TabIndex        =   54
               Top             =   120
               Width           =   1755
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcTipoRelacPri 
               Height          =   285
               Left            =   1560
               TabIndex        =   56
               Top             =   480
               Width           =   3765
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3201
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   7038
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "ID"
               Columns(2).Name =   "ID"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   6641
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProvePriCod 
               Height          =   285
               Left            =   1560
               TabIndex        =   57
               Top             =   1320
               Width           =   1020
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "C�d."
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1799
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProvePriDen 
               Height          =   285
               Left            =   2595
               TabIndex        =   58
               Top             =   1320
               Width           =   2730
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "C�d."
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4815
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblProveRelac 
               Caption         =   "DMostrar proveedores que tienen al siguiente proveedor como subordinado:"
               Height          =   225
               Index           =   3
               Left            =   0
               TabIndex        =   60
               Top             =   960
               Width           =   5640
            End
            Begin VB.Label lblProveRelac 
               Caption         =   "DTipo de relaci�n:"
               Height          =   225
               Index           =   2
               Left            =   0
               TabIndex        =   59
               Top             =   480
               Width           =   1320
            End
         End
      End
      Begin VB.Frame fraSelComp 
         Height          =   1935
         Left            =   225
         TabIndex        =   34
         Top             =   480
         Width           =   6195
         Begin VB.Frame fraEqp 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   510
            Left            =   105
            TabIndex        =   38
            Top             =   1380
            Width           =   6015
            Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
               Height          =   285
               Left            =   1380
               TabIndex        =   6
               Top             =   120
               Width           =   1065
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               GroupHeaders    =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1296
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   5345
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1879
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               DataFieldToDisplay=   "Column 3"
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
               Height          =   285
               Left            =   2475
               TabIndex        =   7
               Top             =   150
               Width           =   3360
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1640
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5927
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblEqp 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Label3"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1380
               TabIndex        =   40
               Top             =   150
               Visible         =   0   'False
               Width           =   4365
            End
            Begin VB.Label lblEqpCod 
               Caption         =   "Equipo:"
               Height          =   255
               Left            =   105
               TabIndex        =   39
               Top             =   195
               Width           =   1020
            End
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
            Height          =   285
            Left            =   1485
            TabIndex        =   0
            Top             =   210
            Width           =   1065
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
            Height          =   285
            Left            =   2565
            TabIndex        =   1
            Top             =   210
            Width           =   3360
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5927
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
            Height          =   285
            Left            =   1485
            TabIndex        =   2
            Top             =   650
            Width           =   1065
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
            Height          =   285
            Left            =   2565
            TabIndex        =   3
            Top             =   645
            Width           =   3360
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5927
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
            Height          =   285
            Left            =   1485
            TabIndex        =   4
            Top             =   1095
            Width           =   1065
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMonDen 
            Height          =   285
            Left            =   2565
            TabIndex        =   5
            Top             =   1095
            Width           =   3360
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5927
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblPaiCod 
            Caption         =   "Pa�s:"
            Height          =   225
            Left            =   210
            TabIndex        =   37
            Top             =   240
            Width           =   1080
         End
         Begin VB.Label lblProviCod 
            Caption         =   "Provincia:"
            Height          =   225
            Left            =   210
            TabIndex        =   36
            Top             =   675
            Width           =   1200
         End
         Begin VB.Label lblMonCod 
            Caption         =   "Moneda:"
            Height          =   225
            Left            =   210
            TabIndex        =   35
            Top             =   1125
            Width           =   1080
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2595
         Left            =   210
         TabIndex        =   27
         Top             =   2550
         Width           =   6195
         Begin VB.CommandButton cmdBorrar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5160
            Picture         =   "frmLstPROVE.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   225
            Width           =   345
         End
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5550
            Picture         =   "frmLstPROVE.frx":0D8F
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   225
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   1485
            TabIndex        =   8
            Top             =   720
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
            Height          =   285
            Left            =   1485
            TabIndex        =   10
            Top             =   1170
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
            Height          =   285
            Left            =   1485
            TabIndex        =   12
            Top             =   1620
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
            Height          =   285
            Left            =   1485
            TabIndex        =   14
            Top             =   2100
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
            Height          =   285
            Left            =   2565
            TabIndex        =   11
            Top             =   1170
            Width           =   3360
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5927
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
            Height          =   285
            Left            =   2565
            TabIndex        =   13
            Top             =   1620
            Width           =   3360
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5927
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
            Height          =   285
            Left            =   2565
            TabIndex        =   15
            Top             =   2100
            Width           =   3360
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1164
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5927
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
            Height          =   285
            Left            =   2565
            TabIndex        =   9
            Top             =   720
            Width           =   3360
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5927
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "Grupo:"
            Height          =   225
            Left            =   210
            TabIndex        =   33
            Top             =   2130
            Width           =   1260
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Commodity:"
            Height          =   225
            Left            =   210
            TabIndex        =   32
            Top             =   750
            Width           =   1275
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "Familia:"
            Height          =   225
            Left            =   210
            TabIndex        =   31
            Top             =   1215
            Width           =   1260
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "Subfamilia:"
            Height          =   225
            Left            =   210
            TabIndex        =   30
            Top             =   1665
            Width           =   1260
         End
         Begin VB.Label lblMaterial 
            Caption         =   "Material:"
            Height          =   255
            Left            =   210
            TabIndex        =   29
            Top             =   300
            Width           =   2490
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Orden"
         Height          =   2745
         Left            =   -71520
         TabIndex        =   26
         Top             =   2400
         Width           =   3085
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            Height          =   285
            Left            =   250
            TabIndex        =   20
            Top             =   780
            Width           =   2000
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   285
            Left            =   250
            TabIndex        =   21
            Top             =   1245
            Width           =   2000
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4335
         Top             =   195
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2745
         Left            =   -74880
         TabIndex        =   25
         Top             =   2400
         Width           =   3255
         Begin VB.CheckBox chkProvRel 
            Caption         =   "Incluir proveedores relacionados"
            Height          =   195
            Left            =   120
            TabIndex        =   51
            Top             =   2400
            Width           =   2895
         End
         Begin VB.CheckBox chkCodErp 
            Caption         =   "Incluir codificaci�n ERP"
            Height          =   195
            Left            =   135
            TabIndex        =   50
            Top             =   2040
            Visible         =   0   'False
            Width           =   2895
         End
         Begin VB.CheckBox chkDatosWeb 
            Caption         =   "Incluir datos acceso web"
            Height          =   195
            Left            =   135
            TabIndex        =   46
            Top             =   1680
            Visible         =   0   'False
            Width           =   2895
         End
         Begin VB.CheckBox chkObs 
            Caption         =   "Incluir Especificaciones"
            Height          =   195
            Left            =   135
            TabIndex        =   19
            Top             =   1320
            Width           =   2925
         End
         Begin VB.CheckBox chkDatosBasicos 
            Caption         =   "Incluir datos b�sicos"
            Height          =   195
            Left            =   135
            TabIndex        =   16
            Top             =   240
            Width           =   2925
         End
         Begin VB.CheckBox chkCalifica 
            Caption         =   "Incluir datos homologaci�n"
            Height          =   195
            Left            =   135
            TabIndex        =   17
            Top             =   960
            Width           =   2985
         End
         Begin VB.CheckBox chkContactos 
            Caption         =   "Incluir contactos"
            Height          =   195
            Left            =   135
            TabIndex        =   18
            Top             =   600
            Width           =   2880
         End
      End
      Begin VB.Frame Frame4 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   -74880
         TabIndex        =   41
         Top             =   480
         Width           =   6450
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   2850
            TabIndex        =   49
            Top             =   750
            Width           =   3200
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DENO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5644
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   1500
            TabIndex        =   48
            Top             =   750
            Width           =   1300
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DENO"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            _ExtentX        =   2293
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblProve 
            Caption         =   "Proveedor"
            Height          =   255
            Left            =   240
            TabIndex        =   47
            Top             =   800
            Width           =   1215
         End
      End
      Begin VB.Frame framePremium 
         Caption         =   " Premium "
         Height          =   1095
         Left            =   180
         TabIndex        =   43
         Top             =   5160
         Visible         =   0   'False
         Width           =   6195
         Begin VB.CheckBox chkPremiumAct 
            Caption         =   "S�lo premiums activos"
            Height          =   195
            Left            =   3540
            TabIndex        =   45
            Top             =   420
            Width           =   2955
         End
         Begin VB.CheckBox chkPremiumProv 
            Caption         =   "S�lo proveedores Premium"
            Height          =   255
            Left            =   300
            TabIndex        =   44
            Top             =   480
            Width           =   2865
         End
      End
      Begin VB.Label lblBusqAvanzada 
         Alignment       =   1  'Right Justify
         Caption         =   "DB�squeda Avanzada"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4820
         TabIndex        =   70
         Top             =   6240
         Width           =   1575
      End
   End
End
Attribute VB_Name = "frmLstPROVE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables para contener materiales, monedas, etc
Private m_oGruposMN1 As CGruposMatNivel1
Private m_oGruposMN2 As CGruposMatNivel2
Private m_oGruposMN3 As CGruposMatNivel3
Private m_oGruposMN4 As CGruposMatNivel4
Private m_oPaises As CPaises
Private m_oMonedas As CMonedas
Private m_oEqps As CEquipos

' Variables de seguridad
Private m_bRMat As Boolean
Public g_bREqp As Boolean
Public g_bProve As Boolean 'para saber si viene del frmPROVE

'Variables para la seleccion en combos
Public g_oEqpSeleccionado As CEquipo
Private m_oPaisSeleccionado As CPais
Private m_oProviSeleccionada As CProvincia
Private m_oMonSeleccionada As CMoneda
Public g_oGMN1Seleccionado As CGrupoMatNivel1
Public g_oGMN2Seleccionado As CGrupoMatNivel2
Public g_oGMN3Seleccionado As CGrupoMatNivel3
Public g_oGMN4Seleccionado As CGrupoMatNivel4
Private m_sMatSeleccionado As String
Private m_oProveedores As CProveedores
Private m_oProveedorSeleccionado As CProveedor
''' Combos
Private m_bEqpRespetarCombo As Boolean
Private m_bEqpCargarComboDesde As Boolean
Private m_bPaiRespetarCombo As Boolean
Private m_bPaiCargarComboDesde As Boolean
Private m_bMonRespetarCombo As Boolean
Private m_bMonCargarComboDesde As Boolean
Private m_bProviRespetarCombo As Boolean
Private m_bProviCargarComboDesde As Boolean
Private m_bGMN1RespetarCombo As Boolean
Private m_bGMN1CargarComboDesde As Boolean
Private m_bGMN2RespetarCombo As Boolean
Private m_bGMN2CargarComboDesde As Boolean
Private m_bGMN3RespetarCombo As Boolean
Private m_bGMN3CargarComboDesde As Boolean
Private m_bGMN4RespetarCombo As Boolean
Private m_bGMN4CargarComboDesde As Boolean
Private m_bSoloPremOWeb As Boolean
Private m_bSoloPremActivosOBloq As Boolean
Public g_bProveRespetarCombo As Boolean
Private m_bProveCargarComboDesde As Boolean

''' Combos de proveedores relacionados
Private bRespetarComboProvePri As Boolean
Private bCargarComboPriDesde As Boolean
Private bRespetarComboProveSub As Boolean
Private bCargarComboSubDesde As Boolean
        

'Multilenguaje
Private m_sFormaPago As String
Private m_sViaPago As String
Private m_sIdiProveedores As String
Private m_sIdiDelPais As String
Private m_sIdiDeProvincia As String
Private m_sIdiConMoneda As String
Private m_sIdiQueSuministran As String
Private m_sIdiAsignEqp As String
Private m_sIdiTitulo As String
Private m_sIdiGenerando As String
Private m_sIdiSeleccionando As String
Private m_sIdiVisualizando As String
Private m_srIdiTitulo As String
Private m_srIdiSeleccion As String
Private m_srIdiProveedor As String
Private m_srIdiNif As String
Private m_srIdiDireccion As String
Private m_srIdiMoneda As String
Private m_srIdiURL As String
Private m_srIdiHomologacion As String
Private m_srIdiObservaciones As String
Private m_srIdiArchivosAdj As String
Private m_srIdiContactos As String
Private m_srIdiPag As String
Private m_srIdiDe As String
' datos contactos
Private m_srIdiTfno1 As String
Private m_srIdiTfno2 As String
Private m_srIdiMovil As String
Private m_srIdiFax As String
Private m_srIdiMail As String
Private m_srIdiConNif As String
' C�digo proveedor Portal
Private m_srIdiCodPortal As String
Private m_sPremiumAct As String
Private m_sPremiumNoAct As String
Private m_sNoPremium As String
' C�digo acceso web
Private m_sIdiCodigoAcceso As String
Private m_sIdiCuentaBloq As String
Private m_sSi As String
Private m_sNo As String

Private m_sIdiCif As String
Private m_sIdiRazonSocial As String
Private m_sIdiDescripcion As String
Private m_sIdiCogigosERP As String

' datos proveedores relacionados
Private m_sIdiProveRel As String
Private m_sIdiProveRelPri As String
Private m_oProveRelac As CProveedores
Private oTiposRelac As CRelacTipos
'Literales del recuadro de b�squeda avanzada para proveedores relacionados
Private sLiteralBusqAvanzada As String
Private sLiteralOcultar As String
Private sCodProveSelecc As String

Private sLiteralTipoRel As String

Private m_sIdiTipoRel As String
Private m_sIdiConTipoRel As String
Private m_sIdiSinTipoRel As String
Private m_sIdiProveSub As String
Private m_sIdiProvePri As String

Private m_sIdiAutoFactura As String
Private m_sIdiAutoFacturaEImpuestos As String

Private m_srPrincipal As String

Private Sub cmdBorrar_Click()
    sdbcGMN1_4Cod = ""
    stabProve.Tab = 0
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And m_bRMat Then
        cmdObtener.Enabled = False
    End If
    If Me.Visible Then sdbcGMN1_4Cod.SetFocus
End Sub

''' <summary>
''' Lanza el report con todos sus par�metros
''' </summary>
''' <remarks>Llamada desde: frmLstProve Tiempo m�ximo: 1 seg.</remarks>
''' <revision>JVS 18/08/2011</revision>
Private Sub cmdObtener_Click()
    Dim sSelectionText As String
    Dim sSQL As String
    Dim sSelectionTextRpt(1 To 2, 1 To 31) As String
    Dim oReport As Object
    Dim SubListado As CRAXDRT.Report
    Dim oCRProveedores As CRProveedores
    Dim oFos As FileSystemObject
    Dim sRepPath As String
    Dim i As Integer
    Dim sCodGMN1 As String
    Dim sCodGMN2 As String
    Dim sCodGMN3 As String
    Dim sCodGMN4 As String
    Dim sCodEqp As String
    Dim scodPais As String
    Dim sCodProvi As String
    Dim sCodMon As String
    Dim scodProve As String
    Dim pv As Preview
    'Variables para B�squeda de proveedores relacionados
    Dim iBusquedaAvanzada As Integer
    Dim sTipoRel As String
    Dim bConTipoRel As Boolean
    Dim sCodProvePriSub As String
    Dim bHayAlgunFiltro As Boolean
    
    ''' * Objetivo: Obtener un listado de los proveedores

    Set oCRProveedores = GenerarCRProveedores

    bHayAlgunFiltro = False
    
    Screen.MousePointer = vbHourglass
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    sRepPath = gParametrosInstalacion.gsRPTPATH & "\rptPROVE.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(sRepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    If crs_crapp Is Nothing Then Exit Sub

    If g_oEqpSeleccionado Is Nothing Then
        If g_oGMN1Seleccionado Is Nothing And g_oGMN2Seleccionado Is Nothing And g_oGMN3Seleccionado Is Nothing And g_oGMN4Seleccionado Is Nothing Then
            m_sMatSeleccionado = ""
        Else
            If Not g_oGMN4Seleccionado Is Nothing Then
                m_sMatSeleccionado = g_oGMN1Seleccionado.Cod & " - " & g_oGMN2Seleccionado.Cod & " - " & g_oGMN3Seleccionado.Cod & " - " & g_oGMN4Seleccionado.Cod
            Else
                If Not g_oGMN3Seleccionado Is Nothing Then
                    m_sMatSeleccionado = g_oGMN1Seleccionado.Cod & " - " & g_oGMN2Seleccionado.Cod & " - " & g_oGMN3Seleccionado.Cod
                Else
                    If Not g_oGMN2Seleccionado Is Nothing Then
                        m_sMatSeleccionado = g_oGMN1Seleccionado.Cod & " - " & g_oGMN2Seleccionado.Cod
                    Else
                        m_sMatSeleccionado = g_oGMN1Seleccionado.Cod
                    End If
                End If
            End If
            
        End If
    Else
        If g_oGMN1Seleccionado Is Nothing And g_oGMN2Seleccionado Is Nothing And g_oGMN3Seleccionado Is Nothing And g_oGMN4Seleccionado Is Nothing Then
            m_sMatSeleccionado = ""
        Else
            If Not g_oGMN4Seleccionado Is Nothing Then
                m_sMatSeleccionado = g_oGMN1Seleccionado.Cod & " - " & g_oGMN2Seleccionado.Cod & " - " & g_oGMN3Seleccionado.Cod & " - " & g_oGMN4Seleccionado.Cod
            Else
                If Not g_oGMN3Seleccionado Is Nothing Then
                    m_sMatSeleccionado = g_oGMN1Seleccionado.Cod & " - " & g_oGMN2Seleccionado.Cod & " - " & g_oGMN3Seleccionado.Cod
                Else
                    If Not g_oGMN2Seleccionado Is Nothing Then
                        m_sMatSeleccionado = g_oGMN1Seleccionado.Cod & " - " & g_oGMN2Seleccionado.Cod
                    Else
                        m_sMatSeleccionado = g_oGMN1Seleccionado.Cod
                    End If
                End If
            End If
            
        End If
    End If
    
    'Selecci�n que se lista
    sSelectionText = ""
    If Not m_oPaisSeleccionado Is Nothing Then
        sSelectionText = m_sIdiDelPais & " " & m_oPaisSeleccionado.Den & ";"
        
        bHayAlgunFiltro = True
    End If
    If Not m_oProviSeleccionada Is Nothing Then
        sSelectionText = Trim(sSelectionText) & " " & m_sIdiDeProvincia & " " & m_oProviSeleccionada.Den & ";"
        
        bHayAlgunFiltro = True
    End If
    If Not m_oMonSeleccionada Is Nothing Then
        sSelectionText = Trim(sSelectionText) & " " & m_sIdiConMoneda & " " & m_oMonSeleccionada.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & ";"
        
        bHayAlgunFiltro = True
    End If
    If Not (g_oGMN1Seleccionado Is Nothing And g_oGMN2Seleccionado Is Nothing And g_oGMN3Seleccionado Is Nothing And g_oGMN4Seleccionado Is Nothing) Then
        sSelectionText = Trim(sSelectionText) & " " & m_sIdiQueSuministran & " " & m_sMatSeleccionado & ";"
        
        bHayAlgunFiltro = True
    End If
    If Not g_oEqpSeleccionado Is Nothing Then
        sSelectionText = Trim(sSelectionText) & " " & m_sIdiAsignEqp & " " & g_oEqpSeleccionado.Cod & ";"
        
        bHayAlgunFiltro = True
    End If
    
    'Premium
    '   Nunca visible luego nada q controlar
    
    'Datos de proveedores relacionados
    iBusquedaAvanzada = 0
    
    If fraBusqAvanzada.Visible Then
        If optProveedores(0) Then
            iBusquedaAvanzada = 2
            If sdbcTipoRelacSub.Text <> "" Then
                sTipoRel = sdbcTipoRelacSub.Columns(2).Value
                sSelectionText = Trim(sSelectionText) & " " & m_sIdiTipoRel & " " & sdbcTipoRelacSub.Columns(1).Value & ";"
                
                bHayAlgunFiltro = True
            End If
            If sdbcProveSubCod.Text <> "" Then
                sCodProvePriSub = sdbcProveSubCod.Text
                sSelectionText = Trim(sSelectionText) & " " & m_sIdiProveSub & " " & sdbcProvePriDen.Text & ";"
                
                bHayAlgunFiltro = True
            End If
        ElseIf optProveedores(1) Then
            iBusquedaAvanzada = 1
            bConTipoRel = optTRel(0)
            If sdbcTipoRelacPri.Text <> "" Then
                sTipoRel = sdbcTipoRelacPri.Columns(2).Value
                If bConTipoRel Then
                    sSelectionText = Trim(sSelectionText) & " " & m_sIdiConTipoRel & " " & sdbcTipoRelacPri.Columns(1).Value & ";"
                Else
                    sSelectionText = Trim(sSelectionText) & " " & m_sIdiSinTipoRel & " " & sdbcTipoRelacPri.Columns(1).Value & ";"
                End If
                
                bHayAlgunFiltro = True
            End If
            If sdbcProvePriCod.Text <> "" Then
                sCodProvePriSub = sdbcProvePriCod.Text
                sSelectionText = Trim(sSelectionText) & " " & m_sIdiProvePri & " " & sdbcProvePriDen.Text & ";"
                
                bHayAlgunFiltro = True
            End If

        End If
    End If
    
    'DATOS OPCIONALES
    'datos b�sicos_______________________________________________________________________
    sSelectionTextRpt(2, 1) = "OcultarDatos"
    If chkDatosBasicos.Value = vbUnchecked Then
        sSelectionTextRpt(1, 1) = "S"
    Else
        sSelectionTextRpt(1, 1) = "N"
    End If
        
    'contactos ________________________________________________________________
    sSelectionTextRpt(2, 2) = "OcultarContactos"
    If chkContactos.Value = vbUnchecked Then
        sSelectionTextRpt(1, 2) = "S"
    Else
        sSelectionTextRpt(1, 2) = "N"
    End If
    
    'datos homologaci�n ______________________________________________
    sSelectionTextRpt(2, 3) = "OcultarHomologacion"
    If chkCalifica.Value = vbUnchecked Then
        sSelectionTextRpt(1, 3) = "S"
    Else
        sSelectionTextRpt(1, 3) = "N"
    End If
    
    '�especificaciones?____________________________________________________
    sSelectionTextRpt(2, 4) = "OcultarObs"
    If chkObs.Value = vbUnchecked Then
        sSelectionTextRpt(1, 4) = "S"
    Else
        sSelectionTextRpt(1, 4) = "N"
    End If
        

    
    sSelectionTextRpt(2, 5) = "txtTITULO"
    sSelectionTextRpt(1, 5) = m_srIdiTitulo
    
    sSelectionTextRpt(2, 6) = "txtSELECCION"
    sSelectionTextRpt(1, 6) = m_srIdiSeleccion
    
    sSelectionTextRpt(2, 7) = "txtPROVEEDOR"
    sSelectionTextRpt(1, 7) = m_srIdiProveedor & ":"
    
    sSelectionTextRpt(2, 8) = "txtNIF"
    sSelectionTextRpt(1, 8) = m_srIdiNif & ":"
    
    sSelectionTextRpt(2, 9) = "txtDIRECCION"
    sSelectionTextRpt(1, 9) = m_srIdiDireccion & ":"
    
    sSelectionTextRpt(2, 10) = "txtMONEDA"
    sSelectionTextRpt(1, 10) = m_srIdiMoneda & ":"
    
    sSelectionTextRpt(2, 11) = "txtURL"
    sSelectionTextRpt(1, 11) = m_srIdiURL & ":"
    
    sSelectionTextRpt(2, 12) = "txtHOMOLOGACION"
    sSelectionTextRpt(1, 12) = m_srIdiHomologacion & ":"
    
    sSelectionTextRpt(2, 13) = "txtOBSERVACIONES"
    sSelectionTextRpt(1, 13) = m_srIdiObservaciones & ":"
    
    sSelectionTextRpt(2, 14) = "txtPAG"
    sSelectionTextRpt(1, 14) = m_srIdiPag
    
    sSelectionTextRpt(2, 15) = "txtCODPORTAL"
    sSelectionTextRpt(1, 15) = m_srIdiCodPortal & ":"
    
    sSelectionTextRpt(2, 16) = "txtPremiumAct"
    sSelectionTextRpt(1, 16) = m_sPremiumAct
    
    sSelectionTextRpt(2, 17) = "txtPremiumNoAct"
    sSelectionTextRpt(1, 17) = m_sPremiumNoAct
    
    sSelectionTextRpt(2, 18) = "txtNoPremium"
    sSelectionTextRpt(1, 18) = m_sNoPremium
    
    sSelectionTextRpt(2, 19) = "txtArchAdj"
    sSelectionTextRpt(1, 19) = m_srIdiArchivosAdj & ":"
    
    
    'datos acceso web ______________________________________________________
    sSelectionTextRpt(2, 20) = "OcultarDatosWeb"
    If chkDatosWeb.Value = vbUnchecked Then
        sSelectionTextRpt(1, 20) = "S"
    Else
        sSelectionTextRpt(1, 20) = "N"
    End If
    sSelectionTextRpt(2, 21) = "txtCODWEB"
    sSelectionTextRpt(1, 21) = m_sIdiCodigoAcceso
    sSelectionTextRpt(2, 22) = "txtBLOQ"
    sSelectionTextRpt(1, 22) = m_sIdiCuentaBloq
    sSelectionTextRpt(2, 23) = "txtSiBloqueo"
    sSelectionTextRpt(1, 23) = m_sSi
    sSelectionTextRpt(2, 24) = "txtNoBloqueo"
    sSelectionTextRpt(1, 24) = m_sNo
    sSelectionTextRpt(2, 25) = "BLOQINTENTOS"
    If gParametrosGenerales.giLOGPREBLOQ > 0 Then
        sSelectionTextRpt(1, 25) = CStr(gParametrosGenerales.giLOGPREBLOQ)
    Else
        sSelectionTextRpt(1, 25) = CStr(10000)
    End If
        
    'codificaci�n ERP _______________________________________________________
    If Me.chkCodErp = vbChecked Then
        sSelectionTextRpt(1, 26) = "S"
    Else
        sSelectionTextRpt(1, 26) = "N"
    End If
    sSelectionTextRpt(2, 26) = "CODERP"
    
    '_______________________________________________________________________
    sSelectionTextRpt(2, 27) = "txtFormaPago"
    sSelectionTextRpt(1, 27) = m_sFormaPago
    
    sSelectionTextRpt(2, 28) = "txtViaPago"
    sSelectionTextRpt(1, 28) = m_sViaPago
    '�����������������������������������������������������������������������
    
    'proveedores relacionados____________________________________________________
    sSelectionTextRpt(2, 29) = "OcultarProvRel"
    If chkProvRel.Value = vbUnchecked Then
        sSelectionTextRpt(1, 29) = "S"
    Else
        sSelectionTextRpt(1, 29) = "N"
    End If
    
    sSelectionTextRpt(2, 30) = "txtAutoFactura"
    sSelectionTextRpt(1, 30) = m_sIdiAutoFactura
    
    sSelectionTextRpt(2, 31) = "txtAutofacturaEImpuestos"
    sSelectionTextRpt(1, 31) = m_sIdiAutoFacturaEImpuestos
    
    If (m_oProveedorSeleccionado Is Nothing) And sdbcProveCod.Text = "" Then
        scodProve = ""
    ElseIf sdbcProveCod.Text <> "" Then
        scodProve = sdbcProveCod.Text
        
        bHayAlgunFiltro = True
    Else
        scodProve = m_oProveedorSeleccionado.Cod
        
        bHayAlgunFiltro = True
    End If
    
    If bHayAlgunFiltro = False Then
        Screen.MousePointer = vbNormal
        oMensajes.ProveSeleccioneAlgo
        Set oFos = Nothing
        Exit Sub
    End If
    
    Set oReport = crs_crapp.OpenReport(sRepPath, crOpenReportByTempCopy)
    ConectarReport oReport, crs_Server, crs_Database, crs_User, crs_Password
    
    ' FORMULA FIELDS REPORT
    For i = 1 To UBound(sSelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, sSelectionTextRpt(2, i))).Text = """" & sSelectionTextRpt(1, i) & """"
    Next i
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IDI")).Text = """" & gParametrosInstalacion.gIdioma & """"
    
    If gParametrosGenerales.giINSTWEB = conweb Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "OcultarDatosPortal")).Text = """" & "S" & """"
    End If
        
    If chkCalifica.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptCAL1")
        ConectarReport SubListado, crs_Server, crs_Database, crs_User, crs_Password
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CAL1NOM")).Text = """" & gParametrosGenerales.gsDEN_CAL1 & ":" & """"
        Set SubListado = oReport.OpenSubreport("prtCAL2")
        ConectarReport SubListado, crs_Server, crs_Database, crs_User, crs_Password
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CAL2NOM")).Text = """" & gParametrosGenerales.gsDEN_CAL2 & ":" & """"
        Set SubListado = oReport.OpenSubreport("rptCAL3")
        ConectarReport SubListado, crs_Server, crs_Database, crs_User, crs_Password
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "CAL3NOM")).Text = """" & gParametrosGenerales.gsDEN_CAL3 & ":" & """"
    End If
    
    If chkContactos.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptCON")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCONTACTOS")).Text = """" & m_srIdiContactos & ":" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTfno1")).Text = """" & m_srIdiTfno1 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTfno2")).Text = """" & m_srIdiTfno2 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTfnoMovil")).Text = """" & m_srIdiMovil & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFax")).Text = """" & m_srIdiFax & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtMail")).Text = """" & m_srIdiMail & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNIF")).Text = """" & m_srIdiConNif & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrincipal")).Text = """" & m_srPrincipal & """"
    End If
    
    If Me.chkCodErp.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptProveErp")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTituloERP")).Text = """" & "C�digos ERP" & """"
        
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNIF")).Text = """" & m_sIdiCif & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDEN")).Text = """" & m_sIdiRazonSocial & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCODERP")).Text = """" & "C�digo" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDENERP")).Text = """" & m_sIdiDescripcion & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCAMPO1")).Text = """" & NullToStr(gParametrosGenerales.gsCampo1ERP) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCAMPO2")).Text = """" & NullToStr(gParametrosGenerales.gsCampo2ERP) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCAMPO3")).Text = """" & NullToStr(gParametrosGenerales.gsCampo3ERP) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCAMPO4")).Text = """" & NullToStr(gParametrosGenerales.gsCampo4ERP) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtMOSTRARCAMPO1")).Text = """" & IIf(gParametrosGenerales.gbCampo1ERPAct, "S", "N") & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtMOSTRARCAMPO2")).Text = """" & IIf(gParametrosGenerales.gbCampo2ERPAct, "S", "N") & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtMOSTRARCAMPO3")).Text = """" & IIf(gParametrosGenerales.gbCampo3ERPAct, "S", "N") & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtMOSTRARCAMPO4")).Text = """" & IIf(gParametrosGenerales.gbCampo4ERPAct, "S", "N") & """"
        
        '....
        
    End If
    If opOrdCod.Value = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOrden")).Text = """" & "COD" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtOrden")).Text = """" & "DEN" & """"
    End If
    
    If chkProvRel.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptPROREL")
        ConectarReport SubListado, crs_Server, crs_Database, crs_User, crs_Password
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPROVEREL")).Text = """" & m_sIdiProveRel & ":" & """"
        Set SubListado = oReport.OpenSubreport("rptPROPRI")
        ConectarReport SubListado, crs_Server, crs_Database, crs_User, crs_Password
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPROVEPRI")).Text = """" & m_sIdiProveRelPri & """"
    End If
    Set SubListado = Nothing
    
    
    
    If gParametrosGenerales.gbPremium Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "PortalPremium")).Text = """" & "T" & """"
    End If
        
    If g_oGMN1Seleccionado Is Nothing Then
        sCodGMN1 = ""
    Else
        sCodGMN1 = g_oGMN1Seleccionado.Cod
    End If
    If g_oGMN2Seleccionado Is Nothing Then
        sCodGMN2 = ""
    Else
        sCodGMN2 = g_oGMN2Seleccionado.Cod
    End If
    If g_oGMN3Seleccionado Is Nothing Then
        sCodGMN3 = ""
    Else
        sCodGMN3 = g_oGMN3Seleccionado.Cod
    End If
    If g_oGMN4Seleccionado Is Nothing Then
        sCodGMN4 = ""
    Else
        sCodGMN4 = g_oGMN4Seleccionado.Cod
    End If
    If g_oEqpSeleccionado Is Nothing Then
        sCodEqp = ""
    Else
        sCodEqp = g_oEqpSeleccionado.Cod
    End If
    
    If m_oPaisSeleccionado Is Nothing Then
        scodPais = ""
    Else
        scodPais = m_oPaisSeleccionado.Cod
    End If
    If m_oProviSeleccionada Is Nothing Then
        sCodProvi = ""
    Else
        sCodProvi = m_oProviSeleccionada.Cod
    End If
    If m_oMonSeleccionada Is Nothing Then
        sCodMon = ""
    Else
        sCodMon = m_oMonSeleccionada.Cod
    End If
    
    Dim sSQLQueryString As String
    sSQLQueryString = oCRProveedores.ObtenerSQLProveedores(scodProve, sCodGMN1, sCodGMN2, sCodGMN3, sCodGMN4, sCodEqp, scodPais, sCodProvi, sCodMon, opOrdCod, , , , , chkDatosWeb, iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub)
    Dim oProveedores As CProveedores
    Dim ADORs As ADODB.Recordset
    Set oProveedores = oFSGSRaiz.generar_CProveedores
    Set ADORs = oProveedores.CargarDatosListadoProveedores(sSQLQueryString)
    If Not ADORs Is Nothing Then
        oCRProveedores.ListadoProveedores ADORs, oReport, scodProve, sCodGMN1, sCodGMN2, sCodGMN3, sCodGMN4, sCodEqp, scodPais, sCodProvi, sCodMon, opOrdCod, chkContactos, chkCalifica, , , , , chkDatosWeb, Me.chkCodErp = vbChecked, chkProvRel, iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
    End If
    Set oProveedores = Nothing
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    Me.Hide
    
    frmESPERA.lblGeneral.caption = m_sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = m_sIdiSeleccionando
    frmESPERA.lblDetalle = m_sIdiVisualizando
    
    Set pv = New Preview
    pv.Hide
    pv.caption = m_sIdiTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    

End Sub

''' <summary>Activaci�n del formulario</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub Form_Activate()
    Me.Width = 6810
    Me.Height = 6505
    stabProve.Height = 5655
    Picture1.Top = 5215
    fraSelComp.Top = 480

    chkDatosWeb.Visible = False
    
    If framePremium.Visible = False Then
        lblBusqAvanzada.Top = lblBusqAvanzada.Top - framePremium.Height
        fraBusqAvanzada.Top = fraBusqAvanzada.Top - framePremium.Height
    End If
        
    
    If g_bREqp Then
        
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp.caption = basOptimizacion.gCodEqpUsuario & " " & oUsuarioSummit.comprador.DenEqp
        sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
        Set g_oEqpSeleccionado = Nothing
        Set g_oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
        g_oEqpSeleccionado.Cod = basOptimizacion.gCodEqpUsuario
    End If
    
    If FSEPConf = True Then
            fraEqp.Visible = False
            chkDatosWeb.Visible = False
            stabProve.Height = 7200
            Picture1.Top = 6155
            Frame1.Top = 2200
    Else
        fraEqp.Visible = True
        Frame1.Top = 2400
        fraSelComp.Height = 1935
    
    End If
    
If basParametros.gParametrosGenerales.gbActivarCodProveErp Then
    Me.chkCodErp.Visible = True
Else
    Me.chkCodErp.Visible = False
End If


Dim iNumChecks As Integer
Dim iIntervalo As Integer


If Me.chkDatosBasicos.Visible Then
    iNumChecks = iNumChecks + 1
End If

If Me.chkContactos.Visible Then
    iNumChecks = iNumChecks + 1
End If

If Me.chkCalifica.Visible Then
    iNumChecks = iNumChecks + 1
End If

If Me.chkObs.Visible Then
    iNumChecks = iNumChecks + 1
End If

If Me.chkDatosWeb.Visible Then
    iNumChecks = iNumChecks + 1
End If

If Me.chkCodErp.Visible Then
    iNumChecks = iNumChecks + 1
End If

If Me.chkProvRel.Visible Then
    iNumChecks = iNumChecks + 1
End If

iIntervalo = 1800 / (iNumChecks - 1)
Dim iTop As Integer

iTop = 240

If Me.chkDatosBasicos.Visible Then
    Me.chkDatosBasicos.Top = iTop
    iTop = iTop + iIntervalo
End If

If Me.chkContactos.Visible Then
    Me.chkContactos.Top = iTop
    iTop = iTop + iIntervalo
End If

If Me.chkCalifica.Visible Then
    Me.chkCalifica.Top = iTop
    iTop = iTop + iIntervalo
End If

If Me.chkObs.Visible Then
    Me.chkObs.Top = iTop
    iTop = iTop + iIntervalo
End If

If Me.chkDatosWeb.Visible Then
    Me.chkDatosWeb.Top = iTop
    iTop = iTop + iIntervalo
End If

If Me.chkCodErp.Visible Then
    Me.chkCodErp.Top = iTop
    iTop = iTop + iIntervalo
End If

If Me.chkProvRel.Visible Then
    Me.chkProvRel.Top = iTop
    iTop = iTop + iIntervalo
End If
    
    
End Sub

''' <summary>Evento descarga del formulario, donde se borran los objetos</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub Form_Unload(Cancel As Integer)

    Set oTiposRelac = Nothing
    Set g_oEqpSeleccionado = Nothing
    Set m_oPaisSeleccionado = Nothing
    Set m_oProviSeleccionada = Nothing
    Set m_oMonSeleccionada = Nothing
    Set m_oProveedorSeleccionado = Nothing
    Set g_oGMN1Seleccionado = Nothing
    Set g_oGMN2Seleccionado = Nothing
    Set g_oGMN3Seleccionado = Nothing
    Set g_oGMN4Seleccionado = Nothing

    Set m_oGruposMN1 = Nothing
    Set m_oGruposMN2 = Nothing
    Set m_oGruposMN3 = Nothing
    Set m_oGruposMN4 = Nothing
    Set m_oEqps = Nothing
    Set m_oPaises = Nothing
    Set m_oMonedas = Nothing

    
End Sub

''' <summary>Evento de click en la etiqueta de b�squeda avanzada</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub lblBusqAvanzada_Click()
Dim vsstabProveH As Integer
Dim iH As Integer

    fraBusqAvanzada.Visible = Not fraBusqAvanzada.Visible
    If fraBusqAvanzada.Visible Then
        lblBusqAvanzada.caption = sLiteralOcultar
        vsstabProveH = Me.Height
        iH = 4000
        Me.Height = vsstabProveH + iH
        stabProve.Height = stabProve.Height + iH
    Else
        lblBusqAvanzada.caption = sLiteralBusqAvanzada
        vsstabProveH = Me.Height
        iH = 4000
        Me.Height = vsstabProveH - iH
        stabProve.Height = stabProve.Height - iH
    End If
End Sub

''' <summary>Evento de selecci�n de una opci�n de b�squeda de proveedores relacionados</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub optProveedores_Click(Index As Integer)
    If Index = 0 Then
        fraProveSub.Enabled = True
        fraProvePri.Enabled = False
        sdbcTipoRelacSub.Enabled = True
        sdbcProveSubCod.Enabled = True
        sdbcProveSubDen.Enabled = True
        optTRel(0).Enabled = False
        optTRel(1).Enabled = False
        sdbcTipoRelacPri.Enabled = False
        sdbcProvePriCod.Enabled = False
        sdbcProvePriDen.Enabled = False
        
    ElseIf Index = 1 Then
        fraProveSub.Enabled = False
        fraProvePri.Enabled = True
        sdbcTipoRelacSub.Enabled = False
        sdbcProveSubCod.Enabled = False
        sdbcProveSubDen.Enabled = False
        optTRel(0).Enabled = True
        optTRel(1).Enabled = True
        sdbcTipoRelacPri.Enabled = True
        sdbcProvePriCod.Enabled = True
        sdbcProvePriDen.Enabled = True
    ElseIf Index = 2 Then
        fraProveSub.Enabled = False
        fraProvePri.Enabled = False
        sdbcTipoRelacSub.Enabled = False
        sdbcProveSubCod.Enabled = False
        sdbcProveSubDen.Enabled = False
        optTRel(0).Enabled = False
        optTRel(1).Enabled = False
        sdbcTipoRelacPri.Enabled = False
        sdbcProvePriCod.Enabled = False
        sdbcProvePriDen.Enabled = False
    End If
    
End Sub

Private Sub sdbcProveCod_Change()
    
    If Not g_bProveRespetarCombo Then
    
        g_bProveRespetarCombo = True
        sdbcProveDen.Text = ""
        g_bProveRespetarCombo = False
        
        m_bProveCargarComboDesde = True
        
        Set m_oProveedorSeleccionado = Nothing
        
    End If
    
End Sub

Private Sub sdbcProveCod_CloseUp()
 
If sdbcProveCod.Value = "..." Then
    sdbcProveCod.Text = ""
    Exit Sub
End If
    
g_bProveRespetarCombo = True
sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
g_bProveRespetarCombo = False
    
Screen.MousePointer = vbHourglass

'Set m_oProveedorSeleccionado = m_oProveedores.Item(sdbcProveCod.Columns(0).Value)
ProveedorSeleccionado
 
m_bProveCargarComboDesde = False
        
Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveCod_DropDown()
    'Dim adoresProve As Ador.Recordset
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    Set m_oProveedores = Nothing
    Set m_oProveedores = oFSGSRaiz.generar_CProveedores
    sdbcProveCod.RemoveAll
    If m_bProveCargarComboDesde Then
        m_oProveedores.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), Trim(sdbcProveCod.Text), , Trim(sdbcPaiCod.Text), Trim(sdbcMonCod.Text), Trim(sdbcProviCod.Text), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False
    Else
        m_oProveedores.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , , Trim(sdbcPaiCod.Text), Trim(sdbcMonCod.Text), Trim(sdbcProviCod.Text), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False
    End If
    Codigos = m_oProveedores.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oProveedores.EOF Then
        sdbcProveCod.AddItem "..."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
  
End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    m_bGMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    m_bGMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    m_bGMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

    Dim udtCodigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN1_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
        
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        If m_bGMN1CargarComboDesde Then
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If m_bGMN1CargarComboDesde Then
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            m_oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    udtCodigos = m_oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem udtCodigos.Cod(i) & Chr(m_lSeparador) & udtCodigos.Den(i)
   
    Next

    If m_bGMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

Dim oGMN1s As CGruposMatNivel1
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    
    If sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
    Else
         oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set g_oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        m_bGMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        m_bGMN1RespetarCombo = False
        GMN1Seleccionado
        m_bGMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Public Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)
Dim oGMN2s As CGruposMatNivel2
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not g_oGMN1Seleccionado Is Nothing Then
    
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
             Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
            g_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = g_oGMN1Seleccionado.GruposMatNivel2
        End If
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        m_bGMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        
        m_bGMN2RespetarCombo = False
        GMN2Seleccionado
        m_bGMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Public Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

Dim oGMN3s As CGruposMatNivel3
Dim oIMAsig As IMaterialAsignado
Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not g_oGMN2Seleccionado Is Nothing Then
    
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
             g_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = g_oGMN2Seleccionado.GruposMatNivel3
        End If
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
    Else
        m_bGMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        
        m_bGMN3RespetarCombo = False
        GMN3Seleccionado
        m_bGMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not m_bGMN3RespetarCombo Then

        m_bGMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        m_bGMN3RespetarCombo = False
        Set g_oGMN3Seleccionado = Nothing
        m_bGMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    m_bGMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    m_bGMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    m_bGMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

Dim udtCodigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If g_oGMN2Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
         Set m_oGruposMN3 = Nothing
        If m_bGMN3CargarComboDesde Then
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False)
        End If
        
    Else
        
         If m_bGMN3CargarComboDesde Then
            g_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            g_oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN3 = g_oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    udtCodigos = m_oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
   
    Next

    If m_bGMN3CargarComboDesde And Not m_oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Public Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If Not g_oGMN3Seleccionado Is Nothing Then
    
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
             g_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = g_oGMN3Seleccionado.GruposMatNivel4
        End If
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        m_bGMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        
        m_bGMN4RespetarCombo = False
        GMN4Seleccionado
        m_bGMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub
Private Sub sdbcGMN1_4Cod_Change()

    If Not m_bGMN1RespetarCombo Then
        If sdbcGMN2_4Cod.Value = "" And gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And m_bRMat Then
            cmdObtener.Enabled = False
        End If
        m_bGMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        m_bGMN1RespetarCombo = False
        Set g_oGMN1Seleccionado = Nothing
        m_bGMN1CargarComboDesde = True
        
    End If

End Sub
Private Sub sdbcGMN1_4Den_DropDown()
Dim udtCodigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer

    sdbcGMN1_4Den.RemoveAll

    Screen.MousePointer = vbHourglass
    If m_bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        If m_bGMN1CargarComboDesde Then
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If m_bGMN1CargarComboDesde Then
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            m_oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    udtCodigos = m_oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
   
    Next

    If m_bGMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not m_bGMN2RespetarCombo Then
    
        m_bGMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        m_bGMN2RespetarCombo = False
        Set g_oGMN2Seleccionado = Nothing
        m_bGMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    m_bGMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    m_bGMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    m_bGMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()
Dim udtCodigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer

    sdbcGMN2_4Cod.RemoveAll

    If g_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN2 = Nothing
        If m_bGMN2CargarComboDesde Then
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
       If m_bGMN2CargarComboDesde Then
            g_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            g_oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set m_oGruposMN2 = g_oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    udtCodigos = m_oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem udtCodigos.Cod(i) & Chr(m_lSeparador) & udtCodigos.Den(i)
   
    Next

    If m_bGMN2CargarComboDesde And Not m_oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not m_bGMN2RespetarCombo Then

        m_bGMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        m_bGMN2RespetarCombo = False
        Set g_oGMN2Seleccionado = Nothing
        m_bGMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    m_bGMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    m_bGMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    m_bGMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

Dim udtCodigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer

    sdbcGMN2_4Den.RemoveAll
    
    If g_oGMN1Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN2 = Nothing
        If m_bGMN2CargarComboDesde Then
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , False)
        End If
    
    Else
        
        If m_bGMN2CargarComboDesde Then
            g_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            g_oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set m_oGruposMN2 = g_oGMN1Seleccionado.GruposMatNivel2
        
    End If

    udtCodigos = m_oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
   
    Next

    If m_bGMN2CargarComboDesde And Not m_oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcGMN3_4Cod_Change()
    
    If Not m_bGMN3RespetarCombo Then
    
        m_bGMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        m_bGMN3RespetarCombo = False
        Set g_oGMN3Seleccionado = Nothing
        m_bGMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    m_bGMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    m_bGMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    m_bGMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

Dim udtCodigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer

    sdbcGMN3_4Cod.RemoveAll

    If g_oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN3 = Nothing
        If m_bGMN3CargarComboDesde Then
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
       If m_bGMN3CargarComboDesde Then
            g_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            g_oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN3 = g_oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    udtCodigos = m_oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem udtCodigos.Cod(i) & Chr(m_lSeparador) & udtCodigos.Den(i)
   
    Next

    If m_bGMN3CargarComboDesde And Not m_oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not m_bGMN1RespetarCombo Then
        If sdbcGMN1_4Den.Value = "" And gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And m_bRMat Then
            cmdObtener.Enabled = False
        End If
        m_bGMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        m_bGMN1RespetarCombo = False
        Set g_oGMN1Seleccionado = Nothing
        m_bGMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    m_bGMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    m_bGMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    m_bGMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
    If Not m_bGMN4RespetarCombo Then
    
        m_bGMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        m_bGMN4RespetarCombo = False
        Set g_oGMN4Seleccionado = Nothing
        m_bGMN4CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    m_bGMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    m_bGMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    m_bGMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

Dim udtCodigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer

    sdbcGMN4_4Cod.RemoveAll

    If g_oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN4 = Nothing
        If m_bGMN4CargarComboDesde Then
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else
        
        If m_bGMN4CargarComboDesde Then
            g_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            g_oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN4 = g_oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    udtCodigos = m_oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem udtCodigos.Cod(i) & Chr(m_lSeparador) & udtCodigos.Den(i)
   
    Next

    If m_bGMN4CargarComboDesde And Not m_oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not m_bGMN4RespetarCombo Then

        m_bGMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        m_bGMN4RespetarCombo = False
        Set g_oGMN4Seleccionado = Nothing
        m_bGMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    m_bGMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    m_bGMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    m_bGMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim udtCodigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Den.RemoveAll
    
    If g_oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN4 = Nothing
        If m_bGMN4CargarComboDesde Then
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False)
        End If
    Else
        
       If m_bGMN4CargarComboDesde Then
            g_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            g_oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN4 = g_oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    udtCodigos = m_oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
   
    Next

    If m_bGMN4CargarComboDesde And Not m_oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    Screen.MousePointer = vbNormal
    
End Sub




Private Sub cmdSelMat_Click()
 
    frmSELMAT.sOrigen = "frmLstPROVE"
    frmSELMAT.bRComprador = m_bRMat
    frmSELMAT.Show 1

End Sub

''' <summary>
''' Carga del formulario de b�squeda para listado
''' </summary>
''' <remarks>Llamada desde: frmLstProve; Tiempo m�ximo: 1 seg.</remarks>
''' <revision>JVS 29/08/2011</revision>
Private Sub Form_Load()
Dim adoresProve As Ador.Recordset

    Me.Width = 6810
    Me.Height = 6505
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    cmdObtener.Enabled = False
    Timer1.Enabled = False
    opOrdCod = True
    
    ConfigurarNombres
    
    ConfigurarSeguridad
    
    'Si no hay restricci�n de material pueden sacar todo, sino deben meter un mat.
    If Not m_bRMat Then
        cmdObtener.Enabled = True
    End If
    Set m_oEqps = oFSGSRaiz.Generar_CEquipos
    'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And m_bRMat Then
        CargarGMN1Automaticamente
    End If
    
    fraProveSub.Enabled = False
    fraProvePri.Enabled = False
    
    'Carga los tipos de relaci�n de proveedores
    Set oTiposRelac = oFSGSRaiz.Generar_CRelacTipos
    'If sOrigen = "ProveRelac" Then
        lblBusqAvanzada.Visible = True
        sdbcTipoRelacSub.Enabled = False
        sdbcProveSubCod.Enabled = False
        sdbcProveSubDen.Enabled = False
        optTRel(0).Enabled = False
        optTRel(1).Enabled = False
        sdbcTipoRelacPri.Enabled = False
        sdbcProvePriCod.Enabled = False
        sdbcProvePriDen.Enabled = False
        'Lista de proveedores ya relacionados
        If Not frmPROVE.g_oProveSeleccionado Is Nothing Then
            Set m_oProveRelac = frmPROVE.g_oProveSeleccionado.ProveRel
            sCodProveSelecc = frmPROVE.g_oProveSeleccionado.Cod
        End If
    'Else
    '    lblBusqAvanzada.Visible = False
    'End If
    fraBusqAvanzada.Visible = False
End Sub
Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then

    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVERestMatComp)) Is Nothing) Then
        m_bRMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorEQPRestEquipo)) Is Nothing) Then
        g_bREqp = True
    End If

End If

If Not basParametros.gParametrosGenerales.gbOblProveEqp Then
    
    fraEqp.Visible = False
    fraSelComp.Height = fraSelComp.Height - 400
    fraSelComp.Top = fraSelComp.Top + 150
    
End If


End Sub

Private Sub sdbcEqpCod_Change()

    If Not m_bEqpRespetarCombo Then
    
        m_bEqpRespetarCombo = True
        sdbcEqpDen.Text = ""
        m_bEqpRespetarCombo = False

        Set g_oEqpSeleccionado = Nothing
        m_bEqpCargarComboDesde = True
        
    End If


End Sub
Private Sub sdbcEqpCod_CloseUp()

    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    m_bEqpRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    m_bEqpRespetarCombo = False
    
    m_bEqpCargarComboDesde = False
    
    EquipoSeleccionado
    
End Sub

Private Sub sdbcEqpCod_DropDown()
Dim udtCodigos As TipoDatosCombo
Dim i As Integer
    
    sdbcEqpCod.RemoveAll

    Screen.MousePointer = vbHourglass
    If m_bEqpCargarComboDesde Then
        m_oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , , False
    Else
        m_oEqps.CargarTodosLosEquipos , , , , False
    End If
        
    udtCodigos = m_oEqps.DevolverLosCodigos

    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcEqpCod.AddItem udtCodigos.Cod(i) & Chr(m_lSeparador) & udtCodigos.Den(i)
   
    Next

    If m_bEqpCargarComboDesde And Not m_oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcEqpCod_InitColumnProps()
    
    sdbcEqpCod.DataField = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub


Private Sub sdbcEqpCod_PositionList(ByVal Text As String)
PositionList sdbcEqpCod, Text
End Sub

Public Sub sdbcEqpCod_Validate(Cancel As Boolean)
Dim oEquipos As CEquipos
Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
   Screen.MousePointer = vbHourglass
   oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        Set g_oEqpSeleccionado = Nothing
        sdbcEqpCod.Text = ""
    Else
        m_bEqpRespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        
        sdbcEqpCod.Columns(0).Value = sdbcEqpCod.Text
        sdbcEqpCod.Columns(1).Value = sdbcEqpDen.Text
        
        EquipoSeleccionado
        m_bEqpRespetarCombo = False
        m_bEqpCargarComboDesde = False
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpDen_Change()

    If Not m_bEqpRespetarCombo Then

        m_bEqpRespetarCombo = True
        sdbcEqpCod.Text = ""
        m_bEqpRespetarCombo = False
        Set g_oEqpSeleccionado = Nothing

        m_bEqpCargarComboDesde = True
        
    End If

    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpDen.RemoveAll
        
    End If
End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcEqpDen_CloseUp()
   
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    m_bEqpRespetarCombo = True
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    m_bEqpRespetarCombo = False
    
    m_bEqpCargarComboDesde = False
    
    EquipoSeleccionado
    
End Sub

Private Sub sdbcEqpDen_DropDown()
Dim udtCodigos As TipoDatosCombo
Dim i As Integer
    
    sdbcEqpDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If m_bEqpCargarComboDesde Then
        m_oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), , False
    Else
        m_oEqps.CargarTodosLosEquipos , , , , False
    End If
    
    udtCodigos = m_oEqps.DevolverLosCodigos

    For i = 0 To UBound(udtCodigos.Cod) - 1
   
        sdbcEqpDen.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
   
    Next

    If m_bEqpCargarComboDesde And Not m_oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)
PositionList sdbcEqpDen, Text
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)

Dim oProveedores As CProveedores
Dim bExiste As Boolean
    
    Set oProveedores = oFSGSRaiz.generar_CProveedores
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    oProveedores.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), Trim(sdbcProveCod.Text), , Trim(sdbcPaiCod.Text), Trim(sdbcMonCod.Text), Trim(sdbcProviCod.Text), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod)
    
    bExiste = Not (oProveedores.Count = 0)
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        g_bProveRespetarCombo = True
        sdbcProveDen.Text = oProveedores.Item(1).Den
        
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        
        Set m_oProveedorSeleccionado = oProveedores.Item(1)
         g_bProveRespetarCombo = False

    End If
    
    Set oProveedores = Nothing
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProveDen_Change()
    
    If Not g_bProveRespetarCombo Then
    
        g_bProveRespetarCombo = True
        sdbcProveCod.Text = ""
        g_bProveRespetarCombo = False
        m_bProveCargarComboDesde = True
        Set m_oProveedorSeleccionado = Nothing
        
    End If
    
End Sub

Private Sub sdbcProveDen_CloseUp()

If sdbcProveDen.Value = "..." Then
    sdbcProveDen.Text = ""
    Exit Sub
End If
    
g_bProveRespetarCombo = True
sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
g_bProveRespetarCombo = False
    
Screen.MousePointer = vbHourglass
'Set m_oProveedorSeleccionado = m_oProveedores.Item(sdbcProveDen.Columns(1).Text)
ProveedorSeleccionado
m_bProveCargarComboDesde = False
        
Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
     
    Screen.MousePointer = vbHourglass
    Set m_oProveedores = Nothing
    Set m_oProveedores = oFSGSRaiz.generar_CProveedores
    
    sdbcProveDen.RemoveAll
       
    If m_bProveCargarComboDesde Then
        m_oProveedores.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , Trim(sdbcProveDen.Text), Trim(sdbcPaiCod.Text), Trim(sdbcMonCod.Text), Trim(sdbcProviCod.Text), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False
    Else
        m_oProveedores.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , , Trim(sdbcPaiCod.Text), Trim(sdbcMonCod.Text), Trim(sdbcProviCod.Text), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False
    End If
    
    Codigos = m_oProveedores.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
  
    If Not m_oProveedores.EOF Then
        sdbcProveDen.AddItem "..."
    End If
   
    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh
    Screen.MousePointer = vbNormal
 
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub stabPROVE_Click(PreviousTab As Integer)

If stabProve.Tab = 1 Then
    ComprobarMaterialSeleccionado
End If

End Sub

Public Sub ComprobarMaterialSeleccionado()
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer

    If m_bRMat Then
        If g_oGMN4Seleccionado Is Nothing And g_oGMN3Seleccionado Is Nothing And g_oGMN2Seleccionado Is Nothing And g_oGMN1Seleccionado Is Nothing Then
            cmdObtener.Enabled = False
            stabProve.Tab = 0
            Exit Sub
        End If
           
        If Not g_oGMN4Seleccionado Is Nothing Then
            cmdObtener.Enabled = True
             Exit Sub
        End If
            
        If Not g_oGMN3Seleccionado Is Nothing Then
            Set oICompAsignado = g_oGMN3Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivelAsig = 0 Or iNivelAsig > 3 Then
                cmdObtener.Enabled = False
                stabProve.Tab = 0
                Exit Sub
            End If
            cmdObtener.Enabled = True
            Exit Sub
        End If
                
        If Not g_oGMN2Seleccionado Is Nothing Then
            Set oICompAsignado = g_oGMN2Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivelAsig = 0 Or iNivelAsig > 2 Then
                cmdObtener.Enabled = False
                stabProve.Tab = 0
                Exit Sub
            End If
            cmdObtener.Enabled = True
            Exit Sub
                
        End If
            
        If Not g_oGMN1Seleccionado Is Nothing Then
            Set oICompAsignado = g_oGMN1Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                cmdObtener.Enabled = False
                stabProve.Tab = 0
                Exit Sub
            End If
            cmdObtener.Enabled = True
            Exit Sub
            
        End If

    End If
 
End Sub
Private Sub ProveedorSeleccionado()
    Set m_oProveedores = Nothing
    Set m_oProveedores = oFSGSRaiz.generar_CProveedores
    m_oProveedores.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, sdbcProveCod.Text
    Set m_oProveedorSeleccionado = Nothing
    Set m_oProveedorSeleccionado = m_oProveedores.Item(sdbcProveCod.Text)
End Sub
Public Sub EquipoSeleccionado()
    
    Set g_oEqpSeleccionado = Nothing
    
    If Trim(sdbcEqpCod) <> "" Then
        Set g_oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
        g_oEqpSeleccionado.Cod = Trim(sdbcEqpCod)
         
    End If
End Sub
Private Sub PaisSeleccionado()
    
    Set m_oPaisSeleccionado = Nothing
    
    If Trim(sdbcPaiCod) <> "" Then
        Set m_oPaisSeleccionado = oFSGSRaiz.generar_CPais
        m_oPaisSeleccionado.Cod = Trim(sdbcPaiCod)
    End If

End Sub
Private Sub ProviSeleccionada()
    
    Set m_oProviSeleccionada = Nothing
    
    If Trim(sdbcProviCod) <> "" Then
        Set m_oProviSeleccionada = oFSGSRaiz.generar_CProvincia
        m_oProviSeleccionada.Cod = Trim(sdbcProviCod)
         
    End If

End Sub
Private Sub MonedaSeleccionada()
    
    Set m_oMonSeleccionada = Nothing
    
    If Trim(sdbcMonCod) <> "" Then
        Set m_oMonSeleccionada = oFSGSRaiz.generar_CMoneda
        m_oMonSeleccionada.Cod = Trim(sdbcMonCod)
         
    End If

End Sub

Private Sub GMN1Seleccionado()
    
    sdbcGMN2_4Cod.RemoveAll
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN2_4Den.RemoveAll
    sdbcGMN2_4Cod.Text = ""
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcGMN2_4Den.Text = ""
    
    Set g_oGMN1Seleccionado = Nothing
    Set g_oGMN2Seleccionado = Nothing
    Set g_oGMN3Seleccionado = Nothing
    Set g_oGMN4Seleccionado = Nothing
    
    If sdbcGMN1_4Cod <> "" Then
        Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
        g_oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
        ComprobarMaterialSeleccionado
    End If
    
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set g_oGMN3Seleccionado = Nothing
    Set g_oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    
    Set g_oGMN2Seleccionado = Nothing
    If sdbcGMN2_4Cod.Text <> "" Then
        Set g_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2

        g_oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
        g_oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    
        ComprobarMaterialSeleccionado
    End If
    
End Sub
Private Sub GMN3Seleccionado()
    

    Set g_oGMN3Seleccionado = Nothing
    Set g_oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    If sdbcGMN3_4Cod <> "" Then
        Set g_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
        g_oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
        g_oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
        g_oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    
        ComprobarMaterialSeleccionado
    End If
End Sub
Private Sub GMN4Seleccionado()

    Set g_oGMN4Seleccionado = Nothing
    If sdbcGMN4_4Cod <> "" Then
        Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
        g_oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
        g_oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
        g_oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
        g_oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
        ComprobarMaterialSeleccionado
    End If
    
End Sub



Private Sub ConfigurarNombres()

    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"
    
End Sub

Public Sub PonerMatSeleccionadoEnCombos()
    
    Set g_oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not g_oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = g_oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set g_oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not g_oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = g_oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set g_oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not g_oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = g_oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set g_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not g_oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = g_oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If
    
    '    ComprobarMaterialSeleccionado
  
End Sub

Private Sub sdbcPaiCod_Change()
    
    If Not m_bPaiRespetarCombo Then
    
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = ""
        m_bPaiRespetarCombo = False
        
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing
        
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
    
End Sub

Private Sub sdbcPaiCod_CloseUp()
    
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    m_bPaiRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    m_bPaiRespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiCod.Columns(0).Text)
    
    If Not m_oPaisSeleccionado.Moneda Is Nothing Then
        sdbcMonCod.Text = m_oPaisSeleccionado.Moneda.Cod
        sdbcMonCod_Validate False
    End If

    m_bPaiCargarComboDesde = False
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPaiCod_DropDown()

    Dim udtCodigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
     
    sdbcPaiCod.RemoveAll
    
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If
    
    udtCodigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
        sdbcPaiCod.AddItem udtCodigos.Cod(i) & Chr(m_lSeparador) & udtCodigos.Den(i)
    Next
    
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcPaiCod.AddItem "..."
    End If

    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPaiCod_InitColumnProps()

    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcPaiCod_PositionList(ByVal Text As String)
PositionList sdbcPaiCod, Text
End Sub
Private Sub sdbcPaiCod_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    If sdbcPaiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    
    Screen.MousePointer = vbHourglass
    oPaises.CargarTodosLosPaises sdbcPaiCod.Text, , True, , False
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiCod.Text = ""
    Else
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = oPaises.Item(1).Den
        
        sdbcPaiCod.Columns(0).Value = sdbcPaiCod.Text
        sdbcPaiCod.Columns(1).Value = sdbcPaiDen.Text
        
        m_bPaiRespetarCombo = False
        Set m_oPaisSeleccionado = oPaises.Item(1)
        m_bPaiCargarComboDesde = False
        If Not m_oPaisSeleccionado.Moneda Is Nothing Then
            sdbcMonCod.Text = m_oPaisSeleccionado.Moneda.Cod
            sdbcMonCod_Validate False
        End If

    End If
    
    Set oPaises = Nothing
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPaiDen_Change()
    
    If Not m_bPaiRespetarCombo Then
    
        m_bPaiRespetarCombo = True
        sdbcPaiCod.Text = ""
        m_bPaiRespetarCombo = False
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing

    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
        
End Sub
Private Sub sdbcPaiDen_CloseUp()
    
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    m_bPaiRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    m_bPaiRespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiDen.Columns(1).Text)
    If Not m_oPaisSeleccionado.Moneda Is Nothing Then
        sdbcMonCod.Text = m_oPaisSeleccionado.Moneda.Cod
        sdbcMonCod_Validate False
    End If
    
    PaisSeleccionado
    m_bPaiCargarComboDesde = False
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPaiDen_DropDown()
    
    Dim udtCodigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
     
    sdbcPaiDen.RemoveAll
    
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        m_oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    udtCodigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
        sdbcPaiDen.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
    Next
    
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiCod.Refresh
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub


Private Sub sdbcPaiDen_PositionList(ByVal Text As String)
PositionList sdbcPaiDen, Text
End Sub

Private Sub sdbcProviCod_Change()
    
    If Not m_bProviRespetarCombo Then
    
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = ""
        m_bProviRespetarCombo = False
        
        m_bProviCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcProviCod_Click()
    
    If Not sdbcProviCod.DroppedDown Then
        sdbcProviCod = ""
        sdbcProviDen = ""
    End If
    
End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcProviCod.Value = "..." Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProviCod.Value = "" Then Exit Sub
    
    m_bProviRespetarCombo = True
    sdbcProviDen.Text = sdbcProviCod.Columns(1).Text
    sdbcProviCod.Text = sdbcProviCod.Columns(0).Text
    m_bProviRespetarCombo = False
    
    m_bProviCargarComboDesde = False
    Screen.MousePointer = vbHourglass
    Set m_oProviSeleccionada = m_oPaisSeleccionado.Provincias.Item(sdbcProviCod.Columns(0).Text)
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProviCod_DropDown()

    Dim udtCodigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcProviCod.RemoveAll
        
    If Not m_oPaisSeleccionado Is Nothing Then
    
        
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProviCod.Text), , , False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , , False
        End If
        
        udtCodigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(udtCodigos.Cod) - 1
            sdbcProviCod.AddItem udtCodigos.Cod(i) & Chr(m_lSeparador) & udtCodigos.Den(i)
        Next
        
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then
            sdbcProviCod.AddItem "..."
        End If

    End If
    
    sdbcProviCod.SelStart = 0
    sdbcProviCod.SelLength = Len(sdbcProviCod.Text)
    sdbcProviCod.Refresh
    
End Sub
Private Sub sdbcProviCod_InitColumnProps()

    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)
PositionList sdbcProviCod, Text
End Sub
Private Sub sdbcProviCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    Screen.MousePointer = vbHourglass
    If Not m_oPaisSeleccionado Is Nothing Then
    
        m_oPaisSeleccionado.CargarTodasLasProvincias sdbcProviCod.Text, , True, , False
    
        bExiste = Not (m_oPaisSeleccionado.Provincias.Count = 0)
        
    Else
    
        bExiste = False
        
    End If
    
        
    If Not bExiste Then
        sdbcProviCod.Text = ""
    Else
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = m_oPaisSeleccionado.Provincias.Item(1).Den
        
        sdbcProviCod.Columns(0).Value = sdbcProviCod.Text
        sdbcProviCod.Columns(1).Value = sdbcProviDen.Text
        
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = False
        Set m_oProviSeleccionada = m_oPaisSeleccionado.Provincias.Item(1)
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcProviDen_Change()
    
    If Not m_bProviRespetarCombo Then
    
        m_bProviRespetarCombo = True
        sdbcProviCod.Text = ""
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcProviDen_Click()
    
    If Not sdbcProviDen.DroppedDown Then
        sdbcProviCod = ""
        sdbcProviDen = ""
    End If
    
End Sub

Private Sub sdbcProviDen_CloseUp()
    
    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProviDen.Value = "" Then Exit Sub
    
    m_bProviRespetarCombo = True
    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    m_bProviRespetarCombo = False
    
    m_bProviCargarComboDesde = False
    
    Screen.MousePointer = vbHourglass
    Set m_oProviSeleccionada = m_oPaisSeleccionado.Provincias.Item(sdbcProviDen.Columns(1).Text)
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcProviDen_DropDown()
    
    Dim udtCodigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcProviDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If Not m_oPaisSeleccionado Is Nothing Then
    
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProviDen.Text), True, False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        
        udtCodigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(udtCodigos.Cod) - 1
            sdbcProviDen.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
        Next
        
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then
            sdbcProviDen.AddItem "..."
        End If

    End If
    
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
    sdbcProviCod.Refresh

    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcProviDen_InitColumnProps()

    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProviDen_PositionList(ByVal Text As String)
PositionList sdbcProviDen, Text
End Sub

Private Sub sdbcMonCod_Change()
    
    If Not m_bMonRespetarCombo Then
    
        m_bMonRespetarCombo = True
        sdbcMonDen.Text = ""
        m_bMonRespetarCombo = False
        
        m_bMonCargarComboDesde = True
        Set m_oMonSeleccionada = Nothing
    End If
    
End Sub

Private Sub sdbcMonCod_CloseUp()
    
    If sdbcMonCod.Value = "..." Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    m_bMonRespetarCombo = True
    sdbcMonDen.Text = sdbcMonCod.Columns(1).Text
    sdbcMonCod.Text = sdbcMonCod.Columns(0).Text
    m_bMonRespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set m_oMonSeleccionada = m_oMonedas.Item(sdbcMonCod.Columns(0).Text)
    Screen.MousePointer = vbNormal
    m_bMonCargarComboDesde = False

        
End Sub

Private Sub sdbcMonCod_DropDown()

    Dim udtCodigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set m_oMonedas = Nothing
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
   
    sdbcMonCod.RemoveAll
    
    If m_bMonCargarComboDesde Then
        m_oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcMonCod.Text), , , True
    Else
        m_oMonedas.CargarTodasLasMonedas , , , , , False, True
    End If
    
    udtCodigos = m_oMonedas.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
        sdbcMonCod.AddItem udtCodigos.Cod(i) & Chr(m_lSeparador) & udtCodigos.Den(i)
    Next
    
    If m_bMonCargarComboDesde And Not m_oMonedas.EOF Then
        sdbcMonCod.AddItem "..."
    End If

    sdbcMonCod.SelStart = 0
    sdbcMonCod.SelLength = Len(sdbcMonCod.Text)
    sdbcMonCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcMonCod_InitColumnProps()

    sdbcMonCod.DataFieldList = "Column 0"
    sdbcMonCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMonCod_PositionList(ByVal Text As String)
PositionList sdbcMonCod, Text
End Sub
Private Sub sdbcMonCod_Validate(Cancel As Boolean)

    Dim oMonedas As CMonedas
    Dim bExiste As Boolean
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    
    If sdbcMonCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedas sdbcMonCod.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMonCod.Text = ""
    Else
        m_bMonRespetarCombo = True
        sdbcMonDen.Text = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        
        sdbcMonCod.Columns(0).Value = sdbcMonCod.Text
        sdbcMonCod.Columns(1).Value = sdbcMonDen.Text
        
        m_bMonRespetarCombo = False
        m_bMonCargarComboDesde = False
        Set m_oMonSeleccionada = oMonedas.Item(1)

    End If
    
    Set oMonedas = Nothing
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcMonden_Change()
    
    If Not m_bMonRespetarCombo Then
    
        m_bMonRespetarCombo = True
        sdbcMonCod.Text = ""
        m_bMonRespetarCombo = False
        m_bMonCargarComboDesde = True
        Set m_oMonSeleccionada = Nothing
    End If
    
End Sub
Private Sub sdbcMonDen_CloseUp()
    
    If sdbcMonDen.Value = "..." Then
        sdbcMonDen.Text = ""
        Exit Sub
    End If
    
    m_bMonRespetarCombo = True
    sdbcMonCod.Text = sdbcMonDen.Columns(1).Text
    sdbcMonDen.Text = sdbcMonDen.Columns(0).Text
    m_bMonRespetarCombo = False
    
    m_bMonCargarComboDesde = False
    
    Screen.MousePointer = vbHourglass
    Set m_oMonSeleccionada = m_oMonedas.Item(sdbcMonDen.Columns(1).Text)
    Screen.MousePointer = vbNormal
        
End Sub

Private Sub sdbcMonDen_DropDown()
    
    Dim udtCodigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set m_oMonedas = Nothing
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
     
    sdbcMonDen.RemoveAll
    
    If m_bMonCargarComboDesde Then
        m_oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcMonDen.Text), True, True
    Else
        m_oMonedas.CargarTodasLasMonedas , , , True, , False, True
    End If
    
    udtCodigos = m_oMonedas.DevolverLosCodigos
    
    For i = 0 To UBound(udtCodigos.Cod) - 1
        sdbcMonDen.AddItem udtCodigos.Den(i) & Chr(m_lSeparador) & udtCodigos.Cod(i)
    Next
    
    If m_bMonCargarComboDesde And Not m_oMonedas.EOF Then
        sdbcMonDen.AddItem "..."
    End If

    sdbcMonDen.SelStart = 0
    sdbcMonDen.SelLength = Len(sdbcMonDen.Text)
    sdbcMonCod.Refresh
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcMonDen_InitColumnProps()

    sdbcMonDen.DataFieldList = "Column 0"
    sdbcMonDen.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcMonDen_PositionList(ByVal Text As String)
PositionList sdbcMonDen, Text
End Sub

Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub

''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmLstProve.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub CargarRecursos()

Dim Adores As Ador.Recordset

    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
        caption = Adores(0).Value '1
        Adores.MoveNext
        stabProve.TabCaption(0) = Adores(0).Value
        Adores.MoveNext
        stabProve.TabCaption(1) = Adores(0).Value
        Adores.MoveNext
        lblPaiCod.caption = Adores(0).Value
        m_sIdiDelPais = Adores(0).Value
        Adores.MoveNext
        lblProviCod.caption = Adores(0).Value '5
        m_sIdiDeProvincia = Adores(0).Value
        Adores.MoveNext
        lblMonCod.caption = Adores(0).Value
        m_sIdiConMoneda = Adores(0).Value
        Adores.MoveNext
        lblEqpCod.caption = Adores(0).Value
        m_sIdiAsignEqp = Adores(0).Value
        Adores.MoveNext
        lblMaterial.caption = Adores(0).Value
        m_sIdiQueSuministran = Adores(0).Value
        Adores.MoveNext
        chkDatosBasicos.caption = Adores(0).Value
        Adores.MoveNext
        chkContactos.caption = Adores(0).Value '10
        Adores.MoveNext
        chkCalifica.caption = Adores(0).Value
        Adores.MoveNext
        chkObs.caption = Adores(0).Value
        Adores.MoveNext
        'lblOrden.Caption = Adores(0).Value
        Frame3.caption = Adores(0).Value
        Adores.MoveNext
        opOrdCod.caption = Adores(0).Value
        Adores.MoveNext
        opOrdDen.caption = Adores(0).Value '15 Denominaci�n
        sdbcGMN1_4Cod.Columns(1).caption = Adores(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Adores(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Adores(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Adores(0).Value
        sdbcGMN1_4Den.Columns(0).caption = Adores(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Adores(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Adores(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Adores(0).Value
        sdbcMonCod.Columns(1).caption = Adores(0).Value
        sdbcProveCod.Columns(1).caption = Adores(0).Value
        sdbcPaiCod.Columns(1).caption = Adores(0).Value
        sdbcProviCod.Columns(1).caption = Adores(0).Value
        sdbcEqpCod.Columns(1).caption = Adores(0).Value
        sdbcMonDen.Columns(0).caption = Adores(0).Value
        sdbcPaiDen.Columns(0).caption = Adores(0).Value
        sdbcProviDen.Columns(0).caption = Adores(0).Value
        sdbcEqpDen.Columns(0).caption = Adores(0).Value
        sdbcProveDen.Columns(0).caption = Adores(0).Value
        Adores.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Adores(0).Value '16 C�d
        sdbcGMN2_4Cod.Columns(0).caption = Adores(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Adores(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Adores(0).Value
        sdbcGMN1_4Den.Columns(1).caption = Adores(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Adores(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Adores(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Adores(0).Value
        sdbcMonCod.Columns(0).caption = Adores(0).Value
        sdbcPaiCod.Columns(0).caption = Adores(0).Value
        sdbcProviCod.Columns(0).caption = Adores(0).Value
        sdbcEqpCod.Columns(0).caption = Adores(0).Value
        sdbcProveCod.Columns(0).caption = Adores(0).Value
        sdbcMonDen.Columns(1).caption = Adores(0).Value
        sdbcPaiDen.Columns(1).caption = Adores(0).Value
        sdbcProviDen.Columns(1).caption = Adores(0).Value
        sdbcProveDen.Columns(1).caption = Adores(0).Value
        sdbcEqpDen.Columns(1).caption = Adores(0).Value
        
        Adores.MoveNext
        cmdObtener.caption = Adores(0).Value '18 Salto uno
        Adores.MoveNext
        m_srIdiMovil = Adores(0).Value '19
        Adores.MoveNext
        m_sIdiTitulo = Adores(0).Value '25
        Adores.MoveNext
        m_sIdiGenerando = Adores(0).Value
        Adores.MoveNext
        m_sIdiSeleccionando = Adores(0).Value
        Adores.MoveNext
        m_sIdiVisualizando = Adores(0).Value
                
        Adores.MoveNext
        'framePremium.caption = Adores(0).Value
        Adores.MoveNext
        'chkPremiumProv.caption = Adores(0).Value '30
        Adores.MoveNext
        'chkPremiumAct.caption = Adores(0).Value
        
        'Idioma del report
        Adores.MoveNext
        m_srIdiTitulo = Adores(0).Value '200
        Adores.MoveNext
        m_srIdiSeleccion = Adores(0).Value
        Adores.MoveNext
        m_srIdiProveedor = Adores(0).Value
        lblProve.caption = Adores(0).Value
        Adores.MoveNext
        m_srIdiNif = Adores(0).Value
        Adores.MoveNext
        m_srIdiDireccion = Adores(0).Value
        Adores.MoveNext
        m_srIdiMoneda = Adores(0).Value
        Adores.MoveNext
        m_srIdiHomologacion = Adores(0).Value
        Adores.MoveNext
        m_srIdiObservaciones = Adores(0).Value
        Adores.MoveNext
        m_srIdiContactos = Adores(0).Value
        Adores.MoveNext
        m_srIdiPag = Adores(0).Value
        Adores.MoveNext
        m_srIdiDe = Adores(0).Value
        Adores.MoveNext
        m_srIdiTfno1 = Adores(0).Value
        Adores.MoveNext
        m_srIdiTfno2 = Adores(0).Value
        Adores.MoveNext
        m_srIdiFax = Adores(0).Value
        Adores.MoveNext
        m_srIdiMail = Adores(0).Value
        Adores.MoveNext
        m_srIdiCodPortal = Adores(0).Value
        Adores.MoveNext
        m_sPremiumAct = Adores(0).Value
        Adores.MoveNext
        m_sPremiumNoAct = Adores(0).Value
        Adores.MoveNext
        m_sNoPremium = Adores(0).Value
        Adores.MoveNext
        m_srIdiURL = Adores(0).Value
        Adores.MoveNext
'        fraWeb.caption = Adores(0).Value
        Adores.MoveNext
'        chkSoloWeb.caption = Adores(0).Value
        Adores.MoveNext
        'chkBloq.caption = Adores(0).Value
        Adores.MoveNext
        chkDatosWeb.caption = Adores(0).Value
        Adores.MoveNext
        m_srIdiArchivosAdj = Adores(0).Value
        Adores.MoveNext
        m_sIdiCodigoAcceso = Adores(0).Value
        Adores.MoveNext
        m_sIdiCuentaBloq = Adores(0).Value
        Adores.MoveNext
        m_sSi = Adores(0).Value
        Adores.MoveNext
        m_sNo = Adores(0).Value
        
        Adores.MoveNext
        Me.chkCodErp.caption = Adores(0).Value
        Adores.MoveNext
        m_sIdiCif = Adores(0).Value
        Adores.MoveNext
        m_sIdiRazonSocial = Adores(0).Value
        Adores.MoveNext
        m_sIdiDescripcion = Adores(0).Value
        Adores.MoveNext
        m_sIdiCogigosERP = Adores(0).Value
        Adores.MoveNext
        m_sFormaPago = Adores(0).Value
        m_sFormaPago = m_sFormaPago & ":"
        Adores.MoveNext
        m_sViaPago = Adores(0).Value
        m_sViaPago = m_sViaPago & ":"
        Adores.MoveNext
        chkProvRel.caption = Adores(0).Value '236 Incluir proveedores relacionados
        Adores.MoveNext
        m_sIdiProveRel = Adores(0).Value '237 Proveedores relacionados:
        Adores.MoveNext
        m_sIdiProveRelPri = Adores(0).Value '238 Proveedores principales:
        Adores.MoveNext
        sLiteralBusqAvanzada = Adores(0).Value  '239 B�squeda Avanzada
        lblBusqAvanzada.caption = sLiteralBusqAvanzada
        Adores.MoveNext
        sLiteralOcultar = Adores(0).Value  '240 Ocultar
        Adores.MoveNext
        sLiteralTipoRel = Adores(0).Value  '241 Tipo de relaci�n
        Adores.MoveNext
        m_sIdiConTipoRel = Adores(0).Value  '242 Con tipo de relaci�n:
        Adores.MoveNext
        m_sIdiSinTipoRel = Adores(0).Value  '243 Sin tipo de relaci�n:
        Adores.MoveNext
        m_sIdiProveSub = Adores(0).Value  '244 Proveedor relacionado:
        Adores.MoveNext
        m_sIdiProvePri = Adores(0).Value  '245 Proveedor principal:
        Adores.MoveNext
        m_sIdiTipoRel = Adores(0).Value  '246 Tipo de relaci�n:
        Adores.MoveNext
        fraBusqAvanzada.caption = Adores(0).Value  '247 Proveedores relacionados
        Adores.MoveNext
        optProveedores(0).caption = Adores(0).Value  '248 Mostrar proveedores subordinados
        Adores.MoveNext
        lblProveRelac(0).caption = m_sIdiTipoRel  'Tipo de relaci�n:
        lblProveRelac(1).caption = Adores(0).Value  '249 Mostrar proveedores subordinados al siguiente proveedor:
        lblProveRelac(2).caption = lblProveRelac(1).caption
        Adores.MoveNext
        optProveedores(1).caption = Adores(0).Value  '250 Mostrar proveedores principales
        Adores.MoveNext
        optTRel(0).caption = Adores(0).Value  '251 Con tipo de relaci�n
        Adores.MoveNext
        optTRel(1).caption = Adores(0).Value  '252 Sin tipo de relaci�n
        Adores.MoveNext
        lblProveRelac(3).caption = Adores(0).Value  '253 Mostrar proveedores que tienen al siguiente proveedor como subordinado:
        Adores.MoveNext
        optProveedores(2).caption = Adores(0).Value  '254 Mostrar todos los proveedores
        Adores.MoveNext
        m_sIdiAutoFactura = Adores(0).Value
        Adores.MoveNext
        m_sIdiAutoFacturaEImpuestos = Adores(0).Value
        
        'Adores.MoveNext
        'm_srIdiConNif = Adores(0).Value '257 Nif
        m_srIdiConNif = "Nif"
        Adores.MoveNext
        m_srPrincipal = Adores(0).Value
        
        Adores.Close
    
    End If


   Set Adores = Nothing



End Sub

'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
Private Sub CargarGMN1Automaticamente()
    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
    Set m_oIMAsig = oUsuarioSummit.comprador
    Set oGMN1s = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
    If Not oGMN1s.Count = 0 Then
        m_bGMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        m_bGMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    Set oGMN1s = Nothing
End Sub

''' <summary>Evento de cambio del c�digo de proveedor principal </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_Change()
    
   If Not bRespetarComboProvePri Then
    
        bRespetarComboProvePri = True
        sdbcProvePriDen.Text = ""
        bRespetarComboProvePri = False
        
        bCargarComboPriDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre el c�digo de proveedor principal </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_Click()
     
     If Not sdbcProvePriCod.DroppedDown Then
        sdbcProvePriCod = ""
        sdbcProvePriDen = ""
        bCargarComboPriDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">c�digo de proveedor seleccionado</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_PositionList(ByVal Text As String)
PositionList sdbcProvePriCod, Text
End Sub

''' <summary>
''' Validacion del codigo del proveedor
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIasig As IAsignaciones
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oProves As CProveedores
    
    If sdbcProvePriCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProvePriCod.Text), , True, , False
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProvePriCod.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProvePriCod.Text = ""
    Else
        bRespetarComboProvePri = True
        sdbcProvePriCod.Text = oProves.Item(1).Cod
        sdbcProvePriDen.Text = oProves.Item(1).Den
        
        sdbcProvePriCod.Columns(0).Text = sdbcProvePriCod.Text
        sdbcProvePriCod.Columns(1).Text = sdbcProvePriDen.Text
            
        bRespetarComboProvePri = False
    End If
    bCargarComboPriDesde = True
End Sub

''' <summary>Evento de cambio de denominaci�n de proveedor principal </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_Change()
    
    If Not bRespetarComboProvePri Then
    
        bRespetarComboProvePri = True
        sdbcProvePriCod.Text = ""
        bRespetarComboProvePri = False
        bCargarComboPriDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre la denominaci�n de proveedor principal </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_Click()
    
    If Not sdbcProvePriDen.DroppedDown Then
        sdbcProvePriCod = ""
        sdbcProvePriDen = ""
        bCargarComboPriDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">c�digo de proveedor seleccionado</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_PositionList(ByVal Text As String)
PositionList sdbcProvePriDen, Text
End Sub

''' <summary>
''' Validacion de la denominaci�n del proveedor principal
''' </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIasig As IAsignaciones
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oProves As CProveedores
    
    If sdbcProvePriDen.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProvePriDen.Text), True, , False
                    
    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProvePriDen.Text = ""
    Else
        bRespetarComboProvePri = True
        sdbcProvePriCod.Text = oProves.Item(1).Cod
        sdbcProvePriDen.Text = oProves.Item(1).Den
        sdbcProvePriDen.Columns(1).Text = sdbcProvePriCod.Text
        sdbcProvePriDen.Columns(0).Text = sdbcProvePriDen.Text
        
        bRespetarComboProvePri = False
    End If
    Screen.MousePointer = vbNormal
    bCargarComboPriDesde = False
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_CloseUp()

    
    If sdbcProvePriDen.Value = "..." Then
        sdbcProvePriDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProvePriDen.Value = "" Then Exit Sub
    
    bRespetarComboProvePri = True
    sdbcProvePriCod.Text = sdbcProvePriDen.Columns(1).Text
    sdbcProvePriDen.Text = sdbcProvePriDen.Columns(0).Text
    bRespetarComboProvePri = False
    bCargarComboPriDesde = False
    DoEvents
            
End Sub

''' <summary>
''' DropDown del combo de denominacion de proveedores, carga la lista
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el combo de denominacion de proveedor; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oIasig As IAsignaciones
    Dim sDen As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oProves As CProveedores
    
    sdbcProvePriDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
         
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboPriDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProvePriDen.Text), , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboPriDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProvePriDen.Text), , , , , , , , , , , True
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProvePriDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProvePriDen.AddItem "..."
    End If

    sdbcProvePriDen.SelStart = 0
    sdbcProvePriDen.SelLength = Len(sdbcProvePriDen.Text)
    sdbcProvePriCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboPriDesde = False
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_InitColumnProps()
    sdbcProvePriDen.DataFieldList = "Column 0"
    sdbcProvePriDen.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>Evento closeup en el combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_CloseUp()
    
    If sdbcProvePriCod.Value = "..." Then
        sdbcProvePriCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProvePriCod.Value = "" Then Exit Sub
    
    bRespetarComboProvePri = True
    sdbcProvePriDen.Text = sdbcProvePriCod.Columns(1).Text
    sdbcProvePriCod.Text = sdbcProvePriCod.Columns(0).Text
    bRespetarComboProvePri = False
    bCargarComboPriDesde = False
    DoEvents
        
End Sub

''' <summary>
''' DropDown del combo de c�digo de proveedores principales, carga la lista
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el combo de denominacion de proveedor; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oIasig As IAsignaciones
    Dim sCod As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oProves As CProveedores
    
    sdbcProvePriCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboPriDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProvePriCod.Text), , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboPriDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProvePriCod.Text)
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProvePriCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProvePriCod.AddItem "..."
    End If

    sdbcProvePriCod.SelStart = 0
    sdbcProvePriCod.SelLength = Len(sdbcProvePriCod.Text)
    sdbcProvePriCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboPriDesde = False
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_InitColumnProps()
    sdbcProvePriCod.DataFieldList = "Column 0"
    sdbcProvePriCod.DataFieldToDisplay = "Column 0"
End Sub



''' <summary>Evento de cambio de c�digo de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_Change()
    
   If Not bRespetarComboProveSub Then
    
        bRespetarComboProveSub = True
        sdbcProveSubDen.Text = ""
        bRespetarComboProveSub = False
        
        bCargarComboSubDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre el c�digo de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_Click()
     
     If Not sdbcProveSubCod.DroppedDown Then
        sdbcProveSubCod = ""
        sdbcProveSubDen = ""
        bCargarComboSubDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">c�digo de proveedor seleccionado</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_PositionList(ByVal Text As String)
PositionList sdbcProveSubCod, Text
End Sub

''' <summary>
''' Validacion del codigo del proveedor
''' </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIasig As IAsignaciones
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oProves As CProveedores
    
    If sdbcProveSubCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveSubCod.Text), , True, , False
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveSubCod.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProveSubCod.Text = ""
    Else
        bRespetarComboProveSub = True
        sdbcProveSubCod.Text = oProves.Item(1).Cod
        sdbcProveSubDen.Text = oProves.Item(1).Den
        
        sdbcProveSubCod.Columns(0).Text = sdbcProveSubCod.Text
        sdbcProveSubCod.Columns(1).Text = sdbcProveSubDen.Text
            
        bRespetarComboProveSub = False
    End If
    bCargarComboSubDesde = True
End Sub

'' <summary>Evento de cambio de denominaci�n de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_Change()
    
    If Not bRespetarComboProveSub Then
    
        bRespetarComboProveSub = True
        sdbcProveSubCod.Text = ""
        bRespetarComboProveSub = False
        bCargarComboSubDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre la denominaci�n de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_Click()
    
    If Not sdbcProveSubDen.DroppedDown Then
        sdbcProveSubCod = ""
        sdbcProveSubDen = ""
        bCargarComboSubDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">denominaci�n de proveedor seleccionada</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_PositionList(ByVal Text As String)
PositionList sdbcProveSubDen, Text
End Sub

''' <summary>Evento de validaci�n en el combo</summary>
''' <param name="Cancel">Cancela el cambio de columna o fila</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIasig As IAsignaciones
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oProves As CProveedores
    
    If sdbcProveSubDen.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveSubDen.Text), True, , False
                    
    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProveSubDen.Text = ""
    Else
        bRespetarComboProveSub = True
        sdbcProveSubCod.Text = oProves.Item(1).Cod
        sdbcProveSubDen.Text = oProves.Item(1).Den
        sdbcProveSubDen.Columns(1).Text = sdbcProveSubCod.Text
        sdbcProveSubDen.Columns(0).Text = sdbcProveSubDen.Text
        
        bRespetarComboProveSub = False
    End If
    Screen.MousePointer = vbNormal
    bCargarComboSubDesde = False
End Sub

''' <summary>Evento closeup en el combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_CloseUp()

    
    If sdbcProveSubDen.Value = "..." Then
        sdbcProveSubDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveSubDen.Value = "" Then Exit Sub
    
    bRespetarComboProveSub = True
    sdbcProveSubCod.Text = sdbcProveSubDen.Columns(1).Text
    sdbcProveSubDen.Text = sdbcProveSubDen.Columns(0).Text
    bRespetarComboProveSub = False
    bCargarComboSubDesde = False
    DoEvents
            
End Sub

''' <summary>
''' DropDown del combo de denominacion de proveedores, carga la lista
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el combo de denominacion de proveedor; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oIasig As IAsignaciones
    Dim sDen As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oProves As CProveedores
    
    sdbcProveSubDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
         
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboSubDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProveSubDen.Text), , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboSubDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProveSubDen.Text), , , , , , , , , , , True
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveSubDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProveSubDen.AddItem "..."
    End If

    sdbcProveSubDen.SelStart = 0
    sdbcProveSubDen.SelLength = Len(sdbcProveSubDen.Text)
    sdbcProveSubCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboSubDesde = False
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_InitColumnProps()
    sdbcProveSubDen.DataFieldList = "Column 0"
    sdbcProveSubDen.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_CloseUp()
    
    If sdbcProveSubCod.Value = "..." Then
        sdbcProveSubCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveSubCod.Value = "" Then Exit Sub
    
    bRespetarComboProveSub = True
    sdbcProveSubDen.Text = sdbcProveSubCod.Columns(1).Text
    sdbcProveSubCod.Text = sdbcProveSubCod.Columns(0).Text
    bRespetarComboProveSub = False
    bCargarComboSubDesde = False
    DoEvents
        
End Sub

''' <summary>Evento dropdown del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oIasig As IAsignaciones
    Dim sCod As String
    Dim bNuevoProce As Boolean
    Dim oProces As CProcesos
    Dim oProves As CProveedores
    
    
    sdbcProveSubCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboSubDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProveSubCod.Text), , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboSubDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProveSubCod.Text)
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveSubCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveSubCod.AddItem "..."
    End If

    sdbcProveSubCod.SelStart = 0
    sdbcProveSubCod.SelLength = Len(sdbcProveSubCod.Text)
    sdbcProveSubCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboSubDesde = False
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_InitColumnProps()
    sdbcProveSubCod.DataFieldList = "Column 0"
    sdbcProveSubCod.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_CloseUp()
    If sdbcTipoRelacSub.Value = "..." Or sdbcTipoRelacSub.Text = "" Then
        sdbcTipoRelacSub.Text = ""
        Exit Sub
    End If
    
    
End Sub

''' <summary>Evento dropdown del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_DropDown()
    Dim i As Integer
    Dim oTipoRelac As CRelacTipo
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoRelacSub.RemoveAll
    
    oTiposRelac.CargarTodosLosRelacTipos True
    
    For Each oTipoRelac In oTiposRelac
        sdbcTipoRelacSub.AddItem oTipoRelac.Cod & Chr(m_lSeparador) & oTipoRelac.Descripciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & Chr(m_lSeparador) & oTipoRelac.Id
    Next
        
    sdbcTipoRelacSub.Columns(2).Visible = False
    sdbcTipoRelacSub.SelStart = 0
    sdbcTipoRelacSub.SelLength = Len(sdbcTipoRelacSub.Text)
    sdbcTipoRelacSub.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_InitColumnProps()
    sdbcTipoRelacSub.DataFieldList = "Column 1"
    sdbcTipoRelacSub.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">denominaci�n de proveedor seleccionada</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoRelacSub.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoRelacSub.Rows - 1
            bm = sdbcTipoRelacSub.GetBookmark(i)
            If UCase(sdbcTipoRelacSub.Text) = UCase(Mid(sdbcTipoRelacSub.Columns(1).CellText(bm), 1, Len(sdbcTipoRelacSub.Text))) Then
                sdbcTipoRelacSub.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>Evento de validaci�n en el combo</summary>
''' <param name="Cancel">Cancela el cambio de columna o fila</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_Validate(Cancel As Boolean)
    
    If sdbcTipoRelacSub.Text = "" Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    oTiposRelac.CargarTodosLosRelacTipos True

    If oTiposRelac.Count = 0 Then
        sdbcTipoRelacSub.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sLiteralTipoRel
    End If

    Screen.MousePointer = vbNormal
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_CloseUp()
    If sdbcTipoRelacPri.Value = "..." Or sdbcTipoRelacPri.Text = "" Then
        sdbcTipoRelacPri.Text = ""
        Exit Sub
    End If
   
   
End Sub

''' <summary>Evento dropdown del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_DropDown()
    Dim i As Integer
    Dim oTipoRelac As CRelacTipo
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoRelacPri.RemoveAll
    
    oTiposRelac.CargarTodosLosRelacTipos True
    
    For Each oTipoRelac In oTiposRelac
        sdbcTipoRelacPri.AddItem oTipoRelac.Cod & Chr(m_lSeparador) & oTipoRelac.Descripciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & Chr(m_lSeparador) & oTipoRelac.Id
    Next
        
    sdbcTipoRelacPri.SelStart = 0
    sdbcTipoRelacPri.SelLength = Len(sdbcTipoRelacPri.Text)
    sdbcTipoRelacPri.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_InitColumnProps()
    sdbcTipoRelacPri.DataFieldList = "Column 1"
    sdbcTipoRelacPri.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">denominaci�n de proveedor seleccionada</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoRelacPri.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoRelacPri.Rows - 1
            bm = sdbcTipoRelacPri.GetBookmark(i)
            If UCase(sdbcTipoRelacPri.Text) = UCase(Mid(sdbcTipoRelacPri.Columns(1).CellText(bm), 1, Len(sdbcTipoRelacPri.Text))) Then
                sdbcTipoRelacPri.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>Evento de validaci�n en el combo</summary>
''' <param name="Cancel">Cancela el cambio de columna o fila</param>
''' <remarks>Llamada desde:frmLstProve; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_Validate(Cancel As Boolean)
    
    If sdbcTipoRelacPri.Text = "" Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    oTiposRelac.CargarTodosLosRelacTipos True

    If oTiposRelac.Count = 0 Then
        sdbcTipoRelacPri.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sLiteralTipoRel
    End If

    Screen.MousePointer = vbNormal
End Sub


