VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFlujosConfEtapa 
   Caption         =   "DConfiguraci�n de etapa"
   ClientHeight    =   7860
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8685
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosConfEtapa.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7860
   ScaleWidth      =   8685
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab sstabConf 
      Height          =   7275
      Left            =   120
      TabIndex        =   1
      Top             =   540
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   12832
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "DRoles/Acciones"
      TabPicture(0)   =   "frmFlujosConfEtapa.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblAccionesRol"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblRolSeleccionado"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblRolesEtapa"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdAnyadirRol"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdEliminarRol"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "sdbgAccionesRol"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "sdbgRoles"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "chkEtapaInt"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      TabCaption(1)   =   "DRoles/Cumplimentaci�n"
      TabPicture(1)   =   "frmFlujosConfEtapa.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblCumplimentacion"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "ssTabGrupos"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "sdbcRoles"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "cmdBajarCampo"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "cmdSubirCampo"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "sdbgCampos"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "DRestricciones de inicio y fin de etapa"
      TabPicture(2)   =   "frmFlujosConfEtapa.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "sdbgBloqueos"
      Tab(2).Control(1)=   "optBloqueo(0)"
      Tab(2).Control(2)=   "optBloqueo(1)"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "DAcciones"
      TabPicture(3)   =   "frmFlujosConfEtapa.frx":0D06
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "sdbgAcciones"
      Tab(3).Control(1)=   "cmdAnyadirAccion"
      Tab(3).Control(2)=   "cmdEliminarAccion"
      Tab(3).ControlCount=   3
      TabCaption(4)   =   "DListados"
      TabPicture(4)   =   "frmFlujosConfEtapa.frx":0D22
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "cmdDeshacer"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "cmdRestaurar"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "sdbcRolesListados"
      Tab(4).Control(3)=   "sdbgListados"
      Tab(4).Control(4)=   "cmdA�adir"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).Control(5)=   "cmdEliminar"
      Tab(4).Control(5).Enabled=   0   'False
      Tab(4).Control(6)=   "cmdModoEdicion"
      Tab(4).Control(6).Enabled=   0   'False
      Tab(4).Control(7)=   "lblListados"
      Tab(4).ControlCount=   8
      Begin VB.CheckBox chkEtapaInt 
         Caption         =   "DEtapa Integraci�n"
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   90
         MaskColor       =   &H00FFFFFF&
         TabIndex        =   29
         Top             =   330
         Width           =   2295
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgRoles 
         Height          =   1300
         Left            =   120
         TabIndex        =   28
         Top             =   900
         Width           =   8295
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   4
         DividerType     =   0
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   4
         Columns(0).Width=   14631
         Columns(0).Caption=   "ROL"
         Columns(0).Name =   "ROL"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3519
         Columns(1).Caption=   "PERMITIR_TRASLADO_BLOQUE"
         Columns(1).Name =   "PERMITIR_TRASLADO_BLOQUE"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   11
         Columns(1).FieldLen=   256
         Columns(1).Style=   2
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "PERMITIR_TRASLADO_ROL"
         Columns(3).Name =   "PERMITIR_TRASLADO_ROL"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   11
         Columns(3).FieldLen=   256
         _ExtentX        =   14631
         _ExtentY        =   2293
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   -72600
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   6750
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   -74880
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   6750
         Width           =   1005
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAcciones 
         Height          =   6300
         Left            =   -74880
         TabIndex        =   19
         Top             =   840
         Width           =   8295
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   3
         stylesets.count =   6
         stylesets(0).Name=   "No"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmFlujosConfEtapa.frx":0D3E
         stylesets(0).AlignmentPicture=   2
         stylesets(1).Name=   "S�"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmFlujosConfEtapa.frx":0D5A
         stylesets(1).AlignmentPicture=   2
         stylesets(2).Name=   "Gris"
         stylesets(2).BackColor=   -2147483633
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmFlujosConfEtapa.frx":0D76
         stylesets(3).Name=   "Normal"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmFlujosConfEtapa.frx":0D92
         stylesets(4).Name=   "ActiveRow"
         stylesets(4).ForeColor=   16777215
         stylesets(4).BackColor=   8388608
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmFlujosConfEtapa.frx":0DAE
         stylesets(5).Name=   "Amarillo"
         stylesets(5).BackColor=   12648447
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmFlujosConfEtapa.frx":0DCA
         DividerType     =   2
         BevelColorHighlight=   16777215
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   10583
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3466
         Columns(2).Caption=   "DETALLE"
         Columns(2).Name =   "DETALLE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   4
         Columns(2).ButtonsAlways=   -1  'True
         _ExtentX        =   14631
         _ExtentY        =   11112
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgBloqueos 
         Height          =   5940
         Left            =   -74880
         TabIndex        =   18
         Top             =   1200
         Width           =   8295
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   4
         stylesets.count =   6
         stylesets(0).Name=   "No"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmFlujosConfEtapa.frx":0DE6
         stylesets(0).AlignmentPicture=   2
         stylesets(1).Name=   "S�"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmFlujosConfEtapa.frx":0E02
         stylesets(1).AlignmentPicture=   2
         stylesets(2).Name=   "Gris"
         stylesets(2).BackColor=   -2147483633
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmFlujosConfEtapa.frx":0E1E
         stylesets(3).Name=   "Normal"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmFlujosConfEtapa.frx":0E3A
         stylesets(4).Name=   "ActiveRow"
         stylesets(4).ForeColor=   16777215
         stylesets(4).BackColor=   8388608
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmFlujosConfEtapa.frx":0E56
         stylesets(5).Name=   "Amarillo"
         stylesets(5).BackColor=   12648447
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmFlujosConfEtapa.frx":0E72
         DividerType     =   2
         BevelColorHighlight=   16777215
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID_ENLACE"
         Columns(0).Name =   "ID_ENLACE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   8758
         Columns(1).Caption=   "DHasta que no est�n aprobadas las siguientes etapas"
         Columns(1).Name =   "DEN_ETAPA_ORIGEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   2646
         Columns(2).Caption=   "DBloquear inicio"
         Columns(2).Name =   "INICIO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   11
         Columns(2).FieldLen=   256
         Columns(2).Style=   2
         Columns(3).Width=   2646
         Columns(3).Caption=   "DBloquear salida"
         Columns(3).Name =   "SALIDA"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   11
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         _ExtentX        =   14631
         _ExtentY        =   10477
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
         Height          =   5415
         Left            =   -74340
         TabIndex        =   17
         Top             =   1620
         Width           =   7635
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   12
         stylesets.count =   8
         stylesets(0).Name=   "Bloqueo"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmFlujosConfEtapa.frx":0E8E
         stylesets(0).AlignmentPicture=   4
         stylesets(1).Name=   "No"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmFlujosConfEtapa.frx":100F
         stylesets(1).AlignmentPicture=   2
         stylesets(2).Name=   "Calculado"
         stylesets(2).BackColor=   16766421
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmFlujosConfEtapa.frx":102B
         stylesets(2).AlignmentPicture=   1
         stylesets(3).Name=   "S�"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmFlujosConfEtapa.frx":1047
         stylesets(3).AlignmentPicture=   2
         stylesets(4).Name=   "Gris"
         stylesets(4).BackColor=   -2147483633
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmFlujosConfEtapa.frx":1063
         stylesets(5).Name=   "Normal"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmFlujosConfEtapa.frx":107F
         stylesets(6).Name=   "ActiveRow"
         stylesets(6).ForeColor=   16777215
         stylesets(6).BackColor=   8388608
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmFlujosConfEtapa.frx":109B
         stylesets(7).Name=   "Amarillo"
         stylesets(7).BackColor=   12648447
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmFlujosConfEtapa.frx":10B7
         DividerType     =   2
         BevelColorHighlight=   16777215
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   185
         Columns.Count   =   12
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(0).HeadStyleSet=   "Bloqueo"
         Columns(1).Width=   1931
         Columns(1).Name =   "ATRIBUTO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777160
         Columns(2).Width=   6429
         Columns(2).Caption=   "DDato"
         Columns(2).Name =   "DATO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "TIPO"
         Columns(3).Name =   "TIPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   2
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "CAMPO_GS"
         Columns(4).Name =   "CAMPO_GS"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   2
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "SUBTIPO"
         Columns(5).Name =   "SUBTIPO"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   2
         Columns(5).FieldLen=   256
         Columns(6).Width=   1376
         Columns(6).Caption=   "Dvis."
         Columns(6).Name =   "VISIBLE"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   11
         Columns(6).FieldLen=   256
         Columns(6).Style=   2
         Columns(7).Width=   1376
         Columns(7).Caption=   "DEsc."
         Columns(7).Name =   "ESCRITURA"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   11
         Columns(7).FieldLen=   256
         Columns(7).Style=   2
         Columns(8).Width=   503
         Columns(8).Name =   "BLOQUEO"
         Columns(8).Alignment=   2
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Style=   4
         Columns(8).ButtonsAlways=   -1  'True
         Columns(8).HeadStyleSet=   "Bloqueo"
         Columns(8).StyleSet=   "Bloqueo"
         Columns(9).Width=   1376
         Columns(9).Caption=   "DObl."
         Columns(9).Name =   "OBLIGATORIO"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   11
         Columns(9).FieldLen=   256
         Columns(9).Style=   2
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "INTRO"
         Columns(10).Name=   "INTRO"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   11
         Columns(10).FieldLen=   256
         Columns(10).Style=   2
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "VINCULADO"
         Columns(11).Name=   "VINCULADO"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   11
         Columns(11).FieldLen=   256
         _ExtentX        =   13467
         _ExtentY        =   9551
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAccionesRol 
         Height          =   4020
         Left            =   120
         TabIndex        =   16
         Top             =   3120
         Width           =   8295
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   6
         stylesets.count =   6
         stylesets(0).Name=   "No"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmFlujosConfEtapa.frx":10D3
         stylesets(0).AlignmentPicture=   2
         stylesets(1).Name=   "S�"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmFlujosConfEtapa.frx":10EF
         stylesets(1).AlignmentPicture=   2
         stylesets(2).Name=   "Gris"
         stylesets(2).ForeColor=   12632256
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmFlujosConfEtapa.frx":110B
         stylesets(3).Name=   "Normal"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmFlujosConfEtapa.frx":1127
         stylesets(4).Name=   "ActiveRow"
         stylesets(4).ForeColor=   16777215
         stylesets(4).BackColor=   8388608
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmFlujosConfEtapa.frx":1143
         stylesets(5).Name=   "Amarillo"
         stylesets(5).BackColor=   12648447
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmFlujosConfEtapa.frx":115F
         DividerType     =   2
         BevelColorHighlight=   16777215
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   6
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   1588
         Columns(1).Caption=   "ASIGNADO"
         Columns(1).Name =   "ASIGNADO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   11
         Columns(1).FieldLen=   256
         Columns(1).Style=   2
         Columns(2).Width=   5821
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1588
         Columns(3).Caption=   "APROBAR"
         Columns(3).Name =   "APROBAR"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   11
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         Columns(3).StyleSet=   "No"
         Columns(4).Width=   1588
         Columns(4).Caption=   "RECHAZAR"
         Columns(4).Name =   "RECHAZAR"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   11
         Columns(4).FieldLen=   256
         Columns(4).Style=   2
         Columns(5).Width=   3466
         Columns(5).Caption=   "DETALLE"
         Columns(5).Name =   "DETALLE"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Style=   4
         Columns(5).ButtonsAlways=   -1  'True
         _ExtentX        =   14631
         _ExtentY        =   7091
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdEliminarRol 
         Height          =   312
         Left            =   7965
         Picture         =   "frmFlujosConfEtapa.frx":117B
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   480
         Width           =   432
      End
      Begin VB.CommandButton cmdAnyadirRol 
         Height          =   312
         Left            =   7485
         Picture         =   "frmFlujosConfEtapa.frx":120D
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   480
         Width           =   432
      End
      Begin VB.CommandButton cmdAnyadirAccion 
         Height          =   312
         Left            =   -67515
         Picture         =   "frmFlujosConfEtapa.frx":128F
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   480
         Width           =   432
      End
      Begin VB.CommandButton cmdEliminarAccion 
         Height          =   312
         Left            =   -67035
         Picture         =   "frmFlujosConfEtapa.frx":1311
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   480
         Width           =   432
      End
      Begin VB.OptionButton optBloqueo 
         Caption         =   "DInicio y fin de etapa sin restricciones"
         Height          =   255
         Index           =   0
         Left            =   -74880
         TabIndex        =   11
         Top             =   480
         Width           =   5000
      End
      Begin VB.OptionButton optBloqueo 
         Caption         =   "DBloquear seg�n configuraci�n indicada:"
         Height          =   255
         Index           =   1
         Left            =   -74880
         TabIndex        =   10
         Top             =   795
         Width           =   5000
      End
      Begin VB.CommandButton cmdSubirCampo 
         Height          =   312
         Left            =   -74760
         Picture         =   "frmFlujosConfEtapa.frx":13A3
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   1620
         Width           =   375
      End
      Begin VB.CommandButton cmdBajarCampo 
         Height          =   312
         Left            =   -74760
         Picture         =   "frmFlujosConfEtapa.frx":13FD
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1980
         Width           =   375
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcRoles 
         Height          =   285
         Left            =   -74880
         TabIndex        =   8
         Top             =   480
         Width           =   8295
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   14631
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   14631
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin ComctlLib.TabStrip ssTabGrupos 
         Height          =   5940
         Left            =   -74880
         TabIndex        =   7
         Top             =   1200
         Width           =   8295
         _ExtentX        =   14631
         _ExtentY        =   10478
         _Version        =   327682
         BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
            NumTabs         =   1
            BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   ""
               Key             =   ""
               Object.Tag             =   ""
               ImageVarType    =   2
            EndProperty
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcRolesListados 
         Height          =   285
         Left            =   -74910
         TabIndex        =   20
         Top             =   480
         Width           =   8295
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   14631
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   14631
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgListados 
         Height          =   5265
         Left            =   -74880
         TabIndex        =   21
         Top             =   1290
         Width           =   8265
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         DefColWidth     =   3528
         BevelColorHighlight=   16777215
         AllowAddNew     =   -1  'True
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3528
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "LISPER"
         Columns(0).Name =   "LISPER"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         _ExtentX        =   14579
         _ExtentY        =   9287
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         Height          =   345
         Left            =   -74880
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   6750
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   -73740
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   6750
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   -67620
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   6750
         Width           =   1005
      End
      Begin VB.Label lblListados 
         Caption         =   "DIndique qu� listados podr� visualizar el rol en esta etapa:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   25
         Top             =   930
         Width           =   8280
      End
      Begin VB.Label lblCumplimentacion 
         Caption         =   "DIndique que campos tendr� visibles, cuales podr� modificar, y cuales ser�n obligatorios de cumplimentar:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   9
         Top             =   900
         Width           =   8280
      End
      Begin VB.Label lblRolesEtapa 
         Caption         =   "DConfigure la lista de roles que participaran en esta etapa:"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   630
         Width           =   5835
      End
      Begin VB.Label lblRolSeleccionado 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   2400
         Width           =   8295
      End
      Begin VB.Label lblAccionesRol 
         Caption         =   "DConfigure la lista acciones disponibles para el ROL seleccionado:"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   2805
         Width           =   5835
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcEtapas 
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8415
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   14843
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   14843
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
End
Attribute VB_Name = "frmFlujosConfEtapa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables Publicas
Public lIdFlujo As Long
Public lIdFormulario As Long
Public lIdBloqueSel As Long
Public iTipoSolicitud As Integer
Public m_bModifFlujo As Boolean

'Variables Privadas
Private msAcciones As String
Private msDetalle As String
Private msAsignado As String
Private msAprobar As String
Private msRechazar As String
Private msDato As String
Private msVisible As String
Private msEscritura As String
Private msObligatorio As String
Private msEtapas As String
Private msBloqIni As String
Private msBloqSal As String
Private msArchivoRpt As String
Private msDenominacion As String
Private msConsulta As String
Private msEdicion As String
Private msRol As String
Private msPermitirTraslados As String

Private m_oBloque As CBloque
Private m_oAcciones As CAccionesBloque
Private m_oRoles As CPMRoles
Private m_oEnlaces As CEnlaces

Private m_bErrorAccionesRol As Boolean
Private m_bErrorBloqueos As Boolean
Private m_bErrorCumplimentaciones As Boolean

Private m_bCargandoDatos As Boolean
Private m_bCambiaOrden As Boolean

Private arrIDI() As String

''' Variables de control
Private bModoEdicion As Boolean
Private bAnyadir As Boolean

''' Control de errores
Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean

Private m_bCambiosEtapaInt As Boolean
Private Sub chkEtapaInt_Click()
m_oBloque.EtapaInt = BooleanToSQLBinary(chkEtapaInt.Value)
m_bCambiosEtapaInt = True
End Sub

Private Sub cmdAnyadirAccion_Click()
    If Not m_oBloque Is Nothing Then
        
        frmFlujosDetalleAccion.m_bModifFlujo = True
        frmFlujosDetalleAccion.lIdFlujo = lIdFlujo
        frmFlujosDetalleAccion.lIdBloque_Orig = m_oBloque.Id
        frmFlujosDetalleAccion.m_AccionSummit = ACCAccionBloqueAnya
        frmFlujosDetalleAccion.m_bSelAccion = False
        frmFlujosDetalleAccion.iTipoSolicitud = iTipoSolicitud
        frmFlujosDetalleAccion.Show vbModal
        Unload frmFlujosDetalleAccion
        
        CargarDatosTab
    End If
End Sub


'Revisado por: SRA (08/11/2011)
'Descripcion: Abre una ventana para seleccionar el rol que se va a a�adir a la lista de roles.
'Llamada desde: al hacer click sobre cmdAnyadirRol
'Tiempo ejecucion:0,2seg.
Private Sub cmdAnyadirRol_Click()
    Dim teserror As TipoErrorSummit
    Dim lIdRol As Long
    Dim vDatos As Variant
    Dim bPermitirTraslados As Boolean

    vDatos = MostrarFormSELPMROL(lIdFlujo, "frmFlujosConfEtapa", oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz)
    lIdRol = 0
    If Not IsEmpty(vDatos) Then
        lIdRol = vDatos(0)
        bPermitirTraslados = vDatos(1)
    End If
        
    If Not m_oBloque Is Nothing And lIdRol > 0 Then
        teserror = m_oBloque.AnyadirRol(lIdRol, bPermitirTraslados)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCBloqueAnyaRol, "Bloque=" & m_oBloque.Id & ",Rol:" & lIdRol
        End If
        CargarDatosTab
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
    
End Sub

Private Sub cmdA�adir_Click()
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgListados.Scroll 0, sdbgListados.Rows - sdbgListados.Row
    
    If sdbgListados.VisibleRows > 0 Then
        If sdbgListados.VisibleRows >= sdbgListados.Rows Then
            sdbgListados.Row = sdbgListados.Rows
        Else
            sdbgListados.Row = sdbgListados.Rows - (sdbgListados.Rows - sdbgListados.VisibleRows) - 1
        End If
    End If
    
    bAnyadir = True
    If Me.Visible Then sdbgListados.SetFocus
End Sub

Private Sub cmdBajarCampo_Click()
Dim i As Integer
Dim arrValoresMat() As Variant
Dim arrValoresArt() As Variant
Dim arrValoresDen() As Variant
Dim arrValoresAux() As Variant
Dim bOrdenoMat, bOrdenoPais As Boolean
Dim bOrdenoArt As Boolean, bOrdenoProvi As Boolean
Dim bOrdenoCampoMat As Boolean, bOrdenoCampoPais As Boolean
Dim bOrdenoDen As Boolean
Dim vbm As Variant

    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    
With sdbgCampos
    If .AddItemRowIndex(.SelBookmarks.Item(0)) = .Rows - 1 Then Exit Sub
        
    m_bCambiaOrden = True
    i = 0
    ReDim arrValoresMat(.Columns.Count - 1)
    ReDim arrValoresArt(.Columns.Count - 1)
    ReDim arrValoresDen(.Columns.Count - 1)
    ReDim arrValoresAux(.Columns.Count - 1)
    
    'Ordenacion del Material // Provincia
    If (.Columns("CAMPO_GS").Value = TipoCampoGS.Pais And .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.provincia) Or _
        (.Columns("CAMPO_GS").Value = TipoCampoGS.material And ((.Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.CodArticulo) Or (.Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.NuevoCodArticulo))) Then
            If .Columns("CAMPO_GS").Value = TipoCampoGS.material And (.Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoGS.DenArticulo) Then
                If val(.Row) = .Rows - 3 Then Exit Sub
                bOrdenoMat = True
            Else
                If val(.Row) = .Rows - 2 Then Exit Sub
                bOrdenoPais = True
            End If
                
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).Value
            Next i
    
    'Ordenacion del articulo // Provincia
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.provincia Or .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
        vbm = .GetBookmark(-1)
        If (.Columns("CAMPO_GS").Value = TipoCampoGS.provincia And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais) Or _
           ((.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) Or (.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo) And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material) Then
            If .Columns("CAMPO_GS").Value = TipoCampoGS.provincia Or .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) <> TipoCampoGS.DenArticulo Then
                If val(.Row) = .Rows - 1 Then Exit Sub
                bOrdenoProvi = True
            Else
                If val(.Row) = .Rows - 2 Then Exit Sub
                bOrdenoArt = True
            End If
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).CellValue(vbm)
                arrValoresArt(i) = .Columns(i).Value
            Next i
        End If
    'Ordenacion de la denominacion
    ElseIf (.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
        If val(.Row) = .Rows - 1 Then Exit Sub
        bOrdenoDen = True
        For i = 0 To .Columns.Count - 1
            arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
            arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
            arrValoresDen(i) = .Columns(i).Value
        Next i
    Else
        vbm = .GetBookmark(1)
        
        If (.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais And .Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoGS.provincia) Or _
           (.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And ((.Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoGS.CodArticulo) Or .Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoGS.NuevoCodArticulo)) Then
            'Por si debajo del campo a bajar, esta el Material // Provincia
            If (.Columns("CAMPO_GS").CellValue(.GetBookmark(3)) = TipoCampoGS.DenArticulo) Then
                bOrdenoCampoMat = True
            Else
                bOrdenoCampoPais = True
            End If
            For i = 0 To .Columns.Count - 1
                arrValoresAux(i) = .Columns(i).Value
                arrValoresMat(i) = .Columns(i).CellValue(vbm)
                arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(2))
                arrValoresDen(i) = .Columns(i).CellValue(.GetBookmark(3))
            Next i
        End If
    End If
    
    
    If bOrdenoMat Or bOrdenoPais Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresArt(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresDen(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresArt(i)
        Next i
       
        If bOrdenoMat Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                arrValoresAux(i) = .Columns(i).Value
                .Columns(i).Value = arrValoresDen(i)
            Next i
            .MovePrevious
        End If
        
        .MovePrevious
        .MovePrevious
        If bOrdenoMat Then
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresAux(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
        Else
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresDen(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
        End If
        
        .MoveNext
        .SelBookmarks.RemoveAll

    ElseIf bOrdenoArt Or bOrdenoProvi Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresMat(i)  'donde est� la provincia pongo el pa�s
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresDen(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdenoArt Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                arrValoresAux(i) = .Columns(i).Value
                .Columns(i).Value = arrValoresDen(i)
            Next i
            .MovePrevious
        End If
        
        .MovePrevious
        .MovePrevious
        If bOrdenoArt Then
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresAux(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
        Else
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresDen(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
        End If
        
        .MoveNext
        .MoveNext
        .SelBookmarks.RemoveAll
    
    ElseIf bOrdenoCampoMat Or bOrdenoCampoPais Then
        .MoveNext
        .MoveNext
        If bOrdenoCampoMat Then
            .MoveNext
        End If
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
       
        If bOrdenoCampoMat Then
            .MovePrevious
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresDen(i)
            Next i
        End If
      
        .MovePrevious
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        .MovePrevious
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        .MoveNext
        If bOrdenoCampoMat Then
            .MoveNext
        End If
        .SelBookmarks.RemoveAll
        
    ElseIf bOrdenoDen = True Then
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresDen(i)
        Next i
        .MovePrevious
        .MovePrevious
        
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MovePrevious
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
        
        .MoveNext
        .MoveNext
        .MoveNext
        .SelBookmarks.RemoveAll
    Else  'Ordeno normal,de 1 en 1

        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
        Next i
        
        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresMat(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresAux(i)
        Next i
        .MovePrevious
        
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresMat(i)
        Next i
        
        .MoveNext
        .SelBookmarks.RemoveAll
    End If
    .SelBookmarks.Add .Bookmark
    .Update
End With
End Sub

Private Sub cmdDeshacer_Click()
    ''' * Objetivo: Deshacer la edicion en el pais actual
    
    sdbgListados.CancelUpdate
    sdbgListados.DataChanged = False
    
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False

    bAnyadir = False
End Sub

Private Sub cmdEliminar_Click()
    ''' * Objetivo: Eliminar el listado actual
    
    If sdbgListados.Rows = 0 Then Exit Sub
    
    sdbgListados.SelBookmarks.Add sdbgListados.Bookmark
    sdbgListados.DeleteSelected
    sdbgListados.SelBookmarks.RemoveAll
End Sub

Private Sub cmdEliminarAccion_Click()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    
    If sdbgAcciones.Columns("ID").Value <> "" And sdbgAcciones.Columns("ID").Value <> "0" Then
        If oMensajes.PreguntaEliminar(sdbgAcciones.Columns("DEN").Value) = vbYes Then
            Set oIBaseDatos = m_oAcciones.Item(CStr(sdbgAcciones.Columns("ID").Value))
            teserror = oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oIBaseDatos = Nothing
                Exit Sub
            Else
                basSeguridad.RegistrarAccion AccionesSummit.ACCAccionBloqueElim, "ID:" & sdbgAcciones.Columns("ID").Value
            End If
            CargarDatosTab
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: elimina el rol seleccionado de la lista
'Llamada desde: al hacer click sobre cmdEliminarRol
'Tiempo ejecucion:0,2seg.
Private Sub cmdEliminarRol_Click()
    Dim teserror As TipoErrorSummit
    Dim lIdRol As Long
    If Not m_oBloque Is Nothing And sdbgRoles.Columns("ID").Value <> "" Then
        lIdRol = sdbgRoles.Columns("ID").Value
        teserror = m_oBloque.EliminarRol(lIdRol)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCBloqueElimRol, "Bloque=" & m_oBloque.Id & ",Rol:" & lIdRol
        End If
        CargarDatosTab
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
End Sub

Private Sub cmdModoEdicion_Click()
 ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
        
        sdbgListados.AllowAddNew = True
        sdbgListados.AllowUpdate = True
        sdbgListados.AllowDelete = True
        
        cmdRestaurar.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
        
        cmdModoEdicion.caption = msConsulta
            
        bModoEdicion = True
    Else
        If sdbgListados.DataChanged = True Then
            v = sdbgListados.ActiveCell.Value
            If Me.Visible Then sdbgListados.SetFocus
            If (v <> "") Then
                sdbgListados.ActiveCell.Value = v
            End If

            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        End If
        
        sdbgListados.AllowAddNew = False
        sdbgListados.AllowUpdate = False
        sdbgListados.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdRestaurar.Visible = True
        
        cmdModoEdicion.caption = msEdicion
        
        bModoEdicion = False
    End If
    If Me.Visible Then sdbgListados.SetFocus
End Sub

Private Sub cmdRestaurar_Click()
    CargarListadosRol
End Sub

''' <summary>
''' Evento que salta al hacer click sobre la flecha para subir el campo una posici�n.
''' Si el material viene acompa�ado de c�digo de art�culo y denominaci�n estos se mueven tambi�n.
''' Lo mismo pasa con la provincia y el pa�s.
''' </summary>
''' <remarks>Tiempo m�ximo:0seg.</remarks>
Private Sub cmdSubirCampo_Click()
Dim i As Integer
Dim arrValoresMat() As Variant
Dim arrValoresArt() As Variant
Dim arrValoresDen() As Variant
Dim arrValoresAux() As Variant
Dim bOrdenoMat, bOrdenoPais As Boolean
Dim bOrdenoArt, bOrdenoProv As Boolean
Dim bOrdenoDen As Boolean
Dim bOrdenoCampoMat, bOrdenoCampoProv As Boolean
Dim vbm As Variant

    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    
    With sdbgCampos
    
    If .AddItemRowIndex(.SelBookmarks.Item(0)) = 0 Then Exit Sub
    
    m_bCambiaOrden = True
    ReDim arrValoresMat(.Columns.Count - 1)
    ReDim arrValoresArt(.Columns.Count - 1)
    ReDim arrValoresDen(.Columns.Count - 1)
    ReDim arrValoresAux(.Columns.Count - 1)
    i = 0
    '''Ordenacion del Material // Pais
    If (.Columns("CAMPO_GS").Value = TipoCampoGS.material) Or (.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
        vbm = .GetBookmark(1)
        If (.Columns("CAMPO_GS").Value = TipoCampoGS.Pais And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Or _
           (.Columns("CAMPO_GS").Value = TipoCampoGS.material And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Or _
           (.Columns("CAMPO_GS").Value = TipoCampoGS.material And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo) Then
           
            If (.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Or _
            (.Columns("CAMPO_GS").CellValue(.GetBookmark(2))) <> TipoCampoGS.DenArticulo Then
                bOrdenoPais = True
                For i = 0 To .Columns.Count - 1
                   arrValoresMat(i) = .Columns(i).Value
                   arrValoresArt(i) = .Columns(i).CellValue(vbm)
                
                Next i
            Else
                bOrdenoMat = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).Value
                    arrValoresArt(i) = .Columns(i).CellValue(vbm)
                    arrValoresDen(i) = .Columns(i).CellValue(.GetBookmark(2))
                Next i
            End If
        Else
            For i = 0 To .Columns.Count - 1
                arrValoresAux(i) = .Columns(i).Value
            Next i
        End If
    'Ordenacion del c�digo del art�culo // Provincia
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or _
    .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or _
     .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia Then
        If val(.Row) = 1 Then Exit Sub
        vbm = .GetBookmark(-1)
        If .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
            bOrdenoArt = True
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).CellValue(vbm)
                arrValoresArt(i) = .Columns(i).Value
                arrValoresDen(i) = .Columns(i).CellValue(.GetBookmark(1))
            Next i
        ElseIf .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais Or .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
            bOrdenoProv = True
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).CellValue(vbm)
                arrValoresArt(i) = .Columns(i).Value
            Next i
        End If

    'Ordenacion de la denominacion
    ElseIf (.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
        If val(.Row) = 2 Then Exit Sub
        bOrdenoDen = True
        For i = 0 To .Columns.Count - 1
            arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
            arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
            arrValoresDen(i) = .Columns(i).Value
        Next i
    Else
        vbm = .GetBookmark(-1)
        If .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Or _
         .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia Or _
         .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Then
           If .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Then
               bOrdenoCampoMat = True
           Else
               bOrdenoCampoProv = True
           End If
        End If
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
        Next i
    End If

    If bOrdenoMat Or bOrdenoArt Or bOrdenoDen Or bOrdenoPais Or bOrdenoProv Then
        .MovePrevious
        If bOrdenoArt Or bOrdenoDen Or bOrdenoProv Then
            .MovePrevious
        End If
        
        If bOrdenoDen Then
            .MovePrevious
        End If
        
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        
        If Not bOrdenoPais And Not bOrdenoProv Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresDen(i)
            Next i
        End If
        
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
        
        .SelBookmarks.RemoveAll
        
        If Not bOrdenoArt And Not bOrdenoDen And Not bOrdenoPais And Not bOrdenoProv Then
            .MovePrevious
        End If
        If Not bOrdenoDen And Not bOrdenoProv Then
            .MovePrevious
        End If
        .MovePrevious

    ElseIf bOrdenoCampoMat Or bOrdenoCampoProv Then
        
        .MovePrevious
        .MovePrevious
        If Not bOrdenoCampoProv Then
            .MovePrevious
        End If

        For i = 0 To .Columns.Count - 1
            arrValoresMat(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresAux(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresArt(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i

        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresDen(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresArt(i)
        Next i
        
        If Not bOrdenoCampoProv Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresDen(i)
            Next i
        End If

        .SelBookmarks.RemoveAll
        .MovePrevious
        .MovePrevious
        If Not bOrdenoCampoProv Then
            .MovePrevious
        End If

    Else  'Ordeno normal,de 1 en 1
    
        .MovePrevious
        For i = 0 To .Columns.Count - 1
            arrValoresMat(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresAux(i)
        Next i
        .MoveNext

        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .SelBookmarks.RemoveAll
        .MovePrevious
   End If

    .SelBookmarks.Add .Bookmark
    .Update
    End With
End Sub

''' <summary>
''' Se llama al m�todo que actualiza el campo ORDEN de todos los campos con el orden que ha quedado en el grid
''' Antes de 32100.7.002 se almacenaba el orden campo a campo a medida que lo cambiaban, ahora no solo se guarda al cambiar de pesta�a o al cerrar el form
''' Tambi�n se actualiza la variable de control para no tener que realizar la actualizaci�n m�s de una vez
''' </summary>
''' <remarks>Llamada desde: frmFlujos.frm, frmFlujosAnyadirEtapa.frm, frmFlujosConfEtapa.frm, frmFlujosDetalleAccion.frm, frmFujosNombreEtapa.frm
''' frmFlujosRoles, frmBloqueosCondiciones, frmBloqueoFlujos</remarks>

Private Function GrabarOrdenTodos() As Boolean
Dim oCumplimentacion As CPMConfCumplimentacion

Dim teserror As TipoErrorSummit
Dim i As Integer
Dim vBookmark  As Variant

GrabarOrdenTodos = True
With sdbgCampos
    For i = 0 To .Rows - 1
        vBookmark = .AddItemBookmark(i)
        Set oCumplimentacion = m_oBloque.Cumplimentaciones.Item(CStr(.Columns("ID").CellValue(vBookmark)))
        oCumplimentacion.Orden = i + 1
    Next
End With

teserror = m_oBloque.Cumplimentaciones.GuardarOrdenCampos
If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    GrabarOrdenTodos = False
End If
Set oCumplimentacion = Nothing

'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
frmFlujos.HayCambios
End Function

Private Sub Form_Load()
    Me.Height = 8370
    Me.Width = 8805
    m_bCambiosEtapaInt = False
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    ConfigurarSeguridad
    PonerFieldSeparator Me
    EstablecerBloqueSel
    bModoEdicion = False
    CargarDatosTab
End Sub


'Revisado por: SRA (08/11/2011)
'Descripcion: carga las descripciones en el idioma correspondiente
'Llamada desde:Form_Load
'Tiempo ejecucion:0,2seg.
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSCONFETAPA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1
        Ador.MoveNext
        sstabConf.TabCaption(3) = Ador(0).Value
        Ador.MoveNext
        sstabConf.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabConf.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sstabConf.TabCaption(2) = Ador(0).Value '5
        Ador.MoveNext
        cmdAnyadirAccion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarAccion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        msAcciones = Ador(0).Value
        Ador.MoveNext
        msDetalle = Ador(0).Value
        Ador.MoveNext
        lblRolesEtapa.caption = Ador(0).Value '10
        Ador.MoveNext
        cmdAnyadirRol.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarRol.ToolTipText = Ador(0).Value
        Ador.MoveNext
        lblAccionesRol.caption = Ador(0).Value
        Ador.MoveNext
        msAsignado = Ador(0).Value
        Ador.MoveNext
        lblCumplimentacion.caption = Ador(0).Value '15
        Ador.MoveNext
        cmdSubirCampo.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdBajarCampo.ToolTipText = Ador(0).Value
        Ador.MoveNext
        msDato = Ador(0).Value
        Ador.MoveNext
        msVisible = Ador(0).Value
        Ador.MoveNext
        msEscritura = Ador(0).Value '20
        Ador.MoveNext
        msObligatorio = Ador(0).Value
        Ador.MoveNext
        optBloqueo(0).caption = Ador(0).Value
        Ador.MoveNext
        optBloqueo(1).caption = Ador(0).Value
        Ador.MoveNext
        msEtapas = Ador(0).Value
        Ador.MoveNext
        msBloqIni = Ador(0).Value
        Ador.MoveNext
        msBloqSal = Ador(0).Value
        Ador.MoveNext
        
        
        sstabConf.TabCaption(4) = Ador(0).Value
        Ador.MoveNext
        lblListados = Ador(0).Value
        Ador.MoveNext
        msArchivoRpt = Ador(0).Value
        Ador.MoveNext
        msDenominacion = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        msEdicion = Ador(0).Value
        cmdModoEdicion.caption = Ador(0).Value
        Ador.MoveNext
        
        msConsulta = Ador(0).Value
        Ador.MoveNext
        
        msAprobar = Ador(0).Value
        Ador.MoveNext
        msRechazar = Ador(0).Value
        Ador.MoveNext
        
        msRol = Ador(0).Value
        Ador.MoveNext
        msPermitirTraslados = Ador(0).Value
        Ador.MoveNext
        chkEtapaInt.caption = Ador(0).Value
        Ador.Close
    End If
End Sub

Private Sub ConfigurarSeguridad()
    Dim i As Integer
    
    If Not m_bModifFlujo Then
        cmdAnyadirRol.Enabled = False
        cmdEliminarRol.Enabled = False
        sdbgAccionesRol.Columns("ASIGNADO").Locked = True
        
        cmdSubirCampo.Enabled = False
        cmdBajarCampo.Enabled = False
        
        For i = 0 To sdbgCampos.Cols - 1
            sdbgCampos.Columns(i).Locked = True
        Next
        
        optBloqueo(0).Enabled = False
        optBloqueo(1).Enabled = False
        sdbgBloqueos.AllowUpdate = False
        
        cmdAnyadirAccion.Enabled = False
        cmdEliminarAccion.Enabled = False
    End If
    
    If iTipoSolicitud = TipoSolicitud.SolicitudDePedidoCatalogo Or iTipoSolicitud = TipoSolicitud.SolicitudDePedidoContraPedidoAbierto Then
        sdbgCampos.Columns("BLOQUEO").Visible = False
        sdbgCampos.Columns("OBLIGATORIO").Visible = False
    Else
        sdbgCampos.Columns("BLOQUEO").Visible = True
        sdbgCampos.Columns("OBLIGATORIO").Visible = True
    End If
End Sub

Private Sub EstablecerBloqueSel()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    
    Set m_oBloque = oFSGSRaiz.Generar_CBloque
    m_oBloque.Id = lIdBloqueSel
    Set oIBaseDatos = m_oBloque
    teserror = oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oIBaseDatos = Nothing
        Set m_oBloque = Nothing
        Exit Sub
    End If
    sdbcEtapas.Value = m_oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
    chkEtapaInt.Value = BooleanToSQLBinary(m_oBloque.EtapaInt)
    Set oIBaseDatos = Nothing
    
    sstabConf.Tab = 0
    CargarDatosTab
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: carga los datos del tab seleccionado
'Llamada desde:al pinchar en cada tab
'Tiempo ejecucion:0,2seg.
Private Sub CargarDatosTab()
    If Not m_oBloque Is Nothing Then
        Select Case sstabConf.Tab
            
            Case 0  'Roles/Acciones
                cmdAnyadirRol.Enabled = (m_oBloque.Tipo <> TipoBloque.final)
                cmdEliminarRol.Enabled = (m_oBloque.Tipo <> TipoBloque.final)
                cmdAnyadirRol.Visible = True
                cmdEliminarRol.Visible = True
                cmdAnyadirAccion.Visible = False
                cmdEliminarAccion.Visible = False
                sdbgRoles.Enabled = (m_oBloque.Tipo <> TipoBloque.final)
                If m_oBloque.Tipo = TipoBloque.Peticionario Then
                    sdbgRoles.Columns("PERMITIR_TRASLADO_BLOQUE").Visible = False
                    sdbgRoles.Columns("ROL").Width = 8300
                Else
                    sdbgRoles.Columns("PERMITIR_TRASLADO_BLOQUE").Visible = True
                    sdbgRoles.Columns("ROL").Width = 6300
                    sdbgRoles.Columns("PERMITIR_TRASLADO_BLOQUE").Width = 2000
                End If
                sdbgAccionesRol.Enabled = (m_oBloque.Tipo <> TipoBloque.final)
                CargarRolesBloque
                If sdbgRoles.Columns("ID").Value <> "" Then
                    lblRolSeleccionado.caption = sdbgRoles.Columns("ROL").Value
                Else
                    lblRolSeleccionado.caption = ""
                End If
                CargarAccionesRolSeleccionado
            Case 1  'Roles/Cumplimentacion
                sdbcRoles.Enabled = (m_oBloque.Tipo <> TipoBloque.final)
                cmdSubirCampo.Enabled = (m_oBloque.Tipo <> TipoBloque.final) And m_bModifFlujo
                cmdBajarCampo.Enabled = (m_oBloque.Tipo <> TipoBloque.final) And m_bModifFlujo
                sdbgCampos.Enabled = (m_oBloque.Tipo <> TipoBloque.final)
            
                CargarRolesCumplimentacion
                If m_oRoles.Count > 0 Then
                    sdbcRoles.Value = m_oRoles.Item(1).Id
                Else
                    sdbcRoles.Value = ""
                End If
                CargarGrupos
                CargarCumplimentacionRol
                
            Case 2  'Bloqueos
                sdbgBloqueos.Columns("INICIO").Locked = Not (m_oBloque.Tipo <> TipoBloque.final)
                CargarBloqueos

            Case 3  'Acciones
                Dim bTieneEnlacesEntrantes As Boolean
                
                bTieneEnlacesEntrantes = m_oBloque.ComprobarEnlacesEntrantes
                cmdAnyadirAccion.Enabled = (bTieneEnlacesEntrantes Or m_oBloque.Tipo = TipoBloque.Peticionario) And (m_oBloque.Tipo <> TipoBloque.final) And m_bModifFlujo
                cmdEliminarAccion.Enabled = (bTieneEnlacesEntrantes Or m_oBloque.Tipo = TipoBloque.Peticionario) And (m_oBloque.Tipo <> TipoBloque.final) And m_bModifFlujo
                cmdAnyadirRol.Visible = False
                cmdEliminarRol.Visible = False
                cmdAnyadirAccion.Visible = True
                cmdEliminarAccion.Visible = True
                sdbgAcciones.Enabled = (bTieneEnlacesEntrantes Or m_oBloque.Tipo = TipoBloque.Peticionario) And (m_oBloque.Tipo <> TipoBloque.final)
                CargarAccionesBloque
                
            Case 4  'Listados
                'Deshabilitamos los botones, la combo y el grid si estamos en la etapa final
                cmdModoEdicion.Enabled = (m_oBloque.Tipo <> TipoBloque.final) And m_oRoles.Count > 0
                cmdRestaurar.Enabled = (m_oBloque.Tipo <> TipoBloque.final) And m_oRoles.Count > 0
                sdbcRolesListados.Enabled = (m_oBloque.Tipo <> TipoBloque.final)
                sdbgListados.Enabled = (m_oBloque.Tipo <> TipoBloque.final) And m_oRoles.Count > 0

                CargarRolesListados
                If m_oRoles.Count > 0 Then
                    sdbcRolesListados.Value = m_oRoles.Item(1).Id
                    CargarListadosRol
                Else
                    sdbcRolesListados.Value = ""
                End If
        End Select
    End If
End Sub

Private Sub CargarAccionesBloque()
    Dim oAccion As CAccionBloque

    sdbgAcciones.RemoveAll
    If Not m_oBloque Is Nothing Then
        Set m_oAcciones = oFSGSRaiz.Generar_CAccionesBloque
        m_oAcciones.CargarAcciones basPublic.gParametrosInstalacion.gIdioma, m_oBloque.Id
        For Each oAccion In m_oAcciones
            sdbgAcciones.AddItem oAccion.Id & Chr(m_lSeparador) & oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
        Next
    End If
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: carga los roles del bloque actual
'Llamada desde:CargarDatosTab
'Tiempo ejecucion:0,1seg.
Private Sub CargarRolesBloque()
    Dim oRol As CPMRol
    
    sdbgRoles.RemoveAll
    If Not m_oBloque Is Nothing Then
        Set m_oRoles = oFSGSRaiz.Generar_CPMRoles
        m_oRoles.CargarRolesBloque (m_oBloque.Id)
        For Each oRol In m_oRoles
            sdbgRoles.AddItem oRol.Den & Chr(m_lSeparador) & oRol.PermitirTrasladosBloque & Chr(m_lSeparador) & oRol.Id & Chr(m_lSeparador) & oRol.PermitirTraslados
        Next
    End If
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: carga las acciones del rol seleccionado
'Llamada desde:CargarDatosTab, sdbgRoles_Click
'Tiempo ejecucion:0,1seg.
Private Sub CargarAccionesRolSeleccionado()
    Dim oAccion As CAccionBloque

    sdbgAccionesRol.RemoveAll
    If Not m_oBloque Is Nothing And sdbgRoles.Columns("ID").Value <> "" Then
        Set m_oAcciones = oFSGSRaiz.Generar_CAccionesBloque
        m_oAcciones.CargarAccionesBloqueRol basPublic.gParametrosInstalacion.gIdioma, m_oBloque.Id, sdbgRoles.Columns("ID").Value
        For Each oAccion In m_oAcciones
            sdbgAccionesRol.AddItem oAccion.Id & Chr(m_lSeparador) & oAccion.AsignadoARol & Chr(m_lSeparador) & oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oAccion.Aprobar & Chr(m_lSeparador) & oAccion.Rechazar
        Next
    End If
End Sub

Private Sub CargarRolesCumplimentacion()
    Dim oRol As CPMRol
    
    Screen.MousePointer = vbHourglass
    
    sdbcRoles.RemoveAll
    If Not m_oBloque Is Nothing Then
        Set m_oRoles = oFSGSRaiz.Generar_CPMRoles
        m_oRoles.CargarRolesBloque (m_oBloque.Id)
        For Each oRol In m_oRoles
            sdbcRoles.AddItem oRol.Id & Chr(m_lSeparador) & oRol.Den
        Next
    End If
    
    Screen.MousePointer = vbNormal
    Set oRol = Nothing
End Sub

Private Sub CargarRolesListados()
    Dim oRol As CPMRol
    
    Screen.MousePointer = vbHourglass
    
    sdbcRolesListados.RemoveAll
    If Not m_oBloque Is Nothing Then
        Set m_oRoles = oFSGSRaiz.Generar_CPMRoles
        m_oRoles.CargarRolesBloque (m_oBloque.Id)
        For Each oRol In m_oRoles
            sdbcRolesListados.AddItem oRol.Id & Chr(m_lSeparador) & oRol.Den
        Next
    End If
    
    Screen.MousePointer = vbNormal
    Set oRol = Nothing
End Sub

Private Sub CargarCumplimentacionRol()
    Dim oCumplimentacion As CPMConfCumplimentacion
    sdbgCampos.RemoveAll
    If sdbcRoles.Value <> "" And sdbcRoles.Value <> "0" And ssTabGrupos.selectedItem.Tag <> "" Then
        m_oBloque.CargarCumplimentacion CLng(sdbcRoles.Value), CLng(ssTabGrupos.selectedItem.Tag)
        If Not m_oBloque.Cumplimentaciones Is Nothing Then
            For Each oCumplimentacion In m_oBloque.Cumplimentaciones
                sdbgCampos.AddItem oCumplimentacion.IdCampo & Chr(m_lSeparador) & oCumplimentacion.CodAtrib & Chr(m_lSeparador) & oCumplimentacion.Campo.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCumplimentacion.Campo.TipoPredef & Chr(m_lSeparador) & oCumplimentacion.Campo.CampoGS & Chr(m_lSeparador) & oCumplimentacion.Campo.Tipo & Chr(m_lSeparador) & IIf(oCumplimentacion.Visible, 1, 0) & Chr(m_lSeparador) & IIf(oCumplimentacion.ESCRITURA, 1, 0) & Chr(m_lSeparador) & IIf(oCumplimentacion.EscrituraFormula, "...", "") & Chr(m_lSeparador) & IIf(oCumplimentacion.Obligatorio, 1, 0) & Chr(m_lSeparador) & oCumplimentacion.Intro & Chr(m_lSeparador) & oCumplimentacion.Campo.Vinculado
            Next
        End If
    End If
End Sub

Private Sub CargarListadosRol()
    'Carga todos los listados personalizados
    Dim Ador As Ador.Recordset
    Dim sListaProductos As String
    
    Dim oIdioma As CIdioma
    Dim oIdiomas As CIdiomas
    Dim iNewCol As Integer
    Dim intCols As Integer
    
    On Error GoTo CargarListadosRol_Error

    Screen.MousePointer = vbHourglass
    
    If sdbgListados.Columns.Count = 1 Then
        Set oIdiomas = oGestorParametros.DevolverIdiomas
        
        ReDim arrIDI(0)
        
        DoEvents
        
        For Each oIdioma In oIdiomas

            ReDim Preserve arrIDI(UBound(arrIDI) + 1)
            arrIDI(UBound(arrIDI)) = oIdioma.Cod
            
            iNewCol = sdbgListados.Columns.Count
            sdbgListados.Columns.Add iNewCol
            sdbgListados.Columns(iNewCol).Name = "ARCHIVO_RPT_" & oIdioma.Cod
            sdbgListados.Columns(iNewCol).caption = msArchivoRpt & " (" & oIdioma.Den & ")"
            sdbgListados.Columns(iNewCol).DataType = 8 'Text
            sdbgListados.Columns(iNewCol).FieldLen = 100
            sdbgListados.Columns(iNewCol).Visible = True
            sdbgListados.Columns(iNewCol).TagVariant = oIdioma.Den
            sdbgListados.Columns(iNewCol).FieldLen = 100
            
            iNewCol = sdbgListados.Columns.Count
            sdbgListados.Columns.Add iNewCol
            sdbgListados.Columns(iNewCol).Name = "DEN_" & oIdioma.Cod
            sdbgListados.Columns(iNewCol).caption = msDenominacion & " (" & oIdioma.Den & ")"
            sdbgListados.Columns(iNewCol).DataType = 8 'Text
            sdbgListados.Columns(iNewCol).FieldLen = 100
            sdbgListados.Columns(iNewCol).Visible = True
            sdbgListados.Columns(iNewCol).TagVariant = oIdioma.Den
            sdbgListados.Columns(iNewCol).FieldLen = 50
        Next
        
        Set oIdioma = Nothing
        Set oIdiomas = Nothing
    End If
    
    sdbgListados.RemoveAll
    
    If m_oBloque.Tipo <> TipoBloque.final Then

        Set Ador = oGestorListadosPers.DevolverListadosRolBloque(CLng(sdbcRolesListados.Value), m_oBloque.Id)
        
        If Not Ador Is Nothing Then
            Ador.MoveFirst
            
            While Not Ador.EOF
                sListaProductos = Ador(0)
                For intCols = 1 To Ador.Fields.Count - 1
                    sListaProductos = sListaProductos & Chr(m_lSeparador) & Ador(intCols)
                Next
        
                sdbgListados.AddItem sListaProductos
                
                Ador.MoveNext
            Wend
            
            Ador.Close
            Set Ador = Nothing
        End If
    End If
    
    Screen.MousePointer = vbDefault

    On Error GoTo 0
    Exit Sub

CargarListadosRol_Error:

    MsgBox "Error " & err.Number & " (" & err.Description & ") en el procedimiento CargarListadosRol", vbOKOnly
End Sub

Private Sub CargarGrupos()
Dim oFormulario As CFormulario
Dim iTab As Integer
Dim oGrupo As CFormGrupo

    
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = lIdFormulario
    oFormulario.CargarTodosLosGrupos
    
    ssTabGrupos.Tabs.clear
    iTab = 1
    
    For Each oGrupo In oFormulario.Grupos
        ssTabGrupos.Tabs.Add iTab, "A" & oGrupo.Id, NullToStr(oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        ssTabGrupos.Tabs(iTab).Tag = CStr(oGrupo.Id)
    
        iTab = iTab + 1
    Next
    
    Set oGrupo = Nothing
    Set oFormulario = Nothing
    
End Sub

Private Sub CargarBloqueos()
    Dim oEnlace As CEnlace
    Dim oBloqueOrigen As CBloque
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    
    Dim bInicio As Boolean
    Dim bFin As Boolean
    
    m_bCargandoDatos = True
    
    optBloqueo(0).Value = True
    sdbgBloqueos.Enabled = False
    
    sdbgBloqueos.RemoveAll
    If Not m_oBloque Is Nothing Then
        Set m_oEnlaces = oFSGSRaiz.Generar_CEnlaces
        m_oEnlaces.CargarEnlacesEntrantesABloque m_oBloque.Id
        If m_oEnlaces.Count > 0 Then
            For Each oEnlace In m_oEnlaces
                Set oBloqueOrigen = oFSGSRaiz.Generar_CBloque
                oBloqueOrigen.Id = oEnlace.BloqueOrigen
                Set oIBaseDatos = oBloqueOrigen
                teserror = oIBaseDatos.IniciarEdicion
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oBloqueOrigen = Nothing
                    Set oEnlace = Nothing
                    m_bCargandoDatos = False
                    Exit Sub
                End If
                bInicio = (oEnlace.Bloquea = TipoBloqueoPMEnlace.inicio)
                bFin = (oEnlace.Bloquea = TipoBloqueoPMEnlace.fin)
                optBloqueo(1).Value = optBloqueo(1).Value Or (oEnlace.Bloquea <> TipoBloqueoPMEnlace.NoBloquea)
                sdbgBloqueos.Enabled = sdbgBloqueos.Enabled Or (oEnlace.Bloquea <> TipoBloqueoPMEnlace.NoBloquea)
                sdbgBloqueos.AddItem oEnlace.Id & Chr(m_lSeparador) & oBloqueOrigen.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & bInicio & Chr(m_lSeparador) & bFin
            Next
            optBloqueo(0).Enabled = True
            optBloqueo(1).Enabled = True
        Else
            optBloqueo(0).Enabled = False
            optBloqueo(1).Enabled = False
        End If
        Set oIBaseDatos = Nothing
        Set oBloqueOrigen = Nothing
        Set oEnlace = Nothing
        
        m_bCargandoDatos = False
    End If
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: redimensiona los controles del formulario
'Llamada desde: al modificar el tama�o del formulario
'Tiempo ejecucion:0,1seg.
Private Sub Form_Resize()
    If Me.Width < 2000 Or Me.Height < 4275 Then Exit Sub
    'Anchos:
    sdbcEtapas.Width = Me.Width - 420
    
    sstabConf.Width = Me.Width - 300
    
    cmdEliminarRol.Left = Me.Width - 870
    cmdAnyadirRol.Left = Me.Width - 1370
       
    sdbgRoles.Width = Me.Width - 540
    lblRolSeleccionado.Width = Me.Width - 540
    sdbgAccionesRol.Width = Me.Width - 540
    sdbgAccionesRol.Columns("DEN").Width = sdbgAccionesRol.Width - sdbgAccionesRol.Columns("ASIGNADO").Width - sdbgAccionesRol.Columns("DETALLE").Width - sdbgAccionesRol.Columns("APROBAR").Width - sdbgAccionesRol.Columns("RECHAZAR").Width - 340
    
    sdbcRoles.Width = Me.Width - 540
    sdbcRolesListados.Width = Me.Width - 540
    ssTabGrupos.Width = Me.Width - 540
    sdbgCampos.Width = Me.Width - 1215
    If iTipoSolicitud = TipoSolicitud.SolicitudDePedidoCatalogo Or iTipoSolicitud = TipoSolicitud.SolicitudDePedidoContraPedidoAbierto Then
        sdbgCampos.Columns("DATO").Width = sdbgCampos.Width - sdbgCampos.Columns("ATRIBUTO").Width - sdbgCampos.Columns("VISIBLE").Width - sdbgCampos.Columns("ESCRITURA").Width - 580
    Else
        sdbgCampos.Columns("DATO").Width = sdbgCampos.Width - sdbgCampos.Columns("ATRIBUTO").Width - sdbgCampos.Columns("VISIBLE").Width - sdbgCampos.Columns("ESCRITURA").Width - sdbgCampos.Columns("BLOQUEO").Width - sdbgCampos.Columns("OBLIGATORIO").Width - 580
    End If
    
    sdbgBloqueos.Width = Me.Width - 540
    sdbgBloqueos.Columns("DEN_ETAPA_ORIGEN").Width = sdbgBloqueos.Width - sdbgBloqueos.Columns("INICIO").Width - sdbgBloqueos.Columns("SALIDA").Width - 340
    
    cmdEliminarAccion.Left = Me.Width - 870
    cmdAnyadirAccion.Left = Me.Width - 1370
    sdbgAcciones.Width = Me.Width - 540
    sdbgAcciones.Columns("DEN").Width = sdbgAcciones.Width - sdbgAcciones.Columns("DETALLE").Width - 340
    
    cmdModoEdicion.Left = Me.Width - 1500
    cmdEliminar.Top = Me.Width - 1500
    cmdDeshacer.Top = Me.Width - 1500
    cmdRestaurar.Top = Me.Width - 1500
    cmdA�adir.Top = Me.Width - 1500
    
    sdbgListados.Width = Me.Width - 600
    
    'Altos:
    sstabConf.Height = Me.Height - 1085
    
    sdbgAccionesRol.Height = Me.Height - 4275
    
    ssTabGrupos.Height = Me.Height - 2355
    sdbgCampos.Height = Me.Height - 2880
    
    sdbgBloqueos.Height = Me.Height - 2355
    
    sdbgAcciones.Height = Me.Height - 1995
    
    sdbgListados.Height = Me.Height - 3000
    
    cmdModoEdicion.Top = Me.Height - 1600
    cmdEliminar.Top = Me.Height - 1600
    cmdDeshacer.Top = Me.Height - 1600
    cmdRestaurar.Top = Me.Height - 1600
    cmdA�adir.Top = Me.Height - 1600
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If actualizarYSalir Then
        Cancel = True
        Exit Sub
    End If
    m_bCambiaOrden = False
End Sub

Private Function actualizarYSalir() As Boolean
Dim oIBaseDatos As IBaseDatos
Dim teserror As TipoErrorSummit
' Evita el bug de la grid de Infragistics
' moviendonos a una fila adyacente y
' regresando luego a la actual en caso de
' que no haya error. (jf)
If m_bCambiosEtapaInt Then
    Set oIBaseDatos = m_oBloque
    teserror = oIBaseDatos.FinalizarEdicionModificando
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        actualizarYSalir = True
        Set oIBaseDatos = Nothing
        Set m_oBloque = Nothing
        Exit Function
    End If
    m_bCambiosEtapaInt = False
    frmFlujos.HayCambios
End If
'Si han modificado el orden de los campos lo guardo
If m_bCambiaOrden Then
    If Not GrabarOrdenTodos Then
        actualizarYSalir = True
        Exit Function
    End If
End If

If sdbgListados.DataChanged Then
    bValError = False
    bAnyaError = False
    bModError = False
    If sdbgListados.Row = 0 Then
        sdbgListados.MoveNext
        DoEvents
        If bValError Or bAnyaError Or bModError Then
            actualizarYSalir = True
            Exit Function
        Else
            sdbgListados.MovePrevious
        End If
    Else
        sdbgListados.MovePrevious
        DoEvents
        If bValError Or bAnyaError Or bModError Then
            actualizarYSalir = True
            Exit Function
        Else
            sdbgListados.MoveNext
        End If
    End If
Else
    actualizarYSalir = False
End If
End Function

Private Sub optBloqueo_Click(Index As Integer)
    Dim oEnlace As CEnlace
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit

    If Not m_bCargandoDatos Then
        If optBloqueo(0).Value And Not m_oEnlaces Is Nothing Then
            If oMensajes.PreguntaEliminarBloqueosPMEnlaces = vbYes Then
                '''Recorrer todos los enlaces y quitarles el bloqueo
                For Each oEnlace In m_oEnlaces
                    If oEnlace.Bloquea <> TipoBloqueoPMEnlace.NoBloquea Then
                        oEnlace.Bloquea = TipoBloqueoPMEnlace.NoBloquea
                        Set oIBaseDatos = oEnlace
                        teserror = oIBaseDatos.FinalizarEdicionModificando
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Set oIBaseDatos = Nothing
                            Set oEnlace = Nothing
                            CargarDatosTab
                            Exit Sub
                        End If
                    End If
                Next
                Set oIBaseDatos = Nothing
                Set oEnlace = Nothing
                CargarDatosTab
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            Else
                optBloqueo(1).Value = True
            End If
        Else
            sdbgBloqueos.Enabled = True
        End If
    End If
End Sub

Private Sub sdbcEtapas_CloseUp()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    
    If sdbcEtapas.Value = "" Or sdbcEtapas.Value = "0" Then
        Set m_oBloque = Nothing
    Else
        Set m_oBloque = oFSGSRaiz.Generar_CBloque
        m_oBloque.Id = CLng(sdbcEtapas.Value)
        Set oIBaseDatos = m_oBloque
        teserror = oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oIBaseDatos = Nothing
            Set m_oBloque = Nothing
            Exit Sub
        End If
        chkEtapaInt.Value = BooleanToSQLBinary(m_oBloque.EtapaInt)
        Set oIBaseDatos = Nothing
    End If
    CargarDatosTab
End Sub

Private Sub sdbcEtapas_DropDown()
    Dim oBloques As CBloques
    Dim oBloque As CBloque
            
    If m_bCambiaOrden Then
        If Not GrabarOrdenTodos Then Exit Sub
        m_bCambiaOrden = False
    End If
            
    Screen.MousePointer = vbHourglass

    Set oBloques = oFSGSRaiz.Generar_CBloques
    oBloques.CargarBloques basPublic.gParametrosInstalacion.gIdioma, lIdFlujo
        
    sdbcEtapas.RemoveAll
    For Each oBloque In oBloques
        sdbcEtapas.AddItem oBloque.Id & Chr(m_lSeparador) & oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
    Next
    Set oBloque = Nothing
    Set oBloques = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEtapas_InitColumnProps()
    sdbcEtapas.DataFieldList = "Column 0"
    sdbcEtapas.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcEtapas_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEtapas.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEtapas.Rows - 1
            bm = sdbcEtapas.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEtapas.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcEtapas.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcRoles_CloseUp()

    If sdbcRoles.Value <> "" And sdbcRoles.Value <> "0" Then
        CargarCumplimentacionRol
    End If
End Sub

Private Sub sdbcRoles_DropDown()
    If m_bCambiaOrden Then
        If Not GrabarOrdenTodos Then Exit Sub
        m_bCambiaOrden = False
    End If
    CargarRolesCumplimentacion
End Sub

Private Sub sdbcRoles_InitColumnProps()
    sdbcRoles.DataFieldList = "Column 0"
    sdbcRoles.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcRoles_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcRoles.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcRoles.Rows - 1
            bm = sdbcRoles.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcRoles.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcRoles.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcRolesListados_CloseUp()
    If sdbcRolesListados.Value <> "" And sdbcRolesListados.Value <> "0" Then
        If bModoEdicion Then cmdModoEdicion_Click
        CargarListadosRol
    End If
End Sub

Private Sub sdbcRolesListados_DropDown()
    If m_bCambiaOrden Then
        If Not GrabarOrdenTodos Then Exit Sub
        m_bCambiaOrden = False
    End If

    CargarRolesListados
End Sub

Private Sub sdbcRolesListados_InitColumnProps()
    sdbcRolesListados.DataFieldList = "Column 0"
    sdbcRolesListados.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcRolesListados_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcRolesListados.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcRolesListados.Rows - 1
            bm = sdbcRolesListados.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcRolesListados.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcRolesListados.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgAcciones_BtnClick()
    If sdbgAcciones.col < 0 Then Exit Sub
    
    Select Case sdbgAcciones.Columns(sdbgAcciones.col).Name
        Case "DETALLE"
            If sdbgAcciones.Columns("ID").Value <> "" And sdbgAcciones.Columns("ID").Value <> "0" Then
                frmFlujosDetalleAccion.m_bModifFlujo = m_bModifFlujo
                frmFlujosDetalleAccion.lIdFlujo = lIdFlujo
                frmFlujosDetalleAccion.lIdFormulario = lIdFormulario
                frmFlujosDetalleAccion.lIdAccion = CLng(sdbgAcciones.Columns("ID").Value)
                frmFlujosDetalleAccion.iTipoSolicitud = iTipoSolicitud
                frmFlujosDetalleAccion.Show vbModal
                Unload frmFlujosDetalleAccion
                
                CargarDatosTab
            End If
    End Select
End Sub

Private Sub sdbgAcciones_InitColumnProps()
    sdbgAcciones.Columns("DEN").caption = msAcciones
    sdbgAcciones.Columns("DETALLE").caption = msDetalle
End Sub


Private Sub sdbgAccionesRol_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: comprobaciones que realiza antes de actualizar las acciones de un rol
'Llamada desde: al cambiar sdbgAccionesRol
'Tiempo ejecucion:0,1seg.
Private Sub sdbgAccionesRol_BeforeUpdate(Cancel As Integer)
    Dim oAccion As CAccionBloque
    Dim lIdRol As Long
    Dim teserror As TipoErrorSummit
    Dim bRecargarAcciones As Boolean
    Dim vbm As Variant
    Dim i As Integer
    
    If sdbgRoles.Columns("ID").Value <> "" And sdbgAccionesRol.Columns("ID").Value <> "" And sdbgAccionesRol.Columns("ID").Value <> "0" Then
        Set oAccion = m_oAcciones.Item(CStr(sdbgAccionesRol.Columns("ID").Value))
        If Not oAccion Is Nothing Then
            lIdRol = sdbgRoles.Columns("ID").Value
            If Not oAccion.AsignadoARol And CBool(sdbgAccionesRol.Columns("ASIGNADO").Value) Then
                oAccion.Aprobar = False
                oAccion.Rechazar = False
                If CBool(sdbgAccionesRol.Columns("APROBAR").Value) Then
                    For i = 1 = 0 To sdbgAccionesRol.Rows - 1
                        If i <> sdbgAccionesRol.Row Then
                            vbm = sdbgAccionesRol.RowBookmark(i)
                            If CBool(sdbgAccionesRol.Columns("ASIGNADO").CellValue(vbm)) And CBool(sdbgAccionesRol.Columns("APROBAR").CellValue(vbm)) Then
                                sdbgAccionesRol.Columns("APROBAR").Value = False
                                Exit For
                            End If
                        End If
                    Next
                End If
                If CBool(sdbgAccionesRol.Columns("RECHAZAR").Value) Then
                    For i = 0 To sdbgAccionesRol.Rows - 1
                        If i <> sdbgAccionesRol.Row Then
                            vbm = sdbgAccionesRol.RowBookmark(i)
                            If CBool(sdbgAccionesRol.Columns("ASIGNADO").CellValue(vbm)) And CBool(sdbgAccionesRol.Columns("RECHAZAR").CellValue(vbm)) Then
                                sdbgAccionesRol.Columns("RECHAZAR").Value = False
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
            bRecargarAcciones = oAccion.AsignadoARol And (Not oAccion.Aprobar And CBool(sdbgAccionesRol.Columns("APROBAR").Value) Or Not oAccion.Rechazar And CBool(sdbgAccionesRol.Columns("RECHAZAR").Value))
            oAccion.Aprobar = CBool(sdbgAccionesRol.Columns("APROBAR").Value)
            oAccion.Rechazar = CBool(sdbgAccionesRol.Columns("RECHAZAR").Value)
            
            If CBool(sdbgAccionesRol.Columns("ASIGNADO").Value) Then
                teserror = oAccion.AsignarARol(lIdRol)
                sdbgAccionesRol.Columns("APROBAR").CellStyleSet ("Normal")
                sdbgAccionesRol.Columns("RECHAZAR").CellStyleSet ("Normal")
            Else
                teserror = oAccion.DesAsignarARol(lIdRol)
                sdbgAccionesRol.Columns("APROBAR").Value = oAccion.Aprobar
                sdbgAccionesRol.Columns("RECHAZAR").Value = oAccion.Rechazar
                sdbgAccionesRol.Columns("APROBAR").CellStyleSet ("Gris")
                sdbgAccionesRol.Columns("RECHAZAR").CellStyleSet ("Gris")
            End If
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                sdbgAccionesRol.CancelUpdate
                Set oAccion = Nothing
                m_bErrorAccionesRol = True
                Exit Sub
            Else
                If CBool(sdbgAccionesRol.Columns("ASIGNADO").Value) And Not oAccion.AsignadoARol Then
                    oAccion.AsignadoARol = True
                    basSeguridad.RegistrarAccion AccionesSummit.ACCBloqueAsigAccionRol, "Bloque=" & m_oBloque.Id & ",Accion:" & oAccion.Id & ",Rol:" & lIdRol
                    bRecargarAcciones = bRecargarAcciones Or oAccion.Aprobar Or oAccion.Rechazar
                ElseIf Not CBool(sdbgAccionesRol.Columns("ASIGNADO").Value) And oAccion.AsignadoARol Then
                    oAccion.AsignadoARol = False
                    basSeguridad.RegistrarAccion AccionesSummit.ACCBloqueDesAsigAccionRol, "Bloque=" & m_oBloque.Id & ",Accion:" & oAccion.Id & ",Rol:" & lIdRol
                End If
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            End If
            m_bErrorAccionesRol = False
            If bRecargarAcciones Then
                CargarAccionesRolSeleccionado
                If oAccion.Aprobar Or oAccion.Rechazar Then
                    ' Comprobar si hay otras acciones con el mismo nombre
                    teserror = oAccion.AccionesSimilares(lIdFlujo, basPublic.gParametrosInstalacion.gIdioma, lIdRol)
                    If teserror.NumError < 0 Then
                        If oMensajes.PreguntaActAccionesMismoNombre(oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den, oAccion.Aprobar) = vbYes Then
                            teserror = oAccion.ActualizarAccionesSimilares(lIdFlujo, basPublic.gParametrosInstalacion.gIdioma, lIdRol)
                            If teserror.NumError < 0 Then
                                oMensajes.AvisoAccionesNoVinculadas (oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den)
                            ElseIf teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                            End If
                        End If
                    ElseIf teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                    End If
                End If
            End If
            Set oAccion = Nothing
        End If
    End If
End Sub

Private Sub sdbgAccionesRol_BtnClick()
    If sdbgAccionesRol.col < 0 Then Exit Sub
    
    Select Case sdbgAccionesRol.Columns(sdbgAccionesRol.col).Name
        Case "DETALLE"
            If sdbgAccionesRol.Columns("ID").Value <> "" And sdbgAccionesRol.Columns("ID").Value <> "0" Then
                If sdbgAccionesRol.DataChanged Then
                    sdbgAccionesRol.Update
                    If m_bErrorAccionesRol Then Exit Sub
                End If
            
                frmFlujosDetalleAccion.m_bModifFlujo = m_bModifFlujo
                frmFlujosDetalleAccion.lIdFlujo = lIdFlujo
                frmFlujosDetalleAccion.lIdFormulario = lIdFormulario
                frmFlujosDetalleAccion.lIdAccion = CLng(sdbgAccionesRol.Columns("ID").Value)
                frmFlujosDetalleAccion.iTipoSolicitud = iTipoSolicitud
                frmFlujosDetalleAccion.Show vbModal
                Unload frmFlujosDetalleAccion
                
                CargarDatosTab
            End If
    End Select
End Sub

Private Sub sdbgAccionesRol_Change()
    Dim oAccion As CAccionBloque
    Dim b As Boolean
    If sdbgAccionesRol.DataChanged Then
        Set oAccion = m_oAcciones.Item(CStr(sdbgAccionesRol.Columns("ID").Value))
        If Not sdbgAccionesRol.Columns("ASIGNADO").Value Then
            sdbgAccionesRol.Columns("APROBAR").Value = oAccion.Aprobar
            sdbgAccionesRol.Columns("RECHAZAR").Value = oAccion.Rechazar
        ElseIf CBool(sdbgAccionesRol.Columns("APROBAR").Value) And Not oAccion.Aprobar Then
            sdbgAccionesRol.Columns("RECHAZAR").Value = False
        ElseIf CBool(sdbgAccionesRol.Columns("RECHAZAR").Value) And Not oAccion.Rechazar Then
            sdbgAccionesRol.Columns("APROBAR").Value = False
        End If
        'Solo si hay cambios reales se grabar�
        If sdbgAccionesRol.Columns("APROBAR").Value <> oAccion.Aprobar Or _
            sdbgAccionesRol.Columns("RECHAZAR").Value <> oAccion.Rechazar Or _
            sdbgAccionesRol.Columns("ASIGNADO").Value <> oAccion.AsignadoARol Then
            sdbgAccionesRol.Update
        Else
            sdbgAccionesRol.DataChanged = False
        End If
    End If
End Sub

Private Sub sdbgAccionesRol_InitColumnProps()
    sdbgAccionesRol.Columns("ASIGNADO").caption = msAsignado
    sdbgAccionesRol.Columns("DEN").caption = msAcciones
    sdbgAccionesRol.Columns("DETALLE").caption = msDetalle
    sdbgAccionesRol.Columns("APROBAR").caption = msAprobar
    sdbgAccionesRol.Columns("RECHAZAR").caption = msRechazar
End Sub

Private Sub sdbgAccionesRol_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgAccionesRol.DataChanged Then
                sdbgAccionesRol.Update
                If m_bErrorAccionesRol Then Exit Sub
            End If
    End Select
End Sub

Private Sub sdbgAccionesRol_LostFocus()
    If sdbgAccionesRol.DataChanged Then sdbgAccionesRol.Update
End Sub

Private Sub sdbgAccionesRol_RowLoaded(ByVal Bookmark As Variant)
    If CBool(sdbgAccionesRol.Columns("ASIGNADO").Value) Then
        sdbgAccionesRol.Columns("APROBAR").CellStyleSet ("Normal")
        sdbgAccionesRol.Columns("RECHAZAR").CellStyleSet ("Normal")
    Else
        sdbgAccionesRol.Columns("APROBAR").CellStyleSet ("Gris")
        sdbgAccionesRol.Columns("RECHAZAR").CellStyleSet ("Gris")
    End If
End Sub

Private Sub sdbgBloqueos_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
End Sub

Private Sub sdbgBloqueos_BeforeUpdate(Cancel As Integer)
    Dim oEnlace As CEnlace
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    
    If sdbgBloqueos.Columns("ID_ENLACE").Value <> "" And sdbgBloqueos.Columns("ID_ENLACE").Value <> "0" Then
        Set oEnlace = m_oEnlaces.Item(CStr(sdbgBloqueos.Columns("ID_ENLACE").Value))
        If Not oEnlace Is Nothing Then
            If CBool(sdbgBloqueos.Columns("INICIO").Value) Then
                oEnlace.Bloquea = TipoBloqueoPMEnlace.inicio
            ElseIf CBool(sdbgBloqueos.Columns("SALIDA").Value) Then
                oEnlace.Bloquea = TipoBloqueoPMEnlace.fin
            Else
                oEnlace.Bloquea = TipoBloqueoPMEnlace.NoBloquea
            End If
            Set oIBaseDatos = oEnlace
            teserror = oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                sdbgBloqueos.CancelUpdate
                If Me.Visible Then sdbgBloqueos.SetFocus
                Set oEnlace = Nothing
                Set oIBaseDatos = Nothing
                m_bErrorBloqueos = True
                Exit Sub
            End If
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
            Set oEnlace = Nothing
            Set oIBaseDatos = Nothing
            m_bErrorBloqueos = False
        End If
    End If
End Sub

Private Sub sdbgBloqueos_Change()
    If sdbgBloqueos.col < 0 Then Exit Sub
    
    Select Case sdbgBloqueos.Columns(sdbgBloqueos.col).Name
        Case "INICIO"
            If CBool(sdbgBloqueos.Columns("INICIO").Value) Then
                sdbgBloqueos.Columns("SALIDA").Value = False
            End If
        Case "SALIDA"
            If CBool(sdbgBloqueos.Columns("SALIDA").Value) Then
                sdbgBloqueos.Columns("INICIO").Value = False
            End If
    End Select
    
    If sdbgBloqueos.DataChanged Then sdbgBloqueos.Update
End Sub

Private Sub sdbgBloqueos_InitColumnProps()
    sdbgBloqueos.Columns("DEN_ETAPA_ORIGEN").caption = msEtapas
    sdbgBloqueos.Columns("INICIO").caption = msBloqIni
    sdbgBloqueos.Columns("SALIDA").caption = msBloqSal
End Sub

Private Sub sdbgBloqueos_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgBloqueos.DataChanged Then
                sdbgBloqueos.Update
                If m_bErrorBloqueos Then Exit Sub
            End If
    End Select
End Sub

Private Sub sdbgBloqueos_LostFocus()
    If sdbgBloqueos.DataChanged Then
        sdbgBloqueos.Update
    End If
End Sub

Private Sub sdbgCampos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim oCumplimentacion As CPMConfCumplimentacion
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit

    If sdbgCampos.Columns("ID").Value <> "" And sdbgCampos.Columns("ID").Value <> "0" Then
        Set oCumplimentacion = m_oBloque.Cumplimentaciones.Item(CStr(sdbgCampos.Columns("ID").Value))
        If Not oCumplimentacion Is Nothing Then
            If oCumplimentacion.Visible = CBool(sdbgCampos.Columns("VISIBLE").Value) And _
                oCumplimentacion.ESCRITURA = CBool(sdbgCampos.Columns("ESCRITURA").Value) And _
                oCumplimentacion.Obligatorio = CBool(sdbgCampos.Columns("OBLIGATORIO").Value) Then
                'No hay cambios com lo cual no grabamos nada
                Exit Sub
            End If
        
            oCumplimentacion.Visible = CBool(sdbgCampos.Columns("VISIBLE").Value)
            oCumplimentacion.ESCRITURA = CBool(sdbgCampos.Columns("ESCRITURA").Value)
            oCumplimentacion.Obligatorio = CBool(sdbgCampos.Columns("OBLIGATORIO").Value)
            Set oIBaseDatos = oCumplimentacion
            teserror = oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                sdbgCampos.CancelUpdate
                If Me.Visible Then sdbgCampos.SetFocus
                Set oCumplimentacion = Nothing
                Set oIBaseDatos = Nothing
                m_bErrorCumplimentaciones = True
                Exit Sub
            End If
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
            Set oCumplimentacion = Nothing
            Set oIBaseDatos = Nothing
            m_bErrorCumplimentaciones = False
            'Si hay solicitudes favoritas almacenadas para usuarios del rol peticionario, y se cambia la cumplimentacion de escritura a solo lectura
            'se eliminar�n los valores almacenados.
            If m_oBloque.Tipo = TipoBloque.Peticionario And Not (CBool(sdbgCampos.Columns("ESCRITURA").Value)) Then
                EliminarValorSolicitudFavorita sdbgCampos.Columns("ID").Value
            End If
        End If
    End If
End Sub


Private Sub sdbgCampos_BtnClick()
    If sdbgCampos.col < 0 Then Exit Sub
    Select Case sdbgCampos.Columns(sdbgCampos.col).Name
        Case "DATO"
            If Not m_oBloque Is Nothing And _
                sdbcRoles.Value > 0 And _
                ssTabGrupos.selectedItem.Tag <> "" And ssTabGrupos.selectedItem.Tag <> "0" And _
                sdbgCampos.Columns("ID").Value > 0 Then
                    frmFlujosCumplDesglose.m_bModifFlujo = m_bModifFlujo
                    frmFlujosCumplDesglose.mlBloque = m_oBloque.Id
                    frmFlujosCumplDesglose.msBloque = m_oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
                    frmFlujosCumplDesglose.mlRol = sdbcRoles.Value
                    frmFlujosCumplDesglose.msRol = sdbcRoles.Columns("DEN").Value
                    frmFlujosCumplDesglose.m_udtTipoBloque = m_oBloque.Tipo
                    frmFlujosCumplDesglose.mlGrupo = CLng(ssTabGrupos.selectedItem.Tag)
                    frmFlujosCumplDesglose.msGrupo = ssTabGrupos.selectedItem.caption
                    frmFlujosCumplDesglose.mlCampoPadre = sdbgCampos.Columns("ID").Value
                    frmFlujosCumplDesglose.msCampoPadre = sdbgCampos.Columns("DATO").Value
                    frmFlujosCumplDesglose.mlFormulario = lIdFormulario
                    frmFlujosCumplDesglose.mbVinculado = sdbgCampos.Columns("VINCULADO").Value
                    frmFlujosCumplDesglose.Show vbModal
                    Unload frmFlujosCumplDesglose
            End If
        Case "ATRIBUTO"
            frmFlujosCumplValorLista.mlBloque = m_oBloque.Id
            frmFlujosCumplValorLista.mlRol = sdbcRoles.Value
            frmFlujosCumplValorLista.mlCampo = sdbgCampos.Columns("ID").Value
            frmFlujosCumplValorLista.mlSubTipo = sdbgCampos.Columns("SUBTIPO").Value
            frmFlujosCumplValorLista.Show vbModal
            
        Case "BLOQUEO"
            If sdbgCampos.Columns("ESCRITURA").Value = True Then
                frmFlujosCondicionBloqueo.m_lBloque = m_oBloque.Id
                frmFlujosCondicionBloqueo.m_lRol = sdbcRoles.Value
                frmFlujosCondicionBloqueo.m_lCampo = sdbgCampos.Columns("ID").Value
                frmFlujosCondicionBloqueo.m_lIdFormulario = lIdFormulario
                frmFlujosCondicionBloqueo.Show vbModal
                CargarCumplimentacionRol
            End If
    End Select
End Sub

Private Sub sdbgCampos_Change()

    If sdbgCampos.col < 0 Then Exit Sub
                   
    With sdbgCampos
    Select Case .Columns(.col).Name
        Case "VISIBLE"
            If Not .Columns("VISIBLE").Value Then
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado
                If .Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                    .Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Or .Columns("CAMPO_GS").Value = TipoCampoGS.DesgloseDePedido Then
                        .Columns("VISIBLE").Value = True
                        .DataChanged = False
                        Exit Sub
                End If
                
                If m_oBloque.Tipo = TipoBloque.Peticionario And .Columns("CAMPO_GS").Value = TipoCampoGS.Moneda Then
                    .Columns("VISIBLE").Value = True
                    .DataChanged = False
                    Exit Sub
                End If
                                
                .Columns("ESCRITURA").Value = False
                .Columns("OBLIGATORIO").Value = False
            Else
                If m_oBloque.Tipo = TipoBloque.Peticionario And .Columns("CAMPO_GS").Value = TipoCampoGS.Moneda Then
                    .Columns("ESCRITURA").Value = True
                    .Columns("OBLIGATORIO").Value = True
                End If
            End If
            
        Case "ESCRITURA"
            If .Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitarioAdj Or _
                .Columns("CAMPO_GS").Value = TipoCampoSC.ProveedorAdj Or _
                .Columns("CAMPO_GS").Value = TipoCampoSC.CantidadAdj Or _
                .Columns("CAMPO_GS").Value = TipoCampoGS.NumSolicitERP Or _
                .Columns("CAMPO_GS").Value = TipoCampoGS.ImporteSolicitudesVinculadas Or _
                .Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                    .Columns("ESCRITURA").Value = False
                    .DataChanged = False
                    Exit Sub
            End If
            
            If .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                .Columns("ESCRITURA").Value = False
                .DataChanged = False
                Exit Sub
            End If
            
            If Not .Columns("ESCRITURA").Value Then
                If .Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                   .Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        .Columns("ESCRITURA").Value = True
                        .DataChanged = False
                        Exit Sub
                End If
                
                'Tarea 2918 : El check de obligatorio no se podr� modificar, el campo moneda siempre tiene que ser obligatorio para el ROL Peticionario que es que da de alta la solicitud.
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Moneda Then
                    If m_oBloque.Tipo = TipoBloque.Peticionario Then
                        .Columns("ESCRITURA").Value = True
                        .DataChanged = False
                        Exit Sub
                    Else
                        .Columns("OBLIGATORIO").Value = False
                    End If
                End If
            Else
                If Not .Columns("VISIBLE").Value Then
                    .Columns("ESCRITURA").Value = False
                    .DataChanged = False
                    Exit Sub
                End If
                
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Moneda Then
                    .Columns("OBLIGATORIO").Value = True
                End If
            End If
            
        Case "OBLIGATORIO"
            'Tarea 2918 : El check de obligatorio no se podr� modificar, el campo moneda siempre tiene que ser obligatorio para el ROL Peticionario que es que da de alta la solicitud.
                
            If Not .Columns("OBLIGATORIO").Value Then
                'If m_oBloque.Tipo = TipoBloque.Peticionario Then
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.Moneda Then
                        'si est� marcado como escritura no podemos desmarcar el obligatorio
                        .DataChanged = False
                        .Columns("OBLIGATORIO").Value = True
                        Exit Sub
                    End If
                'End If
            End If
            
            If .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Or _
                .Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                .Columns("OBLIGATORIO").Value = False
                .DataChanged = False
                Exit Sub
            End If
            
            If .Columns("CAMPO_GS").Value = TipoCampoGS.ImporteSolicitudesVinculadas Then
                .Columns("OBLIGATORIO").Value = False
                .DataChanged = False
                Exit Sub
            End If
            
            If Not .Columns("OBLIGATORIO").Value Then
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If .Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                   .Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        .Columns("OBLIGATORIO").Value = True
                        .DataChanged = False
                        Exit Sub
                End If
            Else
                If Not .Columns("VISIBLE").Value Or Not .Columns("ESCRITURA").Value Then
                    .Columns("OBLIGATORIO").Value = False
                    .DataChanged = False
                    Exit Sub
                End If
            End If
    End Select
    
    If .DataChanged Then .Update
    End With
End Sub

Private Sub sdbgCampos_InitColumnProps()
    With sdbgCampos
        .Columns("DATO").caption = msDato
        .Columns("VISIBLE").caption = msVisible
        .Columns("ESCRITURA").caption = msEscritura
        .Columns("OBLIGATORIO").caption = msObligatorio
    End With
End Sub

Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgCampos.DataChanged Then
                sdbgCampos.Update
                If m_bErrorCumplimentaciones Then
                    Exit Sub
                End If
            End If
    End Select
End Sub

Private Sub sdbgCampos_LostFocus()
    If sdbgCampos.DataChanged Then
        sdbgCampos.Update
    End If
End Sub

''' <summary>
''' Evento que salta al cambiar la posicion de fila de la grid.
''' Formatea la celda segun el tipo de dato
''' </summary>
''' <param name="LastRow">Ultima posicion de fila</param>
''' <param name="LastCol">Ultima posicion de columna</param>
''' <remarks>Tiempo m�ximo:0seg.</remarks>
Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    With sdbgCampos
        If (.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose) And (.Columns("CAMPO_GS").Value <> TipoCampoGS.DesgloseActividad) Then
            .Columns("DATO").Style = ssStyleEditButton
        Else
            .Columns("DATO").Style = ssStyleEdit
        End If
        If .Columns("TIPO").Value = TipoCampoPredefinido.Atributo And .Columns("INTRO").Value Then
            .Columns("ATRIBUTO").Style = ssStyleEditButton
        Else
            .Columns("ATRIBUTO").Style = ssStyleEdit
        End If
    End With
End Sub

Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)
    If sdbgCampos.Columns("VISIBLE").CellValue(Bookmark) = False Then
        sdbgCampos.Columns("DATO").CellStyleSet "Gris"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgCampos.Columns("DATO").CellStyleSet "Amarillo"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        sdbgCampos.Columns("DATO").CellStyleSet "Calculado"
    Else
        sdbgCampos.Columns("DATO").CellStyleSet ""
    End If
End Sub

Private Sub sdbgListados_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    If Me.Visible Then sdbgListados.SetFocus
    sdbgListados.Bookmark = sdbgListados.RowBookmark(sdbgListados.Row)
End Sub

Private Sub sdbgListados_AfterInsert(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
    
    bAnyadir = False
End Sub

Private Sub sdbgListados_AfterUpdate(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
End Sub

Private Sub sdbgListados_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit
    
    DispPromptMsg = 0
    
    If sdbgListados.IsAddRow Then
        Cancel = True
        Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminarListPers(sdbgListados.Columns(0).Value)
    
    If irespuesta = vbNo Then
        Cancel = True
    Else
        ''' Eliminamos de la base de datos
        teserror = oGestorListadosPers.EliminarListadoRol(CLng(sdbcRolesListados.Value), CLng(m_oBloque.Id), CLng(sdbgListados.Columns(0).Value))
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            sdbgListados.Refresh
            Cancel = True
            If Me.Visible Then sdbgListados.SetFocus
        End If
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
End Sub

Private Sub sdbgListados_BeforeInsert(Cancel As Integer)
    If Not bModoEdicion Then Exit Sub
    
    bAnyadir = True
    bAnyaError = False
End Sub

Private Sub sdbgListados_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
    Dim v As Variant
    Dim intCol As Integer
    Dim arrARCHIV() As String
    Dim arrDEN() As String
    Dim intLISPER As Long
    
    ''' * Objetivo: Validar los datos y si todo es correcto guardar los cambios en BD
    
    bValError = False
    Cancel = False
    
    ReDim arrARCHIV(0)
    ReDim arrDEN(0)
    
    For intCol = 0 To sdbgListados.Columns.Count - 1
        Select Case Left(sdbgListados.Columns(intCol).Name, 4)

        Case "ARCH"
           
            'Comprueba que se rellene el fichero .rpt
            If Trim(sdbgListados.Columns(intCol).Value) <> "" Then
            
                'Comprueba que el fichero introducido tenga extensi�n .rpt
                If Len(sdbgListados.Columns(intCol).Value) < 4 Then
                    oMensajes.ArchivoRpt
                    Cancel = True
                    GoTo Salir
                ElseIf Mid(LCase(sdbgListados.Columns(intCol).Value), Len(sdbgListados.Columns(intCol).Value) - 3) <> ".rpt" Then 'Que tenga extensi�n .rpt
                    oMensajes.ArchivoRpt
                    Cancel = True
                    GoTo Salir
                End If
            'Archivo_rpt vac�o
            Else
                'Denominaci�n informada
                If Trim(sdbgListados.Columns(intCol + 1).Value) <> "" Then
                    oMensajes.NoValido msArchivoRpt & " (" & sdbgListados.Columns(intCol).TagVariant & ")"
                    Cancel = True
                    GoTo Salir
                End If
            
            End If
            
            ReDim Preserve arrARCHIV(UBound(arrARCHIV) + 1)
            arrARCHIV(UBound(arrARCHIV)) = sdbgListados.Columns(intCol).Value
            
        Case "DEN_"
             'Comprueba que se rellene la denominaci�n del informe
            If Trim(sdbgListados.Columns(intCol).Value) = "" Then
            'Si la denominaci�n est� vac�a
                'Miramos si el archivo_rpt est� informado
                 If Trim(sdbgListados.Columns(intCol - 1).Value) <> "" Then
                    oMensajes.NoValida msDenominacion & " (" & sdbgListados.Columns(intCol).TagVariant & ")"
                    Cancel = True
                    GoTo Salir
                 End If
            End If
            
            ReDim Preserve arrDEN(UBound(arrDEN) + 1)
            arrDEN(UBound(arrDEN)) = sdbgListados.Columns(intCol).Value
            
        End Select
    Next
    
    If bAnyadir And sdbgListados.IsAddRow Then   'Inserci�n
        bAnyaError = False
        ''' Anyadir a la base de datos
        intLISPER = oGestorListadosPers.DevolverIdListado(CLng(sdbcRolesListados.Value), CLng(m_oBloque.Id))
        teserror = oGestorListadosPers.A�adirListadoRol(CLng(sdbcRolesListados.Value), CLng(m_oBloque.Id), intLISPER, arrIDI, arrARCHIV, arrDEN)
        If teserror.NumError <> TESnoerror Then
            v = sdbgListados.ActiveCell.Value
            TratarError teserror
            If Me.Visible Then sdbgListados.SetFocus
            bAnyaError = True
            sdbgListados.ActiveCell.Value = v
            Exit Sub
        Else
            bAnyadir = False
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    Else  'Modificaci�n
        bModError = False
        ''' Modificamos en la base de datos
        teserror = oGestorListadosPers.ModificarListadoRol(CLng(sdbcRolesListados.Value), CLng(m_oBloque.Id), CLng(sdbgListados.Columns(0).Value), arrIDI, arrARCHIV, arrDEN)
        If teserror.NumError <> TESnoerror Then
            v = sdbgListados.ActiveCell.Value
            TratarError teserror
            If Me.Visible Then sdbgListados.SetFocus
            bModError = True
            sdbgListados.ActiveCell.Value = v
            Exit Sub
        End If
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
    
Salir:
        
    bValError = Cancel
    If Me.Visible Then sdbgListados.SetFocus
End Sub


Private Sub sdbgListados_Change()
    If cmdDeshacer.Enabled = False Then
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    End If
End Sub

Private Sub sdbgListados_HeadClick(ByVal ColIndex As Integer)
    'Carga todos los listados personalizados ordenados seg�n la columna
    Dim Ador As Ador.Recordset
    Dim sListaProductos As String
    Dim intCol As Integer
    
    sdbgListados.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set Ador = oGestorListadosPers.DevolverListadosRolBloque(CLng(sdbcRolesListados.Value), m_oBloque.Id, ColIndex)
    
    If Not Ador Is Nothing Then
        Ador.MoveFirst
        
        While Not Ador.EOF
            sListaProductos = Ador(0)
            For intCol = 1 To Ador.Fields.Count - 1
                sListaProductos = sListaProductos & Chr(m_lSeparador) & Ador(intCol)
            Next
            sdbgListados.AddItem sListaProductos
            Ador.MoveNext
        Wend
        
        Ador.Close
        Set Ador = Nothing
    End If

    Screen.MousePointer = vbDefault
End Sub

Private Sub sdbgListados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgListados.IsAddRow Then
        If sdbgListados.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
    Else
        If sdbgListados.DataChanged = True Then cmdDeshacer.Enabled = True
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgListados.Row) Then
                sdbgListados.col = 1
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
    End If
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: al cambiar el check de permitir traslados de la lista de roles en un bloque, comprueba si permite traslados ese rol
'Llamada desde: al cambiar sdbgRoles
'Tiempo ejecucion:0,1seg.
Private Sub sdbgRoles_Change()
    Dim teserror As TipoErrorSummit
    
    If sdbgRoles.col < 0 Then Exit Sub
                    
    Select Case sdbgRoles.Columns(sdbgRoles.col).Name
        Case "PERMITIR_TRASLADO_BLOQUE"
            If Not CBool(sdbgRoles.Columns("PERMITIR_TRASLADO_ROL").Value) Then
                sdbgRoles.Columns("PERMITIR_TRASLADO_BLOQUE").Value = False
                Exit Sub
            End If
    End Select
    If Not m_oBloque Is Nothing And sdbgRoles.Columns("ID").Value <> "" Then
        teserror = m_oBloque.ModificarRol(sdbgRoles.Columns("ID").Value, sdbgRoles.Columns("PERMITIR_TRASLADO_BLOQUE").Value)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCBloqueModif, "Bloque=" & m_oBloque.Id & ",Rol:" & sdbgRoles.Columns("ID").Value
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
End Sub

Private Sub sdbgRoles_Click()
    lblRolSeleccionado.caption = sdbgRoles.Columns("ROL").Value
    CargarAccionesRolSeleccionado
End Sub

Private Sub sdbgRoles_InitColumnProps()
    sdbgRoles.Columns("ROL").caption = msRol
    sdbgRoles.Columns("PERMITIR_TRASLADO_BLOQUE").caption = msPermitirTraslados
End Sub

Private Sub sstabConf_Click(PreviousTab As Integer)
    cmdAnyadirAccion.Visible = False
    cmdEliminarAccion.Visible = False
    cmdAnyadirRol.Visible = False
    cmdEliminarRol.Visible = False
        
    If PreviousTab = 1 And m_bCambiaOrden Then
        If Not GrabarOrdenTodos Then Exit Sub
        m_bCambiaOrden = False
    End If
        
    If bModoEdicion Then cmdModoEdicion_Click
        
    CargarDatosTab
End Sub

Private Sub SSTabGrupos_Click()
    If sdbgCampos.DataChanged Then
        sdbgCampos.Update
        If m_bErrorCumplimentaciones Then Exit Sub
    End If
    'Si han modificado el orden de los campos lo guardo
    If m_bCambiaOrden Then
        If Not GrabarOrdenTodos Then Exit Sub
        m_bCambiaOrden = False
    End If
    
    CargarCumplimentacionRol
End Sub

Sub EliminarValorSolicitudFavorita(ByVal IdCampo As Long)
    Dim i As Integer
    Dim sPer, sUON1, sUON2, sUON3 As String
    
    For i = 1 To m_oRoles.Count
        If m_oRoles.Item(i).Id = Me.sdbcRoles.Value Then
            sPer = m_oRoles.Item(i).Per
            sUON1 = m_oRoles.Item(i).UON1
            sUON2 = m_oRoles.Item(i).UON2
            sUON3 = m_oRoles.Item(i).UON3
            Exit For
        End If
    Next
    
    Dim oSolicitud As CSolicitud
    Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
    oSolicitud.EliminarValorSolicitudFavorita 0, IdCampo, sPer, sUON1, sUON2, sUON3
End Sub
