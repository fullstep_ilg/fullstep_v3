VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELMATMultiple 
   BackColor       =   &H00808000&
   Caption         =   "DSelección de material"
   ClientHeight    =   5325
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8205
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELMATMultiple.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5325
   ScaleWidth      =   8205
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   4770
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8070
      _ExtentX        =   14235
      _ExtentY        =   8414
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   4065
      TabIndex        =   2
      Top             =   4920
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2880
      TabIndex        =   1
      Top             =   4920
      Width           =   1005
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":0CB2
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":1106
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":1218
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":132A
            Key             =   "Raiz2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":177C
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":182C
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":18EE
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":199E
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":1A60
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":1B10
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":1BD2
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATMultiple.frx":1C9A
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSELMATMultiple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public sOrigen As String
Public m_bModif As Boolean
Public SeleccionSimple As Boolean   'Indica que no se seleccionará todos los nodos hijos del nodo que se seleccione
Public RestMatComp As Boolean

Public oGruposMN1Seleccionados As CGruposMatNivel1
Public oGruposMN2Seleccionados As CGruposMatNivel2
Public oGruposMN3Seleccionados As CGruposMatNivel3
Public oGruposMN4Seleccionados As CGruposMatNivel4

'Variable para no ejecutar el node check si se ha disparado el mousedown
Private bMouseDown As Boolean
Private SelNode As MSComctlLib.node

Private sIdioma() As String

Private Sub cmdAceptar_Click()
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
    Dim nod4 As MSComctlLib.node
    Dim nodx As MSComctlLib.node
        
    Screen.MousePointer = vbHourglass

    LockWindowUpdate Me.hWnd
    
    Set oGruposMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
    
    Set nodx = tvwEstrMat.Nodes(1)
    If nodx.Children = 0 Then
         LockWindowUpdate 0&
         Screen.MousePointer = vbNormal
         Exit Sub
    End If
       
    Set nod1 = nodx.Child
    While Not nod1 Is Nothing
        If SeleccionSimple Then
            If nod1.Checked Then oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
                            
            Set nod2 = nod1.Child
            While Not nod2 Is Nothing
                If nod2.Checked Then oGruposMN2Seleccionados.Add DevolverCod(nod1), "", DevolverCod(nod2), ""
                
                Set nod3 = nod2.Child
                While Not nod3 Is Nothing
                    If nod3.Checked Then oGruposMN3Seleccionados.Add DevolverCod(nod1), DevolverCod(nod2), DevolverCod(nod3), "", "", ""
                    
                    Set nod4 = nod3.Child
                    While Not nod4 Is Nothing
                        If nod4.Checked Then oGruposMN4Seleccionados.Add DevolverCod(nod1), DevolverCod(nod2), DevolverCod(nod3), "", "", "", DevolverCod(nod4), ""
                        Set nod4 = nod4.Next
                    Wend
                    
                    Set nod3 = nod3.Next
                Wend
                
                Set nod2 = nod2.Next
            Wend
        Else
            If nod1.Checked Then
                oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
            Else
                Set nod2 = nod1.Child
                While Not nod2 Is Nothing
                    If nod2.Checked Then
                        oGruposMN2Seleccionados.Add DevolverCod(nod1), "", DevolverCod(nod2), ""
                    Else
                        Set nod3 = nod2.Child
                        While Not nod3 Is Nothing
                            If nod3.Checked Then
                                oGruposMN3Seleccionados.Add DevolverCod(nod1), DevolverCod(nod2), DevolverCod(nod3), "", "", ""
                            Else
                                Set nod4 = nod3.Child
                                While Not nod4 Is Nothing
                                    If nod4.Checked Then
                                        oGruposMN4Seleccionados.Add DevolverCod(nod1), DevolverCod(nod2), DevolverCod(nod3), "", "", "", DevolverCod(nod4), ""
                                    End If
                                    Set nod4 = nod4.Next
                                Wend
                            End If
                            Set nod3 = nod3.Next
                        Wend
                    End If
                    Set nod2 = nod2.Next
                Wend
            End If
        End If
        
        Set nod1 = nod1.Next
    Wend
    
    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal
    
    Select Case sOrigen
        Case "frmFlujosRoles", "frmEST"
            Me.Hide 'Lo descargo en el origen
            Exit Sub
    End Select
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
        
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    ConfigurarSeguridad
    GenerarEstructuraMatAsignable
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELMAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 8)
        For i = 1 To 7
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        Me.caption = sIdioma(3)
        cmdAceptar.caption = Ador(0).Value '8
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.MoveNext
        sIdioma(8) = Ador(0).Value
         
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub ConfigurarSeguridad()
    If Not m_bModif Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        Me.Height = Me.Height - cmdAceptar.Height
    End If
End Sub

''' <summary>Genera la estructura de materiales</summary>
''' <remarks>Llmada desde: Form_Load</remarks>
''' <revision>LTG 28/05/2013</revision>

Private Sub GenerarEstructuraMatAsignable()
    Dim oGrupsMN1 As CGruposMatNivel1
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim nodx As MSComctlLib.node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    On Error GoTo Error

    tvwEstrMat.ImageList = Me.ImageList1

    tvwEstrMat.Nodes.clear

    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(3), "Raiz")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    If RestMatComp Then
        oGrupsMN1.GenerarEstructuraMateriales False, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oGrupsMN1.GenerarEstructuraMateriales False
    End If

    
    Select Case gParametrosGenerales.giNEM
    
    Case 1
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
'                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den)
                nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            Next
            
    Case 2
        
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
'            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den)
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
'                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den)
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
            Next
        Next
            
            
    Case 3
        
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
'            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den)
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
'                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den)
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
                For Each oGMN3 In oGMN2.GruposMatNivel3
                    scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                    Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
'                    Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den)
                    nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                Next
            Next
        Next
        
        
    Case 4
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
'            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den)
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
                        
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
'                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den)
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
                    
                    For Each oGMN3 In oGMN2.GruposMatNivel3
                        
                        scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
'                        Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den)
                        nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                                
                                For Each oGMN4 In oGMN3.GruposMatNivel4
                                    scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                    Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4")
'                                    Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den)
                                    nodx.Tag = "GMN4" & CStr(oGMN4.Cod)
                                Next
                    Next
            Next
        Next
        
        
    End Select
        
    Set oGrupsMN1 = Nothing
    
    If sOrigen = "frmFlujosRoles" Or sOrigen = "frmEST" Then
        If Not oGruposMN1Seleccionados Is Nothing Then
            For Each oGMN1 In oGruposMN1Seleccionados
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                If Not tvwEstrMat.Nodes("GMN1" & scod1) Is Nothing Then
                    Set nodx = tvwEstrMat.Nodes("GMN1" & scod1)
                    nodx.Checked = True
                    If Not SeleccionSimple Then MarcarTodosLosHijos nodx
                End If
            Next
        End If
        If Not oGruposMN2Seleccionados Is Nothing Then
            For Each oGMN2 In oGruposMN2Seleccionados
                scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                If Not tvwEstrMat.Nodes("GMN2" & scod1 & scod2) Is Nothing Then
                    Set nodx = tvwEstrMat.Nodes("GMN2" & scod1 & scod2)
                    nodx.Checked = True
                    tvwEstrMat.Nodes("GMN1" & scod1).Expanded = True
                    If Not SeleccionSimple Then MarcarTodosLosHijos nodx
                End If
            Next
        End If
        If Not oGruposMN3Seleccionados Is Nothing Then
            For Each oGMN3 In oGruposMN3Seleccionados
                scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
                scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                If Not tvwEstrMat.Nodes("GMN3" & scod1 & scod2 & scod3) Is Nothing Then
                    Set nodx = tvwEstrMat.Nodes("GMN3" & scod1 & scod2 & scod3)
                    nodx.Checked = True
                    tvwEstrMat.Nodes("GMN1" & scod1).Expanded = True
                    tvwEstrMat.Nodes("GMN2" & scod1 & scod2).Expanded = True
                    If Not SeleccionSimple Then MarcarTodosLosHijos nodx
                End If
            Next
        End If
        If Not oGruposMN4Seleccionados Is Nothing Then
            For Each oGMN4 In oGruposMN4Seleccionados
                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                If Not tvwEstrMat.Nodes("GMN4" & scod1 & scod2 & scod3 & scod4) Is Nothing Then
                    Set nodx = tvwEstrMat.Nodes("GMN4" & scod1 & scod2 & scod3 & scod4)
                    nodx.Checked = True
                    tvwEstrMat.Nodes("GMN1" & scod1).Expanded = True
                    tvwEstrMat.Nodes("GMN2" & scod1 & scod2).Expanded = True
                    tvwEstrMat.Nodes("GMN3" & scod1 & scod2 & scod3).Expanded = True
                    'MarcarTodosLosHijos nodx
                End If
            Next
        End If
    End If


Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
    
End Sub

Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.node)

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = True
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = True
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = True
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub
Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.node)

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = False
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = False
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = False
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = False
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = False
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
    DoEvents
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
    DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
'    Select Case Left(Node.Tag, 4)
'        Case "GMN1"
'            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
'        Case "GMN2"
'            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
'        Case "GMN3"
'            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
'       Case "GMN4"
'            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
'    End Select
End Function


Private Sub Form_Resize()
    If Me.Height - 960 > 0 Then
        If m_bModif Then
            tvwEstrMat.Height = Me.Height - 960
        Else
            tvwEstrMat.Height = Me.Height - 645
        End If
    End If
    If Me.Width - 250 > 0 Then
        tvwEstrMat.Width = Me.Width - 250
    End If

    If Me.Height - 810 > 0 Then
        cmdAceptar.Top = Me.Height - 810
        cmdCancelar.Top = Me.Height - 810
    End If
    cmdAceptar.Left = Me.Width * 0.3459
    cmdCancelar.Left = cmdAceptar.Left + 1185
End Sub

Private Sub Form_Unload(Cancel As Integer)
    sOrigen = ""

    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing

    Set SelNode = Nothing
End Sub

Private Sub tvwEstrMat_KeyUp(KeyCode As Integer, Shift As Integer)
   
    If bMouseDown Then
        
        bMouseDown = False
        
        If SelNode.Tag = "Raiz" Then
            SelNode.Checked = Not SelNode.Checked
            DoEvents
            Exit Sub
        End If
        
        If SelNode.Checked Then
            If Not SeleccionSimple Then MarcarTodosLosHijos SelNode
        Else
            If Not SeleccionSimple Then
                QuitarMarcaTodosSusPadres SelNode
                QuitarMarcaTodosLosHijos SelNode
            End If
        End If
    End If
End Sub

Private Sub tvwEstrMat_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim nod As MSComctlLib.node
    Dim tvhti As TVHITTESTINFO
                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMat.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMat.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiará al finalizar este procedimiento
                             
                bMouseDown = True
                Set SelNode = nod
                             
                End If
            End If
        End If
    End If
End Sub

Private Sub tvwEstrMat_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    tvwEstrMat_KeyUp 0, Shift
End Sub

Private Sub tvwEstrMat_NodeCheck(ByVal node As MSComctlLib.node)
    If bMouseDown Then
        Exit Sub
    End If
    
    bMouseDown = True
    Set SelNode = node
End Sub
