VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSolicitudAprobar 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DAprobar solicitud"
   ClientHeight    =   5535
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5715
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudAprobar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5535
   ScaleWidth      =   5715
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   2790
      TabIndex        =   6
      Top             =   5160
      Width           =   1095
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Height          =   315
      Left            =   1470
      TabIndex        =   5
      Top             =   5160
      Width           =   1095
   End
   Begin VB.TextBox txtComentarioPet 
      Height          =   1575
      Left            =   120
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   1440
      Width           =   5535
   End
   Begin VB.TextBox txtComentarioComp 
      BackColor       =   &H00C8D0D4&
      Enabled         =   0   'False
      Height          =   1575
      Left            =   120
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   3480
      Width           =   5535
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcComprador 
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Top             =   600
      Width           =   3015
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5318
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcEquipo 
      Height          =   285
      Left            =   1440
      TabIndex        =   0
      Top             =   120
      Width           =   3015
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5318
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblComprador 
      BackColor       =   &H00808000&
      Caption         =   "DComprador:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label lblComentPet 
      BackColor       =   &H00808000&
      Caption         =   "DIntroduzca un comentario para el peticionario si lo desea:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   1080
      Width           =   4815
   End
   Begin VB.Label lblComentCom 
      BackColor       =   &H00808000&
      Caption         =   "DIntroduzca un comentario para el comprador si lo desea:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   3120
      Width           =   4815
   End
   Begin VB.Label lblEquipo 
      BackColor       =   &H00808000&
      Caption         =   "DEquipo:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmSolicitudAprobar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_oSolicitud As CInstancia
Public g_sOrigen As String
Public g_sOrigen2 As String

'Variables privadas
Private m_bRespetarCombo As Boolean
Private m_oEqpSeleccionado As CEquipo
Private m_oEquipos As CEquipos

Private m_oCompSeleccionado As CComprador
Private m_oComps As CCompradores
Private m_bEnviarComprador As Boolean

Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String


''' <summary>
''' Aprobar
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
Dim arMensaje As Variant
Dim arMensaje2 As Variant
Dim oEmails As CEmailSolicitudes
Dim teserror As TipoErrorSummit
Dim vComp As Variant
Dim vMotivoComp As Variant
Dim iRes As Integer
Dim bSesionIniciada As Boolean
Dim oCompradores As CCompradores

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arMensaje = ""
    arMensaje2 = ""
    
    'Validaci�n de datos
    If sdbcEquipo.Text = "" Then
        oMensajes.FaltaComprador
        If Me.Visible Then sdbcEquipo.SetFocus
        Exit Sub
    End If
    
    If sdbcComprador.Text = "" Then
        oMensajes.FaltaComprador
        If Me.Visible Then sdbcComprador.SetFocus
        Exit Sub
    End If
    
    
    'Aprueba la solicitud que est� seleccionada en la grid de solicitudes
    If m_bEnviarComprador = True Then
        If m_oCompSeleccionado Is Nothing Then Exit Sub
        vComp = m_oCompSeleccionado.Cod
        vMotivoComp = txtComentarioComp.Text
    Else
        vComp = Null
        vMotivoComp = Null
    End If
    
    Screen.MousePointer = vbHourglass
    cmdAceptar.Enabled = False
    
    'Env�o de email al peticionario
    Set oEmails = oFSGSRaiz.Generar_CEmailSolicitudes
    arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 1)
    'Comprueba la ruta si hay que enviar mail al comprador
    If m_bEnviarComprador = True Then
        arMensaje2 = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 0)
    End If
    
    If arMensaje(0) = "" Then
    ElseIf arMensaje(2) = "1010" Then 'No encontr� la ruta o plantilla
        iRes = oMensajes.PreguntaRutaSolicitudesNoEncontrada
        If iRes = vbNo Then
            Set oEmails = Nothing
            Screen.MousePointer = vbNormal
            cmdAceptar.Enabled = True
            Unload Me
            Exit Sub
        Else
            arMensaje(2) = ""
        End If
    Else
        'Solo si no le ha salido el mensaje comprueba el segundo mensaje
        If m_bEnviarComprador = True Then
            If arMensaje2(0) = "" Then
            ElseIf arMensaje2(2) = "1010" Then 'No encontr� la ruta o plantilla
                iRes = oMensajes.PreguntaRutaSolicitudesNoEncontrada
                If iRes = vbNo Then
                    Set oEmails = Nothing
                    Screen.MousePointer = vbNormal
                    cmdAceptar.Enabled = True
                    Unload Me
                    Exit Sub
                End If
            End If
        End If
    End If
    
    teserror = g_oSolicitud.AprobarSolicitud(basOptimizacion.gCodPersonaUsuario, txtComentarioPet.Text, vComp, vMotivoComp)
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        
    Else
        arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 1)
        If arMensaje(2) = "1010" Then arMensaje(2) = ""
        
        If Not bSesionIniciada Or oIdsMail Is Nothing Then
            Set oIdsMail = IniciarSesionMail
            bSesionIniciada = True
        End If

        teserror = ComponerMensaje(arMensaje(0), arMensaje(1), arMensaje(2), , , arMensaje(4), , , arMensaje(5), _
                        entidadNotificacion:=entidadNotificacion.Solicitud, _
                        tipoNotificacion:=TipoNotificacionEmail.AprobacionSolicitudPet, _
                        lIdInstancia:=g_oSolicitud.Id, _
                        sToName:=g_oSolicitud.Peticionario.nombre & " " & g_oSolicitud.Peticionario.Apellidos)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        End If
        
        'Si se ha seleccionado otro comprador se env�a el mail
        If m_bEnviarComprador = True Then
        
            If (g_oSolicitud.comprador Is Nothing) And (Not IsNull(vComp)) Then
            'Se acaba de establecer, a quien se le envia sera al nuevo.
                Set oCompradores = oFSGSRaiz.generar_CCompradores
                oCompradores.CargarTodosLosCompradores carinicod:=vComp, coincidenciatotal:=True, usarindice:=True
            
                Set g_oSolicitud.comprador = Nothing
                Set g_oSolicitud.comprador = oCompradores.Item("0")
            ElseIf (Not g_oSolicitud.comprador Is Nothing) And (Not IsNull(vComp)) Then
                If g_oSolicitud.comprador.Cod <> vComp Then
                'Ha cambiado, a quien se le envia sera al nuevo.
                    Set oCompradores = oFSGSRaiz.generar_CCompradores
                    oCompradores.CargarTodosLosCompradores carinicod:=vComp, coincidenciatotal:=True, usarindice:=True
            
                    Set g_oSolicitud.comprador = Nothing
                    Set g_oSolicitud.comprador = oCompradores.Item("0")
                End If
            End If
        
            If Not (g_oSolicitud.comprador Is Nothing) Then
                arMensaje2 = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 0)
                If arMensaje2(2) = "1010" Then arMensaje2(2) = ""
                If Not bSesionIniciada Or oIdsMail Is Nothing Then
                    Set oIdsMail = IniciarSesionMail
                    bSesionIniciada = True
                End If
                'doevents
                teserror = ComponerMensaje(arMensaje2(0), arMensaje2(1), arMensaje2(2), , , arMensaje(4), , , arMensaje2(5), _
                                        entidadNotificacion:=entidadNotificacion.Solicitud, _
                                        tipoNotificacion:=AprobacionSolicitudPet, _
                                        lIdInstancia:=g_oSolicitud.Id, _
                                        sToName:=g_oSolicitud.comprador.nombre & " " & g_oSolicitud.comprador.Apel)
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                End If
            End If
        End If
        
        If bSesionIniciada Then
            FinalizarSesionMail
        End If
        Set oEmails = Nothing

        Select Case g_sOrigen2
            Case "frmSolicitudes"  'S�lo si el formulario de llamada es el de solicitudes
                'Actualiza la grid y la colecci�n del formulario de solicitudes
                frmSolicitudes.ActualizarEstadoEnGrid g_oSolicitud.Id, EstadoSolicitud.Aprobada
    
                If m_bEnviarComprador = True Then
                    'Actualiza la colecci�n y la grid de solicitudes con el nuevo comprador asignado:
                    frmSolicitudes.ActualizarCompradorEnGrid g_oSolicitud.Id, m_oCompSeleccionado
                End If
                
                If g_sOrigen = "frmSolicitudDetalle" Then
                    frmSolicitudes.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada, oUsuarioSummit.Persona, Date
                    If m_bEnviarComprador = True Then
                        'Actualiza la colecci�n y la grid de solicitudes con el nuevo comprador asignado:
                        frmSolicitudes.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                    End If
                End If
        
            Case "frmPROCE"
                frmPROCE.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada, oUsuarioSummit.Persona, Date
                If m_bEnviarComprador = True Then
                    'Actualiza la colecci�n y la grid de solicitudes con el nuevo comprador asignado:
                    frmPROCE.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                End If
                
            Case "frmCatalogo"
                frmCatalogo.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada, oUsuarioSummit.Persona, Date
                If m_bEnviarComprador = True Then
                    'Actualiza la colecci�n y la grid de solicitudes con el nuevo comprador asignado:
                    frmCatalogo.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                End If
                
            Case "frmPedidos"
                frmPEDIDOS.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada, oUsuarioSummit.Persona, Date
                If m_bEnviarComprador = True Then
                    'Actualiza la colecci�n y la grid de solicitudes con el nuevo comprador asignado:
                    frmPEDIDOS.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                End If
                
            Case "frmSeguimiento"
                frmSeguimiento.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada, oUsuarioSummit.Persona, Date
                If m_bEnviarComprador = True Then
                    'Actualiza la colecci�n y la grid de solicitudes con el nuevo comprador asignado:
                    frmSeguimiento.g_ofrmDetalleSolic.ActualizarCompradorEnGrid m_oCompSeleccionado
                End If
        End Select
        
        ''' Registro de acciones
        basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAprobar, "Id:" & g_oSolicitud.Id
    End If
    cmdAceptar.Enabled = True
    Screen.MousePointer = vbNormal
    
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not m_bActivado Then
        m_bActivado = True
   End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    m_bActivado = False
    cmdAceptar.Enabled = True
    CargarRecursos
    
    m_bEnviarComprador = False
    
    If g_oSolicitud Is Nothing Then Exit Sub

    'Muestra en las combos el equipo y comprador que se han seleccionado desde el EP:
    If Not g_oSolicitud.comprador Is Nothing Then
        If g_oSolicitud.comprador.codEqp <> "" Then
            'Si la persona asignada desde el EP no es un comprador no muestra nada
            sdbcEquipo.Columns(0).Value = g_oSolicitud.comprador.codEqp
            sdbcEquipo.Text = g_oSolicitud.comprador.codEqp & " - " & NullToStr(g_oSolicitud.comprador.DenEqp)
                
            sdbcComprador.Columns(0).Value = g_oSolicitud.comprador.Cod
            sdbcComprador.Text = g_oSolicitud.comprador.Cod & " - " & NullToStr(g_oSolicitud.comprador.nombre) & " " & NullToStr(g_oSolicitud.comprador.Apel)
        End If
    End If
    
    ConfigurarSeguridad
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLIC_APROBAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value       '1 Aprobar solicitud
        Ador.MoveNext
        lblEquipo.caption = Ador(0).Value
        Ador.MoveNext
        lblComprador.caption = Ador(0).Value
        Ador.MoveNext
        lblComentPet.caption = Ador(0).Value
        Ador.MoveNext
        lblComentCom.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
      oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oEqpSeleccionado = Nothing
    Set m_oEquipos = Nothing
    Set m_oCompSeleccionado = Nothing
    Set m_oComps = Nothing
    Set g_oSolicitud = Nothing
    g_sOrigen = ""
    g_sOrigen2 = ""
    m_bRespetarCombo = False
    m_bEnviarComprador = False
    m_bActivado = False
    m_sMsgError = ""
   
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcComprador_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarCombo Then
        Set m_oCompSeleccionado = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcComprador_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcComprador_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcComprador.DroppedDown Then
        sdbcComprador = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcComprador_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcComprador_CloseUp()
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcComprador.Value = "..." Then
        sdbcComprador.Text = ""
        Exit Sub
    End If
    
    If sdbcComprador.Value = "" Then Exit Sub
    
    sCod = m_oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(m_oEqpSeleccionado.Cod))
    
    Set m_oCompSeleccionado = m_oComps.Item(sCod & sdbcComprador.Columns(0).Text)
        
    'Si se ha seleccionado un comprador distinto al que hab�a se habilita el comentario
    'para el comprador
    If Not g_oSolicitud.comprador Is Nothing Then
        If g_oSolicitud.comprador.Cod <> sdbcComprador.Columns("COD").Value Then
            m_bEnviarComprador = True
            txtComentarioComp.Enabled = True
            txtComentarioComp.BackColor = &H80000005
        End If
    Else
        m_bEnviarComprador = True
        txtComentarioComp.Enabled = True
        txtComentarioComp.BackColor = &H80000005
    End If
    
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcComprador_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub


Private Sub sdbcComprador_DropDown()
    Dim oComp As CComprador
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcComprador.RemoveAll
    
    If m_oEqpSeleccionado Is Nothing Then Exit Sub
    
    Set m_oComps = Nothing
    
    Screen.MousePointer = vbHourglass

    Set m_oComps = oFSGSRaiz.generar_CCompradores
    

    m_oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, False, False
    
    Set m_oComps = m_oEqpSeleccionado.Compradores
    
    For Each oComp In m_oComps
        sdbcComprador.AddItem oComp.Cod & Chr(9) & oComp.Cod & " - " & NullToStr(oComp.nombre) & " " & NullToStr(oComp.Apel)
    Next
    
    sdbcComprador.SelStart = 0
    sdbcComprador.SelLength = Len(sdbcComprador.Text)
    sdbcComprador.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcComprador_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcComprador_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcComprador.DataFieldList = "Column 1"
    sdbcComprador.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcComprador_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcComprador_PositionList(ByVal Text As String)
     ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcComprador.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcComprador.Rows - 1
            bm = sdbcComprador.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcComprador.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcComprador.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbcComprador_Validate(Cancel As Boolean)
    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ''' Solo continuamos si existe el comprador
    If sdbcComprador.Text = "" Then Exit Sub
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    Screen.MousePointer = vbHourglass
   
    m_oEqpSeleccionado.CargarTodosLosCompradores sdbcComprador.Columns(0).Value, , , True, False, False, False
    
    Set oCompradores = m_oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcComprador = ""
    Else
        Set m_oCompSeleccionado = oCompradores.Item(1)
        If Not g_oSolicitud.comprador Is Nothing Then
            If oCompradores.Item(1).Cod <> g_oSolicitud.comprador.Cod Then
                m_bEnviarComprador = True
                txtComentarioComp.Enabled = True
                txtComentarioComp.BackColor = &H80000005
            End If
        Else
            m_bEnviarComprador = True
            txtComentarioComp.Enabled = True
            txtComentarioComp.BackColor = &H80000005
        End If
    End If
    
    Set oCompradores = Nothing

    Screen.MousePointer = vbNormal
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcComprador_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub sdbcEquipo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcComprador = ""
        sdbcComprador.RemoveAll
        m_bRespetarCombo = False
    
        Set m_oEqpSeleccionado = Nothing
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcEquipo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEquipo_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcEquipo.DroppedDown Then
        sdbcEquipo = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcEquipo_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEquipo_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEquipo.Value = "..." Then
        sdbcEquipo.Columns(0).Value = ""
        sdbcEquipo.Columns(1).Value = ""
        Exit Sub
    End If
    
    If sdbcEquipo.Value = "" Then Exit Sub
    
    sdbcComprador = ""
    sdbcComprador.RemoveAll
    
    Screen.MousePointer = vbHourglass

    Set m_oEqpSeleccionado = m_oEquipos.Item(sdbcEquipo.Columns(0).Text)
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcEquipo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub sdbcEquipo_DropDown()
    Dim oeqp As CEquipo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oEquipos = Nothing
    Set m_oEquipos = oFSGSRaiz.Generar_CEquipos
    
    Screen.MousePointer = vbHourglass
    
    sdbcEquipo.RemoveAll
    
    m_oEquipos.CargarTodosLosEquipos , , False, True, False
    
    For Each oeqp In m_oEquipos
        sdbcEquipo.AddItem oeqp.Cod & Chr(9) & oeqp.Cod & " - " & oeqp.Den
    Next
    
    sdbcEquipo.SelStart = 0
    sdbcEquipo.SelLength = Len(sdbcEquipo.Columns(0).Value)
    sdbcEquipo.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcEquipo_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEquipo_InitColumnProps()
    sdbcEquipo.DataFieldList = "Column 1"
    sdbcEquipo.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcEquipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEquipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEquipo.Rows - 1
            bm = sdbcEquipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEquipo.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcEquipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbcEquipo_Validate(Cancel As Boolean)
    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEquipo.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
   
    oEquipos.CargarTodosLosEquipos sdbcEquipo.Columns(0).Value, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEquipo.Text = ""
        sdbcComprador = ""
        sdbcComprador.RemoveAll

    Else
        Set m_oEqpSeleccionado = oEquipos.Item(1)
    End If
    
    Set oEquipos = Nothing
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "sdbcEquipo_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub ConfigurarSeguridad()
    Dim bRestricEqp As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bRestricEqp = False
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        'Asignar comprador
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAsignar)) Is Nothing) Then
            sdbcComprador.Enabled = False
            sdbcEquipo.Enabled = False
            txtComentarioComp.Enabled = False
        End If
        
        'Restricci�n de equipo
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestEquipo)) Is Nothing) Then
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
                bRestricEqp = True
            End If
        End If
    End If
    
    
    Set m_oEquipos = Nothing
    Set m_oEquipos = oFSGSRaiz.Generar_CEquipos
   
    'Si tiene restricciones de equipo
    If bRestricEqp = True Then
        m_oEquipos.CargarTodosLosEquipos g_oSolicitud.comprador.codEqp, , True
        Set m_oEqpSeleccionado = m_oEquipos.Item(1)
        
        'Hace invisible el combo de equipos y redimensiona el resto de la grid
        sdbcEquipo.Visible = False
        lblEquipo.Visible = False
        sdbcComprador.Top = sdbcEquipo.Top
        lblComprador.Top = lblEquipo.Top
        lblComentPet.Top = lblComentPet.Top - sdbcEquipo.Height - sdbcEquipo.Top
        txtComentarioPet.Top = txtComentarioPet.Top - sdbcEquipo.Height - sdbcEquipo.Top
        lblComentCom.Top = lblComentCom.Top - sdbcEquipo.Height - sdbcEquipo.Top
        txtComentarioComp.Top = txtComentarioComp.Top - sdbcEquipo.Height - sdbcEquipo.Top
        cmdAceptar.Top = cmdAceptar.Top - sdbcEquipo.Height - sdbcEquipo.Top
        cmdCancelar.Top = cmdCancelar.Top - sdbcEquipo.Height - sdbcEquipo.Top
        Me.Height = Me.Height - sdbcEquipo.Height - sdbcEquipo.Top
        
    Else
        m_oEquipos.CargarTodosLosEquipos , , False, True, False
        If Not g_oSolicitud.comprador Is Nothing Then
            Set m_oEqpSeleccionado = m_oEquipos.Item(CStr(g_oSolicitud.comprador.codEqp))
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudAprobar", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
