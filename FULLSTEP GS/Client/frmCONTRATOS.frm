VERSION 5.00
Object = "{1E47E980-2496-11D3-ACB1-C0A64FC10000}#1.52#0"; "Pajant.dll"
Object = "{05BFD3F1-6319-4F30-B752-C7A22889BCC4}#1.0#0"; "AcroPDF.dll"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{3D6D5D32-B9F2-101C-AED5-00608CF525A5}#1.5#0"; "Tx4ole.ocx"
Object = "{14ACBB92-9C4A-4C45-AFD2-7AE60E71E5B3}#4.0#0"; "IGSplitter40.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCONTRATOS 
   Appearance      =   0  'Flat
   Caption         =   "Contratos"
   ClientHeight    =   8370
   ClientLeft      =   1740
   ClientTop       =   1845
   ClientWidth     =   11760
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONTRATOS.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8370
   ScaleWidth      =   11760
   Begin SSSplitter.SSSplitter SSSplitter1 
      Height          =   6495
      Left            =   0
      TabIndex        =   19
      Top             =   480
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   11456
      _Version        =   262144
      PaneTree        =   "frmCONTRATOS.frx":014A
      Begin VB.PictureBox picPrincipal 
         BackColor       =   &H00808000&
         Height          =   6435
         Left            =   3375
         ScaleHeight     =   6375
         ScaleWidth      =   8190
         TabIndex        =   21
         Top             =   30
         Width           =   8250
         Begin VB.PictureBox picApartado 
            Height          =   6315
            Index           =   2
            Left            =   30
            ScaleHeight     =   6255
            ScaleWidth      =   8145
            TabIndex        =   55
            Top             =   60
            Visible         =   0   'False
            Width           =   8205
            Begin VB.PictureBox picToolBarDoc 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   345
               Left            =   4740
               ScaleHeight     =   345
               ScaleWidth      =   3405
               TabIndex        =   60
               Top             =   0
               Width           =   3405
               Begin VB.CommandButton cmdMenos 
                  Appearance      =   0  'Flat
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Left            =   30
                  Picture         =   "frmCONTRATOS.frx":019C
                  Style           =   1  'Graphical
                  TabIndex        =   68
                  TabStop         =   0   'False
                  ToolTipText     =   "Zoom -"
                  Top             =   30
                  Width           =   330
               End
               Begin VB.CommandButton cmdMas 
                  Appearance      =   0  'Flat
                  Height          =   300
                  Left            =   1620
                  Picture         =   "frmCONTRATOS.frx":0224
                  Style           =   1  'Graphical
                  TabIndex        =   67
                  TabStop         =   0   'False
                  ToolTipText     =   "Zoom +"
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
               Begin VB.CommandButton cmdMenu 
                  Appearance      =   0  'Flat
                  Height          =   300
                  Left            =   1290
                  Picture         =   "frmCONTRATOS.frx":02AD
                  Style           =   1  'Graphical
                  TabIndex        =   66
                  TabStop         =   0   'False
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
               Begin VB.TextBox txtZoom 
                  Height          =   315
                  Left            =   360
                  TabIndex        =   65
                  Text            =   "Text1"
                  Top             =   30
                  Width           =   915
               End
               Begin VB.CommandButton cmdAbrir 
                  Height          =   300
                  Index           =   0
                  Left            =   2730
                  Picture         =   "frmCONTRATOS.frx":02FB
                  Style           =   1  'Graphical
                  TabIndex        =   64
                  TabStop         =   0   'False
                  ToolTipText     =   "Abrir"
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
               Begin VB.CommandButton cmdSalvar 
                  Height          =   300
                  Index           =   0
                  Left            =   2400
                  Picture         =   "frmCONTRATOS.frx":0377
                  Style           =   1  'Graphical
                  TabIndex        =   63
                  TabStop         =   0   'False
                  ToolTipText     =   "Salvar una copia"
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
               Begin VB.CommandButton cmdPDFCon 
                  Height          =   300
                  Left            =   2070
                  Picture         =   "frmCONTRATOS.frx":03F8
                  Style           =   1  'Graphical
                  TabIndex        =   62
                  ToolTipText     =   "Exportar a PDF"
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
               Begin VB.CommandButton cmdImprimir 
                  Height          =   300
                  Index           =   0
                  Left            =   3060
                  Picture         =   "frmCONTRATOS.frx":0517
                  Style           =   1  'Graphical
                  TabIndex        =   61
                  TabStop         =   0   'False
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
            End
            Begin VB.PictureBox picTextoDoc 
               Height          =   5895
               Left            =   30
               ScaleHeight     =   5835
               ScaleWidth      =   8055
               TabIndex        =   57
               Top             =   360
               Width           =   8115
               Begin VB.Label lblVisualizable 
                  Alignment       =   2  'Center
                  Caption         =   "Los formatos visualizables son: .doc, .txt, .rtf, .pdf "
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   14.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   915
                  Left            =   960
                  TabIndex        =   58
                  Top             =   2220
                  Width           =   6405
               End
               Begin VB.Label lblNoVisualizable 
                  Alignment       =   2  'Center
                  Caption         =   "No es posible visualizar el archivo. "
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   14.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   915
                  Left            =   960
                  TabIndex        =   59
                  Top             =   1740
                  Width           =   6405
               End
            End
            Begin Tx4oleLib.TXTextControl TXTextContrato 
               Height          =   5895
               Left            =   30
               TabIndex        =   56
               Top             =   360
               Width           =   8115
               _Version        =   65541
               _ExtentX        =   14314
               _ExtentY        =   10398
               _StockProps     =   73
               BackColor       =   16777215
               Language        =   34
               BorderStyle     =   1
               BackStyle       =   1
               ControlChars    =   0   'False
               EditMode        =   0
               HideSelection   =   -1  'True
               InsertionMode   =   -1  'True
               MousePointer    =   0
               ZoomFactor      =   100
               ViewMode        =   0
               ClipChildren    =   0   'False
               ClipSiblings    =   -1  'True
               SizeMode        =   0
               TabKey          =   -1  'True
               FormatSelection =   0   'False
               VTSpellDictionary=   "C:\PROGRA~1\THEIMA~1\TXTEXT~1.0\Bin\AMERICAN.VTD"
               ScrollBars      =   0
               PageWidth       =   0
               PageHeight      =   0
               PageMarginL     =   1440
               PageMarginT     =   1440
               PageMarginR     =   1440
               PageMarginB     =   1440
               PrintZoom       =   100
               PrintOffset     =   0   'False
               PrintColors     =   -1  'True
               FontName        =   "Arial"
               FontSize        =   12
               FontBold        =   0   'False
               FontItalic      =   0   'False
               FontStrikethru  =   0   'False
               FontUnderline   =   0   'False
               Baseline        =   0
               TextBkColor     =   16777215
               Alignment       =   0
               LineSpacing     =   100
               LineSpacingT    =   0
               FrameStyle      =   32
               FrameDistance   =   0
               FrameLineWidth  =   20
               IndentL         =   0
               IndentR         =   0
               IndentFL        =   0
               IndentT         =   0
               IndentB         =   0
               Text            =   "TXTextControl1"
               WordWrapMode    =   1
            End
            Begin VB.Label lblContratoDoc 
               Alignment       =   2  'Center
               BackColor       =   &H00C0E0FF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Contrato"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   0
               TabIndex        =   69
               Top             =   0
               Width           =   4725
            End
         End
         Begin VB.PictureBox picApartado 
            Height          =   6315
            Index           =   3
            Left            =   30
            ScaleHeight     =   6255
            ScaleWidth      =   8145
            TabIndex        =   110
            Top             =   60
            Visible         =   0   'False
            Width           =   8205
            Begin AcroPDFLibCtl.AcroPDF AcroPDFControl 
               Height          =   2055
               Left            =   0
               TabIndex        =   119
               Top             =   360
               Width           =   3615
               _cx             =   5080
               _cy             =   5080
            End
            Begin VB.Label lblContratoPdf 
               Alignment       =   2  'Center
               BackColor       =   &H00C0E0FF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "ContratoPdf"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   0
               TabIndex        =   111
               Top             =   0
               Width           =   8085
            End
         End
         Begin VB.PictureBox picApartado 
            Height          =   6315
            Index           =   4
            Left            =   30
            ScaleHeight     =   6255
            ScaleWidth      =   8145
            TabIndex        =   112
            Top             =   60
            Visible         =   0   'False
            Width           =   8205
            Begin PAJANTLibCtl.PajantImageCtrl PajantImagen 
               Height          =   3705
               Left            =   0
               OleObjectBlob   =   "frmCONTRATOS.frx":059C
               TabIndex        =   118
               Top             =   360
               Width           =   3300
            End
            Begin VB.PictureBox picToolBarImg 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   405
               Left            =   7080
               ScaleHeight     =   405
               ScaleWidth      =   1005
               TabIndex        =   113
               Top             =   0
               Width           =   1005
               Begin VB.CommandButton cmdAbrir 
                  Height          =   300
                  Index           =   1
                  Left            =   330
                  Picture         =   "frmCONTRATOS.frx":05C0
                  Style           =   1  'Graphical
                  TabIndex        =   116
                  TabStop         =   0   'False
                  ToolTipText     =   "Abrir"
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
               Begin VB.CommandButton cmdSalvar 
                  Height          =   300
                  Index           =   1
                  Left            =   0
                  Picture         =   "frmCONTRATOS.frx":063C
                  Style           =   1  'Graphical
                  TabIndex        =   115
                  TabStop         =   0   'False
                  ToolTipText     =   "Salvar una copia"
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
               Begin VB.CommandButton cmdImprimir 
                  Height          =   300
                  Index           =   1
                  Left            =   660
                  Picture         =   "frmCONTRATOS.frx":06BD
                  Style           =   1  'Graphical
                  TabIndex        =   114
                  TabStop         =   0   'False
                  Top             =   30
                  UseMaskColor    =   -1  'True
                  Width           =   330
               End
            End
            Begin VB.Label lblContratoImg 
               Alignment       =   2  'Center
               BackColor       =   &H00C0E0FF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Contrato Img"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   0
               TabIndex        =   117
               Top             =   0
               Width           =   7125
            End
         End
         Begin VB.PictureBox picApartado 
            BorderStyle     =   0  'None
            Height          =   6315
            Index           =   1
            Left            =   30
            ScaleHeight     =   6315
            ScaleWidth      =   8205
            TabIndex        =   22
            Top             =   60
            Visible         =   0   'False
            Width           =   8205
            Begin VB.PictureBox picDatos 
               BorderStyle     =   0  'None
               Height          =   4890
               Left            =   330
               ScaleHeight     =   4890
               ScaleWidth      =   7470
               TabIndex        =   23
               Top             =   1020
               Width           =   7470
               Begin VB.TextBox txtObsProve 
                  BackColor       =   &H80000018&
                  Height          =   1005
                  HideSelection   =   0   'False
                  Left            =   3480
                  Locked          =   -1  'True
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   24
                  Top             =   3765
                  Width           =   3765
               End
               Begin SSDataWidgets_B.SSDBGrid sdbgContactos 
                  Height          =   1425
                  Left            =   180
                  TabIndex        =   25
                  Top             =   2250
                  Width           =   7065
                  ScrollBars      =   3
                  _Version        =   196617
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  RecordSelectors =   0   'False
                  GroupHeaders    =   0   'False
                  Col.Count       =   10
                  stylesets.count =   2
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCONTRATOS.frx":0742
                  stylesets(1).Name=   "Tan"
                  stylesets(1).BackColor=   10079487
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCONTRATOS.frx":075E
                  AllowUpdate     =   0   'False
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   0
                  BalloonHelp     =   0   'False
                  MaxSelectedRows =   1
                  HeadStyleSet    =   "Normal"
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorEven   =   -2147483633
                  BackColorOdd    =   -2147483633
                  RowHeight       =   423
                  SplitterPos     =   1
                  SplitterVisible =   -1  'True
                  Columns.Count   =   10
                  Columns(0).Width=   3200
                  Columns(0).Caption=   "Apellidos"
                  Columns(0).Name =   "APE"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16777215
                  Columns(1).Width=   4022
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   50
                  Columns(1).HasBackColor=   -1  'True
                  Columns(1).BackColor=   16777215
                  Columns(2).Width=   3200
                  Columns(2).Caption=   "Departamento"
                  Columns(2).Name =   "DEP"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).HasBackColor=   -1  'True
                  Columns(2).BackColor=   16777215
                  Columns(3).Width=   3200
                  Columns(3).Caption=   "Cargo"
                  Columns(3).Name =   "CAR"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(3).HasBackColor=   -1  'True
                  Columns(3).BackColor=   16777215
                  Columns(4).Width=   2328
                  Columns(4).Caption=   "Tel�fono"
                  Columns(4).Name =   "TFNO"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(4).HasBackColor=   -1  'True
                  Columns(4).BackColor=   16777215
                  Columns(5).Width=   3200
                  Columns(5).Caption=   "Fax"
                  Columns(5).Name =   "FAX"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(5).HasBackColor=   -1  'True
                  Columns(5).BackColor=   16777215
                  Columns(6).Width=   3200
                  Columns(6).Caption=   "Tel�fono m�vil"
                  Columns(6).Name =   "TFNO_MOVIL"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(6).HasBackColor=   -1  'True
                  Columns(6).BackColor=   16777215
                  Columns(7).Width=   3200
                  Columns(7).Caption=   "Mail"
                  Columns(7).Name =   "MAIL"
                  Columns(7).DataField=   "Column 7"
                  Columns(7).DataType=   8
                  Columns(7).FieldLen=   256
                  Columns(7).HasBackColor=   -1  'True
                  Columns(7).BackColor=   16777215
                  Columns(8).Width=   2540
                  Columns(8).Caption=   "Recibe peticiones"
                  Columns(8).Name =   "DEF"
                  Columns(8).DataField=   "Column 8"
                  Columns(8).DataType=   8
                  Columns(8).FieldLen=   256
                  Columns(8).Style=   2
                  Columns(8).HasBackColor=   -1  'True
                  Columns(8).BackColor=   16777215
                  Columns(9).Width=   3200
                  Columns(9).Visible=   0   'False
                  Columns(9).Caption=   "PORT"
                  Columns(9).Name =   "PORT"
                  Columns(9).DataField=   "Column 9"
                  Columns(9).DataType=   11
                  Columns(9).FieldLen=   256
                  _ExtentX        =   12462
                  _ExtentY        =   2514
                  _StockProps     =   79
                  Caption         =   "Contactos"
                  BackColor       =   14737632
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin VB.Label Label13 
                  Caption         =   "Moneda:"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Left            =   180
                  TabIndex        =   51
                  Top             =   1110
                  Width           =   915
               End
               Begin VB.Label Label12 
                  Caption         =   "Direcci�n:"
                  ForeColor       =   &H00000000&
                  Height          =   210
                  Left            =   180
                  TabIndex        =   50
                  Top             =   30
                  Width           =   885
               End
               Begin VB.Label Label10 
                  Caption         =   "C�digo postal:"
                  ForeColor       =   &H00000000&
                  Height          =   225
                  Left            =   180
                  TabIndex        =   49
                  Top             =   390
                  Width           =   1065
               End
               Begin VB.Label Label9 
                  Caption         =   "Poblaci�n:"
                  ForeColor       =   &H00000000&
                  Height          =   225
                  Left            =   2400
                  TabIndex        =   48
                  Top             =   360
                  Width           =   705
               End
               Begin VB.Label Label4 
                  Caption         =   "Pa�s:"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Left            =   180
                  TabIndex        =   47
                  Top             =   750
                  Width           =   1020
               End
               Begin VB.Label Label3 
                  Caption         =   "Provincia:"
                  ForeColor       =   &H00000000&
                  Height          =   225
                  Left            =   180
                  TabIndex        =   46
                  Top             =   1470
                  Width           =   960
               End
               Begin VB.Label lblcalif1 
                  Caption         =   "Calificaci�n 1"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Left            =   180
                  TabIndex        =   45
                  Top             =   3840
                  Width           =   1290
               End
               Begin VB.Label lblCal1Den 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1500
                  TabIndex        =   44
                  Top             =   3765
                  Width           =   915
               End
               Begin VB.Label lblcal2den 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1500
                  TabIndex        =   43
                  Top             =   4125
                  Width           =   915
               End
               Begin VB.Label lblcal3den 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1500
                  TabIndex        =   42
                  Top             =   4485
                  Width           =   915
               End
               Begin VB.Label lblCalif2 
                  Caption         =   "Calificaci�n 2"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Left            =   180
                  TabIndex        =   41
                  Top             =   4185
                  Width           =   1290
               End
               Begin VB.Label lblcalif3 
                  Caption         =   "Calificaci�n 3"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Left            =   180
                  TabIndex        =   40
                  Top             =   4515
                  Width           =   1290
               End
               Begin VB.Label lblCp 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1260
                  TabIndex        =   39
                  Top             =   330
                  Width           =   1020
               End
               Begin VB.Label lblMonDen 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   2340
                  TabIndex        =   38
                  Top             =   1050
                  Width           =   4815
               End
               Begin VB.Label lblPaiDen 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   2340
                  TabIndex        =   37
                  Top             =   690
                  Width           =   4815
               End
               Begin VB.Label lblProviDen 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   2340
                  TabIndex        =   36
                  Top             =   1410
                  Width           =   4815
               End
               Begin VB.Label lblPaiCod 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1260
                  TabIndex        =   35
                  Top             =   690
                  Width           =   1020
               End
               Begin VB.Label lblMonCod 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1260
                  TabIndex        =   34
                  Top             =   1050
                  Width           =   1020
               End
               Begin VB.Label lblProviCod 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1260
                  TabIndex        =   33
                  Top             =   1410
                  Width           =   1020
               End
               Begin VB.Label lblPob 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   3195
                  TabIndex        =   32
                  Top             =   330
                  Width           =   3960
               End
               Begin VB.Label lblCal1Val 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   2460
                  TabIndex        =   31
                  Top             =   3765
                  Width           =   930
               End
               Begin VB.Label lblCal2Val 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   2460
                  TabIndex        =   30
                  Top             =   4125
                  Width           =   930
               End
               Begin VB.Label lblCal3Val 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   2460
                  TabIndex        =   29
                  Top             =   4485
                  Width           =   930
               End
               Begin VB.Label Label1 
                  Caption         =   "URL:"
                  ForeColor       =   &H00000000&
                  Height          =   255
                  Left            =   180
                  TabIndex        =   28
                  Top             =   1920
                  Width           =   915
               End
               Begin VB.Label lblURL 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   315
                  Left            =   1245
                  TabIndex        =   27
                  Top             =   1800
                  Width           =   5940
               End
               Begin VB.Label lblDir 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   1260
                  TabIndex        =   26
                  Top             =   -15
                  Width           =   5895
               End
            End
            Begin VB.Label lblCodPort 
               Caption         =   "C�digo portal:"
               ForeColor       =   &H00000000&
               Height          =   225
               Left            =   510
               TabIndex        =   54
               Top             =   645
               Width           =   1065
            End
            Begin VB.Label lblPortal 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1590
               TabIndex        =   53
               Top             =   600
               Width           =   1845
            End
            Begin VB.Label Label11 
               Alignment       =   2  'Center
               BackColor       =   &H00C0E0FF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Datos del proveedor"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   0
               TabIndex        =   52
               Top             =   0
               Width           =   8205
            End
         End
         Begin VB.PictureBox picApartado 
            ForeColor       =   &H00000000&
            Height          =   6315
            Index           =   0
            Left            =   30
            ScaleHeight     =   6255
            ScaleWidth      =   8145
            TabIndex        =   70
            Top             =   60
            Width           =   8205
            Begin VB.ListBox lstMaterial 
               BackColor       =   &H80000018&
               Height          =   450
               Left            =   1770
               TabIndex        =   71
               Top             =   420
               Width           =   5700
            End
            Begin VB.PictureBox Picture1 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               ForeColor       =   &H00000000&
               Height          =   345
               Left            =   3570
               ScaleHeight     =   345
               ScaleWidth      =   4170
               TabIndex        =   73
               Top             =   2010
               Width           =   4170
               Begin VB.CheckBox chkPermAdjDir 
                  Caption         =   "Permitir adjudicaci�n directa"
                  ForeColor       =   &H00000000&
                  Height          =   315
                  Left            =   150
                  TabIndex        =   74
                  Top             =   0
                  Width           =   3750
               End
            End
            Begin VB.TextBox txtMat 
               BackColor       =   &H80000018&
               ForeColor       =   &H00000000&
               Height          =   315
               Left            =   1770
               Locked          =   -1  'True
               TabIndex        =   72
               TabStop         =   0   'False
               Top             =   540
               Width           =   5685
            End
            Begin VB.Label lblPago 
               Caption         =   "Forma de pago"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   300
               TabIndex        =   109
               Top             =   4710
               Width           =   1395
            End
            Begin VB.Label lblMon 
               Caption         =   "Moneda"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   300
               TabIndex        =   108
               Top             =   3165
               Width           =   1380
            End
            Begin VB.Label lblSolicitud 
               BackStyle       =   0  'Transparent
               Caption         =   "Referencia"
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   0
               Left            =   3675
               TabIndex        =   107
               Top             =   2670
               Width           =   1905
            End
            Begin VB.Label lblPresupuesto 
               BackStyle       =   0  'Transparent
               Caption         =   "Presupuesto"
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   0
               Left            =   300
               TabIndex        =   106
               Top             =   2670
               Width           =   1380
            End
            Begin VB.Label lblFecAdj 
               Caption         =   "Adjudicaci�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   300
               TabIndex        =   105
               Top             =   2055
               Width           =   1320
            End
            Begin VB.Label lblFecApe 
               BackStyle       =   0  'Transparent
               Caption         =   "Apertura"
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   0
               Left            =   300
               TabIndex        =   104
               Top             =   1185
               Width           =   720
            End
            Begin VB.Label lblFecNec 
               BackStyle       =   0  'Transparent
               Caption         =   "Necesidad:"
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   0
               Left            =   3675
               TabIndex        =   103
               Top             =   1185
               Width           =   1350
            End
            Begin VB.Label lblGMN4_4 
               Caption         =   "Material:"
               ForeColor       =   &H80000008&
               Height          =   225
               Left            =   300
               TabIndex        =   102
               Top             =   585
               Width           =   1290
            End
            Begin VB.Line Line1 
               BorderColor     =   &H00808080&
               BorderWidth     =   2
               X1              =   0
               X2              =   8160
               Y1              =   3555
               Y2              =   3540
            End
            Begin VB.Label lblDest 
               Caption         =   "Destino"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   300
               TabIndex        =   101
               Top             =   4230
               Width           =   1395
            End
            Begin VB.Line Line7 
               BorderColor     =   &H00808080&
               BorderWidth     =   2
               X1              =   0
               X2              =   8160
               Y1              =   2490
               Y2              =   2490
            End
            Begin VB.Line Line8 
               BorderColor     =   &H00808080&
               BorderWidth     =   2
               X1              =   0
               X2              =   8160
               Y1              =   1005
               Y2              =   990
            End
            Begin VB.Label lblDatosGenProce 
               Alignment       =   2  'Center
               BackColor       =   &H00C0E0FF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Datos generales de proceso"
               ForeColor       =   &H00000000&
               Height          =   315
               Left            =   0
               TabIndex        =   100
               Top             =   0
               Width           =   8190
            End
            Begin VB.Label lblFecFin 
               BackStyle       =   0  'Transparent
               Caption         =   "Fin suministro"
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   0
               Left            =   3675
               TabIndex        =   99
               Top             =   3735
               Width           =   1395
            End
            Begin VB.Label lblFecApe 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   97
               Top             =   1155
               Width           =   1350
            End
            Begin VB.Label lblFecAdj 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   96
               Top             =   2040
               Width           =   1350
            End
            Begin VB.Label lblFecNec 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   5505
               TabIndex        =   95
               Top             =   1140
               Width           =   1350
            End
            Begin VB.Label lblPresupuesto 
               Alignment       =   1  'Right Justify
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   94
               Top             =   2655
               Width           =   1350
            End
            Begin VB.Label lblSolicitud 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   5505
               TabIndex        =   93
               Top             =   2655
               Width           =   1605
            End
            Begin VB.Label lblCambio 
               Alignment       =   1  'Right Justify
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   5505
               TabIndex        =   92
               Top             =   3120
               Width           =   1605
            End
            Begin VB.Label lblFecFin 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   5505
               TabIndex        =   91
               Top             =   3720
               Width           =   1395
            End
            Begin VB.Label lblDest 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   2
               Left            =   3150
               TabIndex        =   90
               Top             =   4200
               Width           =   4215
            End
            Begin VB.Label lblPago 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   2
               Left            =   3150
               TabIndex        =   89
               Top             =   4680
               Width           =   4215
            End
            Begin VB.Label lblProveProce 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   2
               Left            =   3150
               TabIndex        =   88
               Top             =   5160
               Width           =   4215
            End
            Begin VB.Label lblProveProce 
               Caption         =   "Proveedor actual"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   300
               TabIndex        =   87
               Top             =   5190
               Visible         =   0   'False
               Width           =   1410
            End
            Begin VB.Label lblPago 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   86
               Top             =   4680
               Width           =   1350
            End
            Begin VB.Label lblDest 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   85
               Top             =   4200
               Width           =   1350
            End
            Begin VB.Label lblFecIni 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   84
               Top             =   3720
               Width           =   1350
            End
            Begin VB.Label lblMon 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   83
               Top             =   3120
               Width           =   1350
            End
            Begin VB.Label lblProveProce 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   82
               Top             =   5160
               Width           =   1350
            End
            Begin VB.Label lblCambio 
               Caption         =   "Equivalencia"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   3675
               TabIndex        =   81
               Top             =   3165
               Width           =   1230
            End
            Begin VB.Label lblFecPres 
               BackStyle       =   0  'Transparent
               Caption         =   "Presentaci�n"
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   0
               Left            =   300
               TabIndex        =   80
               Top             =   1635
               Width           =   1290
            End
            Begin VB.Label lblFecPres 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   79
               Top             =   1605
               Width           =   1350
            End
            Begin VB.Label lblFecLimit 
               BackStyle       =   0  'Transparent
               Caption         =   "L�mite ofertas"
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   0
               Left            =   3675
               TabIndex        =   78
               Top             =   1620
               Width           =   1290
            End
            Begin VB.Label lblFecLimit 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   5505
               TabIndex        =   77
               Top             =   1590
               Width           =   1350
            End
            Begin VB.Label lblRequest 
               Caption         =   "Solicitud de compra:"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   300
               TabIndex        =   76
               Top             =   5670
               Visible         =   0   'False
               Width           =   1470
            End
            Begin VB.Label lblRequest 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   1
               Left            =   1770
               TabIndex        =   75
               Top             =   5640
               Width           =   5595
            End
            Begin VB.Label lblFecIni 
               BackStyle       =   0  'Transparent
               Caption         =   "Inicio suministro"
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   0
               Left            =   300
               TabIndex        =   98
               Top             =   3735
               Width           =   1515
            End
         End
      End
      Begin MSComctlLib.TreeView tvwProce 
         Height          =   6435
         Left            =   30
         TabIndex        =   20
         Top             =   30
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   11351
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList3"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdResponsable 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11355
      Picture         =   "frmCONTRATOS.frx":077A
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   45
      Width           =   315
   End
   Begin VB.CommandButton cmdBuscar 
      Height          =   285
      Left            =   11000
      Picture         =   "frmCONTRATOS.frx":0801
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   45
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
      Height          =   285
      Left            =   660
      TabIndex        =   5
      Top             =   45
      Width           =   840
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   1693
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1482
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
      Height          =   285
      Left            =   4710
      TabIndex        =   6
      Top             =   45
      Width           =   1185
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1879
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   8467
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "INVI"
      Columns(2).Name =   "INVI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   2090
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
      Height          =   285
      Left            =   5940
      TabIndex        =   7
      Top             =   45
      Width           =   5030
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   7355
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1693
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "INVI"
      Columns(2).Name =   "INVI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   8872
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
      Height          =   285
      Left            =   2340
      TabIndex        =   8
      Top             =   45
      Width           =   1065
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   900
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "INVI"
      Columns(2).Name =   "INVI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   1879
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin MSComctlLib.ImageList ImageList3 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCONTRATOS.frx":0B43
            Key             =   "Proceso"
            Object.Tag             =   "Proceso"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCONTRATOS.frx":0F95
            Key             =   "Prove"
            Object.Tag             =   "Prove"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCONTRATOS.frx":186F
            Key             =   "Contrato"
            Object.Tag             =   "Contrato"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdAdjun 
      Left            =   1410
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin VB.PictureBox picControl 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   510
      Index           =   2
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   11760
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   11760
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   1230
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Index           =   2
         Left            =   2310
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Sustituir"
         Height          =   345
         Left            =   150
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
   End
   Begin VB.PictureBox picControl 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   390
      Index           =   0
      Left            =   0
      ScaleHeight     =   390
      ScaleWidth      =   11760
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   7470
      Visible         =   0   'False
      Width           =   11760
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   30
         Width           =   1005
      End
   End
   Begin VB.PictureBox picControl 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   510
      Index           =   1
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   11760
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   6960
      Visible         =   0   'False
      Width           =   11760
      Begin VB.CommandButton cmdAdjuntar 
         Caption         =   "&Adjuntar contrato"
         Height          =   345
         Left            =   1890
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   90
         Width           =   1485
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Index           =   1
         Left            =   3495
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   90
         Width           =   1485
      End
      Begin VB.CommandButton cmdCrear 
         Caption         =   "&Generar contrato"
         Height          =   345
         Left            =   120
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   90
         Width           =   1620
      End
   End
   Begin VB.Label lblAnyo 
      Caption         =   "A�o:"
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   0
      TabIndex        =   10
      Top             =   90
      Width           =   615
   End
   Begin VB.Label lblGMN1_4 
      Caption         =   "Comm:"
      Height          =   225
      Left            =   1710
      TabIndex        =   11
      Top             =   90
      Width           =   810
   End
   Begin VB.Label lblCProceCod 
      Caption         =   "Proceso:"
      Height          =   180
      Left            =   3705
      TabIndex        =   9
      Top             =   120
      Width           =   960
   End
End
Attribute VB_Name = "frmCONTRATOS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const iAptDatosGenerales = 0
Const iAptProveedor = 1
Const iAptContratoDoc = 2
Const iAptContratoPdf = 3
Const iAptContratoImg = 4

Private m_sFormatoNumber As String
Private Accion As accionessummit

'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private sayFileNames() As String

Public g_sNombreNuevo As String

Private m_bRespetarCombo    As Boolean
Private m_bCargarComboDesde As Boolean
Private m_oProcesos As CProcesos
Public m_oProves As CProveedores
Private m_oGruposMN1 As CGruposMatNivel1
Private m_oGMN1Seleccionado As CGrupoMatNivel1
Public g_oProcesoSeleccionado As CProceso
Public g_oProveSeleccionado As CProveedor
Private m_oContratoSeleccionado As CContrato
Private m_oIBaseDatos As IBaseDatos

'Restricciones y permisos
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRMat As Boolean  'Se aplica a la combo de procesos
Private m_bREqp As Boolean  'Se aplica a la combo de procesos
Private m_bRComp As Boolean 'Se aplica a la combo de procesos
Private m_bRCompResp As Boolean 'Se aplica a la combo de procesos
Private m_bRUsuAper As Boolean 'Se aplica a la combo de procesos
Private m_bRContrAsigEqp As Boolean 'Se aplica a los proveedores
Private m_bRContrAsigComp As Boolean 'Se aplica a los proveedores
Private m_bRContrUsu As Boolean 'Se aplica a los contratos
Private m_bCrear As Boolean
Private m_bMod As Boolean
Private m_bEli As Boolean
Private m_bModifResponsable As Boolean
Private g_bSoloInvitado As Boolean
Private m_bRestProvMatComp As Boolean

Private m_arZoom(0 To 8) As Integer
Private m_iZoom As Integer
Private m_bRespetarTxt As Boolean
Private m_picActual As vb.PictureBox
Private m_picControlActual As vb.PictureBox

Private m_sIdiProceso As String
Private m_sIdiCodigo As String
Private m_sIdiFecha As String
Private m_sIdiMon As String
Private m_sIdiSelProceso As String
Private m_sIdiTodosArchivos As String
Private m_sIdiElArchivo As String
Private m_sIdiGuardar As String
Private m_sIdiTipoOrig As String
Private m_sIdiSelecAdjunto  As String
Private m_sIdiMaterial As String
Private m_sIdiArchivosPDF As String
Private m_sIdiElContrato As String
Private m_sIdiAceptar As String
Private m_sIdiCancelar As String
Private m_sIdiNombre As String
Private m_sIdiAdjuntarTitle As String
Private m_sIdiZoom As String
Private m_sIdiExport As String
Public g_sIdiNombreContr As String

Private oFos As FileSystemObject


Private Sub ConfigurarBotones()

If Not m_bMod Then
    cmdModificar.Visible = False 'picControlContrato
    cmdRestaurar(2).Left = cmdEliminar.Left
    cmdEliminar.Left = cmdModificar.Left
End If
If Not m_bEli Then
    cmdEliminar.Visible = False
    cmdRestaurar(2).Left = cmdEliminar.Left
End If
If Not m_bCrear Then
    cmdCrear.Visible = False 'picControlProve
    cmdAdjuntar.Visible = False
    cmdRestaurar(1).Left = cmdCrear.Left
End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRCrear)) Is Nothing) Then
        m_bCrear = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRModificar)) Is Nothing) Then
        m_bMod = True
    End If

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTREliminar)) Is Nothing) Then
        m_bEli = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestComprador)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        m_bRComp = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestEquipo)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        m_bREqp = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestResponsable)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        m_bRCompResp = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestMatComprador)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        m_bRMat = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestUsuAper)) Is Nothing) Then
        m_bRUsuAper = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestContrCom)) Is Nothing) Then
        m_bRContrAsigComp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestContrEqp)) Is Nothing) Then
        m_bRContrAsigEqp = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestUsuContr)) Is Nothing) Then
        m_bRContrUsu = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestUsuUON)) Is Nothing) Then
        m_bRUsuUON = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestUsuDep)) Is Nothing) Then
        m_bRUsuDep = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRModifResponsable)) Is Nothing) Then
        m_bModifResponsable = True
    Else
        m_bModifResponsable = False
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRRestProvMatComp)) Is Nothing Then
        m_bRestProvMatComp = True
    Else
        m_bRestProvMatComp = False
    End If

    ConfigurarBotones
End Sub

Public Function HabilitarBotonesInvitado()
' funci�n para mostrar/ocultar botones si se es invitado

If g_oProcesoSeleccionado.Invitado Then

    cmdCrear.Visible = False
    cmdAdjuntar.Visible = False
    cmdModificar.Visible = False
    cmdEliminar.Visible = False
    
    cmdRestaurar(0).Left = 100
    cmdRestaurar(1).Left = 100
Else
    cmdCrear.Visible = True
    cmdAdjuntar.Visible = True
    cmdModificar.Visible = True
    cmdEliminar.Visible = True
    
    cmdRestaurar(0).Left = 100
    cmdRestaurar(1).Left = cmdAdjuntar.Left + 1600

End If

End Function

''' <summary>
''' Abre el fichero en windows q se esta mostrando actualmente en la pantalla contratos.
''' </summary>
''' <param name="Index">Quien pide abrir. Panel Doc o Imagen</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdAbrir_Click(Index As Integer)
Dim DataFile As Integer
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim bytChunk() As Byte
Dim lSize As Long
Dim lInit As Long
    
    
On Error GoTo Cancelar:

If m_oContratoSeleccionado Is Nothing Then Exit Sub

sFileName = basUtilidades.DevolverPathFichTemp
sFileTitle = m_oContratoSeleccionado.nombre & "." & m_oContratoSeleccionado.Extension
sFileName = sFileName & sFileTitle

If m_oContratoSeleccionado.dataSize = 0 Then
    basMensajes.NoValido m_sIdiElArchivo & " " & sFileTitle
    Exit Sub
End If

teserror = m_oContratoSeleccionado.ComenzarLecturaData
If teserror.NumError <> TESnoerror Then
    TratarError teserror
    Exit Sub
End If
DataFile = 1

Screen.MousePointer = vbHourglass
'Abrimos el fichero para escritura binaria
Open sFileName For Binary Access Write As DataFile

lInit = 0
lSize = giChunkSize

While lInit < m_oContratoSeleccionado.dataSize
    bytChunk = m_oContratoSeleccionado.ReadData(lInit, lSize)
    Put DataFile, , bytChunk
    lInit = lInit + lSize + 1
Wend

'Verificar escritura
If LOF(DataFile) <> m_oContratoSeleccionado.dataSize Then
    basMensajes.ErrorDeEscrituraEnDisco
End If
Close DataFile
        
sayFileNames(UBound(sayFileNames)) = sFileName
ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)

'Lanzamos la aplicacion
ShellExecute MDI.hwnd, "Open", sFileName, 0&, "C:\", 1

DataFile = 0

Cancelar:

    If DataFile <> 0 Then
        Close DataFile
        DataFile = 0
    End If
Screen.MousePointer = vbNormal
End Sub

Private Sub cmdBuscar_Click()
    If Accion = ACCContratosCon Then
        frmPROCEBuscar.bRDest = False
        frmPROCEBuscar.m_bProveAsigComp = False
        frmPROCEBuscar.m_bProveAsigEqp = False
        frmPROCEBuscar.bRUsuAper = m_bRUsuAper
        frmPROCEBuscar.bRAsig = m_bRComp
        frmPROCEBuscar.bREqpAsig = m_bREqp
        frmPROCEBuscar.bRMat = m_bRMat
        frmPROCEBuscar.bRUsuDep = m_bRUsuDep
        frmPROCEBuscar.bRUsuUON = m_bRUsuUON
        frmPROCEBuscar.bRCompResponsable = m_bRCompResp
        frmPROCEBuscar.bRestProvMatComp = m_bRestProvMatComp
        frmPROCEBuscar.bRestProvEquComp = False
        frmPROCEBuscar.sOrigen = "frmCONTRATOS"
        frmPROCEBuscar.sdbcAnyo = sdbcAnyo.Text
        frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod.Text
        frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
        frmPROCEBuscar.Show 1
        
    End If

End Sub
Public Sub CargarProcesoConBusqueda()

    LimpiarCampos
    
    Set m_oProcesos = Nothing
    Set m_oProcesos = frmPROCEBuscar.oProceEncontrados
    
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo.Text = m_oProcesos.Item(1).Anyo
    m_bRespetarCombo = True
    sdbcGMN1_4Cod.Text = m_oProcesos.Item(1).GMN1Cod
    sdbcGMN1_4Cod_Validate False
    sdbcProceCod.Text = m_oProcesos.Item(1).Cod
    sdbcProceDen.Text = m_oProcesos.Item(1).Den
    sdbcProceCod_Validate False
    m_bRespetarCombo = False
    ProcesoSeleccionado

End Sub

''' <summary>
''' Imprime el fichero q se esta mostrando actualmente en la pantalla contratos.
''' </summary>
''' <param name="Index">Quien pide imprimir. Panel Doc o Imagen</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdImprimir_Click(Index As Integer)
    Dim lApp As Long
    Dim i As Integer
    Dim DataFile As Integer
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    Dim bytChunk() As Byte
    Dim lSize As Long
    Dim lInit As Long
    Dim objPrinter As Printer
    Dim sDeviceName As String
    Dim X As Variant

    On Error GoTo err_FilePrint
    
    'Si es un documento que no es v�lido para cargarlo en el TXTText (distinto de .doc,.txt o .rtf) o en el PajantImage
    'lo copiamos en el temporal ,lo abrimos y que se imprima desde all� sino al imprimir sacar� una hoja en blanco.
    
    i = DevolverExtension(m_oContratoSeleccionado.Extension)
    
    If i = 0 Then
        sFileName = basUtilidades.DevolverPathFichTemp
        sFileTitle = m_oContratoSeleccionado.nombre & "." & m_oContratoSeleccionado.Extension
        sFileName = sFileName & sFileTitle
        
        If m_oContratoSeleccionado.dataSize = 0 Then
            basMensajes.NoValido m_sIdiElArchivo & " " & sFileTitle
            Exit Sub
        End If
    
        teserror = m_oContratoSeleccionado.ComenzarLecturaData
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Exit Sub
        End If
        DataFile = 1
    
        'Abrimos el fichero para escritura binaria
        Screen.MousePointer = vbHourglass
        Open sFileName For Binary Access Write As DataFile
        
        lInit = 0
        lSize = giChunkSize
    
        While lInit < m_oContratoSeleccionado.dataSize
            bytChunk = m_oContratoSeleccionado.ReadData(lInit, lSize)
            Put DataFile, , bytChunk
            lInit = lInit + lSize + 1
        Wend
        
        'Verificar escritura
        If LOF(DataFile) <> m_oContratoSeleccionado.dataSize Then
            Screen.MousePointer = vbNormal
            basMensajes.ErrorDeEscrituraEnDisco
        End If
        Close DataFile
        
        sayFileNames(UBound(sayFileNames)) = sFileName
        ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
    
        
        'Lanzamos la aplicacion
        lApp = ShellExecute(MDI.hwnd, "Open", sFileName, 0&, "C:\", 1)
        If lApp < 32 Then
            Screen.MousePointer = vbNormal
            basMensajes.ImposibleImprimir
        Else
            'una vez que est� abierto se podr�a lanzar a impresora as�:
            'ShellExecute MDI.hWnd, "Print", sFileName, "", "C:\", 0
        End If
        
        DataFile = 0

    ElseIf i = 13 Then   ' es una imagen
        '''''
        Dim pictureOut As Variant
        'Dim prn As Printer
    
        'Export to temporary picture object
        Me.PajantImagen.pi.ExportTo PJT_PICTURE, 0, 0, pictureOut, PJT_FRAME

        sDeviceName = Printer.DeviceName

        cmmdAdjun.Copies = 1
        cmmdAdjun.FromPage = 1
        cmmdAdjun.ToPage = 1
        cmmdAdjun.min = 1
        cmmdAdjun.max = 1
        cmmdAdjun.FLAGS = cdlPDHidePrintToFile Or cdlPDNoSelection
        cmmdAdjun.CancelError = True
        cmmdAdjun.ShowPrinter
        
        DoEvents
        
        'Print picture as default size using standard method PaintPicture
        Printer.PaintPicture pictureOut, 0, 0
        
        'Commit the print
        Printer.EndDoc
        
'        'Restablecer la impresora original
'        If Printer.DeviceName <> sDeviceName Then
'            For Each objPrinter In Printers
'                If objPrinter.DeviceName = sDeviceName Then
'                    Set Printer = objPrinter
'                    Exit For
'                End If
'            Next
'        End If
    Else ' es un .doc, .rtf o .txt
        ' Initialize and call the common print dialog
        cmmdAdjun.Copies = 1
        cmmdAdjun.FromPage = 1
        cmmdAdjun.ToPage = TXTextContrato.CurrentPages
        cmmdAdjun.min = 1
        cmmdAdjun.max = TXTextContrato.CurrentPages
        cmmdAdjun.FLAGS = cdlPDHidePrintToFile Or cdlPDNoSelection
        cmmdAdjun.CancelError = True
        cmmdAdjun.ShowPrinter
    
        ' Print selected pages
        TXTextContrato.PrintDoc m_oContratoSeleccionado.nombre & "." & m_oContratoSeleccionado.Extension, cmmdAdjun.FromPage, cmmdAdjun.ToPage, cmmdAdjun.Copies
    End If

    
    cmmdAdjun.FLAGS = cdlOFNPathMustExist Or cdlOFNFileMustExist
    Screen.MousePointer = vbNormal
    Exit Sub

err_FilePrint:
    Screen.MousePointer = vbNormal
    If Err <> cdlCancel And Err <> 482 Then MsgBox Err.Description, vbCritical, "FULLSTEP"

    If DataFile <> 0 Then
        Close DataFile
        DataFile = 0
    End If
End Sub

Private Sub cmdResponsable_Click()
    'Si no tiene permisos para modificar el responsable del proceso muestra directamente el detalle del responsable,
    'si no muestra el men� del responsable de proceso
    
    If m_bModifResponsable = False Or g_oProcesoSeleccionado.Estado >= conadjudicaciones Or g_oProcesoSeleccionado.Invitado Then
        VerDetalleResponsable
    Else
        Set MDI.g_ofrmOrigenResponsable = Me
        PopupMenu MDI.mnuPOPUPResponsable
    End If
End Sub

Private Sub cmdRestaurar_Click(Index As Integer)
Select Case Index
Case 0
    ProcesoSeleccionado
Case 1
    Screen.MousePointer = vbHourglass
    m_oProves.CargarDatosProveedor (g_oProveSeleccionado.Cod)
    g_oProveSeleccionado.CargarTodosLosContactos , , , , True
    Set g_oProveSeleccionado.Contratos = Nothing
    If m_bRContrUsu Then
        m_oProves.CargarContratosProce g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, basOptimizacion.gvarCodUsuario, g_oProveSeleccionado.Cod
    Else
        m_oProves.CargarContratosProce g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, , g_oProveSeleccionado.Cod
    End If
    RestaurarArbol 1
    MostrarProveedor
    Screen.MousePointer = vbNormal
Case 2
    Screen.MousePointer = vbHourglass
    m_oContratoSeleccionado.CargarDatos
    If IsNull(m_oContratoSeleccionado.Id) Then
        basMensajes.DatoEliminado m_sIdiElContrato & ": " & m_oContratoSeleccionado.nombre
        cmdRestaurar_Click 1
    Else
        RestaurarArbol 2
        MostrarContrato
    End If
    Screen.MousePointer = vbNormal
End Select

End Sub

''' <summary>
''' Salva a disco duro el fichero q se esta mostrando actualmente en la pantalla contratos.
''' Se muestra el archivo sin extensi�n en el showsave para el caso de descargar una imagen Imagen1.jpg
''' si ellos ponen pdfImg.pdf, raro pero deja hacerlo, se grabe pdfImg.pdf.jpg y se pueda abrir el archivo.
''' En el showsave, pone en 'save as type:' tipo original, esto ya da la idea de q un jpg se guarda como jpg
''' y un doc se guarda como un doc.
''' </summary>
''' <param name="Index">Quien pide salvar. Panel Doc o Imagen</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdSalvar_Click(Index As Integer)
Dim DataFile As Integer
Dim sFileName As String
Dim sFileTitle As String
Dim sAuxFileTitle As String
Dim teserror As TipoErrorSummit
Dim bytChunk() As Byte
Dim lSize As Long
Dim lInit As Long
    

On Error GoTo Cancelar:

    If m_oContratoSeleccionado Is Nothing Then Exit Sub

    cmmdAdjun.DialogTitle = m_sIdiGuardar
    cmmdAdjun.CancelError = True
    cmmdAdjun.Filter = m_sIdiTipoOrig & "|*.*"
    cmmdAdjun.filename = m_oContratoSeleccionado.nombre & "." & m_oContratoSeleccionado.Extension
    cmmdAdjun.ShowSave

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle
    If sFileTitle = "" Then
        basMensajes.NoValido m_sIdiElArchivo
        Exit Sub
    End If
    
    sAuxFileTitle = sFileTitle
    If InStr(sAuxFileTitle, ".") > 0 Then
        While InStr(sAuxFileTitle, ".") > 0
            sAuxFileTitle = Right(sAuxFileTitle, Len(sAuxFileTitle) - InStr(sAuxFileTitle, "."))
        Wend
        
        If Not (sAuxFileTitle = m_oContratoSeleccionado.Extension) Then
            sFileName = sFileName & "." & m_oContratoSeleccionado.Extension
        End If
    Else
        sFileName = sFileName & "." & m_oContratoSeleccionado.Extension
    End If
    
    DataFile = 1

    'Abrimos el fichero para escritura binaria
    Open sFileName For Binary Access Write As DataFile

    If m_oContratoSeleccionado.dataSize = 0 Then
        basMensajes.NoValido m_sIdiElArchivo & " " & m_oContratoSeleccionado.nombre
        If DataFile <> 0 Then
            Close DataFile
            DataFile = 0
        End If
        Exit Sub
    End If
    teserror = m_oContratoSeleccionado.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If DataFile <> 0 Then
            Close DataFile
            DataFile = 0
        End If
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    lInit = 0
    lSize = giChunkSize
    
    While lInit < m_oContratoSeleccionado.dataSize
        bytChunk = m_oContratoSeleccionado.ReadData(lInit, lSize)
        Put DataFile, , bytChunk
        lInit = lInit + lSize + 1
    Wend

    'Verificar escritura
    If LOF(DataFile) <> m_oContratoSeleccionado.dataSize Then
        basMensajes.ErrorDeEscrituraEnDisco
    End If
    Screen.MousePointer = vbNormal
    
    Close DataFile
    DataFile = 0
    Exit Sub
    
Cancelar:

    If Err.Number = 32755 Then '32755 = han pulsado cancel
        Screen.MousePointer = vbNormal
    ElseIf Err.Number = 75 Or Err.Number = 70 Then
        ImposibleSalvarArchivo
    Else
        MsgBox Err.Description, vbCritical, "FULLSTEP GS"
    End If
    Screen.MousePointer = vbNormal
    If DataFile <> 0 Then
        Close DataFile
        DataFile = 0
    End If
End Sub

''' <summary>
''' Redimensionar la pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub Form_Resize()

If Me.Height < 2000 Then Exit Sub
If Me.Width < 4000 Then Exit Sub

tvwProce.Height = Me.Height - 1275
picPrincipal.Height = Me.Height - 1275
picPrincipal.Width = Me.Width - 3525
''Datos Gen
picApartado(0).Height = Me.Height - 1720 '695

''Prove
picApartado(1).Height = picApartado(0).Height

''Contrato Doc
picApartado(2).Height = picApartado(0).Height
TXTextContrato.Height = picApartado(2).Height - Me.lblContratoDoc.Height - 120
picTextoDoc.Height = TXTextContrato.Height

''Contrato Pdf
picApartado(3).Height = picApartado(0).Height
AcroPDFControl.Height = picApartado(3).Height - Me.lblContratoPdf.Height - 120

''Contrato Img
picApartado(4).Height = picApartado(0).Height
PajantImagen.Height = picApartado(4).Height - Me.lblContratoImg.Height - 120

Me.SSSplitter1.Height = Me.Height - 1475
Me.SSSplitter1.Width = Me.Width - 150

End Sub

Private Sub sdbcGMN1_4Cod_Change()
    
    If Not m_bRespetarCombo Then
        m_bCargarComboDesde = True
        Set m_oGMN1Seleccionado = Nothing
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Text = ""
        sdbcProceCod.Columns(1).Text = ""
        sdbcProceCod.RemoveAll
        
    End If
    
End Sub
Private Sub sdbcGMN1_4Cod_Click()
    
    If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod = ""
    End If
        
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    m_bRespetarCombo = False
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Text = ""
    sdbcProceCod.Columns(1).Text = ""
    sdbcProceCod.RemoveAll
    
    GMN1Seleccionado
    m_bCargarComboDesde = False
    
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set m_oGruposMN1 = Nothing
    Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    
    If m_bCargarComboDesde Then
    
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
      
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
         
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
       
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = m_oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        If IsMissing(m_oGruposMN1.Item(i + 1).Invitado) Then
            sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & ""
        Else
            sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & m_oGruposMN1.Item(i + 1).Invitado
        End If
    Next
    
    If Not m_oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If
    
    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN1_4Cod_InitColumnProps()

    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el grupo
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If m_oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            basMensajes.NoValido m_sIdiMaterial
        Else
            Set m_oGMN1Seleccionado = Nothing
            Set m_oGMN1Seleccionado = m_oGruposMN1.Item(scod1)
            m_bCargarComboDesde = False
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
 
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
            
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            basMensajes.NoValido m_sIdiMaterial
        Else
            Set m_oGMN1Seleccionado = Nothing
            Set m_oGMN1Seleccionado = oGMN1
            m_bCargarComboDesde = False
        End If
    
    End If
    Screen.MousePointer = vbNormal
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set m_oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
End Sub

Public Sub GMN1Seleccionado()
    
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
        
    m_oGMN1Seleccionado.Cod = sdbcGMN1_4Cod.Text
    
End Sub
Private Sub sdbcProceCod_Change()
    
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
    
        sdbcProceDen.Text = ""
        Set g_oProcesoSeleccionado = Nothing
        Set g_oProveSeleccionado = Nothing
        Set m_oContratoSeleccionado = Nothing

        m_bCargarComboDesde = True
        
        LimpiarCampos
        
        m_bRespetarCombo = False
    End If
    
End Sub
Private Sub sdbcProceCod_Click()
    
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceCod.Text = ""
    End If

End Sub


Private Sub sdbcProceCod_CloseUp()
    
    If sdbcProceCod.Value = "..." Or Trim(sdbcProceCod.Value) = "" Then
        sdbcProceCod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    m_bRespetarCombo = False
    
    ProcesoSeleccionado
    m_bCargarComboDesde = False
        
End Sub

Private Sub sdbcProceCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim oProceso As CProceso
    
    sdbcProceCod.RemoveAll
    
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set g_oProcesoSeleccionado = Nothing
    Set g_oProveSeleccionado = Nothing
    Set m_oContratoSeleccionado = Nothing
        
    If m_bCargarComboDesde Then
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado
    Else
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado
    End If
        
    For Each oProceso In m_oProcesos
        sdbcProceCod.AddItem oProceso.Cod & Chr(m_lSeparador) & oProceso.Den & Chr(m_lSeparador) & oProceso.Invitado
    Next
   
    
    'For i = 0 To UBound(Codigos.Cod) - 1
    '    sdbcProceCod.AddItem Codigos.Cod(i) & chr(m_lSeparador) & Codigos.Den(i) & chr(m_lSeparador) & m_oProcesos.Item(i + 1).Invitado
    'Next
    
    If Not m_oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceCod_InitColumnProps()

    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcProceCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceCod.Rows - 1
            bm = sdbcProceCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcProceCod_Validate(Cancel As Boolean)


    
    If Trim(sdbcProceCod.Text = "") Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        Set g_oProcesoSeleccionado = Nothing
        Set g_oProveSeleccionado = Nothing
        Set m_oContratoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        basMensajes.NoValido m_sIdiProceso
        Set g_oProcesoSeleccionado = Nothing
        Set g_oProveSeleccionado = Nothing
        Set m_oContratoSeleccionado = Nothing
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        basMensajes.NoValido m_sIdiCodigo
        Set g_oProcesoSeleccionado = Nothing
        Set g_oProveSeleccionado = Nothing
        Set m_oContratoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        m_bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        m_bRespetarCombo = False
        
        If g_oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
        m_bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
        m_bRespetarCombo = False
        If g_oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proceso
    
    m_oProcesos.CargarTodosLosProcesosDesde 1, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado
                
    If m_oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Text = ""
        Screen.MousePointer = vbNormal
        basMensajes.NoValido m_sIdiProceso
        Set g_oProcesoSeleccionado = Nothing
        Set g_oProveSeleccionado = Nothing
        Set m_oContratoSeleccionado = Nothing

    Else
        m_bRespetarCombo = True
        sdbcProceDen.Text = m_oProcesos.Item(1).Den
        
        sdbcProceCod.Columns("COD").Text = sdbcProceCod.Text
        sdbcProceCod.Columns("DEN").Text = sdbcProceDen.Text
        sdbcProceCod.Columns("INVI").Text = m_oProcesos.Item(1).Invitado
        
        m_bRespetarCombo = False
        Set g_oProcesoSeleccionado = Nothing
        Set g_oProveSeleccionado = Nothing
        Set m_oContratoSeleccionado = Nothing
       
        m_bCargarComboDesde = False
        ProcesoSeleccionado
    End If

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProceDen_Change()
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcProceCod.RemoveAll
        sdbcProceCod.Text = ""
        Set g_oProcesoSeleccionado = Nothing
        Set g_oProveSeleccionado = Nothing
        Set m_oContratoSeleccionado = Nothing
        m_bCargarComboDesde = True 'ZZ cuidadin
        
        LimpiarCampos
        
        m_bRespetarCombo = False
    End If
       
End Sub
Private Sub sdbcProceDen_Click()

    MostrarApartado 1000
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen = ""
        sdbcProceCod = ""
    End If
        
End Sub

Private Sub sdbcProceDen_CloseUp()
    
    If sdbcProceDen.Value = "....." Or Trim(sdbcProceDen.Value) = "" Then
        sdbcProceDen.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    m_bRespetarCombo = False
    
    ProcesoSeleccionado
    m_bCargarComboDesde = False
        
End Sub

Private Sub sdbcProceDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim oProceso As CProceso
    
    sdbcProceDen.RemoveAll
    
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set g_oProcesoSeleccionado = Nothing
    Set g_oProveSeleccionado = Nothing
    Set m_oContratoSeleccionado = Nothing
    
    If m_bCargarComboDesde Then
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , sdbcProceDen, False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado
    Else
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado
    End If
        
    For Each oProceso In m_oProcesos
        sdbcProceDen.AddItem oProceso.Den & Chr(m_lSeparador) & oProceso.Cod & Chr(m_lSeparador) & oProceso.Invitado
    Next
    
    'For i = 0 To UBound(Codigos.Cod) - 1
    '    sdbcProceDen.AddItem Codigos.Den(i) & chr(m_lSeparador) & Codigos.Cod(i) & chr(m_lSeparador) & m_oProcesos.Item(i + 1).Invitado
    'Next
    
    If Not m_oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceDen_InitColumnProps()

    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProceDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceDen.Rows - 1
            bm = sdbcProceDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub




Private Sub cmdMas_Click()

MDI.mnuZoom(m_iZoom).Checked = False
If Left(Trim(txtZoom.Text), Len(Trim(txtZoom.Text)) - 1) <> m_arZoom(m_iZoom) Then
    If Left(Trim(txtZoom.Text), Len(Trim(txtZoom.Text)) - 1) < m_arZoom(m_iZoom) Then
        m_iZoom = m_iZoom
    Else
        If m_iZoom <> 0 Then m_iZoom = m_iZoom - 1
    End If
Else
    m_iZoom = m_iZoom - 1
End If

If m_iZoom = 0 Then cmdMas.Enabled = False
cmdMenos.Enabled = True
TXTextContrato.ZoomFactor = m_arZoom(m_iZoom)
m_bRespetarTxt = True
txtZoom.Text = m_arZoom(m_iZoom) & "%"
MDI.mnuZoom(m_iZoom).Checked = True

End Sub

Private Sub cmdMenos_Click()

MDI.mnuZoom(m_iZoom).Checked = False
If Left(Trim(txtZoom.Text), Len(Trim(txtZoom.Text)) - 1) <> m_arZoom(m_iZoom) Then
    If Left(Trim(txtZoom.Text), Len(Trim(txtZoom.Text)) - 1) > m_arZoom(m_iZoom) Then
        m_iZoom = m_iZoom
    Else
        If m_iZoom <> 8 Then m_iZoom = m_iZoom + 1
    End If
Else
    m_iZoom = m_iZoom + 1
End If
If m_iZoom = 8 Then cmdMenos.Enabled = False
cmdMas.Enabled = True

TXTextContrato.ZoomFactor = m_arZoom(m_iZoom)
m_bRespetarTxt = True
txtZoom.Text = m_arZoom(m_iZoom) & "%"
MDI.mnuZoom(m_iZoom).Checked = True

End Sub

Public Sub MenuZoom(i As Integer)

m_iZoom = i
cmdMas.Enabled = True
cmdMenos.Enabled = True
If m_iZoom = 8 Then cmdMenos.Enabled = False
If m_iZoom = 0 Then cmdMas.Enabled = False
TXTextContrato.ZoomFactor = m_arZoom(m_iZoom)
m_bRespetarTxt = True
txtZoom.Text = m_arZoom(m_iZoom) & "%"

End Sub

''' <summary>
''' Abre la selecci�n de zoom del TXTText
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdMenu_Click()
    PopupMenu MDI.mnuZoomP, , picPrincipal.Left + cmdMenu.Left + Me.picToolBarDoc.Left + 50, picPrincipal.Top + cmdMenu.Top + picToolBarDoc.Top + 400
End Sub

Private Sub cmdPDFCon_Click()
Dim sNombre  As String

On Error GoTo Error

        If m_oContratoSeleccionado Is Nothing Then Exit Sub
        
        cmmdAdjun.CancelError = True
        cmmdAdjun.DialogTitle = m_sIdiExport
        cmmdAdjun.Filter = m_sIdiArchivosPDF & " (*.pdf)|*.pdf"
        cmmdAdjun.filename = m_oContratoSeleccionado.nombre & ".pdf"
        cmmdAdjun.ShowSave

        sNombre = cmmdAdjun.FileTitle
        'Salvamos a formato PDF
        TXTextContrato.Save sNombre, , 12
        
        Exit Sub
Error:
    If Err.Number = cdlCancel Then
        Exit Sub
    ElseIf Err.Number = 75 Or Err.Number = 70 Then
        basMensajes.ImposibleSalvarArchivo
        Exit Sub
    Else
        MsgBox Err.Description, vbCritical, "FULLSTEP GS"
    End If

End Sub


Private Sub Form_Load()
Dim i As Integer

    Me.Width = 11880
    Me.Height = 7770
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Obtiene el n� de decimales a mostrar en esta pantalla
    m_sFormatoNumber = "#,##0."
    For i = 1 To gParametrosInstalacion.giNumDecimalesComp
        m_sFormatoNumber = m_sFormatoNumber & "0"
    Next i
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    g_bSoloInvitado = False
    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        If oUsuarioSummit.EsInvitado Then
            If oUsuarioSummit.Acciones Is Nothing Then
                g_bSoloInvitado = True
            Else
                If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONTRConsultar)) Is Nothing Then
                    g_bSoloInvitado = True
                End If
            End If
        End If
    End If
    
    ConfigurarSeguridad
    LimpiarCampos
    
    CargarAnyos
    
    Accion = ACCContratosCon
    
    Set m_oProcesos = oFSGSRaiz.generar_CProcesos
    
    'Inicializar el array de ficheros
    ReDim sayFileNames(0)
    cmmdAdjun.FLAGS = cdlOFNPathMustExist Or cdlOFNFileMustExist
    TXTextContrato.EditMode = 1
    
    'Muestra la imagen
    PajantImagen.pi.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"
    
    m_iZoom = 4
    m_arZoom(0) = 400
    m_arZoom(1) = 200
    m_arZoom(2) = 150
    m_arZoom(3) = 125
    m_arZoom(4) = 100
    m_arZoom(5) = 75
    m_arZoom(6) = 50
    m_arZoom(7) = 25
    m_arZoom(8) = 10
    txtZoom.Text = m_arZoom(m_iZoom) & "%"

    Screen.MousePointer = vbNormal
    'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador And m_bRMat Then
        CargarGMN1Automaticamente
    End If
       
End Sub

''' <summary>
''' Mostrar/Ocultar controles dentro de un panel. Resetea los controles para mostrar ficheros
''' </summary>
''' <param name="Ver">Mostrar/Ocultar</param>
''' <param name="Quien">Que panel mostrar/ocultar</param>
''' <remarks>Llamada desde: cmdAdjuntar_Click   GenerarContrato     cmdModificar_Click      MostrarContrato
''' ; Tiempo m�ximo: 0</remarks>
Private Sub VisualizarTX(ByVal Ver As Boolean, ByVal Quien As Integer)

    If Quien = iAptContratoDoc Then
        cmdMenos.Enabled = Ver
        cmdMas.Enabled = Ver
        txtZoom.Enabled = Ver
        cmdMenu.Enabled = Ver
        cmdPDFCon.Enabled = Ver
        TXTextContrato.ResetContents
        TXTextContrato.Visible = Ver
        Me.picTextoDoc.Visible = Not Ver
    ElseIf Quien = iAptContratoPdf Then
        Me.AcroPDFControl.Visible = Ver
    Else
        PajantImagen.pi.DeleteFrames PJT_FRAME
        PajantImagen.Visible = Ver
    End If
End Sub

''' <summary>
''' Graba en bbdd, tabla PROCE_CONTR, un fichero
''' </summary>
''' <param name="sFileName">Nombre del fichero a grabar en bbdd</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: cmdAdjuntar_Click       GenerarContrato     cmdModificar_Click; Tiempo m�ximo:0,1</remarks>
Private Function CrearAdjunto(ByVal sFileName As String) As TipoErrorSummit
Dim DataFile As Integer
Dim Fl As Long
Dim Chunks As Integer
Dim Fragment As Integer
Dim Chunk() As Byte
Dim lSize As Long
Dim lInit As Long
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim imgNew As Variant
Dim RutaTemp As String

On Error GoTo Cancelar

        If m_oContratoSeleccionado Is Nothing Then
            teserror.NumError = TESOtroerror
            CrearAdjunto = teserror
            Exit Function
        End If
        teserror.NumError = TESnoerror
        Screen.MousePointer = vbHourglass
        
        If UCase(m_oContratoSeleccionado.Extension) = "OCX" Then
            m_oContratoSeleccionado.Extension = "DOC"
        ElseIf UCase(m_oContratoSeleccionado.Extension) = "PEG" Then
            m_oContratoSeleccionado.Extension = "JPG"
        ElseIf UCase(m_oContratoSeleccionado.Extension) = "IFF" Then
            m_oContratoSeleccionado.Extension = "TIF"
        End If
        
        If DevolverExtension(m_oContratoSeleccionado.Extension) = 13 Then
            If UCase(m_oContratoSeleccionado.Extension) = "GIF" Then
                RutaTemp = basUtilidades.DevolverPathFichTemp
                If basUtilidades.ConvertirGIFaJPEG(sFileName, "GIF", RutaTemp) Then
                    m_oContratoSeleccionado.Extension = "JPG"
                    sFileName = RutaTemp
                    sayFileNames(UBound(sayFileNames)) = sFileName
                    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                Else
                    Screen.MousePointer = vbNormal
                    ImposibleCargarGIF
                    teserror.NumError = TESOtroerror
                    CrearAdjunto = teserror
                    Exit Function
                End If
            ElseIf UCase(m_oContratoSeleccionado.Extension) = "PEG" Then
                m_oContratoSeleccionado.Extension = "JPG"
            ElseIf UCase(m_oContratoSeleccionado.Extension) = "IFF" Then
                m_oContratoSeleccionado.Extension = "TIF"
            End If
            
            PajantImagen.pi.Load sFileName, 0
            PajantImagen.pi.ExportTo PJT_MEMPNG, 0, 0, imgNew, PJT_IMAGE Or PJT_TONEWFRAME
            
            Chunk = imgNew
        Else
                
            DataFile = 1
            'Abrimos el fichero para lectura binaria
            Open sFileName For Binary Access Read As DataFile
            Fl = LOF(DataFile)    ' Length of data in file
            If Fl = 0 Then
                Close DataFile
                NoValido 124
                teserror.NumError = TESOtroerror
                CrearAdjunto = teserror
                Set m_oContratoSeleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Function
            End If
    
            ' Se lo asignamos en bloques
            lInit = 0
            If giChunkSize > Fl Then
                lSize = Fl
            Else
                lSize = giChunkSize
            End If
            ReDim Chunk(lSize - 1)
            Get DataFile, , Chunk()
        End If
        
        teserror = m_oContratoSeleccionado.ComenzarEscrituraData(lInit, Chunk)
        If teserror.NumError <> TESnoerror Then
            CrearAdjunto = teserror
            Set m_oContratoSeleccionado = Nothing
            Close DataFile
            Screen.MousePointer = vbNormal
            Exit Function
        End If
        lInit = lInit + lSize
        While lInit < Fl
            If lInit + giChunkSize > Fl Then
                lSize = Fl - lInit
            End If
            ReDim Chunk(lSize - 1)
            Get DataFile, , Chunk()
            teserror = m_oContratoSeleccionado.WriteData(lInit, Chunk)
            If teserror.NumError <> TESnoerror Then
                CrearAdjunto = teserror
                Set m_oContratoSeleccionado = Nothing
                Close DataFile
                Screen.MousePointer = vbNormal
                Exit Function
            End If
            lInit = lInit + lSize
        Wend
        teserror = m_oContratoSeleccionado.FinalizarEscrituraData(Fl)
        If teserror.NumError <> TESnoerror Then
            CrearAdjunto = teserror
            Set m_oContratoSeleccionado = Nothing
            Close DataFile
            Screen.MousePointer = vbNormal
            Exit Function
        End If

        Close DataFile

        CrearAdjunto = teserror
        Screen.MousePointer = vbNormal
        
    Exit Function

Cancelar:
    
    If Err.Number <> 32755 Then
        Close DataFile
        teserror.NumError = TESOtroerror
        CrearAdjunto = teserror
        Set m_oContratoSeleccionado = Nothing
    Else
        MsgBox Err.Description, vbCritical, "FULLSTEP GS"
        Close DataFile
    End If
    Screen.MousePointer = vbNormal
End Function

''' <summary>
''' Graba en bbdd, tabla PROCE_CONTR, un fichero
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdAdjuntar_Click()
Dim nodx As MSComctlLib.Node
Dim nodp As MSComctlLib.Node
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim vdata As Variant

On Error GoTo Error

        If g_oProveSeleccionado Is Nothing Then Exit Sub
        cmmdAdjun.DialogTitle = m_sIdiSelecAdjunto
        cmmdAdjun.CancelError = True
        cmmdAdjun.filename = ""
        cmmdAdjun.Filter = m_sIdiTodosArchivos & "|*.*"
        cmmdAdjun.ShowOpen
        
        If cmmdAdjun.FileTitle <> "" Then
            frmCONTRDatos.caption = m_sIdiAdjuntarTitle
            frmCONTRDatos.lblCant.caption = m_sIdiNombre
            frmCONTRDatos.cmdAceptar.caption = m_sIdiAceptar
            frmCONTRDatos.cmdCancelar.caption = m_sIdiCancelar
            frmCONTRDatos.Show 1
            Screen.MousePointer = vbHourglass
            If g_sNombreNuevo <> "" Then
                Set m_oContratoSeleccionado = oFSGSRaiz.Generar_CContrato
                m_oContratoSeleccionado.Extension = UCase(Right(cmmdAdjun.FileTitle, 3))
                m_oContratoSeleccionado.nombre = g_sNombreNuevo
                m_oContratoSeleccionado.Anyo = g_oProcesoSeleccionado.Anyo
                m_oContratoSeleccionado.GMN1Cod = g_oProcesoSeleccionado.GMN1Cod
                m_oContratoSeleccionado.Proce = g_oProcesoSeleccionado.Cod
                m_oContratoSeleccionado.Prove = g_oProveSeleccionado.Cod
                m_oContratoSeleccionado.Usuario = basOptimizacion.gvarCodUsuario
                m_oContratoSeleccionado.Id = Null
                teserror = CrearAdjunto(cmmdAdjun.filename)
                If teserror.NumError <> TESnoerror Then
                    If teserror.NumError <> TESOtroerror Then
                        TratarError teserror
                    End If
                    Set m_oContratoSeleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                basSeguridad.RegistrarAccion accionessummit.ACCContratosAnya, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & g_oProveSeleccionado.Cod & "Contrato:" & m_oContratoSeleccionado.nombre
                If g_oProveSeleccionado.Contratos Is Nothing Then
                    Set g_oProveSeleccionado.Contratos = oFSGSRaiz.Generar_CContratos
                End If
                g_oProveSeleccionado.Contratos.Add m_oContratoSeleccionado.Id, m_oContratoSeleccionado.nombre, m_oContratoSeleccionado.Extension, m_oContratoSeleccionado.Anyo, m_oContratoSeleccionado.GMN1Cod, m_oContratoSeleccionado.Proce, m_oContratoSeleccionado.Prove, m_oContratoSeleccionado.Usuario, m_oContratoSeleccionado.dataSize, m_oContratoSeleccionado.FECACT
                                
                Me.lblContratoDoc.caption = m_oContratoSeleccionado.nombre & " - " & Format(m_oContratoSeleccionado.FECACT, "Short date")
                Me.lblContratoPdf.caption = Me.lblContratoDoc.caption
                Me.lblContratoImg.caption = Me.lblContratoDoc.caption
                
                Set nodp = tvwProce.selectedItem
                Set nodx = tvwProce.Nodes.Add(nodp.key, tvwChild, "C_" & CStr(m_oContratoSeleccionado.Id), m_oContratoSeleccionado.nombre, "Contrato")
                nodx.Tag = "CO_" & CStr(m_oContratoSeleccionado.Id)
                nodx.Selected = True
                
                i = DevolverExtension(m_oContratoSeleccionado.Extension)
                If i = 0 Then
                    MostrarApartado iAptContratoDoc
                    VisualizarTX False, iAptContratoDoc
                ElseIf i = 12 Then
                    MostrarApartado iAptContratoPdf
                    VisualizarTX True, iAptContratoPdf
                    'Salvar a disco y Usar el AcroPdf
                    AbrirFicheroPDF SalvarDisco
                    vdata = Null
                ElseIf i = 13 Then
                    MostrarApartado iAptContratoImg
                    VisualizarTX True, iAptContratoImg
                    vdata = m_oContratoSeleccionado.LoadContrato
                    AbrirFicheroImagen vdata
                    vdata = Null
                Else
                    MostrarApartado iAptContratoDoc
                    VisualizarTX True, iAptContratoDoc
                    AbrirFichero cmmdAdjun.filename, i
                End If
            End If
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
Error:

    If Err.Number = cdlCancel Then
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        MsgBox Err.Description, vbCritical, "FULLSTEP GS"
        Screen.MousePointer = vbNormal
    End If
End Sub

''' <summary>
''' Generar un contrato para el proceso en curso
''' </summary>
''' <param name="sFileName">Nombre del fichero a grabar en bbdd</param>
''' <remarks>Llamada desde: frmContrWizard/WizardContrato; Tiempo m�ximo:0,3</remarks>
Public Sub GenerarContrato(ByVal sFileName As String)
Dim teserror As TipoErrorSummit
Dim nodx As MSComctlLib.Node
Dim nodp As MSComctlLib.Node


On Error GoTo Error

    Screen.MousePointer = vbHourglass
    
    Set m_oContratoSeleccionado = oFSGSRaiz.Generar_CContrato
    m_oContratoSeleccionado.Id = Null
    m_oContratoSeleccionado.Extension = "doc"
    m_oContratoSeleccionado.nombre = frmCONTRWizard.txtNom.Text
    m_oContratoSeleccionado.Anyo = frmCONTRATOS.g_oProcesoSeleccionado.Anyo
    m_oContratoSeleccionado.GMN1Cod = frmCONTRATOS.g_oProcesoSeleccionado.GMN1Cod
    m_oContratoSeleccionado.Proce = frmCONTRATOS.g_oProcesoSeleccionado.Cod
    m_oContratoSeleccionado.Prove = frmCONTRATOS.g_oProveSeleccionado.Cod
    m_oContratoSeleccionado.Usuario = basOptimizacion.gvarCodUsuario

    sayFileNames(UBound(sayFileNames)) = sFileName
    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)

    teserror = CrearAdjunto(sFileName)
    If teserror.NumError <> TESnoerror Then
        If teserror.NumError <> TESOtroerror Then
            TratarError teserror
        End If
        Set m_oContratoSeleccionado = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    basSeguridad.RegistrarAccion accionessummit.ACCContratosAnya, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & g_oProveSeleccionado.Cod & "Contrato:" & m_oContratoSeleccionado.nombre
    If g_oProveSeleccionado.Contratos Is Nothing Then
        Set g_oProveSeleccionado.Contratos = oFSGSRaiz.Generar_CContratos
    End If
    g_oProveSeleccionado.Contratos.Add m_oContratoSeleccionado.Id, m_oContratoSeleccionado.nombre, m_oContratoSeleccionado.Extension, m_oContratoSeleccionado.Anyo, m_oContratoSeleccionado.GMN1Cod, m_oContratoSeleccionado.Proce, m_oContratoSeleccionado.Prove, m_oContratoSeleccionado.Usuario, m_oContratoSeleccionado.dataSize, m_oContratoSeleccionado.FECACT
                    
    lblContratoDoc.caption = m_oContratoSeleccionado.nombre & " - " & Format(m_oContratoSeleccionado.FECACT, "Short date")
    Me.lblContratoPdf.caption = Me.lblContratoDoc.caption
    Me.lblContratoImg.caption = Me.lblContratoDoc.caption
    'Lo crea como doc. Ni en pdf ni en imagen
    
    Set nodp = tvwProce.selectedItem
    Set nodx = tvwProce.Nodes.Add(nodp.key, tvwChild, "C_" & CStr(m_oContratoSeleccionado.Id), m_oContratoSeleccionado.nombre, "Contrato")
    nodx.Tag = "CO_" & CStr(m_oContratoSeleccionado.Id)
    nodx.Selected = True
    MostrarApartado iAptContratoDoc
    
    VisualizarTX True, iAptContratoDoc
    AbrirFichero sFileName, 9
    Screen.MousePointer = vbNormal
    Exit Sub
Error:

    MsgBox Err.Description, vbCritical, "FULLSTEP GS"
    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' Determina el codigo q necesita TXTText para mostrarlo. Si es pdf o imagen se usa otro control, se
''' distingue entre pdf e imagen por el codigo devuelto.
''' </summary>
''' <param name="sExten">Extensi�n como texto</param>
''' <returns>Codigo de extensi�n</returns>
''' <remarks>Llamada desde: cmdAdjuntar_Click   cmdModificar_Click      MostrarContrato     cmdImprimir_Click
''' CrearAdjunto        ; Tiempo m�ximo: 0</remarks>
Private Function DevolverExtension(ByVal sExten As String) As Integer
    Dim i As Integer
    
    Select Case UCase(sExten)
    Case "TXT"
        DevolverExtension = 2
    Case "DOC", "DOCX"
        DevolverExtension = 9
    Case "RTF"
        DevolverExtension = 5
    'Case "HTML", "HTM"
     '   DevolverExtension = 4
    Case "PDF"
        DevolverExtension = 12
    Case "JPEG", "JPG", "TIFF", "TIF", "GIF", "BMP", "MNG", "PNG"
        DevolverExtension = 13
    Case Else
        DevolverExtension = 0
    End Select
End Function

Private Sub cmdCrear_Click()
    AnyadirContrato 0
End Sub

Public Sub AnyadirContrato(i As Integer)
Dim sNombre As String

    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    If i = 0 Then
        If Not g_oProveSeleccionado.Contratos Is Nothing Then
            sNombre = ComprobarNombreContrato(sdbcAnyo.Value & "_" & sdbcGMN1_4Cod.Value & "_" & sdbcProceCod.Value & "_" & g_oProveSeleccionado.Cod, 1)
        Else
            sNombre = sdbcAnyo.Value & "_" & sdbcGMN1_4Cod.Value & "_" & sdbcProceCod.Value & "_" & g_oProveSeleccionado.Cod
        End If
        frmCONTRWizard.txtNom.Text = sNombre
        frmCONTRWizard.Show
        Exit Sub
    End If
    If i = 1 Then
        cmdAdjuntar_Click
        Exit Sub
    End If
End Sub

Private Function ComprobarNombreContrato(ByVal sNombre As String, ByVal iIndice As Integer)
Dim oContr As CContrato
Dim sContrato As String

    If iIndice = 1 Then
        sContrato = sNombre
    Else
        sContrato = sNombre & "_" & iIndice
    End If
    
    For Each oContr In g_oProveSeleccionado.Contratos
        If UCase(oContr.nombre) = UCase(sContrato) Then
            sContrato = ComprobarNombreContrato(sNombre, iIndice + 1)
        End If
    Next

    ComprobarNombreContrato = sContrato
End Function

Private Sub cmdEliminar_Click()
 Dim i As Integer
 Dim teserror As TipoErrorSummit
 
    If g_oProveSeleccionado Is Nothing Then Exit Sub
    If m_oContratoSeleccionado Is Nothing Then Exit Sub
    i = basMensajes.PreguntaEliminar(m_sIdiElContrato & ": " & tvwProce.selectedItem.Text)

    If i = vbYes Then
        Screen.MousePointer = vbHourglass
        Set m_oIBaseDatos = m_oContratoSeleccionado
        teserror = m_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        RegistrarAccion accionessummit.ACCContratosEli, "Anyo:" & sdbcAnyo.Value & "GMN1:" & sdbcGMN1_4Cod.Value & "Proce:" & sdbcProceCod.Value & "Prove:" & g_oProveSeleccionado.Cod & "Contr:" & m_oContratoSeleccionado.nombre
        g_oProveSeleccionado.Contratos.Remove CStr(m_oContratoSeleccionado.Id)
        Set m_oContratoSeleccionado = Nothing
        QuitarContratoDeTree
        Screen.MousePointer = vbNormal
    End If
    
End Sub

Private Sub QuitarContratoDeTree()
Dim nodx As MSComctlLib.Node
Dim nody As MSComctlLib.Node

Set nodx = tvwProce.selectedItem
Set nody = nodx.Parent

tvwProce.Nodes.Remove nodx.Index

tvwProce_NodeClick nody

Set nodx = Nothing
Set nody = Nothing

End Sub

''' <summary>
''' Sustituye el fichero q se esta mostrando actualmente en la pantalla contratos.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdModificar_Click()
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim vdata As Variant

    On Error GoTo Error

        cmmdAdjun.DialogTitle = m_sIdiSelecAdjunto
        cmmdAdjun.CancelError = True
        cmmdAdjun.filename = ""
        cmmdAdjun.Filter = m_sIdiTodosArchivos & "|*.*"
        cmmdAdjun.ShowOpen

        If cmmdAdjun.FileTitle <> "" Then
            
            frmCONTRDatos.caption = m_sIdiAdjuntarTitle
            frmCONTRDatos.lblCant.caption = m_sIdiNombre
            frmCONTRDatos.cmdAceptar.caption = m_sIdiAceptar
            frmCONTRDatos.cmdCancelar.caption = m_sIdiCancelar
            frmCONTRDatos.Show 1
            
            Screen.MousePointer = vbHourglass
            
            If g_sNombreNuevo <> "" Then
                m_oContratoSeleccionado.Extension = Right(cmmdAdjun.FileTitle, 3)
                m_oContratoSeleccionado.nombre = g_sNombreNuevo
                m_oContratoSeleccionado.Usuario = basOptimizacion.gvarCodUsuario
                teserror = CrearAdjunto(cmmdAdjun.filename)
                If teserror.NumError <> TESnoerror Then
                    If teserror.NumError <> TESOtroerror Then
                        TratarError teserror
                    End If
                    Set m_oContratoSeleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                basSeguridad.RegistrarAccion accionessummit.ACCContratosMod, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & g_oProveSeleccionado.Cod & "Contrato:" & m_oContratoSeleccionado.nombre
                                
                lblContratoDoc.caption = m_oContratoSeleccionado.nombre & " - " & Format(m_oContratoSeleccionado.FECACT, "Short date")
                lblContratoPdf.caption = lblContratoDoc.caption
                lblContratoImg.caption = lblContratoDoc.caption
                
                RestaurarArbol 2
                
                i = DevolverExtension(m_oContratoSeleccionado.Extension)
                If i = 0 Then
                    MostrarApartado iAptContratoDoc
                    VisualizarTX False, iAptContratoDoc
                ElseIf i = 12 Then
                    MostrarApartado iAptContratoPdf
                    VisualizarTX True, iAptContratoPdf
                    'Salvar a disco y Usar el AcroPdf
                    AbrirFicheroPDF SalvarDisco
                    vdata = Null
                ElseIf i = 13 Then
                    MostrarApartado iAptContratoImg
                    VisualizarTX True, iAptContratoImg
                    vdata = m_oContratoSeleccionado.LoadContrato
                    AbrirFicheroImagen vdata
                    vdata = Null
                Else
                    MostrarApartado iAptContratoDoc
                    VisualizarTX True, iAptContratoDoc
                    AbrirFichero cmmdAdjun.filename, i
                End If
            End If
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
Error:
    If Err.Number = cdlCancel Then
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        MsgBox Err.Description, vbCritical, "FULLSTEP GS"
        Screen.MousePointer = vbNormal
    End If
End Sub
Private Sub AbrirFichero(ByVal filename As String, ByVal FileExtension As Integer, Optional ByVal Data As Variant)

On Error GoTo Error
    
    With TXTextContrato
        .ViewMode = 1
        .ZoomFactor = m_arZoom(m_iZoom)
        .ScrollBars = 3
        .FontBold = 0
        .FontItalic = 0
        .FontStrikethru = 0
        .FontUnderline = 0
        .PageWidth = 11906
        .PageHeight = 16838
        .LoadSaveAttribute(txLoadImages) = True
        
        If Not IsMissing(Data) Then
            .LoadFromMemory Data, FileExtension
        Else
            .Load filename, , FileExtension
        End If
        If .LoadSaveAttribute(txDocWidth) > 0 Then
            .PageWidth = .LoadSaveAttribute(txDocWidth)
        Else
            .PageWidth = 11906
        End If
        If .LoadSaveAttribute(txDocHeight) > 0 Then
            .PageHeight = .LoadSaveAttribute(txDocHeight)
        Else
            .PageHeight = 16838
        End If
        If .LoadSaveAttribute(txDocLeftMargin) > 0 Then .PageMarginL = .LoadSaveAttribute(txDocLeftMargin)
        If .LoadSaveAttribute(txDocTopMargin) > 0 Then .PageMarginT = .LoadSaveAttribute(txDocTopMargin)
        If .LoadSaveAttribute(txDocRightMargin) > 0 Then .PageMarginR = .LoadSaveAttribute(txDocRightMargin)
        If .LoadSaveAttribute(txDocBottomMargin) > 0 Then .PageMarginB = .LoadSaveAttribute(txDocBottomMargin)

    End With
    Exit Sub
    
Error:
        MsgBox Err.Description, vbCritical, "FULLSTEP GS"
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim i As Integer
Dim FOSFile As Scripting.FileSystemObject
Dim bBorrando As Boolean
    
Set m_oProcesos = Nothing
Set m_oGruposMN1 = Nothing
Set m_oGMN1Seleccionado = Nothing
Set g_oProcesoSeleccionado = Nothing
Set g_oProveSeleccionado = Nothing
Set m_oContratoSeleccionado = Nothing
Set m_oIBaseDatos = Nothing
    
Unload frmCONTRWizard
Unload frmCONTRWizardGru
Unload frmCONTRWizardItem

On Error GoTo Error
    Set FOSFile = New Scripting.FileSystemObject
    
    i = 0
    While i < UBound(sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(sayFileNames(i)) Then
            FOSFile.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    Me.Visible = False
    Exit Sub

Error:
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
        
End Sub

Private Sub sdbcAnyo_Click()
    'Lo hace todo el change de proce cod
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Text = ""
    sdbcProceCod.Columns(1).Text = ""
    sdbcProceCod.RemoveAll
End Sub

''' <summary>
''' Redimensionar el spliter
''' </summary>
''' <param name="BorderPanes">paneles del spliter</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub SSSplitter1_Resize(ByVal BorderPanes As SSSplitter.Panes)
    Me.tvwProce.Width = Me.SSSplitter1.Panes(0).Width
    
    Me.picApartado(0).Width = Me.SSSplitter1.Panes(1).Width - 150
    lblDatosGenProce.Width = picApartado(0).Width - 70
    
    Me.picApartado(1).Width = Me.picApartado(0).Width
    Label11.Width = lblDatosGenProce.Width + 20
    
    Me.picApartado(2).Width = Me.picApartado(0).Width
    If picApartado(2).Width > Me.picToolBarDoc.Width + 50 Then
        Me.lblContratoDoc.Width = picApartado(2).Width - picToolBarDoc.Width - 50
    End If
    picToolBarDoc.Left = lblContratoDoc.Width
    TXTextContrato.Width = Me.picApartado(2).Width - 90
    picTextoDoc.Width = TXTextContrato.Width
    Me.lblNoVisualizable.Left = (picTextoDoc.Width - Me.lblNoVisualizable.Width) / 2
    Me.lblVisualizable.Left = Me.lblNoVisualizable.Left
    Me.lblNoVisualizable.Top = (picTextoDoc.Height - Me.lblNoVisualizable.Height - 2000) / 2
    Me.lblVisualizable.Top = Me.lblNoVisualizable.Top + 400
    
    Me.picApartado(3).Width = Me.picApartado(0).Width
    lblContratoPdf.Width = lblDatosGenProce.Width
    AcroPDFControl.Width = lblDatosGenProce.Width
    
    Me.picApartado(4).Width = Me.picApartado(0).Width
    If picApartado(4).Width > Me.picToolBarImg.Width + 50 Then
        Me.lblContratoImg.Width = picApartado(4).Width - picToolBarImg.Width - 50
    End If
    picToolBarImg.Left = lblContratoImg.Width
    PajantImagen.Width = Me.picApartado(4).Width - 90

End Sub


Private Sub tvwProce_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Node As MSComctlLib.Node

    If Button = 2 Then
        If m_bCrear Then
            Set Node = tvwProce.selectedItem
            If Not Node Is Nothing Then
                If Left(Node.Tag, 2) = "PV" Then
                    PopupMenu MDI.mnuPOPUPContratos
                End If
            End If
        End If
    End If
    Set Node = Nothing
End Sub

Public Sub tvwProce_NodeClick(ByVal Node As MSComctlLib.Node)
Dim sTag As String
Dim sCod As String

If g_oProcesoSeleccionado Is Nothing Then Exit Sub

Screen.MousePointer = vbHourglass

sTag = Left(Node.Tag, 2)

Select Case sTag
Case "RA"
    Set g_oProveSeleccionado = Nothing
    Set m_oContratoSeleccionado = Nothing
    TXTextContrato.ResetContents
    MostrarDatosGenerales
Case "PV"
    sCod = Right(Node.Tag, Len(Node.Tag) - 3)
    Set g_oProveSeleccionado = m_oProves.Item(sCod)
    Set m_oContratoSeleccionado = Nothing
    If g_oProveSeleccionado.Contactos Is Nothing Then
        m_oProves.CargarDatosProveedor sCod
        g_oProveSeleccionado.CargarTodosLosContactos , , , , True
    End If
    TXTextContrato.ResetContents
    MostrarProveedor
    
Case "CO"
    sCod = Right(Node.Parent.Tag, Len(Node.Parent.Tag) - 3)
    If Not g_oProveSeleccionado Is Nothing Then
        If g_oProveSeleccionado.Cod = m_oProves.Item(sCod).Cod Then
            If Not m_oContratoSeleccionado Is Nothing Then
                sCod = Right(Node.Tag, Len(Node.Tag) - 3)
                If m_oContratoSeleccionado.nombre = g_oProveSeleccionado.Contratos.Item(sCod).nombre Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
        End If
    End If
    sCod = Right(Node.Parent.Tag, Len(Node.Parent.Tag) - 3)
    Set g_oProveSeleccionado = m_oProves.Item(sCod)
    sCod = Right(Node.Tag, Len(Node.Tag) - 3)
    Set m_oContratoSeleccionado = g_oProveSeleccionado.Contratos.Item(sCod)
    MostrarContrato

End Select


Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Muestra en su panel y control correspondiente el contrato seleccionado
''' </summary>
''' <remarks>Llamada desde: tvwProce_NodeClick      cmdRestaurar_Click; Tiempo m�ximo: 0,1</remarks>
Private Sub MostrarContrato()
    Dim i As Integer
    Dim vdata As Variant

    If g_oProveSeleccionado Is Nothing Then Exit Sub
    If m_oContratoSeleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
        
    m_iZoom = 4
    m_bRespetarTxt = True
    txtZoom.Text = m_arZoom(m_iZoom) & "%"
    
    lblContratoDoc.caption = m_oContratoSeleccionado.nombre & " - " & Format(m_oContratoSeleccionado.FECACT, "Short date")
    lblContratoPdf.caption = lblContratoDoc.caption
    lblContratoImg.caption = lblContratoDoc.caption
    
    i = DevolverExtension(m_oContratoSeleccionado.Extension)
    If i = 0 Then
        MostrarApartado iAptContratoDoc
        VisualizarTX False, iAptContratoDoc
    ElseIf i = 12 Then
        MostrarApartado iAptContratoPdf
        VisualizarTX True, iAptContratoPdf
        'Salvar a disco y Usar el AcroPdf
        AbrirFicheroPDF SalvarDisco
        vdata = Null
    ElseIf i = 13 Then
        MostrarApartado iAptContratoImg
        VisualizarTX True, iAptContratoImg
        vdata = m_oContratoSeleccionado.LoadContrato
        AbrirFicheroImagen vdata
        vdata = Null
    Else
        MostrarApartado iAptContratoDoc
        VisualizarTX True, iAptContratoDoc
        vdata = m_oContratoSeleccionado.LoadContrato
        AbrirFichero "", i, vdata
        vdata = Null
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub txtZoom_Validate(Cancel As Boolean)
Dim sZoom As String
Dim i As Integer
Dim iAprox As Integer

'If m_bRespetarTxt Then Exit Sub
If txtZoom.Text = "" Then Exit Sub
i = InStr(1, txtZoom.Text, "%")
If i <> 0 Then
    sZoom = Left(txtZoom.Text, i - 1)
    sZoom = sZoom & Right(txtZoom.Text, Len(txtZoom.Text) - i)
Else
    sZoom = txtZoom.Text
End If

If IsNumeric(sZoom) Then
    cmdMas.Enabled = True
    cmdMenos.Enabled = True
    If CDbl(sZoom) >= 10 And CDbl(sZoom) <= 400 Then
        TXTextContrato.ZoomFactor = CDbl(sZoom)
        txtZoom.Text = sZoom & "%"
        iAprox = 8
        m_iZoom = 1000
        For i = 8 To 0 Step -1
            If m_arZoom(i) = CInt(sZoom) Then
                m_iZoom = i
                MDI.mnuZoom.Item(i).Checked = True
            Else
                If CInt(sZoom) > m_arZoom(i) And m_arZoom(i) >= m_arZoom(iAprox) Then
                    iAprox = i
                Else
                    If (m_arZoom(i) - CInt(sZoom)) < (CInt(sZoom) - m_arZoom(iAprox)) And m_iZoom = 1000 Then
                        m_iZoom = i
                    Else
                        m_iZoom = iAprox
                    End If
                End If
                MDI.mnuZoom.Item(i).Checked = False
            End If
        Next
        If CInt(sZoom) = 10 Then cmdMenos.Enabled = False: cmdMas.Enabled = True
        If CInt(sZoom) = 400 Then cmdMenos.Enabled = True: cmdMas.Enabled = False
    Else
        MsgBox m_sIdiZoom, vbInformation, "FULLSTEP"
        Exit Sub
    End If
Else
    txtZoom.Text = ""
End If
m_bRespetarTxt = False

End Sub
Private Sub LimpiarProce()
txtMat.Text = ""
lstMaterial.Clear
lblFecApe(1).caption = ""
lblFecPres(1).caption = ""
lblFecLimit(1).caption = ""
lblFecNec(1).caption = ""
lblFecAdj(1).caption = ""
chkPermAdjDir.Value = vbUnchecked
lblPresupuesto(1).caption = ""
lblMon(1).caption = ""
lblSolicitud(1).caption = ""
lblCambio(1).caption = ""
lblFecIni(1).caption = ""
lblFecFin(1).caption = ""
lblDest(1).caption = ""
lblDest(2).caption = ""
lblPago(1).caption = ""
lblPago(2).caption = ""
lblProveProce(1).caption = ""
lblProveProce(2).caption = ""
LimpiarTreeViewProce
End Sub
Private Sub LimpiarTreeViewProce()
Dim NodeRaiz As MSComctlLib.Node

tvwProce.Nodes.Clear
Set NodeRaiz = tvwProce.Nodes.Add(, , "Raiz", m_sIdiSelProceso, "Proceso")
NodeRaiz.Expanded = True
NodeRaiz.Tag = "RA"
Set NodeRaiz = Nothing
End Sub

Private Sub LimpiarProve()
lblPortal.caption = ""
lblDir.caption = ""
lblCp.caption = ""
lblPob.caption = ""
lblPaiCod.caption = ""
lblPaiDen.caption = ""
lblProviCod.caption = ""
lblProviDen.caption = ""
lblMonCod.caption = ""
lblMonDen.caption = ""
lblURL.caption = ""
sdbgContactos.RemoveAll
lblCal1Den.caption = ""
lblcal2den.caption = ""
lblcal3den.caption = ""
lblCal1Val.caption = ""
lblCal2Val.caption = ""
lblCal3Val.caption = ""
txtObsProve.Text = ""
End Sub

''' <summary>
''' Inicializar los controles de pantalla
''' </summary>
''' <remarks>Llamada desde: ProcesoSeleccionado     CargarProcesoConBusqueda        sdbcProceCod_Change
''' sdbcProceDen_Change     Form_Load; Tiempo m�ximo: 0</remarks>
Private Sub LimpiarCampos()
    
    LimpiarProce
    LimpiarProve
    cmdRestaurar(0).Enabled = False
    TXTextContrato.ResetContents
    'Me.AcroPDFControl.resetear
    Me.PajantImagen.pi.DeleteFrames PJT_FRAME
    MostrarApartado iAptDatosGenerales
    cmdResponsable.ToolTipText = ""
    cmdResponsable.Enabled = False
End Sub

Private Sub ProcesoSeleccionado()
Dim sCod As String
Dim oIAdjs  As IAdjudicaciones

    Screen.MousePointer = vbHourglass
    
    sCod = sdbcGMN1_4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod))
    sCod = CStr(sdbcAnyo) & sCod & sdbcProceCod
       
    
    Set g_oProcesoSeleccionado = oFSGSRaiz.Generar_CProceso
    
    g_oProcesoSeleccionado.Anyo = sdbcAnyo.Value
    g_oProcesoSeleccionado.GMN1Cod = sdbcGMN1_4Cod.Value
    g_oProcesoSeleccionado.Cod = sdbcProceCod.Value
    If sdbcProceCod.Columns("INVI").Value = "" Then
        g_oProcesoSeleccionado.Invitado = GridCheckToBoolean(sdbcProceDen.Columns("INVI").Value)
    Else
        g_oProcesoSeleccionado.Invitado = GridCheckToBoolean(sdbcProceCod.Columns("INVI").Value)
    End If
    
    'Datos del proceso
    g_oProcesoSeleccionado.CargarDatosGeneralesProceso bSinpres:=True, bSinGRupos:=True
    
    LimpiarCampos
    
    If g_oProcesoSeleccionado.Den = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oIAdjs = g_oProcesoSeleccionado
    If oIAdjs Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If g_oProcesoSeleccionado.Invitado Then
        m_bMod = False
        m_bCrear = False
        m_bEli = False
        m_bModifResponsable = False
        ConfigurarBotones
    Else
        ConfigurarSeguridad
    End If
    
    'Cargamos los proveedores asignados al proceso con adjudicaciones
    If m_bRContrAsigComp Then
        Set m_oProves = oIAdjs.DevolverProveedoresConAdjudicaciones(False, gCodEqpUsuario, gCodCompradorUsuario)
    Else
        If m_bRContrAsigEqp Then
            Set m_oProves = oIAdjs.DevolverProveedoresConAdjudicaciones(False, gCodEqpUsuario)
        Else
            Set m_oProves = oIAdjs.DevolverProveedoresConAdjudicaciones
        End If
    End If
    If m_oProves Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    'Cargamos los contratos del proceso
    If m_bRContrUsu Then
        m_oProves.CargarContratosProce g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, basOptimizacion.gvarCodUsuario
    Else
        m_oProves.CargarContratosProce g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
    End If
    Set oIAdjs = Nothing
    
   
    
    CargarTreeViewProce
    
    MostrarDatosGenerales
    
    cmdRestaurar(0).Enabled = True

    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarTreeViewProce()
'***************************************************************************************
' Carga el �rbol del proceso seg�n su configuraci�n.
'***************************************************************************************

Dim NodeRaiz As MSComctlLib.Node
Dim NodeProveedor As MSComctlLib.Node
Dim NodeContrato As MSComctlLib.Node
Dim oProve As CProveedor
Dim oContrato As CContrato
Dim sCod As String

    Screen.MousePointer = vbHourglass
    tvwProce.Nodes.Clear
    
    Set NodeRaiz = tvwProce.Nodes.Add(, , "Raiz", m_sIdiProceso, "Proceso")
    NodeRaiz.Expanded = True
    NodeRaiz.Tag = "RA"


    For Each oProve In m_oProves
        'Proveedor
        Set NodeProveedor = tvwProce.Nodes.Add("Raiz", tvwChild, "P_" & oProve.Cod, oProve.Cod & "-" & oProve.Den, "Prove")
        NodeProveedor.Tag = "PV_" & oProve.Cod
        If Not oProve.Contratos Is Nothing Then
            For Each oContrato In oProve.Contratos
                'Contrato
                Set NodeContrato = tvwProce.Nodes.Add(NodeProveedor.key, tvwChild, "C_" & CStr(oContrato.Id), oContrato.nombre, "Contrato")
                NodeContrato.Tag = "CO_" & CStr(oContrato.Id)
            Next
        End If
    Next

    Set NodeProveedor = Nothing
    Set NodeRaiz = Nothing
    Set NodeContrato = Nothing
    Screen.MousePointer = vbNormal

End Sub
Private Sub RestaurarArbol(ByVal Index As Integer)
'***************************************************************************************
' Carga el �rbol del proceso seg�n su configuraci�n.
'***************************************************************************************

Dim NodeProveedor As MSComctlLib.Node
Dim NodeContrato As MSComctlLib.Node
Dim oContrato As CContrato
Dim sCod As String

Select Case Index
Case 1
    While tvwProce.Nodes.Item("P_" & g_oProveSeleccionado.Cod).Children > 0
       tvwProce.Nodes.Remove (tvwProce.Nodes.Item("P_" & g_oProveSeleccionado.Cod).Child.Index)
    Wend
    
    'Proveedor
    Set NodeProveedor = tvwProce.Nodes.Item("P_" & g_oProveSeleccionado.Cod)
    If Not g_oProveSeleccionado.Contratos Is Nothing Then
        For Each oContrato In g_oProveSeleccionado.Contratos
            'Contrato
            Set NodeContrato = tvwProce.Nodes.Add(NodeProveedor.key, tvwChild, "C_" & CStr(oContrato.Id), oContrato.nombre, "Contrato")
            NodeContrato.Tag = "CO_" & CStr(oContrato.Id)
        Next
    End If

    Set NodeProveedor = Nothing
    Set NodeContrato = Nothing
Case 2
    tvwProce.Nodes.Item("C_" & CStr(m_oContratoSeleccionado.Id)).Text = m_oContratoSeleccionado.nombre
End Select
End Sub

Private Sub MostrarDatosGenerales()
Dim oMat As CGrupoMatNivel4

lblFecIni(0).Visible = True
lblFecIni(1).Visible = True
lblFecFin(0).Visible = True
lblFecFin(1).Visible = True
lblDest(0).Visible = True
lblDest(1).Visible = True
lblDest(2).Visible = True
lblPago(0).Visible = True
lblPago(1).Visible = True
lblPago(2).Visible = True
lblProveProce(0).Visible = True
lblProveProce(1).Visible = True
lblProveProce(2).Visible = True
If gParametrosGenerales.gbSolicitudesCompras Then
    lblRequest(0).Visible = True
    lblRequest(1).Visible = True
Else
    lblRequest(0).Visible = False
    lblRequest(1).Visible = False
End If
lblFecIni(0).Top = 3735
lblFecIni(1).Top = 3735
lblFecFin(0).Top = 3735
lblFecFin(1).Top = 3735
lblDest(0).Top = 4230
lblDest(1).Top = 4230
lblDest(2).Top = 4230
lblPago(0).Top = 4710
lblPago(1).Top = 4710
lblPago(2).Top = 4710
lblProveProce(0).Top = 5190
lblProveProce(1).Top = 5190
lblProveProce(2).Top = 5190
lblRequest(0).Top = 5670
lblRequest(1).Top = 5670


With g_oProcesoSeleccionado
    If Not .MaterialProce Is Nothing Then
        If .MaterialProce.Count = 1 Then
            txtMat.Text = .MaterialProce.Item(1).GMN1Cod & "-" & .MaterialProce.Item(1).GMN2Cod & "-" & .MaterialProce.Item(1).GMN3Cod & "-" & .MaterialProce.Item(1).Cod & "-" & .MaterialProce.Item(1).Den
            lstMaterial.Visible = False
            txtMat.Visible = True
        Else
            txtMat.Visible = False
            lstMaterial.Visible = True
            For Each oMat In .MaterialProce
                lstMaterial.AddItem oMat.GMN1Cod & "-" & oMat.GMN2Cod & "-" & oMat.GMN3Cod & "-" & oMat.Cod & "-" & oMat.Den
            Next
        End If
    End If
    lblFecApe(1).caption = NullToStr(.FechaApertura)
    lblFecNec(1).caption = NullToStr(.FechaNecesidad)
    lblFecPres(1).caption = NullToStr(.FechaPresentacion)
    lblFecAdj(1).caption = NullToStr(.FechaCierre)
    If .PermitirAdjDirecta Then
        chkPermAdjDir.Value = vbChecked
    Else
        chkPermAdjDir.Value = vbUnchecked
    End If
    lblPresupuesto(1).caption = DblToStr(.PresGlobal)
    lblSolicitud(1).caption = NullToStr(.Referencia)
    lblMon(1).caption = NullToStr(.MonCod)
    lblCambio(1).caption = FormateoNumerico(.Cambio)
    lblFecLimit(1).caption = NullToStr(.FechaMinimoLimOfertas)
    
    'Muestra el responsable en el tooltiptext del bot�n de responsable
    cmdResponsable.Enabled = True
    If Not .responsable Is Nothing Then
        If .responsable.Cod <> "" Then
            If .responsable.nombre <> "" Then
                cmdResponsable.ToolTipText = .responsable.nombre & " " & .responsable.Apel
            Else
                cmdResponsable.ToolTipText = .responsable.Apel
            End If
        End If
    End If
    
    If .DefFechasSum = EnProceso And .DefDestino = EnProceso And .DefFormaPago = EnProceso And .DefProveActual = EnProceso Then
        lblFecIni(1).caption = NullToStr(.FechaInicioSuministro)
        lblFecFin(1).caption = NullToStr(.FechaFinSuministro)
        lblDest(1).caption = NullToStr(.DestCod)
        lblDest(2).caption = NullToStr(.DestDen)
        lblPago(1).caption = NullToStr(.PagCod)
        lblPago(2).caption = NullToStr(.PagDen)
        lblProveProce(1).caption = NullToStr(.ProveActual)
        lblProveProce(2).caption = NullToStr(.ProveActDen)
        If .DefSolicitud = EnProceso And gParametrosGenerales.gbSolicitudesCompras Then
            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
        End If
        MostrarApartado iAptDatosGenerales
        Exit Sub
    End If
    If .DefDestino <> EnProceso And .DefFechasSum <> EnProceso And .DefFormaPago <> EnProceso And .DefProveActual <> EnProceso And .DefSolicitud <> EnProceso Then
        lblFecIni(0).Visible = False
        lblFecIni(1).Visible = False
        lblFecFin(0).Visible = False
        lblFecFin(1).Visible = False
        lblDest(0).Visible = False
        lblDest(1).Visible = False
        lblDest(2).Visible = False
        lblPago(0).Visible = False
        lblPago(1).Visible = False
        lblPago(2).Visible = False
        lblProveProce(0).Visible = False
        lblProveProce(1).Visible = False
        lblProveProce(2).Visible = False
        lblRequest(0).Visible = False
        lblRequest(1).Visible = False
        MostrarApartado iAptDatosGenerales
        Exit Sub
    End If

    If Not .DefFechasSum = EnProceso Then
        lblFecIni(0).Visible = False
        lblFecIni(1).Visible = False
        lblFecFin(0).Visible = False
        lblFecFin(1).Visible = False
        If gParametrosGenerales.gbSolicitudesCompras Then
            lblRequest(0).Top = lblProveProce(1).Top
            lblRequest(1).Top = lblProveProce(1).Top
        End If
        lblProveProce(0).Top = lblPago(1).Top
        lblProveProce(1).Top = lblPago(1).Top
        lblProveProce(2).Top = lblPago(1).Top
        lblPago(0).Top = lblDest(0).Top
        lblPago(1).Top = lblDest(0).Top
        lblPago(2).Top = lblDest(0).Top
        lblDest(0).Top = lblFecIni(0).Top
        lblDest(1).Top = lblFecIni(0).Top
        lblDest(2).Top = lblFecIni(0).Top
        If Not .DefDestino = EnProceso Then
            lblDest(0).Visible = False
            lblDest(1).Visible = False
            lblDest(2).Visible = False
            If gParametrosGenerales.gbSolicitudesCompras Then
                lblRequest(0).Top = lblProveProce(1).Top
                lblRequest(1).Top = lblProveProce(1).Top
            End If
            lblProveProce(0).Top = lblPago(1).Top
            lblProveProce(1).Top = lblPago(1).Top
            lblProveProce(2).Top = lblPago(1).Top
            lblPago(0).Top = lblDest(0).Top
            lblPago(1).Top = lblDest(0).Top
            lblPago(2).Top = lblDest(0).Top
            If Not .DefFormaPago = EnProceso Then
                lblPago(0).Visible = False
                lblPago(1).Visible = False
                lblPago(2).Visible = False
                If gParametrosGenerales.gbSolicitudesCompras Then
                    lblRequest(0).Top = lblProveProce(1).Top
                    lblRequest(1).Top = lblProveProce(1).Top
                End If
                lblProveProce(0).Top = lblPago(1).Top
                lblProveProce(1).Top = lblPago(1).Top
                lblProveProce(2).Top = lblPago(1).Top
                If Not .DefProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Visible = True
                            lblRequest(1).Visible = True
                            lblRequest(0).Top = lblProveProce(1).Top
                            lblRequest(1).Top = lblProveProce(1).Top
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(1).caption = NullToStr(.ProveActual)
                    lblProveProce(2).caption = NullToStr(.ProveActDen)
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Visible = True
                            lblRequest(1).Visible = True
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                End If
            Else
                lblPago(1).caption = NullToStr(.PagCod)
                lblPago(2).caption = NullToStr(.PagDen)
                lblPago(0).Visible = True
                lblPago(1).Visible = True
                lblPago(2).Visible = True
                If Not .DefProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Top = lblProveProce(1).Top
                            lblRequest(1).Top = lblProveProce(1).Top
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(1).caption = NullToStr(.ProveActual)
                    lblProveProce(2).caption = NullToStr(.ProveActDen)
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                End If
            End If
        Else
            lblDest(0).Visible = True
            lblDest(1).Visible = True
            lblDest(2).Visible = True
            lblDest(1).caption = NullToStr(.DestCod)
            lblDest(2).caption = NullToStr(.DestDen)
            If Not .DefFormaPago = EnProceso Then
                lblPago(0).Visible = False
                lblPago(1).Visible = False
                lblPago(2).Visible = False
                If gParametrosGenerales.gbSolicitudesCompras Then
                    lblRequest(0).Top = lblProveProce(1).Top
                    lblRequest(1).Top = lblProveProce(1).Top
                End If
                lblProveProce(0).Top = lblPago(1).Top
                lblProveProce(1).Top = lblPago(1).Top
                lblProveProce(2).Top = lblPago(1).Top
                If Not .DefProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Top = lblProveProce(1).Top
                            lblRequest(1).Top = lblProveProce(1).Top
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(1).caption = NullToStr(.ProveActual)
                    lblProveProce(2).caption = NullToStr(.ProveActDen)
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                End If
            Else
                lblPago(0).Visible = True
                lblPago(1).Visible = True
                lblPago(2).Visible = True
                lblPago(1).caption = NullToStr(.PagCod)
                lblPago(2).caption = NullToStr(.PagDen)
                If Not .DefProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Top = lblProveProce(1).Top
                            lblRequest(1).Top = lblProveProce(1).Top
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(1).caption = NullToStr(.ProveActual)
                    lblProveProce(2).caption = NullToStr(.ProveActDen)
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                End If
            End If
            
        End If
    Else
        lblFecIni(0).Visible = True
        lblFecIni(1).Visible = True
        lblFecFin(0).Visible = True
        lblFecFin(1).Visible = True
        lblFecIni(1).caption = NullToStr(.FechaInicioSuministro)
        lblFecFin(1).caption = NullToStr(.FechaFinSuministro)
        If Not .DefDestino = EnProceso Then
            lblDest(0).Visible = False
            lblDest(1).Visible = False
            lblDest(2).Visible = False
            If gParametrosGenerales.gbSolicitudesCompras Then
                lblRequest(0).Top = lblProveProce(1).Top
                lblRequest(1).Top = lblProveProce(1).Top
            End If
            lblProveProce(0).Top = lblPago(1).Top
            lblProveProce(1).Top = lblPago(1).Top
            lblProveProce(2).Top = lblPago(1).Top
            lblPago(0).Top = lblDest(0).Top
            lblPago(1).Top = lblDest(0).Top
            lblPago(2).Top = lblDest(0).Top
            If Not .DefFormaPago = EnProceso Then
                If gParametrosGenerales.gbSolicitudesCompras Then
                    lblRequest(0).Top = lblProveProce(1).Top
                    lblRequest(1).Top = lblProveProce(1).Top
                End If
                lblProveProce(0).Top = lblPago(1).Top
                lblProveProce(1).Top = lblPago(1).Top
                lblProveProce(2).Top = lblPago(1).Top
                lblPago(0).Visible = False
                lblPago(1).Visible = False
                lblPago(2).Visible = False
                If Not .DefProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Top = lblProveProce(1).Top
                            lblRequest(1).Top = lblProveProce(1).Top
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(1).caption = NullToStr(.ProveActual)
                    lblProveProce(2).caption = NullToStr(.ProveActDen)
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                End If
            Else
                lblPago(1).caption = NullToStr(.PagCod)
                lblPago(2).caption = NullToStr(.PagDen)
                lblPago(0).Visible = True
                lblPago(1).Visible = True
                lblPago(2).Visible = True
                If Not .DefProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Top = lblProveProce(1).Top
                            lblRequest(1).Top = lblProveProce(1).Top
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(1).caption = NullToStr(.ProveActual)
                    lblProveProce(2).caption = NullToStr(.ProveActDen)
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                End If
            End If
        Else
            lblDest(0).Visible = True
            lblDest(1).Visible = True
            lblDest(2).Visible = True
            lblDest(1).caption = NullToStr(.DestCod)
            lblDest(2).caption = NullToStr(.DestDen)
            If Not .DefFormaPago = EnProceso Then
                lblPago(0).Visible = False
                lblPago(1).Visible = False
                lblPago(2).Visible = False
                If gParametrosGenerales.gbSolicitudesCompras Then
                    lblRequest(0).Top = lblProveProce(1).Top
                    lblRequest(1).Top = lblProveProce(1).Top
                End If
                lblProveProce(0).Top = lblPago(1).Top
                lblProveProce(1).Top = lblPago(1).Top
                lblProveProce(2).Top = lblPago(1).Top
                
                If Not .DefProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Top = lblProveProce(1).Top
                            lblRequest(1).Top = lblProveProce(1).Top
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(1).caption = NullToStr(.ProveActual)
                    lblProveProce(2).caption = NullToStr(.ProveActDen)
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                End If
            Else
                lblPago(0).Visible = True
                lblPago(1).Visible = True
                lblPago(2).Visible = True
                lblPago(1).caption = NullToStr(.PagCod)
                lblPago(2).caption = NullToStr(.PagDen)
                If Not .DefProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(0).Top = lblProveProce(1).Top
                            lblRequest(1).Top = lblProveProce(1).Top
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(1).caption = NullToStr(.ProveActual)
                    lblProveProce(2).caption = NullToStr(.ProveActDen)
                    If gParametrosGenerales.gbSolicitudesCompras Then
                        If Not .DefSolicitud = EnProceso Then
                            lblRequest(0).Visible = False
                            lblRequest(1).Visible = False
                        Else
                            lblRequest(1).caption = NullToStr(.SolicitudId) & " " & NullToStr(.SolicitudDen)
                        End If
                    End If
                End If
            End If
            
        End If
    End If
    
End With

MostrarApartado iAptDatosGenerales

End Sub

''' <summary>
''' Mostrar/Ocultar paneles
''' </summary>
''' <param name="idx">Que panel mostrar/ocultar</param>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde: sdbcProceDen_Click  cmdAdjuntar_Click   GenerarContrato   cmdModificar_Click
''' MostrarContrato     LimpiarCampos       MostrarDatosGenerales   MostrarProveedor; Tiempo m�ximo: 0</remarks>
Private Sub MostrarApartado(idx As Integer)
    Dim i As Integer

    For i = 0 To picApartado.UBound
        picApartado(i).Visible = IIf(idx = i, True, False)
    Next

    picControl(0).Visible = IIf(idx = 0, True, False)
    picControl(1).Visible = IIf(idx = 1, True, False)
    picControl(2).Visible = IIf(idx > 1, True, False)
    
End Sub

Private Sub MostrarProveedor()
Dim oCon As CContacto

Screen.MousePointer = vbHourglass

LimpiarProve

With g_oProveSeleccionado
    lblPortal.caption = NullToStr(.CodPortal)
    lblDir.caption = NullToStr(.Direccion)
    lblCp.caption = NullToStr(.cP)
    lblPob.caption = NullToStr(.Poblacion)
    lblPaiCod.caption = NullToStr(.codPais)
    lblPaiDen.caption = NullToStr(.DenPais)
    lblProviCod.caption = NullToStr(.codProvi)
    lblProviDen.caption = NullToStr(.DenProvi)
    lblMonCod.caption = NullToStr(.CodMon)
    lblMonDen.caption = NullToStr(.DenMon)
    lblURL.caption = NullToStr(.URLPROVE)
    sdbgContactos.RemoveAll
    For Each oCon In .Contactos
        sdbgContactos.AddItem NullToStr(oCon.Apellidos) & Chr(m_lSeparador) & NullToStr(oCon.nombre) & Chr(m_lSeparador) & NullToStr(oCon.Departamento) & Chr(m_lSeparador) & NullToStr(oCon.Cargo) & Chr(m_lSeparador) & NullToStr(oCon.Tfno) _
        & Chr(m_lSeparador) & NullToStr(oCon.Fax) & Chr(m_lSeparador) & NullToStr(oCon.tfnomovil) & Chr(m_lSeparador) & NullToStr(oCon.mail) & Chr(m_lSeparador) & BooleanToSQLBinary(oCon.Def) '& chr(m_lSeparador) & BooleanToSQLBinary(oCon.NotifSubasta)
    Next
    
    lblCal1Den = NullToStr(.Calif1)
    lblcal2den = NullToStr(.Calif2)
    lblcal3den = NullToStr(.Calif3)
    
    lblCal1Val = DblToStr(.Val1, m_sFormatoNumber)
    lblCal2Val = DblToStr(.Val2, m_sFormatoNumber)
    lblCal3Val = DblToStr(.Val3, m_sFormatoNumber)
    txtObsProve.Text = NullToStr(.obs)

End With

MostrarApartado iAptProveedor

Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Carga multiidioma de los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo:0,1</remarks>
Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONTRATOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value '1
        ador.MoveNext
        lblAnyo.caption = ador(0).Value
        ador.MoveNext
        lblCProceCod.caption = ador(0).Value '3
        m_sIdiProceso = Left(lblCProceCod.caption, Len(lblCProceCod.caption) - 1)
        ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = ador(0).Value '4 Cod
        sdbcProceCod.Columns(0).caption = ador(0).Value
        sdbcProceDen.Columns(1).caption = ador(0).Value
        ador.MoveNext
        sdbcGMN1_4Cod.Columns(1).caption = ador(0).Value '5 Denominacion
        sdbcProceCod.Columns(1).caption = ador(0).Value
        sdbcProceDen.Columns(0).caption = ador(0).Value
        ador.MoveNext
        m_sIdiCodigo = ador(0).Value
        ador.MoveNext
        m_sIdiFecha = ador(0).Value 'Fecha
        ador.MoveNext
        Label13.caption = ador(0).Value
        lblMon(0).caption = ador(0).Value
        m_sIdiMon = Left(lblMonCod.caption, Len(lblMonCod.caption) - 1)
        ador.MoveNext
        lblCambio(0).caption = ador(0).Value
        ador.MoveNext
        lblGMN4_4.caption = ador(0).Value & ":" '10
        m_sIdiMaterial = ador(0).Value
        ador.MoveNext
        lblFecApe.Item(0).caption = ador(0).Value
        ador.MoveNext
        lblFecNec(0).caption = ador(0).Value
        ador.MoveNext
        lblFecPres(0).caption = ador(0).Value
        ador.MoveNext
        lblFecAdj(0).caption = ador(0).Value
        ador.MoveNext
        chkPermAdjDir.caption = ador(0).Value '15
        ador.MoveNext
        lblPresupuesto(0).caption = ador(0).Value
        ador.MoveNext
        lblFecIni(0).caption = ador(0).Value
        ador.MoveNext
        lblFecFin(0).caption = ador(0).Value
        ador.MoveNext
        lblDest(0).caption = ador(0).Value
        ador.MoveNext
        lblPago(0).caption = ador(0).Value
        ador.MoveNext
        lblProveProce(0).caption = ador(0).Value
        ador.MoveNext
        lblDatosGenProce.caption = ador(0).Value
        ador.MoveNext
        lblFecLimit(0).caption = ador(0).Value '23
        ador.MoveNext
        m_sIdiSelProceso = ador(0).Value
        ador.MoveNext
        m_sIdiTodosArchivos = ador(0).Value '25
        ador.MoveNext
        m_sIdiElArchivo = ador(0).Value
        ador.MoveNext
        m_sIdiGuardar = ador(0).Value
        ador.MoveNext
        m_sIdiTipoOrig = ador(0).Value
        ador.MoveNext
        Label11.caption = ador(0).Value
        ador.MoveNext
        lblCodPort.caption = ador(0).Value '30
        ador.MoveNext
        Label12.caption = ador(0).Value 'Direccion
        ador.MoveNext
        Label10.caption = ador(0).Value 'CP
        ador.MoveNext
        Label9.caption = ador(0).Value 'Poblacion
        ador.MoveNext
        Label4.caption = ador(0).Value 'Pais
        ador.MoveNext
        Label13.caption = ador(0).Value 'Moneda '35
        ador.MoveNext
        Label3.caption = ador(0).Value 'Provincia
        ador.MoveNext
        Label1.caption = ador(0).Value 'url
        ador.MoveNext
         sdbgContactos.Columns(0).caption = ador(0).Value
        ador.MoveNext
         sdbgContactos.Columns(1).caption = ador(0).Value
        ador.MoveNext
          sdbgContactos.Columns(2).caption = ador(0).Value '40
        ador.MoveNext
          sdbgContactos.Columns(3).caption = ador(0).Value
        ador.MoveNext
          sdbgContactos.Columns(4).caption = ador(0).Value
        ador.MoveNext
          sdbgContactos.Columns(5).caption = ador(0).Value
        ador.MoveNext
          sdbgContactos.Columns(6).caption = ador(0).Value
        ador.MoveNext
          sdbgContactos.Columns(7).caption = ador(0).Value '45
        ador.MoveNext
        sdbgContactos.Columns(8).caption = ador(0).Value
        ador.MoveNext
        'sdbgContactos.Columns(10).Caption = Ador(0).Value
        ador.MoveNext
        cmdEliminar.caption = ador(0).Value
        ador.MoveNext
        ador.MoveNext
        cmdModificar.caption = ador(0).Value
        ador.MoveNext
        cmdRestaurar.Item(0).caption = ador(0).Value
        cmdRestaurar.Item(1).caption = ador(0).Value
        cmdRestaurar.Item(2).caption = ador(0).Value
        ador.MoveNext
        m_sIdiSelecAdjunto = ador(0).Value
        ador.MoveNext
        cmdCrear.caption = ador(0).Value
        ador.MoveNext
        cmdAdjuntar.caption = ador(0).Value
        ador.MoveNext
        cmdMenos.ToolTipText = ador(0).Value
        ador.MoveNext
        cmdMas.ToolTipText = ador(0).Value
        ador.MoveNext
        cmdPDFCon.ToolTipText = ador(0).Value
        m_sIdiExport = ador(0).Value
        ador.MoveNext
        cmdSalvar(0).ToolTipText = ador(0).Value
        cmdSalvar(1).ToolTipText = ador(0).Value
        ador.MoveNext
        Me.cmdAbrir(0).ToolTipText = ador(0).Value
        Me.cmdAbrir(1).ToolTipText = ador(0).Value
        ador.MoveNext
        Me.cmdImprimir(0).ToolTipText = ador(0).Value
        Me.cmdImprimir(1).ToolTipText = ador(0).Value
        ador.MoveNext
        m_sIdiArchivosPDF = ador(0).Value
        ador.MoveNext
        m_sIdiElContrato = ador(0).Value
        ador.MoveNext
        m_sIdiZoom = ador(0).Value
        ador.MoveNext
        lblRequest(0).caption = ador(0).Value
        ador.MoveNext
        m_sIdiAdjuntarTitle = ador(0).Value
        ador.MoveNext
        m_sIdiNombre = ador(0).Value
        ador.MoveNext
        m_sIdiAceptar = ador(0).Value
        ador.MoveNext
        m_sIdiCancelar = ador(0).Value
        ador.MoveNext
        g_sIdiNombreContr = ador(0).Value
        ador.MoveNext
        Me.lblNoVisualizable.caption = ador(0).Value    'No es posible visualizar el archivo
        ador.MoveNext
        Me.lblVisualizable.caption = ador(0).Value 'Los formatos visualizables son: .doc, .txt , .rtf .pdf imagen

        ador.Close
    End If
    Set ador = Nothing
    
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    lblSolicitud(0).caption = gParametrosGenerales.gsDenSolicitudCompra & ":"

End Sub



Public Sub VerDetalleResponsable()
Dim oPer As CPersona
Dim teserror As TipoErrorSummit

    'Muestra el detalle del responsable del proceso
    If g_oProcesoSeleccionado.responsable Is Nothing Then
        basMensajes.ProcesoSinResponsable
        Exit Sub
    End If
    If g_oProcesoSeleccionado.responsable.Cod = "" Then
        basMensajes.ProcesoSinResponsable
        Exit Sub
    End If
    
    Set oPer = oFSGSRaiz.Generar_CPersona
    oPer.Cod = g_oProcesoSeleccionado.responsable.Cod
            
    teserror = oPer.CargarTodosLosDatos
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Exit Sub
    End If
        
    Set frmESTRORGPersona.frmOrigen = Me
    frmESTRORGPersona.txtCod = oPer.Cod
    frmESTRORGPersona.txtNom = NullToStr(oPer.nombre)
    frmESTRORGPersona.txtApel = oPer.Apellidos
    frmESTRORGPersona.txtCargo = NullToStr(oPer.Cargo)
    frmESTRORGPersona.txtFax = NullToStr(oPer.Fax)
    frmESTRORGPersona.txtMail = NullToStr(oPer.mail)
    frmESTRORGPersona.txtTfno = NullToStr(oPer.Tfno)
    frmESTRORGPersona.txtTfno2 = NullToStr(oPer.Tfno2)
    frmESTRORGPersona.sdbcEquipo.Text = NullToStr(oPer.codEqp)
    If NullToStr(oPer.codEqp) <> "" Then
        frmESTRORGPersona.chkFunCompra.Value = vbChecked
    End If
    Screen.MousePointer = vbNormal
    frmESTRORGPersona.Show vbModal
    Set oPer = Nothing
    
End Sub

Public Sub SustituirResponsable()
    'Muestra el formulario para seleccionar el comprador responsable del proceso.
    If g_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    Set frmSELPROVEResp.g_oOrigen = Me
    Set frmSELPROVEResp.g_oProcesoSeleccionado = g_oProcesoSeleccionado
    frmSELPROVEResp.g_udtAccion = accionessummit.ACCModRespContratos
    
    Screen.MousePointer = vbNormal
    frmSELPROVEResp.Show vbModal
End Sub

Public Sub ResponsableSustituido(ByVal oPer As CPersona)
    
    If g_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If g_oProcesoSeleccionado.responsable Is Nothing Then
        Set g_oProcesoSeleccionado.responsable = oFSGSRaiz.Generar_CComprador
    End If
    
    g_oProcesoSeleccionado.responsable.Cod = oPer.Cod
    g_oProcesoSeleccionado.responsable.codEqp = oPer.codEqp
    g_oProcesoSeleccionado.responsable.nombre = NullToStr(oPer.nombre)
    g_oProcesoSeleccionado.responsable.Apel = oPer.Apellidos
    g_oProcesoSeleccionado.responsable.Cargo = NullToStr(oPer.Cargo)
    g_oProcesoSeleccionado.responsable.Fax = NullToStr(oPer.Fax)
    g_oProcesoSeleccionado.responsable.mail = NullToStr(oPer.mail)
    g_oProcesoSeleccionado.responsable.Tfno = NullToStr(oPer.Tfno)
    g_oProcesoSeleccionado.responsable.Tfno2 = NullToStr(oPer.Tfno2)
        
    Set oPer = Nothing
    
    If g_oProcesoSeleccionado.responsable.nombre <> "" Then
        cmdResponsable.ToolTipText = g_oProcesoSeleccionado.responsable.nombre & " " & g_oProcesoSeleccionado.responsable.Apel
    Else
        cmdResponsable.ToolTipText = g_oProcesoSeleccionado.responsable.Apel
    End If
    

End Sub
'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
Private Sub CargarGMN1Automaticamente()
    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
    Set m_oIMAsig = oUsuarioSummit.comprador
    Set oGMN1s = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
    If Not oGMN1s.Count = 0 Then
        m_bRespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4Cod.Columns(0).Text = sdbcGMN1_4Cod.Text
        m_bRespetarCombo = False
        GMN1Seleccionado
    End If
    Set oGMN1s = Nothing
End Sub

''' <summary>
''' Mostrar un Pdf
''' </summary>
''' <param name="NomFichero">Nombre del fichero a mostrar</param>
''' <remarks>Llamada desde: cmdAdjuntar_Click   cmdModificar_Click  MostrarContrato; Tiempo m�ximo:0,1</remarks>
Private Sub AbrirFicheroPDF(ByVal NomFichero As String)
    If NomFichero = "" Then Exit Sub
    
    'Lanzamos la aplicacion
    Me.AcroPDFControl.LoadFile NomFichero
    
End Sub

''' <summary>
''' Para poder mostrar un Pdf este debe estar en disco duro.
''' </summary>
''' <remarks>Llamada desde: cmdAdjuntar_Click   cmdModificar_Click  MostrarContrato; Tiempo m�ximo: 0,1</remarks>
Private Function SalvarDisco() As String
    Dim DataFile As Integer
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    Dim bytChunk() As Byte
    Dim lSize As Long
    Dim lInit As Long
                
    On Error GoTo Cancelar:
    
    SalvarDisco = ""
    
    If m_oContratoSeleccionado Is Nothing Then Exit Function
    
    sFileName = basUtilidades.DevolverPathFichTemp
    sFileTitle = m_oContratoSeleccionado.nombre & "." & m_oContratoSeleccionado.Extension
    sFileName = sFileName & sFileTitle
    
    If m_oContratoSeleccionado.dataSize = 0 Then
        basMensajes.NoValido m_sIdiElArchivo & " " & sFileTitle
        Exit Function
    End If
    
    teserror = m_oContratoSeleccionado.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Exit Function
    End If
    DataFile = 1
    
    Screen.MousePointer = vbHourglass
    'Abrimos el fichero para escritura binaria
    Open sFileName For Binary Access Write As DataFile
    
    lInit = 0
    lSize = giChunkSize
    
    While lInit < m_oContratoSeleccionado.dataSize
        bytChunk = m_oContratoSeleccionado.ReadData(lInit, lSize)
        Put DataFile, , bytChunk
        lInit = lInit + lSize + 1
    Wend
    
    'Verificar escritura
    If LOF(DataFile) <> m_oContratoSeleccionado.dataSize Then
        basMensajes.ErrorDeEscrituraEnDisco
    End If
    Close DataFile
            
    sayFileNames(UBound(sayFileNames)) = sFileName
    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
    
    DataFile = 0
    
    SalvarDisco = sFileName
    
Cancelar:
    
        If DataFile <> 0 Then
            Close DataFile
            DataFile = 0
        End If
    Screen.MousePointer = vbNormal
End Function

''' <summary>
''' Mostrar una Imagen
''' </summary>
''' <param name="NomFichero">Nombre del fichero a mostrar</param>
''' <remarks>Llamada desde: cmdAdjuntar_Click   cmdModificar_Click  MostrarContrato; Tiempo m�ximo:0,1</remarks>
Private Sub AbrirFicheroImagen(ByVal vdata As Variant)
    
    On Error GoTo Error
    
    If IsNull(vdata) Then
        PajantImagen.pi.DisplayMode = PJT_NONE
    Else
        PajantImagen.pi.DisplayMode = PJT_SCROLL
    End If
    
    'Fondo verde de la imagen
    PajantImagen.pi.SetBackground &H808000, 0
     
    PajantImagen.pi.ImportFrom PJT_MEMPNG, 0, 0, vdata, PJT_TONEWIMAGE
    Exit Sub
    
Error:
    MsgBox Err.Description, vbCritical, "FULLSTEP GS"
End Sub

