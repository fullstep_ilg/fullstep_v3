VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmArtCentralUnificar 
   Caption         =   "DArticulos Agregados"
   ClientHeight    =   7605
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   9405
   Icon            =   "frmArtCentUnificar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7605
   ScaleWidth      =   9405
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "           >>"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4680
      TabIndex        =   12
      Top             =   7200
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3480
      TabIndex        =   11
      Top             =   7200
      Width           =   1095
   End
   Begin VB.PictureBox picAgregados 
      Height          =   6015
      Left            =   120
      ScaleHeight     =   5955
      ScaleWidth      =   9075
      TabIndex        =   2
      Top             =   1080
      Width           =   9135
      Begin SSDataWidgets_B.SSDBGrid sdbgArticulos 
         Height          =   3975
         Left            =   120
         TabIndex        =   10
         Top             =   1800
         Width           =   8805
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         Col.Count       =   6
         stylesets.count =   9
         stylesets(0).Name=   "Atributos"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmArtCentUnificar.frx":014A
         stylesets(1).Name=   "IntOK"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   -2147483633
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmArtCentUnificar.frx":0203
         stylesets(2).Name=   "Normal"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmArtCentUnificar.frx":021F
         stylesets(3).Name=   "ActiveRow"
         stylesets(3).ForeColor=   16777215
         stylesets(3).BackColor=   -2147483647
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmArtCentUnificar.frx":023B
         stylesets(3).AlignmentText=   0
         stylesets(4).Name=   "IntHeader"
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmArtCentUnificar.frx":0257
         stylesets(4).AlignmentText=   0
         stylesets(4).AlignmentPicture=   0
         stylesets(5).Name=   "Adjudica"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmArtCentUnificar.frx":03AF
         stylesets(6).Name=   "styEspAdjSi"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmArtCentUnificar.frx":0724
         stylesets(7).Name=   "IntKO"
         stylesets(7).ForeColor=   16777215
         stylesets(7).BackColor=   255
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmArtCentUnificar.frx":07A1
         stylesets(8).Name=   "HayImpuestos"
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmArtCentUnificar.frx":081E
         stylesets(8).AlignmentText=   2
         stylesets(8).AlignmentPicture=   2
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         MaxSelectedRows =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         CaptionAlignment=   0
         Columns.Count   =   6
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6959
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1402
         Columns(2).Caption=   "UNIDAD"
         Columns(2).Name =   "UNIDAD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "UONS"
         Columns(3).Name =   "UONS"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   1
         Columns(4).Width=   1244
         Columns(4).Caption=   "ESPEC"
         Columns(4).Name =   "ESPEC"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Style=   4
         Columns(4).ButtonsAlways=   -1  'True
         Columns(4).StyleSet=   "styEspAdjSi"
         Columns(5).Width=   1720
         Columns(5).Caption=   "ATRIBUTOS"
         Columns(5).Name =   "ATRIBUTOS"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Style=   4
         Columns(5).ButtonsAlways=   -1  'True
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   -2147483633
         Columns(5).StyleSet=   "Atributos"
         _ExtentX        =   15531
         _ExtentY        =   7011
         _StockProps     =   79
         Caption         =   "dSeleccione los art�culos y haga 'clic' en '>>' para continuar ... "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8520
         Picture         =   "frmArtCentUnificar.frx":0B93
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1200
         Width           =   315
      End
      Begin VB.Frame fraCabecera 
         Height          =   1575
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   8295
         Begin VB.CommandButton cmdSelMat 
            Height          =   315
            Left            =   7770
            Picture         =   "frmArtCentUnificar.frx":0ED5
            Style           =   1  'Graphical
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   360
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrarMaterial 
            Height          =   315
            Left            =   7400
            Picture         =   "frmArtCentUnificar.frx":0F41
            Style           =   1  'Graphical
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   360
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrarUon 
            Height          =   315
            Left            =   7395
            Picture         =   "frmArtCentUnificar.frx":0FE6
            Style           =   1  'Graphical
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   720
            Width           =   345
         End
         Begin VB.CommandButton cmdSelUon 
            Height          =   315
            Left            =   7770
            Picture         =   "frmArtCentUnificar.frx":108B
            Style           =   1  'Graphical
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   720
            Width           =   345
         End
         Begin VB.TextBox txtCodArticulo 
            Height          =   285
            Left            =   960
            MaxLength       =   100
            TabIndex        =   6
            Top             =   1200
            Width           =   1695
         End
         Begin VB.TextBox txtDenominacion 
            Height          =   285
            Left            =   4320
            MaxLength       =   100
            TabIndex        =   5
            Top             =   1200
            Width           =   3255
         End
         Begin VB.CommandButton cmdAyuda 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   7680
            Picture         =   "frmArtCentUnificar.frx":10F7
            Style           =   1  'Graphical
            TabIndex        =   4
            Top             =   1200
            Width           =   225
         End
         Begin VB.Label lblMaterial 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1800
            TabIndex        =   20
            Top             =   360
            Width           =   5520
         End
         Begin VB.Label lblMat 
            BackStyle       =   0  'Transparent
            Caption         =   "Material:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   120
            TabIndex        =   19
            Top             =   405
            Width           =   1050
         End
         Begin VB.Label lblUniOrg 
            BackStyle       =   0  'Transparent
            Caption         =   "DUnidad organizativa"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   120
            TabIndex        =   18
            Top             =   765
            Width           =   1530
         End
         Begin VB.Label lblUon 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1800
            TabIndex        =   17
            Top             =   720
            Width           =   5520
         End
         Begin VB.Label lbCodigo 
            Caption         =   "DCodigo"
            Height          =   285
            Left            =   120
            TabIndex        =   8
            Top             =   1200
            Width           =   1995
         End
         Begin VB.Label lblDenominacion 
            Caption         =   "DDenominacion"
            Height          =   285
            Left            =   3000
            TabIndex        =   7
            Top             =   1200
            Width           =   1335
         End
      End
   End
   Begin VB.Label lblArtDen 
      Caption         =   "xxxxxxx- XXXXXXXXXXXX"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   9135
   End
   Begin VB.Label Label1 
      Caption         =   "dSeleccione los art�culos que desee unificar bajo : "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9135
   End
End
Attribute VB_Name = "frmArtCentralUnificar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private m_bRestrMantUsu As Boolean
Private m_bRestrMantPerf As Boolean
Private m_bModifArti As Boolean

Private m_oUonsSeleccionadas As CUnidadesOrganizativas
Private m_oArticuloCentral As CArticulo
Private m_oArticulos As CArticulos
Public g_sDenominacion, g_scodart As String

'Separadores
Private Const lSeparador = 127

'idiomas
Private m_sVariasUnidades As String

Public isAlta As Boolean
Private m_oGMNSeleccionada As Object
Private m_bModoEdicion As Boolean


Private Sub cmdBorrarMaterial_Click()
    Set m_oGMNSeleccionada = oFSGSRaiz.Generar_CGrupoMatNivel4
    Me.lblMaterial.caption = ""
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Set m_oUonsSeleccionadas = Nothing

End Sub

Public Property Let Denominacion(ByVal sDenom As String)
    g_sDenominacion = sDenom
End Property

Public Property Let Usuario(ByVal oUsuario As CUsuario)
    Set oUsuarioSummit = oUsuario
End Property

Public Property Set ArticuloCentral(ByRef oArtCentral As CArticulo)
    Set m_oArticuloCentral = oArtCentral
    Me.lblArtDen.caption = m_oArticuloCentral.CodDen
End Property

Public Property Get ArticuloCentral() As CArticulo
    Set ArticuloCentral = m_oArticuloCentral
End Property

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ARTCENTRALUNIFICAR, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        Me.Label1.caption = Ador(0).Value
        Ador.MoveNext
        Me.lbCodigo.caption = Ador(0).Value        '3
        Ador.MoveNext
        Me.lblDenominacion.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblUniOrg.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("UNIDAD").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("UONS").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("ESPEC").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("ATRIBUTOS").caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sVariasUnidades = Ador(0).Value
        Ador.MoveNext
        Me.sdbgArticulos.caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
    'pargen lit
    Me.caption = gParametrosGenerales.gsArticulosPlanta
    
End Sub

Private Sub cmdAyuda_Click()
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub cmdBuscar_Click()
    Dim bCoincidenciaCod, bCodFinalizanPor, bCoincidenciaDen, bDenFinalizanPor, bCoincidenciaTotalDoble As Boolean
    Dim sCod, sDen As String
    Dim oIMaterialComprador As IMaterialAsignado
    Dim oArticulos As CArticulos
    
    If Me.txtCodArticulo.Text <> "" Then
        If InStr(txtCodArticulo.Text, "*") > 0 Or InStr(txtCodArticulo.Text, "*") = Len(txtCodArticulo.Text) Then
            bCoincidenciaCod = False
            If InStr(txtCodArticulo.Text, "*") > 1 Then 'div*
                sCod = Left(txtCodArticulo.Text, Len(txtCodArticulo.Text) - 1)
                bCodFinalizanPor = False
            Else '*div � *div*
                sCod = Mid(txtCodArticulo.Text, 2)
                bCodFinalizanPor = True
                If InStr(sCod, "*") > 1 Then '*div*
                    sCod = Replace(sCod, "*", "%", , , vbTextCompare)
                End If
            End If
        Else
            sCod = txtCodArticulo.Text
            bCoincidenciaCod = True
            bCodFinalizanPor = False
        End If
    End If
    If Me.txtDenominacion.Text <> "" Then
        If InStr(txtDenominacion.Text, "*") > 0 Or InStr(txtDenominacion.Text, "*") = Len(txtDenominacion.Text) Then
            bCoincidenciaDen = False
            If InStr(txtDenominacion.Text, "*") > 1 Then 'div*
                sDen = Left(txtDenominacion.Text, Len(txtDenominacion.Text) - 1)
                bDenFinalizanPor = False
            Else '*div � *div*
                sDen = Mid(txtDenominacion.Text, 2)
                bDenFinalizanPor = True
                If InStr(sDen, "*") > 1 Then '*div*
                    sDen = Replace(sDen, "*", "%", , , vbTextCompare)
                End If
            End If
        Else
            sDen = txtDenominacion.Text
            bCoincidenciaDen = True
            bDenFinalizanPor = False
        End If
    End If
    
    
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    Dim iPerfil As Integer
    If Not oUsuarioSummit.Perfil Is Nothing Then
        iPerfil = oUsuarioSummit.Perfil.Id
    Else
        iPerfil = 0
    End If
    
    Screen.MousePointer = vbHourglass
    bCoincidenciaTotalDoble = (sCod <> vbNullString) And (sDen <> vbNullString)
    
    If frmESTRMAT.g_bRComprador Then
        Set oIMaterialComprador = oUsuarioSummit.comprador
        Set oArticulos = oIMaterialComprador.DevolverArticulos(sCod, sDen, (bCoincidenciaCod And bCoincidenciaDen), , False, , , , , , m_oGMNSeleccionada.GMN1Cod, m_oGMNSeleccionada.GMN2Cod, m_oGMNSeleccionada.GMN3Cod, m_oGMNSeleccionada.GMN4Cod, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen, False, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, m_oUonsSeleccionadas, False, True, True, False)
    Else
        oArticulos.CargarTodosLosArticulos sCod, sDen, (bCoincidenciaDen Or bCoincidenciaCod), , , True, m_oGMNSeleccionada.GMN1Cod, m_oGMNSeleccionada.GMN2Cod, m_oGMNSeleccionada.GMN3Cod, _
                                     m_oGMNSeleccionada.GMN4Cod, , bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaTotalDoble, bCoincidenciaCod, bCoincidenciaDen, _
                                    bMostrarArtCentrales:=False, _
                                    bmostraruons:=True, _
                                    oUons:=m_oUonsSeleccionadas, _
                                    bIncluirAscendientesUon:=False, _
                                    bIncluirDescendientesUon:=True, _
                                    sUON1:=basOptimizacion.gUON1Usuario, _
                                    sUON2:=basOptimizacion.gUON2Usuario, _
                                    sUON3:=basOptimizacion.gUON3Usuario, _
                                    bRUsuUON:=m_bRestrMantUsu, _
                                    bRPerfUON:=m_bRestrMantPerf, _
                                    lIdPerf:=iPerfil, _
                                    bIncluirContadores:=True
    End If
                            
                                
    Set m_oArticulos = oArticulos
    CargarGridArticulos
    Screen.MousePointer = vbNormal
    Set oArticulos = Nothing
End Sub

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonUsu)) Is Nothing) Then
        m_bRestrMantUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonPerf)) Is Nothing) Then
        m_bRestrMantPerf = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificar)) Is Nothing) Then
        m_bModifArti = True
    End If
End Sub
Private Sub CargarGridArticulos()
    Dim oArticulo As CArticulo
    sdbgArticulos.RemoveAll
    For Each oArticulo In m_oArticulos
        If oArticulo.Cod <> Me.ArticuloCentral.Cod Then
            Dim sLinea As String
            sLinea = oArticulo.Cod & Chr(lSeparador)
            sLinea = sLinea & oArticulo.CodDen & Chr(lSeparador)
            sLinea = sLinea & oArticulo.CodigoUnidad & Chr(lSeparador)
            sLinea = sLinea & oArticulo.NombreUON & Chr(lSeparador)
            sdbgArticulos.AddItem sLinea
        End If
    Next
    
End Sub

Private Sub cmdSelUon_Click()
    
    '''Cargar unidades organizativas en funci�n de las restricciones
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    

    Dim frm As frmSELUO
    Set frm = New frmSELUO
    frm.multiselect = True
    frm.CheckChildren = True
    
    If Not m_oUonsSeleccionadas Is Nothing Then
        Set frm.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    
    oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , , False
    oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , , False
    oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , False

    frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
    frm.Show vbModal
    If frm.Aceptar Then
        Set Me.UonsSeleccionadas = frm.UonsSeleccionadas
    End If
    
    Set frm = Nothing
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.bPermProcMultiMaterial = False
    frmSELMAT.bRComprador = frmESTRMAT.g_bRComprador
    frmSELMAT.bRCompResponsable = frmESTRMAT.g_bRCompResp
    frmSELMAT.Show vbModal
    If frmSELMAT.g_bAceptar Then
        Set GMNSeleccionada = frmSELMAT.GMNSeleccionado
    End If

End Sub

Private Sub cmdCancelar_Click()
    Set m_oArticuloCentral = Nothing
    Unload Me
End Sub

Private Sub cmdContinuar_Click()
    Dim bk As Variant
    For Each bk In Me.sdbgArticulos.SelBookmarks
        'Me.sdbgArticulos.Rows(bk).Columns("cod").Value
        Dim Index As Variant
        Index = Me.sdbgArticulos.Columns("COD").CellValue(bk)
        Me.ArticuloCentral.agregarArticulo m_oArticulos.Item(Index)
    Next
    frmArtCentralUnifConfirm.g_scodart = Me.g_scodart
    frmArtCentralUnifConfirm.g_sDenominacion = Me.g_sDenominacion
    Set frmArtCentralUnifConfirm.ArticuloCentral = Me.ArticuloCentral
    Set frmArtCentralUnifConfirm.UonsSeleccionadas = m_oUonsSeleccionadas
    Set frmArtCentralUnifConfirm.GMNSeleccionada = m_oGMNSeleccionada
    frmArtCentralUnifConfirm.isAlta = True
    frmArtCentralUnifConfirm.ModoEdicion = Me.ModoEdicion
    frmArtCentralUnifConfirm.Show
    Unload Me
End Sub

Private Sub cmdBorrarUon_Click()
    Me.lblUon.caption = ""
    m_oUonsSeleccionadas.clear
End Sub

Private Sub Form_Initialize()
    Set m_oArticulos = oFSGSRaiz.Generar_CArticulos
    Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
End Sub

Private Sub Form_Load()
    CargarRecursos
    ConfigurarSeguridad
    PonerFieldSeparator Me
    Me.txtDenominacion = g_sDenominacion
    Me.txtCodArticulo = g_scodart
    
    If Me.sdbgArticulos.Rows = 0 Then
        CargarGridArticulos
    End If
    
    If Not m_oUonsSeleccionadas Is Nothing Then
        Me.lblUon.caption = m_oUonsSeleccionadas.titulo
    End If
    If Not m_oGMNSeleccionada Is Nothing Then
        Me.lblMaterial.caption = m_oGMNSeleccionada.titulo
    Else
        Set m_oGMNSeleccionada = oFSGSRaiz.Generar_CGrupoMatNivel4
    End If
End Sub


Private Sub Form_Terminate()
    Set m_oUonsSeleccionadas = Nothing
    Set m_oArticuloCentral = Nothing
    Set m_oArticulos = Nothing
    Set m_oGMNSeleccionada = Nothing
End Sub


Private Sub sdbgArticulos_BtnClick()
    Dim teserror As TipoErrorSummit
    Dim oArticulo As CArticulo
    Select Case sdbgArticulos.Columns(sdbgArticulos.Col).Name
        Case "ATRIBUTOS"
            If sdbgArticulos.Columns("COD").Value <> "" Then
                frmESTRMATAtrib.g_sOrigen = "FRMARTCENTRALUNIFICAR"
                frmESTRMATAtrib.g_bModoEdicion = True
                Set frmESTRMATAtrib.Articulo = m_oArticulos.Item(sdbgArticulos.Columns("COD").Value)
                Set frmESTRMATAtrib.g_oAtributos = m_oArticulos.Item(sdbgArticulos.Columns("COD").Value).DevolverTodosLosAtributosDelArticulo
                frmESTRMATAtrib.g_vCodArt = sdbgArticulos.Columns("COD").Value
                Set frmESTRMATAtrib.Articulo = m_oArticulos.Item(sdbgArticulos.Columns("COD").Value)
                frmESTRMATAtrib.cmdAsignar.Enabled = False
                frmESTRMATAtrib.cmdDesAsignar.Enabled = False
                frmESTRMATAtrib.Show
            End If
        Case "ESPEC"
            'Si no tiene permisos para modificar los art�culos:
                If m_bModifArti = False Then
                    frmArticuloEsp.txtArticuloEsp.Locked = True
                    frmArticuloEsp.cmdA�adirEsp.Enabled = False
                    frmArticuloEsp.cmdEliminarEsp.Enabled = False
                    frmArticuloEsp.cmdModificarEsp.Enabled = False
                    frmArticuloEsp.cmdSalvarEsp.Enabled = True
                    frmArticuloEsp.cmdAbrirEsp.Enabled = True
                    frmArticuloEsp.cmdRestaurar.Visible = False
                End If
                
                Set frmArticuloEsp.g_oArticulo = m_oArticulos.Item(sdbgArticulos.Columns("cod").Value)
                If frmArticuloEsp.g_oArticulo Is Nothing Then Exit Sub
                ''' Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n
                frmArticuloEsp.g_oArticulo.Usuario = basOptimizacion.gvarCodUsuario
                
                Set frmArticuloEsp.g_oIBaseDatos = frmArticuloEsp.g_oArticulo
                teserror = frmArticuloEsp.g_oIBaseDatos.IniciarEdicion
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set frmArticuloEsp.g_oIBaseDatos = Nothing
                    Set frmArticuloEsp.g_oArticulo = Nothing
                    Exit Sub
                End If
                frmArticuloEsp.g_oIBaseDatos.CancelarEdicion

                frmArticuloEsp.g_sOrigen = "frmESTRMAT"
                frmArticuloEsp.g_oArticulo.CargarTodasLasEspecificaciones
                If Not sdbgArticulos.IsAddRow Then
                    'Configurar parametros del commondialog
                    frmArticuloEsp.cmmdEsp.FLAGS = cdlOFNHideReadOnly
                    frmArticuloEsp.caption = frmArticuloEsp.caption & sdbgArticulos.Columns("DEN").Value

                    frmArticuloEsp.g_bRespetarCombo = True
                    frmArticuloEsp.txtArticuloEsp.Text = NullToStr(frmArticuloEsp.g_oArticulo.esp)
                    frmArticuloEsp.g_bRespetarCombo = False
                    frmArticuloEsp.AnyadirEspsALista
                    frmArticuloEsp.Show 1
                    If m_oArticulos.Item(sdbgArticulos.Columns("cod").Value).EspAdj = 1 Then
                        sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
                    Else
                        sdbgArticulos.Columns("ESPEC").CellStyleSet ""
                    End If
                    sdbgArticulos.Refresh
                End If
                If Me.Visible Then sdbgArticulos.SetFocus
                sdbgArticulos.Col = 7
        Case "UONS"
            Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
            Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
            Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
            Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
            Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
            Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
            
            oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id
            oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id
            oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id
            
            Dim frm As frmSELUO
            Set frm = New frmSELUO
            frm.multiselect = True
            frm.EnableCheck = False
            frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
            
            Set oArticulo = m_oArticulos.Item(sdbgArticulos.Columns("COD").Text)
            
            frm.subtitulo = oArticulo.CodDen
                                
            
            Set frm.UonsSeleccionadas = oArticulo.uons
            
            frm.Show vbModal
        
            Set frm = Nothing
    End Select
End Sub

Private Sub sdbgArticulos_RowLoaded(ByVal Bookmark As Variant)
    Dim oArticulo As CArticulo
    Set oArticulo = Nothing
    If Not IsNull(Bookmark) And Not IsEmpty(Bookmark) Then
        Set oArticulo = m_oArticulos.Item(Me.sdbgArticulos.Columns("COD").Value)
        If Not oArticulo Is Nothing Then
            If oArticulo.EspAdj <> 0 Then
                sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
            Else
                sdbgArticulos.Columns("ESPEC").CellStyleSet ""
            End If
            If oArticulo.ConAtributos Then
                sdbgArticulos.Columns("ATRIBUTOS").CellStyleSet "Atributos"
            Else
                sdbgArticulos.Columns("ATRIBUTOS").CellStyleSet ""
            End If
            If sdbgArticulos.Columns("UONS").Value = "Multiples*" Then
               sdbgArticulos.Columns("UONS").Value = m_sVariasUnidades
            End If
        End If
    End If
End Sub

Private Sub txtCodArticulo_Change()
    g_scodart = Me.txtCodArticulo.Text
End Sub

Private Sub txtDenominacion_Change()
    g_sDenominacion = Me.txtDenominacion.Text
End Sub

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas
    Set UonsSeleccionadas = m_oUonsSeleccionadas
End Property

Public Property Set UonsSeleccionadas(oUonsSeleccionadas As CUnidadesOrganizativas)
    If Not oUonsSeleccionadas Is Nothing Then
        Set m_oUonsSeleccionadas = oUonsSeleccionadas
        Me.lblUon.caption = m_oUonsSeleccionadas.titulo
    End If
End Property

Public Property Get GMNSeleccionada() As Object
    Set GMNSeleccionada = m_oGMNSeleccionada
End Property

Public Property Set GMNSeleccionada(oGMNSeleccionada As Object)
    If Not oGMNSeleccionada Is Nothing Then
        Set m_oGMNSeleccionada = oGMNSeleccionada
        Me.lblMaterial.caption = oGMNSeleccionada.titulo
    End If
End Property

Public Property Get ModoEdicion() As Boolean
    ModoEdicion = m_bModoEdicion
End Property

Public Property Let ModoEdicion(Value As Boolean)
    m_bModoEdicion = Value
End Property
