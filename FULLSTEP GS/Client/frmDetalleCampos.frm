VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmDetalleCampos 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DDetalle del campo:"
   ClientHeight    =   6090
   ClientLeft      =   6075
   ClientTop       =   7005
   ClientWidth     =   7395
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetalleCampos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6090
   ScaleWidth      =   7395
   Begin SSDataWidgets_B.SSDBGrid sdbgLista 
      Height          =   1875
      Left            =   300
      TabIndex        =   22
      Top             =   3400
      Width           =   6135
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   4
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmDetalleCampos.frx":0CB2
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      RowSelectionStyle=   1
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   9790
      Columns(0).Caption=   "VALOR"
      Columns(0).Name =   "VALOR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "EXISTE"
      Columns(1).Name =   "EXISTE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ORDEN"
      Columns(2).Name =   "ORDEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ORDEN_ANT"
      Columns(3).Name =   "ORDEN_ANT"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   10821
      _ExtentY        =   3316
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picCampoOrigen 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   300
      ScaleHeight     =   285
      ScaleWidth      =   6585
      TabIndex        =   20
      Top             =   330
      Width           =   6585
      Begin VB.Label lblCampoOrigen 
         BackColor       =   &H00808000&
         Caption         =   "DCampo origen: "
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   21
         Top             =   15
         Width           =   6225
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   350
      Left            =   2280
      ScaleHeight     =   345
      ScaleWidth      =   2535
      TabIndex        =   3
      Top             =   5620
      Width           =   2535
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   1200
         TabIndex        =   2
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   1005
      End
   End
   Begin MSComDlg.CommonDialog cmmdExcel 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
      MaxFileSize     =   1500
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   5400
      Left            =   60
      TabIndex        =   4
      Top             =   90
      Width           =   6975
      Begin VB.PictureBox picDatos 
         Appearance      =   0  'Flat
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   5085
         Left            =   240
         ScaleHeight     =   5085
         ScaleWidth      =   6645
         TabIndex        =   5
         Top             =   240
         Width           =   6640
         Begin VB.Frame FraLista 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   3825
            Left            =   0
            TabIndex        =   11
            Top             =   1110
            Width           =   6615
            Begin VB.CheckBox chkNoFecAntSis 
               BackColor       =   &H00808000&
               Caption         =   "DNo permitir fecha anterior a la fecha del sistema"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   390
               MaskColor       =   &H00FFFFFF&
               TabIndex        =   40
               Top             =   570
               Visible         =   0   'False
               Width           =   5175
            End
            Begin VB.OptionButton optLista 
               BackColor       =   &H00808000&
               Caption         =   "DIntroducci�n mediante selecci�n en lista"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   12
               Top             =   840
               Width           =   6495
            End
            Begin VB.OptionButton optListaExterna 
               BackColor       =   &H00808000&
               Caption         =   "DIntroducci�n mediante selecci�n en lista (LISTA EXTERNA)"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   39
               Top             =   840
               Width           =   6495
            End
            Begin VB.CommandButton cmdComprobar 
               Height          =   285
               Left            =   6120
               Picture         =   "frmDetalleCampos.frx":0CCE
               Style           =   1  'Graphical
               TabIndex        =   38
               Top             =   840
               Visible         =   0   'False
               Width           =   321
            End
            Begin VB.TextBox txtFormato 
               Height          =   285
               Left            =   3120
               MaxLength       =   500
               TabIndex        =   37
               Top             =   0
               Visible         =   0   'False
               Width           =   4900
            End
            Begin VB.TextBox txtValorComprob 
               Height          =   285
               Left            =   120
               TabIndex        =   36
               Top             =   1080
               Visible         =   0   'False
               Width           =   4580
            End
            Begin VB.Frame FrameBotonesGrid 
               Appearance      =   0  'Flat
               BackColor       =   &H00808000&
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   375
               Left            =   4200
               TabIndex        =   24
               Top             =   1560
               Width           =   1935
               Begin VB.CommandButton cmdImportarExcel 
                  Enabled         =   0   'False
                  Height          =   285
                  Left            =   120
                  Picture         =   "frmDetalleCampos.frx":1040
                  Style           =   1  'Graphical
                  TabIndex        =   30
                  Top             =   0
                  Width           =   321
               End
               Begin VB.CommandButton cmdMvtoElemLista 
                  Enabled         =   0   'False
                  Height          =   285
                  Index           =   1
                  Left            =   1560
                  Picture         =   "frmDetalleCampos.frx":1552
                  Style           =   1  'Graphical
                  TabIndex        =   28
                  Top             =   0
                  UseMaskColor    =   -1  'True
                  Width           =   321
               End
               Begin VB.CommandButton cmdMvtoElemLista 
                  Enabled         =   0   'False
                  Height          =   285
                  Index           =   0
                  Left            =   1200
                  Picture         =   "frmDetalleCampos.frx":1894
                  Style           =   1  'Graphical
                  TabIndex        =   27
                  Top             =   0
                  UseMaskColor    =   -1  'True
                  Width           =   321
               End
               Begin VB.CommandButton cmdElimValor 
                  Enabled         =   0   'False
                  Height          =   285
                  Left            =   840
                  Picture         =   "frmDetalleCampos.frx":1BD6
                  Style           =   1  'Graphical
                  TabIndex        =   26
                  Top             =   0
                  Width           =   321
               End
               Begin VB.CommandButton cmdAnyaValor 
                  Enabled         =   0   'False
                  Height          =   285
                  Left            =   480
                  Picture         =   "frmDetalleCampos.frx":1F18
                  Style           =   1  'Graphical
                  TabIndex        =   25
                  Top             =   0
                  Width           =   321
               End
            End
            Begin VB.TextBox txtMax 
               Enabled         =   0   'False
               Height          =   285
               Left            =   4380
               TabIndex        =   15
               Top             =   225
               Width           =   1272
            End
            Begin VB.TextBox txtMin 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1530
               TabIndex        =   14
               Top             =   225
               Width           =   1272
            End
            Begin VB.OptionButton optLibre 
               BackColor       =   &H00808000&
               Caption         =   "DIntroducci�n libre"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   0
               TabIndex        =   13
               Top             =   -30
               Width           =   4000
            End
            Begin VB.Frame FraListasEnlazadas 
               BackColor       =   &H00808000&
               BorderStyle     =   0  'None
               Caption         =   "Frame2"
               Height          =   495
               Left            =   0
               TabIndex        =   31
               Top             =   1080
               Width           =   4335
               Begin SSDataWidgets_B.SSDBCombo sdbcListaCampoPadre 
                  Height          =   285
                  Left            =   1530
                  TabIndex        =   32
                  Top             =   120
                  Width           =   2715
                  DataFieldList   =   "Column 1"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  AllowNull       =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   -2147483640
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "ID"
                  Columns(0).Name =   "ID"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DENOMINACION"
                  Columns(1).Name =   "DENOMINACION"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "TIPO_CAMPO_GS"
                  Columns(2).Name =   "TIPO_CAMPO_GS"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   4784
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  Enabled         =   0   'False
               End
               Begin VB.Label lblCampoPadre 
                  AutoSize        =   -1  'True
                  BackColor       =   &H00808000&
                  Caption         =   "dCampo padre:"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   195
                  Left            =   0
                  TabIndex        =   33
                  Top             =   120
                  Width           =   1110
               End
            End
            Begin VB.Label lblValorComprob 
               BackColor       =   &H00808000&
               Caption         =   "DValor comprobaci�n:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   4560
               TabIndex        =   35
               Top             =   1080
               Visible         =   0   'False
               Width           =   1590
            End
            Begin VB.Label lblFormato 
               BackColor       =   &H00808000&
               Caption         =   "DFormato:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   4920
               TabIndex        =   34
               Top             =   1320
               Visible         =   0   'False
               Width           =   1335
            End
            Begin VB.Label lblMax 
               BackColor       =   &H00808000&
               Caption         =   "DM�ximo"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   3000
               TabIndex        =   17
               Top             =   270
               Width           =   1275
            End
            Begin VB.Label lblMin 
               BackColor       =   &H00808000&
               Caption         =   "DM�nimo"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   240
               TabIndex        =   16
               Top             =   270
               Width           =   1035
            End
         End
         Begin VB.CheckBox chkRecordarValores 
            BackColor       =   &H00808000&
            Caption         =   "DRecordar valores"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4380
            TabIndex        =   23
            Top             =   300
            Visible         =   0   'False
            Width           =   2055
         End
         Begin VB.Frame fraMaxLength 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   465
            Left            =   20
            TabIndex        =   8
            Top             =   690
            Width           =   5535
            Begin VB.TextBox txtMaxLength 
               Height          =   315
               Left            =   1890
               MaxLength       =   5
               TabIndex        =   18
               Top             =   -15
               Width           =   600
            End
            Begin MSComCtl2.UpDown UpdMaxLength 
               Height          =   315
               Left            =   2490
               TabIndex        =   19
               Top             =   15
               Width           =   255
               _ExtentX        =   450
               _ExtentY        =   556
               _Version        =   393216
               Value           =   1
               BuddyControl    =   "txtMaxLength"
               BuddyDispid     =   196642
               OrigLeft        =   2760
               OrigTop         =   70
               OrigRight       =   3000
               OrigBottom      =   385
               Max             =   4000
               Min             =   1
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin VB.Label lblHastaCaracteres 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "D(hasta 25 caracteres)"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   3090
               TabIndex        =   10
               Top             =   30
               Width           =   1665
            End
            Begin VB.Label lblMaxLength 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DN� m�ximo de caracteres"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   -30
               TabIndex        =   9
               Top             =   30
               Width           =   1905
            End
         End
         Begin VB.PictureBox picTipo 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   0
            ScaleHeight     =   315
            ScaleWidth      =   4335
            TabIndex        =   6
            Top             =   270
            Width           =   4335
            Begin SSDataWidgets_B.SSDBCombo sdbcTipo 
               Height          =   285
               Left            =   1380
               TabIndex        =   0
               Top             =   0
               Width           =   2712
               DataFieldList   =   "Column 1"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               MaxDropDownItems=   9
               AllowInput      =   0   'False
               AllowNull       =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   -2147483640
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4736
               Columns(1).Caption=   "TIPO"
               Columns(1).Name =   "TIPO"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4784
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblTipo 
               BackColor       =   &H00808000&
               Caption         =   "DTipo:"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   7
               Top             =   20
               Width           =   1275
            End
         End
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValoresLista 
      Height          =   1140
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   3510
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmDetalleCampos.frx":225A
      BevelColorFace  =   12632256
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "CAMPO_PADRE"
      Columns(0).Name =   "CAMPO_PADRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ORDEN"
      Columns(1).Name =   "ORDEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "TEXTO"
      Columns(2).Name =   "TEXTO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   6191
      _ExtentY        =   2011
      _StockProps     =   77
   End
End
Attribute VB_Name = "frmDetalleCampos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oIdiomas As CIdiomas

Public g_oCampo As CFormItem
Public g_oCampoPredef As CCampoPredef
Public g_bModif As Boolean

Public g_bCampoEnUso As Boolean
Public g_sOrigen As String

Private m_bRespetarLista As Boolean
Private m_bModifLista As Boolean
Private m_bSaltarFoco As Boolean 'Variable que sirve para saltarse el lostFocus
Private m_bError As Boolean

'Variables de idiomas:
Private m_sIdiTipo(2 To 11) As String
Private m_sIdiTp As String
Private m_sIdiValor As String
Private m_sPeso As String
Private m_sMensaje(1 To 4) As String
Private m_sIdiMaximo As String
Private m_sIdiMinimo As String
Private m_sMensajeHasta(1 To 3) As String
Private m_bMultiIdioma As Boolean

Private m_sMaxLength As String
Private m_sTipo As String
Private m_sOrdenEliminado As String 'Variable que almacena el Orden de los elementos de la lista eliminado (Ya existentes en BBDD)
Private m_lCampoPadreSeleccionado As Long
Private m_sTextoCampoPadreSeleccionado As String
Private m_iTipoCampoPadre As Integer

Private m_iNumeroPadres As Integer
Private m_sListaIdiomaTexto As String

Sub ComprobarValoresLista()
    Dim i As Integer
    Dim oIdioma As CIdioma
    Dim bMensajeMostrado As Boolean 'Por si hay varios valores de la lista que superan la maxima longitud, que s�lo nos muestre una vez el mensaje
    Dim irespuesta As Integer
    Dim iCol As Integer
    
    If txtMaxLength.Text = "" Then Exit Sub
    
    If sdbgLista.Rows > 0 Then
        iCol = sdbgLista.col
        
        sdbgLista.MoveFirst
                
        If m_bMultiIdioma And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                For Each oIdioma In g_oIdiomas
                    If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                        If Len(sdbgLista.Columns(oIdioma.Cod).Value) > CInt(txtMaxLength.Text) Then
                            If Not bMensajeMostrado Then
                                irespuesta = oMensajes.TruncarValor
                            Else
                                irespuesta = vbYes
                            End If
                            If irespuesta = vbYes Then
                                m_bModifLista = True
                                bMensajeMostrado = True
                                sdbgLista.Columns(oIdioma.Cod).Value = Left(sdbgLista.Columns(oIdioma.Cod).Value, CInt(txtMaxLength.Text))
                            Else
                                txtMaxLength.Text = txtMaxLength.Text + 1
                                If Me.Visible Then txtMaxLength.SetFocus
                                UpdMaxLength.Enabled = True
                                m_bSaltarFoco = False
                                Exit Sub
                            End If
                        End If
                    End If
                Next
                sdbgLista.MovePrevious
            Next i
            sdbgLista.Update
        Else
            For i = 0 To sdbgLista.Rows
                m_bSaltarFoco = True
                If Len(sdbgLista.Columns("VALOR").Value) > CInt(txtMaxLength.Text) Then
                    If Not bMensajeMostrado Then
                        irespuesta = oMensajes.TruncarValor
                    Else
                        irespuesta = vbYes
                    End If
                    If irespuesta = vbYes Then
                        bMensajeMostrado = True
                        m_bModifLista = True
                        sdbgLista.Columns("VALOR").Value = Left(sdbgLista.Columns("VALOR").Value, CInt(txtMaxLength.Text))
                        m_bSaltarFoco = False
                    Else
                        txtMaxLength.Text = NullToStr(g_oCampo.MaxLength)
                        If Me.Visible Then txtMaxLength.SetFocus
                        m_bSaltarFoco = False
                        UpdMaxLength.Enabled = True
                        Exit Sub
                    End If
                End If
                sdbgLista.MoveNext
                If m_bError Then
                    UpdMaxLength.Enabled = True
                    If Me.Visible Then sdbgLista.SetFocus
                    Exit Sub
                End If
            Next i
            sdbgLista.MoveFirst
        End If
        
        sdbgLista.col = iCol
    End If
End Sub

Private Sub chkNoFecAntSis_Click()
g_oCampo.NoFecAntSis = SQLBinaryToBoolean(chkNoFecAntSis.Value)
End Sub

Private Sub chkRecordarValores_Click()
    ComprobarOptLista
End Sub

''' <summary>
''' Graba el campo.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim bBorrarValor As Boolean
    Dim bBorrarAdjuntos As Boolean
    Dim irespuesta As Integer
    Dim i As Integer
    Dim vbm As Variant
    Dim bModifAtrib As Boolean
    Dim oValores As CCampoValorListas
    Dim oValoresTxt As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim iAccion As Integer
    Dim arGMNs() As String
    Dim j As Integer
    Dim k As Integer
    Dim iOrdenCampoPadre As Integer
    Dim iOrden As Integer
    Dim iOrdenAnt As Integer
                        
    If sdbgLista.DataChanged Then
        m_bError = False
        sdbgLista.Update
        If m_bError Then
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If

    If txtMaxLength.Text <> "" Then
        If Comprobar_txtMaxLength = False Then
            If Me.Visible Then txtMaxLength.SetFocus
            Exit Sub
        End If
    End If

    'Comprobacion del valor minimo
    If txtMin.Enabled Then
        If txtMin.Text <> "" Then
            If Comprobar_txtMin = False Then
                If Me.Visible Then txtMin.SetFocus
                Exit Sub
            End If
        End If
    End If
    
    'Comprobacion del valor m�ximo
    If txtMax.Enabled Then
        If txtMax.Text <> "" Then
            If Comprobar_txtMax = False Then
                If Me.Visible Then txtMax.SetFocus
                Exit Sub
            End If
        End If
    End If
        
    m_bError = False
    
    If txtMin.Text <> "" And txtMax.Text <> "" Then
        If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
            If CDbl(txtMin.Text) > CDbl(txtMax.Text) Then
                oMensajes.NoValido m_sIdiMinimo & ". " & m_sMensaje(4)
                If Me.Visible Then txtMin.SetFocus
                Exit Sub
            End If
        Else
            If CDate(txtMin.Text) > CDate(txtMax.Text) Then
                oMensajes.NoValido m_sIdiMinimo & ". " & m_sMensaje(4)
                If Me.Visible Then txtMin.SetFocus
                Exit Sub
            End If
        End If
    End If
            
    'Si es de tipo lista comprueba que se haya introducido alg�n valor a la lista:
    
    If Me.optLista.Value And g_oCampo.TipoPredef <> Atributo Then
        If sdbgLista.Rows = 0 Then
            oMensajes.IntroducirListaValores
            Exit Sub
        Else
            sdbgLista.MoveFirst
            If m_bError Then
                If Me.Visible Then sdbgLista.SetFocus
                Exit Sub
            End If
            
            If g_oCampo.Grupo.Formulario.Multiidioma Then
                If (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
                    If sdbgLista.Columns(CStr(gParametrosInstalacion.gIdioma)).Value = "" Then
                        oMensajes.IntroducirListaValores
                        Exit Sub
                    End If
                Else
                    If sdbgLista.Columns("VALOR").Value = "" Then
                        oMensajes.IntroducirListaValores
                        Exit Sub
                    End If
                End If
            Else
                If sdbgLista.Columns("VALOR").Value = "" Then
                    oMensajes.IntroducirListaValores
                    Exit Sub
                End If
            End If
            
            If g_oCampo.Peso Then
                'Comprobar los pesos para cada valor de la lista
                For i = 0 To sdbgLista.Rows - 1
                    vbm = sdbgLista.AddItemBookmark(i)
                    If sdbgLista.Columns("ORDEN").CellValue(vbm) = "" Then
                        oMensajes.IntroducirListaPesos
                        Exit Sub
                    End If
                Next i
            End If
        End If
    End If
    
    'Guarda en BD los cambios realizados en el campo.Antes de eso comprueba que:
    '1-Si ha cambiado el tipo del campo comprueba que no haya valores ya introducidos.si es as� tendr� que borrarlos:
    '2-Si ha cambiado el m�nimo y el m�ximo comprueba que el valor existente est� dentro de estos valores
    '3-Comprueba que el valor existente est� dentro de la lista de valores:
    
    Select Case sdbcTipo.Columns("ID").Value
        Case TiposDeAtributos.TipoBoolean
            If Not IsNull(g_oCampo.valorBool) Then
                If sdbcTipo.Columns("ID").Value <> g_oCampo.Tipo Then bBorrarValor = True
            End If
            
        Case TiposDeAtributos.TipoFecha
            If Not IsNull(g_oCampo.valorFec) Then
                If sdbcTipo.Columns("ID").Value <> g_oCampo.Tipo Then bBorrarValor = True
            
                'si ha cambiado el m�nimo y el m�ximo comprueba que el valor existente est� dentro de estos valores
                If bBorrarValor = False And (StrToNull(Trim(txtMax.Text)) <> g_oCampo.Maximo Or StrToNull(Trim(txtMin.Text)) <> g_oCampo.Minimo) Then
                    If Trim(txtMax.Text) <> "" Then
                        If CDate(g_oCampo.valorFec) > CDate(Trim(txtMax.Text)) Then bBorrarValor = True
                    End If
                    If Trim(txtMin.Text) <> "" Then
                        If CDate(g_oCampo.valorFec) < CDate(Trim(txtMin.Text)) Then bBorrarValor = True
                    End If
                End If
                
                'Comprueba que el valor existente est� dentro de la lista de valores:
                If bBorrarValor = False Then
                    For i = 0 To sdbgLista.Rows - 1
                        bBorrarValor = True
                        vbm = sdbgLista.AddItemBookmark(i)
                        If CDate(sdbgLista.Columns("VALOR").CellValue(i)) = CDate(g_oCampo.valorFec) Then
                            g_oCampo.valorNum = i + 1
                            bBorrarValor = False
                            Exit For
                        End If
                    Next i
                End If
            End If
            
        Case TiposDeAtributos.TipoNumerico
            If Not IsNull(g_oCampo.valorNum) Or Not IsNull(g_oCampo.valorText) Or Not IsNull(g_oCampo.valorFec) Or Not IsNull(g_oCampo.valorBool) Then
                If sdbcTipo.Columns("ID").Value <> g_oCampo.Tipo Then bBorrarValor = True
            End If
            If Not IsNull(g_oCampo.valorNum) Then
                'If sdbcTipo.Columns("ID").Value <> g_oCampo.Tipo Then bBorrarValor = True
            
                'si ha cambiado el m�nimo y el m�ximo comprueba que el valor existente est� dentro de estos valores
                If bBorrarValor = False And (StrToNull(Trim(txtMax.Text)) <> g_oCampo.Maximo Or StrToNull(Trim(txtMin.Text)) <> g_oCampo.Minimo) Then
                    If Trim(txtMax.Text) <> "" Then
                        If CDbl(g_oCampo.valorNum) > CDbl(Trim(txtMax.Text)) Then bBorrarValor = True
                    End If
                    If Trim(txtMin.Text) <> "" Then
                        If CDbl(g_oCampo.valorNum) < CDbl(Trim(txtMin.Text)) Then bBorrarValor = True
                    End If
                End If
                'Comprueba que el valor existente est� dentro de la lista de valores:
                If bBorrarValor = False Then
                    For i = 0 To sdbgLista.Rows - 1
                        bBorrarValor = True
                        vbm = sdbgLista.AddItemBookmark(i)
                        If CDbl(sdbgLista.Columns("VALOR").CellValue(i)) = CDbl(g_oCampo.valorNum) Then
                            bBorrarValor = False
                            Exit For
                        End If
                    Next i
                End If
            End If
            
        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
            If Not IsNull(g_oCampo.valorText) And Not (g_oCampo.valorText = "") And Not (txtMaxLength.Text = "") Then
                If sdbcTipo.Columns("ID").Value <> g_oCampo.Tipo Then
                    If Not ((g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto And sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio) Or _
                       (g_oCampo.Tipo = TiposDeAtributos.TipoString And sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo) Or _
                       (g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio And sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo)) Then
                            If (sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoTextoCorto) And sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoTextoMedio Or sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoTextoLargo Then
                                bBorrarValor = True
                            Else
                                If Not IsNull(g_oCampo.valorText) Then
                                    If Len(g_oCampo.valorText) > CInt(txtMaxLength.Text) Then
                                        bBorrarValor = True
                                    End If
                                End If
                            End If
                    End If
                Else
                    If Not IsNull(g_oCampo.valorText) And Not (g_oCampo.valorText = "") Then
                        If Len(g_oCampo.valorText) > CLng(txtMaxLength.Text) Then
                            bBorrarValor = True
                        End If
                    End If
                
                End If
                'Comprueba que el valor existente est� dentro de la lista de valores:
                If bBorrarValor = False Then
                    For i = 0 To sdbgLista.Rows - 1
                        bBorrarValor = True
                        vbm = sdbgLista.AddItemBookmark(i)
                        If g_oCampo.Grupo.Formulario.Multiidioma = True And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
                            If sdbgLista.Columns(CStr(gParametrosInstalacion.gIdioma)).CellValue(i) = g_oCampo.valorText Then
                                g_oCampo.valorNum = i + 1
                                bBorrarValor = False
                                Exit For
                            End If
                        Else
                            If sdbgLista.Columns("VALOR").CellValue(i) = g_oCampo.valorText Then
                                g_oCampo.valorNum = i + 1
                                bBorrarValor = False
                                Exit For
                            End If
                        End If
                    Next i
                End If
            Else
                If Not (txtMaxLength.Text = "") Then
                    If g_sOrigen = "frmDesglose" And (g_oCampo.MaxLength > CLng(txtMaxLength.Text)) Then
                        'bBorrarValor = True
                        Dim lCont As Long
                        
                        lCont = g_oCampo.ObtenerCampoDesgloseTextoLongitudSuperior(txtMaxLength.Text)
                        
                        If lCont > 0 Then
                            If oMensajes.PreguntaEliminarValorCampo = vbYes Then
                                g_oCampo.EliminarCampoTextoLongitudInferior (txtMaxLength.Text)
                            Else
                                If Me.Visible Then txtMaxLength.SetFocus
                                m_bSaltarFoco = False
                                Exit Sub
                            End If
                        Else
                            irespuesta = vbYes
                        End If
                    End If
                End If
            End If
            
        Case TiposDeAtributos.TipoArchivo
            'Si tiene archivos los borrar�
            If sdbcTipo.Columns("ID").Value <> g_oCampo.Tipo Then
                g_oCampo.CargarAdjuntos
                If g_oCampo.Adjuntos.Count > 0 Then
                    bBorrarValor = True
                    bBorrarAdjuntos = True
                End If
            End If
    End Select

    If sdbcTipo.Columns("ID").Value <> g_oCampo.Tipo Then
        If Not g_oCampo.CampoPadre Is Nothing Then
            If g_sOrigen = "frmDesglose" Then
                g_oCampo.eliminarCampoSolicitudFavoritas 1
            End If
        Else
            If g_sOrigen = "frmFormularios" Then
               g_oCampo.eliminarCampoSolicitudFavoritas 0
            End If
        End If
    End If
            
    If bBorrarValor Then
        If bBorrarAdjuntos Then
            irespuesta = oMensajes.PreguntaEliminarAdjuntosCampo
        Else
            irespuesta = oMensajes.PreguntaEliminarValorCampo
        End If
        If irespuesta = vbNo Then Exit Sub
    End If
        
    'si es un atributo modificar� su definici�n:
    If g_oCampo.TipoPredef = TipoCampoPredefinido.Atributo Then
        If (optLibre.Value = True And g_oCampo.TipoIntroduccion = Introselec) Or (optLista.Value And g_oCampo.TipoIntroduccion = IntroLibre) Or _
           NullToStr(g_oCampo.Maximo) <> Trim(txtMax.Text) Or NullToStr(g_oCampo.Minimo) <> Trim(txtMin.Text) Then
            irespuesta = oMensajes.ModifDefinicionAtrib
            If irespuesta = vbYes Then
                bModifAtrib = True
            End If
        End If
    End If

    If bBorrarValor Then
        Select Case g_oCampo.Tipo
            Case TiposDeAtributos.TipoFecha
                g_oCampo.valorFec = Null
            Case TiposDeAtributos.TipoNumerico
                g_oCampo.valorNum = Null
            Case TiposDeAtributos.TipoBoolean
                g_oCampo.valorBool = Null
            Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                g_oCampo.valorText = Null
                If g_oCampo.TipoIntroduccion = Introselec Then
                    g_oCampo.valorNum = Null
                End If
        End Select
    End If
    
    g_oCampo.Maximo = Trim(txtMax.Text)
    g_oCampo.Minimo = Trim(txtMin.Text)
    g_oCampo.Formato = txtFormato.Text
    g_oCampo.Tipo = sdbcTipo.Columns("ID").Value
    g_oCampo.MaxLength = txtMaxLength.Text
    If optLibre.Value Then
        g_oCampo.TipoIntroduccion = IntroLibre
    Else
        If optListaExterna.Value Then
            g_oCampo.TipoIntroduccion = IntroListaExterna
        Else
            g_oCampo.TipoIntroduccion = Introselec
        End If
    End If
    
    'Carga la colecci�n de valores de lista:
    If optLista.Value Then
        If m_bModifLista Then
            If Me.sdbcListaCampoPadre.Columns("ID").Value = "" Then
                g_oCampo.IDListaCampoPadre = 0
            Else
                g_oCampo.IDListaCampoPadre = Me.sdbcListaCampoPadre.Columns("ID").Value
            End If
            
            sdbgLista.MoveFirst
            Set oValores = oFSGSRaiz.Generar_CCampoValorListas
            For i = 0 To sdbgLista.Rows - 1
                vbm = sdbgLista.AddItemBookmark(i)
                iOrden = IIf(g_oCampo.Peso, sdbgLista.Columns("ORDEN").CellValue(vbm), i + 1)
                iOrdenAnt = IIf(g_oCampo.Peso, StrToDbl0(sdbgLista.Columns("ORDEN_ANT").CellValue(vbm)), StrToDbl0(sdbgLista.Columns("ORDEN").CellValue(vbm)))
                
                If m_iTipoCampoPadre = TipoCampoGS.material Then
                    'Comprobar valores introducidos
                    j = 0
                    k = 0
                    While (j < sdbgLista.Rows)
                        For k = sdbgLista.Columns.Count - 1 To 0 Step -1
                            If Left(sdbgLista.Columns(k).Name, 16) = "CAMPOPADREVALOR_" Then
                                If sdbgLista.Columns(k).Value = "" Then
                                    oMensajes.IntroducirCampoPadre
                                    Exit Sub
                                End If
                            End If
                        Next k
                        
                        j = j + 1
                    Wend
                        
                    'Origen campo material
                    arGMNs = Split(sdbgLista.Columns("CAMPOPADREVALOR_" & g_oCampo.IDListaCampoPadre).CellValue(vbm), "|")
                    ReDim Preserve arGMNs(0 To 3)
                    
                    Select Case g_oCampo.Tipo
                        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                            Set oValoresTxt = oFSGSRaiz.Generar_CMultiidiomas
                            For Each oIdioma In g_oIdiomas
                                If g_oCampo.Grupo.Formulario.Multiidioma And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
                                    If sdbgLista.Columns(CStr(oIdioma.Cod)).CellValue(vbm) <> "" Then
                                        oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns(CStr(oIdioma.Cod)).CellValue(vbm)
                                    End If
                                Else
                                    If sdbgLista.Columns("VALOR").CellValue(vbm) <> "" Then
                                        oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns("VALOR").CellValue(vbm)
                                    End If
                                End If
                            Next
                                                      
                            If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                                oValores.Add iOrden, g_oCampo, , oValoresTxt, , , , , g_oCampo.IDListaCampoPadre, , iOrdenAnt, arGMNs(0), arGMNs(1), arGMNs(2), arGMNs(3)
                            Else
                                oValores.Add iOrden, g_oCampo, , oValoresTxt, , , , , g_oCampo.IDListaCampoPadre, , , arGMNs(0), arGMNs(1), arGMNs(2), arGMNs(3)
                            End If
                            Set oValoresTxt = Nothing
                            
                        Case TiposDeAtributos.TipoNumerico
                            If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                                    oValores.Add iOrden, g_oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , , , g_oCampo.IDListaCampoPadre, , iOrdenAnt, arGMNs(0), arGMNs(1), _
                                                arGMNs(2), arGMNs(3)
                                Else
                                    oValores.Add iOrden, g_oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , , , g_oCampo.IDListaCampoPadre, , , arGMNs(0), arGMNs(1), arGMNs(2), arGMNs(3)
                                End If
                            End If
                            
                        Case TiposDeAtributos.TipoFecha
                            If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    oValores.Add iOrden, g_oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , g_oCampo.IDListaCampoPadre, , iOrdenAnt, arGMNs(0), arGMNs(1), _
                                                arGMNs(2), arGMNs(3)
                                Else
                                    oValores.Add iOrden, g_oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , g_oCampo.IDListaCampoPadre, , , arGMNs(0), arGMNs(1), arGMNs(2), arGMNs(3)
                                End If
                            End If
                    End Select
                Else
                    'Origen campo lista
                    If g_oCampo.IDListaCampoPadre > 0 Then
                        j = 0
                        k = 0
                        While (j < sdbgLista.Rows)
                            For k = sdbgLista.Columns.Count - 1 To 0 Step -1
                                If Left(sdbgLista.Columns(k).Name, 11) = "ORDENPADRE_" Then
                                    If sdbgLista.Columns(k).Value = "" Then
                                        oMensajes.IntroducirCampoPadre
                                        Exit Sub
                                    End If
                                End If
                            Next k
                            
                            j = j + 1
                        Wend
                        
                        iOrdenCampoPadre = sdbgLista.Columns("ORDENPADRE_" & g_oCampo.IDListaCampoPadre).CellValue(vbm)
                        Select Case g_oCampo.Tipo
                            Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                                Set oValoresTxt = oFSGSRaiz.Generar_CMultiidiomas
                                For Each oIdioma In g_oIdiomas
                                    If g_oCampo.Grupo.Formulario.Multiidioma = True And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
                                        If sdbgLista.Columns(CStr(oIdioma.Cod)).CellValue(vbm) <> "" Then
                                            oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns(CStr(oIdioma.Cod)).CellValue(vbm)
                                        End If
                                    Else
                                        If sdbgLista.Columns("VALOR").CellValue(vbm) <> "" Then
                                            oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns("VALOR").CellValue(vbm)
                                        End If
                                    End If
                                Next
                                If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                                    oValores.Add iOrden, g_oCampo, , oValoresTxt, , , , , g_oCampo.IDListaCampoPadre, iOrdenCampoPadre, lordenanterior:=iOrdenAnt
                                Else
                                    oValores.Add iOrden, g_oCampo, , oValoresTxt, , , , , g_oCampo.IDListaCampoPadre, iOrdenCampoPadre
                                End If
                                
                                Set oValoresTxt = Nothing
                                
                            Case TiposDeAtributos.TipoNumerico
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                                        oValores.Add iOrden, g_oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , , , g_oCampo.IDListaCampoPadre, iOrdenCampoPadre, lordenanterior:=iOrdenAnt
                                    Else
                                        oValores.Add iOrden, g_oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , , , g_oCampo.IDListaCampoPadre, iOrdenCampoPadre
                                    End If
                                End If
                            Case TiposDeAtributos.TipoFecha
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                                        oValores.Add iOrden, g_oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , g_oCampo.IDListaCampoPadre, iOrdenCampoPadre, lordenanterior:=iOrdenAnt
                                    Else
                                        oValores.Add iOrden, g_oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), , , , g_oCampo.IDListaCampoPadre, iOrdenCampoPadre
                                    End If
                                End If
                        End Select
                    Else
                        Select Case g_oCampo.Tipo
                            Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                                Set oValoresTxt = oFSGSRaiz.Generar_CMultiidiomas
                                For Each oIdioma In g_oIdiomas
                                    If g_oCampo.Grupo.Formulario.Multiidioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
                                        If sdbgLista.Columns(CStr(oIdioma.Cod)).CellValue(vbm) <> "" Then
                                            oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns(CStr(oIdioma.Cod)).CellValue(vbm)
                                        End If
                                    Else
                                        If sdbgLista.Columns("VALOR").CellValue(vbm) <> "" Then
                                            oValoresTxt.Add oIdioma.Cod, sdbgLista.Columns("VALOR").CellValue(vbm)
                                        End If
                                    End If
                                Next
                                If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                                    oValores.Add iOrden, g_oCampo, , oValoresTxt, lordenanterior:=iOrdenAnt
                                Else
                                    oValores.Add iOrden, g_oCampo, , oValoresTxt
                                End If
                                
                                Set oValoresTxt = Nothing
                                
                            Case TiposDeAtributos.TipoNumerico
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                                        oValores.Add iOrden, g_oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), lordenanterior:=iOrdenAnt
                                    Else
                                        oValores.Add iOrden, g_oCampo, Trim(sdbgLista.Columns("VALOR").CellValue(vbm))
                                    End If
                                End If
                            Case TiposDeAtributos.TipoFecha
                                If Trim(sdbgLista.Columns("VALOR").CellValue(vbm)) <> "" Then
                                    If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                                        oValores.Add iOrden, g_oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm)), lordenanterior:=iOrdenAnt
                                    Else
                                        oValores.Add iOrden, g_oCampo, , , Trim(sdbgLista.Columns("VALOR").CellValue(vbm))
                                    End If
                                End If
                        End Select
                    
                    End If
                End If
                
                If sdbgLista.Columns("EXISTE").CellValue(vbm) = "1" Then
                    iAccion = 1
                Else
                    iAccion = 0
                End If
                oValores.Item(i + 1).AccionElementoLista = iAccion
            Next i
            
            Set g_oCampo.ValoresLista = oValores
        End If
    Else
        Set g_oCampo.ValoresLista = Nothing
        g_oCampo.IDListaCampoPadre = 0
    End If
    
    If m_sOrdenEliminado <> "" Then
        m_sOrdenEliminado = Left(m_sOrdenEliminado, Len(m_sOrdenEliminado) - 1)
    End If
        
    If chkRecordarValores.Value = vbChecked Then
        g_oCampo.RecordarValores = True
    Else
        g_oCampo.RecordarValores = False
    End If
            
    If g_sOrigen = "frmDesglose" Then
        teserror = g_oCampo.ModificarDefinicionCampo(bModifAtrib, m_bModifLista, m_sOrdenEliminado, True)
    Else
        teserror = g_oCampo.ModificarDefinicionCampo(bModifAtrib, m_bModifLista, m_sOrdenEliminado, False)
    End If
    
    If teserror.NumError = TESnoerror Then
        If g_sOrigen = "frmFormularios" Then
            'actualizar los cambios en la grid de los formularios
            frmFormularios.g_bUpdate = False
            frmFormularios.sdbgCampos.Columns("SUBTIPO").Value = g_oCampo.Tipo
            frmFormularios.sdbgCampos.Columns("INTRO").Value = g_oCampo.TipoIntroduccion
            If bBorrarValor = True Then
                frmFormularios.sdbgCampos.Columns("VALOR").Value = ""
            End If
            frmFormularios.sdbgCampos.Update
            frmFormularios.g_bUpdate = True
            RegistrarAccion ACCFormItemModif, "Id:" & g_oCampo.Id
            
        ElseIf g_sOrigen = "frmDesglose" Then
            'Es de desglose
            RegistrarAccion ACCDesgloseCampoModif, "Id:" & g_oCampo.Id
        End If
    Else
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If
    
    Set oValores = Nothing
    Set oValoresTxt = Nothing
    
    Unload Me
End Sub

Private Sub cmdAnyaValor_Click()
    sdbgLista.Scroll 0, sdbgLista.Rows - sdbgLista.Row
    
    If sdbgLista.VisibleRows > 0 Then
        If sdbgLista.VisibleRows > sdbgLista.Rows Then
            sdbgLista.Row = sdbgLista.Rows
        Else
            sdbgLista.Row = sdbgLista.Rows - (sdbgLista.Rows - sdbgLista.VisibleRows) - 1
        End If
        
        m_bModifLista = True
    End If

    If Me.Visible Then sdbgLista.SetFocus
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdComprobar_Click()
Dim rex As RegExp
Set rex = New RegExp

Dim colMatches   As MatchCollection
Dim objMatch As Match
Dim RetStr As String

If (txtFormato.Text <> "") Then

    rex.Pattern = txtFormato.Text
    
    ' Set Case Insensitivity.
    rex.IgnoreCase = True
    
    'Set global applicability.
    rex.Global = True
    
    'Test whether the String can be compared.
    If (rex.Test(txtValorComprob.Text) = True) Then
    
    'Get the matches.
    Set colMatches = rex.Execute(txtValorComprob.Text)   ' Execute search.
    
    For Each objMatch In colMatches   ' Iterate Matches collection.
        RetStr = RetStr & "Match found at position "
        RetStr = RetStr & objMatch.FirstIndex & ". Match Value is '"
        RetStr = RetStr & objMatch.Value & "'." & vbCrLf
    Next
        txtValorComprob.Backcolor = &HC000&
    Else
        RetStr = "String Matching Failed"
        txtValorComprob.Backcolor = &HFF&
    End If
    
    txtValorComprob.SetFocus

Else

    txtValorComprob.Backcolor = &H80000005

End If
End Sub

Private Sub cmdElimValor_Click()
Dim sCampoRelacionado As String
Dim sCampo As String

    If sdbgLista.Rows > 0 Then
    
        If g_oCampo.Grupo.Formulario.Multiidioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
            If sdbgLista.Columns(CStr(gParametrosInstalacion.gIdioma)).Value = "" Then Exit Sub
            sCampo = sdbgLista.Columns(CStr(gParametrosInstalacion.gIdioma)).Value
        Else
            If sdbgLista.Columns("VALOR").Value = "" Then Exit Sub
            sCampo = sdbgLista.Columns("VALOR").Value
        End If
    
    
        'Comprobar que el elemento a eliminar no esta en una lista anidada
        If Not g_oCampo Is Nothing Then
            sCampoRelacionado = g_oCampo.DevolverNombreCampoHijo(sdbgLista.AddItemRowIndex(sdbgLista.Bookmark) + 1)
            If sCampoRelacionado <> "" Then
                oMensajes.NoModificarCampoPadre sCampo, sCampoRelacionado
                Exit Sub
            End If
        End If
                
        Screen.MousePointer = vbHourglass
        
        'Almaceno en la variable el orden de la variable que elimino (Si existe anteriormente en BBDD)
        If sdbgLista.Columns("EXISTE").Value = "1" Then
            m_sOrdenEliminado = m_sOrdenEliminado & sdbgLista.Columns("ORDEN").Value & ","
        End If
        
        sdbgLista.SelBookmarks.Add sdbgLista.Bookmark
        
        If sdbgLista.AddItemRowIndex(sdbgLista.Bookmark) > -1 Then
            sdbgLista.RemoveItem (sdbgLista.AddItemRowIndex(sdbgLista.Bookmark))
        Else
            sdbgLista.RemoveItem (0)
        End If
    
        If sdbgLista.Rows > 0 Then
            If IsEmpty(sdbgLista.RowBookmark(sdbgLista.Row)) Then
                sdbgLista.Bookmark = sdbgLista.RowBookmark(sdbgLista.Row - 1)
            Else
                sdbgLista.Bookmark = sdbgLista.RowBookmark(sdbgLista.Row)
            End If
        End If
        sdbgLista.SelBookmarks.RemoveAll
        If Me.Visible Then sdbgLista.SetFocus

        m_bModifLista = True
        
        Screen.MousePointer = vbNormal
        
    End If
End Sub



'Descripcion:=  Traslada la fila seleccionada hacia arriba o hacia abajo.
'               Metemos los valores de la fila en un array y luego nos movemos de fila y metemos los valores de esa lista en otro array, a su vez copiamos los elementos de la primera lista en la nueva fila
'Parametro  index = 0 Subir elemento
'           index = 1 Bajar elemento
'Tiempo ejecucion:=0,15seg.
Private Sub cmdMvtoElemLista_Click(Index As Integer)
Dim sArrText() As String
Dim sArrText2() As String
Dim i As Integer
Dim iTope As Integer

    If sdbgLista.SelBookmarks.Count = 0 Then Exit Sub
    If Index = 0 Then
        If sdbgLista.AddItemRowIndex(sdbgLista.SelBookmarks.Item(0)) = 0 Then Exit Sub
    Else
        If sdbgLista.AddItemRowIndex(sdbgLista.SelBookmarks.Item(0)) = sdbgLista.Rows - 1 Then Exit Sub
    End If
  

    iTope = sdbgLista.Columns.Count - 1
    ReDim Preserve sArrText(iTope)
    ReDim Preserve sArrText2(iTope)
    
    For i = 0 To iTope
        sArrText(i) = sdbgLista.Columns(i).Value
    Next
    If Index = 0 Then
        sdbgLista.MovePrevious
    Else
        sdbgLista.MoveNext
    End If
    For i = 0 To iTope
        sArrText2(i) = sdbgLista.Columns(i).Value
        sdbgLista.Columns(i).Value = sArrText(i)
    Next
    m_bRespetarLista = True
    If Index = 0 Then
        sdbgLista.MoveNext
    Else
        sdbgLista.MovePrevious
    End If
    For i = 0 To iTope
        sdbgLista.Columns(i).Value = sArrText2(i)
    Next
     
    sdbgLista.SelBookmarks.RemoveAll
    If Index = 0 Then
        sdbgLista.MovePrevious
    Else
        sdbgLista.MoveNext
    End If
    m_bRespetarLista = False
    sdbgLista.SelBookmarks.Add sdbgLista.Bookmark
    
    m_bModifLista = True

End Sub

''' <summary>
''' Carga la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    Dim i As Integer
    Dim oIdioma As CIdioma
    Dim sCaption As String
    
    Me.Height = 6570
    Me.Width = 7185
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Select Case g_sOrigen
        Case "frmSolicitudDetalle"
            Me.caption = Me.caption & " " & g_oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
                        
            ConfiguracionControlesTipoLista g_oCampo.TipoIntroduccion
                        
        Case "frmCamposSolic"
            sCaption = NullToStr(g_oCampoPredef.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
            For Each oIdioma In g_oIdiomas
                If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                    If Not IsNull(g_oCampoPredef.Denominaciones.Item(CStr(oIdioma.Cod)).Den) Then
                        sCaption = sCaption & "/" & g_oCampoPredef.Denominaciones.Item(CStr(oIdioma.Cod)).Den
                    End If
                End If
            Next
            Me.caption = Me.caption & " " & sCaption
            fraMaxLength.Visible = False
            fraLista.Top = fraLista.Top - 400
            FraListasEnlazadas.Visible = False
            FrameBotonesGrid.Top = FrameBotonesGrid.Top - 500
            Frame1.Height = Frame1.Height - 500
            picDatos.Height = picDatos.Height - 300
            sdbgLista.Top = sdbgLista.Top - 700
            Me.Height = 5700
            
            ConfiguracionControlesTipoLista g_oCampoPredef.TipoIntroduccion
            
            m_sTipo = g_oCampoPredef.Tipo
            
            m_bMultiIdioma = True
            
        Case Else
            ConfiguracionControlesTipoLista g_oCampo.TipoIntroduccion
            
            If g_oCampo.Grupo.Formulario.Multiidioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoLargo) Then
                sCaption = NullToStr(g_oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
                For Each oIdioma In g_oIdiomas
                    If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                        If Not IsNull(g_oCampo.Denominaciones.Item(CStr(oIdioma.Cod)).Den) Then
                            sCaption = sCaption & "/" & g_oCampo.Denominaciones.Item(CStr(oIdioma.Cod)).Den
                        End If
                    End If
                Next
                Me.caption = Me.caption & " " & sCaption
            Else
                Me.caption = Me.caption & " " & NullToStr(g_oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
            End If
            
            m_sTipo = g_oCampo.Tipo
            
            m_bMultiIdioma = g_oCampo.Grupo.Formulario.Multiidioma
    End Select
    
    For i = 2 To 9
        sdbcTipo.AddItem i & Chr(m_lSeparador) & m_sIdiTipo(i)
    Next i
    
    sdbcTipo.AddItem TiposDeAtributos.TipoEditor & Chr(m_lSeparador) & m_sIdiTipo(10)
    sdbcTipo.AddItem TiposDeAtributos.TipoEnlace & Chr(m_lSeparador) & m_sIdiTipo(11)
    
    CargarDatosCampo
    CargarComboCamposPadre
    ConfigurarGridLista

    If g_bCampoEnUso Then
        sdbcTipo.Enabled = False
        optLibre.Enabled = False
        optLista.Enabled = False
        sdbcListaCampoPadre.Enabled = False
    End If
    
    If g_bModif = False Then
        picEdit.Visible = False
        Me.Height = Me.Height - picEdit.Height
        picDatos.Enabled = False
        sdbcListaCampoPadre.Enabled = False
        sdbgLista.AllowUpdate = False
    Else
        If (g_sOrigen = "frmFormularios") And g_oCampo.TipoPredef = Atributo Then
            picEdit.Visible = True
            picTipo.Enabled = False
            fraLista.Enabled = False
            sdbgLista.AllowUpdate = False
            fraMaxLength.Enabled = True
        ElseIf g_sOrigen = "frmDesglose" Then
            lblCampoPadre.Visible = False
            sdbcListaCampoPadre.Visible = False
        End If
    End If
    MostrarOpcionRecordarValores
    
    m_bModifLista = False
    m_bSaltarFoco = False
    
    m_sTipo = ""
    m_sMaxLength = ""
    
    Dim NumeroColumnas As Integer
    m_iNumeroPadres = 0
    m_sListaIdiomaTexto = ""
    
    If m_bMultiIdioma Then
        Select Case sdbcTipo.Columns("ID").Value
        Case TiposDeAtributos.TipoFecha
            NumeroColumnas = sdbgLista.Columns.Count
        Case TiposDeAtributos.TipoNumerico
            NumeroColumnas = sdbgLista.Columns.Count
        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
            NumeroColumnas = sdbgLista.Columns.Count - g_oIdiomas.Count + 1
            
            For Each oIdioma In g_oIdiomas
                m_sListaIdiomaTexto = m_sListaIdiomaTexto & oIdioma.Cod & "##"
            Next
        End Select
    Else
        NumeroColumnas = sdbgLista.Columns.Count
    End If
    If NumeroColumnas > 3 Then
        'Compara Padre
        m_iNumeroPadres = (NumeroColumnas - 3) / 3
    End If
End Sub

''' <summary>Configura la visibilidad y posicion de los controles de tipo lista</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub ConfiguracionControlesTipoLista(ByVal TipoIntroduccion As TAtributoIntroduccion)
    Dim bListaExt As Boolean
    
    bListaExt = (TipoIntroduccion = IntroListaExterna)
    optListaExterna.Visible = bListaExt
    optLista.Visible = Not bListaExt
    lblCampoPadre.Visible = Not bListaExt
    sdbcListaCampoPadre.Visible = Not bListaExt
    cmdImportarExcel.Visible = Not bListaExt
    cmdAnyaValor.Visible = Not bListaExt
    cmdElimValor.Visible = Not bListaExt
    cmdMvtoElemLista(0).Visible = Not bListaExt
    cmdMvtoElemLista(1).Visible = Not bListaExt
    sdbgLista.Visible = Not bListaExt
End Sub

Private Sub Form_Resize()
    optListaExterna.Top = optLista.Top
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oIdiomas = Nothing
    Set g_oCampo = Nothing
    Set g_oCampoPredef = Nothing
    g_sOrigen = ""
    m_lCampoPadreSeleccionado = 0
    m_sTextoCampoPadreSeleccionado = ""
    m_iTipoCampoPadre = 0
    m_sOrdenEliminado = ""
    g_bModif = False
End Sub



Private Sub optLibre_Click()
Dim sCampoHijo As String

    If sdbgLista.DataChanged Then
        m_bError = False
        sdbgLista.Update
        If m_bError Then
            sdbgLista.CancelUpdate
            sdbgLista.Refresh
        End If
    End If

    If optLibre.Value = True Then
        chkNoFecAntSis.Enabled = True
        If FraListasEnlazadas.Visible Then
            'Comprobamos si el campo es CampoPadre de otra lista
            If Not g_oCampo.ValoresLista Is Nothing Then
                sCampoHijo = g_oCampo.DevolverNombreCampoHijo
                
                If sCampoHijo <> "" Then
                    oMensajes.NoModificarIntroduccionLibre sCampoHijo
                    'MsgBox "No se puede eliminar mediante selecci�n en lista, " & vbCrLf & "ya que los valores de dicha lista est�n asociados" & vbCrLf & " al campo " & sCampoHijo
                    optLista.Value = True
                    Exit Sub
                End If
                
            End If
        End If
        txtMax.Text = ""
        txtMin.Text = ""
        
        If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
            txtMax.Enabled = True
            txtMin.Enabled = True
        Else
            txtMax.Enabled = False
            txtMin.Enabled = False
        End If
        
        Me.sdbcListaCampoPadre.Enabled = False
        sdbgLista.AllowUpdate = False
        cmdAnyaValor.Enabled = False
        cmdElimValor.Enabled = False
        cmdMvtoElemLista(0).Enabled = False
        cmdMvtoElemLista(1).Enabled = False
        cmdImportarExcel.Enabled = False
        MostrarOpcionRecordarValores
    End If
    m_bSaltarFoco = False
End Sub

Private Sub optLista_Click()
If txtMaxLength.Text <> "" Then
    If Comprobar_txtMaxLength = False Then
        If Me.Visible Then txtMaxLength.SetFocus
        Exit Sub
    End If
    'Si se modifica el n� m�ximo de caracteres permitidos, si los valores definidos no cumplen esta longitud se eliminar�n.
    ComprobarValoresLista
End If
    
    
If optLista.Value Then
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Or _
    sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio Then
        Me.sdbcListaCampoPadre.Enabled = True
        sdbgLista.AllowUpdate = True
        cmdAnyaValor.Enabled = True
        cmdElimValor.Enabled = True
        cmdMvtoElemLista(0).Enabled = True
        cmdMvtoElemLista(1).Enabled = True
        cmdImportarExcel.Enabled = True
    End If
    chkNoFecAntSis.Value = 0
    chkNoFecAntSis.Enabled = False
    txtMax.Text = ""
    txtMin.Text = ""
    txtMax.Enabled = False
    txtMin.Enabled = False
    Me.chkRecordarValores.Value = vbUnchecked
    Me.chkRecordarValores.Visible = False
End If
m_bSaltarFoco = False
End Sub

''' <summary>
''' Habilita/ deshabilita los campos pantalla seg�n el detalle de q campo se trate
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:sdbcTipo_CloseUp     form_load; Tiempo m�ximo:0</remarks>
Private Sub ConfigurarGridLista()
    Dim oIdioma As CIdioma
    Dim iCampoGS As Integer
    Dim sCampoRelacionado As String
    
    iCampoGS = 0
    If Not g_oCampo Is Nothing Then
        iCampoGS = NullToDbl0(g_oCampo.CampoGS)
        If g_oCampo.Peso Then
            sdbgLista.Columns("ORDEN").Visible = True
            If g_oCampo.Tipo = TipoNumerico Then sdbgLista.Columns("VALOR").Visible = False
            sCampoRelacionado = g_oCampo.DevolverNombreCampoHijo()
            If sCampoRelacionado <> "" Then sdbgLista.Columns("ORDEN").Locked = True
        End If
    End If

    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoBoolean Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoArchivo Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoDesglose Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoEditor Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoEnlace Then
    
        sdbgLista.RemoveAll
        
        optLibre.Enabled = False
        optLista.Enabled = False
        If optLista.Value = True Then
            optLibre.Value = True
            'optLista.Value = False
            sdbgLista.AllowUpdate = False
        End If
        
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoArchivo Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Then
                
        optLibre.Enabled = Not g_bCampoEnUso
        optLista.Enabled = Not g_bCampoEnUso
        
        If val(m_sTipo) <> sdbcTipo.Columns("ID").Value Then
            sdbgLista.RemoveAll
        End If
        
        If optLista.Value = True Then
            sdbgLista.AllowUpdate = True
        Else
            sdbgLista.AllowUpdate = False
        End If
        
        sdbgLista.Columns.Item("VALOR").FieldLen = 0
    ElseIf iCampoGS = TipoCampoNoConformidad.Subtipo Then
        Me.sdbcTipo.Enabled = False
        Me.txtMaxLength.Enabled = False
        Me.UpdMaxLength.Enabled = False
        Me.optLibre.Enabled = False
        Me.optLista.Enabled = False
    Else
        optLibre.Enabled = Not g_bCampoEnUso
        optLista.Enabled = Not g_bCampoEnUso
        
        If val(m_sTipo) <> sdbcTipo.Columns("ID").Value Then
            sdbgLista.RemoveAll
        End If
        
        If optLista.Value = True Then
            sdbgLista.AllowUpdate = True
        Else
            sdbgLista.AllowUpdate = False
        End If
        
        If val(m_sMaxLength) > val(txtMaxLength.Text) Then
        
            Comprobar_txtMaxLength
        Else
            If Not g_sOrigen = "frmSolicitudDetalle" Then
                If g_oCampo Is Nothing Then
                Else
                    If m_bMultiIdioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
                        For Each oIdioma In g_oIdiomas
                            sdbgLista.Columns(oIdioma.Cod).FieldLen = val(txtMaxLength.Text)
                        Next
                    Else
                        sdbgLista.Columns.Item("VALOR").FieldLen = val(txtMaxLength.Text)
                    End If
                End If
            End If

        End If
    End If
End Sub
''' <summary>Esta funci�n muestra u oculta el check de Recordar Valores dependiendo del tipo de campo</summary>
''' Tiempo m�ximo: 1 sec </remarks>
Private Sub MostrarOpcionRecordarValores()
    Select Case sdbcTipo.Columns("ID").Value
        Case TiposDeAtributos.TipoTextoCorto
             chkRecordarValores.Visible = True
        Case TiposDeAtributos.TipoTextoMedio
            chkRecordarValores.Visible = True
        Case TiposDeAtributos.TipoTextoLargo
            chkRecordarValores.Visible = True
        Case Else
            chkRecordarValores.Visible = False
    End Select
    
    'Si es de tipo Lista no hay que mostrar el "Recordar valores"
    If optLista.Value = True Then
        chkRecordarValores.Visible = False
    End If
    
    'Reviso que tambien sea campo de edicion libre
    If chkRecordarValores.Visible = True And Me.optLibre.Value = False Then
        chkRecordarValores.Visible = False
    End If
End Sub

''' <summary>Esta funci�n inhabilita que se pueda elegir la opcion lista si esta seleccionada la opci�n recordar valores</summary>
''' Tiempo m�ximo: 0,1 sec </remarks>
Private Sub ComprobarOptLista()
    If chkRecordarValores.Value = vbChecked Then
        Me.optLibre.Value = True
    End If
End Sub

Private Sub sdbcListaCampoPadre_DropDown()
    If sdbcListaCampoPadre.Rows = 0 Then
        CargarComboCamposPadre
    End If
End Sub

Private Sub sdbcTipo_CloseUp()

    If val(m_sTipo) = sdbcTipo.Columns("ID").Value Then
        Exit Sub
    End If
    'Muestro u oculto el check de recordar valores
    MostrarOpcionRecordarValores
    
    txtMax.Text = ""
    txtMin.Text = ""
    txtMaxLength.Text = ""
    txtMaxLength.Enabled = False
    UpdMaxLength.Enabled = False
    lblHastaCaracteres.Visible = False
    chkNoFecAntSis.Visible = False
    If sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoFecha And sdbcTipo.Columns("ID").Value <> TiposDeAtributos.TipoNumerico Then
        txtMax.Enabled = False
        txtMin.Enabled = False
        If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Then
            txtMaxLength.Enabled = True
            UpdMaxLength.Enabled = True
            Select Case sdbcTipo.Columns("ID").Value
                Case TiposDeAtributos.TipoTextoCorto
                    lblHastaCaracteres.caption = m_sMensajeHasta(1)
                    UpdMaxLength.min = 1
                    UpdMaxLength.Max = 100
                    txtMaxLength.Text = "100"
                    chkRecordarValores.Visible = True
                Case TiposDeAtributos.TipoTextoMedio
                    lblHastaCaracteres.caption = m_sMensajeHasta(2)
                    UpdMaxLength.min = 100
                    UpdMaxLength.Max = 800
                    txtMaxLength.Text = "800"
                    chkRecordarValores.Visible = True
                Case TiposDeAtributos.TipoTextoLargo
                    lblHastaCaracteres.caption = m_sMensajeHasta(3)
                    UpdMaxLength.min = 800
                    UpdMaxLength.Max = 99999
                    txtMaxLength.Text = ""
                    chkRecordarValores.Visible = True
                Case Else
                    chkRecordarValores.Visible = False
            End Select
            lblHastaCaracteres.Visible = True
        End If
    Else
        Select Case sdbcTipo.Columns("ID").Value
            Case TiposDeAtributos.TipoFecha
                chkNoFecAntSis.Visible = True
        End Select
        If Me.optLista.Value = True Then
            txtMax.Enabled = False
            txtMin.Enabled = False
        Else
            txtMax.Enabled = True
            txtMin.Enabled = True
        End If
    End If
    
    g_oCampo.Tipo = sdbcTipo.Columns("ID").Value
    
    CrearGridLista
    ConfigurarGridLista
    
    m_sTipo = sdbcTipo.Columns("ID").Value
    m_sMaxLength = txtMaxLength.Text
End Sub

Private Sub sdbcTipo_GotFocus()
    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If
End Sub


''' <summary>
''' Valida el valor introducido
''' </summary>
''' <param name="Text">texto introducido</param>
''' <param name="RtnPassed">Es el valor que devuelve al notificar al control que los datos son v�lidos o no</param>
''' <remarks>Llamada desde:=Evento que salta cuando cambiamos de valor en la celda que contiene la combo y cambiamos de celda; Tiempo m�ximo:=0seg.</remarks>
Private Sub sdbddValoresLista_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Long
Dim bm As Variant

On Error Resume Next

sdbddValoresLista.MoveFirst

If Text <> "" Then
    For i = 0 To sdbddValoresLista.Rows - 1
        bm = sdbddValoresLista.GetBookmark(i)
        If UCase(Text) = UCase(sdbddValoresLista.Columns(2).CellText(bm)) Then
            sdbgLista.Columns(sdbgLista.col + 1).Value = sdbddValoresLista.Columns("ORDEN").CellText(bm)
            sdbddValoresLista.Bookmark = bm
            Exit For
        End If
    Next i
    If i > sdbddValoresLista.Rows - 1 Then
        'No ha encontrado el valor
        sdbgLista.Columns(sdbgLista.col).Text = ""
        sdbgLista.Columns(sdbgLista.col + 1).Value = ""
        sdbddValoresLista.Row = 0
    End If
End If
End Sub

Private Sub sdbgLista_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' Comprobaciones antes de grabar
''' </summary>
''' <param name="Cancel">Cancelar la grabaci�n</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgLista_BeforeUpdate(Cancel As Integer)
    Dim i As Integer
    Dim vbm As Variant
    Dim oIdioma As CIdioma
    Dim j As Integer
    Dim DifiereEnPadre As Boolean
    
    If m_bRespetarLista Then Exit Sub
    If m_bSaltarFoco Then Exit Sub
    
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
        If g_oCampo.Peso And g_oCampo.Tipo = TipoNumerico Then 'Si es campo peso y numerico pongo al valor el orden que es la columna que se ve (peso)
            sdbgLista.Columns("VALOR").Value = sdbgLista.Columns("ORDEN").Value
        End If
        If Not IsNumeric(sdbgLista.Columns("VALOR").Value) Then
            oMensajes.NoValido m_sIdiValor & " " & m_sMensaje(1)
            If Me.Visible Then sdbgLista.SetFocus
            m_bError = True
            Cancel = True
            Exit Sub
        End If
        
        If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
            For j = 1 To m_iNumeroPadres
                sdbgLista.Columns(j * 3).DropDownHwnd = 0
            Next
        End If
        
        For i = 0 To sdbgLista.Rows - 1
            vbm = sdbgLista.AddItemBookmark(i)
            If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                If sdbgLista.Columns("VALOR").CellValue(vbm) = sdbgLista.Columns("VALOR").Value Then
                    DifiereEnPadre = False
                    For j = 1 To m_iNumeroPadres
                        If sdbgLista.Columns(j * 3).CellValue(vbm) <> sdbgLista.Columns(j * 3).Text Then
                            DifiereEnPadre = True
                            Exit For
                        End If
                    Next
                    
                    If Not DifiereEnPadre Then
                        oMensajes.NoValido m_sMensaje(3)
                        If Me.Visible Then sdbgLista.SetFocus
                        Cancel = True
                        m_bError = True
                        For j = 1 To m_iNumeroPadres
                            sdbgLista.Columns(j * 3).DropDownHwnd = sdbddValoresLista.hWnd
                        Next
                        Exit Sub
                    End If
                End If
            End If
        Next i
                        
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Then
        If Not IsDate(sdbgLista.Columns("VALOR").Value) Then
            oMensajes.NoValido m_sIdiValor & " " & m_sMensaje(2)
            If Me.Visible Then sdbgLista.SetFocus
            Cancel = True
            m_bError = True
            Exit Sub
        End If
        
        If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
            For j = 1 To m_iNumeroPadres
                sdbgLista.Columns(j * 3).DropDownHwnd = 0
            Next
        End If
        
        For i = 0 To sdbgLista.Rows - 1
            vbm = sdbgLista.AddItemBookmark(i)
            If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                If sdbgLista.Columns("VALOR").CellValue(vbm) = sdbgLista.Columns("VALOR").Value Then
                    DifiereEnPadre = False
                    For j = 1 To m_iNumeroPadres
                        If sdbgLista.Columns(j * 3).CellValue(vbm) <> sdbgLista.Columns(j * 3).Text Then
                            DifiereEnPadre = True
                            Exit For
                        End If
                    Next
                    
                    If Not DifiereEnPadre Then
                        oMensajes.NoValido m_sMensaje(3)
                        If Me.Visible Then sdbgLista.SetFocus
                        Cancel = True
                        m_bError = True
                        For j = 1 To m_iNumeroPadres
                            sdbgLista.Columns(j * 3).DropDownHwnd = sdbddValoresLista.hWnd
                        Next
                        Exit Sub
                    End If
                End If
            End If
        Next i
    
    ElseIf (sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio) Then
        If g_oCampo.Grupo.Formulario.Multiidioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
            If sdbgLista.Columns(gParametrosInstalacion.gIdioma).Value = "" Then
                oMensajes.NoValido sdbgLista.Columns(gParametrosInstalacion.gIdioma).caption
                If Me.Visible Then sdbgLista.SetFocus
                Cancel = True
                m_bError = True
                Exit Sub
            End If
            
            If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
                For j = 1 To m_iNumeroPadres
                    sdbgLista.Columns((j * 3) + 2).DropDownHwnd = 0
                Next
            End If
            
            For i = 0 To sdbgLista.Rows - 1
                vbm = sdbgLista.AddItemBookmark(i)
                If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                    For Each oIdioma In g_oIdiomas
                        If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                            If sdbgLista.Columns(oIdioma.Cod).CellValue(vbm) = sdbgLista.Columns(oIdioma.Cod).Value Then
                                DifiereEnPadre = False
                                For j = 1 To m_iNumeroPadres
                                    If sdbgLista.Columns((j * 3) + 2).CellValue(vbm) <> sdbgLista.Columns((j * 3) + 2).Text Then
                                        DifiereEnPadre = True
                                        Exit For
                                    End If
                                Next
                                
                                If Not DifiereEnPadre Then
                                    oMensajes.NoValido m_sMensaje(3)
                                    If Me.Visible Then sdbgLista.SetFocus
                                    Cancel = True
                                    m_bError = True
                                    For j = 1 To m_iNumeroPadres
                                        sdbgLista.Columns((j * 3) + 2).DropDownHwnd = sdbddValoresLista.hWnd
                                    Next
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next
                End If
            Next i
        Else
            If sdbgLista.Columns("VALOR").Value = "" Then
                oMensajes.NoValido m_sIdiValor
                If Me.Visible Then sdbgLista.SetFocus
                Cancel = True
                m_bError = True
                Exit Sub
            End If
            
            If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value <> TipoCampoGS.material Then
                For j = 1 To m_iNumeroPadres
                    sdbgLista.Columns(j * 3).DropDownHwnd = 0
                Next
            End If
            
            For i = 0 To sdbgLista.Rows - 1
                vbm = sdbgLista.AddItemBookmark(i)
                If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                    If sdbgLista.Columns("VALOR").CellValue(vbm) = sdbgLista.Columns("VALOR").Value Then
                        DifiereEnPadre = False
                        For j = 1 To m_iNumeroPadres
                            If sdbgLista.Columns(j * 3).CellValue(vbm) <> sdbgLista.Columns(j * 3).Text Then
                                DifiereEnPadre = True
                                Exit For
                            End If
                        Next
                        
                        If Not DifiereEnPadre Then
                            oMensajes.NoValido m_sMensaje(3)
                            If Me.Visible Then sdbgLista.SetFocus
                            Cancel = True
                            m_bError = True
                            For j = 1 To m_iNumeroPadres
                                sdbgLista.Columns(j * 3).DropDownHwnd = sdbddValoresLista.hWnd
                            Next
                            Exit Sub
                        End If
                    End If
                End If
            Next i
        End If
    End If
    
    'Comprobar valores peso
    If g_oCampo.Peso Then
        For i = 0 To sdbgLista.Rows - 1
            vbm = sdbgLista.AddItemBookmark(i)
            If CStr(vbm) <> CStr(sdbgLista.Bookmark) Then
                If sdbgLista.Columns("ORDEN").CellValue(vbm) = sdbgLista.Columns("ORDEN").Value Then
                    oMensajes.NoValido m_sMensaje(3)
                    If Me.Visible Then sdbgLista.SetFocus
                    Cancel = True
                    m_bError = True
                    Exit Sub
                End If
            End If
        Next i
    End If
    
    j = 0
    i = 0
    While (j < sdbgLista.Rows)
        For i = sdbgLista.Columns.Count - 1 To 0 Step -1
            If Left(sdbgLista.Columns(i).Name, 11) = "ORDENPADRE_" Or Left(sdbgLista.Columns(i).Name, 16) = "CAMPOPADREVALOR_" Then
                If sdbgLista.Columns(i).Value = "" Then
                    oMensajes.IntroducirCampoPadre
                    If Me.Visible Then sdbgLista.SetFocus
                    m_bError = True
                    Cancel = True
                    Exit Sub
                End If
            End If
        Next i
        
        j = j + 1
    Wend
    m_bModifLista = True
End Sub

Private Sub sdbgLista_BtnClick()
    If Mid(sdbgLista.Columns(sdbgLista.col).Name, 1, 11) = "CAMPOPADRE_" Then
        Dim oCampo As CFormItem
        Set oCampo = DevolverCampoPadre
        
        'Busco el campo padre
        If oCampo.CampoGS = TipoCampoGS.material Then
            'Abrir el buscador de materiales
            frmSELMAT.sOrigen = "frmDetalleCampos"
            frmSELMAT.Show vbModal
        End If
    End If
End Sub

''' <summary>Devuelve el campo padre del campo actual</summary>
''' <remarks>Llamada desde: sdbgLista_BtnClick</remarks>
Private Function DevolverCampoPadre() As CFormItem
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim bEncontrado As Boolean
    
    For Each oGrupo In g_oCampo.Grupo.Formulario.Grupos
        For Each oCampo In oGrupo.Campos
            If oCampo.Id = m_lCampoPadreSeleccionado Then
                bEncontrado = True
                Exit For
            End If
        Next

        If bEncontrado Then Exit For
    Next
    
    Set DevolverCampoPadre = oCampo
    
    Set oGrupo = Nothing
    Set oCampo = Nothing
End Function

''' <summary>Pone en el grid el material seleccionado en el buscador de material</summary>
''' <remarks>Llamada desde: frmSELMAT</remarks>

Public Sub PonerMatSeleccionado()
    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    Dim sValor As String
    Dim sDen As String
    Dim lIdCampoPadre As Long
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sValor = oGMN1Seleccionado.Cod
        sDen = oGMN1Seleccionado.Cod & " - " & oGMN1Seleccionado.Den
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sValor = oGMN2Seleccionado.GMN1Cod & "|" & oGMN2Seleccionado.Cod
        sDen = oGMN2Seleccionado.GMN1Cod & " - " & oGMN2Seleccionado.Cod & " - " & oGMN2Seleccionado.Den
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sValor = oGMN3Seleccionado.GMN1Cod & "|" & oGMN3Seleccionado.GMN2Cod & "|" & oGMN3Seleccionado.Cod
        sDen = oGMN3Seleccionado.GMN1Cod & " - " & oGMN3Seleccionado.GMN2Cod & " - " & oGMN3Seleccionado.Cod & " - " & oGMN3Seleccionado.Den
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sValor = oGMN4Seleccionado.GMN1Cod & "|" & oGMN4Seleccionado.GMN2Cod & "|" & oGMN4Seleccionado.GMN3Cod & "|" & oGMN4Seleccionado.Cod
        sDen = oGMN4Seleccionado.GMN1Cod & " - " & oGMN4Seleccionado.GMN2Cod & " - " & oGMN4Seleccionado.GMN3Cod & " - " & oGMN4Seleccionado.Cod & " - " & oGMN4Seleccionado.Den
    End If
            
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    lIdCampoPadre = Mid(sdbgLista.Columns(sdbgLista.col).Name, 12, Len(sdbgLista.Columns(sdbgLista.col).Name) - 11)
    sdbgLista.Columns("CAMPOPADREVALOR_" & lIdCampoPadre).Value = sValor
    sdbgLista.Columns("CAMPOPADRE_" & lIdCampoPadre).Value = sDen
End Sub

Private Sub sdbgLista_GotFocus()
    If m_bError Then Exit Sub
    If txtMaxLength.Text <> "" Then
        ComprobarValoresLista
    End If
    m_bSaltarFoco = False
End Sub

'Descripcion:=
    ''' * Objetivo: Capturar la tecla Supr para
    ''' * Objetivo: poder no dejar eliminar desde el teclado
    '''   Si se trata de una lista anidada y es de tipo combo, cuando le damos al back key eliminar el contenido
'Parametro:=
    'KeyCode:=n� de la tecla que hemos pulsado
    'Shift:=Si tenemos pulsado la tecla.
'Llamada desde:= Evento que salta al pulsar una tecla
'Tiempo ejecucion:=0seg.
Private Sub sdbgLista_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Or g_bCampoEnUso Then
        KeyCode = 0
        Exit Sub
    ElseIf KeyCode = vbKeyBack Then
        If Mid(sdbgLista.Columns(sdbgLista.col).Name, 1, 10) = "CAMPOPADRE" Then
                sdbgLista.Columns(sdbgLista.col).Text = ""
                sdbgLista.Columns(sdbgLista.col + 1).Text = ""
        End If
    End If
End Sub

''' <summary>
''' Tras moverse de fila/columna realiza los cambios correspondientes en las celdas (locked, cargar combo, etc)
''' </summary>
''' <param name="LastRow">ultima fila</param>
''' <param name="LastCol">ultima columna</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgLista_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    'Comprobar si el campo padre seleccionado tb dispone de otro campo padre...y sucesivos
    Dim sCampoPadre_CampoPadre As String
    Dim lCampoPadreAnt As Long
    Dim aux() As String
    Dim oIdioma As CIdioma
    Dim sColumnaValor() As String
    Dim i As Integer
    
    If sdbgLista.AllowUpdate = False Then Exit Sub
    If Not g_sOrigen = "frmSolicitudDetalle" And g_oCampo.TipoPredef <> Atributo And _
      g_oCampo.Grupo.Formulario.Multiidioma = True And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto _
      Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
        ReDim sColumnaValor(0 To g_oIdiomas.Count - 1)
        For Each oIdioma In g_oIdiomas
            sColumnaValor(i) = oIdioma.Cod
            i = i + 1
        Next
    Else
        ReDim sColumnaValor(0)
        sColumnaValor(0) = "VALOR"
    End If
    
    For i = 0 To UBound(sColumnaValor)
        sdbgLista.Columns(sColumnaValor(i)).Locked = False
    Next i
    
    If sdbgLista.IsAddRow Then
        cmdElimValor.Enabled = False
        
        lCampoPadreAnt = m_lCampoPadreSeleccionado
        sCampoPadre_CampoPadre = g_oCampo.CargarCampoPadre(m_lCampoPadreSeleccionado, (g_sOrigen = "frmFormularios"))
            
        While sCampoPadre_CampoPadre <> ""
            If sdbgLista.Columns("CAMPOPADREPADRE_" & lCampoPadreAnt).Value = "" Then
                aux = Split(sCampoPadre_CampoPadre, "$$$")
                sdbgLista.Columns("CAMPOPADREPADRE_" & lCampoPadreAnt).Value = aux(0)
                    
                lCampoPadreAnt = aux(0)
                sCampoPadre_CampoPadre = g_oCampo.CargarCampoPadre(aux(0), (g_sOrigen = "frmFormularios"))
            Else
                sCampoPadre_CampoPadre = ""
            End If
        Wend
        
    Else
        cmdElimValor.Enabled = True
        If g_bCampoEnUso Then
            For i = 0 To UBound(sColumnaValor)
                sdbgLista.Columns(sColumnaValor(i)).Locked = True
            Next i
        End If
    End If
    cmdAnyaValor.Enabled = True
    
    If sdbgLista.col > -1 Then
        If Mid(sdbgLista.Columns(sdbgLista.col).Name, 1, 10) = "CAMPOPADRE" Then
            'Busco el campo padre
            Dim oCampo As CFormItem
            Set oCampo = DevolverCampoPadre
        
            If oCampo.CampoGS = TipoCampoGS.material Then
                'Campo de tipo material
                sdbgLista.Columns(sdbgLista.col).DropDownHwnd = 0
                sdbgLista.Columns(sdbgLista.col).Style = ssStyleEditButton
                sdbgLista.Columns(sdbgLista.col).Locked = True
            Else
                'Campo de tipo lista
                If LastCol >= 0 Then sdbgLista.Columns(LastCol).DropDownHwnd = 0
                sdbgLista.Columns(sdbgLista.col).Style = ssStyleComboBox
                sdbgLista.Columns(sdbgLista.col).DropDownHwnd = 0
                sdbddValoresLista.RemoveAll
                sdbddValoresLista.AddItem ""
                If g_bCampoEnUso And Not sdbgLista.IsAddRow Then
                    sdbgLista.Columns(sdbgLista.col).Locked = True
                Else
                    sdbgLista.Columns(sdbgLista.col).Locked = False
                    sdbgLista.Columns(sdbgLista.col).DropDownHwnd = sdbddValoresLista.hWnd
                    sdbgLista.Columns(sdbgLista.col).Locked = True
                    sdbddValoresLista.Enabled = True
                    sdbddValoresLista_DropDown
                    sdbddValoresLista.DroppedDown = True
                End If
            End If
        End If
    End If
End Sub

Private Sub sdbgLista_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    Dim iCampoPadreTipoCampoGS As Integer
    
    If m_iTipoCampoPadre <> TipoCampoGS.material Then
        For i = 0 To sdbgLista.Cols - 1
            sdbgLista.Columns(i).DropDownHwnd = 0
        Next
    End If
    
    If sdbgLista.IsAddRow Then
        cmdElimValor.Enabled = False
    Else
        cmdElimValor.Enabled = True
    End If
End Sub

Private Sub sdbgLista_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
    If sdbgLista.SelBookmarks.Count > 0 Then
        cmdMvtoElemLista(0).Enabled = True
        cmdMvtoElemLista(1).Enabled = True
    Else
        cmdMvtoElemLista(0).Enabled = False
        cmdMvtoElemLista(1).Enabled = False
    End If
End Sub

Private Sub CargarRecursos()
    Dim i As Integer
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DETALLE_CAMPO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value & ":"    '1 Detalle de campo
        Ador.MoveNext
        optLibre.caption = Ador(0).Value '2 Introducci�n libre
        Ador.MoveNext
        lblTipo.caption = Ador(0).Value & ":" '3 Tipo
        m_sIdiTp = Ador(0).Value
        Ador.MoveNext
        lblMin.caption = Ador(0).Value & ":" '4 M�nimo
        m_sIdiMinimo = Ador(0).Value
        Ador.MoveNext
        lblMax.caption = Ador(0).Value & ":" '5 M�ximo
        m_sIdiMaximo = Ador(0).Value
        Ador.MoveNext
        optLista.caption = Ador(0).Value  '6 Introducci�n mediante selecci�n en lista
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '7 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '8 &Cancelar
        For i = 2 To 8
            Ador.MoveNext
            m_sIdiTipo(i) = Ador(0).Value
        Next i
        Ador.MoveNext
        m_sIdiValor = Ador(0).Value  '16 Valor
        sdbgLista.Columns.Item("VALOR").caption = Ador(0).Value
        
        Ador.MoveNext
        cmdAnyaValor.ToolTipText = Ador(0).Value  '17 A�adir valor
        Ador.MoveNext
        cmdElimValor.ToolTipText = Ador(0).Value  '18 Eliminar valor
        For i = 1 To 4
            Ador.MoveNext
            m_sMensaje(i) = Ador(0).Value
        Next i
        Ador.MoveNext
        Ador.MoveNext
        lblMaxLength.caption = Ador(0).Value  '24 N� m�ximo caracteres:
        For i = 1 To 3
            Ador.MoveNext
            m_sMensajeHasta(i) = Ador(0).Value 'hasta XX caracteres
        Next i
        Ador.MoveNext
        lblCampoOrigen.caption = Ador(0).Value  '28 Campo origen:
        Ador.MoveNext
        Me.chkRecordarValores.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblCampoPadre.caption = Ador(0).Value '30 Campo padre
        Ador.MoveNext
        m_sIdiTipo(9) = Ador(0).Value
        Ador.MoveNext
        m_sIdiTipo(10) = Ador(0).Value
        Ador.MoveNext
        lblFormato.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblValorComprob.caption = Ador(0).Value & ":"
        Ador.MoveNext
        optListaExterna.caption = Ador(0).Value     'Introducci�n mediante selecci�n en lista (LISTA EXTERNA)
        Ador.MoveNext
        chkNoFecAntSis.caption = Ador(0).Value
        Ador.MoveNext
        m_sPeso = Ador(0).Value  'Normalmente el campo ORDEN no se muestra salvo cuando la lista est� marcada como Peso. El caption es para estos casos
        Ador.MoveNext
        m_sIdiTipo(11) = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub


''' <summary>
''' Rellena el formulario con los datos obtenidos de base de datos y almacenados en la colecci�n
''' Si se trata de un campo de tipo Lista visualiza la combo de Campo padre y crea en la grid las columnas de combos anidadas
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:form_load() </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub CargarDatosCampo()
    Dim oValor As CCampoValorLista
    Dim i As Integer
    Dim sCadena As String
    Dim sLinea As String
    Dim oIdioma As CIdioma
    Dim aux() As String
    
    Select Case g_sOrigen
    
    Case "frmCamposSolic"   'Detalle de los campos predefinidos:
        If g_oCampoPredef.Tipo = TiposDeAtributos.TipoEnlace Then
            sdbcTipo.Text = m_sIdiTipo(11)
            sdbcTipo.MoveRecords g_oCampoPredef.Tipo
        Else
            sdbcTipo.MoveRecords g_oCampoPredef.Tipo - 2
            sdbcTipo.Text = m_sIdiTipo(g_oCampoPredef.Tipo)
        End If
        
        
        'configura los campos de la grid:
        If g_oCampoPredef.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampoPredef.Tipo = TiposDeAtributos.TipoTextoMedio Or g_oCampoPredef.Tipo = TiposDeAtributos.TipoTextoLargo Then
            sdbgLista.Columns.RemoveAll
            i = 0
            For Each oIdioma In g_oIdiomas
                sdbgLista.Columns.Add i
                sdbgLista.Columns.Item(i).Name = oIdioma.Cod
                sdbgLista.Columns.Item(i).caption = oIdioma.Den
                sdbgLista.Columns.Item(i).Width = sdbgLista.Width / g_oIdiomas.Count - 300
                i = i + 1
            Next
        End If
        
        If g_oCampo Is Nothing Then
            picCampoOrigen.Visible = False
        Else
            If g_oCampo.TipoPredef = Atributo Then
                lblCampoOrigen.caption = lblCampoOrigen & " " & g_oCampo.CodAtrib & "  -  " & g_oCampo.DenAtrib
                picCampoOrigen.Visible = True
            Else
                picCampoOrigen.Visible = False
            End If
        End If
           
        Select Case g_oCampoPredef.TipoIntroduccion
            Case IntroLibre
                optLibre.Value = True
                If g_oCampoPredef.Tipo = TiposDeAtributos.TipoFecha Then
                    txtMax.Text = NullToStr(g_oCampoPredef.MaxFec)
                    txtMin.Text = NullToStr(g_oCampoPredef.MinFec)
                ElseIf g_oCampoPredef.Tipo = TiposDeAtributos.TipoNumerico Then
                    txtMax.Text = NullToStr(g_oCampoPredef.MaxNum)
                    txtMin.Text = NullToStr(g_oCampoPredef.MinNum)
                Else
                    txtMax.Text = ""
                    txtMin.Text = ""
                End If
                    
            Case Introselec, IntroListaExterna
                optLista.Value = (g_oCampoPredef.TipoIntroduccion = Introselec)
                optListaExterna.Value = (g_oCampoPredef.TipoIntroduccion = IntroListaExterna)
                
                'En los de tipo lista externa no se meten valores
                If g_oCampoPredef.TipoIntroduccion = Introselec Then
                    g_oCampoPredef.CargarValoresLista
                    If Not g_oCampoPredef.ValoresLista Is Nothing Then
                        For Each oValor In g_oCampoPredef.ValoresLista
                            Select Case g_oCampoPredef.Tipo
                                Case TiposDeAtributos.TipoFecha
                                    sdbgLista.AddItem oValor.valorFec
                                    
                                Case TiposDeAtributos.TipoNumerico
                                    sdbgLista.AddItem oValor.valorNum
                                    
                                Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                                    sCadena = ""
                                    For Each oIdioma In g_oIdiomas
                                        sCadena = sCadena & oValor.valorText.Item(CStr(oIdioma.Cod)).Den & Chr(m_lSeparador)
                                    Next
                                    sdbgLista.AddItem sCadena
                            End Select
                        Next
                    End If
                End If
        End Select
        
    Case Else  'Detalle de campos normales de formularios o instancias
        sdbcTipo.MoveRecords g_oCampo.Tipo - 2
        If g_oCampo.Tipo = TiposDeAtributos.TipoEditor Then
            sdbcTipo.Text = m_sIdiTipo(10)
        ElseIf g_oCampo.Tipo = TiposDeAtributos.TipoEnlace Then
            sdbcTipo.Text = m_sIdiTipo(11)
            sdbcTipo.MoveRecords g_oCampo.Tipo
        Else
            sdbcTipo.Text = m_sIdiTipo(g_oCampo.Tipo)
        End If
        
        If Not g_sOrigen = "frmSolicitudDetalle" And g_oCampo.TipoPredef <> Atributo Then
            'configura los campos de la grid:
            If g_oCampo.Grupo.Formulario.Multiidioma And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
                sdbgLista.Columns.RemoveAll
                i = 0
                For Each oIdioma In g_oIdiomas
                    sdbgLista.Columns.Add i
                    sdbgLista.Columns.Item(i).Name = oIdioma.Cod
                    sdbgLista.Columns.Item(i).caption = oIdioma.Den
                    sdbgLista.Columns.Item(i).Width = sdbgLista.Width / g_oIdiomas.Count - 300
                    i = i + 1
                Next
                sdbgLista.Columns.Add i
                sdbgLista.Columns.Item(i).Name = "EXISTE"
                sdbgLista.Columns.Item(i).Visible = False
                i = i + 1
                sdbgLista.Columns.Add i
                sdbgLista.Columns.Item(i).Name = "ORDEN"
                sdbgLista.Columns.Item(i).caption = m_sPeso  'La columna ORDEN siempre est� invisible salvo cuando el campop est� marcado como Peso. Este caption es para esos casos.
                sdbgLista.Columns.Item(i).Visible = False
                'Se a�ade una columna nueva para mantener el valor original de ORDEN (Esta columna s�lo tiene sentido cuando el campo est� marcado como Peso)
                i = i + 1
                sdbgLista.Columns.Add i
                sdbgLista.Columns.Item(i).Name = "ORDEN_ANT"
                sdbgLista.Columns.Item(i).Visible = False
            Else
                sdbgLista.Columns.Item("VALOR").Width = sdbgLista.Columns.Item("VALOR").Width - 1000
                sdbgLista.Columns.Item("ORDEN").caption = m_sPeso  'La columna ORDEN siempre est� invisible salvo cuando el campop est� marcado como Peso. Este caption es para esos casos.
                sdbgLista.Columns.Item("ORDEN").Visible = False
            End If
        End If
        
        If g_oCampo.TipoPredef = Atributo Then
            lblCampoOrigen.caption = lblCampoOrigen & " " & g_oCampo.CodAtrib & "  -  " & g_oCampo.DenAtrib
            picCampoOrigen.Visible = True
        Else
            picCampoOrigen.Visible = False
        End If
        
        Select Case g_oCampo.TipoIntroduccion
            Case IntroLibre
                optLibre.Value = True
                txtMax.Text = NullToStr(g_oCampo.Maximo)
                txtMin.Text = NullToStr(g_oCampo.Minimo)
        
            Case Introselec, IntroListaExterna
                optLista.Value = (g_oCampo.TipoIntroduccion = Introselec)
                optListaExterna.Value = (g_oCampo.TipoIntroduccion = IntroListaExterna)
                
                'En los de tipo lista externa no se meten valores
                If g_oCampo.TipoIntroduccion = Introselec Then
                    g_oCampo.CargarValoresLista
                    If Not g_oCampo.ValoresLista Is Nothing Then
                        'A�adir las columnas campo_padre
                        CrearCabecerasGridCampoPadre g_oCampo.Id, False, (g_sOrigen = "frmFormularios")
                    
                        Dim sResultado As String
                        Dim sTextoGrid As String
                        sLinea = ""
                        For Each oValor In g_oCampo.ValoresLista
                            If oValor.IdCampoPadre > 0 Then
                                Dim arCampoPadre() As String
                                arCampoPadre = Split(g_oCampo.CargarCampoPadre(g_oCampo.Id, (g_sOrigen = "frmFormularios")), "$$$")
                                
                                If arCampoPadre(2) = TipoCampoGS.material Then
                                    'Campo padre material
                                    Dim sValor As String
                                    Dim sDen As String
                                    sValor = oValor.GMN1 & IIf(oValor.GMN2 <> "", "|" & oValor.GMN2 & IIf(oValor.GMN3 <> "", "|" & oValor.GMN3 & IIf(oValor.GMN4 <> "", "|" & oValor.GMN4, ""), ""), "")
                                    sDen = oValor.GMN1 & IIf(oValor.GMN2 <> "", " - " & oValor.GMN2 & IIf(oValor.GMN3 <> "", " - " & oValor.GMN3 & IIf(oValor.GMN4 <> "", " - " & oValor.GMN4, ""), ""), "") & _
                                            " - " & oValor.GMNDen.Item(oUsuarioSummit.idioma).Den
                                    sTextoGrid = sDen & Chr(m_lSeparador) & sValor & Chr(m_lSeparador) & oValor.IdCampoPadre
                                Else
                                    'Campo padre lista
                                    sResultado = g_oCampo.CargarCampoPadreOrdenYValor(oValor.Campo.Id, oValor.Orden, (g_sOrigen = "frmFormularios"), oUsuarioSummit.idioma)
                                    'lId & "$$$" & iOrden & "$$$" & sDen & "$$$" & lidCampoPadre
                                    sTextoGrid = ""
                                    While sResultado <> ""
                                        aux = Split(sResultado, "$$$")
                                        If sTextoGrid <> "" Then sTextoGrid = sTextoGrid & Chr(m_lSeparador)
                                        
                                        sTextoGrid = sTextoGrid & aux(2) & Chr(m_lSeparador) & aux(1) & Chr(m_lSeparador) & aux(3)
                                        If aux(4) = 0 Then
                                            sResultado = g_oCampo.CargarCampoPadreOrdenYValor(aux(0), aux(1), (g_sOrigen = "frmFormularios"), oUsuarioSummit.idioma)
                                        Else
                                            sResultado = ""
                                        End If
                                    Wend
                                End If
                            End If
                                        
                            Select Case g_oCampo.Tipo
                                Case TiposDeAtributos.TipoFecha
                                    sCadena = oValor.valorFec
                                    
                                Case TiposDeAtributos.TipoNumerico
                                    sCadena = oValor.valorNum

                                    
                                Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                                    If g_sOrigen = "frmSolicitudDetalle" Or g_oCampo.TipoPredef = Atributo Then
                                        sCadena = oValor.valorText.Item(gParametrosInstalacion.gIdioma).Den
                                    Else
                                        If g_oCampo.Grupo.Formulario.Multiidioma = True And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
                                            sCadena = ""
                                            For Each oIdioma In g_oIdiomas
                                                If sCadena = "" Then
                                                    sCadena = oValor.valorText.Item(CStr(oIdioma.Cod)).Den
                                                Else
                                                    sCadena = sCadena & Chr(m_lSeparador) & oValor.valorText.Item(CStr(oIdioma.Cod)).Den
                                                End If
                                            Next
                                        Else
                                            sCadena = NullToStr(oValor.valorText.Item(gParametrosInstalacion.gIdioma).Den)
                                        End If
                                    End If
                            End Select
                            'Se ha a�adido en dise�o una columna nueva (EXISTE) para indicar que elementos de la lista existian y cuales se han a�adido desde el formulario
                            sCadena = sCadena & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oValor.Orden & Chr(m_lSeparador) & oValor.Orden
                            If sTextoGrid <> "" Then sCadena = sCadena & Chr(m_lSeparador)
                            sLinea = sCadena & sTextoGrid
                            sdbgLista.AddItem sLinea
                        Next
                    End If
                End If
        End Select
    
        If g_oCampo.TipoPredef = TipoCampoPredefinido.Atributo Then   'si es un atributo no puede modificar el tipo
            picTipo.Enabled = False
        Else
            picTipo.Enabled = True
        End If
        
        If g_oCampo.Tipo = TipoTextoCorto Then
            txtMaxLength.Enabled = True
            UpdMaxLength.Enabled = True
            UpdMaxLength.min = 1
            UpdMaxLength.Max = 100
            lblHastaCaracteres.caption = m_sMensajeHasta(1)
            lblHastaCaracteres.Visible = True
            txtMaxLength.Text = NullToStr(g_oCampo.MaxLength)
        ElseIf g_oCampo.Tipo = TipoTextoMedio Then
            txtMaxLength.Enabled = True
            UpdMaxLength.Enabled = True
            UpdMaxLength.min = 100
            UpdMaxLength.Max = 800
            lblHastaCaracteres.caption = m_sMensajeHasta(2)
            lblHastaCaracteres.Visible = True
            txtMaxLength.Text = NullToStr(g_oCampo.MaxLength)
        ElseIf g_oCampo.Tipo = TipoTextoLargo Then
            txtMaxLength.Enabled = True
            UpdMaxLength.Enabled = True
            UpdMaxLength.min = 800
            UpdMaxLength.Max = 99999
            lblHastaCaracteres.caption = m_sMensajeHasta(3)
            lblHastaCaracteres.Visible = True
            If g_oCampo.MaxLength > 0 Then
                txtMaxLength.Text = NullToStr(g_oCampo.MaxLength)
            Else
                txtMaxLength.Text = ""
            End If
        Else
            If g_oCampo.Tipo = TipoFecha Then
                chkNoFecAntSis.Visible = True
            End If
            lblHastaCaracteres.Visible = False
            txtMaxLength.Enabled = False
            UpdMaxLength.Enabled = False
        End If
            
            
        If g_oCampo.TipoPredef = TipoCampoPredefinido.Atributo And g_oCampo.TipoIntroduccion = Introselec Then
            lblHastaCaracteres.Visible = False
            txtMaxLength.Enabled = False
            UpdMaxLength.Enabled = False
        End If

        If g_oCampo.RecordarValores = True Then
            Me.chkRecordarValores.Value = vbChecked
        Else
            Me.chkRecordarValores.Value = vbUnchecked
        End If
        g_oCampo.RecuperarFormatoPorId
        
        If (g_oCampo.Formato <> "") Then
            lblFormato.Visible = True
            lblValorComprob.Visible = True
            txtFormato.Visible = True
            txtValorComprob.Visible = True
            cmdComprobar.Visible = True
            FraListasEnlazadas.Top = FraListasEnlazadas.Top + 600
            FraListasEnlazadas.Left = 0
            FrameBotonesGrid.Top = FraListasEnlazadas.Top
            lblFormato.Left = 0
            lblFormato.Top = 700
            Frame1.Height = Frame1.Height + 200
            picDatos.Height = picDatos.Height + 200
            sdbgLista.Top = sdbgLista.Top + 200
            Me.Height = Me.Height + 300
            picEdit.Top = picEdit.Top + 250
            txtFormato.Left = txtMin.Left
            txtFormato.Top = lblFormato.Top
            optLista.Top = 1500
            lblValorComprob.Left = 0
            lblValorComprob.Top = 1100
            txtValorComprob.Top = lblValorComprob.Top
            txtValorComprob.Left = txtFormato.Left
            sdbcListaCampoPadre.Top = sdbcListaCampoPadre.Top - 50
            cmdImportarExcel.Left = cmdImportarExcel.Left + 10
            'lblCampoPadre.Top = lblCampoPadre.Top + 200
            'lblCampoPadre.Visible = False
            cmdComprobar.Top = txtValorComprob.Top
            cmdComprobar.Left = 6140
            
            txtFormato.Text = g_oCampo.Formato
        End If
        
    End Select
    If Not g_oCampo Is Nothing Then chkNoFecAntSis.Value = BooleanToSQLBinary(g_oCampo.NoFecAntSis)
    
    m_sTipo = sdbcTipo.Columns("ID").Value
    m_sMaxLength = txtMaxLength.Text

End Sub

Private Sub CargarComboCamposPadre()
    Dim oCampo As CFormItem
    Dim rs As Recordset
    Dim lIdCampoDesglose As Long
    Dim iRowSel As Long
    Dim iRow As Long
    
    If g_oCampo Is Nothing Then Exit Sub
    
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    
    If g_oCampo.Grupo.Formulario Is Nothing Then g_oCampo.Grupo.CargarDatosFormGrupo
    Set rs = oCampo.DevolverListasCampoPadre(g_oCampo.Grupo.Formulario.Id, g_oCampo.Grupo.Id, m_lCampoPadreSeleccionado)
    sdbcListaCampoPadre.AddItem 0 & Chr(m_lSeparador) & ""
    iRow = 1
    While Not rs.EOF
        If rs("ID").Value <> g_oCampo.Id Then
            sdbcListaCampoPadre.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("DEN").Value & Chr(m_lSeparador) & rs("TIPO_CAMPO_GS").Value
            If m_lCampoPadreSeleccionado = rs("ID").Value Then iRowSel = iRow
            iRow = iRow + 1
        End If
        rs.MoveNext
    Wend
    
    If m_lCampoPadreSeleccionado <> 0 Then
        sdbcListaCampoPadre.Columns("ID").Value = m_lCampoPadreSeleccionado
        Dim vbm As Variant
        vbm = sdbcListaCampoPadre.AddItemBookmark(iRowSel)
        sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value = sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").CellValue(vbm)
        
        If cmdImportarExcel.Visible Then cmdImportarExcel.Visible = (sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").CellValue(vbm) <> TipoCampoGS.material)
    End If
    
    Set oCampo = Nothing
    rs.Close
    Set rs = Nothing
End Sub

Private Function Comprobar_txtMax() As Boolean
    If Trim(txtMax.Text) = "" Then Exit Function
    If m_bSaltarFoco Then Exit Function
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
        If Not IsNumeric(Trim(txtMax.Text)) Then
            oMensajes.NoValido m_sIdiMaximo & " " & m_sMensaje(1)
            Comprobar_txtMax = False
            Exit Function
        End If
        
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Then
        If Not IsDate(Trim(txtMax.Text)) Then
            oMensajes.NoValido m_sIdiMaximo & " " & m_sMensaje(2)
            Comprobar_txtMax = False
            Exit Function
        End If
    End If
    Comprobar_txtMax = True
End Function



Private Sub txtMaxLength_GotFocus()

    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If

    m_sMaxLength = txtMaxLength.Text

End Sub

Private Function Comprobar_txtMin() As Boolean
    If Trim(txtMin.Text) = "" Then Exit Function
    If m_bSaltarFoco Then Exit Function
    
    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoNumerico Then
        If Not IsNumeric(Trim(txtMin.Text)) Then
            oMensajes.NoValido m_sIdiMinimo & " " & m_sMensaje(1)
            Comprobar_txtMin = False
            Exit Function
        End If
    
    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoFecha Then
        If Not IsDate(Trim(txtMin.Text)) Then
            oMensajes.NoValido m_sIdiMinimo & " " & m_sMensaje(2)
            Comprobar_txtMin = False
            Exit Function
        End If
    End If
    Comprobar_txtMin = True
End Function

Private Sub txtMaxLength_LostFocus()
    If m_sMaxLength <> txtMaxLength.Text Then
        If Comprobar_txtMaxLength = False Then
            txtMaxLength.Text = m_sMaxLength
        End If
    End If
End Sub


Private Sub UpdMaxLength_DownClick()
    Dim i As Integer
    Dim oIdioma As CIdioma
    Dim bMensajeMostrado As Boolean 'Por si hay varios valores de la lista que superan la maxima longitud, que s�lo nos muestre una vez el mensaje
    Dim irespuesta As Integer

    UpdMaxLength.Enabled = False
        
    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If
    
    m_bError = False
    
    'Si se modifica el n� m�ximo de caracteres permitidos, si los valores definidos no cumplen esta longitud se eliminar�n.
    sdbgLista.MoveLast
    If sdbgLista.Rows > 0 Then
        If m_bMultiIdioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                sdbgLista.Row = i
                For Each oIdioma In g_oIdiomas
                    If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                        
                            If Len(sdbgLista.Columns(oIdioma.Cod).Value) > CInt(txtMaxLength.Text) Then
                                If Not bMensajeMostrado Then
                                    irespuesta = oMensajes.TruncarValor
                                Else
                                    irespuesta = vbYes
                                End If
                                If irespuesta = vbYes Then
                                    bMensajeMostrado = True
                                    sdbgLista.Columns(oIdioma.Cod).Value = Left(sdbgLista.Columns(oIdioma.Cod).Value, CInt(txtMaxLength.Text))
                                Else
                                    UpdMaxLength.Enabled = True
                                    txtMaxLength.Text = txtMaxLength.Text + 1
                                    If Me.Visible Then txtMaxLength.SetFocus
                                    Exit Sub
                                End If
                            End If
                        
                    End If
                Next
                sdbgLista.MovePrevious
                If m_bError Then
                    txtMaxLength.Text = m_sMaxLength
                    GoTo Salir
                End If
            Next i
            
            sdbgLista.MoveFirst
            
            If m_bError Then
                txtMaxLength.Text = m_sMaxLength
                GoTo Salir
            End If
            
        Else
            
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                
                    If Len(sdbgLista.Columns("VALOR").Value) > CInt(txtMaxLength.Text) Then
                        If Not bMensajeMostrado Then
                            irespuesta = oMensajes.TruncarValor
                        Else
                            irespuesta = vbYes
                        End If
                        If irespuesta = vbYes Then
                            bMensajeMostrado = True
                            sdbgLista.Columns("VALOR").Value = Left(sdbgLista.Columns("VALOR").Value, CInt(txtMaxLength.Text))
                            sdbgLista.Columns("VALOR").FieldLen = txtMaxLength.Text
                        Else
                            UpdMaxLength.Enabled = True
                            txtMaxLength.Text = txtMaxLength.Text + 1
                            Exit Sub
                        End If
                    End If
                
                sdbgLista.MovePrevious
                If m_bError Then
                    txtMaxLength.Text = m_sMaxLength
                    GoTo Salir
                End If
            Next i
            sdbgLista.MoveFirst
            If m_bError Then
                txtMaxLength.Text = m_sMaxLength
                GoTo Salir
            End If
        End If
    End If

    If m_bMultiIdioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
        For Each oIdioma In g_oIdiomas
            sdbgLista.Columns(oIdioma.Cod).FieldLen = txtMaxLength.Text
        Next
    Else
        sdbgLista.Columns("VALOR").FieldLen = txtMaxLength.Text
    End If
    
    
Salir:
    UpdMaxLength.Enabled = True
    m_sMaxLength = txtMaxLength.Text

End Sub

Private Sub UpdMaxLength_UpClick()
Dim i As Integer
Dim oIdioma As CIdioma

    m_bError = False

    UpdMaxLength.Enabled = False

    If sdbgLista.DataChanged Then
        sdbgLista.Update
        If m_bError Then
            UpdMaxLength.Enabled = True
            If Me.Visible Then sdbgLista.SetFocus
            Exit Sub
        End If
    End If
    
    'Si se modifica el n� m�ximo de caracteres permitidos.
    sdbgLista.MoveLast
    If sdbgLista.Rows > 0 Then
        If m_bMultiIdioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                sdbgLista.Row = i
                For Each oIdioma In g_oIdiomas
                    If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                        If Len(sdbgLista.Columns(oIdioma.Cod).Value) > CInt(txtMaxLength.Text) Then
                          
                            sdbgLista.Columns(oIdioma.Cod).FieldLen = txtMaxLength.Text
                   
                        End If
                    End If
                Next
                sdbgLista.MovePrevious
            Next i
        Else
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                
                sdbgLista.Columns("VALOR").FieldLen = txtMaxLength.Text
                   
                sdbgLista.MovePrevious
            Next i
            sdbgLista.MoveFirst
        End If
    End If

    'Si se modifica el n� m�ximo de caracteres permitidos, si los valores definidos no cumplen esta longitud se eliminar�n.
    m_bSaltarFoco = True
    UpdMaxLength.Enabled = True
    m_bSaltarFoco = False
    m_sMaxLength = txtMaxLength.Text

End Sub


Function Comprobar_txtMaxLength() As Boolean
Dim i As Integer
Dim oIdioma As CIdioma
Dim irespuesta As Integer
Dim bMensajeMostrado As Boolean

    If txtMaxLength.Text = "" And sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Then
        Comprobar_txtMaxLength = True
        Exit Function
    End If
    
    If txtMaxLength.Text = "" Then Exit Function
    If m_bSaltarFoco Then Exit Function

    If Not IsNumeric(Trim(txtMaxLength.Text)) Then
        oMensajes.NoValido Left(lblMaxLength.caption, Len(lblMaxLength.caption) - 1) & " " & m_sMensaje(1)
        Comprobar_txtMaxLength = False
        Exit Function
    End If

    If sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Then
        If txtMaxLength.Text > 100 Or txtMaxLength.Text < 1 Then
            oMensajes.NoValido Chr(10) & lblMaxLength.caption & " " & m_sMensajeHasta(1)
            Comprobar_txtMaxLength = False
            Exit Function
        End If

    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio Then
        If txtMaxLength.Text > 800 Or txtMaxLength.Text < 100 Then
            oMensajes.NoValido Chr(10) & lblMaxLength.caption & " " & m_sMensajeHasta(2)
            Comprobar_txtMaxLength = False
            Exit Function
        End If

    ElseIf sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoLargo Then
        If txtMaxLength.Text < 800 Then
            oMensajes.NoValido Chr(10) & lblMaxLength.caption & " " & m_sMensajeHasta(3)
            Comprobar_txtMaxLength = False
            Exit Function
        End If
    End If
    
     'Si se modifica el n� m�ximo de caracteres permitidos, si los valores definidos no cumplen esta longitud se eliminar�n.
    sdbgLista.MoveLast
    If sdbgLista.Rows > 0 Then
    
        If m_bMultiIdioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
        
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                For Each oIdioma In g_oIdiomas
                    If sdbgLista.Columns(oIdioma.Cod).Value <> "" Then
                        
                            If Len(sdbgLista.Columns(oIdioma.Cod).Value) > CInt(txtMaxLength.Text) Then
                                If Not bMensajeMostrado Then
                                    irespuesta = oMensajes.TruncarValor
                                Else
                                    irespuesta = vbYes
                                End If
                                If irespuesta = vbYes Then
                                    bMensajeMostrado = True
                                    sdbgLista.Columns(oIdioma.Cod).Value = Left(sdbgLista.Columns(oIdioma.Cod).Value, CInt(txtMaxLength.Text))
                                Else
                                    Comprobar_txtMaxLength = False
                                    txtMaxLength.Text = m_sMaxLength
                                    Exit Function
                                End If
                            End If
                        
                    End If
                Next
                sdbgLista.MovePrevious
                If m_bError Then
                    txtMaxLength.Text = m_sMaxLength
                    Comprobar_txtMaxLength = False
                    Exit Function
                End If
            Next
            sdbgLista.MoveFirst
            If m_bError Then
                txtMaxLength.Text = m_sMaxLength
                Comprobar_txtMaxLength = False
                Exit Function
            End If
            
        Else 'NO ES MULTIIDIOMA
        
            For i = (sdbgLista.Rows - 1) To 0 Step -1
                
                If Len(sdbgLista.Columns("VALOR").Value) > CInt(txtMaxLength.Text) Then
                    If Not bMensajeMostrado Then
                        irespuesta = oMensajes.TruncarValor
                    Else
                        irespuesta = vbYes
                    End If
                    If irespuesta = vbYes Then
                        bMensajeMostrado = True
                        sdbgLista.Columns("VALOR").Value = Left(sdbgLista.Columns("VALOR").Value, CInt(txtMaxLength.Text))
                    Else
                        Comprobar_txtMaxLength = False
                        txtMaxLength.Text = m_sMaxLength
                        Exit Function
                    End If
                End If
                
                sdbgLista.MovePrevious
                If m_bError Then
                    txtMaxLength.Text = m_sMaxLength
                    Comprobar_txtMaxLength = False
                    Exit Function
                End If
            Next i
            
            sdbgLista.MoveFirst
            If m_bError Then
                txtMaxLength.Text = m_sMaxLength
                Comprobar_txtMaxLength = False
                Exit Function
            End If
        End If
    End If
    
    If m_bMultiIdioma = True And g_oCampo.TipoPredef <> Atributo And (g_oCampo.Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampo.Tipo = TiposDeAtributos.TipoTextoMedio) Then
        For Each oIdioma In g_oIdiomas
            sdbgLista.Columns(oIdioma.Cod).FieldLen = txtMaxLength.Text
        Next
    ElseIf g_oCampo.Tipo <> TiposDeAtributos.TipoTextoLargo Then
        sdbgLista.Columns("VALOR").FieldLen = txtMaxLength.Text
    End If

    m_bSaltarFoco = False
    Comprobar_txtMaxLength = True
End Function

Private Sub CrearGridLista()
    Dim i As Integer
    Dim oIdioma As CIdioma
    
    If (sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoCorto Or sdbcTipo.Columns("ID").Value = TiposDeAtributos.TipoTextoMedio) _
    And m_bMultiIdioma = True Then
        sdbgLista.Columns.RemoveAll
        
        i = 0
        For Each oIdioma In g_oIdiomas
            sdbgLista.Columns.Add i
            sdbgLista.Columns.Item(i).Name = oIdioma.Cod
            sdbgLista.Columns.Item(i).caption = oIdioma.Den
            sdbgLista.Columns.Item(i).Width = sdbgLista.Width / g_oIdiomas.Count - 100
            i = i + 1
        Next
    Else
        sdbgLista.Columns.RemoveAll
        
        i = 0
        sdbgLista.Columns.Add i
        sdbgLista.Columns.Item(i).Name = "VALOR"
        sdbgLista.Columns.Item(i).caption = m_sIdiValor
        sdbgLista.Columns.Item(i).Width = sdbgLista.Width - 300
    End If
End Sub

'lCampo:= ID del campo Padre
'bInicializar  -> Si necesitamos que las columnas creadas esten vacias. (Si teniamos unos valores y cambiamos de campo padre, se quedan con los valores de las columnas antiguas)
'bDesdeFormularios --> Nos indica si viene desde el dise�o de formularios o no
'Llamada desde:=
Private Sub CrearCabecerasGridCampoPadre(ByVal lCampo As Long, ByVal bInicializar As Boolean, ByVal bDesdeformularios As Boolean)
    Dim lCampoPadre As Long
    Dim sCampoPadre_CampoPadre As String
    Dim oColumn As SSDataWidgets_B.Column
    Dim sw As Boolean
    Dim i As Integer

    sCampoPadre_CampoPadre = g_oCampo.CargarCampoPadre(lCampo, bDesdeformularios)
    sw = False  'Si se trata de un campo marcado como peso se muestra las 2 scrollbars por defecto
    While sCampoPadre_CampoPadre <> ""
        Dim aux() As String
        aux = Split(sCampoPadre_CampoPadre, "$$$")
        
        If Not sw Then
            'Cargar el combo con el valor del primer Campo Padre
            m_lCampoPadreSeleccionado = aux(0)
            m_sTextoCampoPadreSeleccionado = aux(1)
            m_iTipoCampoPadre = aux(2)
            sdbcListaCampoPadre.Columns("ID").Value = aux(0)
            Me.sdbcListaCampoPadre.Value = aux(0)
            Me.sdbcListaCampoPadre.Text = aux(1)
            sw = True
        End If

        sdbgLista.Columns.Add sdbgLista.Columns.Count
        Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
        oColumn.Name = "CAMPOPADRE_" & aux(0)
        oColumn.caption = aux(1)
        oColumn.Alignment = ssCaptionAlignmentLeft
        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
        oColumn.Position = 0
        
        If aux(2) = TipoCampoGS.material Then
            cmdImportarExcel.Visible = False
        
            oColumn.Style = ssStyleEditButton
            
            sdbgLista.Columns.Add sdbgLista.Columns.Count
            Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
            oColumn.Name = "CAMPOPADREVALOR_" & aux(0)
            oColumn.Position = 1
            oColumn.Text = ""
            oColumn.Visible = False
        Else
            oColumn.Style = ssStyleEdit
            
            sdbgLista.Columns.Add sdbgLista.Columns.Count
            Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
            oColumn.Name = "ORDENPADRE_" & aux(0)
            oColumn.caption = "ORDENPADRE_" & aux(0)
            oColumn.Position = 1
            oColumn.Visible = False
            
            'Si Tiene algun campo padre lo primero asociamos al hijo q el padre
            sdbgLista.Columns.Add sdbgLista.Columns.Count
            Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
            oColumn.Name = "CAMPOPADREPADRE_" & aux(0)
            oColumn.caption = "CAMPOPADREPADRE_" & aux(0)
            oColumn.Position = 2
            oColumn.Visible = False
        End If
        
        If bInicializar Then
            sdbgLista.MoveFirst
            For i = 0 To sdbgLista.Rows
                sdbgLista.Columns("CAMPOPADRE_" & aux(0)).Text = ""
                If aux(2) <> TipoCampoGS.material Then
                    sdbgLista.Columns("ORDENPADRE_" & aux(0)).Text = ""
                    sdbgLista.Columns("CAMPOPADREPADRE_" & aux(0)).Text = ""
                End If
                
                sdbgLista.MoveNext
            Next i
            sdbgLista.MoveFirst
            sdbgLista.Update
        End If
        
        lCampoPadre = aux(0)
        sCampoPadre_CampoPadre = g_oCampo.CargarCampoPadre(aux(0), bDesdeformularios)
    Wend
    If sw Or g_oCampo.Peso Then sdbgLista.ScrollBars = ssScrollBarsBoth
End Sub


Private Sub sdbddValoresLista_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddValoresLista.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValoresLista.Rows - 1
            bm = sdbddValoresLista.GetBookmark(i)
            If UCase(Text) = UCase(sdbddValoresLista.Columns(2).CellText(bm)) Then
                sdbddValoresLista.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValoresLista_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddValoresLista.DataFieldList = "Column 0"
    sdbddValoresLista.DataFieldToDisplay = "Column 2"
End Sub

'Descripcion:=Carga la combo con los valores del campo Padre de la lista.
'Llamada:= Evento que salta al click-ar en la boton del dropdown de la celda
'Tiempo ejecucion:= 0,2seg.
Private Sub sdbddValoresLista_DropDown()
    Dim lIdCampoPadre As Long
    Dim rs As Recordset
    Dim lCampoPadrePadre As Long
    Dim iOrdenPadrePadre As Integer
    Dim vbm As Variant
    Dim dColWidth As Single
    Dim sGMN As String

    If Left(sdbgLista.Columns(sdbgLista.col).Name, 11) <> "CAMPOPADRE_" Then Exit Sub
    
    lIdCampoPadre = Mid(sdbgLista.Columns(sdbgLista.col).Name, 12, Len(sdbgLista.Columns(sdbgLista.col).Name) - 11)
    
    ''A�adimos un marcador para obtener el Id del campoPadre del padre, (Se almacena en la primera fila) cndo se creo las cabeceras
    vbm = sdbgLista.AddItemBookmark(0)
   
    If Not sdbgLista.Columns("CAMPOPADREPADRE_" & lIdCampoPadre) Is Nothing Then
        If sdbgLista.Columns("CAMPOPADREPADRE_" & lIdCampoPadre).CellValue(vbm) <> "" Then lCampoPadrePadre = sdbgLista.Columns("CAMPOPADREPADRE_" & lIdCampoPadre).CellValue(vbm)
        If Not sdbgLista.Columns("CAMPOPADREVALOR_" & lCampoPadrePadre) Is Nothing Then sGMN = sdbgLista.Columns("CAMPOPADREVALOR_" & lCampoPadrePadre).Value
        
        If lCampoPadrePadre > 0 Then
            If Not sdbgLista.Columns("ORDENPADRE_" & lCampoPadrePadre) Is Nothing Then
                If sdbgLista.Columns("ORDENPADRE_" & lCampoPadrePadre).Value <> "" Then
                    iOrdenPadrePadre = sdbgLista.Columns("ORDENPADRE_" & lCampoPadrePadre).Value
                Else
                    Exit Sub
                End If
            End If
        End If
    End If
    

    Set rs = g_oCampo.DevolverValoresListasCampoPadre(lIdCampoPadre, iOrdenPadrePadre, sdbcTipo.Columns("ID").Value, sGMN)
    sdbddValoresLista.RemoveAll
    'sdbddValoresLista.AddItem ""
    While Not rs.EOF
        If rs("TIPO_CAMPO_GS").Value = TipoCampoGS.material Then
            dColWidth = 3500
            
            Dim sCod As String
            Dim sDen As String
            sCod = rs("GMN1") & IIf(IsNull(rs("GMN2")), "", "|" & rs("GMN2") & IIf(IsNull(rs("GMN3")), "", "|" & rs("GMN3") & IIf(IsNull(rs("GMN4")), "", "|" & rs("GMN4"))))
            sDen = rs("GMN1") & IIf(IsNull(rs("GMN2")), "", " - " & rs("GMN2") & IIf(IsNull(rs("GMN3")), "", " - " & rs("GMN3") & IIf(IsNull(rs("GMN4")), "", " - " & rs("GMN4")))) & " - " & rs("GMN_DEN").Value
            sdbddValoresLista.AddItem rs("CAMPO").Value & Chr(m_lSeparador) & sCod & Chr(m_lSeparador) & sDen
        Else
            dColWidth = 1814
            sdbddValoresLista.AddItem rs("CAMPO").Value & Chr(m_lSeparador) & rs("ORDEN").Value & Chr(m_lSeparador) & rs("TEXTO").Value
        End If
        
        rs.MoveNext
    Wend
    
    sdbddValoresLista.Columns("TEXTO").Width = dColWidth
End Sub

''' <summary>
''' Tras seleccionar un combo padre se carga el combo hijo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddValoresLista_CloseUp()
    Dim lIdCampoPadre As Long
    Dim HayQLimpiar As Boolean

    If sdbgLista.col = -1 Then Exit Sub
    lIdCampoPadre = Mid(sdbgLista.Columns(sdbgLista.col).Name, 12, Len(sdbgLista.Columns(sdbgLista.col).Name) - 11)

    If Not sdbgLista.Columns("ORDENPADRE_" & lIdCampoPadre) Is Nothing Then
        If sdbgLista.Columns("ORDENPADRE_" & lIdCampoPadre).Value <> sdbddValoresLista.Columns("ORDEN").Value Then
            m_bModifLista = True
            HayQLimpiar = True
        End If
        
        sdbgLista.Columns("ORDENPADRE_" & lIdCampoPadre).Value = sdbddValoresLista.Columns("ORDEN").Value
    Else
        If Not sdbgLista.Columns("CAMPOPADREVALOR_" & lIdCampoPadre) Is Nothing Then
            HayQLimpiar = (sdbgLista.Columns("CAMPOPADREVALOR_" & lIdCampoPadre).Value <> sdbddValoresLista.Columns("ORDEN").Value)
            sdbgLista.Columns("CAMPOPADREVALOR_" & lIdCampoPadre).Value = sdbddValoresLista.Columns("ORDEN").Value
        End If
    End If

    Dim i As Integer
    Dim b_Seguir As Boolean
    Dim l_IdCampoPadre As Long
    b_Seguir = True
    i = 0
    While b_Seguir
        b_Seguir = False
        
        For i = 0 To sdbgLista.Columns.Count - 1
            If Left(sdbgLista.Columns(i).Name, 16) = "CAMPOPADREPADRE_" Then
                If sdbgLista.Columns(i).Value = lIdCampoPadre Then
                    l_IdCampoPadre = Mid(sdbgLista.Columns(i).Name, 17, Len(sdbgLista.Columns(i).Name) - 16)
                    sdbgLista.Columns("CAMPOPADRE_" & l_IdCampoPadre).DropDownHwnd = 0
                    If HayQLimpiar Then sdbgLista.Columns("CAMPOPADRE_" & l_IdCampoPadre).Text = ""
                    b_Seguir = True
                    lIdCampoPadre = l_IdCampoPadre
                    Exit For
                End If
            End If
        Next
    Wend
End Sub

''' <summary>
''' Tras cerrar el combo, reconfigura el grid
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcListaCampoPadre_CloseUp()
    Dim oColumn As SSDataWidgets_B.Column
    Dim i As Integer
    Dim sCampoHijo As String
    
    If m_lCampoPadreSeleccionado <> sdbcListaCampoPadre.Columns("ID").Value Then
        m_bRespetarLista = True
        m_bModifLista = True
        'Lo primero que hare sera no mostrar el combo en las celdas de campos padre para que al eliminar no desaparezcan los valores
        For i = 0 To sdbgLista.Cols - 1
                sdbgLista.Columns(i).DropDownHwnd = 0
        Next
        
        If m_lCampoPadreSeleccionado > 0 Then
            'Comprobamos antes de cambiar de campo padre que la lista no sea padre de otro lista
            sCampoHijo = g_oCampo.DevolverNombreCampoHijo
            If sCampoHijo <> "" Then
                sdbcListaCampoPadre.Value = m_lCampoPadreSeleccionado
                sdbcListaCampoPadre.Text = m_sTextoCampoPadreSeleccionado
                oMensajes.NoEliminarHijosAsociados g_oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den, sCampoHijo
                
                'MsgBox "No se puede cambiar el campo padre de " & g_oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & ", ya que se" & vbCrLf & "perder�an los valores de la lista y estos est�n relacinados con" & vbCrLf & "valores de la lista " & sCampoHijo & ". Elimine esta relaci�n para poder establecer " & vbCrLf & "otro campo padre."
                Exit Sub
            End If
        End If
        
        For i = sdbgLista.Columns.Count - 1 To 0 Step -1
            If Left(sdbgLista.Columns(i).Name, 10) = "CAMPOPADRE" Or Left(sdbgLista.Columns(i).Name, 10) = "ORDENPADRE" Then
                sdbgLista.Columns.Remove (i)
            End If
        Next i
    
        m_lCampoPadreSeleccionado = sdbcListaCampoPadre.Columns("ID").Value
        m_iTipoCampoPadre = StrToDbl0(sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value)
        
        If m_lCampoPadreSeleccionado <> 0 Then
            sdbgLista.Columns.Add sdbgLista.Columns.Count
            Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
            oColumn.Name = "CAMPOPADRE_" & m_lCampoPadreSeleccionado
            oColumn.caption = sdbcListaCampoPadre.Columns("DENOMINACION").Value
            oColumn.Alignment = ssCaptionAlignmentLeft
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            oColumn.Position = 0
            
            If sdbcListaCampoPadre.Columns("TIPO_CAMPO_GS").Value = TipoCampoGS.material Then
                cmdImportarExcel.Visible = False
                
                'Si es un campo de tipo material
                oColumn.Style = ssStyleEditButton
                oColumn.Locked = True
                
                sdbgLista.Columns.Add sdbgLista.Columns.Count
                Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
                oColumn.Name = "CAMPOPADREVALOR_" & m_lCampoPadreSeleccionado
                oColumn.Position = 1
                oColumn.Text = ""
                oColumn.Visible = False
                
                sdbgLista.MoveFirst
                For i = 0 To sdbgLista.Rows
                   sdbgLista.Columns("CAMPOPADRE_" & m_lCampoPadreSeleccionado).Text = ""
                   sdbgLista.Columns("CAMPOPADREVALOR_" & m_lCampoPadreSeleccionado).Text = ""
                   
                   sdbgLista.MoveNext
                Next i
            Else
                cmdImportarExcel.Visible = True
                
                'Si es un campo de tipo lista
                oColumn.Style = ssStyleComboBox
                
                sdbgLista.Columns.Add sdbgLista.Columns.Count
                Set oColumn = sdbgLista.Columns(sdbgLista.Columns.Count - 1)
                oColumn.Name = "ORDENPADRE_" & m_lCampoPadreSeleccionado
                oColumn.Position = 1
                oColumn.Visible = False
                
                sdbgLista.MoveFirst
                For i = 0 To sdbgLista.Rows
                   sdbgLista.Columns("CAMPOPADRE_" & m_lCampoPadreSeleccionado).Text = ""
                   sdbgLista.Columns("ORDENPADRE_" & m_lCampoPadreSeleccionado).Text = ""
                   
                   sdbgLista.MoveNext
                Next i
            End If
        Else
            sdbcListaCampoPadre.Value = 0
            sdbcListaCampoPadre.Text = ""
        End If
        
        CrearCabecerasGridCampoPadre m_lCampoPadreSeleccionado, True, (g_sOrigen = "frmFormularios")
        m_bRespetarLista = False
    End If
    Me.sdbgLista.ScrollBars = ssScrollBarsBoth
End Sub


Private Sub sdbcListaCampoPadre_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyEscape Then
    Exit Sub
ElseIf KeyCode = vbKeyBack Then
    sdbcListaCampoPadre.Text = ""
End If
End Sub

Private Sub sdbcListaCampoPadre_PositionList(ByVal Text As String)
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcListaCampoPadre.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcListaCampoPadre.Rows - 1
            bm = sdbcListaCampoPadre.GetBookmark(i)
            If UCase(Text) = UCase(sdbcListaCampoPadre.Columns(1).CellText(bm)) Then
                sdbcListaCampoPadre.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>
''' Funci�n que se encarga de recoger los datos de una excel e importarlos.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdImportarExcel_Click()

Dim sFileName As String
Dim sIdiDialogoExcel As String
Dim sIdiFiltroExcel As String
Dim sPorDefecto As String

Dim sConnect As String
Dim oExcelAdoConn As ADODB.Connection
Dim oExcelAdoRS As ADODB.Recordset
Dim Resultadoxls()
Dim iNumColumnas, i As Integer
Dim lNumFilas, j As Long
Dim Orden As Integer
Dim ValorIncorrecto As Boolean
Dim Cual As String

sPorDefecto = "Fichero"

On Error GoTo Error


sFileName = MostrarCommonDialog(sIdiDialogoExcel, sIdiFiltroExcel, sPorDefecto & ".xls")
If sFileName = "" Then
    Exit Sub
End If

Set oExcelAdoConn = New ADODB.Connection
sConnect = "Provider=MSDASQL.1;" _
         & "Extended Properties=""DBQ=" & sFileName & ";" _
         & "Driver={Microsoft Excel Driver (*.xls)};" _
         & "FIL=excel 8.0;" _
         & "ReadOnly=0;" _
         & "UID=admin;"""

oExcelAdoConn.Open sConnect

Set oExcelAdoRS = New ADODB.Recordset

oExcelAdoRS.Open "SELECT * FROM [" & "LISTA" & "$]", oExcelAdoConn


Dim k As Integer
For k = 0 To sdbgLista.Cols - 1
        sdbgLista.Columns(k).DropDownHwnd = 0
Next
        
Dim sCampoPadre_CampoPadre As String
Dim sTextoGrid As String
Dim sValor As String
Dim sCadena As String
Dim lIdCampoPadre As Long
Dim sValorABuscar As String
Dim sLinea As String

Dim oCampo As CFormItem
Set oCampo = oFSGSRaiz.Generar_CFormCampo
Set oCampo.Grupo = oFSGSRaiz.Generar_CFormGrupo
oCampo.Grupo.Id = g_oCampo.Grupo.Id

Dim oValor As CCampoValorLista

If Not oExcelAdoRS.EOF Then
    
    Resultadoxls = oExcelAdoRS.GetRows()
    iNumColumnas = UBound(Resultadoxls) + 1
    lNumFilas = UBound(Resultadoxls, 2) + 1
    Dim filasMultiIdioma As Integer
    m_bRespetarLista = True
    
    If m_bMultiIdioma Then
        filasMultiIdioma = g_oIdiomas.Count
    Else
        filasMultiIdioma = 1
    End If
    
    Orden = Me.sdbgLista.Rows + 1
    
    For j = 0 To lNumFilas - 1
        sTextoGrid = ""
        sCadena = ""
        lIdCampoPadre = StrToDbl0(Me.sdbcListaCampoPadre.Columns("ID").Value)
        
        ValorIncorrecto = False
        
        For i = (iNumColumnas - 1) To 0 Step -1
            
            sValor = NullToStr(Resultadoxls(i, j))
            
            If sValor = "" Then
                oMensajes.ImportarDeExcelACombo 2
                
                ValorIncorrecto = True
                Exit For
            End If
            
            If i > iNumColumnas - filasMultiIdioma - 1 Then
                'Columna del valor
                If sCadena = "" Then
                    sCadena = sValor
                Else
                    sCadena = sCadena & Chr(m_lSeparador) & sValor
                End If
            Else
                'Comprobar si se trata de una lista enlazada
                If lIdCampoPadre > 0 Then
                    'Comprobar si el campo padre seleccionado tb dispone de otro campo padre...y sucesivos
                    oCampo.Id = lIdCampoPadre
                    oCampo.Tipo = sdbcTipo.Columns("ID").Value
                    oCampo.CargarValoresLista
                    
                    If Not oCampo.ValoresLista Is Nothing Then
                        'Recorrido de los valores de lista hasta que encontremos el valor a buscar
                        For Each oValor In oCampo.ValoresLista
                            Select Case sdbcTipo.Columns("ID").Value
                                Case TiposDeAtributos.TipoFecha
                                    sValorABuscar = oValor.valorFec
                                Case TiposDeAtributos.TipoNumerico
                                    sValorABuscar = oValor.valorNum
                                Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                                    sValorABuscar = NullToStr(oValor.valorText.Item(gParametrosInstalacion.gIdioma).Den)
                            End Select
                            
                            If sValor = sValorABuscar Then
                                If sTextoGrid <> "" Then sTextoGrid = sTextoGrid & Chr(m_lSeparador)
                                sTextoGrid = sTextoGrid & sValorABuscar & Chr(m_lSeparador) & oValor.Orden
                                'Buscar el campo Padre del campo que estamos buscando
                                sCampoPadre_CampoPadre = oCampo.CargarCampoPadre(lIdCampoPadre, True)
                                If sCampoPadre_CampoPadre <> "" Then
                                    Dim aux() As String
                                    aux = Split(sCampoPadre_CampoPadre, "$$$")
                                    lIdCampoPadre = aux(0)
                                    
                                    sTextoGrid = sTextoGrid & Chr(m_lSeparador) & lIdCampoPadre
                                Else
                                    sTextoGrid = sTextoGrid & Chr(m_lSeparador) & 0
                                End If
                                Exit For
                            End If

                        Next
                        
                    End If
                                                            
                End If
                
            End If
                        
        Next i
        
        If Not ValorIncorrecto Then 'A�adimos la linea encontrada
            'valor+existe+orden
            'valorSpa+valorEng+existe+orden
            sCadena = sCadena & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & Orden & Chr(m_lSeparador) & Orden
            
            If sTextoGrid <> "" Then sCadena = sCadena & Chr(m_lSeparador)
            
            sLinea = sCadena & sTextoGrid
            If (lIdCampoPadre > 0 And sTextoGrid <> "") Or (lIdCampoPadre = 0 And sCadena <> "") Then
                If CtrlRepetidos(sLinea, Cual) Then
                    sdbgLista.AddItem sLinea
                
                    Orden = Orden + 1
                Else
                    oMensajes.ImportarDeExcelACombo 1, Cual
                End If
            End If
        End If
    Next j
    sdbgLista.Update
    m_bModifLista = True
    m_bRespetarLista = False
        
    oExcelAdoRS.Close

End If

Set oCampo = Nothing


Exit Sub


Error:

If err.Number <> cdlCancel Then
    oMensajes.MensajeOKOnly 754
    oExcelAdoConn.Close
End If



End Sub



Function MostrarCommonDialog(titulo As String, filtro As String, sFichero As String) As String
    
    cmmdExcel.CancelError = True

    On Error Resume Next
    cmmdExcel.DialogTitle = titulo
    cmmdExcel.Filter = filtro
    cmmdExcel.filename = ValidFilename(sFichero)
    cmmdExcel.Filter = "Excel files|*.xls"
    cmmdExcel.ShowOpen

    If err.Number = cdlCancel Then
        ' The user canceled.
        MostrarCommonDialog = ""
        Exit Function
    End If
    On Error GoTo 0
    
    If cmmdExcel.FileTitle = "" Then
        MostrarCommonDialog = ""
        Exit Function
    End If

    MostrarCommonDialog = cmmdExcel.filename
    
End Function

''' <summary>
''' Determina si es ya esta entre los valores de la lista o no
''' </summary>
''' <param name="sLinea">Linea a insertar en lista posibles valores</param>
''' <param name="Cual">Padre/Padre/.../Valor repetido</param>
''' <returns>Si es repetido o no</returns>
''' <remarks>Llamada desde: xxx ; Tiempo m�ximo: 0,2</remarks>
Private Function CtrlRepetidos(ByVal sLinea As String, ByRef Cual As String) As Boolean
    Dim vbm As Variant
    Dim i As Integer
    Dim ValorDeLinea() As String
    Dim IdiomasLinea() As String
    Dim j As Integer
    
    CtrlRepetidos = True
    Cual = ""
    
    ValorDeLinea = Split(sLinea, Chr(m_lSeparador))
    If m_sListaIdiomaTexto = "" Then
    Else
        IdiomasLinea = Split(m_sListaIdiomaTexto, "##")
    End If
    
    For i = 0 To sdbgLista.Rows - 1
        vbm = sdbgLista.AddItemBookmark(i)
        
        Cual = ""
        If m_sListaIdiomaTexto = "" Or g_oCampo.TipoPredef = Atributo Then
            If sdbgLista.Columns("VALOR").CellValue(vbm) = ValorDeLinea(0) Then
                Cual = ValorDeLinea(0)
                
                If Not CtrlRepetidosPosible(sLinea, Cual) Then
                    CtrlRepetidos = False
                    Exit Function
                End If
            End If
        Else
            For j = 0 To UBound(IdiomasLinea) - 1
                If sdbgLista.Columns(IdiomasLinea(j)).CellValue(vbm) = ValorDeLinea(0 + j) Then
                    Cual = ValorDeLinea(0 + j)
                    
                    If Not CtrlRepetidosPosible(sLinea, Cual) Then
                        CtrlRepetidos = False
                        Exit Function
                    End If
                End If
            Next
        End If
    Next i
End Function

''' <summary>
''' Determina si es ya esta entre los valores de la lista o no
''' </summary>
''' <param name="sLinea">Linea a insertar en lista posibles valores</param>
''' <param name="Cual">Padre/Padre/.../Valor repetido</param>
''' <returns>Si es repetido o no</returns>
''' <remarks>Llamada desde: CtrlRepetidos ; Tiempo m�ximo: 0,2</remarks>
Private Function CtrlRepetidosPosible(ByVal sLinea As String, ByRef Cual As String) As Boolean
    Dim vbm As Variant
    Dim DifiereEnPadre As Boolean
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim ValorDeLinea() As String
    Dim aux As String
    
    CtrlRepetidosPosible = True
    
    ValorDeLinea = Split(sLinea, Chr(m_lSeparador))
    
    aux = Cual

    For i = 0 To sdbgLista.Rows - 1
        vbm = sdbgLista.AddItemBookmark(i)
        
        Cual = aux
            
        DifiereEnPadre = False
        For j = 1 To m_iNumeroPadres
            If m_sListaIdiomaTexto = "" Then
                k = j * 3
            Else
                k = (j * 3) + 2
            End If
            
            Cual = sdbgLista.Columns(k).CellValue(vbm) & "/" & Cual
            
            If sdbgLista.Columns(k).CellValue(vbm) <> ValorDeLinea(k) Then
                DifiereEnPadre = True
                Exit For
            End If
        Next
        
        If Not DifiereEnPadre Then
            CtrlRepetidosPosible = False
            Exit For
        End If
    Next i
End Function
