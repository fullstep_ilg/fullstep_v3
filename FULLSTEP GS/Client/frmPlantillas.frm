VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPlantillas 
   Caption         =   "Plantillas"
   ClientHeight    =   6150
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   7980
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPlantillas.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6150
   ScaleWidth      =   7980
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   4163
      TabIndex        =   2
      Top             =   5790
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   2843
      TabIndex        =   1
      Top             =   5790
      Width           =   1005
   End
   Begin MSComctlLib.ListView lstPlantilla 
      Height          =   5655
      Left            =   0
      TabIndex        =   0
      Top             =   60
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   9975
      View            =   2
      Arrange         =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillas.frx":014A
            Key             =   "Plantilla"
            Object.Tag             =   "Plantilla"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillas.frx":059C
            Key             =   "PlantillaConVista"
            Object.Tag             =   "PlantillaConVista"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPlantillas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sCaption As String
Private m_oProcesos As CProcesos
Public g_sOrigen As String
'Variable para controlar si se llama al Unload desde Aceptar,Cancelar
'o simplemente se cierra el form
Private m_bCerrar As Boolean

Private m_sIdiDist As String

'edu pm98. variable para pasar al formulario el codigo de grupo 1 que servir� de filtro
Public g_GMN1 As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Function DevolverIdPlantilla(ByVal Item As MSComctlLib.listItem) As Long
Dim iPosicion As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Item Is Nothing Then Exit Function
    
    'posici�n del primer -%$
    iPosicion = InStr(1, Item.key, "-%$")
    DevolverIdPlantilla = Mid(Item.key, 2, iPosicion - 2)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "DevolverIdPlantilla", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function DevolverNombrePlantilla(ByVal Item As MSComctlLib.listItem) As String
Dim iPosicion1 As Integer
Dim iPosicion2 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Item Is Nothing Then Exit Function
    
    'posici�n del primer -%$
    iPosicion1 = InStr(1, Item.key, "-%$")
    iPosicion1 = iPosicion1 + 3
    'posici�n del segundo -%$
    iPosicion2 = InStr(iPosicion1, Item.key, "-%$")
    DevolverNombrePlantilla = Mid(Item.key, iPosicion1, iPosicion2 - iPosicion1)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "DevolverNombrePlantilla", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function DevolverDescrPlantilla(ByVal Item As MSComctlLib.listItem) As String
Dim iPosicion1 As Integer
Dim iPosicion2 As Integer
Dim iPosicion3 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Item Is Nothing Then Exit Function
    
    'posici�n del primer -%$
    iPosicion1 = InStr(1, Item.key, "-%$")
    iPosicion1 = iPosicion1 + 3
    'posici�n del segundo -%$
    iPosicion2 = InStr(iPosicion1, Item.key, "-%$")
    iPosicion2 = iPosicion2 + 3
    'posici�n del tercer -%$
    iPosicion3 = InStr(iPosicion2, Item.key, "-%$")
    
    DevolverDescrPlantilla = Mid(Item.key, iPosicion2, iPosicion3 - iPosicion2)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "DevolverDescrPlantilla", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function DevolverFechaPlantilla(ByVal Item As MSComctlLib.listItem) As String
Dim iPosicion1 As Integer
Dim iPosicion2 As Integer
Dim iPosicion3 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Item Is Nothing Then Exit Function
    
    'posici�n del primer -%$
    iPosicion1 = InStr(1, Item.key, "-%$")
    iPosicion1 = iPosicion1 + 3
    'posici�n del segundo -%$
    iPosicion2 = InStr(iPosicion1, Item.key, "-%$")
    iPosicion2 = iPosicion2 + 3
    'posici�n del tercer -%$
    iPosicion3 = InStr(iPosicion2, Item.key, "-%$")
    iPosicion3 = iPosicion3 + 2
    
    DevolverFechaPlantilla = Right(Item.key, Len(Item.key) - iPosicion3)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "DevolverFechaPlantilla", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Private Function DevolverPresupuestosObligatoriosQueFaltan(ByVal sDatos As String) As String
Dim iPosSeparador As Integer
Dim sPres As String
Dim sCadena As String
Dim iInicio As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iInicio = 1
    iPosSeparador = 1
    sCadena = ""
    While iInicio <= Len(sDatos)
        sPres = Mid(sDatos, iInicio, 1)
        Select Case sPres
                Case "0"
                        If sCadena = "" Then
                            sCadena = m_sIdiDist
                        Else
                            sCadena = sCadena & ", " & m_sIdiDist
                        End If
                Case "1"
                        If sCadena = "" Then
                            sCadena = gParametrosGenerales.gsSingPres1
                        Else
                            sCadena = sCadena & ", " & gParametrosGenerales.gsSingPres1
                        End If
                Case "2"
                        If sCadena = "" Then
                            sCadena = gParametrosGenerales.gsSingPres2
                        Else
                            sCadena = sCadena & ", " & gParametrosGenerales.gsSingPres2
                        End If
                Case "3"
                        If sCadena = "" Then
                            sCadena = gParametrosGenerales.gsSingPres3
                        Else
                            sCadena = sCadena & ", " & gParametrosGenerales.gsSingPres3
                        End If
                Case 4
                        If sCadena = "" Then
                            sCadena = gParametrosGenerales.gsSingPres4
                        Else
                            sCadena = sCadena & ", " & gParametrosGenerales.gsSingPres4
                        End If
        End Select
        iPosSeparador = iInicio + 1
        iInicio = iInicio + 2
    Wend
    
    DevolverPresupuestosObligatoriosQueFaltan = sCadena
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "DevolverPresupuestosObligatoriosQueFaltan", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function

Private Sub cmdAceptar_Click()
Dim itemSeleccionado As MSComctlLib.listItem
Dim oproce As CProceso
Dim udtTeserror As TipoErrorSummit
Dim iResp As Integer
'Dim sDatosObl As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCerrar = False
    If g_sOrigen = "frmPROCEM" Then
        Set itemSeleccionado = lstPlantilla.selectedItem
        frmPROCEPlantilla.g_sOrigen = "frmPROCEPlantillaM"
        Set frmPROCEPlantilla.g_oProceso = frmPROCE.g_oProcesoSeleccionado
        frmPROCEPlantilla.txtFecha.Text = DevolverFechaPlantilla(itemSeleccionado)
        frmPROCEPlantilla.txtDescr.Text = DevolverDescrPlantilla(itemSeleccionado)
        frmPROCEPlantilla.lblNom.caption = DevolverNombrePlantilla(itemSeleccionado)
        frmPROCEPlantilla.g_lIdPlantilla = DevolverIdPlantilla(itemSeleccionado)
        Unload Me
        frmPROCEPlantilla.Show 1
    Else
        If g_sOrigen = "frmPROCEG" Then 'Nuevo proceso desde plantilla
            Set itemSeleccionado = lstPlantilla.selectedItem
            Screen.MousePointer = vbHourglass
            Set oproce = oFSGSRaiz.Generar_CProceso
            oproce.Anyo = frmPROCE.sdbcAnyo.Value
            oproce.GMN1Cod = frmPROCE.sdbcGMN1_4Cod.Value
            udtTeserror = oproce.GenerarProcesoDesdePlantilla(DevolverIdPlantilla(itemSeleccionado), ADMIN_OBLIGATORIO)
            If udtTeserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                If udtTeserror.NumError = TESImposibleAnyadirProceDesdePlantFaltanDatosObl Then
                    If udtTeserror.Arg1 = 126 Then 'Faltan sobres
                        oMensajes.SobresFaltanEnPlantilla
                    Else
                        udtTeserror.Arg1 = DevolverPresupuestosObligatoriosQueFaltan(udtTeserror.Arg1)
                        basErrores.TratarError udtTeserror
                    End If
                    Set oproce = Nothing
                    frmPROCE.g_bCancelar = True
                    Exit Sub
                ElseIf udtTeserror.NumError = TESImposibleAccederAlArchivo Then
                    iResp = oMensajes.ImposibleAccederAFicheroEnAltaProceso(udtTeserror.Arg1)
                    If iResp = vbNo Then
                        Set oproce = Nothing
                        frmPROCE.g_bCancelar = True
                        Exit Sub
                    End If
                Else
                    basErrores.TratarError udtTeserror
                    Set oproce = Nothing
                    frmPROCE.g_bCancelar = True
                    Exit Sub
                End If
            End If
            Set frmPROCE.g_oProcesoSeleccionado = oproce
            frmPROCE.g_bCancelar = False
            Screen.MousePointer = vbNormal
            Unload Me
        End If
    End If
    
    Set oproce = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCerrar = False
    frmPROCE.g_bCancelar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, True
m_bActivado = False
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    Me.caption = m_sCaption
    lstPlantilla.View = lvwIcon
    Set m_oProcesos = oFSGSRaiz.generar_CProcesos
       
    LlenarListaConPlantillas
    
    m_bCerrar = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarRecursos()
Dim Adores As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_PLANTILLAS, basPublic.gParametrosInstalacion.gIdioma)
    If Not Adores Is Nothing Then
        If g_sOrigen = "frmPROCEM" Then
            m_sCaption = Adores(0).Value
            Adores.MoveNext
        Else
            If g_sOrigen = "frmPROCEG" Then
                Adores.MoveNext
                m_sCaption = Adores(0).Value
            End If
        End If
        Adores.MoveNext
        cmdAceptar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        m_sIdiDist = Adores(0).Value
        
        Adores.Close
    End If
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub LlenarListaConPlantillas()
Dim Adores As Ador.Recordset
Dim sClave As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    lstPlantilla.ListItems.clear

    'edu pm98. Si se especifica un codigo de grupo, filtrar plantillas de ese grupo mas plantillas sin restriccion.
    
    If IsNull(g_GMN1) Then
        g_GMN1 = ""
    End If
    
    If frmPROCE.g_bRAperMatComp Then
        If g_GMN1 <> "" Then
            Set Adores = m_oProcesos.DevolverPlantillasDeGMN1(g_GMN1, True, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod)
        Else
            Set Adores = m_oProcesos.DevolverTodasLasPlantillas(True, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod)
        End If
    Else
        If g_GMN1 <> "" Then
            Set Adores = m_oProcesos.DevolverPlantillasDeGMN1(g_GMN1)
        Else
            Set Adores = m_oProcesos.DevolverTodasLasPlantillas
        End If
    End If
    
    While Not Adores.EOF
        sClave = Adores("ID").Value & "-%$" & Adores("NOM").Value & "-%$" & Adores("DESCR").Value & "-%$" & Adores("FECHA").Value
        
        If Adores("CONFVISTAS").Value = 1 Then
            lstPlantilla.ListItems.Add , "P" & sClave, Adores("NOM").Value, "PlantillaConVista"
        Else
            lstPlantilla.ListItems.Add , "P" & sClave, Adores("NOM").Value, "Plantilla"
        End If
        Adores.MoveNext
    Wend

    Adores.Close
    Set Adores = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "LlenarListaConPlantillas", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Resize()

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Me.Width < 300 Then Exit Sub
If Me.Height < 1000 Then Exit Sub

lstPlantilla.Width = Me.Width - 150
lstPlantilla.Height = Me.Height - 900

cmdAceptar.Top = Me.Height - 800
cmdCancelar.Top = Me.Height - 800
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 

End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bCerrar Then
        If g_sOrigen = "frmPROCEG" Then
            frmPROCE.g_bCancelar = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPlantillas", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


