VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMantPedidos 
   Caption         =   "Materiales"
   ClientHeight    =   5535
   ClientLeft      =   1290
   ClientTop       =   2625
   ClientWidth     =   10215
   Icon            =   "frmMantPedidos.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5535
   ScaleWidth      =   10215
   Begin VB.CheckBox chkTodosIdiomas 
      Caption         =   "Mostrar denominaciones en todos los idiomas"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   4455
   End
   Begin SSDataWidgets_B.SSDBDropDown ssdbddConcepto 
      Height          =   1575
      Left            =   360
      TabIndex        =   7
      Top             =   1470
      Width           =   1095
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   1588
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   50
      _ExtentX        =   1931
      _ExtentY        =   2778
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown ssdbddRecepcion 
      Height          =   1575
      Left            =   3780
      TabIndex        =   8
      Top             =   990
      Width           =   1515
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   2461
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   50
      _ExtentX        =   2672
      _ExtentY        =   2778
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown ssdbddAlmacen 
      Height          =   1575
      Left            =   1770
      TabIndex        =   9
      Top             =   1200
      Width           =   1515
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   2461
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   50
      _ExtentX        =   2672
      _ExtentY        =   2778
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddUnidades 
      Height          =   1575
      Left            =   5550
      TabIndex        =   10
      Top             =   1110
      Width           =   3855
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1085
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   4868
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      _ExtentX        =   6800
      _ExtentY        =   2778
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTiposPedido 
      Height          =   4575
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   9885
      _Version        =   196617
      DataMode        =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      stylesets.count =   9
      stylesets(0).Name=   "Atributos"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmMantPedidos.frx":014A
      stylesets(1).Name=   "IntOK"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmMantPedidos.frx":0203
      stylesets(2).Name=   "Normal"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmMantPedidos.frx":021F
      stylesets(3).Name=   "ActiveRow"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   -2147483647
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmMantPedidos.frx":023B
      stylesets(3).AlignmentText=   0
      stylesets(4).Name=   "IntHeader"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmMantPedidos.frx":0257
      stylesets(4).AlignmentText=   0
      stylesets(4).AlignmentPicture=   0
      stylesets(5).Name=   "Adjudica"
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmMantPedidos.frx":03AF
      stylesets(6).Name=   "IntKO"
      stylesets(6).ForeColor=   16777215
      stylesets(6).BackColor=   255
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmMantPedidos.frx":0724
      stylesets(7).Name=   "styEspAdjSi"
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmMantPedidos.frx":0740
      stylesets(8).Name=   "HayImpuestos"
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmMantPedidos.frx":07BD
      stylesets(8).AlignmentText=   2
      stylesets(8).AlignmentPicture=   2
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   2646
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   5
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(0).HeadStyleSet=   "Normal"
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   200
      Columns(1).Locked=   -1  'True
      Columns(1).HeadStyleSet=   "Normal"
      Columns(2).Width=   1508
      Columns(2).Caption=   "Concepto"
      Columns(2).Name =   "CONCEP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HeadStyleSet=   "Normal"
      Columns(3).Width=   1508
      Columns(3).Caption=   "Almac�n"
      Columns(3).Name =   "ALMAC"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1508
      Columns(4).Caption=   "Recepci�n"
      Columns(4).Name =   "RECEP"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Categor�as"
      Columns(5).Name =   "CATEG"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   1
      Columns(6).Width=   1402
      Columns(6).Caption=   "Atributos"
      Columns(6).Name =   "ATRIBUTOS"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   4
      Columns(6).ButtonsAlways=   -1  'True
      Columns(6).HeadStyleSet=   "Normal"
      Columns(7).Width=   794
      Columns(7).Caption=   "Baja"
      Columns(7).Name =   "BAJA"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      _ExtentX        =   17436
      _ExtentY        =   8070
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      ScaleHeight     =   375
      ScaleMode       =   0  'User
      ScaleWidth      =   9975
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   5160
      Width           =   9975
      Begin VB.CommandButton cmdRestaurarTiposPedido 
         Caption         =   "&Actualizar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   0
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   15
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   8880
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacerTiposPedido 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1080
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminarTiposPedido 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   0
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   15
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7440
      Top             =   4920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":0B32
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":0F86
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":1050
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":1100
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":11C1
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":1271
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":1331
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":16C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":1782
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":1832
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":18F2
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidos.frx":19A2
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmMantPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variable que contendra los articulos
Public g_oTiposPedido As CTiposPedido
Private m_oTipoPedidoEnEdicion As CTipoPedido
Private m_oTiposPedidoAModificar As CTiposPedido

' Variables de restricciones
Private m_bModifTipoPedi As Boolean
Private m_bRAtributo As Boolean


'Variables para la grid
Private m_bModoEdicion As Boolean
Private m_lIndiceTiposPedido As Long
Private m_bAnyaError As Boolean
Private m_bModError As Boolean
Private m_bValError As Boolean
Private m_oIBAseDatosEnEdicion As IBaseDatos
Private m_bRespetar As Boolean


' Variables para interactuar con otros forms
Public oIBaseDatos As IBaseDatos

' Variable de control de flujo
Public Accion As accionessummit
Private AccionTipoPedido As accionessummit

'Variables de idiomas
Private m_sIdiTitulos(1 To 3) As String
Private m_sIdiModos(2) As String
Private m_sIdiCod As String
Private m_sIdiDen As String
Private m_sIdiOrden As String
Private m_sMultiplesCategorias As String

Private m_sTextConcepAlmacRecep() As String

Private m_sIdiConcepto As String
Private m_sIdiAlmacenable As String
Private m_sIdiRecepcionable As String

Private m_aBookmarks As Collection

Private m_iNumIdiomas As Integer
Private m_oIdiomas As CMultiidiomas
Private m_bColumnasAnyadidas As Boolean






Private Sub cmdEliminarTiposPedido_Click()
    
    ''' * Objetivo: Eliminar articulos
    Dim vItems As Variant
    Dim irespuesta As Integer
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim sIdioma As String
    Dim oDenominacion As CMultiidioma

        
    If sdbgTiposPedido.Rows = 0 Then Exit Sub
    If IsNull(sdbgTiposPedido.Bookmark) Then Exit Sub
    sdbgTiposPedido.SelBookmarks.Add sdbgTiposPedido.Bookmark

    Select Case sdbgTiposPedido.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
             sIdioma = ""
             If Not m_oIdiomas Is Nothing Then
                For Each oDenominacion In m_oIdiomas
                    If oDenominacion.Cod = basPublic.gParametrosInstalacion.gIdioma Then
                        sIdioma = oDenominacion.Den
                    End If
                Next
             End If
             
             irespuesta = oMensajes.PreguntaEliminar(sdbgTiposPedido.Columns("COD").Value & " (" & sdbgTiposPedido.Columns(sIdioma).Value & ")")
        Case Is > 1
             irespuesta = oMensajes.PreguntaEliminarMultiplesTiposPedido
    End Select
        
    
    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
                                
    ReDim aIdentificadores(sdbgTiposPedido.SelBookmarks.Count, 1)
    i = 0
    While i < sdbgTiposPedido.SelBookmarks.Count
        aIdentificadores(i + 1, 1) = g_oTiposPedido.Item(CStr(sdbgTiposPedido.SelBookmarks(i))).Cod
        i = i + 1
    Wend
    
    Screen.MousePointer = vbHourglass
    
    teserror = g_oTiposPedido.EliminarMultiplesTiposPedidoDeBaseDatos(aIdentificadores)
    
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
    
        If teserror.NumError = TESImposibleEliminar Then
        
            vItems = teserror.Arg1
    
            oMensajes.ImposibleEliminacionMultipleTiposPedido vItems
                
            sdbgTiposPedido.SelBookmarks.RemoveAll
            
            CargarGridTiposPedido
            sdbgTiposPedido.ReBind
        Else
        
            TratarError teserror
            Screen.MousePointer = vbNormal
            
            Exit Sub
        End If

    Else
        '3328
        basSeguridad.RegistrarAccion accionessummit.ACCTipPedEli, "Cod:" & sdbgTiposPedido.Columns("COD").Text
        sdbgTiposPedido.SelBookmarks.RemoveAll
        CargarGridTiposPedido
        sdbgTiposPedido.ReBind
   
    End If
           

End Sub





Private Sub chkTodosIdiomas_Click()
    Dim oDenominacion As CMultiidioma

    If chkTodosIdiomas.Value = 1 Then
        If Not m_oIdiomas Is Nothing Then
            For Each oDenominacion In m_oIdiomas
                If m_bModoEdicion And m_bModifTipoPedi Then
                    sdbgTiposPedido.Columns(oDenominacion.Den).Locked = False
                Else
                    sdbgTiposPedido.Columns(oDenominacion.Den).Locked = True
                End If
                sdbgTiposPedido.Columns(oDenominacion.Den).Visible = True
                sdbgTiposPedido.Columns(oDenominacion.Den).Width = sdbgTiposPedido.Width * (0.55 / m_iNumIdiomas)
            Next
        End If
    Else
        If Not m_oIdiomas Is Nothing Then
            For Each oDenominacion In m_oIdiomas
                If oDenominacion.Cod = basPublic.gParametrosInstalacion.gIdioma Then
                    If m_bModoEdicion And m_bModifTipoPedi Then
                        sdbgTiposPedido.Columns(oDenominacion.Den).Locked = False
                    Else
                        sdbgTiposPedido.Columns(oDenominacion.Den).Locked = True
                    End If
                    sdbgTiposPedido.Columns(oDenominacion.Den).Visible = True
                    sdbgTiposPedido.Columns(oDenominacion.Den).Width = sdbgTiposPedido.Width * 0.55
                Else
                    sdbgTiposPedido.Columns(oDenominacion.Den).Locked = True
                    sdbgTiposPedido.Columns(oDenominacion.Den).Visible = False
                End If
            Next
        End If
    End If
    
End Sub

Private Sub Form_Load()
    Dim sCaption As String
       
    ReDim m_sTextConcepAlmacRecep(2, 2)
        
    m_iNumIdiomas = 0
    m_bColumnasAnyadidas = False
    Set m_oIdiomas = Nothing
        
    Me.Height = 6000
    Me.Width = 10350
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
        
    CargarRecursos
    
    PonerFieldSeparator Me
    
    sCaption = Me.caption
    Me.caption = m_sIdiTitulos(3)
    
    ConfigurarSeguridad
    
    On Error Resume Next

    If gParametrosGenerales.gbPedidosAprov = False Then
        sdbgTiposPedido.Columns("CATEG").Visible = False
        sdbgTiposPedido.Columns("ATRIBUTOS").Visible = False
    Else
        sdbgTiposPedido.Columns("CATEG").Visible = True
        sdbgTiposPedido.Columns("ATRIBUTOS").Visible = True
    End If
    
    On Error GoTo 0
    
    Arrange
        
    Me.Show
    DoEvents
    
    AnyadirColumnasIdiomasDeno
    
    cmdModoEdicion.Enabled = False

    Screen.MousePointer = vbHourglass

    
    AccionTipoPedido = ACCTipPedCon '3328
    Me.caption = sCaption
    
    sdbgTiposPedido.Columns("CONCEP").DropDownHwnd = ssdbddConcepto.hWnd
    sdbgTiposPedido.Columns("ALMAC").DropDownHwnd = ssdbddAlmacen.hWnd
    sdbgTiposPedido.Columns("RECEP").DropDownHwnd = ssdbddRecepcion.hWnd
    CargarGridConConcepto
    CargarGridConAlmacen
    CargarGridConRecepcion
    

    Screen.MousePointer = vbNormal

    cmdModoEdicion.Enabled = True

    caption = m_sIdiTitulos(1)

    CargarGridTiposPedido
                    
    sdbgTiposPedido.Visible = True
    sdbgTiposPedido.ReBind
    
    If sdbgTiposPedido.Rows = 0 Then
        sdbgTiposPedido.Columns("COD").Locked = False
    Else
        sdbgTiposPedido.Columns("COD").Locked = True
    End If
    
    If m_bModoEdicion Then
        If m_bModifTipoPedi Then
            sdbgTiposPedido.Columns("COD").Locked = False
            sdbgTiposPedido.Columns("CONCEP").Locked = False
            sdbgTiposPedido.Columns("ALMAC").Locked = False
            sdbgTiposPedido.Columns("RECEP").Locked = False
            sdbgTiposPedido.Columns("BAJA").Locked = False
            sdbgTiposPedido.Columns("CATEG").Locked = True
        End If
    Else
        sdbgTiposPedido.Columns("COD").Locked = True
        sdbgTiposPedido.Columns("CONCEP").Locked = True
        sdbgTiposPedido.Columns("ALMAC").Locked = True
        sdbgTiposPedido.Columns("RECEP").Locked = True
        sdbgTiposPedido.Columns("BAJA").Locked = True
        sdbgTiposPedido.Columns("CATEG").Locked = True
    End If
    
    If sdbgTiposPedido.Rows <> 0 Then
        sdbgTiposPedido.Bookmark = 0
    End If

End Sub

''' <summary>Configuracion de la pantalla en funcion de las acciones de seguridad</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    m_bRAtributo = True
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.TIPPEDModificar)) Is Nothing) Then
        m_bModifTipoPedi = True
    End If
   
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.TIPPEDAsignar)) Is Nothing) Then
        m_bRAtributo = False
    End If

    If m_bModifTipoPedi Then
        cmdModoEdicion.Visible = True
    Else
        cmdModoEdicion.Visible = False
    End If
    
End Sub

''' <summary>
''' Unload del formulario
''' </summary>
''' <param name="Cancel">Indica si detiene la descarga del formulario</param>
''' <returns></returns>
''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
Private Sub Form_Unload(Cancel As Integer)

''' * Objetivo: Descargar el formulario si no
''' * Objetivo: hay cambios pendientes
    
    Dim v As Variant
    
    If sdbgTiposPedido.DataChanged = True Then
        
        v = sdbgTiposPedido.ActiveCell.Value
        If Me.Visible Then sdbgTiposPedido.SetFocus
        sdbgTiposPedido.ActiveCell.Value = v
        
        sdbgTiposPedido.Update
            
        m_bValError = False
        m_bAnyaError = False
        m_bModError = False
        
        If m_bValError Or m_bAnyaError Or m_bModError Then
            Cancel = True
        End If
        
    End If
    
    Unload frmESTRMATAtrib

    Erase m_sTextConcepAlmacRecep
    
    Set g_oTiposPedido = Nothing
    Set m_oTipoPedidoEnEdicion = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
    Set m_aBookmarks = Nothing
    
    Accion = ACCTipPedCon '3328
    m_bModoEdicion = False
    Me.Visible = False
    
End Sub

''' <summary>
''' Segun el caso que sea. Si la celda de la grid dispone de boton envia el foco a otro formulario
''' </summary>
''' <remarks>Tiempo m�ximo:0,3seg.</remarks>
Private Sub sdbgTiposPedido_BtnClick()
Dim vBookmark As Variant
Dim iRow As Long

 If sdbgTiposPedido.Col = -1 Then Exit Sub
    
    Select Case sdbgTiposPedido.Columns(sdbgTiposPedido.Col).Name
    '3328
         Case "ATRIBUTOS" 'Atributos
                iRow = sdbgTiposPedido.Row
                If m_bModoEdicion Then
                    
                    m_bModError = False
                    m_bAnyaError = False
                    
                    ' Primero acabamos la edicion del articulo
                    sdbgTiposPedido.Update
                    ' Si se ha producido un error no continuamos
                    If m_bValError Or m_bModError Or m_bAnyaError Then
                        Exit Sub
                    End If
                End If
                
                vBookmark = sdbgTiposPedido.RowBookmark(iRow)

                If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                    vBookmark = 0
                End If

                sdbgTiposPedido.Bookmark = vBookmark

                '3328
                frmESTRMATAtrib.g_sOrigen = "TIPPED"
                frmESTRMATAtrib.g_iNumIdiomas = m_iNumIdiomas
                If m_bRAtributo And m_bModoEdicion Then
                    frmESTRMATAtrib.g_bModoEdicion = True
                Else
                    frmESTRMATAtrib.g_bModoEdicion = False
                End If
                g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).DevolverTodosLosAtributosDelTipoPedido
                Set frmESTRMATAtrib.g_oAtributos = g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).ATRIBUTOS
                frmESTRMATAtrib.g_vIdPed = g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).Id 'sdbgTiposPedido.Columns("COD").Value
                frmESTRMATAtrib.g_sCodPed = g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).Cod
                frmESTRMATAtrib.g_sDenPed = g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).Den
                frmESTRMATAtrib.Show
                
         Case "CATEG" 'Categorias
                iRow = sdbgTiposPedido.Row
                If m_bModoEdicion Then
                    
                    m_bModError = False
                    m_bAnyaError = False
                    
                    ' Primero acabamos la edicion del articulo
                    sdbgTiposPedido.Update
                    ' Si se ha producido un error no continuamos
                    If m_bValError Or m_bModError Or m_bAnyaError Then
                        Exit Sub
                    End If
                End If
                
                vBookmark = sdbgTiposPedido.RowBookmark(iRow)

                If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                    vBookmark = 0
                End If

                sdbgTiposPedido.Bookmark = vBookmark

                '3328
                frmMantPedidosCat.g_lTipopedidoId = g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).Id
                If m_bRAtributo And m_bModoEdicion Then
                    frmMantPedidosCat.g_bModoEdicion = True
                Else
                    frmMantPedidosCat.g_bModoEdicion = False
                End If
                DoEvents
                frmMantPedidosCat.Show 1

    End Select

End Sub

''' <summary>
''' Configura la linea de la grid con el estilo
''' </summary>
''' <param name="Bookmark">del evento</param>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgTiposPedido_RowLoaded(ByVal Bookmark As Variant)
    
Dim oTipoPedido As CTipoPedido
    
    Set oTipoPedido = Nothing
    If Not IsNull(Bookmark) And Not IsEmpty(Bookmark) Then
        Set oTipoPedido = g_oTiposPedido.Item(CStr(Bookmark))
        
        sdbgTiposPedido.Columns("ATRIBUTOS").CellStyleSet ""

        If oTipoPedido.ConAtributos = True Then
            sdbgTiposPedido.Columns("ATRIBUTOS").CellStyleSet "Atributos"
        End If
        
    End If

End Sub

Private Sub ssdbddConcepto_CloseUp()
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim oArt As CArticulo
    Dim bk As Variant
    
    Screen.MousePointer = vbHourglass
    If Not m_aBookmarks Is Nothing Then
        If m_aBookmarks.Count > 0 Then
            m_bRespetar = True
            teserror = g_oTiposPedido.ActualizarConcepAlmacRecep(Concepto, TraducirTextoCelda(0, ssdbddConcepto.Columns("DEN").Value), m_aBookmarks)
            If teserror.Arg2 Then
                Screen.MousePointer = vbNormal
                '3328
                oMensajes.ImposibleModificarMultiplesTiposPedido teserror.Arg1
                Screen.MousePointer = vbHourglass
                
                Set m_oTiposPedidoAModificar = Nothing
    
                Screen.MousePointer = vbNormal
                'Vuelvo a seleccionar las filas modificadas
    
                If m_aBookmarks.Count > 0 Then
                    For Each bk In m_aBookmarks
                        sdbgTiposPedido.SelBookmarks.Add bk
                    Next
                End If
            Else
                For Each oArt In m_oTiposPedidoAModificar
                    RegistrarAccion accionessummit.ACCTipPedMod, "Art�culo: " & oArt.Cod & " Concepto: " & oArt.Concepto
                Next
            End If
            sdbgTiposPedido.Update
            sdbgTiposPedido.ReBind
            If m_aBookmarks.Count > 0 Then
                For Each bk In m_aBookmarks
                    sdbgTiposPedido.SelBookmarks.Add bk
                Next
            End If
            Set m_aBookmarks = Nothing
            m_bRespetar = False
        End If
    End If
    Screen.MousePointer = vbNormal
    
End Sub




Private Sub AnyadirColumnasIdiomasDeno()
    Dim oDenominacion As CMultiidioma
    Dim bIdiUsuAnyadido As Boolean
    Dim iColumna As Integer
    Dim oIdioma As CIdioma
    Dim oIdiomas As CIdiomas
    
    bIdiUsuAnyadido = False
    iColumna = 2
    If m_bColumnasAnyadidas = False Then
        
        '3328 Carga de Idiomas
        Set oIdiomas = oGestorParametros.DevolverIdiomas(False, True, False)
        m_iNumIdiomas = 0
        Set m_oIdiomas = Nothing
        Set m_oIdiomas = oFSGSRaiz.Generar_CMultiidiomas
        
        For Each oIdioma In oIdiomas
            m_oIdiomas.Add oIdioma.Cod, oIdioma.Den
            m_iNumIdiomas = m_iNumIdiomas + 1
        Next
        
        For Each oDenominacion In m_oIdiomas
            If bIdiUsuAnyadido Then
                iColumna = 3
            Else
                iColumna = 2
            End If
            sdbgTiposPedido.Columns.Add iColumna
            sdbgTiposPedido.Columns(iColumna).Name = CStr(oDenominacion.Cod)
            sdbgTiposPedido.Columns(iColumna).CaptionAlignment = ssColCapAlignCenter
            sdbgTiposPedido.Columns(iColumna).Alignment = ssCaptionAlignmentLeft
            sdbgTiposPedido.Columns(iColumna).Style = ssStyleEdit
            sdbgTiposPedido.Columns(iColumna).FieldLen = 200
            sdbgTiposPedido.Columns(iColumna).caption = CStr(oDenominacion.Den)
            sdbgTiposPedido.Columns(iColumna).Visible = False
            sdbgTiposPedido.Columns(iColumna).Locked = True
            
            If oDenominacion.Cod = basPublic.gParametrosInstalacion.gIdioma Then
                sdbgTiposPedido.Columns(iColumna).Width = sdbgTiposPedido.Width * 0.55
                sdbgTiposPedido.Columns(iColumna).Visible = True
                bIdiUsuAnyadido = True
            End If
        Next
        m_bColumnasAnyadidas = True
    End If
End Sub



'3328 CARGA GRID TIPOS DE PEDIDO
Private Sub CargarGridTiposPedido()

    Dim oTipoPedido As CTipoPedido

    Set g_oTiposPedido = Nothing
    
    Screen.MousePointer = vbHourglass
    
    Set g_oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    
    'Cargo los Tipos de Pedido que existen en BBDD en memoria
    g_oTiposPedido.CargarTodosLosTiposPedidosYDatos basPublic.gParametrosInstalacion.gIdioma
    
    'Si hay registros, cargo por cada Tipo de Pedido sus denominaciones y sus atributos
    If g_oTiposPedido.Count > 0 Then
        For Each oTipoPedido In g_oTiposPedido
            oTipoPedido.CargarDenominaciones
            oTipoPedido.DevolverTieneCategoriasElTipoPedido
            oTipoPedido.DevolverTieneAtributosElTipoPedido
        Next
    End If
    
    Set oTipoPedido = Nothing
    Set oTipoPedido = oFSGSRaiz.Generar_CTipoPedido
    
    Screen.MousePointer = vbNormal
   
    Accion = ACCTipPedCon '3328
    DoEvents
    
End Sub






Private Sub cmdDeshacerTiposPedido_Click()

    ''' * Objetivo: Deshacer la edicion en el Articulo actual
    
    sdbgTiposPedido.CancelUpdate
    sdbgTiposPedido.DataChanged = False
    DoEvents
        
    If Not m_oTipoPedidoEnEdicion Is Nothing Then
        Set m_oIBAseDatosEnEdicion = m_oTipoPedidoEnEdicion
        
        m_oIBAseDatosEnEdicion.CancelarEdicion
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oTipoPedidoEnEdicion = Nothing
    End If
    
    If Not sdbgTiposPedido.IsAddRow Then
        cmdEliminarTiposPedido.Enabled = True
        cmdDeshacerTiposPedido.Enabled = False
    Else
        cmdDeshacerTiposPedido.Enabled = False
    End If
    
    '3328
    Accion = ACCTipPedCon
    
End Sub



Private Sub cmdModoEdicion_Click()
    
    Dim oDenominacion As CMultiidioma
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not m_bModoEdicion Then
                
        'cmdRestaurar_Click
        
        If m_bModifTipoPedi Then
            sdbgTiposPedido.AllowAddNew = True
            sdbgTiposPedido.AllowDelete = True
            sdbgTiposPedido.Columns("COD").Locked = False
            
            If Not m_oIdiomas Is Nothing Then
                For Each oDenominacion In m_oIdiomas
                    sdbgTiposPedido.Columns(oDenominacion.Den).Locked = False
                    sdbgTiposPedido.Columns(oDenominacion.Den).Visible = True
                    sdbgTiposPedido.Columns(oDenominacion.Den).Width = sdbgTiposPedido.Width * (0.55 / m_iNumIdiomas)
                Next
            End If
            
            sdbgTiposPedido.Columns("CONCEP").Locked = False
            sdbgTiposPedido.Columns("ALMAC").Locked = False
            sdbgTiposPedido.Columns("RECEP").Locked = False
            sdbgTiposPedido.Columns("BAJA").Locked = False
            sdbgTiposPedido.Columns("CATEG").Locked = True
        End If
        
'        Caption = "Art�culos (Edici�n)"
        caption = m_sIdiTitulos(2)
        
'        cmdModoEdicion.Caption = "&Consulta"
        cmdModoEdicion.caption = m_sIdiModos(2)
        
        cmdRestaurarTiposPedido.Visible = False
        cmdEliminarTiposPedido.Visible = True
        cmdDeshacerTiposPedido.Visible = True

        m_bModoEdicion = True
        chkTodosIdiomas.Value = 1
        
        AccionTipoPedido = ACCTipPedCon
        
    Else
                
        If sdbgTiposPedido.DataChanged Or m_bValError Or m_bAnyaError Or m_bModError Then
        
            v = sdbgTiposPedido.ActiveCell.Value
            If Me.Visible Then sdbgTiposPedido.SetFocus
            If (v <> "") Then
                sdbgTiposPedido.ActiveCell.Value = v
            End If
            
            m_bValError = False
            m_bAnyaError = False
            m_bModError = False
            
            sdbgTiposPedido.Update
            
            If m_bValError Or m_bAnyaError Or m_bModError Then
                Exit Sub
            End If
            
        End If
        
        sdbgTiposPedido.AllowAddNew = False
        sdbgTiposPedido.AllowDelete = False
        
        If Not m_oIdiomas Is Nothing Then
            For Each oDenominacion In m_oIdiomas
                sdbgTiposPedido.Columns(oDenominacion.Den).Locked = True
            Next
        End If
        
        sdbgTiposPedido.Columns("COD").Locked = True
        sdbgTiposPedido.Columns("CONCEP").Locked = True
        sdbgTiposPedido.Columns("ALMAC").Locked = True
        sdbgTiposPedido.Columns("RECEP").Locked = True
        sdbgTiposPedido.Columns("BAJA").Locked = True
        sdbgTiposPedido.Columns("CATEG").Locked = True
                
        cmdEliminarTiposPedido.Visible = False
        cmdDeshacerTiposPedido.Visible = False
        cmdRestaurarTiposPedido.Visible = True

'        Caption = "Art�culos (Consulta)"
         caption = m_sIdiTitulos(1)
        
'        cmdModoEdicion.Caption = "&Edici�n"
        cmdModoEdicion.caption = m_sIdiModos(1)
        
        cmdRestaurarTiposPedido_Click
        
        m_bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgTiposPedido.SetFocus
End Sub

''' <summary>Coloca los controles en el formulario en funci�n del tama�o de este</summary>
''' <remarks>Llamada desde: Form_Resize; Tiempo m�ximo: 0</remarks>
Private Sub Arrange()
Dim i As Integer
Dim oDenominacion As CMultiidioma

On Error Resume Next

    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
           
    If Height >= 1475 Then sdbgTiposPedido.Height = Height - 1475
    If Width >= 250 Then sdbgTiposPedido.Width = Width - 250
    
    Picture2.Top = sdbgTiposPedido.Top + sdbgTiposPedido.Height + 100
    Picture2.Width = sdbgTiposPedido.Width
    
    cmdModoEdicion.Left = Picture2.Left + Picture2.Width - cmdModoEdicion.Width - 125

    sdbgTiposPedido.ScrollBars = ssScrollBarsBoth
    sdbgTiposPedido.Columns("COD").Width = sdbgTiposPedido.Width * 0.05
    If gParametrosGenerales.gbPedidosAprov = True Then
        sdbgTiposPedido.Columns("CATEG").Width = sdbgTiposPedido.Width * 0.1
        sdbgTiposPedido.Columns("BAJA").Width = sdbgTiposPedido.Width * 0.03
        sdbgTiposPedido.Columns("ATRIBUTOS").Width = sdbgTiposPedido.Width * 0.04
        sdbgTiposPedido.Columns("CONCEP").Width = sdbgTiposPedido.Width * 0.06
        sdbgTiposPedido.Columns("ALMAC").Width = sdbgTiposPedido.Width * 0.06
        sdbgTiposPedido.Columns("RECEP").Width = sdbgTiposPedido.Width * 0.06
    Else
        sdbgTiposPedido.Columns("BAJA").Width = sdbgTiposPedido.Width * 0.08
        sdbgTiposPedido.Columns("CONCEP").Width = sdbgTiposPedido.Width * 0.09
        sdbgTiposPedido.Columns("ALMAC").Width = sdbgTiposPedido.Width * 0.09
        sdbgTiposPedido.Columns("RECEP").Width = sdbgTiposPedido.Width * 0.09
    End If
    
    '3328
    If chkTodosIdiomas.Value = 1 Then
        If Not m_oIdiomas Is Nothing Then
            For Each oDenominacion In m_oIdiomas
                sdbgTiposPedido.Columns(oDenominacion.Den).Width = sdbgTiposPedido.Width * (0.55 / m_iNumIdiomas)
            Next
        End If
    Else
        If Not m_oIdiomas Is Nothing Then
            For Each oDenominacion In m_oIdiomas
                If oDenominacion.Cod = basPublic.gParametrosInstalacion.gIdioma Then
                    sdbgTiposPedido.Columns(oDenominacion.Den).Width = sdbgTiposPedido.Width * 0.55
                End If
            Next
        End If
    End If

End Sub

Private Sub cmdRestaurarTiposPedido_Click()

    ''' * Objetivo: Restaurar el contenido de las grid
    ''' * Objetivo: desde la base de datos
    
    Me.caption = m_sIdiTitulos(1)
    
    CargarGridTiposPedido
    
    sdbgTiposPedido.ReBind
    If Me.Visible Then sdbgTiposPedido.SetFocus
End Sub

Private Sub Form_Resize()

    ''' * Objetivo: Adecuar los controles
    
    Arrange
    
End Sub



Private Sub sdbgTiposPedido_AfterDelete(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    RtnDispErrMsg = 0
    If sdbgTiposPedido.Rows > 0 Then
        If IsEmpty(sdbgTiposPedido.RowBookmark(sdbgTiposPedido.Row)) Then
            sdbgTiposPedido.Bookmark = sdbgTiposPedido.RowBookmark(sdbgTiposPedido.Row - 1)
        Else
            sdbgTiposPedido.Bookmark = sdbgTiposPedido.RowBookmark(sdbgTiposPedido.Row)
        End If
    End If
    If Me.Visible Then sdbgTiposPedido.SetFocus
End Sub
Private Sub sdbgTiposPedido_AfterInsert(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    If m_bAnyaError = False Then
        cmdEliminarTiposPedido.Enabled = True
        cmdDeshacerTiposPedido.Enabled = False
    End If
    
    'Si el registro es el primero, se le pone como seleccionado
    If sdbgTiposPedido.Rows = 1 Then sdbgTiposPedido.Bookmark = 0
    
End Sub
Private Sub sdbgTiposPedido_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False Then
        cmdEliminarTiposPedido.Enabled = True
        cmdDeshacerTiposPedido.Enabled = False
    End If
    
End Sub



Private Sub sdbgTiposPedido_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

    ''' * Objetivo: Confirmacion antes de eliminar

    DispPromptMsg = 0
    
End Sub


Private Sub sdbgTiposPedido_BeforeUpdate(Cancel As Integer)

    '3328
    ''' * Objetivo: Validar los datos

    Dim oDenominacion As CMultiidioma
    
    If m_bRespetar Then Exit Sub
    
    m_bValError = False
    Cancel = False
    
    If Trim(sdbgTiposPedido.Columns("COD").Value) = "" Then
        oMensajes.NoValido m_sIdiCod
        Cancel = True
        GoTo Salir
    End If
    
    '3328 Si la longitud del codigo es > 5, ERROR
    If Len(Trim(sdbgTiposPedido.Columns("COD").Value)) > basParametros.gLongitudesDeCodigos.giLongCodTIPPED Then
        oMensajes.NoValido m_sIdiCod
        Cancel = True
        GoTo Salir
    End If
    
    If Not m_oIdiomas Is Nothing Then
        For Each oDenominacion In m_oIdiomas
            If Trim(sdbgTiposPedido.Columns(oDenominacion.Den).Value) = "" Then
                oMensajes.NoValida m_sIdiDen
                Cancel = True
                GoTo Salir
            End If
        Next
    End If
    
    If Trim(sdbgTiposPedido.Columns("CONCEP").Value) = "" Then
        oMensajes.NoValida m_sIdiConcepto
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgTiposPedido.Columns("ALMAC").Value) = "" Then
        oMensajes.NoValida m_sIdiAlmacenable
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgTiposPedido.Columns("RECEP").Value) = "" Then
        oMensajes.NoValida m_sIdiRecepcionable
        Cancel = True
        GoTo Salir
    End If
    
    If sdbgTiposPedido.Row = -1 Then
        Cancel = True
        GoTo Salir
    End If
    
Salir:
        
    m_bValError = Cancel
    If Me.Visible Then sdbgTiposPedido.SetFocus
    
End Sub

''' <summary>
''' Evento que se produce al haber cambios en el grid
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento que se produce al haber cambios en el grid; Tiempo m�ximo</remarks>
Private Sub sdbgTiposPedido_Change()
    Dim oDenominacion As CMultiidioma

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If cmdDeshacerTiposPedido.Enabled = False Then

        cmdEliminarTiposPedido.Enabled = False
        cmdDeshacerTiposPedido.Enabled = True
    
    End If
    
    If Not m_oIdiomas Is Nothing Then
        For Each oDenominacion In m_oIdiomas
            If Len(sdbgTiposPedido.Columns(oDenominacion.Den).Value) + 1 > basParametros.gLongitudesDeCodigos.giLongCodDENTIPPED Then
                sdbgTiposPedido.Columns(oDenominacion.Den).Value = Left(sdbgTiposPedido.Columns(oDenominacion.Den).Value, basParametros.gLongitudesDeCodigos.giLongCodDENTIPPED)
            End If
        Next
    End If
        
    '3328
    If Accion = ACCTipPedCon And Not sdbgTiposPedido.IsAddRow Then
    
        Set m_oTipoPedidoEnEdicion = Nothing
        Set m_oTipoPedidoEnEdicion = g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark))
    
        Set m_oIBAseDatosEnEdicion = m_oTipoPedidoEnEdicion
        
        Screen.MousePointer = vbHourglass
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            sdbgTiposPedido.DataChanged = False
            sdbgTiposPedido.Columns("COD").Value = m_oTipoPedidoEnEdicion.Cod
            sdbgTiposPedido.Columns("DEN").Value = m_oTipoPedidoEnEdicion.Den
            sdbgTiposPedido.Columns("CONCEP").Value = m_oTipoPedidoEnEdicion.CodConcep
            sdbgTiposPedido.Columns("ALMAC").Value = m_oTipoPedidoEnEdicion.CodAlmac
            sdbgTiposPedido.Columns("RECEP").Value = m_oTipoPedidoEnEdicion.CodRecep
            teserror.NumError = TESnoerror
        End If
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            If Me.Visible Then sdbgTiposPedido.SetFocus
        Else
            Accion = ACCTipPedMod
        End If
                    
    End If
    
    If sdbgTiposPedido.Columns("CONCEP").Text = "" And sdbgTiposPedido.IsAddRow Then
        sdbgTiposPedido.Columns("CONCEP").Text = m_sTextConcepAlmacRecep(0, 2)
        cmdDeshacerTiposPedido.Enabled = True
    End If
    
    If sdbgTiposPedido.Columns("ALMAC").Text = "" And sdbgTiposPedido.IsAddRow Then
        sdbgTiposPedido.Columns("ALMAC").Text = m_sTextConcepAlmacRecep(1, 2)
        cmdDeshacerTiposPedido.Enabled = True
    End If
    
    If sdbgTiposPedido.Columns("RECEP").Text = "" And sdbgTiposPedido.IsAddRow Then
        sdbgTiposPedido.Columns("RECEP").Text = m_sTextConcepAlmacRecep(2, 2)
        cmdDeshacerTiposPedido.Enabled = True
    End If

    Set m_oTipoPedidoEnEdicion = Nothing
    
    If IsNull(sdbgTiposPedido.Bookmark) Or sdbgTiposPedido.IsAddRow Then
    Else
        Set m_oTipoPedidoEnEdicion = g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark))
    End If
      
    Screen.MousePointer = vbNormal

End Sub



Private Sub sdbgTiposPedido_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    
    Dim sHeadCaption As String
    Dim sNombreColumna As String
    Dim oTipoPedido As CTipoPedido
    
    If m_bModoEdicion Then Exit Sub
    
   
    sNombreColumna = sdbgTiposPedido.Columns(ColIndex).Name
    
    If sNombreColumna = "CATEG" Or sNombreColumna = "ATRIBUTOS" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgTiposPedido.Columns(ColIndex).caption
'    sdbgTiposPedido.Columns(ColIndex).Caption = "Ordenando...."
    sdbgTiposPedido.Columns(ColIndex).caption = m_sIdiOrden
    
    Set g_oTiposPedido = Nothing
    Set g_oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    
    '3328 Cargar los tipos de pedidos ordenados por la columna seleccionada
    g_oTiposPedido.CargarTodosLosTiposPedidosYDatos basParametros.gParametrosGenerales.gIdioma, sNombreColumna
    
    If g_oTiposPedido.Count > 0 Then
        For Each oTipoPedido In g_oTiposPedido
            oTipoPedido.CargarDenominaciones
            oTipoPedido.DevolverTieneCategoriasElTipoPedido
            oTipoPedido.DevolverTieneAtributosElTipoPedido
        Next
    End If
    
    Screen.MousePointer = vbNormal
    
    sdbgTiposPedido.Columns(ColIndex).caption = sHeadCaption
    sdbgTiposPedido.ReBind
    
    DoEvents
    
End Sub

Private Sub sdbgTiposPedido_InitColumnProps()

    sdbgTiposPedido.Columns("COD").FieldLen = basParametros.gLongitudesDeCodigos.giLongCodTIPPED
    sdbgTiposPedido.Columns("DEN").FieldLen = basParametros.gLongitudesDeCodigos.giLongCodDENTIPPED
    
End Sub

Private Sub sdbgTiposPedido_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
    If KeyAscii = vbKeyReturn Or KeyAscii = vbKeySpace Then
        sdbgTiposPedido_BtnClick
    End If
    
    
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgTiposPedido.DataChanged = False Then
            
            sdbgTiposPedido.CancelUpdate
            sdbgTiposPedido.DataChanged = False
            
            If Not m_oTipoPedidoEnEdicion Is Nothing Then
                m_oIBAseDatosEnEdicion.CancelarEdicion
                Set m_oIBAseDatosEnEdicion = Nothing
                Set m_oTipoPedidoEnEdicion = Nothing
            End If
           
            cmdEliminarTiposPedido.Enabled = True
            cmdDeshacerTiposPedido.Enabled = False

            Accion = ACCTipPedCon
            
        Else
        
            If sdbgTiposPedido.IsAddRow Then

                cmdEliminarTiposPedido.Enabled = True
                cmdDeshacerTiposPedido.Enabled = False
           
                Accion = ACCTipPedCon
            
            End If
            
        End If
        
    End If
    
End Sub

''' <summary>
''' Evento que se produce al cambiar de columna o de fila de la grid
''' </summary>
''' <returns></returns>
''' <param name="LastRow">Linea</param>
''' <param name="LastCol">Columna</param>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
Private Sub sdbgTiposPedido_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    Dim teserror As TipoErrorSummit
    Dim bk As Variant

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If sdbgTiposPedido.Col = -1 Then
        Set m_aBookmarks = New Collection
        Set m_oTiposPedidoAModificar = oFSGSRaiz.Generar_CTiposPedido
        For Each bk In sdbgTiposPedido.SelBookmarks
            m_aBookmarks.Add bk
        Next
    Else
        If sdbgTiposPedido.Columns(sdbgTiposPedido.Col).Name = "CONCEP" Or sdbgTiposPedido.Columns(sdbgTiposPedido.Col).Name = "ALMAC" _
            Or sdbgTiposPedido.Columns(sdbgTiposPedido.Col).Name = "RECEP" Then
                If sdbgTiposPedido.SelBookmarks.Count = 0 And Not m_aBookmarks Is Nothing Then
                    For Each bk In m_aBookmarks
                        sdbgTiposPedido.SelBookmarks.Add bk
                    Next
                End If
        Else
            Set m_aBookmarks = Nothing
        End If
    End If

End Sub

Private Sub sdbgTiposPedido_UnboundAddData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, NewRowBookmark As Variant)
    
    ''' * Objetivo: Hemos anyadido una fila al grid,
    ''' * Objetivo: ahora hay que a�adirla a la
    ''' * Objetivo: coleccion
    ''' * Recibe: Buffer con los datos a a�adir y bookmark de la fila
        
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CMultiidioma
    Dim oAtributos As CAtributos
    
    Set oDenominaciones = Nothing
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    Set oAtributos = oFSGSRaiz.Generar_CAtributos
    
    m_bAnyaError = False
    
    ''' A�adir a la colecci�n
    g_oTiposPedido.Add 0, sdbgTiposPedido.Columns("COD").Value, sdbgTiposPedido.Columns("COD").Value, 0, 0, 0, 0, g_oTiposPedido.Count, oDenominaciones, oAtributos, False
    ''' A�adir a la base de datos
    
    Set m_oTipoPedidoEnEdicion = g_oTiposPedido.Item(CStr(g_oTiposPedido.Count - 1))

    
    m_oTipoPedidoEnEdicion.Cod = EliminarEspacioYTab("" & sdbgTiposPedido.Columns("COD").Value)
    m_oTipoPedidoEnEdicion.Den = sdbgTiposPedido.Columns("DEN").Value
    m_oTipoPedidoEnEdicion.CodConcep = TraducirTextoCelda(0, sdbgTiposPedido.Columns("CONCEP").Value)  '''Error de tipos
    m_oTipoPedidoEnEdicion.CodAlmac = TraducirTextoCelda(1, sdbgTiposPedido.Columns("ALMAC").Value)
    m_oTipoPedidoEnEdicion.CodRecep = TraducirTextoCelda(2, sdbgTiposPedido.Columns("RECEP").Value)
    m_oTipoPedidoEnEdicion.Baja = GridCheckToBoolean(sdbgTiposPedido.Columns("BAJA").Value)
    
    '3328 Guardar las denominaciones en los distintos idiomas
    If Not m_oIdiomas Is Nothing Then
        For Each oIdioma In m_oIdiomas
            m_oTipoPedidoEnEdicion.Denominaciones.Add oIdioma.Cod, oIdioma.Den
            m_oTipoPedidoEnEdicion.Denominaciones.Item(oIdioma.Cod).Den = sdbgTiposPedido.Columns(oIdioma.Den).Value
        Next
    End If
        
    Set m_oIBAseDatosEnEdicion = m_oTipoPedidoEnEdicion
    
    Screen.MousePointer = vbHourglass
    teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        
        v = sdbgTiposPedido.ActiveCell.Value
        g_oTiposPedido.Remove (CStr(g_oTiposPedido.Count - 1))
        TratarError teserror
        If Me.Visible Then sdbgTiposPedido.SetFocus
        m_bAnyaError = True
        RowBuf.RowCount = 0
        sdbgTiposPedido.ActiveCell.Value = v
        
    Else
        ''' Registro de acciones
        
        basSeguridad.RegistrarAccion accionessummit.ACCTipPedAnya, "Cod:" & m_oTipoPedidoEnEdicion.Cod
        
        Accion = ACCTipPedCon
    
    End If
    
    Set m_oIBAseDatosEnEdicion = Nothing
    Set m_oTipoPedidoEnEdicion = Nothing
        

End Sub



Private Sub sdbgTiposPedido_UnboundDeleteRow(Bookmark As Variant)

    ''' * Objetivo: Hemos eliminado una fila al grid,
    ''' * Objetivo: ahora hay que eliminarla de la
    ''' * Objetivo: coleccion
    ''' * Recibe: Bookmark de la fila
    
End Sub



Private Sub sdbgTiposPedido_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Long
    Dim i As Long
    Dim j As Long
    
    Dim k As Long
    
    Dim oTipoPedido As CTipoPedido
    
    Dim iNumTiposPedido As Long
    
    Dim oIdioma As CMultiidioma
    Dim oDenominacion As CMultiidioma

    If g_oTiposPedido Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If

    iNumTiposPedido = g_oTiposPedido.Count

    If IsNull(StartLocation) Then
    
        If ReadPriorRows Then
            m_lIndiceTiposPedido = iNumTiposPedido - 1
        Else
            m_lIndiceTiposPedido = 0 '3328
        End If
        
    Else
        
        m_lIndiceTiposPedido = StartLocation
    
        If ReadPriorRows Then
            m_lIndiceTiposPedido = m_lIndiceTiposPedido - 1
        Else
            m_lIndiceTiposPedido = m_lIndiceTiposPedido + 1
        End If
        
    End If
    
    
    For i = 0 To RowBuf.RowCount - 1
        
        If m_lIndiceTiposPedido < 0 Or m_lIndiceTiposPedido > iNumTiposPedido - 1 Then Exit For
    
        Set oTipoPedido = Nothing
        Set oTipoPedido = g_oTiposPedido.Item(CStr(m_lIndiceTiposPedido))
    
        For j = 0 To 7 + m_iNumIdiomas
      
            Select Case j
                    
                Case 0:
                        RowBuf.Value(i, 0) = oTipoPedido.Cod
                        
                'Case 1:
                '        RowBuf.Value(i, 1) = oTipoPedido.Den
                
                Case 2 + m_iNumIdiomas:
                        RowBuf.Value(i, 2 + m_iNumIdiomas) = m_sTextConcepAlmacRecep(0, oTipoPedido.CodConcep)
                
                Case 3 + m_iNumIdiomas:
                        RowBuf.Value(i, 3 + m_iNumIdiomas) = m_sTextConcepAlmacRecep(1, oTipoPedido.CodAlmac)
                        
                Case 4 + m_iNumIdiomas:
                        RowBuf.Value(i, 4 + m_iNumIdiomas) = m_sTextConcepAlmacRecep(2, oTipoPedido.CodRecep)
                        
                Case 5 + m_iNumIdiomas:
                        If oTipoPedido.NumCategorias = 1 Then
                            RowBuf.Value(i, 5 + m_iNumIdiomas) = oTipoPedido.DevolverCategoriaDelTipoPedido
                        ElseIf oTipoPedido.NumCategorias > 1 Then
                            RowBuf.Value(i, 5 + m_iNumIdiomas) = m_sMultiplesCategorias
                        End If
                        
                Case 7 + m_iNumIdiomas:
                       
                        RowBuf.Value(i, 7 + m_iNumIdiomas) = oTipoPedido.Baja
                    
            End Select
            
        Next j
        
        For k = 1 To m_oIdiomas.Count + 1
            For Each oIdioma In m_oIdiomas
                If RowBuf.ColumnName(k) = CStr(oIdioma.Den) Then
                    For Each oDenominacion In oTipoPedido.Denominaciones
                        If CStr(oIdioma.Cod) = CStr(oDenominacion.Cod) Then
                            RowBuf.Value(i, k) = oDenominacion.Den
                        End If
                    Next
                End If
            Next
        Next
        
        RowBuf.Bookmark(i) = m_lIndiceTiposPedido
    
        If ReadPriorRows Then
            m_lIndiceTiposPedido = m_lIndiceTiposPedido - 1
        Else
            m_lIndiceTiposPedido = m_lIndiceTiposPedido + 1
        End If
            r = r + 1
        Next i
    
    RowBuf.RowCount = r
    
End Sub

''' <summary>
''' Evento que se produce al hacer update de la grid
''' </summary>
''' <returns></returns>
''' <param name="RowBuf">Buffer con los datos de la linea</param>
''' <param name="WriteLocation">Tipo de la escritura</param>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>

Private Sub sdbgTiposPedido_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)

    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim v As Variant
    Dim oIdioma As CMultiidioma
    
    If m_bRespetar Then Exit Sub
    If m_oTipoPedidoEnEdicion Is Nothing Then Exit Sub
    
    m_bModError = False
    
    ''' Modificamos en la base de datos
    
    m_oTipoPedidoEnEdicion.Cod = sdbgTiposPedido.Columns("COD").Value
    'm_oTipoPedidoEnEdicion.Den = sdbgTiposPedido.Columns("DEN").Value
    m_oTipoPedidoEnEdicion.Baja = sdbgTiposPedido.Columns("BAJA").Value
    m_oTipoPedidoEnEdicion.CodConcep = TraducirTextoCelda(0, sdbgTiposPedido.Columns("CONCEP").Value)
    m_oTipoPedidoEnEdicion.CodAlmac = TraducirTextoCelda(1, sdbgTiposPedido.Columns("ALMAC").Value)
    m_oTipoPedidoEnEdicion.CodRecep = TraducirTextoCelda(2, sdbgTiposPedido.Columns("RECEP").Value)

    If Not m_oIdiomas Is Nothing Then
        For Each oIdioma In m_oIdiomas
            m_oTipoPedidoEnEdicion.Denominaciones.Item(oIdioma.Cod).Den = sdbgTiposPedido.Columns(oIdioma.Den).Value
        Next
    End If
    
    Screen.MousePointer = vbHourglass
    teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
    Screen.MousePointer = vbNormal
       
    If teserror.NumError <> TESnoerror Then
        v = sdbgTiposPedido.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgTiposPedido.SetFocus
        m_bModError = True
        RowBuf.RowCount = 0
        sdbgTiposPedido.ActiveCell.Value = v
    Else
        ''' Registro de acciones
        '3328
        basSeguridad.RegistrarAccion accionessummit.ACCTipPedAnya, "Cod:" & m_oTipoPedidoEnEdicion.Cod
        
        Accion = ACCTipPedCon
        
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oTipoPedidoEnEdicion = Nothing
    End If
End Sub



''' <summary>
''' Procedimiento que carga los textos que aparecer�n en el formulario en el idioma que corresponda
''' </summary>
''' <remarks>Llamada desde: form_load ; Tiempo m�ximo: 0</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FMR_MANTPEDIDOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
         m_sIdiTitulos(1) = Ador(0).Value & " "
         Ador.MoveNext
         m_sIdiTitulos(2) = Ador(0).Value & " "
         Ador.MoveNext
         m_sIdiTitulos(3) = Ador(0).Value
         caption = m_sIdiTitulos(3)
         Ador.MoveNext

         cmdEliminarTiposPedido.caption = Ador(0).Value
         Ador.MoveNext

         m_sIdiModos(1) = Ador(0).Value
         cmdModoEdicion.caption = m_sIdiModos(1)
         Ador.MoveNext
         
         m_sIdiModos(2) = Ador(0).Value
         Ador.MoveNext

         cmdRestaurarTiposPedido.caption = Ador(0).Value
         Ador.MoveNext

         m_sIdiCod = Ador(0).Value
         sdbgTiposPedido.Columns("COD").caption = Ador(0).Value
         Ador.MoveNext

         sdbgTiposPedido.Columns("ATRIBUTOS").caption = Ador(0).Value
         Ador.MoveNext
          
         m_sTextConcepAlmacRecep(0, 0) = Ador(0).Value 'Gasto
         Ador.MoveNext
         m_sTextConcepAlmacRecep(0, 1) = Ador(0).Value 'Inversi�n
         Ador.MoveNext
         m_sTextConcepAlmacRecep(0, 2) = Ador(0).Value 'Gas./Inv.
         Ador.MoveNext
         
         m_sTextConcepAlmacRecep(1, 1) = Ador(0).Value 'Obligatorio almacenar
         Ador.MoveNext
         m_sTextConcepAlmacRecep(1, 0) = Ador(0).Value 'No almacenable
         Ador.MoveNext
         m_sTextConcepAlmacRecep(1, 2) = Ador(0).Value 'Opcional almacenar
         
         m_sTextConcepAlmacRecep(2, 2) = Ador(0).Value 'Opcional recepcionar
         Ador.MoveNext
         m_sTextConcepAlmacRecep(2, 1) = Ador(0).Value 'Obligatoria recepci�n
         Ador.MoveNext
         m_sTextConcepAlmacRecep(2, 0) = Ador(0).Value 'No recepcionar
         Ador.MoveNext
         
         m_sIdiConcepto = Ador(0).Value
         sdbgTiposPedido.Columns("CONCEP").caption = Ador(0).Value
         Ador.MoveNext
         
         m_sIdiAlmacenable = Ador(0).Value
         sdbgTiposPedido.Columns("ALMAC").caption = Ador(0).Value
         Ador.MoveNext
         
         m_sIdiRecepcionable = Ador(0).Value
         sdbgTiposPedido.Columns("RECEP").caption = Ador(0).Value
         Ador.MoveNext

         sdbgTiposPedido.Columns("BAJA").caption = Ador(0).Value
         Ador.MoveNext

         chkTodosIdiomas.caption = Ador(0).Value
         Ador.MoveNext
         
         m_sIdiDen = Ador(0).Value
         Ador.MoveNext
                  
         m_sIdiOrden = Ador(0).Value
         Ador.MoveNext
         
         cmdDeshacerTiposPedido.caption = Ador(0).Value
         Ador.MoveNext
         
         sdbgTiposPedido.Columns("CATEG").caption = Ador(0).Value
         Ador.MoveNext
         m_sMultiplesCategorias = Ador(0).Value
         
         Ador.Close
    
    End If

    Set Ador = Nothing

End Sub



Private Sub ssdbddConcepto_DropDown()
    
    If sdbgTiposPedido.Columns("CONCEP").Locked Then
        ssdbddConcepto.DroppedDown = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    sdbgTiposPedido.ActiveCell.SelStart = 0
    sdbgTiposPedido.ActiveCell.SelLength = Len(sdbgTiposPedido.ActiveCell.Text)

    Screen.MousePointer = vbNormal

End Sub

Private Sub ssdbddConcepto_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    ssdbddConcepto.DataFieldList = "Column 0"
    ssdbddConcepto.DataFieldToDisplay = "Column 0"
        
End Sub

Private Sub ssdbddConcepto_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    ssdbddConcepto.MoveFirst
    
    If Text <> "" Then
        For i = 0 To ssdbddConcepto.Rows - 1
            bm = ssdbddConcepto.GetBookmark(i)
            If UCase(Text) = UCase(Mid(ssdbddConcepto.Columns(0).CellText(bm), 1, Len(Text))) Then
                ssdbddConcepto.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub ssdbddConcepto_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Integer
Dim bPasa As Boolean

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
            
        For i = 0 To 2
            If UCase(sdbgTiposPedido.Columns("CONCEP").Text) = UCase(m_sTextConcepAlmacRecep(0, i)) Then
                bPasa = True
                Exit For
            End If
        Next
        If bPasa = False Then
            RtnPassed = False
            sdbgTiposPedido.Columns("CONCEP").Text = m_sTextConcepAlmacRecep(0, g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).CodConcep)
        End If
    End If
    
End Sub

Private Sub ssdbddAlmacen_CloseUp()

    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim oArt As CArticulo
    Dim bk As Variant
    
    If m_aBookmarks Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    If m_aBookmarks.Count > 0 Then
        m_bRespetar = True
        teserror = g_oTiposPedido.ActualizarConcepAlmacRecep(Almacenar, TraducirTextoCelda(1, ssdbddAlmacen.Columns(0).Value), m_aBookmarks)
        If teserror.Arg2 Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesArticulos teserror.Arg1
            Screen.MousePointer = vbHourglass
            Set m_oTiposPedidoAModificar = Nothing

            Screen.MousePointer = vbNormal
            'Vuelvo a seleccionar las filas modificadas
            sdbgTiposPedido.ReBind
            If m_aBookmarks.Count > 0 Then
                For Each bk In m_aBookmarks
                    sdbgTiposPedido.SelBookmarks.Add bk
                Next
            End If
        Else
            For Each oArt In m_oTiposPedidoAModificar
                RegistrarAccion accionessummit.ACCTipPedMod, "Art�culo: " & oArt.Cod & " Almacenable: " & oArt.Almacenable
            Next
        End If
        
        sdbgTiposPedido.Update
        sdbgTiposPedido.ReBind
        If m_aBookmarks.Count > 0 Then
            For Each bk In m_aBookmarks
                sdbgTiposPedido.SelBookmarks.Add bk
            Next
        End If
        Set m_aBookmarks = Nothing
        m_bRespetar = False
        
    End If
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub ssdbddAlmacen_DropDown()

    If sdbgTiposPedido.Columns("ALMAC").Locked Then
        ssdbddAlmacen.DroppedDown = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    sdbgTiposPedido.ActiveCell.SelStart = 0
    sdbgTiposPedido.ActiveCell.SelLength = Len(sdbgTiposPedido.ActiveCell.Text)

    Screen.MousePointer = vbNormal

End Sub

Private Sub ssdbddAlmacen_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    ssdbddAlmacen.DataFieldList = "Column 0"
    ssdbddAlmacen.DataFieldToDisplay = "Column 0"
        
End Sub

Private Sub ssdbddAlmacen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    ssdbddAlmacen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To ssdbddAlmacen.Rows - 1
            bm = ssdbddAlmacen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(ssdbddAlmacen.Columns(0).CellText(bm), 1, Len(Text))) Then
                ssdbddAlmacen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub ssdbddAlmacen_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Integer
Dim bPasa As Boolean

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        
        For i = 0 To 2
            If UCase(sdbgTiposPedido.Columns("ALMAC").Text) = UCase(m_sTextConcepAlmacRecep(1, i)) Then
                bPasa = True
                Exit For
            End If
        Next
        If bPasa = False Then
            RtnPassed = False
            sdbgTiposPedido.Columns("ALMAC").Text = m_sTextConcepAlmacRecep(1, g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).CodAlmac)
        End If
    End If
    
End Sub

Private Sub ssdbddRecepcion_CloseUp()
    Dim teserror As TipoErrorSummit
    Dim oArt As CArticulo
    Dim bk As Variant
    
    If m_aBookmarks Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_aBookmarks.Count > 0 Then
        m_bRespetar = True
        teserror = g_oTiposPedido.ActualizarConcepAlmacRecep(Recepcionar, TraducirTextoCelda(2, ssdbddRecepcion.Columns("DEN").Value), m_aBookmarks)
        If teserror.Arg2 Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesTiposPedido teserror.Arg1
            Screen.MousePointer = vbHourglass

            Set m_oTiposPedidoAModificar = Nothing

            Screen.MousePointer = vbNormal
            'Vuelvo a seleccionar las filas modificadas
            sdbgTiposPedido.ReBind
            If m_aBookmarks.Count > 0 Then
                For Each bk In m_aBookmarks
                    sdbgTiposPedido.SelBookmarks.Add bk
                Next
            End If
        Else
            For Each oArt In m_oTiposPedidoAModificar
                RegistrarAccion accionessummit.ACCTipPedMod, "Art�culo: " & oArt.Cod & " Recepcionable: " & oArt.Recepcionable
            Next
        End If
        
        sdbgTiposPedido.Update
        sdbgTiposPedido.ReBind
        If m_aBookmarks.Count > 0 Then
            For Each bk In m_aBookmarks
                sdbgTiposPedido.SelBookmarks.Add bk
            Next
        End If
        Set m_aBookmarks = Nothing
        m_bRespetar = False
        
        
    End If
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub ssdbddRecepcion_DropDown()

    If sdbgTiposPedido.Columns("RECEP").Locked Then
        ssdbddRecepcion.DroppedDown = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

   

    sdbgTiposPedido.ActiveCell.SelStart = 0
    sdbgTiposPedido.ActiveCell.SelLength = Len(sdbgTiposPedido.ActiveCell.Text)

    Screen.MousePointer = vbNormal

End Sub

Private Sub ssdbddRecepcion_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    ssdbddRecepcion.DataFieldList = "Column 0"
    ssdbddRecepcion.DataFieldToDisplay = "Column 0"
        
End Sub

Private Sub ssdbddRecepcion_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    ssdbddRecepcion.MoveFirst
    
    If Text <> "" Then
        For i = 0 To ssdbddRecepcion.Rows - 1
            bm = ssdbddRecepcion.GetBookmark(i)
            If UCase(Text) = UCase(Mid(ssdbddRecepcion.Columns(0).CellText(bm), 1, Len(Text))) Then
                ssdbddRecepcion.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub ssdbddRecepcion_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Integer
Dim bPasa As Boolean

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        
        For i = 0 To 2
            If UCase(sdbgTiposPedido.Columns("RECEP").Text) = UCase(m_sTextConcepAlmacRecep(2, i)) Then
                bPasa = True
                Exit For
            End If
        Next
        If bPasa = False Then
            RtnPassed = False
            sdbgTiposPedido.Columns("RECEP").Text = m_sTextConcepAlmacRecep(2, g_oTiposPedido.Item(CStr(sdbgTiposPedido.Bookmark)).CodRecep)
        End If
    End If
    
End Sub

Private Sub CargarGridConConcepto()

    Dim i As Integer
    
    ssdbddConcepto.RemoveAll
    
    For i = 0 To 2
        ssdbddConcepto.AddItem m_sTextConcepAlmacRecep(0, i)
    Next
    
End Sub

Private Sub CargarGridConAlmacen()

    Dim i As Integer
    
    ssdbddAlmacen.RemoveAll
    
    For i = 0 To 2
        ssdbddAlmacen.AddItem m_sTextConcepAlmacRecep(1, i)
    Next
    
End Sub

Private Sub CargarGridConRecepcion()

    Dim i As Integer
    
    ssdbddRecepcion.RemoveAll
    
    For i = 0 To 2
        ssdbddRecepcion.AddItem m_sTextConcepAlmacRecep(2, i)
    Next
        
End Sub

Private Function TraducirTextoCelda(ByVal Tipo As Integer, sTexto As String) As Integer
Dim i As Integer

For i = 0 To 2
    If UCase(m_sTextConcepAlmacRecep(Tipo, i)) = UCase(sTexto) Then
        TraducirTextoCelda = i
        Exit Function
    End If
Next
End Function
