VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmARTFiltrar 
   BackColor       =   &H00808000&
   Caption         =   "DArt�culos (Filtro)"
   ClientHeight    =   7665
   ClientLeft      =   1785
   ClientTop       =   2820
   ClientWidth     =   8925
   Icon            =   "frmARTFiltrar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7665
   ScaleWidth      =   8925
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3120
      TabIndex        =   31
      Top             =   7200
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4335
      TabIndex        =   30
      Top             =   7200
      Width           =   1005
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   6915
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8715
      Begin VB.TextBox txtUonSeleccionada 
         BackColor       =   &H80000018&
         Enabled         =   0   'False
         Height          =   285
         Left            =   2160
         MaxLength       =   100
         TabIndex        =   34
         Top             =   840
         Width           =   5295
      End
      Begin VB.CommandButton cmdLimpiarUon 
         Height          =   285
         Left            =   7560
         Picture         =   "frmARTFiltrar.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   840
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscarUon 
         Height          =   285
         Left            =   7920
         Picture         =   "frmARTFiltrar.frx":068C
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   840
         Width           =   315
      End
      Begin VB.Frame fraOpciones 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   2655
         Left            =   120
         TabIndex        =   12
         Top             =   4200
         Width           =   8415
         Begin VB.CheckBox chkArtiCent 
            BackColor       =   &H00808000&
            Caption         =   "dBuscar solo Art�culos centrales"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4680
            TabIndex        =   29
            Top             =   360
            Width           =   3015
         End
         Begin VB.CheckBox chkArtiGen 
            BackColor       =   &H00808000&
            Caption         =   "Buscar s�lo art�culos genericos"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   240
            TabIndex        =   28
            Top             =   360
            Width           =   3690
         End
         Begin VB.Frame fraConcepto 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Caption         =   "DConcepto"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   240
            TabIndex        =   23
            Top             =   960
            Width           =   2115
            Begin VB.CheckBox chkConcepto 
               BackColor       =   &H00808000&
               Caption         =   "DGasto"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   26
               Top             =   240
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               BackColor       =   &H00808000&
               Caption         =   "DInversi�n"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   25
               Top             =   570
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               BackColor       =   &H00808000&
               Caption         =   "DGasto/Inversi�n"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   24
               Top             =   870
               Width           =   1935
            End
            Begin VB.Label lblConcepto 
               BackColor       =   &H00808000&
               Caption         =   "DConcepto"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   120
               TabIndex        =   27
               Top             =   0
               Width           =   1095
            End
         End
         Begin VB.Frame fraAlmacen 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Caption         =   "DAlmacenamiento"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   3000
            TabIndex        =   18
            Top             =   960
            Width           =   2115
            Begin VB.CheckBox chkAlmacen 
               BackColor       =   &H00808000&
               Caption         =   "DObligatorio"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   21
               Top             =   240
               Width           =   1905
            End
            Begin VB.CheckBox chkAlmacen 
               BackColor       =   &H00808000&
               Caption         =   "DNo almacenar"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   20
               Top             =   570
               Width           =   1935
            End
            Begin VB.CheckBox chkAlmacen 
               BackColor       =   &H00808000&
               Caption         =   "DOpcional"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   19
               Top             =   840
               Width           =   1875
            End
            Begin VB.Label lblAlmacenamiento 
               BackColor       =   &H00808000&
               Caption         =   "DAlmacenamiento"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Left            =   120
               TabIndex        =   22
               Top             =   0
               Width           =   1575
            End
         End
         Begin VB.Frame fraRecepcion 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Caption         =   "DRecepci�n"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   5880
            TabIndex        =   13
            Top             =   960
            Width           =   2115
            Begin VB.CheckBox chkRecep 
               BackColor       =   &H00808000&
               Caption         =   "DObligatoria"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   16
               Top             =   270
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               BackColor       =   &H00808000&
               Caption         =   "DNo recepcionar"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   15
               Top             =   570
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               BackColor       =   &H00808000&
               Caption         =   "DOpcional"
               ForeColor       =   &H00FFFFFF&
               Height          =   285
               Index           =   2
               Left            =   120
               TabIndex        =   14
               Top             =   840
               Width           =   1935
            End
            Begin VB.Label lblRecepcion 
               BackColor       =   &H00808000&
               Caption         =   "DRecepci�n"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   120
               TabIndex        =   17
               Top             =   0
               Width           =   1215
            End
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00FFFFFF&
            X1              =   -120
            X2              =   8280
            Y1              =   240
            Y2              =   240
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00FFFFFF&
            X1              =   -120
            X2              =   8280
            Y1              =   720
            Y2              =   720
         End
      End
      Begin VB.PictureBox picUons 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         FillStyle       =   0  'Solid
         Height          =   375
         Left            =   240
         ScaleHeight     =   375
         ScaleWidth      =   8175
         TabIndex        =   11
         Top             =   4080
         Width           =   8175
      End
      Begin VB.CommandButton cmdBuscaAtrib 
         Height          =   285
         Left            =   7920
         Picture         =   "frmARTFiltrar.frx":0A0E
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   1560
         Width           =   315
      End
      Begin VB.CommandButton cmdAyuda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8040
         Picture         =   "frmARTFiltrar.frx":0A9B
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   270
         Width           =   225
      End
      Begin VB.TextBox txtDEN 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4800
         MaxLength       =   200
         TabIndex        =   2
         Top             =   240
         Width           =   3195
      End
      Begin VB.TextBox txtCOD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   20
         TabIndex        =   0
         Top             =   240
         Width           =   1905
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddOper 
         Height          =   915
         Left            =   1680
         TabIndex        =   7
         Top             =   2160
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmARTFiltrar.frx":0CCC
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "VALOR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DESC"
         Columns(1).Name =   "DESC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
         Height          =   915
         Left            =   3840
         TabIndex        =   8
         Top             =   2160
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmARTFiltrar.frx":0CE8
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "VALOR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DESC"
         Columns(1).Name =   "DESC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
         Height          =   2295
         Left            =   240
         TabIndex        =   9
         Top             =   1920
         Width           =   8145
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   11
         stylesets.count =   3
         stylesets(0).Name=   "Yellow"
         stylesets(0).BackColor=   11862015
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmARTFiltrar.frx":0D04
         stylesets(1).Name=   "Normal"
         stylesets(1).ForeColor=   0
         stylesets(1).BackColor=   16777215
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmARTFiltrar.frx":0D20
         stylesets(2).Name=   "Header"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmARTFiltrar.frx":0D3C
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   11
         Columns(0).Width=   979
         Columns(0).Name =   "USAR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   3201
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   4419
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1058
         Columns(3).Caption=   "OPER"
         Columns(3).Name =   "OPER"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3889
         Columns(4).Caption=   "VALOR"
         Columns(4).Name =   "VALOR"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "INTRO"
         Columns(5).Name =   "INTRO"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "IDTIPO"
         Columns(6).Name =   "IDTIPO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "ID_ATRIB"
         Columns(7).Name =   "ID_ATRIB"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "MAXIMO"
         Columns(8).Name =   "MAXIMO"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "MINIMO"
         Columns(9).Name =   "MINIMO"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "VALOR_ATRIB"
         Columns(10).Name=   "VALOR_ATRIB"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         _ExtentX        =   14367
         _ExtentY        =   4048
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00FFFFFF&
         X1              =   120
         X2              =   8520
         Y1              =   1200
         Y2              =   1200
      End
      Begin VB.Label lblUons 
         BackColor       =   &H00808000&
         Caption         =   "DUnidades org. :"
         ForeColor       =   &H8000000F&
         Height          =   255
         Left            =   240
         TabIndex        =   35
         Top             =   840
         Width           =   1935
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   120
         X2              =   8520
         Y1              =   680
         Y2              =   680
      End
      Begin VB.Label lblBusqAtrib 
         BackColor       =   &H00808000&
         Caption         =   "B�squeda por atributos"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   3240
         TabIndex        =   4
         Top             =   270
         Width           =   1470
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   270
         Width           =   885
      End
   End
End
Attribute VB_Name = "frmARTFiltrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmESTRMATFiltrar
''' *** Creacion: 1/1/1999 (Javier Arana)
''' *** Ultima revision:

Private Const cnEspacio As Long = 120

Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String
Public m_oUonsSeleccionadas As CUnidadesOrganizativas
Public bArtCentrales As Boolean

Private arOper As Variant
Private msTrue As String
Private msFalse As String
Private msMsgMinMax As String
Private m_arConcepto() As Boolean
Private m_arAlmacen() As Boolean
Private m_arRecep() As Boolean

''seguridad
Private m_bRestrMantUsu As Boolean
Private m_bRestrMantPerf As Boolean
Private oAtribs As CAtributos

Option Explicit

Private Sub cmdAyuda_Click()
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Public Function ArtCentrales() As Boolean
    ArtCentrales = Me.chkArtiCent.Value
End Function

Private Sub cmdBuscaAtrib_Click()
    Dim ofrmATRIB As frmAtribMod

    Set ofrmATRIB = New frmAtribMod

    ofrmATRIB.g_sOrigen = "frmARTFiltrar"

    ofrmATRIB.g_sGmn1 = sGMN1Cod
    ofrmATRIB.g_sGmn2 = sGMN2Cod
    ofrmATRIB.g_sGmn3 = sGMN3Cod
    ofrmATRIB.g_sGmn4 = sGMN4Cod

    ofrmATRIB.sstabGeneral.Tab = 0
    ofrmATRIB.g_GMN1RespetarCombo = True

    ofrmATRIB.sdbcGMN1_4Cod.Text = sGMN1Cod
        
    ofrmATRIB.sdbcGMN1_4Cod.Text = sGMN1Cod
    ofrmATRIB.sdbcGMN1_4Cod_Validate False
    ofrmATRIB.g_GMN1RespetarCombo = False
    
    ofrmATRIB.g_GMN2RespetarCombo = True
    ofrmATRIB.sdbcGMN2_4Cod.Text = sGMN2Cod
    ofrmATRIB.sdbcGMN2_4Cod_Validate False
    ofrmATRIB.g_GMN2RespetarCombo = False
    
    ofrmATRIB.g_GMN3RespetarCombo = True
    ofrmATRIB.sdbcGMN3_4Cod.Text = sGMN3Cod
    ofrmATRIB.sdbcGMN3_4Cod_Validate False
    ofrmATRIB.g_GMN3RespetarCombo = False
    
    ofrmATRIB.g_GMN4RespetarCombo = True
    ofrmATRIB.sdbcGMN4_4Cod.Text = sGMN4Cod
    ofrmATRIB.sdbcGMN4_4Cod_Validate False
    ofrmATRIB.g_GMN4RespetarCombo = False

    ofrmATRIB.cmdSeleccionar.Visible = True

    ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
    ofrmATRIB.g_bSoloSeleccion = True

    ofrmATRIB.Show vbModal
End Sub

Private Sub ConfigurarSeguridad()

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonUsu)) Is Nothing) Then
        m_bRestrMantUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonPerf)) Is Nothing) Then
        m_bRestrMantPerf = True
    End If
End Sub

Private Sub cmdBuscarUon_Click()
    Dim frm As frmSELUO
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    
    'obtenemos las uons en funci�n de las restricciones de seguridad
    oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , , False
    oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , , False
    oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , False
    
    Set frm = New frmSELUO
    frm.CheckChildren = False
    frm.multiselect = True
    frm.GuardarUon0 = False
    'Cargamos las uons en el arbol
    frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
    If Not m_oUonsSeleccionadas Is Nothing Then
        Set frm.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    
    
    frm.Show vbModal
    If frm.Aceptar Then
        Me.txtUonSeleccionada.Text = frm.UonsSeleccionadas.titulo
        Set m_oUonsSeleccionadas = frm.UonsSeleccionadas
    End If
    
    CargarAtributos
End Sub

Private Sub cmdLimpiarUon_Click()
    Me.txtUonSeleccionada.Text = ""
    m_oUonsSeleccionadas.clear
    CargarAtributos
End Sub

Private Sub Form_Initialize()
    Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
End Sub

Private Sub Form_Load()

    ''' * Objetivo: Situar el formulario respecto al
    ''' * Objetivo: principal de Articulos
    
    On Error Resume Next
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
    CargarRecursos
    PonerFieldSeparator Me
    CargarComboOperandos arOper, sdbddOper
    CargarAtributos
    
    Me.Left = frmESTRMAT.Left + 500
    Me.Top = frmESTRMAT.Top + 1000
    
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodART
    
    chkArtiGen.Visible = gParametrosGenerales.gbArticulosGenericos
    Me.chkArtiCent.Visible = gParametrosGenerales.gbArticulosCentrales
    Line2.Visible = gParametrosGenerales.gbArticulosGenericos
    ConfigurarSeguridad
    
    If m_oUonsSeleccionadas.Count > 0 Then
        Me.txtUonSeleccionada.Text = m_oUonsSeleccionadas.titulo
    End If
End Sub

''' <summary>Carga el grid de atributos con los atributos de material</summary>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarAtributos()
    Dim oatrib As CAtributo
    Dim oGMN4 As CGrupoMatNivel4
    
    sdbgAtributos.RemoveAll
    Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
    oGMN4.GMN1Cod = sGMN1Cod
    oGMN4.GMN2Cod = sGMN2Cod
    oGMN4.GMN3Cod = sGMN3Cod
    oGMN4.Cod = sGMN4Cod
    
    If m_oUonsSeleccionadas.Count = 0 Then
        Set oAtribs = oGMN4.DevolverAtribMatYArtMat
    Else
        Set oAtribs = oGMN4.DevolverAtribMatYArtMat(oUons:=m_oUonsSeleccionadas)
    End If
    If Not oAtribs Is Nothing Then
        If oAtribs.Count > 0 Then
            With sdbgAtributos
                For Each oatrib In oAtribs
                    .AddItem "0" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo
                Next
            End With
        End If
    End If
    
    
    Set oGMN4 = Nothing
    Set oatrib = Nothing
End Sub

'''<summary>Carga los idiomas del formulario</summary>
'''<remarks>Tiempo m�ximo: >2 seg</remarks>

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
On Error Resume Next
Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ART_FILTRAR, basPublic.gParametrosInstalacion.gIdioma)
If Not Ador Is Nothing Then
    Ador.MoveNext
    cmdAceptar.caption = Ador(0).Value      '2
    Ador.MoveNext
    cmdCancelar.caption = Ador(0).Value     '3
    Ador.MoveNext
    frmARTFiltrar.caption = Ador(0).Value  '4
    Ador.MoveNext
    lblCod.caption = Ador(0).Value & ":"
    Ador.MoveNext
    lblDen.caption = Ador(0).Value & ":"
    Ador.MoveNext
    lblBusqAtrib.caption = Ador(0).Value & ":"
    Ador.MoveNext
    sdbgAtributos.Columns("COD").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAtributos.Columns("DEN").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAtributos.Columns("VALOR").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAtributos.Columns("OPER").caption = Ador(0).Value
    Ador.MoveNext
    chkArtiGen.caption = Ador(0).Value
    Ador.MoveNext
    lblConcepto.caption = Ador(0).Value
    Ador.MoveNext
    lblAlmacenamiento.caption = Ador(0).Value
    Ador.MoveNext
    lblRecepcion.caption = Ador(0).Value
    Ador.MoveNext
    chkConcepto(0).caption = Ador(0).Value
    Ador.MoveNext
    chkConcepto(1).caption = Ador(0).Value
    Ador.MoveNext
    chkConcepto(2).caption = Ador(0).Value
    Ador.MoveNext
    chkAlmacen(1).caption = Ador(0).Value
    chkRecep(1).caption = Ador(0).Value
    Ador.MoveNext
    chkAlmacen(0).caption = Ador(0).Value
    Ador.MoveNext
    chkAlmacen(2).caption = Ador(0).Value
    chkRecep(2).caption = Ador(0).Value
    Ador.MoveNext
    chkRecep(0).caption = Ador(0).Value
    Ador.MoveNext
    msTrue = Ador(0).Value
    Ador.MoveNext
    msFalse = Ador(0).Value
    Ador.MoveNext
    msMsgMinMax = Ador(0).Value
    Ador.MoveNext
    chkArtiCent.caption = Ador(0).Value
    Ador.MoveNext
    Me.lblUons.caption = Ador(0).Value
    Ador.Close
End If
Set Ador = Nothing
End Sub

Private Sub cmdAceptar_Click()
    Dim oAtributos As CAtributos
    Dim iNumAtrib As Integer
    Dim i As Integer
    Dim o As CAtributo
    
    Screen.MousePointer = vbHourglass
    
    'Preparo los checks de los conceptos en arrays
    ReDim m_arConcepto(0 To 2)
    ReDim m_arAlmacen(0 To 2)
    ReDim m_arRecep(0 To 2)
    For i = 0 To 2
        If chkConcepto(i).Value = vbChecked Then m_arConcepto(i) = True
        If chkAlmacen(i).Value = vbChecked Then m_arAlmacen(i) = True
        If chkRecep(i).Value = vbChecked Then m_arRecep(i) = True
    Next
    
    'Atributos
    If sdbgAtributos.Rows > 0 Then
        Set oAtributos = oFSGSRaiz.Generar_CAtributos
        
        With sdbgAtributos
            iNumAtrib = 0
            Do While iNumAtrib < .Rows
                .Bookmark = iNumAtrib
                If .Columns("USAR").Value Then
                    If .Columns("VALOR").Value <> "" Then
                        Select Case .Columns("IDTIPO").Value
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_text:=Replace(.Columns("VALOR").Value, "*", "%"))
                            Case TiposDeAtributos.TipoNumerico
                                Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_num:=.Columns("VALOR").Value, Formula:=.Columns("OPER").Value)
                            Case TiposDeAtributos.TipoFecha
                                Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_fec:=.Columns("VALOR").Value)
                            Case TiposDeAtributos.TipoBoolean
                                Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_bool:=.Columns("VALOR_ATRIB").Value)
                        End Select
                    Else
                        
                        Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value)
                        
                    End If
                    o.AmbitoAtributo = oAtribs.Item(.Columns("ID_ATRIB").Value).AmbitoAtributo
                End If
                iNumAtrib = iNumAtrib + 1
            Loop
            
            .Bookmark = 0
        End With
    End If
    
    Select Case gParametrosGenerales.giNEM
    
        Case 1
        
        Case 2
        
        Case 3
        
        Case 4
            
            Set frmESTRMAT.g_oArticulos = Nothing
                        
            frmESTRMAT.g_oGMN4Seleccionado.CargarArticulosComodin gParametrosInstalacion.giCargaMaximaCombos, Trim(txtCod), txtDen, _
                                , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, , _
                                , , , , , , , , chkArtiGen.Value, m_arConcepto, m_arAlmacen, m_arRecep, oAtributos, m_bRestrMantUsu, m_bRestrMantPerf, _
                                Me.ArtCentrales, m_oUonsSeleccionadas, True, , oUsuarioSummit.Perfil.Id, basOptimizacion.gUON1Usuario, _
                                basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, False, False
                                
            frmESTRMAT.ponerCaption txtCod, 1, False
                
            Set frmESTRMAT.g_oArticulos = frmESTRMAT.g_oGMN4Seleccionado.ARTICULOS
            frmESTRMAT.sdbgArticulos.ReBind
            MDI.MostrarFormulario frmESTRMAT, True
            frmESTRMAT.sdbgArticulos.MoveFirst
            
            Screen.MousePointer = vbNormal
        
            Unload Me
        
            frmESTRMAT.SetFocus
            
    End Select
    
End Sub
Private Sub cmdCancelar_Click()

    ''' * Objetivo: Cerrar el formulario
    
    MDI.MostrarFormulario frmESTRMAT, True
    
    Unload Me
    
    frmESTRMAT.SetFocus
    
    
    
End Sub
Private Sub Form_Activate()

    ''' * Objetivo: Iniciar el formulario
    
    If Me.Visible Then txtCod.SetFocus
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>Coloca los controles en el formulario en funci�n del tama�o de este</summary>
''' <remarks>Llamada desde: Form_Resize</remarks>

Private Sub Arrange()
    If Me.WindowState <> vbMinimized Then
        If Me.Height < 6705 Then Me.Height = 6705
        If Me.Width < 9045 Then Me.Width = 9045
        
        Frame1.Width = Me.ScaleWidth - (2 * cnEspacio)
        Frame1.Height = Me.ScaleHeight - cmdAceptar.Height - (3 * cnEspacio)
        
        Line1.X2 = Frame1.Width - cnEspacio
        Line2.X2 = Frame1.Width - cnEspacio
        Line3.X2 = Frame1.Width - cnEspacio
        
        fraOpciones.Top = Frame1.Top + Frame1.Height - fraOpciones.Height - cnEspacio
        'fraAlmacen.Top = fraConcepto.Top
        'fraRecepcion.Top = fraConcepto.Top
        
        Line2.Y1 = fraOpciones.Top - (2 * cnEspacio)
        Line2.Y2 = Line3.Y1
            
        If chkArtiGen.Visible Then
            chkArtiGen.Top = Line3.Y1 - chkArtiGen.Height - cnEspacio
            chkArtiCent.Top = chkArtiGen.Top
            Line2.Y1 = chkArtiGen.Top - cnEspacio
            Line2.Y2 = Line2.Y1
        End If
        sdbgAtributos.Height = fraOpciones.Top - cnEspacio - sdbgAtributos.Top - cmdAceptar.Height
        sdbgAtributos.Width = Frame1.Width - (4 * cnEspacio)
        
        sdbgAtributos.Columns("USAR").Width = 600
        sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width - 1200) * 0.22
        sdbgAtributos.Columns("DEN").Width = (sdbgAtributos.Width - 1200) * 0.42
        sdbgAtributos.Columns("OPER").Width = 600
        sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width - 1200) * 0.33

        cmdAceptar.Top = Frame1.Top + Frame1.Height + cnEspacio
        cmdCancelar.Top = cmdAceptar.Top
        cmdAceptar.Left = (Me.Width / 2) - cmdAceptar.Width - 300
        cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + 300
    End If
End Sub

Private Sub Form_Terminate()
    Set m_oUonsSeleccionadas = Nothing
    Set oAtribs = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oAtribs = Nothing
End Sub

Private Sub sdbddOper_InitColumnProps()
    sdbddOper.DataFieldList = "Column 0"
    sdbddOper.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddOper_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddOper.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOper.Rows - 1
            bm = sdbddOper.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.Col).Value = Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOper.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddOper_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim i As Integer

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        
        ''' Comprobar la existencia en la lista
        For i = 0 To UBound(arOper)
            If arOper(i) = sdbgAtributos.Columns(sdbgAtributos.Col).Text Then
                bExiste = True
                Exit For
            End If
        Next
        
        If Not bExiste Then
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.Col).Text
            sdbgAtributos.Columns(sdbgAtributos.Col).Text = ""
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    Dim oLista As CValoresPond
    Dim oElem As CValorPond

    sdbddValor.RemoveAll
    
    sdbddValor.AddItem ""

    iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

    Set oatrib = oFSGSRaiz.Generar_CAtributo
    Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)

    If Not oatrib Is Nothing Then
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            oatrib.CargarListaDeValores
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                sdbddValor.AddItem oElem.ValorLista & Chr(m_lSeparador) & oElem.ValorLista
            Next
            Set oLista = Nothing
        Else
            'Campo de tipo boolean:
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem msTrue & Chr(m_lSeparador) & msTrue
                sdbddValor.AddItem msFalse & Chr(m_lSeparador) & msFalse
            End If
        End If
    
        Set oatrib = Nothing
    End If
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.Col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

            Set oatrib = oFSGSRaiz.Generar_CAtributo
            Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
            
            If oatrib Is Nothing Then Exit Sub
            
            oatrib.CargarListaDeValores
            
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns(sdbgAtributos.Col).Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                If UCase(msTrue) = UCase(sdbgAtributos.Columns(sdbgAtributos.Col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "1"
                    bExiste = True
                End If
                If UCase(msFalse) = UCase(sdbgAtributos.Columns(sdbgAtributos.Col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "0"
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns(sdbgAtributos.Col).Text = ""
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.Col).caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbgatributos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    With sdbgAtributos
        Select Case .Columns(ColIndex).Name
            Case "VALOR"
                'Comprobar que el valor introducido se corresponde con el tipo de atributo
                If .Columns("VALOR").Text <> "" Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoString
                        Case TiposDeAtributos.TipoNumerico
                            If (Not IsNumeric(.Columns("VALOR").Value)) Then
                                oMensajes.AtributoValorNoValido "TIPO2"
                                Cancel = True
                            Else
                                If .Columns("OPER").Text <> "" And .Columns("OPER").Text = "=" Then
                                    If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                        Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value), msMsgMinMax)
                                    End If
                                End If
                            End If
                            
                            If .Columns("OPER").Text = "" Then
                                .Columns("OPER").Text = "="
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value), msMsgMinMax)
                                End If
                            End If
                        Case TiposDeAtributos.TipoFecha
                            If (Not IsDate(.Columns("VALOR").Text) And .Columns("VALOR").Value <> "") Then
                                oMensajes.AtributoValorNoValido "TIPO3"
                                Cancel = True
                            Else
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    If CDate(.Columns("MINIMO").Value) > CDate(.Columns("VALOR").Value) Or CDate(.Columns("MAXIMO").Value) < CDate(.Columns("VALOR").Value) Then
                                        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", .Columns("MINIMO").Value), "@Valor2", .Columns("MAXIMO").Value), vbInformation, "FULLSTEP"
                                        Cancel = True
                                    End If
                                End If
                            End If
                        Case TiposDeAtributos.TipoBoolean
                    End Select
                End If
            Case "OPER"
                'Comprobar si se est� intentando dejar en blanco la col. del operando teniendo un valor introducido
                If .Columns("OPER").Text = vbNullString And .Columns("VALOR").Text <> vbNullString Then
                    Cancel = True
                ElseIf .Columns("OPER").Text = "=" And .Columns("VALOR").Text <> vbNullString Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoNumerico
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value), msMsgMinMax)
                            End If
                    End Select
                End If
        End Select
    End With
End Sub

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bCargarCombo As Boolean
    
    'Combo de operandos
    If sdbgAtributos.Columns("IDTIPO").Value = TipoNumerico Then
        sdbgAtributos.Columns("OPER").DropDownHwnd = sdbddOper.hWnd
        sdbgAtributos.Columns("OPER").Locked = False
    Else
        sdbgAtributos.Columns("OPER").DropDownHwnd = 0
        sdbgAtributos.Columns("OPER").Locked = True
    End If
    
    'Combo de valores
    bCargarCombo = False
    If sdbgAtributos.Col = sdbgAtributos.Columns("VALOR").Position Then
        If sdbgAtributos.Columns("INTRO").Value Then
            bCargarCombo = True
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TipoBoolean Then
                bCargarCombo = True
            End If
        End If
    End If
    
    If bCargarCombo Then
        sdbddValor.RemoveAll
        sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor_DropDown
    Else
        sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
    End If
End Sub

Public Function addAtributo(ByRef oAtributo As CAtributo) As CAtributo
    If Not oAtribs.existe(oAtributo.Id) Then
        Set addAtributo = oAtribs.addAtributo(oAtributo)
    End If
End Function
