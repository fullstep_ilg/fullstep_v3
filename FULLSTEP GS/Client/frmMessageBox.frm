VERSION 5.00
Begin VB.Form frmMessageBox 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FULLSTEP"
   ClientHeight    =   1590
   ClientLeft      =   3585
   ClientTop       =   3015
   ClientWidth     =   3885
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMessageBox.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1590
   ScaleWidth      =   3885
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txt 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   525
      Left            =   1410
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   240
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox chkConfirmar 
      Caption         =   "DConfirmar"
      Height          =   255
      Left            =   1440
      TabIndex        =   4
      Top             =   600
      Width           =   1215
   End
   Begin VB.CommandButton cmdOtro 
      Caption         =   "DAsignarse usted"
      Height          =   375
      Left            =   3250
      TabIndex        =   3
      Top             =   960
      Visible         =   0   'False
      Width           =   1400
   End
   Begin VB.CommandButton cmdNo 
      Caption         =   "No"
      Height          =   375
      Left            =   2062
      TabIndex        =   2
      Top             =   960
      Width           =   1105
   End
   Begin VB.CommandButton cmdYes 
      Caption         =   "Si"
      Height          =   375
      Left            =   837
      TabIndex        =   1
      Top             =   960
      Width           =   1105
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   3
      Left            =   120
      Picture         =   "frmMessageBox.frx":000C
      Top             =   210
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   555
      Index           =   2
      Left            =   120
      Picture         =   "frmMessageBox.frx":0102
      Top             =   210
      Width           =   585
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   1
      Left            =   120
      Picture         =   "frmMessageBox.frx":01FF
      Top             =   210
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   4
      Left            =   120
      Picture         =   "frmMessageBox.frx":02F1
      Top             =   210
      Width           =   480
   End
   Begin VB.Label lblMensaje 
      AutoSize        =   -1  'True
      Caption         =   "Mensaje"
      Height          =   195
      Left            =   750
      TabIndex        =   0
      Top             =   270
      UseMnemonic     =   0   'False
      Width           =   630
   End
End
Attribute VB_Name = "frmMessageBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_udtIcono As TipoIconoMensaje
Public g_udtBotones As TipoBotonesMensaje
Public g_sMensaje As String
Public g_sTitulo As String
Public g_bCancel As Boolean
Public g_iDefaultButton As Integer
Public g_bConfirmar As Boolean 'Se usa para el mensaje ExistenOfertasDelProceso del recalculo de ofertas (frmProce).
Public g_bTextBox As Boolean
Public g_lHeightMax As Long 'Se usa para determinar un tama�o m�ximo de altura del mensaje, en caso del que venga un tama�o dado, solo se aplicar� en caso de que el lbl
                            'exceda ese tama�o, y en ese caso sacar� el scrollbar tambi�n para poder leer todo el mensaje
Private m_iRespuesta As Integer
Private m_sSi As String
Private m_sIdiOK As String
Private vMouse As Variant

Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private m_bActivado As Boolean

Private Sub chkConfirmar_Click()
    If chkConfirmar.Value = 1 Then
        cmdYes.Enabled = True
    Else
        cmdYes.Enabled = False
    End If
End Sub

Private Sub cmdNo_Click()
    g_bCancel = True
    If g_udtBotones = YesNo Or g_udtBotones = Otro Then
        m_iRespuesta = vbNo
    End If
    Unload Me

End Sub

Private Sub cmdOtro_Click()
    g_bCancel = False
    If g_udtBotones = Otro Then
        m_iRespuesta = 10
    End If
    Unload Me
End Sub

Private Sub cmdYes_Click()
    g_bCancel = False
    If g_udtBotones = YesNo Or g_udtBotones = Otro Then
        m_iRespuesta = vbYes
    End If
    Unload Me

End Sub

Private Sub Form_Load()
Dim i As Integer
    'Posiciona el formulario en la mitad del control de la �ptima,de forma que cuando se
    'cierre este formulario no se oculte el picture de la �ptima.
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    vMouse = Screen.MousePointer
    Screen.MousePointer = vbNormal
    
    CargarRecursos
    
    If g_bConfirmar Then
        chkConfirmar.Visible = True
    Else
        chkConfirmar.Visible = False
    End If
    
    If g_sTitulo <> "" Then
        Me.caption = g_sTitulo
    End If
    
    For i = 1 To 4
        Image1(i).Visible = False
    Next
    If g_udtIcono <> TipoIconoMensaje.None Then
        Image1(g_udtIcono).Visible = True
        lblMensaje.Left = 750
    Else
        lblMensaje.Left = 120
    End If
    txt.Left = lblMensaje.Left
    lblMensaje.caption = g_sMensaje
    txt.Text = g_sMensaje
    
    If g_lHeightMax > 0 And g_lHeightMax < lblMensaje.Height Then
        lblMensaje.Height = g_lHeightMax
        g_bTextBox = True
    End If
    If g_bConfirmar Then
        lblMensaje.Height = 950
        txt.Height = lblMensaje.Height
        chkConfirmar.Top = 1050
        chkConfirmar.Left = 2700
    End If
    If MDI.Height > 2200 Then
        If lblMensaje.Height > MDI.Height - 2200 Then
            lblMensaje.Height = MDI.Height - 2200
        End If
        txt.Height = lblMensaje.Height
    Else
        If lblMensaje.Height > MDI.Height Then
            lblMensaje.Height = MDI.Height
        End If
        txt.Height = lblMensaje.Height
    End If
    If g_bTextBox Then
        txt.Visible = True
        lblMensaje.Visible = False
    Else
        txt.Visible = False
        lblMensaje.Visible = True
    End If
    

Select Case g_udtBotones
    Case TipoBotonesMensaje.None
        cmdYes.Visible = False
        cmdNo.Visible = False
        If lblMensaje.Left + lblMensaje.Width + 500 < 2800 Then
            Me.Width = 2800
        Else
            Me.Width = lblMensaje.Left + lblMensaje.Width + 500
        End If
        If lblMensaje.Top + lblMensaje.Height + 800 < 1800 Then
            Me.Height = 1800
        Else
            Me.Height = lblMensaje.Top + lblMensaje.Height + 800
        End If
    Case OKOnly
        cmdYes.Visible = True
        cmdNo.Visible = False
        cmdYes.caption = m_sIdiOK
        If lblMensaje.Left + lblMensaje.Width + 500 < 2800 Then
            Me.Width = 2800
        Else
            Me.Width = lblMensaje.Left + lblMensaje.Width + 500
        End If
        cmdYes.Left = Me.Width / 2 - cmdYes.Width / 2
        cmdYes.Top = lblMensaje.Top + lblMensaje.Height + 300
        If cmdYes.Top + cmdYes.Height + 400 < 1800 Then
            Me.Height = 1800
        Else
            Me.Height = cmdYes.Top + cmdYes.Height + 500
        End If
        cmdNo.Default = False
        cmdYes.Default = True
        
    Case YesNo
        cmdYes.Visible = True
        cmdNo.Visible = True
        cmdYes.caption = m_sSi
        If lblMensaje.Left + lblMensaje.Width + 500 < 2800 Then
            Me.Width = 2800
        Else
            Me.Width = lblMensaje.Left + lblMensaje.Width + 500
        End If
        cmdYes.Left = Me.Width / 2 - cmdYes.Width - 60
        cmdYes.Top = lblMensaje.Top + lblMensaje.Height + 300
        cmdNo.Left = cmdYes.Left + cmdYes.Width + 120
        cmdNo.Top = cmdYes.Top
        If cmdYes.Top + cmdYes.Height + 400 < 1800 Then
            Me.Height = 1800
        Else
            Me.Height = cmdYes.Top + cmdYes.Height + 500
        End If
        If g_iDefaultButton = 2 Then
            cmdYes.Default = False
            cmdNo.Default = True
            cmdNo.TabIndex = 1
            cmdYes.TabIndex = 2
        Else
            cmdNo.Default = False
            cmdYes.Default = True
        End If
    
    Case Otro
        cmdYes.Visible = True
        cmdNo.Visible = True
        cmdOtro.Visible = True
        cmdYes.caption = m_sSi
        If lblMensaje.Left + lblMensaje.Width + 500 < 2800 Then
            Me.Width = 2800
        Else
            Me.Width = lblMensaje.Left + lblMensaje.Width + 500
        End If
        cmdYes.Left = Me.Width / 2 - cmdYes.Width - 500
        cmdYes.Top = lblMensaje.Top + lblMensaje.Height + 300
        cmdNo.Left = cmdYes.Left + cmdYes.Width + 120
        cmdNo.Top = cmdYes.Top
        cmdOtro.Left = cmdNo.Left + cmdNo.Width + 120
        cmdOtro.Top = cmdYes.Top
        
        If cmdYes.Top + cmdYes.Height + 400 < 1800 Then
            Me.Height = 1900
        Else
            Me.Height = cmdYes.Top + cmdYes.Height + 500
        End If
        cmdOtro.Default = True
        cmdYes.Default = False
        cmdNo.Default = False
    End Select
    If g_bTextBox Then
        txt.Width = lblMensaje.Width + 2000
        Me.Width = Me.Width + 2000
        txt.Height = txt.Height + 500
        Me.Height = Me.Height + 500
        cmdYes.Top = cmdYes.Top + 500
        cmdOtro.Top = cmdOtro.Top + 500
        cmdNo.Top = cmdNo.Top + 500
        cmdYes.Left = cmdYes.Left + 1000
        cmdNo.Left = cmdYes.Left + cmdYes.Width + 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmMessageBox", "Load", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_MSGBOX_OPTIMA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sIdiOK = Ador(0).Value
        For i = 1 To 6
            Ador.MoveNext
        Next
        cmdYes.caption = Ador(0).Value
        m_sSi = Ador(0).Value
        Ador.MoveNext
        cmdNo.caption = Ador(0).Value
        
        Ador.MoveNext
        cmdOtro.caption = Ador(0).Value  'Asignarse usted
        
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        chkConfirmar.caption = Ador(0).Value '17 Confirmar
        
        Ador.Close
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPedidos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
            
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If g_bCancel And (g_udtBotones = YesNo Or g_udtBotones = Otro) Then
        m_iRespuesta = vbNo
    End If
    If g_udtBotones = YesNo Or g_udtBotones = Otro Then
        oMensajes.g_iRespuesta = m_iRespuesta
    End If
    g_iDefaultButton = 0
    g_sTitulo = ""
    g_lHeightMax = 0
    g_bConfirmar = False
    g_bTextBox = False
    Screen.MousePointer = vMouse
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmMensaje", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not m_bActivado Then
        m_bActivado = True
   End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmMsgBox", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

