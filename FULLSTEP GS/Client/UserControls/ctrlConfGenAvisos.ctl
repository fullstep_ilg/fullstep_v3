VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.UserControl ctrlConfGenAvisos 
   ClientHeight    =   8175
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7935
   ScaleHeight     =   8175
   ScaleWidth      =   7935
   Begin VB.Frame fraAvisoAnulacion 
      Caption         =   "DAviso a los proveedores"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   45
      TabIndex        =   32
      Top             =   1440
      Width           =   7785
      Begin VB.PictureBox picAvisoAdj 
         BorderStyle     =   0  'None
         Height          =   495
         Index           =   3
         Left            =   75
         ScaleHeight     =   495
         ScaleWidth      =   7200
         TabIndex        =   33
         Top             =   360
         Width           =   7200
         Begin VB.CheckBox chkMailValidAnulacion 
            Caption         =   "DActivar el env�o de email a los proveedores en la anulaci�n del proceso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   75
            TabIndex        =   34
            Top             =   0
            Value           =   1  'Checked
            Width           =   7365
         End
      End
   End
   Begin VB.Frame fraAvisoParticipantes 
      Caption         =   "DAvisos a los participantes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1330
      Left            =   45
      TabIndex        =   30
      Top             =   30
      Width           =   7785
      Begin VB.PictureBox picParticipantes 
         BorderStyle     =   0  'None
         Height          =   995
         Left            =   165
         ScaleHeight     =   990
         ScaleWidth      =   7200
         TabIndex        =   31
         Top             =   240
         Width           =   7200
         Begin VB.CheckBox chkMailValidAper 
            Caption         =   "DActivar el envio de email a los participantes en la validaci�n de la apertura"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   1
            Top             =   0
            Width           =   7500
         End
         Begin VB.CheckBox chkMailValidAdj 
            Caption         =   "Activar el envio de email a los participantes en la validaci�n de la adjudicaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   3
            Top             =   630
            Width           =   7500
         End
         Begin VB.CheckBox chkMailValidPreAdj 
            Caption         =   "Activar el envio de email a los participantes en el guardado de la adjudicaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   2
            Top             =   330
            Width           =   7500
         End
      End
   End
   Begin VB.Frame fraAvisoAdj 
      Caption         =   "DAviso de proximidad de fechas de Despublicaci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4650
      Index           =   1
      Left            =   60
      TabIndex        =   19
      Top             =   3375
      Width           =   7785
      Begin VB.PictureBox picAvisoAdj 
         BorderStyle     =   0  'None
         Height          =   495
         Index           =   1
         Left            =   75
         ScaleHeight     =   495
         ScaleWidth      =   7200
         TabIndex        =   22
         Top             =   660
         Width           =   7200
         Begin VB.CheckBox chkAvisoAdj 
            Caption         =   "DSolo si soy el responsable"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   75
            TabIndex        =   6
            Top             =   60
            Value           =   1  'Checked
            Width           =   3285
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnteAdj 
            Height          =   285
            Index           =   1
            Left            =   5370
            TabIndex        =   7
            Top             =   60
            Width           =   1440
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            MaxDropDownItems=   9
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "PERIODO"
            Columns(0).Name =   "PERIODO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "BUZPER"
            Columns(1).Name =   "BUZPER"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2540
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblAnteAdj 
            Caption         =   "DAntelaci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   4230
            TabIndex        =   23
            Top             =   120
            Width           =   885
         End
      End
      Begin VB.PictureBox picAvisoAdj 
         BorderStyle     =   0  'None
         Height          =   2940
         Index           =   2
         Left            =   75
         ScaleHeight     =   2940
         ScaleWidth      =   7650
         TabIndex        =   20
         Top             =   1575
         Width           =   7650
         Begin VB.Frame fraNotiPed 
            Caption         =   "DPlantillas para los avisos de despublicaci�n de procesos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1470
            Left            =   330
            TabIndex        =   26
            Top             =   1440
            Width           =   7065
            Begin VB.TextBox txtMailSubject 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1065
               MaxLength       =   255
               TabIndex        =   16
               Top             =   1035
               Width           =   5835
            End
            Begin VB.TextBox txtPlantilla 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   1065
               MaxLength       =   255
               TabIndex        =   12
               Top             =   300
               Width           =   5475
            End
            Begin VB.CommandButton cmdPlantilla 
               Height          =   285
               Index           =   0
               Left            =   6585
               Picture         =   "ctrlConfGenAvisos.ctx":0000
               Style           =   1  'Graphical
               TabIndex        =   13
               Top             =   300
               Width           =   315
            End
            Begin VB.TextBox txtPlantilla 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   1065
               MaxLength       =   255
               TabIndex        =   14
               Top             =   660
               Width           =   5475
            End
            Begin VB.CommandButton cmdPlantilla 
               Height          =   285
               Index           =   1
               Left            =   6585
               Picture         =   "ctrlConfGenAvisos.ctx":00BF
               Style           =   1  'Graphical
               TabIndex        =   15
               Top             =   660
               Width           =   315
            End
            Begin VB.Label lblAsunto 
               Caption         =   "DAsunto:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Left            =   120
               TabIndex        =   29
               Top             =   1072
               Width           =   945
            End
            Begin VB.Label LblCartaPed 
               Caption         =   "DTXT:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   28
               Top             =   705
               Width           =   1335
            End
            Begin VB.Label LblCartaPed 
               Caption         =   "DHTML:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   27
               Top             =   315
               Width           =   1125
            End
         End
         Begin VB.CheckBox chkAvisoAdj 
            Caption         =   "DActivar envio de emails a los proveedores"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   75
            TabIndex        =   8
            Top             =   180
            Value           =   1  'Checked
            Width           =   4095
         End
         Begin VB.CheckBox chkAvisoAdj 
            Caption         =   "DNotificar solo a los que todav�a no hayan ofertado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   735
            TabIndex        =   10
            Top             =   615
            Value           =   1  'Checked
            Width           =   4095
         End
         Begin VB.CheckBox chkAvisoAdj 
            Caption         =   "DExcluir a los que hayan comunicado que no van a ofertar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   4
            Left            =   735
            TabIndex        =   11
            Top             =   975
            Value           =   1  'Checked
            Width           =   6015
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnteAdj 
            Height          =   285
            Index           =   2
            Left            =   5370
            TabIndex        =   9
            Top             =   180
            Width           =   1440
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            MaxDropDownItems=   9
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "PERIODO"
            Columns(0).Name =   "PERIODO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "BUZPER"
            Columns(1).Name =   "BUZPER"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   2
            Columns(1).FieldLen=   256
            _ExtentX        =   2540
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblAnteAdj 
            Caption         =   "Antelaci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   2
            Left            =   4230
            TabIndex        =   21
            Top             =   255
            Width           =   1050
         End
      End
      Begin VB.Line Line6 
         Index           =   1
         X1              =   120
         X2              =   7100
         Y1              =   495
         Y2              =   495
      End
      Begin VB.Label lblAviso 
         Caption         =   "Avisos en el visor de GS:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   135
         TabIndex        =   25
         Top             =   285
         Width           =   6180
      End
      Begin VB.Label lblAviso 
         Caption         =   "Avisos a los proveedores con procesos publicados:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   135
         TabIndex        =   24
         Top             =   1305
         Width           =   6180
      End
      Begin VB.Line Line6 
         Index           =   2
         X1              =   120
         X2              =   6980
         Y1              =   1530
         Y2              =   1530
      End
   End
   Begin VB.Frame fraAvisoAdj 
      Caption         =   "DAviso de proximidad de fechas de adjudicaci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   0
      Left            =   45
      TabIndex        =   0
      Top             =   2430
      Width           =   7785
      Begin VB.PictureBox picAvisoAdj 
         BorderStyle     =   0  'None
         Height          =   495
         Index           =   0
         Left            =   75
         ScaleHeight     =   495
         ScaleWidth      =   7200
         TabIndex        =   17
         Top             =   300
         Width           =   7200
         Begin VB.CheckBox chkAvisoAdj 
            Caption         =   "DSolo si soy el responsable"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   75
            TabIndex        =   4
            Top             =   60
            Value           =   1  'Checked
            Width           =   3285
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnteAdj 
            Height          =   285
            Index           =   0
            Left            =   5370
            TabIndex        =   5
            Top             =   60
            Width           =   1440
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            MaxDropDownItems=   9
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "PERIODO"
            Columns(0).Name =   "PERIODO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "BUZPER"
            Columns(1).Name =   "BUZPER"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2540
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblAnteAdj 
            Caption         =   "DAntelaci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   4230
            TabIndex        =   18
            Top             =   120
            Width           =   885
         End
      End
   End
End
Attribute VB_Name = "ctrlConfGenAvisos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Event AbrirDialogoArchivo(Index As Integer)

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String

Private mbModoConsulta As Boolean
Private mbGeneral As Boolean
Private sDias As String
Private sDia As String
Private sMes As String
Private sMeses As String
Private sSemana As String
Private sSemanas As String

Public Property Get General() As Boolean
    General = mbGeneral
End Property
Public Property Let General(bgeneral As Boolean)

    mbGeneral = bgeneral
    SyncTipo
    PropertyChanged "General"

End Property
Public Property Get fraPartic() As Object
    Set fraPartic = fraAvisoParticipantes
End Property
Public Property Get fraAdj0() As Object
    Set fraAdj0 = fraAvisoAdj(0)
End Property
Public Property Get fraAdj1() As Object
    Set fraAdj1 = fraAvisoAdj(1)
End Property

Property Get AvisoAdjSoloSiResp() As Boolean
    AvisoAdjSoloSiResp = (chkAvisoAdj(0).Value = vbChecked)
End Property
Property Let AvisoAdjSoloSiResp(ByVal bValor As Boolean)
    If bValor Then
        chkAvisoAdj(0).Value = vbChecked
    Else
        chkAvisoAdj(0).Value = vbUnchecked
    End If
End Property

Property Get AvisoVisorGSSoloSiResp() As Boolean
    AvisoVisorGSSoloSiResp = (chkAvisoAdj(1).Value = vbChecked)
End Property
Property Let AvisoVisorGSSoloSiResp(ByVal bValor As Boolean)
    If bValor Then
        chkAvisoAdj(1).Value = vbChecked
    Else
        chkAvisoAdj(1).Value = vbUnchecked
    End If
End Property


Property Get AvisoProveDespub() As Boolean
    AvisoProveDespub = (chkAvisoAdj(2).Value = vbChecked)
End Property
Property Let AvisoProveDespub(ByVal bValor As Boolean)
    If bValor Then
        chkAvisoAdj(2).Value = vbChecked
    Else
        chkAvisoAdj(2).Value = vbUnchecked
    End If
End Property


Property Get AvisoNoHanOfertado() As Boolean
    AvisoNoHanOfertado = (chkAvisoAdj(3).Value = vbChecked)
End Property
Property Let AvisoNoHanOfertado(ByVal bValor As Boolean)
    If bValor Then
        chkAvisoAdj(3).Value = vbChecked
    Else
        chkAvisoAdj(3).Value = vbUnchecked
    End If
End Property


Property Get AvisoNoVanOfertar() As Boolean
    AvisoNoVanOfertar = (chkAvisoAdj(4).Value = vbChecked)
End Property
Property Let AvisoNoVanOfertar(ByVal bValor As Boolean)
    If bValor Then
        chkAvisoAdj(4).Value = vbChecked
    Else
        chkAvisoAdj(4).Value = vbUnchecked
    End If
End Property
Property Get EnviarEmailApertura() As Boolean
    EnviarEmailApertura = (chkMailValidAper.Value = vbChecked)
End Property
Property Let EnviarEmailApertura(ByVal bValor As Boolean)
    If bValor Then
        chkMailValidAper.Value = vbChecked
    Else
        chkMailValidAper.Value = vbUnchecked
    End If
End Property
Property Get EnviarEmailPreAdj() As Boolean
    EnviarEmailPreAdj = (chkMailValidPreAdj.Value = vbChecked)
End Property
Property Let EnviarEmailPreAdj(ByVal bValor As Boolean)
    If bValor Then
        chkMailValidPreAdj.Value = vbChecked
    Else
        chkMailValidPreAdj.Value = vbUnchecked
    End If
End Property
Property Get EnviarEmailAdj() As Boolean
    EnviarEmailAdj = (chkMailValidAdj.Value = vbChecked)
End Property
Property Let EnviarEmailAdj(ByVal bValor As Boolean)
    If bValor Then
        chkMailValidAdj.Value = vbChecked
    Else
        chkMailValidAdj.Value = vbUnchecked
    End If
End Property
Property Get EnviarEmailAnulacion() As Boolean
    EnviarEmailAnulacion = (chkMailValidAnulacion.Value = vbChecked)
End Property
Property Let EnviarEmailAnulacion(ByVal bValor As Boolean)
    If bValor Then
        chkMailValidAnulacion.Value = vbChecked
    Else
        chkMailValidAnulacion.Value = vbUnchecked
    End If
End Property

Property Let AntelacionAdjudicacion(ByVal iValor As Integer)
Dim i As Integer
Dim bm As Variant

sdbcAnteAdj(0).MoveFirst

For i = 0 To sdbcAnteAdj(0).Rows - 1
    bm = sdbcAnteAdj(0).GetBookmark(i)
    If iValor = sdbcAnteAdj(0).Columns(1).Value Then
        sdbcAnteAdj(0).Text = sdbcAnteAdj(0).Columns(0).Value
        Exit For
    End If
    sdbcAnteAdj(0).MoveNext
Next i

End Property
Property Get AntelacionAdjudicacion() As Integer
    If IsNumeric(sdbcAnteAdj(0).Columns(1).Value) And sdbcAnteAdj(0).Text <> "" Then
        AntelacionAdjudicacion = sdbcAnteAdj(0).Columns(1).Value
    Else
        AntelacionAdjudicacion = 0
    End If
End Property

Property Let AntelacionVisorGS(ByVal iValor As Integer)
Dim i As Integer
Dim bm As Variant

sdbcAnteAdj(1).MoveFirst

For i = 0 To sdbcAnteAdj(1).Rows - 1
    bm = sdbcAnteAdj(1).GetBookmark(i)
    If iValor = sdbcAnteAdj(1).Columns(1).Value Then
        sdbcAnteAdj(1).Text = sdbcAnteAdj(1).Columns(0).Value
        Exit For
    End If
    sdbcAnteAdj(1).MoveNext
Next i

End Property

Property Get AntelacionVisorGS() As Integer
    If IsNumeric(sdbcAnteAdj(1).Columns(1).Value) And sdbcAnteAdj(1).Text <> "" Then
        AntelacionVisorGS = sdbcAnteAdj(1).Columns(1).Value
    Else
        AntelacionVisorGS = 0
    End If
End Property


Property Let AntelacionDespublicacion(ByVal iValor As Integer)
Dim i As Integer
Dim bm As Variant

sdbcAnteAdj(2).MoveFirst

For i = 0 To sdbcAnteAdj(2).Rows - 1
    bm = sdbcAnteAdj(2).GetBookmark(i)
    If iValor = sdbcAnteAdj(2).Columns(1).Value Then
        sdbcAnteAdj(2).Text = sdbcAnteAdj(2).Columns(0).Value
        Exit For
    End If
    sdbcAnteAdj(2).MoveNext
Next i

End Property


Property Get AntelacionDespublicacion() As Integer
    If IsNumeric(sdbcAnteAdj(2).Columns(1).Value) And sdbcAnteAdj(2).Text <> "" Then
        AntelacionDespublicacion = sdbcAnteAdj(2).Columns(1).Value
    Else
        AntelacionDespublicacion = 0
    End If
End Property

Property Let PlantillaDespub(ByVal Index As Integer, ByVal sTemplate As String)
UserControl.txtPlantilla(Index).Text = sTemplate
End Property
Property Get PlantillaDespub(ByVal Index As Integer) As String
PlantillaDespub = UserControl.txtPlantilla(Index).Text
End Property


Property Let AsuntoAvisoDespub(ByVal sAsunto As String)
    UserControl.txtMailSubject.Text = sAsunto
End Property
Property Get AsuntoAvisoDespub() As String
    AsuntoAvisoDespub = UserControl.txtMailSubject.Text
End Property

Property Get ModoConsulta() As Boolean
    ModoConsulta = mbModoConsulta
End Property

Property Let ModoConsulta(ByVal bModo As Boolean)
    mbModoConsulta = bModo
    
    picAvisoAdj(0).Enabled = Not bModo
    picAvisoAdj(1).Enabled = Not bModo
    picAvisoAdj(2).Enabled = Not bModo
    picAvisoAdj(3).Enabled = Not bModo
    picParticipantes.Enabled = Not bModo
End Property

Private Sub cmdPlantilla_Click(Index As Integer)
    RaiseEvent AbrirDialogoArchivo(Index)
End Sub

Private Sub sdbcAnteAdj_Validate(Index As Integer, Cancel As Boolean)
    Dim bm0 As Variant
    Dim bExiste0 As Boolean
    Dim i0 As Integer
    Dim iFila0 As Integer
    Dim bm1 As Variant
    Dim bExiste1 As Boolean
    Dim i1 As Integer
    Dim iFila1 As Integer
    Dim bm2 As Variant
    Dim bExiste2 As Boolean
    Dim i2 As Integer
    Dim iFila2 As Integer
    
    If Index = 0 Then
        sdbcAnteAdj(0).SelBookmarks.Add sdbcAnteAdj(0).Bookmark
        iFila0 = sdbcAnteAdj(0).AddItemRowIndex(sdbcAnteAdj(0).Bookmark)
        sdbcAnteAdj(0).SelBookmarks.RemoveAll
        
        If Not mbModoConsulta Then
            'Estamos en modo consulta
    
            bExiste0 = False
            sdbcAnteAdj(0).MoveFirst
            
            For i0 = 0 To sdbcAnteAdj(0).Rows - 1
                    bm0 = sdbcAnteAdj(0).GetBookmark(i0)
                    If sdbcAnteAdj(0).Text = sdbcAnteAdj(0).Columns(0).Value Then
                        bExiste0 = True
                        sdbcAnteAdj(0).Bookmark = bm0
                        Exit For
                    End If
                    sdbcAnteAdj(0).MoveNext
            Next i0
            
            If Not bExiste0 Then
                sdbcAnteAdj(0).Text = ""
                chkAvisoAdj(0).Value = vbUnchecked
            Else
                sdbcAnteAdj(0).MoveFirst
                sdbcAnteAdj(0).MoveRecords (iFila0)
                sdbcAnteAdj(0).Columns(0).Value = sdbcAnteAdj(0).Text
            End If
    
        End If
    End If
    
    If Index = 1 Then
        sdbcAnteAdj(1).SelBookmarks.Add sdbcAnteAdj(1).Bookmark
        iFila1 = sdbcAnteAdj(1).AddItemRowIndex(sdbcAnteAdj(1).Bookmark)
        sdbcAnteAdj(1).SelBookmarks.RemoveAll
        
        If Not mbModoConsulta Then
            'Estamos en modo consulta
    
            bExiste1 = False
            sdbcAnteAdj(1).MoveFirst
            
            For i1 = 0 To sdbcAnteAdj(1).Rows - 1
                    bm1 = sdbcAnteAdj(1).GetBookmark(i1)
                    If sdbcAnteAdj(1).Text = sdbcAnteAdj(1).Columns(0).Value Then
                        bExiste1 = True
                        sdbcAnteAdj(1).Bookmark = bm1
                        Exit For
                    End If
                    sdbcAnteAdj(1).MoveNext
            Next i1
            
            If Not bExiste1 Then
                sdbcAnteAdj(1).Text = ""
                chkAvisoAdj(1).Value = vbUnchecked
            Else
                sdbcAnteAdj(1).MoveFirst
                sdbcAnteAdj(1).MoveRecords (iFila1)
                sdbcAnteAdj(1).Columns(0).Value = sdbcAnteAdj(1).Text
            End If
    
        End If
    End If
    If Index = 2 Then
        sdbcAnteAdj(2).SelBookmarks.Add sdbcAnteAdj(2).Bookmark
        iFila2 = sdbcAnteAdj(2).AddItemRowIndex(sdbcAnteAdj(2).Bookmark)
        sdbcAnteAdj(2).SelBookmarks.RemoveAll
        
        If Not mbModoConsulta Then
            'Estamos en modo consulta
    
            bExiste2 = False
            sdbcAnteAdj(2).MoveFirst
            
            For i2 = 0 To sdbcAnteAdj(2).Rows - 1
                    bm1 = sdbcAnteAdj(2).GetBookmark(i2)
                    If sdbcAnteAdj(2).Text = sdbcAnteAdj(2).Columns(0).Value Then
                        bExiste2 = True
                        sdbcAnteAdj(2).Bookmark = bm2
                        Exit For
                    End If
                    sdbcAnteAdj(2).MoveNext
            Next i2
            
            If Not bExiste2 Then
                sdbcAnteAdj(2).Text = ""
                chkAvisoAdj(2).Value = vbUnchecked
                chkAvisoAdj(3).Value = vbUnchecked
                chkAvisoAdj(4).Value = vbUnchecked
            Else
                sdbcAnteAdj(2).MoveFirst
                sdbcAnteAdj(2).MoveRecords (iFila2)
                sdbcAnteAdj(2).Columns(0).Value = sdbcAnteAdj(2).Text
            End If
    
        End If
    End If
End Sub

Private Sub chkAvisoAdj_Click(Index As Integer)
    If Index = 0 Then
        If chkAvisoAdj(0).Value = vbChecked Then
            sdbcAnteAdj(0).Bookmark = 9
            sdbcAnteAdj(0).Text = sdbcAnteAdj(0).Columns(0).Value
        End If
    End If
    If Index = 1 Then
        If chkAvisoAdj(1).Value = vbChecked Then
            sdbcAnteAdj(1).Bookmark = 9
            sdbcAnteAdj(1).Text = sdbcAnteAdj(1).Columns(0).Value
        End If
    End If
    If Index = 2 Then
        If chkAvisoAdj(2).Value = vbChecked Then
            sdbcAnteAdj(2).Bookmark = 9
            sdbcAnteAdj(2).Text = sdbcAnteAdj(2).Columns(0).Value
        End If
    End If
End Sub

Public Sub Initialize()
    Dim i As Integer
    Dim iBuzper As Integer
        
    CargarRecursos
    'Rellenar combo de Antig�edad de las Adjudicaciones
    With sdbcAnteAdj(0)
        i = 1
        .AddItem i & " " & sDia & Chr(9) & i
        For i = 2 To 6
           .AddItem i & " " & sDias & Chr(9) & i
        Next i
        iBuzper = 7
        i = 1
        .AddItem i & " " & sSemana & Chr(9) & iBuzper
        For i = 2 To 3
           .AddItem i & " " & sSemanas & Chr(9) & iBuzper * i
        Next i

        iBuzper = 30
        i = 1
        .AddItem i & " " & sMes & Chr(9) & iBuzper
        For i = 2 To 12
            .AddItem i & " " & sMeses & Chr(9) & iBuzper * i
        Next i
    End With

    'Rellenar combo de Antig�edad de las Despublicaciones
    With sdbcAnteAdj(1)
        i = 1
        .AddItem i & " " & sDia & Chr(9) & i
        For i = 2 To 6
           .AddItem i & " " & sDias & Chr(9) & i
        Next i
        iBuzper = 7
        i = 1
        .AddItem i & " " & sSemana & Chr(9) & iBuzper
        For i = 2 To 3
           .AddItem i & " " & sSemanas & Chr(9) & iBuzper * i
        Next i

        iBuzper = 30
        i = 1
        .AddItem i & " " & sMes & Chr(9) & iBuzper
        For i = 2 To 12
            .AddItem i & " " & sMeses & Chr(9) & iBuzper * i
        Next i
    End With
   
    With sdbcAnteAdj(2)
        i = 1
        .AddItem i & " " & sDia & Chr(9) & i
        For i = 2 To 6
           .AddItem i & " " & sDias & Chr(9) & i
        Next i
        iBuzper = 7
        i = 1
        .AddItem i & " " & sSemana & Chr(9) & iBuzper
        For i = 2 To 3
           .AddItem i & " " & sSemanas & Chr(9) & iBuzper * i
        Next i

        iBuzper = 30
        i = 1
        .AddItem i & " " & sMes & Chr(9) & iBuzper
        For i = 2 To 12
            .AddItem i & " " & sMeses & Chr(9) & iBuzper * i
        Next i
    End With
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    On Error Resume Next

    Set Ador = GestorIdiomas.DevolverTextosDelModulo(CTRL_CONFGENAVISOS, Idioma)

    fraAvisoAdj(0).Caption = Ador(0).Value
    Ador.MoveNext
    chkAvisoAdj(0).Caption = Ador(0).Value
    chkAvisoAdj(1).Caption = Ador(0).Value
    Ador.MoveNext
    lblAnteAdj(0).Caption = Ador(0).Value '250
    lblAnteAdj(1).Caption = Ador(0).Value
    lblAnteAdj(2).Caption = Ador(0).Value
    Ador.MoveNext
    fraAvisoAdj(1).Caption = Ador(0).Value
    Ador.MoveNext
    UserControl.lblAviso(0).Caption = Ador(0).Value
    Ador.MoveNext
    UserControl.lblAviso(1).Caption = Ador(0).Value
    Ador.MoveNext
    UserControl.chkAvisoAdj(2).Caption = Ador(0).Value
    Ador.MoveNext
    UserControl.chkAvisoAdj(3).Caption = Ador(0).Value
    Ador.MoveNext
    UserControl.chkAvisoAdj(4).Caption = Ador(0).Value
    Ador.MoveNext

    sDia = Ador(0).Value
    Ador.MoveNext
    sDias = Ador(0).Value
    Ador.MoveNext
    sMes = Ador(0).Value
    Ador.MoveNext
    sMeses = Ador(0).Value
    Ador.MoveNext
    sSemana = Ador(0).Value
    Ador.MoveNext
    sSemanas = Ador(0).Value
    Ador.MoveNext
    
    UserControl.fraNotiPed.Caption = Ador(0).Value
    Ador.MoveNext
    UserControl.LblCartaPed(0).Caption = Ador(0).Value
    Ador.MoveNext
    UserControl.LblCartaPed(1).Caption = Ador(0).Value
    Ador.MoveNext
    lblAsunto.Caption = Ador(0).Value
    Ador.MoveNext
    fraAvisoParticipantes.Caption = Ador(0).Value   'Modulo=400, ID=20
    Ador.MoveNext
    chkMailValidAper.Caption = Ador(0).Value        'Modulo=400, ID=21
    Ador.MoveNext
    chkMailValidPreAdj.Caption = Ador(0).Value      'Modulo=400, ID=22
    Ador.MoveNext
    chkMailValidAdj.Caption = Ador(0).Value         'Modulo=400, ID=23
    Ador.MoveNext
    fraAvisoAnulacion.Caption = Ador(0).Value
    Ador.MoveNext
    chkMailValidAnulacion.Caption = Ador(0).Value
        
    Ador.Close
    Set Ador = Nothing
End Sub

Private Sub UserControl_InitProperties()
    SyncTipo
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    On Error Resume Next
    General = PropBag.ReadProperty("General")
    SyncTipo
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    PropBag.WriteProperty "General", General
End Sub

Private Sub SyncTipo()
    If Not mbGeneral Then
        chkAvisoAdj(2).Visible = False
        chkAvisoAdj(3).Visible = False
        chkAvisoAdj(4).Visible = False
        lblAnteAdj(2).Visible = False
        sdbcAnteAdj(2).Visible = False
        fraNotiPed.Top = UserControl.chkAvisoAdj(2).Top
        picAvisoAdj(2).Height = 1700
        fraAvisoAdj(1).Height = 3365
        
        fraAvisoAnulacion.Visible = False
        fraAvisoAdj(0).Top = fraAvisoAnulacion.Top
        fraAvisoAdj(1).Top = fraAvisoAdj(0).Top + fraAvisoAdj(0).Height + 40
    Else
        chkAvisoAdj(2).Visible = True
        chkAvisoAdj(3).Visible = True
        chkAvisoAdj(4).Visible = True
        lblAnteAdj(2).Visible = True
        sdbcAnteAdj(2).Visible = True
        picAvisoAdj(2).Height = 2940
        fraAvisoAdj(1).Height = 4665
        fraNotiPed.Top = 1440
    End If
End Sub
