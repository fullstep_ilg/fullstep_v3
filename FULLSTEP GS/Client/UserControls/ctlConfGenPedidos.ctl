VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.UserControl ctlConfGenPedidos 
   ClientHeight    =   8730
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9240
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   ScaleHeight     =   8730
   ScaleWidth      =   9240
   Begin MSComDlg.CommonDialog cdlArchivo 
      Left            =   360
      Top             =   8760
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "DSeleccione plantilla"
      Filter          =   "Plantillas de Word (*.dot)|*.dot"
   End
   Begin TabDlg.SSTab tabPedidos 
      Height          =   8640
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   15240
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "DPedidos"
      TabPicture(0)   =   "ctlConfGenPedidos.ctx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "picPedidos(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "DCat�logo"
      TabPicture(1)   =   "ctlConfGenPedidos.ctx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picPedidos(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Recepciones"
      TabPicture(2)   =   "ctlConfGenPedidos.ctx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "picPedidos(9)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Plantillas"
      TabPicture(3)   =   "ctlConfGenPedidos.ctx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "TabPlantPedidos"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "DComunicaci�n Pedidos"
      TabPicture(4)   =   "ctlConfGenPedidos.ctx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "ctlConfPedCom"
      Tab(4).ControlCount=   1
      Begin VB.PictureBox picPedidos 
         BorderStyle     =   0  'None
         FillColor       =   &H00C0C0C0&
         Height          =   8250
         Index           =   0
         Left            =   180
         ScaleHeight     =   8250
         ScaleWidth      =   8925
         TabIndex        =   27
         Top             =   360
         Width           =   8925
         Begin VB.Frame fraAprovisionamiento 
            Height          =   2790
            Index           =   1
            Left            =   30
            TabIndex        =   66
            Top             =   4860
            Width           =   8850
            Begin VB.CheckBox chkOblPedido 
               Caption         =   "DObligar a pedir s�lo art�culos homologados "
               Height          =   255
               Index           =   0
               Left            =   270
               TabIndex        =   78
               Top             =   165
               Width           =   6600
            End
            Begin VB.PictureBox picPedidos 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   1995
               Index           =   10
               Left            =   60
               ScaleHeight     =   1995
               ScaleWidth      =   8745
               TabIndex        =   68
               Top             =   750
               Width           =   8745
               Begin VB.CheckBox chkHitosFacturacion 
                  Caption         =   "DUsar hitos de facturaci�n"
                  Height          =   255
                  Left            =   210
                  TabIndex        =   152
                  Top             =   1680
                  Width           =   4080
               End
               Begin VB.CheckBox chkOblPedido 
                  Caption         =   "DEstablecer direcci�n de entrega a nivel de cabecera de pedido"
                  Height          =   255
                  Index           =   2
                  Left            =   210
                  TabIndex        =   74
                  Top             =   285
                  Width           =   6600
               End
               Begin VB.CheckBox chkOblPedido 
                  Caption         =   "DDesglosar la emisi�n de pedidos por centro de aprovisionamiento"
                  Height          =   255
                  Index           =   3
                  Left            =   210
                  TabIndex        =   73
                  Top             =   1140
                  Width           =   6600
               End
               Begin VB.CheckBox chkOblPedido 
                  Caption         =   "DPermitir realizar pedidos abiertos"
                  Height          =   255
                  Index           =   4
                  Left            =   210
                  TabIndex        =   72
                  Top             =   570
                  Width           =   4140
               End
               Begin VB.TextBox txtPedidoAbierto 
                  Height          =   285
                  Index           =   0
                  Left            =   7335
                  TabIndex        =   71
                  Top             =   600
                  Width           =   1250
               End
               Begin VB.CheckBox chkOblPedido 
                  Caption         =   "DIncluir v�as de pago en el pedido"
                  Height          =   255
                  Index           =   5
                  Left            =   210
                  TabIndex        =   70
                  Top             =   870
                  Width           =   4180
               End
               Begin VB.CheckBox chkOblPedido 
                  Caption         =   "DObligar a pedir seg�n la distribuci�n de la compra"
                  Height          =   255
                  Index           =   1
                  Left            =   210
                  TabIndex        =   69
                  Top             =   1410
                  Width           =   4080
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
                  Height          =   285
                  Index           =   5
                  Left            =   5235
                  TabIndex        =   75
                  Top             =   600
                  Width           =   2070
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   3651
                  _ExtentY        =   503
                  _StockProps     =   93
                  ForeColor       =   -2147483630
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblIdiPers 
                  Caption         =   "DIdioma"
                  Height          =   195
                  Index           =   4
                  Left            =   4440
                  TabIndex        =   77
                  Top             =   630
                  Width           =   1155
               End
               Begin VB.Label label2 
                  Caption         =   "DS�lo para pedidos emitidos desde GS"
                  Height          =   195
                  Index           =   27
                  Left            =   90
                  TabIndex        =   76
                  Top             =   30
                  Width           =   4200
               End
            End
            Begin VB.CheckBox chkOblPedido 
               Caption         =   "DObligar a indicar la fecha de entrega solicitada"
               Height          =   255
               Index           =   6
               Left            =   270
               TabIndex        =   67
               Top             =   450
               Width           =   6600
            End
         End
         Begin VB.Frame fraAprovisionamiento 
            Height          =   480
            Index           =   5
            Left            =   30
            TabIndex        =   64
            Top             =   7665
            Width           =   8850
            Begin VB.CheckBox chkRegGest 
               Caption         =   "DActivar el Pedido Abierto"
               Height          =   255
               Index           =   2
               Left            =   270
               TabIndex        =   65
               Top             =   150
               Width           =   3945
            End
         End
         Begin VB.Frame fraAprovisionamiento 
            Enabled         =   0   'False
            Height          =   1725
            Index           =   4
            Left            =   30
            TabIndex        =   53
            Top             =   60
            Width           =   8850
            Begin VB.CheckBox chkAprovisionamiento 
               Caption         =   "DHabilitar campo para codificaci�n personalizada de pedidos de aprovisionamiento"
               Height          =   255
               Index           =   2
               Left            =   225
               TabIndex        =   59
               Top             =   660
               Width           =   6675
            End
            Begin VB.CheckBox chkAprovisionamiento 
               Caption         =   "DHabilitar campo para codificaci�n personalizada de pedidos directos"
               Height          =   255
               Index           =   3
               Left            =   225
               TabIndex        =   58
               Top             =   120
               Width           =   6675
            End
            Begin VB.TextBox txtNombreAprov 
               Enabled         =   0   'False
               Height          =   285
               Index           =   0
               Left            =   6255
               TabIndex        =   57
               Top             =   930
               Width           =   2445
            End
            Begin VB.TextBox txtNombreAprov 
               Enabled         =   0   'False
               Height          =   285
               Index           =   2
               Left            =   6255
               TabIndex        =   56
               Top             =   1275
               Width           =   2445
            End
            Begin VB.CheckBox chkAprovisionamiento 
               Caption         =   "DObligatoriedad del campo para codificaci�n personalizada de pedidos de aprovisionamiento  "
               Height          =   255
               Index           =   4
               Left            =   480
               TabIndex        =   55
               Top             =   360
               Width           =   7755
            End
            Begin VB.CheckBox chkAprovisionamiento 
               Caption         =   "DBloquear edici�n"
               Height          =   255
               Index           =   1
               Left            =   210
               TabIndex        =   54
               Top             =   1380
               Width           =   2955
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
               Height          =   285
               Index           =   4
               Left            =   870
               TabIndex        =   60
               Top             =   990
               Width           =   2115
               DataFieldList   =   "Column 0"
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   -2147483630
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3731
               Columns(0).Caption=   "DEN"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "ID"
               Columns(1).Name =   "ID"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "OFFSET"
               Columns(2).Name =   "OFFSET"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   3731
               _ExtentY        =   503
               _StockProps     =   93
               ForeColor       =   -2147483630
               BackColor       =   -2147483643
               Enabled         =   0   'False
            End
            Begin VB.Label label2 
               Caption         =   "Nombre a visualizar en el Portal:"
               Height          =   195
               Index           =   11
               Left            =   3600
               TabIndex        =   63
               Top             =   1305
               Width           =   2640
            End
            Begin VB.Label label2 
               Caption         =   "Nombre a visualizar en GS y EP:"
               Height          =   195
               Index           =   10
               Left            =   3600
               TabIndex        =   62
               Top             =   975
               Width           =   2640
            End
            Begin VB.Label label2 
               Caption         =   "Idioma:"
               Height          =   195
               Index           =   9
               Left            =   225
               TabIndex        =   61
               Top             =   1050
               Width           =   615
            End
         End
         Begin VB.Frame fraAprovisionamiento 
            Height          =   3045
            Index           =   2
            Left            =   30
            TabIndex        =   28
            Top             =   1800
            Width           =   8850
            Begin VB.VScrollBar VScroll 
               Height          =   1125
               Left            =   8580
               TabIndex        =   52
               Top             =   1830
               Width           =   255
            End
            Begin VB.Frame Frame11 
               BorderStyle     =   0  'None
               Height          =   1065
               Index           =   2
               Left            =   240
               TabIndex        =   38
               Top             =   1860
               Width           =   8325
               Begin VB.Frame fraPedPres 
                  BorderStyle     =   0  'None
                  Height          =   2085
                  Left            =   -100
                  TabIndex        =   39
                  Top             =   0
                  Width           =   8085
                  Begin VB.CheckBox chkRegGest 
                     Caption         =   "DObligar a indicar unidad presupuestaria concepto 4, al nivel"
                     Height          =   255
                     Index           =   16
                     Left            =   135
                     TabIndex        =   47
                     Top             =   1560
                     Width           =   6690
                  End
                  Begin VB.CheckBox chkRegGest 
                     Caption         =   "DObligar a indicar unidad presupuestaria concepto 3, al nivel"
                     Height          =   255
                     Index           =   15
                     Left            =   135
                     TabIndex        =   46
                     Top             =   1040
                     Width           =   6690
                  End
                  Begin VB.ComboBox cmbOblDist 
                     Height          =   315
                     Index           =   8
                     IntegralHeight  =   0   'False
                     Left            =   6930
                     Style           =   2  'Dropdown List
                     TabIndex        =   45
                     Top             =   1740
                     Width           =   855
                  End
                  Begin VB.ComboBox cmbOblDist 
                     Height          =   315
                     Index           =   7
                     IntegralHeight  =   0   'False
                     Left            =   6930
                     Style           =   2  'Dropdown List
                     TabIndex        =   44
                     Top             =   1220
                     Width           =   855
                  End
                  Begin VB.ComboBox cmbOblDist 
                     Height          =   315
                     Index           =   6
                     IntegralHeight  =   0   'False
                     Left            =   6930
                     Style           =   2  'Dropdown List
                     TabIndex        =   43
                     Top             =   700
                     Width           =   855
                  End
                  Begin VB.CheckBox chkRegGest 
                     Caption         =   "DObligar a indicar unidad presupuestaria contable, al nivel"
                     Height          =   255
                     Index           =   14
                     Left            =   135
                     TabIndex        =   42
                     Top             =   520
                     Width           =   6645
                  End
                  Begin VB.ComboBox cmbOblDist 
                     Height          =   315
                     Index           =   5
                     Left            =   6930
                     Style           =   2  'Dropdown List
                     TabIndex        =   41
                     Top             =   180
                     Width           =   855
                  End
                  Begin VB.CheckBox chkRegGest 
                     Caption         =   "DObligar a indicar unidad presupuestaria de proyecto, al nivel"
                     Height          =   255
                     Index           =   13
                     Left            =   135
                     TabIndex        =   40
                     Top             =   0
                     Width           =   6675
                  End
                  Begin VB.Label lblOblDist 
                     Caption         =   "Nivel m�nimo de asignaci�n presupuestaria de proyecto"
                     Height          =   255
                     Index           =   5
                     Left            =   405
                     TabIndex        =   51
                     Top             =   275
                     Width           =   6315
                  End
                  Begin VB.Label lblOblDist 
                     Caption         =   "Nivel m�nimo de asignaci�n presupuestaria de contable"
                     Height          =   255
                     Index           =   6
                     Left            =   405
                     TabIndex        =   50
                     Top             =   795
                     Width           =   6315
                  End
                  Begin VB.Label lblOblDist 
                     Caption         =   "Nivel m�nimo de asignaci�n presupuestaria de concepto 3"
                     Height          =   255
                     Index           =   7
                     Left            =   405
                     TabIndex        =   49
                     Top             =   1315
                     Width           =   6315
                  End
                  Begin VB.Label lblOblDist 
                     Caption         =   "Nivel m�nimo de asignaci�n presupuestaria de concepto 4"
                     Height          =   255
                     Index           =   8
                     Left            =   405
                     TabIndex        =   48
                     Top             =   1835
                     Width           =   6315
                  End
               End
            End
            Begin VB.PictureBox picPedidos 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   1635
               Index           =   11
               Left            =   270
               ScaleHeight     =   1635
               ScaleWidth      =   8445
               TabIndex        =   29
               Top             =   180
               Width           =   8445
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "Sincronizar los presupuestos de los items con los presupuestos de las l�neas de pedido"
                  Height          =   255
                  Index           =   22
                  Left            =   0
                  TabIndex        =   36
                  Top             =   1080
                  Width           =   6690
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "Usar los presupuestos de los �tems como valores por defecto para las l�neas de pedido"
                  Height          =   255
                  Index           =   21
                  Left            =   0
                  TabIndex        =   35
                  Top             =   795
                  Width           =   6690
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "Presupuesto 3"
                  Height          =   255
                  Index           =   19
                  Left            =   0
                  TabIndex        =   34
                  Top             =   510
                  Width           =   3945
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "Presupuesto 4"
                  Height          =   255
                  Index           =   20
                  Left            =   4050
                  TabIndex        =   33
                  Top             =   495
                  Width           =   3945
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "Presupuesto 1"
                  Height          =   255
                  Index           =   17
                  Left            =   0
                  TabIndex        =   32
                  Top             =   225
                  Width           =   3945
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "Presupuesto 2"
                  Height          =   255
                  Index           =   18
                  Left            =   4050
                  TabIndex        =   31
                  Top             =   225
                  Width           =   3945
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "Restringir la asignaci�n de presupuestos a la unidad organizativa de los art�culos"
                  Height          =   255
                  Index           =   27
                  Left            =   0
                  TabIndex        =   30
                  Top             =   1350
                  Width           =   6690
               End
               Begin VB.Label label2 
                  Caption         =   "Usar los siguientes conceptos presupuestariso en la emisi�n de pedidos:"
                  Height          =   240
                  Index           =   8
                  Left            =   90
                  TabIndex        =   37
                  Top             =   0
                  Width           =   7035
               End
            End
            Begin VB.Line Line5 
               BorderColor     =   &H80000003&
               Index           =   1
               X1              =   270
               X2              =   8640
               Y1              =   1845
               Y2              =   1845
            End
         End
      End
      Begin VB.PictureBox picPedidos 
         BorderStyle     =   0  'None
         Height          =   6435
         Index           =   1
         Left            =   -74880
         ScaleHeight     =   6435
         ScaleWidth      =   8805
         TabIndex        =   9
         Top             =   480
         Width           =   8805
         Begin VB.Frame fraAprovisionamiento 
            Height          =   2640
            Index           =   0
            Left            =   0
            TabIndex        =   15
            Top             =   0
            Width           =   8730
            Begin VB.PictureBox picPedidos 
               BorderStyle     =   0  'None
               Height          =   585
               Index           =   7
               Left            =   360
               ScaleHeight     =   585
               ScaleWidth      =   7875
               TabIndex        =   22
               Top             =   1785
               Width           =   7875
               Begin VB.OptionButton opEspCat 
                  Caption         =   "DPreguntar si adjuntar"
                  Height          =   210
                  Index           =   4
                  Left            =   3255
                  TabIndex        =   25
                  Top             =   270
                  Width           =   2400
               End
               Begin VB.OptionButton opEspCat 
                  Caption         =   "DNo adjuntar"
                  Height          =   270
                  Index           =   5
                  Left            =   5835
                  TabIndex        =   24
                  Top             =   270
                  Width           =   2000
               End
               Begin VB.OptionButton opEspCat 
                  Caption         =   "DAdjuntar autom�ticamente"
                  Height          =   240
                  Index           =   3
                  Left            =   450
                  TabIndex        =   23
                  Top             =   210
                  Width           =   2650
               End
               Begin VB.Label label2 
                  Caption         =   "Adjuntar especificaciones de cat�logo "
                  Height          =   225
                  Index           =   6
                  Left            =   180
                  TabIndex        =   26
                  Top             =   -30
                  Width           =   6465
               End
            End
            Begin VB.PictureBox picPedidos 
               BorderStyle     =   0  'None
               Height          =   615
               Index           =   6
               Left            =   360
               ScaleHeight     =   615
               ScaleWidth      =   7875
               TabIndex        =   17
               Top             =   855
               Width           =   7875
               Begin VB.OptionButton opEspCat 
                  Caption         =   "DPreguntar si adjuntar"
                  Height          =   210
                  Index           =   1
                  Left            =   3255
                  TabIndex        =   20
                  Top             =   270
                  Width           =   2400
               End
               Begin VB.OptionButton opEspCat 
                  Caption         =   "DNo adjuntar"
                  Height          =   270
                  Index           =   2
                  Left            =   5835
                  TabIndex        =   19
                  Top             =   270
                  Width           =   2000
               End
               Begin VB.OptionButton opEspCat 
                  Caption         =   "DAdjuntar autom�ticamente"
                  Height          =   240
                  Index           =   0
                  Left            =   450
                  TabIndex        =   18
                  Top             =   270
                  Width           =   2650
               End
               Begin VB.Label label2 
                  Caption         =   "Adjuntar especificaciones de cat�logo "
                  Height          =   195
                  Index           =   4
                  Left            =   180
                  TabIndex        =   21
                  Top             =   30
                  Width           =   6465
               End
            End
            Begin VB.CheckBox chkAprovisionamiento 
               Caption         =   "DEliminar l�neas de cat�logo con adjudicaciones consumidas"
               Height          =   255
               Index           =   0
               Left            =   480
               TabIndex        =   16
               Top             =   390
               Width           =   6675
            End
         End
         Begin VB.Frame fraAprovisionamiento 
            Height          =   1185
            Index           =   6
            Left            =   0
            TabIndex        =   10
            Top             =   2880
            Width           =   8730
            Begin VB.TextBox txtPedidoAbierto 
               Height          =   285
               Index           =   1
               Left            =   4680
               TabIndex        =   11
               Top             =   480
               Width           =   525
            End
            Begin VB.Label lblDesvioRecepcion 
               Caption         =   "DDesv�o en la recepci�n para pedidos de cat�logo:"
               Height          =   345
               Index           =   5
               Left            =   480
               TabIndex        =   14
               Top             =   480
               Width           =   4185
            End
            Begin VB.Label label2 
               Caption         =   "%"
               Height          =   225
               Index           =   29
               Left            =   5280
               TabIndex        =   13
               Top             =   480
               Width           =   225
            End
            Begin VB.Label label2 
               Caption         =   "D(Sin l�mite en la recepci�n indicar -1)"
               Height          =   225
               Index           =   30
               Left            =   5520
               TabIndex        =   12
               Top             =   480
               Width           =   2955
            End
         End
      End
      Begin VB.PictureBox picPedidos 
         BorderStyle     =   0  'None
         Height          =   5715
         Index           =   9
         Left            =   -74820
         ScaleHeight     =   5715
         ScaleWidth      =   8925
         TabIndex        =   2
         Top             =   360
         Width           =   8925
         Begin VB.Frame fraAprovisionamiento 
            Height          =   2050
            Index           =   3
            Left            =   0
            TabIndex        =   3
            Top             =   120
            Width           =   8830
            Begin VB.CheckBox chkAprovisionamiento 
               Caption         =   "DHabilitar campo para codificaci�n personalizada de recepciones"
               Height          =   255
               Index           =   5
               Left            =   225
               TabIndex        =   5
               Top             =   240
               Width           =   6675
            End
            Begin VB.TextBox txtNombreAprov 
               Height          =   285
               Index           =   3
               Left            =   6225
               TabIndex        =   4
               Top             =   800
               Width           =   2445
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
               Height          =   285
               Index           =   9
               Left            =   1080
               TabIndex        =   6
               Top             =   800
               Width           =   2115
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   6773
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "ID"
               Columns(1).Name =   "ID"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "OFFSET"
               Columns(2).Name =   "OFFSET"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   3731
               _ExtentY        =   503
               _StockProps     =   93
               ForeColor       =   -2147483630
               BackColor       =   -2147483643
            End
            Begin VB.Label label2 
               Caption         =   "DIdioma"
               Height          =   195
               Index           =   24
               Left            =   225
               TabIndex        =   8
               Top             =   840
               Width           =   615
            End
            Begin VB.Label label2 
               Caption         =   "DNombre a visualizar en GS y EP:"
               Height          =   195
               Index           =   25
               Left            =   3470
               TabIndex        =   7
               Top             =   840
               Width           =   2640
            End
         End
      End
      Begin FSGSUserControls.ctlConfGenPedidosCom ctlConfPedCom 
         Height          =   5205
         Left            =   -75000
         TabIndex        =   1
         Top             =   420
         Width           =   9090
         _ExtentX        =   16034
         _ExtentY        =   9181
      End
      Begin TabDlg.SSTab TabPlantPedidos 
         Height          =   4740
         Left            =   -75000
         TabIndex        =   79
         Top             =   450
         Width           =   8955
         _ExtentX        =   15796
         _ExtentY        =   8361
         _Version        =   393216
         Style           =   1
         Tabs            =   5
         TabsPerRow      =   5
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "dNotificaci�n de emisi�n"
         TabPicture(0)   =   "ctlConfGenPedidos.ctx":008C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "picPedidos(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "dNotificacion aceptacion"
         TabPicture(1)   =   "ctlConfGenPedidos.ctx":00A8
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "picPedidos(8)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "dAdnulaci�n"
         TabPicture(2)   =   "ctlConfGenPedidos.ctx":00C4
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "picPedidos(3)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "dRecepci�n"
         TabPicture(3)   =   "ctlConfGenPedidos.ctx":00E0
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "picPedidos(4)"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "dIm�genes y atributos para cat�logo"
         TabPicture(4)   =   "ctlConfGenPedidos.ctx":00FC
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "picPedidos(5)"
         Tab(4).ControlCount=   1
         Begin VB.PictureBox picPedidos 
            BorderStyle     =   0  'None
            Height          =   2715
            Index           =   8
            Left            =   -74910
            ScaleHeight     =   2715
            ScaleWidth      =   8775
            TabIndex        =   142
            Top             =   480
            Width           =   8775
            Begin VB.Frame fraNotiPed 
               Caption         =   "DPlantillas para notificaci�n de aceptaci�n de pedido"
               Height          =   1095
               Index           =   2
               Left            =   75
               TabIndex        =   144
               Top             =   45
               Width           =   8640
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   41
                  Left            =   1335
                  MaxLength       =   255
                  TabIndex        =   148
                  Top             =   255
                  Width           =   6810
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   42
                  Left            =   8235
                  Picture         =   "ctlConfGenPedidos.ctx":0118
                  Style           =   1  'Graphical
                  TabIndex        =   147
                  Top             =   255
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   40
                  Left            =   1335
                  MaxLength       =   255
                  TabIndex        =   146
                  Top             =   660
                  Width           =   6810
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   41
                  Left            =   8235
                  Picture         =   "ctlConfGenPedidos.ctx":01D7
                  Style           =   1  'Graphical
                  TabIndex        =   145
                  Top             =   660
                  Width           =   315
               End
               Begin VB.Label lblMailPed 
                  Caption         =   "DTXT:"
                  Height          =   195
                  Index           =   5
                  Left            =   120
                  TabIndex        =   150
                  Top             =   705
                  Width           =   1200
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DHTML:"
                  Height          =   255
                  Index           =   7
                  Left            =   120
                  TabIndex        =   149
                  Top             =   270
                  Width           =   1125
               End
            End
            Begin VB.TextBox txtMailSubject 
               Height          =   285
               Index           =   9
               Left            =   1995
               MaxLength       =   255
               TabIndex        =   143
               Top             =   1365
               Width           =   6705
            End
            Begin VB.Label label2 
               Caption         =   "DAsunto:"
               Height          =   255
               Index           =   7
               Left            =   135
               TabIndex        =   151
               Top             =   1395
               Width           =   1755
            End
         End
         Begin VB.PictureBox picPedidos 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   2055
            Index           =   3
            Left            =   -74925
            ScaleHeight     =   2055
            ScaleWidth      =   8775
            TabIndex        =   132
            Top             =   540
            Width           =   8775
            Begin VB.Frame fraAnuPed 
               Height          =   1095
               Left            =   90
               TabIndex        =   134
               Top             =   0
               Width           =   8610
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   22
                  Left            =   1335
                  MaxLength       =   255
                  TabIndex        =   138
                  Top             =   255
                  Width           =   6780
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   23
                  Left            =   8205
                  Picture         =   "ctlConfGenPedidos.ctx":0296
                  Style           =   1  'Graphical
                  TabIndex        =   137
                  Top             =   255
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   23
                  Left            =   1335
                  MaxLength       =   255
                  TabIndex        =   136
                  Top             =   660
                  Width           =   6780
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   24
                  Left            =   8205
                  Picture         =   "ctlConfGenPedidos.ctx":0355
                  Style           =   1  'Graphical
                  TabIndex        =   135
                  Top             =   660
                  Width           =   315
               End
               Begin VB.Label lblMailPed 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   1
                  Left            =   120
                  TabIndex        =   140
                  Top             =   705
                  Width           =   1335
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   139
                  Top             =   270
                  Width           =   1125
               End
            End
            Begin VB.TextBox txtMailSubject 
               Height          =   285
               Index           =   5
               Left            =   2025
               MaxLength       =   255
               TabIndex        =   133
               Top             =   1305
               Width           =   6675
            End
            Begin VB.Label label2 
               Caption         =   "DAsunto:"
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   141
               Top             =   1320
               Width           =   1875
            End
         End
         Begin VB.PictureBox picPedidos 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   3600
            Index           =   4
            Left            =   -74925
            ScaleHeight     =   3600
            ScaleWidth      =   8790
            TabIndex        =   112
            Top             =   480
            Width           =   8790
            Begin VB.Frame fraRecepKO 
               Caption         =   "dErr�nea"
               Height          =   1065
               Left            =   75
               TabIndex        =   123
               Top             =   1620
               Width           =   8640
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   27
                  Left            =   8235
                  Picture         =   "ctlConfGenPedidos.ctx":0414
                  Style           =   1  'Graphical
                  TabIndex        =   127
                  Top             =   600
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   26
                  Left            =   1305
                  MaxLength       =   255
                  TabIndex        =   126
                  Top             =   600
                  Width           =   6840
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   28
                  Left            =   8235
                  Picture         =   "ctlConfGenPedidos.ctx":04D3
                  Style           =   1  'Graphical
                  TabIndex        =   125
                  Top             =   240
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   27
                  Left            =   1305
                  MaxLength       =   255
                  TabIndex        =   124
                  Top             =   240
                  Width           =   6840
               End
               Begin VB.Label lblMailPed 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   3
                  Left            =   120
                  TabIndex        =   129
                  Top             =   645
                  Width           =   1335
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   3
                  Left            =   120
                  TabIndex        =   128
                  Top             =   255
                  Width           =   1125
               End
            End
            Begin VB.Frame fraRecepOK 
               Caption         =   "dCorrecta"
               Height          =   1065
               Left            =   75
               TabIndex        =   116
               Top             =   480
               Width           =   8640
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   24
                  Left            =   1305
                  MaxLength       =   255
                  TabIndex        =   120
                  Top             =   240
                  Width           =   6840
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   25
                  Left            =   8235
                  Picture         =   "ctlConfGenPedidos.ctx":0592
                  Style           =   1  'Graphical
                  TabIndex        =   119
                  Top             =   240
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   25
                  Left            =   1305
                  MaxLength       =   255
                  TabIndex        =   118
                  Top             =   630
                  Width           =   6840
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   26
                  Left            =   8235
                  Picture         =   "ctlConfGenPedidos.ctx":0651
                  Style           =   1  'Graphical
                  TabIndex        =   117
                  Top             =   600
                  Width           =   315
               End
               Begin VB.Label lblMailPed 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   2
                  Left            =   120
                  TabIndex        =   122
                  Top             =   645
                  Width           =   1335
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   2
                  Left            =   120
                  TabIndex        =   121
                  Top             =   255
                  Width           =   1125
               End
            End
            Begin VB.TextBox txtMailSubject 
               Height          =   285
               Index           =   4
               Left            =   1920
               MaxLength       =   255
               TabIndex        =   115
               Top             =   2850
               Width           =   6795
            End
            Begin VB.CommandButton cmdPlantilla 
               Height          =   285
               Index           =   32
               Left            =   8400
               Picture         =   "ctlConfGenPedidos.ctx":0710
               Style           =   1  'Graphical
               TabIndex        =   114
               Top             =   135
               Width           =   315
            End
            Begin VB.TextBox txtPlantilla 
               Alignment       =   1  'Right Justify
               Height          =   285
               Index           =   31
               Left            =   1815
               MaxLength       =   255
               TabIndex        =   113
               Top             =   135
               Width           =   6510
            End
            Begin VB.Label lbl 
               Caption         =   "DAsunto:"
               Height          =   255
               Index           =   0
               Left            =   135
               TabIndex        =   131
               Top             =   2865
               Width           =   1710
            End
            Begin VB.Label LblCartaPed 
               Caption         =   "DPlantilla recepci�n:"
               Height          =   255
               Index           =   5
               Left            =   120
               TabIndex        =   130
               Top             =   135
               Width           =   1590
            End
         End
         Begin VB.PictureBox picPedidos 
            BorderStyle     =   0  'None
            Height          =   2055
            Index           =   5
            Left            =   -74940
            ScaleHeight     =   2055
            ScaleWidth      =   8790
            TabIndex        =   102
            Top             =   495
            Width           =   8790
            Begin VB.Frame frmDatExt 
               Caption         =   "dPlantilla para petici�n datos externos"
               Height          =   1065
               Left            =   90
               TabIndex        =   104
               Top             =   45
               Width           =   8640
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   30
                  Left            =   8235
                  Picture         =   "ctlConfGenPedidos.ctx":07CF
                  Style           =   1  'Graphical
                  TabIndex        =   108
                  Top             =   645
                  Width           =   315
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   29
                  Left            =   8235
                  Picture         =   "ctlConfGenPedidos.ctx":088E
                  Style           =   1  'Graphical
                  TabIndex        =   107
                  Top             =   240
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   29
                  Left            =   1305
                  MaxLength       =   255
                  TabIndex        =   106
                  Top             =   645
                  Width           =   6870
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   28
                  Left            =   1305
                  MaxLength       =   255
                  TabIndex        =   105
                  Top             =   240
                  Width           =   6870
               End
               Begin VB.Label lblbuzperiodo 
                  Caption         =   "DXSL"
                  Height          =   255
                  Index           =   3
                  Left            =   120
                  TabIndex        =   110
                  Top             =   645
                  Width           =   735
               End
               Begin VB.Label lblMailPed 
                  Caption         =   "DE-Mail:"
                  Height          =   255
                  Index           =   4
                  Left            =   120
                  TabIndex        =   109
                  Top             =   240
                  Width           =   800
               End
            End
            Begin VB.TextBox txtMailSubject 
               Height          =   285
               Index           =   7
               Left            =   1770
               MaxLength       =   255
               TabIndex        =   103
               Top             =   1305
               Width           =   6960
            End
            Begin VB.Label label2 
               AutoSize        =   -1  'True
               Caption         =   "DAsunto:"
               Height          =   195
               Index           =   3
               Left            =   180
               TabIndex        =   111
               Top             =   1335
               Width           =   1350
            End
         End
         Begin VB.PictureBox picPedidos 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   4200
            Index           =   2
            Left            =   120
            ScaleHeight     =   4200
            ScaleWidth      =   8775
            TabIndex        =   80
            Top             =   495
            Width           =   8775
            Begin VB.Frame fraNotiPed 
               Caption         =   "DFormulario pedido"
               Height          =   1095
               Index           =   1
               Left            =   75
               TabIndex        =   94
               Top             =   45
               Width           =   8625
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   32
                  Left            =   1320
                  MaxLength       =   255
                  TabIndex        =   98
                  Top             =   675
                  Width           =   6810
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   33
                  Left            =   8220
                  Picture         =   "ctlConfGenPedidos.ctx":094D
                  Style           =   1  'Graphical
                  TabIndex        =   97
                  Top             =   660
                  Width           =   315
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   31
                  Left            =   8220
                  Picture         =   "ctlConfGenPedidos.ctx":0A0C
                  Style           =   1  'Graphical
                  TabIndex        =   96
                  Top             =   240
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   30
                  Left            =   1320
                  MaxLength       =   255
                  TabIndex        =   95
                  Top             =   240
                  Width           =   6810
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Index           =   6
                  Left            =   120
                  TabIndex        =   100
                  Top             =   675
                  Width           =   1125
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Index           =   4
                  Left            =   120
                  TabIndex        =   99
                  Top             =   240
                  Width           =   1125
               End
            End
            Begin VB.TextBox txtMailSubject 
               Height          =   285
               Index           =   6
               Left            =   1935
               MaxLength       =   255
               TabIndex        =   93
               Top             =   2490
               Width           =   6765
            End
            Begin VB.Frame fraNotiPed 
               Height          =   1095
               Index           =   0
               Left            =   75
               TabIndex        =   86
               Top             =   1245
               Width           =   8625
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   21
                  Left            =   1320
                  MaxLength       =   255
                  TabIndex        =   90
                  Top             =   300
                  Width           =   6810
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   22
                  Left            =   8220
                  Picture         =   "ctlConfGenPedidos.ctx":0ACB
                  Style           =   1  'Graphical
                  TabIndex        =   89
                  Top             =   300
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   20
                  Left            =   1320
                  MaxLength       =   255
                  TabIndex        =   88
                  Top             =   660
                  Width           =   6810
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   21
                  Left            =   8220
                  Picture         =   "ctlConfGenPedidos.ctx":0B8A
                  Style           =   1  'Graphical
                  TabIndex        =   87
                  Top             =   660
                  Width           =   315
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   0
                  Left            =   150
                  TabIndex        =   92
                  Top             =   300
                  Width           =   1125
               End
               Begin VB.Label lblMailPed 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   0
                  Left            =   150
                  TabIndex        =   91
                  Top             =   690
                  Width           =   1335
               End
            End
            Begin VB.Frame fraNotiPed 
               Caption         =   "DE-mail de emisiones  CC"
               Height          =   1335
               Index           =   3
               Left            =   60
               TabIndex        =   81
               Top             =   2820
               Width           =   8625
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   10
                  Left            =   1845
                  MaxLength       =   255
                  TabIndex        =   84
                  Top             =   270
                  Width           =   6645
               End
               Begin VB.CheckBox chkNotifPed 
                  Caption         =   "DIncluir al Peticionario en CC"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   83
                  Top             =   720
                  Width           =   6675
               End
               Begin VB.CheckBox chkNotifPed 
                  Caption         =   "DIncluir al Comprador en CC"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   82
                  Top             =   1020
                  Width           =   6675
               End
               Begin VB.Label label2 
                  Caption         =   "DLista e-mails CC.:"
                  Height          =   255
                  Index           =   26
                  Left            =   150
                  TabIndex        =   85
                  Top             =   285
                  Width           =   1725
               End
               Begin VB.Line Line5 
                  BorderColor     =   &H80000003&
                  Index           =   0
                  X1              =   120
                  X2              =   8490
                  Y1              =   630
                  Y2              =   630
               End
            End
            Begin VB.Label label2 
               Caption         =   "DAsunto:"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   101
               Top             =   2505
               Width           =   1725
            End
         End
      End
   End
End
Attribute VB_Name = "ctlConfGenPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_oGestorParametros As CGestorParametros
Private m_oParametrosGenerales As ParametrosGenerales
Private m_oIdiomas As CIdiomas
Private m_sIdioma As String
Private sCap(3) As String

Private m_bCodPersonalizPedido As Boolean

Public Sub chkRGest_SetValue(ByVal Index As Integer, ByVal bValor As Boolean)
    chkRegGest(Index).Value = IIf(bValor, vbChecked, vbUnchecked)
End Sub
Public Function chkRGest_GetValue(ByVal Index As Integer) As Boolean
    chkRGest_GetValue = IIf(chkRegGest(Index).Value = vbChecked, True, False)
End Function

Public Function cmbOblDist_SetListIndex(ByVal Index As Integer, ByVal ListIndex As Integer)
    cmbOblDist(Index).ListIndex = ListIndex
End Function

Public Property Get sdbcIdioma4() As Object
    Set sdbcIdioma4 = sdbcIdioma(4)
End Property

Public Property Get sdbcIdioma5() As Object
    Set sdbcIdioma5 = sdbcIdioma(5)
End Property

Public Property Get sdbcIdioma9() As Object
    Set sdbcIdioma9 = sdbcIdioma(9)
End Property

Public Sub txtMailSubject_SetFocus()
    tabPedidos.Tab = 3
    TabPlantPedidos.Tab = 0
    txtMailSubject(10).SetFocus
End Sub

Public Sub Initialize(ByRef oIdiomas As CIdiomas, ByRef oGestorParametros As CGestorParametros, ByRef oParametrosGenerales As Variant, ByVal sIdioma As String, ByVal bIntegracionPedidos As Boolean, ByVal bIntegracionRecep As Boolean)
    Dim oIdioma As CIdioma
    
    PonerFieldSeparator UserControl.Controls
    
    Set m_oIdiomas = oIdiomas
    Set m_oGestorParametros = oGestorParametros
    m_oParametrosGenerales = oParametrosGenerales
    m_sIdioma = sIdioma
    
    For Each oIdioma In m_oIdiomas
        sdbcIdioma(4).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(5).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(9).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
    Next
    Set oIdioma = Nothing
    
    ctlConfPedCom.IntegracionPedidos = bIntegracionPedidos
    ctlConfPedCom.ActPedAbierto = m_oParametrosGenerales.gbActPedAbierto
    
    If oParametrosGenerales.gbPedidosAprov Or oParametrosGenerales.gbPedidosDirectos Then
        If Not oParametrosGenerales.gbPedidosDirectos Then
            picPedidos(10).Visible = False
            'Movemos el pic para que no quede tanto espacio
            fraAprovisionamiento(1).Height = fraAprovisionamiento(1).Height - picPedidos(10).Height
            fraAprovisionamiento(5).Top = fraAprovisionamiento(1).Top + fraAprovisionamiento(1).Height + 40
        End If
        If Not oParametrosGenerales.gbPedidosAprov Then
            tabPedidos.TabVisible(1) = False
            TabPlantPedidos.TabVisible(4) = False
        End If
    End If
    
    ''Tab Recepciones dentro de TabPedidos:
    tabPedidos.TabVisible(2) = bIntegracionRecep
    
    ArrangeControls oParametrosGenerales
End Sub

Private Sub ArrangeControls(ByRef oParametrosGenerales As Variant)
    If oParametrosGenerales.gbPedidosDirectos Then
        If Not oParametrosGenerales.gbUsarPres1 And Not oParametrosGenerales.gbUsarPres2 And Not oParametrosGenerales.gbUsarPres3 And Not oParametrosGenerales.gbUsarPres4 Then
            fraAprovisionamiento(2).Visible = False
            fraAprovisionamiento(1).Top = fraAprovisionamiento(2).Top
            fraAprovisionamiento(5).Top = fraAprovisionamiento(1).Top + fraAprovisionamiento(1).Height + 40
        Else
            fraAprovisionamiento(2).Visible = True
            If oParametrosGenerales.gbUsarPres1 Then
                chkRegGest(17).Visible = True
                chkRegGest(13).Visible = True
                lblOblDist(5).Visible = True
                cmbOblDist(5).Visible = True
                chkRegGest(14).Top = chkRegGest(13).Top + 520
                lblOblDist(6).Top = lblOblDist(5).Top + 520
                cmbOblDist(6).Top = cmbOblDist(5).Top + 520
            Else
                chkRegGest(17).Visible = False
                chkRegGest(20).Left = chkRegGest(19).Left
                chkRegGest(20).Top = chkRegGest(19).Top
                chkRegGest(19).Left = chkRegGest(18).Left
                chkRegGest(19).Top = chkRegGest(18).Top
                chkRegGest(18).Left = chkRegGest(17).Left
                chkRegGest(18).Top = chkRegGest(17).Top
                chkRegGest(13).Visible = False
                lblOblDist(5).Visible = False
                cmbOblDist(6).Visible = False
                chkRegGest(13).Value = 0
                chkRegGest(14).Top = chkRegGest(13).Top
                lblOblDist(6).Top = lblOblDist(5).Top
                cmbOblDist(6).Top = cmbOblDist(5).Top
            End If
            
            If oParametrosGenerales.gbUsarPres2 Then
                chkRegGest(18).Visible = True
                chkRegGest(14).Visible = True
                lblOblDist(6).Visible = True
                cmbOblDist(6).Visible = True
                cmbOblDist(6).Clear
                chkRegGest(15).Top = chkRegGest(14).Top + 520
                lblOblDist(7).Top = lblOblDist(6).Top + 520
                cmbOblDist(7).Top = cmbOblDist(6).Top + 520
            Else
                chkRegGest(18).Visible = False
                chkRegGest(20).Left = chkRegGest(19).Left
                chkRegGest(20).Top = chkRegGest(19).Top
                chkRegGest(19).Left = chkRegGest(18).Left
                chkRegGest(19).Top = chkRegGest(18).Top
                chkRegGest(14).Visible = False
                lblOblDist(6).Visible = False
                cmbOblDist(6).Visible = False
                chkRegGest(14).Value = 0
                chkRegGest(15).Top = chkRegGest(14).Top
                lblOblDist(7).Top = lblOblDist(6).Top
                cmbOblDist(7).Top = cmbOblDist(6).Top
            End If
            
            If oParametrosGenerales.gbUsarPres3 Then
                chkRegGest(19).Visible = True
                chkRegGest(15).Visible = True
                lblOblDist(7).Visible = True
                cmbOblDist(7).Visible = True
                cmbOblDist(7).Clear
                chkRegGest(16).Top = chkRegGest(15).Top + 520
                lblOblDist(8).Top = lblOblDist(7).Top + 520
                cmbOblDist(8).Top = cmbOblDist(7).Top + 492
            Else
                chkRegGest(19).Visible = False
                chkRegGest(20).Left = chkRegGest(19).Left
                chkRegGest(20).Top = chkRegGest(19).Top
                chkRegGest(15).Visible = False
                lblOblDist(7).Visible = False
                cmbOblDist(7).Visible = False
                chkRegGest(15).Value = 0
                chkRegGest(16).Top = chkRegGest(15).Top
                lblOblDist(8).Top = lblOblDist(7).Top
                cmbOblDist(8).Top = cmbOblDist(7).Top
            End If
                
            If oParametrosGenerales.gbUsarPres4 Then
                chkRegGest(20).Visible = True
                chkRegGest(16).Visible = True
                lblOblDist(8).Visible = True
                cmbOblDist(8).Visible = True
                cmbOblDist(8).Clear
            Else
                chkRegGest(20).Visible = False
                chkRegGest(16).Visible = False
                lblOblDist(8).Visible = False
                cmbOblDist(8).Visible = False
            End If
        End If
    End If
End Sub

Public Sub MostrarParametros(ByVal oParametrosGenerales As Variant, ByVal iNumPres As Integer, ByVal idiAprovRow As Variant, ByVal idiRecepRow As Variant, ByVal idiPedAbiertoRow As Variant)
    Dim i As Integer
    
    With oParametrosGenerales
        If .gbPedidosDirectos Or .gbPedidosAprov Then
            TabPlantPedidos.TabVisible(4) = (.gbPedidosAprov = True)
            tabPedidos.TabVisible(1) = (.gbPedidosAprov = True)
            
            'Opciones de Pedidos directos
            picPedidos(10).Visible = .gbPedidosDirectos
        End If
    
        chkAprovisionamiento(0).Value = IIf(.gbElimLinCatalogo, vbChecked, vbUnchecked)
        chkAprovisionamiento(3).Value = IIf(.gbCodPersonalizPedido, vbChecked, vbUnchecked)
        chkAprovisionamiento(2).Value = IIf(.gbCodPersonalizDirecto, vbChecked, vbUnchecked)
        chkAprovisionamiento(4).Value = IIf(.gbOblCodPersonalizPedido, vbChecked, vbUnchecked)
        chkAprovisionamiento(1).Value = IIf(.gbBloqPersonalizPedido, vbChecked, vbUnchecked)
        txtNombreAprov(0).Text = .gsNomCodPersonalizPedido
        
        chkAprovisionamiento(0).Visible = True
        
        If .gbPedidosDirectos Then
            Select Case iNumPres
                Case 1
                    fraPedPres.Height = 555
                Case 2
                    fraPedPres.Height = 1065
                Case 3
                    fraPedPres.Height = 1575
                    VScroll.Max = 10
                Case 4
                    fraPedPres.Height = 2050
                    VScroll.Max = 20
            End Select
            If fraPedPres.Height <= Frame11(2).Height Then
                VScroll.Visible = False
                fraPedPres.Top = 0
            Else
                VScroll.Visible = True
            End If
                            
            fraAprovisionamiento(1).Top = fraAprovisionamiento(2).Height + fraAprovisionamiento(2).Top + 40
            If Not .gbUsarPres1 And Not .gbUsarPres2 And Not .gbUsarPres3 And Not .gbUsarPres4 Then
                fraAprovisionamiento(2).Visible = False
                fraAprovisionamiento(1).Top = fraAprovisionamiento(2).Top
            Else
                fraAprovisionamiento(2).Visible = True
                 
                ArrangeControls oParametrosGenerales
                
                If .gbUsarPres1 Then
                    cmbOblDist(5).Clear
                    cmbOblDist(5).AddItem ""
                    For i = 1 To .giNEPP
                        cmbOblDist(5).AddItem i
                    Next i
                    If .gbUsarPedPres1 Then
                        chkRegGest(17).Value = vbChecked
                        If .gbSincrPresItem Then 'Pongo lo de procesos
                            If .gbOBLPP Then
                                chkRegGest(13).Value = vbChecked
                            Else
                                chkRegGest(13).Value = vbUnchecked
                            End If
                            If .giNIVPP > 0 Then
                                cmbOblDist(5).ListIndex = .giNIVPP
                            Else
                                cmbOblDist(5).ListIndex = -1
                                .giNIVPedPres1 = 0
                            End If
                        Else
                            If .gbOBLPedPres1 Then
                                chkRegGest(13).Value = vbChecked
                            Else
                                chkRegGest(13).Value = vbUnchecked
                            End If
                            If .giNIVPedPres1 > 0 Then
                                cmbOblDist(5).ListIndex = .giNIVPedPres1
                            Else
                                cmbOblDist(5).ListIndex = -1
                                .giNIVPedPres1 = 0
                            End If
                        End If
                    Else
                        chkRegGest(17).Value = vbUnchecked
                        chkRegGest(13).Value = vbUnchecked
                        cmbOblDist(5).ListIndex = -1
                    End If
                    
                Else
                    chkRegGest(13).Value = 0
                End If
                
                If .gbUsarPres2 Then
                    cmbOblDist(6).Clear
                    cmbOblDist(6).AddItem ""
                    For i = 1 To .giNEPC
                        cmbOblDist(6).AddItem i
                    Next i
                    If .gbUsarPedPres2 Then
                        chkRegGest(18).Value = vbChecked
                        If .gbSincrPresItem Then 'Pongo lo de procesos
                            If .gbOBLPC Then
                                chkRegGest(14).Value = vbChecked
                            Else
                                chkRegGest(14).Value = vbUnchecked
                            End If
                            If .giNIVPC > 0 Then
                                cmbOblDist(6).ListIndex = .giNIVPC
                            Else
                                cmbOblDist(6).ListIndex = -1
                                .giNIVPedPres2 = 0
                            End If
                        
                        Else
                            If .gbOBLPedPres2 Then
                                chkRegGest(14).Value = vbChecked
                            Else
                                chkRegGest(14).Value = vbUnchecked
                            End If
                                
                            If .giNIVPedPres2 > 0 Then
                                cmbOblDist(6).ListIndex = .giNIVPedPres2
                            Else
                                cmbOblDist(6).ListIndex = -1
                                .giNIVPedPres2 = 0
                            End If
                        End If
                    Else
                        chkRegGest(18).Value = vbUnchecked
                        chkRegGest(14).Value = vbUnchecked
                        cmbOblDist(6).ListIndex = -1
                    End If
                Else
                    chkRegGest(14).Value = 0
                End If
                
                If .gbUsarPres3 Then
                    cmbOblDist(7).Clear
                    cmbOblDist(7).AddItem ""
                    For i = 1 To .giNEP3
                        cmbOblDist(7).AddItem i
                    Next i
                    If .gbUsarPedPres3 Then
                        chkRegGest(19).Value = vbChecked
                        If .gbSincrPresItem Then 'Pongo lo de procesos
                            chkRegGest(15).Value = IIf(.gbOBLPres3, vbChecked, vbUnchecked)
                            If .giNIVPres3 > 0 Then
                                cmbOblDist(7).ListIndex = .giNIVPres3
                            Else
                                cmbOblDist(7).ListIndex = -1
                                .giNIVPedPres3 = 0
                            End If
                        Else
                            chkRegGest(15).Value = IIf(.gbOBLPedPres3, vbChecked, vbUnchecked)
                                
                            If .giNIVPedPres3 > 0 Then
                                cmbOblDist(7).ListIndex = .giNIVPedPres3
                            Else
                                cmbOblDist(7).ListIndex = -1
                                .giNIVPedPres3 = 0
                            End If
                        End If
                    Else
                        chkRegGest(19).Value = vbUnchecked
                        chkRegGest(15).Value = vbUnchecked
                        cmbOblDist(7).ListIndex = -1
                    End If
                    
                Else
                    chkRegGest(15).Value = 0
                End If
                    
                If .gbUsarPres4 Then
                    cmbOblDist(8).Clear
                    cmbOblDist(8).AddItem ""
                    For i = 1 To .giNEP4
                        cmbOblDist(8).AddItem i
                    Next i
                    If .gbUsarPedPres4 Then
                        chkRegGest(20).Value = vbChecked
                        If .gbSincrPresItem Then 'Pongo lo de procesos
                            chkRegGest(16).Value = IIf(.gbOBLPres4, vbChecked, vbUnchecked)
                            If .giNIVPres4 > 0 Then
                                cmbOblDist(8).ListIndex = .giNIVPres4
                            Else
                                cmbOblDist(8).ListIndex = -1
                                .giNIVPedPres4 = 0
                            End If
                        Else
                            chkRegGest(16).Value = IIf(.gbOBLPedPres4, vbChecked, vbUnchecked)
                            If .giNIVPedPres4 > 0 Then
                                cmbOblDist(8).ListIndex = .giNIVPedPres4
                            Else
                                cmbOblDist(8).ListIndex = -1
                                .giNIVPedPres4 = 0
                            End If
                        End If
                     Else
                        chkRegGest(20).Value = vbUnchecked
                        chkRegGest(16).Value = vbUnchecked
                        cmbOblDist(8).ListIndex = -1
                    End If
                Else
                    chkRegGest(16).Value = 0
                End If
                
                If Not .gbIntegracion Or Not .gbUsarOrgCompras Then
                    chkOblPedido(3).Visible = False
                    chkOblPedido(1).Top = chkOblPedido(3).Top
                End If
                picPedidos(10).Height = chkHitosFacturacion.Top + chkHitosFacturacion.Height + 40
                fraAprovisionamiento(1).Height = picPedidos(10).Top + picPedidos(10).Height + 40
                                
                'Deshabilito la parte de obligar y niveles
                fraPedPres.Enabled = Not (.gbSincrPresItem)
                
                chkRegGest(21).Value = IIf(.gbUsarPresItem, vbChecked, vbUnchecked)
                chkRegGest(22).Value = IIf(.gbSincrPresItem, vbChecked, vbUnchecked)
                chkRegGest(27).Value = IIf(.gbOblAsigPresUonArtPeDir, vbChecked, vbUnchecked)
            End If
                
            chkOblPedido(0).Value = IIf(.gbOblPedidosHom, vbChecked, vbUnchecked)
            chkOblPedido(6).Value = IIf(.gbOblPedidosFechaEntrega, vbChecked, vbUnchecked)
            chkOblPedido(1).Value = IIf(.gbOblPedidosDistCompra, vbChecked, vbUnchecked)
            chkOblPedido(2).Value = IIf(.gbEstablecerDirEntregaNivelCabecera, vbChecked, vbUnchecked)
            chkOblPedido(3).Value = IIf(.gbDesglosarEmisionPorCentroAprov, vbChecked, vbUnchecked)
            
            If .gbRecepAbiertas Then
                chkOblPedido(4).Value = vbChecked
                txtPedidoAbierto(0).Text = .gsPedidoAbierto
            Else
                chkOblPedido(4).Value = vbUnchecked
                txtPedidoAbierto(0).Text = ""
            End If
            
            txtPedidoAbierto(1).Text = .gsDesvioRecepcion
            
            chkOblPedido(5).Value = IIf(.gbPedidosIncluirViaPago, vbChecked, vbUnchecked)
            chkRegGest(2).Value = IIf(.gbActPedAbierto, vbChecked, vbUnchecked)
            chkHitosFacturacion.Value = IIf(.gbPedidosHitosFacturacion, vbChecked, vbUnchecked)
        End If
        
        Select Case .giCatalogoEspecArticulo
            Case 1:     opEspCat(0).Value = True
            Case 2:     opEspCat(1).Value = True
            Case 3:     opEspCat(2).Value = True
        End Select
        
        Select Case .giCatalogoEspecProveArt
            Case 1:     opEspCat(3).Value = True
            Case 2:     opEspCat(4).Value = True
            Case 3:     opEspCat(5).Value = True
        End Select
        
        chkAprovisionamiento(5).Value = IIf(.gbCodPersonalizRecep, vbChecked, vbUnchecked)
        
        sdbcIdioma(4).Text = m_oIdiomas.Item(CStr(m_sIdioma)).Den
        sdbcIdioma(5).Text = m_oIdiomas.Item(CStr(m_sIdioma)).Den
        sdbcIdioma(9).Text = m_oIdiomas.Item(CStr(m_sIdioma)).Den
        sdbcIdioma(4).Bookmark = idiAprovRow
        sdbcIdioma(5).Bookmark = idiPedAbiertoRow
        sdbcIdioma(9).Bookmark = idiRecepRow
        
        txtNombreAprov(2).Text = .gsNomCodPersonalizPedidoPortal
        txtNombreAprov(3).Text = .gsNomCodPersonalizRecep
        txtPlantilla(20) = .gsPEDDEMIMAILDOT
        txtPlantilla(21) = .gsPEDDEMICARTADOT
        txtPlantilla(22) = .gsPEDDANUCARTADOT
        txtPlantilla(23) = .gsPEDDANUMAILDOT
        txtPlantilla(24) = .gsPEDDRECOKCARTADOT
        txtPlantilla(25) = .gsPEDDRECOKMAILDOT
        txtPlantilla(26) = .gsPEDDRECKOCARTADOT
        txtPlantilla(27) = .gsPEDDRECKOMAILDOT
        txtPlantilla(28) = .gsDATEXTMAILDOT
        txtPlantilla(29) = .gsDATEXTXLS
        txtPlantilla(30) = .gsPedidoDot
        txtPlantilla(31) = .gsPEDRECDOT
        txtPlantilla(32) = .gsPedidoXLT
        txtPlantilla(40) = .gsPEDNotifAcepTXT
        txtPlantilla(41) = .gsPEDNotifAcepHTML
        
        txtMailSubject(4) = .gsPEDDRECMAILSUBJECT
        txtMailSubject(5) = .gsPedAnuMailSubject
        txtMailSubject(6) = .gsPedEmiMailSubject
        txtMailSubject(7) = .gsDATEXTMAILSUBJECT
        txtMailSubject(9) = .gsPedNotifMailSubject
        txtMailSubject(10) = .gsPedNotifMailCC
        
        chkNotifPed(0).Value = IIf(.giPedNotifMailCCPeti > 0, vbChecked, vbUnchecked)
        chkNotifPed(1).Value = IIf(.giPedNotifMailCCComp > 0, vbChecked, vbUnchecked)
        
        ctlConfPedCom.PedEmail = .giPedEmail
        ctlConfPedCom.PedEmailModo = .giPedEmailModo
        ctlConfPedCom.PedPubPortal = .giPedPubPortal
        ctlConfPedCom.PedPubPortalPA = .giPedPubPortalPA
        
        fraAprovisionamiento(5).Top = fraAprovisionamiento(1).Top + fraAprovisionamiento(1).Height + 40
    End With
End Sub

Public Sub PrepararParametros(ByRef oParametrosGenerales As Variant)
    With oParametrosGenerales
        .gbCodPersonalizDirecto = (chkAprovisionamiento(2).Value = vbChecked)
        .gbCodPersonalizPedido = (chkAprovisionamiento(3).Value = vbChecked)
        .gsNomCodPersonalizPedido = txtNombreAprov(0).Text
        .gbBloqPersonalizPedido = (chkAprovisionamiento(1).Value = vbChecked)
        .gbOblCodPersonalizPedido = (chkAprovisionamiento(4).Value = vbChecked)
    
        .gbOblPedidosHom = (chkOblPedido(0).Value = vbChecked)
        .gbOblPedidosDistCompra = (chkOblPedido(1).Value = vbChecked)
        .gbEstablecerDirEntregaNivelCabecera = (chkOblPedido(2).Value = vbChecked)
        .gbDesglosarEmisionPorCentroAprov = (chkOblPedido(3).Value = vbChecked)
        .gbRecepAbiertas = (chkOblPedido(4).Value = vbChecked)
        .gbPedidosIncluirViaPago = (chkOblPedido(5).Value = vbChecked)
        .gbOblPedidosFechaEntrega = (chkOblPedido(6).Value = vbChecked)
        .gbPedidosHitosFacturacion = IIf(chkHitosFacturacion.Value, vbChecked, vbUnchecked)
        
        .gbElimLinCatalogo = IIf(chkAprovisionamiento(0).Value = vbUnchecked, False, True)
        
        .gbActPedAbierto = (chkRegGest(2).Value = vbChecked)
        .gbUsarPedPres1 = CBool(chkRegGest(17).Value)
        .gbUsarPedPres2 = CBool(chkRegGest(18).Value)
        .gbUsarPedPres3 = CBool(chkRegGest(19).Value)
        .gbUsarPedPres4 = CBool(chkRegGest(20).Value)
        .gbUsarPresItem = CBool(chkRegGest(21).Value)
        .gbSincrPresItem = CBool(chkRegGest(22).Value)
        .gbOBLPedPres1 = CBool(chkRegGest(13).Value)
        .gbOblAsigPresUonArtPeDir = CBool(chkRegGest(27).Value)
        
        If cmbOblDist(5).Text = "" Then
            .giNIVPedPres1 = 0
        Else
            If cmbOblDist(5).ListIndex > -1 Then
                .giNIVPedPres1 = cmbOblDist(5).Text
            Else
                .giNIVPedPres1 = 0
            End If
        End If
        
        .gbOBLPedPres2 = CBool(chkRegGest(14).Value)
        If cmbOblDist(6).Text = "" Then
            .giNIVPedPres2 = 0
        Else
            If cmbOblDist(6).ListIndex > -1 Then
                .giNIVPedPres2 = cmbOblDist(6).Text
            Else
                .giNIVPedPres2 = 0
            End If
        End If
        
        .gbOBLPedPres3 = CBool(chkRegGest(15).Value)
        If cmbOblDist(7).Text = "" Then
            .giNIVPedPres3 = 0
        Else
            If cmbOblDist(7).ListIndex > -1 Then
                .giNIVPedPres3 = cmbOblDist(7).Text
            Else
                .giNIVPedPres3 = 0
            End If
        End If
        
        .gbOBLPedPres4 = CBool(chkRegGest(16).Value)
        If cmbOblDist(8).Text = "" Then
            .giNIVPedPres4 = 0
        Else
            If cmbOblDist(8).ListIndex > -1 Then
                .giNIVPedPres4 = cmbOblDist(8).Text
            Else
                .giNIVPedPres4 = 0
            End If
        End If
        
        If opEspCat(0).Value = True Then
            .giCatalogoEspecArticulo = Adjuntar
        ElseIf opEspCat(1).Value = True Then
            .giCatalogoEspecArticulo = PreguntarAdj
        Else
            .giCatalogoEspecArticulo = NoAdjuntar
        End If
        
        If opEspCat(3).Value = True Then
            .giCatalogoEspecProveArt = Adjuntar
        ElseIf opEspCat(4).Value = True Then
            .giCatalogoEspecProveArt = PreguntarAdj
        Else
            .giCatalogoEspecProveArt = NoAdjuntar
        End If
        
        .gsDesvioRecepcion = Trim(txtPedidoAbierto(1))
        
        If chkAprovisionamiento(5).Value = vbUnchecked Then
            .gbCodPersonalizRecep = False
        Else
            .gbCodPersonalizRecep = True
        End If
                
        .gsNomCodPersonalizRecep = txtNombreAprov(3).Text
        .gsPEDDEMIMAILDOT = txtPlantilla(20)
        .gsPEDDEMICARTADOT = txtPlantilla(21)
        .gsPEDDANUCARTADOT = txtPlantilla(22)
        .gsPEDDANUMAILDOT = txtPlantilla(23)
        .gsPEDDRECOKCARTADOT = txtPlantilla(24)
        .gsPEDDRECOKMAILDOT = txtPlantilla(25)
        .gsPEDDRECKOMAILDOT = txtPlantilla(26)
        .gsPEDDRECKOCARTADOT = txtPlantilla(27)
        .gsDATEXTMAILDOT = txtPlantilla(28)
        .gsDATEXTXLS = txtPlantilla(29)
        .gsPedidoDot = txtPlantilla(30)
        .gsPEDRECDOT = txtPlantilla(31)
        .gsPedidoXLT = txtPlantilla(32)
        .gsPEDNotifAcepTXT = txtPlantilla(40)
        .gsPEDNotifAcepHTML = txtPlantilla(41)
        
        .gsPEDDRECMAILSUBJECT = txtMailSubject(4)
        .gsPedAnuMailSubject = txtMailSubject(5)
        .gsPedEmiMailSubject = txtMailSubject(6)
        .gsDATEXTMAILSUBJECT = txtMailSubject(7)
        .gsPedNotifMailSubject = txtMailSubject(9)
        .gsPedNotifMailCC = txtMailSubject(10).Text
        
        If chkNotifPed(0).Value Then
            .giPedNotifMailCCPeti = 1
        Else
            .giPedNotifMailCCPeti = 0
        End If
        If chkNotifPed(1).Value Then
            .giPedNotifMailCCComp = 1
        Else
            .giPedNotifMailCCComp = 0
        End If
        
        .giPedEmail = ctlConfPedCom.PedEmail
        .giPedEmailModo = ctlConfPedCom.PedEmailModo
        .giPedPubPortal = ctlConfPedCom.PedPubPortal
        .giPedPubPortalPA = ctlConfPedCom.PedPubPortalPA
    End With
End Sub

Public Function VerificarParametros(ByRef oMensajes As CMensajes, ByRef iCaso As Integer) As Boolean
    Dim bVerificar As Boolean
    Dim oIdioma As CIdioma
    Dim oFos As FileSystemObject
    Dim sFileName As String
    
    If chkAprovisionamiento(2).Value = vbChecked Or chkAprovisionamiento(3).Value = vbChecked Then
        'si se ha seleccionado la codificaci�n personalizada de pedidos ser� obligatorio
        'introducir un nombre para cada idioma.
        For Each oIdioma In m_oIdiomas
            If Not UserControl.Parent.oLiteralesAModificar.Item(oIdioma.Cod & "31") Is Nothing Then
                If IsNull(UserControl.Parent.oLiteralesAModificar.Item(oIdioma.Cod & "31").Den) Or UserControl.Parent.oLiteralesAModificar.Item(oIdioma.Cod & "31").Den = "" Then
                    oMensajes.FaltanReferencias
                    VerificarParametros = False
                    Exit Function
                End If
            Else
                oMensajes.FaltanReferencias
                VerificarParametros = False
                Exit Function
            End If
            If Not UserControl.Parent.oLiteralesAModificar.Item(oIdioma.Cod & "36") Is Nothing Then
                If IsNull(UserControl.Parent.oLiteralesAModificar.Item(oIdioma.Cod & "36").Den) Or UserControl.Parent.oLiteralesAModificar.Item(oIdioma.Cod & "36").Den = "" Then
                    oMensajes.FaltanReferencias
                    VerificarParametros = False
                    Exit Function
                End If
            Else
                oMensajes.FaltanReferencias
                VerificarParametros = False
                Exit Function
            End If
        Next
    End If
    
    If ((opEspCat(0).Value = False) And (opEspCat(1).Value = False) And (opEspCat(2).Value = False)) Then
        oMensajes.ImposibleActualizarParam label2(4).Caption
        VerificarParametros = False
        Exit Function
    End If
    
    If ((opEspCat(3).Value = False) And (opEspCat(4).Value = False) And (opEspCat(5).Value = False)) Then
        oMensajes.ImposibleActualizarParam label2(6).Caption
        VerificarParametros = False
        Exit Function
    End If
    
    If Trim(txtPedidoAbierto(1).Text) <> "" Then
        If Not IsNumeric(Trim(txtPedidoAbierto(1).Text)) Then
            oMensajes.ImposibleActualizarParam oMensajes.CargarTextoMensaje(1539)
            VerificarParametros = False
            Exit Function
        End If
    Else
        txtPedidoAbierto(1).Text = 0
    End If
    
    If Not txtMailSubject(10).Text = "" Then
        If Not ComprobarEmail(txtMailSubject(10).Text) Then
            oMensajes.MailIncorrecto txtMailSubject(10).Text
            VerificarParametros = False
            iCaso = 1
            Exit Function
        End If
    End If
    
    'Comprueba que las plantillas de aceptaci�n de pedido comienzen por "IDIOMA_"
    If Trim(txtPlantilla(41).Text) <> "" Then
        Set oFos = New FileSystemObject
        sFileName = oFos.GetFileName(Trim(txtPlantilla(41).Text))
        Set oFos = Nothing
        
        If m_oIdiomas.Item(Mid(sFileName, 1, 3)) Is Nothing Then
            oMensajes.ImposibleActualizarParamNotifReceptor
            VerificarParametros = False
            Exit Function
        End If
    End If
    If Trim(txtPlantilla(40).Text) <> "" Then
        Set oFos = New FileSystemObject
        sFileName = oFos.GetFileName(Trim(txtPlantilla(40).Text))
        Set oFos = Nothing
        
        If m_oIdiomas.Item(Mid(sFileName, 1, 3)) Is Nothing Then
            oMensajes.ImposibleActualizarParamNotifReceptor
            VerificarParametros = False
            Exit Function
        End If
    End If
    
    Set oIdioma = Nothing
    
    VerificarParametros = True
End Function

Public Sub CargarRecursos(ByVal Recursos As Variant, RecursosCom As Variant)
    'Tabs
    tabPedidos.TabCaption(0) = Recursos(0)
    tabPedidos.TabCaption(1) = Recursos(1)
    tabPedidos.TabCaption(2) = Recursos(2)
    tabPedidos.TabCaption(3) = Recursos(3)
    tabPedidos.TabCaption(4) = Recursos(4)
    'Tab Pedidos
    chkAprovisionamiento(3).Caption = Recursos(5)
    chkAprovisionamiento(4).Caption = Recursos(6)
    chkAprovisionamiento(2).Caption = Recursos(7)
    label2(8).Caption = Recursos(8)
    label2(9).Caption = Recursos(9)
    label2(10).Caption = Recursos(10)
    label2(11).Caption = Recursos(11)
    label2(27).Caption = Recursos(12)
    chkAprovisionamiento(1).Caption = Recursos(13)
    chkRegGest(2).Caption = Recursos(14)
    chkRegGest(13).Caption = Recursos(15)
    chkRegGest(14).Caption = Recursos(16)
    chkRegGest(15).Caption = Recursos(17)
    chkRegGest(16).Caption = Recursos(18)
    chkRegGest(17).Caption = Recursos(19)
    chkRegGest(18).Caption = Recursos(20)
    chkRegGest(19).Caption = Recursos(21)
    chkRegGest(20).Caption = Recursos(22)
    chkRegGest(21).Caption = Recursos(23)
    chkRegGest(22).Caption = Recursos(24)
    chkRegGest(27).Caption = Recursos(25)
    lblOblDist(5).Caption = Recursos(26)
    lblOblDist(6).Caption = Recursos(31)
    lblOblDist(7).Caption = Recursos(36)
    lblOblDist(8).Caption = Recursos(37)
    chkOblPedido(0).Caption = Recursos(34)
    chkOblPedido(2).Caption = Recursos(27)
    chkOblPedido(4).Caption = Recursos(28)
    chkOblPedido(6).Caption = Recursos(29)
    chkOblPedido(5).Caption = Recursos(30)
    chkOblPedido(3).Caption = Recursos(35)
    chkOblPedido(1).Caption = Recursos(32)
    chkHitosFacturacion.Caption = Recursos(33)
    lblIdiPers(4).Caption = Recursos(38)
    'Tab cat�logo
    chkAprovisionamiento(0).Caption = Recursos(39)
    label2(4).Caption = Recursos(40)
    label2(6).Caption = Recursos(41)
    label2(30).Caption = Recursos(42)
    opEspCat(0).Caption = Recursos(43)
    opEspCat(1).Caption = Recursos(44)
    opEspCat(2).Caption = Recursos(45)
    opEspCat(3).Caption = Recursos(46)
    opEspCat(4).Caption = Recursos(47)
    opEspCat(5).Caption = Recursos(48)
    lblDesvioRecepcion(5).Caption = Recursos(49)
    'Tab recepciones
    chkAprovisionamiento(5).Caption = Recursos(50)
    label2(24).Caption = Recursos(51)
    label2(25).Caption = Recursos(52)
    'Tab plantillas
    TabPlantPedidos.TabCaption(0) = Recursos(53)
    TabPlantPedidos.TabCaption(1) = Recursos(54)
    TabPlantPedidos.TabCaption(2) = Recursos(55)
    TabPlantPedidos.TabCaption(3) = Recursos(56)
    TabPlantPedidos.TabCaption(4) = Recursos(57)
    fraNotiPed(1).Caption = Recursos(58)
    LblCartaPed(4).Caption = Recursos(59)
    LblCartaPed(6).Caption = Recursos(60)
    LblCartaPed(0).Caption = Recursos(61)
    lblMailPed(0).Caption = Recursos(62)
    label2(1).Caption = Recursos(63)
    fraNotiPed(3).Caption = Recursos(64)
    label2(26).Caption = Recursos(65)
    chkNotifPed(0).Caption = Recursos(66)
    chkNotifPed(1).Caption = Recursos(67)
    fraNotiPed(2).Caption = Recursos(68)
    LblCartaPed(7).Caption = Recursos(69)
    lblMailPed(5).Caption = Recursos(70)
    label2(7).Caption = Recursos(71)
    LblCartaPed(1).Caption = Recursos(72)
    lblMailPed(1).Caption = Recursos(73)
    label2(2).Caption = Recursos(74)
    LblCartaPed(5).Caption = Recursos(75)
    fraRecepOK.Caption = Recursos(76)
    LblCartaPed(2).Caption = Recursos(77)
    lblMailPed(2).Caption = Recursos(78)
    fraRecepKO.Caption = Recursos(79)
    LblCartaPed(3).Caption = Recursos(80)
    lblMailPed(3).Caption = Recursos(81)
    lbl(0).Caption = Recursos(74)
    frmDatExt.Caption = Recursos(82)
    lblMailPed(4) = Recursos(83)
    lblbuzperiodo(3) = Recursos(84)
    label2(3) = Recursos(85)
    cdlArchivo.DialogTitle = Recursos(86)
    
    sCap(0) = Recursos(87)
    sCap(1) = Recursos(88)
    sCap(2) = Recursos(89)
    sCap(3) = Recursos(90)
    
    ctlConfPedCom.CargarRecursos RecursosCom
End Sub

Public Sub ModoEdicion(ByVal bModoEdicion As Boolean)
    fraAprovisionamiento(1).Enabled = bModoEdicion
    fraAprovisionamiento(4).Enabled = bModoEdicion
    fraAprovisionamiento(5).Enabled = bModoEdicion
    picPedidos(1).Enabled = bModoEdicion
    picPedidos(2).Enabled = bModoEdicion
    picPedidos(3).Enabled = bModoEdicion
    picPedidos(4).Enabled = bModoEdicion
    picPedidos(5).Enabled = bModoEdicion
    picPedidos(8).Enabled = bModoEdicion
    picPedidos(9).Enabled = bModoEdicion
    picPedidos(11).Enabled = bModoEdicion
    txtPedidoAbierto(1).Enabled = bModoEdicion
    
    Frame11(2).Enabled = bModoEdicion
    
    ctlConfPedCom.ModoEdicion bModoEdicion
End Sub

Private Sub chkAprovisionamiento_Click(Index As Integer)
    'Habilitar campo para codificaci�n personalizada de pedidos:
    If chkAprovisionamiento(2).Value = vbChecked Or chkAprovisionamiento(3).Value = vbChecked Then
        'Habilita el idioma y referencia
        sdbcIdioma(4).Enabled = True
        txtNombreAprov(0).Enabled = True
        txtNombreAprov(2).Enabled = True
    Else
        sdbcIdioma(4).Enabled = False
        txtNombreAprov(0).Enabled = False
        txtNombreAprov(2).Enabled = False
    End If
    
    If chkAprovisionamiento(3).Value = vbChecked Then
        chkAprovisionamiento(4).Enabled = True
    Else
        chkAprovisionamiento(4).Enabled = False
        chkAprovisionamiento(4).Value = vbUnchecked
    End If
    
    'Habilitar campo codificaci�n personalizada de recepciones:
    If chkAprovisionamiento(5).Value = vbChecked Then
        'Habilita el idioma y referencia
        sdbcIdioma(9).Enabled = True
        txtNombreAprov(3).Enabled = True
    Else
        sdbcIdioma(9).Enabled = False
        txtNombreAprov(3).Enabled = False
    End If
End Sub

Private Sub chkOblPedido_Click(Index As Integer)
    Select Case Index
        Case 4
            If chkOblPedido(Index) = vbChecked Then
                'se habilitara la caja de texto y la combo para introducir la denominaci�n que se dara a la funcionalidad
                sdbcIdioma(5).Enabled = True
                txtPedidoAbierto(0).Enabled = True
            Else
                sdbcIdioma(5).Enabled = False
                txtPedidoAbierto(0).Enabled = False
                txtPedidoAbierto(0).Text = ""
            End If
        End Select
End Sub

Private Sub chkRegGest_Click(Index As Integer)
    With m_oParametrosGenerales
        Select Case Index
            Case 2
                'Pedido abierto
                ctlConfPedCom.fraPedidoAbiertoVisible = (chkRegGest(2).Value = vbChecked)
            Case 13
                'Si se obliga a distribuir un PRESUP en pedidos la check de usar ese pres en pedidos tiene que estar seleccionada
                If (chkRegGest(13).Value = vbChecked) Then chkRegGest(17).Value = vbChecked
            Case 14
                'Si se obliga a distribuir un PRESUP en pedidos la check de usar ese pres en pedidos tiene que estar seleccionada
                If (chkRegGest(14).Value = vbChecked) Then chkRegGest(18).Value = vbChecked
            Case 15
                'Si se obliga a distribuir un PRESUP en pedidos la check de usar ese pres en pedidos tiene que estar seleccionada
                If (chkRegGest(15).Value = vbChecked) Then chkRegGest(19).Value = vbChecked
            Case 16
                'Si se obliga a distribuir un PRESUP en pedidos la check de usar ese pres en pedidos tiene que estar seleccionada
                If (chkRegGest(16).Value = vbChecked) Then chkRegGest(20).Value = vbChecked
            Case 17
                'Si no se usa un pres en pedidos se deschekea el usar y el nivel
                'Si se obliga a distribuir un PRESUP en pedidos la check de usar ese pres en pedidos tiene que estar seleccionada
                If (chkRegGest(17).Value = vbUnchecked) Then
                    chkRegGest(13).Value = vbUnchecked
                    cmbOblDist(5).ListIndex = -1
                    If chkRegGest(20).Value = vbUnchecked And chkRegGest(18).Value = vbUnchecked And chkRegGest(19).Value = vbUnchecked Then
                        chkRegGest(21).Value = vbUnchecked
                        chkRegGest(22).Value = vbUnchecked
                    End If
                End If
            Case 18
                'Si no se usa un pres en pedidos se deschekea el usar y el nivel
                If (chkRegGest(18).Value = vbUnchecked) Then
                    chkRegGest(14).Value = vbUnchecked
                    cmbOblDist(6).ListIndex = -1
                    If chkRegGest(17).Value = vbUnchecked And chkRegGest(20).Value = vbUnchecked And chkRegGest(19).Value = vbUnchecked Then
                        chkRegGest(21).Value = vbUnchecked
                        chkRegGest(22).Value = vbUnchecked
                    End If
                End If
            Case 19
                'Si no se usa un pres en pedidos se deschekea el usar y el nivel
                'Si se obliga a distribuir un PRESUP en pedidos la check de usar ese pres en pedidos tiene que estar seleccionada
                If (chkRegGest(19).Value = vbUnchecked) Then
                    chkRegGest(15).Value = vbUnchecked
                    cmbOblDist(7).ListIndex = -1
                    If chkRegGest(17).Value = vbUnchecked And chkRegGest(18).Value = vbUnchecked And chkRegGest(20).Value = vbUnchecked Then
                        chkRegGest(21).Value = vbUnchecked
                        chkRegGest(22).Value = vbUnchecked
                    End If
                End If
            Case 20
                'Si no se usa un pres en pedidos se deschekea el usar y el nivel
                'Si se obliga a distribuir un PRESUP en pedidos la check de usar ese pres en pedidos tiene que estar seleccionada
                If (chkRegGest(20).Value = vbUnchecked) Then
                    chkRegGest(16).Value = vbUnchecked
                    cmbOblDist(8).ListIndex = -1
                    If chkRegGest(17).Value = vbUnchecked And chkRegGest(18).Value = vbUnchecked And chkRegGest(19).Value = vbUnchecked Then
                        chkRegGest(21).Value = vbUnchecked
                        chkRegGest(22).Value = vbUnchecked
                    End If
                End If
            Case 22 'Sincronizar presupuestos del pedido con los items
                If (chkRegGest(22).Value = vbChecked) Then
                    If .gbUsarPres1 And .gbUsarPedPres1 Then
                        chkRegGest(13).Value = UserControl.Parent.chkRegGest(3)
                        cmbOblDist(5).ListIndex = UserControl.Parent.cmbOblDist(1).ListIndex
                    End If
                    If .gbUsarPres2 And .gbUsarPedPres2 Then
                        chkRegGest(14).Value = UserControl.Parent.chkRegGest(4)
                        cmbOblDist(6).ListIndex = UserControl.Parent.cmbOblDist(2).ListIndex
                    End If
                    If .gbUsarPres3 And .gbUsarPedPres3 Then
                        chkRegGest(15).Value = UserControl.Parent.chkRegGest(5)
                        cmbOblDist(7).ListIndex = UserControl.Parent.cmbOblDist(3).ListIndex
                    End If
                    If .gbUsarPres4 And .gbUsarPedPres4 Then
                        chkRegGest(16).Value = UserControl.Parent.chkRegGest(6)
                        cmbOblDist(8).ListIndex = UserControl.Parent.cmbOblDist(4).ListIndex
                    End If
                    fraPedPres.Enabled = False
                Else
                    fraPedPres.Enabled = True
                End If
            Case 27
                .gbOblAsigPresUonArtPeDir = IIf(chkRegGest(27).Value = vbChecked, True, False)
        End Select
    End With
End Sub

Private Sub cmbOblDist_Validate(Index As Integer, Cancel As Boolean)
    Select Case Index
        Case 5
            If cmbOblDist(5).Text <> "" And chkRegGest(17).Value = vbUnchecked Then chkRegGest(17).Value = vbChecked
        Case 6
            If cmbOblDist(6).Text <> "" And chkRegGest(18).Value = vbUnchecked Then chkRegGest(18).Value = vbChecked
        Case 7
            If cmbOblDist(7).Text <> "" And chkRegGest(19).Value = vbUnchecked Then chkRegGest(19).Value = vbChecked
        Case 8
            If cmbOblDist(8).Text <> "" And chkRegGest(20).Value = vbUnchecked Then chkRegGest(20).Value = vbChecked
    End Select
End Sub

''' <summary>Muestra una pantalla de seleccion de archivos</summary>
''' <param name="Index">Indicedel boton pulsado</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPlantilla_Click(Index As Integer)
    On Error GoTo Error
    
    cdlArchivo.FileName = ""



    If Index = 33 Then
        cdlArchivo.Filter = sCap(0) & " (*.xlt)|*.xlt"
        cdlArchivo.DefaultExt = "xlt"
    ElseIf Index = 30 Then
        cdlArchivo.Filter = sCap(0) & " (*.xls)|*.xls"
        cdlArchivo.DefaultExt = "xls"
    ElseIf Index = 42 Then
        cdlArchivo.Filter = sCap(1) & " (*.htm)|*.htm"
        cdlArchivo.DefaultExt = "htm"
    ElseIf Index = 41 Then
        cdlArchivo.Filter = sCap(2) & " (*.txt)|*.txt"
        cdlArchivo.DefaultExt = "txt"
    Else
        cdlArchivo.Filter = sCap(3) & " (*.dot)|*.dot"
        cdlArchivo.DefaultExt = "dot"
    End If
    
    cdlArchivo.Action = 1
    txtPlantilla(Index - 1) = cdlArchivo.FileName
    
    Exit Sub
Error:
    If Err.Number = cdlCancel Then
        Exit Sub
    Else
        Resume Next
    End If
End Sub

Private Sub sdbcIdioma_CloseUp(Index As Integer)
    Select Case Index
        Case 4
            sdbcIdiAprov_CloseUp
        Case 5
            sdbcIdiPedidoAbierto_CloseUp
        Case 9
            sdbcIdiRecep_CloseUp
    End Select
End Sub

Private Sub sdbcIdiAprov_CloseUp()
    Dim oLiterales As CLiterales

    If Not txtNombreAprov(0).Enabled Then
        'Estamos en modo consulta
        
        If sdbcIdioma(4).Value <> "" Then
            Screen.MousePointer = vbHourglass
            Set oLiterales = m_oGestorParametros.DevolverLiterales(31, 36, sdbcIdioma(4).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtNombreAprov(0).Text = oLiterales.Item(sdbcIdioma(4).Columns("ID").Value & CStr(31)).Den
                txtNombreAprov(2).Text = oLiterales.Item(sdbcIdioma(4).Columns("ID").Value & CStr(36)).Den
            Else
                txtNombreAprov(0).Text = ""
                txtNombreAprov(2).Text = ""
            End If
        End If
            
        Set oLiterales = Nothing
    Else
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
        If UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(4).Columns("ID").Value & CStr(31)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = m_oGestorParametros.DevolverLiterales(31, 31, sdbcIdioma(4).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtNombreAprov(0).Text = oLiterales.Item(1).Den
            Else
                txtNombreAprov(0).Text = ""
            End If
            UserControl.Parent.oLiteralesAModificar.Add sdbcIdioma(4).Columns("ID").Value, txtNombreAprov(0).Text, 31
            
            Set oLiterales = Nothing
        Else
            'Cargamos desde oLiteralesAModificar
            txtNombreAprov(0).Text = UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(4).Columns("ID").Value & CStr(31)).Den
        End If
        If UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(4).Columns("ID").Value & CStr(36)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = m_oGestorParametros.DevolverLiterales(36, 36, sdbcIdioma(4).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtNombreAprov(2).Text = oLiterales.Item(1).Den
            Else
                txtNombreAprov(2).Text = ""
            End If
            UserControl.Parent.oLiteralesAModificar.Add sdbcIdioma(4).Columns("ID").Value, txtNombreAprov(0).Text, 36
            
            Set oLiterales = Nothing
        Else
            'Cargamos desde oLiteralesAModificar
            txtNombreAprov(2).Text = UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(4).Columns("ID").Value & CStr(36)).Den
        End If
    End If
End Sub

Private Sub sdbcIdiPedidoAbierto_CloseUp()
    Dim oLiterales As CLiterales

    If Not txtPedidoAbierto(0).Enabled Then
        'Estamos en modo consulta
        If sdbcIdioma(5).Value <> "" Then
            Screen.MousePointer = vbHourglass
            Set oLiterales = m_oGestorParametros.DevolverLiterales(37, 37, sdbcIdioma(5).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtPedidoAbierto(0).Text = oLiterales.Item(sdbcIdioma(5).Columns("ID").Value & CStr(37)).Den
            Else
                txtPedidoAbierto(0).Text = ""
            End If
        End If
        Set oLiterales = Nothing
    Else
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
           
        If UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(5).Columns("ID").Value & CStr(37)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Set oLiterales = m_oGestorParametros.DevolverLiterales(37, 37, sdbcIdioma(5).Columns("ID").Value)
         
            Screen.MousePointer = vbHourglass
            Screen.MousePointer = vbNormal
            If oLiterales.Count > 0 Then
                txtPedidoAbierto(0).Text = oLiterales.Item(1).Den
            Else
                txtPedidoAbierto(0).Text = ""
            End If
            UserControl.Parent.oLiteralesAModificar.Add sdbcIdioma(5).Columns("ID").Value, txtPedidoAbierto(0).Text, 37
        Else
            'Cargamos desde oLiteralesAModificar
            txtPedidoAbierto(0).Text = UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(5).Columns("ID").Value & CStr(37)).Den
        End If
        Set oLiterales = Nothing
    End If
End Sub

Private Sub sdbcIdiRecep_CloseUp()
    Dim oLiterales As CLiterales

    If Not txtNombreAprov(3).Enabled Then
        'Estamos en modo consulta
        
        If sdbcIdioma(9).Value <> "" Then
            Screen.MousePointer = vbHourglass
            Set oLiterales = m_oGestorParametros.DevolverLiterales(47, 47, sdbcIdioma(9).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtNombreAprov(3).Text = oLiterales.Item(sdbcIdioma(9).Columns("ID").Value & CStr(47)).Den
            Else
                txtNombreAprov(3).Text = ""
            End If
        End If
            
        Set oLiterales = Nothing
        
    Else
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
        
        If UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(9).Columns("ID").Value & CStr(47)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = m_oGestorParametros.DevolverLiterales(47, 47, sdbcIdioma(9).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtNombreAprov(3).Text = oLiterales.Item(1).Den
            Else
                txtNombreAprov(3).Text = ""
            End If
            UserControl.Parent.oLiteralesAModificar.Add sdbcIdioma(9).Columns("ID").Value, txtNombreAprov(3).Text, 47
            
            Set oLiterales = Nothing
        Else
            'Cargamos desde oLiteralesAModificar
            txtNombreAprov(3).Text = UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(9).Columns("ID").Value & CStr(47)).Den
        End If
    End If
End Sub

Private Sub txtNombreAprov_LostFocus(Index As Integer)
    txtNombreAprov_Validate Index, False
End Sub

Private Sub txtNombreAprov_Validate(Index As Integer, Cancel As Boolean)
    Select Case Index
        Case 0
            UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(4).Columns("ID").Value & CStr(31)).Den = txtNombreAprov(0).Text
        Case 2
            UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(4).Columns("ID").Value & CStr(36)).Den = txtNombreAprov(2).Text
        Case 3
            UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(9).Columns("ID").Value & CStr(47)).Den = txtNombreAprov(3).Text
    End Select
End Sub

Private Sub txtPedidoAbierto_LostFocus(Index As Integer)
    txtPedidoAbierto_Validate Index, False
End Sub

Private Sub txtPedidoAbierto_Validate(Index As Integer, Cancel As Boolean)
    UserControl.Parent.oLiteralesAModificar.Item(sdbcIdioma(5).Columns("ID").Value & CStr(37)).Den = txtPedidoAbierto(0).Text
End Sub

Private Sub VScroll_Change()
    fraPedPres.Move fraPedPres.Left, -VScroll.Value * 80
End Sub
