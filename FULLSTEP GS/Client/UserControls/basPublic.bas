Attribute VB_Name = "basPublic"
Option Explicit

Public Sub PonerFieldSeparator(ByRef oControls As Object)
    Dim sdg As Control
    Dim Name4 As String
    Dim Name5 As String
        
    For Each sdg In oControls
        Name4 = LCase(Left(sdg.Name, 4))
        Name5 = LCase(Left(sdg.Name, 5))
        
        If Name4 = "sdbg" Or Name4 = "sdbc" Or Name5 = "sdbdd" Or Name4 = "ssdb" Then
            If sdg.DataMode = ssDataModeAddItem Then
                sdg.FieldSeparator = Chr(m_lSeparador)
            End If
        End If
    Next
End Sub
