VERSION 5.00
Begin VB.UserControl ctlConfGenPedidosCom 
   ClientHeight    =   4800
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5970
   ScaleHeight     =   4800
   ScaleWidth      =   5970
   Begin VB.Frame fraSep 
      Height          =   30
      Left            =   0
      TabIndex        =   13
      Top             =   3480
      Width           =   5970
   End
   Begin VB.Frame fraPedidosAbiertos 
      Caption         =   "DPedidos Abiertos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   270
      TabIndex        =   12
      Top             =   3600
      Width           =   5505
      Begin VB.CheckBox chkPubIntPA 
         Caption         =   "DPublicar cuando el pedido est� integrado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   9
         Top             =   600
         Width           =   3615
      End
      Begin VB.CheckBox chkActPubPA 
         Caption         =   "DActivar la publicaci�n de pedidos en Portal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   3975
      End
   End
   Begin VB.Frame fraNotifEmision 
      Enabled         =   0   'False
      Height          =   2175
      Left            =   240
      TabIndex        =   10
      Top             =   0
      Width           =   5505
      Begin VB.Frame fraOptNotif 
         BorderStyle     =   0  'None
         Height          =   1095
         Left            =   480
         TabIndex        =   11
         Top             =   960
         Width           =   4935
         Begin VB.OptionButton optAutom 
            Caption         =   "DAutom�ticamente cuando se integre el pedido"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   0
            TabIndex        =   3
            Top             =   0
            Width           =   3975
         End
         Begin VB.OptionButton optManual 
            Caption         =   "DManualmente desde el seguimiento"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   0
            TabIndex        =   4
            Top             =   360
            Width           =   3255
         End
         Begin VB.OptionButton optPreguntar 
            Caption         =   "DPreguntar si autom�tica o manualmente al emitir el pedido"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   0
            TabIndex        =   5
            Top             =   720
            Width           =   4815
         End
      End
      Begin VB.CheckBox chkActNotif 
         Caption         =   "DActivar la notificaci�n de pedido v�a e-mail al proveedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   4815
      End
      Begin VB.CheckBox chkNotifInt 
         Caption         =   "DNotificar cuando el pedido est� integrado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   2
         Top             =   600
         Width           =   4695
      End
   End
   Begin VB.Frame fraNotifPortal 
      Enabled         =   0   'False
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   2280
      Width           =   5505
      Begin VB.CheckBox chkActPub 
         Caption         =   "DActivar la publicaci�n de pedidos en Portal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   3975
      End
      Begin VB.CheckBox chkPubInt 
         Caption         =   "DPublicar cuando el pedido est� integrado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   7
         Top             =   600
         Width           =   3615
      End
   End
End
Attribute VB_Name = "ctlConfGenPedidosCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public ActPedAbierto As Boolean
Public IntegracionPedidos As Boolean

Private m_arRecursos As Variant

Public Property Get PedEmail() As Integer
    If chkActNotif.Value = 0 Then
        PedEmail = ComunicacionPedido.NoActivo
    Else
        If chkNotifInt.Value = 0 Then
            PedEmail = ComunicacionPedido.Activo
        Else
            PedEmail = ComunicacionPedido.ConPedidoIntegrado
        End If
    End If
End Property

Public Property Let PedEmail(ByVal iPedEmail As Integer)
    Select Case iPedEmail
        Case ComunicacionPedido.NoActivo
            chkActNotif.Value = 0
            chkNotifInt.Value = 0
        Case ComunicacionPedido.Activo
            chkActNotif.Value = 1
            chkNotifInt.Value = 0
        Case ComunicacionPedido.ConPedidoIntegrado
            chkActNotif.Value = 1
            chkNotifInt.Value = 1
    End Select
    
    fraOptNotif.Visible = (chkActNotif.Value = 1)
    
    ConfigurarControles
End Property

Public Property Get PedEmailModo() As Integer
    If optAutom.Value Then
        PedEmailModo = TipoComunicacionPedido.ComAuto
    ElseIf optManual.Value Then
        PedEmailModo = TipoComunicacionPedido.ComManual
    Else
        PedEmailModo = TipoComunicacionPedido.ComPreguntar
    End If
End Property

Public Property Let PedEmailModo(ByVal iPedEmailModo As Integer)
    Select Case iPedEmailModo
        Case TipoComunicacionPedido.ComAuto
            optAutom.Value = True
        Case TipoComunicacionPedido.ComManual
            optManual.Value = True
        Case TipoComunicacionPedido.ComPreguntar
            optPreguntar.Value = True
    End Select
End Property

Public Property Get PedPubPortal() As Integer
    If chkActPub.Value = 0 Then
        PedPubPortal = ComunicacionPedidoPORTAL.NoActivo
    Else
        If chkPubInt.Value = 0 Then
            PedPubPortal = ComunicacionPedidoPORTAL.Activo
        Else
            PedPubPortal = ComunicacionPedidoPORTAL.ConPedidoIntegrado
        End If
    End If
End Property

Public Property Let PedPubPortal(ByVal iPedPubPortal As Integer)
   Select Case iPedPubPortal
        Case ComunicacionPedidoPORTAL.NoActivo
            chkActPub.Value = 0
            chkPubInt.Value = 0
        Case ComunicacionPedidoPORTAL.Activo
            chkActPub.Value = 1
            chkPubInt.Value = 0
        Case ComunicacionPedidoPORTAL.ConPedidoIntegrado
            chkActPub.Value = 1
            chkPubInt.Value = 1
    End Select
End Property

Public Property Get PedPubPortalPA() As Integer
    If chkActPubPA.Value = 0 Then
        PedPubPortalPA = ComunicacionPedidoPORTAL.NoActivo
    Else
        If chkPubIntPA.Value = 0 Then
            PedPubPortalPA = ComunicacionPedidoPORTAL.Activo
        Else
            PedPubPortalPA = ComunicacionPedidoPORTAL.ConPedidoIntegrado
        End If
    End If
End Property

Public Property Let PedPubPortalPA(ByVal iPedPubPortalPA As Integer)
   Select Case iPedPubPortalPA
        Case ComunicacionPedidoPORTAL.NoActivo
            chkActPubPA.Value = 0
            chkPubIntPA.Value = 0
        Case ComunicacionPedidoPORTAL.Activo
            chkActPubPA.Value = 1
            chkPubIntPA.Value = 0
        Case ComunicacionPedidoPORTAL.ConPedidoIntegrado
            chkActPubPA.Value = 1
            chkPubIntPA.Value = 1
    End Select
End Property

Public Property Get fraPedidoAbiertoVisible() As Boolean
    fraPedidoAbiertoVisible = fraPedidosAbiertos.Visible
End Property

Public Property Let fraPedidoAbiertoVisible(ByVal bVisible As Boolean)
   fraPedidosAbiertos.Visible = bVisible
   fraSep.Visible = bVisible
End Property

Private Sub chkActNotif_Click()
    If chkActNotif.Value = 0 Then chkNotifInt.Value = 0
    fraOptNotif.Visible = (chkActNotif.Value = 1)
    ConfigurarControles
End Sub

Private Sub chkActPub_Click()
    If chkActPub.Value = 0 Then chkPubInt.Value = 0
    MostrarTextosOpciones (chkNotifInt.Value = 1)
End Sub

Private Sub chkActPubPA_Click()
    If chkActPubPA.Value = 0 Then chkPubIntPA.Value = 0
End Sub

Private Sub chkNotifInt_Click()
    If chkNotifInt.Value = 1 Then chkActNotif.Value = 1
    MostrarTextosOpciones (chkNotifInt.Value = 1)
End Sub

Private Sub chkPubInt_Click()
    If chkPubInt.Value = 1 Then chkActPub.Value = 1
End Sub

Private Sub chkPubIntPA_Click()
    If chkPubIntPA.Value = 1 Then chkActPubPA.Value = 1
End Sub

Private Sub UserControl_Resize()
    fraNotifEmision.Width = UserControl.Width - fraNotifEmision.Left - 240
    fraNotifPortal.Width = UserControl.Width - fraNotifPortal.Left - 240
    fraPedidosAbiertos.Width = UserControl.Width - fraPedidosAbiertos.Left - 240
    fraSep.Width = UserControl.Width
End Sub

Public Function CargarRecursos(ByVal arRecursos As Variant)
    m_arRecursos = arRecursos
    
    chkActNotif.Caption = m_arRecursos(0)
    chkNotifInt.Caption = m_arRecursos(1)
    MostrarTextosOpciones IntegracionPedidos
    chkActPub.Caption = m_arRecursos(5)
    chkActPubPA.Caption = m_arRecursos(5)
    chkPubInt.Caption = m_arRecursos(6)
    chkPubIntPA.Caption = m_arRecursos(6)
    fraPedidosAbiertos.Caption = m_arRecursos(9)
End Function

Private Sub MostrarTextosOpciones(ByVal bIntegracion As Boolean)
    If Not IsEmpty(m_arRecursos) Then
        If bIntegracion Then
            optAutom.Caption = m_arRecursos(2)
            optManual.Caption = m_arRecursos(3)
            optPreguntar.Caption = m_arRecursos(4)
        Else
            optAutom.Caption = m_arRecursos(7)
            optManual.Caption = m_arRecursos(3)
            optPreguntar.Caption = m_arRecursos(8)
        End If
    End If
End Sub

Public Sub ModoEdicion(ByVal bModoEdicion As Boolean)
    fraNotifEmision.Enabled = bModoEdicion
    fraNotifPortal.Enabled = bModoEdicion
    fraPedidosAbiertos.Enabled = bModoEdicion
End Sub

Private Sub UserControl_Show()
    If Ambient.UserMode Then
        chkNotifInt.Visible = IntegracionPedidos
        chkPubInt.Visible = IntegracionPedidos
        If ActPedAbierto Then
            fraPedidosAbiertos.Visible = True
            fraSep.Visible = True
            chkPubIntPA.Visible = IntegracionPedidos
        Else
            fraPedidosAbiertos.Visible = False
            fraSep.Visible = False
        End If
        ConfigurarControles
        MostrarTextosOpciones (chkNotifInt.Value = 1)
    End If
End Sub

Private Sub ConfigurarControles()
    Dim lTop As Long
    
    If chkActNotif.Value = 1 Then
        If chkNotifInt.Visible Then
            fraNotifEmision.Height = 2175
            lTop = chkNotifInt.Top + chkNotifInt.Height + 165
        Else
            fraNotifEmision.Height = 1815
            lTop = chkNotifInt.Top
        End If
        
        fraOptNotif.Top = lTop
    Else
        If IntegracionPedidos Then
            fraNotifEmision.Height = 1095
        Else
            fraNotifEmision.Height = 735
        End If
    End If
    
    If chkPubInt.Visible Then
        fraNotifPortal.Height = 1095
    Else
        fraNotifPortal.Height = 615
    End If
    
    If chkPubIntPA.Visible Then
        fraPedidosAbiertos.Height = 1095
    Else
        fraPedidosAbiertos.Height = 615
    End If
    
    fraNotifPortal.Top = fraNotifEmision.Top + fraNotifEmision.Height + 105
    fraSep.Top = fraNotifPortal.Top + fraNotifPortal.Height + 165
    fraPedidosAbiertos.Top = fraSep.Top + fraSep.Height + 165
End Sub
