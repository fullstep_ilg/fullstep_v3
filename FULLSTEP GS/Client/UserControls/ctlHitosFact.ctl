VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{22ACD161-99EB-11D2-9BB3-00400561D975}#1.0#0"; "PVCalendar9.ocx"
Begin VB.UserControl ctlHitosFact 
   ClientHeight    =   2400
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10230
   ScaleHeight     =   2400
   ScaleWidth      =   10230
   Begin VB.PictureBox PicBotones 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   10230
      TabIndex        =   7
      Top             =   2025
      Width           =   10230
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   0
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2250
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1125
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcClase 
      Height          =   285
      Left            =   1890
      TabIndex        =   5
      Top             =   30
      Width           =   3945
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5292
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6959
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Frame fraCalendar 
      Appearance      =   0  'Flat
      Height          =   2445
      Left            =   480
      TabIndex        =   0
      Top             =   690
      Visible         =   0   'False
      Width           =   2595
      Begin VB.CommandButton cmdCancelarFecha 
         Caption         =   "Cancelar"
         Height          =   285
         Left            =   1290
         TabIndex        =   2
         Top             =   2100
         Width           =   825
      End
      Begin VB.CommandButton cmdAceptarFecha 
         Caption         =   "Aceptar"
         Height          =   285
         Left            =   450
         TabIndex        =   1
         Top             =   2100
         Width           =   825
      End
      Begin PVATLCALENDARLib.PVCalendar PVCalendar 
         Height          =   1935
         Left            =   60
         TabIndex        =   3
         Top             =   150
         Width           =   2475
         _Version        =   524288
         BorderStyle     =   0
         Appearance      =   1
         FirstDay        =   1
         Frame           =   0
         SelectMode      =   1
         DisplayFormat   =   0
         DateOrientation =   8
         CustomTextOrientation=   2
         ImageOrientation=   5
         DOWText0        =   "Dom"
         DOWText1        =   "Lun"
         DOWText2        =   "Mar"
         DOWText3        =   "Mier"
         DOWText4        =   "Jue"
         DOWText5        =   "Vie"
         DOWText6        =   "Sab"
         MonthText0      =   "Enero"
         MonthText1      =   "Febrero"
         MonthText2      =   "Marzo"
         MonthText3      =   "Abril"
         MonthText4      =   "Mayo"
         MonthText5      =   "Junio"
         MonthText6      =   "Julio"
         MonthText7      =   "Agosto"
         MonthText8      =   "Septiembre"
         MonthText9      =   "Octubre"
         MonthText10     =   "Noviembre"
         MonthText11     =   "Diciembre"
         HeaderBackColor =   13160660
         HeaderForeColor =   0
         DisplayBackColor=   16777215
         DisplayForeColor=   0
         DayBackColor    =   16777215
         DayForeColor    =   0
         SelectedDayForeColor=   0
         SelectedDayBackColor=   12632256
         BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DOWFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DaysFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MultiLineText   =   -1  'True
         EditMode        =   1
         BeginProperty TextFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgHitos 
      Height          =   705
      Left            =   60
      TabIndex        =   11
      Top             =   360
      Width           =   10260
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   13
      stylesets.count =   8
      stylesets(0).Name=   "Heredado"
      stylesets(0).BackColor=   16777147
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlHitosFact.ctx":0000
      stylesets(1).Name=   "CamposModificados"
      stylesets(1).BackColor=   8421631
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "ctlHitosFact.ctx":001C
      stylesets(2).Name=   "gris"
      stylesets(2).BackColor=   12632256
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "ctlHitosFact.ctx":0038
      stylesets(3).Name=   "FondoGris"
      stylesets(3).BackColor=   12632256
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "ctlHitosFact.ctx":0054
      stylesets(4).Name=   "Normal"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "ctlHitosFact.ctx":0070
      stylesets(5).Name=   "NoHeredado"
      stylesets(5).BackColor=   8454143
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "ctlHitosFact.ctx":008C
      stylesets(6).Name=   "NoVigente"
      stylesets(6).BackColor=   14671839
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "ctlHitosFact.ctx":00A8
      stylesets(7).Name=   "Trasladar"
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "ctlHitosFact.ctx":00C4
      AllowAddNew     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   13
      Columns(0).Width=   2143
      Columns(0).Caption=   "DFecha"
      Columns(0).Name =   "FECHA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   1
      Columns(1).Width=   3200
      Columns(1).Caption=   "DDescripci�n del hito"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   40
      Columns(2).Width=   3200
      Columns(2).Caption=   "DClase de hito"
      Columns(2).Name =   "CLASE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   3
      Columns(3).Width=   3200
      Columns(3).Caption=   "DTipo de plan"
      Columns(3).Name =   "TIPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   3
      Columns(4).Width=   1244
      Columns(4).Caption=   "%"
      Columns(4).Name =   "PORCEN"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   5
      Columns(4).FieldLen=   256
      Columns(5).Width=   2434
      Columns(5).Caption=   "DValor factura"
      Columns(5).Name =   "VALOR"
      Columns(5).Alignment=   1
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   1482
      Columns(6).Caption=   "DMoneda"
      Columns(6).Name =   "MON"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   582
      Columns(7).Name =   "TRASLADAR"
      Columns(7).CaptionAlignment=   0
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   4
      Columns(7).ButtonsAlways=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "COD_CLASE"
      Columns(8).Name =   "COD_CLASE"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "COD_TIPO"
      Columns(9).Name =   "COD_TIPO"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ID"
      Columns(10).Name=   "ID"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   2
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "VALORMONPROCE"
      Columns(11).Name=   "VALORMONPROCE"
      Columns(11).Alignment=   1
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "FECHA_VALIDADA"
      Columns(12).Name=   "FECHA_VALIDADA"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      _ExtentX        =   18098
      _ExtentY        =   1244
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddClase 
      Height          =   795
      Left            =   4740
      TabIndex        =   12
      Top             =   1140
      Width           =   2265
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlHitosFact.ctx":06E6
      DividerStyle    =   3
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   212
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5292
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3995
      _ExtentY        =   1402
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTipo 
      Height          =   795
      Left            =   7200
      TabIndex        =   13
      Top             =   1140
      Width           =   2265
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlHitosFact.ctx":0702
      DividerStyle    =   3
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   212
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   8811
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3995
      _ExtentY        =   1402
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lbl 
      Caption         =   "DImporte de Linea: 999.999,00 EUR"
      Height          =   195
      Index           =   1
      Left            =   6030
      TabIndex        =   6
      Top             =   90
      Width           =   4035
   End
   Begin VB.Label lbl 
      Caption         =   "DClase de plan:"
      Height          =   255
      Index           =   0
      Left            =   90
      TabIndex        =   4
      Top             =   60
      Width           =   1725
   End
End
Attribute VB_Name = "ctlHitosFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public g_dblImporteLinea As Double
Public g_sMoneda As String
Public g_oLineaSeleccionada As CLineaPedido
Public oMensajes As CMensajes
Public garSimbolos As Variant
Public g_oHitosGrid As CHitosFact
Public gsZINV As String
Public gsZANT As String
Public gbDesdeSeguimiento As Boolean
Public g_bMonProce As Boolean
Public gParametrosIntegracion As Variant
Public gbModoAdd As Boolean
Public gURLSrvInt As String
Public FSGSRaiz As CRaiz
Public g_bValidando As Boolean


Private m_dtFechaAModificar As Date
Private m_sLiterales(0 To 7) As String
Private m_iCont As Integer
Private m_bModoEdicion As Boolean
Private m_bSaltarValidar As Boolean
Private m_bValidacionHitosOk As Boolean

Public Property Let SaltarValidar(ByVal dato As Boolean)
    m_bSaltarValidar = dato
End Property
''' ID del Hito
Public Property Get SaltarValidar() As Boolean
    SaltarValidar = m_bSaltarValidar
End Property

Public Property Let ValidacionHitosOk(ByVal dato As Boolean)
    m_bValidacionHitosOk = dato
End Property
''' ID del Hito
Public Property Get ValidacionHitosOk() As Boolean
    ValidacionHitosOk = m_bValidacionHitosOk
End Property

Private Sub BorrarHito()
    Dim i As Integer
    For i = 1 To g_oLineaSeleccionada.HitosFac.Count
        If sdbgHitos.Columns("ID").Value = g_oLineaSeleccionada.HitosFac.Item(i).Id Then
            If gbDesdeSeguimiento Then
                g_oLineaSeleccionada.HitosFac.Item(i).AccionEdicion = tipoaccionedicion.Eliminacion
            Else
                g_oLineaSeleccionada.HitosFac.Remove i
            End If
            Exit For
        End If
    Next
    For i = 1 To g_oHitosGrid.Count
        If sdbgHitos.Columns("ID").Value = g_oHitosGrid.Item(i).Id Then
            If gbDesdeSeguimiento Then
                g_oHitosGrid.Item(i).AccionEdicion = tipoaccionedicion.Eliminacion
            Else
                g_oHitosGrid.Remove i
            End If
            Exit For
        End If
    Next
    CargarGrid
End Sub

Public Sub CargarGrid(Optional ByVal bEstablecerEdicionHito As Boolean = True)
    Dim oHito As CHitoFact
        
    VaciarGrid
    
    For Each oHito In g_oHitosGrid
        If Not gbDesdeSeguimiento Or (gbDesdeSeguimiento And oHito.AccionEdicion <> tipoaccionedicion.Eliminacion) Then
            sdbddClase_InitColumnProps
            sdbddClase_PositionList oHito.Clase
            
            sdbgHitos.AddItem oHito.Fecha & Chr(m_lSeparador) & oHito.Den & Chr(m_lSeparador) & SacarDenClase(oHito.Clase) & Chr(m_lSeparador) & _
                    SacarDenTipo(oHito.TipoPlan) & Chr(m_lSeparador) & IIf(oHito.TipoPlan = 1 Or oHito.TipoPlan = 4, Format(oHito.Porcen, "standard"), "") & Chr(m_lSeparador) & _
                    Format(oHito.Valor, "standard") & Chr(m_lSeparador) & g_sMoneda & Chr(m_lSeparador) & Chr(m_lSeparador) & oHito.Clase & Chr(m_lSeparador) & oHito.TipoPlan & _
                    Chr(m_lSeparador) & oHito.Id & Chr(m_lSeparador) & Format(oHito.ValorMonProce, "standard") & Chr(m_lSeparador) & "1"
        End If
    Next
    
    'Establecer la edici�n de la primera l�nea (es en la que se sit�a por defecto el grid)
    If m_bModoEdicion And sdbgHitos.Rows > 0 And bEstablecerEdicionHito Then EstablecerEdicionLinea sdbgHitos.AddItemBookmark(0)
End Sub

Private Sub GridRowColChange()
With sdbgHitos
    If .Col > -1 Then
        Select Case .Columns("COD_TIPO").Value
            Case 2, 5   'Por Valor
                .Columns("PORCEN").Locked = True
                .Columns("VALOR").Locked = False
                .Columns("VALORMONPROCE").Locked = False
            Case 1, 4   'Por Porcentaje
                .Columns("PORCEN").Locked = False
                .Columns("VALOR").Locked = True
                .Columns("VALORMONPROCE").Locked = True
        End Select
        Select Case .Columns(.Col).Name
            Case "CLASE"
                .Columns(.Col).DropDownHwnd = sdbddClase.hWnd
                .Columns(.Col).Locked = True
            Case "TIPO"
                .Columns(.Col).DropDownHwnd = sdbddTipo.hWnd
                .Columns(.Col).Locked = True
        End Select
    End If
End With
End Sub

Private Sub ModificarHito()
    Dim b As Boolean
    b = False

    With g_oLineaSeleccionada.HitosFac.Item(CStr(sdbgHitos.Columns("ID").Value))
        If .Fecha <> sdbgHitos.Columns("FECHA").Value Then
            b = True
            'Hay que reordenar la grid porque puede haber movimiento de fechas
        End If
        .Fecha = sdbgHitos.Columns("FECHA").Value
        .Den = sdbgHitos.Columns("DEN").Value
        .Clase = sdbgHitos.Columns("COD_CLASE").Value
        .TipoPlan = sdbgHitos.Columns("COD_TIPO").Value
        .Porcen = IIf(sdbgHitos.Columns("PORCEN").Value <> "", sdbgHitos.Columns("PORCEN").Value, Null)
        .Valor = sdbgHitos.Columns("Valor").Value
        .ValorMonProce = sdbgHitos.Columns("ValorMONPROCE").Value
        If .AccionEdicion <> tipoaccionedicion.Alta Then .AccionEdicion = tipoaccionedicion.Modificacion
    End With

    If b Then
        OrdenarHitos (False)
    Else
        With g_oHitosGrid.Item(CStr(sdbgHitos.Columns("ID").Value))
            .Fecha = sdbgHitos.Columns("FECHA").Value
            .Den = sdbgHitos.Columns("DEN").Value
            .Clase = sdbgHitos.Columns("COD_CLASE").Value
            .TipoPlan = sdbgHitos.Columns("COD_TIPO").Value
            .Porcen = IIf(sdbgHitos.Columns("PORCEN").Value <> "", sdbgHitos.Columns("PORCEN").Value, Null)
            .Valor = sdbgHitos.Columns("Valor").Value
            .ValorMonProce = sdbgHitos.Columns("ValorMONPROCE").Value
            .AccionEdicion = tipoaccionedicion.Modificacion
            If .AccionEdicion <> tipoaccionedicion.Alta Then .AccionEdicion = tipoaccionedicion.Modificacion
        End With
    End If
End Sub

Private Sub VaciarGrid()
    With sdbgHitos
        .RemoveAll
        .Columns("CLASE").DropDownHwnd = 0
        .Columns("CLASE").Locked = False
        .Columns("TIPO").DropDownHwnd = 0
        .Columns("TIPO").Locked = False
    End With
End Sub

Private Sub OrdenarHitos(ByVal bInsertar As Boolean)
Dim oHito As CHitoFact
Dim i  As Integer
Dim bInsertada As Boolean
Dim iId As Integer
bInsertada = False
'Borramos la colecci�n que usaremos para tener la coleccion ordenada en todo momento
For i = 1 To g_oHitosGrid.Count
    g_oHitosGrid.Remove 1
Next i
If bInsertar Then
    iId = m_iCont
Else
    iId = sdbgHitos.Columns("ID").Value
End If

If g_oLineaSeleccionada.HitosFac.Count > 0 Then
    For Each oHito In g_oLineaSeleccionada.HitosFac
        If Not bInsertada And sdbgHitos.Row <> -1 And CDbl(CVDate(sdbgHitos.Columns("FECHA").Value)) < CDbl(CVDate(oHito.Fecha)) Then
            Dim iAccion As tipoaccionedicion
            If bInsertar Then
                iAccion = tipoaccionedicion.Alta
            Else
                iAccion = g_oLineaSeleccionada.HitosFac.Item(CStr(iId)).AccionEdicion
            End If
            g_oHitosGrid.Add iId, sdbgHitos.Columns("FECHA").Value, sdbgHitos.Columns("DEN").Value, sdbgHitos.Columns("COD_CLASE").Value, _
                             sdbgHitos.Columns("COD_TIPO").Value, sdbgHitos.Columns("PORCEN").Value, sdbgHitos.Columns("VALOR").Value, iAccion, sdbgHitos.Columns("VALORMONPROCE").Value
            g_oHitosGrid.Add oHito.Id, oHito.Fecha, oHito.Den, oHito.Clase, oHito.TipoPlan, oHito.Porcen, oHito.Valor, oHito.AccionEdicion, oHito.ValorMonProce
            bInsertada = True
        Else
            If iId <> oHito.Id Then
                g_oHitosGrid.Add oHito.Id, oHito.Fecha, oHito.Den, oHito.Clase, oHito.TipoPlan, oHito.Porcen, oHito.Valor, oHito.AccionEdicion, oHito.ValorMonProce
            End If
        End If
    Next
Else
    g_oHitosGrid.Add iId, sdbgHitos.Columns("FECHA").Value, sdbgHitos.Columns("DEN").Value, sdbgHitos.Columns("COD_CLASE").Value, _
                             sdbgHitos.Columns("COD_TIPO").Value, sdbgHitos.Columns("PORCEN").Value, sdbgHitos.Columns("VALOR").Value, tipoaccionedicion.Alta, sdbgHitos.Columns("VALORMONPROCE").Value
    bInsertada = True
End If
If Not bInsertada Then
   g_oHitosGrid.Add iId, sdbgHitos.Columns("FECHA").Value, sdbgHitos.Columns("DEN").Value, sdbgHitos.Columns("COD_CLASE").Value, _
                             sdbgHitos.Columns("COD_TIPO").Value, sdbgHitos.Columns("PORCEN").Value, sdbgHitos.Columns("VALOR").Value, IIf(bInsertar, tipoaccionedicion.Alta, tipoaccionedicion.Modificacion), sdbgHitos.Columns("VALORMONPROCE").Value
End If
For i = 1 To g_oLineaSeleccionada.HitosFac.Count
    g_oLineaSeleccionada.HitosFac.Remove 1
Next i
For Each oHito In g_oHitosGrid
    g_oLineaSeleccionada.HitosFac.Add oHito.Id, oHito.Fecha, oHito.Den, oHito.Clase, oHito.TipoPlan, oHito.Porcen, oHito.Valor, oHito.AccionEdicion, oHito.ValorMonProce
Next
If bInsertar Then
    m_iCont = m_iCont - 1
End If

End Sub

Private Function SacarDenClase(ByVal sClase As String) As String
Dim s As String
Select Case sClase
    Case "ZANT"
        s = m_sLiterales(0)
    Case "ZINV"
        s = m_sLiterales(1)
End Select
SacarDenClase = s
End Function

Private Function SacarDenTipo(ByVal sTipo As String) As String
Dim s As String
Select Case sTipo
    Case "1"
        s = m_sLiterales(2)
    Case "2"
        s = m_sLiterales(3)
    Case "4"
        s = m_sLiterales(4)
    Case "5"
        s = m_sLiterales(5)
End Select
SacarDenTipo = s
End Function

Private Sub cmdAceptarFecha_Click()
sdbgHitos.Columns("FECHA").Value = PVCalendar.Value
sdbgHitos.Columns("FECHA_VALIDADA").Value = "0"
fraCalendar.Visible = False
sdbgHitos.Enabled = True
PicBotones.Enabled = True
End Sub

Private Sub cmdA�adir_Click()
sdbgHitos.AddNew
End Sub

Private Sub cmdCancelarFecha_Click()
fraCalendar.Visible = False
sdbgHitos.Enabled = True
PicBotones.Enabled = True
End Sub

Private Sub cmdDeshacer_Click()
    sdbgHitos.CancelUpdate
    cmdDeshacer.Enabled = False
End Sub

Private Sub cmdEliminar_Click()
BorrarHito
End Sub

Private Sub sdbcClase_CloseUp()
    g_oLineaSeleccionada.ClaseHitosFac = sdbcClase.Value
End Sub

Private Sub sdbcClase_InitColumnProps()
    sdbcClase.DataFieldList = "Column 0"
    sdbcClase.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcClase_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
    Case 8, 46
        sdbcClase.Text = ""
End Select
End Sub

Private Sub sdbcClase_LostFocus()
    If sdbcClase.Text <> "" Then
        g_oLineaSeleccionada.ClaseHitosFac = sdbcClase.Columns(0).Value
    End If
End Sub

Private Sub sdbddClase_CloseUp()
sdbgHitos.Columns("COD_CLASE").Value = sdbddClase.Columns("COD").Value
sdbgHitos.Columns("CLASE").Value = sdbddClase.Columns(1).Value
Select Case sdbddClase.Columns("COD").Value
    Case "ZANT"
        If sdbgHitos.Columns("TIPO").Text = m_sLiterales(2) Or _
           sdbgHitos.Columns("TIPO").Text = m_sLiterales(3) Then
            sdbgHitos.Columns("TIPO").Value = ""
        End If
    Case "ZINV"
        If sdbgHitos.Columns("TIPO").Text = m_sLiterales(4) Or _
           sdbgHitos.Columns("TIPO").Text = m_sLiterales(5) Then
            sdbgHitos.Columns("TIPO").Value = ""
        End If
End Select

End Sub

Private Sub sdbddClase_DropDown()
sdbddClase.RemoveAll
sdbddClase.AddItem "ZANT" & Chr(m_lSeparador) & m_sLiterales(0)
sdbddClase.AddItem "ZINV" & Chr(m_lSeparador) & m_sLiterales(1)
End Sub

Private Sub sdbddClase_InitColumnProps()
    sdbddClase.DataFieldList = "Column 0"
    sdbddClase.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddClase_PositionList(ByVal Text As String)
Dim i As Long
Dim bm As Variant
On Error Resume Next
With sdbddClase
    .MoveFirst
    If Text <> "" Then
        For i = 0 To .Rows - 1
            bm = .GetBookmark(i)
            If UCase(Text) = UCase(.Columns(0).CellText(bm)) Or UCase(Text) = UCase(SacarDenClase(.Columns(0).CellText(bm))) Then
                .Bookmark = bm
                Exit For
            End If
        Next i
    End If
End With

End Sub

Private Sub sdbddTipo_CloseUp()
If sdbddTipo.Columns(0).Value = "" Then Exit Sub
sdbgHitos.Columns("COD_TIPO").Value = CInt(sdbddTipo.Columns(0).Value)
sdbgHitos.Columns("TIPO").Value = sdbddTipo.Columns(1).Value
Select Case CInt(sdbddTipo.Columns(0).Value)
    Case 2, 5
        sdbgHitos.Columns("PORCEN").Value = ""
        sdbgHitos.Columns("PORCEN").Locked = True
        sdbgHitos.Columns("VALOR").Locked = False
        sdbgHitos.Columns("VALORMONPROCE").Locked = False
    Case 1, 4
        sdbgHitos.Columns("PORCEN").CellStyleSet ""
        sdbgHitos.Columns("PORCEN").Locked = False
        sdbgHitos.Columns("VALOR").Locked = True
        sdbgHitos.Columns("VALORMONPROCE").Locked = True
End Select
End Sub

Private Sub sdbddTipo_DropDown()
    With sdbddTipo
        .RemoveAll
        Select Case sdbgHitos.Columns("COD_CLASE").Value
            Case "ZINV"    'Facturaci�n parcial
                .AddItem TipoPlanHitoFac.FacturacionParcialPorcentual & Chr(m_lSeparador) & m_sLiterales(2)
                .AddItem TipoPlanHitoFac.FacturacionParcialValor & Chr(m_lSeparador) & m_sLiterales(3)
            Case "ZANT"    'Anticipo
                .AddItem TipoPlanHitoFac.AnticipoParcialPorcentual & Chr(m_lSeparador) & m_sLiterales(4)
                .AddItem TipoPlanHitoFac.AnticipoParcialValor & Chr(m_lSeparador) & m_sLiterales(5)
        End Select
    End With
End Sub

Private Sub sdbddTipo_InitColumnProps()
    sdbddTipo.DataFieldList = "Column 0"
    sdbddTipo.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbgHitos_AfterUpdate(RtnDispErrMsg As Integer)
CargarGrid False
cmdDeshacer.Enabled = False
End Sub

Private Sub sdbgHitos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
If sdbgHitos.IsAddRow Then sdbgHitos.Columns("MON").Text = g_sMoneda

End Sub

Private Sub sdbgHitos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    BorrarHito
End Sub

Private Sub sdbgHitos_BeforeInsert(Cancel As Integer)
cmdDeshacer.Enabled = True
End Sub

Private Sub sdbgHitos_BeforeRowColChange(Cancel As Integer)
With sdbgHitos
    If .Col = -1 Then Exit Sub
    If g_bValidando Then Exit Sub
    g_bValidando = True
    Select Case .Columns(.Col).Name
        Case "FECHA"
            If .Columns(.Col).Value <> "" Then
                If Not IsDate(.Columns(.Col).Value) Then
                    oMensajes.FechaNoValida
                    Cancel = True
                    g_bValidando = False
                    Exit Sub
                End If
            End If
        Case "PORCEN"
            If (Not IsNumeric(.Columns("PORCEN").Text) Or .Columns("PORCEN").Value > 100) And .Columns("PORCEN").Text <> "" Then
                oMensajes.PorcentajeVacio
                Cancel = True
            Else
                If (.Columns("TIPO").Text = m_sLiterales(2) Or .Columns("TIPO").Text = m_sLiterales(4)) Then
                    If CDbl(.Columns("PORCEN").Value) <> Round(.Columns("PORCEN").Value, 2) Then
                        oMensajes.No_mas_de_2Decimales (1549)
                        Cancel = True
                        g_bValidando = False
                        Exit Sub
                    End If
                End If
                If .Columns(.Col).Value <> "" Then
                    If Not g_bMonProce Then
                        .Columns("VALOR").Value = Round(CDec(g_dblImporteLinea) * (CDec(.Columns(.Col).Value) / 100), 2)
                        .Columns("VALORMONPROCE").Value = Round(.Columns("VALOR").Value / g_oLineaSeleccionada.CambioMonOferta, 2)
                    Else
                        .Columns("VALORMONPROCE").Value = Round(CDec(g_dblImporteLinea) * (CDec(.Columns(.Col).Value) / 100), 2)
                        .Columns("VALOR").Value = Round(.Columns("VALORMONPROCE").Value * g_oLineaSeleccionada.CambioMonOferta, 2)
                    End If
                End If
            End If
        Case "VALOR"
            If IsNumeric(.Columns("VALOR").Value) Then
                If CDbl(.Columns("VALOR").Value) <> Round(.Columns("VALOR").Value, 2) Then
                    oMensajes.No_mas_de_2Decimales (1550)
                    Cancel = True
                    g_bValidando = False
                    Exit Sub
                End If
                .Columns("VALORMONPROCE").Value = Round(.Columns("VALOR").Value / g_oLineaSeleccionada.CambioMonOferta, 2)
            Else
                oMensajes.ValorVacio
                Cancel = True
            End If
        Case "VALORMONPROCE"
            If IsNumeric(.Columns("VALORMONPROCE").Value) Then
                If CDbl(.Columns("VALORMONPROCE").Value) <> Round(.Columns("VALORMONPROCE").Value, 2) Then
                    oMensajes.No_mas_de_2Decimales (1550)
                    g_bValidando = False
                    Cancel = True
                    Exit Sub
                End If
                .Columns("VALOR").Value = Round(.Columns("VALORMONPROCE").Value * g_oLineaSeleccionada.CambioMonOferta, 2)
            End If
    End Select
    g_bValidando = False
End With
End Sub

Private Sub sdbgHitos_BeforeUpdate(Cancel As Integer)
    If Validar Then
        If sdbgHitos.IsAddRow Then
            OrdenarHitos (True)
        Else
            ModificarHito
        End If
    Else
        Cancel = True
        sdbgHitos.SetFocus
    End If
End Sub

Private Sub sdbgHitos_BtnClick()
    If Not m_bModoEdicion And gbDesdeSeguimiento Then Exit Sub
    
    With sdbgHitos
        If NullToStr(.Columns("ID").Value) <> "" Then
            If Not g_oLineaSeleccionada.HitosFac.Item(CStr(.Columns("ID").Value)).Editable Then Exit Sub
        End If
        
        Select Case .Columns(sdbgHitos.Col).Name
            Case "FECHA"
                m_dtFechaAModificar = Date
                PVCalendar.Value = Date
                If IsDate(.Columns("FECHA").Value = "") Then
                    m_dtFechaAModificar = CDate(.Columns("FECHA").Value)
                End If
                PVCalendar.Value = m_dtFechaAModificar
                fraCalendar.Visible = True
                fraCalendar.ZOrder
                PicBotones.Enabled = False
                .Enabled = False
            Case "TRASLADAR"
                If Not sdbgHitos.IsAddRow Then CopiarHitoSiguienteRegistro
        End Select
    End With
End Sub

Private Sub CopiarHitoSiguienteRegistro()
Dim oHito As CHitoFact
Dim b As Boolean
Dim Fecha As Date
Dim Den As String
Dim Clase As String
Dim TipoPlan As Integer
Dim Porcen As Variant
Dim Valor As Double
Dim ValorMonProce As Double
Dim DenTipo As String
Dim DenClase As String

Fecha = sdbgHitos.Columns("FECHA").Value
Den = sdbgHitos.Columns("DEN").Value
Clase = sdbgHitos.Columns("COD_CLASE").Value
DenClase = sdbgHitos.Columns("CLASE").Text
DenTipo = sdbgHitos.Columns("TIPO").Text
TipoPlan = sdbgHitos.Columns("COD_TIPO").Value
Porcen = IIf(sdbgHitos.Columns("PORCEN").Value <> "", sdbgHitos.Columns("PORCEN").Value, Null)
Valor = sdbgHitos.Columns("Valor").Value
ValorMonProce = sdbgHitos.Columns("ValorMonProce").Value
b = False
For Each oHito In g_oLineaSeleccionada.HitosFac
    If b Then
        oHito.Fecha = Fecha
        oHito.Den = Den
        oHito.Clase = Clase
        oHito.TipoPlan = TipoPlan
        oHito.Porcen = IIf(Porcen <> "", Porcen, Null)
        oHito.Valor = Valor
        oHito.ValorMonProce = ValorMonProce
        'Una vez copiado salimos del m�todo
        GoTo Salir
    End If
    If sdbgHitos.Columns("ID").Value = oHito.Id Then
        'Tenemos que copiar en el siguiente hito lo que hay en la grid
        b = True
    End If
Next
'Si llega a esta parte queire decir que hay que insertar una l�nea nueva
sdbgHitos.AddNew
sdbgHitos.Columns("CLASE").DropDownHwnd = 0
sdbgHitos.Columns("CLASE").Locked = False
sdbgHitos.Columns("TIPO").DropDownHwnd = 0
sdbgHitos.Columns("TIPO").Locked = False
sdbgHitos.Columns("FECHA").Value = Fecha
sdbgHitos.Columns("DEN").Value = Den
sdbgHitos.Columns("COD_CLASE").Value = Clase
sdbgHitos.Columns("COD_TIPO").Value = TipoPlan
sdbgHitos.Columns("TIPO").Value = DenTipo
sdbgHitos.Columns("CLASE").Value = DenClase
sdbgHitos.Columns("PORCEN").Value = IIf(Porcen <> "", Porcen, Null)
sdbgHitos.Columns("Valor").Value = Valor
sdbgHitos.Columns("ValorMonProce").Value = ValorMonProce
sdbgHitos.Update
Exit Sub
Salir:
    If b Then
        OrdenarHitos (Not b)
        CargarGrid
    End If
End Sub

Private Sub sdbgHitos_Change()
cmdDeshacer.Enabled = True
With sdbgHitos
    Select Case .Columns(.Col).Name
        Case "PORCEN"
            If (.Columns("TIPO").Text = m_sLiterales(2) Or .Columns("TIPO").Text = m_sLiterales(4)) And IsNumeric(.Columns("PORCEN").Value) Then
                If .Columns(.Col).Value <> "" Then
                    If Not g_bMonProce Then
                        .Columns("VALOR").Value = Round(CDec(g_dblImporteLinea) * (CDec(.Columns(.Col).Value) / 100), 2)
                        .Columns("VALORMONPROCE").Value = Round(.Columns("VALOR").Value / g_oLineaSeleccionada.CambioMonOferta, 2)
                    Else
                        .Columns("VALORMONPROCE").Value = Round(CDec(g_dblImporteLinea) * (CDec(.Columns(.Col).Value) / 100), 2)
                        .Columns("VALOR").Value = Round(.Columns("VALORMONPROCE").Value * g_oLineaSeleccionada.CambioMonOferta, 2)
                    End If
                End If
            End If
    End Select
End With
End Sub

Private Sub sdbgHitos_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case 27
        sdbgHitos.CancelUpdate
End Select
End Sub

Public Function RealizarCambiosPdtes() As Boolean
    If sdbgHitos.DataChanged Then
        If Validar() Then
            m_bValidacionHitosOk = True
            sdbgHitos.Update
        Else
            m_bValidacionHitosOk = False
            If sdbgHitos.Visible Then
                sdbgHitos.SetFocus
            End If
        End If
    End If
    RealizarCambiosPdtes = Not sdbgHitos.DataChanged
End Function

Private Sub sdbgHitos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bEditable As Boolean
    
    With sdbgHitos
        GridRowColChange
        If gbDesdeSeguimiento Then
            bEditable = True
            If m_bModoEdicion And Val(.AddItemBookmark(.Row)) <> Val(IIf(IsNull(LastRow), -2, LastRow)) Then EstablecerEdicionLinea .AddItemBookmark(.Row)
        End If
    End With
End Sub

Private Sub sdbgHitos_RowLoaded(ByVal Bookmark As Variant)
    With sdbgHitos
        .Columns("TRASLADAR").CellStyleSet "Trasladar"
            
        If g_oLineaSeleccionada.Baja_LOG Then
            .Columns("FECHA").CellStyleSet "FondoGris"
            .Columns("DEN").CellStyleSet "FondoGris"
            .Columns("CLASE").CellStyleSet "FondoGris"
            .Columns("TIPO").CellStyleSet "FondoGris"
            .Columns("PORCEN").CellStyleSet "FondoGris"
            .Columns("VALOR").CellStyleSet "FondoGris"
            .Columns("VALORMONPROCE").CellStyleSet "FondoGris"
            .Columns("MON").CellStyleSet "FondoGris"
        End If
    End With
End Sub

Private Sub sdbgHitos_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
    If m_bModoEdicion Then EstablecerEdicionLinea sdbgHitos.AddItemBookmark(sdbgHitos.Row)
End Sub

Private Sub EstablecerEdicionLinea(ByVal vRow As Variant)
    Dim bEditable  As Boolean
    
    bEditable = True
    
    With sdbgHitos
        If gbDesdeSeguimiento Then
            If gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) Or gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_Aprov) Then
                'Comprobar si el hito es editable
                If .IsAddRow Or gbModoAdd Then
                    'Si se trata de un hito nuevo o est� en modo a�adir l�neas los hitos siempre editables
                    bEditable = True
                Else
                    If NullToStr(.Columns("ID").CellValue(vRow)) <> "" Then
                        If g_oLineaSeleccionada.HitosFac.Item(CStr(.Columns("ID").CellValue(vRow))).AccionEdicion = tipoaccionedicion.Alta Then
                            g_oLineaSeleccionada.HitosFac.Item(CStr(.Columns("ID").CellValue(vRow))).Editable = True
                        Else
                            bEditable = ComprobarEdicionHito(g_oLineaSeleccionada.ObjetoOrden.Empresa, g_oLineaSeleccionada.ObjetoOrden.Id, g_oLineaSeleccionada.Id, .Columns("ID").CellValue(vRow))
                            g_oLineaSeleccionada.HitosFac.Item(CStr(.Columns("ID").CellValue(vRow))).Editable = bEditable
                        End If
                    End If
                End If
            End If
        End If
        
        Dim i As Integer
        For i = 0 To .Columns.Count - 1
            .Columns(i).Locked = Not bEditable
        Next
        
        If bEditable Then
            cmdEliminar.Enabled = True
            GridRowColChange
        Else
            cmdEliminar.Enabled = False
            
            .Columns("CLASE").DropDownHwnd = 0
            .Columns("TIPO").DropDownHwnd = 0
        End If
    End With
End Sub

Private Sub UserControl_Resize()
If UserControl.Height < 1200 Then Exit Sub
With sdbgHitos
    If UserControl.Width < 5980 Then
        UserControl.Width = 5980
    End If

    If UserControl.Height < 1000 Then
        UserControl.Height = 1000
    End If

    If .Width > 400 Then
        .Width = Width - 100
    End If
    
    If PicBotones.Top > 1815 Then
        .Height = PicBotones.Top - 500
    End If
    
    .Columns("FECHA").Width = .Width * 0.1
    .Columns("DEN").Width = .Width * 0.25
    .Columns("CLASE").Width = .Width * 0.18
    .Columns("TIPO").Width = .Width * 0.18
    .Columns("PORCEN").Width = .Width * 0.05
    .Columns("VALOR").Width = .Width * 0.1
    .Columns("MON").Width = .Width * 0.07
    .Columns("TRASLADAR").Width = .Width * 0.02
    .Columns("VALORMONPROCE").Width = .Columns("VALOR").Width
End With
End Sub

Public Sub CargarRecursos(ByVal oGestorIdiomas As FSGSIdiomas.CGestorIdiomas, ByVal sIdioma As String, ByVal iModulo As ModuleName)
Dim Ador As Ador.Recordset
Dim i As Integer
On Error Resume Next
Set Ador = oGestorIdiomas.DevolverTextosDelModulo(iModulo, sIdioma)

If Not Ador Is Nothing Then
    With sdbgHitos
        lbl(0).Caption = Ador(0).Value & ":"
        Ador.MoveNext
        lbl(1).Caption = Ador(0).Value & ": " & Format(g_dblImporteLinea, "standard") & " " & g_sMoneda
        Ador.MoveNext
        .Columns("FECHA").Caption = Ador(0).Value
        Ador.MoveNext
        .Columns("DEN").Caption = Ador(0).Value
        Ador.MoveNext
        .Columns("CLASE").Caption = Ador(0).Value
        Ador.MoveNext
        .Columns("TIPO").Caption = Ador(0).Value
        Ador.MoveNext
        .Columns("VALOR").Caption = Ador(0).Value
        .Columns("VALORMONPROCE").Caption = Ador(0).Value
        Ador.MoveNext
        .Columns("MON").Caption = Ador(0).Value
        Ador.MoveNext
        UserControl.cmdA�adir.Caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.Caption = Ador(0).Value
        m_sLiterales(0) = gsZANT
        m_sLiterales(1) = gsZINV
        For i = 2 To 7
            Ador.MoveNext
            m_sLiterales(i) = Ador(0).Value 'Anticipo, Facturaci�n parcial , 1 - Facturaci�n parcial porcentual ,2 - Facturaci�n parcial seg�n valor ,4 - Anticipo en facturaci�n parcial porcentual ,5 - Anticipo facturaci�n parcial por valor
        Next i
        
        Ador.Close
    End With
End If

Set Ador = Nothing
End Sub

Public Sub Initialize()
    Dim oHito As CHitoFact
    
    m_bValidacionHitosOk = True
    
    sdbgHitos.RemoveAll
    Screen.MousePointer = vbNormal
    PonerFieldSeparator UserControl.Controls
    
    If g_oLineaSeleccionada.Baja_LOG Then
        sdbcClase.BackColor = RGB(192, 192, 192)
    Else
        sdbcClase.BackColor = RGB(255, 255, 255)
    End If
    
    'Cargamos los diferentes combos
    sdbcClase.RemoveAll
    sdbcClase.AddItem "ZF" & Chr(m_lSeparador) & m_sLiterales(6)
    sdbcClase.AddItem "ZI" & Chr(m_lSeparador) & m_sLiterales(7)
    sdbddClase.RemoveAll
    sdbddClase.AddItem "ZANT" & Chr(m_lSeparador) & m_sLiterales(0)
    sdbddClase.AddItem "ZINV" & Chr(m_lSeparador) & m_sLiterales(1)
    sdbddTipo.RemoveAll
    sdbddTipo.AddItem "" & Chr(m_lSeparador) & ""
    sdbgHitos.Columns("DEN").Position = 6
    If g_bMonProce Then
        sdbgHitos.Columns("VALORMONPROCE").Position = 4
        sdbgHitos.Columns("VALOR").Visible = False
        sdbgHitos.Columns("VALORMONPROCE").Visible = True
    Else
        sdbgHitos.Columns("VALOR").Position = 4
        sdbgHitos.Columns("VALORMONPROCE").Visible = False
        sdbgHitos.Columns("VALOR").Visible = True
    End If
    sdbgHitos.Columns("MON").Position = 5
    'Rellenamos la coleccion para cargar le grid
    For Each oHito In g_oLineaSeleccionada.HitosFac
        g_oHitosGrid.Add oHito.Id, oHito.Fecha, oHito.Den, oHito.Clase, oHito.TipoPlan, oHito.Porcen, oHito.Valor, oHito.AccionEdicion, oHito.ValorMonProce
    Next
    
    'Cargar datos
    sdbcClase.Value = g_oLineaSeleccionada.ClaseHitosFac
    CargarGrid
End Sub

Public Sub RefrescarHitosGrid()
    Dim oHito As CHitoFact
    
    Set g_oHitosGrid = New CHitosFact
    For Each oHito In g_oLineaSeleccionada.HitosFac
        g_oHitosGrid.Add oHito.Id, oHito.Fecha, oHito.Den, oHito.Clase, oHito.TipoPlan, oHito.Porcen, oHito.Valor, oHito.AccionEdicion, oHito.ValorMonProce
    Next
    
    Set oHito = Nothing
End Sub

Public Function Validar() As Boolean
Dim sCampoValor As String
Validar = True
If m_bSaltarValidar Then
    m_bSaltarValidar = False
    Exit Function
End If
If Not sdbgHitos.DataChanged Then Exit Function
If g_bValidando Then Exit Function
Validar = False
g_bValidando = True
With sdbgHitos
    'Fecha de hito
    If .Columns("FECHA").Value <> "" Then
        If Not IsDate(.Columns("FECHA").Value) Then
            oMensajes.FechaNoValida
            .Columns("FECHA").Value = ""
            GoTo Salir
        End If
        If CDbl(CVDate(.Columns("FECHA").Value)) < CDbl(CVDate(Date)) And .Columns("FECHA_VALIDADA").Value = "0" Then
            If oMensajes.FechaFactMenor_a_la_Actual = vbNo Then
                .Columns("FECHA").Value = ""
                .Columns("FECHA_VALIDADA").Value = "0"
                GoTo Salir
            Else
                .Columns("FECHA_VALIDADA").Value = "1"
            End If
        End If
    Else
        oMensajes.FechaNoValida
        .Columns("FECHA").Value = ""
        GoTo Salir
    End If
    
    'Clase de Hito de Facturaci�n
    If .Columns("CLASE").Value = "" Then
        oMensajes.ClaseHitoVacia
        GoTo Salir
    End If
    'Tipo de Plan
    If .Columns("TIPO").Value = "" Then
        oMensajes.TipoPlanVacio
        GoTo Salir
    End If
    'Porcentaje
    If (.Columns("TIPO").Text = m_sLiterales(2) Or .Columns("TIPO").Text = m_sLiterales(4)) And .Columns("PORCEN").Text = "" Then
        oMensajes.PorcentajeVacio
        GoTo Salir
    End If
    If (.Columns("TIPO").Text = m_sLiterales(2) Or .Columns("TIPO").Text = m_sLiterales(4)) And (Not IsNumeric(.Columns("PORCEN").Value) Or IsEmpty(.Columns("PORCEN").Value)) Then
        oMensajes.PorcentajeVacio
        GoTo Salir
    End If
    If (.Columns("TIPO").Text = m_sLiterales(2) Or .Columns("TIPO").Text = m_sLiterales(4)) And .Columns("PORCEN").Value > 100 Then
        oMensajes.PorcentajeVacio
        GoTo Salir
    End If
    If (.Columns("TIPO").Text = m_sLiterales(2) Or .Columns("TIPO").Text = m_sLiterales(4)) Then
        If CDbl(.Columns("PORCEN").Value) <> Round(.Columns("PORCEN").Value, 2) Then
            oMensajes.No_mas_de_2Decimales (1549)
            GoTo Salir
        End If
    End If
    'Valor
    If g_bMonProce Then
        sCampoValor = "VALORMONPROCE"
    Else
        sCampoValor = "VALOR"
    End If
    If .Columns(sCampoValor).Text = "" Then
        oMensajes.ValorVacio
        GoTo Salir
    End If
    If Not IsNumeric(.Columns(sCampoValor).Value) Then
        oMensajes.ValorVacio
        .Columns("VALOR").Value = ""
        .Columns("VALORMONPROCE").Value = ""
        GoTo Salir
    End If
    If CDbl(.Columns(sCampoValor).Value) <> Round(.Columns(sCampoValor).Value, 2) Then
        oMensajes.No_mas_de_2Decimales (1551)
        .Columns(sCampoValor).Value = ""
        GoTo Salir
    End If
    'Denominaci�n del hito
    If .Columns("DEN").Value = "" Then
        oMensajes.DenominacionVacia
        GoTo Salir
    End If
End With
Validar = True

Salir:
    g_bValidando = False
End Function

Public Sub ModoEdicion(ByVal bEdicion As Boolean, Optional ByVal bPermitirGuardar As Boolean = False)
    cmdA�adir.Enabled = bEdicion
    cmdEliminar.Enabled = bEdicion
    cmdDeshacer.Enabled = bEdicion
    sdbcClase.Enabled = bEdicion
    sdbgHitos.AllowAddNew = bEdicion
    sdbgHitos.AllowDelete = bEdicion
    sdbgHitos.AllowUpdate = bEdicion
    m_bModoEdicion = bEdicion
    
    If m_bModoEdicion And sdbgHitos.Rows > 0 Then EstablecerEdicionLinea sdbgHitos.AddItemBookmark(0)
End Sub

Public Function HayHitosModificados() As Boolean
    Dim oHito As CHitoFact
    Dim bHayModif As Boolean
    
    If Not g_oLineaSeleccionada Is Nothing Then
        If Not g_oLineaSeleccionada.HitosFac Is Nothing Then
            For Each oHito In g_oLineaSeleccionada.HitosFac
                If oHito.AccionEdicion <> tipoaccionedicion.SinAccion Then
                    bHayModif = True
                    Exit For
                End If
            Next
        End If
    End If
    
    HayHitosModificados = bHayModif
End Function

Private Function ComprobarEdicionHito(ByVal lEmpresa As Long, ByVal IdOrden As Long, ByVal IDLinea As Long, ByVal IdHito As Long) As Boolean
    Dim iNumError As Integer
    Dim sError As String
    Dim oWebSvc As FSGSLibrary.CWebService
    Dim arLineas(0) As Long
    Dim arHitos(0) As Long
    Dim oErp As CERPInt
    Dim sCodErp As String
    
    Set oErp = FSGSRaiz.Generar_CERPInt
    sCodErp = oErp.ObtenerCodERP("", lEmpresa)
    Set oErp = Nothing
    
    arLineas(0) = IDLinea
    arHitos(0) = IdHito
    Set oWebSvc = New FSGSLibrary.CWebService
    ComprobarEdicionHito = oWebSvc.LlamadaWebServiceComprobarHitosFacturados(gURLSrvInt, iNumError, sError, sCodErp, IdOrden, arLineas, arHitos)(0)
    
    If iNumError < 0 Then oMensajes.ErrorServicioIntegracionHitosFact
    
    Set oWebSvc = Nothing
End Function
