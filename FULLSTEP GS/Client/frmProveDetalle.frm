VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmProveDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle de proveedor"
   ClientHeight    =   6540
   ClientLeft      =   1950
   ClientTop       =   2940
   ClientWidth     =   12225
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmProveDetalle.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6540
   ScaleWidth      =   12225
   ShowInTaskbar   =   0   'False
   Begin TabDlg.SSTab SSTab 
      Height          =   6435
      Left            =   30
      TabIndex        =   0
      Top             =   60
      Width           =   12165
      _ExtentX        =   21458
      _ExtentY        =   11351
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      BackColor       =   8421376
      TabCaption(0)   =   "DDatos proveedor"
      TabPicture(0)   =   "frmProveDetalle.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblComentarios"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "picDatos"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "picPortal"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "PicPedido"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "DDatos proveedor en el ERP"
      TabPicture(1)   =   "frmProveDetalle.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "PicERP"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.PictureBox PicERP 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   5895
         Left            =   -74940
         ScaleHeight     =   5895
         ScaleWidth      =   12015
         TabIndex        =   43
         Top             =   360
         Width           =   12015
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "D&Cancelar"
            Height          =   375
            Left            =   5790
            TabIndex        =   52
            TabStop         =   0   'False
            Top             =   5430
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "D&Aceptar"
            Height          =   375
            Left            =   4410
            TabIndex        =   51
            TabStop         =   0   'False
            Top             =   5430
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.CommandButton cmdERP 
            Caption         =   "..."
            Height          =   285
            Index           =   1
            Left            =   7410
            TabIndex        =   49
            Top             =   1050
            Width           =   300
         End
         Begin VB.CommandButton cmdERP 
            Caption         =   "..."
            Height          =   285
            Index           =   0
            Left            =   7410
            TabIndex        =   46
            Top             =   420
            Width           =   300
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCodERP 
            Height          =   285
            Left            =   3780
            TabIndex        =   50
            Top             =   420
            Width           =   3960
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   -2147483640
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   6
            Columns(0).Width=   2593
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5212
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3016
            Columns(2).Caption=   "CAMPO1"
            Columns(2).Name =   "CAMPO1"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3016
            Columns(3).Caption=   "CAMPO2"
            Columns(3).Name =   "CAMPO2"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3016
            Columns(4).Caption=   "CAMPO3"
            Columns(4).Name =   "CAMPO3"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3016
            Columns(5).Caption=   "CAMPO4"
            Columns(5).Name =   "CAMPO4"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            _ExtentX        =   6985
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblCodERPFactText 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   3780
            TabIndex        =   48
            Top             =   1050
            Width           =   3555
         End
         Begin VB.Label lblCodERPFact 
            Caption         =   "DC�digo proveedor emisor factura en el ERP:"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   330
            TabIndex        =   47
            Top             =   1095
            Width           =   3480
         End
         Begin VB.Label lblCodERPText 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   3780
            TabIndex        =   45
            Top             =   420
            Width           =   3555
         End
         Begin VB.Label lblCodERP 
            Caption         =   "DC�digo proveedor en el ERP:"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   330
            TabIndex        =   44
            Top             =   465
            Width           =   2370
         End
      End
      Begin VB.PictureBox PicPedido 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   405
         Left            =   60
         ScaleHeight     =   405
         ScaleWidth      =   11715
         TabIndex        =   38
         Top             =   570
         Visible         =   0   'False
         Width           =   11715
         Begin VB.Label lblFecPed 
            Caption         =   "DPedido visto por el proveedor:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   3300
            TabIndex        =   42
            Top             =   90
            Width           =   2355
         End
         Begin VB.Label lblFecPedTexto 
            BackColor       =   &H00FFFFC0&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   5715
            TabIndex        =   41
            Top             =   30
            Width           =   2805
         End
         Begin VB.Label lblNumExt 
            Caption         =   "DN�m. externo:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   30
            TabIndex        =   40
            Top             =   90
            Width           =   1125
         End
         Begin VB.Label lblNumExtTexto 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1155
            TabIndex        =   39
            Top             =   30
            Width           =   1815
         End
      End
      Begin VB.PictureBox picPortal 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   60
         ScaleHeight     =   420
         ScaleWidth      =   6555
         TabIndex        =   33
         Top             =   960
         Width           =   6555
         Begin VB.Label lblPremium 
            Caption         =   "Label7"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   3210
            TabIndex        =   36
            Top             =   90
            Width           =   3195
         End
         Begin VB.Label lblCodPort 
            Caption         =   "C�digo portal:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   45
            TabIndex        =   35
            Top             =   105
            Width           =   1065
         End
         Begin VB.Label lblCodPortProve 
            BackColor       =   &H0099CCFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1140
            TabIndex        =   34
            Top             =   60
            Width           =   1845
         End
      End
      Begin VB.PictureBox picDatos 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4890
         Left            =   60
         ScaleHeight     =   4890
         ScaleWidth      =   11970
         TabIndex        =   1
         Top             =   1380
         Width           =   11970
         Begin VB.TextBox Text1 
            BackColor       =   &H80000018&
            Height          =   2085
            Left            =   6600
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   4
            Top             =   0
            Width           =   5265
         End
         Begin VB.CommandButton cmdSalvarEsp 
            Height          =   300
            Left            =   10950
            Picture         =   "frmProveDetalle.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   4550
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAbrirEsp 
            Height          =   300
            Left            =   11430
            Picture         =   "frmProveDetalle.frx":0D6B
            Style           =   1  'Graphical
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   4550
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgContactos 
            Height          =   1425
            Left            =   0
            TabIndex        =   5
            Top             =   2280
            Width           =   6465
            ScrollBars      =   3
            _Version        =   196617
            DataMode        =   2
            BorderStyle     =   0
            GroupHeaders    =   0   'False
            Col.Count       =   10
            stylesets.count =   2
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmProveDetalle.frx":0DE7
            stylesets(1).Name=   "Tan"
            stylesets(1).BackColor=   10079487
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmProveDetalle.frx":0E03
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            BalloonHelp     =   0   'False
            MaxSelectedRows =   1
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns.Count   =   10
            Columns(0).Width=   3200
            Columns(0).Caption=   "Apellidos"
            Columns(0).Name =   "APE"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   3200
            Columns(1).Caption=   "Departamento"
            Columns(1).Name =   "DEP"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            Columns(2).Width=   3200
            Columns(2).Caption=   "Cargo"
            Columns(2).Name =   "CAR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).HasBackColor=   -1  'True
            Columns(2).BackColor=   16777215
            Columns(3).Width=   4022
            Columns(3).Caption=   "Nombre"
            Columns(3).Name =   "DEN"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   50
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   16777215
            Columns(4).Width=   2328
            Columns(4).Caption=   "Tel�fono"
            Columns(4).Name =   "TFNO"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).HasBackColor=   -1  'True
            Columns(4).BackColor=   16777215
            Columns(5).Width=   3200
            Columns(5).Caption=   "Fax"
            Columns(5).Name =   "FAX"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).HasBackColor=   -1  'True
            Columns(5).BackColor=   16777215
            Columns(6).Width=   3200
            Columns(6).Caption=   "Tel�fono m�vil"
            Columns(6).Name =   "TFNO_MOVIL"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).HasBackColor=   -1  'True
            Columns(6).BackColor=   16777215
            Columns(7).Width=   3200
            Columns(7).Caption=   "Mail"
            Columns(7).Name =   "MAIL"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).HasBackColor=   -1  'True
            Columns(7).BackColor=   16777215
            Columns(8).Width=   2540
            Columns(8).Caption=   "Recibe peticiones"
            Columns(8).Name =   "DEF"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Style=   2
            Columns(8).HasBackColor=   -1  'True
            Columns(8).BackColor=   16777215
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "PORT"
            Columns(9).Name =   "PORT"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   11
            Columns(9).FieldLen=   256
            _ExtentX        =   11404
            _ExtentY        =   2514
            _StockProps     =   79
            BackColor       =   -2147483633
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComctlLib.ListView lstvwEsp 
            Height          =   2205
            Left            =   6600
            TabIndex        =   6
            Top             =   2280
            Width           =   5265
            _ExtentX        =   9287
            _ExtentY        =   3889
            View            =   3
            Arrange         =   1
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HotTracking     =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Fichero"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Comentario"
               Object.Width           =   6588
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Fecha"
               Object.Width           =   2867
            EndProperty
         End
         Begin MSComDlg.CommonDialog cmmdEsp 
            Left            =   6500
            Top             =   4500
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            CancelError     =   -1  'True
            DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
         End
         Begin MSComctlLib.ImageList ImageList 
            Left            =   7080
            Top             =   4440
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   16
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   1
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmProveDetalle.frx":0E1F
                  Key             =   "ESP"
                  Object.Tag             =   "ESP"
               EndProperty
            EndProperty
         End
         Begin VB.Label Label21 
            Caption         =   "Moneda:"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   60
            TabIndex        =   32
            Top             =   1110
            Width           =   915
         End
         Begin VB.Label Label2 
            Caption         =   "DDirecci�n:"
            ForeColor       =   &H00000000&
            Height          =   210
            Left            =   60
            TabIndex        =   31
            Top             =   30
            Width           =   885
         End
         Begin VB.Label Label3 
            Caption         =   "C�digo postal:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   60
            TabIndex        =   30
            Top             =   390
            Width           =   1065
         End
         Begin VB.Label Label4 
            Caption         =   "Poblaci�n:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   2250
            TabIndex        =   29
            Top             =   360
            Width           =   825
         End
         Begin VB.Label Label5 
            Caption         =   "Pa�s:"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   60
            TabIndex        =   28
            Top             =   750
            Width           =   1020
         End
         Begin VB.Label Label6 
            Caption         =   "Provincia:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   60
            TabIndex        =   27
            Top             =   1470
            Width           =   1065
         End
         Begin VB.Label lblcalif1 
            Caption         =   "Sin calificar"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   60
            TabIndex        =   26
            Top             =   3840
            Width           =   1500
         End
         Begin VB.Label lblCal1Den 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1650
            TabIndex        =   25
            Top             =   3765
            Width           =   3700
         End
         Begin VB.Label lblcal2den 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1650
            TabIndex        =   24
            Top             =   4125
            Width           =   3700
         End
         Begin VB.Label lblcal3den 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1650
            TabIndex        =   23
            Top             =   4485
            Width           =   3700
         End
         Begin VB.Label lblCalif2 
            Caption         =   "Sin calificar"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   60
            TabIndex        =   22
            Top             =   4185
            Width           =   1500
         End
         Begin VB.Label lblcalif3 
            Caption         =   "Sin calificar"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   60
            TabIndex        =   21
            Top             =   4515
            Width           =   1500
         End
         Begin VB.Label lblCp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1140
            TabIndex        =   20
            Top             =   330
            Width           =   1020
         End
         Begin VB.Label lblMonDen 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   2220
            TabIndex        =   19
            Top             =   1050
            Width           =   4215
         End
         Begin VB.Label lblPaiDen 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   2220
            TabIndex        =   18
            Top             =   690
            Width           =   4215
         End
         Begin VB.Label lblProviDen 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   2220
            TabIndex        =   17
            Top             =   1410
            Width           =   4215
         End
         Begin VB.Label lblPaiCod 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1140
            TabIndex        =   16
            Top             =   690
            Width           =   1020
         End
         Begin VB.Label lblMonCod 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1140
            TabIndex        =   15
            Top             =   1050
            Width           =   1020
         End
         Begin VB.Label lblProviCod 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1140
            TabIndex        =   14
            Top             =   1410
            Width           =   1020
         End
         Begin VB.Label lblPob 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   3075
            TabIndex        =   13
            Top             =   330
            Width           =   3360
         End
         Begin VB.Label lblCal1Val 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   5400
            TabIndex        =   12
            Top             =   3765
            Width           =   1030
         End
         Begin VB.Label lblCal2Val 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   5400
            TabIndex        =   11
            Top             =   4125
            Width           =   1030
         End
         Begin VB.Label lblCal3Val 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   5400
            TabIndex        =   10
            Top             =   4485
            Width           =   1030
         End
         Begin VB.Label Label7 
            Caption         =   "URL:"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   60
            TabIndex        =   9
            Top             =   1860
            Width           =   915
         End
         Begin VB.Label lblURL 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1140
            TabIndex        =   8
            Top             =   1800
            Width           =   5280
         End
         Begin VB.Label lblDir 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1140
            TabIndex        =   7
            Top             =   -15
            Width           =   5295
         End
      End
      Begin VB.Label lblComentarios 
         Caption         =   "Comentarios"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   6690
         TabIndex        =   37
         Top             =   1050
         Width           =   1995
      End
   End
End
Attribute VB_Name = "frmProveDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_bPremium As Boolean
Public g_bActivo As Boolean
Private m_sIdiPremium As String

Public g_oProveSeleccionado As CProveedor
Private m_oIBAseDatosEnEdicion As IBaseDatos

'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private m_sArFileNames() As String
'Guardar archivo de especificaci�n
Private m_sIdiGuardar As String
'Tipo original
Private m_sIdiTipoOrig As String
'Archivo
Private m_sIdiArchivo As String

Public m_bDescargarFrm As Boolean

Dim m_bActivado As Boolean

Private m_bUnload As Boolean

Public ProveERP As CProveERP
Public g_bMostrarPesta�as As Boolean
Public g_bModificar As Boolean

Private m_bRespetarComboCodERP As Boolean
Private m_bCargarComboDesde As Boolean
Private m_oProvesERP As CProveERPs
Public m_sEmpresa As String
Public g_sOrigen As String
Public m_bBloquearEP As Boolean
Private m_sMsgError As String
''' <summary>
''' Change de la combo de c�digos ERP.
''' </summary>
''' <param name></param>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdAceptar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case UCase(g_sOrigen)
    Case "FRMSEGUIMIENTO"
        frmSeguimiento.sdbgSeguimiento.Columns("COD_ERP").Value = sdbcCodERP.Text
        frmSeguimiento.sdbgSeguimiento.Columns("COD_PROVE_ERP_FACTURA").Value = lblCodERPFactText.caption
        DoEvents
        Unload Me
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmProveDetalle", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmProveDetalle", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub sdbcCodERP_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarComboCodERP Then
       m_bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPEDIDOS", "sdbcCodERP_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Click sobre la combo de c�digos ERP.
''' </summary>
''' <param name></param>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub sdbcCodERP_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcCodERP.DroppedDown Then
        sdbcCodERP = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPEDIDOS", "sdbcCodERP_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' CloseUp de la combo de c�digos ERP.
''' </summary>
''' <param name></param>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub sdbcCodERP_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCodERP.Value = "..." Or sdbcCodERP.Text = "" Then
        sdbcCodERP.Text = ""
        Exit Sub
    End If
    
    m_bRespetarComboCodERP = True
    sdbcCodERP.Value = sdbcCodERP.Columns(0).Value
    lblCodERPText.caption = sdbcCodERP.Columns(0).Value
    m_bRespetarComboCodERP = False
    m_bCargarComboDesde = False
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPEDIDOS", "sdbcCodERP_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Evento que salta cuando se despliega la combo de Provedores ERP.
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisado por ngo</remarks>
Private Sub sdbcCodERP_DropDown()
Dim oProveERP As CProveERP
Dim sCampo As String
Dim sCampo2 As String
Dim sCampo3 As String
Dim sCampo4 As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    sdbcCodERP.RemoveAll

    Set m_oProvesERP = Nothing
    Set m_oProvesERP = oFSGSRaiz.Generar_CProveERPs
     
    If m_bCargarComboDesde Then
        m_oProvesERP.CargarProveedoresERP CLng(m_sEmpresa), g_oProveSeleccionado.Cod, sdbcCodERP.Text
    Else
        m_oProvesERP.CargarProveedoresERP CLng(m_sEmpresa), g_oProveSeleccionado.Cod
    End If
    
    For Each oProveERP In m_oProvesERP
        
        'Los campos pueden estar activos a la vez, por lo que deben tratarse individualmente
        If gParametrosGenerales.gbCampo1ERPAct = True Then
            sCampo = NullToStr(oProveERP.Campo1)
        End If
        
        If gParametrosGenerales.gbCampo2ERPAct Then
            sCampo2 = NullToStr(oProveERP.Campo2)
        End If
        
        If gParametrosGenerales.gbCampo3ERPAct Then
            sCampo3 = NullToStr(oProveERP.Campo3)
        End If
        
        If gParametrosGenerales.gbCampo4ERPAct Then
            sCampo4 = NullToStr(oProveERP.Campo4)
        End If
    
        
        sdbcCodERP.AddItem oProveERP.Cod & Chr(m_lSeparador) & oProveERP.Den & Chr(m_lSeparador) & sCampo & Chr(m_lSeparador) & sCampo2 & Chr(m_lSeparador) & sCampo3 & Chr(m_lSeparador) & sCampo4
    Next

    sdbcCodERP.SelStart = 0
    sdbcCodERP.SelLength = Len(sdbcCodERP.Text)
    sdbcCodERP.Refresh

    m_bCargarComboDesde = False

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPEDIDOS", "sdbcCodERP_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcCodERP_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCodERP.DataFieldList = "Column 0"
    sdbcCodERP.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPEDIDOS", "sdbcCodERP_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Se posiciona en la combo de Proveedores ERP seg�n la selecci�n.
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>

Private Sub sdbcCodERP_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcCodERP.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCodERP.Rows - 1
            bm = sdbcCodERP.GetBookmark(i)
            If UCase(sdbcCodERP.Text) = UCase(Mid(sdbcCodERP.Columns(0).CellText(bm), 1, Len(sdbcCodERP.Text))) Then
                sdbcCodERP.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmPEDIDOS", "sdbcCodERP_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Validate de la combo de Proveedores ERP.
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisado por ngo</remarks>
Private Sub sdbcCodERP_Validate(Cancel As Boolean)
Dim oProvesERP As CProveERPs
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCodERP.Text = "" Then
        m_bCargarComboDesde = False
        Exit Sub
    End If
        
    Set oProvesERP = oFSGSRaiz.Generar_CProveERPs
    oProvesERP.CargarProveedoresERP CLng(m_sEmpresa), g_oProveSeleccionado.Cod, sdbcCodERP.Columns(0).Value

    If oProvesERP Is Nothing Then
        lblCodERPText.caption = ""
        sdbcCodERP.Text = ""
    ElseIf oProvesERP.Count = 0 Then
        lblCodERPText.caption = ""
        sdbcCodERP.Text = ""
    End If

    Set oProvesERP = Nothing

    Screen.MousePointer = vbHourglass
    If m_oProvesERP Is Nothing Then
        Set m_oProvesERP = oFSGSRaiz.Generar_CProveERPs
    End If
    m_oProvesERP.CargarProveedoresERP CLng(m_sEmpresa), g_oProveSeleccionado.Cod, sdbcCodERP.Text

    If m_oProvesERP.Count = 0 Then
        sdbcCodERP.Text = ""
        Screen.MousePointer = vbNormal
    Else
        m_bRespetarComboCodERP = True

        sdbcCodERP.Columns(0).Value = m_oProvesERP.Item(1).Cod
        sdbcCodERP.Columns(1).Value = m_oProvesERP.Item(1).Den
        m_bRespetarComboCodERP = False

        m_bCargarComboDesde = False
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmProveDetalle", "sdbcCodERP_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
Private Sub cmdAbrirEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    Set m_oIBAseDatosEnEdicion = g_oProveSeleccionado
        
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion(False)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oIBAseDatosEnEdicion = Nothing
        Exit Sub
    End If
    
    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
                
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing

        ' Cargamos el contenido en la esp.
        Set oEsp = g_oProveSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProveedor)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspProveedor, oEsp.DataSize, sFileName
                
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
    
    Set m_oIBAseDatosEnEdicion = Nothing
    
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Set oEsp = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number = 70 Then
        Resume Next
    
   ElseIf err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmProveDetalle", "cmdAbrirEsp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      GoTo Cancelar
      Exit Sub
   End If

End Sub

Private Sub cmdERP_Click(Index As Integer)
Dim oEmpresa As CEmpresa
Dim oProvERP As CProveERP
Dim oProveedoresERP As CProveERPs
Dim sCod As String
Dim sDen As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case Index
    Case 0  'Proveedor en el ERP
        Set oEmpresa = oFSGSRaiz.Generar_CEmpresa
        oEmpresa.Id = CLng(m_sEmpresa)
        Set oProveedoresERP = oEmpresa.CargarERPs(g_oProveSeleccionado.Cod)
        
        For Each oProvERP In oProveedoresERP
            If oProvERP.Cod = lblCodERPText.caption Then
                FSGSForm.MostrarFormDetalleProveERP oProvERP, gParametrosGenerales, gParametrosInstalacion, oGestorIdiomas
            End If
        Next

    Case 1  'Proveedor emisor de la factura en el ERP
        If g_bModificar Then
            'Si se puede modificar mostramos el formulario de selecci�n
            If lblCodERPFactText.caption & "" <> "" Then
                pgSacar_Cod_Den_ProveERP lblCodERPFactText.caption, sCod, sDen, val(m_sEmpresa)
            End If
            Screen.MousePointer = vbNormal
            'Pantalla de selecci�n de Proveedor ERP
            frmSelProveERP.sOrigen = Me.Name
            frmSelProveERP.m_lEmpresa = val(m_sEmpresa)
            frmSelProveERP.sdbcCodERP.Text = lblCodERPFactText.caption
            frmSelProveERP.sdbcProveCod.Text = sCod
            frmSelProveERP.sdbcProveDen.Text = sDen
            frmSelProveERP.Show vbModal
        Else
            'Pantalla de proveedor FullStep cuando no se est� editando
            If lblCodERPFactText.caption & "" <> "" Then
                pgSacar_Cod_Den_ProveERP lblCodERPFactText.caption, sCod, sDen, val(m_sEmpresa)
            End If
            Screen.MousePointer = vbNormal
            'Pantalla de proveedor FullStep cuando no se est� editando
            If Not sCod = "" Then
                MostrarDetalleProveedor (sCod)
            End If
        End If
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmProveDetalle", "cmdERP_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub MostrarDetalleProveedor(ByVal CodProve As String)
Dim oProves As CProveedores
Dim oProve As CProveedor
Dim oCon As CContacto
Dim oFrm As frmProveDetalle
Dim sFormatoNumber As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    Set oFrm = New frmProveDetalle
    Set oFrm.g_oProveSeleccionado = oProve
    oFrm.g_bModificar = False
    oFrm.g_bMostrarPesta�as = False
    oFrm.caption = CodProve & "  " & oProve.Den
    oFrm.lblCodPortProve = NullToStr(oProve.CodPortal)
    oFrm.g_bPremium = oProve.EsPremium
    oFrm.g_bActivo = oProve.EsPremiumActivo
    oFrm.lblDir = NullToStr(oProve.Direccion)
    oFrm.lblCp = NullToStr(oProve.cP)
    oFrm.lblPob = NullToStr(oProve.Poblacion)
    oFrm.lblPaiCod = NullToStr(oProve.CodPais)
    oFrm.lblPaiDen = NullToStr(oProve.DenPais)
    oFrm.lblMonCod = NullToStr(oProve.CodMon)
    oFrm.lblMonDen = NullToStr(oProve.DenMon)
    oFrm.lblProviCod = NullToStr(oProve.CodProvi)
    oFrm.lblProviDen = NullToStr(oProve.DenProvi)
    oFrm.lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    oFrm.lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    oFrm.lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then
        oFrm.lblCal1Den = oProve.Calif1
    End If
    
    If Trim(oProve.Calif2) <> "" Then
        oFrm.lblcal2den = oProve.Calif2
    End If
    
    If Trim(oProve.Calif3) <> "" Then
        oFrm.lblcal3den = oProve.Calif3
    End If
    sFormatoNumber = "#,##0."
    oFrm.lblCal1Val = DblToStr(oProve.Val1, sFormatoNumber)
    oFrm.lblCal2Val = DblToStr(oProve.Val2, sFormatoNumber)
    oFrm.lblCal3Val = DblToStr(oProve.Val3, sFormatoNumber)

    oFrm.lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        
        oFrm.sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    
    Next
        
    Set oProves = Nothing
    Set oProve = Nothing
    
    
    oFrm.Show 1
    
    Set oFrm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmProveDetalle", "MostrarDetalleProveedor", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub


Private Sub cmdSalvarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    Set m_oIBAseDatosEnEdicion = g_oProveSeleccionado
        
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion(False)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oIBAseDatosEnEdicion = Nothing
        Exit Sub
    End If
    
    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else

        cmmdEsp.DialogTitle = m_sIdiGuardar

        cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdiArchivo
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        
        ' Cargamos el contenido en la esp.
        Set oEsp = g_oProveSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProveedor)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
                
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspProveedor, oEsp.DataSize, sFileName
    End If
    
Cancelar:
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set m_oIBAseDatosEnEdicion = Nothing
        Set oEsp = Nothing
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmProveDetalle", "cmdSalvarEsp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      GoTo Cancelar
      Exit Sub
   End If

End Sub


Private Sub Form_Activate()
   If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
        SSTab.TabVisible(1) = g_bMostrarPesta�as
        lblCodERPText.Visible = Not (g_bModificar And Not m_bBloquearEP)
        cmdERP(0).Visible = Not (g_bModificar And Not m_bBloquearEP)
        sdbcCodERP.Visible = g_bModificar And Not m_bBloquearEP
         
        If g_bModificar Then
            sdbcCodERP.Text = lblCodERPText.caption
        Else
            lblCodERPFactText.BackColor = lblCodERPText.BackColor
        End If
        cmdAceptar.Visible = g_bModificar
    End If
    cmdCancelar.Visible = cmdAceptar.Visible
    If FSEPConf Then
        sdbgContactos.Columns("DEF").Visible = False
    End If
    
    If Not PicPedido.Visible Then
        PicPedido.Height = 0
    End If
    
    If lblCodPortProve = "" Then
        picPortal.Visible = False
        picDatos.Top = 400 + PicPedido.Height + PicPedido.Top
        Me.Height = 5850 + PicPedido.Height + PicPedido.Top
        
    Else
        picPortal.Visible = True
        picDatos.Top = 450 + PicPedido.Height + PicPedido.Top
        Me.Height = 5990 + PicPedido.Height + PicPedido.Top
        If g_bPremium Then
            If g_bActivo Then
                lblPremium.caption = m_sIdiPremium
            Else
                lblPremium.caption = ""
            End If
        Else
            lblPremium.caption = ""
        End If
        lblDir.BackColor = " 10079487 "
        lblCp.BackColor = " 10079487 "
        lblPob.BackColor = " 10079487 "
        lblPaiCod.BackColor = " 10079487 "
        lblPaiDen.BackColor = " 10079487 "
        lblMonCod.BackColor = " 10079487 "
        lblMonDen.BackColor = " 10079487 "
        lblProviCod.BackColor = " 10079487 "
        lblProviDen.BackColor = " 10079487 "
        lblURL.BackColor = " 10079487 "
    End If
    lblComentarios.Top = picDatos.Top - 300
    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.35
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.35
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.279
        '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    
    If err.Number <> 0 Then
        m_bUnload = True
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmProveDetalle", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub Form_Load()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    m_bDescargarFrm = False
    m_bActivado = False
    m_bUnload = False
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
    
    'Inicializar el array de ficheros
    ReDim m_sArFileNames(0)

    g_oProveSeleccionado.CargarTodasLasEspecificaciones True, False
    Text1.Text = NullToStr(g_oProveSeleccionado.obs)
    AnyadirEspsALista
    
    PonerFieldSeparator Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmProveDetalle", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub AnyadirEspsALista()
Dim oEsp As CEspecificacion


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lstvwEsp.ListItems.clear
    
    For Each oEsp In g_oProveSeleccionado.especificaciones
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmProveDetalle", "AnyadirEspsALista", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub
Private Sub CargarRecursos()
Dim Adores As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVE_DETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
        Label2.caption = Adores(0).Value '1
        Adores.MoveNext
        Label21.caption = Adores(0).Value
        Adores.MoveNext
        Label3.caption = Adores(0).Value
        Adores.MoveNext
        Label4.caption = Adores(0).Value
        Adores.MoveNext
        Label5.caption = Adores(0).Value '5
        Adores.MoveNext
        Label6.caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(0).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(1).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(2).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(3).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(4).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(5).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(7).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(6).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(8).caption = Adores(0).Value
        Adores.MoveNext
        lblCodPort.caption = Adores(0).Value
        Adores.MoveNext
        m_sIdiPremium = Adores(0).Value
        Adores.MoveNext
        Label7 = Adores(0).Value
        Adores.MoveNext
        m_sIdiGuardar = Adores(0).Value
        Adores.MoveNext
        m_sIdiTipoOrig = Adores(0).Value
        Adores.MoveNext
        m_sIdiArchivo = Adores(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(1).Text = Adores(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(2).Text = Adores(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(3).Text = Adores(0).Value
        Adores.MoveNext
        lblComentarios = Adores(0).Value
        
        Adores.MoveNext
        SSTab.TabCaption(0) = Adores(0).Value
        Adores.MoveNext
        SSTab.TabCaption(1) = Adores(0).Value
        Adores.MoveNext
        lblNumExt.caption = Adores(0).Value & ":"
        Adores.MoveNext
        lblFecPed.caption = Adores(0).Value & ":"
        Adores.MoveNext
        lblCodERP.caption = Adores(0).Value & ":"
        Adores.MoveNext
        lblCodERPFact.caption = Adores(0).Value & ":"
        Adores.MoveNext
        cmdAceptar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.Close
        
    End If
    
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmProveDetalle", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim FOSFile As Scripting.FileSystemObject
Dim i As Integer
Dim bBorrando As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

On Error GoTo ERROR_Frm
    'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject
    
    i = 0
    While i < UBound(m_sArFileNames)
        bBorrando = True
        If FOSFile.FileExists(m_sArFileNames(i)) Then
            FOSFile.DeleteFile m_sArFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
       
    Set FOSFile = Nothing
    
    Exit Sub

ERROR_Frm:
    
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = m_sArFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELUO", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      GoTo ERROR_Frm
      Exit Sub
   End If

End Sub


Private Sub sdbgContactos_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgContactos.Columns("PORT").Value Then
        For i = 0 To sdbgContactos.Cols - 1
            sdbgContactos.Columns(i).CellStyleSet "Tan"
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmProveDetalle", "sdbgContactos_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub




