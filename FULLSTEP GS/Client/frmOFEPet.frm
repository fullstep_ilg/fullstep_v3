VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{1E673DEF-C82F-4FBB-BAB9-77B0634E8F4D}#1.0#0"; "ProceSelector.ocx"
Begin VB.Form frmOFEPet 
   Caption         =   "Comunicaci�n con proveedores"
   ClientHeight    =   4980
   ClientLeft      =   2070
   ClientTop       =   1755
   ClientWidth     =   11685
   Icon            =   "frmOFEPet.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4980
   ScaleWidth      =   11685
   Begin VB.PictureBox picAvisoDespub 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   210
      Left            =   720
      ScaleHeight     =   210
      ScaleWidth      =   10035
      TabIndex        =   25
      Top             =   450
      Width           =   10035
      Begin VB.CheckBox chkPubMatProve 
         Caption         =   "S�lo publicar a cada proveedor los materiales que el suministra"
         Height          =   195
         Left            =   4680
         TabIndex        =   27
         Top             =   0
         Width           =   5145
      End
      Begin VB.CheckBox chkAvisoDespub 
         Caption         =   "Activar avisos autom�ticos de despublicaci�n"
         Height          =   195
         Left            =   0
         TabIndex        =   26
         Top             =   0
         Width           =   3690
      End
   End
   Begin VB.PictureBox picProceA�a 
      BorderStyle     =   0  'None
      Height          =   525
      Left            =   60
      ScaleHeight     =   525
      ScaleWidth      =   11610
      TabIndex        =   0
      Top             =   0
      Width           =   11610
      Begin VB.CommandButton cmdResponsable 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11195
         Picture         =   "frmOFEPet.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   105
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   10840
         Picture         =   "frmOFEPet.frx":0D39
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   105
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   2670
         TabIndex        =   2
         Top             =   105
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2249
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4339
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   665
         TabIndex        =   1
         Top             =   105
         Width           =   960
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         BevelColorFrame =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1693
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
         Height          =   285
         Left            =   5330
         TabIndex        =   3
         Top             =   105
         Width           =   1305
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1799
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7726
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INVI"
         Columns(2).Name =   "INVI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   2302
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
         Height          =   285
         Left            =   6635
         TabIndex        =   4
         Top             =   105
         Width           =   4170
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   6456
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1905
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INVI"
         Columns(2).Name =   "INVI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7355
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SelectorDeProcesos.ProceSelector ProceSelector1 
         Height          =   285
         Left            =   3765
         TabIndex        =   24
         Top             =   105
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   503
      End
      Begin VB.Label lblCProceCod 
         Caption         =   "Proceso:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4400
         TabIndex        =   8
         Top             =   165
         Width           =   900
      End
      Begin VB.Label lblAnyo 
         Caption         =   "A�o:"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   75
         TabIndex        =   7
         Top             =   165
         Width           =   515
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Comm:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   1875
         TabIndex        =   6
         Top             =   165
         Width           =   735
      End
      Begin VB.Label lblProceCod 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   3082
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5330
         TabIndex        =   5
         Top             =   105
         Width           =   1305
      End
   End
   Begin TabDlg.SSTab sstabPet 
      Height          =   4275
      Left            =   0
      TabIndex        =   9
      Top             =   660
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   7541
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Hist�rico de notificaciones"
      TabPicture(0)   =   "frmOFEPet.frx":0DC6
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "picNavigate"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "picPet"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Publicaciones en el web"
      TabPicture(1)   =   "frmOFEPet.frx":0DE2
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picGridPub"
      Tab(1).Control(1)=   "picPub"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      Begin VB.PictureBox picPet 
         Appearance      =   0  'Flat
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3315
         Left            =   120
         ScaleHeight     =   3315
         ScaleWidth      =   11355
         TabIndex        =   10
         Top             =   480
         Width           =   11355
         Begin SSDataWidgets_B.SSDBGrid sdbgPet 
            Height          =   3315
            Left            =   0
            TabIndex        =   23
            Top             =   0
            Width           =   11355
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   12
            stylesets.count =   2
            stylesets(0).Name=   "ConPeticiones"
            stylesets(0).BackColor=   13172735
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmOFEPet.frx":0DFE
            stylesets(1).Name=   "Normal"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmOFEPet.frx":0E1A
            UseGroups       =   -1  'True
            BeveColorScheme =   1
            BevelColorFace  =   12632256
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Groups.Count    =   3
            Groups(0).Width =   5821
            Groups(0).Caption=   "Proveedores"
            Groups(0).HasHeadForeColor=   -1  'True
            Groups(0).HasHeadBackColor=   -1  'True
            Groups(0).HeadForeColor=   16777215
            Groups(0).HeadBackColor=   8421504
            Groups(0).Columns.Count=   2
            Groups(0).Columns(0).Width=   2196
            Groups(0).Columns(0).Caption=   "C�digo"
            Groups(0).Columns(0).Name=   "PROVECOD"
            Groups(0).Columns(0).DataField=   "Column 0"
            Groups(0).Columns(0).DataType=   8
            Groups(0).Columns(0).FieldLen=   256
            Groups(0).Columns(0).Locked=   -1  'True
            Groups(0).Columns(1).Width=   3625
            Groups(0).Columns(1).Caption=   "Denominaci�n"
            Groups(0).Columns(1).Name=   "PROVEDEN"
            Groups(0).Columns(1).DataField=   "Column 1"
            Groups(0).Columns(1).DataType=   8
            Groups(0).Columns(1).FieldLen=   256
            Groups(1).Width =   4683
            Groups(1).Caption=   "Contactos"
            Groups(1).HasHeadForeColor=   -1  'True
            Groups(1).HasHeadBackColor=   -1  'True
            Groups(1).HeadForeColor=   16777215
            Groups(1).HeadBackColor=   8421504
            Groups(1).Columns.Count=   2
            Groups(1).Columns(0).Width=   3149
            Groups(1).Columns(0).Caption=   "Apellidos"
            Groups(1).Columns(0).Name=   "CONCOD"
            Groups(1).Columns(0).DataField=   "Column 2"
            Groups(1).Columns(0).DataType=   8
            Groups(1).Columns(0).FieldLen=   256
            Groups(1).Columns(1).Width=   1535
            Groups(1).Columns(1).Caption=   "Nombre"
            Groups(1).Columns(1).Name=   "CONDEN"
            Groups(1).Columns(1).DataField=   "Column 3"
            Groups(1).Columns(1).DataType=   8
            Groups(1).Columns(1).FieldLen=   256
            Groups(2).Width =   8414
            Groups(2).Caption=   "Comunicaci�n"
            Groups(2).HasHeadForeColor=   -1  'True
            Groups(2).HasHeadBackColor=   -1  'True
            Groups(2).HeadForeColor=   16777215
            Groups(2).HeadBackColor=   8421504
            Groups(2).Columns.Count=   8
            Groups(2).Columns(0).Width=   1561
            Groups(2).Columns(0).Caption=   "Notificado"
            Groups(2).Columns(0).Name=   "FEC"
            Groups(2).Columns(0).DataField=   "Column 4"
            Groups(2).Columns(0).DataType=   8
            Groups(2).Columns(0).FieldLen=   256
            Groups(2).Columns(1).Width=   1720
            Groups(2).Columns(1).Visible=   0   'False
            Groups(2).Columns(1).Caption=   "CODCON"
            Groups(2).Columns(1).Name=   "CODCON"
            Groups(2).Columns(1).DataField=   "Column 5"
            Groups(2).Columns(1).DataType=   8
            Groups(2).Columns(1).FieldLen=   256
            Groups(2).Columns(2).Width=   1640
            Groups(2).Columns(2).Caption=   "Tipo "
            Groups(2).Columns(2).Name=   "TIPO"
            Groups(2).Columns(2).DataField=   "Column 6"
            Groups(2).Columns(2).DataType=   8
            Groups(2).Columns(2).FieldLen=   256
            Groups(2).Columns(3).Width=   1508
            Groups(2).Columns(3).Caption=   "Decidido"
            Groups(2).Columns(3).Name=   "FECOBJ"
            Groups(2).Columns(3).DataField=   "Column 7"
            Groups(2).Columns(3).DataType=   8
            Groups(2).Columns(3).FieldLen=   256
            Groups(2).Columns(4).Width=   1244
            Groups(2).Columns(4).Caption=   "Web"
            Groups(2).Columns(4).Name=   "WEB"
            Groups(2).Columns(4).DataField=   "Column 8"
            Groups(2).Columns(4).DataType=   8
            Groups(2).Columns(4).FieldLen=   256
            Groups(2).Columns(4).Style=   2
            Groups(2).Columns(5).Width=   1376
            Groups(2).Columns(5).Caption=   "Mail"
            Groups(2).Columns(5).Name=   "MAIL"
            Groups(2).Columns(5).DataField=   "Column 9"
            Groups(2).Columns(5).DataType=   8
            Groups(2).Columns(5).FieldLen=   256
            Groups(2).Columns(5).Style=   2
            Groups(2).Columns(6).Width=   1085
            Groups(2).Columns(6).Caption=   "Carta"
            Groups(2).Columns(6).Name=   "IMP"
            Groups(2).Columns(6).DataField=   "Column 10"
            Groups(2).Columns(6).DataType=   11
            Groups(2).Columns(6).FieldLen=   256
            Groups(2).Columns(6).Style=   2
            Groups(2).Columns(7).Width=   1349
            Groups(2).Columns(7).Visible=   0   'False
            Groups(2).Columns(7).Caption=   "ANTCIERRE"
            Groups(2).Columns(7).Name=   "ANTCIERRE"
            Groups(2).Columns(7).DataField=   "Column 11"
            Groups(2).Columns(7).DataType=   8
            Groups(2).Columns(7).FieldLen=   256
            _ExtentX        =   20029
            _ExtentY        =   5847
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.PictureBox picPub 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   -74940
         ScaleHeight     =   435
         ScaleWidth      =   9480
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   3840
         Width           =   9480
         Begin VB.CommandButton cmdRestaurarPub 
            Caption         =   "&Restaurar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   60
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdListadoPub 
            Caption         =   "&Listado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1140
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.Timer Timer1 
            Interval        =   60000
            Left            =   -120
            Top             =   -120
         End
      End
      Begin VB.PictureBox picGridPub 
         Appearance      =   0  'Flat
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3315
         Left            =   -74880
         ScaleHeight     =   3315
         ScaleWidth      =   11235
         TabIndex        =   12
         Top             =   480
         Width           =   11235
         Begin SSDataWidgets_B.SSDBGrid sdbgPub 
            Height          =   3315
            Left            =   0
            TabIndex        =   13
            Top             =   0
            Width           =   11235
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   6
            stylesets.count =   1
            stylesets(0).Name=   "Activa"
            stylesets(0).BackColor=   13172735
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmOFEPet.frx":0E36
            UseGroups       =   -1  'True
            BeveColorScheme =   1
            BevelColorFace  =   12632256
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Groups.Count    =   3
            Groups(0).Width =   6138
            Groups(0).Caption=   "Proveedores"
            Groups(0).HasHeadForeColor=   -1  'True
            Groups(0).HasHeadBackColor=   -1  'True
            Groups(0).HeadForeColor=   16777215
            Groups(0).HeadBackColor=   8421504
            Groups(0).Columns.Count=   2
            Groups(0).Columns(0).Width=   2117
            Groups(0).Columns(0).Caption=   "C�digo"
            Groups(0).Columns(0).Name=   "PROVECOD"
            Groups(0).Columns(0).DataField=   "Column 0"
            Groups(0).Columns(0).DataType=   8
            Groups(0).Columns(0).FieldLen=   256
            Groups(0).Columns(0).Locked=   -1  'True
            Groups(0).Columns(1).Width=   4022
            Groups(0).Columns(1).Caption=   "Denominaci�n"
            Groups(0).Columns(1).Name=   "PROVEDEN"
            Groups(0).Columns(1).DataField=   "Column 1"
            Groups(0).Columns(1).DataType=   8
            Groups(0).Columns(1).FieldLen=   256
            Groups(0).Columns(1).Locked=   -1  'True
            Groups(1).Width =   6218
            Groups(1).Caption=   "PublicacionPetOfe"
            Groups(1).HasHeadForeColor=   -1  'True
            Groups(1).HasHeadBackColor=   -1  'True
            Groups(1).HeadForeColor=   16777215
            Groups(1).HeadBackColor=   8421504
            Groups(1).Columns.Count=   2
            Groups(1).Columns(0).Width=   3731
            Groups(1).Columns(0).Caption=   "Fecha"
            Groups(1).Columns(0).Name=   "FECHAPUBOFE"
            Groups(1).Columns(0).Alignment=   2
            Groups(1).Columns(0).DataField=   "Column 2"
            Groups(1).Columns(0).DataType=   8
            Groups(1).Columns(0).FieldLen=   256
            Groups(1).Columns(0).Locked=   -1  'True
            Groups(1).Columns(1).Width=   2487
            Groups(1).Columns(1).Caption=   "Activada"
            Groups(1).Columns(1).Name=   "PUBOFE"
            Groups(1).Columns(1).CaptionAlignment=   2
            Groups(1).Columns(1).DataField=   "Column 3"
            Groups(1).Columns(1).DataType=   8
            Groups(1).Columns(1).FieldLen=   256
            Groups(1).Columns(1).Style=   2
            Groups(2).Width =   5794
            Groups(2).Caption=   "PublicacionObj"
            Groups(2).HasHeadForeColor=   -1  'True
            Groups(2).HasHeadBackColor=   -1  'True
            Groups(2).HeadForeColor=   16777215
            Groups(2).HeadBackColor=   8421504
            Groups(2).Columns.Count=   2
            Groups(2).Columns(0).Width=   2884
            Groups(2).Columns(0).Caption=   "FechaObj"
            Groups(2).Columns(0).Name=   "FECHAPUBOBJ"
            Groups(2).Columns(0).Alignment=   2
            Groups(2).Columns(0).DataField=   "Column 4"
            Groups(2).Columns(0).DataType=   8
            Groups(2).Columns(0).FieldLen=   256
            Groups(2).Columns(1).Width=   2910
            Groups(2).Columns(1).Caption=   "ActivadaObj"
            Groups(2).Columns(1).Name=   "PUBOBJ"
            Groups(2).Columns(1).CaptionAlignment=   2
            Groups(2).Columns(1).DataField=   "Column 5"
            Groups(2).Columns(1).DataType=   8
            Groups(2).Columns(1).FieldLen=   256
            Groups(2).Columns(1).Style=   2
            _ExtentX        =   19817
            _ExtentY        =   5847
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.PictureBox picNavigate 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   60
         ScaleHeight     =   435
         ScaleWidth      =   9660
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   3840
         Width           =   9660
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3885
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdNuevas 
            Caption         =   "&Comunicaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   60
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   60
            Width           =   1365
         End
         Begin VB.CommandButton cmdEliminar 
            Caption         =   "&Eliminar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1575
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "&Restaurar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2730
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.Timer timSelProve 
            Interval        =   60000
            Left            =   -120
            Top             =   -120
         End
      End
   End
End
Attribute VB_Name = "frmOFEPet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables para func. combos
Public bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
' Variable para func. checkbox
Private bRespetarCheckBox As Boolean


'Variable de control de flujo de proceso
Public Accion As AccionesSummit

Public g_bVisor As Boolean

'Variables de seguridad
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRPerfUON As Boolean
Private bRMat As Boolean 'Se aplica a la combo de procesos
Public bREqp As Boolean  'Se aplica a la combo de procesos y a los proveedores
Public bRComp As Boolean ' 'Se aplica a la combo de procesos y a los proveedores
Private bPeticiones As Boolean ' incluye peticiones de oferta y comunicaci�n de objetivos
Private bEli As Boolean
Private bModPub As Boolean 'Activar o desactivar publicaciones
Private bRCompResp As Boolean
Private bCierre As Boolean ' notificaciones de cierre de proceso
Public bOrdenCompra As Boolean
Private bModifFecLimit As Boolean
Private bModSubasta As Boolean
Private bRUsuAper As Boolean
Public bComunAsigEqp As Boolean
Public bComunAsigComp As Boolean
Private m_bModifResponsable  As Boolean
Private g_bSoloInvitado As Boolean
Private m_vTZHora As Variant

Private m_bRestProvMatComp As Boolean

'Proceso seleccionado
Public oProcesoSeleccionado As cProceso
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGruposMN1 As CGruposMatNivel1

' Interface para manejar las peticiones
Public oIPet As IPeticiones
'interface para manejar las publicaciones
Private oIPub As IPublicaciones

'Coleccion que contendra las publicaciones
Private oPublicaciones As CPublicaciones
' Coleccion de procesos a cargar
Public oProcesos As CProcesos
' Coleccion de peticiones actuales
Public oPets As CPetOfertas

'Como optimizacion quedaria que fuesen colecciones locales
Public oProveedores As CProveedores

' Colecciones necesarias en frmOFEPEtanya para saber las denominaciones
' Lo cargamos aqu� para no ralentizar el formulario de petofeanya
Public oDestinos As CDestinos
Public oPagos As CPagos
Public oUnidades As CUnidades
' variables para determinar que radio button activar/desactivar en frmTIPOCom
Private bNotificacion As Boolean
Private bOferta As Boolean
Private bObjetivo As Boolean
Private bAvisoDespub As Boolean
Private bPubMatProve As Boolean

' Variables para guardar selecci�n de la dialog frmTIPOCom y frmTIPOCom2
Public bOpcObj As Boolean
Public bOpcOfe As Boolean
Public bOpcAdj As Boolean
Public bOpcAviso As Boolean
' coleccion para guardar los proveedores adjudicados para NOTIFICACIONES
Public oProvesAdj As CProveedores
' Contiene la fecha de la ULTIMA reunion con objetivos fijados que se pasa a NUEVOS OBJETIVOS
Public FechaReunion As Date
' variable para listado
Public ofrmLstPROCE As frmLstPROCE

'Multilenguaje
Private sIdiPeticion As String
Private sIdiObjetivo As String
Private sIdiComunicProve As String
Private sIdiCodigo As String
Private sIdiProce As String
Private sIdiAdj As String
Private sIdiExclusion As String
Private sIdiMaterial As String
Private sIdiFecLimit As String
Private sIdiComunicaciones As String
Private sIdiAvisoDespub As String
Private sIdiAnulacion As String

Private m_sProceso As String
Private m_sGrupo As String
Private m_sItem As String


Private Sub ListarProceso()

    Set ofrmLstPROCE = New frmLstPROCE
    ofrmLstPROCE.sOrigen = "frmOFEPet"
    ofrmLstPROCE.g_bEsInvitado = oProcesoSeleccionado.Invitado
    If Not oProcesoSeleccionado Is Nothing Then
        ofrmLstPROCE.sdbcAnyo.Text = oProcesoSeleccionado.Anyo
        ofrmLstPROCE.sdbcGMN1Proce_4Cod.Text = oProcesoSeleccionado.GMN1Cod
        ofrmLstPROCE.sdbcGMN1Proce_4Cod_Validate False
        ofrmLstPROCE.txtCod.Text = CStr(oProcesoSeleccionado.Cod)
        ofrmLstPROCE.txtDen.Text = oProcesoSeleccionado.Den
    End If
        
    ofrmLstPROCE.Show vbModal

End Sub

Private Sub chkAvisoDespub_Click()
Dim teserror As TipoErrorSummit

If bRespetarCheckBox Then Exit Sub
If oProcesoSeleccionado Is Nothing Then Exit Sub

teserror = oProcesoSeleccionado.ActualizarConfAvisoDespub(chkAvisoDespub.Value = vbChecked)

If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    bRespetarCheckBox = True
    If chkAvisoDespub.Value = vbChecked Then
        chkAvisoDespub.Value = vbUnchecked
    Else
        chkAvisoDespub.Value = vbChecked
    End If
    Exit Sub

End If

End Sub

Private Sub chkPubMatProve_Click()
Dim teserror As TipoErrorSummit
If bRespetarCheckBox Then
    Exit Sub
End If

If oProcesoSeleccionado Is Nothing Then
    Exit Sub
End If
    
If oProcesoSeleccionado.Estado = Cerrado Or oProcesoSeleccionado.Estado = ParcialmenteCerrado Or oProcesoSeleccionado.Estado = conadjudicaciones Or oProcesoSeleccionado.Estado = ConAdjudicacionesNotificadas Then
    bRespetarCheckBox = True
    If chkPubMatProve.Value = vbChecked Then
        chkPubMatProve.Value = vbUnchecked
    Else
        chkPubMatProve.Value = vbChecked
    End If
    bRespetarCheckBox = False
    Exit Sub
    
End If

teserror = oProcesoSeleccionado.ActualizarPubMatProve(chkPubMatProve.Value = vbChecked)
bPubMatProve = chkPubMatProve.Value

If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    bRespetarCheckBox = True
    If chkPubMatProve.Value = vbChecked Then
        chkPubMatProve.Value = vbUnchecked
    Else
        chkPubMatProve.Value = vbChecked
    End If
    Exit Sub

End If
End Sub


Private Sub cmdBuscar_Click()

    If Accion = ACCPetOfeCon Then
    
        frmPROCEBuscar.bRDest = False
        frmPROCEBuscar.bRUsuAper = bRUsuAper
        frmPROCEBuscar.bRAsig = bRComp
        frmPROCEBuscar.bREqpAsig = bREqp
        frmPROCEBuscar.bRMat = bRMat
        frmPROCEBuscar.bRCompResponsable = bRCompResp
        frmPROCEBuscar.bRUsuDep = m_bRUsuDep
        frmPROCEBuscar.bRUsuUON = m_bRUsuUON
        frmPROCEBuscar.sOrigen = "frmOFEPet"
        frmPROCEBuscar.m_bProveAsigComp = bComunAsigComp
        frmPROCEBuscar.m_bProveAsigEqp = bComunAsigEqp
        frmPROCEBuscar.bRestProvMatComp = m_bRestProvMatComp
        frmPROCEBuscar.bRestProvEquComp = False
        frmPROCEBuscar.sdbcAnyo = sdbcAnyo
        frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
        frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
        
        frmPROCEBuscar.Show 1
        
    End If
End Sub

Private Sub cmdEliminar_Click()
Dim irespuesta As Integer
Dim oPetsAEli As CPetOfertas
Dim oPet As CPetOferta
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer

'**********************************************************************************
'*** Descripci�n: Elimina los registros del hist�rico seleccionadas con opcion  ***
'***                    de eliminar varios a la vez                             ***
'**********************************************************************************

    If sdbgPet.Rows = 0 Then Exit Sub
    
    If sdbgPet.SelBookmarks.Count = 0 Then
        sdbgPet.SelBookmarks.Add sdbgPet.Bookmark
    End If
    
    'Comprobaciones
    For i = 0 To sdbgPet.SelBookmarks.Count - 1
        If sdbgPet.Columns(2).CellValue(sdbgPet.SelBookmarks(i)) = "" Then Exit Sub
        'NO se permite eliminar comunicaciones anteriores al cierre parcial del proceso
        If oProcesoSeleccionado.Estado = ParcialmenteCerrado And sdbgPet.Columns("ANTCIERRE").CellValue(sdbgPet.SelBookmarks(i)) = "1" Then
            oMensajes.ImposibleEliminarComunicacionAnteriorAlCierreParcial
            Exit Sub
        End If
        
        ' NO se permite eliminar peticiones de ofertas y objetivos si el proceso est� ConAdjudicaciones
        If oProcesoSeleccionado.Estado = conadjudicaciones Then
            If sdbgPet.Columns(6).CellValue(sdbgPet.SelBookmarks(i)) = sIdiPeticion Or sdbgPet.Columns(6).CellValue(sdbgPet.SelBookmarks(i)) = sIdiObjetivo Then
                Exit Sub
            End If
        End If
    Next i
        
    If sdbgPet.SelBookmarks.Count > 1 Then
        irespuesta = oMensajes.PreguntaEliminarMonedas(sIdiComunicaciones)
    Else
        irespuesta = oMensajes.PreguntaEliminar(" " & sIdiComunicProve & " " & sdbgPet.Columns(0).Value & " (" & sdbgPet.Columns(1).Value & ")")
    End If
    
    'Elimina las peticiones seleccionadas:
    If irespuesta = vbYes Then
        Set oPetsAEli = Nothing
        Set oPetsAEli = oFSGSRaiz.Generar_CPetOfertas
        
        For i = 0 To sdbgPet.SelBookmarks.Count - 1
            ' marta, meto el valor date al final porque no se muy bien como va, para que no de error de compilacion
            oPetsAEli.Add sdbgPet.Columns(0).CellValue(sdbgPet.SelBookmarks(i)), "", 0, 0, 0, Date, sdbgPet.Columns(5).CellValue(sdbgPet.SelBookmarks(i)), "", Date
        Next i
        
        Screen.MousePointer = vbHourglass
        teserror = oIPet.EliminarPeticiones(oPetsAEli)
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESnoerror Then
            If sdbgPet.SelBookmarks.Count > 1 Then
                If teserror.NumError = TESImposibleEliminar Then
                    iIndice = 1
                    aAux = teserror.Arg1
                    inum = UBound(teserror.Arg1, 2)
                    For i = 0 To inum
                        teserror.Arg1(2, i) = oPets.Item(CStr(sdbgPet.SelBookmarks(aAux(2, i) - iIndice))).DenProve
                        sdbgPet.SelBookmarks.Remove (aAux(2, i) - iIndice)
                        iIndice = iIndice + 1
                    Next
                    oMensajes.ImposibleEliminacionMultiple 504, teserror.Arg1
                    cmdRestaurar_Click
                Else
                    basErrores.TratarError teserror
                    Exit Sub
                End If
            Else
                basErrores.TratarError teserror
                Exit Sub
            End If
        Else
            For Each oPet In oPetsAEli
                basSeguridad.RegistrarAccion AccionesSummit.ACCPetOfeEli, "Anyo:" & CStr(sdbcAnyo.Value) & "GMN1:" & CStr(sdbcGMN1_4Cod.Value) & "Proce:" & Int(sdbcProceCod.Value) & "Prove:" & sdbgPet.Columns(0).Value & "Con:" & sdbgPet.Columns(2).Value & " " & sdbgPet.Columns(3).Value
            Next
            cmdRestaurar_Click
        End If
        
    End If

End Sub


''' <summary>
''' Carga las Notificaciones
''' </summary>
''' <returns>--</returns>
''' <remarks>Llamada desde cmdNuevas; Tiempo m�ximo=0</remarks>

Private Sub CargarNotificaciones()
    Dim oPet As CPetOferta
    Dim oIAdjudicaciones As IAdjudicaciones
    
    Screen.MousePointer = vbHourglass
    
    If bComunAsigComp Then
        'Se reusa AntesDeCierreParcial para guardar si es html � txt
        Set oPets = oIPet.DevolverNotificacionesPotenciales(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    Else
        If bComunAsigEqp Then
            'Se reusa AntesDeCierreParcial para guardar si es html � txt
            Set oPets = oIPet.DevolverNotificacionesPotenciales(basOptimizacion.gCodEqpUsuario)
        Else
            'Se reusa AntesDeCierreParcial para guardar si es html � txt
            Set oPets = oIPet.DevolverNotificacionesPotenciales
        End If
    End If
    
    ' Cargamos la colecci�n de proveedores adjudicados en la �ltima reunion con resultados
    Set oIAdjudicaciones = oProcesoSeleccionado
    Set oProvesAdj = oIAdjudicaciones.DevolverProveedoresConAdjudicaciones(, , , , True)
    
            
    If gParametrosGenerales.giMail = 0 Then
        frmADJAnya.sdbgPet.Columns("MAIL").Locked = True
    End If
    
    Screen.MousePointer = vbNormal
    
    ' Solo instalaci�n de env�o de mails
    If gParametrosGenerales.giMail <> 0 Then
        
        For Each oPet In oPets
            If oPet.Email <> "" Then
                If oProvesAdj.Item(oPet.CodProve) Is Nothing Then
                    frmADJAnya.sdbgPet.AddItem 0 & Chr(m_lSeparador) & oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                Else
                    frmADJAnya.sdbgPet.AddItem 1 & Chr(m_lSeparador) & oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                End If
            Else
                If oProvesAdj.Item(oPet.CodProve) Is Nothing Then
                    frmADJAnya.sdbgPet.AddItem 0 & Chr(m_lSeparador) & oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                Else
                    frmADJAnya.sdbgPet.AddItem 1 & Chr(m_lSeparador) & oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                End If
            End If
            
        Next
    Else
        'Ni web ni emails
        For Each oPet In oPets
            DoEvents
            If oProvesAdj.Item(oPet.CodProve) Is Nothing Then
                frmADJAnya.sdbgPet.AddItem 0 & Chr(m_lSeparador) & oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
            Else
                frmADJAnya.sdbgPet.AddItem 1 & Chr(m_lSeparador) & oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
            End If
            DoEvents
        Next
    End If
    
        
    'oIBaseDeDatos.CancelarEdicion
           
    If IsNull(oProcesoSeleccionado.FechaNecesidad) Then
        frmADJAnya.lblFecNecBox = ""
    Else
        frmADJAnya.lblFecNecBox = Format(CDate(oProcesoSeleccionado.FechaNecesidad), "short date")
    End If
    If IsNull(oProcesoSeleccionado.FechaPresentacion) Then
        frmADJAnya.lblFecPresBox = ""
    Else
        frmADJAnya.lblFecPresBox = Format(CDate(oProcesoSeleccionado.FechaPresentacion), "short date")
    End If
    If IsNull(oProcesoSeleccionado.FechaUltimaReunion) Then
        frmADJAnya.LblFecCierreBox = ""
    Else
        frmADJAnya.LblFecCierreBox = Format(CDate(oProcesoSeleccionado.FechaUltimaReunion))
    End If
    frmADJAnya.Show
    
    
End Sub

''' <summary>Carga las ofertas</summary>
''' <remarks>Llamada desde: cmdNuevas_Click; Tiempo m�ximo=0</remarks>
''' <revision>LTG 11/03/2011</revision>

Private Sub CargarOfertas()
    Dim Ador As Ador.Recordset
    Dim bHaOfertadoWeb As Boolean
    Dim sContacto  As String
    Dim sProveAnt As String
    Dim bCargado As Boolean
    Dim bAdd As Boolean
    Dim sTZ As String
    Dim dtFechaIniSub As Date
    Dim dtHoraIniSub As Date
    Dim dtFechaFinSub As Date
    Dim dtHoraFinSub As Date
    Dim tzInfo As TIME_ZONE_INFO
    
    If bComunAsigComp Then
        Set Ador = oIPet.DevolverPeticionesPotenciales(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    Else
        If bComunAsigEqp Then
            Set Ador = oIPet.DevolverPeticionesPotenciales(basOptimizacion.gCodEqpUsuario)
        Else
            Set Ador = oIPet.DevolverPeticionesPotenciales
        End If
    End If
            
    frmOFEPetAnya.bModificarFecLimit = bModifFecLimit
    frmOFEPetAnya.bModSubasta = bModSubasta
    frmOFEPetAnya.bPubMatProve = bPubMatProve
        
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        frmOFEPetAnya.sdbgPet.Columns("WEB").Locked = True
    End If
    
    If gParametrosGenerales.giMail = 0 Then
        frmOFEPetAnya.sdbgPet.Columns("WEB").Locked = True
        frmOFEPetAnya.sdbgPet.Columns("MAIL").Locked = True
    End If
    
    frmOFEPetAnya.Anyo = oProcesoSeleccionado.Anyo
    frmOFEPetAnya.GMN1 = oProcesoSeleccionado.GMN1Cod
    frmOFEPetAnya.Cod = oProcesoSeleccionado.Cod
    
    If (gParametrosGenerales.giINSTWEB = ConPortal Or gParametrosGenerales.giINSTWEB = conweb) And gParametrosGenerales.giMail <> 0 Then
       sProveAnt = "*****proveedorprimero*********"
       ' instalaci�n con p�gina de presentaci�n de ofertas
        frmOFEPetAnya.sdbgPet.RemoveAll
        While Not Ador.EOF
            bAdd = True
            If oProcesoSeleccionado.ModoSubasta And Not SQLBinaryToBoolean(Ador("SUBASTA").Value) Then bAdd = False
            
            If bAdd Then
                If Ador("COD").Value <> sProveAnt Then
                    sProveAnt = Ador("COD").Value
                    bCargado = False
                End If
                If NullToStr(Ador("NOM").Value) <> "" Then
                    sContacto = NullToStr(Ador("APE").Value) & ", " & NullToStr(Ador("NOM").Value)
                Else
                    sContacto = NullToStr(Ador("APE").Value)
                End If
                bHaOfertadoWeb = False
                If Ador("EMAIL").Value <> "" And Not IsNull(Ador("EMAIL").Value) Then
                    If gParametrosGenerales.giINSTWEB = ConPortal Then
                        ' peticion via web para proveedores del portal
                        If IsNull(Ador("FSP_COD").Value) Or Ador("FSP_COD").Value = "" Then
                            DoEvents 'Via Mail
                            frmOFEPetAnya.sdbgPet.AddItem NullToStr(Ador("COD").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & NullToStr(Ador("ID").Value) & Chr(m_lSeparador) & IIf(bHaOfertadoWeb, "1", "0") & Chr(m_lSeparador) & Ador("PORT").Value & Chr(m_lSeparador) & NullToDbl0(Ador("TIPOEMAIL").Value)
                            bCargado = True
                        Else
                            bHaOfertadoWeb = RecibidaOfertaWebDelProveedor(Ador("COD").Value)
                            DoEvents 'Via Web
                            If Ador("PORT").Value = 1 Then 'Si prove enlazado solo cargo los contactos del portal
                                frmOFEPetAnya.sdbgPet.AddItem NullToStr(Ador("COD").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & NullToStr(Ador("ID").Value) & Chr(m_lSeparador) & IIf(bHaOfertadoWeb, "1", "0") & Chr(m_lSeparador) & Ador("PORT").Value & Chr(m_lSeparador) & NullToDbl0(Ador("TIPOEMAIL").Value)
                                bCargado = True
                            Else
                                If Not bCargado Then
                                    frmOFEPetAnya.sdbgPet.AddItem NullToStr(Ador("COD").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & NullToStr(Ador("ID").Value) & Chr(m_lSeparador) & IIf(bHaOfertadoWeb, "1", "0") & Chr(m_lSeparador) & Ador("PORT").Value & Chr(m_lSeparador) & NullToDbl0(Ador("TIPOEMAIL").Value)
                                    bCargado = True
                                End If
                            End If
                        End If
                    Else
                        DoEvents 'Con BS, via WEb
                        bHaOfertadoWeb = RecibidaOfertaWebDelProveedor(Ador("COD").Value)
                        frmOFEPetAnya.sdbgPet.AddItem NullToStr(Ador("COD").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & NullToStr(Ador("ID").Value) & Chr(m_lSeparador) & IIf(bHaOfertadoWeb, "1", "0") & Chr(m_lSeparador) & Ador("PORT").Value & Chr(m_lSeparador) & NullToDbl0(Ador("TIPOEMAIL").Value)
                        bCargado = True
                    End If
                Else
                    bHaOfertadoWeb = RecibidaOfertaWebDelProveedor(Ador("COD").Value)
                    DoEvents 'Via carta, Portal o BS
                    frmOFEPetAnya.sdbgPet.AddItem NullToStr(Ador("COD").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & NullToStr(Ador("ID").Value) & Chr(m_lSeparador) & IIf(bHaOfertadoWeb, "1", "0") & Chr(m_lSeparador) & Ador("PORT").Value & Chr(m_lSeparador) & NullToDbl0(Ador("TIPOEMAIL").Value)
                    bCargado = True
                End If
            End If
            
            Ador.MoveNext
        Wend
    
    Else 'Sin Portal o BS
        ' Solo instalaci�n de env�o de mails
        If gParametrosGenerales.giMail <> 0 Then
        
            frmOFEPetAnya.sdbgPet.Columns("WEB").Locked = True
        
            While Not Ador.EOF
                bAdd = True
                If oProcesoSeleccionado.ModoSubasta And Not SQLBinaryToBoolean(Ador("SUBASTA").Value) Then bAdd = False
                
                If bAdd Then
                    If NullToStr(Ador("NOM").Value) <> "" Then
                        sContacto = NullToStr(Ador("APE").Value) & ", " & NullToStr(Ador("NOM").Value)
                    Else
                        sContacto = NullToStr(Ador("APE").Value)
                    End If
                    
                    If Ador("EMAIL").Value <> "" Then
                        DoEvents 'Via Mail
                        frmOFEPetAnya.sdbgPet.AddItem NullToStr(Ador("COD").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & NullToStr(Ador("ID").Value) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & NullToDbl0(Ador("TIPOEMAIL").Value)
                    Else
                        DoEvents
                        frmOFEPetAnya.sdbgPet.AddItem NullToStr(Ador("COD").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & NullToStr(Ador("ID").Value) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & NullToDbl0(Ador("TIPOEMAIL").Value)
                    End If
                End If
                
                Ador.MoveNext
            Wend
        Else
                'Ni web ni emails
                While Not Ador.EOF
                    bAdd = True
                    If oProcesoSeleccionado.ModoSubasta And Not SQLBinaryToBoolean(Ador("SUBASTA").Value) Then bAdd = False
                    
                    If bAdd Then
                        If NullToStr(Ador("NOM").Value) <> "" Then
                            sContacto = NullToStr(Ador("APE").Value) & ", " & NullToStr(Ador("NOM").Value)
                        Else
                            sContacto = NullToStr(Ador("APE").Value)
                        End If
                        
                        frmOFEPetAnya.sdbgPet.AddItem NullToStr(Ador("COD").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & NullToStr(Ador("ID").Value) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & NullToDbl0(Ador("TIPOEMAIL").Value)
                    End If
                    
                    Ador.MoveNext
                Wend
        End If
        
    End If
    
    Ador.Close
    Set Ador = Nothing
           
    If IsNull(oProcesoSeleccionado.FechaNecesidad) Then
        frmOFEPetAnya.lblFecNecBox = ""
    Else
        frmOFEPetAnya.lblFecNecBox = Format(CDate(oProcesoSeleccionado.FechaNecesidad), "short date")
    End If
    
    If Not IsNull(oUsuarioSummit.TimeZone) And Not IsEmpty(oUsuarioSummit.TimeZone) Then
        sTZ = oUsuarioSummit.TimeZone
    Else
        'Asignar la zona horaria del equipo
        sTZ = GetTimeZone.key
    End If
    tzInfo = GetTimeZoneByKey(sTZ)
    frmOFEPetAnya.vTZHora = sTZ
        
    frmOFEPetAnya.lblTZHoraFinSub = Mid(tzInfo.DisplayName, 2, 9)
    
    'Fecha l�mite ofertas
    If IsNull(oProcesoSeleccionado.FechaMinimoLimOfertas) Then
        frmOFEPetAnya.lblFecLimit = vbNullString
        frmOFEPetAnya.lblHoraLimite = vbNullString
            
        If Not oProcesoSeleccionado.ModoSubasta Then
            frmOFEPetAnya.txtFecLimOfe = ""
            frmOFEPetAnya.txtHoraLimite.Text = ""
        End If
    Else
        ConvertirUTCaTZ DateValue(oProcesoSeleccionado.FechaMinimoLimOfertas), TimeValue(oProcesoSeleccionado.FechaMinimoLimOfertas), sTZ, dtFechaFinSub, dtHoraFinSub
            
        frmOFEPetAnya.lblFecLimit = Format(dtFechaFinSub, "short date")
        frmOFEPetAnya.lblHoraLimite = Format(dtHoraFinSub, "short time")
        
        If Not oProcesoSeleccionado.ModoSubasta Then
            ConvertirUTCaTZ DateValue(oProcesoSeleccionado.FechaMinimoLimOfertas), TimeValue(oProcesoSeleccionado.FechaMinimoLimOfertas), sTZ, dtFechaFinSub, dtHoraFinSub
            
            frmOFEPetAnya.txtFecLimOfe = Format(dtFechaFinSub, "short date")
            frmOFEPetAnya.txtHoraLimite.Text = Format(dtHoraFinSub, "short time")
        End If
    End If
    
    'Fecha inicio subasta
    If oProcesoSeleccionado.ModoSubasta Then
        ConvertirUTCaTZ DateValue(oProcesoSeleccionado.FechaInicioSubasta), TimeValue(oProcesoSeleccionado.FechaInicioSubasta), sTZ, dtFechaIniSub, dtHoraIniSub
        
        frmOFEPetAnya.lblFecInicioSubasta = Format(dtFechaIniSub, "short date")
        frmOFEPetAnya.lblHoraInicioSubasta = Format(dtHoraIniSub, "short time")
        frmOFEPetAnya.lblTZHoraIniSub = Mid(tzInfo.DisplayName, 2, 9)
    End If
    
    
    If IsNull(oProcesoSeleccionado.FechaPresentacion) Then
        frmOFEPetAnya.lblFecPresBox = ""
    Else
        frmOFEPetAnya.lblFecPresBox = Format(CDate(oProcesoSeleccionado.FechaPresentacion), "short date")
    End If
    
    frmOFEPetAnya.Show
    
End Sub

Private Sub CargarAvisos()
    Dim Ador As Ador.Recordset
    Dim sContacto  As Variant
    Dim sEmail As Variant
    Dim lID As Variant
    Dim sTZ As String
    Dim dtFechaFinSub As Date
    Dim dtHoraFinSub As Date
    Dim tzInfo As TIME_ZONE_INFO
    
    If bComunAsigComp Then
        Set Ador = oProcesoSeleccionado.DevolverProveedoresAAvisar(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    Else
        If bComunAsigEqp Then
            Set Ador = oProcesoSeleccionado.DevolverProveedoresAAvisar(basOptimizacion.gCodEqpUsuario)
        Else
            Set Ador = oProcesoSeleccionado.DevolverProveedoresAAvisar()
        End If
    End If
                            
    frmOFEAvisoAnya.sdbgPet.RemoveAll
    
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            
            If IsNull(Ador("C1ID").Value) Then
                lID = Ador("C2ID").Value
                If Not IsNull(Ador("C2NOM").Value) Then
                    sContacto = Ador("C2APE").Value & ", " & Ador("C2NOM").Value
                Else
                    sContacto = Ador("C2APE").Value
                End If
                sEmail = Ador("C2EMAIL").Value
            Else
                lID = Ador("C1ID").Value
                If Not IsNull(Ador("C1NOM").Value) Then
                    sContacto = Ador("C1APE").Value & ", " & Ador("C1NOM").Value
                Else
                    sContacto = Ador("C1APE").Value
                End If
                sEmail = Ador("C1EMAIL").Value
            End If
            
            frmOFEAvisoAnya.sdbgPet.AddItem NullToStr(Ador("PROVE").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEN").Value) & Chr(m_lSeparador) & NullToStr(sContacto) & Chr(m_lSeparador) & NullToStr(lID) & Chr(m_lSeparador) & NullToStr(sEmail) & Chr(m_lSeparador) & NullToStr(Ador.Fields("EQP").Value) & Chr(m_lSeparador) & NullToStr(Ador.Fields("COM").Value)
            
            Ador.MoveNext
        Wend
    
        Ador.Close
    End If
    Set Ador = Nothing
           
    frmOFEAvisoAnya.Anyo = oProcesoSeleccionado.Anyo
    frmOFEAvisoAnya.GMN1 = oProcesoSeleccionado.GMN1Cod
    frmOFEAvisoAnya.Cod = oProcesoSeleccionado.Cod
    
    If Not IsNull(oUsuarioSummit.TimeZone) And Not IsEmpty(oUsuarioSummit.TimeZone) Then
        sTZ = oUsuarioSummit.TimeZone
    Else
        'Asignar la zona horaria del equipo
        sTZ = GetTimeZone.key
    End If
    tzInfo = GetTimeZoneByKey(sTZ)
    frmOFEAvisoAnya.vTZHora = sTZ
        
    frmOFEAvisoAnya.lblTZHoraFin = Mid(tzInfo.DisplayName, 2, 9)
    
    If IsNull(oProcesoSeleccionado.FechaMinimoLimOfertas) Then
        frmOFEAvisoAnya.lblFecLimit.caption = ""
        frmOFEAvisoAnya.lblFechaLimite.Visible = False
        frmOFEAvisoAnya.lblFecLimit.Visible = False
        frmOFEAvisoAnya.lblHoraLimite.caption = ""
        frmOFEAvisoAnya.lblHoraLimite.Visible = False
    Else
        ConvertirUTCaTZ DateValue(oProcesoSeleccionado.FechaMinimoLimOfertas), TimeValue(oProcesoSeleccionado.FechaMinimoLimOfertas), sTZ, dtFechaFinSub, dtHoraFinSub
        
        frmOFEAvisoAnya.lblFecLimit = Format(dtFechaFinSub, "short date")
        frmOFEAvisoAnya.lblHoraLimite = Format(dtHoraFinSub, "short time")
    End If
    
    
    frmOFEAvisoAnya.Show
    
End Sub

Private Sub cmdlistado_Click()
ListarProceso
End Sub

Private Sub cmdListadoPub_Click()

    Set ofrmLstPROCE = New frmLstPROCE
    ofrmLstPROCE.sOrigen = "frmOFEPet"
    ofrmLstPROCE.g_bEsInvitado = oProcesoSeleccionado.Invitado
    ofrmLstPROCE.sdbcAnyo.Text = oProcesoSeleccionado.Anyo
    ofrmLstPROCE.sdbcGMN1_4Cod.Text = oProcesoSeleccionado.GMN1Cod
    ofrmLstPROCE.sdbcGMN1_4Cod_Validate False
    ofrmLstPROCE.txtCod.Text = CStr(oProcesoSeleccionado.Cod)
    ofrmLstPROCE.txtDen.Text = oProcesoSeleccionado.Den
    ofrmLstPROCE.chkComPet.Value = vbUnchecked
    ofrmLstPROCE.chkComObj.Value = vbUnchecked
    ofrmLstPROCE.chkComAdj.Value = vbUnchecked
    ofrmLstPROCE.chkComSinPet.Value = vbUnchecked
    ofrmLstPROCE.chkComExc.Value = vbUnchecked
    ofrmLstPROCE.Show vbModal

End Sub


''' <summary>
''' Muestra la pantalla de las posibles comunicaciones con proveedores
''' </summary>
''' <returns>--</returns>
''' <remarks>Llamada desde=Al pulsar el boton Comunicacion; Tiempo m�ximo=0</remarks>

Private Sub cmdNuevas_Click()
    Dim iOptSel As Integer
    
    If oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
        bOpcObj = False
        bOpcOfe = False
        bOpcAdj = False
        bOpcAviso = False
                
        iOptSel = MostrarFormTipoCom2(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, bObjetivo, bAvisoDespub, Me.cmdNuevas.Left + 500, Me.cmdNuevas.Top + 2945)
        Select Case iOptSel
            Case 1
                bOpcOfe = True
            Case 2
                bOpcObj = True
            Case 3
                bOpcAdj = True
            Case 4
                bOpcAviso = True
        End Select
        
        If bOpcOfe Then
            CargarOfertas
        Else
            If bOpcObj Then
                CargarObjetivos
            Else
                If bOpcAdj Then
                    CargarNotificaciones
                Else
                    If bOpcAviso Then
                        CargarAvisos
                    End If
                End If
            End If
        End If
    Else
        bOpcObj = False
        bOpcOfe = False
        bOpcAviso = False
        
        If bNotificacion Then
            CargarNotificaciones
        Else
            If bOferta And bObjetivo Then
                iOptSel = MostrarFormTipoCom(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, bAvisoDespub, cmdNuevas.Left + 500, cmdNuevas.Top + 2945)
                Select Case iOptSel
                    Case 1
                        bOpcOfe = True
                    Case 2
                        bOpcObj = True
                    Case 3
                        bOpcAviso = True
                End Select
            Else
                If bAvisoDespub Then
                    iOptSel = MostrarFormTipoCom3(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, bObjetivo, Me.cmdNuevas.Left + 500, Me.cmdNuevas.Top + 2945)
                    Select Case iOptSel
                        Case 1
                            bOpcOfe = True
                        Case 2
                            bOpcObj = True
                        Case 3
                            bOpcAviso = True
                    End Select
                Else
                    If bObjetivo Then
                        CargarObjetivos
                    Else
                        CargarOfertas
                    End If
                End If
            End If
        End If
                     
        If bOpcOfe Then
            CargarOfertas
        Else
            If bOpcObj Then
                CargarObjetivos
            Else
                If bOpcAviso Then
                    CargarAvisos
                End If
            End If
        End If
    End If
End Sub

''' <summary>
''' Carga las ofertas a comunicar objetivos
''' </summary>
''' <remarks>Llamada desde: cmdNuevas_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarObjetivos()
    Dim oPet As CPetOferta
    'para obtener cod. prov. del portal
    Dim oProves As CProveedores
    Dim sTZ As String
    Dim dtFechaFinSub As Date
    Dim dtHoraFinSub As Date
    Dim tzInfo As TIME_ZONE_INFO
    
    If bComunAsigComp Then
        'Se reusa AntesDeCierreParcial para guardar si es html � txt
        Set oPets = oIPet.DevolverComunicacionObjPotenciales(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    Else
        If bComunAsigEqp Then
            'Se reusa AntesDeCierreParcial para guardar si es html � txt
            Set oPets = oIPet.DevolverComunicacionObjPotenciales(basOptimizacion.gCodEqpUsuario)
        Else
            'Se reusa AntesDeCierreParcial para guardar si es html � txt
            Set oPets = oIPet.DevolverComunicacionObjPotenciales
        End If
    End If
    
    FrmOBJAnya.bModificarFecLimit = bModifFecLimit
    FrmOBJAnya.bModSubasta = bModSubasta
    
    FrmOBJAnya.Anyo = oProcesoSeleccionado.Anyo
    FrmOBJAnya.GMN1 = oProcesoSeleccionado.GMN1Cod
    FrmOBJAnya.Cod = oProcesoSeleccionado.Cod
    
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        FrmOBJAnya.sdbgPet.Columns("WEB").Locked = True
    End If
    
    If gParametrosGenerales.giMail = 0 Then
        FrmOBJAnya.sdbgPet.Columns("WEB").Locked = True
        FrmOBJAnya.sdbgPet.Columns("MAIL").Locked = True
    End If
    
    If (gParametrosGenerales.giINSTWEB = conweb Or gParametrosGenerales.giINSTWEB = ConPortal) And gParametrosGenerales.giMail <> 0 Then
       ' instalaci�n con posibilidad de enviar mail y pagina web
        FrmOBJAnya.sdbgPet.RemoveAll
    
        For Each oPet In oPets
            If oPet.Email <> "" Then
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    ' peticion via web para proveedores del portal
                    Set oProves = Nothing
                    Set oProves = oFSGSRaiz.generar_CProveedores
                    oProves.Add oPet.CodProve, oPet.DenProve
                    oProves.CargarDatosProveedor oPet.CodProve
                    If IsNull(oProves.Item(1).CodPortal) Or oProves.Item(1).CodPortal = "" Then
                        DoEvents
                        FrmOBJAnya.sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                    Else
                        DoEvents
                        FrmOBJAnya.sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                    End If
                Else
                    DoEvents
                    FrmOBJAnya.sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                End If
            Else
                'sin email
                DoEvents
                FrmOBJAnya.sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
            End If
           
        Next
    
    Else
        ' Solo instalaci�n de env�o de mails
        If gParametrosGenerales.giMail <> 0 Then
        
            FrmOBJAnya.sdbgPet.Columns("WEB").Locked = True
        
                For Each oPet In oPets
                    
                    If oPet.Email <> "" Then
                        DoEvents
                        FrmOBJAnya.sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                    Else
                        DoEvents
                        FrmOBJAnya.sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
                    End If
                    
                Next
        Else
                'Ni web ni emails
                For Each oPet In oPets
                    DoEvents
                    FrmOBJAnya.sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & oPet.Email & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & oPet.Tfno & Chr(m_lSeparador) & oPet.Tfno2 & Chr(m_lSeparador) & oPet.Fax & Chr(m_lSeparador) & oPet.tfnomovil & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.PortContacto) & Chr(m_lSeparador) & BooleanToSQLBinary(oPet.AntesDeCierreParcial)
            
                Next
        End If
        
    End If
    
    If IsNull(oProcesoSeleccionado.FechaNecesidad) Then
        FrmOBJAnya.lblFecNecBox = ""
    Else
        FrmOBJAnya.lblFecNecBox = Format(CDate(oProcesoSeleccionado.FechaNecesidad), "short date")
    End If
    
    If Not IsNull(oUsuarioSummit.TimeZone) And Not IsEmpty(oUsuarioSummit.TimeZone) Then
        sTZ = oUsuarioSummit.TimeZone
    Else
        'Asignar la zona horaria del equipo
        sTZ = GetTimeZone.key
    End If
    tzInfo = GetTimeZoneByKey(sTZ)
    FrmOBJAnya.vTZHora = sTZ
        
    FrmOBJAnya.lblTZHoraFin = Mid(tzInfo.DisplayName, 2, 9)
    
    'Fecha l�mite de ofertas
    If IsNull(oProcesoSeleccionado.FechaMinimoLimOfertas) Then
        FrmOBJAnya.txtFecLimOfe = ""
        FrmOBJAnya.lblFecLimit = ""
        FrmOBJAnya.lblHoraLimite = ""
        FrmOBJAnya.txtHoraLimite.Text = ""
    Else
        ConvertirUTCaTZ DateValue(oProcesoSeleccionado.FechaMinimoLimOfertas), TimeValue(oProcesoSeleccionado.FechaMinimoLimOfertas), sTZ, dtFechaFinSub, dtHoraFinSub
        
        FrmOBJAnya.txtFecLimOfe = Format(dtFechaFinSub, "short date")
        FrmOBJAnya.lblFecLimit = Format(dtFechaFinSub, "short date")
        FrmOBJAnya.txtHoraLimite.Text = Format(dtHoraFinSub, "short time")
        FrmOBJAnya.lblHoraLimite = Format(dtHoraFinSub, "short time")
    End If
    
    If IsNull(oProcesoSeleccionado.FechaPresentacion) Then
        FrmOBJAnya.lblFecPresBox = ""
    Else
        FrmOBJAnya.lblFecPresBox = Format(CDate(oProcesoSeleccionado.FechaPresentacion), "short date")
    End If
    
    ' fecha de reunion con objetivos la devuelve
    FrmOBJAnya.LblFecReuBox = Format(FechaReunion, "short date") & " " & Format(CDate(FechaReunion), "short time")
    
    FrmOBJAnya.Show
    
End Sub

Private Sub cmdResponsable_Click()
    'Si no tiene permisos para modificar el responsable del proceso muestra directamente el detalle del responsable,
    'si no muestra el men� del responsable de proceso
    
    If m_bModifResponsable = False Or oProcesoSeleccionado.Estado >= conadjudicaciones Then
        VerDetalleResponsable
    Else
        Set MDI.g_ofrmOrigenResponsable = Me
        PopupMenu MDI.mnuPOPUPResponsable
    End If
End Sub

Public Sub cmdRestaurar_Click()
    
    ProcesoSeleccionado

End Sub

Private Sub cmdRestaurarPub_Click()
    sstabPet_Click 0
End Sub

Private Sub Form_Activate()
    
    Unload frmOFEPetAnya
    Unload FrmOBJAnya
    Unload frmADJAnya
    
    If g_bVisor Then
        g_bVisor = False
        LockWindowUpdate 0&
    End If
    
    'Si ha cambiado la zona horaria volver a obtener los datos del proceso para refrescar las fechas
    If oUsuarioSummit.TimeZone <> m_vTZHora Then
        m_vTZHora = oUsuarioSummit.TimeZone
    
        If Not oProcesoSeleccionado Is Nothing Then
            oProcesoSeleccionado.CargarDatosGeneralesProceso bSinpres:=True
        End If
    End If
End Sub

Private Sub Form_Load()

    Me.Height = 5325
    Me.Width = 11715
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    
    PonerFieldSeparator Me
    
    Accion = ACCPetOfeCon
    
    ProceSelector1.ModoExtendido = True
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    CargarAnyos
    
    g_bSoloInvitado = False
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If oUsuarioSummit.EsInvitado Then
            If oUsuarioSummit.Acciones Is Nothing Then
                g_bSoloInvitado = True
            Else
                If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEConsulta)) Is Nothing Then
                    g_bSoloInvitado = True
                End If
            End If
        End If
    End If
    
    SetFormTZ
    
    ConfigurarSeguridad
    
    cmdNuevas.Enabled = False
    cmdEliminar.Enabled = False
    cmdRestaurar.Enabled = False
    cmdListado.Enabled = False
    sstabPet.TabVisible(1) = False
    
    If basParametros.gParametrosGenerales.giINSTWEB = SinWeb Then
        sstabPet.TabVisible(1) = False
    End If
    
    If Not basParametros.gParametrosGenerales.gbMultiMaterial Then
        chkPubMatProve.Visible = False
    End If
    
    Set oProcesos = oFSGSRaiz.generar_CProcesos
    
    If basPublic.gParametrosInstalacion.giLockTimeOut <= 0 Then
        basPublic.gParametrosInstalacion.giLockTimeOut = basParametros.gParametrosGenerales.giLockTimeOut
    End If
    
    
    oProcesos.EstablecerTiempoDeBloqueo basPublic.gParametrosInstalacion.giLockTimeOut
    
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    Set oPagos = oFSGSRaiz.Generar_CPagos
    
    oDestinos.CargarTodosLosDestinos , , , , , , , , , , True, True
    oUnidades.CargarTodasLasUnidades
    oPagos.CargarTodosLosPagos
    
    If g_bVisor Then
        LockWindowUpdate Me.hWnd
    End If
     'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And bRMat Then
        CargarGMN1Automaticamente
    End If
End Sub

''' <summary>Asigna la zona horaria por defecto que se usar� para las horas de subasta</summary>

Private Sub SetFormTZ()
    If Not IsNull(oUsuarioSummit.TimeZone) And Not IsEmpty(oUsuarioSummit.TimeZone) Then
        m_vTZHora = oUsuarioSummit.TimeZone
    Else
        'Asignar la zona horaria del equipo
        m_vTZHora = GetTimeZone.key
    End If
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Arrange()
    If Me.Height < 2200 Then Exit Sub
    If Me.Width < 2000 Then Exit Sub
    
    sstabPet.Height = Me.Height - 1050
    sstabPet.Width = Me.Width - 200
    picPet.Width = sstabPet.Width - 200
    picPet.Height = sstabPet.Height - 1000
    picGridPub.Width = picPet.Width
    picGridPub.Height = picPet.Height
    sdbgPet.Height = picPet.Height
    sdbgPet.Width = picPet.Width
    sdbgPub.Height = sdbgPet.Height
    sdbgPub.Width = sdbgPet.Width
    
    picNavigate.Top = sstabPet.Height - 500
    picPub.Top = picNavigate.Top
    
    
    sdbgPet.Groups(2).Width = sdbgPet.Width * 0.45
    sdbgPet.Groups(1).Width = sdbgPet.Width * 0.2
    sdbgPet.Groups(0).Width = sdbgPet.Width * 0.3
    sdbgPet.Groups(2).Columns(0).Width = 950
    sdbgPet.Groups(2).Columns(2).Width = 800
    sdbgPet.Groups(2).Columns(3).Width = 950
    
    If Not oProcesoSeleccionado Is Nothing Then
        If oProcesoSeleccionado.TieneObjetivos Then
            sdbgPub.Groups(0).Width = sdbgPet.Width * 0.4 * 0.9
            sdbgPub.Groups(1).Width = sdbgPet.Width * 0.3 * 0.9
            sdbgPub.Groups(2).Width = sdbgPet.Width * 0.3 * 0.9
        Else
            sdbgPub.Groups(0).Width = sdbgPet.Width * 0.6 * 0.9
            sdbgPub.Groups(1).Width = sdbgPet.Width * 0.4 * 0.9
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oGMN1Seleccionado = Nothing
    Set oProcesoSeleccionado = Nothing
    Set oProcesos = Nothing
    Set oGruposMN1 = Nothing
    Set oPets = Nothing
    Set oIPet = Nothing
    Set oDestinos = Nothing
    Set oUnidades = Nothing
    Set oPagos = Nothing
    Me.Visible = False
    
End Sub



Private Sub ProceSelector1_Click(ByVal Opcion As SelectorDeProcesos.PSSeleccion)
    
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    
End Sub

Private Sub sdbcAnyo_Click()
    
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    
End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
    End If
End Sub

Private Sub sdbcProceCod_Change()
    
    If Not bRespetarCombo Then
        bRespetarCombo = True
    
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        Set oProcesoSeleccionado = Nothing
        cmdNuevas.Enabled = False
        cmdEliminar.Enabled = False
        cmdRestaurar.Enabled = False
        cmdListado.Enabled = False
        cmdResponsable.Enabled = False
        cmdResponsable.ToolTipText = ""
        
        sdbgPet.RemoveAll
        sdbgPub.RemoveAll
        
        bRespetarCombo = False
        

        bCargarComboDesde = True
        
        bRespetarCheckBox = True
        Me.picAvisoDespub.Enabled = False
        Me.chkAvisoDespub.Value = vbUnchecked
        Me.chkPubMatProve.Value = vbUnchecked

        bRespetarCheckBox = False
        
        
        sstabPet.Tab = 0
        
        sstabPet.TabVisible(1) = False
        
    
    End If
    
End Sub

Private Sub sdbcProceCod_CloseUp()
    
    If sdbcProceCod.Value = "..." Or Trim(sdbcProceCod.Value) = "" Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcProceCod_DropDown()
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim oProceso As cProceso
    Dim lIdPerfil As Long
    
    sdbcProceCod.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProcesoSeleccionado = Nothing
    cmdEliminar.Enabled = False
    cmdRestaurar.Enabled = False
    cmdListado.Enabled = False
    cmdResponsable.Enabled = False
    cmdResponsable.ToolTipText = ""
    cmdNuevas.Enabled = False
    
    Select Case ProceSelector1.Seleccion
        
        
        Case PSSeleccion.PSPendEnviarPet
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.conasignacionvalida
        Case PSSeleccion.PSPendComObj
            udtEstDesde = TipoEstadoProceso.ConObjetivosSinNotificar
            udtEstHasta = TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
        Case PSSeleccion.PSPendNotAdj
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.conadjudicaciones
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    End If
    
    For Each oProceso In oProcesos
        sdbcProceCod.AddItem oProceso.Cod & Chr(m_lSeparador) & oProceso.Den & Chr(m_lSeparador) & BooleanToSQLBinary(oProceso.Invitado)
    Next
    
    'If bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceCod_InitColumnProps()

    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProceCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceCod.Rows - 1
            bm = sdbcProceCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcProceCod_Validate(Cancel As Boolean)
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim lIdPerfil As Long
    
    If Trim(sdbcProceCod.Text = "") Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProce
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
'        oMensajes.NoValido "Codigo"
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProce
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proceso
    Screen.MousePointer = vbHourglass
    
    
    Select Case ProceSelector1.Seleccion
        
        
        Case PSSeleccion.PSPendEnviarPet
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.conasignacionvalida
        Case PSSeleccion.PSPendComObj
            udtEstDesde = TipoEstadoProceso.ConObjetivosSinNotificar
            udtEstHasta = TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
        Case PSSeleccion.PSPendNotAdj
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.conadjudicaciones
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    oProcesos.CargarTodosLosProcesosDesde 1, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod), , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
        
    If oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
'        oMensajes.NoValido "Proceso"
        oMensajes.NoValido sIdiProce
        Set oProcesoSeleccionado = Nothing
    Else
        bRespetarCombo = True
        sdbcProceDen.Text = oProcesos.Item(1).Den
        
        sdbcProceCod.Columns("COD").Text = sdbcProceCod.Text
        sdbcProceCod.Columns("DEN").Text = sdbcProceDen.Text
        sdbcProceCod.Columns("INVI").Text = BooleanToSQLBinary(oProcesos.Item(1).Invitado)
        
        bRespetarCombo = False
        Set oProcesoSeleccionado = Nothing
        Set oProcesoSeleccionado = oProcesos.Item(1)
      
        bCargarComboDesde = False
        ProcesoSeleccionado
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 07/01/2013</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEOfeyObj)) Is Nothing) Then
        bPeticiones = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEliminar)) Is Nothing) Then
        bEli = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEModifPublic)) Is Nothing) Then
        bModPub = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestComprador)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        bRComp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestEquipo)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        bREqp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestResponsable)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        bRCompResp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestMatComp)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        bRMat = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVECierre)) Is Nothing) Then
        bCierre = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEPermOrdCompra)) Is Nothing) Then
        bOrdenCompra = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEModifFecLimit)) Is Nothing) Then
        bModifFecLimit = False
    Else
        bModifFecLimit = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModifSubasta)) Is Nothing Then
        bModSubasta = True
    Else
        bModSubasta = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuAper)) Is Nothing) Then
        bRUsuAper = True
    End If
    
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestAsigEqp)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        bComunAsigEqp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestAsigComp)) Is Nothing) And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        bComunAsigComp = True
    End If
    
    m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuUON)) Is Nothing)
    m_bRUsuDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestUsuDep)) Is Nothing)
    m_bRPerfUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestPerfUON)) Is Nothing)
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEModifResponsable)) Is Nothing) Then
        m_bModifResponsable = True
    Else
        m_bModifResponsable = False
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestProvMatComp)) Is Nothing Then
        m_bRestProvMatComp = True
    Else
        m_bRestProvMatComp = False
    End If

    ConfigurarBotones
End Sub

Private Sub ConfigurarBotones()


If Not bPeticiones And Not bCierre Then
    cmdNuevas.Visible = False
    cmdEliminar.Left = cmdNuevas.Left
    cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 150
    cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 150
ElseIf bPeticiones And bCierre Then
    cmdNuevas.Visible = True
    cmdNuevas.Left = 100
    cmdEliminar.Left = cmdNuevas.Left + cmdNuevas.Width + 150
    cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 150
    cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 150
End If

If Not bEli Then
    cmdEliminar.Visible = False
    cmdRestaurar.Left = cmdEliminar.Left
    cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 150
ElseIf bEli Then
    cmdEliminar.Visible = True
    cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 150
    cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 150
End If

End Sub

''' <summary>
''' Cargar las comunicaciones de un proceso
''' </summary>
''' <remarks>Llamada desde: sdbcProceCod_CloseUp    sdbcProceCod_Validate   sdbcProceDen_CloseUp    CargarProcesoConBusqueda    cmdRestaurar_Click; Tiempo m�ximo: 0,2</remarks>
Public Sub ProcesoSeleccionado()
Dim sCod As String
Dim vFecha As Variant
Dim oSobre As CSobre

    sCod = sdbcGMN1_4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod))
    sCod = CStr(sdbcAnyo) & sCod & sdbcProceCod
       
    Set oProcesoSeleccionado = Nothing
    Set oProcesoSeleccionado = oProcesos.Item(sCod)
    
    If oProcesoSeleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    oProcesoSeleccionado.CargarDatosGeneralesProceso bSinpres:=True
    'Si es de Admin pub y alguna de las fechas de apertura prevista es null es pq han cambiado a mano los datos
    'le damos un mensaje y no lo cargamos
    If oProcesoSeleccionado.AdminPublica Then
        For Each oSobre In oProcesoSeleccionado.Sobres
            If IsNull(oSobre.FechaAperturaPrevista) Then
                oMensajes.FechaDeSobreCorrupta
                sdbcProceCod.Value = ""
                Set oProcesoSeleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Next
    End If
    
    If oProcesoSeleccionado.Invitado Then
        bPeticiones = False
        bEli = False
        bRMat = False
        bModPub = False
        bCierre = False
        bOrdenCompra = False
        bModifFecLimit = False
        bModSubasta = False
        m_bModifResponsable = False
        ConfigurarBotones
        sdbgPub.Columns("PUBOFE").Locked = True
        sdbgPub.Columns("PUBOBJ").Locked = True
        'picAvisoDespub.Visible = False
        picAvisoDespub.Enabled = False
    Else
        sdbgPub.Columns("PUBOFE").Locked = False
        sdbgPub.Columns("PUBOBJ").Locked = False
        ConfigurarSeguridad
        'picAvisoDespub.Visible = True
        picAvisoDespub.Enabled = True
    End If
    
       
    'se activa el bot�n en funci�n de estado del proceso y de las opciones de seguridad
    
    If oProcesoSeleccionado.Estado >= conadjudicaciones Then
        sstabPet.Tab = 0
        sstabPet.TabVisible(1) = False
    Else
        If basParametros.gParametrosGenerales.giINSTWEB = SinWeb Then
            sstabPet.Tab = 0
            sstabPet.TabVisible(1) = False
        Else
            sstabPet.Tab = 0
            sstabPet.TabVisible(1) = True
        End If
    End If
    
    If bEli Then
        cmdEliminar.Enabled = True
    Else
        cmdEliminar.Enabled = False
    End If
    
    cmdRestaurar.Enabled = True
    cmdListado.Enabled = True
    
    CargarHistPeticiones (TipoOrdenacionPeticiones.OrdPorCodProve)

    If sstabPet.Tab = 1 Then
        sstabPet_Click 0
    End If
        
        
    'Muestra el responsable en el tooltiptext del bot�n de responsable
    cmdResponsable.Enabled = True
    If Not oProcesoSeleccionado.responsable Is Nothing Then
        If oProcesoSeleccionado.responsable.Cod <> "" Then
            If oProcesoSeleccionado.responsable.nombre <> "" Then
                cmdResponsable.ToolTipText = oProcesoSeleccionado.responsable.nombre & " " & oProcesoSeleccionado.responsable.Apel
            Else
                cmdResponsable.ToolTipText = oProcesoSeleccionado.responsable.Apel
            End If
        End If
    End If
    
        
    ' Si el proceso est� ConAdjudicaciones habilitar el bot�n de notificaciones
    If oProcesoSeleccionado.Estado >= conadjudicaciones Then
        sstabPet.Tab = 0
        sstabPet.TabVisible(1) = False
        bNotificacion = True
        bOferta = False
        bObjetivo = False
        bAvisoDespub = False
        'SEGURIDAD
        If bCierre Then
            cmdNuevas.Enabled = True
        Else
            cmdNuevas.Enabled = False
        End If
    Else
        sstabPet.Tab = 0
        If gParametrosGenerales.giINSTWEB = SinWeb Then
            sstabPet.TabVisible(1) = False
        Else
            sstabPet.TabVisible(1) = True
        End If
        
        bOferta = True
        
        If oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            bNotificacion = True
        Else
            bNotificacion = False
        End If
        
        vFecha = oProcesoSeleccionado.ComunicacionObjetivos
        If Not IsNull(vFecha) Then
            bObjetivo = True
            frmOFEPet.FechaReunion = vFecha
        Else
            bObjetivo = False
        End If
        
        If oProcesoSeleccionado.Estado > conasignacionvalida And Not IsNull(oProcesoSeleccionado.FechaMinimoLimOfertas) And oProcesoSeleccionado.ProcesoPublicado(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario) Then
            bAvisoDespub = True
        Else
            bAvisoDespub = False
        End If
        
        ' SEGURIDAD
        If bPeticiones Then
            cmdNuevas.Enabled = True
        Else
            cmdNuevas.Enabled = False
        End If
    End If

    If oProcesoSeleccionado.Estado = Cerrado Then
        'Como el proceso ha sido anulado solo podremos consultar el hist�rico
        cmdEliminar.Enabled = False
        cmdNuevas.Enabled = False
    End If
        
    picAvisoDespub.Enabled = (bPeticiones Or bModPub)
    'Me.picAvisoDespub.Enabled = True
    bRespetarCheckBox = True
    Me.chkAvisoDespub.Value = IIf(oProcesoSeleccionado.AvisarDespublicacion, vbChecked, vbUnchecked)
    Me.chkPubMatProve.Value = IIf(oProcesoSeleccionado.PubMatProve, vbChecked, vbUnchecked)
    bPubMatProve = oProcesoSeleccionado.PubMatProve
    bRespetarCheckBox = False
    Screen.MousePointer = vbNormal
    
    basSeguridad.RegistrarAccion AccionesSummit.ACCPetOfeComProve, "Anyo:" & CStr(sdbcAnyo.Value) & " GMN1:" & CStr(sdbcGMN1_4Cod.Value) & " Proce:" & Int(sdbcProceCod.Value)
    
End Sub
Private Sub sdbcGMN1_4Cod_Change()
    
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
           
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    'If bCargarComboDesde And Not oGruposMN1.EOF Then
    If Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        'Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
      
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
'            oMensajes.NoValido "Material"
            oMensajes.NoValido sIdiMaterial
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
         
            bCargarComboDesde = False
            'If bPeticiones Then
            If bPeticiones Or bCierre Then
                cmdNuevas.Enabled = True
            End If
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
       
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
'            oMensajes.NoValido "Material"
            oMensajes.NoValido sIdiMaterial
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
         
            bCargarComboDesde = False
            'If bPeticiones Then
            If bPeticiones Or bCierre Then
                cmdNuevas.Enabled = True
            End If
        End If
    
    End If
    
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
    Screen.MousePointer = vbNormal
    
End Sub

Public Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
      
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
End Sub


Private Sub sdbcProceDen_Click()
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
    End If
End Sub

Private Sub sdbcProceDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Set oProcesoSeleccionado = Nothing
        bRespetarCombo = False
        
        sdbgPet.RemoveAll
        cmdNuevas.Enabled = False
        cmdEliminar.Enabled = False
        
        bCargarComboDesde = True
        sstabPet.TabVisible(1) = False
    End If
    
End Sub

Private Sub sdbcProceDen_CloseUp()
    
    If sdbcProceDen.Value = "....." Or Trim(sdbcProceDen.Value) = "" Then
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcProceDen_DropDown()
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim oProceso As cProceso
    Dim lIdPerfil As Long
    
    sdbcProceDen.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProcesoSeleccionado = Nothing
    
    Select Case ProceSelector1.Seleccion
        
        
        Case PSSeleccion.PSPendEnviarPet
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.conasignacionvalida
        Case PSSeleccion.PSPendComObj
            udtEstDesde = TipoEstadoProceso.ConObjetivosSinNotificar
            udtEstHasta = TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
        Case PSSeleccion.PSPendNotAdj
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.conadjudicaciones
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , sdbcProceDen.Value, False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, bRMat, bRComp, bRCompResp, bREqp, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    End If
    
    For Each oProceso In oProcesos
        sdbcProceDen.AddItem oProceso.Den & Chr(m_lSeparador) & oProceso.Cod & Chr(m_lSeparador) & BooleanToSQLBinary(oProceso.Invitado)
    Next
        
    'If bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceDen_InitColumnProps()

    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProceDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceDen.Rows - 1
            bm = sdbcProceDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbgPet_HeadClick(ByVal ColIndex As Integer)
Dim udtOrd   As TipoOrdenacionPeticiones
    
    If sdbgPet.Rows = 0 Then Exit Sub
    
    Select Case sdbgPet.Columns(ColIndex).Name
    Case "PROVECOD"
        udtOrd = TipoOrdenacionPeticiones.OrdPorCodProve
    Case "PROVEDEN"
        udtOrd = TipoOrdenacionPeticiones.OrdPorDenProve
    Case "CONCOD"
        udtOrd = TipoOrdenacionPeticiones.OrdPorApe
    Case "CONDEN"
        udtOrd = TipoOrdenacionPeticiones.OrdPorNom
    Case "FEC"
        udtOrd = TipoOrdenacionPeticiones.OrdPorFecha
    Case "TIPO"
        udtOrd = TipoOrdenacionPeticiones.OrdPorTipoComu
    Case "FECOBJ"
        udtOrd = TipoOrdenacionPeticiones.OrdPorFechaDec
    Case "WEB"
        udtOrd = TipoOrdenacionPeticiones.OrdPorWeb
    Case "MAIL"
        udtOrd = TipoOrdenacionPeticiones.OrdPorEMail
    Case "IMP"
        udtOrd = TipoOrdenacionPeticiones.OrdPorImpreso
    End Select
    
    CargarHistPeticiones udtOrd

End Sub

Private Sub sdbgPet_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If sdbgPet.Row = -1 Then Exit Sub

If oProcesoSeleccionado.Estado >= conadjudicaciones Then
        Select Case sdbgPet.Columns("TIPO").Value
        Case sIdiAdj, sIdiExclusion
            cmdEliminar.Enabled = True
        Case Else
            cmdEliminar.Enabled = False
        End Select
End If
End Sub

Private Sub sdbgPet_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer

    If sdbgPet.Columns(4).Value <> "" Then
        For i = 0 To sdbgPet.Cols - 1
            sdbgPet.Columns(i).CellStyleSet "ConPeticiones"
        Next i
    End If
End Sub

''' <summary>
''' Evento que salta al haber algun cambio en el gri de publicacion
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: al hacer click en algun check del grid; Tiempo m�ximo: 0</remarks>

Private Sub sdbgPub_Change()
    Dim teserror As TipoErrorSummit
    Dim teserrorOfer As TipoErrorSummit
    Dim oIPet As IPeticiones
    Dim i As Integer
            
    Dim oAtribsEspec As CAtributosEspecificacion
    Dim oAtribEsp As CAtributoEspecificacion
    Dim sCadena As String
    Dim sCadenaTmp As String
            
    If sdbgPub.Columns(sdbgPub.col).Name = "PUBOFE" Then
        If sdbgPub.ActiveCell.Value = "-1" Then  'activa la publicaci�n
        
    
            'Tendr� que mirar si la fecha l�mite de ofertas es mayor que la actual y
            'en caso contrario ponerla a null, si es con portal
            If IsDate(oProcesoSeleccionado.FechaMinimoLimOfertas) Then
                If CDate(Format(oProcesoSeleccionado.FechaMinimoLimOfertas, "short date")) < Date Then
                    If oProcesoSeleccionado.AdminPublica Then
                        oMensajes.ImposiblePublicarCaducado
                        sdbgPub.DataChanged = False
                        Exit Sub
                    Else
                        If bModifFecLimit Then
                            If oProcesoSeleccionado.ModoSubasta And Not bModSubasta Then
                                oMensajes.ImposiblePublicarCaducado
                                sdbgPub.DataChanged = False
                                Exit Sub
                            Else
                                i = oMensajes.PreguntaEliminarFecLimite()
                                If i = vbYes Then
                                    Set oIPet = oProcesoSeleccionado
                                    teserror = oIPet.ModificarFechaLimiteDeOfertas(Null)
                                    Set oIPet = Nothing
                                    If teserrorOfer.NumError <> TESnoerror Then
                                        TratarError teserrorOfer
                                        sdbgPub.DataChanged = False
                                        Exit Sub
                                    Else
                                       basSeguridad.RegistrarAccion AccionesSummit.ACCPetOfeModFecLimit, "Anyo:" & CStr(sdbcAnyo.Value) & "GMN1:" & CStr(sdbcGMN1_4Cod.Value) & "Proce:" & Int(sdbcProceCod.Value) & "Prove:" & sdbgPub.Columns(0).Value & "Activa:" & sdbgPub.Columns(3).Value
                                       oProcesoSeleccionado.FechaMinimoLimOfertas = Null
                                       bAvisoDespub = False
                                    End If
                                Else
                                    sdbgPub.DataChanged = False
                                    Exit Sub
                                End If
                            End If
                        Else
                            oMensajes.ImposiblePublicarCaducado
                            sdbgPub.DataChanged = False
                            Exit Sub
                        End If
                    End If
                End If
            Else
                'Si se trata de un proceso en modo subasta la fecha de fin de subasta (FechaMinimoLimOfertas) es obligatoria
                If oProcesoSeleccionado.ModoSubasta And IsNull(oProcesoSeleccionado.FechaMinimoLimOfertas) Then
                    oMensajes.FechaFinSubastaObligatoria
                    sdbgPub.DataChanged = False
                    Exit Sub
                End If
            End If
            
            'Comprobar la fecha de inicio de subasta si se trata de un proceso en modo subasta
            If oProcesoSeleccionado.ModoSubasta And IsNull(oProcesoSeleccionado.FechaInicioSubasta) Then
                oMensajes.FechaInicioSubastaObligatoria
                sdbgPub.DataChanged = False
                Exit Sub
            End If
            
            'Comprobar que, si se trata de un proceso en modo subasta, el proveedor est� marcado de subasta
            If oProcesoSeleccionado.ModoSubasta And Not oProcesoSeleccionado.ComprobarProveSubasta(sdbgPub.Columns(0).Value) Then
                oMensajes.ProveedorNoInvitado
                sdbgPub.DataChanged = False
                Exit Sub
            End If
                    
            
            If Not oProcesoSeleccionado.AtributosEspValidados Then
                'Comprobacion de atributo de epecificacion obligatorios con validacion en la publicacion
                Set oAtribsEspec = frmOFEPet.oProcesoSeleccionado.ValidarAtributosEspObligatorios(TValidacionAtrib.Publicacion)
                If Not oAtribsEspec.Count = 0 Then
                    sCadena = ""
                    sCadenaTmp = ""
                    For Each oAtribEsp In oAtribsEspec
                        sCadenaTmp = oAtribEsp.Den
                        Select Case oAtribEsp.ambito
                            Case TipoAmbitoProceso.AmbProceso
                                sCadenaTmp = sCadenaTmp & " " & m_sProceso & vbCrLf
                            Case TipoAmbitoProceso.AmbGrupo
                                sCadenaTmp = sCadenaTmp & " " & m_sGrupo & vbCrLf
                            Case TipoAmbitoProceso.AmbItem
                                sCadenaTmp = sCadenaTmp & " " & m_sItem & vbCrLf
                        End Select
                        If InStr(1, sCadena, sCadenaTmp) = 0 Then
                            sCadena = sCadena & sCadenaTmp
                        End If
                    Next
                    oMensajes.AtributosEspObl sCadena, TValidacionAtrib.Publicacion
                    sdbgPub.DataChanged = False
                    Exit Sub
                End If
            
                oProcesoSeleccionado.ActualizarValidacionAtribEsp (True)
                oProcesoSeleccionado.AtributosEspValidados = True
            End If
            
            
            'Activa la publicaci�n
            sdbgPub.Columns(2).Value = Format(Date, "Short Date")
            If gParametrosGenerales.giINSTWEB = ConPortal Then
                frmEsperaPortal.Show
                DoEvents
            End If
            teserror = oIPub.IActivarPublicacion(sdbgPub.Columns(0).Value, TPublicacionOferta)
            If gParametrosGenerales.giINSTWEB = ConPortal Then
                Unload frmEsperaPortal
            End If
            
        Else  'Desactiva la publicaci�n
            sdbgPub.Columns(2).Value = ""
            If gParametrosGenerales.giINSTWEB = ConPortal Then
                frmEsperaPortal.Show
                DoEvents
            End If
            teserror = oIPub.IDesActivarPublicacion(sdbgPub.Columns(0).Value, TPublicacionOferta)
            If gParametrosGenerales.giINSTWEB = ConPortal Then
                Unload frmEsperaPortal
            End If
        End If
    End If
    
    'Publicaci�n despublicaci�n de objetivos
    If sdbgPub.Columns(sdbgPub.col).Name = "PUBOBJ" Then
        'If oProcesoSeleccionado.Estado > ConObjetivosSinNotificar Then
            If sdbgPub.ActiveCell.Value = "-1" Then  'activa la publicaci�n
            'Activa la publicaci�n
                sdbgPub.Columns("FECHAPUBOBJ").Value = Format(Date, "Short Date")
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    frmEsperaPortal.Show
                    DoEvents
                End If
                teserror = oIPub.IActivarPublicacion(sdbgPub.Columns(0).Value, ttipopublicacion.TPublicacionObjetivo)
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    Unload frmEsperaPortal
                End If
            Else
                'Desactiva la publicaci�n
                sdbgPub.Columns("FECHAPUBOBJ").Value = ""
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    frmEsperaPortal.Show
                    DoEvents
                End If
                teserror = oIPub.IDesActivarPublicacion(sdbgPub.Columns(0).Value, ttipopublicacion.TPublicacionObjetivo)
                If gParametrosGenerales.giINSTWEB = ConPortal Then
                    Unload frmEsperaPortal
                End If
            End If
'        Else
'            sdbgPub.DataChanged = False
'            Exit Sub
'        End If
    End If
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        sdbgPub.DataChanged = False
    Else
        sdbgPub.Update
        basSeguridad.RegistrarAccion AccionesSummit.ACCPetOfeModPub, "Anyo:" & CStr(sdbcAnyo.Value) & "GMN1:" & CStr(sdbcGMN1_4Cod.Value) & "Proce:" & Int(sdbcProceCod.Value) & "Prove:" & sdbgPub.Columns(0).Value & "Activa:" & sdbgPub.Columns(3).Value
    End If
    
End Sub

Private Sub sdbgPub_HeadClick(ByVal ColIndex As Integer)
Dim i As Integer

Select Case sdbgPub.Columns(ColIndex).Name
Case "PROVECOD"
    i = 1
Case "PROVEDEN"
    i = 2
Case "FECHAPUBOFE"
    i = 3
Case "PUBOFE"
    i = 4
Case "FECHAPUBOBJ"
    i = 5
Case "PUBOBJ"
    i = 6
End Select

CargarPublicaciones i
End Sub

Private Sub sdbgPub_InitColumnProps()
    
    If bModPub Then
        sdbgPub.AllowUpdate = True
    Else
        sdbgPub.AllowUpdate = False
    End If
    
End Sub

Private Sub sdbgPub_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
    
    If sdbgPub.Columns(3).Value = "-1" Or sdbgPub.Columns(3).Value = "1" Then
        For i = 0 To sdbgPub.Cols - 1
            sdbgPub.Columns(i).CellStyleSet "Activa"
        Next i
    End If
    
End Sub

Private Sub sstabPet_Click(PreviousTab As Integer)

    If PreviousTab = 0 Then
        'Ocultamos el grupo de publicaci�n de objetivos en caso de que el proceso seleccionado
        'no haya llegado al estado de publicaci�n pendiente de objetivos
        If oProcesoSeleccionado.TieneObjetivos Then
            Me.sdbgPub.Groups(2).Visible = True
        Else
            Me.sdbgPub.Groups(2).Visible = False
        End If
        Arrange
        CargarPublicaciones 1
    End If
        
End Sub
Private Sub CargarPublicaciones(iOrden As Integer)
    Dim oPub As Cpublicacion
    Dim activa As Integer
    Dim bOrdCod As Boolean
    Dim bOrdDen As Boolean
    Dim bOrdFec As Boolean
    Dim bOrdPub As Boolean
    Dim bOrdFecObj As Boolean
    Dim bOrdPubObj As Boolean
    bOrdCod = False
    bOrdDen = False
    bOrdFec = False
    bOrdPub = False
    Select Case iOrden
    Case 1
        bOrdCod = True
    Case 2
        bOrdDen = True
    Case 3
        bOrdFec = True
    Case 4
        bOrdPub = True
    Case 5
        bOrdFecObj = True
    Case 6
        bOrdPubObj = True
    End Select
    
    Set oPublicaciones = Nothing
    Set oIPub = oProcesoSeleccionado
    If bComunAsigComp Then
        Screen.MousePointer = vbHourglass
        Set oPublicaciones = oIPub.DevolverPublicaciones(gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bOrdCod, bOrdDen, bOrdFec, bOrdPub, , bOrdFecObj, bOrdPubObj)
        Screen.MousePointer = vbNormal
    Else
        If bComunAsigEqp Then
            Screen.MousePointer = vbHourglass
            Set oPublicaciones = oIPub.DevolverPublicaciones(gCodEqpUsuario, , bOrdCod, bOrdDen, bOrdFec, bOrdPub, , bOrdFecObj, bOrdPubObj)
            Screen.MousePointer = vbNormal
        Else
            Screen.MousePointer = vbHourglass
            Set oPublicaciones = oIPub.DevolverPublicaciones(, , bOrdCod, bOrdDen, bOrdFec, bOrdPub, , bOrdFecObj, bOrdPubObj)
            Screen.MousePointer = vbNormal
        End If
    End If
    
    sdbgPub.RemoveAll
        
    For Each oPub In oPublicaciones
        
        If oPub.activa Then
            activa = 1
        Else
            activa = 0
        End If
        
        If NullToStr(oPub.FechaActivacion) = "" Then
            sdbgPub.AddItem oPub.CodProve & Chr(m_lSeparador) & oPub.DenProve & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & activa & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & IIf(oPub.PublicacionObjetivoActiva, 1, 0)
        Else
            sdbgPub.AddItem oPub.CodProve & Chr(m_lSeparador) & oPub.DenProve & Chr(m_lSeparador) & oPub.FechaActivacion & Chr(m_lSeparador) & activa & Chr(m_lSeparador) & oPub.fechaPublicacionObjetivo & Chr(m_lSeparador) & IIf(oPub.PublicacionObjetivoActiva, 1, 0)
        End If
    Next
    
    

End Sub

Public Sub CargarProcesoConBusqueda()

    Set oProcesos = Nothing
    Set oProcesos = frmPROCEBuscar.oProceEncontrados
  
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    
    If oProcesos.Item(1).Estado = conasignacionvalida Then
        ProceSelector1.Seleccion = 0
    Else
        If oProcesos.Item(1).Estado > conasignacionvalida And oProcesos.Item(1).Estado < conadjudicaciones Then
            ProceSelector1.Seleccion = 1
        Else
            ProceSelector1.Seleccion = 2
        End If
    End If
    
    sdbcAnyo = oProcesos.Item(1).Anyo
    bRespetarCombo = True
    sdbcGMN1_4Cod = oProcesos.Item(1).GMN1Cod
    sdbcGMN1_4Cod_Validate False
    sdbcProceCod = oProcesos.Item(1).Cod
    sdbcProceDen = oProcesos.Item(1).Den
    sdbcProceCod_Validate False
    bRespetarCombo = False
    'Lo hace sdbcProceCod_Validate
    'ProcesoSeleccionado
    
    
    
End Sub

''' <summary>
''' Carga los recursos de la pagina
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;Load de la pagina Tiempo m�ximo 0</remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OFEPET, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value
        'Ador.MoveNext
        'lblGMN1_4.Caption = Ador(0).Value
        Ador.MoveNext
        lblCProceCod.caption = Ador(0).Value
        Ador.MoveNext
        sstabPet.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabPet.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(9).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(10).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPub.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(1).caption = Ador(0).Value
        sdbgPub.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPub.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPub.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPub.Columns(2).caption = Ador(0).Value
        sdbgPub.Columns("FechaObj").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPub.Columns(3).caption = Ador(0).Value
        sdbgPub.Columns("ActivadaObj").caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcProceCod.Columns(0).caption = Ador(0).Value
        sdbcProceDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcProceCod.Columns(1).caption = Ador(0).Value
        sdbcProceDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        cmdListadoPub.caption = Ador(0).Value
        Ador.MoveNext
        cmdNuevas.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        cmdRestaurarPub.caption = Ador(0).Value
        Ador.MoveNext
        sIdiPeticion = Ador(0).Value
        Ador.MoveNext
        sIdiObjetivo = Ador(0).Value
        Ador.MoveNext
        sIdiComunicProve = Ador(0).Value
        Ador.MoveNext
        sIdiCodigo = Ador(0).Value
        Ador.MoveNext
        sIdiProce = Ador(0).Value
        Ador.MoveNext
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sIdiExclusion = Ador(0).Value
        Ador.MoveNext
        sIdiMaterial = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaPendEnvPet = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaPendComObj = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaPendNotAdj = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaTodos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaPendEnvPet = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaPendComObj = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaPendNotAdj = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaTodos = Ador(0).Value
        Ador.MoveNext
        sIdiFecLimit = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        sIdiComunicaciones = Ador(0).Value  '55 comunicaciones a los proveedores
        Ador.MoveNext
        sIdiAvisoDespub = Ador(0).Value
        Ador.MoveNext
        chkAvisoDespub.caption = Ador(0).Value
        Ador.MoveNext
        chkPubMatProve.caption = Ador(0).Value
        Ador.MoveNext
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        m_sGrupo = Ador(0).Value
        Ador.MoveNext
        m_sItem = Ador(0).Value
        Ador.MoveNext
        sdbgPub.Groups("publicacionobj").caption = Ador(0).Value
        Ador.MoveNext
        sIdiAnulacion = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Public Function RecibidaOfertaWebDelProveedor(ByVal CodProve As String) As Boolean
    Dim IOfer As iOfertas
    Dim oOfertas As COfertas
    
    If oProcesoSeleccionado Is Nothing Then Exit Function
    
    Set IOfer = oProcesoSeleccionado
    
    Set oOfertas = IOfer.DevolverOfertasWeb(, True, , , , , CodProve)
    
    If oOfertas.Count > 0 Then
        RecibidaOfertaWebDelProveedor = True
    Else
        RecibidaOfertaWebDelProveedor = False
    End If
    
    Set IOfer = Nothing
    Set oOfertas = Nothing
End Function

Public Function FechaMinimaDeSobre() As Date
Dim dFecha As Date
Dim i As Integer

If Not oProcesoSeleccionado.Sobres Is Nothing Then
    With oProcesoSeleccionado.Sobres
        For i = 1 To .Count
            If i = 1 Then
                dFecha = .Item(i).FechaAperturaPrevista
            ElseIf .Item(i).FechaAperturaPrevista < dFecha Then
                dFecha = .Item(i).FechaAperturaPrevista
            End If
        Next
    End With
    FechaMinimaDeSobre = dFecha
End If
End Function
Private Sub CargarHistPeticiones(ByVal udtOrden As TipoOrdenacionPeticiones)
    Dim oPet As CPetOferta
    Dim oProves As CProveedores
    Dim oProve As CProveedor
    Dim sTipo As String ' literal indicador del tipo de Comunicaci�n con el Proveedor
    Dim web As Integer
    Dim mail As Integer
    Dim impr As Integer
    Dim sAntes As String
    
    sdbgPet.RemoveAll

    Set oIPet = Nothing
    Set oIPet = oProcesoSeleccionado
    Set oPets = Nothing
    
    If bComunAsigComp Then
        Set oPets = oIPet.DevolverPeticiones(udtOrden, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
        ' Cargar los proveedores pendientes de enviarles peticiones
        Set oProves = oIPet.DevolverProveedoresSinPeticiones(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , udtOrden)
    Else
        If bComunAsigEqp Then
            Set oPets = oIPet.DevolverPeticiones(udtOrden, basOptimizacion.gCodEqpUsuario)
            Set oProves = oIPet.DevolverProveedoresSinPeticiones(basOptimizacion.gCodEqpUsuario, , , udtOrden)
        Else
            Set oPets = oIPet.DevolverPeticiones(udtOrden)
            Set oProves = oIPet.DevolverProveedoresSinPeticiones(, , , udtOrden)
        End If
    End If
     
    sdbgPet.RemoveAll
        
    For Each oPet In oPets
        Select Case oPet.TipoComunicacion
            Case TNPetOferta
                sTipo = sIdiPeticion
            Case TNObjetivo
                sTipo = sIdiObjetivo
            Case TNAdjudicacion
                sTipo = sIdiAdj
            Case TNExclusion
                sTipo = sIdiExclusion
            Case TNAvisoAutomatico
                sTipo = sIdiAvisoDespub
            Case TNAnulacion
                sTipo = sIdiAnulacion
            Case Else
                sTipo = ""
        End Select
        
        If oPet.viaEMail Then
            mail = 1
        Else
            mail = 0
        End If
        
        If oPet.ViaWeb Then
            web = 1
        Else
            web = 0
        End If
        
        If oPet.ViaImp Then
            impr = 1
        Else
            impr = 0
        End If
        If oPet.AntesDeCierreParcial = True Then
            sAntes = "1"
        Else
            sAntes = "0"
        End If
        
        If IsNull(oPet.FechaReunion) Then
            sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & Format(oPet.Fecha, "short date") & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & web & Chr(m_lSeparador) & mail & Chr(m_lSeparador) & impr & Chr(m_lSeparador) & sAntes
        Else
            sdbgPet.AddItem oPet.CodProve & Chr(m_lSeparador) & oPet.DenProve & Chr(m_lSeparador) & oPet.Apellidos & Chr(m_lSeparador) & oPet.nombre & Chr(m_lSeparador) & Format(oPet.Fecha, "short date") & Chr(m_lSeparador) & oPet.Id & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & Format(CDate(oPet.FechaReunion), "short date") & " " & Format(CDate(oPet.FechaReunion), "short time") & Chr(m_lSeparador) & web & Chr(m_lSeparador) & mail & Chr(m_lSeparador) & impr & Chr(m_lSeparador) & sAntes
        End If
    Next
    
    For Each oProve In oProves
        sdbgPet.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den
    Next
End Sub


Public Sub VerDetalleResponsable()
Dim oPer As CPersona
Dim teserror As TipoErrorSummit

    'Muestra el detalle del responsable del proceso
    If oProcesoSeleccionado.responsable Is Nothing Then
        oMensajes.ProcesoSinResponsable
        Exit Sub
    End If
    If oProcesoSeleccionado.responsable.Cod = "" Then
        oMensajes.ProcesoSinResponsable
        Exit Sub
    End If
    
    Set oPer = oFSGSRaiz.Generar_CPersona
    oPer.Cod = oProcesoSeleccionado.responsable.Cod
            
    teserror = oPer.CargarTodosLosDatos
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Exit Sub
    End If
        
    Set frmESTRORGPersona.frmOrigen = Me
    frmESTRORGPersona.txtCod = oPer.Cod
    frmESTRORGPersona.txtNom = NullToStr(oPer.nombre)
    frmESTRORGPersona.txtApel = oPer.Apellidos
    frmESTRORGPersona.txtCargo = NullToStr(oPer.Cargo)
    frmESTRORGPersona.txtFax = NullToStr(oPer.Fax)
    frmESTRORGPersona.txtMail = NullToStr(oPer.mail)
    frmESTRORGPersona.txtTfno = NullToStr(oPer.Tfno)
    frmESTRORGPersona.txtTfno2 = NullToStr(oPer.Tfno2)
    frmESTRORGPersona.sdbcEquipo.Text = NullToStr(oPer.codEqp)
    If NullToStr(oPer.codEqp) <> "" Then
        frmESTRORGPersona.chkFunCompra.Value = vbChecked
    End If
    Screen.MousePointer = vbNormal
    frmESTRORGPersona.Show vbModal
    Set oPer = Nothing
    
End Sub

Public Sub SustituirResponsable()
    'Muestra el formulario para seleccionar el comprador responsable del proceso.
    If oProcesoSeleccionado Is Nothing Then Exit Sub
    
    Set frmSELPROVEResp.g_oOrigen = Me
    Set frmSELPROVEResp.g_oProcesoSeleccionado = oProcesoSeleccionado
    frmSELPROVEResp.g_udtAccion = AccionesSummit.ACCModRespOfePet
    
    Screen.MousePointer = vbNormal
    frmSELPROVEResp.Show vbModal
End Sub

Public Sub ResponsableSustituido(ByVal oPer As CPersona)
    
    If oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If oProcesoSeleccionado.responsable Is Nothing Then
        Set oProcesoSeleccionado.responsable = oFSGSRaiz.generar_CComprador
    End If
    
    oProcesoSeleccionado.responsable.Cod = oPer.Cod
    oProcesoSeleccionado.responsable.codEqp = oPer.codEqp
    oProcesoSeleccionado.responsable.nombre = NullToStr(oPer.nombre)
    oProcesoSeleccionado.responsable.Apel = oPer.Apellidos
    oProcesoSeleccionado.responsable.Cargo = NullToStr(oPer.Cargo)
    oProcesoSeleccionado.responsable.Fax = NullToStr(oPer.Fax)
    oProcesoSeleccionado.responsable.mail = NullToStr(oPer.mail)
    oProcesoSeleccionado.responsable.Tfno = NullToStr(oPer.Tfno)
    oProcesoSeleccionado.responsable.Tfno2 = NullToStr(oPer.Tfno2)
        
    Set oPer = Nothing
    
    If oProcesoSeleccionado.responsable.nombre <> "" Then
        cmdResponsable.ToolTipText = oProcesoSeleccionado.responsable.nombre & " " & oProcesoSeleccionado.responsable.Apel
    Else
        cmdResponsable.ToolTipText = oProcesoSeleccionado.responsable.Apel
    End If
    

End Sub
'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
Private Sub CargarGMN1Automaticamente()
    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
    Set m_oIMAsig = oUsuarioSummit.comprador
    Set oGMN1s = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
    If Not oGMN1s.Count = 0 Then
        bRespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4Cod.Columns(0).Text = sdbcGMN1_4Cod.Text
        bRespetarCombo = False
        GMN1Seleccionado
    End If
    Set oGMN1s = Nothing
End Sub


