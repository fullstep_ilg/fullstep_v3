VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPRESPorCon4 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   5640
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7155
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPRESPorCon4.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5640
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   7155
      TabIndex        =   0
      Top             =   5265
      Width           =   7155
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   5835
         TabIndex        =   1
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5285
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   7170
      _ExtentX        =   12647
      _ExtentY        =   9313
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstPRESPorCon4.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "frmSel"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstPRESPorCon4.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame frmSel 
         Height          =   4675
         Left            =   150
         TabIndex        =   8
         Top             =   360
         Width           =   6825
         Begin VB.Frame Frame3 
            Height          =   1270
            Left            =   240
            TabIndex        =   19
            Top             =   3210
            Width           =   6375
            Begin VB.CheckBox chkBajaLog 
               Caption         =   "Incluir bajas l�gicas"
               Height          =   225
               Left            =   360
               TabIndex        =   22
               Top             =   360
               Width           =   2000
            End
            Begin VB.ComboBox cmbOblPC 
               Height          =   315
               IntegralHeight  =   0   'False
               Left            =   2520
               Style           =   2  'Dropdown List
               TabIndex        =   21
               Top             =   775
               Width           =   855
            End
            Begin VB.Label lblOblPC 
               AutoSize        =   -1  'True
               Caption         =   "Desglosar listado a nivel:"
               Height          =   195
               Left            =   360
               TabIndex        =   20
               Top             =   825
               Width           =   1785
            End
         End
         Begin VB.Frame Frame2 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1455
            Left            =   240
            TabIndex        =   14
            Top             =   1710
            Width           =   6375
            Begin VB.CommandButton cmdSelCon4 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5850
               Picture         =   "frmLstPRESPorCon4.frx":0CEA
               Style           =   1  'Graphical
               TabIndex        =   16
               TabStop         =   0   'False
               Top             =   780
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrar 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5490
               Picture         =   "frmLstPRESPorCon4.frx":0D56
               Style           =   1  'Graphical
               TabIndex        =   15
               Top             =   780
               Width           =   315
            End
            Begin VB.Label lblNomPar 
               Caption         =   "Partida:"
               Height          =   225
               Left            =   360
               TabIndex        =   18
               Top             =   465
               Width           =   3555
            End
            Begin VB.Label lblParCon 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   420
               TabIndex        =   17
               Top             =   780
               Width           =   5010
            End
         End
         Begin VB.Frame Frame4 
            Height          =   1320
            Left            =   240
            TabIndex        =   9
            Top             =   315
            Width           =   6375
            Begin VB.CommandButton cmdSelUO 
               Height          =   285
               Left            =   5895
               Picture         =   "frmLstPRESPorCon4.frx":0DFB
               Style           =   1  'Graphical
               TabIndex        =   11
               TabStop         =   0   'False
               Top             =   720
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarUO 
               Height          =   285
               Left            =   5535
               Picture         =   "frmLstPRESPorCon4.frx":0E67
               Style           =   1  'Graphical
               TabIndex        =   10
               Top             =   720
               Width           =   315
            End
            Begin VB.Label lblFiltroUOSel 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   405
               TabIndex        =   13
               Top             =   720
               Width           =   5085
            End
            Begin VB.Label lblFiltroUO 
               Caption         =   "DFiltro por unidad organizativa:"
               Height          =   240
               Left            =   360
               TabIndex        =   12
               Top             =   315
               Width           =   2625
            End
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1800
         Left            =   -74835
         TabIndex        =   3
         Top             =   510
         Width           =   5985
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            Height          =   240
            Left            =   450
            TabIndex        =   7
            Top             =   450
            Width           =   2235
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   240
            Left            =   3225
            TabIndex        =   6
            Top             =   450
            Width           =   2205
         End
         Begin VB.OptionButton opOrdPres 
            Caption         =   "Presupuesto"
            Height          =   240
            Left            =   450
            TabIndex        =   5
            Top             =   1140
            Width           =   2220
         End
         Begin VB.OptionButton opOrdObj 
            Caption         =   "Objetivo"
            Height          =   240
            Left            =   3225
            TabIndex        =   4
            Top             =   1140
            Width           =   2310
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4815
         Top             =   15
      End
   End
End
Attribute VB_Name = "frmLstPRESPorCon4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Public iAnyoSeleccionado As Integer
Public iNivelSeleccionado As Integer
Public sParCon1 As String
Public sParCon2 As String
Public sParCon3 As String
Public sParCon4 As String

'Variables de Multilenguaje

Private sIdiTitulo As String
Private SIdiOpciones  As String
'Private sIdiAnyoInic As String
'Private sIdiPresup  As String
Private sIdiDe As String
Private sIdiGenerando  As String
Private sIdiSeleccionando As String
Private sIdiVisualizando  As String
Private srIdiSeleccion  As String
'Private srIdiPresupuestosAnyo  As String
Private srIdiPresupuesto  As String
Private srIdiObjetivo  As String
Private srIdiPag As String
Private srIdiDe As String
Private srIdiBajaLog As String

Private txtImp As String
Private txtObj As String
Private txtTitulo As String

'unidad organizativa a la que est� restringido (en caso de estarlo)
Public m_sUON1 As String
Public m_sUON2 As String
Public m_sUON3 As String
Public m_sUODescrip As String
Public m_bRuo As Boolean
Public m_sUON1Sel As String
Public m_sUON2Sel As String
Public m_sUON3Sel As String
Public m_bVerBajaLog As Boolean

Private Sub ConfigurarNombres()

lblNomPar.caption = gParametrosGenerales.gsSingPres4 & ":"
Me.caption = sIdiTitulo & " - " & gParametrosGenerales.gsplurpres4 & " " & SIdiOpciones

End Sub

Private Sub cmbOblPC_Click()
    If cmbOblPC.ListIndex > -1 Then
        iNivelSeleccionado = cmbOblPC
    Else
        iNivelSeleccionado = 0
    End If
End Sub

Private Sub cmdBorrar_Click()
    lblParCon = ""
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    CargarNivelesDeDesglose (1)
End Sub

Public Sub MostrarParConSeleccionada()
        
    Dim iNivelDesglose As Integer
    
    If Not frmSELCon4.oPres4Con4Seleccionado Is Nothing Then
        lblParCon = frmSELCon4.oPres4Con4Seleccionado.CodPRES1 & " - " & frmSELCon4.oPres4Con4Seleccionado.CodPRES2 & " - " & frmSELCon4.oPres4Con4Seleccionado.CodPRES3 & " - " & frmSELCon4.oPres4Con4Seleccionado.Cod & " - " & frmSELCon4.oPres4Con4Seleccionado.Den
        sParCon1 = frmSELCon4.oPres4Con4Seleccionado.CodPRES1
        sParCon2 = frmSELCon4.oPres4Con4Seleccionado.CodPRES2
        sParCon3 = frmSELCon4.oPres4Con4Seleccionado.CodPRES3
        sParCon4 = frmSELCon4.oPres4Con4Seleccionado.Cod
        iNivelDesglose = 4
        CargarNivelesDeDesglose (iNivelDesglose)
        cmbOblPC.Text = iNivelDesglose
        Exit Sub
    End If
    
    If Not frmSELCon4.oPres4Con3Seleccionado Is Nothing Then
        lblParCon = frmSELCon4.oPres4Con3Seleccionado.CodPRES1 & " - " & frmSELCon4.oPres4Con3Seleccionado.CodPRES2 & " - " & frmSELCon4.oPres4Con3Seleccionado.Cod & " - " & frmSELCon4.oPres4Con3Seleccionado.Den
        sParCon1 = frmSELCon4.oPres4Con3Seleccionado.CodPRES1
        sParCon2 = frmSELCon4.oPres4Con3Seleccionado.CodPRES2
        sParCon3 = frmSELCon4.oPres4Con3Seleccionado.Cod
        sParCon4 = ""
        iNivelDesglose = 3
        CargarNivelesDeDesglose (iNivelDesglose)
        cmbOblPC.Text = iNivelDesglose
        Exit Sub
    End If
    
    If Not frmSELCon4.oPres4Con2Seleccionado Is Nothing Then
        lblParCon = frmSELCon4.oPres4Con2Seleccionado.CodPRES1 & " - " & frmSELCon4.oPres4Con2Seleccionado.Cod & " - " & frmSELCon4.oPres4Con2Seleccionado.Den
        sParCon1 = frmSELCon4.oPres4Con2Seleccionado.CodPRES1
        sParCon2 = frmSELCon4.oPres4Con2Seleccionado.Cod
        sParCon3 = ""
        sParCon4 = ""
        iNivelDesglose = 2
        CargarNivelesDeDesglose (iNivelDesglose)
        cmbOblPC.Text = iNivelDesglose
        Exit Sub
    End If
    
    If Not frmSELCon4.oPres4Con1Seleccionado Is Nothing Then
        lblParCon = frmSELCon4.oPres4Con1Seleccionado.Cod & " - " & frmSELCon4.oPres4Con1Seleccionado.Den
        sParCon1 = frmSELCon4.oPres4Con1Seleccionado.Cod
        sParCon2 = ""
        sParCon3 = ""
        sParCon4 = ""
        iNivelDesglose = 1
        CargarNivelesDeDesglose (iNivelDesglose)
        cmbOblPC.Text = iNivelDesglose
        Exit Sub
    End If
    
    lblParCon = ""
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    
End Sub

Private Sub cmdBorrarUO_Click()
    lblFiltroUOSel.caption = ""
    m_sUON1Sel = ""
    m_sUON2Sel = ""
    m_sUON3Sel = ""
    m_sUODescrip = ""

End Sub

Private Sub cmdSelCon4_Click()
    frmSELCon4.sOrigen = Me.Name
    
    frmSELCon4.m_sUON1Sel = m_sUON1Sel
    frmSELCon4.m_sUON2Sel = m_sUON2Sel
    frmSELCon4.m_sUON3Sel = m_sUON3Sel
    frmSELCon4.m_bVerBajaLog = m_bVerBajaLog
    
    frmSELCon4.Show 1
End Sub

Private Sub cmdSelUO_Click()
    frmSELUO.sOrigen = Me.Name
    frmSELUO.bRUO = m_bRuo
    frmSELUO.sUON1 = m_sUON1
    frmSELUO.sUON2 = m_sUON2
    frmSELUO.sUON3 = m_sUON3
    frmSELUO.Show 1
End Sub

Private Sub Form_Load()
Dim i As Integer

    Me.Height = 6015
    Me.Width = 7245
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    txtTitulo = sIdiTitulo & " - " & gParametrosGenerales.gsplurpres4
    Timer1.Enabled = False
    opOrdCod = True
    
    For i = gParametrosGenerales.giNEPC To 1 Step -1
        cmbOblPC.AddItem i
    Next i
    
    'CargarAnyos

    'iAnyoSeleccionado = Year(Date)
    iNivelSeleccionado = 0
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""

    
    ConfigurarNombres
    
End Sub
Function CargarNivelesDeDesglose(iNivel As Integer)
    Dim i As Integer
    
    cmbOblPC.clear
    
    For i = gParametrosGenerales.giNEPC To iNivel Step -1
        If i = 0 Then Exit For
        cmbOblPC.AddItem i
    Next i

End Function

Private Sub cmdObtener_Click()
   ''' * Objetivo: Obtener un listado de Presupuestos por partidas contables
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    Dim SelectionTextRpt(1 To 2, 1 To 4) As String
    Dim oReport As CRAXDRT.Report
    Dim oCRPresPorParCon As CRPresParCon
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim pv As Preview
    Dim i As Integer
    Dim sNivel As String
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRPresPorParCon = GenerarCRPresParCon
    
    If lblFiltroUOSel.caption = "" Then
        oMensajes.SeleccioneUO
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
           oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPRESPorPres4.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Set oReport = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    '*********** FORMULA FIELDS REPORT
    
    SelectionTextRpt(2, 1) = "SEL"
    If lblParCon = "" Then
        SelectionTextRpt(1, 1) = ""
    Else
        SelectionTextRpt(1, 1) = gParametrosGenerales.gsplurpres4 & ": " & lblParCon
    End If
               
                
    SelectionTextRpt(2, 2) = "NIVEL"
    If lblParCon = "" Then
        If cmbOblPC.Text = "" Then
            SelectionTextRpt(1, 2) = CStr(gParametrosGenerales.giNEPP)
        Else
            SelectionTextRpt(1, 2) = cmbOblPC.Text
        End If
    Else
        SelectionTextRpt(1, 2) = cmbOblPC.Text
    End If
    
    sNivel = SelectionTextRpt(1, 2)
    
    SelectionTextRpt(2, 3) = "TITULO"
    SelectionTextRpt(1, 3) = sIdiTitulo
                
    SelectionTextRpt(2, 4) = "ANIO"
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next i
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & srIdiSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPres")).Text = """" & gParametrosGenerales.gsplurpres4 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtImp")).Text = """" & srIdiPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtObj")).Text = """" & srIdiObjetivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtBajaLog")).Text = """" & srIdiBajaLog & """"
    
   
    oCRPresPorParCon.ListadoParaPresCon oReport, sNivel, sParCon1, sParCon2, sParCon3, sParCon4, opOrdDen, opOrdCod, opOrdPres, opOrdObj, m_sUON1Sel, m_sUON2Sel, m_sUON3Sel, m_sUODescrip, 4, CBool(chkBajaLog.Value)
 
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sIdiSeleccionando & " - " & gParametrosGenerales.gsplurpres4
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    
    pv.caption = sIdiTitulo
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oCRPresPorParCon = Nothing
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPRES_CONCEPTO4, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
       ' Caption = Ador(0).Value '1
       ' Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        'lblAnyo.Caption = Ador(0).Value
        Ador.MoveNext
        lblNomPar.caption = Ador(0).Value '5
        Ador.MoveNext
        lblOblPC.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        opOrdPres.caption = Ador(0).Value
        Ador.MoveNext
        opOrdObj.caption = Ador(0).Value '10
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value
        Ador.MoveNext
        SIdiOpciones = Ador(0).Value
        Ador.MoveNext
        'sIdiAnyoInic = Ador(0).Value
        Ador.MoveNext
        chkBajaLog.caption = Ador(0).Value '15
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        'sIdiPresup = Ador(0).Value '15
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        'sIdiGenerando = Ador(0).Value
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
        'Filtro por UO
        lblFiltroUO.caption = Ador(0).Value
        Ador.MoveNext
        srIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        'sIdiPresupuestosAnyo = Ador(0).Value
        Ador.MoveNext
        srIdiPresupuesto = Ador(0).Value
        Ador.MoveNext
        srIdiObjetivo = Ador(0).Value
        Ador.MoveNext
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        Ador.MoveNext
        srIdiBajaLog = Ador(0).Value
        Ador.Close
    
    End If


   Set Ador = Nothing

End Sub

Public Sub MostrarUOSeleccionada()
'*****************************************************************************
'*** Descripci�n: Muestra en la ventana, en el control lblFiltroUOSel la   ***
'***              unidad organizativa seleccionada.                        ***
'***                                                                       ***
'*** Par�metros:  ---------                                                ***
'***                                                                       ***
'*** Valor que devuelve: Simplemente rellena la propiedad Caption del      ***
'***                     control lblFiltroUOSel.                           ***
'*****************************************************************************

    lblFiltroUOSel.caption = ""
    If m_sUON1Sel <> "" Then
        lblFiltroUOSel.caption = m_sUON1Sel
    End If
    If m_sUON2Sel <> "" Then
        lblFiltroUOSel.caption = lblFiltroUOSel.caption & " - " & m_sUON2Sel
    End If
    If m_sUON3Sel <> "" Then
        lblFiltroUOSel.caption = lblFiltroUOSel.caption & " - " & m_sUON3Sel
    End If
    If m_sUODescrip <> "" Then
        If lblFiltroUOSel.caption <> "" Then
            lblFiltroUOSel.caption = lblFiltroUOSel.caption & " - " & m_sUODescrip
        Else
            lblFiltroUOSel.caption = m_sUODescrip
        End If
    End If
End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If
End Sub
