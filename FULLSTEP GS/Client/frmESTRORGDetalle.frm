VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmESTRORGDetalle 
   BackColor       =   &H00808000&
   Caption         =   "Detalle "
   ClientHeight    =   7320
   ClientLeft      =   3000
   ClientTop       =   4245
   ClientWidth     =   6990
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRORGDetalle.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7320
   ScaleWidth      =   6990
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picEmpresa 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   240
      ScaleHeight     =   285
      ScaleWidth      =   1455
      TabIndex        =   69
      Top             =   960
      Width           =   1455
      Begin VB.CheckBox chkEmpresa 
         BackColor       =   &H00808000&
         Caption         =   "Empresa"
         CausesValidation=   0   'False
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   0
         MaskColor       =   &H00808000&
         TabIndex        =   6
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   1260
      End
   End
   Begin VB.Frame fraEstrOrganizativa 
      BackColor       =   &H00808000&
      Height          =   1170
      Left            =   120
      TabIndex        =   7
      Top             =   5610
      Width           =   6735
      Begin SSDataWidgets_B.SSDBCombo sdbcAlmacen 
         Height          =   285
         Left            =   2055
         TabIndex        =   13
         Top             =   795
         Width           =   3090
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5450
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         Enabled         =   0   'False
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCentroApr 
         Height          =   285
         Left            =   2055
         TabIndex        =   11
         Top             =   480
         Width           =   3090
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5450
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         Enabled         =   0   'False
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcOrgCompra 
         Height          =   285
         Left            =   2055
         TabIndex        =   9
         Top             =   165
         Width           =   3090
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5450
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         Enabled         =   0   'False
      End
      Begin VB.Label lblCentro 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2055
         TabIndex        =   56
         Top             =   480
         Width           =   3090
      End
      Begin VB.Label lblOrgCompras 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2055
         TabIndex        =   55
         Top             =   165
         Width           =   3090
      End
      Begin VB.Label lblAlmacen 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2055
         TabIndex        =   57
         Top             =   795
         Width           =   3090
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "Almac�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   780
         Width           =   660
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "Centro de aprovision.:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   480
         Width           =   1620
      End
      Begin VB.Label Label2 
         BackColor       =   &H00808000&
         Caption         =   "Organizaci�n de compra:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   195
         Width           =   1785
      End
   End
   Begin VB.Frame fraEmpresa 
      BackColor       =   &H00808000&
      Height          =   4575
      Left            =   120
      TabIndex        =   5
      Top             =   1040
      Visible         =   0   'False
      Width           =   6735
      Begin TabDlg.SSTab sstabDatosEmpresa 
         Height          =   4215
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   7435
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BackColor       =   8421376
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Datos empresa"
         TabPicture(0)   =   "frmESTRORGDetalle.frx":0CB2
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraDatosEmpresa"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Datos de facturaci�n"
         TabPicture(1)   =   "frmESTRORGDetalle.frx":0CCE
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "SSTabFacturacion"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraDatosEmpresa 
            BorderStyle     =   0  'None
            Height          =   3735
            Left            =   120
            TabIndex        =   61
            Top             =   360
            Width           =   6255
            Begin VB.TextBox txtRmte 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1515
               MaxLength       =   100
               TabIndex        =   74
               Top             =   2340
               Width           =   3615
            End
            Begin VB.TextBox txtPOB 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1515
               MaxLength       =   100
               TabIndex        =   19
               Top             =   1380
               Width           =   3615
            End
            Begin VB.TextBox txtCP 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1515
               MaxLength       =   20
               TabIndex        =   18
               Top             =   1065
               Width           =   1020
            End
            Begin VB.TextBox txtDIR 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1515
               MaxLength       =   255
               TabIndex        =   17
               Top             =   750
               Width           =   3615
            End
            Begin VB.TextBox txtDenCia 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1515
               MaxLength       =   255
               TabIndex        =   16
               Top             =   435
               Width           =   3615
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
               Height          =   285
               Left            =   1515
               TabIndex        =   20
               Top             =   1695
               Width           =   3615
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6376
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               Enabled         =   0   'False
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
               Height          =   285
               Left            =   1515
               TabIndex        =   21
               Top             =   2010
               Width           =   3615
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6376
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               Enabled         =   0   'False
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcNIF 
               Height          =   285
               Left            =   1515
               TabIndex        =   15
               Top             =   120
               Width           =   2055
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               HeadLines       =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   2117
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6165
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "ID"
               Columns(2).Name =   "ID"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   3625
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               Enabled         =   0   'False
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               Caption         =   "DRemitente:"
               Height          =   195
               Left            =   0
               TabIndex        =   75
               Top             =   2385
               Width           =   900
            End
            Begin VB.Label lblCIF 
               AutoSize        =   -1  'True
               Caption         =   "(*) CIF:"
               Height          =   195
               Left            =   0
               TabIndex        =   68
               Top             =   165
               Width           =   570
            End
            Begin VB.Label lblPai 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s:"
               Height          =   195
               Left            =   0
               TabIndex        =   67
               Top             =   1755
               UseMnemonic     =   0   'False
               Width           =   345
            End
            Begin VB.Label lblProvi 
               AutoSize        =   -1  'True
               Caption         =   "Provincia:"
               Height          =   195
               Left            =   0
               TabIndex        =   66
               Top             =   2040
               Width           =   705
            End
            Begin VB.Label lblPob 
               AutoSize        =   -1  'True
               Caption         =   "Poblaci�n:"
               Height          =   195
               Left            =   0
               TabIndex        =   65
               Top             =   1425
               Width           =   735
            End
            Begin VB.Label lblCP 
               AutoSize        =   -1  'True
               Caption         =   "C�digo postal:"
               Height          =   195
               Left            =   0
               TabIndex        =   64
               Top             =   1110
               Width           =   1035
            End
            Begin VB.Label lblDir 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n:"
               Height          =   195
               Left            =   0
               TabIndex        =   63
               Top             =   795
               Width           =   705
            End
            Begin VB.Label lblDenCia 
               AutoSize        =   -1  'True
               Caption         =   "(*) Raz�n social:"
               Height          =   195
               Left            =   0
               TabIndex        =   62
               Top             =   450
               Width           =   1200
            End
         End
         Begin VB.CommandButton Command1 
            Caption         =   "&Cerrar"
            Height          =   345
            Left            =   -71745
            TabIndex        =   59
            Top             =   3390
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton Command2 
            Caption         =   "&Seleccionar"
            Height          =   345
            Left            =   -72885
            TabIndex        =   58
            Top             =   3390
            Visible         =   0   'False
            Width           =   1005
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgProveedores 
            Height          =   2805
            Left            =   -74880
            TabIndex        =   60
            Top             =   480
            Width           =   7320
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   6
            stylesets.count =   2
            stylesets(0).Name=   "Normal"
            stylesets(0).ForeColor=   0
            stylesets(0).BackColor=   16777215
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRORGDetalle.frx":0CEA
            stylesets(1).Name=   "Selected"
            stylesets(1).ForeColor=   16777215
            stylesets(1).BackColor=   8388608
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmESTRORGDetalle.frx":0D06
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            SelectTypeCol   =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   6
            Columns(0).Width=   1773
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3969
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   1111
            Columns(2).Caption=   "CAL1"
            Columns(2).Name =   "CAL1"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   1111
            Columns(3).Caption=   "CAL2"
            Columns(3).Name =   "CAL2"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   1111
            Columns(4).Caption=   "CAL3"
            Columns(4).Name =   "CAL3"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3201
            Columns(5).Caption=   "C�d.web"
            Columns(5).Name =   "CodWeb"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            _ExtentX        =   12912
            _ExtentY        =   4948
            _StockProps     =   79
            BackColor       =   12632256
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab SSTabFacturacion 
            Height          =   3735
            Left            =   -74880
            TabIndex        =   22
            Top             =   360
            Width           =   6285
            _ExtentX        =   11086
            _ExtentY        =   6588
            _Version        =   393216
            Style           =   1
            Tabs            =   2
            TabHeight       =   520
            TabCaption(0)   =   "Direcciones "
            TabPicture(0)   =   "frmESTRORGDetalle.frx":0D22
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "fraDirecciones"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Autofacturaci�n"
            TabPicture(1)   =   "frmESTRORGDetalle.frx":0D3E
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "fraAutofacturacion"
            Tab(1).ControlCount=   1
            Begin VB.Frame fraAutofacturacion 
               BorderStyle     =   0  'None
               Height          =   2835
               Left            =   -74880
               TabIndex        =   24
               Top             =   480
               Width           =   6015
               Begin VB.TextBox txtNumMaxLineasFra 
                  Height          =   285
                  Left            =   2520
                  TabIndex        =   26
                  Top             =   0
                  Width           =   855
               End
               Begin VB.Frame fraFacturacionMensual 
                  Caption         =   "Ventana de facturaci�n mensual"
                  Height          =   1815
                  Left            =   0
                  TabIndex        =   27
                  Top             =   360
                  Width           =   5895
                  Begin VB.ComboBox cboDiaSemana 
                     Height          =   315
                     Left            =   1680
                     TabIndex        =   39
                     Top             =   700
                     Width           =   1575
                  End
                  Begin VB.ComboBox cboSemanaMes 
                     Height          =   315
                     Left            =   840
                     TabIndex        =   38
                     Top             =   700
                     Width           =   855
                  End
                  Begin VB.TextBox txtPeriodoMeses2 
                     Height          =   300
                     Left            =   4080
                     TabIndex        =   41
                     Top             =   700
                     Width           =   400
                  End
                  Begin VB.TextBox txtPeriodoMeses1 
                     Height          =   300
                     Left            =   2520
                     TabIndex        =   33
                     Top             =   300
                     Width           =   400
                  End
                  Begin VB.TextBox txtDia 
                     Height          =   300
                     Left            =   840
                     TabIndex        =   30
                     Top             =   300
                     Width           =   400
                  End
                  Begin VB.OptionButton optDia 
                     Height          =   300
                     Left            =   120
                     TabIndex        =   28
                     Top             =   300
                     Width           =   255
                  End
                  Begin VB.OptionButton optSemana 
                     Height          =   300
                     Left            =   120
                     TabIndex        =   36
                     Top             =   700
                     Width           =   255
                  End
                  Begin MSComCtl2.DTPicker dtpHoraEjecucion 
                     Height          =   375
                     Left            =   1800
                     TabIndex        =   45
                     Top             =   1155
                     Width           =   1335
                     _ExtentX        =   2355
                     _ExtentY        =   661
                     _Version        =   393216
                     Format          =   181600258
                     CurrentDate     =   41023
                  End
                  Begin MSComCtl2.UpDown updPeriodoMeses2 
                     Height          =   300
                     Left            =   4480
                     TabIndex        =   42
                     Top             =   700
                     Width           =   255
                     _ExtentX        =   450
                     _ExtentY        =   529
                     _Version        =   393216
                     Value           =   1
                     BuddyControl    =   "txtPeriodoMeses2"
                     BuddyDispid     =   196640
                     OrigLeft        =   4480
                     OrigTop         =   705
                     OrigRight       =   4735
                     OrigBottom      =   1005
                     Max             =   99
                     SyncBuddy       =   -1  'True
                     BuddyProperty   =   0
                     Enabled         =   -1  'True
                  End
                  Begin MSComCtl2.UpDown updPeriodoMeses1 
                     Height          =   300
                     Left            =   2920
                     TabIndex        =   34
                     Top             =   300
                     Width           =   255
                     _ExtentX        =   450
                     _ExtentY        =   529
                     _Version        =   393216
                     Value           =   1
                     BuddyControl    =   "txtPeriodoMeses1"
                     BuddyDispid     =   196641
                     OrigLeft        =   2930
                     OrigTop         =   300
                     OrigRight       =   3185
                     OrigBottom      =   600
                     Max             =   99
                     SyncBuddy       =   -1  'True
                     BuddyProperty   =   0
                     Enabled         =   -1  'True
                  End
                  Begin MSComCtl2.UpDown updDia 
                     Height          =   300
                     Left            =   1240
                     TabIndex        =   31
                     Top             =   300
                     Width           =   255
                     _ExtentX        =   450
                     _ExtentY        =   529
                     _Version        =   393216
                     Value           =   1
                     BuddyControl    =   "txtDia"
                     BuddyDispid     =   196642
                     OrigLeft        =   1240
                     OrigTop         =   300
                     OrigRight       =   1495
                     OrigBottom      =   600
                     Max             =   31
                     SyncBuddy       =   -1  'True
                     BuddyProperty   =   0
                     Enabled         =   -1  'True
                  End
                  Begin VB.Label lblGMT 
                     Caption         =   "GMT+01:00"
                     Height          =   210
                     Left            =   3240
                     TabIndex        =   73
                     Top             =   1200
                     Width           =   1455
                  End
                  Begin VB.Label lblHoraEjecucion 
                     Caption         =   "Hora de ejecuci�n:"
                     Height          =   210
                     Left            =   120
                     TabIndex        =   44
                     Top             =   1200
                     Width           =   1695
                  End
                  Begin VB.Label lblMeses2 
                     Caption         =   "mes(es)"
                     Height          =   210
                     Left            =   4920
                     TabIndex        =   43
                     Top             =   720
                     Width           =   735
                  End
                  Begin VB.Label lblDecada2 
                     Caption         =   "de cada"
                     Height          =   210
                     Left            =   3280
                     TabIndex        =   40
                     Top             =   750
                     Width           =   845
                  End
                  Begin VB.Label lblEl 
                     Caption         =   "El"
                     Height          =   210
                     Left            =   435
                     TabIndex        =   37
                     Top             =   750
                     Width           =   375
                  End
                  Begin VB.Label lblMeses 
                     Caption         =   "mes(es)"
                     Height          =   210
                     Left            =   3380
                     TabIndex        =   35
                     Top             =   345
                     Width           =   855
                  End
                  Begin VB.Label lblDecada 
                     Caption         =   "de cada"
                     Height          =   210
                     Left            =   1695
                     TabIndex        =   32
                     Top             =   345
                     Width           =   840
                  End
                  Begin VB.Label lblDia 
                     Caption         =   "D�a"
                     Height          =   210
                     Left            =   435
                     TabIndex        =   29
                     Top             =   345
                     Width           =   495
                  End
               End
               Begin VB.Frame fraTolerancia 
                  Caption         =   "Tolerancia"
                  Height          =   615
                  Left            =   0
                  TabIndex        =   46
                  Top             =   2160
                  Width           =   5895
                  Begin VB.TextBox txtToleranciaPorcentaje 
                     Height          =   300
                     Left            =   3360
                     TabIndex        =   50
                     Top             =   240
                     Width           =   1095
                  End
                  Begin VB.TextBox txtToleranciaImporte 
                     Alignment       =   1  'Right Justify
                     Height          =   300
                     Left            =   840
                     TabIndex        =   48
                     Top             =   240
                     Width           =   1575
                  End
                  Begin VB.Label lblToleranciaPorcentaje 
                     Caption         =   "%"
                     Height          =   300
                     Left            =   4455
                     TabIndex        =   51
                     Top             =   285
                     Width           =   375
                  End
                  Begin VB.Label lblMoneda 
                     Caption         =   "MON"
                     Height          =   300
                     Left            =   2430
                     TabIndex        =   49
                     Top             =   285
                     Width           =   705
                  End
                  Begin VB.Label lblToleranciaImporte 
                     Caption         =   "Importe:"
                     Height          =   300
                     Left            =   120
                     TabIndex        =   47
                     Top             =   290
                     Width           =   855
                  End
               End
               Begin VB.Label lblNumMaxLineasFra 
                  Caption         =   "N� m�ximo de l�neas de factura:"
                  Height          =   255
                  Left            =   0
                  TabIndex        =   25
                  Top             =   0
                  Width           =   2535
               End
            End
            Begin VB.Frame fraDirecciones 
               BorderStyle     =   0  'None
               Height          =   3255
               Left            =   80
               TabIndex        =   70
               Top             =   360
               Width           =   6015
               Begin SSDataWidgets_B.SSDBDropDown sdbddDirecciones 
                  Height          =   1260
                  Left            =   360
                  TabIndex        =   71
                  Top             =   480
                  Width           =   3990
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmESTRORGDetalle.frx":0D5A
                  BevelColorFace  =   12632256
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   9
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "ID"
                  Columns(0).Name =   "ID"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   4419
                  Columns(1).Caption=   "Direcci�n"
                  Columns(1).Name =   "DIR"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2302
                  Columns(2).Caption=   "C�digo Postal"
                  Columns(2).Name =   "CP"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   3200
                  Columns(3).Caption=   "Poblaci�n"
                  Columns(3).Name =   "POB"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   3200
                  Columns(4).Visible=   0   'False
                  Columns(4).Caption=   "PAI"
                  Columns(4).Name =   "PAI"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(5).Width=   3200
                  Columns(5).Visible=   0   'False
                  Columns(5).Caption=   "PROVI"
                  Columns(5).Name =   "PROVI"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(6).Width=   3200
                  Columns(6).Caption=   "Povincia"
                  Columns(6).Name =   "PROVIDEN"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(7).Width=   3200
                  Columns(7).Caption=   "Pa�s"
                  Columns(7).Name =   "PAIDEN"
                  Columns(7).DataField=   "Column 7"
                  Columns(7).DataType=   8
                  Columns(7).FieldLen=   256
                  Columns(8).Width=   3200
                  Columns(8).Visible=   0   'False
                  Columns(8).Caption=   "FECACT"
                  Columns(8).Name =   "FECACT"
                  Columns(8).DataField=   "Column 8"
                  Columns(8).DataType=   8
                  Columns(8).FieldLen=   256
                  _ExtentX        =   7038
                  _ExtentY        =   2222
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddPaises 
                  Height          =   1260
                  Left            =   720
                  TabIndex        =   72
                  Top             =   840
                  Width           =   3990
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmESTRORGDetalle.frx":0D76
                  BevelColorFace  =   12632256
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1773
                  Columns(0).Caption=   "COD"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   4419
                  Columns(1).Caption=   "DEN"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   7038
                  _ExtentY        =   2222
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBGrid sdbgDirecciones 
                  Height          =   3090
                  Left            =   90
                  TabIndex        =   23
                  Top             =   120
                  Width           =   5865
                  _Version        =   196617
                  DataMode        =   2
                  Col.Count       =   9
                  stylesets.count =   2
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmESTRORGDetalle.frx":0D92
                  stylesets(1).Name=   "NormalInterno"
                  stylesets(1).BackColor=   16777152
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmESTRORGDetalle.frx":0DAE
                  stylesets(1).AlignmentPicture=   1
                  AllowAddNew     =   -1  'True
                  AllowDelete     =   -1  'True
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeRow   =   1
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   9
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "ID"
                  Columns(0).Name =   "ID"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   4419
                  Columns(1).Caption=   "Direcci�n"
                  Columns(1).Name =   "DIR"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2117
                  Columns(2).Caption=   "C�digo Postal"
                  Columns(2).Name =   "CP"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   3200
                  Columns(3).Caption=   "Poblaci�n"
                  Columns(3).Name =   "POB"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   3200
                  Columns(4).Visible=   0   'False
                  Columns(4).Caption=   "PROVI"
                  Columns(4).Name =   "PROVI"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(5).Width=   3200
                  Columns(5).Visible=   0   'False
                  Columns(5).Caption=   "PAI"
                  Columns(5).Name =   "PAI"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(6).Width=   3200
                  Columns(6).Caption=   "Pa�s"
                  Columns(6).Name =   "PAIDEN"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(7).Width=   3200
                  Columns(7).Caption=   "Provincia"
                  Columns(7).Name =   "PROVIDEN"
                  Columns(7).DataField=   "Column 7"
                  Columns(7).DataType=   8
                  Columns(7).FieldLen=   256
                  Columns(8).Width=   3200
                  Columns(8).Visible=   0   'False
                  Columns(8).Caption=   "FECACT"
                  Columns(8).Name =   "FECACT"
                  Columns(8).DataField=   "Column 8"
                  Columns(8).DataType=   8
                  Columns(8).FieldLen=   256
                  _ExtentX        =   10345
                  _ExtentY        =   5450
                  _StockProps     =   79
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
            End
         End
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1200
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   52
      Top             =   6840
      Width           =   3825
      Begin VB.CommandButton cmdCancelar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "Cancelar"
         CausesValidation=   0   'False
         Height          =   315
         Left            =   1980
         TabIndex        =   54
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   840
         TabIndex        =   53
         Top             =   75
         Width           =   1005
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   1335
      ScaleHeight     =   945
      ScaleWidth      =   4200
      TabIndex        =   0
      Top             =   0
      Width           =   4200
      Begin VB.TextBox txtDen 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   30
         MaxLength       =   100
         TabIndex        =   4
         Top             =   480
         Width           =   4020
      End
      Begin VB.TextBox txtCod 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   30
         TabIndex        =   2
         Top             =   60
         Width           =   1230
      End
   End
   Begin VB.Label lblCod 
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   1
      Top             =   45
      Width           =   1185
   End
   Begin VB.Label lblDen 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominaci�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   3
      Top             =   495
      Width           =   1215
   End
End
Attribute VB_Name = "frmESTRORGDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type Dimensiones
    Ancho As Long
    Largo As Long
End Type

Private Const AlturaFormCaption = 500
Private Const Separacion = 150

Private m_bPaiRespetarCombo As Boolean
Private m_bPaiCargarComboDesde As Boolean
Private m_bNIFCargarComboDesde As Boolean
Private m_bProviRespetarCombo As Boolean
Private m_bProviCargarComboDesde As Boolean
Private m_bNIFRespetarCombo As Boolean

Private m_oPaises As CPaises
Private m_oPaisSeleccionado As CPais
Private m_oEmpresas As CEmpresas
Private moEmpresaSeleccionada As CEmpresa

Private moOrgCompras As COrganizacionesCompras
Private moCentros As CCentros
Private moAlmacenes As CAlmacenes
Private moOrgCompra As COrganizacionCompras
Private moCentro As CCentro
Private moAlmacen As CAlmacen
Public m_oUnidadesOrgN1 As CUnidadOrgNivel1
Public m_oUnidadesOrgN2 As CUnidadOrgNivel2
Public m_oUnidadesOrgN3 As CUnidadOrgNivel3
Public m_oUnidadesOrgN4 As CUnidadOrgNivel4
Public mCodUON1 As String
Public mCodUON2 As String
Public mCodUON3 As String
Public mCodUON4 As String

Public blnEmpresa As Boolean
Public blnNoExisteOrgCompras As Boolean
Public blnNoExisteCentroApr As Boolean
Public blnNoExisteAlmacen As Boolean

Private sIdiCod As String
Private sIdiDen As String
Private sIdiNif As String
Private sIdiRazonSocial As String
Private m_bCargaForm As Boolean 'Nos indica la primera carga del formularios
Private m_sDireccionObligatoria As String
Private m_sDireccionRepetida As String
Private m_lIDDirBorrada As Long
Private sDiasSemana(1 To 7) As String
Private sSemanasMes(1 To 4) As String

''' <summary>
''' Eliminar una empresa has deschequeado el check empresa.
''' </summary>
''' <returns>True no ha habido errores. False ha habido errores</returns>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo: 0,1</remarks>
Private Function GestionarBajaEmpresa() As Boolean
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    Dim teserror As TipoErrorSummit
    Dim sUnidades As String
    Dim sUON1 As String
    Dim sUON2 As String
    Dim sUON3 As String
    Dim sUON4 As String
    Dim dtFecAct As Variant
    Dim sCod As String
    Dim nodx As node
     
    
    GestionarBajaEmpresa = True
    
    Select Case frmESTRORG.Accion
        Case ACCUON1Anya, ACCUON1Mod
            sUON1 = Me.txtCod.Text
            sUON2 = ""
            sUON3 = ""
            sUON4 = ""
            sCod = sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sUON1))
        Case ACCUON2Anya, ACCUON2MOD
            sUON1 = frmESTRORG.g_vUON1
            sUON2 = Me.txtCod.Text
            sUON3 = ""
            sUON4 = ""
            sCod = sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sUON1))
            sCod = sCod & sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sUON2))
        
        Case ACCUON3Anya, ACCUON3MOD
            sUON1 = frmESTRORG.g_vUON1
            sUON2 = frmESTRORG.g_vUON2
            sUON3 = Me.txtCod.Text
            sUON4 = ""
            sCod = sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sUON1))
            sCod = sCod & sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sUON2))
            sCod = sCod & sUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sUON3))
        
        Case ACCUON4Anya, ACCUON4MOD
            sUON1 = frmESTRORG.g_vUON1
            sUON2 = frmESTRORG.g_vUON2
            sUON3 = frmESTRORG.g_vUON3
            sUON4 = Me.txtCod.Text
            sCod = sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sUON1))
            sCod = sCod & sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sUON2))
            sCod = sCod & sUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sUON3))
            sCod = sCod & sUON4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(sUON4))
            
    End Select
    
    
    If Not moEmpresaSeleccionada Is Nothing Then
        If Not moEmpresaSeleccionada.cargarUons(sUON1, sUON2, sUON3, sUON4) Then
            If oMensajes.PreguntaEliminarEmpresa = vbYes Then
                If moEmpresaSeleccionada.erp Then
                    oMensajes.ImposibleEliminarEmpresaERP
                    GestionarBajaEmpresa = False
                    Exit Function
                End If
                moEmpresaSeleccionada.Usuario = basOptimizacion.gvarCodUsuario
                teserror = moEmpresaSeleccionada.EliminarDeBaseDatos(sUON1, sUON2, sUON3, dtFecAct, sUON4)
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    GestionarBajaEmpresa = False
                    Exit Function
                Else
                    Select Case frmESTRORG.Accion
                        Case ACCUON1Anya, ACCUON1Mod
                            frmESTRORG.oUON1Seleccionada.FECACT = dtFecAct
                            Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("UON1" & sCod)
                            nodx.Image = "UON1"
                        Case ACCUON2Anya, ACCUON2MOD
                            frmESTRORG.oUON2Seleccionada.FECACT = dtFecAct
                            Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("UON2" & sCod)
                            nodx.Image = "UON2"
                        Case ACCUON3Anya, ACCUON3MOD
                            frmESTRORG.oUON3Seleccionada.FECACT = dtFecAct
                            Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("UON3" & sCod)
                            nodx.Image = "UON3"
                        Case ACCUON4Anya, ACCUON4MOD
                            frmESTRORG.oUON4Seleccionada.FECACT = dtFecAct
                            Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("UON4" & sCod)
                            nodx.Image = "UON4"
                    End Select
                End If
            End If
        End If
    End If

End Function

Private Sub chkEmpresa_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oDim As Dimensiones
    
    If Button = vbLeftButton Then
        If Me.chkEmpresa.Value = vbChecked Then
            blnEmpresa = True
            m_bNIFRespetarCombo = True
            If Not moEmpresaSeleccionada Is Nothing Then
                Me.sdbcNIF.Text = moEmpresaSeleccionada.NIF
                Me.sdbcNIF.Columns(2).Value = moEmpresaSeleccionada.Id
                Me.sdbcNIF.Columns(0).Value = moEmpresaSeleccionada.NIF
                Me.sdbcNIF.Columns(1).Value = moEmpresaSeleccionada.Den
                EmpresaSeleccionada
            End If
            m_bNIFRespetarCombo = False
        Else
            blnEmpresa = False
        
            LimpiarCampos
            m_bNIFRespetarCombo = True
            
            Me.sdbcNIF.Text = ""
            Me.sdbcNIF.Columns(2).Value = Null
            Me.sdbcNIF.Columns(0).Value = Null
            Me.sdbcNIF.Columns(1).Value = Null
            
            m_bNIFRespetarCombo = False
        End If
    
        PintarFormulario
        oDim = CalcularDimensionesFormulario
                
        If Me.WindowState = vbMaximized Then
            ResizeFormulario
        Else
            Me.Height = oDim.Largo
            Me.Width = oDim.Ancho
        End If
    End If
End Sub

''' Revisado por: blp. Fecha: 16/07/2012
''' <summary>
''' Grabara los cambios realizados en la estructura organizativa
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim oIBaseDatos As IBaseDatos
    Dim oDepAsoc As CDepAsociado
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    Dim oUON4 As CUnidadOrgNivel4
    Dim teserror As TipoErrorSummit
    Dim oMonedas As CMonedas
    Dim sMoneda As String
    Dim iRes As Integer
    Dim bControlImportes As Boolean
    Dim bConControl As Boolean
    
    
    Screen.MousePointer = vbHourglass
    teserror.NumError = TESnoerror
    bControlImportes = False
    
    If Me.chkEmpresa.Value = vbChecked Then
        blnEmpresa = True
        If Me.sdbcNIF.Text = "" Then
            oMensajes.DatoObligatorio 147
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        If Me.txtDenCia.Text = "" Then
            oMensajes.DatoObligatorio sIdiRazonSocial
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        If txtRmte.Text <> "" Then
            If Not ComprobarEmail(txtRmte.Text) Then
                 oMensajes.NoValida Replace(Label4.caption, ":", "")
                 Screen.MousePointer = vbNormal
                 Exit Sub
            End If
        End If
    End If
    
    If moEmpresaSeleccionada Is Nothing Then
        Set moEmpresaSeleccionada = oFSGSRaiz.Generar_CEmpresa
    Else
        If Me.chkEmpresa.Value = vbUnchecked Or Me.sdbcNIF.Text <> moEmpresaSeleccionada.NIF Then
            GestionarBajaEmpresa
        End If
    End If
        
    moEmpresaSeleccionada.Usuario = basOptimizacion.gvarCodUsuario
        
    If Me.chkEmpresa.Value = vbChecked Then
        
        With moEmpresaSeleccionada
            .NIF = Me.sdbcNIF.Text
            .Den = Me.txtDenCia.Text
            .cP = Me.txtCP.Text
            .Poblacion = Me.txtPOB.Text
            .Direccion = Me.txtDIR.Text
            If Me.sdbcPaiDen.Text <> "" Then
                .CodPais = Me.sdbcPaiDen.Columns(1).Value
            End If
            If Me.sdbcProviDen.Text <> "" Then
                .CodProvi = Me.sdbcProviDen.Columns(1).Value
            End If
            
            'Direcciones
            sdbgDirecciones.Update
            If Not ObtenerDireccionesEnvio Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            .Remitente = txtRmte.Text
            'Facturaci�n
            .FactMaxLineas = StrToDbl0(txtNumMaxLineasFra.Text)
            .FactFecha1DiadelMes = StrToDbl0(txtDia.Text)
            .FactFecha1PeriodoMes = StrToDbl0(txtPeriodoMeses1.Text)
            .FactFecha2SemanadelMes = cboSemanaMes.ListIndex + 1 'Los valores de este listindex van de 0 a 3. Guardamos del 1 al 4-> 0:Nada seleccionado, 1:primera semana, dos:segunda, etc.
            .FactFecha2DiadelaSemana = cboDiaSemana.ListIndex + 1 'Los valores de este listindex van de 0 a 6. Guardamos del 1 al 7-> 0:Nada seleccionado, 1:Lunes, 2:Martes, ..., 7: Domingo
            .FactFecha2PeriodoMes = StrToDbl0(txtPeriodoMeses2.Text)
            .FactFechaHora = dtpHoraEjecucion.Value
            .FactToleranciaImporte = StrToDbl0(txtToleranciaImporte.Text)
            .FactToleranciaPorcentaje = StrToDbl0(txtToleranciaPorcentaje.Text)
        End With
    Else
        Set moEmpresaSeleccionada = Nothing
    End If
    
    Select Case frmESTRORG.Accion
        
        Case ACCUON1Anya
                                    
                '********* Validar datos *********
                If Not ValidarDatos(True) Then Exit Sub
    
                Set oUON1 = oFSGSRaiz.generar_CUnidadOrgNivel1
                oUON1.Cod = Trim(txtCod)
                oUON1.Den = Trim(txtDen)
                If Me.chkEmpresa.Value = vbChecked Then
                    Set oUON1.Empresa = moEmpresaSeleccionada
                Else
                    Set oUON1.Empresa = Nothing
                End If
                oUON1.Usuario = basOptimizacion.gvarCodUsuario
                
                If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
                    Set oUON1.Centro = oFSGSRaiz.Generar_CCentro
                    Set oUON1.Almacen = oFSGSRaiz.Generar_CAlmacen
                    Set oUON1.OrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                    
                    If sdbcAlmacen.Text <> "" Then
                        oUON1.Almacen.Id = CInt(sdbcAlmacen.Columns(1).Text)
                        oUON1.Almacen.Cod = sdbcAlmacen.Columns(2).Text
                        oUON1.Almacen.Den = sdbcAlmacen.Columns(0).Text
                    End If
                    If sdbcCentroApr.Text <> "" Then
                        oUON1.Centro.Cod = sdbcCentroApr.Columns(1).Text
                        oUON1.Centro.Den = sdbcCentroApr.Columns(0).Text
                    End If
                    If sdbcOrgCompra.Text <> "" Then
                        oUON1.OrgCompra.Cod = sdbcOrgCompra.Columns(1).Text
                        oUON1.OrgCompra.Den = sdbcOrgCompra.Columns(0).Text
                    End If
                End If
                            
                Set oIBaseDatos = oUON1
                teserror = oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError = TESnoerror Then
                    AnyadirUOAEstructura 1
                    RegistrarAccion ACCUON1Anya, "Cod:" & Trim(txtCod)
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    If Me.Visible Then txtCod.SetFocus
                    Exit Sub
                End If
                
                Set oUON1 = Nothing
                Set oIBaseDatos = Nothing
                
        Case ACCUON2Anya
                
                '********* Validar datos *********
                If Not ValidarDatos(True) Then Exit Sub
                
                Set oUON2 = oFSGSRaiz.generar_CUnidadOrgNivel2
                oUON2.CodUnidadOrgNivel1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                oUON2.Cod = Trim(txtCod)
                oUON2.Den = Trim(txtDen)
              
                If Me.chkEmpresa.Value = vbChecked Then
                    Set oUON2.Empresa = moEmpresaSeleccionada
                Else
                    Set oUON2.Empresa = Nothing
                End If
                oUON2.Usuario = basOptimizacion.gvarCodUsuario
            
                If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
                    Set oUON2.Centro = oFSGSRaiz.Generar_CCentro
                    Set oUON2.Almacen = oFSGSRaiz.Generar_CAlmacen
                    Set oUON2.OrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                    If sdbcAlmacen.Text <> "" Then
                        oUON2.Almacen.Id = CInt(sdbcAlmacen.Columns(1).Text)
                        oUON2.Almacen.Cod = sdbcAlmacen.Columns(2).Text
                        oUON2.Almacen.Den = sdbcAlmacen.Columns(0).Text
                    End If
                    If sdbcCentroApr.Text <> "" Then
                        oUON2.Centro.Cod = sdbcCentroApr.Columns(1).Text
                        oUON2.Centro.Den = sdbcCentroApr.Columns(0).Text
                    End If
                    If sdbcOrgCompra.Text <> "" Then
                        oUON2.OrgCompra.Cod = sdbcOrgCompra.Columns(1).Text
                        oUON2.OrgCompra.Den = sdbcOrgCompra.Columns(0).Text
                    End If
                End If
                            
                Set oIBaseDatos = oUON2
                teserror = oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError = TESnoerror Then
                    AnyadirUOAEstructura 2
                    RegistrarAccion ACCUON2Anya, "Cod:" & Trim(txtCod)
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    If Me.Visible Then txtCod.SetFocus
                    Exit Sub
                End If
                Set oUON2 = Nothing
                Set oIBaseDatos = Nothing
                             
                             
        Case ACCUON3Anya
                
                '********* Validar datos *********
                If Not ValidarDatos(True) Then Exit Sub
    '            '********* Validar datos *********
                
                Set oUON3 = oFSGSRaiz.generar_CUnidadOrgNivel3
                oUON3.CodUnidadOrgNivel2 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                oUON3.CodUnidadOrgNivel1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent)
                oUON3.Cod = Trim(txtCod)
                oUON3.Den = Trim(txtDen)
                If Me.chkEmpresa.Value = vbChecked Then
                    Set oUON3.Empresa = moEmpresaSeleccionada
                Else
                    Set oUON3.Empresa = Nothing
                End If
                oUON3.Usuario = basOptimizacion.gvarCodUsuario
                
                If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
                    Set oUON3.Centro = oFSGSRaiz.Generar_CCentro
                    Set oUON3.Almacen = oFSGSRaiz.Generar_CAlmacen
                    Set oUON3.OrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                    If sdbcAlmacen.Text <> "" Then
                        oUON3.Almacen.Id = CInt(sdbcAlmacen.Columns(1).Text)
                        oUON3.Almacen.Cod = sdbcAlmacen.Columns(2).Text
                        oUON3.Almacen.Den = sdbcAlmacen.Columns(0).Text
                    End If
                    If sdbcCentroApr.Text <> "" Then
                        oUON3.Centro.Cod = sdbcCentroApr.Columns(1).Text
                        oUON3.Centro.Den = sdbcCentroApr.Columns(0).Text
                    End If
                    If sdbcOrgCompra.Text <> "" Then
                        oUON3.OrgCompra.Cod = sdbcOrgCompra.Columns(1).Text
                        oUON3.OrgCompra.Den = sdbcOrgCompra.Columns(0).Text
                    End If
                    
                End If
                
                Set oIBaseDatos = oUON3
                teserror = oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError = TESnoerror Then
                    AnyadirUOAEstructura 3
                    RegistrarAccion ACCUON3Anya, "Cod:" & Trim(txtCod)
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    If Me.Visible Then txtCod.SetFocus
                    Exit Sub
                End If
                Set oUON3 = Nothing
                Set oIBaseDatos = Nothing
                
        Case ACCUON4Anya
                                    
                '********* Validar datos *********
                If Not ValidarDatos(True) Then Exit Sub
                
                Set oUON4 = oFSGSRaiz.generar_CUnidadOrgNivel4
                
                oUON4.CodUnidadOrgNivel3 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                oUON4.CodUnidadOrgNivel2 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent)
                oUON4.CodUnidadOrgNivel1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent.Parent)
                
                oUON4.Cod = Trim(txtCod.Text)
                oUON4.Den = Trim(txtDen.Text)
                
                If Me.chkEmpresa.Value = vbChecked Then
                    Set oUON4.Empresa = moEmpresaSeleccionada
                Else
                    Set oUON4.Empresa = Nothing
                End If
                oUON4.Usuario = basOptimizacion.gvarCodUsuario
                
                Set oIBaseDatos = oUON4
                teserror = oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError = TESnoerror Then
                    AnyadirUOAEstructura 4
                    RegistrarAccion ACCUON4Anya, "Cod:" & Trim(txtCod.Text)
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    If Me.Visible Then txtCod.SetFocus
                    Exit Sub
                End If
                
                Set oUON1 = Nothing
                Set oIBaseDatos = Nothing
                
        Case ACCUON1Mod
                
                '********* Validar datos *********
                If Not ValidarDatos(False) Then Exit Sub
                            
                frmESTRORG.oUON1Seleccionada.Den = Trim(txtDen)
                
                If Me.chkEmpresa.Value = vbChecked Then
                    Set frmESTRORG.oUON1Seleccionada.Empresa = moEmpresaSeleccionada
                Else
                    Set frmESTRORG.oUON1Seleccionada.Empresa = Nothing
                End If
                frmESTRORG.oUON1Seleccionada.Usuario = basOptimizacion.gvarCodUsuario
                
                If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
                    Set frmESTRORG.oUON1Seleccionada.Centro = oFSGSRaiz.Generar_CCentro
                    Set frmESTRORG.oUON1Seleccionada.OrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                    Set frmESTRORG.oUON1Seleccionada.Almacen = oFSGSRaiz.Generar_CAlmacen
                    If sdbcAlmacen.Text <> "" Then
                        If sdbcAlmacen.Columns(1).Text <> "" Then
                            frmESTRORG.oUON1Seleccionada.Almacen.Id = CInt(sdbcAlmacen.Columns(1).Text)
                        End If
                        frmESTRORG.oUON1Seleccionada.Almacen.Cod = sdbcAlmacen.Columns(2).Text
                        frmESTRORG.oUON1Seleccionada.Almacen.Den = sdbcAlmacen.Columns(0).Text
                    End If
                    If sdbcCentroApr.Text <> "" Then
                        frmESTRORG.oUON1Seleccionada.Centro.Cod = sdbcCentroApr.Columns(1).Text
                        frmESTRORG.oUON1Seleccionada.Centro.Den = sdbcCentroApr.Columns(0).Text
                    End If
                    If sdbcOrgCompra.Text <> "" Then
                        frmESTRORG.oUON1Seleccionada.OrgCompra.Cod = sdbcOrgCompra.Columns(1).Text
                        frmESTRORG.oUON1Seleccionada.OrgCompra.Den = sdbcOrgCompra.Columns(0).Text
                    End If
                End If
    
                teserror = frmESTRORG.oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    ModificarUOEnEstructura
                    RegistrarAccion ACCUON1Mod, "Cod:" & Trim(txtCod)
                    frmESTRORG.Accion = ACCUOCon
                    Unload Me
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
    
                
        Case ACCUON2MOD
                
                '********* Validar datos *********
                If Not ValidarDatos(False) Then Exit Sub
                
                frmESTRORG.oUON2Seleccionada.Den = Trim(txtDen)
                
                If Me.chkEmpresa.Value = vbChecked Then
                    Set frmESTRORG.oUON2Seleccionada.Empresa = moEmpresaSeleccionada
                Else
                    Set frmESTRORG.oUON2Seleccionada.Empresa = Nothing
                End If
                frmESTRORG.oUON2Seleccionada.Usuario = basOptimizacion.gvarCodUsuario
                
                If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
                    Set frmESTRORG.oUON2Seleccionada.Centro = oFSGSRaiz.Generar_CCentro
                    Set frmESTRORG.oUON2Seleccionada.OrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                    Set frmESTRORG.oUON2Seleccionada.Almacen = oFSGSRaiz.Generar_CAlmacen
                    If sdbcAlmacen.Text <> "" Then
                        If sdbcAlmacen.Columns(1).Text <> "" Then
                            frmESTRORG.oUON2Seleccionada.Almacen.Id = CInt(sdbcAlmacen.Columns(1).Text)
                        End If
                        frmESTRORG.oUON2Seleccionada.Almacen.Cod = sdbcAlmacen.Columns(2).Text
                        frmESTRORG.oUON2Seleccionada.Almacen.Den = sdbcAlmacen.Columns(0).Text
                    End If
                    If sdbcCentroApr.Text <> "" Then
                        frmESTRORG.oUON2Seleccionada.Centro.Cod = sdbcCentroApr.Columns(1).Text
                        frmESTRORG.oUON2Seleccionada.Centro.Den = sdbcCentroApr.Columns(0).Text
                    End If
                    If sdbcOrgCompra.Text <> "" Then
                        frmESTRORG.oUON2Seleccionada.OrgCompra.Cod = sdbcOrgCompra.Columns(1).Text
                        frmESTRORG.oUON2Seleccionada.OrgCompra.Den = sdbcOrgCompra.Columns(0).Text
                    End If
                End If
    
                teserror = frmESTRORG.oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    ModificarUOEnEstructura
                    RegistrarAccion ACCUON2MOD, "Cod:" & Trim(txtCod)
                    frmESTRORG.Accion = ACCUOCon
                    Unload Me
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
    
        
        Case ACCUON3MOD
                
                '********* Validar datos *********
                If Not ValidarDatos(False) Then Exit Sub
                
                frmESTRORG.oUON3Seleccionada.Den = Trim(txtDen)
                
                If Me.chkEmpresa.Value = vbChecked Then
                    Set frmESTRORG.oUON3Seleccionada.Empresa = moEmpresaSeleccionada
                Else
                    Set frmESTRORG.oUON3Seleccionada.Empresa = Nothing
                End If
                frmESTRORG.oUON3Seleccionada.Usuario = basOptimizacion.gvarCodUsuario
                
                If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
                    Set frmESTRORG.oUON3Seleccionada.Centro = oFSGSRaiz.Generar_CCentro
                    Set frmESTRORG.oUON3Seleccionada.OrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                    Set frmESTRORG.oUON3Seleccionada.Almacen = oFSGSRaiz.Generar_CAlmacen
                    If sdbcAlmacen.Text <> "" Then
                        If sdbcAlmacen.Columns(1).Text <> "" Then
                            frmESTRORG.oUON3Seleccionada.Almacen.Id = CInt(sdbcAlmacen.Columns(1).Text)
                        End If
                        frmESTRORG.oUON3Seleccionada.Almacen.Cod = sdbcAlmacen.Columns(2).Text
                        frmESTRORG.oUON3Seleccionada.Almacen.Den = sdbcAlmacen.Columns(0).Text
                    End If
                    If sdbcCentroApr.Text <> "" Then
                        frmESTRORG.oUON3Seleccionada.Centro.Cod = sdbcCentroApr.Columns(1).Text
                        frmESTRORG.oUON3Seleccionada.Centro.Den = sdbcCentroApr.Columns(0).Text
                    End If
                    If sdbcOrgCompra.Text <> "" Then
                        frmESTRORG.oUON3Seleccionada.OrgCompra.Cod = sdbcOrgCompra.Columns(1).Text
                        frmESTRORG.oUON3Seleccionada.OrgCompra.Den = sdbcOrgCompra.Columns(0).Text
                    End If
                End If
    
                teserror = frmESTRORG.oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    ModificarUOEnEstructura
                    RegistrarAccion ACCUON3MOD, "Cod:" & Trim(txtCod)
                    frmESTRORG.Accion = ACCUOCon
                    Unload Me
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
                
        Case ACCUON4MOD
        
                '********* Validar datos *********
                If Not ValidarDatos(False) Then Exit Sub
                
                frmESTRORG.oUON4Seleccionada.Den = Trim(txtDen.Text)
                If Me.chkEmpresa.Value = vbChecked Then
                    Set frmESTRORG.oUON4Seleccionada.Empresa = moEmpresaSeleccionada
                Else
                    Set frmESTRORG.oUON4Seleccionada.Empresa = Nothing
                End If
                frmESTRORG.oUON4Seleccionada.Usuario = basOptimizacion.gvarCodUsuario
                
                teserror = frmESTRORG.oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    ModificarUOEnEstructura
                    RegistrarAccion ACCUON4MOD, "Cod:" & Trim(txtCod.Text)
                    frmESTRORG.Accion = ACCUOCon
                    Unload Me
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
         
        Case ACCDepMod
                
                If Trim(txtDen) = "" Then
                    Screen.MousePointer = vbNormal
                    oMensajes.NoValido sIdiDen
                    If Me.Visible Then txtDen.SetFocus
                    Exit Sub
                End If
                
                frmESTRORG.oDepAsocSeleccionado.Den = Trim(txtDen)
                teserror = frmESTRORG.oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    ModificarDepEnEstructura
                    RegistrarAccion ACCDepMod, "Cod:" & Trim(txtCod)
                    Unload Me
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
                   
    End Select
          
    frmESTRORG.Accion = ACCUOCon
    
    frmESTRORG.tvwestrorg_NodeClick frmESTRORG.tvwestrorg.selectedItem
    
    Screen.MousePointer = vbNormal
    
    Unload Me
End Sub

''' <summary>Crea una colecci�n de objetos de tipo CFacDirEnvio con las dir. del grid de direcciones</summary>
''' <returns>Estructura de tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde cmdAceptar_Click</remarks>

Private Function ObtenerDireccionesEnvio() As Boolean
    Dim i As Integer
    Dim vbm As Variant
    Dim lID As Long

    ObtenerDireccionesEnvio = False

    If gParametrosGenerales.gbPedidosDirFac Then
        Set moEmpresaSeleccionada.DireccionesEnvio = Nothing
        Set moEmpresaSeleccionada.DireccionesEnvio = oFSGSRaiz.Generar_CFacDirEnvios
        
        With sdbgDirecciones
            For i = 0 To .Rows - 1
                vbm = .AddItemBookmark(i)
    
                If .Columns("DIR").CellValue(vbm) <> vbNullString Then
                    If .Columns("ID").CellValue(vbm) <> vbNullString Then
                        lID = CLng(.Columns("ID").CellValue(vbm))
                    Else
                        lID = -i    'Para que no se repita la key en la col. de dir. de env�os
                    End If
                    If moEmpresaSeleccionada.DireccionesEnvio.Item(CStr(lID)) Is Nothing Then
                        moEmpresaSeleccionada.DireccionesEnvio.Add lID, .Columns("DIR").CellValue(vbm), _
                                .Columns("CP").CellValue(vbm), .Columns("POB").CellValue(vbm), .Columns("PROVI").CellValue(vbm), _
                                .Columns("PROVIDEN").CellValue(vbm), .Columns("PAI").CellValue(vbm), _
                                .Columns("PAIDEN").CellValue(vbm), , .Columns("FECACT").CellValue(vbm)
                    Else
                        oMensajes.MensajeOKOnly m_sDireccionRepetida
                        Exit Function
                    End If
                Else
                    oMensajes.MensajeOKOnly m_sDireccionObligatoria
                    Exit Function
                End If
            Next
        End With
    End If
    
    ObtenerDireccionesEnvio = True
    
End Function

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

''' <summary>Calcula las dimensiones del formulario</summary>
''' <returns>Objeto Dimensiones</returns>
''' <remarks>Llamada desde Form_Activate</remarks>
''' <revision>LTG 07/07/2010</revision>

Private Function CalcularDimensionesFormulario() As Dimensiones
    Dim oDim As Dimensiones
    
    If fraEmpresa.Visible Or fraEstrOrganizativa.Visible Then
        oDim.Ancho = fraEmpresa.Left + fraEmpresa.Width + 1.5 * Separacion
    Else
        oDim.Ancho = picDatos.Left + picDatos.Width + 1.5 * Separacion
    End If
    
    oDim.Largo = AlturaFormCaption + picDatos.Height + _
                IIf(picEmpresa.Visible, picEmpresa.Top + picEmpresa.Height - picDatos.Height, 0) + _
                IIf(fraEmpresa.Visible, fraEmpresa.Height - (picEmpresa.Height / 2), 0) + _
                IIf(fraEstrOrganizativa.Visible, fraEstrOrganizativa.Height, 0) + _
                IIf(picEdit.Visible, picEdit.Height, Separacion)
    
    CalcularDimensionesFormulario = oDim
End Function

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>Visualiza y ubica los controles del formulario en cada caso</summary>
''' <remarks>Llamada desde Form_Activate y chkEmpresa_MouseUp. Maximo 0,1 seg.</remarks>

Private Sub PintarFormulario()
    Select Case frmESTRORG.Accion
        Case ACCDepMod
            PintarACCDepMod
        Case ACCUON1Eli, ACCUON2Eli, ACCUON3Eli, ACCDepEli
            PintarEli
        Case ACCUON1Det, ACCUON2DET, ACCUON3DET, ACCDepDet
            PintarDet
        Case ACCUON1Anya
            PintarACCUON1Anya
        Case ACCUON2Anya
            PintarACCUON2Anya
        Case ACCUON3Anya
            PintarACCUON3Anya
        Case ACCUON4Anya
            PintarACCUON4Anya
        Case ACCUON4MOD
            PintarACCUON4Mod
        Case ACCUON4DET
            PintarACCUON4DET
        Case ACCUON1Mod, ACCUON2MOD, ACCUON3MOD
            PintarACCUONMod
        Case ACCDepEli
            picDatos.Enabled = False
    End Select
    
    If Me.chkEmpresa.Value = vbChecked Then
        Me.sdbcNIF.Enabled = True
        Me.txtCP.Enabled = True
        Me.txtDenCia.Enabled = True
        Me.txtDIR.Enabled = True
        Me.txtPOB.Enabled = True
        Me.sdbcPaiDen.Enabled = True
        Me.sdbcProviDen.Enabled = True
        txtRmte.Enabled = True
        fraEmpresa.Visible = True
    Else
        Me.sdbcNIF.Enabled = False
        Me.txtCP.Enabled = False
        Me.txtDenCia.Enabled = False
        Me.txtDIR.Enabled = False
        Me.txtPOB.Enabled = False
        Me.sdbcPaiDen.Enabled = False
        Me.sdbcProviDen.Enabled = False
        txtRmte.Enabled = False
        fraEmpresa.Visible = False
    End If
    
    If (gParametrosGenerales.gbPedidosDirFac Or (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM)) Then
        sstabDatosEmpresa.TabVisible(1) = True
        SSTabFacturacion.TabVisible(0) = gParametrosGenerales.gbPedidosDirFac 'Direcciones
        SSTabFacturacion.TabVisible(1) = (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM) 'Autofacturaci�n
    Else
        sstabDatosEmpresa.TabVisible(1) = False
    End If
        
    m_bCargaForm = False
End Sub

''' <summary>Visualiza y ubica los controles del formulario para la acci�n ACCDepMod</summary>
''' <remarks>Llamada desde</remarks>

Private Sub PintarACCDepMod()
    fraEstrOrganizativa.Visible = False
    picEmpresa.Visible = False
    txtCod.Enabled = False
End Sub

''' <summary>Visualiza y ubica los controles del formulario para las acciones ACCUON1Eli, ACCUON2Eli, ACCUON3Eli, ACCDepEli</summary>
''' <remarks>Llamada desde</remarks>

Private Sub PintarEli()
    picDatos.Enabled = False
    fraEmpresa.Enabled = False
End Sub

''' <summary>Visualiza y ubica los controles del formulario para las acciones ACCUON1Det, ACCUON2DET, ACCUON3DET, ACCDepDet</summary>
''' <remarks>Llamada desde</remarks>
''' <revision>25/05/2012</revision>

Private Sub PintarDet()
    picDatos.Enabled = False
    picEdit.Visible = False
    picEmpresa.Enabled = False
    fraDatosEmpresa.Enabled = False
    fraAutofacturacion.Enabled = False
    BloquearGridDirecciones
    fraEstrOrganizativa.Visible = False
    If frmESTRORG.Accion = ACCDepDet Then picEmpresa.Visible = False
    
    If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
        CargarOrganizacionesCompras
        sdbcOrgCompra.Visible = False
        sdbcCentroApr.Visible = False
        sdbcAlmacen.Visible = False
        
        If sdbcOrgCompra.Value <> "" Or lblOrgCompras.caption <> "" Then
            fraEstrOrganizativa.Visible = True
            If chkEmpresa.Value <> vbChecked Then
                fraEstrOrganizativa.Top = 1000
                fraEmpresa.Visible = False
                lblOrgCompras.Visible = True
                If sdbcCentroApr.Value <> "" Or lblCentro.caption <> "" Then
                    lblCentro.Visible = True
                    If sdbcAlmacen.Value <> "" Or lblAlmacen.caption <> "" Then
                        lblAlmacen.Visible = True
                        fraEstrOrganizativa.Height = 1200
                    Else
                        lblAlmacen.Visible = False
                        Label3.Visible = False
                        fraEstrOrganizativa.Height = 880
                    End If
                Else
                    'Solo se muestra el campo de organizacion de compras
                    fraEstrOrganizativa.Height = 520
                End If
                
            Else
                lblOrgCompras.Visible = True
                If sdbcCentroApr.Value <> "" Or lblCentro.caption <> "" Then
                    lblCentro.Visible = True
                    If sdbcAlmacen.Value <> "" Or lblAlmacen.caption <> "" Then
                        lblAlmacen.Visible = True
                        fraEstrOrganizativa.Height = 1200
                    Else
                        lblAlmacen.Visible = False
                        Label3.Visible = False
                        fraEstrOrganizativa.Height = 880
                    End If
                Else
                    'Solo se muestra el campo de organizacion de compras
                    fraEstrOrganizativa.Height = 520
                End If
            End If
            'Me.Height = Me.Height + fraEstrOrganizativa.Height
        Else
            fraEstrOrganizativa.Visible = False
            If chkEmpresa.Value = vbChecked Then
                fraEstrOrganizativa.Visible = False
            End If
        End If
    End If
End Sub

''' <summary>Visualiza y ubica los controles del formulario para la acci�n ACCUON1Anya</summary>
''' <remarks>Llamada desde</remarks>

Private Sub PintarACCUON1Anya()
    If Not (gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras) Then
        lblOrgCompras.Visible = False
        sdbcOrgCompra.Visible = False
        lblCentro.Visible = False
        sdbcCentroApr.Visible = False
        lblAlmacen.Visible = False
        sdbcAlmacen.Visible = False
        fraEstrOrganizativa.Visible = False
    End If
    
    If Not moEmpresaSeleccionada Is Nothing Then
        Me.fraEmpresa.Visible = True
    End If
    
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodUON1
    If Me.Visible Then txtCod.SetFocus
    fraEstrOrganizativa.Enabled = True
    sdbcOrgCompra.Enabled = True
    blnEmpresa = False
    If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
        CargarOrganizacionesCompras
    End If
End Sub

''' <summary>Visualiza y ubica los controles del formulario para la acci�n ACCUON2Anya</summary>
''' <remarks>Llamada desde</remarks>

Private Sub PintarACCUON2Anya()
    If Not (gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras) Then
        lblOrgCompras.Visible = False
        sdbcOrgCompra.Visible = False
        lblCentro.Visible = False
        sdbcCentroApr.Visible = False
        lblAlmacen.Visible = False
        sdbcAlmacen.Visible = False
        fraEstrOrganizativa.Visible = False
    End If
        
    If Not moEmpresaSeleccionada Is Nothing Then
        Me.fraEmpresa.Visible = True
    End If
    
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodUON2
    fraEstrOrganizativa.Enabled = True
    sdbcOrgCompra.Enabled = True
    blnEmpresa = False
    If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
        CargarOrganizacionesCompras
    End If
End Sub

''' <summary>Visualiza y ubica los controles del formulario para la acci�n ACCUON3Anya</summary>
''' <remarks>Llamada desde</remarks>

Private Sub PintarACCUON3Anya()
    If Not (gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras) Then
        lblOrgCompras.Visible = False
        sdbcOrgCompra.Visible = False
        lblCentro.Visible = False
        sdbcCentroApr.Visible = False
        lblAlmacen.Visible = False
        sdbcAlmacen.Visible = False
        fraEstrOrganizativa.Visible = False
    End If
        
    If Not moEmpresaSeleccionada Is Nothing Then
        Me.fraEmpresa.Visible = True
    End If
    
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodUON3
    If Me.Visible Then txtCod.SetFocus
    fraEstrOrganizativa.Enabled = True
    sdbcOrgCompra.Enabled = True
    blnEmpresa = False
    If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
        CargarOrganizacionesCompras
    End If
End Sub

''' <summary>Visualiza y ubica los controles del formulario para la acci�n ACCUON4Anya</summary>
''' <remarks>Llamada desde</remarks>

Private Sub PintarACCUON4Anya()
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodUON4
    If Me.Visible Then txtCod.SetFocus
    fraEmpresa.Visible = True
        
    lblOrgCompras.Visible = False
    sdbcOrgCompra.Visible = False
    lblCentro.Visible = False
    sdbcCentroApr.Visible = False
    lblAlmacen.Visible = False
    sdbcAlmacen.Visible = False
    fraEstrOrganizativa.Visible = False
End Sub

''' <summary>Visualiza y ubica los controles del formulario para la acci�n ACCUON4Mod</summary>
''' <remarks>Llamada desde</remarks>

Private Sub PintarACCUON4Mod()
    txtCod.Enabled = False
    If txtDen.Enabled And Me.Visible Then txtDen.SetFocus
    fraEmpresa.Visible = True
        
    lblOrgCompras.Visible = False
    sdbcOrgCompra.Visible = False
    lblCentro.Visible = False
    sdbcCentroApr.Visible = False
    lblAlmacen.Visible = False
    sdbcAlmacen.Visible = False
    fraEstrOrganizativa.Visible = False
End Sub

''' Revisado por: blp. Fecha: 05/11/2012
''' <summary>Visualiza y ubica los controles del formulario para la acci�n ACCUON4DET</summary>
''' <remarks>Llamada desde frmESTRORGDetalle. M�x. 0,3 seg.</remarks>
Private Sub PintarACCUON4DET()
    picDatos.Enabled = False
    picEdit.Visible = False
    picEmpresa.Enabled = False
    fraDatosEmpresa.Enabled = False
    fraAutofacturacion.Enabled = False
    BloquearGridDirecciones
    fraEmpresa.Visible = True
    fraEstrOrganizativa.Visible = False
    
    If Not moEmpresaSeleccionada Is Nothing Then
        lblOrgCompras.Visible = False
        sdbcOrgCompra.Visible = False
        lblCentro.Visible = False
        sdbcCentroApr.Visible = False
        lblAlmacen.Visible = False
        sdbcAlmacen.Visible = False
    End If
End Sub

''' <summary>Visualiza y ubica los controles del formulario para las acciones ACCUON1Mod, ACCUON2MOD, ACCUON3MOD</summary>
''' <remarks>Llamada desde</remarks>

Private Sub PintarACCUONMod()
    If Not (gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras) Then
        lblOrgCompras.Visible = False
        sdbcOrgCompra.Visible = False
        lblCentro.Visible = False
        sdbcCentroApr.Visible = False
        lblAlmacen.Visible = False
        sdbcAlmacen.Visible = False
        fraEstrOrganizativa.Visible = False
    End If
        
    If Not moEmpresaSeleccionada Is Nothing Then
        Me.fraEmpresa.Visible = True
    End If
    
    txtCod.Enabled = False
    If gParametrosGenerales.gbIntegracion And gParametrosGenerales.gbUsarOrgCompras Then
        CargarOrganizacionesCompras
    End If
End Sub

''' <summary>Bloquea la edici�n del grid de direcciones</summary>
''' <remarks>Llamada desde PintarDet y PintarACCUON4DET</remarks>

Private Sub BloquearGridDirecciones()
    With sdbgDirecciones
        .AllowAddNew = False
        .AllowDelete = False
        .AllowUpdate = False
    End With
End Sub

Private Sub CargarOrganizacionesCompras()
    Dim rsOrgCompras As ADODB.Recordset
    Dim tsError As TipoErrorSummit
        
    Me.fraEstrOrganizativa.Visible = True
    Set moOrgCompras = oFSGSRaiz.Generar_COrganizacionesCompras
    
    If Not blnNoExisteAlmacen And Not blnNoExisteCentroApr And Not blnNoExisteOrgCompras Then
        Select Case frmESTRORG.Accion
            Case ACCUON1Mod, ACCUON1Det
                If chkEmpresa.Value <> vbChecked Then
                     fraEstrOrganizativa.Enabled = True
                     sdbcOrgCompra.Enabled = True
                     blnEmpresa = False
                Else
                     blnEmpresa = True
                End If
                tsError = moOrgCompras.CargarOrgCompraEmpresa(rsOrgCompras, blnEmpresa, mCodUON1)
                If Not rsOrgCompras.EOF Then
                    If rsOrgCompras.Fields("U1COMPRAS").Value <> "" Then
                        Set moOrgCompra = Nothing
                        Set moOrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                        sdbcOrgCompra.AddItem moOrgCompra.DevolverOrganizacionCompras(rsOrgCompras.Fields("U1COMPRAS").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1COMPRAS").Value
                        sdbcOrgCompra.Text = sdbcOrgCompra.Columns(0).Text
                        lblOrgCompras.Visible = False
                        lblOrgCompras.caption = sdbcOrgCompra.Text
                        sdbcOrgCompra.Visible = True
                        sdbcOrgCompra.Enabled = True
                        sdbcCentroApr.Enabled = True
                        If rsOrgCompras.Fields("U1CENTRO").Value <> "" Then
                            Set moCentro = Nothing
                            Set moCentro = oFSGSRaiz.Generar_CCentro
                            sdbcCentroApr.AddItem moCentro.DevolverCentro(rsOrgCompras.Fields("U1CENTRO").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1CENTRO").Value
                            sdbcCentroApr.Text = sdbcCentroApr.Columns(0).Text
                            lblCentro.Visible = False
                            lblCentro.Visible = True
                            lblCentro.caption = sdbcCentroApr.Text
                            sdbcCentroApr.Visible = True
                            If rsOrgCompras.Fields("U1ALMACEN").Value <> "" Then
                                Set moAlmacen = Nothing
                                Set moAlmacen = oFSGSRaiz.Generar_CAlmacen
                                    sdbcAlmacen.AddItem moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U1ALMACEN").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1ALMACEN").Value
                                    sdbcAlmacen.Text = sdbcAlmacen.Columns(0).Text
                                    Label3.Visible = True
                                    lblAlmacen.Visible = False
                                    lblAlmacen.caption = sdbcAlmacen.Text
                                    sdbcAlmacen.Visible = True
                            Else
                                lblAlmacen.Visible = False
                                If frmESTRORG.Accion = ACCUON1Mod Then
                                    Label3.Visible = True
                                Else
                                    Me.Label3.Visible = False
                                End If
                                sdbcAlmacen.Visible = True
                                sdbcAlmacen.Enabled = True
                            End If
                        Else
                            If frmESTRORG.Accion = ACCUON1Mod Then
                                Label1.Visible = True
                                sdbcCentroApr.Visible = True
                            Else
                                Label1.Visible = False
                                lblCentro.Visible = False
                            End If
                            
                            
                            sdbcCentroApr.Enabled = True
                        End If
                    Else
                        lblOrgCompras.Visible = False
                        sdbcOrgCompra.Visible = True
                        sdbcOrgCompra.Enabled = True
                    End If
                End If
            Case ACCUON2Anya
                    tsError = moOrgCompras.CargarOrgCompraEmpresa(rsOrgCompras, blnEmpresa, mCodUON1)
                    If Not rsOrgCompras Is Nothing Then
                        If rsOrgCompras.Fields("DENORG").Value <> "" Then
                            lblOrgCompras.caption = NullToStr(rsOrgCompras.Fields("DENORG").Value)
                            lblOrgCompras.Visible = True
                            sdbcOrgCompra.Visible = False
                            sdbcOrgCompra.AddItem rsOrgCompras.Fields("DENORG").Value & Chr(m_lSeparador) & rsOrgCompras.Fields("ORGCOMPRAS").Value
                            sdbcCentroApr.Enabled = True
                            If rsOrgCompras.Fields("CENTROS").Value <> "" Then
                                lblCentro.caption = NullToStr(rsOrgCompras.Fields("DENCENTRO").Value)
                                lblCentro.Visible = True
                                sdbcCentroApr.Visible = False
                                sdbcCentroApr.AddItem rsOrgCompras.Fields("DENCENTRO").Value & Chr(m_lSeparador) & rsOrgCompras.Fields("CENTROS").Value
                                sdbcAlmacen.Enabled = True
                                If rsOrgCompras.Fields("ALMACEN").Value <> "" Then
                                    lblAlmacen.caption = NullToStr(rsOrgCompras.Fields("DENALMACEN").Value)
                                    lblAlmacen.Visible = True
                                    sdbcAlmacen.Visible = False
                                    sdbcAlmacen.AddItem rsOrgCompras.Fields("DENALMACEN").Value & Chr(m_lSeparador) & rsOrgCompras.Fields("ALMACEN").Value
                                Else
                                    sdbcAlmacen.Visible = True
                                    sdbcAlmacen.Enabled = True
                                    lblAlmacen.Visible = False
                                End If
                            Else
                                sdbcCentroApr.Visible = True
                                sdbcCentroApr.Enabled = True
                                lblCentro.Visible = False
                            End If
                        Else
                            lblOrgCompras.Visible = False
                            sdbcOrgCompra.Visible = True
                            sdbcOrgCompra.Enabled = True
                        End If
                    End If
            Case ACCUON2MOD, ACCUON2DET
                'Si estamos modificando una uon2 y la uon1 madre tiene una organizacion de compra asignada
                If chkEmpresa.Value <> vbChecked Then
                     fraEstrOrganizativa.Enabled = True
                     sdbcOrgCompra.Enabled = True
                     blnEmpresa = False
                Else
                     blnEmpresa = True
                End If
                tsError = moOrgCompras.CargarOrgCompraEmpresa(rsOrgCompras, blnEmpresa, mCodUON1, mCodUON2)
                If Not rsOrgCompras.EOF Then
                    If rsOrgCompras.Fields("U1COMPRAS").Value <> "" Or rsOrgCompras.Fields("U2COMPRAS").Value <> "" Then
                        Set moOrgCompra = Nothing
                        Set moOrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                        If rsOrgCompras.Fields("U1COMPRAS").Value <> "" Then
                            sdbcOrgCompra.AddItem moOrgCompra.DevolverOrganizacionCompras(rsOrgCompras.Fields("U1COMPRAS").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1COMPRAS").Value
                            sdbcOrgCompra.Text = sdbcOrgCompra.Columns(0).Text
                            lblOrgCompras.caption = sdbcOrgCompra.Text
                            lblOrgCompras.Visible = True
                            sdbcOrgCompra.Visible = False
                        Else
                            sdbcOrgCompra.AddItem moOrgCompra.DevolverOrganizacionCompras(rsOrgCompras.Fields("U2COMPRAS").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U2COMPRAS").Value
                            sdbcOrgCompra.Text = sdbcOrgCompra.Columns(0).Text
                            lblOrgCompras.caption = sdbcOrgCompra.Text
                            lblOrgCompras.Visible = False
                            sdbcOrgCompra.Enabled = True
                        End If
                        
                        If rsOrgCompras.Fields("U1CENTRO").Value <> "" Or rsOrgCompras.Fields("U2CENTRO").Value <> "" Then
                            Set moCentro = Nothing
                            Set moCentro = oFSGSRaiz.Generar_CCentro
                            If rsOrgCompras.Fields("U1CENTRO").Value <> "" Then
                                sdbcCentroApr.AddItem moCentro.DevolverCentro(rsOrgCompras.Fields("U1CENTRO").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1CENTRO").Value
                                sdbcCentroApr.Text = sdbcCentroApr.Columns(0).Text
                                lblCentro.caption = sdbcCentroApr.Text
                                lblCentro.Visible = True
                                sdbcCentroApr.Visible = False
                            Else
                                sdbcCentroApr.AddItem moCentro.DevolverCentro(rsOrgCompras.Fields("U2CENTRO").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U2CENTRO").Value
                                sdbcCentroApr.Text = sdbcCentroApr.Columns(0).Text
                                lblCentro.caption = sdbcCentroApr.Text
                                lblCentro.Visible = False
                                sdbcCentroApr.Visible = True
                                sdbcCentroApr.Enabled = True
                            End If
                            
                            If rsOrgCompras.Fields("U1ALMACEN").Value <> 0 Or rsOrgCompras.Fields("U2ALMACEN").Value <> 0 Then
                                Set moAlmacen = Nothing
                                Set moAlmacen = oFSGSRaiz.Generar_CAlmacen
                                If rsOrgCompras.Fields("U1ALMACEN").Value <> 0 Then
                                    lblAlmacen.Visible = True
                                    sdbcAlmacen.Visible = False
                                                                        
                                    sdbcAlmacen.AddItem moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U1ALMACEN").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1ALMACEN").Value
                                    sdbcAlmacen.Text = sdbcAlmacen.Columns(0).Text
                                    lblAlmacen.caption = sdbcAlmacen.Text
                                Else
                                    sdbcAlmacen.AddItem moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U2ALMACEN").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U2ALMACEN").Value
                                    sdbcAlmacen.Text = sdbcAlmacen.Columns(0).Text
                                    lblAlmacen.caption = sdbcAlmacen.Text
                                    lblAlmacen.Visible = False
                                    sdbcAlmacen.Visible = True
                                    sdbcAlmacen.Enabled = True
                                End If
                            Else
                                lblAlmacen.Visible = False
                                If frmESTRORG.Accion <> ACCUON2DET Then
                                    sdbcAlmacen.Visible = True
                                Else
                                    sdbcAlmacen.Visible = False
                                End If
                                If frmESTRORG.Accion = ACCUON2MOD Then
                                    Label3.Visible = True
                                Else
                                    Me.Label3.Visible = False
                                End If
                            End If
                        Else
'                            If frmESTRORG.Accion = ACCUON2DET Then
'                                lblOrgCompras.caption = ""
'                                sdbcOrgCompra.Text = ""
'                            End If
                            lblCentro.Visible = False
                            If frmESTRORG.Accion = ACCUON2MOD Then
                                Label1.Visible = True
                                sdbcCentroApr.Visible = True
                            Else
                                Label1.Visible = False
                                lblCentro.Visible = False
                            End If
                            sdbcCentroApr.Enabled = True
                        End If
                    Else
                        lblOrgCompras.Visible = False
                        sdbcOrgCompra.Visible = True
                        sdbcOrgCompra.Enabled = True
                    End If
                End If
           
            Case ACCUON3Anya
                tsError = moOrgCompras.CargarOrgCompraEmpresa(rsOrgCompras, blnEmpresa, mCodUON1, mCodUON2)
                If Not rsOrgCompras.EOF Then
                    If rsOrgCompras.Fields("U2COMPRAS").Value <> "" Then
                        Set moOrgCompra = Nothing
                        Set moOrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                        
                        sdbcOrgCompra.AddItem moOrgCompra.DevolverOrganizacionCompras(rsOrgCompras.Fields("U2COMPRAS").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U2COMPRAS").Value
                        sdbcOrgCompra.Text = sdbcOrgCompra.Columns(0).Text
                        lblOrgCompras.caption = sdbcOrgCompra.Text
                        lblOrgCompras.Visible = True
                        sdbcOrgCompra.Visible = False
                      
                        If rsOrgCompras.Fields("CENTROS").Value <> "" Then
                            lblCentro.caption = NullToStr(rsOrgCompras.Fields("DENCENTRO").Value)
                            lblCentro.Visible = True
                            sdbcCentroApr.Visible = False
                            sdbcCentroApr.AddItem rsOrgCompras.Fields("DENCENTRO").Value & Chr(m_lSeparador) & rsOrgCompras.Fields("CENTROS").Value
                            sdbcAlmacen.Enabled = True
                            If rsOrgCompras.Fields("ALMACEN").Value <> 0 Then
                                    lblAlmacen.caption = NullToStr(rsOrgCompras.Fields("DENALMACEN").Value)
                                    lblAlmacen.Visible = True
                                    sdbcAlmacen.Visible = False
                                    sdbcAlmacen.AddItem rsOrgCompras.Fields("DENALMACEN").Value & Chr(m_lSeparador) & rsOrgCompras.Fields("ALMACEN").Value
                            Else
                                sdbcAlmacen.Visible = True
                                sdbcAlmacen.Enabled = True
                                lblAlmacen.Visible = False
                            End If
                        Else
                            sdbcCentroApr.Visible = True
                            sdbcCentroApr.Enabled = True
                            lblCentro.Visible = False
                        End If
                    Else
                        sdbcOrgCompra.Visible = True
                        sdbcOrgCompra.Enabled = True
                        lblOrgCompras.Visible = False
                    End If
                End If
            Case ACCUON3MOD, ACCUON3DET
                'Si estamos modificando una uon2 y la uon1 madre tiene una organizacion de compra asignada
                If chkEmpresa.Value <> vbChecked Then
                     fraEstrOrganizativa.Enabled = True
                     sdbcOrgCompra.Enabled = True
                     blnEmpresa = False
                Else
                     blnEmpresa = True
                End If
                tsError = moOrgCompras.CargarOrgCompraEmpresa(rsOrgCompras, blnEmpresa, mCodUON1, mCodUON2, mCodUON3)
                If Not rsOrgCompras.EOF Then
                    If rsOrgCompras.Fields("U1COMPRAS").Value <> "" Or rsOrgCompras.Fields("U2COMPRAS").Value <> "" Or rsOrgCompras.Fields("U3COMPRAS").Value <> "" Then
                        Set moOrgCompra = Nothing
                        Set moOrgCompra = oFSGSRaiz.Generar_COrganizacionCompras
                        If rsOrgCompras.Fields("U1COMPRAS").Value <> "" Then
                            sdbcOrgCompra.AddItem moOrgCompra.DevolverOrganizacionCompras(rsOrgCompras.Fields("U1COMPRAS").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1COMPRAS").Value
                            sdbcOrgCompra.Text = sdbcOrgCompra.Columns(0).Text
                            lblOrgCompras.caption = sdbcOrgCompra.Text
                            lblOrgCompras.Visible = True
                            sdbcOrgCompra.Visible = False
                           
                        Else
                            If rsOrgCompras.Fields("U2COMPRAS").Value <> "" Then
                                sdbcOrgCompra.AddItem moOrgCompra.DevolverOrganizacionCompras(rsOrgCompras.Fields("U2COMPRAS").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U2COMPRAS").Value
                                sdbcOrgCompra.Text = sdbcOrgCompra.Columns(0).Text
                                lblOrgCompras.caption = sdbcOrgCompra.Text
                                lblOrgCompras.Visible = True
                                sdbcOrgCompra.Visible = False
                            Else
                                sdbcOrgCompra.AddItem moOrgCompra.DevolverOrganizacionCompras(rsOrgCompras.Fields("U3COMPRAS").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U3COMPRAS").Value
                                sdbcOrgCompra.Text = sdbcOrgCompra.Columns(0).Text
                                lblOrgCompras.caption = sdbcOrgCompra.Text
                                lblOrgCompras.Visible = False
                                sdbcOrgCompra.Visible = True
                            End If
                        End If
                        If rsOrgCompras.Fields("U1CENTRO").Value <> "" Or rsOrgCompras.Fields("U2CENTRO").Value <> "" Or rsOrgCompras.Fields("U3CENTRO").Value <> "" Then
                            Set moCentro = Nothing
                            Set moCentro = oFSGSRaiz.Generar_CCentro
                            If rsOrgCompras.Fields("U1CENTRO").Value <> "" Then
                                sdbcCentroApr.AddItem moCentro.DevolverCentro(rsOrgCompras.Fields("U1CENTRO").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1CENTRO").Value
                                sdbcCentroApr.Text = sdbcCentroApr.Columns(0).Text
                                lblCentro.caption = sdbcCentroApr.Text
                                lblCentro.Visible = True
                                sdbcCentroApr.Visible = False
                            Else
                                If rsOrgCompras.Fields("U2CENTRO").Value <> "" Then
                                    sdbcCentroApr.AddItem moCentro.DevolverCentro(rsOrgCompras.Fields("U2CENTRO").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U2CENTRO").Value
                                    sdbcCentroApr.Text = sdbcCentroApr.Columns(0).Text
                                    lblCentro.caption = sdbcCentroApr.Text
                                    lblCentro.Visible = True
                                    sdbcCentroApr.Visible = False
                                Else
                                    sdbcCentroApr.AddItem moCentro.DevolverCentro(rsOrgCompras.Fields("U3CENTRO").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U3CENTRO").Value
                                    sdbcCentroApr.Text = sdbcCentroApr.Columns(0).Text
                                    lblCentro.caption = sdbcCentroApr.Text
                                    If frmESTRORG.Accion = ACCUON3DET Then
                                        lblCentro.Visible = True
                                        sdbcCentroApr.Visible = False
                                    Else
                                        lblCentro.Visible = False
                                        sdbcCentroApr.Visible = True
                                        sdbcCentroApr.Enabled = True
                                    End If
                                    
                                End If
                            End If
                            If rsOrgCompras.Fields("U1ALMACEN").Value <> 0 Or rsOrgCompras.Fields("U2ALMACEN").Value <> 0 Or rsOrgCompras.Fields("U3ALMACEN").Value <> 0 Then
                                Set moAlmacen = Nothing
                                Set moAlmacen = oFSGSRaiz.Generar_CAlmacen
                                If rsOrgCompras.Fields("U1ALMACEN").Value <> 0 Then
                                    lblAlmacen.caption = moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U1ALMACEN").Value)
                                    sdbcAlmacen.AddItem moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U1ALMACEN").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U1ALMACEN").Value
                                    lblAlmacen.Visible = True
                                    sdbcAlmacen.Visible = False
                                Else
                                    If rsOrgCompras.Fields("U2ALMACEN").Value <> 0 Then
                                        If frmESTRORG.Accion <> ACCUON3DET Then
                                            lblAlmacen.caption = moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U2ALMACEN").Value)
                                            sdbcAlmacen.AddItem moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U2ALMACEN").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U2ALMACEN").Value
                                            lblAlmacen.Visible = True
                                            sdbcAlmacen.Visible = False
                                        Else
                                            lblAlmacen.caption = moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U2ALMACEN").Value)
                                            lblAlmacen.Visible = True
                                            sdbcAlmacen.Visible = False
                                        End If
                                    Else
                                        sdbcAlmacen.AddItem moAlmacen.DevolverAlmacen(rsOrgCompras.Fields("U3ALMACEN").Value) & Chr(m_lSeparador) & rsOrgCompras.Fields("U3ALMACEN").Value
                                        sdbcAlmacen.Text = sdbcAlmacen.Columns(0).Text
                                        lblAlmacen.caption = sdbcAlmacen.Text
                                        If frmESTRORG.Accion = ACCUON3DET And rsOrgCompras.Fields("U3ALMACEN").Value <> 0 Then
                                            lblAlmacen.Visible = True
                                            sdbcAlmacen.Visible = False
                                        Else
                                            lblAlmacen.Visible = False
                                            sdbcAlmacen.Visible = True
                                        End If
                                        
                                    End If
                                    
                                End If
                            Else
                                lblAlmacen.Visible = False
                                If frmESTRORG.Accion <> ACCUON3DET Then
                                    sdbcAlmacen.Visible = True
                                Else
                                    sdbcAlmacen.Visible = False
                                End If
                                If frmESTRORG.Accion = ACCUON3MOD Then
                                    Label3.Visible = True
                                Else
                                    Me.Label3.Visible = False
                                End If
                            End If
                        Else
'                            If frmESTRORG.Accion = ACCUON3DET Then
'                                lblOrgCompras.caption = ""
'                                sdbcOrgCompra.Text = ""
'                            End If
                                If frmESTRORG.Accion = ACCUON3MOD Then
                                    Label1.Visible = True
                                Else
                                    Me.Label1.Visible = False
                                End If
                            lblCentro.Visible = False
                            sdbcCentroApr.Visible = True
                            sdbcCentroApr.Enabled = True
                        End If
                    Else
                        lblOrgCompras.Visible = False
                        sdbcOrgCompra.Visible = True
                        sdbcOrgCompra.Enabled = True
                End If
                
            End If
        End Select
    End If
    Set rsOrgCompras = Nothing

End Sub

Private Sub Form_Activate()
    Dim oDim As Dimensiones
    
    PintarFormulario
    If Not Me.WindowState = vbMinimized And Not Me.WindowState = vbMaximized Then
        oDim = CalcularDimensionesFormulario
        Me.Height = oDim.Largo
        Me.Width = oDim.Ancho
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Carga Inicial del formulario
''' </summary>
''' <remarks>
''' Llamada desde evento Load. M�x 0,3 seg.
''' </remarks>
Private Sub Form_Load()
    blnNoExisteAlmacen = False
    blnNoExisteCentroApr = False
    blnNoExisteOrgCompras = False
    
    m_bCargaForm = True

    PonerFieldSeparator Me
    ConfigurarComboDirecciones
    
    Label4.Visible = gParametrosGenerales.gbUsarRemitenteEmpresa
    txtRmte.Visible = gParametrosGenerales.gbUsarRemitenteEmpresa
    
    Select Case frmESTRORG.Accion
        Case ACCDepMod
                
            txtCod = frmESTRORG.oDepAsocSeleccionado.Cod
            txtDen = frmESTRORG.oDepAsocSeleccionado.Den
        
        Case ACCUON1Mod, ACCUON2MOD, ACCUON3MOD, _
             ACCUON1Det, ACCUON2DET, ACCUON3DET, _
             ACCUON1Anya, ACCUON2Anya, ACCUON3Anya
                              
            Select Case frmESTRORG.Accion
                Case ACCUON1Mod, ACCUON1Det
                    Set moEmpresaSeleccionada = frmESTRORG.oUON1Seleccionada.Empresa
                Case ACCUON2MOD, ACCUON2DET
                    Set moEmpresaSeleccionada = frmESTRORG.oUON2Seleccionada.Empresa
                Case ACCUON3MOD, ACCUON3DET
                    Set moEmpresaSeleccionada = frmESTRORG.oUON3Seleccionada.Empresa
                Case Else
                    Set moEmpresaSeleccionada = Nothing
            End Select
                
            If (frmESTRORG.Accion = ACCUON1Anya Or frmESTRORG.Accion = ACCUON2Anya Or frmESTRORG.Accion = ACCUON3Anya Or frmESTRORG.Accion = ACCUON1Mod Or frmESTRORG.Accion = ACCUON2MOD Or frmESTRORG.Accion = ACCUON3MOD) _
                Or (Not moEmpresaSeleccionada Is Nothing) Then
                               
                If Not moEmpresaSeleccionada Is Nothing Then
                    m_bNIFRespetarCombo = True
                    Me.sdbcNIF.Text = moEmpresaSeleccionada.NIF
                    Me.sdbcNIF.Columns(2).Value = moEmpresaSeleccionada.Id
                    Me.sdbcNIF.Columns(0).Value = moEmpresaSeleccionada.NIF
                    Me.sdbcNIF.Columns(1).Value = moEmpresaSeleccionada.Den
                    m_bNIFRespetarCombo = False
                    EmpresaSeleccionada
                End If
            End If
                    
        Case ACCUON4Anya, ACCUON4MOD, ACCUON4DET
            If frmESTRORG.Accion = ACCUON4MOD Or frmESTRORG.Accion = ACCUON4DET Then
                  Set moEmpresaSeleccionada = frmESTRORG.oUON4Seleccionada.Empresa
            Else
                  Set moEmpresaSeleccionada = Nothing
            End If
                
            If (frmESTRORG.Accion = ACCUON4Anya Or frmESTRORG.Accion = ACCUON4MOD) Or (Not moEmpresaSeleccionada Is Nothing) Then
                If Not moEmpresaSeleccionada Is Nothing Then
                    m_bNIFRespetarCombo = True
                    Me.sdbcNIF.Text = moEmpresaSeleccionada.NIF
                    Me.sdbcNIF.Columns(2).Value = moEmpresaSeleccionada.Id
                    Me.sdbcNIF.Columns(0).Value = moEmpresaSeleccionada.NIF
                    Me.sdbcNIF.Columns(1).Value = moEmpresaSeleccionada.Den
                    m_bNIFRespetarCombo = False
                    EmpresaSeleccionada
                End If
            End If
    End Select
    
    CargarRecursos
    
    RellenarDatosFacturacion

    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = (MDI.ScaleHeight / 2 - Me.Height / 2) + 450
End Sub

''' <summary>Asigna el combo de direcciones a la columna de direcciones del grid</summary>
''' <remarks>Llamada Form_Load</remarks>

Private Sub ConfigurarComboDirecciones()
    sdbddDirecciones.AddItem ""
    sdbgDirecciones.Columns("DIR").DropDownHwnd = sdbddDirecciones.hWnd
End Sub

Private Sub Form_Resize()
    'Si se ha minimizado no hacer el resize
    If Not Me.WindowState = vbMinimized Then
        ResizeFormulario
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>Efectua el redimensionamiento del formulario.</summary>
''' <remarks>Llamada desde: Form_Resize y chkEmpresa_MouseUp. Maximo 0,1 seg</remarks>
Private Sub ResizeFormulario()
    Dim lMinWidth As Long
    Dim lMinHeight As Long
    Dim lGridMinWidth As Long
    
    lMinHeight = AlturaFormCaption + picEmpresa.Top + IIf(picEmpresa.Visible, picEmpresa.Height, 0) + IIf(picEdit.Visible, picEdit.Height + Separacion, Separacion)
    If fraEmpresa.Visible Then
        lMinWidth = fraEmpresa.Left + sstabDatosEmpresa.Left + fraDatosEmpresa.Width + (4 * Separacion)
        lMinHeight = lMinHeight + fraDatosEmpresa.Height + (4 * Separacion) - (chkEmpresa.Height / 2)
        
        If fraEstrOrganizativa.Visible Then
            lMinHeight = lMinHeight + fraEstrOrganizativa.Height '+ (2 * Separacion)
        End If
    Else
        'Restablecer el tama�o del frame al m�nimo
        sdbgDirecciones.Width = fraDatosEmpresa.Width
        fraDirecciones.Width = fraDatosEmpresa.Width + Separacion
        sstabDatosEmpresa.Width = fraDirecciones.Width + Separacion
        fraEmpresa.Width = fraDirecciones.Width + Separacion
        sdbgDirecciones.Height = fraDatosEmpresa.Height
        fraDirecciones.Height = fraDatosEmpresa.Height + Separacion
        sstabDatosEmpresa.Height = fraDatosEmpresa.Height + (5 * Separacion)
        fraEmpresa.Height = fraDirecciones.Height + (4 * Separacion) ' + fraAutofacturacion.Height + (4 * Separacion)
        
        lMinWidth = picDatos.Left + picDatos.Width
        
        If fraEstrOrganizativa.Visible Then
            lMinHeight = lMinHeight + fraEstrOrganizativa.Height '+ (2 * Separacion)
        End If
    End If
    
    If Me.Width < lMinWidth Then Me.Width = lMinWidth
    If Me.Height < lMinHeight Then Me.Height = lMinHeight
    
    If fraEmpresa.Visible Then
        '1. Anchos
        fraEmpresa.Width = Me.Width - fraEmpresa.Left - (1.5 * Separacion)
        sstabDatosEmpresa.Width = fraEmpresa.Width - (1.5 * Separacion)
        SSTabFacturacion.Width = sstabDatosEmpresa.Width - (2 * Separacion)
        sdbgDirecciones.Width = SSTabFacturacion.Width - (2 * Separacion)
        fraDirecciones.Width = sdbgDirecciones.Width + Separacion
        fraFacturacionMensual.Width = SSTabFacturacion.Width - (2 * Separacion)
        fraTolerancia.Width = SSTabFacturacion.Width - (2 * Separacion)
        fraAutofacturacion.Width = fraFacturacionMensual.Width + Separacion
        
        '2. Alturas
        fraEmpresa.Height = Me.Height - AlturaFormCaption - (picEmpresa.Top + picEmpresa.Height / 2) - IIf(picEdit.Visible, picEdit.Height, 0)
        
        If fraEstrOrganizativa.Visible Then
            fraEmpresa.Height = fraEmpresa.Height - fraEstrOrganizativa.Height '- Separacion
        End If
        
        sstabDatosEmpresa.Height = fraEmpresa.Height - (2 * Separacion)
        SSTabFacturacion.Height = sstabDatosEmpresa.Height - (4 * Separacion)
        sdbgDirecciones.Height = SSTabFacturacion.Height - (4 * Separacion)
        fraDirecciones.Height = sdbgDirecciones.Height + Separacion
        
    End If
    
    If fraEstrOrganizativa.Visible Then
        fraEstrOrganizativa.Width = fraEmpresa.Width
        
        If fraEmpresa.Visible Then
            fraEstrOrganizativa.Top = fraEmpresa.Top + fraEmpresa.Height '+ Separacion
        Else
            fraEstrOrganizativa.Top = picEmpresa.Top + picEmpresa.Height '+ Separacion
        End If
    End If
    
    'Redimensionar las columnas del grid
    With sdbgDirecciones
        If .Width > 9500 Then
            .Columns("DIR").Width = .Width * 0.26
            .Columns("CP").Width = .Width * 0.13
            .Columns("POB").Width = .Width * 0.19
            .Columns("PROVIDEN").Width = .Width * 0.19
            .Columns("PAIDEN").Width = .Width * 0.19
        Else
            .Columns("DIR").Width = 2500
            .Columns("CP").Width = 1200
            .Columns("POB").Width = 1800
            .Columns("PROVIDEN").Width = 1800
            .Columns("PAIDEN").Width = 1800
        End If
    End With
    
    
    picEdit.Top = Me.ScaleHeight - picEdit.Height
    picEdit.Left = (Me.Width - picEdit.Width) / 2
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Select Case frmESTRORG.Accion
        Case ACCUON1Mod, ACCUON2MOD, ACCUON3MOD, ACCDepMod, ACCUON1Eli, ACCUON2Eli, ACCUON3Eli, ACCDepEli
            frmESTRORG.oIBaseDatos.CancelarEdicion
    End Select
    
    frmESTRORG.Accion = ACCUOCon
End Sub


''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Al seleccionar el control deshabilitamos la opci�n alternativa y vac�amos sus valores
''' </summary>
''' <returns></returns>
''' <remarks>
''' Llamada desde el evento click. M�ximo 0,1 seg.
''' </remarks>
Private Sub optDia_Click()
    optSemana.Value = False
    cboSemanaMes.Text = ""
    cboDiaSemana.Text = ""
    txtPeriodoMeses2.Text = ""
    optDia_Enable (True)
    optSemana_Enable (False)
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Al seleccionar el control deshabilitamos la opci�n alternativa y vac�amos sus valores
''' </summary>
''' <remarks>
''' Llamada desde el evento click. M�ximo 0,1 seg.
''' </remarks>
Private Sub optSemana_Click()
    optDia.Value = False
    txtDia.Text = ""
    txtPeriodoMeses1.Text = ""
    optDia_Enable (False)
    optSemana_Enable (True)
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Deshabilitamos o habilitar los controles de la opci�n
''' </summary>
''' <remarks>
''' Llamada desde optDia_Click y RellenarDatosFacturacion. M�ximo 0,1 seg.
''' </remarks>
Private Sub optDia_Enable(ByVal enable As Boolean)
    txtDia.Enabled = enable
    updDia.Enabled = enable
    txtPeriodoMeses1.Enabled = enable
    updPeriodoMeses1.Enabled = enable
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Deshabilitamos o habilitar los controles de la opci�n
''' </summary>
''' <remarks>
''' Llamada desde optDia_Click y RellenarDatosFacturacion. M�ximo 0,1 seg.
''' </remarks>
Private Sub optSemana_Enable(ByVal enable As Boolean)
    cboSemanaMes.Enabled = enable
    cboDiaSemana.Enabled = enable
    txtPeriodoMeses2.Enabled = enable
    updPeriodoMeses2.Enabled = enable
End Sub

Private Sub sdbcAlmacen_Change()
    If sdbcAlmacen.Value = "" Then
        sdbcAlmacen.Text = ""
        lblAlmacen.caption = ""
    End If
End Sub

Private Sub sdbcAlmacen_CloseUp()
    sdbcAlmacen.Text = sdbcAlmacen.Columns(0).Text
End Sub

Private Sub sdbcAlmacen_DropDown()
    'Cargar todas los almacen de los centros de aprovisionamiento
    Screen.MousePointer = vbHourglass
    Dim rsAlmacenes As ADODB.Recordset
    
    Set moAlmacenes = Nothing
    Set moAlmacenes = oFSGSRaiz.Generar_CAlmacenes
    Dim i As Integer
    
    moAlmacenes.CargarTodosLosAlmacenes , sdbcCentroApr.Columns(1).Text
    
    sdbcAlmacen.RemoveAll
    i = 1
    If moAlmacenes.Count > 0 Then
       For i = 1 To moAlmacenes.Count
            sdbcAlmacen.AddItem moAlmacenes.Item(i).Den & Chr(m_lSeparador) & moAlmacenes.Item(i).Id & Chr(m_lSeparador) & moAlmacenes.Item(i).Cod
        Next
    End If
    sdbcAlmacen.Columns(2).Visible = False
    sdbcAlmacen.Columns(1).Visible = True
    sdbcAlmacen.Columns(0).Visible = True
    
    sdbcAlmacen.ListAutoPosition = True
    'sdbcAlmacen.SelStart = 0
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcAlmacen_InitColumnProps()
    sdbcAlmacen.DataFieldList = "Column 0"
    sdbcAlmacen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcAlmacen_LostFocus()
    Dim i As Integer
    If sdbcAlmacen.Text <> "" Then
        Set moAlmacenes = Nothing
        Set moAlmacenes = oFSGSRaiz.Generar_CAlmacenes
     
        moAlmacenes.CargarTodosLosAlmacenes , sdbcCentroApr.Columns(1).Text
        
        If moAlmacenes.Count > 0 Then
            For i = 1 To moAlmacenes.Count
                If UCase(moAlmacenes.Item(i).Den) = UCase(sdbcAlmacen.Text) Then
                    sdbcAlmacen.Columns(2).Text = moAlmacenes.Item(i).Cod
                    sdbcAlmacen.Columns(1).Text = moAlmacenes.Item(i).Id
                    sdbcAlmacen.Columns(0).Text = moAlmacenes.Item(i).Den
                    sdbcAlmacen.Text = moAlmacenes.Item(i).Den
                    blnNoExisteAlmacen = False
                    Exit Sub
                Else
                    blnNoExisteAlmacen = True
                End If
            Next
        End If
        If blnNoExisteAlmacen Then
            oMensajes.NoExisteAlmacen
        End If
    End If
End Sub

Private Sub sdbcAlmacen_PositionList(ByVal Text As String)
PositionList sdbcAlmacen, Text
End Sub

Private Sub sdbcCentroApr_Change()
    sdbcAlmacen.Value = ""
    sdbcAlmacen.RemoveAll
    If sdbcCentroApr.Value = "" Then
        sdbcCentroApr.RemoveAll
        sdbcAlmacen.Enabled = False
    Else
        sdbcAlmacen.Enabled = True
    End If
End Sub

Private Sub sdbcCentroApr_CloseUp()
    sdbcCentroApr.Text = sdbcCentroApr.Columns(0).Text
    If sdbcCentroApr.Text <> "" Then
        sdbcAlmacen.Text = ""
        sdbcAlmacen.Enabled = True
    Else
        sdbcAlmacen.Enabled = False
    End If
End Sub

Private Sub sdbcCentroApr_DropDown()
    'Cargar todas los centros de aprovisionamiento para la organizacion de compra seleccionada
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    
    Set moCentros = Nothing
    Set moCentros = oFSGSRaiz.Generar_CCentros
     
    sdbcCentroApr.RemoveAll
    
    moCentros.CargarCentrosDeLaOrganizacion (sdbcOrgCompra.Columns(1).Text)
    If moCentros.Count > 0 Then
        For i = 1 To moCentros.Count
            sdbcCentroApr.AddItem moCentros.Centros(i).Den & Chr(m_lSeparador) & moCentros.Centros(i).Cod
        Next
    Else
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    sdbcCentroApr.ListAutoPosition = True
    'sdbcCentroApr.SelStart = 0
    Screen.MousePointer = vbNormal
            
End Sub

Private Sub sdbcCentroApr_InitColumnProps()
    sdbcCentroApr.DataFieldList = "Column 0"
    sdbcCentroApr.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCentroApr_LostFocus()
    Dim i As Integer
    If sdbcCentroApr.Text <> "" Then
        Set moCentros = Nothing
        Set moCentros = oFSGSRaiz.Generar_CCentros
     
        moCentros.CargarCentrosDeLaOrganizacion (sdbcOrgCompra.Columns(1).Text)
        If moCentros.Count > 0 Then
            For i = 1 To moCentros.Count
                If UCase(moCentros.Item(i).Den) = UCase(sdbcCentroApr.Text) Then
                    sdbcCentroApr.Columns(1).Text = moCentros.Item(i).Cod
                    sdbcCentroApr.Columns(0).Text = moCentros.Item(i).Den
                    sdbcCentroApr.Text = moCentros.Item(i).Den
                    blnNoExisteCentroApr = False
                    Exit Sub
                Else
                    blnNoExisteCentroApr = True
                End If
            Next
        End If
        If blnNoExisteCentroApr Then
            oMensajes.NoExisteCentroAprov
            sdbcAlmacen.Enabled = False
        End If
    End If
        
End Sub

Private Sub sdbcCentroApr_PositionList(ByVal Text As String)
PositionList sdbcCentroApr, Text
End Sub


Private Sub sdbcNIF_Change()
    Dim sOldTxt As String
    
    If Not m_bNIFRespetarCombo Then
            
'        If Not moEmpresaSeleccionada Is Nothing Then
'            If moEmpresaSeleccionada.nif <> sdbcNIF.Text Then
'                If Not GestionarBajaEmpresa Then
'                    m_bNIFRespetarCombo = True
'                    Me.sdbcNIF.Columns(2).Value = moEmpresaSeleccionada.Id
'                    Me.sdbcNIF.Columns(0).Value = moEmpresaSeleccionada.nif
'                    Me.sdbcNIF.Columns(1).Value = moEmpresaSeleccionada.Den
'                    sdbcNIF.Text = moEmpresaSeleccionada.nif
'                    m_bNIFRespetarCombo = False
'                    Screen.MousePointer = vbNormal
'                    Exit Sub
'                End If
'            End If
'        End If
        If Me.sdbcNIF.DroppedDown Then
            sOldTxt = Me.sdbcNIF.Text
            Me.sdbcNIF.DroppedDown = False
            m_bNIFRespetarCombo = True
            Me.sdbcNIF.Text = sOldTxt
            Me.sdbcNIF.SelStart = Len(Me.sdbcNIF.Text)
            m_bNIFRespetarCombo = False
        End If
        m_bNIFCargarComboDesde = True
        Set moEmpresaSeleccionada = Nothing
        LimpiarCampos
        

    End If

End Sub

Private Sub sdbcNIF_CloseUp()
    
    
'    If Not moEmpresaSeleccionada Is Nothing Then
'        If moEmpresaSeleccionada.nif <> sdbcNIF.Text Then
'            If Not GestionarBajaEmpresa Then
'                m_bNIFRespetarCombo = True
'                Me.sdbcNIF.Columns(2).Value = moEmpresaSeleccionada.Id
'                Me.sdbcNIF.Columns(0).Value = moEmpresaSeleccionada.nif
'                Me.sdbcNIF.Columns(1).Value = moEmpresaSeleccionada.Den
'                sdbcNIF.Text = moEmpresaSeleccionada.nif
'                m_bNIFRespetarCombo = False
'                Screen.MousePointer = vbNormal
'                Exit Sub
'            Else
'                Set moEmpresaSeleccionada = Nothing
'            End If
'        End If
'    End If
    
    m_bNIFRespetarCombo = True



'    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcNIF.Text = sdbcNIF.Columns(0).Text
    m_bNIFRespetarCombo = False
    
    Set moEmpresaSeleccionada = m_oEmpresas.Item(CStr(sdbcNIF.Columns(2).Text))
    DoEvents
    
    Screen.MousePointer = vbHourglass
    EmpresaSeleccionada
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcNIF_DropDown()
    
    Dim i As Integer
    Dim oEmpresa As CEmpresa
    
    Screen.MousePointer = vbHourglass
    
    Set m_oEmpresas = Nothing
    Set m_oEmpresas = oFSGSRaiz.Generar_CEmpresas
     
    sdbcNIF.RemoveAll
    
    If m_bNIFCargarComboDesde Then
        m_oEmpresas.CargarTodasLasEmpresasDesde Me.sdbcNIF.Text, , , , , True, False
    Else
        m_oEmpresas.CargarTodasLasEmpresasDesde , , , , , , False
    End If
    
    For Each oEmpresa In m_oEmpresas
        sdbcNIF.AddItem oEmpresa.NIF & Chr(m_lSeparador) & oEmpresa.Den & Chr(m_lSeparador) & oEmpresa.Id
    Next
    
    sdbcNIF.SelStart = 0
    sdbcNIF.SelLength = Len(sdbcNIF.Text)
'    sdbcPaiCod.Refresh

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcNIF_Validate(Cancel As Boolean)
    Dim oEmpresa As CEmpresa
    
    
    If Trim(Me.sdbcNIF.Text) = "" Then
        
        m_bNIFRespetarCombo = True
        Me.sdbcNIF.Text = ""
        m_bNIFRespetarCombo = False
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    
    Set m_oEmpresas = Nothing
    Set m_oEmpresas = oFSGSRaiz.Generar_CEmpresas
                    
     
    m_oEmpresas.CargarTodasLasEmpresasDesde Me.sdbcNIF.Text, , , , , False, False
    
    If m_oEmpresas.Count = 0 Then
        If oMensajes.NoExisteEmpresa = vbNo Then
            Cancel = True
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    Else
        If sdbcNIF.Rows = 0 Then
            For Each oEmpresa In m_oEmpresas
                sdbcNIF.AddItem oEmpresa.NIF & Chr(m_lSeparador) & oEmpresa.Den & Chr(m_lSeparador) & oEmpresa.Id
            Next
            sdbcNIF.MoveFirst
        End If
        m_bNIFRespetarCombo = True
        sdbcNIF.Text = sdbcNIF.Columns(0).Text
        m_bNIFRespetarCombo = False
        
        For Each moEmpresaSeleccionada In m_oEmpresas
            Exit For
        Next
        DoEvents
        EmpresaSeleccionada
    End If
    Screen.MousePointer = vbNormal

End Sub

Private Sub AnyadirUOAEstructura(ByVal iNivel As Integer)

Dim nodPadre As node
Dim node As node
Dim nodUltimaUO As node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Set nodPadre = frmESTRORG.tvwestrorg.selectedItem
Set nodUltimaUO = BuscarUltimaUOHija(nodPadre)

Select Case iNivel

Case 1
        scod1 = Trim(txtCod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(Trim(txtCod)))
        
        If nodUltimaUO Is Nothing Then
            Set node = frmESTRORG.tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, Trim(txtCod) & " - " & Trim(txtDen), "UON1")
        Else
            Set node = frmESTRORG.tvwestrorg.Nodes.Add(nodUltimaUO.key, tvwNext, "UON1" & scod1, Trim(txtCod) & " - " & Trim(txtDen), "UON1")
        End If
        If Me.chkEmpresa Then
            node.Image = "SELLO"
        End If
        
        node.Tag = "UON1" & Trim(txtCod)
Case 2
        scod1 = frmESTRORG.DevolverCod(nodPadre) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(frmESTRORG.DevolverCod(nodPadre)))
        scod2 = scod1 & Trim(txtCod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(Trim(txtCod)))
        
        If nodUltimaUO Is Nothing Then
            Set node = frmESTRORG.tvwestrorg.Nodes.Add(nodPadre.key, tvwChild, "UON2" & scod2, Trim(txtCod) & " - " & Trim(txtDen), "UON2")
        Else
            Set node = frmESTRORG.tvwestrorg.Nodes.Add(nodUltimaUO, tvwNext, "UON2" & scod2, Trim(txtCod) & " - " & Trim(txtDen), "UON2")
        End If
        If Me.chkEmpresa Then
            node.Image = "SELLO"
        End If
        node.Tag = "UON2" & Trim(txtCod)
Case 3
        
        scod1 = frmESTRORG.DevolverCod(nodPadre.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(frmESTRORG.DevolverCod(nodPadre.Parent)))
        scod2 = scod1 & frmESTRORG.DevolverCod(nodPadre) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(frmESTRORG.DevolverCod(nodPadre)))
        scod3 = scod2 & Trim(txtCod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(Trim(txtCod)))
        
        If nodUltimaUO Is Nothing Then
            Set node = frmESTRORG.tvwestrorg.Nodes.Add(nodPadre.key, tvwChild, "UON3" & scod3, Trim(txtCod) & " - " & Trim(txtDen), "UON3")
        Else
            Set node = frmESTRORG.tvwestrorg.Nodes.Add(nodUltimaUO, tvwNext, "UON3" & scod3, Trim(txtCod) & " - " & Trim(txtDen), "UON3")
        End If
        
        If Me.chkEmpresa Then
            node.Image = "SELLO"
        End If
        node.Tag = "UON3" & Trim(txtCod)
Case 4
        scod1 = frmESTRORG.DevolverCod(nodPadre.Parent.Parent) & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(frmESTRORG.DevolverCod(nodPadre.Parent.Parent)))
        scod2 = scod1 & frmESTRORG.DevolverCod(nodPadre.Parent) & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(frmESTRORG.DevolverCod(nodPadre.Parent)))
        scod3 = scod2 & frmESTRORG.DevolverCod(nodPadre) & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(frmESTRORG.DevolverCod(nodPadre)))
        scod4 = scod3 & Trim(txtCod.Text) & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(Trim(txtCod.Text)))
        If nodUltimaUO Is Nothing Then
            Set node = frmESTRORG.tvwestrorg.Nodes.Add(nodPadre.key, tvwChild, "UON4" & scod4, Trim(txtCod.Text) & " - " & Trim(txtDen.Text), "UON4")
        Else
            Set node = frmESTRORG.tvwestrorg.Nodes.Add(nodUltimaUO, tvwNext, "UON4" & scod4, Trim(txtCod.Text) & " - " & Trim(txtDen.Text), "UON4")
        End If
        
        If Me.chkEmpresa Then
            node.Image = "SELLO"
        End If
        node.Tag = "UON4" & Trim(txtCod)

End Select
node.Selected = True
node.EnsureVisible
Set nodPadre = Nothing
Set node = Nothing
Set nodUltimaUO = Nothing
End Sub
Private Sub ModificarUOEnEstructura()

Dim nod As node

Set nod = frmESTRORG.tvwestrorg.selectedItem

    nod.Text = Trim(txtCod) & " - " & Trim(txtDen)
    If Me.chkEmpresa Then
        nod.Image = "SELLO"
    Else
        Select Case frmESTRORG.Accion
            Case ACCUON1Mod
                nod.Image = "UON1"
            Case ACCUON2MOD
                nod.Image = "UON2"
            Case ACCUON3MOD
                nod.Image = "UON3"
            Case ACCUON4MOD
                nod.Image = "UON4"
        End Select
    End If
    
    
Set nod = Nothing

End Sub
Private Sub ModificarDepEnEstructura()

Dim nod As node
Dim varCod As Variant
Dim sDen As String

Set nod = frmESTRORG.tvwestrorg.selectedItem
varCod = frmESTRORG.DevolverCod(nod)
sDen = Trim(txtDen)

Set nod = Nothing

For Each nod In frmESTRORG.tvwestrorg.Nodes
    If Left(nod.Tag, 3) = "DEP" And Right(nod.Tag, Len(nod.Tag) - 4) = CStr(varCod) Then
        nod.Text = Trim(txtCod) & " - " & sDen
    End If
Next

End Sub
Private Function BuscarUltimaUOHija(ByVal nodx As node) As node
Dim nodo As node
Dim bContinuar As Boolean

bContinuar = True

If Not nodx.Child Is Nothing Then
    Set nodo = nodx.Child.LastSibling
    
    While bContinuar
        
        If nodo.Previous Is Nothing Then
            bContinuar = False
            If Left(nodo.Tag, 3) = "DEP" Then
                Set nodo = Nothing
             End If
        Else
            If Left(nodo.Tag, 3) <> "DEP" Then
                bContinuar = False
            Else
                Set nodo = nodo.Previous
            End If
            
        End If
        
    Wend
End If

Set BuscarUltimaUOHija = nodo
Set nodo = Nothing
End Function

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Cargamos los textos
''' </summary>
''' <remarks>
''' Llamada desde frmEstrorgDetalle.frm. M�ximo 0,1 seg.
''' </remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Byte
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTROG_DETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Ador.MoveFirst
        
        caption = Ador(0).Value
        lblCod.caption = TextoSiguiente(Ador)
        lblDen.caption = TextoSiguiente(Ador)
        cmdAceptar.caption = TextoSiguiente(Ador)
        cmdCancelar.caption = TextoSiguiente(Ador)
        sIdiCod = TextoSiguiente(Ador)
        sIdiDen = TextoSiguiente(Ador)
        sIdiNif = TextoSiguiente(Ador)
        sIdiRazonSocial = TextoSiguiente(Ador)
        Me.lblDenCia.caption = "(*) " & sIdiRazonSocial & ":"
        Me.lblCIF.caption = "(*) " & sIdiNif & ":"
        Me.lblCP.caption = TextoSiguiente(Ador)
        Me.lblDir.caption = TextoSiguiente(Ador)
        Me.lblPob.caption = TextoSiguiente(Ador)
        Me.lblProvi.caption = TextoSiguiente(Ador)
        Me.lblPai.caption = TextoSiguiente(Ador)
        Me.chkEmpresa.caption = TextoSiguiente(Ador)
        For i = 1 To 15
            Ador.MoveNext
        Next
        sstabDatosEmpresa.TabCaption(0) = TextoSiguiente(Ador)
        SSTabFacturacion.TabCaption(0) = TextoSiguiente(Ador)
        sdbgDirecciones.Columns("DIR").caption = TextoSiguiente(Ador)
        sdbddDirecciones.Columns("DIR").caption = Ador(0).Value
        sdbgDirecciones.Columns("CP").caption = TextoSiguiente(Ador)
        sdbddDirecciones.Columns("CP").caption = Ador(0).Value
        sdbgDirecciones.Columns("POB").caption = TextoSiguiente(Ador)
        sdbddDirecciones.Columns("POB").caption = Ador(0).Value
        sdbgDirecciones.Columns("PROVIDEN").caption = TextoSiguiente(Ador)
        sdbddDirecciones.Columns("PROVIDEN").caption = Ador(0).Value
        sdbgDirecciones.Columns("PAIDEN").caption = TextoSiguiente(Ador)
        sdbddDirecciones.Columns("PAIDEN").caption = Ador(0).Value
                
        sdbddPaises.Columns(0).caption = TextoSiguiente(Ador)
        sdbddPaises.Columns(1).caption = TextoSiguiente(Ador)
        m_sDireccionObligatoria = TextoSiguiente(Ador)
        Ador.MoveNext
        m_sDireccionRepetida = TextoSiguiente(Ador)
        sstabDatosEmpresa.TabCaption(1) = TextoSiguiente(Ador)
        lblNumMaxLineasFra.caption = TextoSiguiente(Ador)
        fraAutofacturacion.caption = TextoSiguiente(Ador)
        lblDia.caption = TextoSiguiente(Ador)
        lblDecada.caption = TextoSiguiente(Ador)
        lblDecada2.caption = lblDecada.caption
        lblMeses.caption = TextoSiguiente(Ador)
        lblMeses2.caption = lblMeses.caption
        lblEl.caption = TextoSiguiente(Ador)
        lblHoraEjecucion.caption = TextoSiguiente(Ador)
        fraTolerancia.caption = TextoSiguiente(Ador)
        lblToleranciaImporte.caption = TextoSiguiente(Ador)
        For i = 1 To 7 'Lunes, Martes, Mi�rcoles, etc.
            sDiasSemana(i) = TextoSiguiente(Ador)
        Next
        For i = 1 To 4 '1er, 2�, 3er, 4�
            sSemanasMes(i) = TextoSiguiente(Ador)
        Next
        SSTabFacturacion.TabCaption(1) = TextoSiguiente(Ador)
        Label4.caption = TextoSiguiente(Ador) & ":"
        Ador.Close
        
        lblMoneda.caption = gParametrosGenerales.gsMONCEN
    End If

    Set Ador = Nothing

End Sub

''' <summary>devuelve valor del texto siguiente registro del recordset pasado como par�metro</summary>
''' <param name="Ador">Recordset en el que buscar.</param>
''' <returns>El valor del texto en el campo 0</returns>
''' <remarks>Llamada desde Form_Load</remarks>

Private Function TextoSiguiente(ByVal Ador As ADODB.Recordset) As String
    Ador.MoveNext
    TextoSiguiente = Ador(0).Value
End Function

Private Sub sdbcOrgCompra_Change()
    sdbcCentroApr.Value = ""
    sdbcAlmacen.Value = ""
    sdbcCentroApr.RemoveAll
    sdbcAlmacen.RemoveAll
    If sdbcOrgCompra.Value = "" Then
        lblOrgCompras.caption = ""
        sdbcOrgCompra.RemoveAll
        sdbcCentroApr.Enabled = False
        sdbcAlmacen.Enabled = False
    Else
        If Not blnNoExisteOrgCompras Then
            sdbcCentroApr.Enabled = True
        Else
                sdbcCentroApr.Enabled = False
        End If
    End If
End Sub

Private Sub sdbcOrgCompra_CloseUp()
    'Si se ha elegido una Organizacion de compras cargar sus centros de aprovisionamiento
      
    sdbcOrgCompra.Text = sdbcOrgCompra.Columns(0).Text
    If sdbcOrgCompra.Text <> "" Then
        sdbcCentroApr.Text = ""
        sdbcCentroApr.Enabled = True
        sdbcAlmacen.Text = ""
        sdbcAlmacen.Enabled = False
    Else
        sdbcCentroApr.Enabled = False
    End If
        
End Sub

Private Sub sdbcOrgCompra_DropDown()
    'Cargar todas las organizaciones de compras
    
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    
    Set moOrgCompras = Nothing
    Set moOrgCompras = oFSGSRaiz.Generar_COrganizacionesCompras
                      
    moOrgCompras.CargarOrganizacionesCompra
    
    sdbcOrgCompra.RemoveAll
    If moOrgCompras.Count > 0 Then
        For i = 1 To moOrgCompras.Count
            sdbcOrgCompra.AddItem moOrgCompras.Item(i).Den & Chr(m_lSeparador) & moOrgCompras.Item(i).Cod
        Next
    End If
    
    sdbcOrgCompra.ListAutoPosition = True
    'sdbcOrgCompra.SelStart = 0
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcOrgCompra_InitColumnProps()
    sdbcOrgCompra.DataFieldList = "Column 0"
    sdbcOrgCompra.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcOrgCompra_LostFocus()
    'Comprobar si la Organizacion de compras existe para esa unidad organizativa
    Dim i As Integer
    
    If sdbcOrgCompra.Text <> "" Then
        Set moOrgCompras = Nothing
        Set moOrgCompras = oFSGSRaiz.Generar_COrganizacionesCompras
                          
        moOrgCompras.CargarOrganizacionesCompra
        If moOrgCompras.Count > 0 Then
            For i = 1 To moOrgCompras.Count
                If UCase(moOrgCompras.Item(i).Den) = UCase(sdbcOrgCompra.Text) Then
                    sdbcOrgCompra.Columns(1).Text = moOrgCompras.Item(i).Cod
                    sdbcOrgCompra.Columns(0).Text = moOrgCompras.Item(i).Den
                    sdbcOrgCompra.Text = moOrgCompras.Item(i).Den
                    sdbcCentroApr.Enabled = True
                    blnNoExisteOrgCompras = False
                    Exit Sub
                Else
                    blnNoExisteOrgCompras = True
                End If
            Next
        End If
        If blnNoExisteOrgCompras Then
            oMensajes.NoExisteOrganizacionCompras
            sdbcCentroApr.Enabled = False
        End If
    End If
End Sub

Private Sub sdbcOrgCompra_PositionList(ByVal Text As String)
PositionList sdbcOrgCompra, Text
End Sub

Private Sub sdbcPaiDen_Change()
    
    If Not m_bPaiRespetarCombo Then
    
        m_bPaiRespetarCombo = True
'        sdbcPaiCod.Text = ""
        m_bPaiRespetarCombo = False
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing

    End If
    
'    sdbcProviCod = ""
    sdbcProviDen = ""
        
End Sub


Private Sub sdbcPaiDen_CloseUp()
    
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    m_bPaiRespetarCombo = True
'    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    m_bPaiRespetarCombo = False
    
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiDen.Columns(1).Text)
    If Not m_oPaisSeleccionado Is Nothing Then
        If Not m_oPaisSeleccionado.Moneda Is Nothing Then
'            sdbcMonCod.Text = m_oPaisSeleccionado.Moneda.Cod
'            sdbcMonCod_Validate False
        End If
    End If
    
    m_bPaiCargarComboDesde = False
        
End Sub

Private Sub sdbcPaiDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
     
    sdbcPaiDen.RemoveAll
    
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        m_oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    Codigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
'    sdbcPaiCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcPaiDen_PositionList(ByVal Text As String)
PositionList sdbcPaiDen, Text
End Sub


Private Sub sdbcProviDen_Change()
    
    If Not m_bProviRespetarCombo Then
    
        m_bProviRespetarCombo = True
'        sdbcProviCod.Text = ""
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcProviDen_Click()
    
    If Not sdbcProviDen.DroppedDown Then
'        sdbcProviCod = ""
        sdbcProviDen = ""
    End If
    
End Sub

Private Sub sdbcProviDen_CloseUp()
    
    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    
    'If sdbcProviDen.Value = "" Then Exit Sub
    
    m_bProviRespetarCombo = True
'    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    m_bProviRespetarCombo = False
    
    m_bProviCargarComboDesde = False
        
End Sub
Private Sub sdbcProviDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProviDen.RemoveAll
    
    If Not m_oPaisSeleccionado Is Nothing Then
         
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProviDen.Text), True, False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        
        Codigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
        
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then
            sdbcProviDen.AddItem "..."
        End If

    End If
    
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
'    sdbcProviCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProviDen_InitColumnProps()

    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcProviDen_PositionList(ByVal Text As String)
PositionList sdbcProviDen, Text
End Sub


Private Sub sdbcPaiDen_Validate(Cancel As Boolean)

    Dim m_oPaises As CPaises
    Dim bExiste As Boolean
    
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    
    If sdbcPaiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    m_oPaises.CargarTodosLosPaises , sdbcPaiDen.Text, True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (m_oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiDen.Text = ""
    Else
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = m_oPaises.Item(1).Den
        sdbcPaiDen.Columns(0).Value = m_oPaises.Item(1).Den
        sdbcPaiDen.Columns(1).Value = m_oPaises.Item(1).Cod
        m_bPaiRespetarCombo = False
        
        Set m_oPaisSeleccionado = m_oPaises.Item(1)
        m_bPaiCargarComboDesde = False
        If Not m_oPaisSeleccionado.Moneda Is Nothing Then
'            sdbcMonCod.Text = m_oPaisSeleccionado.Moneda.Cod
'            sdbcMonCod_Validate False
        End If

    End If
    
    Set m_oPaises = Nothing
    
    
End Sub


Private Sub sdbcProviDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not m_oPaisSeleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        m_oPaisSeleccionado.CargarTodasLasProvincias , sdbcProviDen.Text, True, , False
    
        bExiste = Not (m_oPaisSeleccionado.Provincias.Count = 0)
        Screen.MousePointer = vbNormal
    Else
    
        bExiste = False
        
    End If
    
        
    If Not bExiste Then
        sdbcProviDen.Text = ""
    Else
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = m_oPaisSeleccionado.Provincias.Item(1).Den
        sdbcProviDen.Columns(1).Value = m_oPaisSeleccionado.Provincias.Item(1).Cod
        sdbcProviDen.Columns(0).Value = m_oPaisSeleccionado.Provincias.Item(1).Den
        
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = False
    End If
    
End Sub

''' Revisado por: blp. Fecha: 24/04/2012
''' <summary>
''' Cargamos los datos de la empresa
''' </summary>
''' <remarks>
''' Llamada desde Load, chkEmpresa_mouseUp, sdbcNIF_closeUp, sdbcNIF_Validate. M�ximo 0,3 seg
''' </remarks>
Private Sub EmpresaSeleccionada()
    Dim oPaises As CPaises
    Dim tsError As TipoErrorSummit
    Dim oDirEnvio As CFacDirEnvio
    
    On Error GoTo Error
    
    If moEmpresaSeleccionada Is Nothing Then
        Exit Sub
    End If
    
    Me.chkEmpresa.Value = vbChecked
    blnEmpresa = True
    LimpiarCampos
    With moEmpresaSeleccionada
        Me.txtCP.Text = NullToStr(.cP)
        Me.txtDenCia.Text = .Den
        Me.txtDIR.Text = NullToStr(.Direccion)
        Me.txtPOB.Text = NullToStr(.Poblacion)
        If Not IsNull(.CodPais) Then
            Set oPaises = oFSGSRaiz.Generar_CPaises
            
            oPaises.CargarTodosLosPaisesDesde 1, .CodPais
            If Not oPaises.Item(.CodPais) Is Nothing Then
                m_bPaiRespetarCombo = True
                Me.sdbcPaiDen.Text = oPaises.Item(.CodPais).Den
                sdbcPaiDen_Validate False
                m_bPaiRespetarCombo = False
                If Not IsNull(.CodProvi) Then
                    oPaises.Item(.CodPais).CargarTodasLasProvinciasDesde 1, .CodProvi
                    If Not oPaises.Item(.CodPais).Provincias.Item(.CodProvi) Is Nothing Then
                        m_bProviRespetarCombo = True
                        Me.sdbcProviDen.Text = oPaises.Item(.CodPais).Provincias.Item(.CodProvi).Den
                        sdbcProviDen_Validate False
                        m_bProviRespetarCombo = False
                    End If
                End If
            End If
        End If
        
        Me.txtRmte.Text = NullToStr(.Remitente)
        Set oPaises = Nothing
        
        'Obtener direcciones de env�o
        If gParametrosGenerales.gbPedidosDirFac Then
            .CargarDireccionesEnvio
            
            'Cargar el grid de direcciones de env�o
            For Each oDirEnvio In .DireccionesEnvio
                sdbgDirecciones.AddItem oDirEnvio.Id & Chr(m_lSeparador) & oDirEnvio.Direccion & Chr(m_lSeparador) & _
                                        oDirEnvio.cP & Chr(m_lSeparador) & oDirEnvio.Poblacion & Chr(m_lSeparador) & _
                                        oDirEnvio.ProviCod & Chr(m_lSeparador) & oDirEnvio.PaiCod & Chr(m_lSeparador) & _
                                        oDirEnvio.PaiDen & Chr(m_lSeparador) & oDirEnvio.ProviDen & Chr(m_lSeparador) & oDirEnvio.FECACT
            Next
        End If
        
        'Obtener datos de facturaci�n de la empresa
        If gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM Then
            .CargarDatosFacturaci�n
            txtNumMaxLineasFra.Text = .FactMaxLineas
        End If
    End With
    
Salir:
    Set oDirEnvio = Nothing
    Exit Sub

Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume Salir
End Sub

Private Sub LimpiarCampos()
    Me.txtCP.Text = ""
    Me.txtDenCia.Text = ""
    Me.txtDIR.Text = ""
    Me.txtPOB.Text = ""
    txtRmte.Text = ""
    m_bPaiRespetarCombo = True
    Me.sdbcPaiDen.Text = ""
    m_bPaiRespetarCombo = False
    m_bProviRespetarCombo = True
    Me.sdbcProviDen.Text = ""
    m_bProviRespetarCombo = False
    
    sdbgDirecciones.RemoveAll
End Sub

Private Function ValidarDatos(ByVal bA�adir As Boolean) As Boolean
            '********* Validar datos *********
    If bA�adir Then
        If Trim(txtCod.Text) = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiCod
            ValidarDatos = False
            If Me.Visible Then txtCod.SetFocus
            Exit Function
        End If
    End If

    If Trim(txtDen.Text) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiDen
        ValidarDatos = False
        If Me.Visible Then txtDen.SetFocus
        Exit Function
    End If
    
    ValidarDatos = True
End Function

Private Sub sdbddDirecciones_Click()
    With sdbgDirecciones
        .Columns("ID").Text = sdbddDirecciones.Columns("ID").Text
        .Columns("DIR").Text = sdbddDirecciones.Columns("DIR").Text
        .Columns("POB").Text = sdbddDirecciones.Columns("POB").Text
        .Columns("CP").Text = sdbddDirecciones.Columns("CP").Text
        .Columns("PROVI").Text = sdbddDirecciones.Columns("PROVI").Text
        .Columns("PROVIDEN").Text = sdbddDirecciones.Columns("PROVIDEN").Text
        .Columns("PAI").Text = sdbddDirecciones.Columns("PAI").Text
        .Columns("PAIDEN").Text = sdbddDirecciones.Columns("PAIDEN").Text
        .Columns("FECACT").Text = sdbddDirecciones.Columns("FECACT").Text
    End With
End Sub

Private Sub sdbddDirecciones_DropDown()
    Dim oFDEs As CFacDirEnvios
    Dim teserror As TipoErrorSummit
    Dim i As Integer
    
    On Error GoTo Error

    With sdbddDirecciones
        .RemoveAll
        
        Set oFDEs = oFSGSRaiz.Generar_CFacDirEnvios
        teserror = oFDEs.CargarTodosLasDirecciones(, , True, True, True, True)
        If teserror.NumError <> TESnoerror Then
            oMensajes.MensajeOKOnly teserror.NumError & " - " & teserror.Arg1 & " - " & teserror.Arg2
            Exit Sub
        End If
        
        If oFDEs.Count > 0 Then
            For i = 1 To oFDEs.Count
                .AddItem oFDEs.Item(i).Id & Chr(m_lSeparador) & oFDEs.Item(i).Direccion & Chr(m_lSeparador) & oFDEs.Item(i).cP & Chr(m_lSeparador) & _
                         oFDEs.Item(i).Poblacion & Chr(m_lSeparador) & oFDEs.Item(i).PaiCod & Chr(m_lSeparador) & oFDEs.Item(i).ProviCod & Chr(m_lSeparador) & _
                         oFDEs.Item(i).ProviDen & Chr(m_lSeparador) & oFDEs.Item(i).PaiDen & Chr(m_lSeparador) & oFDEs.Item(i).FECACT
            Next
        End If
        
        If .Rows = 0 Then .AddItem ""
    End With
    
Salir:
    Set oFDEs = Nothing
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume Salir
End Sub

Private Sub sdbddDirecciones_InitColumnProps()
    sdbddDirecciones.DataFieldList = "Column 1"
    sdbddDirecciones.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddDirecciones_PositionList(ByVal Text As String)
    Dim bm As Variant

    bm = BuscarDir(EliminarCaracteresEspeciales(Text))
    If Not IsEmpty(bm) Then sdbddDirecciones.Bookmark = bm
End Sub

Private Sub sdbddDirecciones_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then
        RtnPassed = True 'Si lo dejan vacio pongo la property a nothing
        
        With sdbgDirecciones
            .Columns("ID").Text = ""
            .Columns("DIR").Text = ""
            .Columns("CP").Text = ""
            .Columns("POB").Text = ""
            .Columns("PAI").Text = ""
            .Columns("PAIDEN").Text = ""
            .Columns("PROVI").Text = ""
            .Columns("PROVIDEN").Text = ""
            .Columns("FECACT").Text = ""
        End With
    End If
End Sub

Private Sub sdbddPaises_Click()
    With sdbgDirecciones
        Select Case .Columns(.col).Name
            Case "PAIDEN"
                .Columns("PAI").Text = sdbddPaises.Columns("COD").Text
                .Columns("PAIDEN").Text = sdbddPaises.Columns("DEN").Text
            Case "PROVIDEN"
                .Columns("PROVI").Text = sdbddPaises.Columns("COD").Text
                .Columns("PROVIDEN").Text = sdbddPaises.Columns("DEN").Text
        End Select
    End With
End Sub

Private Sub sdbddPaises_DropDown()
    With sdbgDirecciones
        Select Case .Columns(.col).Name
            Case "PAIDEN"
                CargarComboPaises
            Case "PROVIDEN"
                CargarComboProvincias
        End Select
              
        .ActiveCell.SelStart = 0
        .ActiveCell.SelLength = Len(.ActiveCell.Text)
    End With
End Sub

''' <summary>Carga el combo sdbddPaises con los pa�ses</summary>
''' <remarks>Llamada desde sdbddPaises_DropDown</remarks>

Private Sub CargarComboPaises()
    Dim oPaises As CPaises
    Dim oPais As CPais

    On Error GoTo Error

    With sdbddPaises
        .RemoveAll
        Set oPaises = oFSGSRaiz.Generar_CPaises
        oPaises.CargarTodosLosPaises , , , True, , False
        
        If oPaises.Count > 0 Then
            For Each oPais In oPaises
                .AddItem oPais.Cod & Chr(m_lSeparador) & oPais.Den
            Next
        End If
        
        If .Rows = 0 Then .AddItem ""
    End With
    
Salir:
    Set oPaises = Nothing
    Set oPais = Nothing
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume Salir
End Sub

''' <summary>Carga el combo sdbddPaises con las provincias</summary>
''' <remarks>Llamada desde sdbddPaises_DropDown</remarks>

Private Sub CargarComboProvincias()
    Dim oPaises As CPaises
    Dim oPais As CPais
    Dim Provis As TipoDatosCombo
    Dim i As Integer

    On Error GoTo Error

    With sdbddPaises
        .RemoveAll
        
        If sdbgDirecciones.Columns("PAIDEN").Text <> vbNullString Then
            Set oPaises = oFSGSRaiz.Generar_CPaises
            oPaises.CargarTodosLosPaises
            Set oPais = oPaises.Item(sdbgDirecciones.Columns("PAI").Text)
        
            oPais.CargarTodasLasProvincias , , , True, False
            Provis = oPais.DevolverLosCodigosDeProvincias
            For i = 0 To UBound(Provis.Cod) - 1
                .AddItem Provis.Cod(i) & Chr(m_lSeparador) & Provis.Den(i)
            Next
                        
            If .Rows = 0 Then .AddItem ""
        End If
    End With
    
Salir:
    Set oPaises = Nothing
    Set oPais = Nothing
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume Salir
End Sub

Private Sub sdbddPaises_InitColumnProps()
    sdbddPaises.DataFieldList = "Column 1"
    sdbddPaises.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddPaises_PositionList(ByVal Text As String)
PositionList sdbddPaises, Text, "DEN"
End Sub

Private Sub sdbddPaises_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then
        RtnPassed = True 'Si lo dejan vacio pongo la property a nothing
        
        With sdbgDirecciones
            Select Case .Columns(.col).Name
                Case "PAIDEN"
                    .Columns("PAI").Text = ""
                    .Columns("PAIDEN").Text = ""
                Case "PROVIDEN"
                    .Columns("PROVI").Text = ""
                    .Columns("PROVIDEN").Text = ""
            End Select
        End With
    Else
        Select Case sdbgDirecciones.Columns(sdbgDirecciones.col).Name
            Case "PAIDEN"
                With sdbddPaises
                    ''' Comprobar la existencia en la lista
                    If .Columns("DEN").Text = Text Then
                        sdbgDirecciones.Columns("PAIDEN").Text = .Columns("DEN").Text
                        sdbgDirecciones.Columns("PAI").Text = .Columns("COD").Text
                        RtnPassed = True
                        Exit Sub
                    End If
                End With
                
                If ValidarPais(Text) Then RtnPassed = True
            Case "PROVIDEN"
                With sdbddPaises
                    ''' Comprobar la existencia en la lista
                    If .Columns("DEN").Text = Text Then
                        sdbgDirecciones.Columns("PROVIDEN").Text = .Columns("DEN").Text
                        sdbgDirecciones.Columns("PROVI").Text = .Columns("COD").Text
                        RtnPassed = True
                        Exit Sub
                    End If
                End With
                
                If ValidarProvincia(sdbgDirecciones.Columns("PAI").Text, Text) Then RtnPassed = True
        End Select
    End If
End Sub

''' <summary>Valida que el pa�s introducido exista</summary>
''' <param name="sText">Denominaci�n del pa�s</param>
''' <returns>True, si lo ha encontrado // false si no lo ha encontrado</returns>
''' <remarks>Llamada desde sdbddPaises_ValidateList</remarks>
Private Function ValidarPais(ByVal sText As String) As Boolean
    Dim oPaises As CPaises
    
    On Error GoTo Error

    Set oPaises = oFSGSRaiz.Generar_CPaises
    oPaises.CargarTodosLosPaises , UCase(sText), , False
    If oPaises.Count > 0 Then
        sdbgDirecciones.Columns("PAI").Value = oPaises.Item(1).Cod
        sdbgDirecciones.Columns("PAIDEN").Value = oPaises.Item(1).Den
        
        ValidarPais = True
    Else
        sdbgDirecciones.Columns("PAI").Value = ""
        sdbgDirecciones.Columns("PAIDEN").Value = ""
        
        ValidarPais = False
    End If
    
Salir:
    Set oPaises = Nothing
    Exit Function
Error:
    ValidarPais = False
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume Salir
End Function

''' <summary>Valida que la provincia introducida exista</summary>
''' <param name="sText">Denominaci�n de la provincia</param>
''' <returns>True, si lo ha encontrado // false si no lo ha encontrado</returns>
''' <remarks>Llamada desde sdbddPaises_ValidateList</remarks>
Private Function ValidarProvincia(ByVal sPais As String, ByVal sText As String) As Boolean
    Dim oPaises As CPaises
    Dim oPais As CPais
    Dim Provis As TipoDatosCombo

    ValidarProvincia = False

    Set oPaises = oFSGSRaiz.Generar_CPaises
    oPaises.CargarTodosLosPaises sPais
    Set oPais = oPaises.Item(sPais)
    oPais.CargarTodasLasProvincias , sText, True
    If oPais.Provincias.Count > 0 Then
        Provis = oPais.DevolverLosCodigosDeProvincias
        sdbgDirecciones.Columns("PROVI").Value = Provis.Cod(0)
        sdbgDirecciones.Columns("PROVIDEN").Value = Provis.Den(0)
        
        ValidarProvincia = True
    Else
        sdbgDirecciones.Columns("PROVI").Value = ""
        sdbgDirecciones.Columns("PROVIDEN").Value = ""
    End If
    
    Set oPaises = Nothing
    Set oPais = Nothing
End Function

Private Sub sdbgDirecciones_AfterDelete(RtnDispErrMsg As Integer)
    Dim oEmpFacDir As CEmpFacDir
    Dim iResp As Integer
    Dim oError As TipoErrorSummit
    
    'Comprobar si la dir. est� asociada a alguna otra empresa
    If m_lIDDirBorrada > 0 Then
        Set oEmpFacDir = oFSGSRaiz.Generar_CEmpFacDir
        oEmpFacDir.Empresa = moEmpresaSeleccionada.Id
        oEmpFacDir.FacDir = m_lIDDirBorrada
        If Not oEmpFacDir.TieneEmpresasAsocidas Then
            If oMensajes.PreguntaEliminarDireccionEnvio = vbYes Then
                oError = oEmpFacDir.EliminarDeBaseDatosTx(True)
                If oError.NumError <> TESnoerror Then
                    basErrores.TratarError oError
                End If
            End If
        End If
        
        m_lIDDirBorrada = 0
    End If
End Sub

Private Sub sdbgDirecciones_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    With sdbgDirecciones
        Select Case .Columns(ColIndex).Name
            Case "PAIDEN"
                .Columns("PROVI").Text = vbNullString
                .Columns("PROVIDEN").Text = vbNullString
            Case "DIR"
                If .Columns(.col).Text = vbNullString Then
                    oMensajes.MensajeOKOnly m_sDireccionObligatoria
                    If sstabDatosEmpresa.Tab <> 1 Then sstabDatosEmpresa.Tab = 1
                    If Me.Visible Then .SetFocus
                    '.col = .col 'Refrescar la posici�n del cursor cuando el grid no estaba activo
                    Cancel = True
                End If
        End Select
    End With
End Sub

Private Sub sdbgDirecciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
    
    If sdbgDirecciones.Columns("ID").Text <> vbNullString Then
        m_lIDDirBorrada = CLng(sdbgDirecciones.Columns("ID").Text)
    Else
        m_lIDDirBorrada = 0
    End If
End Sub

Private Sub sdbgDirecciones_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim sDir As String
    Dim bm As Variant

    If (KeyCode < 112 Or KeyCode > 123) And (KeyCode < 37 Or KeyCode > 40) And KeyCode <> 27 Then
        With sdbgDirecciones
            If .col >= 0 Then
                Select Case .Columns(.col).Name
                    Case "DIR"
                        If Not sdbddDirecciones.DroppedDown Then SendKeys "{F4}"
                        sDir = .Columns("DIR").Text
                        sDir = EliminarCaracteresEspeciales(sDir)
                        bm = BuscarDir(sDir)
                        sdbddDirecciones.Bookmark = bm
                End Select
            End If
        End With
    End If
End Sub

''' <summary>Busca una dir. en el combo de direcciones</summary>
''' <param name="sDir">Cadena con la dir.</param>
''' <returns>El bookmark del elemento encontrado en el combo de direcciones</returns>
''' <remarks>Llamada desde sdbgDirecciones_KeyDown y sdbddDirecciones_PositionList</remarks>

Private Function BuscarDir(ByVal sDir As String) As Variant
    Dim i As Long
    Dim bm As Variant

    With sdbddDirecciones
        .MoveFirst
    
        If sDir <> "" Then
            For i = 0 To .Rows - 1
                bm = .GetBookmark(i)
                If InStr(1, UCase(EliminarCaracteresEspeciales(.Columns("DIR").CellText(bm))), UCase(sDir)) > 0 Then
                    BuscarDir = bm
                    Exit For
                End If
            Next i
        End If
    End With
End Function

Private Sub sdbgDirecciones_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    With sdbgDirecciones
        If .col >= 0 Then
            Select Case .Columns(.col).Name
                Case "DIR"
                    sdbddDirecciones_DropDown
                Case "PAIDEN"
                    .Columns("PROVIDEN").DropDownHwnd = 0
                    sdbddPaises.AddItem ""
                    .Columns("PAIDEN").DropDownHwnd = sdbddPaises.hWnd
                Case "PROVIDEN"
                    .Columns("PAIDEN").DropDownHwnd = 0
                    sdbddPaises.AddItem ""
                    .Columns("PROVIDEN").DropDownHwnd = sdbddPaises.hWnd
            End Select
        End If
    End With
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Carga de los datos de facturaci�n
''' </summary>
''' <remarks>
''' Llamada desde Form_Load. M�x 0,3 seg.
''' </remarks>
Private Sub RellenarDatosFacturacion()
    If gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM Then
        If Not moEmpresaSeleccionada Is Nothing Then
            txtNumMaxLineasFra.Text = moEmpresaSeleccionada.FactMaxLineas
            'Rellenar combo semana del mes
            Dim i As Integer
            For i = 1 To 4
             cboSemanaMes.AddItem sSemanasMes(i)
            Next
            'cboSemanaMes.Refresh
            'Rellenar combo d�as semana
            For i = 1 To 7
                cboDiaSemana.AddItem sDiasSemana(i)
            Next
            'cboDiaSemana.Refresh
            If moEmpresaSeleccionada.FactFecha1DiadelMes = 0 And moEmpresaSeleccionada.FactFecha2DiadelaSemana = 0 Then
                optDia_Enable (False)
                optSemana_Enable (False)
            End If
            If moEmpresaSeleccionada.FactFecha1DiadelMes > 0 Then
                optDia.Value = True
                txtDia.Text = moEmpresaSeleccionada.FactFecha1DiadelMes
                txtPeriodoMeses1.Text = moEmpresaSeleccionada.FactFecha1PeriodoMes
                'Deshabilitamos y vaciamos la opci�n 2
                optDia_Click
            End If
            If moEmpresaSeleccionada.FactFecha2DiadelaSemana > 0 Then
                optSemana.Value = True
                If moEmpresaSeleccionada.FactFecha2SemanadelMes > 0 Then
                    cboSemanaMes.ListIndex = moEmpresaSeleccionada.FactFecha2SemanadelMes - 1 'En FactFecha2SemanadelMes hemos guardado valores del 1 al 4 pero el listindex va de 0 a 3. Hay que restar el valor 1 que le sumamos al guardar
                End If
                If moEmpresaSeleccionada.FactFecha2DiadelaSemana > 0 Then
                    cboDiaSemana.ListIndex = moEmpresaSeleccionada.FactFecha2DiadelaSemana - 1 'En FactFecha2DiadelaSemana hemos guardado valores del 1 al 7 pero el listindex va de 0 a 6. Hay que restar el valor 1 que le sumamos al guardar
                End If
                txtPeriodoMeses2.Text = moEmpresaSeleccionada.FactFecha2PeriodoMes
                'Deshabilitamos y vaciamos la opci�n 1
                optSemana_Click
            End If
            dtpHoraEjecucion.Value = moEmpresaSeleccionada.FactFechaHora
            txtToleranciaImporte.Text = moEmpresaSeleccionada.FactToleranciaImporte
            txtToleranciaPorcentaje.Text = moEmpresaSeleccionada.FactToleranciaPorcentaje
        End If
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Validaci�n del control
''' </summary>
''' <remarks>
''' Llamada desde evento Validate. M�x 0,3 seg.
''' </remarks>
Private Sub txtDia_Validate(Cancel As Boolean)
    Dim iMens As Integer
    If txtDia.Text = "" Then txtDia.Text = 1
    If Not IsNumeric(txtDia.Text) Then
            iMens = 145
        oMensajes.NoValido iMens
        txtDia.Text = 1
        Exit Sub
    ElseIf txtDia.Text < 1 Then
            iMens = 145
        oMensajes.NoValido iMens
        txtDia.Text = 1
        Exit Sub
    Else
        If CInt(txtDia.Text) > 31 Then
            txtDia.Text = 31
        End If
        txtDia.Text = CLng(txtDia.Text)
    End If
End Sub



''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Validaci�n del control
''' </summary>
''' <remarks>
''' Llamada desde evento Validate. M�x 0,3 seg.
''' </remarks>
Private Sub txtPeriodoMeses1_Validate(Cancel As Boolean)
    Dim iMens As Integer
    If (txtPeriodoMeses1.Text = "") Then txtPeriodoMeses1.Text = 1
    If Not IsNumeric(txtPeriodoMeses1.Text) Then
            iMens = 146
        oMensajes.NoValido iMens
        txtPeriodoMeses1.Text = 1
        Exit Sub
    ElseIf txtPeriodoMeses1.Text < 1 Then
            iMens = 146
        oMensajes.NoValido iMens
        txtPeriodoMeses1.Text = 1
        Exit Sub
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Validaci�n del control
''' </summary>
''' <remarks>
''' Llamada desde evento Validate. M�x 0,3 seg.
''' </remarks>
Private Sub txtPeriodoMeses2_Validate(Cancel As Boolean)
    Dim iMens As Integer
    If Not IsNumeric(txtPeriodoMeses2.Text) Then
            iMens = 146
        oMensajes.NoValido iMens
        txtPeriodoMeses2.Text = 1
        Exit Sub
    ElseIf txtPeriodoMeses2.Text < 1 Then
            iMens = 146
        oMensajes.NoValido iMens
        txtPeriodoMeses2.Text = 1
        Exit Sub
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Validaci�n del control
''' </summary>
''' <remarks>
''' Llamada desde evento Validate. M�x 0,3 seg.
''' </remarks>
Private Sub cboSemanaMes_Validate(Cancel As Boolean)
    Dim iMens As Integer
    If cboSemanaMes.Text = "" Then
            iMens = 194
        oMensajes.NoValido iMens
        cboSemanaMes.ListIndex = 0
        Exit Sub
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Validaci�n del control
''' </summary>
''' <remarks>
''' Llamada desde evento Validate. M�x 0,3 seg.
''' </remarks>
Private Sub cboDiaSemana_Validate(Cancel As Boolean)
    Dim iMens As Integer
    If cboDiaSemana.Text = "" Then
            iMens = 195
        oMensajes.NoValido iMens
        cboDiaSemana.ListIndex = 0
        Exit Sub
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Validaci�n del control
''' </summary>
''' <remarks>
''' Llamada desde evento Validate. M�x 0,3 seg.
''' </remarks>
Private Sub txtToleranciaImporte_Validate(Cancel As Boolean)
    Dim iMens As Integer
    If Not IsNumeric(txtToleranciaImporte.Text) Then
            iMens = 196
        oMensajes.NoValido iMens
        txtToleranciaImporte.Text = 0
        Exit Sub
    ElseIf txtToleranciaImporte.Text < 0 Then
            iMens = 196
        oMensajes.NoValido iMens
        txtToleranciaImporte.Text = 0
        Exit Sub
    Else
        txtToleranciaImporte.Text = CDbl(txtToleranciaImporte.Text)
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Validaci�n del control
''' </summary>
''' <remarks>
''' Llamada desde evento Validate. M�x 0,3 seg.
''' </remarks>
Private Sub txtToleranciaPorcentaje_Validate(Cancel As Boolean)
    Dim iMens As Integer
    If Not IsNumeric(txtToleranciaPorcentaje.Text) Then
            iMens = 197
        oMensajes.NoValido iMens
        txtToleranciaPorcentaje.Text = 0
        Exit Sub
    ElseIf txtToleranciaPorcentaje.Text < 0 Then
            iMens = 197
        oMensajes.NoValido iMens
        txtToleranciaPorcentaje.Text = 0
        Exit Sub
    Else
        txtToleranciaPorcentaje.Text = CDbl(txtToleranciaPorcentaje.Text)
    End If
End Sub

''' Revisado por: blp. Fecha: 25/04/2012
''' <summary>
''' Validaci�n del control
''' </summary>
''' <remarks>
''' Llamada desde evento Validate. M�x 0,3 seg.
''' </remarks>
Private Sub txtNumMaxLineasFra_Validate(Cancel As Boolean)
    Dim iMens As Integer
    If Not IsNumeric(txtNumMaxLineasFra.Text) Then
            iMens = 198
        oMensajes.NoValido iMens
        txtNumMaxLineasFra.Text = 1
        Exit Sub
    ElseIf txtNumMaxLineasFra.Text < 0 Then
            iMens = 198
        oMensajes.NoValido iMens
        txtNumMaxLineasFra.Text = 1
        Exit Sub
    Else
        txtNumMaxLineasFra.Text = CLng(txtNumMaxLineasFra.Text)
    End If
End Sub

