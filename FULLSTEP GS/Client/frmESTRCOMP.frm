VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmESTRCOMP 
   Caption         =   "Compradores+"
   ClientHeight    =   4965
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6765
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRCOMP.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4965
   ScaleWidth      =   6765
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMP.frx":014A
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMP.frx":059E
            Key             =   "Comprador"
            Object.Tag             =   "Comprador"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMP.frx":06B2
            Key             =   "Equipo"
            Object.Tag             =   "Equipo"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwEstrComp 
      Height          =   4395
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   7752
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   0
      ScaleHeight     =   450
      ScaleWidth      =   6765
      TabIndex        =   1
      Top             =   4515
      Width           =   6765
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado+"
         Height          =   345
         Left            =   5640
         TabIndex        =   7
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar+"
         Height          =   345
         Left            =   4530
         TabIndex        =   6
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar+"
         Height          =   345
         Left            =   3420
         TabIndex        =   5
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdModif 
         Caption         =   "&Modificar+"
         Height          =   345
         Left            =   1200
         TabIndex        =   3
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdEli 
         Caption         =   "&Eliminar+"
         Height          =   345
         Left            =   2310
         TabIndex        =   4
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir+"
         Height          =   345
         Left            =   90
         TabIndex        =   2
         Top             =   75
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmESTRCOMP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

' Variables para el resize
Private iAltura As Integer
Private iAnchura As Integer

' Variables de restricciones
Private bModif As Boolean
Public bREqp As Boolean
Private bRuo As Boolean
Private bRDep As Boolean

' Variables de la estructura
Public oEquipos As CEquipos

' Variables para interactuar con otros forms
Public oEquipoSeleccionado As CEquipo
Public oCompradorSeleccionado As CComprador
Public oIBaseDatos As IBaseDatos

' Variables para el cambio de c�digo
Public g_bCodigoCancelar As Boolean
Public g_sCodigoNuevo As String

' Variable de control de flujo
Public Accion As accionessummit

' Variable de orden del listado
Public sOrdenarPorDen As Boolean
'MULTILENGUAJE
Private sFrmTitulo(7) As String  'Captions formularios : frmMONCOD , frmESTRCOMDetalle,frmESTRCOMDetalleCom
Private sMensajes(3) As String  'mensajes
Private sRaiz As String          'Nodo raiz : Equipos de compra

Public Sub cmdA�adir_Click()

Dim nodx As Node
    
    Set nodx = tvwEstrComp.selectedItem
    
    NodoSeleccionado nodx
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 3)
        
            Case "Rai"
                        Accion = ACCEqpAnya
                        Screen.MousePointer = vbHourglass
                        Set oEquipoSeleccionado = oFSGSRaiz.generar_CEquipo
                        Screen.MousePointer = vbNormal
                        frmESTRCOMPDetalle.caption = sFrmTitulo(2)  '"A�adir equipo"
                        frmESTRCOMPDetalle.Show 1
            
            Case "EQP"
                        Accion = ACCCompAnya
                        Screen.MousePointer = vbHourglass
                        Set oCompradorSeleccionado = oFSGSRaiz.generar_CComprador
                        Screen.MousePointer = vbNormal
                        oCompradorSeleccionado.codEqp = DevolverCod(nodx)
                        Set oIBaseDatos = oCompradorSeleccionado
                        frmESTRCOMPSelPer.caption = sFrmTitulo(3)
                        frmESTRCOMPSelPer.Show 1
                        
        End Select
    
    End If
            
End Sub

Private Sub cmdBuscar_Click()
    
    frmESTRCOMPBuscar.Show 1
    
End Sub

Public Sub cmdEli_Click()
Dim nodx As Node
Dim oEquipo As CEquipo
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim oPers As CPersonas
Dim adoresPer As Ador.Recordset

Set nodx = tvwEstrComp.selectedItem

NodoSeleccionado nodx

If Not nodx Is Nothing Then
        
Select Case Left(nodx.Tag, 3)
        
    Case "EQP"
            
            Accion = ACCEqpEli
                       
            Set oIBaseDatos = oEquipoSeleccionado
            
            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.IniciarEdicion
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                
                irespuesta = oMensajes.PreguntaEliminar(sMensajes(1) & " " & CStr(oEquipoSeleccionado.Cod) & " (" & oEquipoSeleccionado.Den & ")")
                
                If irespuesta = vbYes Then
                    
                    teserror = oIBaseDatos.FinalizarEdicionEliminando
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                    Else
                       EliminarEquipoDeEstructura
                       RegistrarAccion ACCEqpEli, "Cod:" & CStr(DevolverCod(tvwEstrComp.selectedItem))
                    End If
                    
                End If
            
            End If
        
    Case "COM"
            'Comprobamos si tiene restrccion de UO o Dep que puede ver la persona
            If bRuo Or bRDep Then
                Set oPers = oFSGSRaiz.Generar_CPersonas
                Set adoresPer = oPers.CargarTodasLasPersonas2(oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, oUsuarioSummit.Persona.CodDep, bRuo, bRDep, oCompradorSeleccionado.Cod, , , True)
                If adoresPer.RecordCount = 0 Then
                    adoresPer.Close
                    Set adoresPer = Nothing
                    Set oPers = Nothing
                    oMensajes.PermisoDenegadoComprador bRuo
                    Exit Sub
                End If
                adoresPer.Close
                Set adoresPer = Nothing
                Set oPers = Nothing
            End If
            
            Accion = ACCCompEli
                 
            Set oIBaseDatos = oCompradorSeleccionado
            
            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.IniciarEdicion
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(sMensajes(2) & " " & CStr(oCompradorSeleccionado.Cod) & " (" & oCompradorSeleccionado.Apel & " , " & oCompradorSeleccionado.nombre & ")")
                
                If irespuesta = vbYes Then
                    
                    Screen.MousePointer = vbHourglass
                    'teserror = oIBaseDatos.FinalizarEdicionEliminando
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    Screen.MousePointer = vbNormal
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                    Else
                       EliminarCompradorDeEstructura
                       RegistrarAccion accionessummit.ACCCompEli, "CodEQP:" & CStr(DevolverCod(tvwEstrComp.selectedItem.Parent) & "Cod:" & CStr(DevolverCod(tvwEstrComp.selectedItem)))
                    End If
                
                End If
            Else
                    oMensajes.DatoEliminado 9
            End If
            
    End Select

End If

End Sub


Private Sub cmdListado_Click()
Dim nodx As Node
Dim oEqp As CEquipo
    Set nodx = tvwEstrComp.selectedItem
    If Not nodx Is Nothing Then
        NodoSeleccionado nodx
        If Left(nodx.Tag, 3) = "EQP" Then
            Set oEqp = oEquipoSeleccionado
        End If
    End If
    AbrirLstESTRCOMP sOrdenarPorDen, oEqp, "frmESTRCOMP"
End Sub

Public Sub cmdModif_Click()
Dim nodx As Node
Dim teserror As TipoErrorSummit
Dim oPers As CPersonas
Dim adoresPer As Ador.Recordset

Set nodx = tvwEstrComp.selectedItem

NodoSeleccionado nodx

If Not nodx Is Nothing Then
        
    Select Case Left(nodx.Tag, 3)
        
    Case "EQP"
            
            Accion = ACCEqpMod
            
            Set oIBaseDatos = oEquipoSeleccionado
            
            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.IniciarEdicion
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                frmESTRCOMPDetalle.caption = sFrmTitulo(4) 'Modificar equipo
                frmESTRCOMPDetalle.txtCod = oEquipoSeleccionado.Cod
                frmESTRCOMPDetalle.txtDen = oEquipoSeleccionado.Den
                frmESTRCOMPDetalle.Show 1
            End If
    
    Case "COM"
            
            Accion = ACCCompMod
          
            Set oIBaseDatos = oCompradorSeleccionado
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                 'Comprobamos si tiene restrccion de UO o Dep que puede ver la persona
                If bRuo Or bRDep Then
                    Set oPers = oFSGSRaiz.Generar_CPersonas
                    Set adoresPer = oPers.CargarTodasLasPersonas2(oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, oUsuarioSummit.Persona.CodDep, bRuo, bRDep, oCompradorSeleccionado.Cod, , , True)
                    If adoresPer.RecordCount = 0 Then
                        adoresPer.Close
                        Set adoresPer = Nothing
                        Set oPers = Nothing
                        oMensajes.PermisoDenegadoComprador bRuo
                        Exit Sub
                    End If
                    adoresPer.Close
                    Set adoresPer = Nothing
                    Set oPers = Nothing
                End If
                Screen.MousePointer = vbHourglass
                teserror = oIBaseDatos.IniciarEdicion
                Screen.MousePointer = vbNormal
                
                If teserror.NumError = TESnoerror Then
                    frmESTRCOMPSelPer.caption = sFrmTitulo(5) 'Modificar comprador
                    frmESTRCOMPSelPer.txtPersona = oCompradorSeleccionado.Cod & " - " & oCompradorSeleccionado.Apel & " " & oCompradorSeleccionado.nombre
                    'Se posiciona en esa persona
                    frmESTRCOMPSelPer.Show 1
                End If
            Else
               oMensajes.DatoEliminado 9
            End If
            
    End Select

End If

End Sub

Public Sub cmdRestaurar_Click()

    tvwEstrComp.Nodes.clear
    sOrdenarPorDen = Not MDI.mnuPopUpEstrCompOrdPorCod.Checked
    GenerarEstructuraCompras Not MDI.mnuPopUpEstrCompOrdPorCod.Checked

End Sub

Private Sub Form_Activate()

    Unload frmESTRCOMPDetalle
    Unload frmESTRCOMPDetalleCom

End Sub
Public Sub CambiarCodigo()

    Dim teserror As TipoErrorSummit
    
    Dim nodx As Node
    
    Set nodx = tvwEstrComp.selectedItem
    
    Select Case Left(nodx.Tag, 3)
    
    Case "EQP"
    
        Set oIBaseDatos = Nothing
        Set oEquipoSeleccionado = Nothing
        Set oEquipoSeleccionado = oFSGSRaiz.generar_CEquipo
        oEquipoSeleccionado.Cod = DevolverCod(nodx)
      
        Set oIBaseDatos = oEquipoSeleccionado
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.IniciarEdicion
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            If Me.Visible Then tvwEstrComp.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = sFrmTitulo(6)  'Equipo (C�digo)
        frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodeqp
        frmMODCOD.txtCodAct.Text = oEquipoSeleccionado.Cod
        Set frmMODCOD.fOrigen = frmESTRCOMP
        g_bCodigoCancelar = False
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If g_sCodigoNuevo = "" Then
            oMensajes.NoValido sMensajes(3)
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(oEquipoSeleccionado.Cod) Then
            oMensajes.NoValido sMensajes(3)
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        cmdRestaurar_Click
                                            
    End Select


End Sub

Private Sub Form_Load()

If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If

' MULTIIDIOMA
CargarRecursos

ConfigurarSeguridad

iAltura = 5370
iAnchura = 6885
Me.Width = 6885
Me.Height = 5370

sOrdenarPorDen = False
GenerarEstructuraCompras False

tvwEstrComp_NodeClick tvwEstrComp.Nodes.Item("Raiz")

End Sub

''' <summary>Configuraci�n de la seguridad de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPModificar)) Is Nothing) Then
        bModif = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPRestEquipo)) Is Nothing) Then
        bREqp = True
    End If
    If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Or basOptimizacion.gTipoDeUsuario = Persona) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestUO)) Is Nothing) Then
        bRuo = True
    End If
    If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Or basOptimizacion.gTipoDeUsuario = Persona) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestDep)) Is Nothing) Then
        bRDep = True
    End If

    If Not bModif Then
        cmdA�adir.Visible = False
        cmdModif.Visible = False
        cmdEli.Visible = False
        cmdRestaurar.Left = tvwEstrComp.Left
        cmdBuscar.Left = cmdRestaurar.Left + cmdRestaurar.Width + 100
        cmdListado.Left = cmdBuscar.Left + cmdBuscar.Width + 100
    End If
End Sub
Private Sub GenerarEstructuraCompras(ByVal OrdenadoPorDen As Boolean)

Dim oEquipo As CEquipo
Dim iIndice As Integer
Dim nodx As Node

Set oEquipos = Nothing
Set oEquipos = oFSGSRaiz.generar_cequipos

If bREqp And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
    oEquipos.CargarTodosLosEquipos oUsuarioSummit.comprador.codEqp, , True, OrdenadoPorDen, False
Else
    oEquipos.CargarTodosLosEquipos , , , OrdenadoPorDen, False
End If

Set nodx = tvwEstrComp.Nodes.Add(, , "Raiz", sRaiz, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True
    
For Each oEquipo In oEquipos

    Set nodx = tvwEstrComp.Nodes.Add("Raiz", tvwChild, "EQP" & CStr(oEquipo.Cod), CStr(oEquipo.Cod) & " - " & oEquipo.Den, "Equipo")
    nodx.Tag = "EQP" & CStr(oEquipo.Cod)
    
    GenerarEstructuraEquipo oEquipo, OrdenadoPorDen
    Set oEquipo = Nothing

Next
    
Set oEquipos = Nothing

End Sub

Private Sub GenerarEstructuraEquipo(ByVal oEquipo As CEquipo, ByVal OrdenadoPorDen As Boolean)

Dim oCom As CComprador
Dim oComs As CCompradores
Dim scod As String
Dim nodx As Node

    If oEquipo Is Nothing Then Exit Sub

    oEquipo.CargarTodosLosCompradores , , , , , OrdenadoPorDen, False
    
    For Each oCom In oEquipo.Compradores
        scod = oCom.codEqp & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oCom.codEqp))
        Set nodx = tvwEstrComp.Nodes.Add("EQP" & CStr(oCom.codEqp), tvwChild, "COM" & scod & CStr(oCom.Cod), CStr(oCom.Cod) & " - " & oCom.Apel & " , " & oCom.nombre, "Comprador")
        nodx.Tag = "COM" & CStr(oCom.Cod)
    Next
    
End Sub

Private Sub Form_Resize()
Dim iX As Integer
Dim iY As Integer


    iX = tvwEstrComp.Width + Me.Width - iAnchura
    iY = tvwEstrComp.Height + Me.Height - iAltura

    If iX > 0 And iY > 0 Then
        tvwEstrComp.Width = iX
        tvwEstrComp.Height = iY
        iAltura = Me.Height
        iAnchura = Me.Width
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)

    Unload frmESTRCOMPDetalle
    Unload frmESTRCOMPDetalleCom
    Me.Visible = False
    
End Sub

Private Sub tvwEstrComp_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

Dim nodx As Node

If Button = 2 Then

    Set nodx = tvwEstrComp.selectedItem
    If Not nodx Is Nothing Then
    
        Select Case Left(nodx.Tag, 3)
        
        Case "Rai"
                    PopupMenu MDI.mnuPopUpEstrComp
        
        Case "EQP"
                    PopupMenu MDI.mnuPopUpEstrCompEqp
        
        Case "COM"
                    PopupMenu MDI.mnuPopUpEstrCompCom
        
        End Select
    
    End If

End If
 
End Sub
Private Sub NodoSeleccionado(ByRef nodx As Node)

If Not nodx Is Nothing Then

    Set oEquipoSeleccionado = Nothing
    Set oCompradorSeleccionado = Nothing
    
    Select Case Left(nodx.Tag, 3)
    
    Case "EQP"
            Set oEquipoSeleccionado = oFSGSRaiz.generar_CEquipo
            oEquipoSeleccionado.Cod = DevolverCod(nodx)
            
    Case "COM"
            
            Set oCompradorSeleccionado = oFSGSRaiz.generar_CComprador
            oCompradorSeleccionado.Cod = DevolverCod(nodx)
            oCompradorSeleccionado.codEqp = DevolverCod(nodx.Parent)
            
    End Select

End If

End Sub

''' <summary>Configura las acciones de seguridad del formulario</summary>
''' <param name="nodx">Nodo seleccionado</param>
''' <remarks>Llamada desde: tvwEstrComp_NodeClick</remarks>
''' <revision>LTG 11/06/2013</revision>

Private Sub ConfigurarInterfazSeguridad(ByVal nodx As Node)
    Dim sTag As String
    
    sTag = Left(nodx.Tag, 3)
    
    Select Case sTag
        Case "Rai"
            
            If bModif Then
                If Not bREqp Then
                    MDI.mnuPopUpEstrCompAnyaEqp.Enabled = True
                    cmdA�adir.Enabled = True
                Else
                    MDI.mnuPopUpEstrCompAnyaEqp.Enabled = False
                    cmdA�adir.Enabled = False
                End If
            Else
                MDI.mnuPopUpEstrCompAnyaEqp.Visible = False
                MDI.mnuPopUpEstrCompSep.Visible = False
                cmdA�adir.Enabled = False
            End If
            cmdModif.Enabled = False
            cmdEli.Enabled = False
                    
        Case "EQP"
            
            If bModif Then
                MDI.mnuPopUpEstrCompEliEqp.Enabled = True
                MDI.mnuPopUpEstrCompModEqp.Enabled = True
                MDI.mnuPopUpEstrCompAnyaComp.Enabled = True
                cmdModif.Enabled = True
                cmdA�adir.Enabled = True
                
                If nodx.Children > 0 Then
                    cmdEli.Enabled = False
                Else
                    cmdEli.Enabled = True
                End If
                
            Else
                MDI.mnuPopUpEstrCompEliEqp.Visible = False
                MDI.mnuPopUpEstrCompModEqp.Visible = False
                MDI.mnuPopUpEstrCompAnyaComp.Visible = False
                MDI.mnuPopUpEstrCompEqpSep1.Visible = False
                MDI.mnuPopUpEstrCompEqpSep2.Visible = False
                cmdModif.Enabled = False
                cmdEli.Enabled = False
                cmdA�adir.Enabled = False
            End If
            
        Case "COM"
            
            If bModif Then
                MDI.mnuPopUpEstrCompModCom.Enabled = True
                MDI.mnuPOpUpEstrCompEliCom.Enabled = True
                cmdModif.Enabled = True
                cmdEli.Enabled = True
            Else
                MDI.mnuPopUpEstrCompModCom.Visible = False
                MDI.mnuPOpUpEstrCompEliCom.Visible = False
                MDI.mnuPopUpEstrCompComSep1.Visible = False
                cmdModif.Enabled = False
                cmdEli.Enabled = False
            End If
            cmdA�adir.Enabled = False
    End Select
End Sub

Private Sub tvwEstrComp_NodeClick(ByVal Node As MSComctlLib.Node)
    ConfigurarInterfazSeguridad Node
End Sub

Private Function DevolverCod(ByVal nodx As Node) As Variant

Select Case Left(nodx.Tag, 3)

Case "EQP"
    
        DevolverCod = Right(nodx.Tag, Len(nodx.Tag) - 3)
    
Case "COM"
    
        DevolverCod = Right(nodx.Tag, Len(nodx.Tag) - 3)
    
End Select

End Function
Public Sub AnyadirEquipoAEstructura(ByVal Cod As Variant, Den As String)
Dim nodx As Node
Dim nodo As Node

Set nodx = tvwEstrComp.selectedItem
Set nodo = tvwEstrComp.Nodes.Add(nodx.key, tvwChild, "EQP" & CStr(Cod), CStr(Cod) & " - " & Den, "Equipo")
nodo.Tag = "EQP" & CStr(Cod)
nodx.Expanded = True
'nodx.Selected = True
'nodx.EnsureVisible
Set nodo = Nothing
Set nodx = Nothing

End Sub
Public Function ModificarEquipoEnEstructura(ByVal Den As String)

Dim nodx As Node

Set nodx = tvwEstrComp.selectedItem
nodx.Text = oEquipoSeleccionado.Cod & " - " & Den
Set nodx = Nothing

End Function
Public Function ModificarCompradorEnEstructura()

Dim nodx As Node

Set nodx = tvwEstrComp.selectedItem
nodx.Text = oCompradorSeleccionado.Cod & " - " & oCompradorSeleccionado.Apel & " , " & oCompradorSeleccionado.nombre
nodx.Tag = "COM" & CStr(oCompradorSeleccionado.Cod)
Set nodx = Nothing

End Function

Public Function EliminarEquipoDeEstructura()

Dim nod As MSComctlLib.Node
Dim nodSiguiente As MSComctlLib.Node

Set nod = tvwEstrComp.selectedItem

    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If

    tvwEstrComp.Nodes.Remove nod.key
        
    Set nod = Nothing

    nodSiguiente.Selected = True
    ConfigurarInterfazSeguridad nodSiguiente


End Function
Public Function EliminarCompradorDeEstructura()

Dim nodx As Node
Dim nod As MSComctlLib.Node
Dim nodSiguiente As MSComctlLib.Node

Set nod = tvwEstrComp.selectedItem

    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
        Else
            Set nodSiguiente = nod.Parent
        End If
    End If

    tvwEstrComp.Nodes.Remove nod.key
        
    Set nod = Nothing

    nodSiguiente.Selected = True
    ConfigurarInterfazSeguridad nodSiguiente

End Function

Public Function AnyadirCompradorAEstructura(ByVal Cod As Variant, ByVal nombre As String, ByVal Apel As String)
Dim nodx As Node
Dim nodo As Node
Dim scod As String
Set nodx = tvwEstrComp.selectedItem
scod = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(DevolverCod(nodx)))
Set nodo = tvwEstrComp.Nodes.Add(nodx.key, tvwChild, "COM" & scod & CStr(Cod), CStr(Cod) & " - " & Apel & " , " & nombre, "Comprador")
nodo.Tag = "COM" & CStr(Cod)
'nodo.Selected = True
nodo.EnsureVisible
Set nodo = Nothing
Set nodx = Nothing

End Function
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTR_COMP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        cmdBuscar.caption = Ador(0).Value
        Ador.MoveNext
        cmdEli.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdModif.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        frmESTRCOMP.caption = Ador(0).Value
        Ador.MoveNext
        sFrmTitulo(1) = Ador(0).Value 'Detalle
        Ador.MoveNext
        sFrmTitulo(2) = Ador(0).Value 'A�adir equipo
        Ador.MoveNext
        sFrmTitulo(3) = Ador(0).Value 'A�adir comprador
        Ador.MoveNext
        sFrmTitulo(4) = Ador(0).Value 'Modificar equipo
        Ador.MoveNext
        sFrmTitulo(5) = Ador(0).Value 'Modificar comprador
        Ador.MoveNext
        sFrmTitulo(6) = Ador(0).Value  'Equipo (C�digo)
        Ador.MoveNext
        sFrmTitulo(7) = Ador(0).Value  'Comprador (C�digo)
        Ador.MoveNext
        sRaiz = Ador(0).Value  'Equipos de compra
        Ador.MoveNext
        sMensajes(1) = Ador(0).Value  'el equipo
        Ador.MoveNext
        sMensajes(2) = Ador(0).Value  'el comprador
        Ador.MoveNext
        sMensajes(3) = Ador(0).Value  'C�digo
        'Ador.MoveNext
        'sRegAccion(1) = Ador(0).Value   'Cod:
        'Ador.MoveNext
        'sRegAccion(2) = Ador(0).Value   'CodEQP:
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub
Public Sub cmdDet_Click()
Dim adoresPer As Ador.Recordset
Dim oPers As CPersonas
Dim nodx As Node

Dim teserror As TipoErrorSummit

Set nodx = tvwEstrComp.selectedItem

NodoSeleccionado nodx

If Not nodx Is Nothing Then
        
Select Case Left(nodx.Tag, 3)
        
    Case "EQP"
            
            Accion = ACCEqpDet
           
            oEquipoSeleccionado.Cod = DevolverCod(nodx)
            Set oIBaseDatos = oEquipoSeleccionado
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                
                frmESTRCOMPDetalle.caption = sFrmTitulo(1)  '"Detalle"
                frmESTRCOMPDetalle.txtCod = DevolverCod(nodx)
                frmESTRCOMPDetalle.txtDen = oEquipoSeleccionado.Den
                frmESTRCOMPDetalle.Show 1
                       
            End If
    
    Case "COM"
            oCompradorSeleccionado.Cod = DevolverCod(nodx)
            oCompradorSeleccionado.codEqp = DevolverCod(nodx.Parent)
            
            'Comprobamos si tiene restrccion de UO o Dep que puede ver la persona
            If bRuo Or bRDep Then
                Set oPers = oFSGSRaiz.Generar_CPersonas
                Set adoresPer = oPers.CargarTodasLasPersonas2(oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, oUsuarioSummit.Persona.CodDep, bRuo, bRDep, oCompradorSeleccionado.Cod, , , True)
                If adoresPer.RecordCount = 0 Then
                    adoresPer.Close
                    Set adoresPer = Nothing
                    Set oPers = Nothing
                    oMensajes.PermisoDenegadoComprador bRuo
                    Exit Sub
                End If
                adoresPer.Close
                Set adoresPer = Nothing
                Set oPers = Nothing
            End If
            Me.Accion = ACCCompDet
           
            Set oIBaseDatos = oCompradorSeleccionado
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                
                frmESTRCOMPDetalleCom.caption = sFrmTitulo(1)  '"Detalle"
                
                frmESTRCOMPDetalleCom.UON1 = oCompradorSeleccionado.UON1
                frmESTRCOMPDetalleCom.UON2 = oCompradorSeleccionado.UON2
                frmESTRCOMPDetalleCom.UON3 = oCompradorSeleccionado.UON3
                frmESTRCOMPDetalleCom.Dep = oCompradorSeleccionado.Dep
                frmESTRCOMPDetalleCom.txtCod = oCompradorSeleccionado.Cod
                frmESTRCOMPDetalleCom.txtNom = oCompradorSeleccionado.nombre
                frmESTRCOMPDetalleCom.txtApel = oCompradorSeleccionado.Apel
                frmESTRCOMPDetalleCom.txtTfno = oCompradorSeleccionado.Tfno
                frmESTRCOMPDetalleCom.txtMail = oCompradorSeleccionado.mail
                frmESTRCOMPDetalleCom.txtFax = oCompradorSeleccionado.Fax
                frmESTRCOMPDetalleCom.txtTfno2 = oCompradorSeleccionado.Tfno2
                frmESTRCOMPDetalleCom.txtCargo = oCompradorSeleccionado.Cargo
                    
                frmESTRCOMPDetalleCom.sOrigen = "frmESTRCOMP"
                
                frmESTRCOMPDetalleCom.Show 1
            Else
                oMensajes.DatoEliminado 9
            End If
            
    End Select

End If

End Sub

Public Sub Ordenar(ByVal bOrdPorDen As Boolean)
    tvwEstrComp.Nodes.clear
    sOrdenarPorDen = bOrdPorDen
    GenerarEstructuraCompras bOrdPorDen
End Sub
