VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmVarCALIFICACION 
   Caption         =   "Calificaci�n de las puntuaciones"
   ClientHeight    =   6480
   ClientLeft      =   1575
   ClientTop       =   1485
   ClientWidth     =   9015
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVarCALIFICACION.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6480
   ScaleWidth      =   9015
   Begin VB.PictureBox picCabecera 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   30
      ScaleHeight     =   735
      ScaleWidth      =   8550
      TabIndex        =   8
      Top             =   120
      Width           =   8550
      Begin VB.CheckBox chkMultiidioma 
         Caption         =   "dMostrar denominaciones en todos los idiomas"
         Height          =   192
         Left            =   4320
         TabIndex        =   2
         Top             =   165
         Width           =   3825
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVariables 
         Height          =   285
         Left            =   1260
         TabIndex        =   1
         Top             =   435
         Width           =   2880
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterPos     =   1
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "NIVEL"
         Columns(1).Name =   "NIVEL"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   11695
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "ID1"
         Columns(3).Name =   "ID1"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "ID2"
         Columns(4).Name =   "ID2"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "ID3"
         Columns(5).Name =   "ID3"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "ID4"
         Columns(6).Name =   "ID4"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "ID5"
         Columns(7).Name =   "ID5"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   5080
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPYME 
         Height          =   285
         Left            =   1260
         TabIndex        =   0
         Top             =   120
         Width           =   2880
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1773
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   9922
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   5080
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblPYME 
         AutoSize        =   -1  'True
         Caption         =   "PYME:"
         Height          =   195
         Left            =   225
         TabIndex        =   11
         Top             =   165
         Width           =   450
      End
      Begin VB.Label lblVariable 
         Caption         =   "Variable:"
         Height          =   285
         Left            =   225
         TabIndex        =   9
         Top             =   480
         Width           =   1035
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCalif 
      Height          =   4890
      Left            =   120
      TabIndex        =   3
      Top             =   960
      Width           =   8460
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   4
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVarCALIFICACION.frx":014A
      UseGroups       =   -1  'True
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   2
      Groups(0).Width =   4498
      Groups(0).Caption=   "RANGO"
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   1058
      Groups(0).Columns(0).Visible=   0   'False
      Groups(0).Columns(0).Caption=   "ID"
      Groups(0).Columns(0).Name=   "ID"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   2302
      Groups(0).Columns(1).Caption=   "INF"
      Groups(0).Columns(1).Name=   "INF"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(2).Width=   2196
      Groups(0).Columns(2).Caption=   "SUP"
      Groups(0).Columns(2).Name=   "SUP"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(1).Width =   9446
      Groups(1).Caption=   "CALIF"
      Groups(1).Columns(0).Width=   9446
      Groups(1).Columns(0).Caption=   "NOM"
      Groups(1).Columns(0).Name=   "NOM"
      Groups(1).Columns(0).DataField=   "Column 3"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      _ExtentX        =   14922
      _ExtentY        =   8625
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   585
      Left            =   0
      ScaleHeight     =   585
      ScaleWidth      =   9015
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   5895
      Width           =   9015
      Begin VB.CommandButton cmdAceptar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Aceptar"
         Height          =   345
         Left            =   3150
         TabIndex        =   6
         Top             =   225
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Cancelar"
         Height          =   345
         Left            =   4290
         TabIndex        =   7
         Top             =   225
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModificar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Modificar"
         Height          =   345
         Left            =   165
         TabIndex        =   4
         Top             =   225
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Actualizar"
         Height          =   345
         Left            =   1275
         TabIndex        =   5
         Top             =   225
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmVarCALIFICACION"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_oVarsCalidad As CVariablesCalidad
Private m_oVarSeleccionada As CVariableCalidad
Private m_lIDCalif As Long

Private m_bError As Boolean
Private m_bModoEdicion As Boolean
Private m_sMensaje(0 To 5) As String
Private m_sTotalProveedor As String
Private m_sNombreDen As String
Private m_sIdiomaSPA As String
Private m_oIdiomas As CIdiomas

Private m_oCalifs As CVarCalificaciones

Private Sub Form_Load()
Dim i, iPosition As Byte
Dim oIdioma As CIdioma

    CargarRecursos
    
    PonerFieldSeparator Me
    
    
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(, , False)
 
    i = sdbgCalif.Columns.Count
    iPosition = 3
    
    sdbgCalif.Columns.Add i
    sdbgCalif.Columns(i).Name = gParametrosInstalacion.gIdioma
    sdbgCalif.Columns(i).caption = m_sNombreDen
    sdbgCalif.Columns(i).Position = iPosition

     For Each oIdioma In m_oIdiomas
        If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
            i = i + 1
            iPosition = iPosition + 1
            sdbgCalif.Columns.Add i
            sdbgCalif.Columns(i).Name = oIdioma.Cod
            sdbgCalif.Columns(i).caption = oIdioma.Den
            sdbgCalif.Columns(i).Position = iPosition
            sdbgCalif.Columns(i).Visible = False
        Else
            m_sIdiomaSPA = oIdioma.Den
        End If
    Next
    
    sdbgCalif.Columns.Remove ("NOM")
    
    If Not gParametrosGenerales.gbPymes Then
        CargarVariablesCalidad
        Me.lblPYME.Visible = False
        Me.sdbcPYME.Visible = False
        Me.lblVariable.Top = 165
        Me.sdbcVariables.Top = 120
    End If
    
    Me.Height = 6885
    Me.Width = 8790

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    Set oIdioma = Nothing
End Sub
Private Sub CargarVariablesCalidad(Optional ByVal lIdPyme As Long = 0)
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Carga todas las variable de calidad en la coleccion. Carga tambien la formula del proveedor (Variable Calidad 0)
'                   Se realizan 2 lecturas para obtener una copiar para comprobar si ha habido cambios
'*** Par�metros de entrada: idPyme = Id de la PYME si se esta trabajando en modo PYME
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 1,14seg
'************************************************************
'    Dim inicio As Double, final As Double, tiempoTranscurrido As Double
'    inicio = Timer

    Dim oVarCal0 As CVariableCalidad
    Dim oVarCal1 As CVariableCalidad
    Dim oVarCal2 As CVariableCalidad
    Dim oVarCal3 As CVariableCalidad
    Dim oVarCal4 As CVariableCalidad
    Dim oVarCal5 As CVariableCalidad
    Dim iNumVar As Integer
    
    iNumVar = 0
    
    sdbcVariables.RemoveAll
    
    Set m_oVarSeleccionada = Nothing
    
    Set m_oVarsCalidad = oFSGSRaiz.Generar_CVariablesCalidad
    m_oVarsCalidad.CargarVariables lIdPyme:=lIdPyme, bPuntuacionesVarCal:=True
    
    Set oVarCal0 = m_oVarsCalidad.Item(1)
    Dim IdTotalProveedor As Integer
    Dim DenTotalProveedor As String
    IdTotalProveedor = oVarCal0.Id
    DenTotalProveedor = oVarCal0.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
    sdbcVariables.AddItem "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & oVarCal0.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "", 0
                    
    For Each oVarCal1 In oVarCal0.VariblesCal
        sdbcVariables.AddItem oVarCal1.Id & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "    " & oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oVarCal0.Id & Chr(m_lSeparador) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        If Not oVarCal1.VariblesCal Is Nothing Then
            For Each oVarCal2 In oVarCal1.VariblesCal
                sdbcVariables.AddItem oVarCal2.Id & Chr(m_lSeparador) & "2" & Chr(m_lSeparador) & "        " & oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oVarCal0.Id & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                If Not oVarCal2.VariblesCal Is Nothing Then
                    For Each oVarCal3 In oVarCal2.VariblesCal
                        sdbcVariables.AddItem oVarCal3.Id & Chr(m_lSeparador) & "3" & Chr(m_lSeparador) & "               " & oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oVarCal0.Id & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                        If Not oVarCal3.VariblesCal Is Nothing Then
                            For Each oVarCal4 In oVarCal3.VariblesCal
                                sdbcVariables.AddItem oVarCal4.Id & Chr(m_lSeparador) & "4" & Chr(m_lSeparador) & "                   " & oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oVarCal0.Id & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & oVarCal3.Id & Chr(m_lSeparador) & ""
                                If Not oVarCal4.VariblesCal Is Nothing Then
                                    For Each oVarCal5 In oVarCal4.VariblesCal
                                        sdbcVariables.AddItem oVarCal5.Id & Chr(m_lSeparador) & "5" & Chr(m_lSeparador) & "                       " & oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oVarCal0.Id & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & oVarCal3.Id & Chr(m_lSeparador) & oVarCal4.Id
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Next
        End If
    Next
    
    
    If Not m_oVarSeleccionada Is Nothing Then
        sdbcVariables.Columns("ID").Value = m_oVarSeleccionada.Id
        sdbcVariables.Columns("NIVEL").Value = m_oVarSeleccionada.Nivel
        sdbcVariables.Columns("ID1").Value = NullToStr(m_oVarSeleccionada.IdVarCal1)
        sdbcVariables.Columns("ID2").Value = NullToStr(m_oVarSeleccionada.IdVarCal2)
        sdbcVariables.Columns("ID3").Value = NullToStr(m_oVarSeleccionada.IdVarCal3)
        sdbcVariables.Columns("ID4").Value = NullToStr(m_oVarSeleccionada.IdVarCal4)
        sdbcVariables.Columns("DEN").Value = m_oVarSeleccionada.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcVariables.Value = m_oVarSeleccionada.Denominaciones.Item(gParametrosGenerales.gIdioma).Den
        
    Else
        sdbcVariables.Columns("ID").Value = IdTotalProveedor
        sdbcVariables.Columns("NIVEL").Value = "0"
        sdbcVariables.Columns("ID1").Value = ""
        sdbcVariables.Columns("ID2").Value = ""
        sdbcVariables.Columns("ID3").Value = ""
        sdbcVariables.Columns("ID4").Value = ""
        sdbcVariables.Columns("DEN").Value = DenTotalProveedor
        sdbcVariables.Value = DenTotalProveedor
        
    End If
    
    VariableSeleccionada

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIFICACIONES, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value   '1
        Ador.MoveNext
        lblVariable.caption = Ador(0).Value '2 Variable:
        Ador.MoveNext
        sdbgCalif.Groups("RANGO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCalif.Columns("INF").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCalif.Columns("SUP").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCalif.Groups("CALIF").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCalif.Columns("NOM").caption = Ador(0).Value
        m_sNombreDen = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value '9
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '10
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(0) = Ador(0).Value 'El intervalo ya existe
        Ador.MoveNext
        m_sMensaje(1) = Ador(0).Value 'el campo desde debe ser inferior al campo hasta
        Ador.MoveNext
        m_sMensaje(2) = Ador(0).Value 'debe ser num�rico
        Ador.MoveNext
        m_sMensaje(3) = Ador(0).Value 'el valor ya existe
        Ador.MoveNext
        m_sMensaje(4) = Ador(0).Value 'El intervalo introducido contiene uno de los ya existentes
        Ador.MoveNext
        m_sMensaje(5) = Ador(0).Value 'Modifique el �ltimo intervalo introducido
        Ador.MoveNext
        m_sTotalProveedor = Ador(0).Value '21
        Ador.MoveNext
        Ador.MoveNext
        lblPYME.caption = Ador(0).Value
        Ador.MoveNext
        chkMultiidioma.caption = Ador(0).Value
        Ador.Close
    End If

    Set Ador = Nothing

End Sub


Private Sub Form_Resize()
Dim iAlturaCombos As Integer

On Error Resume Next

If Me.Width < 4000 Then Exit Sub
If Me.Height < 4000 Then Exit Sub

If gParametrosGenerales.gbPymes Then
    iAlturaCombos = 1850
Else
    iAlturaCombos = 1850
    sdbgCalif.Top = 850
    
End If

sdbgCalif.Height = Me.Height - iAlturaCombos
sdbgCalif.Width = Me.Width - 330
cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 100
cmdCancelar.Left = Me.Width / 2 + 50
sdbgCalif.Groups(0).Width = (sdbgCalif.Width / 3)
sdbgCalif.Groups(1).Width = (sdbgCalif.Width / 3) * 2 - 600
sdbgCalif.Columns("INF").Width = (sdbgCalif.Width / 3) / 2
sdbgCalif.Columns("SUP").Width = sdbgCalif.Columns("INF").Width
sdbgCalif.Columns("NOM").Width = (sdbgCalif.Width / 3) * 2 - 600
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Me.Visible = False
End Sub



Private Sub cmdModificar_Click()
        
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta

    
        
    sdbcVariables.Enabled = False
    
    sdbgCalif.AllowAddNew = True
    sdbgCalif.AllowUpdate = True
    sdbgCalif.AllowDelete = True
            
    cmdModificar.Visible = False
    cmdRestaurar.Visible = False
    cmdAceptar.Visible = True
    cmdCancelar.Visible = True
        
    m_bModoEdicion = True
    
    
End Sub

Private Sub cmdCancelar_Click()
                    
                    
    ModoConsulta
                    
    VariableSeleccionada

End Sub

Private Sub ModoConsulta()

    sdbcVariables.Enabled = True
    
    sdbgCalif.AllowAddNew = False
    sdbgCalif.AllowUpdate = False
    sdbgCalif.AllowDelete = False
            
    cmdModificar.Visible = True
    cmdRestaurar.Visible = True
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
        
    m_bModoEdicion = False
    
End Sub

Private Sub cmdAceptar_Click()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Proceso que guarda las puntuaciones / Denominaciones de la variable de calidad seleccionada en la combo
'***                Si ha modificado/eliminado/dado de alta una calificacion que ya estaba en la tabla de puntuaciones en curso, recalculara las puntuaciones
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,9seg
'************************************************************



Dim oCalificaciones As CVarCalificaciones
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim vbm As Variant
Dim lID As Long
Dim iNivel As Integer
Dim lIdPyme As Long
Dim rs As Recordset
Dim numEnCurso As Long


Dim k As Integer

Dim oDenominaciones As CMultiidiomas
Dim sCod, sDen As String



    If sdbgCalif.DataChanged = True Then
        sdbgCalif.Update
        If m_bError Then
            Exit Sub
        End If
    End If
    
    lIdPyme = 0
    If gParametrosGenerales.gbPymes Then
        lIdPyme = Me.sdbcPYME.Columns("ID").Value
    
    End If
    
        
    If m_oVarSeleccionada Is Nothing Then
        lID = 0
        iNivel = 0
        Set m_oVarSeleccionada = oFSGSRaiz.Generar_CVariableCalidad
        If lIdPyme > 0 Then
            lID = m_oVarSeleccionada.devolverIDNivel0(lIdPyme)
            Set m_oVarSeleccionada = Nothing
        End If
            
    Else
        lID = m_oVarSeleccionada.Id
        iNivel = m_oVarSeleccionada.Nivel
    End If
    
    Set oCalificaciones = oFSGSRaiz.Generar_CVarCalificaciones

    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas


    For i = 0 To sdbgCalif.Rows - 1
        vbm = sdbgCalif.AddItemBookmark(i)
        Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
        For k = 1 To m_oIdiomas.Count
            sCod = m_oIdiomas.Item(k).Cod
            sDen = sdbgCalif.Columns(sCod).CellValue(vbm)
            oDenominaciones.Add sCod, sDen
        
        
        Next
        oCalificaciones.Add lID, iNivel, i + 1, oDenominaciones, sdbgCalif.Columns("INF").CellValue(vbm), sdbgCalif.Columns("SUP").CellValue(vbm)
    Next
    
    
    Set rs = m_oVarSeleccionada.ExistenPuntuaciones
    If Not rs.EOF Then
        numEnCurso = rs(0).Value
    End If
    
    teserror = oCalificaciones.GuardarCalificaciones(lID, iNivel, lIdPyme)
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
    Else
        RegistrarAccion ACCCalifMod, "Var: " & CStr(lID) & " Nivel: " & CStr(iNivel)
        If Not m_oVarSeleccionada Is Nothing Then
            Set m_oVarSeleccionada.Calificaciones = oCalificaciones
        End If
        ModoConsulta
    End If

    If numEnCurso > 0 Then
        'La variable tiene puntuaciones en curso, por lo que habr� que recalcular las puntuaciones
        Dim oWebSvc As FSGSLibrary.CWebService
        Set oWebSvc = New FSGSLibrary.CWebService
        oWebSvc.LlamarWebServiceQA gParametrosGenerales, oUsuarioSummit.Cod, oUsuarioSummit.Pwd
        Set oWebSvc = Nothing
    End If
    
    Set oCalificaciones = Nothing
    Set oDenominaciones = Nothing
End Sub

Private Sub cmdRestaurar_Click()
    '''Objetivo : Restaurar los datos
    
    VariableSeleccionada
    
    CargarVariablesCalidad
End Sub
Private Sub sdbcVariables_Click()
    
    If sdbcVariables.DroppedDown Then
        Exit Sub
    End If
    
End Sub

Private Sub sdbcVariables_CloseUp()
    If sdbcVariables.Value = "" Then Exit Sub
    
    VariableSeleccionada
    
    
End Sub

Private Sub sdbcVariables_InitColumnProps()
    
    sdbcVariables.DataFieldList = "Column 2"
    sdbcVariables.DataFieldToDisplay = "Column 2"
    
End Sub

Private Sub sdbcVariables_PositionList(ByVal Text As String)
     ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcVariables.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVariables.Rows - 1
            bm = sdbcVariables.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVariables.Columns(2).CellText(bm), 1, Len(Text))) Then
                sdbcVariables.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub VariableSeleccionada()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Carga la puntuacion de la variable seleccionada
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,03seg
'************************************************************

'    Dim inicio As Double, final As Double, tiempoTranscurrido As Double
'    inicio = Timer


Dim oCalif As CVarCalificacion
Dim sValor As String
Dim i As Integer
Dim lIdPyme As Long

lIdPyme = 0
If gParametrosGenerales.gbPymes Then
    If Me.sdbcPYME.Value <> "" Then
        lIdPyme = sdbcPYME.Columns("ID").Value
    End If
End If

    
    Set m_oVarSeleccionada = Nothing
    
    sdbgCalif.RemoveAll
    
    Select Case sdbcVariables.Columns("NIVEL").Value
    Case 0
        Set m_oVarSeleccionada = m_oVarsCalidad.Item(CStr("0" & CStr(sdbcVariables.Columns("ID").Value)))
    Case 1
        Set m_oVarSeleccionada = m_oVarsCalidad.Item(CStr("0" & CStr(sdbcVariables.Columns("ID1").Value))).VariblesCal.Item(CStr("1" & CStr(sdbcVariables.Columns("ID").Value)))
    Case 2
        Set m_oVarSeleccionada = m_oVarsCalidad.Item(CStr("0" & CStr(sdbcVariables.Columns("ID1").Value))).VariblesCal.Item(CStr("1" & CStr(sdbcVariables.Columns("ID2").Value))).VariblesCal.Item(CStr("2" & CStr(sdbcVariables.Columns("ID").Value)))
    Case 3
        Set m_oVarSeleccionada = m_oVarsCalidad.Item(CStr("0" & CStr(sdbcVariables.Columns("ID1").Value))).VariblesCal.Item(CStr("1" & CStr(sdbcVariables.Columns("ID2").Value))).VariblesCal.Item(CStr("2" & CStr(sdbcVariables.Columns("ID3").Value))).VariblesCal.Item(CStr("3" & CStr(sdbcVariables.Columns("ID").Value)))
    Case 4
        Set m_oVarSeleccionada = m_oVarsCalidad.Item(CStr("0" & CStr(sdbcVariables.Columns("ID1").Value))).VariblesCal.Item(CStr("1" & CStr(sdbcVariables.Columns("ID2").Value))).VariblesCal.Item(CStr("2" & CStr(sdbcVariables.Columns("ID3").Value))).VariblesCal.Item(CStr("3" & CStr(sdbcVariables.Columns("ID4").Value))).VariblesCal.Item(CStr("4" & CStr(sdbcVariables.Columns("ID").Value)))
    Case 5
        Set m_oVarSeleccionada = m_oVarsCalidad.Item(CStr("0" & CStr(sdbcVariables.Columns("ID1").Value))).VariblesCal.Item(CStr("1" & CStr(sdbcVariables.Columns("ID2").Value))).VariblesCal.Item(CStr("2" & CStr(sdbcVariables.Columns("ID3").Value))).VariblesCal.Item(CStr("3" & CStr(sdbcVariables.Columns("ID4").Value))).VariblesCal.Item(CStr("4" & CStr(sdbcVariables.Columns("ID5").Value))).VariblesCal.Item(CStr("5" & CStr(sdbcVariables.Columns("ID").Value)))
    End Select
    
    If Not m_oVarSeleccionada Is Nothing Then
                    
        Set m_oCalifs = m_oVarSeleccionada.CargarCalificacion(lIdPyme)
    Else
        Set m_oCalifs = oFSGSRaiz.Generar_CVarCalificaciones
        m_oCalifs.CargarCalificacionTotalProve lIdPyme
            
    End If
    
    m_lIDCalif = 0

    If Not m_oCalifs Is Nothing Then
        For Each oCalif In m_oCalifs
            If Me.chkMultiidioma.Value = vbChecked Then
                sValor = oCalif.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                For i = 1 To oCalif.Denominaciones.Count
                    If UCase(oCalif.Denominaciones.Item(i).Cod) <> gParametrosInstalacion.gIdioma And oCalif.Denominaciones.Item(i).Den <> "" Then
                        sValor = sValor & Chr(m_lSeparador) & oCalif.Denominaciones.Item(i).Den
                    End If
                Next
            Else
                sValor = oCalif.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            End If
            sdbgCalif.AddItem oCalif.Id & Chr(m_lSeparador) & oCalif.ValorInf & Chr(m_lSeparador) & oCalif.ValorSup & Chr(m_lSeparador) & sValor
            m_lIDCalif = oCalif.Id
        Next
    End If
    
    Set oCalif = Nothing
    
'    final = Timer
'    tiempoTranscurrido = (final - inicio)
End Sub

Private Sub sdbgCalif_BeforeUpdate(Cancel As Integer)

Dim vValorDesde As Variant
Dim vValorHasta As Variant
Dim irespuesta As Integer
Dim i As Integer

    m_bError = False
    
    If Not IsNumeric(sdbgCalif.Columns("INF").Text) Then
        oMensajes.NoValido sdbgCalif.Columns("INF").caption & " " & m_sMensaje(2)
        If Me.Visible Then sdbgCalif.SetFocus
        Cancel = True
        m_bError = True
    Else
        If Not IsNumeric(sdbgCalif.Columns("SUP").Text) Then
            oMensajes.NoValido sdbgCalif.Columns("INF").caption & " " & m_sMensaje(2)
            If Me.Visible Then sdbgCalif.SetFocus
            Cancel = True
            m_bError = True
        Else
            If sdbgCalif.Columns(gParametrosInstalacion.gIdioma).Value = "" Then
                oMensajes.NoValido sdbgCalif.Groups(1).caption
                If Me.Visible Then sdbgCalif.SetFocus
                Cancel = True
                m_bError = True
            End If
        End If
    End If
        
    If Not m_bError Then
        If VarToDec0(sdbgCalif.Columns("INF").Text) > VarToDec0(sdbgCalif.Columns("SUP").Text) Then
            oMensajes.NoValida m_sMensaje(1)
            If Me.Visible Then sdbgCalif.SetFocus
            Cancel = True
            m_bError = True
        End If
        If Not m_bError Then
            vValorDesde = VarToDec0(sdbgCalif.Columns("INF").Text)
            vValorHasta = VarToDec0(sdbgCalif.Columns("SUP").Text)
            For i = 0 To sdbgCalif.Rows - 1
                If sdbgCalif.AddItemRowIndex(sdbgCalif.Bookmark) <> i Then
                    If VarToDec0(vValorDesde) >= VarToDec0(sdbgCalif.Columns("INF").CellValue(sdbgCalif.AddItemBookmark(i))) And VarToDec0(vValorDesde) <= VarToDec0(sdbgCalif.Columns("SUP").CellValue(sdbgCalif.AddItemBookmark(i))) Then
                        'el intervalo definido ya existe
                        oMensajes.NoValido m_sMensaje(0)
                        If Me.Visible Then sdbgCalif.SetFocus
                        Cancel = True
                        m_bError = True
                        Exit Sub
                    End If
                    If VarToDec0(vValorHasta) >= VarToDec0(sdbgCalif.Columns("INF").CellValue(sdbgCalif.AddItemBookmark(i))) And VarToDec0(vValorHasta) <= VarToDec0(sdbgCalif.Columns("SUP").CellValue(sdbgCalif.AddItemBookmark(i))) Then
                        'el intervalo definido ya existe
                        oMensajes.NoValida m_sMensaje(0)
                        If Me.Visible Then sdbgCalif.SetFocus
                        Cancel = True
                        m_bError = True
                        Exit Sub
                    End If
                    If VarToDec0(vValorDesde) < VarToDec0(sdbgCalif.Columns("INF").CellValue(sdbgCalif.AddItemBookmark(i))) And VarToDec0(vValorHasta) > VarToDec0(sdbgCalif.Columns("SUP").CellValue(sdbgCalif.AddItemBookmark(i))) Then
                        'el intervalo introducido contiene uno de los ya existe
                        irespuesta = oMensajes.PreguntaEliminarIntervalo(m_sMensaje(4) & ": " & vbCrLf & sdbgCalif.Columns("INF").CellValue(sdbgCalif.AddItemBookmark(i)) & " - " & sdbgCalif.Columns("SUP").CellValue(sdbgCalif.AddItemBookmark(i)))
                        If irespuesta = vbNo Then
                            MsgBox m_sMensaje(5), vbInformation, "FULLSTEP"
                            If Me.Visible Then sdbgCalif.SetFocus
                            Cancel = True
                            m_bError = True
                            Exit Sub
                        Else
                            sdbgCalif.RemoveItem i
                            Exit Sub
                        End If
                    End If
                End If
            Next
        End If
    End If

End Sub
Private Sub sdbgCalif_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
End Sub

Private Sub sdbgCalif_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
 DispPromptMsg = 0
End Sub

Private Sub chkMultiidioma_Click()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Muestra/Oculta los idiomas que hay en la aplicacion en la grid de Puntuaciones de calidad
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,01seg
'************************************************************

'    Dim inicio As Double, final As Double, tiempoTranscurrido As Double
'    inicio = Timer

Dim i As Byte
Dim oCalif As CVarCalificacion
Dim sValor As String
    
    sdbgCalif.RemoveAll

    If chkMultiidioma.Value = vbChecked Then
        sdbgCalif.Columns(gParametrosInstalacion.gIdioma).caption = m_sIdiomaSPA
    Else
        sdbgCalif.Columns(gParametrosInstalacion.gIdioma).caption = m_sNombreDen
    End If
    
    For i = 1 To m_oIdiomas.Count
        If m_oIdiomas.Item(i).Cod <> gParametrosInstalacion.gIdioma Then
            sdbgCalif.Columns(m_oIdiomas.Item(i).Cod).Visible = Me.chkMultiidioma.Value
            
        Else
            sdbgCalif.Columns(gParametrosInstalacion.gIdioma).Visible = True
            
        End If
        If Me.chkMultiidioma.Value = vbChecked Then
            sdbgCalif.Columns(m_oIdiomas.Item(i).Cod).Width = (sdbgCalif.Groups(1).Width / 3) + 10
        End If
    Next
    
    m_lIDCalif = 0
    sValor = ""
    If Not m_oCalifs Is Nothing Then
        For Each oCalif In m_oCalifs
            If Me.chkMultiidioma.Value = vbChecked Then
                sValor = oCalif.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                For i = 1 To oCalif.Denominaciones.Count
                    If UCase(oCalif.Denominaciones.Item(i).Cod) <> gParametrosInstalacion.gIdioma And oCalif.Denominaciones.Item(i).Den <> "" Then
                        sValor = sValor & Chr(m_lSeparador) & oCalif.Denominaciones.Item(i).Den
                        sdbgCalif.Columns(oCalif.Denominaciones.Item(i).Cod).Width = sdbgCalif.Groups(1).Width / oCalif.Denominaciones.Count
                    End If
                Next
            Else
                sValor = oCalif.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            End If
            
            sdbgCalif.AddItem oCalif.Id & Chr(m_lSeparador) & oCalif.ValorInf & Chr(m_lSeparador) & oCalif.ValorSup & Chr(m_lSeparador) & sValor
            m_lIDCalif = oCalif.Id
        Next
    End If
    
    Set oCalif = Nothing
    
'    final = Timer
'    tiempoTranscurrido = (final - inicio)

    
End Sub


Private Sub sdbgCalif_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgCalif.IsAddRow Then
        Me.chkMultiidioma.Value = vbChecked
    End If
End Sub

Private Sub sdbcPYME_CloseUp()
    If sdbcPYME.Value = "" Then Exit Sub
    sdbcVariables.Value = ""
    CargarVariablesCalidad sdbcPYME.Columns("ID").Value
End Sub

Private Sub sdbcPYME_DropDown()

'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Carga la combo con todas las pymes que hay en la aplicacion
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,1seg
'************************************************************

'    Dim inicio As Double, final As Double, tiempoTranscurrido As Double
'    inicio = Timer


Dim oPymes As cPymes
Dim oPyme As cPyme

    Screen.MousePointer = vbHourglass
    
    sdbcPYME.RemoveAll
    Set oPymes = oFSGSRaiz.Generar_CPymes
    oPymes.CargarTodasLasPymes , True
    For Each oPyme In oPymes
        sdbcPYME.AddItem oPyme.Id & Chr(m_lSeparador) & oPyme.Cod & Chr(m_lSeparador) & oPyme.Den
    Next
    
    Set oPymes = Nothing
    Set oPyme = Nothing
    Screen.MousePointer = vbNormal
    
'    final = Timer
'    tiempoTranscurrido = (final - inicio)
End Sub

Private Sub sdbcPYME_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcPYME.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPYME.Rows - 1
            bm = sdbcPYME.GetBookmark(i)
            If UCase(Text) = UCase(sdbcPYME.Columns("DEN").CellText(bm)) Then
                sdbcPYME.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcPYME_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyBack Then
        sdbcPYME.Value = ""
        sdbcVariables.Value = ""
        sdbcVariables.RemoveAll
        Me.sdbgCalif.RemoveAll
    End If
End Sub

Private Sub sdbcPYME_InitColumnProps()
    sdbcPYME.DataFieldList = "Column 0"
    sdbcPYME.DataFieldToDisplay = "Column 2"
End Sub
