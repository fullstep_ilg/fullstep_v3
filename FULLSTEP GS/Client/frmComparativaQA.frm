VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmComparativaQA 
   Caption         =   "DComparativa de calidad"
   ClientHeight    =   4605
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12015
   Icon            =   "frmComparativaQA.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4605
   ScaleWidth      =   12015
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer1 
      Left            =   0
      Top             =   600
   End
   Begin VB.PictureBox picNoVisibles 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      ForeColor       =   &H80000008&
      Height          =   585
      Left            =   960
      ScaleHeight     =   555
      ScaleWidth      =   9015
      TabIndex        =   7
      Top             =   960
      Visible         =   0   'False
      Width           =   9045
      Begin VB.Label lblNoVisibles 
         BackColor       =   &H80000018&
         Caption         =   "Existen proveedores que no cumplen los umbrales de calidad para las siguientes variables no visibles:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   0
         Width           =   8985
      End
   End
   Begin VB.PictureBox picUnqa_GridPv 
      Height          =   1575
      Left            =   720
      ScaleHeight     =   1515
      ScaleWidth      =   3675
      TabIndex        =   43
      Top             =   2880
      Visible         =   0   'False
      Width           =   3735
      Begin VB.CheckBox chkTodasUnidades_GridPv 
         Caption         =   "dChequear todas las unidades de negocio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   120
         Width           =   3375
      End
      Begin MSComctlLib.TreeView tvwUndNegUsu_GridPv 
         Height          =   975
         Left            =   120
         TabIndex        =   45
         Top             =   480
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   1720
         _Version        =   393217
         HideSelection   =   0   'False
         Style           =   7
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frameVista 
      Height          =   615
      Left            =   120
      TabIndex        =   29
      Top             =   0
      Width           =   11775
      Begin VB.CommandButton cmdNoVisibles 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5520
         Picture         =   "frmComparativaQA.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdVistaDefecto 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10680
         Picture         =   "frmComparativaQA.frx":0296
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdGuardarVista 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9600
         Picture         =   "frmComparativaQA.frx":061F
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdGuardarVistaNueva 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9240
         Picture         =   "frmComparativaQA.frx":06A0
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdEliminarVista 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10320
         Picture         =   "frmComparativaQA.frx":0A70
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdRenombrarVista 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9960
         Picture         =   "frmComparativaQA.frx":0E2F
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdExcel 
         Height          =   285
         Left            =   11400
         Picture         =   "frmComparativaQA.frx":11EA
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdInvertir 
         Height          =   285
         Left            =   11040
         Picture         =   "frmComparativaQA.frx":16FC
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   240
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaActual 
         Height          =   255
         Left            =   840
         TabIndex        =   31
         Top             =   240
         Width           =   4590
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   3
         Columns(0).Width=   8096
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8096
         _ExtentY        =   450
         _StockProps     =   93
         Text            =   "Vista 1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblVista 
         Caption         =   "DVista:"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.PictureBox picProves 
      Height          =   2415
      Left            =   4680
      ScaleHeight     =   2355
      ScaleWidth      =   3675
      TabIndex        =   27
      Top             =   1800
      Visible         =   0   'False
      Width           =   3735
      Begin VB.CheckBox chkTodosProves 
         Caption         =   "dChequear todos los proveedores"
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   120
         Value           =   1  'Checked
         Width           =   3375
      End
      Begin VB.CheckBox chkVerPuntos 
         Caption         =   "dMostrar Puntos"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   840
         Value           =   1  'Checked
         Width           =   3495
      End
      Begin VB.CheckBox chkVerCalificacion 
         Caption         =   "dMostrar Calificaciones"
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   480
         Value           =   1  'Checked
         Width           =   3495
      End
      Begin MSComctlLib.TreeView tvwProves 
         Height          =   735
         Left            =   120
         TabIndex        =   28
         Top             =   1200
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   1296
         _Version        =   393217
         HideSelection   =   0   'False
         Style           =   7
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAlmenosUno 
         Caption         =   "dAl menos un proveedor debe ser visible"
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   120
         TabIndex        =   46
         Top             =   2040
         Width           =   3495
      End
   End
   Begin VB.PictureBox picVarCal 
      Height          =   1575
      Left            =   5760
      ScaleHeight     =   1515
      ScaleWidth      =   3675
      TabIndex        =   13
      Top             =   600
      Visible         =   0   'False
      Width           =   3735
      Begin VB.CheckBox chkTodasVariables 
         Caption         =   "dChequear todas las variables"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   120
         Width           =   3375
      End
      Begin VB.CheckBox chkMostrarVarsMaterialProceso 
         Caption         =   "DSolo variables del material del proceso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   480
         Width           =   3375
      End
      Begin MSComctlLib.TreeView tvwVarCal 
         Height          =   615
         Left            =   120
         TabIndex        =   15
         Top             =   840
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   1085
         _Version        =   393217
         HideSelection   =   0   'False
         Style           =   7
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picUnqa 
      Height          =   1575
      Left            =   720
      ScaleHeight     =   1515
      ScaleWidth      =   3675
      TabIndex        =   10
      Top             =   1080
      Visible         =   0   'False
      Width           =   3735
      Begin VB.CheckBox chkTodasUnidades 
         Caption         =   "dChequear todas las unidades de negocio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   120
         Width           =   3375
      End
      Begin MSComctlLib.TreeView tvwUndNegUsu 
         Height          =   975
         Left            =   120
         TabIndex        =   12
         Top             =   480
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   1720
         _Version        =   393217
         HideSelection   =   0   'False
         Style           =   7
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picUmbrales 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      ForeColor       =   &H80000008&
      Height          =   2730
      Left            =   4800
      ScaleHeight     =   2700
      ScaleWidth      =   6345
      TabIndex        =   3
      Top             =   1245
      Visible         =   0   'False
      Width           =   6375
      Begin VB.Label lblMenorUmbral 
         BackColor       =   &H80000018&
         Caption         =   "menor o igual que -25,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   360
         TabIndex        =   25
         Top             =   2280
         Width           =   4080
      End
      Begin VB.Label lblMayorUmbral 
         BackColor       =   &H80000018&
         Caption         =   "mayor o igual que -25,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   360
         TabIndex        =   24
         Top             =   2040
         Width           =   4080
      End
      Begin VB.Label lblTipoUmbral 
         BackColor       =   &H80000018&
         Caption         =   "dAviso en puntuaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   1800
         Width           =   4320
      End
      Begin VB.Line lnUmbral 
         BorderColor     =   &H00000000&
         X1              =   120
         X2              =   6240
         Y1              =   1680
         Y2              =   1680
      End
      Begin VB.Label lblPuntProve 
         BackColor       =   &H80000018&
         Caption         =   "dPuntuaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   22
         Top             =   1320
         Width           =   1080
      End
      Begin VB.Label lblDenUnidad 
         BackColor       =   &H80000018&
         Caption         =   "GS - GESTAMP"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         TabIndex        =   21
         Top             =   960
         Width           =   4560
      End
      Begin VB.Label lblUnidad 
         BackColor       =   &H80000018&
         Caption         =   "dUnidad de negocio:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   20
         Top             =   960
         Width           =   1560
      End
      Begin VB.Label lblDenVarCal 
         BackColor       =   &H80000018&
         Caption         =   "dNCs- Incidencias (nivel 2)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         TabIndex        =   19
         Top             =   600
         Width           =   4560
      End
      Begin VB.Label lblVarCal 
         BackColor       =   &H80000018&
         Caption         =   "dVariable de Calidad:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   18
         Top             =   600
         Width           =   1560
      End
      Begin VB.Label lblDenProve 
         BackColor       =   &H80000018&
         Caption         =   "xxx"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   17
         Top             =   240
         Width           =   5160
      End
      Begin VB.Label lblAdjAviso 
         BackColor       =   &H80000018&
         Caption         =   "dAVISO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   0
         Width           =   6135
      End
      Begin VB.Label lblAdjProhibida 
         BackColor       =   &H80000018&
         Caption         =   "ADJUDICACI�N PROHIBIDA"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   0
         Width           =   4335
      End
      Begin VB.Label lblPuntProv 
         BackColor       =   &H80000018&
         Caption         =   "-25,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   5
         Top             =   1320
         Width           =   2895
      End
      Begin VB.Label lblProve 
         BackColor       =   &H80000018&
         Caption         =   "dProveedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   960
      End
   End
   Begin VB.PictureBox PicVarsQA 
      Height          =   2765
      Left            =   3720
      ScaleHeight     =   2700
      ScaleWidth      =   7710
      TabIndex        =   1
      Top             =   1080
      Visible         =   0   'False
      Width           =   7770
      Begin SSDataWidgets_B.SSDBGrid sdbgVarsRestric 
         Height          =   2715
         Left            =   30
         TabIndex        =   2
         Top             =   0
         Width           =   7695
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   13
         stylesets.count =   11
         stylesets(0).Name=   "Nivel1"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   8421504
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmComparativaQA.frx":1778
         stylesets(0).AlignmentText=   0
         stylesets(1).Name=   "Nivel2"
         stylesets(1).BackColor=   13619151
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmComparativaQA.frx":1794
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Nivel3"
         stylesets(2).BackColor=   13417139
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmComparativaQA.frx":17B0
         stylesets(2).AlignmentText=   0
         stylesets(3).Name=   "Normal"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmComparativaQA.frx":17CC
         stylesets(4).Name=   "Nivel4"
         stylesets(4).BackColor=   12566463
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmComparativaQA.frx":17E8
         stylesets(4).AlignmentText=   0
         stylesets(5).Name=   "Nivel5"
         stylesets(5).BackColor=   11513775
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmComparativaQA.frx":1804
         stylesets(5).AlignmentText=   0
         stylesets(6).Name=   "Nivel5Sel"
         stylesets(6).BackColor=   11513775
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmComparativaQA.frx":1820
         stylesets(6).AlignmentText=   0
         stylesets(7).Name=   "Nivel4Sel"
         stylesets(7).BackColor=   12566463
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmComparativaQA.frx":183C
         stylesets(7).AlignmentText=   0
         stylesets(8).Name=   "Nivel3Sel"
         stylesets(8).BackColor=   13417139
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmComparativaQA.frx":1858
         stylesets(8).AlignmentText=   0
         stylesets(9).Name=   "Nivel2Sel"
         stylesets(9).BackColor=   13619151
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmComparativaQA.frx":1874
         stylesets(9).AlignmentText=   0
         stylesets(10).Name=   "Nivel1Sel"
         stylesets(10).ForeColor=   16777215
         stylesets(10).BackColor=   8421504
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmComparativaQA.frx":1890
         stylesets(10).AlignmentText=   0
         UseGroups       =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Groups.Count    =   3
         Groups(0).Width =   3651
         Groups(0).Caption=   "DVariable"
         Groups(0).Columns.Count=   3
         Groups(0).Columns(0).Width=   820
         Groups(0).Columns(0).Visible=   0   'False
         Groups(0).Columns(0).Caption=   "VARCAL"
         Groups(0).Columns(0).Name=   "VARCAL"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(1).Width=   1005
         Groups(0).Columns(1).Visible=   0   'False
         Groups(0).Columns(1).Caption=   "NIVEL"
         Groups(0).Columns(1).Name=   "NIVEL"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(2).Width=   3651
         Groups(0).Columns(2).Caption=   "Variable"
         Groups(0).Columns(2).Name=   "DEN"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(2).Locked=   -1  'True
         Groups(1).Width =   4445
         Groups(1).Caption=   "DRestriccion"
         Groups(1).AllowSizing=   0   'False
         Groups(1).Columns.Count=   3
         Groups(1).Columns(0).Width=   1667
         Groups(1).Columns(0).Caption=   "Restricci�n"
         Groups(1).Columns(0).Name=   "RESTRICCION"
         Groups(1).Columns(0).AllowSizing=   0   'False
         Groups(1).Columns(0).DataField=   "Column 3"
         Groups(1).Columns(0).DataType=   8
         Groups(1).Columns(0).FieldLen=   256
         Groups(1).Columns(0).Locked=   -1  'True
         Groups(1).Columns(0).Style=   2
         Groups(1).Columns(1).Width=   1296
         Groups(1).Columns(1).Caption=   "Inferior"
         Groups(1).Columns(1).Name=   "R_INF"
         Groups(1).Columns(1).AllowSizing=   0   'False
         Groups(1).Columns(1).DataField=   "Column 4"
         Groups(1).Columns(1).DataType=   8
         Groups(1).Columns(1).FieldLen=   256
         Groups(1).Columns(1).Locked=   -1  'True
         Groups(1).Columns(2).Width=   1482
         Groups(1).Columns(2).Caption=   "Superior"
         Groups(1).Columns(2).Name=   "R_SUP"
         Groups(1).Columns(2).AllowSizing=   0   'False
         Groups(1).Columns(2).DataField=   "Column 5"
         Groups(1).Columns(2).DataType=   8
         Groups(1).Columns(2).FieldLen=   256
         Groups(1).Columns(2).Locked=   -1  'True
         Groups(2).Width =   4445
         Groups(2).Caption=   "DAviso"
         Groups(2).AllowSizing=   0   'False
         Groups(2).Columns.Count=   7
         Groups(2).Columns(0).Width=   1667
         Groups(2).Columns(0).Caption=   "Aviso"
         Groups(2).Columns(0).Name=   "AVISO"
         Groups(2).Columns(0).AllowSizing=   0   'False
         Groups(2).Columns(0).DataField=   "Column 6"
         Groups(2).Columns(0).DataType=   8
         Groups(2).Columns(0).FieldLen=   256
         Groups(2).Columns(0).Locked=   -1  'True
         Groups(2).Columns(0).Style=   2
         Groups(2).Columns(1).Width=   1296
         Groups(2).Columns(1).Caption=   "Inferior"
         Groups(2).Columns(1).Name=   "A_INF"
         Groups(2).Columns(1).AllowSizing=   0   'False
         Groups(2).Columns(1).DataField=   "Column 7"
         Groups(2).Columns(1).DataType=   8
         Groups(2).Columns(1).FieldLen=   256
         Groups(2).Columns(1).Locked=   -1  'True
         Groups(2).Columns(2).Width=   1482
         Groups(2).Columns(2).Caption=   "Superior"
         Groups(2).Columns(2).Name=   "A_SUP"
         Groups(2).Columns(2).AllowSizing=   0   'False
         Groups(2).Columns(2).DataField=   "Column 8"
         Groups(2).Columns(2).DataType=   8
         Groups(2).Columns(2).FieldLen=   256
         Groups(2).Columns(2).Locked=   -1  'True
         Groups(2).Columns(3).Width=   4260
         Groups(2).Columns(3).Visible=   0   'False
         Groups(2).Columns(3).Caption=   "ID_VAR_CAL1"
         Groups(2).Columns(3).Name=   "ID_VAR_CAL1"
         Groups(2).Columns(3).DataField=   "Column 9"
         Groups(2).Columns(3).DataType=   8
         Groups(2).Columns(3).FieldLen=   256
         Groups(2).Columns(4).Width=   8493
         Groups(2).Columns(4).Visible=   0   'False
         Groups(2).Columns(4).Caption=   "ID_VAR_CAL2"
         Groups(2).Columns(4).Name=   "ID_VAR_CAL2"
         Groups(2).Columns(4).DataField=   "Column 10"
         Groups(2).Columns(4).DataType=   8
         Groups(2).Columns(4).FieldLen=   256
         Groups(2).Columns(5).Width=   8493
         Groups(2).Columns(5).Visible=   0   'False
         Groups(2).Columns(5).Caption=   "ID_VAR_CAL3"
         Groups(2).Columns(5).Name=   "ID_VAR_CAL3"
         Groups(2).Columns(5).DataField=   "Column 11"
         Groups(2).Columns(5).DataType=   8
         Groups(2).Columns(5).FieldLen=   256
         Groups(2).Columns(6).Width=   7382
         Groups(2).Columns(6).Visible=   0   'False
         Groups(2).Columns(6).Caption=   "ID_VAR_CAL4"
         Groups(2).Columns(6).Name=   "ID_VAR_CAL4"
         Groups(2).Columns(6).DataField=   "Column 12"
         Groups(2).Columns(6).DataType=   8
         Groups(2).Columns(6).FieldLen=   256
         _ExtentX        =   13573
         _ExtentY        =   4789
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgProveVarsQA 
      Height          =   3780
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   10755
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      GroupHeadLines  =   2
      Row.Count       =   3
      Col.Count       =   126
      Row(0).Col(0)   =   "PROVE1"
      Row(0).Col(1)   =   "Proveedor 1"
      Row(1).Col(0)   =   "PROVE2"
      Row(1).Col(1)   =   "Proveedor 2"
      Row(2).Col(0)   =   "PROVE3"
      Row(2).Col(1)   =   "Proveedor 3"
      stylesets.count =   16
      stylesets(0).Name=   "GrayHeadCeldaUnqa"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmComparativaQA.frx":18AC
      stylesets(0).AlignmentPicture=   0
      stylesets(1).Name=   "Grupo"
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmComparativaQA.frx":1928
      stylesets(1).AlignmentText=   2
      stylesets(1).AlignmentPicture=   0
      stylesets(2).Name=   "ValorRed"
      stylesets(2).BackColor=   4744445
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmComparativaQA.frx":19A4
      stylesets(2).AlignmentText=   1
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmComparativaQA.frx":19C0
      stylesets(4).Name=   "ValorOrange"
      stylesets(4).BackColor=   33023
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmComparativaQA.frx":19DC
      stylesets(5).Name=   "IMGRESTRIC"
      stylesets(5).BackColor=   7303167
      stylesets(5).Picture=   "frmComparativaQA.frx":19F8
      stylesets(5).AlignmentPicture=   1
      stylesets(6).Name=   "Proveedor"
      stylesets(6).BackColor=   9876917
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmComparativaQA.frx":1C36
      stylesets(6).AlignmentText=   0
      stylesets(6).AlignmentPicture=   0
      stylesets(7).Name=   "ProveedorNoVisible"
      stylesets(7).BackColor=   9876917
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmComparativaQA.frx":2019
      stylesets(7).AlignmentText=   2
      stylesets(7).AlignmentPicture=   0
      stylesets(8).Name=   "UnQa"
      stylesets(8).BackColor=   9876917
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmComparativaQA.frx":23FC
      stylesets(8).AlignmentText=   0
      stylesets(8).AlignmentPicture=   0
      stylesets(9).Name=   "IMGAVISO"
      stylesets(9).BackColor=   33023
      stylesets(9).Picture=   "frmComparativaQA.frx":2478
      stylesets(9).AlignmentPicture=   1
      stylesets(10).Name=   "CalificacionRed"
      stylesets(10).BackColor=   4744445
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "frmComparativaQA.frx":26B6
      stylesets(10).AlignmentText=   0
      stylesets(11).Name=   "Calificacion"
      stylesets(11).BackColor=   13170165
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "frmComparativaQA.frx":26D2
      stylesets(11).AlignmentText=   0
      stylesets(12).Name=   "Valor"
      stylesets(12).BackColor=   13170165
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmComparativaQA.frx":26EE
      stylesets(12).AlignmentText=   1
      stylesets(13).Name=   "CalificacionOrange"
      stylesets(13).BackColor=   33023
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "frmComparativaQA.frx":270A
      stylesets(13).AlignmentText=   0
      stylesets(14).Name=   "GrayHead"
      stylesets(14).ForeColor=   0
      stylesets(14).BackColor=   -2147483633
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "frmComparativaQA.frx":2726
      stylesets(15).Name=   "CeldaUnqa"
      stylesets(15).BackColor=   9876917
      stylesets(15).HasFont=   -1  'True
      BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(15).Picture=   "frmComparativaQA.frx":2742
      stylesets(15).AlignmentPicture=   0
      UseGroups       =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Groups.Count    =   2
      Groups(0).Width =   4551
      Groups(0).Caption=   "Proveedores"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadBackColor=   9876917
      Groups(0).Columns.Count=   2
      Groups(0).Columns(0).Width=   2249
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "COD"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).HasHeadForeColor=   -1  'True
      Groups(0).Columns(0).HasHeadBackColor=   -1  'True
      Groups(0).Columns(0).HasForeColor=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).HeadBackColor=   9876917
      Groups(0).Columns(0).BackColor=   9876917
      Groups(0).Columns(0).HeadStyleSet=   "Normal"
      Groups(0).Columns(1).Width=   2302
      Groups(0).Columns(1).Caption=   "Denominaci�n"
      Groups(0).Columns(1).Name=   "DEN"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HasHeadForeColor=   -1  'True
      Groups(0).Columns(1).HasHeadBackColor=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).HeadBackColor=   9876917
      Groups(0).Columns(1).BackColor=   9876917
      Groups(0).Columns(1).HeadStyleSet=   "Normal"
      Groups(1).Width =   3200
      Groups(1).Caption=   "Unidades de negocio"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadBackColor=   9876917
      Groups(1).Columns.Count=   2
      Groups(1).Columns(0).Width=   2514
      Groups(1).Columns(0).Caption=   "dUNQA"
      Groups(1).Columns(0).Name=   "UNQA"
      Groups(1).Columns(0).AllowSizing=   0   'False
      Groups(1).Columns(0).DataField=   "Column 2"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Locked=   -1  'True
      Groups(1).Columns(0).HasHeadForeColor=   -1  'True
      Groups(1).Columns(0).HasHeadBackColor=   -1  'True
      Groups(1).Columns(0).HasForeColor=   -1  'True
      Groups(1).Columns(0).HasBackColor=   -1  'True
      Groups(1).Columns(0).HeadBackColor=   9876917
      Groups(1).Columns(0).BackColor=   9876917
      Groups(1).Columns(0).HeadStyleSet=   "Normal"
      Groups(1).Columns(1).Width=   688
      Groups(1).Columns(1).Name=   "BTUNQA"
      Groups(1).Columns(1).AllowSizing=   0   'False
      Groups(1).Columns(1).DataField=   "Column 3"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Locked=   -1  'True
      Groups(1).Columns(1).HasHeadForeColor=   -1  'True
      Groups(1).Columns(1).HasHeadBackColor=   -1  'True
      Groups(1).Columns(1).HasForeColor=   -1  'True
      Groups(1).Columns(1).HasBackColor=   -1  'True
      Groups(1).Columns(1).HeadBackColor=   9876917
      Groups(1).Columns(1).BackColor=   9876917
      Groups(1).Columns(1).HeadStyleSet=   "Normal"
      UseDefaults     =   0   'False
      _ExtentX        =   18971
      _ExtentY        =   6667
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgVarsQAProve 
      Height          =   3780
      Left            =   120
      TabIndex        =   9
      Top             =   720
      Visible         =   0   'False
      Width           =   11940
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      GroupHeadLines  =   2
      Col.Count       =   5
      stylesets.count =   16
      stylesets(0).Name=   "GrayHeadCeldaUnqa"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmComparativaQA.frx":27BE
      stylesets(0).AlignmentText=   1
      stylesets(0).AlignmentPicture=   0
      stylesets(1).Name=   "Grupo"
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmComparativaQA.frx":283A
      stylesets(1).AlignmentText=   0
      stylesets(1).AlignmentPicture=   1
      stylesets(2).Name=   "VariablesNoVisible"
      stylesets(2).BackColor=   9876917
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmComparativaQA.frx":2856
      stylesets(2).AlignmentText=   2
      stylesets(2).AlignmentPicture=   0
      stylesets(3).Name=   "ValorRed"
      stylesets(3).BackColor=   4744445
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmComparativaQA.frx":28D2
      stylesets(3).AlignmentText=   1
      stylesets(4).Name=   "GrayHeadCabecera"
      stylesets(4).BackColor=   -2147483633
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmComparativaQA.frx":28EE
      stylesets(4).AlignmentText=   0
      stylesets(4).AlignmentPicture=   0
      stylesets(5).Name=   "Variables"
      stylesets(5).BackColor=   9876917
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmComparativaQA.frx":2CD1
      stylesets(5).AlignmentText=   0
      stylesets(5).AlignmentPicture=   0
      stylesets(6).Name=   "ValorOrange"
      stylesets(6).BackColor=   33023
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmComparativaQA.frx":2D4D
      stylesets(6).AlignmentText=   1
      stylesets(7).Name=   "Normal"
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmComparativaQA.frx":2D69
      stylesets(8).Name=   "IMGRESTRIC"
      stylesets(8).BackColor=   6184703
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmComparativaQA.frx":2D85
      stylesets(8).AlignmentText=   0
      stylesets(8).AlignmentPicture=   1
      stylesets(9).Name=   "IMGAVISO"
      stylesets(9).BackColor=   33023
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmComparativaQA.frx":2FC3
      stylesets(9).AlignmentPicture=   1
      stylesets(10).Name=   "CalificacionRed"
      stylesets(10).BackColor=   4744445
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "frmComparativaQA.frx":3201
      stylesets(10).AlignmentText=   0
      stylesets(10).AlignmentPicture=   1
      stylesets(11).Name=   "CalificacionOrange"
      stylesets(11).BackColor=   33023
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "frmComparativaQA.frx":321D
      stylesets(11).AlignmentText=   0
      stylesets(12).Name=   "Valor"
      stylesets(12).BackColor=   13170165
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmComparativaQA.frx":3239
      stylesets(13).Name=   "Calificacion"
      stylesets(13).BackColor=   13170165
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "frmComparativaQA.frx":3255
      stylesets(13).AlignmentText=   0
      stylesets(14).Name=   "GrayHead"
      stylesets(14).ForeColor=   0
      stylesets(14).BackColor=   -2147483633
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "frmComparativaQA.frx":3271
      stylesets(15).Name=   "CeldaUnqa"
      stylesets(15).BackColor=   9876917
      stylesets(15).HasFont=   -1  'True
      BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(15).Picture=   "frmComparativaQA.frx":328D
      stylesets(15).AlignmentPicture=   0
      UseGroups       =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   265
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Groups.Count    =   2
      Groups(0).Width =   5265
      Groups(0).Caption=   "Variables de calidad"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadBackColor=   9876917
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   2619
      Groups(0).Columns(0).Caption=   "DC�digo"
      Groups(0).Columns(0).Name=   "COD"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).Style=   1
      Groups(0).Columns(0).ButtonsAlways=   -1  'True
      Groups(0).Columns(0).HasHeadBackColor=   -1  'True
      Groups(0).Columns(0).HasForeColor=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).HeadForeColor=   16777215
      Groups(0).Columns(0).HeadBackColor=   9876917
      Groups(0).Columns(0).BackColor=   9876917
      Groups(0).Columns(0).HeadStyleSet=   "Normal"
      Groups(0).Columns(1).Width=   2646
      Groups(0).Columns(1).Caption=   "DDenominaci�n"
      Groups(0).Columns(1).Name=   "DEN"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HasHeadForeColor=   -1  'True
      Groups(0).Columns(1).HasHeadBackColor=   -1  'True
      Groups(0).Columns(1).HasForeColor=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).HeadBackColor=   9876917
      Groups(0).Columns(1).BackColor=   9876917
      Groups(0).Columns(1).HeadStyleSet=   "Normal"
      Groups(0).Columns(2).Width=   2619
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "DATOS_VAR"
      Groups(0).Columns(2).Name=   "DATOS_VAR"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(1).Width =   3200
      Groups(1).Caption=   "Unidades de negocio"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadBackColor=   9876917
      Groups(1).Columns.Count=   2
      Groups(1).Columns(0).Width=   2752
      Groups(1).Columns(0).Caption=   "DUnidad de negocio"
      Groups(1).Columns(0).Name=   "UNQA"
      Groups(1).Columns(0).AllowSizing=   0   'False
      Groups(1).Columns(0).DataField=   "Column 3"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Locked=   -1  'True
      Groups(1).Columns(0).ButtonsAlways=   -1  'True
      Groups(1).Columns(0).HasHeadForeColor=   -1  'True
      Groups(1).Columns(0).HasHeadBackColor=   -1  'True
      Groups(1).Columns(0).HasForeColor=   -1  'True
      Groups(1).Columns(0).HasBackColor=   -1  'True
      Groups(1).Columns(0).HeadBackColor=   9876917
      Groups(1).Columns(0).BackColor=   9876917
      Groups(1).Columns(0).HeadStyleSet=   "Normal"
      Groups(1).Columns(1).Width=   450
      Groups(1).Columns(1).Name=   "BTUNQA"
      Groups(1).Columns(1).AllowSizing=   0   'False
      Groups(1).Columns(1).DataField=   "Column 4"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Locked=   -1  'True
      Groups(1).Columns(1).HasHeadBackColor=   -1  'True
      Groups(1).Columns(1).HasForeColor=   -1  'True
      Groups(1).Columns(1).HasBackColor=   -1  'True
      Groups(1).Columns(1).HeadBackColor=   9876917
      Groups(1).Columns(1).BackColor=   9876917
      Groups(1).Columns(1).HeadStyleSet=   "Normal"
      UseDefaults     =   0   'False
      _ExtentX        =   21061
      _ExtentY        =   6667
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmComparativaQA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public m_oProvesAsig As CProveedores
Private m_sCalificacion As String
Private m_sValor As String

Public m_oVarsCalidad As CVariablesCalidad
Private m_oVarCal0 As CVariableCalidad
Private m_oVarCal1 As CVariableCalidad
Private m_oVarCal2 As CVariableCalidad
Private m_oVarCal3 As CVariableCalidad
Private m_oVarCal4 As CVariableCalidad
Private m_oVarCal5 As CVariableCalidad

Public m_oVistaCalSeleccionada As CConfVistaCalGlobal
Public m_oProcesoSeleccionado As CProceso
Public m_udtTipoVQA As TipoDeVistaqa
Public m_oConfVistasCalGlobal As CConfVistasCalGlobal
Public m_sVistaGlobal As String
Public m_sCaption As String

Public m_bEsResReu As Boolean

Private m_iIDRest As Variant
Private m_iNivelRest As Integer

Private m_sVarNivel As String
Private m_sNivel As String
Private m_sBloqueo As String
Private m_sMenoresDe As String
Private m_sMayoresDe As String
Private m_sAviso As String
Private m_sValorProv As String
Private m_sProveVarNoVisible1 As String
Private m_sProveVarNoVisible2 As String
Private m_sVarsNoVisible() As String 'colecci�n de variables invisibles. Estilos de lineas
Private m_sVarsUnqaNoVisible() As String 'colecci�n de variable-unidad invisibles. Tooltip

Private m_bCambios As Boolean
Private m_sIdiGeneral As String
Private m_sVariablesCal As String
Private m_sIdiCodigo As String
Private m_sIdiNombre As String
Private m_sIdiInicial As String
Private m_sIdiGenComp As String
Private m_sIdiAbrExcel As String
Private m_sIdiProceso As String
Private m_oFos As Scripting.FileSystemObject

Const POS_ID = 0
Const POS_ID_VARCAL1 = 1
Const POS_ID_VARCAL2 = 2
Const POS_ID_VARCAL3 = 3
Const POS_ID_VARCAL4 = 4
Const POS_NIVEL = 5
Const POS_RESTRICCION = 6
Const POS_R_INF = 7
Const POS_R_SUP = 8
Const POS_AVISO = 9
Const POS_A_INF = 10
Const POS_A_SUP = 11
Const POS_UNQA = 12
Const POS_UNQA_HIJOS = 13
Const POS_UNQA_NIVEL = 14
Const POS_TIPO = 15
Const POS_SUBTIPO = 16

Private m_bPermitirVistasCompQA As Boolean

Private m_bDeshabilidar_MouseMove As Boolean
Private m_bDeshabilidar_Form_Deactivate As Boolean
Private m_bDeshabilidar_Chk_ChangeTodas As Boolean
Public g_sNombreVista As String
Private m_oVistasCombo As CConfVistasCalGlobal

Private m_oUnidadesQa As CUnidadesNegQA 'coleccion unidades. Bucles unidades
Private m_oUnqaEnProve As CUnidadesNegQA 'coleccion unidades con puntos. Rellenar grids/arboles visibilidad
Private m_oUnqaVarcalEnProve As CVariablesCalidad 'coleccion unidad-proveedor con puntos. Rellenar grids/arboles visibilidad

Private m_oOcultarUnqaProve_GridPv As CProveedores 'colecciones "desligadas de la vista" de visibilidades de unidades grid ProveVars
Private m_oMostrarUnqaProve_GridPv As CProveedores 'colecciones "desligadas de la vista" de visibilidades de unidades grid ProveVars

Private m_lIdNivel0 As Long

Private m_bVistaInicialMantUnqa As Boolean
Private m_bVistaCambiaUnqas As Boolean
Private m_bVistaCambiaUnqas_GridPv As Boolean

Private m_bVistaCambiaVarCal As Boolean

Private m_bGrpGenerarEstrucUnQaLimpiado As Boolean
Private m_bGrpGenerarEstrucUnQaPulsado As Boolean
Private m_bGrpGenerarEstrucUnQa1ra As Boolean
Private m_bGrpGenerarEstrucUnQaCambio As Boolean

Private Declare Function SetCursorPos Lib "user32" (ByVal X As Long, ByVal Y As Long) As Long

Private Const CTE_WIDTH_3COLBT = 360

Private m_bVistaCambiaProve As Boolean

Private m_sIdiUnidad As String
Private m_sIdiUnidadCN As String

Private m_bSetFocusGrid As Boolean

Private m_bBtDefectoCanDisabled As Boolean

Private m_iEtiqNoVisibles As Integer

Private m_sIdiRenombrar As String
Private m_sIdiGuardaNueva As String

Private Const IDC_HAND = 32649&
Private Declare Function LoadCursorLong Lib "user32" Alias "LoadCursorA" _
  (ByVal hInstance As Long, ByVal lpCursorName As Long) As Long

Private Declare Function SetCursor Lib "user32" _
  (ByVal hCursor As Long) As Long
Private m_sfrmWeb As String
'Variables para saber si hay que ocultar la columna del icono la cual da acceso a ver el tooltip del aviso o restriccion
Private m_bConAvisoRestricProveVarsQA As Boolean
Private m_bConAvisoRestricVarsQAProve As Boolean
Private m_bActivadoParaGrid As Boolean

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Sub pmCargarColumnaAVISORESTRIC(oColumn As SSDataWidgets_B.Column, ogroup As SSDataWidgets_B.Group, sTexto As String, iColumna As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
ogroup.Columns.Add iColumna
Set oColumn = ogroup.Columns(iColumna)
oColumn.Name = "AVISORESTRIC" & sTexto
oColumn.CaptionAlignment = ssColCapAlignCenter
oColumn.caption = ""
oColumn.StyleSet = "Valor"
oColumn.HeadStyleSet = "GrayHead"
oColumn.Alignment = ssCaptionAlignmentCenter
oColumn.Visible = True
oColumn.AllowSizing = False
oColumn.Width = 300
oColumn.Locked = True
DoEvents

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "pmCargarColumnaAVISORESTRIC", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Function Poner_Cursor_En_Celda(iNivel As Integer, iTipo As Integer, iSubTipo As Integer) As Boolean
    Dim bMano As Boolean
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bMano = False
    
    If iNivel > 1 Then
        If iTipo = 0 Then
            Select Case iSubTipo
                Case CalidadSubtipo.CalCertificado, CalidadSubtipo.CalNoConformidad, CalidadSubtipo.CalPPM, CalidadSubtipo.CalCargoProveedores, CalidadSubtipo.CalTasaServicios, CalidadSubtipo.CalEncuesta
                    bMano = True
            End Select
        End If
    End If
    
    If bMano Then
        SetMouseCursor (IDC_HAND)
    Else
        Screen.MousePointer = vbArrow
    End If
    
    Poner_Cursor_En_Celda = bMano
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Poner_Cursor_En_Celda", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Function SetMouseCursor(CursorType As Long)
  Dim hCursor As Long
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
  hCursor = LoadCursorLong(0&, CursorType)
  hCursor = SetCursor(hCursor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "SetMouseCursor", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Establece q para la vista selecccionada se muestren o se oculten las variables q no sean del material del proceso
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub chkMostrarVarsMaterialProceso_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bDeshabilidar_Chk_ChangeTodas Then Exit Sub

    m_oVistaCalSeleccionada.Materiales_Proceso = Me.chkMostrarVarsMaterialProceso.Value

    m_oVistaCalSeleccionada.HayCambios = True
    
    m_bVistaCambiaVarCal = True

    GenerarEstructuraVarCal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "chkMostrarVarsMaterialProceso_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Chequea/Deschequea todos los nodos del arbol de selecci�n de Unidades
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub chkTodasUnidades_Click()
    Dim nodx As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bDeshabilidar_Chk_ChangeTodas Then Exit Sub
    
    For Each nodx In Me.tvwUndNegUsu.Nodes
        If Not (IIf(nodx.Checked, 1, 0) = chkTodasUnidades.Value) Then
            nodx.Checked = chkTodasUnidades.Value
            tvwUndNegUsu_NodeCheck nodx
        End If
    Next
    
    For Each nodx In Me.tvwUndNegUsu.Nodes
        Me.tvwUndNegUsu.selectedItem = nodx
        Exit For
    Next
    Me.tvwUndNegUsu.selectedItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "chkTodasUnidades_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
''' <summary>
''' - Chequea/Deschequea todos los nodos del arbol de selecci�n de Unidades del grid ProveVars.
''' - Se diferencia entre grids pq en este grid no se muestran todas las unidades visibles en la vista. Me explico
''' las invisibles en vista no se ven obviamente y de entre las visibles en vista solo se veran las q tengan
''' algun sentido para el proveedor, vamos las q tenga relacionadas (tabla prove_unqa)
''' - Ahora bien lo q ves no debe ser estatico, debes poder mostrar/ocultar para un proveedor (vista es para todos)
''' esto es lo q hace el arbol tvwUndNegUsu_GridPv esta funcion es para llevar una colecci�n de visibilidades aparte
''' de la vista a traves de los check del arbol.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub chkTodasUnidades_GridPv_Click()
    Dim nodx As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bDeshabilidar_Chk_ChangeTodas Then Exit Sub
    
    For Each nodx In Me.tvwUndNegUsu_GridPv.Nodes
        If Not (IIf(nodx.Checked, 1, 0) = chkTodasUnidades_GridPv.Value) Then
            nodx.Checked = chkTodasUnidades_GridPv.Value
            tvwUndNegUsu_GridPv_NodeCheck nodx
        End If
    Next
    
    For Each nodx In Me.tvwUndNegUsu_GridPv.Nodes
        Me.tvwUndNegUsu_GridPv.selectedItem = nodx
        Exit For
    Next
    Me.tvwUndNegUsu_GridPv.selectedItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "chkTodasUnidades_GridPv_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Chequea/Deschequea todos los nodos del arbol de selecci�n de Variables
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub chkTodasVariables_Click()
    Dim nodx As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bDeshabilidar_Chk_ChangeTodas Then Exit Sub
    
    For Each nodx In Me.tvwVarCal.Nodes
        If Not (IIf(nodx.Checked, 1, 0) = chkTodasVariables.Value) Then
            nodx.Checked = chkTodasVariables.Value
            tvwVarCal_NodeCheck nodx
        End If
    Next
    
    For Each nodx In Me.tvwVarCal.Nodes
        Me.tvwVarCal.selectedItem = nodx
        Exit For
    Next
    Me.tvwVarCal.selectedItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "chkTodasVariables_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Chequea todos los nodos del arbol de selecci�n de proveedores. No deschequea todos pq al menos debe haber uno visible.
''' Muestra una label informativa "al menos debe haber uno visible" en caso de intentar deschequear todo.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub chkTodosProves_Click()
    Dim nodx As MSComctlLib.node
    Dim ProvesVisibles As Integer
    Dim TodosProves As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bDeshabilidar_Chk_ChangeTodas Then Exit Sub
    
    ProvesVisibles = DameNumeroProvesVisibles
    TodosProves = m_oProvesAsig.Count
    
    If chkTodosProves.Value = 0 Then
        m_bDeshabilidar_Chk_ChangeTodas = True
        chkTodosProves.Value = 1
        
        If Me.lblAlmenosUno.Visible = False Then
            tvwProves.Height = picProves.Height - 1320 - Me.lblAlmenosUno.Height - 30
            Me.lblAlmenosUno.Visible = True
        End If
        m_bDeshabilidar_Chk_ChangeTodas = False
        
        If TodosProves = ProvesVisibles Then Exit Sub
    End If
        
    For Each nodx In Me.tvwProves.Nodes
        If Not (IIf(nodx.Checked, 1, 0) = chkTodosProves.Value) Then
            nodx.Checked = chkTodosProves.Value
            tvwProves_NodeCheck nodx
        End If
    Next
    
    For Each nodx In Me.tvwProves.Nodes
        Me.tvwProves.selectedItem = nodx
        Exit For
    Next
    Me.tvwProves.selectedItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "chkTodosProves_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Establece q para la vista selecccionada se muestre o se oculte la columna de calificaci�n
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub chkVerCalificacion_Click()
    Dim iGrupos As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    If m_bDeshabilidar_Chk_ChangeTodas Then Exit Sub
    
    m_oVistaCalSeleccionada.Calificaciones = Not m_oVistaCalSeleccionada.Calificaciones
        
    m_oVistaCalSeleccionada.HayCambios = True
    
    LockWindowUpdate frmComparativaQA.hWnd
    Screen.MousePointer = vbHourglass
    
    For iGrupos = 2 To sdbgVarsQAProve.Groups.Count - 1
        sdbgVarsQAProve.Groups(iGrupos).Columns(0).Visible = m_oVistaCalSeleccionada.Calificaciones
    Next
    
    For iGrupos = 2 To Me.sdbgProveVarsQA.Groups.Count - 1
        sdbgProveVarsQA.Groups(iGrupos).Columns(0).Visible = m_oVistaCalSeleccionada.Calificaciones
    Next
    
    RedimensionarGrid
    
    LockWindowUpdate 0&

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "chkVerCalificacion_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Establece q para la vista selecccionada se muestre o se oculte la columna de puntos
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub chkVerPuntos_Click()
    Dim iGrupos As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    If m_bDeshabilidar_Chk_ChangeTodas Then Exit Sub
    
    m_oVistaCalSeleccionada.Puntos = Not m_oVistaCalSeleccionada.Puntos
    
    m_oVistaCalSeleccionada.HayCambios = True
    
    LockWindowUpdate frmComparativaQA.hWnd
    Screen.MousePointer = vbHourglass

    For iGrupos = 2 To sdbgVarsQAProve.Groups.Count - 1
        sdbgVarsQAProve.Groups(iGrupos).Columns(1).Visible = m_oVistaCalSeleccionada.Puntos
    Next
    
    For iGrupos = 2 To Me.sdbgProveVarsQA.Groups.Count - 1
        sdbgProveVarsQA.Groups(iGrupos).Columns(1).Visible = m_oVistaCalSeleccionada.Puntos
    Next
    
    RedimensionarGrid

    LockWindowUpdate 0&

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "chkVerPuntos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Elimina la Vista seleccionada.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub cmdEliminarVista_Click()
Dim irespuesta As Integer
Dim oIBaseDatos As IBaseDatos
Dim teserror As TipoErrorSummit
Dim iVistaVieja As Integer
Dim udtTipoVistaVieja As TipoDeVistaqa
Dim bDefecto As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDeshabilidar_Form_Deactivate = True

    iVistaVieja = m_oVistaCalSeleccionada.Vista
    udtTipoVistaVieja = m_oVistaCalSeleccionada.TipoVista
    irespuesta = oMensajes.PreguntaEliminar(sdbcVistaActual.Text)
    
    If irespuesta = vbYes Then
        LockWindowUpdate Me.hWnd
        Screen.MousePointer = vbHourglass
        
        Set oIBaseDatos = m_oVistaCalSeleccionada
        teserror = oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        Else
            EliminarVistaDeColeccionQA CStr(iVistaVieja)
        End If

        VistaDespuesDeEliminar iVistaVieja, udtTipoVistaVieja

        ConfigurarBotonesVistas m_oVistaCalSeleccionada.TipoVista

        If CStr(iVistaVieja & udtTipoVistaVieja) = CStr(m_oProcesoSeleccionado.VistaDefectoQA & m_oProcesoSeleccionado.VistaDefectoTipoQA) Then
            m_oProcesoSeleccionado.VistaDefectoQA = m_oVistaCalSeleccionada.Vista
            m_oProcesoSeleccionado.VistaDefectoTipoQA = m_oVistaCalSeleccionada.TipoVista
            m_oProcesoSeleccionado.NombreVistaQA = m_oVistaCalSeleccionada.nombre

            teserror = m_oProcesoSeleccionado.ModificarVistaDefectoProcesoQA(oUsuarioSummit.Cod)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            End If
    
            bDefecto = True
            
        End If

        MostrarVistaEnCombos
        
        If Me.sdbgVarsQAProve.Visible Then
            If Me.Visible Then Me.sdbgVarsQAProve.SetFocus
        Else
            If Me.Visible Then Me.sdbgProveVarsQA.SetFocus
        End If
        
        LockWindowUpdate 0&
        Screen.MousePointer = vbDefault
    End If

    m_bDeshabilidar_Form_Deactivate = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "cmdEliminarVista_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdExcel_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    LockWindowUpdate Me.hWnd
    
    FocoAGrid
        
    Call ObtenerHojaComparativaQA
    
    LockWindowUpdate 0&
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "cmdExcel_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Guarda la Vista seleccionada.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub cmdGuardarVista_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_oVistaCalSeleccionada Is Nothing Then
        GuardarVistaGeneral 'Actualizamos el objeto m_oVistaCalSeleccionada
    End If

    GuardarVistas
    
    LockWindowUpdate Me.hWnd
        
    If Not m_oConfVistasCalGlobal.Item(CStr(sdbcVistaActual.Columns("COD").Value)) Is Nothing Then
        m_oConfVistasCalGlobal.Remove CStr(sdbcVistaActual.Columns("COD").Value)
    End If
    
    ConfiguracionVistaActualQA sdbcVistaActual.Columns("COD").Value, sdbcVistaActual.Columns("TIPO").Value
        
    FocoAGrid
    
    LockWindowUpdate 0&
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "cmdGuardarVista_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Guarda la Vista seleccionada como una vista nueva.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub cmdGuardarVistaNueva_Click()
    Dim teserror As TipoErrorSummit
    Dim iVistaVieja As Integer
    Dim iDecimales As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDeshabilidar_Form_Deactivate = True

    iDecimales = m_oVistaCalSeleccionada.decimales
    
    'Ponemos en la colecci�n lo que se haya modificado en pantalla
    GuardarVistaGeneral

    frmADJGuardarVista.g_sOrigen = "frmComparativaQA"
    frmADJGuardarVista.caption = m_sIdiGuardaNueva
    frmADJGuardarVista.Show vbModal
    
    iVistaVieja = m_oVistaCalSeleccionada.Vista
    If g_sNombreVista <> "" Then
        teserror = m_oVistaCalSeleccionada.GuardarVistaNuevaBd(g_sNombreVista)
        
        m_oVistaCalSeleccionada.HayCambios = False
        m_bCambios = False

        m_bSetFocusGrid = False
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        Else
            EliminarVistaDeColeccionQA CStr(iVistaVieja)
            oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
        End If
        m_bSetFocusGrid = True

        Screen.MousePointer = vbHourglass
        LockWindowUpdate Me.hWnd
        ConfiguracionVistaActualQA teserror.Arg1, TipoDeVistaqa.otravistaqa

        Me.caption = m_sCaption & " (" & m_sVistaGlobal & " " & g_sNombreVista & ")"
        
        
        MostrarVistaEnCombos
        sdbcVistaActual.Text = m_sVistaGlobal & " " & g_sNombreVista
        sdbcVistaActual.Refresh
        ConfigurarBotonesVistas TipoDeVistaqa.otravistaqa
        
        m_bDeshabilidar_Chk_ChangeTodas = True
        
        Me.chkVerCalificacion.Value = IIf(m_oVistaCalSeleccionada.Calificaciones, vbChecked, vbUnchecked)
        Me.chkVerPuntos.Value = IIf(m_oVistaCalSeleccionada.Puntos, vbChecked, vbUnchecked)
        
        m_bDeshabilidar_Chk_ChangeTodas = False

        If Not (iDecimales = m_oVistaCalSeleccionada.decimales) Then
            CargarGridVarCalidad
        
            FocoAGrid
        End If
        
        LockWindowUpdate 0&

        Screen.MousePointer = vbDefault

    End If
    
    m_bDeshabilidar_Form_Deactivate = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "cmdGuardarVistaNueva_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Cambiar la forma de ver las variables, unidades y proveedores.
''' En caso de visualizar ProveVars inicializa las colecciones "desligadas de la vista"
''' m_oMostrarUnqaProve_GridPv/m_oOcultarUnqaProve_GridPv de visibilidades de unidades y
''' en caso de q lo q este cargado en ProveVars no sea lo de la vista aplica la vista
''' ademas de inicializar. Esto es as� pq viendo VarsProve puedes cambiar visibilidades
''' q repercuten en el ,en ese momento, invisible ProveVars pero es q anteriormente ProveVars
''' ha podido estar visible y haberle configurado cosas fuera de la vista.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdInvertir_Click()
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCambios = True
    
    'Arbol unidades a nivel de grupo limpio
    m_bGrpGenerarEstrucUnQaCambio = True
    
    'Al invertir hace un scroll de las grids para dejarlas en la posici�n inicial:
    If sdbgVarsQAProve.GrpPosition(0) > 0 Then
        i = sdbgVarsQAProve.Groups(0).Position
        sdbgVarsQAProve.Scroll -i, 0
        sdbgVarsQAProve.Update
        sdbgVarsQAProve.AllowColumnMoving = ssRelocateWithinGroup
    End If
    If sdbgProveVarsQA.GrpPosition(0) > 0 Then
        i = sdbgProveVarsQA.Groups(0).Position
        sdbgProveVarsQA.Scroll -i, 0
        sdbgProveVarsQA.Update
        sdbgProveVarsQA.AllowColumnMoving = ssRelocateWithinGroup
    End If
    For i = 0 To sdbgVarsQAProve.Groups.Count - 1
        sdbgVarsQAProve.Groups(i).Position = i
    Next i
    For i = 0 To sdbgProveVarsQA.Groups.Count - 1
        sdbgProveVarsQA.Groups(i).Position = i
    Next i
    
    'Invierte las grids
    If sdbgVarsQAProve.Visible = True Then
        Screen.MousePointer = vbHourglass
        LockWindowUpdate frmComparativaQA.hWnd
        
        sdbgVarsQAProve.Visible = False
        sdbgProveVarsQA.Visible = True
       
        If m_oOcultarUnqaProve_GridPv.Count > 0 Or m_oMostrarUnqaProve_GridPv.Count > 0 Then
            Set m_oOcultarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
            Set m_oMostrarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
        
            'Las unidades q solo estan visible/invisibles pq lo has indicado desde el nodo estan en
            'm_oMostrarUnqaProve_GridPv/m_oOcultarUnqaProve_GridPv si las limpia hay q aplicarlo al grid
            Rellenar_sdbgProveVarsQA
        End If
               
        m_oVistaCalSeleccionada.TipoVision = 1
                
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
    Else
        sdbgVarsQAProve.Visible = True
        sdbgProveVarsQA.Visible = False
       
        m_oVistaCalSeleccionada.TipoVision = 0

    End If
    
    FocoAGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "cmdInvertir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
       
End Sub

''' <summary>
''' Muestra la label de variables invisibles q implicar�an q al menos un proveedor sea no adjudicable � adjudicable
''' solo tras un aviso
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub cmdNoVisibles_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    NoVisiblesControles
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "cmdNoVisibles_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Renombrar la Vista seleccionada
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub cmdRenombrarVista_Click()
    Dim teserror As TipoErrorSummit
    Dim iDecimales As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDeshabilidar_Form_Deactivate = True
    
    frmADJGuardarVista.m_sNombreVistaAnterior = m_oVistaCalSeleccionada.nombre
    frmADJGuardarVista.g_sOrigen = "frmComparativaQA"
    frmADJGuardarVista.caption = m_sIdiRenombrar

    iDecimales = m_oVistaCalSeleccionada.decimales

    frmADJGuardarVista.Show vbModal
    
    If g_sNombreVista <> "" Then
        teserror = m_oVistaCalSeleccionada.RenombrarVista(g_sNombreVista)
        
        m_bSetFocusGrid = False
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        Else
            oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
        End If
        m_bSetFocusGrid = True
        
        sdbcVistaActual.Refresh
    
        sdbcVistaActual.Text = m_sVistaGlobal & " " & g_sNombreVista
        If m_oVistaCalSeleccionada.Vista = m_oProcesoSeleccionado.VistaDefectoQA And m_oVistaCalSeleccionada.TipoVista = m_oProcesoSeleccionado.VistaDefectoTipoQA Then
            m_oProcesoSeleccionado.NombreVistaQA = g_sNombreVista
        End If
        Me.caption = m_sCaption & " (" & m_sVistaGlobal & " " & g_sNombreVista & ")"
        
        If Not (iDecimales = m_oVistaCalSeleccionada.decimales) Then
            LockWindowUpdate Me.hWnd
            CargarGridVarCalidad
            LockWindowUpdate 0&
            FocoAGrid
        End If
    End If
    
    m_bDeshabilidar_Form_Deactivate = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "cmdRenombrarVista_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Pone por defecto la vista que se ha seleccionado
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,1</remarks>
Private Sub cmdVistaDefecto_Click()
Dim teserror As TipoErrorSummit

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If CInt(Me.sdbcVistaActual.Columns("COD").Value) = CInt(m_oProcesoSeleccionado.VistaDefectoQA) Then
        Exit Sub
    End If

    'Pone por defecto la vista que se ha seleccionado
    m_udtTipoVQA = sdbcVistaActual.Columns("TIPO").Value
    m_oProcesoSeleccionado.VistaDefectoQA = sdbcVistaActual.Columns("COD").Value
    m_oProcesoSeleccionado.VistaDefectoTipoQA = sdbcVistaActual.Columns("TIPO").Value
    m_oProcesoSeleccionado.NombreVistaQA = DevolverNombreVista(sdbcVistaActual.Text)

    teserror = m_oProcesoSeleccionado.ModificarVistaDefectoProcesoQA(oUsuarioSummit.Cod)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    m_bBtDefectoCanDisabled = True
    Me.cmdVistaDefecto.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "cmdVistaDefecto_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Para quitarle al combo de vistas el foco pq tras mostrar un arbol aunque el codigo no lo pone
''  explicitamente resulta q lo recibe. Y este combo reacciona a la ruleta y flechas de tal manera
''  q a veces se limpiaba aparentemente sin mas.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0</remarks>
Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    If m_bSetFocusGrid = True Then
        If sdbgVarsQAProve.Visible Then
            If Me.Visible Then sdbgVarsQAProve.SetFocus
        ElseIf sdbgVarsQAProve.Visible Then
            If Me.Visible Then sdbgProveVarsQA.SetFocus
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>Carga la pantalla de Comparativa QA, sus combos, checks, grids y botones</summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0,3</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub Form_Load()
    Dim Ador As ADODB.Recordset
    Dim oProve As CProveedor
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    m_bActivadoParaGrid = False
    m_bConAvisoRestricProveVarsQA = False
    m_bConAvisoRestricVarsQAProve = False
    m_bActivado = False
    m_bDescargarFrm = False
    m_bSetFocusGrid = True
    Set Ador = oUsuarioSummit.DevolverDimensionFormVistasQA
    If Not Ador.EOF Then
        If IsNull(Ador.Fields("QA_WIDTH").Value) Then
            Me.Width = 11745
        Else
            Me.Width = Ador.Fields("QA_WIDTH").Value
        End If
        If IsNull(Ador.Fields("QA_HEIGHT").Value) Then
            Me.Height = 3810
        Else
            Me.Height = Ador.Fields("QA_HEIGHT").Value
        End If
    End If
    Ador.Close
    Set Ador = Nothing
        
    If m_bEsResReu Then
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREUPermitirVistasCompQA)) Is Nothing Then
            m_bPermitirVistasCompQA = True
        Else
            m_bPermitirVistasCompQA = False
        End If
    Else
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPPermitirVistasCompQA)) Is Nothing Then
            m_bPermitirVistasCompQA = True
        Else
            m_bPermitirVistasCompQA = False
        End If
    End If

    CargarRecursos
    
    PonerFieldSeparator Me
    
    Set m_oUnidadesQa = oFSGSRaiz.Generar_CUnidadesNegQA
    Set m_oUnqaEnProve = oFSGSRaiz.Generar_CUnidadesNegQA
    Set m_oUnqaVarcalEnProve = oFSGSRaiz.Generar_CVariablesCalidad
    
    m_oUnidadesQa.GenerarEstructuraUnidadesNegQA oUsuarioSummit.Cod, gParametrosInstalacion.gIdioma, gParametrosGenerales.gbPymes, basOptimizacion.gPYMEUsuario, True
    m_bDeshabilidar_MouseMove = False
    
    m_bVistaInicialMantUnqa = False
    m_bVistaCambiaUnqas = False
    m_bVistaCambiaVarCal = False
    m_bVistaCambiaProve = False
    
    m_bGrpGenerarEstrucUnQaLimpiado = False
    m_bGrpGenerarEstrucUnQaPulsado = False
    m_bGrpGenerarEstrucUnQa1ra = False
    m_bGrpGenerarEstrucUnQaCambio = False
    
    m_iEtiqNoVisibles = 0
    
    CargarVistaActualCalidad
    
    Set m_oOcultarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
    Set m_oMostrarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
    
    If Not m_oProvesAsig Is Nothing Then
        For Each oProve In m_oProvesAsig
            m_oProvesAsig.Item(oProve.Cod).VisibleEnGrafico = True
            
            Set oProve.VariablesCalidad = oFSGSRaiz.Generar_CVariablesCalidad
            oProve.VariablesCalidad.CargarVariablesProveedor oProve.Cod, IIf(gParametrosGenerales.gbPymes, basOptimizacion.gPYMEUsuario, 0), m_oProcesoSeleccionado.Anyo, m_oProcesoSeleccionado.GMN1Cod, m_oProcesoSeleccionado.Cod
        Next
    End If
    
    CargarGridVarCalidad
    
    m_bBtDefectoCanDisabled = m_oConfVistasCalGlobal.ExisteVistaDefecto(m_oProcesoSeleccionado.Anyo, m_oProcesoSeleccionado.GMN1Cod, m_oProcesoSeleccionado.Cod, oUsuarioSummit.Cod)
    
    MostrarVistaEnCombos
    ConfigurarBotonesVistas m_udtTipoVQA
    
    If sdbgVarsQAProve.Visible Then
        sdbgVarsQAProve.Row = 0
        sdbgVarsQAProve.col = 0
    Else
        sdbgProveVarsQA.Row = 0
        sdbgProveVarsQA.col = 0
    End If
    
    Timer1.Enabled = False
    Timer1.Interval = 400
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo: 0,1</remarks>
Public Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim sRestriccion As String
Dim sInferior As String
Dim sSuperior As String

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Screen.MousePointer = vbHourglass
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_COMPARATIVAQA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value                                                              'Comparativa de calidad
        m_sCaption = Me.caption
        Ador.MoveNext
        m_sVistaGlobal = Ador(0).Value                                                          'Vista global
        Me.caption = m_sCaption & " (" & m_sVistaGlobal & ")"
        Ador.MoveNext
        m_sVariablesCal = Ador(0).Value                                                         'Variables de calidad
        sdbgVarsQAProve.Groups(0).caption = m_sVariablesCal
        Ador.MoveNext
        sdbgProveVarsQA.Groups(0).caption = Ador(0).Value                           'Proveedores
        Ador.MoveNext
        m_sIdiCodigo = Ador(0).Value                                                            'C�digo
        sdbgVarsQAProve.Groups(0).Columns("COD").caption = m_sIdiCodigo
        sdbgProveVarsQA.Groups(0).Columns("COD").caption = m_sIdiCodigo
        Ador.MoveNext
        m_sIdiNombre = Ador(0).Value                                                            'Nombre
        sdbgVarsQAProve.Groups(0).Columns("DEN").caption = m_sIdiNombre
        Ador.MoveNext
        m_sCalificacion = Ador(0).Value                                                         'Calificaci�n
        Ador.MoveNext
        m_sValor = Ador(0).Value                                                                'Valor
        Ador.MoveNext
        sdbgProveVarsQA.Groups("0").Columns("DEN").caption = Ador(0).Value            'Denominaci�n
        Ador.MoveNext
        sdbgVarsRestric.Groups(0).caption = Ador(0).Value                             'Variable
        Ador.MoveNext
        sRestriccion = Ador(0).Value                                                            'Restricci�n
        sdbgVarsRestric.Groups(1).caption = sRestriccion
        Ador.MoveNext
        sdbgVarsRestric.Groups(2).caption = Ador(0).Value                                'S�lo Aviso
        Ador.MoveNext
        sInferior = Ador(0).Value                                                               'Inferior
        sdbgVarsRestric.Groups(1).Columns("R_INF").caption = sInferior
        sdbgVarsRestric.Groups(2).Columns("A_INF").caption = sInferior
        Ador.MoveNext
        sSuperior = Ador(0).Value                                                               'Superior
        sdbgVarsRestric.Groups(1).Columns("R_SUP").caption = sSuperior
        sdbgVarsRestric.Groups(2).Columns("A_SUP").caption = sSuperior
        Ador.MoveNext
        sdbgVarsRestric.Groups(2).Columns("AVISO").caption = Ador(0).Value               'Aviso
        Me.lblAdjAviso.caption = UCase(Ador(0).Value)
        Ador.MoveNext
        m_sVarNivel = Ador(0).Value                                                             'Variable de nivel
        Ador.MoveNext
        m_sNivel = Ador(0).Value                                                                'Nivel
        Ador.MoveNext
        m_sBloqueo = Ador(0).Value                                                              'Bloqueo de adjudicaciones para valores:
        Ador.MoveNext
        m_sMayoresDe = Ador(0).Value                                                            'mayor o igual que X
        Ador.MoveNext
        m_sMenoresDe = Ador(0).Value                                                            'menor o igual que Y
        Ador.MoveNext
        m_sAviso = Ador(0).Value                                                                'Aviso en PUNTUACI�N:
        Ador.MoveNext
        m_sValorProv = " " & Ador(0).Value                                                      'Valor proveedor
        Ador.MoveNext
        lblAdjProhibida.caption = Ador(0).Value                                                 'ADJUDICACI�N PROHIBIDA
        Ador.MoveNext
        m_sProveVarNoVisible1 = Ador(0).Value                                                   'Exixten proveedores que no cumplen los umbrales de calidad
        Ador.MoveNext
        m_sProveVarNoVisible2 = Ador(0).Value                                                   'para las siguientes variables no visibles:
        Ador.MoveNext
        m_sIdiGeneral = Ador(0).Value                                                           'General
        Ador.MoveNext
        m_sIdiGenComp = Ador(0).Value                                                           'Generando hoja comparativa...
        Ador.MoveNext
        m_sIdiAbrExcel = Ador(0).Value                                                          'Abriendo Microsoft Excel...
        Ador.MoveNext
        m_sIdiProceso = Ador(0).Value                                                           'Proceso:
        Ador.MoveNext
        m_sIdiInicial = Ador(0).Value                                                           'Vista inicial
        Ador.MoveNext
        Me.lblVista.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkTodosProves.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkTodasVariables.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkVerPuntos.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkVerCalificacion.caption = Ador(0).Value
        Ador.MoveNext
        sdbgVarsQAProve.Groups(1).caption = Ador(0).Value
        sdbgProveVarsQA.Groups(1).caption = Ador(0).Value
        m_sIdiUnidad = Ador(0).Value
        Ador.MoveNext
        Me.chkTodasUnidades.caption = Ador(0).Value
        Me.chkTodasUnidades_GridPv.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkMostrarVarsMaterialProceso.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblProve.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblVarCal.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblUnidad.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblPuntProve.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgVarsQAProve.Groups(1).Columns("UNQA").caption = Ador(0).Value
        Me.sdbgProveVarsQA.Groups(1).Columns("UNQA").caption = Ador(0).Value
        m_sIdiUnidadCN = Ador(0).Value
        Ador.MoveNext
        lblAlmenosUno.caption = Ador(0).Value
        lblNoVisibles.caption = m_sProveVarNoVisible1 & " " & m_sProveVarNoVisible2
        Ador.MoveNext
        m_sIdiGuardaNueva = Ador(0).Value
        Ador.MoveNext
        m_sIdiRenombrar = Ador(0).Value
        Ador.MoveNext
        m_sfrmWeb = Ador(0).Value
    End If
    
    Ador.Close
    Set Ador = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>
''' Crea la estructura de grupos/columnas para el grid VarsProve
''' </summary>
''' <returns>Rellena el grid</returns>
''' <remarks>Llamada desde frmComparativaQA.CargarGridVarCalidad() ; Tiempo m�ximo:0,4</remarks>
Private Sub CargarVarsQAProve()
    Dim oProv As CProveedor
    Dim i As Integer
    Dim iGroupIndex As Integer
    Dim ogroup As SSDataWidgets_B.Group
    Dim oColumn As SSDataWidgets_B.Column

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    'Limpiamos la grid
    sdbgVarsQAProve.RemoveAll
    i = sdbgVarsQAProve.Groups.Count
    While i > 2
        sdbgVarsQAProve.Groups.Remove (2)
        i = sdbgVarsQAProve.Groups.Count
    Wend

    iGroupIndex = 2

    sdbgVarsQAProve.Groups(0).Columns("DATOS_VAR").Width = 0
    sdbgVarsQAProve.Groups(0).HeadStyleSet = "Variables"

    sdbgVarsQAProve.Groups(1).HeadStyleSet = "Variables"

    For Each oProv In m_oProvesAsig
        'A�adimos el proveedor a la grid sdbgVarsQAProve
        sdbgVarsQAProve.Groups.Add iGroupIndex
        Set ogroup = sdbgVarsQAProve.Groups(iGroupIndex)
        ogroup.HeadStyleSet = "Grupo"
        ogroup.caption = oProv.Cod & " " & oProv.Den & " "
        ogroup.CaptionAlignment = ssCaptionAlignmentCenter
        ogroup.HeadStyleSet = "GrayHeadCabecera"
        ogroup.AllowSizing = True

        ogroup.Columns.Add 0
        Set oColumn = ogroup.Columns(0)
        oColumn.Name = "Calificaci�n" & oProv.Cod
        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
        oColumn.Alignment = ssCaptionAlignmentLeft
        oColumn.caption = m_sCalificacion
        oColumn.StyleSet = "Calificacion"
        oColumn.HeadStyleSet = "GrayHead"
        oColumn.Visible = m_oVistaCalSeleccionada.Calificaciones
        oColumn.AllowSizing = True
        oColumn.Locked = True
        DoEvents
        
        ogroup.Columns.Add 1
        Set oColumn = ogroup.Columns(1)
        oColumn.Name = "Valor" & oProv.Cod
        oColumn.CaptionAlignment = ssColCapAlignRightJustify
        oColumn.caption = m_sValor
        oColumn.StyleSet = "Valor"
        oColumn.HeadStyleSet = "GrayHead"
        oColumn.Alignment = ssCaptionAlignmentRight
        oColumn.NumberFormat = FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)
        oColumn.Visible = m_oVistaCalSeleccionada.Puntos
        oColumn.AllowSizing = True
        oColumn.Locked = True
        DoEvents
        
        
        pmCargarColumnaAVISORESTRIC oColumn, ogroup, oProv.Cod, 2
        
        ogroup.TagVariant = oProv.Cod  'almacena en el tag del grupo el c�digo del proveedor

        DoEvents

        iGroupIndex = iGroupIndex + 1

    Next

    'Ponemos el splitter visible
    If sdbgVarsQAProve.Groups.Count > 2 Then
        sdbgVarsQAProve.SplitterVisible = True
        sdbgVarsQAProve.SplitterPos = 2
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CargarVarsQAProve", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Crea la estructura de grupos/columnas para el grid ProveVars
''' </summary>
''' <remarks>Llamada desde: frmComparativaQA.CargarGridVarCalidad() ; Tiempo m�ximo:0,4</remarks>
Private Sub CargarProveVarsQA()
    Dim iGroupIndex As Integer
    Dim ogroup As SSDataWidgets_B.Group
    Dim oColumn As SSDataWidgets_B.Column
    Dim bVisibleVista As Boolean
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Dim bAlgunaVar As Boolean
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Dim oGroupProveedor As New SSDataWidgets_B.Group
    Dim oColumnCod As New SSDataWidgets_B.Column
    Dim oColumnDen As New SSDataWidgets_B.Column
    
    Set oGroupProveedor = sdbgProveVarsQA.Groups(0)
    Set oColumnCod = sdbgProveVarsQA.Groups(0).Columns(0)
    Set oColumnDen = sdbgProveVarsQA.Groups(0).Columns(1)
    
    Dim oGroupUnidades As New SSDataWidgets_B.Group
    Dim oColumnUnidades As New SSDataWidgets_B.Column
    Dim oColumnBtUnidades As New SSDataWidgets_B.Column
    
    Set oGroupUnidades = sdbgProveVarsQA.Groups(1)
    Set oColumnUnidades = sdbgProveVarsQA.Groups(1).Columns(0)
    Set oColumnBtUnidades = sdbgProveVarsQA.Groups(1).Columns(1)
    
    sdbgProveVarsQA.Reset
    
    bAlgunaVar = False
    
    iGroupIndex = 0
    
    sdbgProveVarsQA.Groups.Add iGroupIndex
    Set ogroup = sdbgProveVarsQA.Groups(iGroupIndex)
    ogroup.caption = oGroupProveedor.caption
    ogroup.CaptionAlignment = oGroupProveedor.CaptionAlignment
    ogroup.Visible = oGroupProveedor.Visible
    ogroup.AllowSizing = oGroupProveedor.AllowSizing
    ogroup.HeadStyleSet = "Proveedor"
    ogroup.StyleSet = oGroupProveedor.StyleSet
    
    ogroup.Columns.Add 0
    Set oColumn = ogroup.Columns(0)
    oColumn.Name = oColumnCod.Name
    oColumn.CaptionAlignment = oColumnCod.CaptionAlignment
    oColumn.Alignment = oColumnCod.Alignment
    oColumn.caption = oColumnCod.caption
    oColumn.StyleSet = oColumnCod.StyleSet
    oColumn.HeadStyleSet = oColumnCod.HeadStyleSet
    oColumn.HeadBackColor = oColumnCod.HeadBackColor
    oColumn.HeadForeColor = oColumnCod.HeadForeColor
    oColumn.Backcolor = oColumnCod.Backcolor
    oColumn.Forecolor = oColumnCod.Forecolor
    oColumn.Visible = oColumnCod.Visible
    oColumn.AllowSizing = oColumnCod.AllowSizing
    oColumn.Locked = oColumnCod.Locked
    DoEvents

    ogroup.Columns.Add 1
    Set oColumn = ogroup.Columns(1)
    oColumn.Name = oColumnDen.Name
    oColumn.CaptionAlignment = oColumnDen.CaptionAlignment
    oColumn.Alignment = oColumnDen.Alignment
    oColumn.caption = oColumnDen.caption
    oColumn.StyleSet = oColumnDen.StyleSet
    oColumn.HeadStyleSet = oColumnDen.HeadStyleSet
    oColumn.HeadBackColor = oColumnDen.HeadBackColor
    oColumn.HeadForeColor = oColumnDen.HeadForeColor
    oColumn.Backcolor = oColumnDen.Backcolor
    oColumn.Forecolor = oColumnDen.Forecolor
    oColumn.Visible = oColumnDen.Visible
    oColumn.AllowSizing = oColumnDen.AllowSizing
    oColumn.Locked = oColumnDen.Locked
    DoEvents
    
    iGroupIndex = 1
    
    sdbgProveVarsQA.Groups.Add iGroupIndex
    Set ogroup = sdbgProveVarsQA.Groups(iGroupIndex)
    ogroup.caption = oGroupUnidades.caption
    ogroup.CaptionAlignment = oGroupUnidades.CaptionAlignment
    ogroup.Visible = oGroupUnidades.Visible
    ogroup.AllowSizing = oGroupUnidades.AllowSizing
    ogroup.HeadStyleSet = "Unqa"
    ogroup.StyleSet = oGroupUnidades.StyleSet
    
    ogroup.Columns.Add 0
    Set oColumn = ogroup.Columns(0)
    oColumn.Name = oColumnUnidades.Name
    oColumn.CaptionAlignment = oColumnUnidades.CaptionAlignment
    oColumn.Alignment = oColumnUnidades.Alignment
    oColumn.caption = oColumnUnidades.caption
    oColumn.StyleSet = oColumnUnidades.StyleSet
    oColumn.HeadStyleSet = oColumnUnidades.HeadStyleSet
    oColumn.HeadBackColor = oColumnUnidades.HeadBackColor
    oColumn.HeadForeColor = oColumnUnidades.HeadForeColor
    oColumn.Backcolor = oColumnUnidades.Backcolor
    oColumn.Forecolor = oColumnUnidades.Forecolor
    oColumn.Visible = oColumnUnidades.Visible
    oColumn.AllowSizing = oColumnUnidades.AllowSizing
    oColumn.Locked = oColumnUnidades.Locked
    DoEvents

    ogroup.Columns.Add 1
    Set oColumn = ogroup.Columns(1)
    oColumn.Name = oColumnBtUnidades.Name
    oColumn.CaptionAlignment = oColumnBtUnidades.CaptionAlignment
    oColumn.Alignment = oColumnBtUnidades.Alignment
    oColumn.caption = oColumnBtUnidades.caption
    oColumn.StyleSet = oColumnBtUnidades.StyleSet
    oColumn.HeadStyleSet = oColumnBtUnidades.HeadStyleSet
    oColumn.HeadBackColor = oColumnBtUnidades.HeadBackColor
    oColumn.HeadForeColor = oColumnBtUnidades.HeadForeColor
    oColumn.Backcolor = oColumnBtUnidades.Backcolor
    oColumn.Forecolor = oColumnBtUnidades.Forecolor
    oColumn.Visible = oColumnBtUnidades.Visible
    oColumn.AllowSizing = oColumnBtUnidades.AllowSizing
    oColumn.Locked = oColumnBtUnidades.Locked
    DoEvents
    
    'Carga las columnas de la grid sdbgProveVarsQA
    For Each m_oVarCal0 In m_oVarsCalidad
    
        If m_oVarCal0.Nivel = 0 Then
        
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_oVarCal0.Id)
        If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0) Is Nothing Then
            bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
        Else
            bVisibleVista = False
        End If
        iGroupIndex = iGroupIndex + 1
        'A�adimos el grupo a la grid sdbgProveVarsQA
        sdbgProveVarsQA.Groups.Add iGroupIndex
        Set ogroup = sdbgProveVarsQA.Groups(iGroupIndex)
        ogroup.caption = " " & m_oVarCal0.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        ogroup.HeadStyleSet = "Grupo"
        ogroup.CaptionAlignment = ssCaptionAlignmentCenter
        ogroup.Visible = (m_oVarCal0.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista
        ogroup.AllowSizing = True
        
        bAlgunaVar = bAlgunaVar Or ogroup.Visible
        
        ogroup.Columns.Add 0
        Set oColumn = ogroup.Columns(0)
        oColumn.Name = "Calif0" & m_oVarCal0.Id
        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
        oColumn.Alignment = ssCaptionAlignmentLeft
        oColumn.caption = m_sCalificacion
        oColumn.StyleSet = "Calificacion"
        oColumn.HeadStyleSet = "GrayHead"
        oColumn.Visible = m_oVistaCalSeleccionada.Calificaciones
        oColumn.AllowSizing = True
        oColumn.Locked = True
        DoEvents

        ogroup.Columns.Add 1
        Set oColumn = ogroup.Columns(1)
        oColumn.Name = "Valor0" & m_oVarCal0.Id
        oColumn.CaptionAlignment = ssColCapAlignRightJustify
        oColumn.caption = m_sValor
        oColumn.StyleSet = "Valor"
        oColumn.HeadStyleSet = "GrayHead"
        oColumn.Alignment = ssCaptionAlignmentRight
        oColumn.NumberFormat = FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)
        oColumn.Visible = m_oVistaCalSeleccionada.Puntos
        oColumn.AllowSizing = True
        oColumn.Locked = True
        DoEvents
        
        'Columna que contiene informaci�n de la variable de calidad
        '(RESTRICCION, R_INF, R_SUP, AVISO, A_INF, A_SUP, ID, ID_VARCAL1, ID_VARCAL2, ID_VARCAL3, ID_VARCAL4, NIVEL)
        ogroup.Columns.Add 2
        Set oColumn = ogroup.Columns(2)
        oColumn.Name = "DATOS_VAR0" & m_oVarCal0.Id
        oColumn.CaptionAlignment = ssColCapAlignRightJustify
        oColumn.Visible = False
        DoEvents
        
        'Para Mostrar el tooltip
        pmCargarColumnaAVISORESTRIC oColumn, ogroup, "0" & CStr(m_oVarCal0.Id), 3
        
    
        For Each m_oVarCal1 In m_oVarsCalidad
        If m_oVarCal1.Nivel <> 0 Then
            scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(m_oVarCal1.Id)
            If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).Visible
                
                
                If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal1.MaterialProceso Then
                    bVisibleVista = False
                End If
            Else
                bVisibleVista = False
            End If
            iGroupIndex = iGroupIndex + 1
            'A�adimos el grupo a la grid sdbgProveVarsQA
            sdbgProveVarsQA.Groups.Add iGroupIndex
            Set ogroup = sdbgProveVarsQA.Groups(iGroupIndex)
            ogroup.caption = m_oVarCal1.Cod & " " & m_oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            ogroup.HeadStyleSet = "Grupo"
            ogroup.CaptionAlignment = ssCaptionAlignmentCenter
            ogroup.Visible = (m_oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista
            ogroup.AllowSizing = True
            
            bAlgunaVar = bAlgunaVar Or ogroup.Visible
            
            ogroup.Columns.Add 0
            Set oColumn = ogroup.Columns(0)
            oColumn.Name = "Calif" & m_oVarCal1.Id
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            oColumn.Alignment = ssCaptionAlignmentLeft
            oColumn.caption = m_sCalificacion
            oColumn.StyleSet = "Calificacion"
            oColumn.HeadStyleSet = "GrayHead"
            oColumn.Visible = m_oVistaCalSeleccionada.Calificaciones
            oColumn.AllowSizing = True
            oColumn.Locked = True
            DoEvents
    
            ogroup.Columns.Add 1
            Set oColumn = ogroup.Columns(1)
            oColumn.Name = "Valor" & m_oVarCal1.Id
            oColumn.CaptionAlignment = ssColCapAlignRightJustify
            oColumn.caption = m_sValor
            oColumn.StyleSet = "Valor"
            oColumn.HeadStyleSet = "GrayHead"
            oColumn.Alignment = ssCaptionAlignmentRight
            oColumn.NumberFormat = FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)
            oColumn.Visible = m_oVistaCalSeleccionada.Puntos
            oColumn.AllowSizing = True
            oColumn.Locked = True
            DoEvents
        
            'Columna que contiene informaci�n de la variable de calidad
            '(RESTRICCION, R_INF, R_SUP, AVISO, A_INF, A_SUP, ID, ID_VARCAL1, ID_VARCAL2, ID_VARCAL3, ID_VARCAL4, NIVEL)
            ogroup.Columns.Add 2
            Set oColumn = ogroup.Columns(2)
            oColumn.Name = "DATOS_VAR" & m_oVarCal1.Id
            oColumn.CaptionAlignment = ssColCapAlignRightJustify
            oColumn.Visible = False
            DoEvents
            'Para Mostrar el tooltip
            pmCargarColumnaAVISORESTRIC oColumn, ogroup, m_oVarCal1.Id, 3
        
            If Not m_oVarCal1.VariblesCal Is Nothing Then
                For Each m_oVarCal2 In m_oVarCal1.VariblesCal
                    scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(m_oVarCal2.Id)
                    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2) Is Nothing Then
                        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                            If .VariblesCalVista Is Nothing Then
                                bVisibleVista = False
                            Else
                                If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal2.MaterialProceso Then
                                    bVisibleVista = False
                                Else
                                    If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                        bVisibleVista = .VariblesCalVista.Item(scod2).Visible
                                    Else
                                        bVisibleVista = False
                                    End If
                                End If
                            End If
                        End With
                    End If
                    iGroupIndex = iGroupIndex + 1
                    'A�adimos el grupo a la grid sdbgProveVarsQA
                    sdbgProveVarsQA.Groups.Add iGroupIndex
                    Set ogroup = sdbgProveVarsQA.Groups(iGroupIndex)
                    ogroup.caption = m_oVarCal2.Cod & " " & m_oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                    ogroup.HeadStyleSet = "Grupo"
                    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
                    ogroup.Visible = (m_oVarCal2.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista
                    ogroup.AllowSizing = True
                    
                    bAlgunaVar = bAlgunaVar Or ogroup.Visible
                    
                    ogroup.Columns.Add 0
                    Set oColumn = ogroup.Columns(0)
                    oColumn.Name = "Calif" & m_oVarCal1.Id & "-" & m_oVarCal2.Id
                    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                    oColumn.Alignment = ssCaptionAlignmentLeft
                    oColumn.caption = m_sCalificacion
                    oColumn.StyleSet = "Calificacion"
                    oColumn.HeadStyleSet = "GrayHead"
                    oColumn.Visible = m_oVistaCalSeleccionada.Calificaciones
                    oColumn.AllowSizing = True
                    oColumn.Locked = True
                    DoEvents
            
                    ogroup.Columns.Add 1
                    Set oColumn = ogroup.Columns(1)
                    oColumn.Name = "Valor" & m_oVarCal1.Id & "-" & m_oVarCal2.Id
                    oColumn.CaptionAlignment = ssColCapAlignRightJustify
                    oColumn.caption = m_sValor
                    oColumn.StyleSet = "Valor"
                    oColumn.HeadStyleSet = "GrayHead"
                    oColumn.Alignment = ssCaptionAlignmentRight
                    oColumn.NumberFormat = FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)
                    oColumn.Visible = m_oVistaCalSeleccionada.Puntos
                    oColumn.AllowSizing = True
                    oColumn.Locked = True
                    DoEvents
    
                    ogroup.Columns.Add 2
                    Set oColumn = ogroup.Columns(2)
                    oColumn.Name = "DATOS_VAR" & m_oVarCal1.Id & "-" & m_oVarCal2.Id
                    oColumn.CaptionAlignment = ssColCapAlignRightJustify
                    oColumn.Visible = False
                    DoEvents
                    'Para Mostrar el tooltip
                    pmCargarColumnaAVISORESTRIC oColumn, ogroup, m_oVarCal1.Id & "-" & m_oVarCal2.Id, 3
                    If Not m_oVarCal2.VariblesCal Is Nothing Then
                        For Each m_oVarCal3 In m_oVarCal2.VariblesCal
                            scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(m_oVarCal3.Id)
                            If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3) Is Nothing Then
                                With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                    If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                        With .VariblesCalVista.Item(scod2)
                                        If .VariblesCalVista Is Nothing Then
                                            bVisibleVista = False
                                        Else
                                            If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal3.MaterialProceso Then
                                                bVisibleVista = False
                                            Else
                                                If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                    bVisibleVista = .VariblesCalVista.Item(scod3).Visible
                                                Else
                                                    bVisibleVista = False
                                                End If
                                            End If
                                        End If
                                        End With
                                    End If
                                End With
                            End If
                            iGroupIndex = iGroupIndex + 1
                            'A�adimos el grupo a la grid sdbgProveVarsQA
                            sdbgProveVarsQA.Groups.Add iGroupIndex
                            Set ogroup = sdbgProveVarsQA.Groups(iGroupIndex)
                            ogroup.caption = m_oVarCal3.Cod & " " & m_oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                            ogroup.HeadStyleSet = "Grupo"
                            ogroup.CaptionAlignment = ssCaptionAlignmentCenter
                            ogroup.Visible = (m_oVarCal3.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista
                            ogroup.AllowSizing = True
            
                            bAlgunaVar = bAlgunaVar Or ogroup.Visible
                                                        
                            ogroup.Columns.Add 0
                            Set oColumn = ogroup.Columns(0)
                            oColumn.Name = "Calif" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id
                            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                            oColumn.Alignment = ssCaptionAlignmentLeft
                            oColumn.caption = m_sCalificacion
                            oColumn.StyleSet = "Calificacion"
                            oColumn.HeadStyleSet = "GrayHead"
                            oColumn.Visible = m_oVistaCalSeleccionada.Calificaciones
                            oColumn.AllowSizing = True
                            oColumn.Locked = True
                            DoEvents
                    
                            ogroup.Columns.Add 1
                            Set oColumn = ogroup.Columns(1)
                            oColumn.Name = "Valor" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id
                            oColumn.CaptionAlignment = ssColCapAlignRightJustify
                            oColumn.caption = m_sValor
                            oColumn.StyleSet = "Valor"
                            oColumn.HeadStyleSet = "GrayHead"
                            oColumn.Alignment = ssCaptionAlignmentRight
                            oColumn.NumberFormat = FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)
                            oColumn.Visible = m_oVistaCalSeleccionada.Puntos
                            oColumn.AllowSizing = True
                            oColumn.Locked = True
                            DoEvents
    
                            ogroup.Columns.Add 2
                            Set oColumn = ogroup.Columns(2)
                            oColumn.Name = "DATOS_VAR" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id
                            oColumn.CaptionAlignment = ssColCapAlignRightJustify
                            oColumn.Visible = False
                            DoEvents
                            'Para Mostrar el tooltip
                            pmCargarColumnaAVISORESTRIC oColumn, ogroup, m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id, 3
                            If Not m_oVarCal3.VariblesCal Is Nothing Then
                                For Each m_oVarCal4 In m_oVarCal3.VariblesCal
                                    scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(m_oVarCal4.Id)
                                    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                                        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                With .VariblesCalVista.Item(scod2)
                                                    If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                        With .VariblesCalVista.Item(scod3)
                                                        If .VariblesCalVista Is Nothing Then
                                                            bVisibleVista = False
                                                        Else
                                                            If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal4.MaterialProceso Then
                                                                bVisibleVista = False
                                                            Else
                                                                If Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                                                    bVisibleVista = .VariblesCalVista.Item(scod4).Visible
                                                                Else
                                                                    bVisibleVista = False
                                                                End If
                                                            End If
                                                        End If
                                                        End With
                                                    End If
                                                End With
                                            End If
                                        End With
                                    End If
                                    iGroupIndex = iGroupIndex + 1
                                    'A�adimos el grupo a la grid sdbgProveVarsQA
                                    sdbgProveVarsQA.Groups.Add iGroupIndex
                                    Set ogroup = sdbgProveVarsQA.Groups(iGroupIndex)
                                    ogroup.caption = m_oVarCal4.Cod & " " & m_oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                    ogroup.HeadStyleSet = "Grupo"
                                    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
                                    ogroup.Visible = (m_oVarCal4.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista
                                    ogroup.AllowSizing = True
                                    
                                    bAlgunaVar = bAlgunaVar Or ogroup.Visible
                                    
                                    ogroup.Columns.Add 0
                                    Set oColumn = ogroup.Columns(0)
                                    oColumn.Name = "Calif" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id & "-" & m_oVarCal4.Id
                                    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                                    oColumn.Alignment = ssCaptionAlignmentLeft
                                    oColumn.caption = m_sCalificacion
                                        oColumn.StyleSet = "Calificacion"
                                    oColumn.HeadStyleSet = "GrayHead"
                                    oColumn.Visible = m_oVistaCalSeleccionada.Calificaciones
                                    oColumn.AllowSizing = True
                                    oColumn.Locked = True
                                    DoEvents
    
                                    ogroup.Columns.Add 1
                                    Set oColumn = ogroup.Columns(1)
                                    oColumn.Name = "Valor" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id & "-" & m_oVarCal4.Id
                                    oColumn.CaptionAlignment = ssColCapAlignRightJustify
                                    oColumn.caption = m_sValor
                                    oColumn.StyleSet = "Valor"
                                    oColumn.HeadStyleSet = "GrayHead"
                                    oColumn.Alignment = ssCaptionAlignmentRight
                                    oColumn.NumberFormat = FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)
                                    oColumn.Visible = m_oVistaCalSeleccionada.Puntos
                                    oColumn.AllowSizing = True
                                    oColumn.Locked = True
                                    DoEvents
    
                                    ogroup.Columns.Add 2
                                    Set oColumn = ogroup.Columns(2)
                                    oColumn.Name = "DATOS_VAR" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id & "-" & m_oVarCal4.Id
                                    oColumn.CaptionAlignment = ssColCapAlignRightJustify
                                    oColumn.Visible = False
                                    DoEvents
                                    'Para Mostrar el tooltip
                                    pmCargarColumnaAVISORESTRIC oColumn, ogroup, m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id & "-" & m_oVarCal4.Id, 3
                                    If Not m_oVarCal4.VariblesCal Is Nothing Then
                                        For Each m_oVarCal5 In m_oVarCal4.VariblesCal
                                            scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & CStr(m_oVarCal5.Id)
                                            If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                                                With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                                    If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                        With .VariblesCalVista.Item(scod2)
                                                            If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                                With .VariblesCalVista.Item(scod3)
                                                                    If Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                                                        With .VariblesCalVista.Item(scod4)
                                                                        If .VariblesCalVista Is Nothing Then
                                                                            bVisibleVista = False
                                                                        Else
                                                                            If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal5.MaterialProceso Then
                                                                                bVisibleVista = False
                                                                            Else
                                                                                If Not .VariblesCalVista.Item(scod5) Is Nothing Then
                                                                                    bVisibleVista = .VariblesCalVista.Item(scod5).Visible
                                                                                Else
                                                                                    bVisibleVista = False
                                                                                End If
                                                                            End If
                                                                        End If
                                                                        End With
                                                                    End If
                                                                End With
                                                            End If
                                                        End With
                                                    End If
                                                End With
                                            End If
                                            iGroupIndex = iGroupIndex + 1
                                            'A�adimos el grupo a la grid sdbgProveVarsQA
                                            sdbgProveVarsQA.Groups.Add iGroupIndex
                                            Set ogroup = sdbgProveVarsQA.Groups(iGroupIndex)
                                            ogroup.caption = m_oVarCal5.Cod & " " & m_oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                            ogroup.HeadStyleSet = "Grupo"
                                            ogroup.CaptionAlignment = ssCaptionAlignmentCenter
                                            ogroup.Visible = (m_oVarCal5.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista
                                            ogroup.AllowSizing = True
                                            
                                            bAlgunaVar = bAlgunaVar Or ogroup.Visible
                                            
                                            ogroup.Columns.Add 0
                                            Set oColumn = ogroup.Columns(0)
                                            oColumn.Name = "Calif" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id & "-" & m_oVarCal4.Id & "-" & m_oVarCal5.Id
                                            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                                            oColumn.Alignment = ssCaptionAlignmentLeft
                                            oColumn.caption = m_sCalificacion
                                            oColumn.StyleSet = "Calificacion"
                                            oColumn.HeadStyleSet = "GrayHead"
                                            oColumn.Visible = m_oVistaCalSeleccionada.Calificaciones
                                            oColumn.AllowSizing = True
                                            oColumn.Locked = True
                                            DoEvents
    
                                            ogroup.Columns.Add 1
                                            Set oColumn = ogroup.Columns(1)
                                            oColumn.Name = "Valor" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id & "-" & m_oVarCal4.Id & "-" & m_oVarCal5.Id
                                            oColumn.CaptionAlignment = ssColCapAlignRightJustify
                                            oColumn.caption = m_sValor
                                            oColumn.StyleSet = "Valor"
                                            oColumn.HeadStyleSet = "GrayHead"
                                            oColumn.Alignment = ssCaptionAlignmentRight
                                            oColumn.NumberFormat = FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)
                                            oColumn.Visible = m_oVistaCalSeleccionada.Puntos
                                            oColumn.AllowSizing = True
                                            oColumn.Locked = True
                                            DoEvents
                                            
                                            ogroup.Columns.Add 2
                                            Set oColumn = ogroup.Columns(2)
                                            oColumn.Name = "DATOS_VAR" & m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id & "-" & m_oVarCal4.Id & "-" & m_oVarCal5.Id
                                            oColumn.CaptionAlignment = ssColCapAlignRightJustify
                                            oColumn.Visible = False
                                            DoEvents
                                            'Para Mostrar el tooltip
                                            pmCargarColumnaAVISORESTRIC oColumn, ogroup, m_oVarCal1.Id & "-" & m_oVarCal2.Id & "-" & m_oVarCal3.Id & "-" & m_oVarCal4.Id & "-" & m_oVarCal5.Id, 3
                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If

            ogroup.TagVariant = m_oVarCal1.Cod    'almacena en el tag del grupo el c�digo del grupo
            DoEvents
            End If
        Next
        
        ogroup.TagVariant = m_oVarCal0.Cod    'almacena en el tag del grupo el c�digo del grupo
        DoEvents

    End If
    Next

    'Ponemos el splitter visible
    If sdbgProveVarsQA.Groups.Count > 1 Then
        sdbgProveVarsQA.SplitterVisible = True
        If bAlgunaVar Then sdbgProveVarsQA.SplitterPos = 2
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CargarProveVarsQA", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Oculta el tooltip de los intervalos y los diferentes arboles de selecci�n. De haberse cambiado algo en la
''' configuraci�n de la vista o visibilidades "desligadas de la vista" aplica el cambio en los grids
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde esta  el rat�n</param>
''' <param name="Y">Coordenada X donde esta  el rat�n</param>
''' <remarks>Llamada desde: Evento que salta al mover el rat�n sobre el grid; Tiempo m�ximo:0</remarks>
Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'Screen.MousePointer = vbNormal
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picNoVisibles.Visible = False
    If Not m_bDeshabilidar_MouseMove Then picUmbrales.Visible = False
    PicVarsQA.Visible = False
    If Not m_bDeshabilidar_MouseMove Then picUnqa.Visible = False
    If m_bVistaCambiaUnqas Then
        m_bVistaCambiaUnqas = False
        
        Set m_oOcultarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
        Set m_oMostrarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
        
        Rellenar_sdbgVarsQAProve
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    If Not m_bDeshabilidar_MouseMove Then picUnqa_GridPv.Visible = False
    If m_bVistaCambiaUnqas_GridPv Then
        m_bVistaCambiaUnqas_GridPv = False
        
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    picVarCal.Visible = False
    If m_bVistaCambiaVarCal Then
        m_bVistaCambiaVarCal = False
        
        Rellenar_sdbgVarsQAProve
        
        OcultarMostrarVarCal
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    picProves.Visible = False
    If m_bVistaCambiaProve Then
        m_bVistaCambiaProve = False
        
        OcultarMostrarProveedores
        
        Rellenar_sdbgVarsQAProve
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Form_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Al cambiar de tama�o la pantalla, debe cambiar de tama�o los grids y reposionarse todo.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo: 0</remarks>
Private Sub Form_Resize()
    Dim Wi As Integer
    
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.WindowState = vbMinimized Then Exit Sub
        
    If Me.Height < 4695 Then
        Me.Height = 4695
    Else
        sdbgVarsQAProve.Height = Me.Height - 1305
        sdbgProveVarsQA.Height = Me.Height - 1305
    End If
        
    Wi = Me.lblVista.Width + Me.lblVista.Left + 30
    Wi = Wi + Me.sdbcVistaActual.Width + Me.sdbcVistaActual.Left + 30
    Wi = Wi + Me.cmdGuardarVista.Width + 30 + Me.cmdGuardarVistaNueva.Width + 30
    Wi = Wi + Me.cmdEliminarVista.Width + 30 + Me.cmdRenombrarVista.Width + 30
    Wi = Wi + Me.cmdVistaDefecto.Width + 30 + Me.cmdInvertir.Width + 30 + Me.cmdExcel.Width + 180
    If Me.Width < Wi Then
        Me.Width = Wi + 80
    End If
    
    sdbgVarsQAProve.Width = Me.Width - 350
    sdbgProveVarsQA.Width = Me.Width - 350
    
    Me.frameVista.Width = Me.Width - 350
        
    If Not m_oVistaCalSeleccionada Is Nothing Then
        ConfigurarBotonesVistas m_oVistaCalSeleccionada.TipoVista
    End If
    
    '''Se utiliza esta variable de control para que siolo haga esto una vez,
    '''El tema es que el evento rowloaded de las grid, no se ejecutan hasta que el formuliario est� visible
    '''o eso creo, y entonces no me coge los valores reales, al hacer esto conseguimos que las columnas de
    '''aviso/restricciones, que sirven para mostrar el tooltip, se visualicen o no en funci�n de si alguna de las
    '''variables tiene avisos/restricciones
    If Not m_bActivadoParaGrid And Me.Visible Then
        m_bActivadoParaGrid = True
        sdbgVarsQAProve.Refresh
        sdbgProveVarsQA.Refresh
        RedimensionarGrid
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>
''' Si se sale sin grabar los cambios en las vistas, avisa dando opci�n a grabar en bbdd los cambios en vista
''' Los cambios en visibilidades "desligadas de la vista" no se graban.
''' </summary>
''' <param name="Cancel">Cancel la descarga</param>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Dim irespuesta As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bDescargarFrm = False Then
    If Not m_bDeshabilidar_Form_Deactivate Then
        If m_udtTipoVQA <> Vistainicialqa And (m_oVistaCalSeleccionada.HayCambios Or m_bCambios) And m_bPermitirVistasCompQA Then
            m_oVistaCalSeleccionada.HayCambios = False
            m_bCambios = False
            m_bSetFocusGrid = False
            irespuesta = oMensajes.PreguntaGuardarVistas
            If irespuesta = vbYes Then
    
                If m_oProcesoSeleccionado Is Nothing Then
                    Exit Sub
                End If
    
                If Not m_oVistaCalSeleccionada Is Nothing Then
                    GuardarVistaGeneral
                    GuardarVistaGeneralEnBd
                End If
            End If
            m_bSetFocusGrid = True
        End If
        
        Call oUsuarioSummit.GuardarDimensionFormVistasQA(Me.Width, Me.Height)
        
    End If
End If
        m_bDescargarFrm = False
    Set m_oVarsCalidad = Nothing
    Set m_oVistaCalSeleccionada = Nothing
    Set m_oFos = Nothing
    
    m_bEsResReu = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Una vez seleccionada una vista, la aplica en pantalla.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbcVistaActual_CloseUp()
    Dim iCod As Long
    Dim iTipo As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub

    If sdbcVistaActual.Columns("COD").Value = "" Then
        
        sdbcVistaActual.Columns("COD").Value = CStr(m_oVistaCalSeleccionada.Vista)
        
        Exit Sub
    End If
    
    If CInt(sdbcVistaActual.Columns("COD").Value) = CInt(m_oVistaCalSeleccionada.Vista) Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    LockWindowUpdate Me.hWnd
    
    ''defecto''
    If Not m_bBtDefectoCanDisabled Then
        Me.cmdVistaDefecto.Enabled = True
    ElseIf CInt(Me.sdbcVistaActual.Columns("COD").Value) = CInt(m_oProcesoSeleccionado.VistaDefectoQA) Then
        Me.cmdVistaDefecto.Enabled = False
    Else
        Me.cmdVistaDefecto.Enabled = True
    End If
    ''
    
    iCod = sdbcVistaActual.Columns("COD").Value
    iTipo = sdbcVistaActual.Columns("TIPO").Value
    
    If Not m_oConfVistasCalGlobal.Item(CStr(sdbcVistaActual.Columns("COD").Value)) Is Nothing Then
        m_oConfVistasCalGlobal.Remove CStr(sdbcVistaActual.Columns("COD").Value)
    End If
    
    ConfiguracionVistaActualQA sdbcVistaActual.Columns("COD").Value, sdbcVistaActual.Columns("TIPO").Value

    m_bDeshabilidar_Chk_ChangeTodas = True
    
    Me.chkVerCalificacion.Value = IIf(m_oVistaCalSeleccionada.Calificaciones, vbChecked, vbUnchecked)
    Me.chkVerPuntos.Value = IIf(m_oVistaCalSeleccionada.Puntos, vbChecked, vbUnchecked)
    
    m_bDeshabilidar_Chk_ChangeTodas = False
    
    m_bCambios = False

    m_bVistaInicialMantUnqa = False
    m_bVistaCambiaUnqas = False
    
    m_bVistaCambiaVarCal = False
    m_bVistaCambiaProve = False
    
    m_bGrpGenerarEstrucUnQaLimpiado = False
    m_bGrpGenerarEstrucUnQaPulsado = False
    m_bGrpGenerarEstrucUnQa1ra = False
    m_bGrpGenerarEstrucUnQaCambio = False
    
    Set m_oOcultarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
    Set m_oMostrarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores

    CargarGridVarCalidad
    
    If iTipo = 0 Then
        Me.caption = m_sCaption & " (" & m_sIdiInicial & ")"
    Else
        Me.caption = m_sCaption & " (" & sdbcVistaActual.Columns("VISTA").Value & ")"
    End If
    
    Form_Resize
    
    LockWindowUpdate 0&
    
    ConfigurarBotonesVistas m_oVistaCalSeleccionada.TipoVista

    sdbcVistaActual.Columns("COD").Value = iCod
    sdbcVistaActual.Columns("TIPO").Value = iTipo
    
    If m_oVistaCalSeleccionada Is Nothing Then
        g_sNombreVista = m_oProcesoSeleccionado.NombreVistaQA
    Else
        g_sNombreVista = m_oVistaCalSeleccionada.nombre
    End If
    
    If Me.sdbgVarsQAProve.Visible Then
        If Me.Visible Then Me.sdbgVarsQAProve.SetFocus
    Else
        If Me.Visible Then Me.sdbgProveVarsQA.SetFocus
    End If
        
    FocoAGrid
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbcVistaActual_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga el combo de vistas
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbcVistaActual_DropDown()
    Dim oVistaCal As CConfVistaCalGlobal
    Dim bTieneVistaInicial As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcVistaActual.RemoveAll
        
    Set m_oVistasCombo = Nothing
    Set m_oVistasCombo = oFSGSRaiz.Generar_CConfVistasCalGlobal
    
    If gParametrosGenerales.gbPymes Then
        m_oVistasCombo.CargarCombosVistas basOptimizacion.gPYMEUsuario
    Else
        m_oVistasCombo.CargarCombosVistas
    End If
    
    If m_oVistasCombo.Count <> 0 Then
        For Each oVistaCal In m_oVistasCombo
            If oVistaCal.TipoVista = TipoDeVistaqa.Vistainicialqa Then
                bTieneVistaInicial = True
                Exit For
            Else
                bTieneVistaInicial = False
            End If
        Next
        If Not bTieneVistaInicial Then
            sdbcVistaActual.AddItem m_sIdiInicial & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & TipoDeVistaqa.Vistainicialqa
        End If
        
        For Each oVistaCal In m_oVistasCombo
            Select Case oVistaCal.TipoVista
                Case TipoDeVistaqa.Vistainicialqa
                    sdbcVistaActual.AddItem m_sIdiInicial & Chr(m_lSeparador) & oVistaCal.Vista & Chr(m_lSeparador) & oVistaCal.TipoVista
                Case TipoDeVistaqa.otravistaqa
                    sdbcVistaActual.AddItem m_sVistaGlobal & " " & oVistaCal.nombre & Chr(m_lSeparador) & oVistaCal.Vista & Chr(m_lSeparador) & oVistaCal.TipoVista
            End Select
        Next
    Else
        sdbcVistaActual.AddItem m_sIdiInicial & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & TipoDeVistaqa.Vistainicialqa
    End If
    
    sdbcVistaActual.SelLength = Len(sdbcVistaActual.Text)
    sdbcVistaActual.Refresh
    
    Set oVistaCal = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbcVistaActual_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Configura el combo de vistas
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbcVistaActual_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcVistaActual.DataFieldList = "Column 0"
    sdbcVistaActual.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbcVistaActual_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Posicionar el combo segun el texto suministrado
''' </summary>
''' <param name="Text">Texto del combo</param>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo:0 </remarks>
Private Sub sdbcVistaActual_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcVistaActual.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVistaActual.Rows - 1
            bm = sdbcVistaActual.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVistaActual.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcVistaActual.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbcVistaActual_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Hace q todos los grupos de "variables" tengan el mismo aspecto
''' </summary>
''' <param name="ColIndex">Indice de columna</param>
''' <param name="Cancel">Cancel el redimensionamiento</param>
''' <remarks>Llamada desde: Evento de redimensionar una columna del grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgProveVarsQA_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
Dim iGrupo As Long
Dim igrupo_resized As Long
Dim sColumna As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     igrupo_resized = sdbgProveVarsQA.Columns(ColIndex).Group
    
    'Al redimensionar una columna tiene que redimensionar esa columna en todos los grupos
    If igrupo_resized = sdbgProveVarsQA.Groups(0).Position Then
        sColumna = sdbgProveVarsQA.Columns(ColIndex).Name
        Select Case sColumna
            Case "COD"
                sdbgProveVarsQA.Columns("COD").Width = sdbgProveVarsQA.ResizeWidth
            Case "DEN"
                sdbgProveVarsQA.Columns("NOMBRE").Width = sdbgProveVarsQA.ResizeWidth
        End Select
        m_bCambios = True
        Exit Sub
    End If

    If sdbgProveVarsQA.Columns(ColIndex).Group > 1 Then
        m_bCambios = True
        For iGrupo = 2 To sdbgProveVarsQA.Groups.Count - 1
            If iGrupo <> igrupo_resized Then
                sdbgProveVarsQA.Groups(iGrupo).Columns(0).Width = sdbgProveVarsQA.ResizeWidth
            End If
        Next iGrupo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgProveVarsQA_ColResize", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Si se clica en Proveedor (0) muestra el arbol de selecci�n de proveedor y checks de mostrar Calificaciones/Puntos
''' Si se clica en Unidades (1) muestra el arbol de selecci�n de unidades
''' Si se clica en un grupo de Variables (2..N) muestra el arbol de selecci�n de variables
''' </summary>
''' <param name="GrpIndex">Indice de grupo</param>
''' <remarks>Llamada desde: Evento de hacer clic en la cabecera de un grupo del grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgProveVarsQA_GrpHeadClick(ByVal GrpIndex As Integer)
    
    Dim Mouse As POINTAPI
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If GrpIndex = 0 Then

        m_bVistaCambiaProve = False
    
        GenerarEstructuraProves

        picProves.Top = Me.sdbgProveVarsQA.Top + 420
        picProves.Left = Me.sdbgProveVarsQA.Left
        
        picProves.Height = sdbgProveVarsQA.Height - 1080

        GetCursorPos Mouse
        SetCursorPos Mouse.X + 10, Mouse.Y + 30
        
        If DameNumeroProvesVisibles = 1 Then
            tvwProves.Height = picProves.Height - 1320 - Me.lblAlmenosUno.Height - 30
                        
            Me.lblAlmenosUno.Visible = True
        Else
            tvwProves.Height = picProves.Height - 1320
            
            Me.lblAlmenosUno.Visible = False
        End If
        
        Me.lblAlmenosUno.Top = picProves.Height - Me.lblAlmenosUno.Height - 80

        picProves.Width = 4200
           
        tvwProves.Width = picProves.Width - 250
        picProves.Visible = True

    ElseIf GrpIndex = 1 Then
        m_bVistaCambiaUnqas = False
        m_bGrpGenerarEstrucUnQaPulsado = True
                    
        GrpGenerarEstructuraUnidadesNegocio
                        
        picUnqa.Top = sdbgProveVarsQA.Top + 420
        picUnqa.Left = Me.sdbgProveVarsQA.Left + Me.sdbgProveVarsQA.Groups(0).Width + 380
        picUnqa.Height = sdbgProveVarsQA.Height - 1080
        
        GetCursorPos Mouse
        SetCursorPos Mouse.X + 10, Mouse.Y + 30
        
        tvwUndNegUsu.Height = picUnqa.Height - 650
        
        picUnqa.Width = 4800
        If sdbgProveVarsQA.Groups(1).Columns(0).Width > picUnqa.Width Then
            picUnqa.Width = 4800 + (sdbgProveVarsQA.Groups(1).Columns(0).Width - 4800)
        End If
        tvwUndNegUsu.Width = picUnqa.Width - 250
        picUnqa.Visible = True
    Else

        m_bVistaCambiaVarCal = False
        
        GenerarEstructuraVarCal

        picVarCal.Top = sdbgProveVarsQA.Top + 420

        picVarCal.Left = Me.sdbgProveVarsQA.Groups(GrpIndex).Left
        
        picVarCal.Height = sdbgProveVarsQA.Height - 1080

        GetCursorPos Mouse
        SetCursorPos Mouse.X + 10, Mouse.Y + 30

        tvwVarCal.Height = picVarCal.Height - 940

        picVarCal.Width = 5200
        
        If picVarCal.Width + picVarCal.Left > Me.sdbgProveVarsQA.Width Then
            picVarCal.Left = Me.sdbgProveVarsQA.Width - picVarCal.Width
        End If
        
        tvwVarCal.Width = picVarCal.Width - 250
        picVarCal.Visible = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgProveVarsQA_GrpHeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Hace q todos los grupos de "variables" tengan el mismo ancho
''' </summary>
''' <param name="GrpIndex">Indice de grupo</param>
''' <param name="Cancel">Cancel el redimensionamiento</param>
''' <remarks>Llamada desde: Evento de redimensionar el grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgProveVarsQA_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
Dim iGrupos As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If GrpIndex = 1 Then
        sdbgProveVarsQA.Groups(1).Columns(0).Width = sdbgProveVarsQA.ResizeWidth - CTE_WIDTH_3COLBT
        sdbgProveVarsQA.Groups(1).Columns(1).Width = CTE_WIDTH_3COLBT
    Else
        For iGrupos = 2 To sdbgProveVarsQA.Groups.Count - 1
            If iGrupos <> GrpIndex Then
                sdbgProveVarsQA.Groups(iGrupos).Width = sdbgProveVarsQA.ResizeWidth
            End If
        Next iGrupos
    End If
    
    m_bCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgProveVarsQA_GrpResize", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Oculta el tooltip de los intervalos y los diferentes arboles de selecci�n. De haberse cambiado algo en la
''' configuraci�n de la vista o visibilidades "desligadas de la vista" aplica el cambio en los grids
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde esta  el rat�n</param>
''' <param name="Y">Coordenada X donde esta  el rat�n</param>
''' <remarks>Llamada desde: Evento que salta al mover el rat�n sobre el grid; Tiempo m�ximo:0</remarks>
Private Sub sdbgProveVarsQA_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim sDatos_Variable() As String
Dim s As String
Dim vBookmark As Variant
Dim sValor As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DoEvents
    
    picNoVisibles.Visible = False
    If Not m_bDeshabilidar_MouseMove Then picUmbrales.Visible = False
    PicVarsQA.Visible = False
    If Not m_bDeshabilidar_MouseMove Then picUnqa.Visible = False
    If m_bVistaCambiaUnqas Then
        m_bVistaCambiaUnqas = False
        
        Set m_oOcultarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
        Set m_oMostrarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
        
        Rellenar_sdbgVarsQAProve
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    If Not m_bDeshabilidar_MouseMove Then picUnqa_GridPv.Visible = False
    If m_bVistaCambiaUnqas_GridPv Then
        m_bVistaCambiaUnqas_GridPv = False
        
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    picVarCal.Visible = False
    If m_bVistaCambiaVarCal Then
        m_bVistaCambiaVarCal = False
        
        Rellenar_sdbgVarsQAProve
        
        OcultarMostrarVarCal
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    picProves.Visible = False
    If m_bVistaCambiaProve Then
        m_bVistaCambiaProve = False
        
        OcultarMostrarProveedores
        
        Rellenar_sdbgVarsQAProve
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    
    If sdbgProveVarsQA.GrpContaining(X) > 2 And sdbgProveVarsQA.ColContaining(X, Y) > 3 Then
        DoEvents
        vBookmark = sdbgProveVarsQA.RowBookmark(sdbgProveVarsQA.RowContaining(Y))
        
        'Se trata de sacar el valor de la columna oculta que siempre est� detras de la columna de caption m_sValor,
        'anterior a esta esta la columna de caption m_sCalificacion, as� que se saca el contenido de la columna con el "churro"
        'Sumando 1 � 2 a la columna sobre la que estamos siempre y cuando el caption de la columna coincida con uno de los dos mencionados
        If sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y)).caption = m_sCalificacion Then
            s = sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y) + 2).CellValue(vBookmark)
            sValor = sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y) + 1).CellValue(vBookmark)
        ElseIf sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y)).caption = m_sValor Then
            s = sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y) + 1).CellValue(vBookmark)
            sValor = sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y)).CellValue(vBookmark)
        End If
        If sValor <> "" Then
            If s <> "" Then
                sDatos_Variable = Split(s, "#")
                Call Poner_Cursor_En_Celda(val(sDatos_Variable(POS_NIVEL)), val(sDatos_Variable(POS_TIPO) & ""), val(sDatos_Variable(POS_SUBTIPO) & ""))
            End If
        End If
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgProveVarsQA_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Evento de rat�n sobre el grid sdbgProveVarsQA. Si es sobre los datos de un grupo de "variables" muestra el
''' tooltip de los intervalos
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde se cliqueo</param>
''' <param name="Y">Coordenada X donde se cliqueo</param>
''' <remarks>Llamada desde: Evento que salta al soltar el rat�n sobre el grid; Tiempo m�ximo:0</remarks>
Private Sub sdbgProveVarsQA_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim sDatos_Variable() As String
    Dim s As String
    Dim sPage As String
    Dim vBookmark As Variant
    Dim sValor As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgProveVarsQA.Grp > 1 Then
        DoEvents
        
        If (sdbgProveVarsQA.WhereIs(X, Y) = ssWhereIsData) Then
            DoEvents
            
            picUmbrales.Visible = False
            
            m_bDeshabilidar_MouseMove = True
            If Left(sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y)).Name, Len("AVISORESTRIC")) = "AVISORESTRIC" Then
                If RellenarTooltip(m_oVistaCalSeleccionada.TipoVision) Then
                    lblAdjProhibida.Visible = True
                    
                    If X + picUmbrales.Width > Me.Width Then
                        picUmbrales.Left = Me.Width - picUmbrales.Width - 600
                    Else
                        picUmbrales.Left = X - 100
                    End If
                    If Y + picUmbrales.Height > Me.Height Then
                        picUmbrales.Top = Me.Height - picUmbrales.Height - 600
                    Else
                        picUmbrales.Top = Y - 200
                    End If
                    picUmbrales.Visible = True
                End If
            Else
                If sdbgProveVarsQA.GrpContaining(X) > 2 And sdbgProveVarsQA.ColContaining(X, Y) > 3 Then
                    DoEvents
                    vBookmark = sdbgProveVarsQA.RowBookmark(sdbgProveVarsQA.RowContaining(Y))
                    'Se trata de sacar el valor de la columna oculta que siempre est� detras de la columna de caption m_sValor,
                    'anterior a esta esta la columna de caption m_sCalificacion, as� que se saca el contenido de la columna con el "churro"
                    'Sumando 1 � 2 a la columna sobre la que estamos siempre y cuando el caption de la columna coincida con uno de los dos mencionados
                    
                    If sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y)).caption = m_sCalificacion Then
                        s = sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y) + 2).Text
                        sValor = sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y) + 1).CellValue(vBookmark)
                    ElseIf sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y)).caption = m_sValor Then
                        s = sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y) + 1).Text
                        sValor = sdbgProveVarsQA.Columns(sdbgProveVarsQA.ColContaining(X, Y)).CellValue(vBookmark)
                    End If
                    If s <> "" And sValor <> "" Then
                        sDatos_Variable = Split(s, "#")
                        If Poner_Cursor_En_Celda(val(sDatos_Variable(POS_NIVEL)), val(sDatos_Variable(POS_TIPO) & ""), val(sDatos_Variable(POS_SUBTIPO) & "")) Then
                            Dim oVarCal As CVariableCalidad
                            Set oVarCal = ObtenerVariableCalidad(sdbgProveVarsQA.Columns(0).Text, val(sDatos_Variable(POS_ID_VARCAL1)), val(sDatos_Variable(POS_ID_VARCAL2)), _
                                                                 val(sDatos_Variable(POS_ID_VARCAL3)), val(sDatos_Variable(POS_ID_VARCAL4)), val(sDatos_Variable(POS_ID)), val(sDatos_Variable(POS_NIVEL)), _
                                                                 val(sDatos_Variable(POS_UNQA)))
                            
                            If oVarCal.Aplica Then
                                Select Case val(sDatos_Variable(POS_SUBTIPO))
                                    Case CalidadSubtipo.CalCertificado
                                        sPage = "DetallePuntuacionCert.aspx"
                                    Case CalidadSubtipo.CalNoConformidad
                                        sPage = "DetallePuntuacionNc.aspx"
                                    Case CalidadSubtipo.CalPPM, CalidadSubtipo.CalCargoProveedores, CalidadSubtipo.CalTasaServicios
                                        sPage = "DetallePuntuacionPpmCargoTasa.aspx"
                                    Case CalidadSubtipo.CalEncuesta
                                        sPage = "DetallePuntuacionEncuesta.aspx"
                                End Select
                                
                                MostrarFormWeb m_sfrmWeb, sPage, sdbgProveVarsQA.Columns(0).Text, sdbgProveVarsQA.Columns(1).Text, sDatos_Variable(POS_ID) & "", sDatos_Variable(POS_NIVEL) & "", _
                                    sDatos_Variable(POS_UNQA) & "", sDatos_Variable(POS_SUBTIPO) & "", Me.Name, oUsuarioSummit, gParametrosGenerales.gsRutasFullstepWeb, gParametrosGenerales.gsRutasFullstepWeb
                            Else
                                oMensajes.VariableCalidadNoAplicaProveedor
                            End If
                        End If
                    End If
                End If
            End If
            Me.Timer1.Enabled = True
        End If
    End If
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
     Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgProveVarsQA_MouseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Si se cliqa en el bot�n de unidades se muestra el arbol de unidades para el proveedor, las N variables y unidad
''' de la fila. Camcios en este arbol son lo q estoy llamando visibilidades "desligadas de la vista"
''' </summary>
''' <param name="LastRow">Fila q pierde foco</param>
''' <param name="LastCol">Columna q pierde foco</param>
''' <remarks>Llamada desde: evento de cambio de filay/o columna en grid; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgProveVarsQA_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim sDatos_Variable() As String
    Dim Mouse As POINTAPI

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DoEvents
    
    If Me.sdbgProveVarsQA.col > -1 Then
        If Left(Me.sdbgProveVarsQA.Columns(Me.sdbgProveVarsQA.col).Name, 4) = "BTUN" Then

            sDatos_Variable = Split(sdbgProveVarsQA.Groups(2).Columns(2).Value, "#")

            m_iIDRest = sDatos_Variable(POS_ID)
            m_iNivelRest = sDatos_Variable(POS_NIVEL)

            If (sDatos_Variable(POS_UNQA_HIJOS) > 0) And HayUnqaVarcalAlgunProve(sDatos_Variable(POS_UNQA), sDatos_Variable(POS_ID), sDatos_Variable(POS_NIVEL), True) Then
                m_bVistaCambiaUnqas_GridPv = False
                m_bDeshabilidar_MouseMove = True

                CellGenerarEstructuraUnidadesNegocio_GridPv sdbgProveVarsQA.Groups(0).Columns(0).Value, sDatos_Variable(POS_UNQA), sDatos_Variable(POS_UNQA_NIVEL)

                picUnqa_GridPv.Top = sdbgProveVarsQA.Top + 420
                picUnqa_GridPv.Left = sdbgProveVarsQA.Groups(0).Width
                picUnqa_GridPv.Left = picUnqa_GridPv.Left + Me.sdbgProveVarsQA.Left + Me.sdbgProveVarsQA.Groups(1).Width
                picUnqa_GridPv.Height = sdbgProveVarsQA.Height - 580

                tvwUndNegUsu_GridPv.Height = picUnqa_GridPv.Height - 650

                picUnqa_GridPv.Width = 4800
                
                If sdbgProveVarsQA.Groups(1).Columns(0).Width > picUnqa_GridPv.Width Then
                    picUnqa_GridPv.Width = 4800 + (sdbgProveVarsQA.Groups(1).Columns(0).Width - 4800)
                End If

                tvwUndNegUsu_GridPv.Width = picUnqa_GridPv.Width - 250
                picUnqa_GridPv.Visible = True
            
                GetCursorPos Mouse
                SetCursorPos Mouse.X + 10, Mouse.Y - 3

                Me.sdbgProveVarsQA.col = Me.sdbgProveVarsQA.col - 1
                
                Me.Timer1.Enabled = True
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgProveVarsQA_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Evento de fila cargada en grid, establece los estilos de celda
''' </summary>
''' <param name="Bookmark">Bookmark de la fila cargada</param>
''' <remarks>Llamada desde: evento de fila cargada en grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgProveVarsQA_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo err_sdbgProveVarsQA_RowLoaded
    
    Dim i As Integer
    Dim bNoCumple As Boolean
    Dim sDatos_Variable() As String
        
    Dim bAlgunaUnqaVarcalAlgunProve As Boolean

    bAlgunaUnqaVarcalAlgunProve = False

    For i = 2 To sdbgProveVarsQA.Groups.Count - 1
        bNoCumple = False
        sdbgProveVarsQA.Groups(i).Columns(0).CellStyleSet "Calificacion"
        sdbgProveVarsQA.Groups(i).Columns(1).CellStyleSet "Valor"
        
        sDatos_Variable = Split(sdbgProveVarsQA.Groups(i).Columns(2).Value, "#")
        
        If Not (sdbgProveVarsQA.Groups(i).Columns(2).Value = "") Then
            If HayUnqaVarcalAlgunProve(sDatos_Variable(POS_UNQA), sDatos_Variable(POS_ID), sDatos_Variable(POS_NIVEL), False) Then
                bAlgunaUnqaVarcalAlgunProve = True
            End If
        End If
        
        If sDatos_Variable(POS_AVISO) <> "" Then
            If sDatos_Variable(POS_AVISO) Then
                If sdbgProveVarsQA.Groups(i).Columns(1).Value <> "" Then
                    If sDatos_Variable(POS_A_INF) <> "" And sDatos_Variable(POS_A_SUP) <> "" Then
                        If CDbl(sdbgProveVarsQA.Groups(i).Columns(1).Value) >= CDbl(sDatos_Variable(POS_A_INF)) And CDbl(sdbgProveVarsQA.Groups(i).Columns(1).Value) <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                            sdbgProveVarsQA.Groups(i).Columns(0).CellStyleSet "CalificacionOrange"
                            sdbgProveVarsQA.Groups(i).Columns(1).CellStyleSet "ValorOrange"
                            sdbgProveVarsQA.Groups(i).Columns(3).CellStyleSet "IMGAVISO"
                            bNoCumple = True
                        End If
                    Else
                        If sDatos_Variable(POS_A_INF) <> "" Then
                            If CDbl(sdbgProveVarsQA.Groups(i).Columns(1).Value) >= CDbl(sDatos_Variable(POS_A_INF)) Then
                                sdbgProveVarsQA.Groups(i).Columns(0).CellStyleSet "CalificacionOrange"
                                sdbgProveVarsQA.Groups(i).Columns(1).CellStyleSet "ValorOrange"
                                sdbgProveVarsQA.Groups(i).Columns(3).CellStyleSet "IMGAVISO"
                                bNoCumple = True
                            End If
                        Else
                            If sDatos_Variable(POS_A_SUP) <> "" Then
                                If CDbl(sdbgProveVarsQA.Groups(i).Columns(1).Value) <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                                    sdbgProveVarsQA.Groups(i).Columns(0).CellStyleSet "CalificacionOrange"
                                    sdbgProveVarsQA.Groups(i).Columns(1).CellStyleSet "ValorOrange"
                                    sdbgProveVarsQA.Groups(i).Columns(3).CellStyleSet "IMGAVISO"
                                    bNoCumple = True
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If sDatos_Variable(POS_RESTRICCION) <> "" Then
            If sDatos_Variable(POS_RESTRICCION) Then
                If sdbgProveVarsQA.Groups(i).Columns(1).Value <> "" Then
                    If sDatos_Variable(POS_R_INF) <> "" And sDatos_Variable(POS_R_SUP) <> "" Then
                        If CDbl(sdbgProveVarsQA.Groups(i).Columns(1).Value) >= CDbl(sDatos_Variable(POS_R_INF)) And CDbl(sdbgProveVarsQA.Groups(i).Columns(1).Value) <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                            sdbgProveVarsQA.Groups(i).Columns(0).CellStyleSet "CalificacionRed"
                            sdbgProveVarsQA.Groups(i).Columns(1).CellStyleSet "ValorRed"
                            sdbgProveVarsQA.Groups(i).Columns(3).CellStyleSet "IMGRESTRIC"
                            bNoCumple = True
                        End If
                    Else
                        If sDatos_Variable(POS_R_INF) <> "" Then
                            If CDbl(sdbgProveVarsQA.Groups(i).Columns(1).Value) >= CDbl(sDatos_Variable(POS_R_INF)) Then
                                sdbgProveVarsQA.Groups(i).Columns(0).CellStyleSet "CalificacionRed"
                                sdbgProveVarsQA.Groups(i).Columns(1).CellStyleSet "ValorRed"
                                sdbgProveVarsQA.Groups(i).Columns(3).CellStyleSet "IMGRESTRIC"
                                bNoCumple = True
                            End If
                        Else
                            If sDatos_Variable(POS_R_SUP) <> "" Then
                                If CDbl(sdbgProveVarsQA.Groups(i).Columns(1).Value) <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                                    sdbgProveVarsQA.Groups(i).Columns(0).CellStyleSet "CalificacionRed"
                                    sdbgProveVarsQA.Groups(i).Columns(1).CellStyleSet "ValorRed"
                                    sdbgProveVarsQA.Groups(i).Columns(3).CellStyleSet "IMGRESTRIC"
                                    bNoCumple = True
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
        
        If Not m_bConAvisoRestricProveVarsQA Then
            m_bConAvisoRestricProveVarsQA = bNoCumple
        End If
        ReDim sDatos_Variable(0)

    Next i
    
    sDatos_Variable = Split(sdbgProveVarsQA.Groups(2).Columns(2).Value, "#")
    
    If bAlgunaUnqaVarcalAlgunProve Then
        If sDatos_Variable(POS_UNQA_HIJOS) > 0 Then
            If sDatos_Variable(POS_UNQA_NIVEL) > 0 Then
                sdbgProveVarsQA.Groups(1).Columns(1).CellStyleSet "GrayHeadCeldaUnqa"
            Else
                sdbgProveVarsQA.Groups(1).Columns(1).CellStyleSet "CeldaUnqa"
            End If
        ElseIf sDatos_Variable(POS_UNQA_NIVEL) > 0 Then
            sdbgProveVarsQA.Groups(1).Columns(1).CellStyleSet "GrayHead"
        End If
    Else
        If sDatos_Variable(POS_UNQA_NIVEL) > 0 Then
            sdbgProveVarsQA.Groups(1).Columns(1).CellStyleSet "GrayHead"
        End If
    End If

    If sDatos_Variable(POS_UNQA_NIVEL) > 0 Then
        sdbgProveVarsQA.Groups(0).Columns(0).CellStyleSet "GrayHead"
        sdbgProveVarsQA.Groups(0).Columns(1).CellStyleSet "GrayHead"

        sdbgProveVarsQA.Groups(1).Columns(0).CellStyleSet "GrayHead"
    End If

    Exit Sub
    
err_sdbgProveVarsQA_RowLoaded:
    Select Case err.Number
        Case 9
            Resume Next
    End Select
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgProveVarsQA_RowLoaded", err, Erl, Me)
        GoTo err_sdbgProveVarsQA_RowLoaded
        Exit Sub
    End If
End Sub

Private Sub sdbgProveVarsQA_SplitterMove(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgProveVarsQA_SplitterMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Muestra el arbol de variables q son hijas de la variable de la fila
''' </summary>
''' <remarks>Llamada desde: Evento que salta al hacer click en el bot�n de una celda; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgVarsQAProve_BtnClick()
    Dim sDatos_Variable() As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sDatos_Variable = Split(sdbgVarsQAProve.Groups(0).Columns("DATOS_VAR").Value, "#")

    If Me.sdbgVarsQAProve.Columns(sdbgVarsQAProve.col).Name = "COD" Then
        m_bVistaCambiaVarCal = False
        
        CargarVarsRestriccion sDatos_Variable(POS_ID), IIf(sDatos_Variable(POS_ID_VARCAL1) = "", Empty, sDatos_Variable(POS_ID_VARCAL1)), IIf(sDatos_Variable(POS_ID_VARCAL2) = "", Empty, sDatos_Variable(POS_ID_VARCAL2)), IIf(sDatos_Variable(POS_ID_VARCAL3) = "", Empty, sDatos_Variable(POS_ID_VARCAL3)), IIf(sDatos_Variable(POS_ID_VARCAL4) = "", Empty, sDatos_Variable(POS_ID_VARCAL4)), IIf(sDatos_Variable(POS_NIVEL) = "", Empty, sDatos_Variable(POS_NIVEL))
        
        PicVarsQA.Left = sdbgVarsQAProve.Groups(0).Columns("COD").Width
        PicVarsQA.Height = sdbgVarsQAProve.Height - 580 '1080
        sdbgVarsRestric.Height = PicVarsQA.Height - 70
        PicVarsQA.Visible = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Hace q todos los grupos de "proveedores" tengan el mismo aspecto
''' </summary>
''' <param name="ColIndex">Indice de columna</param>
''' <param name="Cancel">Cancel el redimensionamiento</param>
''' <remarks>Llamada desde: Evento de redimensionar una columna del grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgVarsQAProve_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
Dim iGrupo As Long
Dim igrupo_resized As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCambios = True

    igrupo_resized = sdbgVarsQAProve.Columns(ColIndex).Group
    If sdbgVarsQAProve.Columns(ColIndex).Group > 1 Then
        
        For iGrupo = 2 To sdbgVarsQAProve.Groups.Count - 1
            If iGrupo <> igrupo_resized Then
                sdbgVarsQAProve.Groups(iGrupo).Columns(0).Width = sdbgVarsQAProve.ResizeWidth
            End If
        Next iGrupo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_ColResize", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Si se clica en Variables (0) muestra el arbol de selecci�n de variables
''' Si se clica en Unidades (1) muestra el arbol de selecci�n de unidades
''' Si se clica en un grupo de Proveedor (2..N) muestra el arbol de selecci�n de proveedor y checks de mostrar
''' Calificaciones/Puntos
''' </summary>
''' <param name="GrpIndex">Indice de grupo</param>
''' <remarks>Llamada desde: Evento de hacer clic en la cabecera de un grupo del grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgVarsQAProve_GrpHeadClick(ByVal GrpIndex As Integer)
    Dim Mouse As POINTAPI
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If GrpIndex = 0 Then
        m_bVistaCambiaVarCal = False
        
        GenerarEstructuraVarCal

        picVarCal.Top = sdbgVarsQAProve.Top + 420
        picVarCal.Left = Me.sdbgVarsQAProve.Left + 380
        picVarCal.Height = sdbgVarsQAProve.Height - 1080

        GetCursorPos Mouse
        SetCursorPos Mouse.X + 10, Mouse.Y + 30

        tvwVarCal.Height = picVarCal.Height - 940

        picVarCal.Width = 5200
        tvwVarCal.Width = picVarCal.Width - 250
        picVarCal.Visible = True
    ElseIf GrpIndex = 1 Then
        m_bVistaCambiaUnqas = False
        m_bGrpGenerarEstrucUnQaPulsado = True
                    
        GrpGenerarEstructuraUnidadesNegocio
                        
        picUnqa.Top = sdbgVarsQAProve.Top + 420
        picUnqa.Left = Me.sdbgVarsQAProve.Left + Me.sdbgVarsQAProve.Groups(0).Width + 380
        picUnqa.Height = sdbgVarsQAProve.Height - 1080
        
        GetCursorPos Mouse
        SetCursorPos Mouse.X + 10, Mouse.Y + 30
        
        tvwUndNegUsu.Height = picUnqa.Height - 650
        
        picUnqa.Width = 4800
        If sdbgVarsQAProve.Groups(1).Columns(0).Width > picUnqa.Width Then
            picUnqa.Width = 4800 + (sdbgVarsQAProve.Groups(1).Columns(0).Width - 4800)
        End If
        tvwUndNegUsu.Width = picUnqa.Width - 250
        picUnqa.Visible = True
    Else
        m_bVistaCambiaProve = False
    
        GenerarEstructuraProves

        picProves.Top = sdbgVarsQAProve.Top + 420
        picProves.Left = Me.sdbgVarsQAProve.Groups(GrpIndex).Left
        picProves.Height = sdbgVarsQAProve.Height - 1080
        
        GetCursorPos Mouse
        SetCursorPos Mouse.X + 10, Mouse.Y + 30

        If DameNumeroProvesVisibles = 1 Then
            tvwProves.Height = picProves.Height - 1320 - Me.lblAlmenosUno.Height - 30
            Me.lblAlmenosUno.Visible = True
        Else
            tvwProves.Height = picProves.Height - 1320
            Me.lblAlmenosUno.Visible = False
        End If
        
        Me.lblAlmenosUno.Top = picProves.Height - Me.lblAlmenosUno.Height - 80

        picProves.Width = 4200
        
        If picProves.Width + picProves.Left > Me.sdbgVarsQAProve.Width Then
            picProves.Left = Me.sdbgVarsQAProve.Width - picProves.Width
        End If
        
        tvwProves.Width = picProves.Width - 250
        picProves.Visible = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_GrpHeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Hace q todos los grupos de "proveedores" tengan el mismo ancho
''' </summary>
''' <param name="GrpIndex">Indice de grupo</param>
''' <param name="Cancel">Cancel el redimensionamiento</param>
''' <remarks>Llamada desde: Evento de redimensionar el grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgVarsQAProve_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
Dim iGrupos As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If GrpIndex = 1 Then
        sdbgVarsQAProve.Groups(1).Columns(0).Width = sdbgVarsQAProve.ResizeWidth - CTE_WIDTH_3COLBT
        sdbgVarsQAProve.Groups(1).Columns(1).Width = CTE_WIDTH_3COLBT
    ElseIf GrpIndex > 1 Then
        For iGrupos = 2 To sdbgVarsQAProve.Groups.Count - 1
            If iGrupos <> GrpIndex Then
                sdbgVarsQAProve.Groups(iGrupos).Width = sdbgVarsQAProve.ResizeWidth
            End If
        Next iGrupos
    End If
    
    m_bCambios = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_GrpResize", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Oculta el tooltip de los intervalos y los diferentes arboles de selecci�n. De haberse cambiado algo en la
''' configuraci�n de la vista o visibilidades "desligadas de la vista" aplica el cambio en los grids
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde esta  el rat�n</param>
''' <param name="Y">Coordenada X donde esta  el rat�n</param>
''' <remarks>Llamada desde: Evento que salta al mover el rat�n sobre el grid; Tiempo m�ximo:0</remarks>
Private Sub sdbgVarsQAProve_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim sDatos_Variable() As String
Dim vBookmark As Variant
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DoEvents
    
    picNoVisibles.Visible = False
    If Not m_bDeshabilidar_MouseMove Then picUmbrales.Visible = False
    PicVarsQA.Visible = False
    If Not m_bDeshabilidar_MouseMove Then picUnqa.Visible = False
    If m_bVistaCambiaUnqas Then
        m_bVistaCambiaUnqas = False
        
        Set m_oOcultarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
        Set m_oMostrarUnqaProve_GridPv = oFSGSRaiz.generar_CProveedores
        
        Rellenar_sdbgVarsQAProve
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    If Not m_bDeshabilidar_MouseMove Then picUnqa_GridPv.Visible = False
    If m_bVistaCambiaUnqas_GridPv Then
        m_bVistaCambiaUnqas_GridPv = False
        
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    picVarCal.Visible = False
    If m_bVistaCambiaVarCal Then
        m_bVistaCambiaVarCal = False
        
        Rellenar_sdbgVarsQAProve
        
        OcultarMostrarVarCal
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    picProves.Visible = False
    If m_bVistaCambiaProve Then
        m_bVistaCambiaProve = False
        
        OcultarMostrarProveedores
        
        Rellenar_sdbgVarsQAProve
        Rellenar_sdbgProveVarsQA
        
        FocoAGrid
    End If
    
    If Not m_bDeshabilidar_MouseMove Then
        vBookmark = sdbgVarsQAProve.RowBookmark(sdbgVarsQAProve.RowContaining(Y))
        If sdbgVarsQAProve.GrpContaining(X) > 1 And sdbgVarsQAProve.ColContaining(X, Y) > 0 Then
            If sdbgVarsQAProve.Groups(sdbgVarsQAProve.GrpContaining(X)).Columns(1).CellValue(vBookmark) <> "" Then 'If sdbgVarsQAProve.Groups(sdbgVarsQAProve.GrpContaining(X)).Columns(1).Text <> "" Then
                DoEvents
                sDatos_Variable = Split(sdbgVarsQAProve.Groups(0).Columns("DATOS_VAR").CellValue(vBookmark), "#")
                Call Poner_Cursor_En_Celda(val(sDatos_Variable(POS_NIVEL)), val(sDatos_Variable(POS_TIPO) & ""), val(sDatos_Variable(POS_SUBTIPO) & ""))
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Evento de rat�n sobre el grid sdbgVarsQAProve. Si es sobre los datos de un grupo de "proveedor" muestra el
''' tooltip de los intervalos
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde se cliqueo</param>
''' <param name="Y">Coordenada X donde se cliqueo</param>
''' <remarks>Llamada desde: Evento que salta al soltar el rat�n sobre el grid; Tiempo m�ximo:0</remarks>
Private Sub sdbgVarsQAProve_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim vBookmark As Variant
    Dim sDatos_Variable() As String
    Dim sPage As String

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgVarsQAProve.Grp > 1 Then
        DoEvents
                
        If (sdbgVarsQAProve.WhereIs(X, Y) = ssWhereIsData) Then
            DoEvents
                            
            picUmbrales.Visible = False

            m_bDeshabilidar_MouseMove = True
            If Left(sdbgVarsQAProve.Columns(sdbgVarsQAProve.ColContaining(X, Y)).Name, Len("AVISORESTRIC")) = "AVISORESTRIC" Then
                If RellenarTooltip(m_oVistaCalSeleccionada.TipoVision) Then
                    lblAdjProhibida.Visible = True
                    If X + picUmbrales.Width > Me.Width Then
                        picUmbrales.Left = Me.Width - picUmbrales.Width - 600
                    Else
                        picUmbrales.Left = X - 100
                    End If
                    If Y + picUmbrales.Height > Me.Height Then
                        picUmbrales.Top = Me.Height - picUmbrales.Height - 600
                    Else
                        picUmbrales.Top = Y - 200
                    End If
                    picUmbrales.Visible = True
                End If
            Else
                Me.Timer1.Enabled = True
                vBookmark = sdbgVarsQAProve.RowBookmark(sdbgVarsQAProve.RowContaining(Y))
                If sdbgVarsQAProve.GrpContaining(X) > 1 And sdbgVarsQAProve.ColContaining(X, Y) > 0 Then
                   If sdbgVarsQAProve.Groups(sdbgVarsQAProve.GrpContaining(X)).Columns(1).CellValue(vBookmark) <> "" Then
                        DoEvents
                        sDatos_Variable = Split(sdbgVarsQAProve.Groups(0).Columns("DATOS_VAR").CellValue(vBookmark), "#")
                        If Poner_Cursor_En_Celda(val(sDatos_Variable(POS_NIVEL)), val(sDatos_Variable(POS_TIPO) & ""), val(sDatos_Variable(POS_SUBTIPO) & "")) Then
                            Dim oVarCal As CVariableCalidad
                            Set oVarCal = ObtenerVariableCalidad(sdbgVarsQAProve.Groups(sdbgVarsQAProve.GrpContaining(X)).TagVariant, val(sDatos_Variable(POS_ID_VARCAL1)), val(sDatos_Variable(POS_ID_VARCAL2)), _
                                                                 val(sDatos_Variable(POS_ID_VARCAL3)), val(sDatos_Variable(POS_ID_VARCAL4)), val(sDatos_Variable(POS_ID)), val(sDatos_Variable(POS_NIVEL)), _
                                                                 val(sDatos_Variable(POS_UNQA)))
                            
                            If oVarCal.Aplica Then
                                Select Case val(sDatos_Variable(POS_SUBTIPO))
                                    Case CalidadSubtipo.CalCertificado
                                        sPage = "DetallePuntuacionCert.aspx"
                                    Case CalidadSubtipo.CalNoConformidad
                                        sPage = "DetallePuntuacionNc.aspx"
                                    Case CalidadSubtipo.CalPPM, CalidadSubtipo.CalCargoProveedores, CalidadSubtipo.CalTasaServicios
                                        sPage = "DetallePuntuacionPpmCargoTasa.aspx"
                                    Case CalidadSubtipo.CalEncuesta
                                        sPage = "DetallePuntuacionEncuesta.aspx"
                                End Select
                                
                                MostrarFormWeb m_sfrmWeb, sPage, sdbgVarsQAProve.Groups(sdbgVarsQAProve.GrpContaining(X)).TagVariant, _
                                    Replace(sdbgVarsQAProve.Groups(sdbgVarsQAProve.GrpContaining(X)).caption, sdbgVarsQAProve.Groups(sdbgVarsQAProve.GrpContaining(X)).TagVariant, ""), _
                                    sDatos_Variable(POS_ID) & "", sDatos_Variable(POS_NIVEL) & "", sDatos_Variable(POS_UNQA) & "", sDatos_Variable(POS_SUBTIPO) & "", Me.Name, _
                                    oUsuarioSummit, gParametrosGenerales.gsRutasFullstepWeb, gParametrosGenerales.gsURLSessionService
                            Else
                                oMensajes.VariableCalidadNoAplicaProveedor
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    DoEvents
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_MouseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Function ObtenerVariableCalidad(ByVal sProve As String, ByVal lID1 As Long, ByVal lID2 As Long, ByVal lID3 As Long, ByVal lID4 As Long, ByVal lID As Long, ByVal iNivel As Integer, ByVal iUNQA As Integer) As CVariableCalidad
    Select Case iNivel
        Case 1
            Set ObtenerVariableCalidad = m_oVarCal1
        Case 2
            Set ObtenerVariableCalidad = m_oProvesAsig.Item(sProve).VariablesCalidad.Item(CStr(1) & CStr(lID1) & "#" & iUNQA).VariblesCal.Item(CStr(2) & CStr(lID) & "#" & iUNQA)
        Case 3
            Set ObtenerVariableCalidad = m_oProvesAsig.Item(sProve).VariablesCalidad.Item(CStr(1) & CStr(lID1) & "#" & iUNQA).VariblesCal.Item(CStr(2) & CStr(lID2) & "#" & iUNQA).VariblesCal.Item(CStr(3) & CStr(lID) & "#" & iUNQA)
        Case 4
            Set ObtenerVariableCalidad = m_oProvesAsig.Item(sProve).VariablesCalidad.Item(CStr(1) & CStr(lID1) & "#" & iUNQA).VariblesCal.Item(CStr(2) & CStr(lID2) & "#" & iUNQA).VariblesCal.Item(CStr(3) & CStr(lID3) & "#" & iUNQA).VariblesCal.Item(CStr(4) & CStr(lID) & "#" & iUNQA)
        Case 5
            Set ObtenerVariableCalidad = m_oProvesAsig.Item(sProve).VariablesCalidad.Item(CStr(1) & CStr(lID1) & "#" & iUNQA).VariblesCal.Item(CStr(2) & CStr(lID2) & "#" & iUNQA).VariblesCal.Item(CStr(3) & CStr(lID3) & "#" & iUNQA).VariblesCal.Item(CStr(4) & CStr(lID4) & "#" & iUNQA).VariblesCal.Item(CStr(5) & CStr(lID) & "#" & iUNQA)
    End Select
End Function

''' <summary>
''' Si se cliqa en el bot�n de unidades se muestra el arbol de unidades para la variable y unidad de la fila
''' </summary>
''' <param name="LastRow">Fila q pierde foco</param>
''' <param name="LastCol">Columna q pierde foco</param>
''' <remarks>Llamada desde: evento de cambio de filay/o columna en grid; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgVarsQAProve_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim Mouse As POINTAPI
    Dim sDatos_Variable() As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DoEvents

    sDatos_Variable = Split(sdbgVarsQAProve.Groups(0).Columns("DATOS_VAR").Value, "#")

    If Me.sdbgVarsQAProve.col > -1 Then

        If Left(Me.sdbgVarsQAProve.Columns(Me.sdbgVarsQAProve.col).Name, 4) = "BTUN" Then
                
            m_iIDRest = sDatos_Variable(POS_ID)
            m_iNivelRest = sDatos_Variable(POS_NIVEL)
                
            If (sDatos_Variable(POS_UNQA_HIJOS) > 0) And HayUnqaVarcalAlgunProve(sDatos_Variable(POS_UNQA), sDatos_Variable(POS_ID), sDatos_Variable(POS_NIVEL), True) Then
                m_bGrpGenerarEstrucUnQaPulsado = False
                m_bVistaCambiaUnqas = False
                
                m_bDeshabilidar_MouseMove = True
                
                CellGenerarEstructuraUnidadesNegocio sDatos_Variable(POS_UNQA), sDatos_Variable(POS_UNQA_NIVEL), StrToDbl0(sDatos_Variable(POS_ID_VARCAL1)), StrToDbl0(sDatos_Variable(POS_ID_VARCAL2)), StrToDbl0(sDatos_Variable(POS_ID_VARCAL3)), StrToDbl0(sDatos_Variable(POS_ID_VARCAL4))
        
                picUnqa.Top = sdbgVarsQAProve.Top + 420
                picUnqa.Left = sdbgVarsQAProve.Groups(0).Width
                picUnqa.Left = picUnqa.Left + Me.sdbgVarsQAProve.Left + Me.sdbgVarsQAProve.Groups(1).Width

                picUnqa.Height = sdbgVarsQAProve.Height - 580
        
                tvwUndNegUsu.Height = picUnqa.Height - 650
        
                picUnqa.Width = 4800
                If sdbgVarsQAProve.Groups(1).Columns(0).Width > picUnqa.Width Then
                    picUnqa.Width = 4800 + (sdbgVarsQAProve.Groups(1).Columns(0).Width - 4800)
                End If
                
                tvwUndNegUsu.Width = picUnqa.Width - 250
                picUnqa.Visible = True
                                    
                GetCursorPos Mouse
                SetCursorPos Mouse.X + 10, Mouse.Y - 3

                Me.sdbgVarsQAProve.col = Me.sdbgVarsQAProve.col - 1
                
                Me.Timer1.Enabled = True
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Evento de fila cargada en grid, establece los estilos de celda
''' </summary>
''' <param name="Bookmark">Bookmark de la fila cargada</param>
''' <remarks>Llamada desde: evento de fila cargada en grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgVarsQAProve_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
Dim sDatos_Variable() As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sDatos_Variable = Split(sdbgVarsQAProve.Groups(0).Columns("DATOS_VAR").Value, "#")
           
    If sDatos_Variable(POS_UNQA_NIVEL) > 0 Then
        sdbgVarsQAProve.Groups(0).Columns(0).CellStyleSet "GrayHead"
        sdbgVarsQAProve.Groups(0).Columns(1).CellStyleSet "GrayHead"
        
        sdbgVarsQAProve.Groups(1).Columns(0).CellStyleSet "GrayHead"
    End If
    
    If Not HayUnqaVarcalAlgunProve(sDatos_Variable(POS_UNQA), sDatos_Variable(POS_ID), sDatos_Variable(POS_NIVEL), False) Then
        If sDatos_Variable(POS_UNQA_NIVEL) > 0 Then
            sdbgVarsQAProve.Groups(1).Columns(1).CellStyleSet "GrayHead"
        End If
    Else
        If sDatos_Variable(POS_UNQA_HIJOS) > 0 Then
            If sDatos_Variable(POS_UNQA_NIVEL) > 0 Then
                sdbgVarsQAProve.Groups(1).Columns(1).CellStyleSet "GrayHeadCeldaUnqa"
            Else
                sdbgVarsQAProve.Groups(1).Columns(1).CellStyleSet "CeldaUnqa"
            End If
        ElseIf sDatos_Variable(POS_UNQA_NIVEL) > 0 Then
            sdbgVarsQAProve.Groups(1).Columns(1).CellStyleSet "GrayHead"
        End If
    End If
    
    For i = 2 To sdbgVarsQAProve.Groups.Count - 1
        
        sdbgVarsQAProve.Groups(i).Columns(0).CellStyleSet "Calificacion"
        sdbgVarsQAProve.Groups(i).Columns(1).CellStyleSet "Valor"
        
        If sDatos_Variable(POS_AVISO) <> "" Then
            If sDatos_Variable(POS_AVISO) Then
                If sdbgVarsQAProve.Groups(i).Columns(1).Value <> "" Then
                    If sDatos_Variable(POS_A_INF) <> "" And sDatos_Variable(POS_A_SUP) <> "" Then
                        If CDbl(sdbgVarsQAProve.Groups(i).Columns(1).Value) >= CDbl(sDatos_Variable(POS_A_INF)) And CDbl(sdbgVarsQAProve.Groups(i).Columns(1).Value) <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                            sdbgVarsQAProve.Groups(i).Columns(0).CellStyleSet "CalificacionOrange"
                            sdbgVarsQAProve.Groups(i).Columns(1).CellStyleSet "ValorOrange"
                            sdbgVarsQAProve.Groups(i).Columns(2).CellStyleSet "IMGAVISO"
                            m_bConAvisoRestricVarsQAProve = True
                        End If
                    Else
                        If sDatos_Variable(POS_A_INF) <> "" Then
                            If CDbl(sdbgVarsQAProve.Groups(i).Columns(1).Value) >= CDbl(sDatos_Variable(POS_A_INF)) Then
                                sdbgVarsQAProve.Groups(i).Columns(0).CellStyleSet "CalificacionOrange"
                                sdbgVarsQAProve.Groups(i).Columns(1).CellStyleSet "ValorOrange"
                                sdbgVarsQAProve.Groups(i).Columns(2).CellStyleSet "IMGAVISO"
                                m_bConAvisoRestricVarsQAProve = True
                            End If
                        Else
                            If sDatos_Variable(POS_A_SUP) <> "" Then
                                If CDbl(sdbgVarsQAProve.Groups(i).Columns(1).Value) <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                                    sdbgVarsQAProve.Groups(i).Columns(0).CellStyleSet "CalificacionOrange"
                                    sdbgVarsQAProve.Groups(i).Columns(1).CellStyleSet "ValorOrange"
                                    sdbgVarsQAProve.Groups(i).Columns(2).CellStyleSet "IMGAVISO"
                                    m_bConAvisoRestricVarsQAProve = True
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
        
        If sDatos_Variable(POS_RESTRICCION) <> "" Then
            If sDatos_Variable(POS_RESTRICCION) Then
                If sdbgVarsQAProve.Groups(i).Columns(1).Value <> "" Then
                    If sDatos_Variable(POS_R_INF) <> "" And sDatos_Variable(POS_R_SUP) <> "" Then
                        If CDbl(sdbgVarsQAProve.Groups(i).Columns(1).Value) >= CDbl(sDatos_Variable(POS_R_INF)) And CDbl(sdbgVarsQAProve.Groups(i).Columns(1).Value) <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                            sdbgVarsQAProve.Groups(i).Columns(0).CellStyleSet "CalificacionRed"
                            sdbgVarsQAProve.Groups(i).Columns(1).CellStyleSet "ValorRed"
                            sdbgVarsQAProve.Groups(i).Columns(2).CellStyleSet "IMGRESTRIC"
                            m_bConAvisoRestricVarsQAProve = True
                        End If
                    Else
                        If sDatos_Variable(POS_R_INF) <> "" Then
                            If CDbl(sdbgVarsQAProve.Groups(i).Columns(1).Value) >= CDbl(sDatos_Variable(POS_R_INF)) Then
                                sdbgVarsQAProve.Groups(i).Columns(0).CellStyleSet "CalificacionRed"
                                sdbgVarsQAProve.Groups(i).Columns(1).CellStyleSet "ValorRed"
                                sdbgVarsQAProve.Groups(i).Columns(2).CellStyleSet "IMGRESTRIC"
                                m_bConAvisoRestricVarsQAProve = True
                            End If
                        Else
                            If sDatos_Variable(POS_R_SUP) <> "" Then
                                If CDbl(sdbgVarsQAProve.Groups(i).Columns(1).Value) <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                                    sdbgVarsQAProve.Groups(i).Columns(0).CellStyleSet "CalificacionRed"
                                    sdbgVarsQAProve.Groups(i).Columns(1).CellStyleSet "ValorRed"
                                    sdbgVarsQAProve.Groups(i).Columns(2).CellStyleSet "IMGRESTRIC"
                                    m_bConAvisoRestricVarsQAProve = True
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

    Next i
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub sdbgVarsQAProve_SplitterMove(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsQAProve_SplitterMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Rellena el grid con las variables de calidad por proveedor y unidad. Se tiene en cuenta configuraci�n de
''' la vista, visibilidades "desligadas de la vista" de unidades y visibilidades de proveedores.
''' </summary>
''' <remarks>Llamada desde: CargarGridVarCalidad()      cmdInvertir_Click   Form_MouseMove  sdbgProveVarsQA_MouseMove
'''     sdbgVarsQAProve_MouseMove   ; Tiempo m�ximo:0,2</remarks>
Private Sub Rellenar_sdbgProveVarsQA()
    Dim oProv As CProveedor
    Dim oUnidadUnqa1 As CUnidadNegQA
    Dim oUnidadUnqa2 As CUnidadNegQA
    Dim oUnidadUnqa3 As CUnidadNegQA
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    'Ahora rellenamos la grid sdbgProveVarsQA con los datos de las adjudicaciones
    sdbgProveVarsQA.RemoveAll
    
    'configuraci�n de la vista unidades: m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas
    'configuraci�n de la vista variables: m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
    'configuraci�n de pantalla proveedores: oProv.VisibleEnGrafico
    'coleccion unidades con puntos: m_oUnqaEnProve (con datos o la de nivel 0 q va siempre)
    'coleccion variable#unidad#proveedor con puntos: m_oUnqaVarcalEnProve
    'colecci�n de variable-unidad invisibles: m_sVarsUnqaNoVisible
    'visibilidades "desligadas de la vista" de unidades: OcultaUnidadProve(oProv.Cod & "#" & oUnidadUnqa1.Id)
       
    For Each oProv In m_oProvesAsig
        If oProv.VisibleEnGrafico Then
            For Each oUnidadUnqa1 In m_oUnidadesQa.Unidades
                If Not OcultaUnidadProve(oProv.Cod & "#" & oUnidadUnqa1.Id) Then Rellenar_sdbgProveVarsQA_Unqa oUnidadUnqa1, oProv, 1
                For Each oUnidadUnqa2 In oUnidadUnqa1.UnidadesNegQA.Unidades
                    If Not OcultaUnidadProve(oProv.Cod & "#" & oUnidadUnqa2.Id) Then Rellenar_sdbgProveVarsQA_Unqa oUnidadUnqa2, oProv, 2
                    For Each oUnidadUnqa3 In oUnidadUnqa2.UnidadesNegQA.Unidades
                        If Not OcultaUnidadProve(oProv.Cod & "#" & oUnidadUnqa3.Id) Then Rellenar_sdbgProveVarsQA_Unqa oUnidadUnqa3, oProv, 3
                    Next
                Next
            Next
        End If
    Next
                       
    'Muestra los splitter
    If sdbgProveVarsQA.Groups.Count > 1 Then
        sdbgProveVarsQA.SplitterVisible = True
        If HayAlgunaVCVisible Then sdbgProveVarsQA.SplitterPos = 2
    End If
    sdbgProveVarsQA.MoveFirst

    Screen.MousePointer = vbNormal

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgProveVarsQA", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga la vista indicada por m_oProcesoSeleccionado.VistaDefectoQA y m_oVistaCalSeleccionada.Vista.
''' No en pantalla solo las colecciones y establece la caption del formulario
''' </summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1</remarks>
Public Sub CargarVistaActualCalidad()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    'Cargamos la configuraci�n de la vista por defecto para el proceso seleccionado
    Set m_oConfVistasCalGlobal = Nothing
    Set m_oConfVistasCalGlobal = oFSGSRaiz.Generar_CConfVistasCalGlobal

    
    ConfiguracionVistaActualQA m_oProcesoSeleccionado.VistaDefectoQA, m_oProcesoSeleccionado.VistaDefectoTipoQA
    m_udtTipoVQA = m_oProcesoSeleccionado.VistaDefectoTipoQA
    
    If m_udtTipoVQA = Vistainicialqa Then
        Me.caption = m_sCaption & " (" & m_sIdiInicial & ")"
    Else
        Me.caption = m_sCaption & " (" & m_sVistaGlobal & " " & m_oProcesoSeleccionado.NombreVistaQA & ")"
    End If
            
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CargarVistaActualCalidad", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Carga la vista indicada en caso de ser necesario. No en pantalla solo las colecciones. Establece
''' la var global m_oVistaCalSeleccionada q es la q se usaba para casi todo en este formulario, ahora
''' con la logica a�adidada de las unidades hay unas cuantas colecciones mas q se relacionan con
''' m_oVistaCalSeleccionada para mostrar/ocultar casi todo en este formulario.
''' </summary>
''' <param name="iVista">ID de la vista</param>
''' <param name="udtTipoVista">Tipo de la vista</param>
''' <remarks>Llamada desde: VistaDespuesDeEliminar  cmdGuardarVista_Click   cmdGuardarVistaNueva_Click
'''     sdbcVistaActual_CloseUp     CargarVistaActualCalidad; Tiempo m�ximo: 0,2</remarks>
Public Sub ConfiguracionVistaActualQA(ByVal iVista As Variant, ByVal udtTipoVista As TipoDeVistaqa)
    
    'Carga las configuraci�n de la vista
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oVistaCalSeleccionada = Nothing

    If m_oConfVistasCalGlobal.Item(CStr(iVista)) Is Nothing Then
        Select Case udtTipoVista
            Case Vistainicialqa
                If gParametrosGenerales.gbPymes Then
                    m_oConfVistasCalGlobal.Add 0, "", 0, True, True, gParametrosGenerales.gbQAVariableMaterialAsig, 2, 4500, 1400, 3600, 2500, 1500, 1000, 2500, 1400, 1600, 2500, 1500, 1000, 2200, basOptimizacion.gPYMEUsuario, TipoDeVistaqa.Vistainicialqa
                Else
                    m_oConfVistasCalGlobal.Add 0, "", 0, True, True, gParametrosGenerales.gbQAVariableMaterialAsig, 2, 4500, 1400, 3600, 2500, 1500, 1000, 2500, 1400, 1600, 2500, 1500, 1000, 2200, , TipoDeVistaqa.Vistainicialqa
                End If
            Case otravistaqa
                If gParametrosGenerales.gbPymes Then
                    m_oConfVistasCalGlobal.CargarTodasLasVistas iVista, , basOptimizacion.gPYMEUsuario
                Else
                    m_oConfVistasCalGlobal.CargarTodasLasVistas iVista
                End If
        End Select
    End If

    Set m_oVistaCalSeleccionada = m_oConfVistasCalGlobal.Item(CStr(iVista))
    
    m_udtTipoVQA = m_oVistaCalSeleccionada.TipoVista
    
    'Carga las configuraciones de las variables de calidad
    If m_oVistaCalSeleccionada.ConfVistasCalGlobalVar Is Nothing Then
        If basOptimizacion.gTipoDeUsuario = Administrador Then
            If gParametrosGenerales.gbPymes Then
                m_oVistaCalSeleccionada.CargarConfCalGlobalVar , Pyme:=basOptimizacion.gPYMEUsuario
            Else
                m_oVistaCalSeleccionada.CargarConfCalGlobalVar
            End If
        Else
            If gParametrosGenerales.gbPymes Then
                m_oVistaCalSeleccionada.CargarConfCalGlobalVar , basOptimizacion.gvarCodUsuario, Pyme:=basOptimizacion.gPYMEUsuario
            Else
                m_oVistaCalSeleccionada.CargarConfCalGlobalVar , basOptimizacion.gvarCodUsuario
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "ConfiguracionVistaActualQA", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga los grids de VarsProve y ProveVars
''' </summary>
''' <remarks>Llamada desde: VistaDespuesDeEliminar  Form_Load   sdbcVistaActual_CloseUp     cmdRenombrarVista_Click
'''     cmdGuardarVistaNueva_Click; Tiempo m�ximo: 0,5</remarks>
Public Sub CargarGridVarCalidad()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
        
    CargarProveVarsQA
    CargarVarsQAProve
    Rellenar_sdbgVarsQAProve
    Rellenar_sdbgProveVarsQA
    
    RedimensionarGrid

    If Not m_oVistaCalSeleccionada Is Nothing Then
        If m_oVistaCalSeleccionada.TipoVision = 0 Then
            sdbgVarsQAProve.Visible = True
            sdbgProveVarsQA.Visible = False
        Else
            sdbgVarsQAProve.Visible = False
            sdbgProveVarsQA.Visible = True
        End If
    End If
    
    If UBound(m_sVarsNoVisible) > 0 Then
        sdbgVarsQAProve.Groups(0).HeadStyleSet = "VariablesNoVisible"
        sdbgVarsQAProve.Groups(1).HeadStyleSet = "VariablesNoVisible"
        sdbgProveVarsQA.Groups(0).HeadStyleSet = "ProveedorNoVisible"
    Else
        sdbgVarsQAProve.Groups(0).HeadStyleSet = "Variables"
        sdbgVarsQAProve.Groups(1).HeadStyleSet = "Variables"
        sdbgProveVarsQA.Groups(0).HeadStyleSet = "Proveedor"
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CargarGridVarCalidad", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Guarda en BD los datos de la Vista
''' </summary>
''' <remarks>Llamada desde:Form_Unload  GuardarVistas; Tiempo m�ximo: 0,5</remarks>
Public Sub GuardarVistaGeneralEnBd()
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oConfVistaCal0 As CConfVistaCalGlobalVar
    Dim oConfVistaCal1 As CConfVistaCalGlobalVar
    Dim oConfVistaCal2 As CConfVistaCalGlobalVar
    Dim oConfVistaCal3 As CConfVistaCalGlobalVar
    Dim oConfVistaCal4 As CConfVistaCalGlobalVar
    Dim oConfVistaCal5 As CConfVistaCalGlobalVar

    'Guarda la configuraci�n de la vista actual
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oIBaseDatos = m_oVistaCalSeleccionada
    
    If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
        'Si no exist�a en base de datos la guarda la vista con los valores por defecto en base de datos
        teserror = oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        End If
        Set oIBaseDatos = Nothing

        If m_oVistaCalSeleccionada.Vista <> TipoDeVistaqa.Vistainicialqa Then
            'Inserta la configuraci�n de los atributos
            If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar Is Nothing Then
            
                For Each oConfVistaCal0 In m_oVistaCalSeleccionada.ConfVistasCalGlobalVar
                    Set oIBaseDatos = oConfVistaCal0
                    If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                        teserror = oIBaseDatos.AnyadirABaseDatos
                        If teserror.NumError <> TESnoerror Then
                           basErrores.TratarError teserror
                           oIBaseDatos.CancelarEdicion
                           Exit Sub
                        End If
                    End If
                    Set oIBaseDatos = Nothing
                    
                    If Not oConfVistaCal0.VariblesCalVista Is Nothing Then
                        For Each oConfVistaCal1 In oConfVistaCal0.VariblesCalVista
                            Set oIBaseDatos = oConfVistaCal1
                            If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                                teserror = oIBaseDatos.AnyadirABaseDatos
                                If teserror.NumError <> TESnoerror Then
                                   basErrores.TratarError teserror
                                   oIBaseDatos.CancelarEdicion
                                   Exit Sub
                                End If
                            End If
                            Set oIBaseDatos = Nothing
                    
                            If Not oConfVistaCal1.VariblesCalVista Is Nothing Then
                                For Each oConfVistaCal2 In oConfVistaCal1.VariblesCalVista
                                    Set oIBaseDatos = oConfVistaCal2
                                    If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                                        teserror = oIBaseDatos.AnyadirABaseDatos
                                        If teserror.NumError <> TESnoerror Then
                                           basErrores.TratarError teserror
                                           oIBaseDatos.CancelarEdicion
                                           Exit Sub
                                        End If
                                    End If
                                    Set oIBaseDatos = Nothing
                                    
                                    If Not oConfVistaCal2.VariblesCalVista Is Nothing Then
                                        For Each oConfVistaCal3 In oConfVistaCal2.VariblesCalVista
                                            Set oIBaseDatos = oConfVistaCal3
                                            If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                                                teserror = oIBaseDatos.AnyadirABaseDatos
                                                If teserror.NumError <> TESnoerror Then
                                                   basErrores.TratarError teserror
                                                   oIBaseDatos.CancelarEdicion
                                                   Exit Sub
                                                End If
                                            End If
                                            Set oIBaseDatos = Nothing
                                            
                                            If Not oConfVistaCal3.VariblesCalVista Is Nothing Then
                                                For Each oConfVistaCal4 In oConfVistaCal3.VariblesCalVista
                                                    Set oIBaseDatos = oConfVistaCal4
                                                    If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                                                        teserror = oIBaseDatos.AnyadirABaseDatos
                                                        If teserror.NumError <> TESnoerror Then
                                                           basErrores.TratarError teserror
                                                           oIBaseDatos.CancelarEdicion
                                                           Exit Sub
                                                        End If
                                                    End If
                                                    Set oIBaseDatos = Nothing
                                                    
                                                    If Not oConfVistaCal4.VariblesCalVista Is Nothing Then
                                                        For Each oConfVistaCal5 In oConfVistaCal4.VariblesCalVista
                                                            Set oIBaseDatos = oConfVistaCal5
                                                            If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                                                                teserror = oIBaseDatos.AnyadirABaseDatos
                                                                If teserror.NumError <> TESnoerror Then
                                                                   basErrores.TratarError teserror
                                                                   oIBaseDatos.CancelarEdicion
                                                                   Exit Sub
                                                                End If
                                                            End If
                                                            Set oIBaseDatos = Nothing
                                                        Next
                                                    End If
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        End If
    Else
        'Modifica
        teserror = oIBaseDatos.FinalizarEdicionModificando

        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        End If
        
        Set oIBaseDatos = Nothing
        
    End If
    m_oVistaCalSeleccionada.HayCambios = False
    m_bCambios = False
    
Set oConfVistaCal1 = Nothing
Set oConfVistaCal2 = Nothing
Set oConfVistaCal3 = Nothing
Set oConfVistaCal4 = Nothing
Set oConfVistaCal5 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "GuardarVistaGeneralEnBd", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


''' <summary>
''' Carga los datos de restricciones de una variables con sus subvariables si es compuesta
''' </summary>
''' <param name=" Id">Id de la variable de calidad</param>
''' <param name=" ID_VARCAL1">Id del padre de nivel 1</param>
''' <param name=" ID_VARCAL2">Id del padre de nivel 2</param>
''' <param name=" ID_VARCAL3">Id del padre de nivel 3</param>
''' <param name=" ID_VARCAL4">Id del padre de nivel 4</param>
''' <param name=" Nivel">Nivel de la variable</param>
''' <returns>Rellena el grid</returns>
''' <remarks>Llamada desde frmComparativaQA.sdbgVarsQAProve_BtnClick; Tiempo m�ximo:0,1</remarks>
Private Sub CargarVarsRestriccion(Optional ByVal Id As Variant, Optional ByVal ID_VARCAL1 As Variant, Optional ByVal ID_VARCAL2 As Variant, Optional ByVal ID_VARCAL3 As Variant, Optional ByVal ID_VARCAL4 As Variant, Optional ByVal Nivel As Variant)
    Dim oCalidad As CVariablesCalidad
    Dim oVarCal0 As CVariableCalidad
    Dim oVarCal1 As CVariableCalidad
    Dim oVarCal2 As CVariableCalidad
    Dim oVarCal3 As CVariableCalidad
    Dim oVarCal4 As CVariableCalidad
    Dim oVarCal5 As CVariableCalidad

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgVarsRestric.RemoveAll

    Set oCalidad = oFSGSRaiz.Generar_CVariablesCalidad
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        oCalidad.CargarVarsRestricc Id, ID_VARCAL1, ID_VARCAL2, ID_VARCAL3, ID_VARCAL4, Nivel, oUsuarioSummit.Cod
    Else
        oCalidad.CargarVarsRestricc Id, ID_VARCAL1, ID_VARCAL2, ID_VARCAL3, ID_VARCAL4, Nivel
    End If

    ''''
    sdbgVarsRestric.Groups(0).AllowSizing = True
    sdbgVarsRestric.Groups(1).AllowSizing = True
    sdbgVarsRestric.Groups(2).AllowSizing = True
    ''''
    sdbgVarsRestric.Groups(0).Columns(0).AllowSizing = True
    sdbgVarsRestric.Groups(0).Columns(1).AllowSizing = True
    sdbgVarsRestric.Groups(0).Columns(2).AllowSizing = True
    
    sdbgVarsRestric.Groups(1).Columns(0).AllowSizing = True
    sdbgVarsRestric.Groups(1).Columns(1).AllowSizing = True
    sdbgVarsRestric.Groups(1).Columns(2).AllowSizing = True
    
    sdbgVarsRestric.Groups(2).Columns(0).AllowSizing = True
    sdbgVarsRestric.Groups(2).Columns(1).AllowSizing = True
    sdbgVarsRestric.Groups(2).Columns(2).AllowSizing = True
    ''''
    
    For Each oVarCal0 In oCalidad
        sdbgVarsRestric.AddItem oVarCal0.Id & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & oVarCal0.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal0.RestriccionSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal0.RestriccionInf) & Chr(m_lSeparador) & DblToStr(oVarCal0.RestriccionSup) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal0.AvisoSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal0.AvisoInf) & Chr(m_lSeparador) & DblToStr(oVarCal0.AvisoSup) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        For Each oVarCal1 In oVarCal0.VariblesCal
            If oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                sdbgVarsRestric.AddItem oVarCal1.Id & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal1.RestriccionSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal1.RestriccionInf) & Chr(m_lSeparador) & DblToStr(oVarCal1.RestriccionSup) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal1.AvisoSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal1.AvisoInf) & Chr(m_lSeparador) & DblToStr(oVarCal1.AvisoSup) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
            End If
            If Not oVarCal1.VariblesCal Is Nothing Then
                For Each oVarCal2 In oVarCal1.VariblesCal
                    If oVarCal2.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                        sdbgVarsRestric.AddItem oVarCal2.Id & Chr(m_lSeparador) & "2" & Chr(m_lSeparador) & "    " & oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal2.RestriccionSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal2.RestriccionInf) & Chr(m_lSeparador) & DblToStr(oVarCal2.RestriccionSup) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal2.AvisoSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal2.AvisoInf) & Chr(m_lSeparador) & DblToStr(oVarCal2.AvisoSup) & Chr(m_lSeparador) & oVarCal2.IdVarCal1 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    End If
                    If Not oVarCal2.VariblesCal Is Nothing Then
                        For Each oVarCal3 In oVarCal2.VariblesCal
                            If oVarCal3.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                                sdbgVarsRestric.AddItem oVarCal3.Id & Chr(m_lSeparador) & "3" & Chr(m_lSeparador) & "          " & oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal3.RestriccionSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal3.RestriccionInf) & Chr(m_lSeparador) & DblToStr(oVarCal3.RestriccionSup) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal3.AvisoSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal3.AvisoInf) & Chr(m_lSeparador) & DblToStr(oVarCal3.AvisoSup) & Chr(m_lSeparador) & oVarCal3.IdVarCal1 & Chr(m_lSeparador) & oVarCal3.IdVarCal2 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                            End If
                            If Not oVarCal3.VariblesCal Is Nothing Then
                                For Each oVarCal4 In oVarCal3.VariblesCal
                                    If oVarCal4.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                                        sdbgVarsRestric.AddItem oVarCal4.Id & Chr(m_lSeparador) & "4" & Chr(m_lSeparador) & "               " & oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal4.RestriccionSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal4.RestriccionInf) & Chr(m_lSeparador) & DblToStr(oVarCal4.RestriccionSup) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal4.AvisoSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal4.AvisoInf) & Chr(m_lSeparador) & DblToStr(oVarCal4.AvisoSup) & Chr(m_lSeparador) & oVarCal4.IdVarCal1 & Chr(m_lSeparador) & oVarCal4.IdVarCal2 & Chr(m_lSeparador) & oVarCal4.IdVarCal3 & Chr(m_lSeparador) & ""
                                    End If
                                    If Not oVarCal4.VariblesCal Is Nothing Then
                                        For Each oVarCal5 In oVarCal4.VariblesCal
                                            If oVarCal5.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                                                sdbgVarsRestric.AddItem oVarCal5.Id & Chr(m_lSeparador) & "5" & Chr(m_lSeparador) & "                   " & oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal5.RestriccionSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal5.RestriccionInf) & Chr(m_lSeparador) & DblToStr(oVarCal5.RestriccionSup) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal5.AvisoSiNo) & Chr(m_lSeparador) & DblToStr(oVarCal5.AvisoInf) & Chr(m_lSeparador) & DblToStr(oVarCal5.AvisoSup) & Chr(m_lSeparador) & oVarCal5.IdVarCal1 & Chr(m_lSeparador) & oVarCal5.IdVarCal2 & Chr(m_lSeparador) & oVarCal5.IdVarCal3 & Chr(m_lSeparador) & oVarCal5.IdVarCal4
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next

    Next

    Set oCalidad = Nothing
    Set oVarCal0 = Nothing
    Set oVarCal1 = Nothing
    Set oVarCal2 = Nothing
    Set oVarCal3 = Nothing
    Set oVarCal4 = Nothing
    Set oVarCal5 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CargarVarsRestriccion", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


''' <summary>
''' Oculta el tooltip de los intervalos � de las variables invisibles q implicar�an q al menos un proveedor sea
''' no adjudicable � adjudicable solo tras un aviso
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde esta  el rat�n</param>
''' <param name="Y">Coordenada X donde esta  el rat�n</param>
''' <remarks>Llamada desde: Evento que salta al mover el rat�n sobre el grid; Tiempo m�ximo:0</remarks>
Private Sub sdbgVarsRestric_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picNoVisibles.Visible = False
    If Not m_bDeshabilidar_MouseMove Then picUmbrales.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsRestric_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Evento de rat�n sobre el grid sdbgVarsRestric. Si es sobre el grupo "variables" muestra el tooltip
''' de los intervalos
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde se cliqueo</param>
''' <param name="Y">Coordenada X donde se cliqueo</param>
''' <remarks>Llamada desde: Evento que salta al soltar el rat�n sobre el grid; Tiempo m�ximo:0</remarks>
Private Sub sdbgVarsRestric_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.sdbgVarsRestric.Grp = 0 Then
        If (Me.sdbgVarsRestric.WhereIs(X, Y) = ssWhereIsData) Then
            If sdbgVarsRestric.Groups(1).Columns("R_INF").Value <> "" _
            Or sdbgVarsRestric.Groups(1).Columns("R_SUP").Value <> "" _
            Or sdbgVarsRestric.Groups(2).Columns("A_INF").Value <> "" _
            Or sdbgVarsRestric.Groups(2).Columns("A_SUP").Value <> "" Then
                m_bDeshabilidar_MouseMove = True
                
                picUmbrales.Visible = False
                
                RellenarTooltip (2)
                lblAdjProhibida.Visible = False
                                     
                If X + picUmbrales.Width > Me.Width Then
                    picUmbrales.Left = Me.Width - picUmbrales.Width - 600
                Else
                    picUmbrales.Left = X - 100
                End If
                If Y + picUmbrales.Height > Me.Height Then
                    picUmbrales.Top = Me.Height - picUmbrales.Height - 200
                Else
                    picUmbrales.Top = Y - 200
                End If
                
                picUmbrales.Visible = True
                
                Me.Timer1.Enabled = True
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsRestric_MouseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub sdbgVarsRestric_RowLoaded(ByVal Bookmark As Variant)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case sdbgVarsRestric.Columns("NIVEL").Value
        Case 0, 1
            If sdbgVarsRestric.Columns("VARCAL").Value = m_iIDRest And m_iNivelRest = 1 Then
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel1Sel"
            Else
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel1"
            End If
        Case 2
            If sdbgVarsRestric.Columns("VARCAL").Value = m_iIDRest And m_iNivelRest = 2 Then
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel2Sel"
            Else
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel2"
            End If
        Case 3
            If sdbgVarsRestric.Columns("VARCAL").Value = m_iIDRest And m_iNivelRest = 3 Then
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel3Sel"
            Else
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel3"
            End If
        Case 4
            If sdbgVarsRestric.Columns("VARCAL").Value = m_iIDRest And m_iNivelRest = 4 Then
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel4Sel"
            Else
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel4"
            End If
        Case 5
            If sdbgVarsRestric.Columns("VARCAL").Value = m_iIDRest And m_iNivelRest = 5 Then
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel5Sel"
            Else
                sdbgVarsRestric.Columns("DEN").CellStyleSet "Nivel5"
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "sdbgVarsRestric_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Rellenar el Tooltip con los intervalos de aviso/restricci�n (de haberlos) de la variable seleccionada.
''' Tambien indica si realmente hay algo q mostrar.
''' </summary>
''' <param name="TipoVision">Visible 0- VarsProve 1- ProveVars 2- Restricciones</param>
''' <returns>Si realmente hay algo q mostrar</returns>
''' <remarks>Llamada desde: sdbgProveVarsQA_MouseUp     sdbgVarsQAProve_MouseUp     sdbgVarsRestric_MouseUp
''' ; Tiempo m�ximo: 0,1</remarks>
Private Function RellenarTooltip(ByVal TipoVision As Integer) As Boolean
Dim i As Integer
Dim oVarCal As CVariablesCalidad
Dim rs As ADODB.Recordset
Dim sDatos_Variable() As String
Dim sRestriccionMenor As String
Dim sRestriccionMayor As String
Dim sAvisoMenor As String
Dim sAvisoMayor As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If TipoVision = 0 Then 'sdbgVarsQAProve
        sDatos_Variable = Split(sdbgVarsQAProve.Groups(0).Columns("DATOS_VAR").Value, "#")
                
        Me.lblDenProve.caption = CStr(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).TagVariant)
        Me.lblDenVarCal.caption = sdbgVarsQAProve.Groups(0).Columns("DEN").Value & " (" & m_sNivel & " " & sDatos_Variable(POS_NIVEL) & ")"
        If sDatos_Variable(POS_NIVEL) > 0 Then
            Me.lblDenVarCal.caption = sdbgVarsQAProve.Groups(0).Columns("COD").Value & " - " & Me.lblDenVarCal.caption
        End If
        Me.lblDenUnidad.caption = sdbgVarsQAProve.Groups(1).Columns("UNQA").Value
        Me.lblPuntProv.caption = CStr(Format(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value, FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)))
        
        If CStr(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) = "" Then
            RellenarTooltip = False
            Exit Function
        End If
        
        If sDatos_Variable(POS_RESTRICCION) <> "" Then
            If sDatos_Variable(POS_RESTRICCION) Then
                If sDatos_Variable(POS_R_INF) <> "" And sDatos_Variable(POS_R_SUP) <> "" Then
                    If CDbl(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) >= CDbl(sDatos_Variable(POS_R_INF)) And CDbl(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                        Me.lblAdjAviso.Visible = False
                        Me.lblAdjProhibida.Visible = True
                        
                        Me.lblTipoUmbral.caption = m_sBloqueo
                        
                        sRestriccionMayor = m_sMayoresDe & " "
                        sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sDatos_Variable(POS_R_INF), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                        Me.lblMayorUmbral.caption = sRestriccionMayor
                        
                        sRestriccionMenor = m_sMenoresDe & " "
                        sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sDatos_Variable(POS_R_SUP), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                        Me.lblMenorUmbral.caption = sRestriccionMenor
                        
                        Me.lblMenorUmbral.Visible = True
                        
                        RellenarTooltip = True
                        
                        Exit Function
                    End If
                Else
                    If sDatos_Variable(POS_R_INF) <> "" Then
                        If CDbl(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) >= CDbl(sDatos_Variable(POS_R_INF)) Then
                            Me.lblAdjAviso.Visible = False
                            Me.lblAdjProhibida.Visible = True

                            Me.lblTipoUmbral.caption = m_sBloqueo
                            
                            sRestriccionMayor = m_sMayoresDe & " "
                            sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sDatos_Variable(POS_R_INF), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                            Me.lblMayorUmbral.caption = sRestriccionMayor
                            
                            Me.lblMenorUmbral.Visible = False
                            
                            RellenarTooltip = True
                        
                            Exit Function
                        End If
                    Else
                        If sDatos_Variable(POS_R_SUP) <> "" Then
                            If CDbl(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                                Me.lblAdjAviso.Visible = False
                                Me.lblAdjProhibida.Visible = True
                                
                                Me.lblTipoUmbral.caption = m_sBloqueo
                                
                                sRestriccionMenor = m_sMenoresDe & " "
                                sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sDatos_Variable(POS_R_SUP), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                                Me.lblMayorUmbral.caption = sRestriccionMenor
                                
                                Me.lblMenorUmbral.Visible = False
                                
                                RellenarTooltip = True
                        
                                Exit Function
                            End If
                        End If
                    End If
                End If
            End If
        End If
        
        If sDatos_Variable(POS_AVISO) <> "" Then
            If sDatos_Variable(POS_AVISO) Then
                If sDatos_Variable(POS_A_INF) <> "" And sDatos_Variable(POS_A_SUP) <> "" Then
                    If CDbl(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) >= CDbl(sDatos_Variable(POS_A_INF)) And CDbl(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                        Me.lblAdjAviso.Visible = True
                        Me.lblAdjProhibida.Visible = False
                        
                        Me.lblTipoUmbral.caption = m_sAviso
                        
                        sRestriccionMayor = m_sMayoresDe & " "
                        sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sDatos_Variable(POS_A_INF), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                        Me.lblMayorUmbral.caption = sRestriccionMayor
                        
                        sRestriccionMenor = m_sMenoresDe & " "
                        sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sDatos_Variable(POS_A_SUP), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                        Me.lblMenorUmbral.caption = sRestriccionMenor
                        
                        Me.lblMenorUmbral.Visible = True
                        
                        RellenarTooltip = True
                        
                        Exit Function
                    End If
                Else
                    If sDatos_Variable(POS_A_INF) <> "" Then
                        If CDbl(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) >= CDbl(sDatos_Variable(POS_A_INF)) Then
                            Me.lblAdjAviso.Visible = True
                            Me.lblAdjProhibida.Visible = False
                            
                            Me.lblTipoUmbral.caption = m_sAviso

                            sRestriccionMayor = m_sMayoresDe & " "
                            sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sDatos_Variable(POS_A_INF), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                            Me.lblMayorUmbral.caption = sRestriccionMayor

                            Me.lblMenorUmbral.Visible = False
                            
                            RellenarTooltip = True
                        
                            Exit Function
                        End If
                    Else
                        If sDatos_Variable(POS_A_SUP) <> "" Then
                            If CDbl(sdbgVarsQAProve.Groups(sdbgVarsQAProve.Grp).Columns(1).Value) <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                                Me.lblAdjAviso.Visible = True
                                Me.lblAdjProhibida.Visible = False
                                
                                Me.lblTipoUmbral.caption = m_sAviso
                                
                                sRestriccionMenor = m_sMenoresDe & " "
                                sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sDatos_Variable(POS_A_SUP), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                                Me.lblMayorUmbral.caption = sRestriccionMenor
                                
                                Me.lblMenorUmbral.Visible = False
                                
                                RellenarTooltip = True
                        
                                Exit Function
                            End If
                        End If
                    End If
                End If
            End If
        End If
        
            
    ElseIf TipoVision = 1 Then
        sDatos_Variable = Split(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(2).Value, "#")
                
        If CStr(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) = "" Then
            RellenarTooltip = False
            Exit Function
        End If
        
        Me.lblDenProve = CStr(sdbgProveVarsQA.Groups(0).Columns("COD").Value)
        
        Set oVarCal = oFSGSRaiz.Generar_CVariablesCalidad
        Set rs = oVarCal.ObtenerCodDenVariable(sDatos_Variable(POS_ID), sDatos_Variable(POS_NIVEL), gParametrosInstalacion.gIdioma)
        Me.lblDenVarCal.caption = CStr(rs.Fields("DEN").Value) & " (" & m_sNivel & " " & sDatos_Variable(POS_NIVEL) & ")"
        If sDatos_Variable(POS_NIVEL) > 0 Then
            Me.lblDenVarCal.caption = CStr(rs.Fields("COD").Value) & " - " & Me.lblDenVarCal.caption
        End If
        Set rs = Nothing
        
        Me.lblDenUnidad = CStr(sdbgProveVarsQA.Groups(1).Columns(0).Value)
        Me.lblPuntProv.caption = CStr(Format(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value, FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False)))
        
        If sDatos_Variable(POS_RESTRICCION) <> "" Then
            If sDatos_Variable(POS_RESTRICCION) Then
                If sDatos_Variable(POS_R_INF) <> "" And sDatos_Variable(POS_R_SUP) <> "" Then
                    If CDbl(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) >= CDbl(sDatos_Variable(POS_R_INF)) And CDbl(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                        Me.lblAdjAviso.Visible = False
                        Me.lblAdjProhibida.Visible = True
                        
                        Me.lblTipoUmbral.caption = m_sBloqueo
                        
                        sRestriccionMayor = m_sMayoresDe & " "
                        sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sDatos_Variable(POS_R_INF), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                        Me.lblMayorUmbral.caption = sRestriccionMayor
                        
                        sRestriccionMenor = m_sMenoresDe & " "
                        sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sDatos_Variable(POS_R_SUP), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                        Me.lblMenorUmbral.caption = sRestriccionMenor
                        
                        Me.lblMenorUmbral.Visible = True
                        
                        RellenarTooltip = True
                        
                        Exit Function
                    End If
                Else
                    If sDatos_Variable(POS_R_INF) <> "" Then
                        If CDbl(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) >= CDbl(sDatos_Variable(POS_R_INF)) Then
                            Me.lblAdjAviso.Visible = False
                            Me.lblAdjProhibida.Visible = True

                            Me.lblTipoUmbral.caption = m_sBloqueo
                            
                            sRestriccionMayor = m_sMayoresDe & " "
                            sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sDatos_Variable(POS_R_INF), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                            Me.lblMayorUmbral.caption = sRestriccionMayor
                            
                            Me.lblMenorUmbral.Visible = False
                            
                            RellenarTooltip = True
                        
                            Exit Function
                        End If
                    Else
                        If sDatos_Variable(POS_R_SUP) <> "" Then
                            If CDbl(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                                Me.lblAdjAviso.Visible = False
                                Me.lblAdjProhibida.Visible = True
                                
                                Me.lblTipoUmbral.caption = m_sBloqueo
                                
                                sRestriccionMenor = m_sMenoresDe & " "
                                sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sDatos_Variable(POS_R_SUP), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                                Me.lblMayorUmbral.caption = sRestriccionMenor
                                
                                Me.lblMenorUmbral.Visible = False
                                
                                RellenarTooltip = True
                        
                                Exit Function
                            End If
                        End If
                    End If
                End If
            End If
        End If
        
        If sDatos_Variable(POS_AVISO) <> "" Then
            If sDatos_Variable(POS_AVISO) Then
                If sDatos_Variable(POS_A_INF) <> "" And sDatos_Variable(POS_A_SUP) <> "" Then
                    If CDbl(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) >= CDbl(sDatos_Variable(POS_A_INF)) And CDbl(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                        Me.lblAdjAviso.Visible = True
                        Me.lblAdjProhibida.Visible = False
                        
                        Me.lblTipoUmbral.caption = m_sAviso
                        
                        sRestriccionMayor = m_sMayoresDe & " "
                        sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sDatos_Variable(POS_A_INF), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                        Me.lblMayorUmbral.caption = sRestriccionMayor
                        
                        sRestriccionMenor = m_sMenoresDe & " "
                        sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sDatos_Variable(POS_A_SUP), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                        Me.lblMenorUmbral.caption = sRestriccionMenor
                        
                        Me.lblMenorUmbral.Visible = True
                        
                        RellenarTooltip = True
                        
                        Exit Function
                    End If
                Else
                    If sDatos_Variable(POS_A_INF) <> "" Then
                        If CDbl(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) >= CDbl(sDatos_Variable(POS_A_INF)) Then
                            Me.lblAdjAviso.Visible = True
                            Me.lblAdjProhibida.Visible = False
                            
                            Me.lblTipoUmbral.caption = m_sAviso

                            sRestriccionMayor = m_sMayoresDe & " "
                            sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sDatos_Variable(POS_A_INF), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                            Me.lblMayorUmbral.caption = sRestriccionMayor

                            Me.lblMenorUmbral.Visible = False
                            
                            RellenarTooltip = True
                        
                            Exit Function
                        End If
                    Else
                        If sDatos_Variable(POS_A_SUP) <> "" Then
                            If CDbl(sdbgProveVarsQA.Groups(sdbgProveVarsQA.Grp).Columns(1).Value) <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                                Me.lblAdjAviso.Visible = True
                                Me.lblAdjProhibida.Visible = False
                                
                                Me.lblTipoUmbral.caption = m_sAviso
                                
                                sRestriccionMenor = m_sMenoresDe & " "
                                sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sDatos_Variable(POS_A_SUP), FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
                                Me.lblMayorUmbral.caption = sRestriccionMenor
                                
                                Me.lblMenorUmbral.Visible = False
                                
                                RellenarTooltip = True
                        
                                Exit Function
                            End If
                        End If
                    End If
                End If
            End If
        End If
                   
    Else
        Me.lblAdjAviso.Visible = False
        Me.lblAdjProhibida.Visible = False
        
        Me.lblProve.caption = ""
        Me.lblDenProve.Visible = False
        
        Me.lblUnidad.Visible = False
        Me.lblDenUnidad.Visible = False
        
        Me.lblVarCal.Visible = False
        Me.lblDenVarCal.Visible = False
        
        Me.lblPuntProve.Visible = False
        Me.lblPuntProv.Visible = False
    
        Me.lnUmbral.Visible = False
    
        Me.lblTipoUmbral.Visible = False
        Me.lblMayorUmbral.Visible = False
        Me.lblMenorUmbral.Visible = False
        
        Me.lblProve.Top = Me.lblAdjAviso.Top
        Me.lblProve.Left = Me.lblAdjAviso.Left
        Me.lblProve.Width = Me.lblAdjAviso.Width
        Me.lblProve.Height = Me.picUmbrales.Height
        
        Set oVarCal = oFSGSRaiz.Generar_CVariablesCalidad
        Set rs = oVarCal.ObtenerCodDenVariable(sdbgVarsRestric.Groups(0).Columns("VARCAL").Value, sdbgVarsRestric.Groups(0).Columns("NIVEL").Value, gParametrosInstalacion.gIdioma)
        lblProve.caption = Chr(13) & Space(3) & m_sVarNivel & " " & sdbgVarsRestric.Groups(0).Columns("NIVEL").Value & ": " & CStr(rs.Fields("COD").Value) & " - " & CStr(rs.Fields("DEN").Value)
        Set rs = Nothing
        If CLng(sdbgVarsRestric.Groups(0).Columns("NIVEL").Value) > 1 Then
            For i = CLng(sdbgVarsRestric.Groups(0).Columns("NIVEL").Value) - 1 To 1 Step -1
                Select Case i
                    Case 1
                        Set rs = oVarCal.ObtenerCodDenVariable(sdbgVarsRestric.Columns("ID_VAR_CAL1").Value, i, gParametrosInstalacion.gIdioma)
                    Case 2
                        Set rs = oVarCal.ObtenerCodDenVariable(sdbgVarsRestric.Columns("ID_VAR_CAL2").Value, i, gParametrosInstalacion.gIdioma)
                    Case 3
                        Set rs = oVarCal.ObtenerCodDenVariable(sdbgVarsRestric.Columns("ID_VAR_CAL3").Value, i, gParametrosInstalacion.gIdioma)
                    Case 4
                        Set rs = oVarCal.ObtenerCodDenVariable(sdbgVarsRestric.Columns("ID_VAR_CAL4").Value, i, gParametrosInstalacion.gIdioma)
                End Select
                If Not rs.EOF Then
                    lblProve.caption = lblProve.caption & Chr(13) & Space(3) & Space(3 * (sdbgVarsRestric.Groups(0).Columns("NIVEL").Value - i)) & m_sNivel & " " & CStr(i) & ": " & CStr(rs.Fields("COD").Value) & " - " & CStr(rs.Fields("DEN").Value)
                End If
                Set rs = Nothing
            Next i
        End If
        lblProve.caption = lblProve.caption & Chr(13) & " "
        If sdbgVarsRestric.Groups(1).Columns("R_INF").Value <> "" Or sdbgVarsRestric.Groups(1).Columns("R_SUP").Value <> "" Then
            lblProve.caption = lblProve.caption & Chr(13) & Space(3) & m_sBloqueo
        End If
        If sdbgVarsRestric.Groups(1).Columns("R_INF").Value <> "" Then
            sRestriccionMayor = m_sMayoresDe & " "
            sRestriccionMayor = Replace(sRestriccionMayor, " X ", " " & CStr(Format(sdbgVarsRestric.Groups(1).Columns("R_INF").Value, FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
            lblProve.caption = lblProve.caption & Chr(13) & Space(9) & sRestriccionMayor
        End If
        If sdbgVarsRestric.Groups(1).Columns("R_SUP").Value <> "" Then
            sRestriccionMenor = m_sMenoresDe & " "
            sRestriccionMenor = Replace(sRestriccionMenor, " Y ", " " & CStr(Format(sdbgVarsRestric.Groups(1).Columns("R_SUP").Value, FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
            lblProve.caption = lblProve.caption & Chr(13) & Space(9) & sRestriccionMenor
        End If
        If sdbgVarsRestric.Groups(2).Columns("A_INF").Value <> "" Or sdbgVarsRestric.Groups(2).Columns("A_SUP").Value <> "" Then
            lblProve.caption = lblProve.caption & Chr(13) & Space(3) & m_sAviso
        End If
        If sdbgVarsRestric.Groups(2).Columns("A_INF").Value <> "" Then
            sAvisoMayor = m_sMayoresDe & " "
            sAvisoMayor = Replace(sAvisoMayor, " X ", " " & CStr(Format(sdbgVarsRestric.Groups(2).Columns("A_INF").Value, FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
            lblProve.caption = lblProve.caption & Chr(13) & Space(9) & sAvisoMayor
        End If
        If sdbgVarsRestric.Groups(2).Columns("A_SUP").Value <> "" Then
            sAvisoMenor = m_sMenoresDe & " "
            sAvisoMenor = Replace(sAvisoMenor, " Y ", " " & CStr(Format(sdbgVarsRestric.Groups(2).Columns("A_SUP").Value, FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))) & " ")
            lblProve.caption = lblProve.caption & Chr(13) & Space(9) & sAvisoMenor
        End If
        
        RellenarTooltip = True
                        
        Exit Function
        
    End If
    
    RellenarTooltip = False
    
    Set oVarCal = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "RellenarTooltip", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Establecer las diferentes anchos para la vista selccionada a lo q esta en pantalla.
''' Anoto en el codigo un par de problemas encontrados y la soluci�n dada.
''' </summary>
''' <remarks>Llamada desde:GuardarVistas    cmdGuardarVista_Click   cmdGuardarVistaNueva_Click  Form_Unload
''' ; Tiempo m�ximo: 0,4</remarks>
Private Sub GuardarVistaGeneral()
Dim i As Long
Dim iScroll As Integer
Dim iScroll2 As Integer
Dim iGrupo As Long
Dim blnHayGruposVisibles As Boolean

    'Si se ha hecho un scroll se scrolla otra vez para dejarlo en la posici�n 0
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgVarsQAProve.GrpPosition(0) > 0 Then
        iScroll = sdbgVarsQAProve.Groups(0).Position
        sdbgVarsQAProve.Scroll -iScroll, 0
        sdbgVarsQAProve.Update
    End If
    If sdbgProveVarsQA.GrpPosition(0) > 0 Then
        iScroll2 = sdbgProveVarsQA.Groups(0).Position
        sdbgProveVarsQA.Scroll -iScroll2, 0
        sdbgProveVarsQA.Update
    End If
    For i = 0 To sdbgVarsQAProve.Groups.Count - 1
        sdbgVarsQAProve.Groups(i).Position = i
    Next i
    For i = 0 To sdbgProveVarsQA.Groups.Count - 1
        sdbgProveVarsQA.Groups(i).Position = i
    Next i

    m_oVistaCalSeleccionada.Width1_grupo_var = sdbgVarsQAProve.Groups(0).Width
    m_oVistaCalSeleccionada.Width1_cod_var = sdbgVarsQAProve.Groups(0).Columns("COD").Width
    m_oVistaCalSeleccionada.Width1_den_var = sdbgVarsQAProve.Groups(0).Columns("DEN").Width
    
    'Nos quedamos con el primer grupo que est� visible
    blnHayGruposVisibles = False
    For iGrupo = 2 To sdbgVarsQAProve.Groups.Count - 1
        If sdbgVarsQAProve.Groups(iGrupo).Visible = True Then
            blnHayGruposVisibles = True
            Exit For
        End If
    Next
    
    m_oVistaCalSeleccionada.Width1_grupo_prove = sdbgVarsQAProve.Groups(iGrupo).Width
    
    If m_oVistaCalSeleccionada.TipoVision = 0 Then
        m_oVistaCalSeleccionada.Width3_grupo_unqa = sdbgVarsQAProve.Groups(1).Width
    Else
        m_oVistaCalSeleccionada.Width3_grupo_unqa = Me.sdbgProveVarsQA.Groups(1).Width
    End If
        

    If m_oVistaCalSeleccionada.Calificaciones = True And m_oVistaCalSeleccionada.Puntos = True Then
        m_oVistaCalSeleccionada.Width1_calificacion = sdbgVarsQAProve.Groups(iGrupo).Columns(0).Width
        m_oVistaCalSeleccionada.Width1_puntos = sdbgVarsQAProve.Groups(iGrupo).Columns(1).Width
    ElseIf m_oVistaCalSeleccionada.Calificaciones = False And m_oVistaCalSeleccionada.Puntos = False Then
    Else
        'Dos problemas
        'Primero: Perfecto, como redimensiones sin ver una de las columnas, te redimensiona las dos columnas??
        'Pues NO, el problema es lo q no hace con la invisible.
        'He pasado de ancho de grupo 3.000 (Col Visible 3.000 Col Invisible 2.500)
        '       Caso1: a ancho de grupo 2.600 (Col Visible 2.600 Col Invisible 2.500) Sin mayor problema al mostrar
        '           la invisible: ancho de grupo 2.600 (Col Visible 100 Col Visible 2.500). Ve y puede redimensionar
        '       Caso2: a ancho de grupo 2.300 (Col Visible 2.300 Col Invisible 2.500) Ups, como grabes estos numeros
        '           al mostrar la invisible: ancho de grupo 2.300 (Col Visible -200 Col Visible 2.500) S�, menos
        '           doscientos de ancho.
        '           Lo he solucionado de manera salomonica, la mitad del ancho para cada uno.
        'Segundo: Aparte, entras viendo solo la columna Calif, ocultas Calif, muestras Puntos. Y ahora resulta q
        'tanto Calif como Puntos tienen como ancho el grupo.
        '       ancho de grupo 2.300 (Col Calif 2.300 Col Puntos 2.500)
        '       pasa a: ancho de grupo 2.300 (Col Calif 2.300 Col Puntos 2.300)
        'Lo he solucionado de tal forma q a bbdd (suponiendo q Width1_puntos 2.000) va a ir ancho de grupo
        '2.300 (Col Calif 300 Col Puntos 2.000)
        If sdbgVarsQAProve.Groups(iGrupo).Columns(1).Visible = False Then
            If sdbgVarsQAProve.Groups(iGrupo).Columns(1).Width = sdbgVarsQAProve.Groups(iGrupo).Width Then
                'Problema segundo.
                m_oVistaCalSeleccionada.Width1_calificacion = sdbgVarsQAProve.Groups(iGrupo).Width - m_oVistaCalSeleccionada.Width1_puntos
            Else
                m_oVistaCalSeleccionada.Width1_calificacion = sdbgVarsQAProve.Groups(iGrupo).Width - sdbgVarsQAProve.Groups(iGrupo).Columns(1).Width
                m_oVistaCalSeleccionada.Width1_puntos = sdbgVarsQAProve.Groups(iGrupo).Columns(1).Width
            End If
            'Evita negativos. Problema primero y no se descarta q Problema segundo provoque negativos.
            If m_oVistaCalSeleccionada.Width1_calificacion < 0 Then
                m_oVistaCalSeleccionada.Width1_calificacion = sdbgVarsQAProve.Groups(iGrupo).Width / 2
                m_oVistaCalSeleccionada.Width1_puntos = sdbgVarsQAProve.Groups(iGrupo).Width - m_oVistaCalSeleccionada.Width1_calificacion
            End If
        Else
            If sdbgVarsQAProve.Groups(iGrupo).Columns(0).Width = sdbgVarsQAProve.Groups(iGrupo).Width Then
                m_oVistaCalSeleccionada.Width1_puntos = sdbgVarsQAProve.Groups(iGrupo).Width - m_oVistaCalSeleccionada.Width1_calificacion
            Else
                m_oVistaCalSeleccionada.Width1_puntos = sdbgVarsQAProve.Groups(iGrupo).Width - sdbgVarsQAProve.Groups(iGrupo).Columns(0).Width
                m_oVistaCalSeleccionada.Width1_calificacion = sdbgVarsQAProve.Groups(iGrupo).Columns(0).Width
            End If
            
            If m_oVistaCalSeleccionada.Width1_puntos < 0 Then
                m_oVistaCalSeleccionada.Width1_puntos = sdbgVarsQAProve.Groups(iGrupo).Width / 2
                m_oVistaCalSeleccionada.Width1_calificacion = sdbgVarsQAProve.Groups(iGrupo).Width - m_oVistaCalSeleccionada.Width1_puntos
            End If
        End If
    End If
    
    m_oVistaCalSeleccionada.Width2_grupo_prove = sdbgProveVarsQA.Groups(0).Width
    m_oVistaCalSeleccionada.Width2_cod_prove = sdbgProveVarsQA.Groups(0).Columns("COD").Width
    m_oVistaCalSeleccionada.Width2_den_prove = sdbgProveVarsQA.Groups(0).Columns("DEN").Width
    
    'Nos quedamos con el primer grupo que est� visible
    blnHayGruposVisibles = False
    For iGrupo = 2 To sdbgProveVarsQA.Groups.Count - 1
        If sdbgProveVarsQA.Groups(iGrupo).Visible = True Then
            blnHayGruposVisibles = True
            Exit For
        End If
    Next
    
    If blnHayGruposVisibles Then
        m_oVistaCalSeleccionada.Width2_grupo_var = sdbgProveVarsQA.Groups(iGrupo).Width
            
        If m_oVistaCalSeleccionada.Calificaciones = True And m_oVistaCalSeleccionada.Puntos = True Then
            m_oVistaCalSeleccionada.Width2_calificacion = sdbgProveVarsQA.Groups(iGrupo).Columns(0).Width
            m_oVistaCalSeleccionada.Width2_puntos = sdbgProveVarsQA.Groups(iGrupo).Columns(1).Width
        ElseIf m_oVistaCalSeleccionada.Calificaciones = False And m_oVistaCalSeleccionada.Puntos = False Then
        Else
            If sdbgProveVarsQA.Groups(iGrupo).Columns(1).Visible = False Then
                If sdbgProveVarsQA.Groups(iGrupo).Columns(1).Width = sdbgProveVarsQA.Groups(iGrupo).Width Then
                    m_oVistaCalSeleccionada.Width2_calificacion = sdbgProveVarsQA.Groups(iGrupo).Width - m_oVistaCalSeleccionada.Width2_puntos
                Else
                    m_oVistaCalSeleccionada.Width2_calificacion = sdbgProveVarsQA.Groups(iGrupo).Width - sdbgProveVarsQA.Groups(iGrupo).Columns(1).Width
                    m_oVistaCalSeleccionada.Width2_puntos = sdbgProveVarsQA.Groups(iGrupo).Columns(1).Width
                End If
                If m_oVistaCalSeleccionada.Width2_calificacion < 0 Then
                    m_oVistaCalSeleccionada.Width2_calificacion = sdbgProveVarsQA.Groups(iGrupo).Width / 2
                    m_oVistaCalSeleccionada.Width2_puntos = sdbgProveVarsQA.Groups(iGrupo).Width - m_oVistaCalSeleccionada.Width2_calificacion
                End If
            Else
                If sdbgProveVarsQA.Groups(iGrupo).Columns(0).Width = sdbgProveVarsQA.Groups(iGrupo).Width Then
                    m_oVistaCalSeleccionada.Width2_puntos = sdbgProveVarsQA.Groups(iGrupo).Width - m_oVistaCalSeleccionada.Width2_calificacion
                Else
                    m_oVistaCalSeleccionada.Width2_puntos = sdbgProveVarsQA.Groups(iGrupo).Width - sdbgProveVarsQA.Groups(iGrupo).Columns(0).Width
                    m_oVistaCalSeleccionada.Width2_calificacion = sdbgProveVarsQA.Groups(iGrupo).Columns(0).Width
                End If
                
                If m_oVistaCalSeleccionada.Width2_puntos < 0 Then
                    m_oVistaCalSeleccionada.Width2_puntos = sdbgProveVarsQA.Groups(iGrupo).Width / 2
                    m_oVistaCalSeleccionada.Width2_calificacion = sdbgProveVarsQA.Groups(iGrupo).Width - m_oVistaCalSeleccionada.Width2_puntos
                End If
            End If
        End If
    End If
    
    'si se hab�a hecho un scroll se deja en la posici�n original
    If iScroll > 0 Then
        sdbgVarsQAProve.Scroll iScroll, 0
        sdbgVarsQAProve.Update
    End If
    
    If iScroll2 > 0 Then
        sdbgProveVarsQA.Scroll iScroll2, 0
        sdbgProveVarsQA.Update
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "GuardarVistaGeneral", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>
''' Rellenamos la comparativa general de resumen
''' </summary>
''' <remarks>Llamada desde: cmdExcel_Click; Tiempo m�ximo:0,2</remarks>
Private Sub ObtenerHojaComparativaQA()
Dim xlApp As Object
Dim xlBook As Object
Dim xlSheet As Object
Dim iIni As Integer
Dim i As Integer

Dim sFileName As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm

    If gParametrosInstalacion.gsComparativaQA = "" And gParametrosGenerales.gsComparativaQA <> "" Then
        gParametrosInstalacion.gsComparativaQA = gParametrosGenerales.gsComparativaQA
        g_GuardarParametrosIns = True
    End If

    'Comprobar las plantillas
    sFileName = gParametrosInstalacion.gsComparativaQA

    If m_oFos Is Nothing Then
        Set m_oFos = New Scripting.FileSystemObject
    End If
    m_bSetFocusGrid = False
    If Not m_oFos.FileExists(sFileName) Then
        oMensajes.PlantillaNoEncontrada sFileName
        Exit Sub
    End If
    m_bSetFocusGrid = True

    Screen.MousePointer = vbHourglass

    frmESPERA.lblGeneral.caption = m_sIdiGenComp
    frmESPERA.lblGeneral.Refresh
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.ProgressBar1.Max = 5
    frmESPERA.ProgressBar2.Max = 4
    frmESPERA.Show
    DoEvents

    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = m_sIdiAbrExcel
    frmESPERA.lblDetalle.Refresh

    'Crear aplicaci�n excell
    Set xlApp = CreateObject("Excel.Application")

    iIni = gParametrosInstalacion.giLongCabCompQA

    If iIni <= 0 Then
        iIni = 1
    End If

    Set xlBook = xlApp.Workbooks.Add(sFileName)

    'Vamos a guardar los valores de los atributos en hojas de la excel
    'Atributos de ambito proceso en una
    Set xlSheet = xlBook.Sheets.Add
    xlSheet.Name = "@P@R@O@C@E@"
    xlSheet.Visible = False
    i = 2

    Dim iNumSheets As Integer
    iNumSheets = xlBook.Sheets.Count

    'Nombramos las hojas
    xlBook.Sheets.Item(iNumSheets).Name = m_sIdiGeneral

   'Rellenamos cada hoja de grupo
    frmESPERA.lblContacto.caption = ""
    frmESPERA.ProgressBar2.Value = 0
    frmESPERA.ProgressBar1.Value = 3

    Set xlSheet = xlBook.Sheets(m_sIdiGeneral)
    
    'Rellenamos la comparativa general de resumen
    ComparativaGeneral xlSheet, iIni
    
    frmESPERA.ProgressBar1.Value = 5

    xlApp.Visible = True
    BringWindowToTop xlApp.hWnd
    
    Screen.MousePointer = vbNormal
    Unload frmESPERA
    DoEvents
    Exit Sub

ERROR_Frm:

    Screen.MousePointer = vbNormal
    MsgBox err.Description
    If err.Number = 7 Then
        xlApp.Visible = True
        Unload frmESPERA
        Exit Sub
    End If

    Unload frmESPERA

    xlApp.Quit
    Set xlApp = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "ObtenerHojaComparativaQA", err, Erl, , m_bActivado)
        GoTo ERROR_Frm
        Exit Sub
    End If

End Sub

''' <summary>
''' Rellenamos la comparativa general de resumen
''' </summary>
''' <param name="xlSheet">objeto hoja de excel donde reflejar la comparativa</param>
''' <param name="iIni">donde comienza a escribir</param>
''' <remarks>Llamada desde: ObtenerHojaComparativaQA; Tiempo m�ximo: 0,2</remarks>
Private Sub ComparativaGeneral(xlSheet As Object, iIni As Integer)
Dim sCelda As String
Dim sLetra As String
Dim i As Integer
Dim j As Integer
Dim orange As Object
Dim sCelda2 As String
Dim sDatosVar As String
Dim lBackColor As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With m_oProcesoSeleccionado
        sCelda = "A" & iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = m_sIdiProceso & " " & CStr(.Anyo) & "/" & .GMN1Cod & "/" & CStr(.Cod) & " " & .Den
    End With
        
    Set orange = xlSheet.Range("A" & iIni & ":" & "G" & iIni + 1)
    orange.Font.Bold = True
    orange.borderaround , 2
    orange.Interior.Color = RGB(192, 192, 192)
    
    'Cabecera variables calidad
    sCelda = "A" & 3 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = m_sVariablesCal
    xlSheet.Range(sCelda & ":B" & 3 + iIni).Merge
    xlSheet.Range(sCelda & ":B" & 3 + iIni).Interior.Color = RGB(128, 128, 0)
    Set orange = xlSheet.Range(sCelda & ":B" & 5 + iIni)
    orange.Font.Bold = True
    orange.borderaround , 3
    
    sCelda = "A" & 4 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = m_sIdiCodigo

    xlSheet.Range(sCelda).Interior.Color = RGB(128, 128, 0)
    xlSheet.Range(sCelda).borderaround , 3
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "B" & 4 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = m_sIdiNombre

    xlSheet.Range(sCelda).Interior.Color = RGB(128, 128, 0)
    xlSheet.Range(sCelda).borderaround , 3
    xlSheet.Range(sCelda).Font.Bold = True
    
    'Cabecera unidad de negocio
    sCelda = "C" & 3 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = m_sIdiUnidad
    xlSheet.Range(sCelda & ":D" & 3 + iIni).Merge
    xlSheet.Range(sCelda & ":D" & 3 + iIni).Interior.Color = RGB(128, 128, 0)
    Set orange = xlSheet.Range(sCelda & ":D" & 5 + iIni)
    orange.Font.Bold = True
    orange.borderaround , 3
    
    sCelda = "C" & 4 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = m_sIdiUnidadCN
    xlSheet.Range(sCelda & ":D" & 4 + iIni).Merge
    xlSheet.Range(sCelda & ":D" & 4 + iIni).Interior.Color = RGB(128, 128, 0)
    xlSheet.Range(sCelda & ":D" & 4 + iIni).borderaround , 3
    xlSheet.Range(sCelda).Font.Bold = True
    
    frmESPERA.ProgressBar2 = 1
    
    'Mostrar cabeceras Proveedores
    sLetra = "C"
    j = 0
    For i = 2 To sdbgVarsQAProve.Groups.Count - 1
        j = j + 2
        sCelda = DevolverLetra(sLetra, j) & 3 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sdbgVarsQAProve.Groups(i).caption
        xlSheet.Range(sCelda).WrapText = True
        
        sCelda2 = DevolverLetra(sLetra, j + 1) & 3 + iIni
        xlSheet.Range(sCelda & ":" & sCelda2).Merge
        Set orange = xlSheet.Range(sCelda & ":" & sCelda2)
        orange.Font.Size = 6
        orange.Interior.Color = RGB(192, 192, 192)
        orange.Font.Bold = True
        orange.borderaround , 3
        Set orange = xlSheet.Range(sCelda & ":" & sCelda2)
        orange.Font.Size = 6
        orange.Font.Bold = True
        orange.borderaround , 3
        orange.WrapText = False
        orange.borderaround , 3
        
        sCelda = DevolverLetra(sLetra, j) & 4 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sdbgVarsQAProve.Groups(i).Columns(0).caption
    
        xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
        xlSheet.Range(sCelda).borderaround , 3
        xlSheet.Range(sCelda).Font.Bold = True
        xlSheet.Range(sCelda).Font.Size = 6
        
        sCelda2 = DevolverLetra(sLetra, j + 1) & 4 + iIni
        xlSheet.Range(sCelda2).NumberFormat = "@"
        xlSheet.Range(sCelda2) = sdbgVarsQAProve.Groups(i).Columns(1).caption
    
        xlSheet.Range(sCelda2).Interior.Color = RGB(192, 192, 192)
        xlSheet.Range(sCelda2).borderaround , 3
        xlSheet.Range(sCelda2).Font.Bold = True
        xlSheet.Range(sCelda2).Font.Size = 6
    Next i
    
    frmESPERA.ProgressBar2 = 2
    
    'Datos variables calidad e unidad
    For j = 1 To sdbgVarsQAProve.Rows
        sLetra = "B"
        Select Case j
            Case 1
                sdbgVarsQAProve.MoveFirst
            Case Else
                sdbgVarsQAProve.MoveNext
        End Select
        i = 0
        
        'variables calidad
        sCelda = DevolverLetra(sLetra, i * 2 - 1) & j + 4 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sdbgVarsQAProve.Groups(i).Columns(0).Text
        Set orange = xlSheet.Range(sCelda)
        orange.Font.Bold = False
        orange.Interior.Color = RGB(128, 128, 0)
        orange.borderaround , 2
        
        sCelda2 = DevolverLetra(sLetra, i * 2) & j + 4 + iIni
        xlSheet.Range(sCelda2).NumberFormat = "@"
        xlSheet.Range(sCelda2) = sdbgVarsQAProve.Groups(i).Columns(1).Text
        Set orange = xlSheet.Range(sCelda2)
        orange.Interior.Color = RGB(128, 128, 0)
        orange.Font.Bold = False
        orange.borderaround , 2
        
        'unidad
        sLetra = "D"
        sCelda = DevolverLetra(sLetra, i * 2 - 1) & j + 4 + iIni
        sCelda2 = DevolverLetra(sLetra, i * 2) & j + 4 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sdbgVarsQAProve.Groups(1).Columns(0).Text
        xlSheet.Range(sCelda & ":" & sCelda2).Merge
        Set orange = xlSheet.Range(sCelda & ":" & sCelda2)
        orange.Font.Bold = False
        orange.Interior.Color = RGB(128, 128, 0)
        orange.borderaround , 2
    Next j
    
    frmESPERA.ProgressBar2 = 3
    
    'Datos proveedores
    For j = 1 To sdbgVarsQAProve.Rows
        sLetra = "B"
        Select Case j
            Case 1
                sdbgVarsQAProve.MoveFirst
            Case Else
                sdbgVarsQAProve.MoveNext
        End Select
        sDatosVar = sdbgVarsQAProve.Groups(0).Columns("DATOS_VAR").Text
        For i = 2 To sdbgVarsQAProve.Groups.Count - 1
            If sdbgVarsQAProve.Groups(i).Columns(1).Text <> "" Then
                lBackColor = DevolverColor(sdbgVarsQAProve.Groups(i).Columns(1).Value, sDatosVar)
            Else
                lBackColor = RGB(245, 245, 200)
            End If
            sCelda = DevolverLetra(sLetra, i * 2 - 1) & j + 4 + iIni
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sdbgVarsQAProve.Groups(i).Columns(0).Text
            Set orange = xlSheet.Range(sCelda)
            orange.Font.Bold = False
            orange.Interior.Color = lBackColor
            orange.borderaround , 2

            sCelda2 = DevolverLetra(sLetra, i * 2) & j + 4 + iIni
            xlSheet.Range(sCelda2).NumberFormat = "@"
            If Not IsNumeric(sdbgVarsQAProve.Groups(i).Columns(1).Value) Then
                xlSheet.Range(sCelda2) = sdbgVarsQAProve.Groups(i).Columns(1).Value
            Else
                xlSheet.Range(sCelda2) = FormateoNumerico(sdbgVarsQAProve.Groups(i).Columns(1).Value, FormateoNumericoComp(m_oVistaCalSeleccionada.decimales, False))
            End If
            Set orange = xlSheet.Range(sCelda2)
            orange.Interior.Color = lBackColor
            orange.Font.Bold = True
            orange.borderaround , 2
        Next i
    Next j
    
    frmESPERA.ProgressBar2 = 4
    
    sdbgVarsQAProve.MoveFirst
    
    Screen.MousePointer = vbNormal
    


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "ComparativaGeneral", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Function DevolverLetra(ByVal sCol As String, Optional ByVal iIncr As Integer) As String
Dim sDecena As String
Dim sUnidad As String

Dim idesvA As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Len(sCol) = 1 Then
    idesvA = iIncr + (Asc(sCol) - 65)
Else
    sDecena = Left(sCol, 1)
    sUnidad = Right(sCol, 1)
    idesvA = (Asc(sDecena) - 64) * 26 + Asc(sUnidad) - 65 + iIncr
End If
If idesvA \ 26 > 0 Then
    sDecena = Chr(65 + idesvA \ 26 - 1)
Else
    sDecena = ""
End If

sUnidad = Chr(65 + idesvA Mod 26)

DevolverLetra = sDecena & sUnidad
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "DevolverLetra", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function


''' <summary>
''' Dependiendo de la puntuaci�n de la variable y de si tiene intervalos de aviso/restricci�n, devuelve el color
''' de fondo pra el excel de la comparativa.
''' </summary>
''' <param name="valor">puntuaci�n de la variable</param>
''' <param name="DatosVar">intervalos de aviso/restricci�n (de haberlos)</param>
''' <returns>Color correspondiente</returns>
''' <remarks>Llamada desde: ComparativaGeneral; Tiempo m�ximo: 0</remarks>
Private Function DevolverColor(ByVal valor As Double, ByVal DatosVar As String) As Long
    Dim sDatos_Variable() As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sDatos_Variable = Split(DatosVar, "#")

    If sDatos_Variable(POS_RESTRICCION) <> "" Then
        If sDatos_Variable(POS_RESTRICCION) Then
            If sDatos_Variable(POS_R_INF) <> "" And sDatos_Variable(POS_R_SUP) <> "" Then
                If valor >= CDbl(sDatos_Variable(POS_R_INF)) And valor <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                    DevolverColor = RGB(255, 0, 0)
                    Exit Function
                End If
            Else
                If sDatos_Variable(POS_R_INF) <> "" Then
                    If valor >= CDbl(sDatos_Variable(POS_R_INF)) Then
                        DevolverColor = RGB(255, 0, 0)
                        Exit Function
                    End If
                Else
                    If sDatos_Variable(POS_R_SUP) <> "" Then
                        If valor <= CDbl(sDatos_Variable(POS_R_SUP)) Then
                            DevolverColor = RGB(255, 0, 0)
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If
    End If

    If sDatos_Variable(POS_AVISO) <> "" Then
        If sDatos_Variable(POS_AVISO) Then
            If sDatos_Variable(POS_A_INF) <> "" And sDatos_Variable(POS_A_SUP) <> "" Then
                If valor >= CDbl(sDatos_Variable(POS_A_INF)) And valor <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                    DevolverColor = RGB(255, 128, 0)
                    Exit Function
                End If
            Else
                If sDatos_Variable(POS_A_INF) <> "" Then
                    If valor >= CDbl(sDatos_Variable(POS_A_INF)) Then
                        DevolverColor = RGB(255, 128, 0)
                        Exit Function
                    End If
                Else
                    If sDatos_Variable(POS_A_SUP) <> "" Then
                        If valor <= CDbl(sDatos_Variable(POS_A_SUP)) Then
                            DevolverColor = RGB(255, 128, 0)
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    DevolverColor = RGB(245, 245, 200)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "DevolverColor", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function


''' <summary>
''' Redimensiona los grupos y columnas de los grids a lo q este grabado en bbdd.
''' Se queda un codigo para recuperarse de posibles errores en los anchos, aunque con lo
''' hecho (y explicado) en GuardarVistaGeneral no me ha dado problemas.
''' </summary>
''' <remarks>Llamada desde:chkVerCalificacion_Click     chkVerPuntos_Click      CargarGridVarCalidad
''' ; Tiempo m�ximo:0,1</remarks>
Private Sub RedimensionarGrid()
    Dim i As Integer
    Dim iScroll As Integer
    Dim iScroll2 As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgVarsQAProve.MoveFirst
    sdbgProveVarsQA.MoveFirst
    
    'Si se ha hecho un scroll se vuelve a hacerlo para dejar la grid en la posici�n
    'original porque sino no funciona bien
    
    If sdbgVarsQAProve.GrpPosition(0) > 0 Then
        iScroll = sdbgVarsQAProve.Groups(0).Position
        sdbgVarsQAProve.Scroll -iScroll, 0
        sdbgVarsQAProve.Update
    End If
    
    If sdbgProveVarsQA.GrpPosition(0) > 0 Then
        iScroll2 = sdbgProveVarsQA.Groups(0).Position
        sdbgProveVarsQA.Scroll -iScroll2, 0
        sdbgProveVarsQA.Update
    End If
    
    For i = 0 To sdbgVarsQAProve.Groups.Count - 1
        sdbgVarsQAProve.Groups(i).Position = i
    Next i
    
    For i = 0 To sdbgProveVarsQA.Groups.Count - 1
        sdbgProveVarsQA.Groups(i).Position = i
    Next i
    
    
    'Redimensionamos la grid en orden
    sdbgVarsQAProve.Groups(0).Width = m_oVistaCalSeleccionada.Width1_grupo_var
    sdbgVarsQAProve.Groups(0).Columns("COD").Width = m_oVistaCalSeleccionada.Width1_cod_var
    sdbgVarsQAProve.Groups(0).Columns("DEN").Width = m_oVistaCalSeleccionada.Width1_den_var

    sdbgVarsQAProve.Groups(1).Width = m_oVistaCalSeleccionada.Width3_grupo_unqa
    sdbgVarsQAProve.Groups(1).Columns("UNQA").Width = m_oVistaCalSeleccionada.Width3_grupo_unqa - CTE_WIDTH_3COLBT
    sdbgVarsQAProve.Groups(1).Columns("BTUNQA").Width = sdbgVarsQAProve.Groups(1).Width - sdbgVarsQAProve.Groups(1).Columns("UNQA").Width
    
    For i = 2 To sdbgVarsQAProve.Groups.Count - 1
        sdbgVarsQAProve.Groups(i).Width = m_oVistaCalSeleccionada.Width1_grupo_prove
        
        If m_oVistaCalSeleccionada.Calificaciones = True And m_oVistaCalSeleccionada.Puntos = True Then
            sdbgVarsQAProve.Groups(i).Columns(0).Width = m_oVistaCalSeleccionada.Width1_calificacion
            sdbgVarsQAProve.Groups(i).Columns(1).Width = m_oVistaCalSeleccionada.Width1_puntos
            
            If (m_oVistaCalSeleccionada.Width1_calificacion + m_oVistaCalSeleccionada.Width1_puntos - 2) > m_oVistaCalSeleccionada.Width1_grupo_prove Then
                sdbgVarsQAProve.Groups(i).Width = sdbgVarsQAProve.Groups(i).Columns(0).Width + sdbgVarsQAProve.Groups(i).Columns(1).Width
            ElseIf m_oVistaCalSeleccionada.Width1_calificacion < 0 Then
                m_oVistaCalSeleccionada.Width1_calificacion = 400
                sdbgVarsQAProve.Groups(i).Columns(0).Width = m_oVistaCalSeleccionada.Width1_calificacion
                sdbgVarsQAProve.Groups(i).Columns(1).Width = m_oVistaCalSeleccionada.Width1_puntos
                sdbgVarsQAProve.Groups(i).Width = sdbgVarsQAProve.Groups(i).Columns(0).Width + sdbgVarsQAProve.Groups(i).Columns(1).Width
            ElseIf m_oVistaCalSeleccionada.Width1_puntos < 0 Then
                m_oVistaCalSeleccionada.Width1_puntos = 400
                sdbgVarsQAProve.Groups(i).Columns(1).Width = m_oVistaCalSeleccionada.Width1_puntos
                sdbgVarsQAProve.Groups(i).Width = sdbgVarsQAProve.Groups(i).Columns(0).Width + sdbgVarsQAProve.Groups(i).Columns(1).Width
            End If
        Else
            If m_oVistaCalSeleccionada.Calificaciones = True Then
                sdbgVarsQAProve.Groups(i).Columns(0).Width = m_oVistaCalSeleccionada.Width1_grupo_prove
            End If
            If m_oVistaCalSeleccionada.Puntos = True Then
                sdbgVarsQAProve.Groups(i).Columns(1).Width = m_oVistaCalSeleccionada.Width1_grupo_prove
            End If
        End If
        If m_bConAvisoRestricVarsQAProve Or m_bConAvisoRestricProveVarsQA Then
            If sdbgVarsQAProve.Groups(i).Columns(1).Width > 400 Then
                sdbgVarsQAProve.Groups(i).Columns(1).Width = sdbgVarsQAProve.Groups(i).Columns(1).Width - 300
                sdbgVarsQAProve.Groups(i).Columns(2).Width = 300
            End If
            sdbgVarsQAProve.Groups(i).Columns(2).Visible = True
        Else
            sdbgVarsQAProve.Groups(i).Columns(2).Visible = False
        End If
    Next i
    
    sdbgProveVarsQA.Groups(0).Width = m_oVistaCalSeleccionada.Width2_grupo_prove
    sdbgProveVarsQA.Groups(0).Columns("COD").Width = m_oVistaCalSeleccionada.Width2_cod_prove
    sdbgProveVarsQA.Groups(0).Columns("DEN").Width = m_oVistaCalSeleccionada.Width2_den_prove

    sdbgProveVarsQA.Groups(1).Width = m_oVistaCalSeleccionada.Width3_grupo_unqa
    sdbgProveVarsQA.Groups(1).Columns("UNQA").Width = m_oVistaCalSeleccionada.Width3_grupo_unqa - CTE_WIDTH_3COLBT
    sdbgProveVarsQA.Groups(1).Columns("BTUNQA").Width = sdbgProveVarsQA.Groups(1).Width - sdbgProveVarsQA.Groups(1).Columns("UNQA").Width

    For i = 2 To sdbgProveVarsQA.Groups.Count - 1
        sdbgProveVarsQA.Groups(i).Width = m_oVistaCalSeleccionada.Width2_grupo_var
        
        If m_oVistaCalSeleccionada.Calificaciones = True And m_oVistaCalSeleccionada.Puntos = True Then
            sdbgProveVarsQA.Groups(i).Columns(0).Width = m_oVistaCalSeleccionada.Width2_calificacion
            sdbgProveVarsQA.Groups(i).Columns(1).Width = m_oVistaCalSeleccionada.Width2_puntos
            
            If (m_oVistaCalSeleccionada.Width2_calificacion + m_oVistaCalSeleccionada.Width2_puntos - 2) > m_oVistaCalSeleccionada.Width2_grupo_var Then
                sdbgProveVarsQA.Groups(i).Width = sdbgProveVarsQA.Groups(i).Columns(0).Width + sdbgProveVarsQA.Groups(i).Columns(1).Width
            ElseIf m_oVistaCalSeleccionada.Width2_calificacion < 0 Then
                m_oVistaCalSeleccionada.Width2_calificacion = 400
                sdbgProveVarsQA.Groups(i).Columns(0).Width = m_oVistaCalSeleccionada.Width2_calificacion
                sdbgProveVarsQA.Groups(i).Columns(1).Width = m_oVistaCalSeleccionada.Width2_puntos
                sdbgProveVarsQA.Groups(i).Width = sdbgProveVarsQA.Groups(i).Columns(0).Width + sdbgProveVarsQA.Groups(i).Columns(1).Width
            ElseIf m_oVistaCalSeleccionada.Width2_puntos < 0 Then
                m_oVistaCalSeleccionada.Width2_puntos = 400
                sdbgProveVarsQA.Groups(i).Columns(1).Width = m_oVistaCalSeleccionada.Width2_puntos
                sdbgProveVarsQA.Groups(i).Width = sdbgProveVarsQA.Groups(i).Columns(0).Width + sdbgProveVarsQA.Groups(i).Columns(1).Width
            End If
        Else
            If m_oVistaCalSeleccionada.Calificaciones = True Then
                sdbgProveVarsQA.Groups(i).Columns(0).Width = m_oVistaCalSeleccionada.Width2_grupo_var
            End If
            If m_oVistaCalSeleccionada.Puntos = True Then
                sdbgProveVarsQA.Groups(i).Columns(1).Width = m_oVistaCalSeleccionada.Width2_grupo_var
            End If
        End If
        If m_bConAvisoRestricProveVarsQA Or m_bConAvisoRestricVarsQAProve Then
            If sdbgProveVarsQA.Groups(i).Columns(1).Width > 400 Then
                sdbgProveVarsQA.Groups(i).Columns(1).Width = sdbgProveVarsQA.Groups(i).Columns(1).Width - 300
                sdbgProveVarsQA.Groups(i).Columns(3).Width = 300
            End If
            sdbgProveVarsQA.Groups(i).Columns(3).Visible = True
        Else
            sdbgProveVarsQA.Groups(i).Columns(3).Visible = False
        End If
        
    Next i
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "RedimensionarGrid", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Tras un cambio de vista, establece el texto en el combo de vistas, establece si ve puntos y calificaciones y
''' habilita/deshabilita el boton de vista por defecto en funci�n si es la vista por defecto o no
''' </summary>
''' <remarks>Llamada desde: cmdEliminarVista_Click      cmdGuardarVistaNueva_Click  Form_Load; Tiempo m�ximo:0</remarks>
Private Sub MostrarVistaEnCombos()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case m_oVistaCalSeleccionada.TipoVista
        Case TipoDeVistaqa.Vistainicialqa
            sdbcVistaActual.Text = m_sIdiInicial
        Case TipoDeVistaqa.otravistaqa
            sdbcVistaActual.Text = m_sVistaGlobal & " " & m_oVistaCalSeleccionada.nombre
    End Select
    sdbcVistaActual.Columns("COD").Value = CStr(m_oVistaCalSeleccionada.Vista)
    sdbcVistaActual.Columns("TIPO").Value = CStr(m_oVistaCalSeleccionada.TipoVista)
    
    m_bDeshabilidar_Chk_ChangeTodas = True
    If Not m_bBtDefectoCanDisabled Then
        Me.cmdVistaDefecto.Enabled = True
    ElseIf CInt(Me.sdbcVistaActual.Columns("COD").Value) = CInt(m_oProcesoSeleccionado.VistaDefectoQA) Then
        Me.cmdVistaDefecto.Enabled = False
    Else
        Me.cmdVistaDefecto.Enabled = True
    End If
    
    Me.chkVerCalificacion.Value = IIf(m_oVistaCalSeleccionada.Calificaciones, vbChecked, vbUnchecked)
    Me.chkVerPuntos.Value = IIf(m_oVistaCalSeleccionada.Puntos, vbChecked, vbUnchecked)
    
    m_bDeshabilidar_Chk_ChangeTodas = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "MostrarVistaEnCombos", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


''' <summary>
''' Si el usuario tiene permisos para crear y modificar vistas de comparativa de calidad y dependiendo del tipo de
''' vista estaran visibles unos botones u otros (botones relativos a vista, renombrar,guardar, ...). Si no
''' tiene permiso ningun bot�n sera visible.
''' El boton para marcar q es la vista por defecto esta siempre visible.
''' </summary>
''' <param name="udtTipoVista">Tipo de la vista</param>
''' <remarks>Llamada desde: cmdEliminarVista_Click      cmdGuardarVistaNueva_Click  Form_Load   Form_Resize
''' sdbcVistaActual_CloseUp; Tiempo m�ximo:0</remarks>
Private Sub ConfigurarBotonesVistas(ByVal udtTipoVista As TipoDeVistaqa)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.cmdExcel.Left = Me.frameVista.Width - Me.cmdExcel.Width - 180
    Me.cmdInvertir.Left = Me.cmdExcel.Left - Me.cmdInvertir.Width - 30
    Me.cmdVistaDefecto.Left = Me.cmdInvertir.Left - Me.cmdVistaDefecto.Width - 30

    If m_bPermitirVistasCompQA Then 'Si el usuario tiene permisos para crear y modificar vistas de comparativa de calidad
    
        If udtTipoVista = TipoDeVistaqa.otravistaqa Then
    
            cmdGuardarVista.Visible = True
            cmdGuardarVistaNueva.Visible = True
            cmdEliminarVista.Visible = True
            cmdRenombrarVista.Visible = True
     
            cmdEliminarVista.Left = Me.cmdVistaDefecto.Left - Me.cmdEliminarVista.Width - 30
            cmdRenombrarVista.Left = cmdEliminarVista.Left - cmdRenombrarVista.Width - 30
            cmdGuardarVista.Left = Me.cmdRenombrarVista.Left - Me.cmdGuardarVista.Width - 30
            cmdGuardarVistaNueva.Left = cmdGuardarVista.Left - cmdGuardarVistaNueva.Width - 30
    
        ElseIf udtTipoVista = TipoDeVistaqa.Vistainicialqa Then
    
            cmdGuardarVista.Visible = False
            cmdGuardarVistaNueva.Visible = True
            cmdEliminarVista.Visible = False
            cmdRenombrarVista.Visible = False
     
            cmdGuardarVistaNueva.Left = Me.cmdVistaDefecto.Left - Me.cmdGuardarVistaNueva.Width - 30
    
        End If
          
    Else
                
        cmdGuardarVista.Visible = False
        cmdGuardarVistaNueva.Visible = False
        cmdEliminarVista.Visible = False
        cmdRenombrarVista.Visible = False
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "ConfigurarBotonesVistas", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


''' <summary>
''' Guarda la vista seleccionada
''' </summary>
''' <remarks>Llamada desde: cmdGuardarVista_Click       ; Tiempo m�ximo: 0,4</remarks>
Private Sub GuardarVistas()
    Dim bGuardarGeneral As Boolean
    Dim bRespuesta As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProcesoSeleccionado Is Nothing Then
        Exit Sub
    End If

    If Not m_oVistaCalSeleccionada Is Nothing Then
        bRespuesta = True
        If m_oVistaCalSeleccionada.HayCambios = True Or m_bCambios Then
            bGuardarGeneral = True
        End If
    End If

    If bGuardarGeneral Then
        GuardarVistaGeneral
        GuardarVistaGeneralEnBd
        m_oVistaCalSeleccionada.HayCambios = False
        m_bCambios = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "GuardarVistas", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>
''' Quitar de la colecci�n de vistas una vista concreta
''' </summary>
''' <param name="sIndice">ID de la vista</param>
''' <remarks>Llamada desde: GuardarVistas   cmdEliminarVista_Click      cmdGuardarVistaNueva_Click; Tiempo m�ximo:0</remarks>
Private Sub EliminarVistaDeColeccionQA(ByVal sIndice As String)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_oConfVistasCalGlobal.Item(sIndice) Is Nothing Then
        m_oConfVistasCalGlobal.Remove (sIndice)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "EliminarVistaDeColeccionQA", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

''' <summary>
''' En el combo de vistas el nombre de las vistas (salvo vista inicial) van precedidos por "Vista usuario:"
''' Esta funci�n saca solo el Nombre de la vista seleccionada sin nada por delante
''' </summary>
''' <param name="sNombre">Nombre de la vista seleccionada en el combo de vistas</param>
''' <returns>el Nombre de la vista seleccionada</returns>
''' <remarks>Llamada desde:cmdVistaDefecto_Click; Tiempo m�ximo:0</remarks>
Private Function DevolverNombreVista(ByVal sNombre As String) As String
    Dim sResultado As String
    Dim iCont As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iCont = 0
    sResultado = ""
    
    If sNombre = "" Then
        DevolverNombreVista = ""
        Exit Function
    Else
    
    If sNombre = m_sIdiInicial Then
       DevolverNombreVista = m_sIdiInicial
       Exit Function
    End If

    iCont = InStr(1, sNombre, ": ")
    
    sResultado = Mid(sNombre, iCont + 2, Len(sNombre) - iCont)
    
    DevolverNombreVista = Trim(sResultado)
    
 End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "DevolverNombreVista", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

''' <summary>
''' Tras eliminar una vista se carga y aplica la vista por defecto y de no haberla la vista por defecto del
''' responsable y de no haberla la vista inicial
''' </summary>
''' <param name="iVistaVieja">ID de la vista eliminada</param>
''' <param name="udtTipoVistaVieja">Tipo de la vista eliminada</param>
''' <remarks>Llamada desde: cmdEliminarVista_Click; Tiempo m�ximo: 0,4</remarks>
Private Sub VistaDespuesDeEliminar(ByVal iVistaVieja As Integer, ByVal udtTipoVistaVieja As TipoDeVistaqa)
    Dim oVistaCal As CConfVistaCalGlobal
    Dim bVista As Boolean
    Dim udtTipoVista As TipoDeVistaqa

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CStr(m_oProcesoSeleccionado.VistaDefectoQA & m_oProcesoSeleccionado.VistaDefectoTipoQA) <> CStr(iVistaVieja & udtTipoVistaVieja) Then
        ConfiguracionVistaActualQA m_oProcesoSeleccionado.VistaDefectoQA, m_oProcesoSeleccionado.VistaDefectoTipoQA
        udtTipoVista = m_oProcesoSeleccionado.VistaDefectoTipoQA
    Else
        Set m_oVistasCombo = Nothing
        Set m_oVistasCombo = oFSGSRaiz.Generar_CConfVistasCalGlobal
        If gParametrosGenerales.gbPymes Then
            m_oVistasCombo.CargarCombosVistas basOptimizacion.gPYMEUsuario
        Else
            m_oVistasCombo.CargarCombosVistas
        End If
        bVista = False
        For Each oVistaCal In m_oVistasCombo
            If oVistaCal.TipoVista <> TipoDeVistaqa.Vistainicialqa Then
                ConfiguracionVistaActualQA oVistaCal.Vista, oVistaCal.TipoVista
                udtTipoVista = oVistaCal.TipoVista
                bVista = True
                Exit For
            End If
        Next
        If bVista = False Then
            ConfiguracionVistaActualQA 0, TipoDeVistaqa.Vistainicialqa
            udtTipoVista = TipoDeVistaqa.Vistainicialqa
        End If
    End If

    CargarGridVarCalidad
    If udtTipoVista = Vistainicialqa Then
        Me.caption = m_sCaption & " (" & m_sIdiInicial & ")"
    Else
        Me.caption = m_sCaption & " (" & m_sVistaGlobal & " " & m_oVistaCalSeleccionada.nombre & ")"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "VistaDespuesDeEliminar", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Determina si la variable dada cumple con su intervalo de aviso/restricci�n configurado (de haberlo)
''' </summary>
''' <param name="Puntos">Puntos de la variable</param>
''' <param name="VarCal">Objeto variable</param>
''' <returns>si cumple o no</returns>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve_Nivel0     Rellenar_sdbgVarsQAProve_Nivel1
'''     Rellenar_sdbgVarsQAProve_Nivel2     Rellenar_sdbgVarsQAProve_Nivel3     Rellenar_sdbgVarsQAProve_Nivel4
'''     Rellenar_sdbgVarsQAProve_Nivel5; Tiempo m�ximo:0</remarks>
Private Function AlgunProveConAvisoORestr(ByVal Puntos As Double, ByVal VarCal As CVariableCalidad) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With VarCal
        If .AvisoSiNo Then
            If .AvisoInf <> "" And .AvisoSup <> "" Then
                If Puntos >= CDbl(.AvisoInf) And Puntos <= CDbl(.AvisoSup) Then AlgunProveConAvisoORestr = True
            Else
                If .AvisoInf <> "" Then
                    If Puntos >= CDbl(.AvisoInf) Then AlgunProveConAvisoORestr = True
                ElseIf .AvisoSup <> "" Then
                    If Puntos <= CDbl(.AvisoSup) Then AlgunProveConAvisoORestr = True
                End If
            End If
        End If
        
        If .RestriccionSiNo Then
            If .RestriccionInf <> "" And .RestriccionSup <> "" Then
                If Puntos >= CDbl(.RestriccionInf) And Puntos <= CDbl(.RestriccionSup) Then AlgunProveConAvisoORestr = True
            Else
                If .RestriccionInf <> "" Then
                    If Puntos >= CDbl(.RestriccionInf) Then AlgunProveConAvisoORestr = True
                ElseIf .RestriccionSup <> "" Then
                    If Puntos <= CDbl(.RestriccionSup) Then AlgunProveConAvisoORestr = True
                End If
            End If
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "AlgunProveConAvisoORestr", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Hay una diferencia de tiempos de respuesta entre desarrollo y el servidor grande. Eso hace q salten los objeto_MouseMove en el servidor
''' con cierta?? facilidad por ello se puso un booleano m_bDeshabilidar_MouseMove q ha resultado insuficiente. Por ello se mete una espera
''' de 4 decimas de segundo para q en servidor de tiempo a mostrar y mover, por programaci�n, el rat�n. Hay q mover el rat�n a dentro del
''' pic o los objeto_MouseMove te ocultaran los pic.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub Timer1_Timer()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Timer1.Enabled = False
    m_bDeshabilidar_MouseMove = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Timer1_Timer", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Si hacer check en un proveedor implica ocultarlo y ya no habr�a ningun proveedor visible
''' este check no debe tener efecto, en todo momento debe haber al menos uno visible.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub tvwProves_Click()
    Dim ProvesVisibles As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ProvesVisibles = DameNumeroProvesVisibles

    If ProvesVisibles = 0 Then
        Me.tvwProves.selectedItem.Checked = True
        m_oProvesAsig.Item(Me.tvwProves.selectedItem.Tag).VisibleEnGrafico = True
        
        Me.lblAlmenosUno.Visible = True
    End If
    
    If Me.Visible Then Me.picProves.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwProves_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Al recibir el foco el arbol se le da el foco al pic
''' </summary>
''' <remarks>Llamada desde:Evento que salta al recibir el foco el arbol; Tiempo m�ximo:0</remarks>
Private Sub tvwProves_GotFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Visible Then Me.picProves.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwProves_GotFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Cuando se chequea/deschequea un proveedor, se indica q el proveedor debe mostrarse/ocultarse en los grids
''' Si hacer check en un proveedor implica ocultarlo y ya no habr�a ningun proveedor visible
''' este check no debe tener efecto, en todo momento debe haber al menos uno visible.
''' </summary>
''' <param name="node">nodo cliqueado</param>
''' <remarks>Llamada desde:Evento que salta al hacer click en el check de un nodo del arbol; Tiempo m�ximo: 0</remarks>
Private Sub tvwProves_NodeCheck(ByVal node As MSComctlLib.node)
    Dim ProvesVisibles As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ProvesVisibles = DameNumeroProvesVisibles

    If ProvesVisibles = 0 Then
        Me.tvwProves.selectedItem = node
        'Con tvwProves_Click recuparamos ese visible deshaciendo el nodecheck
        'No funciona hacer node.checked = true. Solo sirve para ver un instante el nodo chequeado y q se deschequea otra
        'vez. Cositas del VB6??
        If Me.lblAlmenosUno.Visible = False Then
            tvwProves.Height = picProves.Height - 1320 - Me.lblAlmenosUno.Height - 30
            Me.lblAlmenosUno.Visible = True
        End If
        Exit Sub
    Else
        If Me.lblAlmenosUno.Visible = True Then
            tvwProves.Height = picProves.Height - 1320
        End If
        
        Me.lblAlmenosUno.Visible = False
    End If
    
    m_bVistaCambiaProve = True
    
    m_oProvesAsig.Item(node.Tag).VisibleEnGrafico = node.Checked
    
    If Not node.Checked Then
        m_bDeshabilidar_Chk_ChangeTodas = True
        chkTodosProves.Value = 0
        m_bDeshabilidar_Chk_ChangeTodas = False
    End If
    
    Me.tvwProves.selectedItem = node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwProves_NodeCheck", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Cuando se cliquee en el texto de un nodo debe chequearse/deshequearse el nodo
''' Si hacer check en un proveedor implica ocultarlo y ya no habr�a ningun proveedor visible
''' este check no debe tener efecto, en todo momento debe haber al menos uno visible.
''' </summary>
''' <param name="node">nodo cliqueado</param>
''' <remarks>Llamada desde:Evento que salta al hacer click en un nodo del arbol ; Tiempo m�ximo: 0</remarks>
Private Sub tvwProves_NodeClick(ByVal node As MSComctlLib.node)
    Dim ProvesVisibles As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    node.Checked = Not node.Checked
        
    ProvesVisibles = DameNumeroProvesVisibles
    
    If ProvesVisibles = 0 Then
        node.Checked = True
        If Me.lblAlmenosUno.Visible = False Then
            tvwProves.Height = picProves.Height - 1320 - Me.lblAlmenosUno.Height - 30
            Me.lblAlmenosUno.Visible = True
        End If
        Exit Sub
    ElseIf ProvesVisibles = 1 Then
        If Me.lblAlmenosUno.Visible = False Then
            tvwProves.Height = picProves.Height - 1320 - Me.lblAlmenosUno.Height - 30
            Me.lblAlmenosUno.Visible = True
        End If
    Else
        If Me.lblAlmenosUno.Visible = True Then
            tvwProves.Height = picProves.Height - 1320
        End If
        
        Me.lblAlmenosUno.Visible = False
    End If
        
    tvwProves_NodeCheck node
    
    If Not node.Checked Then
        m_bDeshabilidar_Chk_ChangeTodas = True
        chkTodosProves.Value = 0
        m_bDeshabilidar_Chk_ChangeTodas = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwProves_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Obtener Numero de Proves Visibles
''' </summary>
''' <returns>Numero de Proves Visibles</returns>
''' <remarks>Llamada desde:sdbgProveVarsQA_GrpHeadClick  sdbgVarsQAProve_GrpHeadClick; Tiempo m�ximo: 0</remarks>
Private Function DameNumeroProvesVisibles() As Integer
    Dim nodx As MSComctlLib.node
    Dim ProvesVisibles As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ProvesVisibles = 0
    
    For Each nodx In Me.tvwProves.Nodes
        If nodx.Checked Then ProvesVisibles = ProvesVisibles + 1
    Next
    
    DameNumeroProvesVisibles = ProvesVisibles
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "DameNumeroProvesVisibles", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Function
    End If
End Function


''' <summary>
''' Al recibir el foco el arbol se le da el foco al pic
''' </summary>
''' <remarks>Llamada desde:Evento que salta al recibir el foco el arbol; Tiempo m�ximo:0</remarks>
Private Sub tvwUndNegUsu_GotFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Visible Then Me.picUnqa.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwUndNegUsu_GotFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Al recibir el foco el arbol se le da el foco al pic
''' </summary>
''' <remarks>Llamada desde:Evento que salta al recibir el foco el arbol; Tiempo m�ximo:0</remarks>
Private Sub tvwUndNegUsu_GridPv_GotFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Visible Then Me.picUnqa_GridPv.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwUndNegUsu_GridPv_GotFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
''' <summary>
''' - Cuando se chequea/deschequea una unidad, se indica q la unidad debe mostrarse/ocultarse en el grid ProveVars
''' - Se diferencia entre grids pq en grid ProveVars no se muestran todas las unidades visibles en la vista. Me explico
''' las invisibles en vista no se ven obviamente y de entre las visibles en vista solo se veran las q tengan
''' algun sentido para el proveedor, vamos las q tenga relacionadas (tabla prove_unqa)
''' - Ahora bien lo q ves no debe ser estatico, debes poder mostrar/ocultar para un proveedor (vista es para todos)
''' esto es lo q hace el arbol tvwUndNegUsu_GridPv esta funcion es para llevar una colecci�n de visibilidades aparte
''' de la vista a traves de los check del arbol.
''' </summary>
''' <param name="node">nodo cliqueado</param>
''' <remarks>Llamada desde:Evento que salta al hacer click en el check de un nodo del arbol; Tiempo m�ximo: 0</remarks>
Private Sub tvwUndNegUsu_GridPv_NodeCheck(ByVal node As MSComctlLib.node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bVistaCambiaUnqas_GridPv = True
    
    If node.Checked Then
        m_oOcultarUnqaProve_GridPv.Remove node.Tag
        If m_oMostrarUnqaProve_GridPv.Item(node.Tag) Is Nothing Then m_oMostrarUnqaProve_GridPv.Add node.Tag, ""
    Else
        If m_oOcultarUnqaProve_GridPv.Item(node.Tag) Is Nothing Then m_oOcultarUnqaProve_GridPv.Add node.Tag, ""
        m_oMostrarUnqaProve_GridPv.Remove node.Tag
    End If
    
    Me.tvwUndNegUsu_GridPv.selectedItem = node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwUndNegUsu_GridPv_NodeCheck", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Cuando se cliquee en el texto de un nodo debe chequearse/deschequearse el nodo
''' </summary>
''' <param name="node">nodo cliqueado</param>
''' <remarks>Llamada desde:Evento que salta al hacer click en un nodo del arbol ; Tiempo m�ximo: 0</remarks>
Private Sub tvwUndNegUsu_GridPv_NodeClick(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    node.Checked = Not node.Checked
    
    tvwUndNegUsu_GridPv_NodeCheck node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwUndNegUsu_GridPv_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


''' <summary>
''' Cuando se chequea/deschequea una unidad, se indica q la unidad debe mostrarse/ocultarse en los grids.
''' Siempre en ambos grids pq este maneja visibilidades de vista (lo recuerdo por el rollo q hay con
''' colecciones "desligadas de la vista" de visibilidades)
''' </summary>
''' <param name="node">nodo cliqueado</param>
''' <remarks>Llamada desde:Evento que salta al hacer click en el check de un nodo del arbol; Tiempo m�ximo: 0</remarks>
Private Sub tvwUndNegUsu_NodeCheck(ByVal node As MSComctlLib.node)
    Dim ConfVistas0 As CConfVistaCalGlobalVar
    Dim ConfVistas1 As CConfVistaCalGlobalVar
    Dim ConfVistas2 As CConfVistaCalGlobalVar
    Dim ConfVistas3 As CConfVistaCalGlobalVar
    Dim ConfVistas4 As CConfVistaCalGlobalVar
    Dim ConfVistas5 As CConfVistaCalGlobalVar
    
    Dim sTag As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bVistaCambiaUnqas = True
    
    m_oVistaCalSeleccionada.HayCambios = True
    
    If InStr(node.Tag, "#") > 0 Then 'Estamos con vista de una vars, todos proves y unidades de la linea seleccionada
        ModificarUnqaVista node.Tag, node.Checked
        
        m_bGrpGenerarEstrucUnQaCambio = True
        
        m_bGrpGenerarEstrucUnQa1ra = False
    Else 'Estamos con vista de todas vars, todos proves y todas unidades.
        If (Not m_bGrpGenerarEstrucUnQa1ra Or m_bGrpGenerarEstrucUnQaCambio) And Not m_bGrpGenerarEstrucUnQaLimpiado Then
            'Primera vez q configuras visibilidades de unidades a nivel de toda vista, los checks aparecen deschequeados
            'lo q no refleja lo q se ve en pantalla y por lo tanto las colecciones q se van llevando. Esto inicializa estas
            'colecciones, en este caso necesario pq has decidido cambiar vista a nivel global.
            'As� pues si entras y no haces nada pues lo q esta en pantalla esta en las colecciones. Y si entras y haces algo
            'lo q habra en pantalla estara en las colecciones.
            m_bGrpGenerarEstrucUnQaLimpiado = True
            LimpiaUnqaVista
        End If
        
        For Each ConfVistas0 In m_oVistaCalSeleccionada.ConfVistasCalGlobalVar
            sTag = ConfVistas0.VarCal & "#####0#######" & node.Tag
            
            ModificarUnqaVista sTag, node.Checked
            
            For Each ConfVistas1 In ConfVistas0.VariblesCalVista
                sTag = ConfVistas1.VarCal & "#####1#######" & node.Tag
                
                ModificarUnqaVista sTag, node.Checked
                
                If Not ConfVistas1.VariblesCalVista Is Nothing Then
                    For Each ConfVistas2 In ConfVistas1.VariblesCalVista
                        sTag = ConfVistas2.VarCal & "#" & ConfVistas1.VarCal & "####2#######" & node.Tag
                        
                        ModificarUnqaVista sTag, node.Checked
                                    
                        If Not ConfVistas2.VariblesCalVista Is Nothing Then
                            For Each ConfVistas3 In ConfVistas2.VariblesCalVista
                                sTag = ConfVistas3.VarCal & "#" & ConfVistas1.VarCal & "#" & ConfVistas2.VarCal & "###3#######" & node.Tag
                                
                                ModificarUnqaVista sTag, node.Checked
                                            
                                If Not ConfVistas3.VariblesCalVista Is Nothing Then
                                    For Each ConfVistas4 In ConfVistas3.VariblesCalVista
                                        sTag = ConfVistas4.VarCal & "#" & ConfVistas1.VarCal & "#" & ConfVistas2.VarCal & "#" & ConfVistas3.VarCal & "##4#######" & node.Tag
                                        
                                        ModificarUnqaVista sTag, node.Checked
                                                    
                                        If Not ConfVistas4.VariblesCalVista Is Nothing Then
                                            For Each ConfVistas5 In ConfVistas4.VariblesCalVista
                                                sTag = ConfVistas5.VarCal & "#" & ConfVistas1.VarCal & "#" & ConfVistas2.VarCal & "#" & ConfVistas3.VarCal & "#" & ConfVistas4.VarCal & "#5#######" & node.Tag
                                                
                                                ModificarUnqaVista sTag, node.Checked
                                                
                                            Next
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Next
        Next
    End If
    
    If m_bGrpGenerarEstrucUnQaPulsado Then
        'Para marcar q ya no hay q inicializar nada q en posteriores configuraciones a nivel global lo q ves en
        'pantalla y colecciones es lo mismo y has de respetarlo
        If m_bGrpGenerarEstrucUnQa1ra = False Then m_bGrpGenerarEstrucUnQa1ra = True
    End If
    
    Me.tvwUndNegUsu.selectedItem = node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwUndNegUsu_NodeCheck", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Modificar en la vista para la variable e unidad contenidas en parametro key, la visibilidad de unidades
''' </summary>
''' <param name="key">la variable e unidad en la q cambia la visibilidad de unidades</param>
''' <param name="valor">cambia a visible(true) u oculta(false)</param>
''' <param name="Limpia">Obligar a q la visibilidad de unidades sea "nivel de grupo"</param>
''' <remarks>Llamada desde: LimpiaUnqaVista     tvwUndNegUsu_NodeCheck; Tiempo m�ximo: 0,1</remarks>
Private Sub ModificarUnqaVista(ByVal key As String, ByVal valor As Boolean, Optional ByVal Limpia As Boolean = False)
    Dim sDatos_Variable() As String
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sDatos_Variable = Split(key, "#")
    
    Select Case sDatos_Variable(POS_NIVEL)
    Case 0
    
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & sDatos_Variable(POS_ID)
        If Limpia Then
            m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas = ","
        Else
            If valor Then
                'Pq el click en grupo no establece los check del arbol a lo mismo q la selecci�n de unqas de las varcal
                If InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas, "," & sDatos_Variable(POS_UNQA) & ",") = 0 Then
                    m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas & sDatos_Variable(POS_UNQA) & ","
                End If
            Else
                m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas = Replace(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas, "," & sDatos_Variable(POS_UNQA) & ",", ",")
            End If
        End If
    Case 1
        
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID)
        If Limpia Then
            m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas = ","
        Else
            If valor Then
                If InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas, "," & sDatos_Variable(POS_UNQA) & ",") = 0 Then
                    m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas & sDatos_Variable(POS_UNQA) & ","
                End If
            Else
                m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas = Replace(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas, "," & sDatos_Variable(POS_UNQA) & ",", ",")
            End If
        End If
    Case 2
            
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID)
        If Limpia Then
            m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas = ","
        Else
            If valor Then
                If InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas, "," & sDatos_Variable(POS_UNQA) & ",") = 0 Then
                    m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas & sDatos_Variable(POS_UNQA) & ","
                End If
            Else
                m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas = Replace(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas, "," & sDatos_Variable(POS_UNQA) & ",", ",")
            End If
        End If
    Case 3
        
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID)
        If Limpia Then
            m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas = ","
        Else
            If valor Then
                If InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas, "," & sDatos_Variable(POS_UNQA) & ",") = 0 Then
                    m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas & sDatos_Variable(POS_UNQA) & ","
                End If
            Else
                m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas = Replace(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas, "," & sDatos_Variable(POS_UNQA) & ",", ",")
            End If
        End If
    Case 4
                
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID_VARCAL3)
        scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & sDatos_Variable(POS_ID)
        If Limpia Then
            m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas = ","
        Else
            If valor Then
                If InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas, "," & sDatos_Variable(POS_UNQA) & ",") = 0 Then
                    m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas & sDatos_Variable(POS_UNQA) & ","
                End If
            Else
                m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas = Replace(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas, "," & sDatos_Variable(POS_UNQA) & ",", ",")
            End If
        End If
    Case 5
                
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID_VARCAL3)
        scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & sDatos_Variable(POS_ID_VARCAL4)
        scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & sDatos_Variable(POS_ID)
        If Limpia Then
            m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas = ","
        Else
            If valor Then
                If InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas, "," & sDatos_Variable(POS_UNQA) & ",") = 0 Then
                    m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas & sDatos_Variable(POS_UNQA) & ","
                End If
            Else
                m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas = Replace(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas, "," & sDatos_Variable(POS_UNQA) & ",", ",")
            End If
        End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "ModificarUnqaVista", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Llamada para la unidad seleccionada. Crea un arbol con la unidad seleccionada y las unidades de negocio
''' hijas de la selccionada q sean accesibles (si ninguna variable en toda la comparativa tiene puntos en
''' una unidad, dicha unidad no es accesible).
''' En el caso de grid VarsProve esta claro q en la linea tengo seleccionada una variable y una unidad y eso
''' implica un unico registro de CONF_VISTAS_CAL_GLOBAL_VAR. No as� en grid ProveVars, N variables 1 unidad
''' ,por eso ProveVars tiene otro arbol para la unidad seleccionada (lo q he ido llamando colecciones
''' "desligadas de la vista" de visibilidades de unidades).
''' </summary>
''' <param name="Unqa">ID de unidad de negocio</param>
''' <param name="Nivel">Nivel de unidad de negocio</param>
''' <param name="VC1">Id del padre de la variable seleccionada a nivel 1</param>
''' <param name="VC2">Id del padre de la variable seleccionada a nivel 2</param>
''' <param name="VC3">Id del padre de la variable seleccionada a nivel 3</param>
''' <param name="VC4">Id del padre de la variable seleccionada a nivel 4</param>
''' <remarks>Llamada desde: sdbgVarsQAProve_RowColChange; Tiempo m�ximo: 0,1</remarks>
Private Sub CellGenerarEstructuraUnidadesNegocio(ByVal Unqa As String, ByVal Nivel As String, ByVal VC1 As Long, ByVal VC2 As Long, ByVal VC3 As Long, ByVal VC4 As Long)
    Dim oNegocio As CUnidadNegQA
    Dim oNegocio2 As CUnidadNegQA
    Dim oNegocio3 As CUnidadNegQA
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    
    Dim sTag As String
    
    Dim nodx As MSComctlLib.node
    Dim bNiv0 As Boolean
    
    Dim bTodosCheck As Boolean
    Dim bAlgunaUnqa As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Me.tvwUndNegUsu.Visible = True
        
    tvwUndNegUsu.Nodes.clear
    
    Me.chkTodasUnidades.Value = False
   
    sTag = m_iIDRest & "#" & VC1 & "#" & VC2 & "#" & VC3 & "#" & VC4 & "#" & m_iNivelRest & "#######"
    
    bNiv0 = False
    
    bTodosCheck = True
    
    For Each oNegocio In m_oUnidadesQa.Unidades
        If Not bNiv0 Then
            bNiv0 = True
        Else
            scod1 = CStr(oNegocio.Id)
            
            bAlgunaUnqa = HayUnqaVarcalAlgunProve(scod1, m_iIDRest, m_iNivelRest, False)
            
            If ((Nivel = 0) Or ((Nivel = 1) And (scod1 = Unqa))) And bAlgunaUnqa Then
                Set nodx = tvwUndNegUsu.Nodes.Add(, , "UN1" & scod1, CStr(oNegocio.Cod) & " - " & oNegocio.Den)
                nodx.Tag = sTag & scod1
                nodx.EnsureVisible
                nodx.Expanded = True
                
                nodx.Checked = UnqaEstaEnVista(oNegocio.Id, m_iNivelRest, m_iIDRest, VC1, VC2, VC3, VC4)
                
                If nodx.Checked = False Then bTodosCheck = False
            End If
            
            If ((Nivel = 0) _
            Or ((Nivel = 1) And (scod1 = Unqa)) _
            Or (Nivel = 2) _
            Or (Nivel = 3)) And bAlgunaUnqa Then
                For Each oNegocio2 In oNegocio.UnidadesNegQA.Unidades
                    scod2 = CStr(oNegocio2.Id)
                    
                    bAlgunaUnqa = HayUnqaVarcalAlgunProve(scod2, m_iIDRest, m_iNivelRest, False)
                    
                    If ((Nivel = 0) _
                    Or ((Nivel = 1) And (scod1 = Unqa)) _
                    Or ((Nivel = 2) And (scod2 = Unqa)) _
                    Or (Nivel = 3)) And bAlgunaUnqa Then
                        If Nivel = 3 Then
                        Else
                            If Nivel = 2 And (scod2 = Unqa) Then
                                Set nodx = tvwUndNegUsu.Nodes.Add(, , "UN2" & scod2, CStr(oNegocio2.Cod) & " - " & oNegocio2.Den)
                            ElseIf (Nivel = 0) Or (Nivel = 1) Then
                                Set nodx = tvwUndNegUsu.Nodes.Add("UN1" & scod1, tvwChild, "UN2" & scod2, CStr(oNegocio2.Cod) & " - " & oNegocio2.Den)
                            End If
                            nodx.Tag = sTag & scod2
                            nodx.EnsureVisible
                            nodx.Expanded = True
                
                            nodx.Checked = UnqaEstaEnVista(oNegocio2.Id, m_iNivelRest, m_iIDRest, VC1, VC2, VC3, VC4)
                            
                            If nodx.Checked = False Then bTodosCheck = False
                        End If
                                    
                        If ((Nivel = 0) _
                        Or ((Nivel = 1) And (scod1 = Unqa)) _
                        Or ((Nivel = 2) And (scod2 = Unqa)) _
                        Or (Nivel = 3)) And bAlgunaUnqa Then
                            For Each oNegocio3 In oNegocio2.UnidadesNegQA.Unidades
                                scod3 = CStr(oNegocio3.Id)
                                
                                bAlgunaUnqa = HayUnqaVarcalAlgunProve(scod3, m_iIDRest, m_iNivelRest, False)
                                
                                If ((Nivel = 0) _
                                Or ((Nivel = 1) And (scod1 = Unqa)) _
                                Or ((Nivel = 2) And (scod2 = Unqa)) _
                                Or ((Nivel = 3) And (scod3 = Unqa))) And bAlgunaUnqa Then
                                    If Nivel = 3 Then
                                        Set nodx = tvwUndNegUsu.Nodes.Add(, , "UN3" & scod3, CStr(oNegocio3.Cod) & " - " & oNegocio3.Den)
                                    Else
                                        Set nodx = tvwUndNegUsu.Nodes.Add("UN2" & scod2, tvwChild, "UN3" & scod3, CStr(oNegocio3.Cod) & " - " & oNegocio3.Den)
                                    End If
                                    nodx.Tag = sTag & scod3
                                    nodx.EnsureVisible
                                    nodx.Expanded = True
                                    
                                    nodx.Checked = UnqaEstaEnVista(oNegocio3.Id, m_iNivelRest, m_iIDRest, VC1, VC2, VC3, VC4)
                                    
                                    If nodx.Checked = False Then bTodosCheck = False
                                End If
                            Next
                            
                            If (Nivel = 3 And (scod3 = Unqa)) Then Exit For
                        End If
                    End If
                        
                    If (Nivel = 3 And (scod3 = Unqa)) Then Exit For
                    If (Nivel = 2 And (scod2 = Unqa)) Then Exit For
                Next
            
                If (Nivel = 3 And (scod3 = Unqa)) Then Exit For
                If (Nivel = 2 And (scod2 = Unqa)) Then Exit For
                If (Nivel = 1 And (scod1 = Unqa)) Then Exit For
            End If
        End If
    Next
    
    m_bDeshabilidar_Chk_ChangeTodas = True
    If bTodosCheck Then
        Me.chkTodasUnidades.Value = vbChecked
    Else
        Me.chkTodasUnidades.Value = vbUnchecked
    End If
    For Each nodx In Me.tvwUndNegUsu.Nodes
        Me.tvwUndNegUsu.selectedItem = nodx
        Exit For
    Next
    Me.tvwUndNegUsu.selectedItem = Nothing
    m_bDeshabilidar_Chk_ChangeTodas = False
    
    Set oNegocio = Nothing
    Set oNegocio2 = Nothing
    Set oNegocio3 = Nothing
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CellGenerarEstructuraUnidadesNegocio", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Determina si en la vista seleccionada la variable concreta tiene visible la unidad dada o no
''' </summary>
''' <param name="Unqa">ID de unidad de negocio</param>
''' <param name="Nivel">Nivel de la variable seleccionada</param>
''' <param name="Id">Id de la variable seleccionada</param>
''' <param name="VC1">Id del padre de la variable seleccionada a nivel 1</param>
''' <param name="VC2">Id del padre de la variable seleccionada a nivel 2</param>
''' <param name="VC3">Id del padre de la variable seleccionada a nivel 3</param>
''' <param name="VC4">Id del padre de la variable seleccionada a nivel 4</param>
''' <returns>Si esta visible o no</returns>
''' <remarks>Llamada desde: GrpGenerarEstructuraUnidadesNegocio CellGenerarEstructuraUnidadesNegocio
'''     DameCheckedUnQaGriProveVars; Tiempo m�ximo:0</remarks>
Private Function UnqaEstaEnVista(ByVal Unqa As Long, ByVal Nivel As Integer, ByVal Id As Long, ByVal VC1 As Long, _
ByVal VC2 As Long, ByVal VC3 As Long, ByVal VC4 As Long) As Boolean
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Nivel
    Case 0
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(Id)
        UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas, "," & Unqa & ",") > 0)
    Case 1
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(Id)
        UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas, "," & Unqa & ",") > 0)
    Case 2
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(VC1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(Id)
        UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas, "," & Unqa & ",") > 0)
    Case 3
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(VC1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(VC2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(Id)
        UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas, "," & Unqa & ",") > 0)
    Case 4
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(VC1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(VC2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(VC3)
        scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(Id)
        UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas, "," & Unqa & ",") > 0)
    Case 5
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(VC1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(VC2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(VC3)
        scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(VC4)
        scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & CStr(Id)
        UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas, "," & Unqa & ",") > 0)
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "UnqaEstaEnVista", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Rellena el grid VarsProve con las variables de calidad visibles para proveedores
''' visibles y unidad de negocio visibles.
''' </summary>
''' <remarks>Llamada desde: CargarGridVarCalidad    Form_MouseMove  sdbgProveVarsQA_MouseMove
'''     sdbgVarsQAProve_MouseMove   ; Tiempo m�ximo:0,4</remarks>
Private Sub Rellenar_sdbgVarsQAProve()
    
    Dim oVarCalif As CVarCalificacion
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Dim oUnidadUnqa1 As CUnidadNegQA
    Dim oUnidadUnqa2 As CUnidadNegQA
    Dim oUnidadUnqa3 As CUnidadNegQA
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    ReDim m_sVarsNoVisible(0)
    ReDim m_sVarsUnqaNoVisible(0)
    
    sdbgVarsQAProve.RemoveAll
    
    Set oVarCalif = oFSGSRaiz.Generar_CVarCalificacion

    Set m_oUnqaVarcalEnProve = oFSGSRaiz.Generar_CVariablesCalidad
    Set m_oUnqaEnProve = oFSGSRaiz.Generar_CUnidadesNegQA

    'configuraci�n de la vista unidades: m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas
    'configuraci�n de la vista variables: m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
    'configuraci�n de pantalla proveedores: oProv.VisibleEnGrafico
    'coleccion unidades con puntos: m_oUnqaEnProve (con datos o la de nivel 0 q va siempre)
    'coleccion variable#unidad#proveedor con puntos: m_oUnqaVarcalEnProve
    'colecci�n de variable-unidad invisibles: m_sVarsUnqaNoVisible

    If Not m_oVarsCalidad Is Nothing Then
        For Each m_oVarCal1 In m_oVarsCalidad
            For Each oUnidadUnqa1 In m_oUnidadesQa.Unidades
                If m_oVarCal1.Nivel = 0 Then
                    scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_oVarCal1.Id)
                    
                    Rellenar_sdbgVarsQAProve_Nivel0 oUnidadUnqa1, 1, scod0
                    
                    If oUnidadUnqa1.unQA1 = -1 And Not AlgunaUnqa(oUnidadUnqa1.Id) Then
                        m_oUnqaEnProve.Add oUnidadUnqa1.Id, 0, 0, "", "", 0
                    End If
                    
                    For Each oUnidadUnqa2 In oUnidadUnqa1.UnidadesNegQA.Unidades
                        Rellenar_sdbgVarsQAProve_Nivel0 oUnidadUnqa2, 2, scod0
                        
                        For Each oUnidadUnqa3 In oUnidadUnqa2.UnidadesNegQA.Unidades
                            Rellenar_sdbgVarsQAProve_Nivel0 oUnidadUnqa3, 3, scod0
                        Next
                    Next
                Else
                    scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(m_oVarCal1.Id)
                                       
                    Rellenar_sdbgVarsQAProve_Nivel1 oUnidadUnqa1, 1, scod0, scod1
                                        
                    For Each oUnidadUnqa2 In oUnidadUnqa1.UnidadesNegQA.Unidades
                        Rellenar_sdbgVarsQAProve_Nivel1 oUnidadUnqa2, 2, scod0, scod1
                        
                        For Each oUnidadUnqa3 In oUnidadUnqa2.UnidadesNegQA.Unidades
                            Rellenar_sdbgVarsQAProve_Nivel1 oUnidadUnqa3, 3, scod0, scod1
                            
                        Next
                    Next
                    
                End If
            Next 'oUnidadUnqa In oUnidadesUnqa.unidades
                
            If m_oVarCal1.Nivel <> 0 Then
                'Variables de nivel 2 dependientes de la variable de nivel 1 que se est� tratando
                If Not m_oVarCal1.VariblesCal Is Nothing Then
                    For Each m_oVarCal2 In m_oVarCal1.VariblesCal
                        For Each oUnidadUnqa1 In m_oUnidadesQa.Unidades
                            scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(m_oVarCal2.Id)
                        
                            Rellenar_sdbgVarsQAProve_Nivel2 oUnidadUnqa1, 1, scod0, scod1, scod2
                        
                            For Each oUnidadUnqa2 In oUnidadUnqa1.UnidadesNegQA.Unidades
                                Rellenar_sdbgVarsQAProve_Nivel2 oUnidadUnqa2, 2, scod0, scod1, scod2
                                
                                For Each oUnidadUnqa3 In oUnidadUnqa2.UnidadesNegQA.Unidades
                                    Rellenar_sdbgVarsQAProve_Nivel2 oUnidadUnqa3, 3, scod0, scod1, scod2
                
                                Next
                            Next
                        Next 'oUnidadUnqa In oUnidadesUnqa.unidades
                        
                        'Variables de nivel 3 dependientes de la variable de nivel 2 que se est� tratando
                        If Not m_oVarCal2.VariblesCal Is Nothing Then
                            For Each m_oVarCal3 In m_oVarCal2.VariblesCal
                                For Each oUnidadUnqa1 In m_oUnidadesQa.Unidades
                                    scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(m_oVarCal3.Id)
                                
                                    Rellenar_sdbgVarsQAProve_Nivel3 oUnidadUnqa1, 1, scod0, scod1, scod2, scod3
                                
                                    For Each oUnidadUnqa2 In oUnidadUnqa1.UnidadesNegQA.Unidades
                                        Rellenar_sdbgVarsQAProve_Nivel3 oUnidadUnqa2, 2, scod0, scod1, scod2, scod3
                                        
                                        For Each oUnidadUnqa3 In oUnidadUnqa2.UnidadesNegQA.Unidades
                                            Rellenar_sdbgVarsQAProve_Nivel3 oUnidadUnqa3, 3, scod0, scod1, scod2, scod3
                        
                                        Next
                                    Next
                                Next 'oUnidadUnqa In oUnidadesUnqa.unidades
                                
                                'Variables de nivel 4 dependientes de la variable de nivel 3 que se est� tratando
                                If Not m_oVarCal3.VariblesCal Is Nothing Then
                                    For Each m_oVarCal4 In m_oVarCal3.VariblesCal
                                        For Each oUnidadUnqa1 In m_oUnidadesQa.Unidades
                                            scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(m_oVarCal4.Id)
                                        
                                            Rellenar_sdbgVarsQAProve_Nivel4 oUnidadUnqa1, 1, scod0, scod1, scod2, scod3, scod4
                                        
                                            For Each oUnidadUnqa2 In oUnidadUnqa1.UnidadesNegQA.Unidades
                                                Rellenar_sdbgVarsQAProve_Nivel4 oUnidadUnqa2, 2, scod0, scod1, scod2, scod3, scod4
                                                
                                                For Each oUnidadUnqa3 In oUnidadUnqa2.UnidadesNegQA.Unidades
                                                    Rellenar_sdbgVarsQAProve_Nivel4 oUnidadUnqa3, 3, scod0, scod1, scod2, scod3, scod4
                                
                                                Next
                                            Next
                                        Next 'oUnidadUnqa In oUnidadesUnqa.unidades
                                        
                                        'Variables de nivel 5 dependientes de la variable de nivel 4 que se est� tratando
                                        If Not m_oVarCal4.VariblesCal Is Nothing Then
                                            For Each m_oVarCal5 In m_oVarCal4.VariblesCal
                                                For Each oUnidadUnqa1 In m_oUnidadesQa.Unidades
                                                    scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & CStr(m_oVarCal5.Id)
                                                    
                                                    Rellenar_sdbgVarsQAProve_Nivel5 oUnidadUnqa1, 1, scod0, scod1, scod2, scod3, scod4, scod5
                                                
                                                    For Each oUnidadUnqa2 In oUnidadUnqa1.UnidadesNegQA.Unidades
                                                        Rellenar_sdbgVarsQAProve_Nivel5 oUnidadUnqa2, 2, scod0, scod1, scod2, scod3, scod4, scod5
                                                        
                                                        For Each oUnidadUnqa3 In oUnidadUnqa2.UnidadesNegQA.Unidades
                                                            Rellenar_sdbgVarsQAProve_Nivel5 oUnidadUnqa3, 3, scod0, scod1, scod2, scod3, scod4, scod5
                                        
                                                        Next
                                                    Next
                                                Next 'oUnidadUnqa In oUnidadesUnqa.unidades
                                            Next 'm_oVarCal5 In m_oVarCal4.VariblesCal
                                        End If 'Not m_oVarCal4.VariblesCal Is Nothing
                                    Next 'm_oVarCal4 In m_oVarCal3.VariblesCal
                                End If 'Not m_oVarCal3.VariblesCal Is Nothing
                            Next 'm_oVarCal3 In m_oVarCal2.VariblesCal
                        End If 'Not m_oVarCal2.VariblesCal Is Nothing
                    Next 'm_oVarCal2 In m_oVarCal1.VariblesCal
                End If 'Not m_oVarCal1.VariblesCal Is Nothing
            End If 'm_oVarCal1.Nivel <> 0
        Next 'For Each m_oVarCal1 In m_oVarsCalidad
    End If 'm_oVarsCalidad Is Nothing

    'Muestra los splitter
    If sdbgVarsQAProve.Groups.Count > 2 Then
        sdbgVarsQAProve.SplitterVisible = True
        sdbgVarsQAProve.SplitterPos = 2
    End If
    sdbgVarsQAProve.MoveFirst
    
    If m_oVistaCalSeleccionada.TipoVista = Vistainicialqa Then m_bVistaInicialMantUnqa = True
    
    Set oVarCalif = Nothing
    
    cmdNoVisibles.Visible = IIf(UBound(m_sVarsNoVisible) > 0, True, False)
    
    If UBound(m_sVarsNoVisible) > 0 Then
        sdbgVarsQAProve.Groups(0).HeadStyleSet = "VariablesNoVisible"
        sdbgVarsQAProve.Groups(1).HeadStyleSet = "VariablesNoVisible"
        sdbgProveVarsQA.Groups(0).HeadStyleSet = "ProveedorNoVisible"
        sdbgProveVarsQA.Groups(1).HeadStyleSet = "Unqa"
    Else
        sdbgVarsQAProve.Groups(0).HeadStyleSet = "Variables"
        sdbgVarsQAProve.Groups(1).HeadStyleSet = "Variables"
        sdbgProveVarsQA.Groups(0).HeadStyleSet = "Proveedor"
        sdbgProveVarsQA.Groups(1).HeadStyleSet = "Unqa"
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgVarsQAProve", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' A�adir una fila al grid VarsProve en funci�n de q el usuario tenga permiso de consulta, sea visible la variable
''' en la vista, algun proveedor tenga puntos en la variable y sea visible la unidad en la vista.
''' Lineas sin q algun proveedor tenga puntos en la variable son invisibles.
''' Los pares variable-unidad invisibles y con avisos/restricciones se guardan en una colecci�n.
''' Para una variable visible, la unidad a nivel de grupo tiene q mostrarse siempre.
''' visible o no.
''' Por defecto, en la vista inicial se visualizar�n las puntuaciones a nivel de "Grupo" y las puntuaciones de las
''' unidades de negocio que cumplan con al menos una restricci�n o aviso en uno de los proveedores de la vista.
''' </summary>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <param name="scod0">Codigo para la vista de la variable a nivel 0</param>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve ; Tiempo m�ximo: 0,1</remarks>
Private Sub Rellenar_sdbgVarsQAProve_Nivel0(ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer, ByVal scod0 As String)
    Dim oProv As CProveedor
    Dim oVarCalif As CVarCalificacion
    Dim sFilaGrid As String
    Dim bVisibleVista As Boolean
    Dim bAlgunProveConLaUnqa As Boolean
    Dim bAlgunProveConAvisoORestr As Boolean
    Dim sCalif As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    Set oVarCalif = oFSGSRaiz.Generar_CVarCalificacion
    
    sFilaGrid = ""
    
    sFilaGrid = " " & Chr(m_lSeparador) & m_oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    m_lIdNivel0 = m_oVarCal1.Id
    
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal1.Id & "#" & m_oVarCal1.IdVarCal1 & "#" & m_oVarCal1.IdVarCal2
    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.IdVarCal3 & "#" & m_oVarCal1.IdVarCal4 & "#" & m_oVarCal1.Nivel
    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal1.RestriccionSiNo) & "#" & m_oVarCal1.RestriccionInf
    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.RestriccionSup & "#" & CInt(m_oVarCal1.AvisoSiNo)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.AvisoInf & "#" & m_oVarCal1.AvisoSup
    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.Tipo & "#" & m_oVarCal1.Subtipo
    ''unidades de negocio
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oUnidadUnqa.Cod & " - " & oUnidadUnqa.Den & Chr(m_lSeparador) & ""
    ''
    bAlgunProveConLaUnqa = False
    bAlgunProveConAvisoORestr = False
    ''
    For Each oProv In m_oProvesAsig
        If oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        Else
            
            If Not ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal1.Id, 0, oProv.Cod) Then
                m_oUnqaVarcalEnProve.Add m_oVarCal1.Id, 0, "", Nothing, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , CStr(oUnidadUnqa.Id) & "#" & oProv.Cod
            End If
            
            bAlgunProveConLaUnqa = True
            
            With oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id)
                If m_oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                    If .provemat = 1 Then
                        sCalif = ""
                        If Not IsNull(.Puntuacion) Then
                            bAlgunProveConAvisoORestr = bAlgunProveConAvisoORestr Or AlgunProveConAvisoORestr(.Puntuacion, m_oVarCal1)
                            sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                        End If
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                    Else
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    End If
                End If
            End With
        End If
        'Para mostror el tooltip de avisos o restricciones
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
    Next
        
    bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
    If oUnidadUnqa.unQA1 > -1 Then
        ' -1 -> unqa nivel 0
        '>-1 -> unqa >nivel 0
        If m_oVistaCalSeleccionada.TipoVista = Vistainicialqa And (m_bVistaInicialMantUnqa = False) Then
            bVisibleVista = bVisibleVista And bAlgunProveConAvisoORestr
            
            If bAlgunProveConAvisoORestr = True Then
                If InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas, "," & oUnidadUnqa.Id & ",") = 0 Then
                    m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas & oUnidadUnqa.Id & ","
                End If
            End If
        Else
            bVisibleVista = bVisibleVista And (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas, "," & oUnidadUnqa.Id & ",") > 0)
        End If
    End If
    
    'A�ade la fila a la grid
    If (m_oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) _
    And bVisibleVista And (bAlgunProveConLaUnqa Or oUnidadUnqa.unQA1 = -1) Then
        sdbgVarsQAProve.AddItem sFilaGrid
        sdbgVarsQAProve.Update
    ElseIf bAlgunProveConAvisoORestr Then
        MeteEnColeccionNoVisibles m_oVarCal1.Nivel, m_oVarCal1.Id, m_oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den, oUnidadUnqa.Id, oUnidadUnqa.Cod, oUnidadUnqa.Den
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgVarsQAProve_Nivel0", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' A�adir una fila al grid VarsProve en funci�n de q el usuario tenga permiso de consulta, sea visible la variable
''' en la vista, algun proveedor tenga puntos en la variable y sea visible la unidad en la vista.
''' Lineas sin q algun proveedor tenga puntos en la variable son invisibles.
''' Los pares variable-unidad invisibles y con avisos/restricciones se guardan en una colecci�n.
''' Para una variable visible, la unidad a nivel de grupo tiene q mostrarse siempre.
''' visible o no.
''' Por defecto, en la vista inicial se visualizar�n las puntuaciones a nivel de "Grupo" y las puntuaciones de las
''' unidades de negocio que cumplan con al menos una restricci�n o aviso en uno de los proveedores de la vista.
''' </summary>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <param name="scod0">Codigo para la vista de la variable a nivel 0</param>
''' <param name="scod1">Codigo para la vista de la variable a nivel 1</param>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve ; Tiempo m�ximo: 0,1</remarks>
Private Sub Rellenar_sdbgVarsQAProve_Nivel1(ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer, ByVal scod0 As String, ByVal scod1 As String)
    Dim oProv As CProveedor
    Dim oVarCalif As CVarCalificacion
    Dim sFilaGrid As String
    Dim bVisibleVista As Boolean
    Dim bAlgunProveConLaUnqa As Boolean
    Dim bAlgunProveConAvisoORestr As Boolean
    Dim sCalif As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Set oVarCalif = oFSGSRaiz.Generar_CVarCalificacion
    
    sFilaGrid = ""
    
    sFilaGrid = m_oVarCal1.Cod & Chr(m_lSeparador) & m_oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal1.Id & "#" & m_oVarCal1.IdVarCal1 & "#" & m_oVarCal1.IdVarCal2
    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.IdVarCal3 & "#" & m_oVarCal1.IdVarCal4 & "#" & m_oVarCal1.Nivel
    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal1.RestriccionSiNo) & "#" & m_oVarCal1.RestriccionInf
    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.RestriccionSup & "#" & CInt(m_oVarCal1.AvisoSiNo)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.AvisoInf & "#" & m_oVarCal1.AvisoSup
    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.Tipo & "#" & m_oVarCal1.Subtipo
    ''unidades de negocio
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oUnidadUnqa.Cod & " - " & oUnidadUnqa.Den & Chr(m_lSeparador) & ""
    ''
    bAlgunProveConLaUnqa = False
    bAlgunProveConAvisoORestr = False
    ''
    For Each oProv In m_oProvesAsig
        If oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        Else
            If Not ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal1.Id, 1, oProv.Cod) Then 'And oProv.VisibleEnGrafico Then
                m_oUnqaVarcalEnProve.Add m_oVarCal1.Id, 1, "", Nothing, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , oUnidadUnqa.Id & "#" & oProv.Cod
            End If
            
            bAlgunProveConLaUnqa = True
            
            With oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id)
                If m_oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                    If .provemat = 1 Then
                        sCalif = ""
                        If Not IsNull(.Puntuacion) Then
                            bAlgunProveConAvisoORestr = bAlgunProveConAvisoORestr Or AlgunProveConAvisoORestr(.Puntuacion, m_oVarCal1)
                            sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                        End If
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                    Else
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    End If
                End If
            End With
        End If
        'Para mostror el tooltip de avisos o restricciones
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
    Next
    
    If bAlgunProveConLaUnqa And Not AlgunaUnqa(oUnidadUnqa.Id) Then
        m_oUnqaEnProve.Add oUnidadUnqa.Id, 0, 0, "", "", 0
    End If
    
    'Se comprueba si la variable es visible por la vista
    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
        If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal1.MaterialProceso Then
            bVisibleVista = False
        Else
            bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).Visible
            If oUnidadUnqa.unQA1 > -1 Then
                If m_oVistaCalSeleccionada.TipoVista = Vistainicialqa And (m_bVistaInicialMantUnqa = False) Then
                    bVisibleVista = bVisibleVista And bAlgunProveConAvisoORestr
                    
                    If bAlgunProveConAvisoORestr = True Then
                        If InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas, "," & oUnidadUnqa.Id & ",") = 0 Then
                            m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas & oUnidadUnqa.Id & ","
                        End If
                    End If
                Else
                    bVisibleVista = bVisibleVista And (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas, "," & oUnidadUnqa.Id & ",") > 0)
                End If
            End If
        End If
    Else
        bVisibleVista = False
    End If
    
    'A�ade la fila a la grid
    If (m_oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) _
    And bVisibleVista And (bAlgunProveConLaUnqa Or oUnidadUnqa.unQA1 = -1) Then
        sdbgVarsQAProve.AddItem sFilaGrid
        sdbgVarsQAProve.Update
    ElseIf bAlgunProveConAvisoORestr Then
        MeteEnColeccionNoVisibles m_oVarCal1.Nivel, m_oVarCal1.Id, m_oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den, oUnidadUnqa.Id, oUnidadUnqa.Cod, oUnidadUnqa.Den
    End If

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgVarsQAProve_Nivel1", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' A�adir una fila al grid VarsProve en funci�n de q el usuario tenga permiso de consulta, sea visible la variable
''' en la vista, algun proveedor tenga puntos en la variable y sea visible la unidad en la vista.
''' Lineas sin q algun proveedor tenga puntos en la variable son invisibles.
''' Los pares variable-unidad invisibles y con avisos/restricciones se guardan en una colecci�n.
''' Para una variable visible, la unidad a nivel de grupo tiene q mostrarse siempre.
''' visible o no.
''' Por defecto, en la vista inicial se visualizar�n las puntuaciones a nivel de "Grupo" y las puntuaciones de las
''' unidades de negocio que cumplan con al menos una restricci�n o aviso en uno de los proveedores de la vista.
''' </summary>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <param name="scod0">Codigo para la vista de la variable a nivel 0</param>
''' <param name="scod1">Codigo para la vista de la variable a nivel 1</param>
''' <param name="scod2">Codigo para la vista de la variable a nivel 2</param>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve ; Tiempo m�ximo: 0,1</remarks>
Private Sub Rellenar_sdbgVarsQAProve_Nivel2(ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer, ByVal scod0 As String, ByVal scod1 As String, ByVal scod2 As String)
    Dim oProv As CProveedor
    Dim oVarCalif As CVarCalificacion
    Dim sFilaGrid As String
    Dim bVisibleVista As Boolean
    Dim bAlgunProveConLaUnqa As Boolean
    Dim bAlgunProveConAvisoORestr As Boolean
    Dim sCalif As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Set oVarCalif = oFSGSRaiz.Generar_CVarCalificacion
    
    sFilaGrid = ""
    sFilaGrid = m_oVarCal2.Cod & Chr(m_lSeparador) & m_oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal2.Id & "#" & m_oVarCal2.IdVarCal1 & "#" & m_oVarCal2.IdVarCal2
    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.IdVarCal3 & "#" & m_oVarCal2.IdVarCal4 & "#" & m_oVarCal2.Nivel
    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal2.RestriccionSiNo) & "#" & m_oVarCal2.RestriccionInf
    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.RestriccionSup & "#" & CInt(m_oVarCal2.AvisoSiNo)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.AvisoInf & "#" & m_oVarCal2.AvisoSup
    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.Tipo & "#" & m_oVarCal2.Subtipo
    ''unidades de negocio
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oUnidadUnqa.Cod & " - " & oUnidadUnqa.Den & Chr(m_lSeparador) & ""
    ''
    bAlgunProveConLaUnqa = False
    bAlgunProveConAvisoORestr = False
    ''
    For Each oProv In m_oProvesAsig
        If oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        Else
            With oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id)
                If Not oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                    If .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    Else
                        If Not ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal2.Id, 2, oProv.Cod) Then 'And oProv.VisibleEnGrafico Then
                            m_oUnqaVarcalEnProve.Add m_oVarCal2.Id, 2, "", Nothing, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , oUnidadUnqa.Id & "#" & oProv.Cod
                        End If
                        
                        bAlgunProveConLaUnqa = True
                        ''
                        With .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id)
                            If m_oVarCal2.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                                If .provemat = 1 Then
                                    sCalif = ""
                                    If Not IsNull(.Puntuacion) Then
                                        bAlgunProveConAvisoORestr = bAlgunProveConAvisoORestr Or AlgunProveConAvisoORestr(.Puntuacion, m_oVarCal2)
                                        sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                                    End If
                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                                Else
                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                End If
                            End If
                        End With
                    End If
                Else
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                End If
            End With
        End If
        'Para mostror el tooltip de avisos o restricciones
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
    Next
    
    If bAlgunProveConLaUnqa And Not AlgunaUnqa(oUnidadUnqa.Id) Then
        m_oUnqaEnProve.Add oUnidadUnqa.Id, 0, 0, "", "", 0
    End If

    'Se comprueba si la variable es visible por la vista
    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2) Is Nothing Then
        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
            If .VariblesCalVista Is Nothing Then
                bVisibleVista = False
            Else
                If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal2.MaterialProceso Then
                    bVisibleVista = False
                Else
                    If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                        bVisibleVista = .VariblesCalVista.Item(scod2).Visible
                        If oUnidadUnqa.unQA1 > -1 Then
                            If m_oVistaCalSeleccionada.TipoVista = Vistainicialqa And (m_bVistaInicialMantUnqa = False) Then
                                bVisibleVista = bVisibleVista And bAlgunProveConAvisoORestr
                                
                                If bAlgunProveConAvisoORestr = True Then
                                    If InStr(.VariblesCalVista.Item(scod2).UnQas, "," & oUnidadUnqa.Id & ",") = 0 Then
                                        .VariblesCalVista.Item(scod2).UnQas = .VariblesCalVista.Item(scod2).UnQas & oUnidadUnqa.Id & ","
                                    End If
                                End If
                            Else
                                bVisibleVista = bVisibleVista And (InStr(.VariblesCalVista.Item(scod2).UnQas, "," & oUnidadUnqa.Id & ",") > 0)
                            End If
                        End If
                    Else
                        bVisibleVista = False
                    End If
                End If
            End If
        End With
    Else
        bVisibleVista = False
    End If

    'A�ade la fila a la grid
    If (m_oVarCal2.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) _
    And bVisibleVista And (bAlgunProveConLaUnqa Or oUnidadUnqa.unQA1 = -1) Then
        sdbgVarsQAProve.AddItem sFilaGrid
        sdbgVarsQAProve.Update
    ElseIf bAlgunProveConAvisoORestr Then
        MeteEnColeccionNoVisibles m_oVarCal2.Nivel, m_oVarCal2.Id, m_oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den, oUnidadUnqa.Id, oUnidadUnqa.Cod, oUnidadUnqa.Den
    End If

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgVarsQAProve_Nivel2", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' A�adir una fila al grid VarsProve en funci�n de q el usuario tenga permiso de consulta, sea visible la variable
''' en la vista, algun proveedor tenga puntos en la variable y sea visible la unidad en la vista.
''' Lineas sin q algun proveedor tenga puntos en la variable son invisibles.
''' Los pares variable-unidad invisibles y con avisos/restricciones se guardan en una colecci�n.
''' Para una variable visible, la unidad a nivel de grupo tiene q mostrarse siempre.
''' visible o no.
''' Por defecto, en la vista inicial se visualizar�n las puntuaciones a nivel de "Grupo" y las puntuaciones de las
''' unidades de negocio que cumplan con al menos una restricci�n o aviso en uno de los proveedores de la vista.
''' </summary>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <param name="scod0">Codigo para la vista de la variable a nivel 0</param>
''' <param name="scod1">Codigo para la vista de la variable a nivel 1</param>
''' <param name="scod2">Codigo para la vista de la variable a nivel 2</param>
''' <param name="scod3">Codigo para la vista de la variable a nivel 3</param>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve ; Tiempo m�ximo: 0,1</remarks>
Private Sub Rellenar_sdbgVarsQAProve_Nivel3(ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer, ByVal scod0 As String, ByVal scod1 As String, ByVal scod2 As String, ByVal scod3 As String)
    Dim oProv As CProveedor
    Dim oVarCalif As CVarCalificacion
    Dim sFilaGrid As String
    Dim bVisibleVista As Boolean
    Dim bAlgunProveConLaUnqa As Boolean
    Dim bAlgunProveConAvisoORestr As Boolean
    Dim sCalif As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Set oVarCalif = oFSGSRaiz.Generar_CVarCalificacion
    
    sFilaGrid = ""
    sFilaGrid = m_oVarCal3.Cod & Chr(m_lSeparador) & m_oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal3.Id & "#" & m_oVarCal3.IdVarCal1 & "#" & m_oVarCal3.IdVarCal2
    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.IdVarCal3 & "#" & m_oVarCal3.IdVarCal4 & "#" & m_oVarCal3.Nivel
    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal3.RestriccionSiNo) & "#" & m_oVarCal3.RestriccionInf
    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.RestriccionSup & "#" & CInt(m_oVarCal3.AvisoSiNo)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.AvisoInf & "#" & m_oVarCal3.AvisoSup
    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.Tipo & "#" & m_oVarCal3.Subtipo
    ''unidades de negocio

    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oUnidadUnqa.Cod & " - " & oUnidadUnqa.Den & Chr(m_lSeparador) & ""
    ''
    bAlgunProveConLaUnqa = False
    bAlgunProveConAvisoORestr = False
    ''
    For Each oProv In m_oProvesAsig
        If oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        Else
            With oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id)
                If Not oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                    If .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    Else
                        If Not .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                            With .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id)
                                If Not .VariblesCal Is Nothing Then
                                    If .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                    Else
                                        If Not ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal3.Id, 3, oProv.Cod) Then
                                            m_oUnqaVarcalEnProve.Add m_oVarCal3.Id, 3, "", Nothing, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , oUnidadUnqa.Id & "#" & oProv.Cod
                                        End If
                                            
                                        bAlgunProveConLaUnqa = True
                                        ''
                                        With .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id)
                                            If m_oVarCal3.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                                                If .provemat = 1 Then
                                                    sCalif = ""
                                                    If Not IsNull(.Puntuacion) Then
                                                        bAlgunProveConAvisoORestr = bAlgunProveConAvisoORestr Or AlgunProveConAvisoORestr(.Puntuacion, m_oVarCal3)
                                                        sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                                                    End If
                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                                                Else
                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                End If
                                            End If
                                        End With
                                    End If
                                Else
                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                End If
                            End With
                        Else
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                        End If
                    End If
                Else
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                End If
            End With
        End If
        'Para mostror el tooltip de avisos o restricciones
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
    Next
    
    If bAlgunProveConLaUnqa And Not AlgunaUnqa(oUnidadUnqa.Id) Then
        m_oUnqaEnProve.Add oUnidadUnqa.Id, 0, 0, "", "", 0
    End If


    'Se comprueba si la variable es visible por la vista
    
    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3) Is Nothing Then
        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                With .VariblesCalVista.Item(scod2)
                    If .VariblesCalVista Is Nothing Then
                        bVisibleVista = False
                    Else
                        If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal3.MaterialProceso Then
                            bVisibleVista = False
                        Else
                            If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                bVisibleVista = .VariblesCalVista.Item(scod3).Visible
                                If oUnidadUnqa.unQA1 > -1 Then
                                    If m_oVistaCalSeleccionada.TipoVista = Vistainicialqa And (m_bVistaInicialMantUnqa = False) Then
                                        bVisibleVista = bVisibleVista And bAlgunProveConAvisoORestr
                                        
                                        If bAlgunProveConAvisoORestr = True Then
                                            If InStr(.VariblesCalVista.Item(scod3).UnQas, "," & oUnidadUnqa.Id & ",") = 0 Then
                                                .VariblesCalVista.Item(scod3).UnQas = .VariblesCalVista.Item(scod3).UnQas & oUnidadUnqa.Id & ","
                                            End If
                                        End If
                                    Else
                                        bVisibleVista = bVisibleVista And (InStr(.VariblesCalVista.Item(scod3).UnQas, "," & oUnidadUnqa.Id & ",") > 0)
                                    End If
                                End If
                            Else
                                bVisibleVista = False
                            End If
                        End If
                    End If
                End With
            End If
        End With
    Else
        bVisibleVista = False
    End If

    'A�ade la fila a la grid
    If (m_oVarCal3.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) _
    And bVisibleVista And (bAlgunProveConLaUnqa Or oUnidadUnqa.unQA1 = -1) Then
        sdbgVarsQAProve.AddItem sFilaGrid
        sdbgVarsQAProve.Update
    ElseIf bAlgunProveConAvisoORestr Then
        MeteEnColeccionNoVisibles m_oVarCal3.Nivel, m_oVarCal3.Id, m_oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den, oUnidadUnqa.Id, oUnidadUnqa.Cod, oUnidadUnqa.Den
    End If

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgVarsQAProve_Nivel3", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' A�adir una fila al grid VarsProve en funci�n de q el usuario tenga permiso de consulta, sea visible la variable
''' en la vista, algun proveedor tenga puntos en la variable y sea visible la unidad en la vista.
''' Lineas sin q algun proveedor tenga puntos en la variable son invisibles.
''' Los pares variable-unidad invisibles y con avisos/restricciones se guardan en una colecci�n.
''' Para una variable visible, la unidad a nivel de grupo tiene q mostrarse siempre.
''' visible o no.
''' Por defecto, en la vista inicial se visualizar�n las puntuaciones a nivel de "Grupo" y las puntuaciones de las
''' unidades de negocio que cumplan con al menos una restricci�n o aviso en uno de los proveedores de la vista.
''' </summary>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <param name="scod0">Codigo para la vista de la variable a nivel 0</param>
''' <param name="scod1">Codigo para la vista de la variable a nivel 1</param>
''' <param name="scod2">Codigo para la vista de la variable a nivel 2</param>
''' <param name="scod3">Codigo para la vista de la variable a nivel 3</param>
''' <param name="scod4">Codigo para la vista de la variable a nivel 4</param>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve ; Tiempo m�ximo: 0,1</remarks>
Private Sub Rellenar_sdbgVarsQAProve_Nivel4(ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer, ByVal scod0 As String, ByVal scod1 As String, ByVal scod2 As String, ByVal scod3 As String, ByVal scod4 As String)
    Dim oProv As CProveedor
    Dim oVarCalif As CVarCalificacion
    Dim sFilaGrid As String
    Dim bVisibleVista As Boolean
    Dim bAlgunProveConLaUnqa As Boolean
    Dim bAlgunProveConAvisoORestr As Boolean
    Dim sCalif As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Set oVarCalif = oFSGSRaiz.Generar_CVarCalificacion
    
    sFilaGrid = ""
    sFilaGrid = m_oVarCal4.Cod & Chr(m_lSeparador) & m_oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal4.Id & "#" & m_oVarCal4.IdVarCal1 & "#" & m_oVarCal4.IdVarCal2
    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.IdVarCal3 & "#" & m_oVarCal4.IdVarCal4 & "#" & m_oVarCal4.Nivel
    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal4.RestriccionSiNo) & "#" & m_oVarCal4.RestriccionInf
    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.RestriccionSup & "#" & CInt(m_oVarCal4.AvisoSiNo)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.AvisoInf & "#" & m_oVarCal4.AvisoSup
    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.Tipo & "#" & m_oVarCal4.Subtipo
    ''unidades de negocio
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oUnidadUnqa.Cod & " - " & oUnidadUnqa.Den & Chr(m_lSeparador) & ""
    ''
    bAlgunProveConLaUnqa = False
    bAlgunProveConAvisoORestr = False
    ''
    For Each oProv In m_oProvesAsig
        If oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        Else
            With oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id)
                If Not oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                    If .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    Else
                        If Not .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                            With .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id)
                                If Not .VariblesCal Is Nothing Then
                                    If .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                    Else
                                        If Not .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                            With .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id)
                                                If Not .VariblesCal Is Nothing Then
                                                    If .VariblesCal.Item(CStr(m_oVarCal4.Nivel) & CStr(m_oVarCal4.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                    Else
                                                        If Not ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal4.Id, 4, oProv.Cod) Then 'And oProv.VisibleEnGrafico Then
                                                            m_oUnqaVarcalEnProve.Add m_oVarCal4.Id, 4, "", Nothing, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , oUnidadUnqa.Id & "#" & oProv.Cod
                                                        End If
                                                        
                                                        bAlgunProveConLaUnqa = True
                                                        ''
                                                        With .VariblesCal.Item(CStr(m_oVarCal4.Nivel) & CStr(m_oVarCal4.Id) & "#" & oUnidadUnqa.Id)
                                                            If m_oVarCal4.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                                                                If .provemat = 1 Then
                                                                    sCalif = ""
                                                                    If Not IsNull(.Puntuacion) Then
                                                                        bAlgunProveConAvisoORestr = bAlgunProveConAvisoORestr Or AlgunProveConAvisoORestr(.Puntuacion, m_oVarCal4)
                                                                        sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                                                                    End If
                                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                                                                Else
                                                                     sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                End If
                                                            End If
                                                        End With
                                                    End If
                                                Else
                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                End If
                                            End With
                                        Else
                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                        End If
                                    End If
                                Else
                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                End If
                            End With
                        Else
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                        End If
                    End If
                Else
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                End If
            End With
        End If
        'Para mostror el tooltip de avisos o restricciones
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
    Next
    
    If bAlgunProveConLaUnqa And Not AlgunaUnqa(oUnidadUnqa.Id) Then
        m_oUnqaEnProve.Add oUnidadUnqa.Id, 0, 0, "", "", 0
    End If

    'Se comprueba si la variable es visible por la vista
    
    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                With .VariblesCalVista.Item(scod2)
                    If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                        With .VariblesCalVista.Item(scod3)
                        If .VariblesCalVista Is Nothing Then
                            bVisibleVista = False
                        Else
                            If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal4.MaterialProceso Then
                                bVisibleVista = False
                            Else
                                If Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                    bVisibleVista = .VariblesCalVista.Item(scod4).Visible
                                    If oUnidadUnqa.unQA1 > -1 Then
                                        If m_oVistaCalSeleccionada.TipoVista = Vistainicialqa And (m_bVistaInicialMantUnqa = False) Then
                                            bVisibleVista = bVisibleVista And bAlgunProveConAvisoORestr
                                            
                                            If bAlgunProveConAvisoORestr = True Then
                                                If InStr(.VariblesCalVista.Item(scod4).UnQas, "," & oUnidadUnqa.Id & ",") = 0 Then
                                                    .VariblesCalVista.Item(scod4).UnQas = .VariblesCalVista.Item(scod4).UnQas & oUnidadUnqa.Id & ","
                                                End If
                                            End If
                                        Else
                                            bVisibleVista = bVisibleVista And (InStr(.VariblesCalVista.Item(scod4).UnQas, "," & oUnidadUnqa.Id & ",") > 0)
                                        End If
                                    End If
                                Else
                                    bVisibleVista = False
                                End If
                            End If
                        End If
                        End With
                    End If
                End With
            End If
        End With
    Else
        bVisibleVista = False
    End If

    'A�ade la fila a la grid
    If (m_oVarCal4.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) _
    And bVisibleVista And (bAlgunProveConLaUnqa Or oUnidadUnqa.unQA1 = -1) Then
        sdbgVarsQAProve.AddItem sFilaGrid
        sdbgVarsQAProve.Update
    ElseIf bAlgunProveConAvisoORestr Then
        MeteEnColeccionNoVisibles m_oVarCal4.Nivel, m_oVarCal4.Id, m_oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den, oUnidadUnqa.Id, oUnidadUnqa.Cod, oUnidadUnqa.Den
    End If

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgVarsQAProve_Nivel4", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' A�adir una fila al grid VarsProve en funci�n de q el usuario tenga permiso de consulta, sea visible la variable
''' en la vista, algun proveedor tenga puntos en la variable y sea visible la unidad en la vista.
''' Lineas sin q algun proveedor tenga puntos en la variable son invisibles.
''' Los pares variable-unidad invisibles y con avisos/restricciones se guardan en una colecci�n.
''' Para una variable visible, la unidad a nivel de grupo tiene q mostrarse siempre.
''' visible o no.
''' Por defecto, en la vista inicial se visualizar�n las puntuaciones a nivel de "Grupo" y las puntuaciones de las
''' unidades de negocio que cumplan con al menos una restricci�n o aviso en uno de los proveedores de la vista.
''' </summary>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <param name="scod0">Codigo para la vista de la variable a nivel 0</param>
''' <param name="scod1">Codigo para la vista de la variable a nivel 1</param>
''' <param name="scod2">Codigo para la vista de la variable a nivel 2</param>
''' <param name="scod3">Codigo para la vista de la variable a nivel 3</param>
''' <param name="scod4">Codigo para la vista de la variable a nivel 4</param>
''' <param name="scod5">Codigo para la vista de la variable a nivel 5</param>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve ; Tiempo m�ximo: 0,1</remarks>
Private Sub Rellenar_sdbgVarsQAProve_Nivel5(ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer, ByVal scod0 As String, ByVal scod1 As String, ByVal scod2 As String, ByVal scod3 As String, ByVal scod4 As String, _
                                            ByVal scod5 As String)
    Dim oProv As CProveedor
    Dim oVarCalif As CVarCalificacion
    Dim sFilaGrid As String
    Dim bVisibleVista As Boolean
    Dim bAlgunProveConLaUnqa As Boolean
    Dim bAlgunProveConAvisoORestr As Boolean
    Dim sCalif As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Set oVarCalif = oFSGSRaiz.Generar_CVarCalificacion
    
    sFilaGrid = ""
    sFilaGrid = m_oVarCal5.Cod & Chr(m_lSeparador) & m_oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal5.Id & "#" & m_oVarCal5.IdVarCal1 & "#" & m_oVarCal5.IdVarCal2
    sFilaGrid = sFilaGrid & "#" & m_oVarCal5.IdVarCal3 & "#" & m_oVarCal5.IdVarCal4 & "#" & m_oVarCal5.Nivel
    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal5.RestriccionSiNo) & "#" & m_oVarCal5.RestriccionInf
    sFilaGrid = sFilaGrid & "#" & m_oVarCal5.RestriccionSup & "#" & CInt(m_oVarCal5.AvisoSiNo)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal5.AvisoInf & "#" & m_oVarCal5.AvisoSup
    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
    sFilaGrid = sFilaGrid & "#" & m_oVarCal5.Tipo & "#" & m_oVarCal5.Subtipo
    ''unidades de negocio
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oUnidadUnqa.Cod & " - " & oUnidadUnqa.Den & Chr(m_lSeparador) & ""
    ''
    bAlgunProveConLaUnqa = False
    bAlgunProveConAvisoORestr = False
    ''
    For Each oProv In m_oProvesAsig
        If oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        Else
            With oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id)
                If Not oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                    If .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    Else
                        If Not .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                            With .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id)
                                If Not .VariblesCal Is Nothing Then
                                    If .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                    Else
                                        If Not .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                            With .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id)
                                                If Not .VariblesCal Is Nothing Then
                                                    If .VariblesCal.Item(CStr(m_oVarCal4.Nivel) & CStr(m_oVarCal4.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                    Else
                                                        If Not .VariblesCal.Item(CStr(m_oVarCal4.Nivel) & CStr(m_oVarCal4.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                                            With .VariblesCal.Item(CStr(m_oVarCal4.Nivel) & CStr(m_oVarCal4.Id) & "#" & oUnidadUnqa.Id)
                                                                If Not .VariblesCal Is Nothing Then
                                                                    If .VariblesCal.Item(CStr(m_oVarCal5.Nivel) & CStr(m_oVarCal5.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                    Else
                                                                    
                                                                        If Not ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal5.Id, 5, oProv.Cod) Then 'And oProv.VisibleEnGrafico Then
                                                                            m_oUnqaVarcalEnProve.Add m_oVarCal5.Id, 5, "", Nothing, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , oUnidadUnqa.Id & "#" & oProv.Cod
                                                                        End If
                                                                        
                                                                        bAlgunProveConLaUnqa = True
                                                                        ''
                                                                        With .VariblesCal.Item(CStr(m_oVarCal5.Nivel) & CStr(m_oVarCal5.Id) & "#" & oUnidadUnqa.Id)
                                                                            If m_oVarCal5.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador Then
                                                                                If .provemat = 1 Then
                                                                                    sCalif = ""
                                                                                    If Not IsNull(.Puntuacion) Then
                                                                                        bAlgunProveConAvisoORestr = bAlgunProveConAvisoORestr Or AlgunProveConAvisoORestr(.Puntuacion, m_oVarCal5)
                                                                                        sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                                                                                    End If
                                                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                                                                                Else
                                                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                                End If
                                                                            End If
                                                                        End With
                                                                    End If
                                                                Else
                                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                End If
                                                            End With
                                                        Else
                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                        End If
                                                    End If
                                                Else
                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                End If
                                            End With
                                        Else
                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                        End If
                                    End If
                                Else
                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                End If
                            End With
                        Else
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                        End If
                    End If
                Else
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                End If
            End With
        End If
        'Para mostror el tooltip de avisos o restricciones
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
    Next

    If bAlgunProveConLaUnqa And Not AlgunaUnqa(oUnidadUnqa.Id) Then
        m_oUnqaEnProve.Add oUnidadUnqa.Id, 0, 0, "", "", 0
    End If

    'Se comprueba si la variable es visible por la vista

    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                With .VariblesCalVista.Item(scod2)
                    If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                        With .VariblesCalVista.Item(scod3)
                            If Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                With .VariblesCalVista.Item(scod4)
                                If .VariblesCalVista Is Nothing Then
                                    bVisibleVista = False
                                Else
                                    If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal5.MaterialProceso Then
                                        bVisibleVista = False
                                    Else
                                        If Not .VariblesCalVista.Item(scod5) Is Nothing Then
                                            bVisibleVista = .VariblesCalVista.Item(scod5).Visible
                                            If oUnidadUnqa.unQA1 > -1 Then
                                                If m_oVistaCalSeleccionada.TipoVista = Vistainicialqa And (m_bVistaInicialMantUnqa = False) Then
                                                    bVisibleVista = bVisibleVista And bAlgunProveConAvisoORestr
                                                    
                                                    If bAlgunProveConAvisoORestr = True Then
                                                        If InStr(.VariblesCalVista.Item(scod5).UnQas, "," & oUnidadUnqa.Id & ",") = 0 Then
                                                            .VariblesCalVista.Item(scod5).UnQas = .VariblesCalVista.Item(scod5).UnQas & oUnidadUnqa.Id & ","
                                                        End If
                                                    End If
                                                Else
                                                    bVisibleVista = bVisibleVista And (InStr(.VariblesCalVista.Item(scod5).UnQas, "," & oUnidadUnqa.Id & ",") > 0)
                                                End If
                                            End If
                                        Else
                                            bVisibleVista = False
                                        End If
                                    End If
                                End If
                                End With
                            End If
                        End With
                    End If
                End With
            End If
        End With
    End If

    'A�ade la fila a la grid
    If (m_oVarCal5.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) _
    And bVisibleVista And (bAlgunProveConLaUnqa Or oUnidadUnqa.unQA1 = -1) Then
        sdbgVarsQAProve.AddItem sFilaGrid
        sdbgVarsQAProve.Update
    ElseIf bAlgunProveConAvisoORestr Then
        MeteEnColeccionNoVisibles m_oVarCal5.Nivel, m_oVarCal5.Id, m_oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den, oUnidadUnqa.Id, oUnidadUnqa.Cod, oUnidadUnqa.Den
    End If

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgVarsQAProve_Nivel5", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Llamada para todas las variables de la comparativa. Crea un arbol con las unidades de negocio accesibles (si
''' ninguna variable en toda la comparativa tiene puntos en una unidad, dicha unidad no es accesible).
''' </summary>
''' <remarks>Llamada desde:  sdbgProveVarsQA_GrpHeadClick     sdbgVarsQAProve_GrpHeadClick
''' ; Tiempo m�ximo: 0,1</remarks>
Private Sub GrpGenerarEstructuraUnidadesNegocio()
    Dim oNegocio As CUnidadNegQA
    Dim oNegocio2 As CUnidadNegQA
    Dim oNegocio3 As CUnidadNegQA
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    
    Dim nodx As MSComctlLib.node
    Dim bNiv0 As Boolean
    
    Dim bTodosCheck As Boolean
    Dim bAlgunaUnqa As Boolean
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Me.tvwUndNegUsu.Visible = True
        
    tvwUndNegUsu.Nodes.clear
    
    Me.chkTodasUnidades.Value = False
       
    bNiv0 = False
    
    bTodosCheck = True
    
    If m_bGrpGenerarEstrucUnQaCambio Then
        m_bGrpGenerarEstrucUnQaLimpiado = False
    End If
        
    For Each oNegocio In m_oUnidadesQa.Unidades
        If Not bNiv0 Then
            bNiv0 = True
        Else
            scod1 = CStr(oNegocio.Id)
            
            bAlgunaUnqa = AlgunaUnqa(scod1)
            
            If bAlgunaUnqa Then
            
                Set nodx = tvwUndNegUsu.Nodes.Add(, , "UN1" & scod1, CStr(oNegocio.Cod) & " - " & oNegocio.Den)
                nodx.Tag = scod1
                nodx.EnsureVisible
                nodx.Expanded = True
                
                If Not m_bGrpGenerarEstrucUnQa1ra Or m_bGrpGenerarEstrucUnQaCambio Then
                    nodx.Checked = False
                    
                    bTodosCheck = False
                Else
                    nodx.Checked = UnqaEstaEnVista(oNegocio.Id, 0, m_lIdNivel0, 0, 0, 0, 0)
                    
                    If nodx.Checked = False Then bTodosCheck = False
                End If
        
                For Each oNegocio2 In oNegocio.UnidadesNegQA.Unidades
                    scod2 = CStr(oNegocio2.Id)
                        
                    bAlgunaUnqa = AlgunaUnqa(scod2)
                    
                    If bAlgunaUnqa Then
                        
                        Set nodx = tvwUndNegUsu.Nodes.Add("UN1" & scod1, tvwChild, "UN2" & scod2, CStr(oNegocio2.Cod) & " - " & oNegocio2.Den)
                        
                        nodx.Tag = scod2
                        nodx.EnsureVisible
                        nodx.Expanded = True
            
                        If Not m_bGrpGenerarEstrucUnQa1ra Or m_bGrpGenerarEstrucUnQaCambio Then
                            nodx.Checked = False
                            
                            bTodosCheck = False
                        Else
                            nodx.Checked = UnqaEstaEnVista(oNegocio2.Id, 0, m_lIdNivel0, 0, 0, 0, 0)
                            
                            If nodx.Checked = False Then bTodosCheck = False
                        End If
                                    
                        For Each oNegocio3 In oNegocio2.UnidadesNegQA.Unidades
                            scod3 = CStr(oNegocio3.Id)
                            
                            bAlgunaUnqa = AlgunaUnqa(scod3)
                            
                            If bAlgunaUnqa Then
                            
                                Set nodx = tvwUndNegUsu.Nodes.Add("UN2" & scod2, tvwChild, "UN3" & scod3, CStr(oNegocio3.Cod) & " - " & oNegocio3.Den)
                                nodx.Tag = scod3
                                nodx.EnsureVisible
                                nodx.Expanded = True
                                
                                If Not m_bGrpGenerarEstrucUnQa1ra Or m_bGrpGenerarEstrucUnQaCambio Then
                                    nodx.Checked = False
                                    
                                    bTodosCheck = False
                                Else
                                    nodx.Checked = UnqaEstaEnVista(oNegocio3.Id, 0, m_lIdNivel0, 0, 0, 0, 0)
                                    
                                    If nodx.Checked = False Then bTodosCheck = False
                                End If
                            End If 'If bAlgunaUnqa Then
                        Next 'For Each oNegocio3
                    End If 'If bAlgunaUnqa Then
                Next 'For Each oNegocio2
            End If 'If bAlgunaUnqa Then
        End If 'If Not bNiv0 Then
    Next 'For Each oNegocio
        
    m_bDeshabilidar_Chk_ChangeTodas = True
    If bTodosCheck Then
        Me.chkTodasUnidades.Value = vbChecked
    Else
        Me.chkTodasUnidades.Value = vbUnchecked
    End If
    For Each nodx In Me.tvwUndNegUsu.Nodes
        Me.tvwUndNegUsu.selectedItem = nodx
        Exit For
    Next
    Me.tvwUndNegUsu.selectedItem = Nothing
    m_bDeshabilidar_Chk_ChangeTodas = False
        
    m_bGrpGenerarEstrucUnQaCambio = False
            
    Set oNegocio = Nothing
    Set oNegocio2 = Nothing
    Set oNegocio3 = Nothing
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "GrpGenerarEstructuraUnidadesNegocio", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Al hacer click en la cabecera del grupo de unidades de negocio la primera vez se debe mostrar
''' todo deschequeado
'''     -> mas tarde se mantiene el ultimo conjunto de chequeados/deschequeados (a menos q haya
'''        cambios entonces es otra vez la primera vez)
'''     -> todo deschequeado independiente de q este chequeado o q no
''' Entonces en caso de chequear algo resultara q todos las unidades seran invisibles en el grid
''' salvo la q acabo de chequear. Esta establece la visibilidad de todas a oculta para q ese primer
''' check refleje lo q tienes en el arbol.
''' </summary>
''' <remarks>Llamada desde: tvwUndNegUsu_NodeCheck ; Tiempo m�ximo:0,1</remarks>
Private Sub LimpiaUnqaVista()
    Dim ConfVistas0 As CConfVistaCalGlobalVar
    Dim ConfVistas1 As CConfVistaCalGlobalVar
    Dim ConfVistas2 As CConfVistaCalGlobalVar
    Dim ConfVistas3 As CConfVistaCalGlobalVar
    Dim ConfVistas4 As CConfVistaCalGlobalVar
    Dim ConfVistas5 As CConfVistaCalGlobalVar
    
    Dim sTag As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each ConfVistas0 In m_oVistaCalSeleccionada.ConfVistasCalGlobalVar
        sTag = ConfVistas0.VarCal & "#####0"
        
        ModificarUnqaVista sTag, False, True
        
        For Each ConfVistas1 In ConfVistas0.VariblesCalVista
            sTag = ConfVistas1.VarCal & "#####1"
            
            ModificarUnqaVista sTag, False, True
            
            If Not ConfVistas1.VariblesCalVista Is Nothing Then
                For Each ConfVistas2 In ConfVistas1.VariblesCalVista
                    sTag = ConfVistas2.VarCal & "#" & ConfVistas1.VarCal & "####2"
                    
                    ModificarUnqaVista sTag, False, True
                                    
                    If Not ConfVistas2.VariblesCalVista Is Nothing Then
                        For Each ConfVistas3 In ConfVistas2.VariblesCalVista
                            sTag = ConfVistas3.VarCal & "#" & ConfVistas1.VarCal & "#" & ConfVistas2.VarCal & "###3"
                            
                            ModificarUnqaVista sTag, False, True
                                                            
                            If Not ConfVistas3.VariblesCalVista Is Nothing Then
                                For Each ConfVistas4 In ConfVistas3.VariblesCalVista
                                    sTag = ConfVistas4.VarCal & "#" & ConfVistas1.VarCal & "#" & ConfVistas2.VarCal & "#" & ConfVistas3.VarCal & "##4"
                                    
                                    ModificarUnqaVista sTag, False, True
                                                                        
                                    If Not ConfVistas4.VariblesCalVista Is Nothing Then
                                        For Each ConfVistas5 In ConfVistas4.VariblesCalVista
                                            sTag = ConfVistas5.VarCal & "#" & ConfVistas1.VarCal & "#" & ConfVistas2.VarCal & "#" & ConfVistas3.VarCal & "#" & ConfVistas4.VarCal & "#5"
                                            
                                            ModificarUnqaVista sTag, False, True
                                            
                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "LimpiaUnqaVista", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Dinamicamente se muestran/ocultan variables y unidades. Tras ocultar pueden quedar invisibles en
''' la comparativa variables q no cumplen con algun intervalo de aviso/restricci�n. Esta funci�n es
''' para mantener la colecci�n de dichas variables invisibles.
''' </summary>
''' <param name="psNivel">Nivel de la variable</param>
''' <param name="psId">ID de la variable</param>
''' <param name="psDen">Denominaci�n de la variable</param>
''' <param name="psIdUnqa">ID de la unidad de negocio</param>
''' <param name="psCodUnqa">Cod de la unidad de negocio</param>
''' <param name="psUnqa">Denominaci�n de la unidad de negocio</param>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve_Nivel0     Rellenar_sdbgVarsQAProve_Nivel1
'''     Rellenar_sdbgVarsQAProve_Nivel2     Rellenar_sdbgVarsQAProve_Nivel3     Rellenar_sdbgVarsQAProve_Nivel4
'''     Rellenar_sdbgVarsQAProve_Nivel5; Tiempo m�ximo:0,1</remarks>
Private Sub MeteEnColeccionNoVisibles(ByVal psNivel As String, ByVal psId As String, ByVal psDen As String, ByVal psIdUnqa As String, ByVal psCodUnqa As String, ByVal psUnqa As String)
    Dim bEncontrado As Boolean
    Dim iPos As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bEncontrado = False
    For iPos = LBound(m_sVarsNoVisible) To UBound(m_sVarsNoVisible)
        If m_sVarsNoVisible(iPos) = (psDen & "#" & psId & "#" & psNivel) Then
            bEncontrado = True
            Exit For
        End If
    Next iPos
    
    If Not bEncontrado Then
        ReDim Preserve m_sVarsNoVisible(UBound(m_sVarsNoVisible) + 1)
        m_sVarsNoVisible(UBound(m_sVarsNoVisible)) = psDen & "#" & psId & "#" & psNivel
    End If
    
    bEncontrado = False
    For iPos = LBound(m_sVarsUnqaNoVisible) To UBound(m_sVarsUnqaNoVisible)
        If m_sVarsUnqaNoVisible(iPos) = (psUnqa & "#" & psIdUnqa & "#" & psId & "#" & psNivel & "#" & psCodUnqa) Then
            bEncontrado = True
            Exit For
        End If
    Next iPos
    
    If Not bEncontrado Then
        ReDim Preserve m_sVarsUnqaNoVisible(UBound(m_sVarsUnqaNoVisible) + 1)
        m_sVarsUnqaNoVisible(UBound(m_sVarsUnqaNoVisible)) = psUnqa & "#" & psIdUnqa & "#" & psId & "#" & psNivel & "#" & psCodUnqa
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "MeteEnColeccionNoVisibles", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Crea un arbol con los variables de calidad accesibles (restricci�n a solo material de proceso)
''' </summary>
''' <remarks>Llamada desde: chkMostrarVarsMaterialProceso_Click     sdbgProveVarsQA_GrpHeadClick
'''     sdbgVarsQAProve_GrpHeadClick; Tiempo m�ximo: 0,1</remarks>
Private Sub GenerarEstructuraVarCal()
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String

    Dim nodx As MSComctlLib.node
    
    Dim bVisibleVista As Boolean
    Dim bOcultoPorMat As Boolean
    
    Dim bTodosCheck As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bTodosCheck = True

    m_bDeshabilidar_Chk_ChangeTodas = True
    Me.chkMostrarVarsMaterialProceso.Value = IIf(m_oVistaCalSeleccionada.Materiales_Proceso, 1, 0)
    m_bDeshabilidar_Chk_ChangeTodas = False

    Screen.MousePointer = vbHourglass

    tvwVarCal.Visible = True

    tvwVarCal.Nodes.clear

    If Not m_oVarsCalidad Is Nothing Then
        For Each m_oVarCal1 In m_oVarsCalidad
        
            bOcultoPorMat = False
            
            If (m_oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) Then
                If m_oVarCal1.Nivel = 0 Then
                    Set nodx = tvwVarCal.Nodes.Add(, , "NIV0" & CStr(m_oVarCal1.Id), m_oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                    
                    nodx.Tag = m_oVarCal1.Id & "#####0"
                    
                    scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_oVarCal1.Id)
                    
                    bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
                    
                    If bVisibleVista Then
                        nodx.Checked = True
                    Else
                        nodx.Checked = False
                        bTodosCheck = False
                    End If
                    
                    nodx.EnsureVisible
                    nodx.Expanded = True
                    
                Else
                    'La de nivel 0 siempre tiene el usuario permiso de consulta-> las de nivel 1 siempre tienen un padre
                    scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(m_oVarCal1.Id)
                                    
                    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                        If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal1.MaterialProceso Then
                            bOcultoPorMat = True
                        Else
                            bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).Visible
                        End If
                    Else
                        bVisibleVista = False
                    End If
                         
                    If bOcultoPorMat = False Then
                        Set nodx = tvwVarCal.Nodes.Add("NIV0" & CStr(m_lIdNivel0), tvwChild, "NIV1" & CStr(m_oVarCal1.Id), m_oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                    
                        nodx.Tag = m_oVarCal1.Id & "#####1"
                        
                        If bVisibleVista Then
                            nodx.Checked = True
                        Else
                            nodx.Checked = False
                            bTodosCheck = False
                        End If
                        
                        nodx.EnsureVisible
                        nodx.Expanded = True
                    End If
                End If
            End If
                                            
            If m_oVarCal1.Nivel > 0 Then
                If Not m_oVarCal1.VariblesCal Is Nothing Then
                    If (m_oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) _
                     And bOcultoPorMat = False Then
                        For Each m_oVarCal2 In m_oVarCal1.VariblesCal
                        
                            bOcultoPorMat = False
                                                    
                            scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(m_oVarCal2.Id)
                                                    
                            If (m_oVarCal2.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) Then
                                If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2) Is Nothing Then
                                    With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                        If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal2.MaterialProceso Then
                                            bOcultoPorMat = True
                                        Else
                                            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                bVisibleVista = .VariblesCalVista.Item(scod2).Visible
                                            Else
                                                bVisibleVista = False
                                            End If
                                        End If
                                    End With
                                Else
                                    bVisibleVista = False
                                End If
                                
                                If bOcultoPorMat = False Then
                                    Set nodx = tvwVarCal.Nodes.Add("NIV1" & CStr(m_oVarCal1.Id), tvwChild, "NIV2" & CStr(m_oVarCal2.Id), m_oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                    
                                    nodx.Tag = m_oVarCal2.Id & "#" & m_oVarCal1.Id & "####2"
                                    
                                    If bVisibleVista Then
                                        nodx.Checked = True
                                    Else
                                        nodx.Checked = False
                                        bTodosCheck = False
                                    End If
                                    
                                    nodx.EnsureVisible
                                    nodx.Expanded = True
                                End If
                                                    
                                If Not m_oVarCal2.VariblesCal Is Nothing And bOcultoPorMat = False Then
                                    For Each m_oVarCal3 In m_oVarCal2.VariblesCal
                                        bOcultoPorMat = False
                                                                        
                                        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(m_oVarCal3.Id)
                                      
                                        If (m_oVarCal3.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) Then
                                            If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3) Is Nothing Then
                                                With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                                    If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                        With .VariblesCalVista.Item(scod2)
                                                            If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal3.MaterialProceso Then
                                                                bOcultoPorMat = True
                                                            Else
                                                                If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                                    bVisibleVista = .VariblesCalVista.Item(scod3).Visible
                                                                Else
                                                                    bVisibleVista = False
                                                                End If
                                                            End If
                                                        End With
                                                    Else
                                                        bVisibleVista = False
                                                    End If
                                                End With
                                            Else
                                                bVisibleVista = False
                                            End If
                                        
                                            If bOcultoPorMat = False Then
                                                Set nodx = tvwVarCal.Nodes.Add("NIV2" & CStr(m_oVarCal2.Id), tvwChild, "NIV3" & CStr(m_oVarCal3.Id), m_oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                
                                                nodx.Tag = m_oVarCal3.Id & "#" & m_oVarCal1.Id & "#" & m_oVarCal2.Id & "###3"
                                                                                            
                                                If bVisibleVista Then
                                                    nodx.Checked = True
                                                Else
                                                    nodx.Checked = False
                                                    bTodosCheck = False
                                                End If
                                                
                                                nodx.EnsureVisible
                                                nodx.Expanded = True
                                            End If
                                
                                            If Not m_oVarCal3.VariblesCal Is Nothing And bOcultoPorMat = False Then
                                                For Each m_oVarCal4 In m_oVarCal3.VariblesCal
            
                                                    bOcultoPorMat = False
                                                    
                                                    scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(m_oVarCal4.Id)
                                                    
                                                    If (m_oVarCal4.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) Then
                                                        If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                                                            With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                                                If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                                    With .VariblesCalVista.Item(scod2)
                                                                        If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                                            With .VariblesCalVista.Item(scod3)
                                                                                If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal4.MaterialProceso Then
                                                                                    bOcultoPorMat = True
                                                                                Else
                                                                                    If Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                                                                        bVisibleVista = .VariblesCalVista.Item(scod4).Visible
                                                                                    Else
                                                                                        bVisibleVista = False
                                                                                    End If
                                                                                End If
                                                                            End With
                                                                        Else
                                                                            bVisibleVista = False
                                                                        End If
                                                                    End With
                                                                Else
                                                                    bVisibleVista = False
                                                                End If
                                                            End With
                                                        Else
                                                            bVisibleVista = False
                                                        End If
    
                                                        If bOcultoPorMat = False Then
                                                            Set nodx = tvwVarCal.Nodes.Add("NIV3" & CStr(m_oVarCal3.Id), tvwChild, "NIV4" & CStr(m_oVarCal4.Id), m_oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                            
                                                            nodx.Tag = m_oVarCal4.Id & "#" & m_oVarCal1.Id & "#" & m_oVarCal2.Id & "#" & m_oVarCal3.Id & "##4"
                                                            
                                                            If bVisibleVista Then
                                                                nodx.Checked = True
                                                            Else
                                                                nodx.Checked = False
                                                                bTodosCheck = False
                                                            End If
                                                            
                                                            nodx.EnsureVisible
                                                            nodx.Expanded = True
                                                        End If
                                                                
                                                        If Not m_oVarCal4.VariblesCal Is Nothing And bOcultoPorMat = False Then
                                                            For Each m_oVarCal5 In m_oVarCal4.VariblesCal
                                                                bOcultoPorMat = False
                                                                
                                                                scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & CStr(m_oVarCal5.Id)
                                                                                        
                                                                If (m_oVarCal5.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) Then
                                                                    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                                                                        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                                                            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                                                With .VariblesCalVista.Item(scod2)
                                                                                    If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                                                        With .VariblesCalVista.Item(scod3)
                                                                                            If Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                                                                                With .VariblesCalVista.Item(scod4)
                                                                                                    If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal5.MaterialProceso Then
                                                                                                        bOcultoPorMat = True
                                                                                                    Else
                                                                                                        If Not .VariblesCalVista.Item(scod5) Is Nothing Then
                                                                                                            bVisibleVista = .VariblesCalVista.Item(scod5).Visible
                                                                                                        Else
                                                                                                            bVisibleVista = False
                                                                                                        End If
                                                                                                    End If
                                                                                                End With
                                                                                                
                                                                                            Else
                                                                                                bVisibleVista = False
                                                                                            End If
                                                                                        End With
                                                                                    Else
                                                                                        bVisibleVista = False
                                                                                    End If
                                                                                End With
                                                                            Else
                                                                                bVisibleVista = False
                                                                            End If
                                                                        End With
                                                                    Else
                                                                        bVisibleVista = False
                                                                    End If
        
                                                                    If bOcultoPorMat = False Then
                                                                        Set nodx = tvwVarCal.Nodes.Add("NIV4" & CStr(m_oVarCal4.Id), tvwChild, "NIV5" & CStr(m_oVarCal5.Id), m_oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                                                                    
                                                                        nodx.Tag = m_oVarCal5.Id & "#" & m_oVarCal1.Id & "#" & m_oVarCal2.Id & "#" & m_oVarCal3.Id & "#" & m_oVarCal4.Id & "#5"
                                                                    
                                                                        If bVisibleVista Then
                                                                            nodx.Checked = True
                                                                        Else
                                                                            nodx.Checked = False
                                                                            bTodosCheck = False
                                                                        End If
                                                                        
                                                                        nodx.EnsureVisible
                                                                        nodx.Expanded = True
                                                                    End If
                                                                End If 'If (m_oVarCal5.UsuConsultar
                                                            Next 'For Each m_oVarCal5
                                                        End If 'If Not m_oVarCal4.VariblesCal
                                                    End If 'If (m_oVarCal4.UsuConsultar
                                                Next 'For Each m_oVarCal4
                                            End If 'If Not m_oVarCal3.VariblesCal
                                        End If 'If (m_oVarCal3.UsuConsultar
                                    Next 'For Each m_oVarCal3
                                End If 'If Not m_oVarCal2.VariblesCal
                            End If 'If (m_oVarCal2.UsuConsultar
                        Next 'For Each m_oVarCal2
                    End If 'If (m_oVarCal1.UsuConsultar
                End If 'If Not m_oVarCal1.VariblesCal
            End If 'If m_oVarCal1.Nivel > 0
        Next 'For Each m_oVarCal1
    End If 'If Not m_oVarsCalidad
    
    m_bDeshabilidar_Chk_ChangeTodas = True
    If bTodosCheck Then
        Me.chkTodasVariables.Value = vbChecked
    Else
        Me.chkTodasVariables.Value = vbUnchecked
    End If
    
    For Each nodx In Me.tvwVarCal.Nodes
        Me.tvwVarCal.selectedItem = nodx
        Exit For
    Next
    Me.tvwVarCal.selectedItem = Nothing
    
    m_bDeshabilidar_Chk_ChangeTodas = False
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "GenerarEstructuraVarCal", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
''' <summary>
''' Cuando se cliquee en el texto de un nodo debe chequearse/deschequearse el nodo
''' </summary>
''' <param name="node">nodo cliqueado</param>
''' <remarks>Llamada desde:Evento que salta al hacer click en un nodo del arbol ; Tiempo m�ximo: 0</remarks>
Private Sub tvwUndNegUsu_NodeClick(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    node.Checked = Not node.Checked
    
    tvwUndNegUsu_NodeCheck node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwUndNegUsu_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
''' <summary>
''' Al recibir el foco el arbol se le da el foco al pic
''' </summary>
''' <remarks>Llamada desde:Evento que salta al recibir el foco el arbol; Tiempo m�ximo:0</remarks>
Private Sub tvwVarCal_GotFocus()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Visible Then Me.picVarCal.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwVarCal_GotFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
''' <summary>
''' Cuando se chequea/deschequea una variable, se indica q la variable debe mostrarse/ocultarse en los grids
''' </summary>
''' <param name="node">nodo cliqueado</param>
''' <remarks>Llamada desde:Evento que salta al hacer click en el check de un nodo del arbol; Tiempo m�ximo: 0</remarks>
Private Sub tvwVarCal_NodeCheck(ByVal node As MSComctlLib.node)
    
    Dim sDatos_Variable() As String
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bVistaCambiaVarCal = True
    
    m_oVistaCalSeleccionada.HayCambios = True
    
    sDatos_Variable = Split(node.Tag, "#")
    
    Select Case sDatos_Variable(POS_NIVEL)
    Case 0
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & sDatos_Variable(POS_ID)

        m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible = node.Checked
    Case 1
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID)

        m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).Visible = node.Checked
    Case 2
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID)

        m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).Visible = node.Checked
    Case 3
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID)

        m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).Visible = node.Checked
    Case 4
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID_VARCAL3)
        scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & sDatos_Variable(POS_ID)

        m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).Visible = node.Checked
    Case 5
        scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
        scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
        scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
        scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID_VARCAL3)
        scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & sDatos_Variable(POS_ID_VARCAL4)
        scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & sDatos_Variable(POS_ID)

        m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).Visible = node.Checked
    End Select
    
    Me.tvwVarCal.selectedItem = node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwVarCal_NodeCheck", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Cuando se cliquee en el texto de un nodo debe chequearse/deschequearse el nodo
''' </summary>
''' <param name="node">nodo cliqueado</param>
''' <remarks>Llamada desde:Evento que salta al hacer click en un nodo del arbol ; Tiempo m�ximo: 0</remarks>
Private Sub tvwVarCal_NodeClick(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    node.Checked = Not node.Checked
    
    tvwVarCal_NodeCheck node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "tvwVarCal_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Determina si una unidad dada ya esta incluida o no en la colecci�n "existe alguna variable con puntos en la unidad"
''' </summary>
''' <param name="IdUnqa">ID de la unidad de negocio</param>
''' <returns>Si esta incluida o no </returns>
''' <remarks>Llamada desde: Rellenar_sdbgVarsQAProve    Rellenar_sdbgVarsQAProve_Nivel0
'''     Rellenar_sdbgVarsQAProve_Nivel1     Rellenar_sdbgVarsQAProve_Nivel2     Rellenar_sdbgVarsQAProve_Nivel3
'''     Rellenar_sdbgVarsQAProve_Nivel4     Rellenar_sdbgVarsQAProve_Nivel5     GrpGenerarEstructuraUnidadesNegocio
'''     ; Tiempo m�ximo: 0</remarks>
Private Function AlgunaUnqa(ByVal IdUnqa As Long) As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AlgunaUnqa = IIf(Not (m_oUnqaEnProve.Item(CStr(IdUnqa)) Is Nothing), True, False)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "AlgunaUnqa", err, Erl, , m_bActivado)
        Exit Function
    End If
    
End Function

''' <summary>
''' Determina si para un proveedor concreto y para una variable concreta y unidad concreta la variable tiene puntos
''' </summary>
''' <param name="IdUnqa">ID de la unidad de negocio</param>
''' <param name="IdVarcal">ID de la variable de calidad</param>
''' <param name="Nivel">Nivel de la variable de calidad</param>
''' <param name="Prove">Proveedor</param>
''' <returns>Si tiene puntos o no</returns>
''' <remarks>Llamada desde: Rellenar_sdbgProveVarsQA_Unqa          Rellenar_sdbgVarsQAProve_Nivel0
'''     Rellenar_sdbgVarsQAProve_Nivel1     Rellenar_sdbgVarsQAProve_Nivel2     Rellenar_sdbgVarsQAProve_Nivel3
'''     Rellenar_sdbgVarsQAProve_Nivel4     Rellenar_sdbgVarsQAProve_Nivel5; Tiempo m�ximo: 0</remarks>
Private Function ExisteUnqaVarcalProve(ByVal IdUnqa As Long, ByVal IdVarcal As Long, ByVal Nivel As Long, ByVal Prove As String) As Boolean
    Dim sProveUnqaVarcal As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sProveUnqaVarcal = CStr(Nivel) & CStr(IdVarcal) & "#" & CStr(IdUnqa) & "#" & Prove

    ExisteUnqaVarcalProve = Not (m_oUnqaVarcalEnProve.Item(sProveUnqaVarcal) Is Nothing)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "ExisteUnqaVarcalProve", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function
''' <summary>
''' Determina si entre los proveedores visibles hay alguno con puntos para una variable y unidad concreta. Tambien
''' puede hacer lo mismo obviando la visibilidad de los proveedores
''' </summary>
''' <param name="IdUnqa">ID de la unidad de negocio</param>
''' <param name="IdVarcal">ID de la variable de calidad</param>
''' <param name="Nivel">Nivel de la variable de calidad</param>
''' <param name="CtrlVisibleEnGrafico">obviar o no la visibilidad de los proveedores</param>
''' <returns>Si lo hay o no</returns>
''' <remarks>Llamada desde: sdbgProveVarsQA_RowColChange    sdbgVarsQAProve_RowLoaded   sdbgVarsQAProve_RowColChange
'''     sdbgVarsQAProve_RowLoaded   CellGenerarEstructuraUnidadesNegocio    ; Tiempo m�ximo:0</remarks>
Private Function HayUnqaVarcalAlgunProve(ByVal IdUnqa As Long, ByVal IdVarcal As Long, ByVal Nivel As Integer, ByVal CtrlVisibleEnGrafico As Boolean) As Boolean
    Dim oProve As CProveedor
    Dim sProveUnqaVarcal As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    HayUnqaVarcalAlgunProve = False
    
    For Each oProve In m_oProvesAsig
        If Not m_oProvesAsig.Item(oProve.Cod).VisibleEnGrafico Then
            If Not CtrlVisibleEnGrafico Then
                HayUnqaVarcalAlgunProve = ExisteUnqaVarcalProve(IdUnqa, IdVarcal, Nivel, CStr(oProve.Cod))
                If HayUnqaVarcalAlgunProve Then Exit For
            End If
        Else
            sProveUnqaVarcal = CStr(Nivel) & CStr(IdVarcal) & "#" & CStr(IdUnqa) & "#" & CStr(oProve.Cod)
        
            HayUnqaVarcalAlgunProve = Not (m_oUnqaVarcalEnProve.Item(sProveUnqaVarcal) Is Nothing)
                
            If HayUnqaVarcalAlgunProve Then Exit For

        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "HayUnqaVarcalAlgunProve", err, Erl, , m_bActivado)
        Exit Function
    End If
    
End Function
''' <summary>
''' Crea un arbol con los proveedores
''' </summary>
''' <remarks>Llamada desde: sdbgProveVarsQA_GrpHeadClick        sdbgVarsQAProve_GrpHeadClick; Tiempo m�ximo: 0,1</remarks>
Private Sub GenerarEstructuraProves()
    Dim oProve As CProveedor
    Dim nodx As MSComctlLib.node
    Dim bTodosCheck As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    tvwProves.Visible = True

    tvwProves.Nodes.clear
    
    bTodosCheck = True
    
    If Not m_oProvesAsig Is Nothing Then
        For Each oProve In m_oProvesAsig
            Set nodx = tvwProves.Nodes.Add(, , "PR" & CStr(oProve.Cod), oProve.Den)
            
            nodx.Tag = oProve.Cod
            
            nodx.Checked = m_oProvesAsig.Item(oProve.Cod).VisibleEnGrafico
            
            If nodx.Checked = False Then bTodosCheck = False
            
            nodx.EnsureVisible
        Next
    End If

    m_bDeshabilidar_Chk_ChangeTodas = True
    If bTodosCheck Then
        Me.chkTodosProves.Value = vbChecked
    Else
        Me.chkTodosProves.Value = vbUnchecked
    End If
    For Each nodx In Me.tvwProves.Nodes
        Me.tvwProves.selectedItem = nodx
        Exit For
    Next
    Me.tvwProves.selectedItem = Nothing
    m_bDeshabilidar_Chk_ChangeTodas = False
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "GenerarEstructuraProves", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
''' <summary>
''' Ocultar dinamicamente los proveedores en el grid VarsProve
''' </summary>
''' <remarks>Llamada desde:Form_MouseMove       sdbgProveVarsQA_MouseMove       sdbgVarsQAProve_MouseMove; Tiempo m�ximo:0,3</remarks>
Private Sub OcultarMostrarProveedores()
    Dim iGrupos As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    LockWindowUpdate frmComparativaQA.hWnd
    Screen.MousePointer = vbHourglass
    
    If Not m_oProvesAsig Is Nothing Then
        For iGrupos = 2 To sdbgVarsQAProve.Groups.Count - 1
            sdbgVarsQAProve.Groups(iGrupos).Visible = m_oProvesAsig.Item(sdbgVarsQAProve.Groups(iGrupos).TagVariant).VisibleEnGrafico
        Next
    End If
    
    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "OcultarMostrarProveedores", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Rellena el grid con las variables de calidad por proveedor y unidad de negocio. Se tiene en cuenta configuraci�n
''' de la vista y visibilidades "desligadas de la vista" de unidades y visibilidades de proveedores.
''' </summary>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="oProv">Objeto proveedor</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <remarks>Llamada desde: Rellenar_sdbgProveVarsQA; Tiempo m�ximo:0,3</remarks>
Private Sub Rellenar_sdbgProveVarsQA_Unqa(ByVal oUnidadUnqa As CUnidadNegQA, ByVal oProv As CProveedor, ByVal Nivel As Integer)
    Dim sFilaGrid As String
    Dim oVarCalif As CVarCalificacion
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    Dim bEsta As Boolean
    Dim bMostradaUnidadProve As Boolean
    Dim sCalif As String
       
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Set oVarCalif = oFSGSRaiz.Generar_CVarCalificacion

    sFilaGrid = ""
    sFilaGrid = oProv.Cod & Chr(m_lSeparador) & oProv.Den
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oUnidadUnqa.Cod & " - " & oUnidadUnqa.Den & Chr(m_lSeparador) & ""

    'Lines a ser incluida o no en funci�n de todo lo dicho
    bEsta = False
    'visibilidades "desligadas de la vista" de unidades: MostradaUnidadProve(oProv.Cod & "#" & oUnidadUnqa.Id)
    bMostradaUnidadProve = MostradaUnidadProve(oProv.Cod & "#" & oUnidadUnqa.Id)
    'configuraci�n de la vista unidades: m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas
    'configuraci�n de la vista variables: m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
    
    If Not m_oVarsCalidad Is Nothing Then
        For Each m_oVarCal1 In m_oVarsCalidad
            If m_oVarCal1.Nivel = 0 Then
                scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_oVarCal1.Id)
                
                If Not bEsta Then
                    If (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas, "," & oUnidadUnqa.Id & ",") > 0) _
                    Or bMostradaUnidadProve Then
                        If ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal1.Id, 0, oProv.Cod) Then
                            bEsta = True And m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
                        End If
                    End If
                End If
            Else
                scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(m_oVarCal1.Id)
                
                If Not bEsta Then
                    If (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas, "," & oUnidadUnqa.Id & ",") > 0) _
                    Or bMostradaUnidadProve Then
                        If ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal1.Id, 1, oProv.Cod) Then
                            bEsta = True And m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).Visible
                        End If
                    End If
                End If
            End If
        
            If oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal1.Id & "#" & m_oVarCal1.IdVarCal1
                sFilaGrid = sFilaGrid & "#" & m_oVarCal1.IdVarCal2 & "#" & m_oVarCal1.IdVarCal3
                sFilaGrid = sFilaGrid & "#" & m_oVarCal1.IdVarCal4 & "#" & m_oVarCal1.Nivel
                sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal1.RestriccionSiNo) & "#" & m_oVarCal1.RestriccionInf
                sFilaGrid = sFilaGrid & "#" & m_oVarCal1.RestriccionSup & "#" & CInt(m_oVarCal1.AvisoSiNo)
                sFilaGrid = sFilaGrid & "#" & m_oVarCal1.AvisoInf & "#" & m_oVarCal1.AvisoSup
                sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                sFilaGrid = sFilaGrid & "#" & m_oVarCal1.Tipo & "#" & m_oVarCal1.Subtipo
                sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                If m_oVarCal1.Nivel = 0 Then
                Else
                    sFilaGrid = sFilaGrid & CompletarRestoNiveles(m_oVarCal1, oUnidadUnqa, Nivel)
                End If
                
            Else
                With oProv.VariablesCalidad.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id) & "#" & oUnidadUnqa.Id)
                    If .provemat = 1 Then
                        sCalif = ""
                        If Not IsNull(.Puntuacion) Then sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                    Else
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    End If
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal1.Id & "#" & m_oVarCal1.IdVarCal1
                    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.IdVarCal2 & "#" & m_oVarCal1.IdVarCal3
                    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.IdVarCal4 & "#" & m_oVarCal1.Nivel
                    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal1.RestriccionSiNo) & "#" & m_oVarCal1.RestriccionInf
                    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.RestriccionSup & "#" & CInt(m_oVarCal1.AvisoSiNo)
                    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.AvisoInf & "#" & m_oVarCal1.AvisoSup
                    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                    sFilaGrid = sFilaGrid & "#" & m_oVarCal1.Tipo & "#" & m_oVarCal1.Subtipo
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                    If Not m_oVarCal1.VariblesCal Is Nothing And m_oVarCal1.Nivel <> 0 Then
                        For Each m_oVarCal2 In m_oVarCal1.VariblesCal
                            scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(m_oVarCal2.Id)
                            
                            If Not bEsta Then
                                If (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas, "," & oUnidadUnqa.Id & ",") > 0) _
                                Or bMostradaUnidadProve Then
                                    If ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal2.Id, 2, oProv.Cod) Then
                                        bEsta = True And m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).Visible
                                    End If
                                End If
                            End If
                            
                            If .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal2.Id & "#" & m_oVarCal2.IdVarCal1
                                sFilaGrid = sFilaGrid & "#" & m_oVarCal2.IdVarCal2 & "#" & m_oVarCal2.IdVarCal3
                                sFilaGrid = sFilaGrid & "#" & m_oVarCal2.IdVarCal4 & "#" & m_oVarCal2.Nivel
                                sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal2.RestriccionSiNo) & "#" & m_oVarCal2.RestriccionInf
                                sFilaGrid = sFilaGrid & "#" & m_oVarCal2.RestriccionSup & "#" & CInt(m_oVarCal2.AvisoSiNo)
                                sFilaGrid = sFilaGrid & "#" & m_oVarCal2.AvisoInf & "#" & m_oVarCal2.AvisoSup
                                sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                sFilaGrid = sFilaGrid & "#" & m_oVarCal2.Tipo & "#" & m_oVarCal2.Subtipo
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                sFilaGrid = sFilaGrid & CompletarRestoNiveles(m_oVarCal2, oUnidadUnqa, Nivel)
                            Else
                                With .VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id) & "#" & oUnidadUnqa.Id)
                                    If .provemat = 1 Then
                                        sCalif = ""
                                        If Not IsNull(.Puntuacion) Then sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                                    Else
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                    End If
                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal2.Id & "#" & m_oVarCal2.IdVarCal1
                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.IdVarCal2 & "#" & m_oVarCal2.IdVarCal3
                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.IdVarCal4 & "#" & m_oVarCal2.Nivel
                                    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal2.RestriccionSiNo) & "#" & m_oVarCal2.RestriccionInf
                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.RestriccionSup & "#" & CInt(m_oVarCal2.AvisoSiNo)
                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.AvisoInf & "#" & m_oVarCal2.AvisoSup
                                    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal2.Tipo & "#" & m_oVarCal2.Subtipo
                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                    If Not m_oVarCal2.VariblesCal Is Nothing Then
                                        For Each m_oVarCal3 In m_oVarCal2.VariblesCal
                                            scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(m_oVarCal3.Id)
                                            
                                            If Not bEsta Then
                                                If (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas, "," & oUnidadUnqa.Id & ",") > 0) _
                                                Or bMostradaUnidadProve Then
                                                    If ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal3.Id, 3, oProv.Cod) Then
                                                        bEsta = True And m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).Visible
                                                    End If
                                                End If
                                            End If
                                            
                                            If .VariblesCal Is Nothing Then
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal3.Id & "#" & m_oVarCal3.IdVarCal1
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.IdVarCal2 & "#" & m_oVarCal3.IdVarCal3
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.IdVarCal4 & "#" & m_oVarCal3.Nivel
                                                sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal3.RestriccionSiNo) & "#" & m_oVarCal3.RestriccionInf
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.RestriccionSup & "#" & CInt(m_oVarCal3.AvisoSiNo)
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.AvisoInf & "#" & m_oVarCal3.AvisoSup
                                                sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.Tipo & "#" & m_oVarCal3.Subtipo
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                sFilaGrid = sFilaGrid & CompletarRestoNiveles(m_oVarCal3, oUnidadUnqa, Nivel)
                                            ElseIf .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal3.Id & "#" & m_oVarCal3.IdVarCal1
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.IdVarCal2 & "#" & m_oVarCal3.IdVarCal3
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.IdVarCal4 & "#" & m_oVarCal3.Nivel
                                                sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal3.RestriccionSiNo) & "#" & m_oVarCal3.RestriccionInf
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.RestriccionSup & "#" & CInt(m_oVarCal3.AvisoSiNo)
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.AvisoInf & "#" & m_oVarCal3.AvisoSup
                                                sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal3.Tipo & "#" & m_oVarCal3.Subtipo
                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                sFilaGrid = sFilaGrid & CompletarRestoNiveles(m_oVarCal3, oUnidadUnqa, Nivel)
                                            Else
                                                With .VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id) & "#" & oUnidadUnqa.Id)
                                                    If .provemat = 1 Then
                                                        sCalif = ""
                                                        If Not IsNull(.Puntuacion) Then sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                                                    Else
                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                    End If
                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal3.Id & "#" & m_oVarCal3.IdVarCal1
                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.IdVarCal2 & "#" & m_oVarCal3.IdVarCal3
                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.IdVarCal4 & "#" & m_oVarCal3.Nivel
                                                    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal3.RestriccionSiNo) & "#" & m_oVarCal3.RestriccionInf
                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.RestriccionSup & "#" & CInt(m_oVarCal3.AvisoSiNo)
                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.AvisoInf & "#" & m_oVarCal3.AvisoSup
                                                    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal3.Tipo & "#" & m_oVarCal3.Subtipo
                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                    If Not m_oVarCal3.VariblesCal Is Nothing Then
                                                        For Each m_oVarCal4 In m_oVarCal3.VariblesCal
                                                            scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(m_oVarCal4.Id)
                                            
                                                            If Not bEsta Then
                                                                If (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas, "," & oUnidadUnqa.Id & ",") > 0) _
                                                                Or bMostradaUnidadProve Then
                                                                    If ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal4.Id, 4, oProv.Cod) Then
                                                                        bEsta = True And m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).Visible
                                                                    End If
                                                                End If
                                                            End If
                                                            
                                                            If .VariblesCal Is Nothing Then
                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal4.Id & "#" & m_oVarCal4.IdVarCal1
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.IdVarCal2 & "#" & m_oVarCal4.IdVarCal3
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.IdVarCal4 & "#" & m_oVarCal4.Nivel
                                                                sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal4.RestriccionSiNo) & "#" & m_oVarCal4.RestriccionInf
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.RestriccionSup & "#" & CInt(m_oVarCal4.AvisoSiNo)
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.AvisoInf & "#" & m_oVarCal4.AvisoSup
                                                                sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                                sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.Tipo & "#" & m_oVarCal4.Subtipo
                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                                sFilaGrid = sFilaGrid & CompletarRestoNiveles(m_oVarCal4, oUnidadUnqa, Nivel)
                                                            ElseIf .VariblesCal.Item(CStr(m_oVarCal4.Nivel) & CStr(m_oVarCal4.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal4.Id & "#" & m_oVarCal4.IdVarCal1
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.IdVarCal2 & "#" & m_oVarCal4.IdVarCal3
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.IdVarCal4 & "#" & m_oVarCal4.Nivel
                                                                sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal4.RestriccionSiNo) & "#" & m_oVarCal4.RestriccionInf
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.RestriccionSup & "#" & CInt(m_oVarCal4.AvisoSiNo)
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.AvisoInf & "#" & m_oVarCal4.AvisoSup
                                                                sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                                sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal4.Tipo & "#" & m_oVarCal4.Subtipo
                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                                sFilaGrid = sFilaGrid & CompletarRestoNiveles(m_oVarCal4, oUnidadUnqa, Nivel)
                                                            Else
                                                                With .VariblesCal.Item(CStr(m_oVarCal4.Nivel) & CStr(m_oVarCal4.Id) & "#" & oUnidadUnqa.Id)
                                                                    If .provemat = 1 Then
                                                                        sCalif = ""
                                                                        If Not IsNull(.Puntuacion) Then sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                                                                    Else
                                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                    End If
                                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal4.Id & "#" & m_oVarCal4.IdVarCal1
                                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.IdVarCal2 & "#" & m_oVarCal4.IdVarCal3
                                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.IdVarCal4 & "#" & m_oVarCal4.Nivel
                                                                    sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal4.RestriccionSiNo) & "#" & m_oVarCal4.RestriccionInf
                                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.RestriccionSup & "#" & CInt(m_oVarCal4.AvisoSiNo)
                                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.AvisoInf & "#" & m_oVarCal4.AvisoSup
                                                                    sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                                    sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                                    sFilaGrid = sFilaGrid & "#" & m_oVarCal4.Tipo & "#" & m_oVarCal4.Subtipo
                                                                    sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                                    If Not m_oVarCal4.VariblesCal Is Nothing Then
                                                                        For Each m_oVarCal5 In m_oVarCal4.VariblesCal
                                                                            scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & CStr(m_oVarCal5.Id)
                                                            
                                                                            If Not bEsta Then
                                                                                If (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas, "," & oUnidadUnqa.Id & ",") > 0) _
                                                                                Or bMostradaUnidadProve Then
                                                                                    If ExisteUnqaVarcalProve(oUnidadUnqa.Id, m_oVarCal5.Id, 5, oProv.Cod) Then
                                                                                        bEsta = True And m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).Visible
                                                                                    End If
                                                                                End If
                                                                            End If
                                                                        
                                                                            If .VariblesCal Is Nothing Then
                                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal5.Id & "#" & m_oVarCal5.IdVarCal1
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.IdVarCal2 & "#" & m_oVarCal5.IdVarCal3
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.IdVarCal4 & "#" & m_oVarCal5.Nivel
                                                                                sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal5.RestriccionSiNo) & "#" & m_oVarCal5.RestriccionInf
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.RestriccionSup & "#" & CInt(m_oVarCal5.AvisoSiNo)
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.AvisoInf & "#" & m_oVarCal5.AvisoSup
                                                                                sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                                                sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.Tipo & "#" & m_oVarCal5.Subtipo
                                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                                            ElseIf .VariblesCal.Item(CStr(m_oVarCal5.Nivel) & CStr(m_oVarCal5.Id) & "#" & oUnidadUnqa.Id) Is Nothing Then
                                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal5.Id & "#" & m_oVarCal5.IdVarCal1
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.IdVarCal2 & "#" & m_oVarCal5.IdVarCal3
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.IdVarCal4 & "#" & m_oVarCal5.Nivel
                                                                                sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal5.RestriccionSiNo) & "#" & m_oVarCal5.RestriccionInf
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.RestriccionSup & "#" & CInt(m_oVarCal5.AvisoSiNo)
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.AvisoInf & "#" & m_oVarCal5.AvisoSup
                                                                                sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                                                sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                                                sFilaGrid = sFilaGrid & "#" & m_oVarCal5.Tipo & "#" & m_oVarCal5.Subtipo
                                                                                sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                                            Else
                                                                                With .VariblesCal.Item(CStr(m_oVarCal5.Nivel) & CStr(m_oVarCal5.Id) & "#" & oUnidadUnqa.Id)
                                                                                    If .provemat = 1 Then
                                                                                        sCalif = ""
                                                                                        If Not IsNull(.Puntuacion) Then sCalif = oVarCalif.CargarCalificacion(.Id, .Nivel, .Puntuacion)
                                                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCalif & Chr(m_lSeparador) & NullToStr(.Puntuacion)
                                                                                    Else
                                                                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                                                                                    End If
                                                                                End With    '.VariblesCal.Item(CStr(m_oVarCal5.Nivel) & CStr(m_oVarCal5.Id))
                                                                            End If
                                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_oVarCal5.Id & "#" & m_oVarCal5.IdVarCal1
                                                                            sFilaGrid = sFilaGrid & "#" & m_oVarCal5.IdVarCal2 & "#" & m_oVarCal5.IdVarCal3
                                                                            sFilaGrid = sFilaGrid & "#" & m_oVarCal5.IdVarCal4 & "#" & m_oVarCal5.Nivel
                                                                            sFilaGrid = sFilaGrid & "#" & CInt(m_oVarCal5.RestriccionSiNo) & "#" & m_oVarCal5.RestriccionInf
                                                                            sFilaGrid = sFilaGrid & "#" & m_oVarCal5.RestriccionSup & "#" & CInt(m_oVarCal5.AvisoSiNo)
                                                                            sFilaGrid = sFilaGrid & "#" & m_oVarCal5.AvisoInf & "#" & m_oVarCal5.AvisoSup
                                                                            sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
                                                                            sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
                                                                            sFilaGrid = sFilaGrid & "#" & m_oVarCal5.Tipo & "#" & m_oVarCal5.Subtipo
                                                                            sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                                                                        Next    'm_oVarCal5
                                                                    End If
                                                                End With    '.VariblesCal.Item(CStr(m_oVarCal4.Nivel) & CStr(m_oVarCal4.Id))
                                                            End If
                                                        Next    'm_oVarCal4
                                                    End If
                                                End With    '.VariblesCal.Item(CStr(m_oVarCal3.Nivel) & CStr(m_oVarCal3.Id))
                                            End If
                                        Next    'm_oVarCal3
                                    End If
                                End With    '.VariblesCal.Item(CStr(m_oVarCal2.Nivel) & CStr(m_oVarCal2.Id))
                            End If
                        Next    'm_oVarCal2
                    End If
                End With    '.VariblesCal.Item(CStr(m_oVarCal1.Nivel) & CStr(m_oVarCal1.Id))
            End If
        Next    'm_oVarCal1
    End If

    If bEsta Or oUnidadUnqa.unQA1 = -1 Then
        'a�ade la fila a la grid
        sdbgProveVarsQA.AddItem sFilaGrid
        sdbgProveVarsQA.Update
    End If

    Set oVarCalif = Nothing

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "Rellenar_sdbgProveVarsQA_Unqa", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
''' <summary>
''' Ocultar dinamicamente las variables en el grid ProveVars
''' </summary>
''' <remarks>Llamada desde:Form_MouseMove       sdbgProveVarsQA_MouseMove       sdbgVarsQAProve_MouseMove; Tiempo m�ximo:0,3</remarks>
Private Sub OcultarMostrarVarCal()
    Dim iGrupos As Long
    Dim sDatos_Variable() As String
    Dim oVarCal As CVariableCalidad
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    Dim bVisibleVista As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    LockWindowUpdate frmComparativaQA.hWnd
    Screen.MousePointer = vbHourglass
    
    If Not m_oVarsCalidad Is Nothing Then
        For iGrupos = 2 To sdbgProveVarsQA.Groups.Count - 1
            sDatos_Variable = Split(sdbgProveVarsQA.Groups(iGrupos).Columns(2).Value, "#")
                        
            If Not (sdbgProveVarsQA.Groups(iGrupos).Columns(2).Value = "") Then
                Select Case sDatos_Variable(POS_NIVEL)
                Case 0
                    Set oVarCal = m_oVarsCalidad.Item(sDatos_Variable(POS_NIVEL) & sDatos_Variable(POS_ID))
                    
                    scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & sDatos_Variable(POS_ID)
                    
                    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0) Is Nothing Then
                        bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
                    Else
                        bVisibleVista = False
                    End If
                Case 1
                    Set oVarCal = m_oVarsCalidad.Item("1" & sDatos_Variable(POS_ID))
                    
                    scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
                    scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID)
                    
                    If m_oVistaCalSeleccionada.Materiales_Proceso And Not oVarCal.MaterialProceso Then
                        bVisibleVista = False
                    ElseIf Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                        bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).Visible
                    Else
                        bVisibleVista = False
                    End If
                Case 2
                    With m_oVarsCalidad.Item("1" & sDatos_Variable(POS_ID_VARCAL1))
                        Set oVarCal = .VariblesCal.Item(sDatos_Variable(POS_NIVEL) & sDatos_Variable(POS_ID))
                    End With
                    
                    scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
                    scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
                    scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID)
                    
                    With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                        If m_oVistaCalSeleccionada.Materiales_Proceso And Not oVarCal.MaterialProceso Then
                            bVisibleVista = False
                        ElseIf Not .VariblesCalVista.Item(scod2) Is Nothing Then
                            bVisibleVista = .VariblesCalVista.Item(scod2).Visible
                        Else
                            bVisibleVista = False
                        End If
                    End With
                Case 3
                    With m_oVarsCalidad.Item("1" & sDatos_Variable(POS_ID_VARCAL1))
                        With .VariblesCal.Item("2" & sDatos_Variable(POS_ID_VARCAL2))
                            Set oVarCal = .VariblesCal.Item("3" & sDatos_Variable(POS_ID))
                        End With
                    End With
                    
                    scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
                    scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
                    scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
                    scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID)
                    
                    With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                        With .VariblesCalVista.Item(scod2)
                            If m_oVistaCalSeleccionada.Materiales_Proceso And Not oVarCal.MaterialProceso Then
                                bVisibleVista = False
                            ElseIf Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                bVisibleVista = .VariblesCalVista.Item(scod3).Visible
                            Else
                                bVisibleVista = False
                            End If
                        End With
                    End With
                Case 4
                    With m_oVarsCalidad.Item("1" & sDatos_Variable(POS_ID_VARCAL1))
                        With .VariblesCal.Item("2" & sDatos_Variable(POS_ID_VARCAL2))
                            With .VariblesCal.Item("3" & sDatos_Variable(POS_ID_VARCAL3))
                                Set oVarCal = .VariblesCal.Item("4" & sDatos_Variable(POS_ID))
                            End With
                        End With
                    End With
                    
                    scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
                    scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
                    scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
                    scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID_VARCAL3)
                    scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & sDatos_Variable(POS_ID)
                    
                    With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                        With .VariblesCalVista.Item(scod2)
                            With .VariblesCalVista.Item(scod3)
                                If m_oVistaCalSeleccionada.Materiales_Proceso And Not oVarCal.MaterialProceso Then
                                    bVisibleVista = False
                                ElseIf Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                    bVisibleVista = .VariblesCalVista.Item(scod4).Visible
                                Else
                                    bVisibleVista = False
                                End If
                            End With
                        End With
                    End With
                    
                Case 5
                    With m_oVarsCalidad.Item("1" & sDatos_Variable(POS_ID_VARCAL1))
                        With .VariblesCal.Item("2" & sDatos_Variable(POS_ID_VARCAL2))
                            With .VariblesCal.Item("3" & sDatos_Variable(POS_ID_VARCAL3))
                                With .VariblesCal.Item("4" & sDatos_Variable(POS_ID_VARCAL4))
                                    Set oVarCal = .VariblesCal.Item("5" & sDatos_Variable(POS_ID))
                                End With
                            End With
                        End With
                    End With
                    
                    scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
                    scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
                    scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
                    scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & sDatos_Variable(POS_ID_VARCAL3)
                    scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & sDatos_Variable(POS_ID_VARCAL4)
                    scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & sDatos_Variable(POS_ID)
                    
                    With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                        With .VariblesCalVista.Item(scod2)
                            With .VariblesCalVista.Item(scod3)
                                With .VariblesCalVista.Item(scod4)
                                    If m_oVistaCalSeleccionada.Materiales_Proceso And Not oVarCal.MaterialProceso Then
                                        bVisibleVista = False
                                    ElseIf Not .VariblesCalVista.Item(scod5) Is Nothing Then
                                        bVisibleVista = .VariblesCalVista.Item(scod5).Visible
                                    Else
                                        bVisibleVista = False
                                    End If
                                End With
                            End With
                        End With
                    End With
                End Select
                    
                sdbgProveVarsQA.Groups(iGrupos).Visible = (oVarCal.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista
            End If
        Next
    End If
    
    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "OcultarMostrarVarCal", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
''' <summary>
''' Devuelve un string con los datos identificativos de las diferentes variables de calidad para el grid de ProveVars.
''' Se usa para q si por ejemplo la variable de nivel 1 no cumple con el material de proceso sus hijos, sus nietos, etc
''' esten identificados en la linea del grid ProveVars, antes directamente cortaba el string y en consecuencia el grid.additem
''' no llevaba esa informaci�n. Antes esa informaci�n no se usaba, si era string nulo pues sab�a q "algo" impedio q estuviera
''' (pero no el q) y obraba con un resume next. Ahora todas y cada una de las columnas "DATOS_VAR" deben estar rellenas (sino
''' como oculto dinamicamente variables, unqas y variables)
''' </summary>
''' <param name="oVarcalPadre">Objeto variable de calidad de la q cuelgan los hijos</param>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <returns>string con los datos identificativos de las diferentes variables</returns>
''' <remarks>Llamada desde: Rellenar_sdbgProveVarsQA_Unqa ; Tiempo m�ximo: 0,1</remarks>
Private Function CompletarRestoNiveles(ByVal oVarcalPadre As CVariableCalidad, ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer) As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    CompletarRestoNiveles = ""
    
    Select Case oVarcalPadre.Nivel
    Case 0
        CompletarRestoNiveles = CompletarRestoNiveles1(oVarcalPadre, oUnidadUnqa, Nivel)
    Case 1
        CompletarRestoNiveles = CompletarRestoNiveles2(oVarcalPadre, oUnidadUnqa, Nivel)
    Case 2
        CompletarRestoNiveles = CompletarRestoNiveles3(oVarcalPadre, oUnidadUnqa, Nivel)
    Case 3
        CompletarRestoNiveles = CompletarRestoNiveles4(oVarcalPadre, oUnidadUnqa, Nivel)
    Case 4
        CompletarRestoNiveles = CompletarRestoNiveles5(oVarcalPadre, oUnidadUnqa, Nivel)
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CompletarRestoNiveles", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
''' <summary>
''' Devuelve un string con los datos identificativos de las diferentes variables de calidad de nivel 1
''' para el grid de ProveVars
''' </summary>
''' <param name="oVarcalPadre">Objeto variable de calidad de la q cuelgan los hijos</param>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <returns>string con los datos identificativos de las diferentes variables</returns>
''' <remarks>Llamada desde: CompletarRestoNiveles; Tiempo m�ximo: 0,1</remarks>
Private Function CompletarRestoNiveles1(ByVal oVarcalPadre As CVariableCalidad, ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer) As String
    Dim sFilaGrid As String
    Dim oVarcalHijo As CVariableCalidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sFilaGrid = ""
    
    For Each oVarcalHijo In oVarcalPadre.VariblesCal
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oVarcalHijo.Id & "#" & oVarcalHijo.IdVarCal1
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal2 & "#" & oVarcalHijo.IdVarCal3
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal4 & "#" & oVarcalHijo.Nivel
        sFilaGrid = sFilaGrid & "#" & CInt(oVarcalHijo.RestriccionSiNo) & "#" & oVarcalHijo.RestriccionInf
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.RestriccionSup & "#" & CInt(oVarcalHijo.AvisoSiNo)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.AvisoInf & "#" & oVarcalHijo.AvisoSup
        sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
        sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.Tipo & "#" & oVarcalHijo.Subtipo
        sFilaGrid = sFilaGrid & Chr(m_lSeparador)
        sFilaGrid = sFilaGrid & CompletarRestoNiveles2(oVarcalHijo, oUnidadUnqa, Nivel)
    Next
    
    CompletarRestoNiveles1 = sFilaGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CompletarRestoNiveles1", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
''' <summary>
''' Devuelve un string con los datos identificativos de las diferentes variables de calidad de nivel 2
''' para el grid de ProveVars
''' </summary>
''' <param name="oVarcalPadre">Objeto variable de calidad de la q cuelgan los hijos</param>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <returns>string con los datos identificativos de las diferentes variables</returns>
''' <remarks>Llamada desde: CompletarRestoNiveles   CompletarRestoNiveles1; Tiempo m�ximo: 0,1</remarks>
Private Function CompletarRestoNiveles2(ByVal oVarcalPadre As CVariableCalidad, ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer) As String
    Dim sFilaGrid As String
    Dim oVarcalHijo As CVariableCalidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sFilaGrid = ""
    
    If oVarcalPadre.VariblesCal Is Nothing Then Exit Function
    
    For Each oVarcalHijo In oVarcalPadre.VariblesCal
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oVarcalHijo.Id & "#" & oVarcalHijo.IdVarCal1
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal2 & "#" & oVarcalHijo.IdVarCal3
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal4 & "#" & oVarcalHijo.Nivel
        sFilaGrid = sFilaGrid & "#" & CInt(oVarcalHijo.RestriccionSiNo) & "#" & oVarcalHijo.RestriccionInf
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.RestriccionSup & "#" & CInt(oVarcalHijo.AvisoSiNo)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.AvisoInf & "#" & oVarcalHijo.AvisoSup
        sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
        sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.Tipo & "#" & oVarcalHijo.Subtipo
        sFilaGrid = sFilaGrid & Chr(m_lSeparador)
        sFilaGrid = sFilaGrid & CompletarRestoNiveles3(oVarcalHijo, oUnidadUnqa, Nivel)
    Next
    
    CompletarRestoNiveles2 = sFilaGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CompletarRestoNiveles2", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
''' <summary>
''' Devuelve un string con los datos identificativos de las diferentes variables de calidad de nivel 3
''' para el grid de ProveVars
''' </summary>
''' <param name="oVarcalPadre">Objeto variable de calidad de la q cuelgan los hijos</param>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <returns>string con los datos identificativos de las diferentes variables</returns>
''' <remarks>Llamada desde: CompletarRestoNiveles   CompletarRestoNiveles2; Tiempo m�ximo: 0,1</remarks>
Private Function CompletarRestoNiveles3(ByVal oVarcalPadre As CVariableCalidad, ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer) As String
    Dim sFilaGrid As String
    Dim oVarcalHijo As CVariableCalidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sFilaGrid = ""
    
    If oVarcalPadre.VariblesCal Is Nothing Then Exit Function

    For Each oVarcalHijo In oVarcalPadre.VariblesCal
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oVarcalHijo.Id & "#" & oVarcalHijo.IdVarCal1
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal2 & "#" & oVarcalHijo.IdVarCal3
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal4 & "#" & oVarcalHijo.Nivel
        sFilaGrid = sFilaGrid & "#" & CInt(oVarcalHijo.RestriccionSiNo) & "#" & oVarcalHijo.RestriccionInf
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.RestriccionSup & "#" & CInt(oVarcalHijo.AvisoSiNo)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.AvisoInf & "#" & oVarcalHijo.AvisoSup
        sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
        sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.Tipo & "#" & oVarcalHijo.Subtipo
        sFilaGrid = sFilaGrid & Chr(m_lSeparador)
        sFilaGrid = sFilaGrid & CompletarRestoNiveles4(oVarcalHijo, oUnidadUnqa, Nivel)
    Next
    
    CompletarRestoNiveles3 = sFilaGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CompletarRestoNiveles3", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
''' <summary>
''' Devuelve un string con los datos identificativos de las diferentes variables de calidad de nivel 4
''' para el grid de ProveVars
''' </summary>
''' <param name="oVarcalPadre">Objeto variable de calidad de la q cuelgan los hijos</param>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <returns>string con los datos identificativos de las diferentes variables</returns>
''' <remarks>Llamada desde: CompletarRestoNiveles   CompletarRestoNiveles3; Tiempo m�ximo: 0,1</remarks>
Private Function CompletarRestoNiveles4(ByVal oVarcalPadre As CVariableCalidad, ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer) As String
    Dim sFilaGrid As String
    Dim oVarcalHijo As CVariableCalidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sFilaGrid = ""
    
    If oVarcalPadre.VariblesCal Is Nothing Then Exit Function
    
    For Each oVarcalHijo In oVarcalPadre.VariblesCal
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oVarcalHijo.Id & "#" & oVarcalHijo.IdVarCal1
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal2 & "#" & oVarcalHijo.IdVarCal3
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal4 & "#" & oVarcalHijo.Nivel
        sFilaGrid = sFilaGrid & "#" & CInt(oVarcalHijo.RestriccionSiNo) & "#" & oVarcalHijo.RestriccionInf
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.RestriccionSup & "#" & CInt(oVarcalHijo.AvisoSiNo)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.AvisoInf & "#" & oVarcalHijo.AvisoSup
        sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
        sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.Tipo & "#" & oVarcalHijo.Subtipo
        sFilaGrid = sFilaGrid & Chr(m_lSeparador)
        sFilaGrid = sFilaGrid & CompletarRestoNiveles5(oVarcalHijo, oUnidadUnqa, Nivel)
    Next
    
    CompletarRestoNiveles4 = sFilaGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CompletarRestoNiveles4", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
''' <summary>
''' Devuelve un string con los datos identificativos de las diferentes variables de calidad de nivel 5
''' para el grid de ProveVars
''' </summary>
''' <param name="oVarcalPadre">Objeto variable de calidad de la q cuelgan los hijos</param>
''' <param name="oUnidadUnqa">Objeto unidad de negocio</param>
''' <param name="Nivel">Nivel de la unidad de negocio</param>
''' <returns>string con los datos identificativos de las diferentes variables</returns>
''' <remarks>Llamada desde: CompletarRestoNiveles   CompletarRestoNiveles4; Tiempo m�ximo: 0,1</remarks>
Private Function CompletarRestoNiveles5(ByVal oVarcalPadre As CVariableCalidad, ByVal oUnidadUnqa As CUnidadNegQA, ByVal Nivel As Integer) As String
    Dim sFilaGrid As String
    Dim oVarcalHijo As CVariableCalidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sFilaGrid = ""
    
    If oVarcalPadre.VariblesCal Is Nothing Then Exit Function
    
    For Each oVarcalHijo In oVarcalPadre.VariblesCal
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oVarcalHijo.Id & "#" & oVarcalHijo.IdVarCal1
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal2 & "#" & oVarcalHijo.IdVarCal3
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.IdVarCal4 & "#" & oVarcalHijo.Nivel
        sFilaGrid = sFilaGrid & "#" & CInt(oVarcalHijo.RestriccionSiNo) & "#" & oVarcalHijo.RestriccionInf
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.RestriccionSup & "#" & CInt(oVarcalHijo.AvisoSiNo)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.AvisoInf & "#" & oVarcalHijo.AvisoSup
        sFilaGrid = sFilaGrid & "#" & oUnidadUnqa.Id & "#" & IIf(oUnidadUnqa.UnidadesNegQA.Count = 0 And (oUnidadUnqa.unQA1 > -1), 0, 1)
        sFilaGrid = sFilaGrid & "#" & IIf(oUnidadUnqa.unQA1 > -1, Nivel, 0)
        sFilaGrid = sFilaGrid & "#" & oVarcalHijo.Tipo & "#" & oVarcalHijo.Subtipo
        sFilaGrid = sFilaGrid & Chr(m_lSeparador)
    Next
    
    CompletarRestoNiveles5 = sFilaGrid
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CompletarRestoNiveles5", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Tras sacar un pic , el combo de vista recibe el foco, como el combo responde a la rueda del rat�n, a la
''' escritura y a las flechas, pues a veces se cambia/limpia aparenmente solo.
''' </summary>
''' <remarks>Llamada desde: Form_MouseMove  sdbgProveVarsQA_MouseMove   sdbgVarsQAProve_MouseMove   cmdExcel_Click
'''     cmdGuardarVista_Click       cmdGuardarVistaNueva_Click      cmdInvertir_Click       sdbcVistaActual_CloseUp
'''     cmdRenombrarVista_Click     ; Tiempo m�ximo: 0</remarks>
Private Sub FocoAGrid()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.sdbgVarsQAProve.Visible Then
        If Me.Visible Then Me.sdbgVarsQAProve.SetFocus
        
        Me.sdbgVarsQAProve.Row = 0
        Me.sdbgVarsQAProve.col = 0
    Else
        If Me.Visible Then Me.sdbgProveVarsQA.SetFocus
        
        Me.sdbgProveVarsQA.Row = 0
        Me.sdbgProveVarsQA.col = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "FocoAGrid", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Llamada para la unidad seleccionada. Crea un arbol con la unidad seleccionada y las unidades de negocio
''' hijas de la selccionada q sean accesibles (si ninguna variable en toda la comparativa tiene puntos en
''' una unidad, dicha unidad no es accesible).
''' En el caso de grid ProveVars esta claro q en la linea tengo seleccionada un proveedor, una unidad y N
''' variables lo q implica N registros de CONF_VISTAS_CAL_GLOBAL_VAR. As� pues un cambio en la configuraci�n
''' estando en grid ProveVars se aplicar�a a N registros. Lo q puede implicar el caos (caos/dejar de ver mil
''' cosas) en VarsProve y las vistas son globales no por proceso. As� q se decide q estos cambios vayan a
''' colecciones "desligadas de la vista" de visibilidades de unidades.
''' Estas colecciones desligadas se van al invertir o al cambiar de vista.
''' </summary>
''' <param name="Prove">Proveedor</param>
''' <param name="Unqa">ID de unidad de negocio</param>
''' <param name="Nivel">Nivel de unidad de negocio</param>
''' <remarks>Llamada desde: sdbgProveVarsQA_RowColChange; Tiempo m�ximo: 0,1</remarks>
Private Sub CellGenerarEstructuraUnidadesNegocio_GridPv(ByVal Prove As String, ByVal Unqa As String, ByVal Nivel As String)
    Dim oNegocio As CUnidadNegQA
    Dim oNegocio2 As CUnidadNegQA
    Dim oNegocio3 As CUnidadNegQA
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    
    Dim sTag As String
    
    Dim nodx As MSComctlLib.node
    Dim bNiv0 As Boolean
    
    Dim bTodosCheck As Boolean
    Dim bAlgunaUnqa As Boolean
    Dim sProveUnqaVarcal As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Me.tvwUndNegUsu_GridPv.Visible = True
        
    tvwUndNegUsu_GridPv.Nodes.clear
    
    Me.chkTodasUnidades_GridPv.Value = False
   
    sTag = Prove & "#"
    
    bNiv0 = False
    
    bTodosCheck = True
    
    For Each oNegocio In m_oUnidadesQa.Unidades
        If Not bNiv0 Then
            bNiv0 = True
        Else
            scod1 = CStr(oNegocio.Id)
            
            sProveUnqaVarcal = CStr(m_iNivelRest) & CStr(m_iIDRest) & "#" & scod1 & "#" & Prove
            bAlgunaUnqa = Not (m_oUnqaVarcalEnProve.Item(sProveUnqaVarcal) Is Nothing)
                        
            If ((Nivel = 0) Or ((Nivel = 1) And (scod1 = Unqa))) And bAlgunaUnqa Then
                Set nodx = tvwUndNegUsu_GridPv.Nodes.Add(, , "UN1" & scod1, CStr(oNegocio.Cod) & " - " & oNegocio.Den)
                nodx.Tag = sTag & scod1
                nodx.EnsureVisible
                nodx.Expanded = True
                
                If OcultaUnidadProve(Prove & "#" & oNegocio.Id) Then
                    nodx.Checked = False
                ElseIf MostradaUnidadProve(Prove & "#" & oNegocio.Id) Then
                    nodx.Checked = True
                Else
                    nodx.Checked = DameCheckedUnQaGriProveVars(oNegocio.Id)
                End If
                                                              
                If nodx.Checked = False Then bTodosCheck = False
            End If
            
            If ((Nivel = 0) _
            Or ((Nivel = 1) And (scod1 = Unqa)) _
            Or (Nivel = 2) _
            Or (Nivel = 3)) And bAlgunaUnqa Then
                For Each oNegocio2 In oNegocio.UnidadesNegQA.Unidades
                    scod2 = CStr(oNegocio2.Id)
                    
                    sProveUnqaVarcal = CStr(m_iNivelRest) & CStr(m_iIDRest) & "#" & scod2 & "#" & Prove
                    bAlgunaUnqa = Not (m_oUnqaVarcalEnProve.Item(sProveUnqaVarcal) Is Nothing)
                    
                    If ((Nivel = 0) _
                    Or ((Nivel = 1) And (scod1 = Unqa)) _
                    Or ((Nivel = 2) And (scod2 = Unqa)) _
                    Or (Nivel = 3)) And bAlgunaUnqa Then
                        If Nivel = 3 Then
                        Else
                            If Nivel = 2 And (scod2 = Unqa) Then
                                Set nodx = tvwUndNegUsu_GridPv.Nodes.Add(, , "UN2" & scod2, CStr(oNegocio2.Cod) & " - " & oNegocio2.Den)
                            ElseIf (Nivel = 0) Or (Nivel = 1) Then
                                Set nodx = tvwUndNegUsu_GridPv.Nodes.Add("UN1" & scod1, tvwChild, "UN2" & scod2, CStr(oNegocio2.Cod) & " - " & oNegocio2.Den)
                            End If
                            nodx.Tag = sTag & scod2
                            nodx.EnsureVisible
                            nodx.Expanded = True
                
                            If OcultaUnidadProve(Prove & "#" & oNegocio2.Id) Then
                                nodx.Checked = False
                            ElseIf MostradaUnidadProve(Prove & "#" & oNegocio2.Id) Then
                                nodx.Checked = True
                            Else
                                nodx.Checked = DameCheckedUnQaGriProveVars(oNegocio2.Id)
                            End If
                            
                            If nodx.Checked = False Then bTodosCheck = False
                        End If
                                    
                        If ((Nivel = 0) _
                        Or ((Nivel = 1) And (scod1 = Unqa)) _
                        Or ((Nivel = 2) And (scod2 = Unqa)) _
                        Or (Nivel = 3)) And bAlgunaUnqa Then
                            For Each oNegocio3 In oNegocio2.UnidadesNegQA.Unidades
                                scod3 = CStr(oNegocio3.Id)
                                
                                sProveUnqaVarcal = CStr(m_iNivelRest) & CStr(m_iIDRest) & "#" & scod3 & "#" & Prove
                                bAlgunaUnqa = Not (m_oUnqaVarcalEnProve.Item(sProveUnqaVarcal) Is Nothing)
                             
                                If ((Nivel = 0) _
                                Or ((Nivel = 1) And (scod1 = Unqa)) _
                                Or ((Nivel = 2) And (scod2 = Unqa)) _
                                Or ((Nivel = 3) And (scod3 = Unqa))) And bAlgunaUnqa Then
                                    If Nivel = 3 Then
                                        Set nodx = tvwUndNegUsu_GridPv.Nodes.Add(, , "UN3" & scod3, CStr(oNegocio3.Cod) & " - " & oNegocio3.Den)
                                    Else
                                        Set nodx = tvwUndNegUsu_GridPv.Nodes.Add("UN2" & scod2, tvwChild, "UN3" & scod3, CStr(oNegocio3.Cod) & " - " & oNegocio3.Den)
                                    End If
                                    nodx.Tag = sTag & scod3
                                    nodx.EnsureVisible
                                    nodx.Expanded = True
                                    
                                    If OcultaUnidadProve(Prove & "#" & oNegocio3.Id) Then
                                        nodx.Checked = False
                                    ElseIf MostradaUnidadProve(Prove & "#" & oNegocio3.Id) Then
                                        nodx.Checked = True
                                    Else
                                        nodx.Checked = DameCheckedUnQaGriProveVars(oNegocio3.Id)
                                    End If
                                    
                                    If nodx.Checked = False Then bTodosCheck = False
                                End If
                            Next
                            
                            If (Nivel = 3 And (scod3 = Unqa)) Then Exit For
                        End If
                    End If
                        
                    If (Nivel = 3 And (scod3 = Unqa)) Then Exit For
                    If (Nivel = 2 And (scod2 = Unqa)) Then Exit For
                Next
            
                If (Nivel = 3 And (scod3 = Unqa)) Then Exit For
                If (Nivel = 2 And (scod2 = Unqa)) Then Exit For
                If (Nivel = 1 And (scod1 = Unqa)) Then Exit For
            End If
        End If
    Next
    
    m_bDeshabilidar_Chk_ChangeTodas = True
    If bTodosCheck Then
        Me.chkTodasUnidades_GridPv.Value = vbChecked
    Else
        Me.chkTodasUnidades_GridPv.Value = vbUnchecked
    End If
    For Each nodx In Me.tvwUndNegUsu_GridPv.Nodes
        Me.tvwUndNegUsu_GridPv.selectedItem = nodx
        Exit For
    Next
    Me.tvwUndNegUsu_GridPv.selectedItem = Nothing
    m_bDeshabilidar_Chk_ChangeTodas = False
    
    Set oNegocio = Nothing
    Set oNegocio2 = Nothing
    Set oNegocio3 = Nothing
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "CellGenerarEstructuraUnidadesNegocio_GridPv", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Determina si se ha establecido a traves del link de la linea en el grid de ProveVars para un proveedor q
''' una unidad concreta sea invisible
''' </summary>
''' <param name="ProveUnqa">Proveedor y unidad q se esta tratando</param>
''' <returns>Si se ha establecido o no</returns>
''' <remarks>Llamada desde: Rellenar_sdbgProveVarsQA; Tiempo m�ximo: 0</remarks>
Private Function OcultaUnidadProve(ByVal ProveUnqa As String) As Boolean
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
    
    OcultaUnidadProve = Not (m_oOcultarUnqaProve_GridPv.Item(ProveUnqa) Is Nothing)

    Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "OcultaUnidadProve", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Determina si se ha establecido a traves del link de la linea en el grid de ProveVars para un proveedor q
''' una unidad concreta sea visible
''' </summary>
''' <param name="ProveUnqa">Proveedor y unidad q se esta tratando</param>
''' <returns>Si se ha establecido o no</returns>
''' <remarks>Llamada desde: Rellenar_sdbgProveVarsQA_Unqa; Tiempo m�ximo: 0</remarks>
Private Function MostradaUnidadProve(ByVal ProveUnqa As String) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    MostradaUnidadProve = Not (m_oMostrarUnqaProve_GridPv.Item(ProveUnqa) Is Nothing)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "MostradaUnidadProve", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Determina si hay alguna Variable de Calidad visible para el grid ProveVars. Soluci�nn al problema detectado
''' en Rellenar_gridProveVars de q como hagas splitterpos= 2 sin q haya un grupo 2 visible te da un casque
''' </summary>
''' <returns>si la hay o no</returns>
''' <remarks>Llamada desde:Rellenar_sdbgProveVarsQA; Tiempo m�ximo:0</remarks>
Private Function HayAlgunaVCVisible() As Boolean
    Dim bVisibleVista As Boolean
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    HayAlgunaVCVisible = False
    
    For Each m_oVarCal0 In m_oVarsCalidad
        If m_oVarCal0.Nivel = 0 Then
            scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_oVarCal0.Id)
            If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0) Is Nothing Then
                bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible
            Else
                bVisibleVista = False
            End If
            
            If (m_oVarCal0.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista Then
                HayAlgunaVCVisible = True
                Exit Function
            End If
            
            For Each m_oVarCal1 In m_oVarsCalidad
                If m_oVarCal1.Nivel <> 0 Then
                    scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(m_oVarCal1.Id)
                    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                        bVisibleVista = m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).Visible
                        
                        If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal1.MaterialProceso Then
                            bVisibleVista = False
                        End If
                    Else
                        bVisibleVista = False
                    End If
                    
                    If (m_oVarCal1.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista Then
                        HayAlgunaVCVisible = True
                        Exit Function
                    End If
                    
                    If Not m_oVarCal1.VariblesCal Is Nothing Then
                        For Each m_oVarCal2 In m_oVarCal1.VariblesCal
                            scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(m_oVarCal2.Id)
                            If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2) Is Nothing Then
                                With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                    If .VariblesCalVista Is Nothing Then
                                        bVisibleVista = False
                                    Else
                                        If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal2.MaterialProceso Then
                                            bVisibleVista = False
                                        Else
                                            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                bVisibleVista = .VariblesCalVista.Item(scod2).Visible
                                            Else
                                                bVisibleVista = False
                                            End If
                                        End If
                                    End If
                                End With
                            End If
                            
                            If (m_oVarCal2.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista Then
                                HayAlgunaVCVisible = True
                                Exit Function
                            End If
                            
                            If Not m_oVarCal2.VariblesCal Is Nothing Then
                                For Each m_oVarCal3 In m_oVarCal2.VariblesCal
                                    scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(m_oVarCal3.Id)
                                    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3) Is Nothing Then
                                        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                With .VariblesCalVista.Item(scod2)
                                                If .VariblesCalVista Is Nothing Then
                                                    bVisibleVista = False
                                                Else
                                                    If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal3.MaterialProceso Then
                                                        bVisibleVista = False
                                                    Else
                                                        If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                            bVisibleVista = .VariblesCalVista.Item(scod3).Visible
                                                        Else
                                                            bVisibleVista = False
                                                        End If
                                                    End If
                                                End If
                                                End With
                                            End If
                                        End With
                                    End If
                                    
                                    If (m_oVarCal3.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista Then
                                        HayAlgunaVCVisible = True
                                        Exit Function
                                    End If
                                    
                                    If Not m_oVarCal3.VariblesCal Is Nothing Then
                                        For Each m_oVarCal4 In m_oVarCal3.VariblesCal
                                            scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(m_oVarCal4.Id)
                                            If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                                                With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                                    If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                        With .VariblesCalVista.Item(scod2)
                                                            If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                                With .VariblesCalVista.Item(scod3)
                                                                If .VariblesCalVista Is Nothing Then
                                                                    bVisibleVista = False
                                                                Else
                                                                    If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal4.MaterialProceso Then
                                                                        bVisibleVista = False
                                                                    Else
                                                                        If Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                                                            bVisibleVista = .VariblesCalVista.Item(scod4).Visible
                                                                        Else
                                                                            bVisibleVista = False
                                                                        End If
                                                                    End If
                                                                End If
                                                                End With
                                                            End If
                                                        End With
                                                    End If
                                                End With
                                            End If
                                            
                                            If (m_oVarCal4.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista Then
                                                HayAlgunaVCVisible = True
                                                Exit Function
                                            End If
                                            
                                            If Not m_oVarCal4.VariblesCal Is Nothing Then
                                                For Each m_oVarCal5 In m_oVarCal4.VariblesCal
                                                    scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & CStr(m_oVarCal5.Id)
                                                    If Not m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1) Is Nothing Then
                                                        With m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1)
                                                            If Not .VariblesCalVista.Item(scod2) Is Nothing Then
                                                                With .VariblesCalVista.Item(scod2)
                                                                    If Not .VariblesCalVista.Item(scod3) Is Nothing Then
                                                                        With .VariblesCalVista.Item(scod3)
                                                                            If Not .VariblesCalVista.Item(scod4) Is Nothing Then
                                                                                With .VariblesCalVista.Item(scod4)
                                                                                If .VariblesCalVista Is Nothing Then
                                                                                    bVisibleVista = False
                                                                                Else
                                                                                    If m_oVistaCalSeleccionada.Materiales_Proceso And Not m_oVarCal5.MaterialProceso Then
                                                                                        bVisibleVista = False
                                                                                    Else
                                                                                        If Not .VariblesCalVista.Item(scod5) Is Nothing Then
                                                                                            bVisibleVista = .VariblesCalVista.Item(scod5).Visible
                                                                                        Else
                                                                                            bVisibleVista = False
                                                                                        End If
                                                                                    End If
                                                                                End If
                                                                                End With
                                                                            End If
                                                                        End With
                                                                    End If
                                                                End With
                                                            End If
                                                        End With
                                                    End If
                                                    
                                                    If (m_oVarCal5.UsuConsultar Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Administrador) And bVisibleVista Then
                                                        HayAlgunaVCVisible = True
                                                        Exit Function
                                                    End If
                                                Next '5
                                            End If
                                        Next '4
                                    End If
                                Next '3
                            End If
                        Next '2
                    End If
                End If 'If m_oVarCal1.Nivel <> 0 Then
            Next '1
        End If 'If m_oVarCal0.Nivel = 0 Then
    Next '0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "HayAlgunaVCVisible", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function



''' <summary>
''' Muestra la label de variables invisibles q implicar�an q al menos un proveedor sea no adjudicable � adjudicable
''' solo tras un aviso
''' </summary>
''' <remarks>Llamada desde: cmdNoVisibles_Click; Tiempo m�ximo:0,1</remarks>
Private Sub NoVisiblesControles()
    Dim i As Integer
    Dim j As Integer
    Dim Vars() As String
    Dim VarsUnqa() As String
    Dim bEncontrado As Boolean
    Dim bCreadalabel As Boolean
    
    Dim c As Label
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 1 To m_iEtiqNoVisibles
        Controls.Remove ("Label" & CStr(i))
    Next
    
    m_iEtiqNoVisibles = 0
    
    If sdbgVarsQAProve.Groups(0).HeadStyleSet = "VariablesNoVisible" Then
    
        For i = 1 To UBound(m_sVarsNoVisible)
            m_iEtiqNoVisibles = m_iEtiqNoVisibles + 1
            
            Vars = Split(m_sVarsNoVisible(i), "#")
            
            'Variable
            Set c = Controls.Add("VB.Label", "Label" & m_iEtiqNoVisibles)
            
            c.AutoSize = Me.lblNoVisibles.AutoSize
            c.Backcolor = Me.lblNoVisibles.Backcolor
            c.Font = Me.lblNoVisibles.Font
            c.FontBold = True
            c.FontUnderline = True
            c.caption = Vars(0)
            c.Visible = True
            c.Width = Me.lblNoVisibles.Width
            c.Height = Me.lblNoVisibles.Height
            c.Left = Me.lblNoVisibles.Left
            c.Top = Me.lblNoVisibles.Top + (Me.lblNoVisibles.Height * m_iEtiqNoVisibles)
            c.Top = c.Top + (Me.lblNoVisibles.Height / 4)

            Set c.Container = Me.picNoVisibles
            
            bEncontrado = False
            bCreadalabel = False
            
            For j = 1 To UBound(m_sVarsUnqaNoVisible)
            
                VarsUnqa = Split(m_sVarsUnqaNoVisible(j), "#")

                If (VarsUnqa(2) = Vars(1)) And (VarsUnqa(3) = Vars(2)) Then
                    bEncontrado = True
                            
                    'Unidades: van todas en una linea
                    If bCreadalabel = False Then
                        bCreadalabel = True
                        m_iEtiqNoVisibles = m_iEtiqNoVisibles + 1
                        
                        Set c = Controls.Add("VB.Label", "Label" & m_iEtiqNoVisibles)
                        
                        c.AutoSize = True
                        c.Backcolor = Me.lblNoVisibles.Backcolor
                        c.Font = Me.lblNoVisibles.Font
                        c.FontBold = False
                        c.Visible = True
                        c.Width = Me.lblNoVisibles.Width
                        c.Height = Me.lblNoVisibles.Height
                        
                        c.Left = Me.lblNoVisibles.Left
                        c.Top = Me.lblNoVisibles.Top + (Me.lblNoVisibles.Height * m_iEtiqNoVisibles)
                        c.Top = c.Top + (Me.lblNoVisibles.Height / 4)
                        Set c.Container = Me.picNoVisibles
                        
                        c.caption = ""
                    End If
                    
                    c.caption = c.caption & VarsUnqa(4) & " - " & VarsUnqa(0) & ", "
                                        
                    If c.Width > (Me.lblNoVisibles.Width - 100) Then
                        'La linea ser�a demasiado grande. Las unidades van en otra linea mas.
                        c.caption = Left(c.caption, Len(c.caption) - Len(VarsUnqa(4) & " - " & VarsUnqa(0) & ", "))
                                                
                        m_iEtiqNoVisibles = m_iEtiqNoVisibles + 1
                        
                        Set c = Controls.Add("VB.Label", "Label" & m_iEtiqNoVisibles)
                        
                        c.AutoSize = True
                        c.Backcolor = Me.lblNoVisibles.Backcolor
                        c.Font = Me.lblNoVisibles.Font
                        c.FontBold = False
                        c.Visible = True
                        c.Width = Me.lblNoVisibles.Width
                        c.Height = Me.lblNoVisibles.Height
                        
                        c.Left = Me.lblNoVisibles.Left
                        c.Top = Me.lblNoVisibles.Top + (Me.lblNoVisibles.Height * m_iEtiqNoVisibles)
                        Set c.Container = Me.picNoVisibles
                        
                        c.caption = VarsUnqa(4) & " - " & VarsUnqa(0) & ", "
                    End If
                    
                ElseIf bEncontrado Then
                    c.caption = Left(c.caption, Len(c.caption) - 2)
                    Exit For
                End If
            Next j
        Next
        
        If bEncontrado Then
            c.caption = Left(c.caption, Len(c.caption) - 2)
        End If
        
        
        picNoVisibles.Height = Me.lblNoVisibles.Height + (m_iEtiqNoVisibles * Me.lblNoVisibles.Height) + (Me.lblNoVisibles.Height / 2)
        picNoVisibles.Width = Me.lblNoVisibles.Width + 35
        picNoVisibles.Left = sdbgVarsQAProve.Left + sdbgVarsQAProve.Groups(0).Width + 100
        picNoVisibles.Top = sdbgVarsQAProve.Top + 150
        picNoVisibles.Visible = True
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "NoVisiblesControles", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Determina si el nodo del arbol q es una unidad de negocio debe estar chequeado o no en el arbol de unidades
''' para el grid ProveVars. Q este chequeado implica q es visible en el grid.
''' Ver en el codigo de Rellenar_sdbgProveVarsQA rese�a sobre para q es cada colecci�n
''' </summary>
''' <param name="UnidadUnqaId">Id unidad de negocio</param>
''' <returns>Si debe estar chequeado o no</returns>
''' <remarks>Llamada desde: CellGenerarEstructuraUnidadesNegocio_GridPv ; Tiempo m�ximo: 0,1</remarks>
Private Function DameCheckedUnQaGriProveVars(ByVal UnidadUnqaId As Long) As Boolean
    
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DameCheckedUnQaGriProveVars = False
    
    If Not m_oVarsCalidad Is Nothing Then
        For Each m_oVarCal1 In m_oVarsCalidad
            If m_oVarCal1.Nivel = 0 Then
                scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_oVarCal1.Id)
                
                If m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).Visible Then
                    If UnqaEstaEnVista(UnidadUnqaId, m_oVarCal1.Nivel, m_oVarCal1.Id, 0, 0, 0, 0) Then
                        DameCheckedUnQaGriProveVars = True
                        Exit Function
                    End If
                End If
            Else
                scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(m_oVarCal1.Id)
                
                If m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).Visible Then
                    If UnqaEstaEnVista(UnidadUnqaId, m_oVarCal1.Nivel, m_oVarCal1.Id, 0, 0, 0, 0) Then
                        DameCheckedUnQaGriProveVars = True
                        Exit Function
                    End If
                End If
            End If
            
            If Not m_oVarCal1.VariblesCal Is Nothing And m_oVarCal1.Nivel <> 0 Then
                For Each m_oVarCal2 In m_oVarCal1.VariblesCal
                    scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(m_oVarCal2.Id)
                    
                    If m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).Visible Then
                        If UnqaEstaEnVista(UnidadUnqaId, m_oVarCal2.Nivel, m_oVarCal2.Id, m_oVarCal2.IdVarCal1, 0, 0, 0) Then
                            DameCheckedUnQaGriProveVars = True
                            Exit Function
                        End If
                    End If
                    
                    If Not m_oVarCal2.VariblesCal Is Nothing Then
                        For Each m_oVarCal3 In m_oVarCal2.VariblesCal
                            scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(m_oVarCal3.Id)
                            
                            If m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).Visible Then
                                If UnqaEstaEnVista(UnidadUnqaId, m_oVarCal3.Nivel, m_oVarCal3.Id, m_oVarCal3.IdVarCal1, m_oVarCal3.IdVarCal2, 0, 0) Then
                                    DameCheckedUnQaGriProveVars = True
                                    Exit Function
                                End If
                            End If
                            
                            If Not m_oVarCal3.VariblesCal Is Nothing Then
                                For Each m_oVarCal4 In m_oVarCal3.VariblesCal
                                    scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(m_oVarCal4.Id)
                                    
                                    If m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).Visible Then
                                        If UnqaEstaEnVista(UnidadUnqaId, m_oVarCal4.Nivel, m_oVarCal4.Id, m_oVarCal4.IdVarCal1, m_oVarCal4.IdVarCal2, m_oVarCal4.IdVarCal3, 0) Then
                                            DameCheckedUnQaGriProveVars = True
                                            Exit Function
                                        End If
                                    End If
                                    
                                    If Not m_oVarCal4.VariblesCal Is Nothing Then
                                        For Each m_oVarCal5 In m_oVarCal4.VariblesCal
                                            scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & CStr(m_oVarCal5.Id)
                                        
                                            If m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).Visible Then
                                                If UnqaEstaEnVista(UnidadUnqaId, m_oVarCal5.Nivel, m_oVarCal5.Id, m_oVarCal5.IdVarCal1, m_oVarCal5.IdVarCal2, m_oVarCal5.IdVarCal3, m_oVarCal5.IdVarCal4) Then
                                                    DameCheckedUnQaGriProveVars = True
                                                    Exit Function
                                                End If
                                            End If
                                        Next 'For Each m_oVarCal5 In m_oVarCal4.VariblesCal
                                    End If 'If Not m_oVarCal4.VariblesCal Is Nothing Then
                                Next 'For Each m_oVarCal4 In m_oVarCal3.VariblesCal
                            End If 'If Not m_oVarCal3.VariblesCal Is Nothing Then
                        Next 'For Each m_oVarCal3 In m_oVarCal2.VariblesCal
                    End If 'If Not m_oVarCal2.VariblesCal Is Nothing Then
                Next 'For Each m_oVarCal2 In m_oVarCal1.VariblesCal
            End If 'If Not m_oVarCal1.VariblesCal Is Nothing And m_oVarCal1.Nivel <> 0 Then
        Next 'For Each m_oVarCal1 In m_oVarsCalidad
    End If 'If Not m_oVarsCalidad Is Nothing Then
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmComparativaQA", "DameCheckedUnQaGriProveVars", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
