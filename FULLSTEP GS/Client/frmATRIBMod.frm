VERSION 5.00
Begin VB.Form frmAtribMod 
   Caption         =   "DAtributos"
   ClientHeight    =   6885
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10740
   Icon            =   "frmATRIBMod.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6885
   ScaleWidth      =   10740
   Begin FSGSClient.ctlATRIB ctlAtributos 
      Height          =   6855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   12091
   End
End
Attribute VB_Name = "frmAtribMod"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_sOrigen As String

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas
    Set UonsSeleccionadas = ctlAtributos.UonsSeleccionadas
End Property

Public Property Set UonsSeleccionadas(ByRef oUons As CUnidadesOrganizativas)
    Set ctlAtributos.UonsSeleccionadas = oUons
End Property
Public Property Get g_oOrigen() As Form
    Set g_oOrigen = ctlAtributos.g_oOrigen
End Property

Public Property Set g_oOrigen(ByVal vNewValue As Form)
    Set ctlAtributos.g_oOrigen = vNewValue
End Property

'Public Property Get g_sOrigen() As String
'    g_sOrigen = ctlAtributos.g_sOrigen
'End Property
'
'Public Property Let g_sOrigen(ByVal vNewValue As String)
'    ctlAtributos.g_sOrigen = vNewValue
'End Property

Public Property Get bRComprador() As Boolean
    bRComprador = ctlAtributos.bRComprador
End Property

Public Property Let bRComprador(ByVal vNewValue As Boolean)
    bRComprador = vNewValue
End Property

Public Property Get sAmbitoAtribEsp() As String
    sAmbitoAtribEsp = ctlAtributos.sAmbitoAtribEsp
End Property

Public Property Let sAmbitoAtribEsp(ByVal vNewValue As String)
    ctlAtributos.sAmbitoAtribEsp = vNewValue
End Property

Public Property Get sdbcGMN1_4Cod() As SSDBCombo
    Set sdbcGMN1_4Cod = ctlAtributos.sdbcGMN1_4Cod
End Property

Public Property Get sdbcGMN1_4Den() As SSDBCombo
    Set sdbcGMN1_4Den = ctlAtributos.sdbcGMN1_4Den
End Property

Public Property Get sdbcGMN2_4Cod() As SSDBCombo
    Set sdbcGMN2_4Cod = ctlAtributos.sdbcGMN2_4Cod
End Property

Public Property Get sdbcGMN2_4Den() As SSDBCombo
    Set sdbcGMN2_4Den = ctlAtributos.sdbcGMN2_4Den
End Property

Public Property Get sdbcGMN3_4Cod() As SSDBCombo
    Set sdbcGMN3_4Cod = ctlAtributos.sdbcGMN3_4Cod
End Property

Public Property Get sdbcGMN3_4Den() As SSDBCombo
    Set sdbcGMN3_4Den = ctlAtributos.sdbcGMN3_4Den
End Property

Public Property Get sdbcGMN4_4Cod() As SSDBCombo
    Set sdbcGMN4_4Cod = ctlAtributos.sdbcGMN4_4Cod
End Property

Public Property Get sdbcGMN4_4Den() As SSDBCombo
    Set sdbcGMN4_4Den = ctlAtributos.sdbcGMN4_4Den
End Property

Public Property Get sdbgAtributosGrupo() As SSDBGrid
    Set sdbgAtributosGrupo = ctlAtributos.sdbgAtributosGrupo
End Property

Public Property Get sstabGeneral() As SSTab
    Set sstabGeneral = ctlAtributos.sstabGeneral
End Property

Public Property Get g_oAtributoEnEdicion() As CAtributo
    Set g_oAtributoEnEdicion = ctlAtributos.g_oAtributoEnEdicion
End Property

Public Property Set g_oAtributoEnEdicion(ByVal vNewValue As CAtributo)
    Set ctlAtributos.g_oAtributoEnEdicion = vNewValue
End Property

Public Property Get m_bRespuesta() As Boolean
    m_bRespuesta = ctlAtributos.m_bRespuesta
End Property

Public Property Let m_bRespuesta(ByVal vNewValue As Boolean)
    ctlAtributos.m_bRespuesta = vNewValue
End Property

Public Property Get iAmbitoAtribEsp() As Integer
    iAmbitoAtribEsp = ctlAtributos.iAmbitoAtribEsp
End Property

Public Property Let iAmbitoAtribEsp(ByVal vNewValue As Integer)
    ctlAtributos.iAmbitoAtribEsp = vNewValue
End Property

Public Property Get g_sArtCod() As String
    g_sArtCod = ctlAtributos.g_sArtCod
End Property

Public Property Let g_sArtCod(ByVal vNewValue As String)
    ctlAtributos.g_sArtCod = vNewValue
End Property

Public Property Get g_sGmn1() As String
    g_sGmn1 = ctlAtributos.g_sGmn1
End Property

Public Property Let g_sGmn1(ByVal vNewValue As String)
    ctlAtributos.g_sGmn1 = vNewValue
End Property

Public Property Get g_sGmn2() As String
    g_sGmn2 = ctlAtributos.g_sGmn2
End Property

Public Property Let g_sGmn2(ByVal vNewValue As String)
    ctlAtributos.g_sGmn2 = vNewValue
End Property

Public Property Get g_sGmn3() As String
    g_sGmn3 = ctlAtributos.g_sGmn3
End Property

Public Property Let g_sGmn3(ByVal vNewValue As String)
    ctlAtributos.g_sGmn3 = vNewValue
End Property

Public Property Get g_sGmn4() As String
    g_sGmn4 = ctlAtributos.g_sGmn4
End Property

Public Property Let g_sGmn4(ByVal vNewValue As String)
    ctlAtributos.g_sGmn4 = vNewValue
End Property

Public Property Get cmdSeleccionar() As CommandButton
    Set cmdSeleccionar = ctlAtributos.cmdSeleccionar
End Property

Public Property Get cmdModoEdicion() As CommandButton
    Set cmdModoEdicion = ctlAtributos.cmdModoEdicion
End Property

Public Property Get cmdModoEdicion2() As CommandButton
    Set cmdModoEdicion2 = ctlAtributos.cmdModoEdicion2
End Property

Public Property Get g_GMN1RespetarCombo() As Boolean
    g_GMN1RespetarCombo = ctlAtributos.g_GMN1RespetarCombo
End Property

Public Property Let g_GMN1RespetarCombo(ByVal vNewValue As Boolean)
    ctlAtributos.g_GMN1RespetarCombo = vNewValue
End Property

Public Property Get g_GMN2RespetarCombo() As Boolean
    g_GMN2RespetarCombo = ctlAtributos.g_GMN2RespetarCombo
End Property

Public Property Let g_GMN2RespetarCombo(ByVal vNewValue As Boolean)
    ctlAtributos.g_GMN2RespetarCombo = vNewValue
End Property

Public Property Get g_GMN3RespetarCombo() As Boolean
    g_GMN3RespetarCombo = ctlAtributos.g_GMN3RespetarCombo
End Property

Public Property Let g_GMN3RespetarCombo(ByVal vNewValue As Boolean)
    ctlAtributos.g_GMN3RespetarCombo = vNewValue
End Property

Public Property Get g_GMN4RespetarCombo() As Boolean
    g_GMN4RespetarCombo = ctlAtributos.g_GMN4RespetarCombo
End Property

Public Property Let g_GMN4RespetarCombo(ByVal vNewValue As Boolean)
    ctlAtributos.g_GMN4RespetarCombo = vNewValue
End Property

Public Property Get txtCod() As TextBox
    Set txtCod = ctlAtributos.txtCod
End Property

Public Property Get optOption1() As OptionButton
    Set optOption1 = ctlAtributos.optOption1
End Property

Public Property Get optOption2() As OptionButton
    Set optOption2 = ctlAtributos.optOption2
End Property

Public Property Get optOption3() As OptionButton
    Set optOption3 = ctlAtributos.optOption3
End Property

Public Property Get optOption4() As OptionButton
    Set optOption4 = ctlAtributos.optOption4
End Property

Public Property Get optOption5() As OptionButton
    Set optOption5 = ctlAtributos.optOption5
End Property

Public Property Get chkCostes() As CheckBox
    Set chkCostes = ctlAtributos.Costes
End Property

Public Property Get chkDtos() As CheckBox
    Set chkDtos = ctlAtributos.Dtos
End Property

Public Property Get fraTipo() As Frame
    Set fraTipo = ctlAtributos.fraTipo
End Property

Public Property Get g_idAtribCod() As Variant
    g_idAtribCod = ctlAtributos.g_idAtribCod
End Property

Public Property Let g_idAtribCod(ByVal vNewValue As Variant)
    ctlAtributos.g_idAtribCod = vNewValue
End Property

Public Property Get g_bSoloSeleccion() As Boolean
    g_bSoloSeleccion = ctlAtributos.g_bSoloSeleccion
End Property

Public Property Let g_bSoloSeleccion(ByVal vNewValue As Boolean)
    ctlAtributos.g_bSoloSeleccion = vNewValue
End Property

Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
    ctlAtributos.sdbcGMN1_4CodCtrl_Validate Cancel
End Sub

Public Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)
    ctlAtributos.sdbcGMN2_4CodCtrl_Validate Cancel
End Sub

Public Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)
    ctlAtributos.sdbcGMN3_4CodCtrl_Validate Cancel
End Sub

Public Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)
    ctlAtributos.sdbcGMN4_4CodCtrl_Validate Cancel
End Sub

Private Sub Form_Load()
    ctlAtributos.SetContainer Me
    
    Me.Width = 10600
    Me.Height = 7455
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    CargarRecursos
    ctlAtributos.g_sOrigen = g_sOrigen
    ctlAtributos.Initialize
End Sub

''' <summary>Carga los recursos necesarios del modulo</summary>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ATRIB, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        For i = 1 To 19
            Ador.MoveNext
        Next
        Me.caption = Ador(0).Value
    End If
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        If Me.Width >= 10600 Or Me.Height >= 7455 Then
            ctlAtributos.Height = Me.ScaleHeight
            ctlAtributos.Width = Me.ScaleWidth
        Else
            Me.Width = 10600
            Me.Height = 7455
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Me.Visible = False
End Sub

