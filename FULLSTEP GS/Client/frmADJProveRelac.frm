VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmADJProveRelac 
   Caption         =   "DProveedor Relacionado"
   ClientHeight    =   4845
   ClientLeft      =   6975
   ClientTop       =   825
   ClientWidth     =   9720
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADJProveRelac.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4845
   ScaleWidth      =   9720
   ShowInTaskbar   =   0   'False
   Begin SSDataWidgets_B.SSDBDropDown sdbddCodERP 
      Height          =   915
      Left            =   2040
      TabIndex        =   4
      Top             =   1500
      Width           =   3255
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJProveRelac.frx":0CB2
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   159
      Columns.Count   =   2
      Columns(0).Width=   5292
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "COD"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgProveRelac 
      Height          =   4515
      Left            =   30
      TabIndex        =   3
      Top             =   30
      Width           =   9645
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   6
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJProveRelac.frx":0CCE
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   6
      Columns(0).Width=   2461
      Columns(0).Caption=   "Proveedor relacionado"
      Columns(0).Name =   "PROVE_REL"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6006
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3096
      Columns(2).Caption=   "C�digo ERP"
      Columns(2).Name =   "COD_ERP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2223
      Columns(3).Caption=   "Transfer a ERP"
      Columns(3).Name =   "TRANSF_ERP"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   2249
      Columns(4).Caption=   "Cantidad"
      Columns(4).Name =   "CANT"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID_TRASLADO"
      Columns(5).Name =   "ID_TRASLADO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   17013
      _ExtentY        =   7964
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      BorderStyle     =   0  'None
      Height          =   345
      Left            =   3510
      ScaleHeight     =   345
      ScaleWidth      =   2235
      TabIndex        =   0
      Top             =   4560
      Width           =   2235
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   0
         TabIndex        =   2
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   1200
         TabIndex        =   1
         Top             =   0
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmADJProveRelac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_oOrigen As Form
Public m_oProveedoresRelacionados As CItemProves
Public m_dCantidadAdjudic As Variant
Public m_sUnidades As String
Public m_sIdItem As String
Public m_sProvePrincipal As String
Public m_lEmpresa As Long
Private m_dCantidadTotal As Double
Private bProveModif As Boolean
Private m_bComboProve As Boolean
Private m_bError As Boolean
Public m_sMensaje1 As String
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean


Private m_sMsgError As String
Private Sub cmdAceptar_Click()
Dim oItemProve As CItemProve
Dim bEncontrado As Boolean
Dim i As Integer
Dim vb As Variant
Dim irespuesta As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgProveRelac.DataChanged Then
        sdbgProveRelac.Update
        If m_bError Then Exit Sub
    End If
    If bProveModif Then 'Si se ha modificado alg�n proveedor relacionado
        If m_dCantidadTotal < NullToDbl0(m_dCantidadAdjudic) Then
            'La cantidad distribuida es inferior a la cantidad adjudicada para el item
            irespuesta = oMensajes.MensajeYesNo(m_sMensaje1 & " " & CStr(m_dCantidadAdjudic) & " " & m_sUnidades)
            If irespuesta = vbNo Then
                'Unload Me
                Exit Sub
            End If
        End If
        For i = 0 To sdbgProveRelac.Rows - 1
            vb = sdbgProveRelac.AddItemBookmark(i)
            GuardarProvesRelacionados m_sIdItem, sdbgProveRelac.Columns("ID_TRASLADO").CellValue(vb), sdbgProveRelac.Columns("PROVE_REL").CellValue(vb), sdbgProveRelac.Columns("DEN").CellValue(vb), sdbgProveRelac.Columns("COD_ERP").CellValue(vb), GridCheckToBoolean(sdbgProveRelac.Columns("TRANSF_ERP").CellValue(vb)), IIf(sdbgProveRelac.Columns("CANT").CellValue(vb) = "", 0, StrToNull(sdbgProveRelac.Columns("CANT").CellValue(vb))), m_dCantidadAdjudic, m_lEmpresa, m_sProvePrincipal
        Next
    End If
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Memoriza los proveedores relacionados desde la ventana emergente de proveedores relacionados
''' </summary>
''' <param name="IdItem">Identificador del �tem</param>
''' <param name="IdTraslado">Identificador del traslado</param>
''' <param name="CodProveedor">C�digo de Proveedor </param>
''' <param name="DenProveedor">Denominaci�n de Proveedor </param>
''' <param name="CodERP">c�digo ERP</param>
''' <param name="TransferErp">Si True es que se quiere hacer la transferencia del proveedor</param>
''' <param name="CantidadAdjudicada">Cantidad adjudicada</param>
''' <param name="Empresa">Empresa</param>
''' <param name="ProvePr">Proveedor Principal</param>
''' <remarks>Llamada desde: frmADJProveRelac.cmdAceptar_Click; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 18/10/2011</revision>
Private Sub GuardarProvesRelacionados(ByVal IdItem As Long, ByVal IdTraslado As Long, ByVal CodProveedor As String, ByVal DenProveedor As String, _
ByVal CodERP As String, ByVal TransferErp As Boolean, ByVal Cantidad As Variant, ByVal CantidadAdjudicada As Variant, ByVal Empresa As Long, ByVal ProvePr As String)
 Dim oTraslado As CTraslado
 Dim dblCant As Variant
 Dim oRow As SSRow
 Dim oRowP As SSRow
    Dim scod1 As String
    Dim scod2 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    scod1 = ProvePr & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(ProvePr))
    scod2 = CStr(Empresa)
    Set oTraslado = g_oOrigen.m_oTraslados.Item(scod1 & scod2)
    
    dblCant = Null
    If Not IsNull(Cantidad) And Not IsNull(CantidadAdjudicada) Then
        If NullToDbl0(CantidadAdjudicada) <> 0 Then
            dblCant = NullToDbl0(Cantidad) / CantidadAdjudicada
        End If
    End If
    
    If oTraslado.ProvesRelacionados.Item("0_" & CodProveedor) Is Nothing Then Exit Sub
    If oTraslado.ProvesRelacionados.Item("0_" & CodProveedor).Items Is Nothing Then Exit Sub
    With oTraslado.ProvesRelacionados.Item("0_" & CodProveedor).Items
        If .Item(CStr(IdItem)) Is Nothing Then
            .Add IdTraslado, IdItem, CodProveedor, DenProveedor, CodERP, TransferErp, NullToStr(dblCant), Cantidad, CantidadAdjudicada
        Else
            .Item(CStr(IdItem)).Cantidad = Cantidad
            .Item(CStr(IdItem)).TransferErp = TransferErp
            .Item(CStr(IdItem)).Porcen = NullToStr(dblCant)
            .Item(CStr(IdItem)).CodERP = CodERP
        End If
        If oTraslado.ProvesRelacionados.Item("0_" & CodProveedor).CodERP <> .Item(CStr(IdItem)).CodERP Then
            oTraslado.ProvesRelacionados.Item("0_" & CodProveedor).CodERP = .Item(CStr(IdItem)).CodERP
            Set oRow = g_oOrigen.m_oCell.Row.GetParent 'Cojo la fila de la empresa
            Set oRowP = oRow.GetChild(ssChildRowFirst) 'El primer relacionado incluido el mismo
            If oRowP.Cells("PROVES").Value = CodProveedor Then
                oRowP.Cells("MOD_COD_ERP").Value = .Item(CStr(IdItem)).CodERP
                oRowP.Cells("CAMPO_ERP_READ").Value = .Item(CStr(IdItem)).CodERP
            Else
                Do While oRowP.HasNextSibling And oRowP.Band.Index = 1
                    Set oRowP = oRowP.GetSibling(ssSiblingRowNext)
                    If oRowP.Cells("PROVES").Value = CodProveedor Then
                        oRowP.Cells("MOD_COD_ERP").Value = .Item(CStr(IdItem)).CodERP
                        oRowP.Cells("CAMPO_ERP_READ").Value = .Item(CStr(IdItem)).CodERP
                        Exit Do
                    End If
                Loop
            End If
            .Item(CStr(IdItem)).ItemModificado = True
            oTraslado.ProvesRelacionados.Item("0_" & CodProveedor).ItemModificado = True
            g_oOrigen.m_oCell.Appearance.Picture = LoadPicture(App.Path & "\check_black.gif")
            g_oOrigen.m_oCell.Appearance.PictureAlign = ssAlignCenter
        End If
        If oTraslado.ProvesRelacionados.Item("0_" & CodProveedor).Porcen <> StrToDbl0(.Item(CStr(IdItem)).Porcen) Then
            .Item(CStr(IdItem)).ItemModificado = True
            oTraslado.ProvesRelacionados.Item("0_" & CodProveedor).ItemModificado = True
            g_oOrigen.m_oCell.Appearance.Picture = LoadPicture(App.Path & "\alerta.gif")
            g_oOrigen.m_oCell.Appearance.PictureAlign = ssAlignCenter
        End If
        g_oOrigen.sdbgERP.Update
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "GuardarProvesRelacionados", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga la pagina
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub Form_Load()
    
    Dim oItemProve As CItemProve
    Dim Cantidad As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    m_bActivado = False
    Me.Height = 3720
    Me.Width = 9840
    
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2


    bProveModif = False
    m_dCantidadTotal = 0
    PonerFieldSeparator Me
    sdbddCodERP.AddItem ""
    For Each oItemProve In m_oProveedoresRelacionados
        If oItemProve.CantAdj = "" Then
            If oItemProve.Porcen = "" Then
                Cantidad = ""
            ElseIf oItemProve.Porcen = 0 Then
                Cantidad = ""
            Else
                Cantidad = oItemProve.Porcen * m_dCantidadAdjudic
            End If
        Else
            '21802 Cuando se ha modificado el porcentaje se debe recuperar en lugar de la cantidad adjudicada
            If StrToDbl0(oItemProve.Porcen) = 0 Then
                Cantidad = oItemProve.CantAdj
            Else
                Cantidad = oItemProve.Porcen * m_dCantidadAdjudic
            End If
        End If
        sdbgProveRelac.AddItem oItemProve.CodProveedor & Chr(m_lSeparador) & Replace(oItemProve.DenProveedor, "'", "") & Chr(m_lSeparador) & oItemProve.CodERP & Chr(m_lSeparador) & BooleanToSQLBinary(oItemProve.TransferErp) & Chr(m_lSeparador) & NullToStr(Cantidad) & Chr(m_lSeparador) & oItemProve.IdTraslado
        If IsNumeric(Cantidad) Then m_dCantidadTotal = m_dCantidadTotal + Cantidad
    Next
    
    sdbgProveRelac.Columns("CANT").NumberFormat = "#,##0.00"
    sdbgProveRelac.Columns("COD_ERP").DropDownHwnd = sdbddCodERP.hWnd
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub




Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Width < 500 Then Exit Sub
    If Me.Height < 1500 Then Exit Sub

    sdbgProveRelac.Width = Me.Width - 150
    If picEdit.Visible Then
        sdbgProveRelac.Height = Me.Height - 850
    Else
        sdbgProveRelac.Height = Me.Height - 450
    End If
    If sdbgProveRelac.Columns("CANT").Visible Then
        sdbgProveRelac.Columns("PROVE_REL").Width = (sdbgProveRelac.Width - 245) * 0.17
        sdbgProveRelac.Columns("DEN").Width = (sdbgProveRelac.Width - 245) * 0.36
        sdbgProveRelac.Columns("COD_ERP").Width = (sdbgProveRelac.Width - 245) * 0.17
        sdbgProveRelac.Columns("TRANSF_ERP").Width = (sdbgProveRelac.Width - 245) * 0.12
        sdbgProveRelac.Columns("CANT").Width = (sdbgProveRelac.Width - 245) * 0.12
    Else
        sdbgProveRelac.Columns("PROVE_REL").Width = (sdbgProveRelac.Width - 245) * 0.2
        sdbgProveRelac.Columns("DEN").Width = (sdbgProveRelac.Width - 245) * 0.4
        sdbgProveRelac.Columns("COD_ERP").Width = (sdbgProveRelac.Width - 245) * 0.2
        sdbgProveRelac.Columns("TRANSF_ERP").Width = (sdbgProveRelac.Width - 245) * 0.14
        sdbgProveRelac.Columns("CANT").Width = (sdbgProveRelac.Width - 245) * 0.14
    End If
    picEdit.Top = sdbgProveRelac.Top + sdbgProveRelac.Height + 65
    picEdit.Left = Me.Width / 2 - picEdit.Width / 2
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

Private Sub Form_Unload(Cancel As Integer)
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set m_oProveedoresRelacionados = Nothing
m_dCantidadAdjudic = Null
m_sUnidades = ""
m_sIdItem = ""
m_sProvePrincipal = ""
m_lEmpresa = 0
m_dCantidadTotal = 0
bProveModif = False
m_bComboProve = False
m_bError = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Controla q los cambios recien hechos sean correctos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgProveRelac_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    'If sdbgProveRelac.Columns(sdbgProveRelac.col).Name <> "CANT" Then Exit Sub
    'm_oProveedoresRelacionados.Item(m_sIdItem & "_" & sdbgProveRelac.Columns("PROVE_REL").Value).cantidad = sdbgProveRelac.Columns("CANT").Value
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ColIndex = 3 Then
        If sdbgProveRelac.Columns("TRANSF_ERP").Value = False Then
            sdbgProveRelac.Columns("CANT").Value = 0
        End If
    End If
    If ColIndex = 4 Then
        If sdbgProveRelac.Columns("CANT").Value <> "" Then
            If Not IsNumeric(sdbgProveRelac.Columns("CANT").Value) Then
                oMensajes.NoValido sdbgProveRelac.Columns("CANT").caption
                Cancel = True
                Exit Sub
            End If
        End If
    
        If StrToDbl0(sdbgProveRelac.Columns("CANT").Value) > 0 And sdbgProveRelac.Columns("TRANSF_ERP").Value = False Then
            If sdbgProveRelac.Columns("COD_ERP").Value = "" Then
                sdbgProveRelac.Columns("CANT").Value = ""
                oMensajes.SeleccionarCodERP
                Cancel = True
                Exit Sub
            Else
                sdbgProveRelac.Columns("TRANSF_ERP").Value = True
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbgProveRelac_BeforeColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub sdbgProveRelac_BeforeUpdate(Cancel As Integer)
    'Comprobar que la suma de cantidades no supere el total adjudicado
    Dim cantidadNueva As Double
    Dim i As Integer
    Dim vb As Variant
    Dim actual As Variant
    Dim oItemProve As CItemProve
    Dim Cantidad As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bError = False
    'cantidadNueva = NullToDbl0(sdbgProveRelac.Columns("CANT").Value)
    
    'Compruebo si se ha modificado la cantidad
      Set oItemProve = m_oProveedoresRelacionados.Item("0_" & sdbgProveRelac.Columns("PROVE_REL").Value)
     If Not oItemProve Is Nothing Then
        If oItemProve.Porcen = "" Then
            Cantidad = ""
        Else
            Cantidad = oItemProve.Porcen * m_dCantidadAdjudic
        End If
        If VarToDbl0(Cantidad) <> VarToDbl0(sdbgProveRelac.Columns("CANT").Value) Then
            oItemProve.ItemModificado = True
        End If
        If oItemProve.TransferErp <> sdbgProveRelac.Columns("TRANSF_ERP").Value Then
            oItemProve.ItemModificado = True
        End If
    End If

    m_dCantidadTotal = 0
    'Se actualiza la cantidad total
    For i = 0 To sdbgProveRelac.Rows - 1
        vb = sdbgProveRelac.AddItemBookmark(i)

        If CStr(sdbgProveRelac.Bookmark) = CStr(vb) Then
            If IsNumeric(sdbgProveRelac.Columns("CANT").Value) Then
                m_dCantidadTotal = m_dCantidadTotal + CDbl(sdbgProveRelac.Columns("CANT").Value)
            End If
        Else
            If IsNumeric(sdbgProveRelac.Columns("CANT").CellValue(vb)) Then
                m_dCantidadTotal = m_dCantidadTotal + StrToDbl0(sdbgProveRelac.Columns("CANT").CellValue(vb))
            End If
        End If
    Next i

    If m_dCantidadTotal > m_dCantidadAdjudic Then
        'La cantidad distribuida es superior a la cantidad adjudicada para el item
        oMensajes.MensajeOKOnly 1253, Critical
        m_bError = True
        Cancel = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbgProveRelac_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbgProveRelac_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bProveModif = True
    If sdbgProveRelac.Columns(sdbgProveRelac.Col).Name = "TRANSF_ERP" Then
        If GridCheckToBoolean(sdbgProveRelac.Columns(sdbgProveRelac.Col).Value) = True And sdbgProveRelac.Columns("COD_ERP").Value = "" Then
            oMensajes.SeleccionarCodERP
            sdbgProveRelac.CancelUpdate
            bProveModif = False
            Exit Sub
        End If
    End If
    
    'sdbgProveRelac.Update
'    sdbgProveRelac.MoveFirst
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbgProveRelac_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddCodERP_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bComboProve = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbddCodERP_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddCodERP_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If (m_bComboProve = False) Then
        Exit Sub
    End If
    
    m_bComboProve = False

    If sdbddCodERP.Columns("COD").Value <> "" Then
        sdbgProveRelac.Columns("COD_ERP").Value = sdbddCodERP.Columns("COD").Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbddCodERP_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

''' <summary>
''' Carga los Erps de la empresa
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddCodERP_DropDown()
Dim oEmpresa As CEmpresa
Dim oERP As CProveERP
Dim oProvesERP As CProveERPs

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgProveRelac.Columns("COD_ERP").Locked Then
        sdbddCodERP.DroppedDown = False
        Exit Sub
    End If
    
    m_bComboProve = False
    
    sdbddCodERP.RemoveAll

    Set oEmpresa = oFSGSRaiz.Generar_CEmpresa
    oEmpresa.Id = m_lEmpresa
    Set oProvesERP = Nothing
    Set oProvesERP = oEmpresa.CargarERPs(sdbgProveRelac.Columns("PROVE_REL").Value)
    
    If Not oProvesERP Is Nothing Then
        For Each oERP In oProvesERP
            sdbddCodERP.AddItem oERP.Cod & " (" & oERP.Den & ")" & Chr(m_lSeparador) & oERP.Cod
        Next
    End If
    
    If sdbddCodERP.Rows = 0 Then sdbddCodERP.AddItem ""
    
    '''Set oProvesERP = Nothing
    Set oEmpresa = Nothing
    Set oProvesERP = Nothing
    
    sdbgProveRelac.ActiveCell.SelStart = 0
    sdbgProveRelac.ActiveCell.SelLength = Len(sdbgProveRelac.ActiveCell.Text)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbddCodERP_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbddCodERP_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddCodERP.DataFieldList = "Column 0"
    sdbddCodERP.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbddCodERP_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbddCodERP_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    sdbddCodERP.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddCodERP.Rows - 1
            bm = sdbddCodERP.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddCodERP.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddCodERP.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbddCodERP_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbddCodERP_ValidateList(Text As String, RtnPassed As Integer)
Dim oEmpresa As CEmpresa
Dim oERP As CProveERP
Dim oProvesERP As CProveERPs


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Text = "" Then
        RtnPassed = True

    Else

        ''' Comprobar la existencia en la lista
        If sdbddCodERP.Columns(1).Value = Text Then
            RtnPassed = True
            Exit Sub
        End If

        'Comprueba que el proveedor exista
        Set oProvesERP = oFSGSRaiz.Generar_CProveERPs
        oProvesERP.CargarProveedoresERP m_lEmpresa, sdbgProveRelac.Columns("PROVE_REL").Value, sdbgProveRelac.Columns("COD_ERP").Value
        
        If oProvesERP.Count = 0 Then
            'Comprobar que el proveedor introducido existe
            sdbgProveRelac.Columns("COD_ERP").Value = ""
        Else
            sdbgProveRelac.Columns("COD_ERP").Value = oProvesERP.Item(1).Cod
            RtnPassed = True

        End If
        Set oProvesERP = Nothing

    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbddCodERP_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgProveRelac_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgProveRelac.Col = 2 Then
        sdbddCodERP.DroppedDown = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJProveRelac", "sdbgProveRelac_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub
