VERSION 5.00
Object = "{DE173711-6CFE-432E-A95E-F4EF3EE03231}#5.4#0"; "AddFlow5.ocx"
Begin VB.Form frmFlujos 
   Caption         =   "DSolicitud"
   ClientHeight    =   9600
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13485
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujos.frx":0000
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   9600
   ScaleWidth      =   13485
   Begin VB.CommandButton cmdNotifExp 
      Height          =   312
      Left            =   8080
      Picture         =   "frmFlujos.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   60
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   6570
      TabIndex        =   13
      Top             =   9210
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   5370
      TabIndex        =   12
      Top             =   9210
      Width           =   1005
   End
   Begin AddFlow5Lib.AddFlow flujo 
      Height          =   8520
      Left            =   0
      TabIndex        =   11
      Top             =   480
      Width           =   13455
      _Version        =   327684
      _ExtentX        =   23733
      _ExtentY        =   15028
      _StockProps     =   229
      BackColor       =   16777215
      BorderStyle     =   1
      ScrollBars      =   3
      Shape           =   1
      LinkStyle       =   0
      Alignment       =   7
      AutoSize        =   0
      ArrowDst        =   3
      ArrowOrg        =   0
      DrawStyle       =   0
      DrawWidth       =   1
      ReadOnly        =   0   'False
      MultiSel        =   -1  'True
      CanDrawNode     =   0   'False
      CanDrawLink     =   -1  'True
      CanMoveNode     =   -1  'True
      CanSizeNode     =   -1  'True
      CanStretchLink  =   -1  'True
      CanMultiLink    =   -1  'True
      Transparent     =   0   'False
      ShowGrid        =   0   'False
      Hidden          =   0   'False
      Rigid           =   0   'False
      DisplayHandles  =   -1  'True
      AutoScroll      =   -1  'True
      xGrid           =   8
      yGrid           =   8
      xZoom           =   100
      yZoom           =   100
      FillColor       =   16777215
      DrawColor       =   0
      ForeColor       =   0
      BackPicture     =   "frmFlujos.frx":0DA4
      MouseIcon       =   "frmFlujos.frx":0DC0
      AdjustOrg       =   0   'False
      AdjustDst       =   0   'False
      CanReflexLink   =   -1  'True
      SnapToGrid      =   0   'False
      ShowToolTip     =   -1  'True
      ScrollTrack     =   -1  'True
      AllowArrowKeys  =   -1  'True
      ProportionalBars=   -1  'True
      PicturePosition =   9
      LinkCreationMode=   0
      GridStyle       =   0
      ShapeOrientation=   0
      ArrowMid        =   0
      SelectAction    =   0
      GridColor       =   0
      OrthogonalDynamic=   0   'False
      OrientedText    =   0   'False
      EditMode        =   0
      Shadow          =   1
      ShadowColor     =   12632256
      BackMode        =   1
      Ellipsis        =   0
      SelectionHandleSize=   6
      LinkingHandleSize=   6
      xShadowOffset   =   8
      yShadowOffset   =   8
      CanUndoRedo     =   -1  'True
      UndoSize        =   0
      ShowPropertyPages=   0
      NoPrefix        =   0   'False
      MaxInDegree     =   -1
      MaxOutDegree    =   -1
      MaxDegree       =   -1
      CycleMode       =   0
      LogicalOnly     =   0   'False
      ShowJump        =   0
      SizeArrowDst    =   0
      SizeArrowOrg    =   0
      SizeArrowMid    =   0
      ScrollWheel     =   -1  'True
      RemovePointAngle=   2
      ZeroOriginForExport=   0   'False
      CanFireError    =   0   'False
      RoundedCorner   =   0   'False
      JumpSize        =   1
      RoundedCornerSize=   1
      Autorouting     =   0   'False
      RouteStartMethod=   0
      RouteGrain      =   8
      RouteMinDistance=   16
      NodeOwnerDraw   =   0   'False
      LinkOwnerDraw   =   0   'False
      OwnerDraw       =   0   'False
      EditHardReturn  =   0
      Gradient        =   0   'False
      Gradient        =   0
      GradientColor   =   14811135
   End
   Begin VB.CommandButton cmdGuardar 
      Height          =   312
      Left            =   6840
      Picture         =   "frmFlujos.frx":0DDC
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   63
      Width           =   375
   End
   Begin VB.CommandButton cmdAnyadir 
      Height          =   312
      Left            =   5880
      Picture         =   "frmFlujos.frx":111E
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   63
      Width           =   432
   End
   Begin VB.CommandButton cmdEliminar 
      Enabled         =   0   'False
      Height          =   312
      Left            =   6360
      Picture         =   "frmFlujos.frx":11A0
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   63
      Width           =   432
   End
   Begin VB.CommandButton cmdEditar 
      Enabled         =   0   'False
      Height          =   312
      Left            =   5460
      Picture         =   "frmFlujos.frx":1232
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   63
      Width           =   375
   End
   Begin VB.CommandButton cmdConfEtapa 
      Height          =   285
      Left            =   13480
      Picture         =   "frmFlujos.frx":138C
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Dise�o de vistas"
      Top             =   60
      Width           =   315
   End
   Begin VB.CommandButton cmdBloqueos 
      Height          =   312
      Left            =   7665
      Picture         =   "frmFlujos.frx":16CE
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   63
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton cmdRoles 
      Height          =   312
      Left            =   7260
      Picture         =   "frmFlujos.frx":190B
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   63
      Width           =   375
   End
   Begin VB.Label lblEtapa 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   9760
      TabIndex        =   9
      Top             =   60
      Width           =   3645
   End
   Begin VB.Label lblFlujo 
      Caption         =   "DFlujo de trabajo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label lblDenFlujo 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1680
      TabIndex        =   1
      Top             =   90
      Width           =   3645
   End
   Begin VB.Label lblConfEtapa 
      Caption         =   "DEtapa:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8620
      TabIndex        =   8
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmFlujos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables Privadas
Private WithEvents cP As cPopupMenu
Attribute cP.VB_VarHelpID = -1
Private frmX As Single
Private frmY As Single
Private oBloques As CBloques
Private oBloque As CBloque
Private oEnlaces As CEnlaces
Private oEnlace As CEnlace
Private m_bModifDistrFlujo As Boolean
Private m_oIBaseDatos As IBaseDatos
Private m_bCargando As Boolean
Private oSolicitud As CSolicitud
Private m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String
Private m_bCancelado As Boolean
'Variable necesaria para saber si se han realizado cambios en alguna de las partes del flujo o de sus dependencias
Private m_bConCambios As Boolean

'Variables Publicas
Public m_lIdFlujo As Long
Public m_lIdFormulario As Long
Public m_lIdInstancia As Long
Public m_sSolicitud As String
Public g_lSolicitud As Long
Public m_bModifFlujo As Boolean
Public g_lSolicitudTipo As Integer
Public g_iSolicitudTipoTipo As Integer
Public g_lIdSolicitud As Long
Public g_sTipoWorkflow As String
Public g_bDesdeInstancia As Boolean

'IDIOMAS
Private m_sCaption As String
Private m_sCaptionInstancia As String
Private m_sCPAddEtapa As String
Private m_sCPAddAccion As String
Private m_sCPDetalleEtapa As String
Private m_sCPDetalleAccion As String
Private m_sCPNomEtapa As String
Private m_sCPDelEtapa As String
Private m_sCPDelAccion As String
Private m_sCPConfEtapa As String

Private Sub CancelarCambios()
Dim oWorkflow As CWorkflows
Dim teserror As TipoErrorSummit
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Screen.MousePointer = vbHourglass
teserror.NumError = TESnoerror
Set oWorkflow = oFSGSRaiz.generar_CWorkflows

teserror = oWorkflow.Restaurar_desde_Copia(m_lIdFlujo)
If teserror.NumError <> TESnoerror Then
    GoTo ERROR
End If
'Despues de actualizar todo inicializamos la variable
teserror = oWorkflow.TratarPendienteCopia(m_lIdFlujo, 0)
If teserror.NumError <> TESnoerror Then
    GoTo ERROR
End If
'Despues de deshacer todo inicializamos la variable
m_bConCambios = False
Screen.MousePointer = vbNormal
Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        Screen.MousePointer = vbNormal
        
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "CancelarCambios", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Guarda en BD los cambios efectuados en el flujo gr�fico</summary>
''' <remarks>Llamada desde ConfigurarEtapa,DetalleEnlace,Form_Unload,cmdGuardar_Click ; Tiempo m�ximo: 0,1seg</remarks>
''' <revision>MMV 01/12/2011</revision>
Private Sub GuardarCambios()
Dim oWorkflow As CWorkflows
Dim teserror As TipoErrorSummit
Dim sCampo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
teserror.NumError = TESnoerror
Screen.MousePointer = vbHourglass
Set oWorkflow = oFSGSRaiz.generar_CWorkflows
sCampo = "WORKFLOW"
Select Case g_sTipoWorkflow
    Case "BAJA", "MODIF"
        sCampo = sCampo & "_" & g_sTipoWorkflow
End Select

teserror = oWorkflow.Eliminar_Copia(m_lIdFlujo, sCampo)
If teserror.NumError <> TESnoerror Then
    GoTo ERROR
End If
    
teserror = oWorkflow.Crear_Copia(m_lIdFlujo)
If teserror.NumError <> TESnoerror Then
    GoTo ERROR
End If

'Despues de actualizar todo inicializamos la variable
teserror = oWorkflow.TratarPendienteCopia(m_lIdFlujo, 0)
If teserror.NumError <> TESnoerror Then
    GoTo ERROR
End If
m_bConCambios = False
Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    Screen.MousePointer = vbNormal
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "GuardarCambios", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Se llama al m�todo que actualiza el campo PENDIENTE_COPIA para sabesr si hay que actualizalas tablas temporales de flujo
''' Tambi�n se actualiza la variable de control para no tener que realizar la actualizaci�n m�s de una vez
''' </summary>
''' <param name="iMarca">1=Activado con cambios en datos relacionados del flujo; 0=Desactivar, cancelaci�n o guardado del flujo</param name>
''' <remarks>Llamada desde: frmFlujos.frm, frmFlujosAnyadirEtapa.frm, frmFlujosConfEtapa.frm, frmFlujosDetalleAccion.frm, frmFujosNombreEtapa.frm
''' frmFlujosRoles, frmBloqueosCondiciones, frmBloqueoFlujos</remarks>
Public Sub HayCambios(Optional ByVal iMarca As Integer = 1)
Dim oWorkflows As CWorkflows
Dim teserror As TipoErrorSummit
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bConCambios Then
    'Marcamos la tabla de WORKFLOW PENDIENTE_CAMBIO
    Set oWorkflows = oFSGSRaiz.generar_CWorkflows
    teserror = oWorkflows.TratarPendienteCopia(m_lIdFlujo, iMarca)
    If teserror.NumError = TESnoerror And iMarca = 1 Then
        'Ponemos la variable a true para que se sepa que hay cambios y no hay que poner otra vez la variable a 1
        m_bConCambios = True
        'Habilitamos los botones para guardar o cancelar
        cmdAceptar.Enabled = True
        cmdCancelar.Enabled = True
    End If
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "HayCambios", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub cmdAceptar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oMensajes.GuardarFlujoTrabajo = vbYes Then
    'Si se pulsa "S�" se guardar�n todos los cambios realizados
    GuardarCambios
Else
    'Si se pulsa "No" se cancelan todos los cambios realizados
    CancelarCambios
End If
Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub cmdAnyadir_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If cmdAnyadir.ToolTipText = m_sCPAddEtapa Then
        frmX = 0
        frmY = 0
        AnyadirEtapa
    ElseIf cmdAnyadir.ToolTipText = m_sCPAddAccion Then
        If Not flujo.SelectedNode Is Nothing Then
            AnyadirEnlace CLng(val(flujo.SelectedNode.key))
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdAnyadir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdBloqueos_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmBloqueoFlujos.g_lSolicitud = g_lSolicitud
    frmBloqueoFlujos.m_lIdFlujo = m_lIdFlujo
    frmBloqueoFlujos.m_bModifFlujo = m_bModifFlujo
    frmBloqueoFlujos.m_lIdFormulario = m_lIdFormulario
    frmBloqueoFlujos.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdBloqueos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bCancelado = True
CancelarCambios
Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdConfEtapa_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarEtapa
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdConfEtapa_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdEditar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If cmdEditar.ToolTipText = m_sCPNomEtapa Then
        CambiarNombreEtapa
    ElseIf cmdEditar.ToolTipText = m_sCPDetalleAccion Then
        DetalleEnlace
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdEditar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdEliminar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If cmdEliminar.ToolTipText = m_sCPDelEtapa Then
        EliminarEtapa
    ElseIf cmdEliminar.ToolTipText = m_sCPDelAccion Then
        EliminarEnlace
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdEliminar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdGuardar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    GuardarDistribucionFlujo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdGuardar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Guarda en BD los cambios efectuados en el flujo gr�fico</summary>
''' <remarks>Llamada desde ConfigurarEtapa,DetalleEnlace,Form_Unload,cmdGuardar_Click ; Tiempo m�ximo: 0,1seg</remarks>
''' <revision>MMV 01/12/2011</revision>
Private Sub GuardarDistribucionFlujo()
    Dim teserror As TipoErrorSummit
    Dim nodo As afNode
    Dim link As afLink
    Dim punto As afLinkPoint
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    If m_bModifDistrFlujo Then
        For Each nodo In flujo.Nodes
            Set oBloque = oBloques.Item(CStr(val(nodo.key)))
            Set m_oIBaseDatos = oBloque
            teserror = m_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oBloque = Nothing
                Set m_oIBaseDatos = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            For Each link In nodo.OutLinks
                Set oEnlace = oEnlaces.Item(Split(link.key, "&&")(1))
                If Not oEnlace Is Nothing Then
                    oEnlace.ExtraPoints.clear
                    For i = 1 To link.ExtraPoints.Count - 2
                        Set punto = link.ExtraPoints(i)
                        If punto.X > 0 And punto.Y > 0 Then
                            oEnlace.ExtraPoints.Add punto.X, punto.Y, CLng(i)
                        End If
                    Next
                    
                    Set m_oIBaseDatos = oEnlace
                    teserror = m_oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oEnlace = Nothing
                        Set m_oIBaseDatos = Nothing
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
            Next
        Next
        If flujo.Nodes.Count > 0 Then
            'Se llama a la funcion para actualizar el campo PENDIENTE_COPIA
            HayCambios
        End If
    End If
    Set oBloque = Nothing
    Set oEnlace = Nothing
    Set m_oIBaseDatos = Nothing
    m_bModifDistrFlujo = False
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "GuardarDistribucionFlujo", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdNotifExp_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
frmNotifExpiracion.lIdFlujo = m_lIdFlujo
frmNotifExpiracion.m_lIdFormulario = m_lIdFormulario
frmNotifExpiracion.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdNotifExp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Se le pasa al nuevo formulario las variables de este y se abre
''' </summary>
''' <remarks>Tiempo m�ximo:0seg.</remarks>
Private Sub cmdRoles_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmFlujosRoles.lIdFlujo = m_lIdFlujo
    frmFlujosRoles.lIdFormulario = m_lIdFormulario
    frmFlujosRoles.m_bModifFlujo = m_bModifFlujo
    frmFlujosRoles.g_iSolicitudTipoTipo = g_iSolicitudTipoTipo
    frmFlujosRoles.g_lSolicitud = g_lSolicitud
    frmFlujosRoles.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cmdRoles_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cP_Click(ItemNumber As Long)
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case cP.itemKey(ItemNumber)
        Case "BLADD"
            AnyadirEtapa
        Case "ACCADD"
            If Not flujo.SelectedNode Is Nothing Then
                AnyadirEnlace CLng(val(flujo.SelectedNode.key))
            End If
        Case "BLDET"
            ConfigurarEtapa
        Case "ACCDET"
            DetalleEnlace
        Case "BLDEL"
            EliminarEtapa
        Case "ACCDEL"
            EliminarEnlace
        Case "BLNOM"
            CambiarNombreEtapa
        Case "BLGS"
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cP_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub AnyadirEtapa()
    Dim teserror As TipoErrorSummit
    frmFlujosAnyadirEtapa.lIdWorkFlow = m_lIdFlujo
    frmFlujosAnyadirEtapa.Show vbModal
    Set oBloque = frmFlujosAnyadirEtapa.oBloque
    Set frmFlujosAnyadirEtapa.oBloque = Nothing
    Unload frmFlujosAnyadirEtapa
    If Not oBloque Is Nothing Then
        oBloque.Workflow = m_lIdFlujo
        oBloque.Top = frmY
        oBloque.Left = frmX
        If oBloque.Height = 0 Then
            oBloque.Height = 500
        End If
        If oBloque.Width = 0 Then
            oBloque.Width = 1000
        End If
        Set m_oIBaseDatos = oBloque
        teserror = m_oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oBloque = Nothing
            Set m_oIBaseDatos = Nothing
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCBloqueAnya, "ID:" & oBloque.Id
        End If
        Set m_oIBaseDatos = Nothing
        oBloques.AddBloque oBloque
        Set oBloque = Nothing
        'Se llama a la funcion para actualizar el campo PENDIENTE_COPIA
        HayCambios
        DibujarFlujo
    End If

End Sub

Private Sub EliminarEtapa()
    Dim teserror As TipoErrorSummit
    Dim link As afLink
    Dim oRoles As CPMRoles
    Dim rsRoles As ADODB.Recordset
    
    If Not flujo.SelectedNode Is Nothing Then
    
        If oMensajes.PreguntaEliminar(" " & flujo.SelectedNode.Text) = vbYes Then
            Set oBloque = oBloques.Item(CStr(val(flujo.SelectedNode.key)))
            
            'Mirar si hay roles en el flujo que se asignan en este bloque
            Set oRoles = oFSGSRaiz.Generar_CPMRoles
            Set rsRoles = oRoles.DevolverRolesWorkflow(m_lIdFlujo)
            Set oRoles = Nothing
            rsRoles.Filter = "BLOQUE_ASIGNA=" & oBloque.Id
            If rsRoles.RecordCount > 0 Then
                oMensajes.ExistenRolesAsignanEtapa
                Exit Sub
            End If
            
            Set m_oIBaseDatos = oBloque
            teserror = m_oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set m_oIBaseDatos = Nothing
                Set oBloque = Nothing
                Exit Sub
            Else
                basSeguridad.RegistrarAccion AccionesSummit.ACCBloqueElim, "ID:" & oBloque.Id
            End If
            'Se llama a la funcion para actualizar el campo PENDIENTE_COPIA
            HayCambios
            
            Set m_oIBaseDatos = Nothing
            Set oBloque = Nothing
            
            For Each link In flujo.SelectedNode.InLinks
                oEnlaces.Remove (Split(link.key, "&&")(1))
            Next
            For Each link In flujo.SelectedNode.OutLinks
                oEnlaces.Remove (Split(link.key, "&&")(1))
            Next
            
            oBloques.Remove (CStr(val(flujo.SelectedNode.key)))
           
            DibujarFlujo
         End If
    End If
End Sub

Private Sub CambiarNombreEtapa()
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not flujo.SelectedNode Is Nothing Then
        frmFlujosNombreEtapa.lIdBloque = val(flujo.SelectedNode.key)
        frmFlujosNombreEtapa.Show vbModal
        If Not frmFlujosNombreEtapa.oBloque Is Nothing Then
            oBloques.Item(CStr(val(flujo.SelectedNode.key))).Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den = frmFlujosNombreEtapa.oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
            oBloques.Item(CStr(val(flujo.SelectedNode.key))).FECACT = frmFlujosNombreEtapa.oBloque.FECACT
            Set frmFlujosNombreEtapa.oBloque = Nothing
        End If
        Unload frmFlujosNombreEtapa
        
        DibujarFlujo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "CambiarNombreEtapa", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub ConfigurarEtapa()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not flujo.SelectedNode Is Nothing Then
        If m_bModifDistrFlujo Then
            If oMensajes.PreguntaGuardarDistribucionFlujo = vbYes Then
                GuardarDistribucionFlujo
            End If
        End If
    
        frmFlujosConfEtapa.m_bModifFlujo = m_bModifFlujo
        frmFlujosConfEtapa.lIdFlujo = m_lIdFlujo
        frmFlujosConfEtapa.lIdFormulario = m_lIdFormulario
        frmFlujosConfEtapa.lIdBloqueSel = val(flujo.SelectedNode.key)
        frmFlujosConfEtapa.iTipoSolicitud = g_iSolicitudTipoTipo
        frmFlujosConfEtapa.Show vbModal
        
        CargarEstructuraFlujo
        DibujarFlujo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "ConfigurarEtapa", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub AnyadirEnlace(ByVal lIdBloque_Orig As Long, Optional ByVal lIdBloque_Dest As Long)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmFlujosDetalleAccion.m_bModifFlujo = m_bModifFlujo
    frmFlujosDetalleAccion.lIdFlujo = m_lIdFlujo
    frmFlujosDetalleAccion.lIdFormulario = m_lIdFormulario
    frmFlujosDetalleAccion.lIdBloque_Orig = lIdBloque_Orig
    If Not IsMissing(lIdBloque_Dest) Then
        frmFlujosDetalleAccion.lIdBloque_Dest = lIdBloque_Dest
    End If
    frmFlujosDetalleAccion.m_AccionSummit = ACCAccionBloqueAnya
    frmFlujosDetalleAccion.m_bSelAccion = True
    frmFlujosDetalleAccion.iTipoSolicitud = g_iSolicitudTipoTipo
    frmFlujosDetalleAccion.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "AnyadirEnlace", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub EliminarEnlace()
Dim oAccion As CAccionBloque
Dim teserror As TipoErrorSummit
Dim key As String
Dim bEliminarAccion As Boolean
Dim irespuesta As Integer

If Not flujo.SelectedLink Is Nothing Then
    key = Split(flujo.SelectedLink.key, "&&")(1)
    Set oEnlace = oEnlaces.Item(key)
    Set oAccion = oFSGSRaiz.Generar_CAccionBloque
    oAccion.Id = oEnlace.Accion
    Set m_oIBaseDatos = oAccion
    teserror = m_oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oIBaseDatos = Nothing
        Set oAccion = Nothing
        Exit Sub
    End If
    bEliminarAccion = False
    If oAccion.Enlaces.Count = 1 Then
        irespuesta = oMensajes.PreguntaEliminarAccionPorUnicoEnlace
        If irespuesta = vbNo Then
           Exit Sub
        End If
        bEliminarAccion = True
    Else
        irespuesta = oMensajes.PreguntaEliminar(" " & flujo.SelectedLink.Text)
        If irespuesta = vbNo Then
           Exit Sub
        End If
    End If
    If bEliminarAccion Then
        Set m_oIBaseDatos = oAccion
        teserror = m_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Set m_oIBaseDatos = Nothing
            Set oAccion = Nothing
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCAccionBloqueElim, "Accion:" & oAccion.Id
        End If
        Set m_oIBaseDatos = Nothing
        Set oAccion = Nothing
    Else
        Set m_oIBaseDatos = oEnlace
        teserror = m_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set m_oIBaseDatos = Nothing
            Set oEnlace = Nothing
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCEnlaceElim, "ID:" & oEnlace.Id
        End If
        Set m_oIBaseDatos = Nothing
        Set oEnlace = Nothing
    End If
    'Se llama a la funcion para actualizar el campo PENDIENTE_COPIA
    HayCambios
    oEnlaces.Remove (key)
       
    DibujarFlujo
End If
End Sub

Private Sub DetalleEnlace()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not flujo.SelectedLink Is Nothing Then
        If m_bModifDistrFlujo Then
            If oMensajes.PreguntaGuardarDistribucionFlujo = vbYes Then
                GuardarDistribucionFlujo
            End If
        End If
    
        Set oEnlace = oEnlaces.Item(Split(flujo.SelectedLink.key, "&&")(1))
                
        frmFlujosDetalleAccion.m_bModifFlujo = m_bModifFlujo
        frmFlujosDetalleAccion.lIdFlujo = m_lIdFlujo
        frmFlujosDetalleAccion.lIdFormulario = m_lIdFormulario
        frmFlujosDetalleAccion.lIdBloque_Orig = CLng(val(flujo.SelectedLink.Org.key))
        frmFlujosDetalleAccion.lIdAccion = oEnlace.Accion
        frmFlujosDetalleAccion.m_AccionSummit = ACCAccionBloqueModif
        frmFlujosDetalleAccion.m_bSelAccion = True
        frmFlujosDetalleAccion.iTipoSolicitud = g_iSolicitudTipoTipo
        frmFlujosDetalleAccion.Show vbModal
        
        CargarEstructuraFlujo
        DibujarFlujo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "DetalleEnlace", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Public Sub reflejarAccionEnDiagrama()
    Dim oEnlaces_Nuevos As CEnlaces
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oEnlaces_Nuevos = frmFlujosDetalleAccion.oAccion.Enlaces
    
    For Each oEnlace In frmFlujosDetalleAccion.oEnlaces_Eliminados
        oEnlaces.Remove (CStr(oEnlace.Id))
    Next
    
    For Each oEnlace In frmFlujosDetalleAccion.oEnlaces_Modificados
        oEnlaces.Item(CStr(oEnlace.Id)).BloqueDestino = oEnlace.BloqueDestino
        Set oEnlaces.Item(CStr(oEnlace.Id)).DenAccion = oEnlace.DenAccion
        oEnlaces.Item(CStr(oEnlace.Id)).TipoAccion = oEnlace.TipoAccion
        oEnlaces.Item(CStr(oEnlace.Id)).FECACT = oEnlace.FECACT
    Next
    
    For Each oEnlace In frmFlujosDetalleAccion.oEnlaces_Nuevos
        If oEnlaces.Item(CStr(oEnlace.Id)) Is Nothing Then
            oEnlaces.AddEnlace oEnlace
        End If
    Next
    
    DibujarFlujo
    
    Set oEnlace = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "reflejarAccionEnDiagrama", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub flujo_AfterAddLink(ByVal NewLink As AddFlow5Lib.afLink)
    Dim oBloque As CBloque
    Dim bAnyadir As Boolean
    Dim lIdBloque_Orig As Long
    Dim lIdBloque_Dest As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bAnyadir = False
    Set oBloque = oBloques.Item(CStr(val(NewLink.Org.key)))
    If oBloque.Tipo = TipoBloque.Peticionario Or (oBloque.Tipo = TipoBloque.Intermedio And NewLink.Org.InLinks.Count > 0) Then
        bAnyadir = True
    End If
    
    'No se permite crear un enlace a si mismo
    If NewLink.Org.key = NewLink.Dst.key Then
        bAnyadir = False
    End If
           
    If bAnyadir Then
        lIdBloque_Orig = CLng(val(NewLink.Org.key))
        lIdBloque_Dest = CLng(val(NewLink.Dst.key))
        NewLink.key = "TEMP"
        NewLink.Org.Links.Remove (NewLink.key)
        AnyadirEnlace lIdBloque_Orig, lIdBloque_Dest
    Else
        NewLink.key = "TEMP"
        NewLink.Org.Links.Remove (NewLink.key)
    End If
    
    
    Set oBloque = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "flujo_AfterAddLink", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub flujo_AfterMove()
    Dim node As afNode
    Dim teserror As TipoErrorSummit
    If flujo.SelNodes.Count > 0 Then
        m_bModifDistrFlujo = True
        For Each node In flujo.SelNodes
            Set oBloque = oBloques.Item(CStr(val(node.key)))
            oBloque.Top = node.Top
            oBloque.Left = node.Left
            
            '''Guardar La nueva ubicaci�n de la etapa
            Set m_oIBaseDatos = oBloque
            teserror = m_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set m_oIBaseDatos = Nothing
                Exit Sub
            Else
                basSeguridad.RegistrarAccion AccionesSummit.ACCAccionBloqueModif, "ID:" & oBloque.Id
            End If
            Set m_oIBaseDatos = Nothing
            'Se llama a la funcion para actualizar el campo PENDIENTE_COPIA
            HayCambios
        Next
    End If
    Set node = Nothing
    Set oBloque = Nothing
End Sub

Private Sub flujo_AfterResize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not flujo.SelectedNode Is Nothing Then
        Set oBloque = oBloques.Item(CStr(val(flujo.SelectedNode.key)))
        oBloque.Width = flujo.SelectedNode.Width
        oBloque.Height = flujo.SelectedNode.Height
        
        m_bModifDistrFlujo = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "flujo_AfterResize", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub flujo_AfterStretch()
    Dim punto As afLinkPoint
    Dim i As Integer

    'Se da cuando se Estira un Link. Lo utilizo para detectar cambios en los ExtraPoints
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not flujo.SelectedLink Is Nothing Then
        Set oEnlace = oEnlaces.Item(Split(flujo.SelectedLink.key, "&&")(1))
        
        If Not oEnlace Is Nothing Then
            If Not oEnlace.ExtraPoints Is Nothing Then
                oEnlace.ExtraPoints.clear
                For i = 1 To flujo.SelectedLink.ExtraPoints.Count - 2
                    Set punto = flujo.SelectedLink.ExtraPoints(i)
                    oEnlace.ExtraPoints.Add punto.X, punto.Y, CLng(i)
                Next
            End If
        End If
        
        m_bModifDistrFlujo = True
   End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "flujo_AfterStretch", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub flujo_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not flujo.SelectedNode Is Nothing Then
        lblEtapa.caption = flujo.SelectedNode.Text
    Else
        lblEtapa.caption = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "flujo_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub flujo_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmX = flujo.xScroll + X
    frmY = flujo.yScroll + Y
    
    If Shift = 0 Then
        flujo.SelNodes.clear
        flujo.SelLinks.clear
    End If
    
    If Not flujo.GetNodeAtPoint(frmX, frmY) Is Nothing Then
        flujo.SelNodes.Add flujo.GetNodeAtPoint(frmX, frmY)
    End If
    If Not flujo.GetLinkAtPoint(frmX, frmY) Is Nothing Then
        flujo.SelLinks.Add flujo.GetLinkAtPoint(frmX, frmY)
    End If
            
    ConfigurarBotones
    
    If Button = 2 And Not g_bDesdeInstancia Then
        cargarMenu X, Y
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "flujo_MouseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub ConfigurarBotones()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If flujo.SelNodes.Count <= 1 Then
        cmdConfEtapa.Enabled = False
        cmdEditar.Enabled = False
        cmdEliminar.Enabled = False
        If Not flujo.SelectedNode Is Nothing Then
            Set oBloque = oBloques.Item(CStr(val(flujo.SelectedNode.key)))
            
            cmdConfEtapa.Enabled = True
            cmdConfEtapa.ToolTipText = m_sCPConfEtapa & " " & oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
        
            cmdEditar.Enabled = (Not (oBloque.Tipo <> TipoBloque.Intermedio)) And m_bModifFlujo
            cmdEliminar.Enabled = (Not (oBloque.Tipo <> TipoBloque.Intermedio)) And m_bModifFlujo
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "ConfigurarBotones", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cargarMenu(X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cP.clear
    If flujo.SelNodes.Count <= 1 And flujo.SelLinks.Count <= 1 Then
        If Not flujo.SelectedNode Is Nothing Then
            Set oBloque = oBloques.Item(CStr(val(flujo.SelectedNode.key)))
            cP.AddItem m_sCPDetalleEtapa, , 1, , , , , "BLDET"
            If oBloque.Tipo = TipoBloque.Intermedio And m_bModifFlujo Then
                cP.AddItem m_sCPNomEtapa, , 2, , , , , "BLNOM"
                cP.AddItem m_sCPDelEtapa, , 3, , , , , "BLDEL"
                cP.AddItem m_sCPAddAccion, , 4, , , , , "ACCADD"
            Else
                cP.AddItem m_sCPNomEtapa, , 2, , , , False, "BLNOM"
                cP.AddItem m_sCPDelEtapa, , 3, , , , False, "BLDEL"
                If oBloque.Tipo = TipoBloque.Peticionario And m_bModifFlujo Then
                    cP.AddItem m_sCPAddAccion, , 4, , , , , "ACCADD"
                Else
                    cP.AddItem m_sCPAddAccion, , 4, , , , False, "ACCADD"
                End If
            End If
        ElseIf Not flujo.SelectedLink Is Nothing Then
            cP.AddItem m_sCPDetalleAccion, , 1, , , , , "ACCDET"
            cP.AddItem m_sCPDelAccion, , 2, , , , m_bModifFlujo, "ACCDEL"
        Else
            cP.AddItem m_sCPAddEtapa, , 1, , , , m_bModifFlujo, "BLADD"
        End If
        DoEvents
        cP.ShowPopupMenu X, Y
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "cargarMenu", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
flujo.Enabled = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
Dim oWorkflows As CWorkflows

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    m_bDescargarFrm = False
    m_bCancelado = False
    Me.Height = 9240
    Me.Width = 14035
    'Inicializamos la variable
    m_bConCambios = False
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    'Si no hay cambios se dejan deshabilitados los botones
    cmdAceptar.Enabled = False
    cmdCancelar.Enabled = False
    
    CargarRecursos
    ConfigurarInterfaz
    
    If Not g_bDesdeInstancia Then
        Me.caption = m_sCaption & " " & m_sSolicitud
    Else
        Me.caption = m_sCaptionInstancia
        If m_sSolicitud <> "" Then
            Me.caption = Me.caption & " (" & m_sSolicitud & ")"
        End If
    End If
    
    Set cP = New cPopupMenu
    cP.hWndOwner = Me.hWnd
    
    'Cargar el WorkFlow y toda su estructura
    If Not g_bDesdeInstancia Then
        CargarEstructuraFlujo
    Else
        CargarEstructuraFlujoInstancia
    End If
    
    'Dibujar el Diagrama
    DibujarFlujo
    
    'Carga si se permite el pedido directo o no antes de que se acabe el workflow.
    If g_lSolicitud <> 0 Then
        m_bCargando = True
        Dim oSolicitud As CSolicitud
        Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
        oSolicitud.Id = g_lSolicitud
        oSolicitud.CargarConfiguracion , , , , True
        
        'Pongo a visible el boton
        If g_iSolicitudTipoTipo = TipoSolicitud.SolicitudCompras Then
            cmdBloqueos.Visible = True
        Else
            cmdNotifExp.Left = cmdRoles.Left + cmdRoles.Width + 30
        End If
        
        Set oSolicitud = Nothing
        m_bCargando = False
    End If

    m_bModifDistrFlujo = False
    
    Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
    oSolicitud.Id = g_lSolicitud
    
    'Comprobamos si la �ltima vez que se accedi� al flujo se cerro correctamente mirando el campo PENDIENTE_COPIA
    'Si est� a 1 se ha cerrado mal y daremos un mensaje de advertencia
    Set oWorkflows = oFSGSRaiz.generar_CWorkflows
    If oWorkflows.EstaPendienteCopia(m_lIdFlujo) Then
        oMensajes.FlujoTrabajoPendiente
        m_bConCambios = True
        cmdAceptar.Enabled = True
        cmdCancelar.Enabled = True
    End If
    Set oWorkflows = Nothing
    
    Select Case g_iSolicitudTipoTipo
        Case TipoSolicitud.OtrasSolicitudes, TipoSolicitud.SolicitudCompras, TipoSolicitud.Abono, TipoSolicitud.Proyecto, TipoSolicitud.Express, _
            TipoSolicitud.pedido, TipoSolicitud.NoConformidades, TipoSolicitud.SolicitudQA
            cmdNotifExp.Visible = Not g_bDesdeInstancia
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        m_sCaption = Ador(0).Value '1
        Ador.MoveNext
        lblFlujo.caption = Ador(0).Value
        Ador.MoveNext
        cmdGuardar.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdBloqueos.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdRoles.ToolTipText = Ador(0).Value '5
        Ador.MoveNext
        lblConfEtapa.caption = Ador(0).Value
        Ador.MoveNext
        m_sCPConfEtapa = Ador(0).Value
        cmdConfEtapa.ToolTipText = Ador(0).Value
        Ador.MoveNext
        m_sCPAddEtapa = Ador(0).Value
        cmdAnyadir.ToolTipText = Ador(0).Value
        Ador.MoveNext
        m_sCPAddAccion = Ador(0).Value
        Ador.MoveNext
        m_sCPDetalleEtapa = Ador(0).Value '10
        Ador.MoveNext
        m_sCPDetalleAccion = Ador(0).Value
        Ador.MoveNext
        m_sCPNomEtapa = Ador(0).Value
        cmdEditar.ToolTipText = Ador(0).Value
        Ador.MoveNext
        m_sCPDelEtapa = Ador(0).Value
        cmdEliminar.ToolTipText = Ador(0).Value
        Ador.MoveNext
        m_sCPDelAccion = Ador(0).Value
        Ador.MoveNext
        m_sCaptionInstancia = Ador(0).Value '15
        Ador.MoveNext
        cmdNotifExp.ToolTipText = Ador(0).Value '16     Notificaciones de expiraci�n
        Ador.Close
    End If
End Sub

Private Sub ConfigurarInterfaz()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_bDesdeInstancia Then
        cmdAnyadir.Enabled = m_bModifFlujo
        cmdGuardar.Enabled = m_bModifFlujo
        flujo.CanMoveNode = m_bModifFlujo
        flujo.CanSizeNode = m_bModifFlujo
        flujo.CanDrawLink = m_bModifFlujo
        flujo.CanStretchLink = m_bModifFlujo
        flujo.Refresh
    Else
        cmdEditar.Visible = False
        cmdAnyadir.Visible = False
        cmdEliminar.Visible = False
        cmdGuardar.Visible = False
        cmdRoles.Visible = False
        cmdBloqueos.Visible = False
        cmdConfEtapa.Visible = False
        lblConfEtapa.Visible = False
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        cmdNotifExp.Visible = False
        lblEtapa.Visible = False
        flujo.CanMoveNode = m_bModifFlujo
        flujo.CanSizeNode = m_bModifFlujo
        flujo.CanDrawLink = m_bModifFlujo
        flujo.CanStretchLink = m_bModifFlujo
        flujo.Refresh
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "ConfigurarInterfaz", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub CargarEstructuraFlujo()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Set oBloques = oFSGSRaiz.Generar_CBloques
    Set oEnlaces = oFSGSRaiz.Generar_CEnlaces
    If m_lIdFlujo > 0 Then
        oBloques.CargarBloques basPublic.gParametrosInstalacion.gIdioma, m_lIdFlujo
        oEnlaces.CargarEnlaces basPublic.gParametrosInstalacion.gIdioma, m_lIdFlujo
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "CargarEstructuraFlujo", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub CargarEstructuraFlujoInstancia()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Set oBloques = oFSGSRaiz.Generar_CBloques
    Set oEnlaces = oFSGSRaiz.Generar_CEnlaces
    If m_lIdFlujo > 0 Then
        oBloques.CargarBloquesInstancia basPublic.gParametrosInstalacion.gIdioma, m_lIdInstancia, m_lIdFlujo
        oEnlaces.CargarEnlacesInstancia basPublic.gParametrosInstalacion.gIdioma, m_lIdFlujo
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "CargarEstructuraFlujoInstancia", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub DibujarFlujo()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    flujo.Nodes.clear
    
    For Each oBloque In oBloques
        DibujarBloque oBloque
    Next
    For Each oEnlace In oEnlaces
        DibujarEnlace oEnlace
    Next
    
    ConfigurarBotones
    
    Screen.MousePointer = vbNormal
    
    Set oBloque = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "DibujarFlujo", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub DibujarBloque(oBl As CBloque)
    Dim nodo As afNode
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodo = flujo.Nodes.Add(oBl.Left, oBl.Top, oBl.Width, oBl.Height)
    nodo.key = CStr(oBl.Id) & "key"

    nodo.Text = oBl.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
    If m_lIdInstancia > 0 Then
        Select Case oBl.Estado
            Case 0
                nodo.FillColor = RGB(255, 255, 255) 'Blanco
            Case 1
                If oBl.Tipo = TipoBloque.final Then
                    nodo.FillColor = RGB(200, 255, 200) 'Verde
                Else
                    nodo.FillColor = RGB(255, 192, 128) 'Naranja
                End If
            Case 2, 3
                nodo.FillColor = RGB(200, 255, 200) 'Verde
        End Select
    End If
    Set nodo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "DibujarBloque", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub DibujarEnlace(oEN As CEnlace)
    Dim link As afLink
    Dim oExtraPoint As CEnlaceExtraPoint
    Dim i As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set link = flujo.AddLink(flujo.Nodes(CStr(oEN.BloqueOrigen) & "key"), flujo.Nodes(CStr(oEN.BloqueDestino) & "key"))
    link.key = CStr(oEN.Accion) & "&&" & CStr(oEN.Id) & "&&key"
    If oEN.Formula <> "" Then
        link.DrawStyle = afDot
    End If
    'Ahora le pongo los extraPoints
    i = 1
    If Not oEN.ExtraPoints Is Nothing Then
        For Each oExtraPoint In oEN.ExtraPoints
            link.ExtraPoints.Add oExtraPoint.X, oExtraPoint.Y
            i = i + 1
        Next
    End If
    
    'Ahora el nombre de la accion:
    link.Text = oEN.DenAccion.Item(basPublic.gParametrosInstalacion.gIdioma).Den
    'Ahora el color del Link:
    If oEN.TipoAccion = TipoAccionBloque.Rechazo Then
        link.DrawColor = RGB(255, 0, 0)
        link.Forecolor = RGB(255, 0, 0)
    ElseIf oEN.LlamadaExterna = True Then
        link.DrawColor = RGB(0, 0, 255)
        link.Forecolor = RGB(0, 0, 255)
    End If
    
    Set link = Nothing
    Set oExtraPoint = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "DibujarEnlace", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Resize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 2000 Then Exit Sub
    If Me.Width < 2000 Then Exit Sub
        
    flujo.Top = 480
    flujo.Height = Me.Height - cmdAceptar.Height - 1020
    
    flujo.Width = Me.Width - 135
    
    cmdAceptar.Top = (flujo.Height + flujo.Top) + 50
    cmdAceptar.Left = (Me.Width / 2) - cmdAceptar.Width - 200
    cmdCancelar.Top = cmdAceptar.Top
    cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + 100
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "Form_Resize", err, Erl)
        Exit Sub
    End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bModifDistrFlujo Then
        If oMensajes.PreguntaGuardarDistribucionFlujo = vbYes Then
            GuardarDistribucionFlujo
        End If
    End If
    
    'Miramos si se ha cancelado o si se ha salido sin verificar cambios
    If Not m_bCancelado And m_bConCambios Then
        'Si NO se ha pulsado el bot�n cancelar o el Aceptar se preguntar� si se quieren guardar los cambios realizdos o descartarlos
        If oMensajes.SalirSinGuardarFlujoTrabajo = vbYes Then
            'Si la respuesta es "S�" Guardamos los cambios
            If m_bModifDistrFlujo Then
                GuardarDistribucionFlujo
                DoEvents
            End If
            GuardarCambios
        Else
            'Si la respuesta es "No" deshacemos los cambios
            CancelarCambios
        End If
    End If
    g_bDesdeInstancia = False
    Set oBloques = Nothing
    Set oBloque = Nothing
    Set oEnlaces = Nothing
    Set oEnlace = Nothing
    Set m_oIBaseDatos = Nothing
    
    g_lSolicitud = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmFlujos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
