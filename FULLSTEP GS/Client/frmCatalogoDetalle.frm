VERSION 5.00
Begin VB.Form frmCatalogoDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle"
   ClientHeight    =   1350
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4695
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCatalogoDetalle.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1350
   ScaleWidth      =   4695
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   960
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   5
      Top             =   900
      Width           =   3825
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   7
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   6
         Top             =   60
         Width           =   1050
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1290
      Left            =   1365
      ScaleHeight     =   1290
      ScaleWidth      =   3915
      TabIndex        =   1
      Top             =   0
      Width           =   3915
      Begin VB.TextBox txtDen 
         Height          =   285
         Left            =   60
         MaxLength       =   100
         TabIndex        =   3
         Top             =   540
         Width           =   3165
      End
      Begin VB.TextBox txtCod 
         Height          =   285
         Left            =   60
         TabIndex        =   2
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Label lblDen 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominaci�n:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   60
      TabIndex        =   4
      Top             =   600
      Width           =   1380
   End
   Begin VB.Label lblCod 
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo:"
      ForeColor       =   &H8000000E&
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   180
      Width           =   1035
   End
End
Attribute VB_Name = "frmCatalogoDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public iCategoria As Integer

'multiidioma
Private sIdiCod As String
Private sIdiDen As String

Private Function AnyadirCAT1AEstructura(ByVal oCATN1 As CCategoriaN1)
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String

Set nodx = frmCatalogo.tvwCategorias.selectedItem
scod1 = oCATN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(oCATN1.Cod))
'Tengo que a�adirlo a la coleccion que mantiene la inf de la estructura
frmCatalogo.oCategoriasNivel1.Add oCATN1.Id, oCATN1.Cod, oCATN1.Den, , , , oCATN1.FECACT
Set frmCatalogo.oCategoriasNivel1.Item(scod1).CamposPersonalizados = oFSGSRaiz.Generar_CCampos

Set nodo = frmCatalogo.tvwCategorias.Nodes.Add("Raiz", tvwChild, "CAT1" & scod1, oCATN1.Cod & " - " & oCATN1.Den, "CAT")
nodo.Tag = "CAT1-" & oCATN1.Cod & "-%$" & oCATN1.Id

nodo.Selected = True
nodo.EnsureVisible

Set nodo = Nothing
Set nodx = Nothing
Set oCATN1 = Nothing

End Function


Public Function AnyadirCAT2AEstructura(ByVal oCatN2 As CCategoriaN2)
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String

Set nodx = frmCatalogo.tvwCategorias.selectedItem
scod1 = frmCatalogo.DevolverCod(nodx)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
scod2 = oCatN2.Cod
scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))

'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2 Is Nothing Then
    Set frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2 = oFSGSRaiz.Generar_CCategoriasN2
End If

frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Add oCatN2.Id, oCatN2.Cod, oCatN2.Den, oCatN2.Cat1, oCatN2.Cat1Cod, oCatN2.BajaLogica, , , oCatN2.FECACT
Set frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CamposPersonalizados = oFSGSRaiz.Generar_CCampos
Set nodo = frmCatalogo.tvwCategorias.Nodes.Add(nodx.key, tvwChild, "CAT2" & scod1 & scod2, oCatN2.Cod & " - " & oCatN2.Den, "CAT")
nodo.Tag = "CAT2-" & oCatN2.Cod & "-%$" & oCatN2.Id
Select Case oCatN2.BajaLogica
    Case 1
            nodo.Image = "BAJALOG1"
    Case 2
            nodo.Image = "BAJALOG2"
End Select

nodo.Selected = True
nodo.EnsureVisible

Set nodo = Nothing
Set nodx = Nothing
Set oCatN2 = Nothing

End Function
Public Function AnyadirCAT3AEstructura(ByVal oCatN3 As CCategoriaN3)
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String

Set nodx = frmCatalogo.tvwCategorias.selectedItem
scod1 = frmCatalogo.DevolverCod(nodx.Parent)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
scod2 = frmCatalogo.DevolverCod(nodx)
scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
scod3 = oCatN3.Cod
scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))

'Tengo que a�adirlo a la coleccion que mantiene la inf de la estructura
If frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3 Is Nothing Then
    Set frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3 = oFSGSRaiz.Generar_CCategoriasN3
End If

frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Add oCatN3.Id, oCatN3.Cod, oCatN3.Den, oCatN3.Cat1, oCatN3.Cat1Cod, oCatN3.Cat2, oCatN3.Cat2Cod, oCatN3.BajaLogica, , , oCatN3.FECACT
Set frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CamposPersonalizados = oFSGSRaiz.Generar_CCampos

Set nodo = frmCatalogo.tvwCategorias.Nodes.Add(nodx.key, tvwChild, "CAT3" & scod1 & scod2 & scod3, oCatN3.Cod & " - " & oCatN3.Den, "CAT")
nodo.Tag = "CAT3-" & oCatN3.Cod & "-%$" & oCatN3.Id

Select Case oCatN3.BajaLogica
    Case 1
            nodo.Image = "BAJALOG1"
    Case 2
            nodo.Image = "BAJALOG2"
End Select

nodo.Selected = True
nodo.EnsureVisible

Set nodo = Nothing
Set nodx = Nothing
Set oCatN3 = Nothing

End Function

Public Function AnyadirCAT5AEstructura(ByVal oCatN5 As CCategoriaN5)
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Set nodx = frmCatalogo.tvwCategorias.selectedItem
scod1 = frmCatalogo.DevolverCod(nodx.Parent.Parent.Parent)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
scod2 = frmCatalogo.DevolverCod(nodx.Parent.Parent)
scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
scod3 = frmCatalogo.DevolverCod(nodx.Parent)
scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
scod4 = frmCatalogo.DevolverCod(nodx)
scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
scod5 = oCatN5.Cod
scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))

'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5 Is Nothing Then
    Set frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5 = oFSGSRaiz.Generar_CCategoriasN5
End If

frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Add oCatN5.Id, oCatN5.Cod, oCatN5.Den, oCatN5.Cat1, oCatN5.Cat1Cod, oCatN5.Cat2, oCatN5.Cat2Cod, oCatN5.Cat3, oCatN5.CAT3cod, oCatN5.Cat4, oCatN5.CAT4cod, oCatN5.BajaLogica, , , oCatN5.FECACT
Set frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5).CamposPersonalizados = oFSGSRaiz.Generar_CCampos

Set nodo = frmCatalogo.tvwCategorias.Nodes.Add(nodx.key, tvwChild, "CAT5" & scod1 & scod2 & scod3 & scod4 & scod5, oCatN5.Cod & " - " & oCatN5.Den, "CAT")
nodo.Tag = "CAT5-" & oCatN5.Cod & "-%$" & oCatN5.Id

Select Case oCatN5.BajaLogica
    Case 1
            nodo.Image = "BAJALOG1"
    Case 2
            nodo.Image = "BAJALOG2"
End Select

nodo.Selected = True
nodo.EnsureVisible

Set nodo = Nothing
Set nodx = Nothing
Set oCatN5 = Nothing

End Function

Private Function AnyadirCAT4AEstructura(ByVal oCatN4 As CCategoriaN4)
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Set nodx = frmCatalogo.tvwCategorias.selectedItem
scod1 = frmCatalogo.DevolverCod(nodx.Parent.Parent)
scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
scod2 = frmCatalogo.DevolverCod(nodx.Parent)
scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
scod3 = frmCatalogo.DevolverCod(nodx)
scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
scod4 = oCatN4.Cod
scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
'Tengo que anyadirlo a la coleccion que mantiene la inf de la estructura
If frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4 Is Nothing Then
    Set frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4 = oFSGSRaiz.Generar_CCategoriasN4
End If

frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Add oCatN4.Id, oCatN4.Cod, oCatN4.Den, oCatN4.Cat1, oCatN4.Cat1Cod, oCatN4.Cat2, oCatN4.Cat2Cod, oCatN4.Cat3, oCatN4.CAT3cod, oCatN4.BajaLogica, , , oCatN4.FECACT
Set frmCatalogo.oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CamposPersonalizados = oFSGSRaiz.Generar_CCampos

Set nodo = frmCatalogo.tvwCategorias.Nodes.Add(nodx.key, tvwChild, "CAT4" & scod1 & scod2 & scod3 & scod4, oCatN4.Cod & " - " & oCatN4.Den, "CAT")
nodo.Tag = "CAT4-" & oCatN4.Cod & "-%$" & oCatN4.Id

Select Case oCatN4.BajaLogica
    Case 1
            nodo.Image = "BAJALOG1"
    Case 2
            nodo.Image = "BAJALOG2"
End Select

nodo.Selected = True
nodo.EnsureVisible

Set nodo = Nothing
Set nodx = Nothing
Set oCatN4 = Nothing
End Function


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_DETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        lblCod.caption = Ador(0).Value & ":"
        sIdiCod = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value & ":"
        sIdiDen = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.Close
            
    End If
    
    Set Ador = Nothing
    
End Sub
Public Function ModificarCATEnEstructura()
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim nodx As MSComctlLib.node

Set nodx = frmCatalogo.tvwCategorias.selectedItem

Select Case Left(nodx.Tag, 4)
    
    Case "CAT1"
            scod1 = Trim(txtCod)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            nodx.key = "CAT1" & scod1
            
            If nodx.Image = "SEGURO" Then
                nodx.Tag = "CAT1-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx) & "-%$SEG" & frmCatalogo.DevolverSeguridadCategoria(nodx)
            Else
                nodx.Tag = "CAT1-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx)
            End If
    
    Case "CAT2"
            scod1 = frmCatalogo.DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = Trim(txtCod)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            nodx.key = "CAT2" & scod1 & scod2
            
            If nodx.Image = "SEGURO" Then
                nodx.Tag = "CAT2-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx) & "-%$SEG" & frmCatalogo.DevolverSeguridadCategoria(nodx)
            Else
                nodx.Tag = "CAT2-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx)
            End If
            
    Case "CAT3"
            scod1 = frmCatalogo.DevolverCod(nodx.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = frmCatalogo.DevolverCod(nodx.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = Trim(txtCod)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            nodx.key = "CAT3" & scod1 & scod2 & scod3
            
            If nodx.Image = "SEGURO" Then
                nodx.Tag = "CAT3-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx) & "-%$SEG" & frmCatalogo.DevolverSeguridadCategoria(nodx)
            Else
                nodx.Tag = "CAT3-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx)
            End If
            
    Case "CAT4"
            scod1 = frmCatalogo.DevolverCod(nodx.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = frmCatalogo.DevolverCod(nodx.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = frmCatalogo.DevolverCod(nodx.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            scod4 = Trim(txtCod)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
            nodx.key = "CAT4" & scod1 & scod2 & scod3 & scod4
            
            If nodx.Image = "SEGURO" Then
                nodx.Tag = "CAT4-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx) & "-%$SEG" & frmCatalogo.DevolverSeguridadCategoria(nodx)
            Else
                nodx.Tag = "CAT4-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx)
            End If
    
    Case "CAT5"
            scod1 = frmCatalogo.DevolverCod(nodx.Parent.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = frmCatalogo.DevolverCod(nodx.Parent.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = frmCatalogo.DevolverCod(nodx.Parent.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            scod4 = frmCatalogo.DevolverCod(nodx.Parent)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
            scod5 = Trim(txtCod)
            scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
            nodx.key = "CAT5" & scod1 & scod2 & scod3 & scod4 & scod5
            
            If nodx.Image = "SEGURO" Then
                nodx.Tag = "CAT5-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx) & "-%$SEG" & frmCatalogo.DevolverSeguridadCategoria(nodx)
            Else
                nodx.Tag = "CAT5-" & Trim(txtCod.Text) & "-%$" & frmCatalogo.DevolverId(nodx)
            End If
            
End Select
nodx.Text = Trim(txtCod.Text) & " - " & Trim(txtDen.Text)

frmCatalogo.oCategoriasNivel1.GenerarEstructuraCategorias frmCatalogo.chkVerBajas.Value, False
Set nodx = Nothing

End Function

Private Sub cmdAceptar_Click()
Dim oCat1 As CCategoriaN1
Dim oCat2 As CCategoriaN2
Dim oCat3 As CCategoriaN3
Dim oCat4 As CCategoriaN4
Dim oCat5 As CCategoriaN5
Dim oIBaseDatos As IBaseDatos
Dim teserror As TipoErrorSummit

    '********* Validar datos *********
    If Trim(txtCod.Text) = "" Then
        oMensajes.NoValido sIdiCod
        If Me.Visible Then txtCod.SetFocus
        Exit Sub
    End If
    
    If Trim(txtDen.Text) = "" Then
        oMensajes.NoValida sIdiDen
        If Me.Visible Then txtDen.SetFocus
        Exit Sub
    End If

Select Case frmCatalogo.Accion

    '************* MODIFICAR ***************
    
    Case ACCCatCategoriaMod
            
        Screen.MousePointer = vbHourglass
        
        Select Case iCategoria
                
            Case 1
                
                    frmCatalogo.oCategoria1Seleccionada.Den = Trim(txtDen.Text)
                    frmCatalogo.oCategoria1Seleccionada.Cod = Trim(txtCod.Text)
                        
            Case 2
                
                    frmCatalogo.oCategoria2Seleccionada.Den = Trim(txtDen.Text)
                    frmCatalogo.oCategoria2Seleccionada.Cod = Trim(txtCod.Text)
                        
            Case 3
                
                    frmCatalogo.oCategoria3Seleccionada.Den = Trim(txtDen.Text)
                    frmCatalogo.oCategoria3Seleccionada.Cod = Trim(txtCod.Text)
            
            Case 4
                
                    frmCatalogo.oCategoria4Seleccionada.Den = Trim(txtDen.Text)
                    frmCatalogo.oCategoria4Seleccionada.Cod = Trim(txtCod.Text)
                
            Case 5
                
                    frmCatalogo.oCategoria5Seleccionada.Den = Trim(txtDen.Text)
                    frmCatalogo.oCategoria5Seleccionada.Cod = Trim(txtCod.Text)
                    
        End Select
        
        teserror = frmCatalogo.oIBaseDatos.FinalizarEdicionModificando
        If teserror.NumError = TESnoerror Then
            ModificarCATEnEstructura
            RegistrarAccion ACCCatCategoriaMod, "Cod:" & Trim(txtCod.Text)
        Else
            TratarError teserror
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    
    
    '************* A�ADIR ***************
    
    Case ACCCatCategoriaAnya

        Screen.MousePointer = vbHourglass
        
        Select Case iCategoria
            
            Case 1
                    
                    Set oCat1 = oFSGSRaiz.Generar_CCategoriaN1
                    oCat1.Cod = Trim(txtCod.Text)
                    oCat1.Den = Trim(txtDen.Text)
                    Set oIBaseDatos = oCat1
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirCAT1AEstructura oCat1
                        RegistrarAccion Accionessummit.ACCCatCategoriaAnya, "CodCAT1:" & Trim(txtCod.Text)
                        frmCatalogo.tvwCategorias_NodeClick frmCatalogo.tvwCategorias.selectedItem
                    Else
                        TratarError teserror
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    
                    Set oCat1 = Nothing
                    Set oIBaseDatos = Nothing
                   
                    
            Case 2
                
                    Set oCat2 = oFSGSRaiz.Generar_CCategoriaN2
                    oCat2.Cod = Trim(txtCod.Text)
                    oCat2.Cat1Cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem)
                    oCat2.Cat1 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem)
                    oCat2.Den = Trim(txtDen.Text)
                    
                    Set oIBaseDatos = oCat2
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirCAT2AEstructura oCat2
                        RegistrarAccion ACCCatCategoriaAnya, "CodCAT1:" & oCat2.Cod & "CodCAT2:" & Trim(txtCod.Text)
                        frmCatalogo.tvwCategorias_NodeClick frmCatalogo.tvwCategorias.selectedItem
                    Else
                        TratarError teserror
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
        
                    Set oCat2 = Nothing
                    Set oIBaseDatos = Nothing
        
            Case 3
        
                    Set oCat3 = oFSGSRaiz.Generar_CCategoriaN3
                    oCat3.Cat1Cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem.Parent)
                    oCat3.Cat1 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem.Parent)
                    oCat3.Cat2Cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem)
                    oCat3.Cat2 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem)
                    oCat3.Cod = Trim(txtCod.Text)
                    oCat3.Den = Trim(txtDen.Text)
                    
                    Set oIBaseDatos = oCat3
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirCAT3AEstructura oCat3
                        RegistrarAccion ACCCatCategoriaAnya, "CodCAT1:" & oCat3.Cat1Cod & "CodCAT2:" & oCat3.Cat2Cod & "CodCAT3:" & Trim(txtCod.Text)
                        frmCatalogo.tvwCategorias_NodeClick frmCatalogo.tvwCategorias.selectedItem
                    Else
                        TratarError teserror
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
        
                    Set oCat3 = Nothing
                    Set oIBaseDatos = Nothing
        
            Case 4
        
                    Set oCat4 = oFSGSRaiz.Generar_CCategoriaN4
                    oCat4.Cat1Cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem.Parent.Parent)
                    oCat4.Cat1 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem.Parent.Parent)
                    oCat4.Cat2Cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem.Parent)
                    oCat4.Cat2 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem.Parent)
                    oCat4.CAT3cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem)
                    oCat4.Cat3 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem)
                    oCat4.Cod = Trim(txtCod.Text)
                    oCat4.Den = Trim(txtDen.Text)
        
                    Set oIBaseDatos = oCat4
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirCAT4AEstructura oCat4
                        RegistrarAccion ACCCatCategoriaAnya, "CodCAT1:" & oCat4.Cat1Cod & "CodCAT2:" & oCat4.Cat2Cod & "CodCAT3:" & oCat4.CAT3cod & "CodCAT4" & Trim(txtCod.Text)
                        frmCatalogo.tvwCategorias_NodeClick frmCatalogo.tvwCategorias.selectedItem
                    Else
                        TratarError teserror
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
        
                    Set oCat4 = Nothing
                    Set oIBaseDatos = Nothing
        
            Case 5
        
                    Set oCat5 = oFSGSRaiz.Generar_CCategoriaN5
                    oCat5.Cat1Cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem.Parent.Parent.Parent)
                    oCat5.Cat1 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem.Parent.Parent.Parent)
                    oCat5.Cat2Cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem.Parent.Parent)
                    oCat5.Cat2 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem.Parent.Parent)
                    oCat5.CAT3cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem.Parent)
                    oCat5.Cat3 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem.Parent)
                    oCat5.CAT4cod = frmCatalogo.DevolverCod(frmCatalogo.tvwCategorias.selectedItem)
                    oCat5.Cat4 = frmCatalogo.DevolverId(frmCatalogo.tvwCategorias.selectedItem)
                    oCat5.Cod = Trim(txtCod.Text)
                    oCat5.Den = Trim(txtDen.Text)
        
                    Set oIBaseDatos = oCat5
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError = TESnoerror Then
                        AnyadirCAT5AEstructura oCat5
                        RegistrarAccion ACCCatCategoriaAnya, "CodCAT1:" & oCat5.Cat1Cod & "CodCAT2:" & oCat5.Cat2Cod & "CodCAT3:" & oCat5.CAT3cod & "CodCAT4" & oCat5.CAT3cod & "CodCAT5" & Trim(txtCod.Text)
                        frmCatalogo.tvwCategorias_NodeClick frmCatalogo.tvwCategorias.selectedItem
                    Else
                        TratarError teserror
                        If Me.Visible Then txtCod.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
        
                    Set oCat5 = Nothing
                    Set oIBaseDatos = Nothing
                   
                   
        End Select
    
    End Select

    Screen.MousePointer = vbNormal
    Unload Me
    If frmCatalogo.Visible Then frmCatalogo.tvwCategorias.SetFocus

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()

    Select Case frmCatalogo.Accion
    
    '    Case ACCCatCategoriaEli
    '
    '        picDatos.Enabled = False
    '
    '    Case ACCEqpDet, ACCCompDet
    '
    '        picDatos.Enabled = False
    '        picEdit.Visible = False
    '        Height = Height - 300
    '
        
        Case ACCCatCategoriaAnya, ACCCatCategoriaMod
            If Me.Visible Then txtCod.SetFocus
    End Select
    
    CargarRecursos

End Sub

