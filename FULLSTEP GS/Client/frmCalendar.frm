VERSION 5.00
Object = "{22ACD161-99EB-11D2-9BB3-00400561D975}#1.0#0"; "PVCalendar9.ocx"
Begin VB.Form frmCalendar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DCalendario"
   ClientHeight    =   2790
   ClientLeft      =   1500
   ClientTop       =   3435
   ClientWidth     =   2535
   Icon            =   "frmCalendar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2790
   ScaleWidth      =   2535
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1320
      TabIndex        =   2
      Top             =   2430
      Width           =   1095
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   60
      TabIndex        =   1
      Top             =   2415
      Width           =   1095
   End
   Begin PVATLCALENDARLib.PVCalendar Calendar 
      Height          =   2295
      Left            =   0
      TabIndex        =   0
      Top             =   60
      Width           =   2475
      _Version        =   524288
      BorderStyle     =   0
      Appearance      =   1
      FirstDay        =   1
      Frame           =   0
      SelectMode      =   1
      DisplayFormat   =   0
      DateOrientation =   8
      CustomTextOrientation=   2
      ImageOrientation=   5
      DOWText0        =   "Dom"
      DOWText1        =   "Lun"
      DOWText2        =   "Mar"
      DOWText3        =   "Mier"
      DOWText4        =   "Jue"
      DOWText5        =   "Vie"
      DOWText6        =   "Sab"
      MonthText0      =   "January"
      MonthText1      =   "February"
      MonthText2      =   "March"
      MonthText3      =   "April"
      MonthText4      =   "May"
      MonthText5      =   "June"
      MonthText6      =   "July"
      MonthText7      =   "August"
      MonthText8      =   "September"
      MonthText9      =   "October"
      MonthText10     =   "November"
      MonthText11     =   "December"
      HeaderBackColor =   13160660
      HeaderForeColor =   0
      DisplayBackColor=   16777215
      DisplayForeColor=   0
      DayBackColor    =   16777215
      DayForeColor    =   0
      SelectedDayForeColor=   0
      SelectedDayBackColor=   12632256
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DOWFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DaysFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MultiLineText   =   0   'False
      EditMode        =   0
      BeginProperty TextFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCalendar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public frmDestination As Form
Public ctrDestination As Control
Public addtotop As Long
Public addtoleft As Long
Public sOrigen As String
Public g_oOrigen As Form

Private dDateCur As Date
Private m_sMsgError As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
''' <summary>
''' Devuelve la fecha seleccionada
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Calendar_DblClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdAceptar_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "Calendar_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cuando cambia de mes selecciona una fecha "por defecto", no esta bien si no es el cliente quien da la fecha
''' </summary>
''' <param name="NewDate">Fecha para la cual se ha cambiado la selecci�n</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Calendar_Change(ByVal NewDate As Date)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dDateCur = NewDate
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "Calendar_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Devuelve la fecha seleccionada
''' </summary>
''' <returns>Fecha seleccionada</returns>
''' <remarks>Llamada desde: Evento del boton Aceptar y Calendar_DblClick; Tiempo m�ximo: 0,1</remarks>
Private Sub cmdAceptar_Click()
    Dim CalendarValue As Variant
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not Calendar.DATESelected(dDateCur) Then
        CalendarValue = Null
    Else
        CalendarValue = dDateCur
    End If
    
    If Not g_oOrigen Is Nothing Then
        If sOrigen = "1" Then
            g_oOrigen.sdbgMiPedido.Columns("FECHAENTREGA").Value = Format(NullToStr(CalendarValue), "Short Date")
        Else
            ctrDestination = NullToStr(CalendarValue)
            ctrDestination = Format(ctrDestination, "Short Date")
        End If
    Else
        Select Case frmDestination.Name
            Case "frmRESREU"
                ctrDestination = NullToStr(CalendarValue)
                ctrDestination = Format(ctrDestination, "Short Date")
                frmRESREU.sdbcFecReu_Validate False
            
            Case "frmREU"
                ctrDestination = NullToStr(CalendarValue)
                ctrDestination = Format(ctrDestination, "Short Date")
                If sOrigen <> "PlanReu" Then
                    ctrDestination.Columns(0).Value = CalendarValue
                    frmREU.sdbcFecReu_Validate False
                End If
                
            Case "frmSeguimiento"
                Select Case sOrigen
                    Case "1"
                        frmSeguimiento.sdbgItemPorProve.Columns("FECENTPROVE").Value = Format(NullToStr(CalendarValue), "Short Date")
                    Case "2"
                        frmSeguimiento.sdbgItemPorProve.Columns("FECENT").Value = Format(NullToStr(CalendarValue), "Short Date")
                    Case "3"
                        frmSeguimiento.sdbgSeguimiento.Columns("FECEMI").Value = Format(NullToStr(CalendarValue), "Short Date") & " " & Time
                    Case "4"
                        frmSeguimiento.sdbgSeguimiento.Columns("PED_ABIERTO_FECINI").Value = Format(NullToStr(CalendarValue), "Short Date")
                    Case "5"
                        frmSeguimiento.sdbgSeguimiento.Columns("PED_ABIERTO_FECFIN").Value = Format(NullToStr(CalendarValue), "Short Date")
                    Case Else
                        ctrDestination = NullToStr(CalendarValue)
                        ctrDestination = Format(ctrDestination, "Short Date")
                End Select
                
            Case "frmPEDIDOS"
                If sOrigen = "1" Then
                    g_oOrigen.sdbgMiPedido.Columns("FECHAENTREGA").Value = Format(NullToStr(CalendarValue), "Short Date")
                Else
                    ctrDestination = NullToStr(CalendarValue)
                    ctrDestination = Format(ctrDestination, "Short Date")
                End If
                
            Case "frmPROCE"
                
                Select Case sOrigen
                    Case "LIMOFE"
                        frmPROCE.txtFecLimitOfer.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmPROCE.txtFecLimitOfer.Text <> "" And frmPROCE.txtHoraLimite.Text = "" Then
                            frmPROCE.txtHoraLimite.Text = TimeValue("23:59")
                        End If
                    Case "INISUB"
                        frmPROCE.txtFecIniSub.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmPROCE.txtFecIniSub.Text <> "" And frmPROCE.txtHoraIniSub.Text = "" Then
                            frmPROCE.txtHoraIniSub.Text = TimeValue("00:00")
                        End If
                    Case "SOBRE"
                        frmPROCE.txtFecSobre.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmPROCE.txtFecSobre.Text <> "" And frmPROCE.txtHoraSobre.Text = "" Then
                            frmPROCE.txtHoraSobre.Text = TimeValue("00:00")
                        End If
                    Case "INI"
                        frmPROCE.txtFecIni.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmPROCE.txtFecIni.Text <> "" And frmPROCE.txtFecFin.Text = "" Then
                            frmPROCE.txtFecFin.Text = Format(NullToStr(CalendarValue), "Short Date")
                        End If
                    Case "FIN"
                        frmPROCE.txtFecFin.Text = Format(NullToStr(CalendarValue), "Short Date")
                    Case "INI_GRUPO"
                        frmPROCE.txtFecIniGrupo.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmPROCE.txtFecIniGrupo.Text <> "" And frmPROCE.txtFecFinGrupo.Text = "" Then
                            frmPROCE.txtFecFinGrupo.Text = Format(NullToStr(CalendarValue), "Short Date")
                        End If
                    Case "FIN_GRUPO"
                        frmPROCE.txtFecFinGrupo.Text = Format(NullToStr(CalendarValue), "Short Date")
                    Case "INI_ITEMS"
                        frmPROCE.sdbgItems.Columns("INI").Value = Format(NullToStr(CalendarValue), "Short Date")
                    Case "FIN_ITEMS"
                        frmPROCE.sdbgItems.Columns("FIN").Value = Format(NullToStr(CalendarValue), "Short Date")
                    Case "AT_"
                        frmPROCE.sdbgItems.Columns(frmPROCE.sdbgItems.Col).Value = Format(NullToStr(CalendarValue), "Short Date")
                    Case Else
                        ctrDestination = NullToStr(CalendarValue)
                        ctrDestination = Format(ctrDestination, "Short Date")
                End Select
                sOrigen = ""
            
            Case "frmESTRMATProve"
                
                If sOrigen = "INI" Then
                    frmESTRMATProve.sdbgAdjudicaciones.Columns("FECINI").Value = Format(NullToStr(CalendarValue), "Short Date")
                Else
                    If sOrigen = "FIN" Then
                        frmESTRMATProve.sdbgAdjudicaciones.Columns("FECFIN").Value = Format(NullToStr(CalendarValue), "Short Date")
                    End If
                End If
                
                sOrigen = ""
            
            Case "frmCatalogo"
                
                frmCatalogo.sdbgAdjudicaciones.Columns("DESPUB").Value = Format(NullToStr(CalendarValue), "Short Date")
            
            Case "frmItemModificarValores"
                Select Case sOrigen
                    Case "INI"
                            frmItemModificarValores.txtFecIni.Text = Format(NullToStr(CalendarValue), "Short Date")
                    Case "FIN"
                            frmItemModificarValores.txtFecFin.Text = Format(NullToStr(CalendarValue), "Short Date")
                End Select
            
            Case "frmItemsWizard2"
                Select Case sOrigen
                    Case "INI"
                            frmItemsWizard2.txtFecIni.Text = Format(NullToStr(CalendarValue), "Short Date")
                    Case "FIN"
                            frmItemsWizard2.txtFecFin.Text = Format(NullToStr(CalendarValue), "Short Date")
                End Select
            Case "frmDatoAmbitoProce"
                
                Select Case sOrigen
                    Case "LIMOFE"
                        frmDatoAmbitoProce.txtFecLim.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmDatoAmbitoProce.txtFecLim.Text <> "" And frmDatoAmbitoProce.txtHoraLim.Text = "" Then
                            frmDatoAmbitoProce.txtHoraLim.Text = TimeValue("23:59")
                        End If
                    Case "INISUB"
                        frmDatoAmbitoProce.txtFecIniSub.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmDatoAmbitoProce.txtFecIniSub.Text <> "" And frmDatoAmbitoProce.txtHoraIniSub.Text = "" Then
                            frmDatoAmbitoProce.txtHoraIniSub.Text = TimeValue("00:00")
                        End If
                    Case "FINSUB"
                        frmDatoAmbitoProce.txtFecFinSub.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmDatoAmbitoProce.txtFecFinSub.Text <> "" And frmDatoAmbitoProce.txtHoraFinSub.Text = "" Then
                            frmDatoAmbitoProce.txtHoraFinSub.Text = TimeValue("23:59")
                        End If
                        
                    Case "SOBRE"
                        frmDatoAmbitoProce.txtFechaSobre.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmDatoAmbitoProce.txtFechaSobre.Text <> "" And frmDatoAmbitoProce.txtSobre.Text = "" Then
                            frmDatoAmbitoProce.txtSobre.Text = TimeValue("00:00")
                        End If
                
                    Case "INI"
                        frmDatoAmbitoProce.txtFecIni.Text = Format(NullToStr(CalendarValue), "Short Date")
                        If frmDatoAmbitoProce.txtFecIni.Text <> "" And frmDatoAmbitoProce.txtFecFin.Text = "" Then
                            frmDatoAmbitoProce.txtFecFin.Text = Format(NullToStr(CalendarValue), "Short Date")
                        End If
                    Case "FIN"
                        frmDatoAmbitoProce.txtFecFin.Text = Format(NullToStr(CalendarValue), "Short Date")
                    Case Else
                        ctrDestination = NullToStr(CalendarValue)
                        ctrDestination = Format(ctrDestination, "Short Date")
                End Select
                
            Case "FrmOBJAnya"
                FrmOBJAnya.txtFecLimOfe.Text = Format(NullToStr(CalendarValue), "Short Date")
                If FrmOBJAnya.txtFecLimOfe.Text <> "" And FrmOBJAnya.txtHoraLimite.Text = "" Then
                    FrmOBJAnya.txtHoraLimite.Text = TimeValue("23:59")
                End If
                    
            Case "frmOFEPetAnya"
                frmOFEPetAnya.txtFecLimOfe.Text = Format(NullToStr(CalendarValue), "Short Date")
                If frmOFEPetAnya.txtFecLimOfe.Text <> "" And frmOFEPetAnya.txtHoraLimite.Text = "" Then
                    frmOFEPetAnya.txtHoraLimite.Text = TimeValue("23:59")
                End If
                
            Case "frmFormularios"
                frmFormularios.sdbgCampos.Columns("VALOR").Value = Format(NullToStr(CalendarValue), "Short Date")
                
            Case "frmDesgloseValores"
                frmDesgloseValores.ssLineas.ActiveCell.Value = Format(NullToStr(CalendarValue), "Short Date")
                
            Case "frmSolicitudDetalle"
                frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = Format(NullToStr(CalendarValue), "Short Date")
                
            Case "frmSolicitudDesglose"
                If frmDestination.g_sOrigen = "frmPedidos" Then
                    g_oOrigen.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = Format(NullToStr(CalendarValue), "Short Date")
                Else
                    frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = Format(NullToStr(CalendarValue), "Short Date")
                End If
                
            Case "frmSOLAbrirFaltan"
                Select Case sOrigen
                    Case "FECINI"
                        frmSOLAbrirFaltan.txtFecIni.Text = Format(NullToStr(CalendarValue), "Short Date")
                    Case "FECFIN"
                        frmSOLAbrirFaltan.txtFecFin.Text = Format(NullToStr(CalendarValue), "Short Date")
                End Select
                
            Case "frmServicio"
                frmServicio.sdbgParamEnt.Columns("VALOR").Value = Format(NullToStr(CalendarValue), "Short Date")
                
            Case "frmSOLAbrirProc"
                frmSOLAbrirProc.sdbgLineas.Columns(sOrigen).Value = Format(NullToStr(CalendarValue), "Short Date")
                
            Case Else
                ctrDestination = NullToStr(CalendarValue)
                ctrDestination = Format(ctrDestination, "Short Date")
                
        End Select
    End If
    Unload Me

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cargar la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Activate()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

    CargarRecursos
    
    If Calendar.Value = "0:00:00" Then
        Calendar.Value = Date
        Calendar.DATESelected(Date) = True
    End If
        
    dDateCur = Calendar.Value
    
    If gParametrosInstalacion.gIdioma = "SPA" Then
        Me.Calendar.FirstDay = 1
    Else
        Me.Calendar.FirstDay = 7
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga los idiomas del formulario.
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo=0,2</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CALENDAR, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        frmCalendar.caption = Ador(0).Value      '1
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value      '2
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value     '3
        Ador.MoveNext
        Me.Calendar.MonthText(0) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(1) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(2) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(3) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(4) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(5) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(6) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(7) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(8) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(9) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(10) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.MonthText(11) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.DOWText(0) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.DOWText(1) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.DOWText(2) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.DOWText(3) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.DOWText(4) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.DOWText(5) = Ador(0).Value
        Ador.MoveNext
        Me.Calendar.DOWText(6) = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
           
End Sub

Private Sub Form_Initialize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    addtotop = 0
    addtoleft = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "Form_Initialize", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Posiciona la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    On Error Resume Next
    
    If Not ctrDestination Is Nothing Then
        Me.Top = (MDI.Height - MDI.ScaleHeight) + frmDestination.Top + (frmDestination.Height - frmDestination.ScaleHeight) + ctrDestination.Top - 200 + addtotop
        If Me.Top < 0 Then
            Me.Top = 0
        End If
        If Me.Top + Me.Height > Screen.Height Then
            Me.Top = Screen.Height - Me.Height - 300
        End If
        Me.Left = (MDI.Width - MDI.ScaleWidth) + frmDestination.Left + (frmDestination.Width - frmDestination.ScaleWidth) + ctrDestination.Left + ctrDestination.Width - 200 + addtoleft
        If Me.Left + Me.Width > Screen.Width Then
            Me.Left = Screen.Width - Me.Width - 300
        End If
    End If
    
    On Error GoTo 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Libera controla y reinicia variables de pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub Form_Unload(Cancel As Integer)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical

   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set frmDestination = Nothing
    Set ctrDestination = Nothing
    sOrigen = ""
    addtotop = 0
    addtoleft = 0
    dDateCur = "0:00:00"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmCalendar", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


