VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstUSUARIOS 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de usuarios (Opciones)+"
   ClientHeight    =   4350
   ClientLeft      =   1065
   ClientTop       =   4740
   ClientWidth     =   7365
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstUSUARIOS.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4350
   ScaleWidth      =   7365
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   7365
      TabIndex        =   23
      Top             =   3975
      Width           =   7365
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener+"
         Height          =   375
         Left            =   6000
         TabIndex        =   13
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3925
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   6932
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n+"
      TabPicture(0)   =   "frmLstUSUARIOS.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Opciones+"
      TabPicture(1)   =   "frmLstUSUARIOS.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).Control(1)=   "Frame2"
      Tab(1).Control(2)=   "Frame1"
      Tab(1).ControlCount=   3
      Begin VB.Frame Frame4 
         Caption         =   "Bloqueo de cuenta de usuario+"
         Height          =   800
         Left            =   -74880
         TabIndex        =   24
         Top             =   1200
         Width           =   7065
         Begin VB.OptionButton opMostTodos 
            Caption         =   "Mostrar todos+"
            Height          =   195
            Left            =   5280
            TabIndex        =   19
            Top             =   360
            Value           =   -1  'True
            Width           =   1695
         End
         Begin VB.OptionButton opMostBloq 
            Caption         =   "Mostrar solo bloqueados+"
            Height          =   195
            Left            =   2640
            TabIndex        =   18
            Top             =   360
            Width           =   2535
         End
         Begin VB.OptionButton opMostAct 
            Caption         =   "Mostrar solo activos+"
            Height          =   195
            Left            =   360
            TabIndex        =   17
            Top             =   360
            Width           =   2160
         End
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   800
         Left            =   -74880
         TabIndex        =   14
         Top             =   345
         Width           =   7065
         Begin VB.CheckBox chkAcc 
            Caption         =   "Incluir acciones de seguridad disponibles+"
            Height          =   240
            Left            =   3150
            TabIndex        =   16
            Top             =   360
            Width           =   3870
         End
         Begin VB.CheckBox chkDatos 
            Caption         =   "Incluir datos de la persona+"
            Height          =   240
            Left            =   180
            TabIndex        =   15
            Top             =   360
            Value           =   1  'Checked
            Width           =   2880
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Orden+"
         Height          =   800
         Left            =   -74880
         TabIndex        =   22
         Top             =   2040
         Width           =   7065
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo+"
            Height          =   195
            Left            =   1440
            TabIndex        =   20
            Top             =   350
            Value           =   -1  'True
            Width           =   2160
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n+"
            Height          =   195
            Left            =   3840
            TabIndex        =   21
            Top             =   350
            Width           =   2805
         End
      End
      Begin VB.Frame Frame3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3200
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   7065
         Begin VB.OptionButton opSoloFSIM 
            Caption         =   "DS�lo usuarios con acceso al FSIM"
            Height          =   255
            Left            =   915
            TabIndex        =   8
            Top             =   1880
            Width           =   4740
         End
         Begin VB.OptionButton opSoloContratos 
            Caption         =   "DS�lo usuarios con acceso a FSCM"
            Height          =   255
            Left            =   915
            TabIndex        =   9
            Top             =   2160
            Width           =   4740
         End
         Begin VB.OptionButton opSoloINT 
            Caption         =   "DS�lo usuarios con acceso a FSIS"
            Height          =   255
            Left            =   915
            TabIndex        =   10
            Top             =   2440
            Width           =   4740
         End
         Begin VB.OptionButton opSoloQA 
            Caption         =   "DS�lo con acceso a FSQA"
            Height          =   225
            Left            =   915
            TabIndex        =   6
            Top             =   1320
            Width           =   3200
         End
         Begin VB.OptionButton opSoloWS 
            Caption         =   "DS�lo con acceso al FSWS"
            Height          =   225
            Left            =   915
            TabIndex        =   5
            Top             =   1040
            Width           =   4000
         End
         Begin VB.OptionButton opSoloGS 
            Caption         =   "DSolo con acceso al FSGS"
            Height          =   225
            Left            =   915
            TabIndex        =   4
            Top             =   760
            Width           =   3255
         End
         Begin VB.OptionButton opSoloEP 
            Caption         =   "S�lo con acceso al E-Procurement"
            Height          =   225
            Left            =   915
            TabIndex        =   7
            Top             =   1600
            Width           =   3375
         End
         Begin VB.OptionButton opUsuario 
            Caption         =   "Usuario:+"
            Height          =   225
            Left            =   915
            TabIndex        =   11
            Top             =   2720
            Width           =   1860
         End
         Begin VB.OptionButton opTipoUsu 
            Caption         =   "S�lo compradores"
            Height          =   225
            Left            =   915
            TabIndex        =   3
            Top             =   480
            Width           =   1905
         End
         Begin VB.OptionButton opTodos 
            Caption         =   "Todos los usuarios+"
            Height          =   225
            Left            =   915
            TabIndex        =   2
            Top             =   200
            Value           =   -1  'True
            Width           =   2250
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcUsuCod 
            Height          =   285
            Left            =   2925
            TabIndex        =   12
            Top             =   2720
            Width           =   1755
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2434
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7726
            Columns(1).Caption=   "Denominacion"
            Columns(1).Name =   "Denominacion"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Tipo"
            Columns(2).Name =   "Tipo"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   2
            Columns(2).FieldLen=   256
            _ExtentX        =   3096
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4080
         Top             =   150
      End
   End
End
Attribute VB_Name = "frmLstUSUARIOS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private RespetarCombo As Boolean
Private CargarComboDesde As Boolean
'MULTILENGUAJE
Private sEspera(3) As String         'textos para formulario de espera
Private sTitulo As String
Private FormulaRpt(1 To 2, 1 To 22) As String      'formulas textos RPT
Private FormulaSubRpt(1 To 2, 1 To 2) As String   'formulas textos SUBRPT
Private sListadoSel As String                  'formula SEL para listado USUARIOS

''' <summary>procedimiento de carga inicial del formulario</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1 sec</remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub Form_Load()
    Me.Height = 4830
    Me.Width = 7455
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    PonerFieldSeparator Me
    
    If FSEPConf = True Then
        opTipoUsu.Visible = False
        opTipoUsu.Value = False
        
        opSoloGS.Top = opTipoUsu.Top
        opSoloWS.Top = opSoloGS.Top + 280
        opSoloQA.Top = opSoloWS.Top + 280
        opSoloEP.Top = opSoloQA.Top + 280
        opSoloFSIM.Top = opSoloEP.Top + 280
        opSoloContratos.Top = opSoloFSIM.Top + 280
        opSoloINT.Top = opSoloContratos.Top + 280
        opUsuario.Top = opSoloINT.Top + 280
        sdbcUsuCod.Top = opUsuario.Top
    Else
        If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.SinAcceso Then
            opSoloWS.Visible = False
            opSoloWS.Value = False
            opSoloQA.Top = opSoloWS.Top
            opSoloEP.Top = opSoloQA.Top + 280
                        opSoloFSIM.Top = opSoloEP.Top + 280
                        opSoloContratos.Top = opSoloFSIM.Top + 280
                        opSoloINT.Top = opSoloContratos.Top + 280
                        opUsuario.Top = opSoloINT.Top + 280
            sdbcUsuCod.Top = opUsuario.Top
        End If
        
        If gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso Then
            opSoloQA.Visible = False
            opSoloQA.Value = False
            opSoloEP.Top = opSoloQA.Top
                        opSoloFSIM.Top = opSoloEP.Top + 280
                        opSoloContratos.Top = opSoloFSIM.Top + 280
                        opSoloINT.Top = opSoloContratos.Top + 280
                        opUsuario.Top = opSoloINT.Top + 280
            sdbcUsuCod.Top = opUsuario.Top
        End If
        
        If Not gParametrosGenerales.gbPedidosAprov Then
            opSoloEP.Visible = False
            opSoloEP.Value = False
                        opSoloFSIM.Top = opSoloEP.Top
                        opSoloContratos.Top = opSoloFSIM.Top + 280
                        opSoloINT.Top = opSoloContratos.Top + 280
                        opUsuario.Top = opSoloINT.Top + 280
            sdbcUsuCod.Top = opUsuario.Top
        End If
        
        If gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso Then
            opSoloFSIM.Visible = False
            opSoloFSIM.Value = False
                        opSoloContratos.Top = opSoloFSIM.Top
                        opSoloINT.Top = opSoloContratos.Top + 280
                        opUsuario.Top = opSoloINT.Top + 280
            sdbcUsuCod.Top = opUsuario.Top
        End If

        If gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.SinAcceso Then
            opSoloContratos.Visible = False
            opSoloContratos.Value = False
                        opSoloINT.Top = opSoloContratos.Top
                        opUsuario.Top = opSoloINT.Top + 280
            sdbcUsuCod.Top = opUsuario.Top
        End If
        
        If Not gParametrosGenerales.gbIntegracion Then
            opSoloINT.Visible = False
            opSoloINT.Value = False
                        opUsuario.Top = opSoloINT.Top
            sdbcUsuCod.Top = opUsuario.Top
        End If
    End If
    
    'Si no est� activo el bloqueo que desaparezca el frame:
    If gParametrosGenerales.giLOGPREBLOQ = 0 Then
        Frame4.Visible = False
        Frame2.Top = 480
        Frame2.Height = 1000
        Frame1.Height = 1000
        Frame1.Top = Frame4.Top + 400
    End If
    
    opTodos.Value = True
End Sub

Private Sub opSoloEP_Click()
    sdbcUsuCod.Value = ""
End Sub
Private Sub opSoloGS_Click()
    sdbcUsuCod.Value = ""
End Sub
Private Sub opSoloQA_Click()
    sdbcUsuCod.Value = ""
End Sub
Private Sub opSoloWS_Click()
    sdbcUsuCod.Value = ""
End Sub
Private Sub opTodos_Click()
    sdbcUsuCod.Value = ""
End Sub
Private Sub opTipoUsu_Click()
    sdbcUsuCod.Value = ""
End Sub
Private Sub opSoloINT_Click()
    sdbcUsuCod.Value = ""
End Sub
Private Sub opSoloContratos_Click()
    sdbcUsuCod.Value = ""
End Sub
Private Sub opSoloFSIM_Click()
    sdbcUsuCod.Value = ""
End Sub

Private Sub sdbcUsuCod_Change()
    If Not RespetarCombo Then
        CargarComboDesde = True
    End If
End Sub

Private Sub sdbcUsucod_CloseUp()
    If sdbcUsuCod.Value = "..." Then
        sdbcUsuCod.Text = ""
        Exit Sub
    End If
    
    If sdbcUsuCod.Text = "" Then
        CargarComboDesde = False
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcUsuCod.Text = sdbcUsuCod.Columns(0).Text
    RespetarCombo = False
    
    opUsuario.Value = True
    CargarComboDesde = False
End Sub

Private Sub sdbcUsuCod_DropDown()
    Dim Codigos() As TipoDatosUsu
    Dim i As Integer
        
    sdbcUsuCod.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass
    Codigos = oGestorSeguridad.BuscarTodosLosUsuarios
    For i = 0 To UBound(Codigos)
        sdbcUsuCod.AddItem Codigos(i).USU & Chr(m_lSeparador) & Codigos(i).Nom & Chr(m_lSeparador) & Codigos(i).Tipo
    Next

    opUsuario.Value = True
    sdbcUsuCod.SelStart = 0
    sdbcUsuCod.SelLength = Len(sdbcUsuCod.Text)
    sdbcUsuCod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcusucod_InitColumnProps()
    sdbcUsuCod.DataFieldList = "Column 0"
    sdbcUsuCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcusucod_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    sdbcUsuCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcUsuCod.Rows - 1
            bm = sdbcUsuCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcUsuCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcUsuCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Public Sub sdbcusucod_Validate(Cancel As Boolean)
    Dim oUsuarios As CUsuarios
    Dim bExiste As Boolean
    
    Set oUsuarios = oFSGSRaiz.Generar_CUsuarios
    
    If sdbcUsuCod.Text = "" Then
        CargarComboDesde = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el usuario
    Screen.MousePointer = vbHourglass
    Set oUsuarios = oGestorSeguridad.DevolverTodosLosUsuarios(sdbcUsuCod.Text, , True)
    
    bExiste = Not oUsuarios Is Nothing
    
    If Not bExiste Then
        sdbcUsuCod.Text = ""
    Else
        RespetarCombo = True
        sdbcUsuCod.Text = oUsuarios.Item(sdbcUsuCod).Cod
        RespetarCombo = False
        CargarComboDesde = False
    End If
    
    Set oUsuarios = Nothing
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmLstUSUARIOS.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTUSUARIOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        chkAcc.caption = Ador(0).Value
        Ador.MoveNext
        chkDatos.caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        Frame1.caption = Ador(0).Value
        Ador.MoveNext
        frmLstUSUARIOS.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        opTodos.caption = Ador(0).Value
        Ador.MoveNext
        opUsuario.caption = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sTitulo = Ador(0).Value
        FormulaRpt(1, 1) = sTitulo
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        Ador.MoveNext
        sdbcUsuCod.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcUsuCod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        opMostAct.caption = Ador(0).Value
        Ador.MoveNext
        opMostBloq.caption = Ador(0).Value
        Ador.MoveNext
        opMostTodos.caption = Ador(0).Value
        Ador.MoveNext
        Frame4.caption = Ador(0).Value
        Ador.MoveNext
         'FORMULAS rpt
        For i = 2 To 20
            FormulaRpt(1, i) = Ador(0).Value
            Ador.MoveNext
        Next
         'FORMULAS SUBrpt
        For i = 1 To 2
            FormulaSubRpt(1, i) = Ador(0).Value
            Ador.MoveNext
        Next
        sListadoSel = Ador(0).Value
        Ador.MoveNext
        opTipoUsu.caption = Ador(0).Value
        
        Ador.MoveNext
        FormulaRpt(1, 22) = Ador(0).Value
        
        Ador.MoveNext
        opSoloEP.caption = Ador(0).Value  '46 S�lo usuarios con acceso al FSEP
        Ador.MoveNext
        opSoloGS.caption = Ador(0).Value  '47 S�lo usuarios con acceso al FSGS
        Ador.MoveNext
        opSoloWS.caption = Ador(0).Value  '48 S�lo usuarios con acceso al FSWS
        Ador.MoveNext
        opSoloQA.caption = Ador(0).Value '49 S�lo usuarios con acceso al FSQA
        Ador.MoveNext
        opSoloINT.caption = Ador(0).Value '50 S�lo usuarios con acceso a FSIS
        Ador.MoveNext
        opSoloContratos.caption = Ador(0).Value '51 S�lo usuarios con acceso a FSCM
        Ador.MoveNext
        opSoloFSIM.caption = Ador(0).Value '52 S�lo usuarios con acceso al FSIM
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

''' <summary>Obtener el listado de usuarios</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo < 1</remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub cmdObtener_Click()
    Dim oReport As Object
    Dim SubListado As CRAXDRT.Report
    Dim oCRSeguridad As CRSeguridad
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim i As Integer
    Dim sSeleccion As String
    Dim iCuentasUsuario As TipoEstadoCuentasUsuario
    
    If crs_Connected = False Then Exit Sub
        
    Set oCRSeguridad = GenerarCRSeguridad
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If

    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptUSUARIOS.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
' FORMULAS TEXTO RPT
' MISMAS FORMULAS PARA LOS DOS REPORTS
'*************************************
    FormulaRpt(2, 1) = "txtTITULO"
    FormulaRpt(2, 2) = "txtPAG"
    FormulaRpt(2, 3) = "txtDE"
    FormulaRpt(2, 4) = "txtSEL"
    FormulaRpt(2, 5) = "txtUSUARIO"
    FormulaRpt(2, 6) = "txtEQP"
    FormulaRpt(2, 7) = "txtTFNO"
    FormulaRpt(2, 8) = "txtFAX"
    FormulaRpt(2, 9) = "txtEMAIL"
    FormulaRpt(2, 10) = "txtPER"
    FormulaRpt(2, 11) = "txtPERFILPER"
    FormulaRpt(2, 12) = "txtPERFILX"
    FormulaRpt(2, 13) = "txtCARGO"
    FormulaRpt(2, 14) = "txtUO"
    FormulaRpt(2, 15) = "txtDPTO"
    FormulaRpt(2, 16) = "txtTIPOC"
    FormulaRpt(2, 17) = "txtTIPOPER"
    FormulaRpt(2, 18) = "SEL"          'Todos los usuarios clasificados por tipo de usuario
    FormulaRpt(2, 19) = "txtBLOQ"
    FormulaRpt(2, 20) = "txtNO_BLOQ"
    FormulaRpt(2, 21) = "LOGPREBLOQ"
    FormulaRpt(2, 22) = "txtNOMPER"
        
    For i = 1 To UBound(FormulaRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, FormulaRpt(2, i))).Text = """" & FormulaRpt(1, i) & """"
    Next i
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, FormulaRpt(2, 21))).Text = CStr(gParametrosGenerales.giLOGPREBLOQ)
            
    If sdbcUsuCod.Text <> "" Then
        sSeleccion = sListadoSel & " " & sdbcUsuCod
    ElseIf opTipoUsu.Value = True Then
        sSeleccion = opTipoUsu.caption
    ElseIf opSoloEP.Value = True Then
        sSeleccion = opSoloEP.caption
    ElseIf opSoloGS.Value = True Then
        sSeleccion = opSoloGS.caption
    ElseIf opSoloWS.Value = True Then
        sSeleccion = opSoloWS.caption
    ElseIf opSoloQA.Value = True Then
        sSeleccion = opSoloQA.caption
    ElseIf opSoloINT.Value = True Then
        sSeleccion = opSoloINT.caption
    ElseIf opSoloContratos.Value = True Then
        sSeleccion = opSoloContratos.caption
    ElseIf opSoloFSIM.Value = True Then
        sSeleccion = opSoloFSIM.caption
    Else
        sSeleccion = ""
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSEL")).Text = ""
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    
    If FSEPConf Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "PEREQP")).Text = ""
    End If
    
    If chkAcc Then
        Set SubListado = oReport.OpenSubreport("MENU_PERF")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "PROY1")).Text = """" & gParametrosGenerales.gsPlurPres1 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "PROY2")).Text = """" & gParametrosGenerales.gsPlurPres2 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "PROY3")).Text = """" & gParametrosGenerales.gsPlurPres3 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "PROY4")).Text = """" & gParametrosGenerales.gsPlurPres4 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPROP")).Text = """" & FormulaSubRpt(1, 1) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtACCION")).Text = """" & FormulaSubRpt(1, 2) & """"
        Set SubListado = Nothing
        
        Set SubListado = oReport.OpenSubreport("MENU_PERF - 01")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "PROY1")).Text = """" & gParametrosGenerales.gsPlurPres1 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "PROY2")).Text = """" & gParametrosGenerales.gsPlurPres2 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "PROY3")).Text = """" & gParametrosGenerales.gsPlurPres3 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "PROY4")).Text = """" & gParametrosGenerales.gsPlurPres4 & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPROP")).Text = """" & FormulaSubRpt(1, 1) & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtACCION")).Text = """" & FormulaSubRpt(1, 2) & """"
        Set SubListado = Nothing
    End If
    
    If opMostAct.Value = True Then iCuentasUsuario = 0
    If opMostBloq.Value = True Then iCuentasUsuario = 1
    If opMostTodos.Value = True Then iCuentasUsuario = 2
                
    If sdbcUsuCod.Text <> "" Then  'filtro por usuario
        oCRSeguridad.ListadoUSUARIOS oReport, 0, sdbcUsuCod.Text, opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario
    ElseIf opTipoUsu.Value Then   's�lo compradores
        oCRSeguridad.ListadoUSUARIOS oReport, 2, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario
    ElseIf opSoloEP.Value Then  's�lo usuarios con acceso al EP
        oCRSeguridad.ListadoUSUARIOS oReport, 0, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario, True
    ElseIf opSoloGS.Value Then  's�lo usuarios con acceso al GS
        oCRSeguridad.ListadoUSUARIOS oReport, 0, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario, False, True
    ElseIf opSoloWS.Value Then  's�lo usuarios con acceso al WS
        oCRSeguridad.ListadoUSUARIOS oReport, 0, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario, False, False, True
    ElseIf opSoloQA.Value Then  's�lo usuarios con acceso al QA
        oCRSeguridad.ListadoUSUARIOS oReport, 0, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario, False, False, False, True
    ElseIf opSoloINT.Value Then  's�lo usuarios con acceso al visor de integraci�n
        oCRSeguridad.ListadoUSUARIOS oReport, 0, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario, False, False, False, False, True
    ElseIf opSoloContratos.Value Then  's�lo usuarios con acceso al Contratos
        oCRSeguridad.ListadoUSUARIOS oReport, 0, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario, False, False, False, False, False, True
    ElseIf opSoloFSIM.Value Then  's�lo usuarios con acceso a FSIM
        oCRSeguridad.ListadoUSUARIOS oReport, 0, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario, False, False, False, False, False, False, True
    Else  'Todos
        oCRSeguridad.ListadoUSUARIOS oReport, 0, "", opOrdDen.Value, chkDatos.Value, chkAcc.Value, gParametrosGenerales.gbOblProveEqp, basPublic.gParametrosInstalacion.gIdioma, iCuentasUsuario
    End If
        
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo   'Listado de Usuarios
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.lblGeneral.caption = sEspera(1) & " " & pv.caption      'Generando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)  'Seleccionando registros ...
    frmESPERA.lblDetalle = sEspera(3)  'Visualizando listado ...
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
End Sub


Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub
