VERSION 5.00
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmDescripAnya 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DAnyadir grupo"
   ClientHeight    =   3060
   ClientLeft      =   3000
   ClientTop       =   2970
   ClientWidth     =   7050
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDescripAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3060
   ScaleWidth      =   7050
   Begin UltraGrid.SSUltraGrid ssDen 
      Height          =   2325
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   6810
      _ExtentX        =   12012
      _ExtentY        =   4101
      _Version        =   131072
      GridFlags       =   17040384
      LayoutFlags     =   68157444
      ScrollBars      =   0
      Override        =   "frmDescripAnya.frx":0CB2
      Caption         =   "ssDen"
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   2295
      TabIndex        =   1
      Top             =   2640
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3495
      TabIndex        =   2
      Top             =   2640
      Width           =   975
   End
   Begin VB.TextBox txtDen 
      Height          =   2325
      Left            =   135
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   105
      Width           =   6780
   End
   Begin VB.Label lblDen 
      BackColor       =   &H00808000&
      Caption         =   "DDenominaci�n:"
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   120
      TabIndex        =   3
      Top             =   280
      Width           =   1200
   End
End
Attribute VB_Name = "frmDescripAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas:
Public g_sOrigen As String
Public g_bModif As Boolean

'Variables privadas:
Private m_oIdiomas As CIdiomas
Private oIdioma As CIdioma
Private m_dSizeGrid As Double

'Variables de idiomas:
Private m_sIdiDescr As String
Private m_sIdiDen As String
Private m_sIdiCaptionSolDen As String
Private m_sIdiCaptionSolDescr As String
Private m_sIdioma As String

Private Sub Form_Load()
    
    CargarRecursos
    
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, True, False)
    
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("MULT").Value = 1 Then
                 'Es multiidioma,muestra la grid:
                 ssDen.Visible = True
                 lblDen.Visible = False
                 txtDen.Visible = False
                 
                 CargarGridIdiomas
             Else
                 'Se muestra solo el textbox
                 ssDen.Visible = False
                 lblDen.Visible = True
                 txtDen.Visible = True
                 
                 If frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DEN" Then
                    txtDen.Text = NullToStr(frmSOLConfiguracion.g_oSolicitudSeleccionada.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                 Else
                    txtDen.Text = NullToStr(frmSOLConfiguracion.g_oSolicitudSeleccionada.Descripciones.Item(gParametrosInstalacion.gIdioma).Den)
                 End If
             End If
    
            'Caption dependiendo de si es Denominaci�n o Descripci�n
            If frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DEN" Then
                Me.caption = m_sIdiCaptionSolDen
                lblDen.caption = m_sIdiDen
                txtDen.MaxLength = 100
            Else
                Me.caption = m_sIdiCaptionSolDescr
                lblDen.caption = m_sIdiDescr
                txtDen.MaxLength = 500
            End If
            
    End Select
    
    If g_bModif = False Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        Me.Height = Me.Height - 400
        txtDen.Locked = True
    End If
    
    
End Sub


Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DESCRIPANYA, gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        m_sIdiDen = Ador(0).Value   '1
        Ador.MoveNext
        m_sIdiDescr = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '3 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '4 &Cancelar
        Ador.MoveNext
        m_sIdiCaptionSolDen = Ador(0).Value ' 5
        Ador.MoveNext
        m_sIdiCaptionSolDescr = Ador(0).Value
        Ador.MoveNext
        m_sIdioma = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub

Private Sub cmdAceptar_Click()
    Dim oIdioma As CIdioma
    Dim oRow As SSRow
    Dim vDenominacion As Variant
    
    If Not (g_sOrigen = "frmSOLConfiguracion" And frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DESCR") Then
        If txtDen.Visible = True Then
            If Trim(txtDen.Text) = "" Then
                oMensajes.NoValido m_sIdiDen
                If Me.Visible Then txtDen.SetFocus
                Exit Sub
            End If
        Else
            'Tiene que introducir la denominaci�n por lo menos en el idioma de la aplicaci�n:
            ssDen.ActiveRow = ssDen.GetRow(ssChildRowFirst)
            Set oRow = ssDen.ActiveRow
            If (oRow.Cells("DEN").Value = "" Or IsNull(oRow.Cells("DEN").Value)) And oRow.Cells("COD_IDI").Value = gParametrosInstalacion.gIdioma Then
                oMensajes.IntroduzcaDen
                Exit Sub
            End If
            While oRow.HasNextSibling
                Set oRow = oRow.GetSibling(ssSiblingRowNext)
                If (oRow.Cells("DEN").Value = "" Or IsNull(oRow.Cells("DEN").Value)) And oRow.Cells("COD_IDI").Value = gParametrosInstalacion.gIdioma Then
                    oMensajes.NoValido (oRow.Cells("IDIOMA").Value)
                    Exit Sub
                End If
            Wend
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If txtDen.Visible = False Then  'Es multiidioma
                Set oRow = ssDen.GetRow(ssChildRowFirst)
                    
                If Trim(oRow.Cells("DEN").Value) = "" Then
                    vDenominacion = Null
                Else
                    vDenominacion = Trim(oRow.Cells("DEN").Value)
                End If

                If frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DEN" Then
                    frmSOLConfiguracion.g_oSolicitudSeleccionada.Denominaciones.Item(oRow.Cells("COD_IDI").Value).Den = vDenominacion
                Else
                    frmSOLConfiguracion.g_oSolicitudSeleccionada.Descripciones.Item(oRow.Cells("COD_IDI").Value).Den = vDenominacion
                End If

                While oRow.HasNextSibling
                    Set oRow = oRow.GetSibling(ssSiblingRowNext)
                    If Trim(oRow.Cells("DEN").Value) = "" Then
                        vDenominacion = Null
                    Else
                        vDenominacion = Trim(oRow.Cells("DEN").Value)
                    End If
                    If frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DEN" Then
                        frmSOLConfiguracion.g_oSolicitudSeleccionada.Denominaciones.Item(oRow.Cells("COD_IDI").Value).Den = vDenominacion
                    Else
                        frmSOLConfiguracion.g_oSolicitudSeleccionada.Descripciones.Item(oRow.Cells("COD_IDI").Value).Den = vDenominacion
                    End If
                Wend
        
            Else  'No multiidioma
                For Each oIdioma In m_oIdiomas
                    If Trim(txtDen.Text) = "" Then
                        vDenominacion = Null
                    Else
                        vDenominacion = Trim(txtDen.Text)
                    End If
                    
                    If frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DEN" Then
                        frmSOLConfiguracion.g_oSolicitudSeleccionada.Denominaciones.Item(oIdioma.Cod).Den = vDenominacion
                    Else
                        frmSOLConfiguracion.g_oSolicitudSeleccionada.Descripciones.Item(oIdioma.Cod).Den = vDenominacion
                    End If
                Next
            End If
            frmSOLConfiguracion.g_bCancel = False
    End Select
    
    Screen.MousePointer = vbNormal
    
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub CargarGridIdiomas()
    
    Dim Ador As ADODB.Recordset
    Dim oIdioma As CIdioma
    Dim stm  As ADODB.Stream
    Dim rs As ADODB.Recordset

    Set Ador = New Ador.Recordset

    Ador.Fields.Append "COD_IDI", adVarChar, 200
    Ador.Fields.Append "IDIOMA", adVarChar, 200
    
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DEN" Then
                Ador.Fields.Append "DEN", adVarChar, 100, adFldMayBeNull
            Else
                Ador.Fields.Append "DEN", adVarChar, 500, adFldMayBeNull
            End If
    End Select
    
    Ador.Open

    For Each oIdioma In m_oIdiomas
        Ador.AddNew
        Ador("COD_IDI").Value = oIdioma.Cod
        Ador("IDIOMA").Value = oIdioma.Den
        
        Select Case g_sOrigen
            Case "frmSOLConfiguracion"
                If frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DEN" Then
                    If frmSOLConfiguracion.g_oSolicitudSeleccionada.Denominaciones.Count > 0 Then
                        If Not frmSOLConfiguracion.g_oSolicitudSeleccionada.Denominaciones.Item(CStr(oIdioma.Cod)) Is Nothing Then
                            Ador("DEN").Value = frmSOLConfiguracion.g_oSolicitudSeleccionada.Denominaciones.Item(oIdioma.Cod).Den
                        Else
                            Ador("DEN").Value = ""
                        End If
                    Else
                        Ador("DEN").Value = ""
                    End If
                    
                Else 'Descripciones
                    If frmSOLConfiguracion.g_oSolicitudSeleccionada.Descripciones.Count > 0 Then
                        If Not frmSOLConfiguracion.g_oSolicitudSeleccionada.Descripciones.Item(CStr(oIdioma.Cod)) Is Nothing Then
                            Ador("DEN").Value = frmSOLConfiguracion.g_oSolicitudSeleccionada.Descripciones.Item(oIdioma.Cod).Den
                        Else
                            Ador("DEN").Value = ""
                        End If
                    Else
                        Ador("DEN").Value = ""
                    End If
                End If
        End Select
    Next

    Set stm = New ADODB.Stream

    Ador.Save stm, adPersistXML
    Ador.Close
    Set Ador = Nothing
    
    Set rs = New ADODB.Recordset
    rs.Open stm
    rs.ActiveConnection = Nothing
    
    stm.Close
    Set stm = Nothing
    
    Set ssDen.DataSource = rs

    Set rs = Nothing
 
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oIdiomas = Nothing
    g_bModif = False
    g_sOrigen = ""
    m_dSizeGrid = 0
    
    DoEvents
    Set ssDen.DataSource = Nothing
    DoEvents
    
End Sub

Private Sub ssDen_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    If ssDen.Visible = False Then Exit Sub
    
    ssDen.Bands(0).Columns("COD_IDI").Hidden = True
    
    With Layout.Appearances.Add("Gris")
        .BackColor = RGB(223, 223, 223)
    End With
    ssDen.Bands(0).Columns("IDIOMA").CellAppearance = "Gris"
    
    ssDen.Bands(0).Columns("IDIOMA").Activation = ssActivationActivateNoEdit
    If g_bModif = True Then
        ssDen.Bands(0).Columns("DEN").Activation = ssActivationAllowEdit
    Else
        ssDen.Bands(0).Columns("DEN").Activation = ssActivationActivateNoEdit
    End If
    
    ssDen.Override.CellMultiLine = ssCellMultiLineTrue
    ssDen.Override.RowSizing = ssRowSizingAutoFree
    ssDen.Override.RowSizingAutoMaxLines = 0
    ssDen.Bands(0).Columns("DEN").AutoSizeEdit = ssAutoSizeEditTrue
     
    ssDen.Bands(0).Columns("IDIOMA").Width = ssDen.Width * 0.16
    ssDen.Bands(0).Columns("DEN").Width = ssDen.Width * 0.76
    
    'Caption de las columnas:
    ssDen.Bands(0).Columns("IDIOMA").Header.caption = m_sIdioma
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If frmSOLConfiguracion.ssSOLConfig.ActiveCell.Column.key = "DEN" Then
                ssDen.Bands(0).Columns("DEN").Header.caption = m_sIdiDen
            Else
                ssDen.Bands(0).Columns("DEN").Header.caption = m_sIdiDescr
            End If
    End Select
    
    ssDen.ActiveRow = ssDen.GetRow(ssChildRowFirst)
    
    'Si es multiidioma ajusta la la grid a las filas y el formulario a la grid:
    If m_dSizeGrid <> 0 Then
        ssDen.Height = m_dSizeGrid + 270
        If g_bModif = False Then
            Me.Height = ssDen.Top + ssDen.Height + 550
        Else
            cmdAceptar.Top = ssDen.Top + ssDen.Height + 85
            cmdCancelar.Top = cmdAceptar.Top
            Me.Height = cmdAceptar.Top + cmdAceptar.Height + 550
        End If
    End If
End Sub

Private Sub ssDen_InitializeRow(ByVal Context As UltraGrid.Constants_Context, ByVal Row As UltraGrid.SSRow, ByVal ReInitialize As Boolean)
    m_dSizeGrid = m_dSizeGrid + Row.Height
End Sub
