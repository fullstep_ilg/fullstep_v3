VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSOLAbrirProc 
   BackColor       =   &H00808000&
   Caption         =   "DSolicitud de compra..."
   ClientHeight    =   8985
   ClientLeft      =   0
   ClientTop       =   345
   ClientWidth     =   10665
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLAbrirProc.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8985
   ScaleWidth      =   10665
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picAtrbEsp 
      BorderStyle     =   0  'None
      Height          =   1575
      Left            =   240
      ScaleHeight     =   1575
      ScaleWidth      =   10095
      TabIndex        =   21
      Top             =   6480
      Width           =   10095
      Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
         Height          =   915
         Left            =   4800
         TabIndex        =   24
         Top             =   1200
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSOLAbrirProc.frx":0CB2
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns(0).Width=   3201
         Columns(0).Name =   "NOMBRE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
         Height          =   1110
         Left            =   0
         TabIndex        =   22
         Top             =   360
         Width           =   10095
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   8
         stylesets.count =   3
         stylesets(0).Name=   "Checked"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSOLAbrirProc.frx":0CCE
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSOLAbrirProc.frx":0F2A
         stylesets(2).Name=   "UnChecked"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSOLAbrirProc.frx":0F46
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   8
         Columns(0).Width=   2143
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasHeadForeColor=   -1  'True
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).HeadBackColor=   -2147483644
         Columns(0).BackColor=   16777088
         Columns(1).Width=   5741
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   200
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).HeadBackColor=   -2147483644
         Columns(1).BackColor=   16777088
         Columns(2).Width=   3519
         Columns(2).Caption=   "Valor"
         Columns(2).Name =   "VALOR"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   800
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadBackColor=   -2147483644
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "TIPO"
         Columns(3).Name =   "TIPO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "INTRO"
         Columns(4).Name =   "INTRO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "ID_A"
         Columns(5).Name =   "ID_A"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "ID_P"
         Columns(6).Name =   "ID_P"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "OBL"
         Columns(7).Name =   "OBL"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   11
         Columns(7).FieldLen=   256
         _ExtentX        =   17806
         _ExtentY        =   1958
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblOtrosDatos 
         Caption         =   "Otros datos:"
         Height          =   375
         Left            =   0
         TabIndex        =   23
         Top             =   120
         Width           =   975
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgLineas 
      Height          =   2535
      Left            =   240
      TabIndex        =   18
      Top             =   3840
      Width           =   10095
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   27
      stylesets.count =   9
      stylesets(0).Name=   "apedido"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSOLAbrirProc.frx":11A2
      stylesets(0).AlignmentText=   0
      stylesets(0).AlignmentPicture=   3
      stylesets(1).Name=   "RiesgoBajoamarillo"
      stylesets(1).ForeColor=   32768
      stylesets(1).BackColor=   14680063
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSOLAbrirProc.frx":132E
      stylesets(2).Name=   "RiesgoAltoamarillo"
      stylesets(2).ForeColor=   202
      stylesets(2).BackColor=   14680063
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSOLAbrirProc.frx":134A
      stylesets(3).Name=   "normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSOLAbrirProc.frx":1366
      stylesets(4).Name=   "amarillo"
      stylesets(4).BackColor=   14680063
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSOLAbrirProc.frx":1382
      stylesets(5).Name=   "RiesgoBajo"
      stylesets(5).ForeColor=   32768
      stylesets(5).BackColor=   16777215
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmSOLAbrirProc.frx":139E
      stylesets(6).Name=   "RiesgoMedio"
      stylesets(6).ForeColor=   4227327
      stylesets(6).BackColor=   16777215
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmSOLAbrirProc.frx":13BA
      stylesets(7).Name=   "RiesgoAlto"
      stylesets(7).ForeColor=   202
      stylesets(7).BackColor=   16777215
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmSOLAbrirProc.frx":13D6
      stylesets(8).Name=   "RiesgoMedioamarillo"
      stylesets(8).ForeColor=   4227327
      stylesets(8).BackColor=   14680063
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmSOLAbrirProc.frx":13F2
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      CellNavigation  =   1
      StyleSet        =   "normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   27
      Columns(0).Width=   926
      Columns(0).Caption=   "SEL"
      Columns(0).Name =   "SEL"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(0).ButtonsAlways=   -1  'True
      Columns(0).StyleSet=   "amarillo"
      Columns(1).Width=   2037
      Columns(1).Caption=   "DSolicitud"
      Columns(1).Name =   "SOLICITUD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).StyleSet=   "amarillo"
      Columns(2).Width=   1191
      Columns(2).Caption=   "NUM"
      Columns(2).Name =   "NUM"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).StyleSet=   "amarillo"
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "LINEA"
      Columns(3).Name =   "LINEA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "amarillo"
      Columns(4).Width=   3201
      Columns(4).Caption=   "GR_DESTINO_PROC"
      Columns(4).Name =   "GR_DESTINO_PROC"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   1
      Columns(4).StyleSet=   "amarillo"
      Columns(5).Width=   4419
      Columns(5).Caption=   "FAM_MAT"
      Columns(5).Name =   "FAM_MAT"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "amarillo"
      Columns(6).Width=   3200
      Columns(6).Caption=   "COD_ART"
      Columns(6).Name =   "COD_ART"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).StyleSet=   "amarillo"
      Columns(7).Width=   3200
      Columns(7).Caption=   "DESCR_ART"
      Columns(7).Name =   "DESCR_ART"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(7).StyleSet=   "amarillo"
      Columns(8).Width=   3200
      Columns(8).Caption=   "FEC_INI"
      Columns(8).Name =   "FEC_INI"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(8).StyleSet=   "amarillo"
      Columns(9).Width=   3200
      Columns(9).Caption=   "FEC_FIN"
      Columns(9).Name =   "FEC_FIN"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(9).StyleSet=   "amarillo"
      Columns(10).Width=   3200
      Columns(10).Caption=   "CANT"
      Columns(10).Name=   "CANT"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).NumberFormat=   "Standard"
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(10).StyleSet=   "amarillo"
      Columns(11).Width=   3200
      Columns(11).Caption=   "PRECUNI"
      Columns(11).Name=   "PRECUNI"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).NumberFormat=   "Standard"
      Columns(11).FieldLen=   256
      Columns(11).Locked=   -1  'True
      Columns(11).StyleSet=   "amarillo"
      Columns(12).Width=   3200
      Columns(12).Caption=   "UNIDAD"
      Columns(12).Name=   "UNIDAD"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(12).Locked=   -1  'True
      Columns(12).StyleSet=   "amarillo"
      Columns(13).Width=   3200
      Columns(13).Caption=   "IMPORTE"
      Columns(13).Name=   "IMPORTE"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).NumberFormat=   "Standard"
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(13).StyleSet=   "amarillo"
      Columns(14).Width=   3200
      Columns(14).Caption=   "DEST"
      Columns(14).Name=   "DEST"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(14).Locked=   -1  'True
      Columns(14).StyleSet=   "amarillo"
      Columns(15).Width=   3200
      Columns(15).Caption=   "PAGO"
      Columns(15).Name=   "PAGO"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(15).StyleSet=   "amarillo"
      Columns(16).Width=   3200
      Columns(16).Caption=   "PROV"
      Columns(16).Name=   "PROV"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      Columns(16).Style=   1
      Columns(16).StyleSet=   "amarillo"
      Columns(17).Width=   3200
      Columns(17).Visible=   0   'False
      Columns(17).Caption=   "ESP"
      Columns(17).Name=   "ESP"
      Columns(17).DataField=   "Column 17"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      Columns(17).Style=   4
      Columns(17).ButtonsAlways=   -1  'True
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "COD_PROVE"
      Columns(18).Name=   "COD_PROVE"
      Columns(18).DataField=   "Column 18"
      Columns(18).DataType=   8
      Columns(18).FieldLen=   256
      Columns(19).Width=   3200
      Columns(19).Visible=   0   'False
      Columns(19).Caption=   "GMN1"
      Columns(19).Name=   "GMN1"
      Columns(19).DataField=   "Column 19"
      Columns(19).DataType=   8
      Columns(19).FieldLen=   256
      Columns(20).Width=   3200
      Columns(20).Visible=   0   'False
      Columns(20).Caption=   "GMN2"
      Columns(20).Name=   "GMN2"
      Columns(20).DataField=   "Column 20"
      Columns(20).DataType=   8
      Columns(20).FieldLen=   256
      Columns(21).Width=   3200
      Columns(21).Visible=   0   'False
      Columns(21).Caption=   "GMN3"
      Columns(21).Name=   "GMN3"
      Columns(21).DataField=   "Column 21"
      Columns(21).DataType=   8
      Columns(21).FieldLen=   256
      Columns(22).Width=   3200
      Columns(22).Visible=   0   'False
      Columns(22).Caption=   "GMN4"
      Columns(22).Name=   "GMN4"
      Columns(22).DataField=   "Column 22"
      Columns(22).DataType=   8
      Columns(22).FieldLen=   256
      Columns(23).Width=   3200
      Columns(23).Visible=   0   'False
      Columns(23).Caption=   "OLDPROVE"
      Columns(23).Name=   "OLDPROVE"
      Columns(23).DataField=   "Column 23"
      Columns(23).DataType=   8
      Columns(23).FieldLen=   256
      Columns(24).Width=   3200
      Columns(24).Visible=   0   'False
      Columns(24).Caption=   "GRUPO"
      Columns(24).Name=   "GRUPO"
      Columns(24).DataField=   "Column 24"
      Columns(24).DataType=   8
      Columns(24).FieldLen=   256
      Columns(25).Width=   3200
      Columns(25).Visible=   0   'False
      Columns(25).Caption=   "BM_AEP"
      Columns(25).Name=   "BM_AEP"
      Columns(25).AllowSizing=   0   'False
      Columns(25).DataField=   "Column 25"
      Columns(25).DataType=   8
      Columns(25).FieldLen=   256
      Columns(25).Locked=   -1  'True
      Columns(26).Width=   3200
      Columns(26).Visible=   0   'False
      Columns(26).Caption=   "RIESGO"
      Columns(26).Name=   "RIESGO"
      Columns(26).DataField=   "Column 26"
      Columns(26).DataType=   8
      Columns(26).FieldLen=   256
      _ExtentX        =   17806
      _ExtentY        =   4471
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   3400
      ScaleHeight     =   375
      ScaleWidth      =   3255
      TabIndex        =   17
      Top             =   8520
      Width           =   3255
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "DCancelar"
         Height          =   315
         Left            =   1500
         TabIndex        =   12
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdContinuar 
         Caption         =   "DContinuar"
         Height          =   315
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   1215
      End
   End
   Begin VB.Frame fraOpciones 
      BackColor       =   &H00808000&
      Height          =   2040
      Left            =   60
      TabIndex        =   16
      Top             =   1200
      Width           =   10455
      Begin VB.OptionButton optGenerar 
         BackColor       =   &H00808000&
         Caption         =   "DEnviar a pedido existente"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   4
         Left            =   120
         TabIndex        =   25
         Top             =   1600
         Width           =   3800
      End
      Begin VB.CheckBox chkAdjuntos 
         BackColor       =   &H00808000&
         Caption         =   "DTraspasar los archivos adjuntos al proceso"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   4320
         TabIndex        =   9
         Top             =   600
         Value           =   1  'Checked
         Width           =   6000
      End
      Begin VB.CheckBox chkAtrib 
         BackColor       =   &H00808000&
         Caption         =   "DTraspasar los campos de la solicitud como atributos del proceso de compra"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   4320
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   6000
      End
      Begin VB.OptionButton optGenerar 
         BackColor       =   &H00808000&
         Caption         =   "DGenerar pedidos directos"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   3
         Left            =   120
         TabIndex        =   7
         Top             =   1260
         Width           =   3800
      End
      Begin VB.OptionButton optGenerar 
         BackColor       =   &H00808000&
         Caption         =   "DGenerar adjudicaciones"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   2
         Left            =   120
         TabIndex        =   6
         Top             =   920
         Width           =   3800
      End
      Begin VB.OptionButton optGenerar 
         BackColor       =   &H00808000&
         Caption         =   "DGenerar ofertas"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   1
         Left            =   120
         TabIndex        =   5
         Top             =   580
         Width           =   3800
      End
      Begin VB.OptionButton optGenerar 
         BackColor       =   &H00808000&
         Caption         =   "DGenerar proceso de compra"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   3800
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
         Height          =   285
         Left            =   4320
         TabIndex        =   19
         Top             =   1320
         Width           =   5715
         DataFieldList   =   "Column 0"
         ListWidthAutoSize=   0   'False
         ListWidth       =   10081
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   10054
         Columns(0).Caption=   "NOM"
         Columns(0).Name =   "NOM"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "ID"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   10081
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblPlantilla 
         BackColor       =   &H00808000&
         Caption         =   "DIncorporar los atributos y especificaciones de la siguiente plantilla:"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4320
         TabIndex        =   20
         Top             =   1020
         Width           =   5175
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   4140
         X2              =   4140
         Y1              =   240
         Y2              =   1920
      End
   End
   Begin VB.Frame fraCabecera 
      BackColor       =   &H00808000&
      Height          =   1100
      Left            =   60
      TabIndex        =   13
      Top             =   60
      Width           =   10455
      Begin SSDataWidgets_B.SSDBCombo sdbcMat 
         Height          =   285
         Left            =   4680
         TabIndex        =   1
         Top             =   600
         Width           =   4870
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "GMN1"
         Columns(0).Name =   "GMN1"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "GMN2"
         Columns(1).Name =   "GMN2"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "GMN3"
         Columns(2).Name =   "GMN3"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "GMN4"
         Columns(3).Name =   "GMN4"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   8811
         Columns(4).Caption=   "DEN"
         Columns(4).Name =   "DEN"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   8590
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.CommandButton cmdBuscarMat 
         Height          =   285
         Left            =   9945
         Picture         =   "frmSOLAbrirProc.frx":140E
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   600
         Width           =   315
      End
      Begin VB.CommandButton cmdLimpiarMat 
         Height          =   285
         Left            =   9585
         Picture         =   "frmSOLAbrirProc.frx":1790
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   600
         Width           =   315
      End
      Begin VB.TextBox txtProceso 
         Height          =   285
         Left            =   120
         MaxLength       =   100
         TabIndex        =   0
         Top             =   600
         Width           =   4215
      End
      Begin VB.Label lblMat 
         BackColor       =   &H00808000&
         Caption         =   "DSeleccione el material del proceso"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4680
         TabIndex        =   15
         Top             =   240
         Width           =   5175
      End
      Begin VB.Label lblProceso 
         BackColor       =   &H00808000&
         Caption         =   "DNombre del proceso"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   2000
      End
   End
   Begin MSComctlLib.TabStrip ssTabGrupos 
      Height          =   5055
      Left            =   60
      TabIndex        =   10
      Top             =   3345
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   8916
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSOLAbrirProc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas:
Public g_arSolicitudes As Variant
Public g_bIrA As Boolean

'Para el filtro de material
Private m_sGMN1Cod As String
Private m_sGMN2Cod As String
Private m_sGMN3Cod As String
Private m_sGMN4Cod As String

'Clases:
Private m_oProceso As cProceso
Private m_oProcesoPlant As cProceso
Private m_oSolicitudSeleccionada As CInstancia
Private m_oGrupoSeleccionado As CGrupo

'idiomas
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sIdiCaption As String
Private m_sIdiProve As String
Private m_sSolicitud As String
'3275
Private sIdiMayor As String
Private sIdiMenor As String
Private sIdiEntre As String

Private sIdiItem As String
Private sIdiGrupo As String
Private sIdiProceso As String

Private bMostrarAtrEsp As Boolean

'Seguridad:
Private m_bRestrMat As Boolean
Private m_bVerSelecProve As Boolean
Private m_bVerRecOfe As Boolean
Private m_bVerComparativa As Boolean
Private m_bModifProcPlantilla As Boolean
Private m_bRestrAperturaPlantilla As Boolean
Private m_bEnviarPedido As Boolean
Private m_bPedidoDirecto As Boolean

'Combos
Private m_bRespetarCombo As Boolean

'Plantillas
Private m_oPlantillaSeleccionada As CPlantilla
Private m_oPlantillas As CPlantillas
Private m_sPlantilla As String

'Array con las familias de material
Private m_arrFamMat() As Variant '0 GMN1, 1 GMN2, 2 GMN3, 3 GMN4, 4 DEN

'C�digo y equipo del comprador
Private m_sCodComp As String
Private m_sCodEqp As String
Private m_arrMatComp() As Variant

Private m_sIdiErrorEvaluacion(2) As String

'edu gs98
Public g_oGMN4Seleccionado As CGrupoMatNivel4

Private m_bPermProcMultimaterial As Boolean  'Permitir abrir procesos multimaterial
Private m_sProceso As String
Private m_sGrupo As String
Private m_sItem As String

Private m_oEmpresas As CEmpresas

Public b_ErrorPosicionarTabPedidos As Boolean

'3114
Public g_bGenerarPedido As Boolean
Public g_bPedidoExistente As Boolean
Private m_bRestrMatComp As Boolean

'3275
'Colecciones con los atributos de especificacion de la plantilla de Proceso, Grupo e Item
'Private m_oAtribEspePlantP As CAtributos
Private m_oAtribEspePlantG As CAtributos
Private m_oAtribEspePlantI As CAtributos
Private m_oAtribEspeSinValorI As CAtributo
Private m_oAtribEspeSinValorG As CAtributo
Private m_oAtributoI As CAtributo
Private m_oAtributoG As CAtributo
Private bGuardar As Boolean
Private iorigen As Integer
Private bUpdate As Boolean
Private iColumna As Integer
Private iLineaLineas As Variant
Private iLineaAtributos As Variant

'3279
Public m_iGenerar As Integer

'3372
Private oGruposSolicit As CGrupos
Private m_bPrimVez As Boolean
Private m_bCambio As Boolean
Public b_Bloqueo As Boolean

'3416
Private NombrePlantilla1 As String
Private IdPlantilla1 As Variant

'Control de Errores
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String

'Constante para indicar el n�mero de columnas fijas de la Grid
'*********************ACTUALIZAR ESTE VALOR SIEMPRE AUQ SE METAN COLUMNAS FIJAS*******************
Const iNumCol As Integer = 27
''' <summary>
''' Comprueba segun el ambito de la distribuci�n si faltan distribuciones de grupo o �tem y las pide
''' las de proceso se piden en la siguiente ventana
''' </summary>
''' <returns>True si se han completado las distribuciones, False sino</returns>
''' <remarks>Llamada desde: cmdContinuar_click; Tiempo m�ximo:0</remarks>
Private Function PedirDistribQueFaltan() As Boolean
Dim oGrupo As CGrupo

Dim oItem As CItem
Dim b As Boolean
Dim oUON As IUon

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
''----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
PedirDistribQueFaltan = False

Select Case m_oProceso.DefDistribUON
Case EnGrupo
    For Each oGrupo In m_oProceso.Grupos 'Si no hay dist la pido
        b = False
        If oGrupo.DistsNivel1 Is Nothing And oGrupo.DistsNivel2 Is Nothing And oGrupo.DistsNivel3 Is Nothing Then
            b = SeleccionarDistribucion(oUON, 0, oGrupo.Codigo)
            If Not b Then
                'Si no ponen distribucion no se puede seguir
                Exit Function
            Else
                If oUON.CodUnidadOrgNivel3 <> "" Then
                    Set oGrupo.DistsNivel1 = Nothing
                    Set oGrupo.DistsNivel2 = Nothing
                    Set oGrupo.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                    oGrupo.DistsNivel3.Add Nothing, oUON.CodUnidadOrgNivel1, oUON.CodUnidadOrgNivel2, oUON.CodUnidadOrgNivel3, 100, , 0
                ElseIf oUON.CodUnidadOrgNivel2 <> "" Then
                    Set oGrupo.DistsNivel1 = Nothing
                    Set oGrupo.DistsNivel2 = Nothing
                    Set oGrupo.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                    oGrupo.DistsNivel2.Add Nothing, oUON.CodUnidadOrgNivel1, oUON.CodUnidadOrgNivel2, 100, , 0
                ElseIf oUON.CodUnidadOrgNivel1 <> "" Then
                    Set oGrupo.DistsNivel3 = Nothing
                    Set oGrupo.DistsNivel2 = Nothing
                    Set oGrupo.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                    oGrupo.DistsNivel1.Add Nothing, oUON.CodUnidadOrgNivel1, 100, , 0
                Else
                    Exit Function
                End If
            End If
        End If
    Next
Case EnItem
    For Each oGrupo In m_oProceso.Grupos 'Si no hay dist la pido
        If Not oGrupo.Items Is Nothing Then
        For Each oItem In oGrupo.Items
            b = False
            If NoHayParametro(oItem.UON1Cod) And NoHayParametro(oItem.UON2Cod) And NoHayParametro(oItem.UON3Cod) Then
                b = SeleccionarDistribucion(oUON, oItem.Id, oGrupo.Codigo)
                If Not b Then
                    'Si no ponen distribucion no se puede seguir
                    Exit Function

                Else
                    oItem.UON1Cod = oUON.CodUnidadOrgNivel1
                    oItem.UON2Cod = oUON.CodUnidadOrgNivel2
                    oItem.UON3Cod = oUON.CodUnidadOrgNivel3
                End If

            End If
        Next
        End If
    Next
End Select

PedirDistribQueFaltan = True


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirProc", "PedirDistribQueFaltan", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Function
    End If
End Function


Public Property Get oProc() As cProceso
    Set oProc = m_oProceso
End Property

Public Property Set oProc(ByVal Value As cProceso)
    Set m_oProceso = Value
End Property

''' <summary>
''' Muestra una ventana para seleccionar distribucin para un item o un grupo
''' </summary>
''' <returns>True si se ha cogido una distr valida, False sino</returns>
''' <remarks>Llamada desde: PedirDistQueFaltan; Tiempo m�ximo:0</remarks>
Private Function SeleccionarDistribucion(ByRef oUON As IUon, ByVal lItem As Long, ByVal sGrupo As String) As Boolean
Dim uons3 As CUnidadesOrgNivel3
Dim uons2 As CUnidadesOrgNivel2
Dim uons1 As CUnidadesOrgNivel1
Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
SeleccionarDistribucion = False


Set uons1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
Set uons2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
Set uons3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3

If lItem <> 0 Then
    If Not NoHayParametro(m_oProceso.Grupos.Item(sGrupo).Items.Item(CStr(lItem)).ArticuloCod) Then
        uons1.cargarTodasLasUnidadesDeDistribucionART4UON m_oProceso.Grupos.Item(sGrupo).Items.Item(CStr(lItem)).ArticuloCod
        uons2.cargarTodasLasUnidadesDeDistribucionART4UON m_oProceso.Grupos.Item(sGrupo).Items.Item(CStr(lItem)).ArticuloCod
        uons3.cargarTodasLasUnidadesDeDistribucionART4UON m_oProceso.Grupos.Item(sGrupo).Items.Item(CStr(lItem)).ArticuloCod
    End If
End If

Set oUON = Nothing

If lItem <> 0 Then
    frmSELUO.cargarArbol uons1, uons2, uons3
    frmSELUO.caption = frmSELUO.caption & " " & sdbgLineas.Columns("NUM").caption & ": " & CStr(lItem)
Else
    frmSELUO.caption = frmSELUO.caption & " " & sIdiGrupo & ": " & CStr(sGrupo)
End If
frmSELUO.Show vbModal

If frmSELUO.Aceptar Then
    Set oUON = frmSELUO.UonSeleccionada
    If lItem = 0 Then 'Tengo que comprobar que la distrib es valida para todos los �tems del grupo
        If Not m_oProceso.Grupos.Item(sGrupo).Items Is Nothing Then
            For Each oItem In m_oProceso.Grupos.Item(sGrupo).Items
                If ("" & oItem.ArticuloCod) <> "" Then 'Artic NO codificados-> todos uons. Cumple.
                    If Not oItem.Articulo.uons.EsDistribuibleUON(oUON) Then
                        oMensajes.MensajeOKOnly 1467, Critical
                        Exit Function
                    End If
                End If
            Next
        End If
    End If
    
    SeleccionarDistribucion = True
End If

Unload frmSELUO
Set frmSELUO = Nothing

Set uons1 = Nothing
Set uons2 = Nothing
Set uons3 = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirProc", "SeleccionarDistribucion", err, Erl, , m_bActivado, m_sMsgError)
        Exit Function
    End If
End Function

Private Sub cmdBuscarMat_Click()

    ' edu
    ' gs98
    ' seleccionar materiales en funcion de la plantilla
    ' edu pm98
    SeleccionarMaterial Me.Name, m_oProceso, m_bRestrMat, , m_oPlantillaSeleccionada

    
    'frmSELMAT.sOrigen = "frmSOLAbrirProc"
    'frmSELMAT.bRComprador = m_bRestrMat
    'frmSELMAT.Show vbModal
    
End Sub


Private Sub cmdCancelar_Click()
    Unload Me
End Sub



''' <summary>
''' Responde al evento de click en el bot�n de continuar. Hace comprobaciones para ver si el material est�
''' bien establecido y dependiendo de que se quiera crear llama a una u otra funci�n de creaci�n.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub cmdContinuar_Click()
    Dim oItem As CItem
    Dim oGrupo As CGrupo
    Dim i As Long
    Dim oAtribsEspeSinValorI As CAtributos
    Dim oAtributoEsp As CAtributo
    Dim sMensaje As String
    Dim bPrimero As Boolean
    Dim bPaginaSeleccionada As Boolean
    Dim bEncontrado As Boolean
    Dim iFilaGrupo As Variant
    Dim iFilaItem As Variant
    Dim iPagina As Integer
    Dim iFilaGrupoError As Variant
    Dim iFilaItemError As Variant
    Dim k As Long
    Dim m As Long
    Dim j As Long
    Dim IndexAnyadir As Long
    Dim b_CambiarGrupo As Boolean
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If sdbgLineas.DataChanged Then sdbgLineas.Update
    If Not bGuardar Then Exit Sub
    
    Set oAtribsEspeSinValorI = oFSGSRaiz.Generar_CAtributos
    
    'Guardado de la configuraci�n inicial
    If Not m_oProcesoPlant.Grupos Is Nothing Then
        For i = 1 To m_oProcesoPlant.Grupos.Count
            m_oProceso.Grupos.Item(i).Codigo = m_oProcesoPlant.Grupos.Item(i).Codigo
            m_oProceso.Grupos.Item(i).Den = m_oProcesoPlant.Grupos.Item(i).Den
        Next i
    End If
      
    If Trim(txtProceso.Text) = "" Then
        oMensajes.IntroduzcaDenProceso
        Exit Sub
    End If
    If Trim(sdbcMat.Text) = "" Then
        oMensajes.SeleccioneMatNivel4
        Exit Sub
    Else
        If Trim(sdbcMat.Columns("GMN4").Value) = "" Or IsNull(sdbcMat.Columns("GMN4").Value) Then
            oMensajes.SeleccioneMatNivel4
            Exit Sub
        End If
    End If
    
    'Con la acci�n de seguridad "Restringir la apertura de procesos �nicamente desde plantillas"
    'hay que asegurarse de que el desplegable de plantillas no est� vacio
    If (Trim(sdbcPlantilla.Value) = "..." Or Trim(sdbcPlantilla.Text) = "") And m_bRestrAperturaPlantilla Then
        oMensajes.SeleccionarPlantilla
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    bGuardar = True
    'Comprueba si quedan datos del grid sin actualizar
    iorigen = 1
    ActualizarAtribEspeItemPlantilla
    If Not bGuardar Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    'Comprueba si quedan datos del grid sin actualizar
    iorigen = 2
    ActualizarAtribEspeGrupoPlantilla
    If Not bGuardar Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    'Comprobar las fecha de inicio y fin de suministro. Si se haindicado alguna en una l�nea hay que indicarla en todas
    Dim bHayFecIniSum As Boolean
    Dim bHayFecFinSum As Boolean
    Dim bIni As Boolean
    'Esta comprobaci�n solo se debe hacer en el caso de que se informen las fechas en la grid
    If sdbgLineas.Columns("FEC_INI").Visible And sdbgLineas.Columns("FEC_FIN").Visible Then
        For Each oGrupo In m_oProceso.Grupos
            bIni = True
            If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    If Not oItem.eliminado Then
                        If bIni Then
                            bHayFecIniSum = (oItem.FechaInicioSuministro > 0)
                            bHayFecFinSum = (oItem.FechaFinSuministro > 0)
                            bIni = False
                        Else
                            If (bHayFecIniSum And oItem.FechaInicioSuministro = 0) Or (Not bHayFecIniSum And oItem.FechaInicioSuministro > 0) Then
                                oMensajes.FaltanDatos sdbgLineas.Columns("FEC_INI").caption
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                            If (bHayFecFinSum And oItem.FechaFinSuministro = 0) Or (Not bHayFecFinSum And oItem.FechaFinSuministro > 0) Then
                                oMensajes.FaltanDatos sdbgLineas.Columns("FEC_FIN").caption
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                            If bHayFecIniSum And oItem.FechaFinSuministro = 0 Then
                                oMensajes.FaltanDatos sdbgLineas.Columns("FEC_FIN").caption
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                            If bHayFecFinSum And oItem.FechaInicioSuministro = 0 Then
                                oMensajes.FaltanDatos sdbgLineas.Columns("FEC_INI").caption
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Next
     End If
     'Traslado de los items de un grupo a otro (despues de haber hecho las comprobaciones anteriores)
InicioPorCambio:
    For k = 1 To m_oProceso.Grupos.Count
        b_CambiarGrupo = False
        If Not m_oProceso.Grupos.Item(k).Items Is Nothing Then
            For m = 1 To m_oProceso.Grupos.Item(k).Items.Count
                With m_oProceso.Grupos.Item(k).Items.Item(m)
                    If .grupoCod <> m_oProceso.Grupos.Item(k).Codigo Then
                        b_CambiarGrupo = True
                        
                        For j = 1 To m_oProceso.Grupos.Count
                            If .grupoCod = m_oProceso.Grupos.Item(j).Codigo Then
                                IndexAnyadir = j
                                Exit For
                            End If
                        Next j
                        
                        'Grupo nuevo
                        If j > m_oProceso.Grupos.Count Then
                            'A�ado el grupo nuevo
                            m_oProceso.Grupos.Add m_oProceso, .grupoCod, .GrupoDen, lID:=m_oProceso.Grupos.Item(k).Id
                            'Items
                            Set m_oProceso.Grupos.Item(j).Items = oFSGSRaiz.Generar_CItems
                            m_oProceso.Grupos.Item(j).Items.Add m_oProceso, .Id, .DestCod, .UniCod, .PagCod, .Cantidad, .FechaInicioSuministro, .FechaFinSuministro, .ArticuloCod, .Descr, _
                                .Precio, .Presupuesto, , , , , , , , , , .grupoCod, , .SolicitudId, , , , NullToStr(.GMN1Cod), NullToStr(.GMN2Cod), NullToStr(.GMN3Cod), NullToStr(.GMN4Cod), _
                                 , , .CopiaCampoArt, , , .IdLineaSolicit, .IdCampoSolicit
                            m_oProceso.Grupos.Item(k).Items.Remove (CStr(.Id))
                            GoTo InicioPorCambio
                        Else
                        
                            If b_CambiarGrupo Then
                                If m_oProceso.Grupos.Item(j).Items Is Nothing Then
                                    Set m_oProceso.Grupos.Item(j).Items = oFSGSRaiz.Generar_CItems
                                End If
                                m_oProceso.Grupos.Item(j).Items.Add m_oProceso, .Id, .DestCod, .UniCod, .PagCod, .Cantidad, .FechaInicioSuministro, .FechaFinSuministro, .ArticuloCod, .Descr, _
                                    .Precio, .Presupuesto, , , , , , , , , , .grupoCod, , .SolicitudId, , , , NullToStr(.GMN1Cod), NullToStr(.GMN2Cod), NullToStr(.GMN3Cod), NullToStr(.GMN4Cod), _
                                    , , .CopiaCampoArt, , , .IdLineaSolicit, .IdCampoSolicit
                                m_oProceso.Grupos.Item(k).Items.Remove (CStr(.Id))
                                GoTo InicioPorCambio
                            End If
                        End If
                    End If
                End With
            Next m
        End If
    Next k


    b_Bloqueo = True
    
    
    
    '3275
    'Recorro todos los atributos de especificacion de plantilla
    'a nivel de ITEM  y GRUPO a�adidos al proceso para ver si estan informados
    sMensaje = ""
    bPrimero = True
    bPaginaSeleccionada = False
    Set oAtributoEsp = Nothing
    Set oAtributoEsp = oFSGSRaiz.Generar_CAtributo
    Set m_oAtribEspeSinValorI = Nothing
    Set m_oAtribEspeSinValorG = Nothing
    iPagina = 0
    iFilaGrupo = 0
    iFilaItem = 0
        
    'Si el proceso tiene al menos un grupo...y estamos en generar ofertas, generar adjudicaciones y emitir pedido directo se comprueban los atributos de plantilla obligatorios
    
    If Not m_oProceso.Grupos Is Nothing And Not optGenerar.Item(0).Value Then
        ' Por cada grupo del proceso....
        For Each oGrupo In m_oProceso.Grupos
            iFilaGrupo = 0
            If bPaginaSeleccionada = False Then
                iPagina = iPagina + 1
            End If
            'Si el grupo tiene items...
            If Not oGrupo.Items Is Nothing Then
                'Recorro todos los items
                iFilaItem = 0
                For Each oItem In oGrupo.Items
                    If oItem.eliminado Then
                    Else
                        'Si el ITEM tiene atributos de especificacion, miro si alguno es de plantilla y valido si tiene o no valor
                        If Not oItem.AtributosEspecificacion Is Nothing And Not m_oAtribEspePlantI Is Nothing Then
                            For Each oAtributoEsp In oItem.AtributosEspecificacion
                                'Si el atributo de especificacion en curso, es de la plantilla, se comprueba si esta informado
                                If Not m_oAtribEspePlantI.Item(oAtributoEsp.Atrib) Is Nothing Then
                                    Select Case oAtributoEsp.Tipo
                                        Case TiposDeAtributos.TipoString
                                            If IsNull(oAtributoEsp.valorText) Or oAtributoEsp.valorText = "" Then
                                                'marcar como sin informar
                                                
                                                If bPrimero Then
                                                    Set m_oAtribEspeSinValorI = oFSGSRaiz.Generar_CAtributo
                                                    Set m_oAtribEspeSinValorI = oAtributoEsp
                                                    bPrimero = False
                                                    iFilaItemError = iFilaItem
                                                    bPaginaSeleccionada = True
                                                End If
                                                oAtribsEspeSinValorI.addAtributo oAtributoEsp, oAtributoEsp.Atrib
                                            End If
                                        Case TiposDeAtributos.TipoNumerico
                                            If IsNull(oAtributoEsp.valorNum) Or oAtributoEsp.valorNum = "" Then
                                                'marcar como sin informar
                                                 If bPrimero Then
    
                                                    Set m_oAtribEspeSinValorI = oFSGSRaiz.Generar_CAtributo
                                                    Set m_oAtribEspeSinValorI = oAtributoEsp
                                                    bPrimero = False
                                                    iFilaItemError = iFilaItem
                                                    bPaginaSeleccionada = True
                                                End If
                                                oAtribsEspeSinValorI.addAtributo oAtributoEsp, oAtributoEsp.Atrib
                                            End If
                                        Case TiposDeAtributos.TipoFecha
                                            If IsNull(oAtributoEsp.valorFec) Or oAtributoEsp.valorFec = "" Then
                                                'marcar como sin informar
    
                                                If bPrimero Then
                                                    Set m_oAtribEspeSinValorI = oFSGSRaiz.Generar_CAtributo
                                                    Set m_oAtribEspeSinValorI = oAtributoEsp
                                                    bPrimero = False
                                                    iFilaItemError = iFilaItem
                                                    bPaginaSeleccionada = True
                                                End If
                                                oAtribsEspeSinValorI.addAtributo oAtributoEsp, oAtributoEsp.Atrib
                                            End If
                                        Case TiposDeAtributos.TipoBoolean
                                            If IsNull(oAtributoEsp.valorBool) Or oAtributoEsp.valorBool = "" Then
                                                'marcar como sin informar
    
                                                If bPrimero Then
                                                    Set m_oAtribEspeSinValorI = oFSGSRaiz.Generar_CAtributo
                                                    Set m_oAtribEspeSinValorI = oAtributoEsp
                                                    bPrimero = False
                                                    iFilaItemError = iFilaItem
                                                    bPaginaSeleccionada = True
                                                End If
                                                oAtribsEspeSinValorI.addAtributo oAtributoEsp, oAtributoEsp.Atrib
                                            End If
                                    End Select
                                End If
                            Next
                        End If
                    End If
                    iFilaItem = iFilaItem + 1
                Next
            End If
            'Si el GRUPO tiene atributos de especificacion, miro si alguno es de plantilla y valido si tiene o no valor
            If Not oGrupo.AtributosEspecificacion Is Nothing And Not m_oAtribEspePlantG Is Nothing Then
                For Each oAtributoEsp In oGrupo.AtributosEspecificacion
                    'Si el atributo de especificacion en curso, es de la plantilla, se comprueba si esta informado
                    If Not m_oAtribEspePlantG.Item(oAtributoEsp.Atrib) Is Nothing Then
                        Select Case oAtributoEsp.Tipo
                            Case TiposDeAtributos.TipoString
                                If IsNull(oAtributoEsp.valorText) Or oAtributoEsp.valorText = "" Then
                                    'marcar como sin informar
                                    sMensaje = sMensaje & " " & oAtributoEsp.Cod & "-" & oAtributoEsp.CodDen & " (" & sIdiGrupo & ")" & vbCrLf
                                    If bPrimero Then
                                        Set m_oAtribEspeSinValorG = oFSGSRaiz.Generar_CAtributo
                                        Set m_oAtribEspeSinValorG = oAtributoEsp
                                        bPrimero = False
                                        iFilaGrupoError = iFilaGrupo
                                        bPaginaSeleccionada = True
                                    End If
                                End If
                            Case TiposDeAtributos.TipoNumerico
                                If IsNull(oAtributoEsp.valorNum) Or oAtributoEsp.valorNum = "" Then
                                    'marcar como sin informar
                                    sMensaje = sMensaje & " " & oAtributoEsp.Cod & "-" & oAtributoEsp.CodDen & " (" & sIdiGrupo & ")" & vbCrLf
                                    If bPrimero Then
                                        Set m_oAtribEspeSinValorG = oFSGSRaiz.Generar_CAtributo
                                        Set m_oAtribEspeSinValorG = oAtributoEsp
                                        bPrimero = False
                                        iFilaGrupoError = iFilaGrupo
                                        bPaginaSeleccionada = True
                                    End If
                                End If
                            Case TiposDeAtributos.TipoFecha
                                If IsNull(oAtributoEsp.valorFec) Or oAtributoEsp.valorFec = "" Then
                                    'marcar como sin informar
                                    sMensaje = sMensaje & " " & oAtributoEsp.Cod & "-" & oAtributoEsp.CodDen & " (" & sIdiGrupo & ")" & vbCrLf
                                    If bPrimero Then
                                        Set m_oAtribEspeSinValorG = oFSGSRaiz.Generar_CAtributo
                                        Set m_oAtribEspeSinValorG = oAtributoEsp
                                        bPrimero = False
                                        iFilaGrupoError = iFilaGrupo
                                        bPaginaSeleccionada = True
                                    End If
                                End If
                            Case TiposDeAtributos.TipoBoolean
                                If IsNull(oAtributoEsp.valorBool) Or oAtributoEsp.valorBool = "" Then
                                    'marcar como sin informar
                                    sMensaje = sMensaje & " " & oAtributoEsp.Cod & "-" & oAtributoEsp.CodDen & " (" & sIdiGrupo & ")" & vbCrLf
                                    If bPrimero Then
                                        Set m_oAtribEspeSinValorG = oFSGSRaiz.Generar_CAtributo
                                        Set m_oAtribEspeSinValorG = oAtributoEsp
                                        bPrimero = False
                                        iFilaGrupoError = iFilaGrupo
                                        bPaginaSeleccionada = True
                                    End If
                                End If
                        End Select
                        iFilaGrupo = iFilaGrupo + 1
                    End If
                Next
            End If
        Next
    End If
    
    'Si hay algun atributo sin informar....
    If oAtribsEspeSinValorI.Count > 0 Then
        Screen.MousePointer = vbNormal
        Dim oatrib As CAtributo
        For Each oatrib In oAtribsEspeSinValorI
            sMensaje = sMensaje & " " & oatrib.Cod & "-" & oatrib.CodDen & " (" & sIdiItem & ")" & vbCrLf
        Next
        oMensajes.AtributosEspObl sMensaje, 0
        sdbgLineas.CancelUpdate
        sdbgLineas.DataChanged = False
        iLineaLineas = iFilaItemError
        ssTabGrupos.selectedItem = ssTabGrupos.Tabs(iPagina)
        bUpdate = False
        sdbgLineas.Bookmark = iFilaItemError
        PosicionarCeldaGridLineasItem m_oAtribEspeSinValorI
        bGuardar = False
        Exit Sub
    End If
    
    bEncontrado = False
    If Not m_oAtribEspeSinValorG Is Nothing Then
        Screen.MousePointer = vbNormal
        oMensajes.AtributosEspObl sMensaje, 0
        If Me.Visible Then sdbgAtributos.SetFocus
        sdbgAtributos.CancelUpdate
        sdbgAtributos.DataChanged = False
        ssTabGrupos.selectedItem = ssTabGrupos.Tabs(iPagina)
        bUpdate = False
        iLineaAtributos = iFilaGrupoError
        sdbgAtributos_PositionList (m_oAtribEspeSinValorG.Cod)
        bGuardar = False
        Exit Sub
    End If

    '''''
    Dim PasaCtrlArtConUon As Boolean
    PasaCtrlArtConUon = False
    
    Dim ListaCtrlArtSinUon As String
    Dim oArticuloItem As CArticulo
        
    For Each oGrupo In m_oProceso.Grupos
        If Not oGrupo.Items Is Nothing Then
            For Each oItem In oGrupo.Items
                If Not oItem.eliminado Then
                    If Trim(oItem.GMN4Cod) = "" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.SeleccioneMatNivel4
                        Exit Sub
                    End If
                    If sdbgLineas.Columns("PROV").Visible And (optGenerar.Item(3).Value Or optGenerar.Item(4).Value) Then   'Generar Pedido y viene en desglose el proveedor
                        If NullToStr(oItem.ProveAct) = "" Then
                            Screen.MousePointer = vbNormal
                            oMensajes.FaltanDatos 15
                            Exit Sub
                        End If
                    End If
                    
                    Set oArticuloItem = oFSGSRaiz.Generar_CArticulo
                    oArticuloItem.Cod = oItem.ArticuloCod
                    
                    If oArticuloItem.uons.Count > 0 Then
                        PasaCtrlArtConUon = True
                        oItem.UONsValidas = CStr(oArticuloItem.uons.Count)
                    Else
                        ListaCtrlArtSinUon = ListaCtrlArtSinUon & oItem.ArticuloCod & " - " & oItem.Descr & vbCrLf
                        'Aun no pongo oItem.eliminado = True por si responden a la pregunta oMensajes.PreguntaAlgunosArtSinUon(ListaCtrlArtSinUon) q no pero a continuaci�n lanza otra vez la creaci�n proceso.
                    End If
                End If
            Next
        End If
    Next
    
    If Not PasaCtrlArtConUon Then
        Screen.MousePointer = vbNormal
        oMensajes.MensajeTodosArtSinUon
        Exit Sub
    ElseIf ListaCtrlArtSinUon <> "" Then
        Screen.MousePointer = vbNormal
        If oMensajes.PreguntaAlgunosArtSinUon(ListaCtrlArtSinUon) = vbNo Then
            Exit Sub
        Else
            Screen.MousePointer = vbHourglass
            For Each oGrupo In m_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If oItem.UONsValidas = "" Then
                            oItem.eliminado = True
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    EstablecerDistPeticionario
    
    m_oProceso.PedidoDirecto = (optGenerar.Item(3).Value Or optGenerar.Item(4).Value)
            
    If Not PedirDistribQueFaltan Then Exit Sub
    
    '3275
    If optGenerar.Item(0).Value Then
        frmSOLAbrirFaltan.g_bMostrarAtribEspePlatillaProceso = False
    Else
        frmSOLAbrirFaltan.g_bMostrarAtribEspePlatillaProceso = True
        Set frmSOLAbrirFaltan.g_oPlantillaSeleccionada = m_oPlantillaSeleccionada
    End If
    
    If optGenerar.Item(0).Value = True Then
        AbrirProceso
        
    ElseIf optGenerar.Item(1).Value = True Then
        GenerarOfertas
        
    ElseIf optGenerar.Item(2).Value = True Then
        GenerarAdjudicaciones
        
    ElseIf optGenerar.Item(3).Value Or optGenerar.Item(4).Value Then
        'Si el estado de la Instancia es distinta que aprobada, miramos si su solicitud permite los pedidos directos:
        If m_oSolicitudSeleccionada.Estado < EstadoSolicitud.Pendiente Then
            m_oSolicitudSeleccionada.Solicitud.CargarConfiguracion , , , , True
            If Not m_oSolicitudSeleccionada.Solicitud.PermitirPedidoDirecto Then
                Screen.MousePointer = vbNormal
                oMensajes.NoPermitidoPedido
                Exit Sub
            End If
        End If

        If AdjudicarYCerrarProceso Then
            If optGenerar.Item(3).Value Then
                GenerarPedidosDirectos
            Else
                EnviarAPedido
            End If
        End If
    End If
    
    Screen.MousePointer = vbNormal
    
    Set oAtribsEspeSinValorI = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "cmdContinuar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub cmdLimpiarMat_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcMat.Text = "" Then Exit Sub
    
    sdbcMat.Text = ""
    
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "cmdLimpiarMat_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub


Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Load()
    Dim i As Long
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    m_bActivado = False
    
    frmADJCargar.Show
    frmADJCargar.Refresh
    
    m_bPrimVez = True
    b_Bloqueo = False
    m_bCambio = False
    
    ConfigurarSeguridad
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarFormulario
        
    'Genera los datos para el proceso
    CargarDatosProceso
    Set oGruposSolicit = m_oProceso.Grupos
    
    'Generar lista de plantillas
    Set m_oPlantillaSeleccionada = Nothing
    
    Unload frmADJCargar
    
    Me.caption = m_sIdiCaption
    If UBound(g_arSolicitudes) = 0 Then Me.caption = Me.caption & " " & m_oSolicitudSeleccionada.Id & " " & m_oSolicitudSeleccionada.DescrBreve
        
    'Cargo las plantillas en el combo en funcion de la opcion seleccionada
    'Ya que para pedidos directos (m_iGenerar = 3) las plantillas son propias
    bMostrarAtrEsp = False
    
    CargarPlantillas m_iGenerar
    
    'Columna 'Grupo destino proceso' visible con el permiso en 'Apertura de Procesos':
    '"Permitir modificar la estructura de un proceso creado desde plantilla"
    If m_bModifProcPlantilla Then
        sdbgLineas.Columns("GR_DESTINO_PROC").Visible = True
    Else
        sdbgLineas.Columns("GR_DESTINO_PROC").Visible = False
    End If
    
    'Mantener el valor de las pesta�as del proceso
    '(Asi m_oProcesoPlant ya definido aunque luego el usuario no decida elegir plantilla)
    Set m_oProcesoPlant = Nothing
    Set m_oProcesoPlant = oFSGSRaiz.Generar_CProceso
    Set m_oProcesoPlant.Grupos = oFSGSRaiz.Generar_CGrupos
        
    For i = 1 To m_oProceso.Grupos.Count
        m_oProcesoPlant.Grupos.Add m_oProceso, m_oProceso.Grupos.Item(i).Codigo, m_oProceso.Grupos.Item(i).Den
    Next i
    
        'Pongo en el optbox el valor que ha pasado el form llamante
    m_bRespetarCombo = True
    optGenerar.Item(m_iGenerar).Value = True
    m_bRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>Configura los controles del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub ConfigurarFormulario()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bPermProcMultimaterial Then
        lblMat.Visible = False
        sdbcMat.Visible = False
        cmdLimpiarMat.Visible = False
        cmdBuscarMat.Visible = False
        lblProceso.Top = 450
        txtProceso.Top = 450
        txtProceso.Left = 2000
        txtProceso.Width = 8000
    Else
        sdbgLineas.Columns("FAM_MAT").Visible = False
    End If
    
    optGenerar(4).Visible = m_bEnviarPedido
    optGenerar(3).Visible = m_bPedidoDirecto
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ConfigurarFormulario", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_ABRIR_PROC, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.lblProceso.caption = Ador(0).Value   '1 Nombre del proceso
        Ador.MoveNext
        Me.lblMat.caption = Ador(0).Value   '2 Seleccione el material del proceso
        Ador.MoveNext
        Me.optGenerar(0).caption = Ador(0).Value  '3 Generar proceso de compra
        Ador.MoveNext
        Me.optGenerar(1).caption = Ador(0).Value '4 Generar ofertas
        Ador.MoveNext
        Me.optGenerar(2).caption = Ador(0).Value  '5 Generar adjudicaciones
        Ador.MoveNext
        Me.optGenerar(3).caption = Ador(0).Value  '6 Generar pedidos directos
        Ador.MoveNext
        'Me.chkAtrib.caption = ador(0).Value  '7 Traspasar los campos de la solicitud como atributos del proceso de compra
        Ador.MoveNext
        Me.chkAdjuntos.caption = Ador(0).Value  '8 Traspasar los archivos adjuntos al proceso
        Ador.MoveNext
        Me.cmdContinuar.caption = Ador(0).Value  '9 Con&tinuar
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).Value  '10 &Cancelar
        Ador.MoveNext
        m_sIdiCaption = Ador(0).Value  '11 Solicitud de compra. Proceso/Pedido.
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value '12 S�
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value '13 No
        Ador.MoveNext
        sdbgLineas.Columns("SEL").caption = Ador(0).Value  '14 Sel.
        Ador.MoveNext
        sdbgLineas.Columns("COD_ART").caption = Ador(0).Value  '15 Art�culo
        Ador.MoveNext
        sdbgLineas.Columns("DESCR_ART").caption = Ador(0).Value  '16 Descripci�n
        Ador.MoveNext
        sdbgLineas.Columns("FEC_INI").caption = Ador(0).Value  '17 Inicio suministro
        Ador.MoveNext
        sdbgLineas.Columns("FEC_FIN").caption = Ador(0).Value  '18 Fin suministro
        Ador.MoveNext
        sdbgLineas.Columns("CANT").caption = Ador(0).Value  '19 Cantidad
        Ador.MoveNext
        sdbgLineas.Columns("PRECUNI").caption = Ador(0).Value  '20 Precio unitario
        Ador.MoveNext
        sdbgLineas.Columns("UNIDAD").caption = Ador(0).Value  '21 Unidad
        Ador.MoveNext
        sdbgLineas.Columns("IMPORTE").caption = Ador(0).Value  '22 Importe
        Ador.MoveNext
        sdbgLineas.Columns("DEST").caption = Ador(0).Value  '23 Destino
        Ador.MoveNext
        sdbgLineas.Columns("PAGO").caption = Ador(0).Value  '24 Forma de pago
        Ador.MoveNext
        sdbgLineas.Columns("PROV").caption = Ador(0).Value  '25 Proveedor
        m_sIdiProve = Ador(0).Value
        Ador.MoveNext
        sdbgLineas.Columns("ESP").caption = Ador(0).Value  '26 Especificaciones
        Ador.MoveNext
        lblPlantilla.caption = Ador(0).Value '27 Plantilla (Combo)
        Ador.MoveNext
        m_sPlantilla = Ador(0).Value  '28 Plantilla
        Ador.MoveNext
        sdbgLineas.Columns("FAM_MAT").caption = Ador(0).Value '29 Familia de material
        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '30 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '31 Error al realizar el c�lculo:Valores incorrectos.
        Ador.MoveNext
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        m_sGrupo = Ador(0).Value
        Ador.MoveNext
        m_sItem = Ador(0).Value
    
        '3275
        Ador.MoveNext
        sdbgAtributos.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sIdiMayor = Ador(0).Value
        Ador.MoveNext
        sIdiMenor = Ador(0).Value
        Ador.MoveNext
        sIdiEntre = Ador(0).Value
        Ador.MoveNext
        lblOtrosDatos.caption = Ador(0).Value
        Ador.MoveNext
        'borrado
        Ador.MoveNext
        sIdiItem = Ador(0).Value '43
        Ador.MoveNext
        sIdiGrupo = Ador(0).Value '44
        Ador.MoveNext
        sIdiProceso = Ador(0).Value '45
        Ador.MoveNext
        sdbgLineas.Columns("NUM").caption = Ador(0).Value
        Ador.MoveNext
        sdbgLineas.Columns("GR_DESTINO_PROC").caption = Ador(0).Value
        Ador.MoveNext
        Me.optGenerar(3).caption = Ador(0).Value        'Modulo=352, ID=48: Generar pedidos directos
        Ador.MoveNext
        lblPlantilla.caption = Ador(0).Value            'Modulo=352, ID=49: Plantilla para proceso:
        Ador.MoveNext
        optGenerar(4).caption = Ador(0).Value
        Ador.MoveNext
        m_sSolicitud = Ador(0).Value 'Solicitud
        sdbgLineas.Columns("SOLICITUD").caption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Public Sub PonerMatSeleccionado()
Dim oGMN1Seleccionado As CGrupoMatNivel1
Dim oGMN2Seleccionado As CGrupoMatNivel2
Dim oGMN3Seleccionado As CGrupoMatNivel3
Dim oGMN4Seleccionado As CGrupoMatNivel4

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
    sdbcMat.Text = ""
    
    If Not oGMN1Seleccionado Is Nothing Then
        m_sGMN1Cod = oGMN1Seleccionado.Cod
        sdbcMat.Columns("GMN1").Value = m_sGMN1Cod
        sdbcMat.Columns("DEN").Value = m_sGMN1Cod & " - " & oGMN1Seleccionado.Den
        sdbcMat.Text = m_sGMN1Cod & " - " & oGMN1Seleccionado.Den
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        m_sGMN2Cod = oGMN2Seleccionado.Cod
        sdbcMat.Columns("GMN1").Value = m_sGMN1Cod
        sdbcMat.Columns("GMN2").Value = m_sGMN2Cod
        sdbcMat.Columns("DEN").Value = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & oGMN2Seleccionado.Den
        sdbcMat.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & oGMN2Seleccionado.Den
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        m_sGMN3Cod = oGMN3Seleccionado.Cod
        sdbcMat.Columns("GMN1").Value = m_sGMN1Cod
        sdbcMat.Columns("GMN2").Value = m_sGMN2Cod
        sdbcMat.Columns("GMN3").Value = m_sGMN3Cod
        sdbcMat.Columns("DEN").Value = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & oGMN3Seleccionado.Den
        sdbcMat.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & oGMN3Seleccionado.Den
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        m_sGMN4Cod = oGMN4Seleccionado.Cod
        sdbcMat.Columns("GMN1").Value = m_sGMN1Cod
        sdbcMat.Columns("GMN2").Value = m_sGMN2Cod
        sdbcMat.Columns("GMN3").Value = m_sGMN3Cod
        sdbcMat.Columns("GMN4").Value = m_sGMN4Cod
        sdbcMat.Columns("DEN").Value = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & m_sGMN4Cod & " - " & oGMN4Seleccionado.Den
        sdbcMat.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & m_sGMN4Cod & " - " & oGMN4Seleccionado.Den
    End If
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    If Not m_bPermProcMultimaterial Then
        ComprobarArticuloDelMat
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "PonerMatSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub ConfigurarSeguridad()
    'Restricci�n de material:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing) Then
        m_bRestrMat = True
        m_sCodComp = oUsuarioSummit.comprador.Cod
        m_sCodEqp = oUsuarioSummit.comprador.codEqp
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEConsultar)) Is Nothing) Then
        m_bVerSelecProve = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEConsultar)) Is Nothing) Then
        m_bVerRecOfe = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjConsulta)) Is Nothing) Then
        m_bVerComparativa = True
    End If
    
    'Abrir procesos multimaterial
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
        If gParametrosGenerales.gbMultiMaterial Then
            m_bPermProcMultimaterial = True
        Else
            m_bPermProcMultimaterial = False
        End If
    Else
        m_bPermProcMultimaterial = False
    End If
    
    'Apertura de Procesos-> Permitir Modificar la estructura de un proceso creado desde plantilla
    'APEModifProcPlantilla = 12041
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModifProcPlantilla)) Is Nothing) Then
        m_bModifProcPlantilla = True
    Else
        m_bModifProcPlantilla = False
    End If
    
    'Restringir la apertura de procesos �nicamente desde plantilla
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrAperturaPlantillas)) Is Nothing) Then
        m_bRestrAperturaPlantilla = True
    Else
        m_bRestrAperturaPlantilla = False
    End If

    'Restriccion material usuario comprador
    If oUsuarioSummit.Tipo = TipoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing Then
        m_bRestrMatComp = True
    End If
    
    'Enviar a pedido existente o emitir
    m_bEnviarPedido = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICEnviarPedExistente)) Is Nothing)
    m_bPedidoDirecto = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICPedidoDirecto)) Is Nothing)

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub CargarDatosProceso()
    Dim oInstancia As CInstancia
    Dim Ador As Ador.Recordset
    Dim sDen As String
    Dim oGrupo As CGrupo
    Dim i As Integer
    Dim n As Integer
    Dim k As Integer
    Dim oComprador As CCompradores
    Dim bExiste As Boolean

    'Carga los datos de la solicitud:
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Set m_oSolicitudSeleccionada = g_arSolicitudes(0)

    
    'Nombre del proceso:
    If Not IsNull(m_oSolicitudSeleccionada.DenomProceso) Then
        txtProceso.Text = NullToStr(Mid(m_oSolicitudSeleccionada.DenomProceso, 1, 100))
    Else
        Dim oInstancias As CInstancias
        Set oInstancias = oFSGSRaiz.Generar_CInstancias
        oInstancias.BuscarSolicitudes OrdSolicPorId, , m_oSolicitudSeleccionada.Id
        Set m_oSolicitudSeleccionada = oInstancias.Item(1)
        Set oInstancias = Nothing
        txtProceso.Text = NullToStr(m_oSolicitudSeleccionada.DescrBreve)
    End If
    
    'Carga los grupos:
    m_oSolicitudSeleccionada.CargarGrupos
    
    'Si se trata de un comprador con restricci�n de material guardamos sus materiales en un array.
    Set oComprador = Nothing
    Set oComprador = oFSGSRaiz.generar_CCompradores
    If m_bRestrMat Then
        m_arrMatComp = oComprador.DevolverMaterialesDeUnComprador(m_sCodEqp, m_sCodComp)
    End If
    
    'carga las l�neas de desglose y genera la estructura del proceso:
    GenerarProcesoCompra
    
    'Carga la combo de materiales a nivel de campo:
    i = 1
    For k = 0 To UBound(g_arSolicitudes)
        Set oInstancia = g_arSolicitudes(k)
        Set Ador = oInstancia.DevolverMaterialesInstancia_Campo
                      
        While Not Ador.EOF
            'Comprobar si existe ya
            bExiste = False
            If Not IsEmpty(m_arrFamMat) Then
                For n = 0 To i - 2
                    If m_arrFamMat(0, n) = Ador.Fields("VAL_GMN1") And m_arrFamMat(1, n) = Ador.Fields("VAL_GMN2") And m_arrFamMat(2, n) = Ador.Fields("VAL_GMN3") And m_arrFamMat(3, n) = Ador.Fields("VAL_GMN4") Then
                        bExiste = True
                        Exit For
                    End If
                Next
            End If
            
            If Not bExiste Then
                sDen = Ador.Fields("VAL_GMN1")
                If Not IsNull(Ador.Fields("VAL_GMN2")) And Ador.Fields("VAL_GMN2") <> "" Then
                    sDen = sDen & "-" & Ador.Fields("VAL_GMN2")
                    If Not IsNull(Ador.Fields("VAL_GMN3")) And Ador.Fields("VAL_GMN3") <> "" Then
                        sDen = sDen & "-" & Ador.Fields("VAL_GMN3")
                        If Not IsNull(Ador.Fields("VAL_GMN4")) And Ador.Fields("VAL_GMN4") <> "" Then
                            sDen = sDen & "-" & Ador.Fields("VAL_GMN4")
                        End If
                    End If
                End If
                sDen = sDen & "-" & Ador.Fields("DEN")
                ReDim Preserve m_arrFamMat(4, i - 1)
                m_arrFamMat(0, i - 1) = Ador.Fields("VAL_GMN1")
                m_arrFamMat(1, i - 1) = Ador.Fields("VAL_GMN2")
                m_arrFamMat(2, i - 1) = Ador.Fields("VAL_GMN3")
                m_arrFamMat(3, i - 1) = Ador.Fields("VAL_GMN4")
                m_arrFamMat(4, i - 1) = Ador.Fields("DEN")
                
                sdbcMat.AddItem Ador.Fields("VAL_GMN1") & Chr(m_lSeparador) & Ador.Fields("VAL_GMN2") & Chr(m_lSeparador) & Ador.Fields("VAL_GMN3") & Chr(m_lSeparador) & Ador.Fields("VAL_GMN4") & Chr(m_lSeparador) & sDen
                If i = 1 Then  'Selecciona el primer material por orden ascendente:
                    sdbcMat.Columns("GMN1").Value = Ador.Fields("VAL_GMN1")
                    sdbcMat.Columns("GMN2").Value = Ador.Fields("VAL_GMN2")
                    sdbcMat.Columns("GMN3").Value = Ador.Fields("VAL_GMN3")
                    sdbcMat.Columns("GMN4").Value = Ador.Fields("VAL_GMN4")
                    m_sGMN1Cod = Trim(Ador.Fields("VAL_GMN1"))
                    m_sGMN2Cod = Trim(Ador.Fields("VAL_GMN2"))
                    m_sGMN3Cod = Trim(Ador.Fields("VAL_GMN3"))
                    m_sGMN4Cod = Trim(Ador.Fields("VAL_GMN4"))
                    sdbcMat.Text = sDen
                End If
                
                i = i + 1
            End If
                        
            Ador.MoveNext
        Wend
        Ador.Close
        Set Ador = Nothing
    Next
    
     'Carga la combo de materiales a nivel de Desglose:
    For k = 0 To UBound(g_arSolicitudes)
        Set oInstancia = g_arSolicitudes(k)
        Set Ador = oInstancia.DevolverMaterialesInstancia_Desglose
        While Not Ador.EOF
            'Comprobar si existe ya
            bExiste = False
            If Not IsEmpty(m_arrFamMat) Then
                For n = 0 To i - 2
                    If m_arrFamMat(0, n) = Ador.Fields("VAL_GMN1") And m_arrFamMat(1, n) = Ador.Fields("VAL_GMN2") And m_arrFamMat(2, n) = Ador.Fields("VAL_GMN3") And m_arrFamMat(3, n) = Ador.Fields("VAL_GMN4") Then
                        bExiste = True
                        Exit For
                    End If
                Next
            End If
            
            If Not bExiste Then
                sDen = Ador.Fields("VAL_GMN1")
                If Not IsNull(Ador.Fields("VAL_GMN2")) And Ador.Fields("VAL_GMN2") <> "" Then
                    sDen = sDen & "-" & Ador.Fields("VAL_GMN2")
                    If Not IsNull(Ador.Fields("VAL_GMN3")) And Ador.Fields("VAL_GMN3") <> "" Then
                        sDen = sDen & "-" & Ador.Fields("VAL_GMN3")
                        If Not IsNull(Ador.Fields("VAL_GMN4")) And Ador.Fields("VAL_GMN4") <> "" Then
                            sDen = sDen & "-" & Ador.Fields("VAL_GMN4")
                        End If
                    End If
                End If
                sDen = sDen & "-" & Ador.Fields("DEN")
                ReDim Preserve m_arrFamMat(4, i - 1)
                m_arrFamMat(0, i - 1) = Ador.Fields("VAL_GMN1")
                m_arrFamMat(1, i - 1) = Ador.Fields("VAL_GMN2")
                m_arrFamMat(2, i - 1) = Ador.Fields("VAL_GMN3")
                m_arrFamMat(3, i - 1) = Ador.Fields("VAL_GMN4")
                m_arrFamMat(4, i - 1) = Ador.Fields("DEN")
                
                sdbcMat.AddItem Ador.Fields("VAL_GMN1") & Chr(m_lSeparador) & Ador.Fields("VAL_GMN2") & Chr(m_lSeparador) & Ador.Fields("VAL_GMN3") & Chr(m_lSeparador) & Ador.Fields("VAL_GMN4") & Chr(m_lSeparador) & sDen
                If i = 1 Then  'Selecciona el primer material por orden ascendente:
                    sdbcMat.Columns("GMN1").Value = Ador.Fields("VAL_GMN1")
                    sdbcMat.Columns("GMN2").Value = Ador.Fields("VAL_GMN2")
                    sdbcMat.Columns("GMN3").Value = Ador.Fields("VAL_GMN3")
                    sdbcMat.Columns("GMN4").Value = Ador.Fields("VAL_GMN4")
                    m_sGMN1Cod = Trim(Ador.Fields("VAL_GMN1"))
                    m_sGMN2Cod = Trim(Ador.Fields("VAL_GMN2"))
                    m_sGMN3Cod = Trim(Ador.Fields("VAL_GMN3"))
                    m_sGMN4Cod = Trim(Ador.Fields("VAL_GMN4"))
                    sdbcMat.Text = sDen
                End If
                
                i = i + 1
            End If
                        
            Ador.MoveNext
        Wend
        Ador.Close
        Set Ador = Nothing
    Next
    
    'A�ade los grupos al tab y se posiciona en el primero:
    ssTabGrupos.Tabs.clear
    If Not m_oProceso.Grupos Is Nothing Then
        For Each oGrupo In m_oProceso.Grupos
            ssTabGrupos.Tabs.Add , "G" & oGrupo.Codigo, oGrupo.Codigo & "-" & oGrupo.Den
        Next
    End If
    If ssTabGrupos.Tabs.Count > 0 Then
        ssTabGrupos.Tabs(1).Selected = True
    End If
    
    'Comprueba que los art�culos pertenezcan al material seleccionado:
    If Not m_bPermProcMultimaterial Then
        ComprobarArticuloDelMat
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "CargarDatosProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub

''' <summary>procedimiento de ajuste del tama�o</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1 sec</remarks>
Private Sub Form_Resize()
    Dim i As Integer

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Width < 1500 Then Exit Sub
    If Me.Height < 3000 Then Exit Sub
    
    fraCabecera.Width = Me.Width - 330
    fraOpciones.Width = fraCabecera.Width
    ssTabGrupos.Width = fraCabecera.Width
    
    If m_bEnviarPedido Then
        fraOpciones.Height = 2040
        ssTabGrupos.Top = 3345
        Line1.Y2 = 1920
    Else
        fraOpciones.Height = 1800
        ssTabGrupos.Top = 3105
        Line1.Y2 = 1680
    End If
    
    ssTabGrupos.Height = Me.Height - (fraCabecera.Height + fraOpciones.Height + picNavigate.Height + 900)
    sdbgLineas.Height = ((ssTabGrupos.Height - 720) / 3) * 2
    sdbgLineas.Width = ssTabGrupos.Width - 355
    sdbgLineas.Top = ssTabGrupos.Top + 625
    
    '3275
    picAtrbEsp.Height = ssTabGrupos.Height - 720 - sdbgLineas.Height
    picAtrbEsp.Width = ssTabGrupos.Width - 355
    picAtrbEsp.Top = sdbgLineas.Top + sdbgLineas.Height
    lblOtrosDatos.Top = 30
    sdbgAtributos.Height = ssTabGrupos.Height - 720 - sdbgLineas.Height - lblOtrosDatos.Height
    sdbgAtributos.Width = ssTabGrupos.Width - 355
    sdbgAtributos.Top = lblOtrosDatos.Height
    If optGenerar.Item(0).Value = False And bMostrarAtrEsp = True Then
        picAtrbEsp.Visible = True
        sdbgLineas.Height = ((ssTabGrupos.Height - 720) / 3) * 2

    Else
        picAtrbEsp.Visible = False
        sdbgLineas.Height = ssTabGrupos.Height - 720
    End If
    
    picNavigate.Top = ssTabGrupos.Top + ssTabGrupos.Height + 120
    picNavigate.Left = Me.Width / 3
    
    sdbgLineas.Columns("COD_ART").Width = Me.sdbgLineas.Width * 0.1
    sdbgLineas.Columns("DESCR_ART").Width = Me.sdbgLineas.Width * 0.25
    sdbgLineas.Columns("FEC_INI").Width = Me.sdbgLineas.Width * 0.1
    sdbgLineas.Columns("FEC_FIN").Width = Me.sdbgLineas.Width * 0.1
    sdbgLineas.Columns("CANT").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("PRECUNI").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("IMPORTE").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("UNIDAD").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("DEST").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("PAGO").Width = Me.sdbgLineas.Width * 0.12
    sdbgLineas.Columns("PROV").Width = Me.sdbgLineas.Width * 0.12
    
    '3275
    sdbgAtributos.Columns("COD").Width = Me.sdbgAtributos.Width * 0.16
    sdbgAtributos.Columns("DEN").Width = Me.sdbgAtributos.Width * 0.5
    sdbgAtributos.Columns("VALOR").Width = Me.sdbgAtributos.Width * 0.3
    
    For i = iNumCol To sdbgLineas.Columns.Count - 1
        sdbgLineas.Columns.Item(i).Width = Me.sdbgLineas.Width * 0.12
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   
End Sub

''' <summary>
''' Libera objetos
''' </summary>
''' <param name="Cancel">Cancelar el cierre</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_bGenerarPedido = False
    
    Set m_oSolicitudSeleccionada = Nothing
    Set m_oProceso = Nothing
    Set m_oGrupoSeleccionado = Nothing
    Set m_oPlantillaSeleccionada = Nothing
    
    m_bCambio = False
    frmSELGrupo.txtLabelCod = ""
    frmSELGrupo.txtLabelDen = ""
    
    Set m_oPlantillaSeleccionada = Nothing
    Set m_oPlantillas = Nothing
    Set m_oEmpresas = Nothing
    m_sPlantilla = ""
    Set m_oAtribEspePlantG = Nothing
    Set m_oAtribEspePlantI = Nothing
    Set m_oAtribEspeSinValorI = Nothing
    Set m_oAtribEspeSinValorG = Nothing
    Set m_oAtributoI = Nothing
    Set m_oAtributoG = Nothing
    bGuardar = False
    iorigen = 0
    bUpdate = False
    iColumna = 0
    iLineaLineas = Empty
    iLineaAtributos = Empty
    
    NombrePlantilla1 = ""
    IdPlantilla1 = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Rellena la variable global a la pantalla 'm_oProceso'. Esta variable es el proceso a generar � del q generar
''' oferta � del q generar adjuducaciones � del q generar pedido directo.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: CargarDatosProceso; Tiempo m�ximo:0,6</remarks>
Private Sub GenerarProcesoCompra()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim oGrProc As CGrupo
    Dim bHayReten  As Boolean
    Dim bVisibleProve As Boolean
    Dim bFinBusquedaProve As Boolean
    Dim oLineaDesglose As CLineaDesglose

    'carga las l�neas de desglose y genera la estructura del proceso:
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oAtribEspePlantG = Nothing
    Set m_oAtribEspePlantI = Nothing
    Set m_oEmpresas = oFSGSRaiz.Generar_CEmpresas
    Set m_oProceso = oFSGSRaiz.Generar_CProceso
    
    bHayReten = False
    m_oProceso.RetencionEnGarantia = ""
    
    m_oSolicitudSeleccionada.GenerarEstructuraProceso m_oProceso, g_oParametrosSM, m_oEmpresas, m_bRestrMat, m_sIdiTrue, m_sIdiFalse, m_arrMatComp, , g_arSolicitudes
    
    'Si la solicitud no ten�a ning�n grupo (aparte del de los datos generales) le a�adimos al proceso el de por defecto:
    If m_oProceso.Grupos Is Nothing Then
        Set oGrProc = Nothing
        Set oGrProc = oFSGSRaiz.generar_cgrupo
        oGrProc.Codigo = gParametrosInstalacion.gsGrupoCod
        oGrProc.Den = gParametrosInstalacion.gsGrupoDen
        oGrProc.Descripcion = gParametrosInstalacion.gsGrupoDescrip
        
        Set m_oProceso.Grupos = oFSGSRaiz.Generar_CGrupos

        m_oProceso.Grupos.Add m_oProceso, oGrProc.Codigo, oGrProc.Den, oGrProc.Codigo, oGrProc.Descripcion, oGrProc.DefDestino, oGrProc.DestCod, oGrProc.DefFechasSum, oGrProc.FechaInicioSuministro, oGrProc.FechaFinSuministro, oGrProc.DefFormaPago, oGrProc.PagCod, oGrProc.DefProveActual, oGrProc.ProveActual, oGrProc.DefDistribUON, , oGrProc.DefPresAnualTipo1, , oGrProc.DefPresAnualTipo2, , oGrProc.DefPresTipo1, , oGrProc.DefPresTipo2, , oGrProc.DefEspecificaciones, oGrProc.esp, , oGrProc.Items, , , , , , , , , , , , , oGrProc.AtributosEspecificacion, , , , , , , oGrProc.DistsNivel1, oGrProc.DistsNivel2, oGrProc.DistsNivel3
    End If
    
    'Comprobar que las UONS obtenidas cumplen con el 'Nivel m�nimo de asignaci�n a unidades organizativas
    ComprobarNivelMinimo
    
    With sdbgLineas
        If Not IsEmpty(g_arSolicitudes) Then
            .Columns("SOLICITUD").Visible = (UBound(g_arSolicitudes) > 0)
        End If
    
        'Si el destino,forma de pago y fechas de suministro no est�n a nivel de �tem no se muestran en la grid:
        If m_oProceso.DefDestino <> EnItem Then .Columns("DEST").Visible = False
        
        If m_oProceso.DefFormaPago <> EnItem Then .Columns("PAGO").Visible = False
        
        If m_oProceso.DefFechasSum <> EnItem Then
            .Columns("FEC_INI").Visible = False
            .Columns("FEC_FIN").Visible = False
        Else
            .Columns("FEC_INI").StyleSet = "normal"
            .Columns("FEC_FIN").StyleSet = "normal"
                
            'Edici�n de fechas inicio y fin de suministro
            .Columns("FEC_INI").Style = ssStyleEditButton
            .Columns("FEC_INI").Locked = False
            .Columns("FEC_FIN").Style = ssStyleEditButton
            .Columns("FEC_FIN").Locked = False
        End If
    End With
    
    If g_bGenerarPedido Or g_bPedidoExistente Then
        bVisibleProve = False
        bFinBusquedaProve = False
                       
        For Each oGrupo In m_oSolicitudSeleccionada.Grupos
            For Each oCampo In oGrupo.CAMPOS
                If oCampo.CampoGS = TipoCampoGS.Proveedor Then
                    bVisibleProve = False
                    bFinBusquedaProve = True
                                        
                    Exit For
                ElseIf Not oCampo.LineasDesglose Is Nothing Then
                    For Each oLineaDesglose In oCampo.LineasDesglose
                        If oLineaDesglose.CampoHijo.CampoGS = TipoCampoGS.Proveedor Then
                            bVisibleProve = True
                            bFinBusquedaProve = True
    
                            Exit For
                        End If
                    Next
                    
                    If bFinBusquedaProve Then Exit For
                End If
            Next
            
            If bFinBusquedaProve Then Exit For
        Next
                            
        sdbgLineas.Columns("PROV").Visible = bVisibleProve
    Else
        If m_oProceso.DefProveActual <> EnItem Then
            sdbgLineas.Columns("PROV").Visible = False
        End If
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "GenerarProcesoCompra", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Generar pedido (index 3)-> fondo de proveedor a blanco
''' Eoc -> fondo de proveedor a amarillo
''' </summary>
''' <param name="Index">q option se pulso</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub optGenerar_Click(Index As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Index
        Case 3
            sdbgLineas.Columns("PROV").StyleSet = "normal"
        Case Else
            sdbgLineas.Columns("PROV").StyleSet = "amarillo"
    End Select
    
    Form_Resize
    If m_bRespetarCombo Then Exit Sub
    m_iGenerar = Index
    Screen.MousePointer = vbHourglass
    CargarPlantillas Index
    Screen.MousePointer = vbNormal

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "optGenerar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcMat_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sGMN1Cod = sdbcMat.Columns("GMN1").Value
    m_sGMN2Cod = sdbcMat.Columns("GMN2").Value
    m_sGMN3Cod = sdbcMat.Columns("GMN3").Value
    m_sGMN4Cod = sdbcMat.Columns("GMN4").Value
    
    If Not m_bPermProcMultimaterial Then
        ComprobarArticuloDelMat
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbcMat_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMat_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcMat.DataFieldList = "Column 4"
    sdbcMat.DataFieldToDisplay = "Column 4"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbcMat_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcMat_PositionList(ByVal Text As String)
PositionList sdbcMat, Text, 4
End Sub



Private Sub sdbgAtributos_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbgAtributos.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbgAtributos.Rows - 1
            bm = sdbgAtributos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbgAtributos.Columns(0).CellText(bm), 1, Len(Text))) Then
                If Me.Visible Then sdbgAtributos.SetFocus
                sdbgAtributos.Bookmark = bm
                sdbgAtributos.col = 2
                Exit For
            End If
        Next i
    End If

End Sub



Private Sub sdbgLineas_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbgLineas.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbgLineas.Rows - 1
            bm = sdbgLineas.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbgLineas.Columns("BM_AEP").CellText(bm), 1, Len(Text))) Then
                If Me.Visible Then sdbgLineas.SetFocus
                sdbgLineas.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcPlantilla_CloseUp()
       
    'Se a�ade la condici�n de "NombrePlantilla1" ya que ahora con el nuevo permiso de apertura obligatoria con plantillas,
    'puede que no se seleccione nada en el desplegable de plantillas pero tengamos la plantilla elegida desde 'CargarPlantillas'
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
'si hay plantilla seleccionada, miramos si es la misma para no repetir todo
If Not m_oPlantillaSeleccionada Is Nothing Then
    If sdbcPlantilla.Columns(0).Value = m_oPlantillaSeleccionada.nombre Then
        Exit Sub
    Else 'Si no han seleccionado plantilla pero tiene que tenerla
        If sdbcPlantilla.Columns(1).Value = "..." And NombrePlantilla1 <> "" Then
            sdbcPlantilla.Text = NombrePlantilla1
            sdbcPlantilla_PositionList (sdbcPlantilla.Text)
            Exit Sub
        End If
    End If
Else 'Si no hay seleccionada y seleccionan el vacio tb salimos para no repetir
    If sdbcPlantilla.Columns(1).Value = "..." Then Exit Sub
End If

'Quita la plantilla se genera desde solicitud
If (sdbcPlantilla.Value = "..." Or sdbcPlantilla.Text = "") Then
        sdbcPlantilla.Text = ""
        Set m_oPlantillaSeleccionada = Nothing
        '3275
        'Cada vez que se carga una plantilla, se borran del proceso los atributos de especificacion
        'de la anterior plantilla seleccionada
        EliminarAtribEspePlantillaDelProceso
        
        b_Bloqueo = False
        
        'Caso en el se ha elegido plantilla, pero luego se vuelve a elegir la fila vacia, es decir no plantilla
        Set m_oProceso = Nothing
        Set m_oProcesoPlant = Nothing
        
        GenerarProcesoCompra
        
        PonerGruposPlantillaEnTab
        If ssTabGrupos.Tabs.Count > 0 Then
            ssTabGrupos.Tabs(1).Selected = True
        End If
        
Else 'Han elegido plantilla
        Set m_oPlantillas = oFSGSRaiz.Generar_CPlantillas
        
        sdbcPlantilla.Value = sdbcPlantilla.Columns(0).Value
        
        m_oPlantillas.CargarTodasLasPlantillas sdbcPlantilla.Columns(1).Value, False
        Set m_oPlantillaSeleccionada = m_oPlantillas.Item(sdbcPlantilla.Columns(1).Value)
        PlantillaSeleccionada
        
        b_Bloqueo = False
End If
    
    'Columna 'Grupo destino proceso' visible al seleccionar una plantilla que tiene mas de un grupo
    If Not m_oPlantillaSeleccionada Is Nothing Then
        If m_oPlantillaSeleccionada.Grupos.Count > 1 Or m_bModifProcPlantilla Then
            sdbgLineas.Columns("GR_DESTINO_PROC").Visible = True
        Else
            sdbgLineas.Columns("GR_DESTINO_PROC").Visible = False
        End If
    
    End If

    frmSELGrupo.txtLabelCod = ""
    frmSELGrupo.txtLabelDen = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbcPlantilla_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Cambia los tabs de la solicitud por los de la plantilla seleccionada
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: PlantillaSeleccionada, sdbcPlantilla_CloseUp; Tiempo m�ximo:0</remarks>
Private Sub PonerGruposPlantillaEnTab()
Dim oGrPlant As CGrupo
Dim i As Integer

        ssTabGrupos.Tabs.clear
        If Not m_oProceso.Grupos Is Nothing Then
            For Each oGrPlant In m_oProceso.Grupos
                ssTabGrupos.Tabs.Add , "G" & oGrPlant.Codigo, oGrPlant.Codigo & "-" & oGrPlant.Den
            Next
        
            'Mantener el valor de las pesta�as del proceso
            Set m_oProcesoPlant = Nothing
            Set m_oProcesoPlant = oFSGSRaiz.Generar_CProceso
            Set m_oProcesoPlant.Grupos = oFSGSRaiz.Generar_CGrupos
        
            For i = 1 To m_oProceso.Grupos.Count
                m_oProcesoPlant.Grupos.Add m_oProceso, m_oProceso.Grupos.Item(i).Codigo, m_oProceso.Grupos.Item(i).Den
            Next i

        
        End If

End Sub


Private Sub sdbcPlantilla_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPlantilla.DataFieldList = "Column 0"
    sdbcPlantilla.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbcPlantilla_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

        
End Sub

'''<summary>
'''Capturar la tecla Supr (KeyCode = vbKeyDelete)
'''</summary>
Private Sub sdbcPlantilla_KeyDown(KeyCode As Integer, Shift As Integer)
    If m_bRestrAperturaPlantilla Then
        KeyCode = 0
    End If
End Sub

''' <summary>
''' Impide que pueda borrarse el value del combo de la plantilla si se tiene el permiso de abrir con plantilla
''' </summary>
''' <param name="KeyAscii">tecla pulsada</param>
''' <remarks>Llamada desde: Sistema </remarks>
Private Sub sdbcPlantilla_KeyPress(KeyAscii As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bRestrAperturaPlantilla Then
    KeyAscii = 0         'Car�cter nulo: Indicamos que no se debe tener en cuenta la pulsaci�n de la tecla
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbcPlantilla_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub


Private Sub sdbcPlantilla_PositionList(ByVal Text As String)
PositionList sdbcPlantilla, Text
End Sub




Private Sub sdbcPlantilla_Validate(Cancel As Boolean)
Dim bExiste As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPlantilla.Value = "..." Or sdbcPlantilla.Text = "" Then
        sdbcPlantilla.Text = ""
        Set m_oPlantillaSeleccionada = Nothing
    Else
        Set m_oPlantillas = oFSGSRaiz.Generar_CPlantillas

        m_oPlantillas.CargarTodasLasPlantillas sdbcPlantilla.Columns(1).Value, False
        bExiste = Not (m_oPlantillas.Count = 0)
        If Not bExiste Then
            sdbcPlantilla.Value = ""
            Set m_oPlantillaSeleccionada = Nothing
        Else
            sdbcPlantilla.Value = m_oPlantillas.Item(sdbcPlantilla.Columns(1).Value).nombre
            Set m_oPlantillaSeleccionada = m_oPlantillas.Item(sdbcPlantilla.Columns(1).Value)
        End If
    End If
    
    PlantillaSeleccionada
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbcPlantilla_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

''' <summary>
''' Cuando se selecciona o cambia de plantilla se realizan las acciones necesarias
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sdbcPlantilla_CloseUp, sdbcPlantilla_Validate; Tiempo m�ximo:0</remarks>
Private Sub PlantillaSeleccionada()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If m_oPlantillaSeleccionada Is Nothing Then Exit Sub
    
    'Cada vez que se carga una plantilla, se borran del proceso los atributos de especificacion
    'de la anterior plantilla seleccionada
    EliminarAtribEspePlantillaDelProceso
    
    'Generamos la estructura de m_oProceso seg�n la estructura de grupos de la plantilla seleccionada
    If m_oPlantillaSeleccionada.Id <> m_oProceso.Plantilla Then
        GenerarProcesoPlantilla
    End If
    
    'Cambio en la visualizaci�n de las pesta�as del grid
    'Ahora aparecen como pesta�as no los grupos de la solicitud, sino los grupos de la plantilla
    PonerGruposPlantillaEnTab

    EstablecerGrupoEnItems
    'Si hay plantilla seleccionada y no es apertura de proceso primera opcion, a�adimos sus atributos de especificacion obligatorios
    'en el proceso
    If m_iGenerar <> 0 Then
        AnyadirAtribEspeObligatoriosPlantillaAProceso
    End If

    If ssTabGrupos.Tabs.Count > 0 Then
        ssTabGrupos.Tabs(1).Selected = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "EstablecerGrupoEnItems", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
Private Sub EstablecerGrupoEnItems()
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim oGrupoPlant As CGrupo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_oPlantillaSeleccionada.Grupos.Count = 0 Then

Else

    For Each oGrupo In oGruposSolicit
        Set oGrupoPlant = DevolverGrupoPlantillaParaItem(oGrupo.Codigo, oGrupo.Den, oGrupo.Id)
        If Not oGrupoPlant Is Nothing Then
            If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    oItem.grupoCod = oGrupoPlant.Codigo
                Next
            End If
        End If
    Next
    
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "EstablecerGrupoEnItems", err, Erl, , m_bActivado)
      Exit Sub
   End If


End Sub


''' <summary>
''' Muestra los atributos de especificacion de ambito GRUPO en el grid "Otros Datos:"
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Tiempo m�ximo:0</remarks>

Private Sub MostrarAtributosGrupo()
Dim oAtr As CAtributo
Dim scod As String

Dim sGrupo As String
Dim sValor As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.RemoveAll
    
    sGrupo = Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2)
    
    If Not m_oAtribEspePlantG Is Nothing Then
    
        If picAtrbEsp.Visible = False And bMostrarAtrEsp = False Then
            sdbgLineas.Height = sdbgLineas.Height - 1500
            picAtrbEsp.Visible = True
        End If
        
        bMostrarAtrEsp = True
        
        For Each oAtr In m_oProceso.Grupos.Item(sGrupo).AtributosEspecificacion
            If m_oAtribEspePlantG.ExisteAtributo(oAtr.Atrib) Then
                If oAtr.Obligatorio Then
                    scod = "(*) " & oAtr.Cod
                Else
                    scod = oAtr.Cod
                End If
                
                sValor = ""
                
                Select Case oAtr.Tipo
                    Case TiposDeAtributos.TipoString
                        If Not IsNull(oAtr.valorText) Then
                            sValor = oAtr.valorText
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        If Not IsNull(oAtr.valorNum) Then
                            sValor = CStr(oAtr.valorNum)
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If Not IsNull(oAtr.valorFec) Then
                            sValor = CStr(oAtr.valorFec)
                        End If
                    Case TiposDeAtributos.TipoBoolean
                        If Not IsNull(oAtr.valorBool) Then
                            If oAtr.valorBool = True Then
                                sValor = m_sIdiTrue
                            Else
                                sValor = m_sIdiFalse
                            End If
                        End If
                    End Select

                sdbgAtributos.AddItem scod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & sValor & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Atrib & Chr(m_lSeparador) & oAtr.idAtribProce & Chr(m_lSeparador) & oAtr.Obligatorio
            End If
        Next
    Else
       
        If picAtrbEsp.Visible = True And bMostrarAtrEsp = True Then
            picAtrbEsp.Visible = False
            sdbgLineas.Height = sdbgLineas.Height + 1500
        End If
            
        bMostrarAtrEsp = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "MostrarAtributosGrupo", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub






Private Sub sdbgAtributos_BtnClick()
Dim sUON1 As String
Dim sUON2 As String
Dim sUON3 As String
Dim sValAtr As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
With sdbgAtributos
    Select Case .Columns(.col).Name
        Case "VALOR"
            If m_oAtribEspePlantG.Item(CStr(sdbgAtributos.Columns("ID_A").Value)).ListaExterna Then
                'Si el proceso ya tiene Distribuci�n se coge la primera que se encuentre, da igual el ambito y el porcentaje,
                'se coge la primera que se encuentre,
                'se necesita para pasar a la dll de integraci�n aunque despu�s en la consulta no se va a usar
                Select Case m_oProceso.DefDistribUON
                    Case EnProceso
                        If Not m_oProceso.DistsNivel3 Is Nothing Then
                            sUON1 = m_oProceso.DistsNivel3.Item(1).CodUON1
                            sUON2 = m_oProceso.DistsNivel3.Item(1).CodUON2
                            sUON3 = m_oProceso.DistsNivel3.Item(1).CodUON3
                        ElseIf Not m_oProceso.DistsNivel2 Is Nothing Then
                            sUON1 = m_oProceso.DistsNivel2.Item(1).CodUON1
                            sUON2 = m_oProceso.DistsNivel2.Item(1).CodUON2
                            sUON3 = ""
                        ElseIf Not m_oProceso.DistsNivel1 Is Nothing Then
                            sUON1 = m_oProceso.DistsNivel1.Item(1).CodUON1
                            sUON2 = ""
                            sUON3 = ""
                        End If
                    Case EnGrupo
                        If Not m_oProceso.Grupos Is Nothing Then
                            If Not m_oProceso.Grupos.Item(1).DistsNivel3 Is Nothing Then
                                sUON1 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).DistsNivel3.Item(1).CodUON1
                                sUON2 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).DistsNivel3.Item(1).CodUON2
                                sUON3 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).DistsNivel3.Item(1).CodUON3
                            ElseIf Not m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).DistsNivel2 Is Nothing Then
                                sUON1 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).DistsNivel2.Item(1).CodUON1
                                sUON2 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).DistsNivel2.Item(1).CodUON2
                                sUON3 = ""
                            ElseIf Not m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).DistsNivel1 Is Nothing Then
                                sUON1 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).DistsNivel1.Item(1).CodUON1
                                sUON2 = ""
                                sUON3 = ""
                            End If
                        End If
                    Case EnItem
                        sUON1 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).Items.Item(1).UON1Cod
                        sUON2 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).Items.Item(1).UON2Cod
                        sUON3 = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).Items.Item(1).UON3Cod
                End Select
                If sUON1 = "" Then
                    'Es obligatorio que indique previamente la distribuci�n de la compra
                    oMensajes.DistribucionObligatoria
                    Exit Sub
                End If
                sValAtr = MostrarFormSelAtribListaExterna(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, _
                                                .Columns("COD").Value, .Columns("DEN").Value, .Columns("ID_A").Value, _
                                                oUsuarioSummit.Cod, oFSGSRaiz, sUON1, sUON2, sUON3, , m_oProceso.AtributosEspecificacion, , m_oSolicitudSeleccionada.Id)
                If sValAtr <> "" Then
                    .Columns(.col).Value = sValAtr
                End If
            End If
    End Select
End With

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirProc", "sdbgAtributos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAtributos_KeyPress(KeyAscii As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbgAtributos.col = -1 Then Exit Sub
Select Case sdbgAtributos.Columns(sdbgAtributos.col).Name
    Case "VALOR"
        Set m_oAtributoG = oFSGSRaiz.Generar_CAtributo
        Set m_oAtributoG = m_oAtribEspePlantG.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
        If m_oAtributoG Is Nothing Then Exit Sub
        If m_oAtributoG.ListaExterna Then
            If KeyAscii = 8 Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = ""
            End If
        End If
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirProc", "sdbgAtributos_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgLineas_AfterUpdate(RtnDispErrMsg As Integer)

Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    RtnDispErrMsg = 0
    iorigen = 1
    
If Not m_oAtributoI Is Nothing Then
    If Not sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).Value = "" And bUpdate = True Then
            
        If Not m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).Items Is Nothing Then
            Set oItem = m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).Items.Item(CStr(sdbgLineas.Columns("LINEA").Value))
            If Not oItem Is Nothing Then
                If Not oItem.AtributosEspecificacion.Item(m_oAtributoI.Atrib) Is Nothing Then
                    Select Case m_oAtributoI.Tipo
                        Case TiposDeAtributos.TipoString
                            oItem.AtributosEspecificacion.Item(m_oAtributoI.Atrib).valorText = CStr(sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).Value)
                        Case TiposDeAtributos.TipoNumerico
                            oItem.AtributosEspecificacion.Item(m_oAtributoI.Atrib).valorNum = CDbl(sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).Value)
                        Case TiposDeAtributos.TipoFecha
                            oItem.AtributosEspecificacion.Item(m_oAtributoI.Atrib).valorFec = CDate(sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).Value)
                        Case TiposDeAtributos.TipoBoolean
                            If UCase(sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).Value) = UCase(m_sIdiTrue) Then
                                oItem.AtributosEspecificacion.Item(m_oAtributoI.Atrib).valorBool = True
                            Else
                                oItem.AtributosEspecificacion.Item(m_oAtributoI.Atrib).valorBool = False
                            End If
                    End Select
                End If
            End If
        End If
    End If
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgLineas_AfterUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub








Private Sub sdbgatributos_AfterUpdate(RtnDispErrMsg As Integer)



If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    RtnDispErrMsg = 0
    iorigen = 2

    If Not sdbgAtributos.Columns("VALOR").Value = "" And bUpdate = True Then
        'Si el grupo tiene atributos de especificacion...
        If Not m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).AtributosEspecificacion Is Nothing Then
            'Si existe el atributo en este grupo, se actualiza su valor
            If Not m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).AtributosEspecificacion.Item(m_oAtributoG.Atrib) Is Nothing Then
                Select Case m_oAtributoG.Tipo
                    Case TiposDeAtributos.TipoString
                        m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).AtributosEspecificacion.Item(m_oAtributoG.Atrib).valorText = CStr(sdbgAtributos.Columns("VALOR").Value)
                    Case TiposDeAtributos.TipoNumerico
                        m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).AtributosEspecificacion.Item(m_oAtributoG.Atrib).valorNum = CDbl(sdbgAtributos.Columns("VALOR").Value)
                    Case TiposDeAtributos.TipoFecha
                        m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).AtributosEspecificacion.Item(m_oAtributoG.Atrib).valorFec = CDate(sdbgAtributos.Columns("VALOR").Value)
                    Case TiposDeAtributos.TipoBoolean
                        If UCase(sdbgAtributos.Columns("VALOR").Value) = UCase(m_sIdiTrue) Then
                            m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).AtributosEspecificacion.Item(m_oAtributoG.Atrib).valorBool = True
                        Else
                            m_oProceso.Grupos.Item(m_oGrupoSeleccionado.Codigo).AtributosEspecificacion.Item(m_oAtributoG.Atrib).valorBool = False
                        End If
                    End Select
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgatributos_AfterUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub ActualizarAtribEspeGrupoPlantilla()


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAtributos.DataChanged Then
        If sdbgAtributos.Rows = 1 Then
            sdbgAtributos.Update
        Else
            sdbgAtributos.MoveNext
            If bGuardar = True Then
                sdbgAtributos.MovePrevious
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ActualizarAtribEspeGrupoPlantilla", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub

Private Sub ActualizarAtribEspeItemPlantilla()
    Dim bEncontrado As Boolean
    Dim sError As String
    Dim bSalir As Boolean
    Dim oElem As CValorPond
    Dim vValorM As Variant
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    With sdbgLineas
        If Not m_oAtributoI Is Nothing Then
            If .Columns(CStr(m_oAtributoI.Atrib)).Text = "" Then
                oMensajes.DatoObligatorio m_oAtributoI.Cod
                bGuardar = False
                InicializaValorAtributo m_oAtributoI
                bUpdate = False
                sdbgLineas_PositionList .Columns("BM_AEP").Text
                PosicionarCeldaGridLineasItem m_oAtributoI
                Exit Sub
            End If
        
            If .Columns(CStr(m_oAtributoI.Atrib)).Text <> "" Then
                bSalir = False
                Select Case m_oAtributoI.Tipo
                    Case TiposDeAtributos.TipoNumerico
                            sError = m_oAtributoI.Cod
                            If Not IsNumeric(.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                            bSalir = True
                        End If
                    Case TiposDeAtributos.TipoFecha
                            sError = m_oAtributoI.Cod
                            If Not IsDate(.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                            bSalir = True
                        End If
                    Case TiposDeAtributos.TipoBoolean
                            sError = m_oAtributoI.Cod
                            If UCase(.Columns(CStr(m_oAtributoI.Atrib)).Text) <> UCase(m_sIdiTrue) And UCase(.Columns(CStr(m_oAtributoI.Atrib)).Text) <> UCase(m_sIdiFalse) Then
                            bSalir = True
                        End If
                End Select
                
                If bSalir Then
                    oMensajes.NoValido sError
                    bGuardar = False
                    .Columns(CStr(m_oAtributoI.Atrib)).Text = ""
                    .CancelUpdate
                    .DataChanged = False
                    InicializaValorAtributo m_oAtributoI
                    bUpdate = False
                    sdbgLineas_PositionList .Columns("BM_AEP").Text
                    PosicionarCeldaGridLineasItem m_oAtributoI
                    Exit Sub
                End If
                
                bEncontrado = False
        
                If m_oAtributoI.TipoIntroduccion = Introselec Then
                    m_oAtributoI.CargarListaDeValores
                    For Each oElem In m_oAtributoI.ListaPonderacion
                        Select Case m_oAtributoI.Tipo
                            Case TiposDeAtributos.TipoString
                                If oElem.ValorLista = .Columns(CStr(m_oAtributoI.Atrib)).Text Then
                                    bEncontrado = True
                                    Exit For
                                End If
                            Case TiposDeAtributos.TipoNumerico
                                If CDbl(oElem.ValorLista) = CDbl(.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                                    bEncontrado = True
                                    Exit For
                                End If
                            Case TiposDeAtributos.TipoFecha
                                If CDate(oElem.ValorLista) = CDate(.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                                    bEncontrado = True
                                    Exit For
                                End If
                        End Select
                    Next
                    Set oElem = Nothing
                    
                    If Not bEncontrado Then
                        oMensajes.NoValido m_oAtributoI.Cod
                        bGuardar = False
                        .CancelUpdate
                        .Columns(CStr(m_oAtributoI.Atrib)).Text = ""
                        .DataChanged = False
                        InicializaValorAtributo m_oAtributoI
                        bUpdate = False
                        sdbgLineas_PositionList .Columns("BM_AEP").Text
                        PosicionarCeldaGridLineasItem m_oAtributoI
                        Exit Sub
                    End If
                End If
                bSalir = False
                sError = ""
                
                If m_oAtributoI.TipoIntroduccion = IntroLibre Then
                    Select Case m_oAtributoI.Tipo
                        Case TiposDeAtributos.TipoNumerico
                            vValorM = CDec(.Columns(CStr(m_oAtributoI.Atrib)).Text)
        
                            If IsNumeric(m_oAtributoI.Maximo) Then
                                sError = FormateoNumerico(m_oAtributoI.Maximo)
                                If CDbl(m_oAtributoI.Maximo) < CDbl(vValorM) Then
                                    bSalir = True
                                End If
                            End If
                            If IsNumeric(m_oAtributoI.Minimo) Then
                                If sError = "" Then
                                    sError = sIdiMayor & " " & FormateoNumerico(m_oAtributoI.Minimo)
                                Else
                                    sError = sIdiEntre & "  " & FormateoNumerico(m_oAtributoI.Minimo) & " - " & sError
                                End If
                                If CDbl(m_oAtributoI.Minimo) > CDbl(vValorM) Then
                                    bSalir = True
                                End If
                            Else
                                If sError <> "" Then
                                    sError = sIdiMenor & " " & sError
                                End If
                            End If
                        Case TiposDeAtributos.TipoFecha
                            If IsDate(m_oAtributoI.Maximo) Then
                                sError = m_oAtributoI.Maximo
                                If CDate(m_oAtributoI.Maximo) < CDate(.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                                    bSalir = True
                                End If
                            End If
                            If IsDate(m_oAtributoI.Minimo) Then
                                If sError = "" Then
                                    sError = sIdiMayor & " " & m_oAtributoI.Minimo
                                Else
                                    sError = sIdiEntre & "  " & m_oAtributoI.Minimo & " - " & sError
                                End If
                                If CDate(m_oAtributoI.Minimo) > CDate(.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                                    bSalir = True
                                End If
                            Else
                                If sError <> "" Then
                                    sError = sIdiMenor & " " & sError
                                End If
                            End If
                    End Select
                    
                    If bSalir Then
                        oMensajes.AtributoValorNoValido sError
                        bGuardar = False
                        .CancelUpdate
                        .Columns(CStr(m_oAtributoI.Atrib)).Text = ""
                        .DataChanged = False
                        InicializaValorAtributo m_oAtributoI
                        bUpdate = False
                        sdbgLineas_PositionList .Columns("BM_AEP").Text
                        PosicionarCeldaGridLineasItem m_oAtributoI
                        Exit Sub
                    End If
                End If
            End If
        End If
    End With
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ActualizarAtribEspeItemPlantilla", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Comprueba las fechas de inicio y fin de suministro</summary>
''' <returns>Booleano indicando si la validaci�n ha sido correcta</returns>
''' <remarks>Llamada desde: sdbgLineas_BeforeUpdate</remarks>

Private Function ValidarFechasInicioFinSuministro() As Boolean
    Dim bOk As Boolean
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
   
    bOk = True
    With sdbgLineas
        If .Columns("FEC_INI").Visible Then
            If Trim(.Columns("FEC_INI").Value) <> "" Then
                If Not IsDate(.Columns("FEC_INI").Value) Then
                    bOk = False
                    oMensajes.NoValida .Columns("FEC_INI").caption
                End If
            End If
        End If
        
        If bOk And .Columns("FEC_FIN").Visible Then
            If Trim(.Columns("FEC_FIN").Value) <> "" Then
                If Not IsDate(.Columns("FEC_FIN").Value) Then
                    bOk = False
                    oMensajes.NoValida .Columns("FEC_FIN").caption
                Else
                    If .Columns("FEC_INI").Value <> "" Then
                        If CDate(.Columns("FEC_FIN").Value) < CDate(.Columns("FEC_INI").Value) Then
                            bOk = False
                            oMensajes.FechaDesdeMayorFechaHasta 2
                        End If
                    End If
                End If
            End If
        End If
    End With
    
Salir:
    ValidarFechasInicioFinSuministro = bOk
    Exit Function
Error:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ActualizarAtribEspeItemPlantilla", err, Erl, , m_bActivado)
       bOk = False
       Resume Salir
    End If
End Function

Private Sub sdbgLineas_BeforeUpdate(Cancel As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 1
    
    If Not bUpdate Then Exit Sub
    
    With sdbgLineas
        iLineaLineas = .Bookmark
        
        'Comprobaci�n de las fechas de inicio y fin de suministro
        If Not ValidarFechasInicioFinSuministro Then
            Cancel = True
            bGuardar = False
            If Me.Visible Then .SetFocus
            Exit Sub
        Else
            'Actualizar objeto
            If .Columns("FEC_INI").Value <> "" Then
                m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items.Item(CStr(.Columns("LINEA").Value)).FechaInicioSuministro = .Columns("FEC_INI").Value
            Else
                m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items.Item(CStr(.Columns("LINEA").Value)).FechaInicioSuministro = 0
            End If
            If .Columns("FEC_FIN").Value <> "" Then
                m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items.Item(CStr(.Columns("LINEA").Value)).FechaFinSuministro = .Columns("FEC_FIN").Value
            Else
                m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items.Item(CStr(.Columns("LINEA").Value)).FechaFinSuministro = 0
            End If
        End If
        
        ActualizarAtribEspeItemPlantilla
    End With
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgLineas_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub

Private Sub PosicionarCeldaGridLineasItem(ByRef oAtributo As CAtributo)

    Dim bEncontrado As Boolean
    Dim j As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bEncontrado = False

    bUpdate = False
    
    For j = 0 To sdbgLineas.Columns.Count - 1

        If CStr(sdbgLineas.Columns(j).Name) = oAtributo.Atrib Then
            bUpdate = False
            sdbgLineas.col = j
            iColumna = j
            bEncontrado = True
            Exit For
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "PosicionarCeldaGridLineasItem", err, Erl, , m_bActivado)
      Exit Sub
   End If


End Sub




Private Sub sdbgatributos_BeforeUpdate(Cancel As Integer)
    Dim bSalir As Boolean
    Dim bEncontrado As Boolean
    Dim sError As String
    Dim oElem As CValorPond
    Dim vValorM As Variant
    Dim oAtributo As CAtributo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 2
    iLineaAtributos = sdbgAtributos.Bookmark
    
    If bUpdate = False Then
        Exit Sub
    End If
    
    Set oAtributo = Nothing
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    Set oAtributo = m_oAtribEspePlantG.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
    
    If sdbgAtributos.Columns("VALOR").Text = "" Then
        oMensajes.DatoObligatorio oAtributo.Cod
        bUpdate = False
        sdbgAtributos_PositionList oAtributo.Cod
        InicializaValorAtributo oAtributo
        bGuardar = False
        Exit Sub
    End If

    If sdbgAtributos.Columns("VALOR").Text <> "" Then
        bSalir = False
        Select Case sdbgAtributos.Columns("TIPO").Value
            Case TiposDeAtributos.TipoNumerico
                sError = m_oAtributoG.Cod
                If Not IsNumeric(sdbgAtributos.Columns("VALOR").Text) Then
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoFecha
                sError = m_oAtributoG.Cod
                If Not IsDate(sdbgAtributos.Columns("VALOR").Text) Then
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoBoolean
                sError = m_oAtributoG.Cod
                If UCase(sdbgAtributos.Columns("VALOR").Text) <> UCase(m_sIdiTrue) And UCase(sdbgAtributos.Columns("VALOR").Text) <> UCase(m_sIdiFalse) Then
                    bSalir = True
                End If
        End Select
        If bSalir Then
            sdbgAtributos.Columns("VALOR").Text = ""
            sdbgAtributos.CancelUpdate
            sdbgAtributos.DataChanged = False
            InicializaValorAtributo oAtributo
            oMensajes.NoValido sError
            bUpdate = False
            sdbgAtributos_PositionList oAtributo.Cod
            bGuardar = False
            Exit Sub
        End If
        
        bEncontrado = False

        If m_oAtributoG.TipoIntroduccion = Introselec Then
            m_oAtributoG.CargarListaDeValores
            For Each oElem In m_oAtributoG.ListaPonderacion
                Select Case sdbgAtributos.Columns("TIPO").Value
                Case TiposDeAtributos.TipoString
                    If oElem.ValorLista = sdbgAtributos.Columns("VALOR").Text Then
                        bEncontrado = True
                        Exit For
                    End If
                Case TiposDeAtributos.TipoNumerico
                    If CDbl(oElem.ValorLista) = CDbl(sdbgAtributos.Columns("VALOR").Text) Then
                        bEncontrado = True
                        Exit For
                    End If
                Case TiposDeAtributos.TipoFecha
                    If CDate(oElem.ValorLista) = CDate(sdbgAtributos.Columns("VALOR").Text) Then
                        bEncontrado = True
                        Exit For
                    End If
                End Select
            Next
            If Not bEncontrado Then
                oMensajes.NoValido m_oAtributoG.Cod
                bGuardar = False
                sdbgAtributos.CancelUpdate
                sdbgAtributos.Columns("VALOR").Text = ""
                sdbgAtributos.DataChanged = False
                InicializaValorAtributo oAtributo
                bUpdate = False
                sdbgAtributos_PositionList oAtributo.Cod
                Exit Sub
            End If
        End If
        bSalir = False
        sError = ""
        If m_oAtributoG.TipoIntroduccion = IntroLibre Then
            Select Case sdbgAtributos.Columns("TIPO").Value
                Case TiposDeAtributos.TipoNumerico

                        vValorM = CDec(sdbgAtributos.Columns("VALOR").Text)

                    If IsNumeric(m_oAtributoG.Maximo) Then
                    
                        sError = FormateoNumerico(m_oAtributoG.Maximo)
                        If CDbl(m_oAtributoG.Maximo) < CDbl(vValorM) Then
                            bSalir = True
                        End If
                        
                    End If
                    If IsNumeric(m_oAtributoG.Minimo) Then
                    
                        If sError = "" Then
                            sError = sIdiMayor & " " & FormateoNumerico(m_oAtributoG.Minimo)
                        Else
                            sError = sIdiEntre & "  " & FormateoNumerico(m_oAtributoG.Minimo) & " - " & sError
                        End If
                        If CDbl(m_oAtributoG.Minimo) > CDbl(vValorM) Then
                            bSalir = True
                        End If
                        
                    Else
                    
                        If sError <> "" Then
                            sError = sIdiMenor & " " & sError
                        End If
                        
                    End If
                Case TiposDeAtributos.TipoFecha
                    If IsDate(m_oAtributoG.Maximo) Then
                        sError = m_oAtributoG.Maximo
                        If CDate(m_oAtributoG.Maximo) < CDate(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    End If
                    If IsDate(m_oAtributoG.Minimo) Then
                        If sError = "" Then
                            sError = sIdiMayor & " " & m_oAtributoG.Minimo
                        Else
                            sError = sIdiEntre & "  " & m_oAtributoG.Minimo & " - " & sError
                        End If
                        If CDate(m_oAtributoG.Minimo) > CDate(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    Else
                        If sError <> "" Then
                            sError = sIdiMenor & " " & sError
                        End If
                    End If
            End Select
            If bSalir Then
                sdbgAtributos.Columns("VALOR").Text = ""
                sdbgAtributos.CancelUpdate
                sdbgAtributos.DataChanged = False
                InicializaValorAtributo oAtributo
                oMensajes.AtributoValorNoValido sError
                bUpdate = False
                sdbgAtributos_PositionList oAtributo.Cod
                bGuardar = False
                Exit Sub
            End If
        End If
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgatributos_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub




Private Sub sdbgatributos_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 2

    'DoEvents
    
    bUpdate = True
    
    Set m_oAtributoG = Nothing
    Set m_oAtributoG = oFSGSRaiz.Generar_CAtributo
    Set m_oAtributoG = m_oAtribEspePlantG.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
    
    iLineaAtributos = sdbgAtributos.Bookmark
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgatributos_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Sub sdbgLineas_KeyPress(KeyAscii As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbgLineas.col = -1 Then Exit Sub
If Not m_oAtribEspePlantI Is Nothing And Not sdbgLineas.col < 0 Then
    If Not m_oAtribEspePlantI.Item(CStr(sdbgLineas.Columns(sdbgLineas.col).Name)) Is Nothing Then
        Set m_oAtributoI = oFSGSRaiz.Generar_CAtributo
        Set m_oAtributoI = m_oAtribEspePlantI.Item(CStr(sdbgLineas.Columns(sdbgLineas.col).Name))
        If m_oAtributoI.ListaExterna Then
            If KeyAscii = 8 Then
                sdbgLineas.Columns(sdbgLineas.col).Value = ""
            End If
        End If
    End If
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSOLAbrirProc", "sdbgLineas_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgLineas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 1
    
    With sdbgLineas
        If Not bUpdate Then
            bUpdate = True
            sdbgLineas.Bookmark = iLineaLineas
            sdbgLineas.col = iColumna
        End If
        bGuardar = True
        
        'Proponer fecha fin de suministro a partir de la fecha de inicio
        If LastCol > -1 Then
            If .Columns("FEC_INI").Visible And .Columns("FEC_FIN").Visible Then
                If .Columns(LastCol).Name = "FEC_INI" Then
                    If .Columns("FEC_FIN").Value = "" And .Columns("FEC_INI").Value <> "" Then
                        .Columns("FEC_FIN").Value = .Columns("FEC_INI").Value
                    End If
                End If
            End If
        End If
        
        Set m_oAtributoI = Nothing
        If Not m_oAtribEspePlantI Is Nothing And Not .col < 0 Then
            If Not m_oAtribEspePlantI.Item(CStr(.Columns(.col).Name)) Is Nothing Then
                Set m_oAtributoI = oFSGSRaiz.Generar_CAtributo
                Set m_oAtributoI = m_oAtribEspePlantI.Item(CStr(.Columns(.col).Name))
    
                iLineaLineas = sdbgLineas.Bookmark
                
                Select Case m_oAtributoI.TipoIntroduccion
                    Case 1  'Seleccion
                        sdbddValor.RemoveAll
                        sdbddValor.AddItem ""
                        If m_oAtributoI.Tipo = 2 Then
                            sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
                        Else
                            sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                        End If
                        If m_oAtributoI.ListaExterna Then
                            .Columns(CStr(m_oAtributoI.Atrib)).DropDownHwnd = 0
                            .Columns(CStr(m_oAtributoI.Atrib)).Locked = True
                            .Columns(CStr(m_oAtributoI.Atrib)).Style = ssStyleEditButton
                        Else
                            .Columns(CStr(m_oAtributoI.Atrib)).DropDownHwnd = sdbddValor.hWnd
                            sdbddValor.Enabled = True
                        End If
                    Case 0 'Libre
                        If m_oAtributoI.Tipo = 4 Then
                            sdbddValor.RemoveAll
                            sdbddValor.AddItem ""
                            .Columns(CStr(m_oAtributoI.Atrib)).DropDownHwnd = sdbddValor.hWnd
                            sdbddValor.Enabled = True
                        Else
                            .Columns(CStr(m_oAtributoI.Atrib)).DropDownHwnd = 0
                            sdbddValor.Enabled = False
                        End If
                End Select
            End If
        End If
    End With
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgLineas_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub



Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 2
    
    If bUpdate = False Then
        bUpdate = True
        sdbgAtributos.Bookmark = iLineaAtributos
        sdbgAtributos.col = 2
    End If
    
    If CStr(sdbgAtributos.Columns("ID_A").Value) <> "" Then
        Set m_oAtributoG = Nothing
        Set m_oAtributoG = oFSGSRaiz.Generar_CAtributo
        Set m_oAtributoG = m_oAtribEspePlantG.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
    End If
    sdbgAtributos.Columns("VALOR").Locked = False
    Select Case sdbgAtributos.Columns("INTRO").Value
        Case 1:  'Seleccion
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            If sdbgAtributos.Columns("TIPO").Value = 2 Then
                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
            Else
                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
            End If
            If m_oAtributoG.ListaExterna Then
                sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                sdbgAtributos.Columns("VALOR").Locked = True
                sdbgAtributos.Columns("VALOR").Style = ssStyleEditButton
            Else
                sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                sdbddValor.Enabled = True
            End If
        Case 0: 'Libre
            If sdbgAtributos.Columns("TIPO").Value = 4 Then
                sdbddValor.RemoveAll
                sdbddValor.AddItem ""
                sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                sdbddValor.Enabled = True
            Else
                sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                sdbddValor.Enabled = False
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgAtributos_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Sub sdbddValor_DropDown()

    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.RemoveAll

    Select Case iorigen
        Case 1
            If m_oAtributoI.TipoIntroduccion = "1" Then
                Set oatrib = m_oAtributoI
                oatrib.CargarListaDeValores
                Set oLista = oatrib.ListaPonderacion
                For Each oElem In oLista
                    sdbddValor.AddItem oElem.ValorLista
                Next
                Set oatrib = Nothing
                Set oLista = Nothing
            Else
                If m_oAtributoI.Tipo = 4 Then
                    sdbddValor.AddItem m_sIdiTrue
                    sdbddValor.AddItem m_sIdiFalse
                End If
            End If
            
        Case 2
            If sdbgAtributos.Columns("INTRO").Value = "1" Then
                If Not m_oGrupoSeleccionado Is Nothing Then
                    Set oatrib = m_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
                Else
                    Set oatrib = m_oProceso.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
                End If
            
                oatrib.CargarListaDeValores
                
                Set oLista = oatrib.ListaPonderacion
                For Each oElem In oLista
                    sdbddValor.AddItem oElem.ValorLista
                Next
                Set oatrib = Nothing
                Set oLista = Nothing
            Else
                If sdbgAtributos.Columns("TIPO").Value = 4 Then
                    sdbddValor.AddItem m_sIdiTrue
                    sdbddValor.AddItem m_sIdiFalse
                End If
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbddValor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbddValor_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbddValor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                Select Case iorigen
                    Case 1
                        sdbgLineas.Columns(sdbgLineas.col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                    Case 2
                        sdbgAtributos.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                End Select
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion

Dim bExiste As Boolean
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case iorigen
        Case 1
            bExiste = False
            ''' Comprobar la existencia en la lista
            If m_oAtributoI.TipoIntroduccion = "1" Then
                Set oatrib = m_oAtributoI
                oatrib.CargarListaDeValores
                Set oLista = oatrib.ListaPonderacion
                For Each oElem In oLista
                    If UCase(oElem.ValorLista) = UCase(sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                        bExiste = True
                        Exit For
                    End If
                Next
            Else
                If m_oAtributoI.Tipo = 4 Then
                    If UCase(m_sIdiTrue) = UCase(sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                        bExiste = True
                    End If
                    If UCase(m_sIdiFalse) = UCase(sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).Text) Then
                        bExiste = True
                    End If
                    
                End If
            End If
            If Not bExiste Then
                bUpdate = False
                oMensajes.NoValido sdbgLineas.Columns(CStr(m_oAtributoI.Atrib)).caption
                sdbgLineas.DataChanged = False
                bUpdate = False
                sdbgLineas_PositionList sdbgLineas.Columns("BM_AEP").Text
                PosicionarCeldaGridLineasItem m_oAtributoI
                RtnPassed = False
                bGuardar = False
                Exit Sub
            End If
            RtnPassed = True
        Case 2
            bExiste = False
            ''' Comprobar la existencia en la lista
            If sdbgAtributos.Columns("INTRO").Value = "1" Then
                If Not m_oGrupoSeleccionado Is Nothing Then
                    Set oatrib = m_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
                Else
                    Set oatrib = m_oProceso.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ID_A").Value))
                End If
                oatrib.CargarListaDeValores
                Set oLista = oatrib.ListaPonderacion
                For Each oElem In oLista
                    If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                        bExiste = True
                        Exit For
                    End If
                Next
            Else
                If sdbgAtributos.Columns("TIPO").Value = 4 Then
                    If UCase(m_sIdiTrue) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                        bExiste = True
                    End If
                    If UCase(m_sIdiFalse) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                        bExiste = True
                    End If
                    
                End If
            End If
            If Not bExiste Then
                bUpdate = False
                oMensajes.NoValido sdbgAtributos.Columns("VALOR").caption
                sdbgAtributos.DataChanged = False
                bUpdate = False
                sdbgAtributos_PositionList oatrib.Cod
                RtnPassed = False
                bGuardar = False
                Exit Sub
            End If
            RtnPassed = True
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbddValor_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If


End Sub



Private Sub InicializaValorAtributo(ByRef oAtributo As CAtributo)

    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim oAtributoEsp As CAtributo
    
    Dim bEncontrado As Boolean
    Dim iPosicion As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bEncontrado = False

    If Not m_oProceso.Grupos Is Nothing Then
        ' Por cada grupo del proceso....
        For Each oGrupo In m_oProceso.Grupos
            'Si el GRUPO tiene atributos de especificacion, miro si alguno es de plantilla y valido si tiene o no valor
            If Not oGrupo.AtributosEspecificacion Is Nothing Then
                For Each oAtributoEsp In oGrupo.AtributosEspecificacion
                    'Si el atributo de especificacion en curso, es de la plantilla, se comprueba si esta informado
                    If oAtributoEsp.Atrib = oAtributo.Atrib Then
                        
                        oAtributoEsp.valorBool = Null
                        oAtributoEsp.valorFec = Null
                        oAtributoEsp.valorNum = Null
                        oAtributoEsp.valorText = Null
                        
                        bEncontrado = True
                        
                    End If
                    
                    If bEncontrado Then
                        Exit For
                    End If
                Next
            End If
            If bEncontrado = True Then
                Exit For
            End If
            If Not oGrupo.Items Is Nothing Then
                iPosicion = 0
                For Each oItem In oGrupo.Items
                'Si el GRUPO tiene atributos de especificacion, miro si alguno es de plantilla y valido si tiene o no valor
                    If Not oItem.AtributosEspecificacion Is Nothing Then
                        For Each oAtributoEsp In oItem.AtributosEspecificacion
                            'Si el atributo de especificacion en curso, es de la plantilla, se comprueba si esta informado
                            If oAtributoEsp.Atrib = oAtributo.Atrib And iPosicion = CInt(CStr(iLineaLineas)) Then
                            
                                oAtributoEsp.valorBool = Null
                                oAtributoEsp.valorFec = Null
                                oAtributoEsp.valorNum = Null
                                oAtributoEsp.valorText = Null
                                
                                bEncontrado = True
                                
                            End If
                            
                            If bEncontrado Then
                                Exit For
                            End If
                        Next
                    End If
                    If bEncontrado Then
                        Exit For
                    End If
                    iPosicion = iPosicion + 1
                Next
            End If
            If bEncontrado Then
                Exit For
            End If
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "InicializaValorAtributo", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


''' <summary>
''' Abre la pantalla de seleccion de prove si "generar pedido" y si "proveedor vacio desde solicitud"
''' eoc muestra la pantalla de detalle de proveedor
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgLineas_BtnClick()
    Dim oGrPlant As CGrupo
    Dim k As Long
    Dim sUON1 As String
    Dim sUON2 As String
    Dim sUON3 As String
    Dim sValAtr As String
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 1

    Select Case sdbgLineas.Columns(sdbgLineas.col).Name
        Case "PROV"
            If Me.optGenerar(3).Value Or Me.optGenerar(4).Value Then
                If sdbgLineas.Columns("OLDPROVE").Value <> "" Then
                    'Muestra el detalle del proveedor:
                    MostrarDetalleProveedor (sdbgLineas.Columns("COD_PROVE").Value)
                Else
                    With frmPROVEBuscar
                        .sOrigen = "frmSOLAbrirProc"
                        .bRMat = m_bRestrMatComp
                        .chkTrasladarProve.Visible = True
                        .Show 1
                    End With
                End If
            Else
                'Muestra el detalle del proveedor:
                MostrarDetalleProveedor (sdbgLineas.Columns("COD_PROVE").Value)
            End If
            
            
        'Bot�n para elegir el Grupo Destino Proceso
        Case "GR_DESTINO_PROC"
            With frmSELGrupo
                If m_bModifProcPlantilla Then
                    .PermisoModificar = True
                Else
                    .PermisoModificar = False
                End If
                .FormOrigen = "frmSOLAbrirProc"
                    
                'El desplegable sdbcPlantillas de frmSELGrupo tiene los grupos de la plantilla seleccionada
                If Not m_oPlantillaSeleccionada Is Nothing Then
                    .sdbcGruposDestino.RemoveAll
                    'Plantilla sin grupos: Se cargue el grupo de la solicitud
                    If m_oPlantillaSeleccionada.Grupos.Count = 0 Then
                        .sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oGrupoSeleccionado.Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oGrupoSeleccionado.Den & Chr(m_lSeparador) & m_oGrupoSeleccionado.Codigo & " - " & m_oGrupoSeleccionado.Den
                    Else
                        For Each oGrPlant In m_oPlantillaSeleccionada.Grupos
                            'Id Plantilla, Cod Grupo, Id Grupo, Den Grupo, Cod-Den
                            .sdbcGruposDestino.AddItem oGrPlant.IDPlantilla & Chr(m_lSeparador) & oGrPlant.Codigo & Chr(m_lSeparador) & oGrPlant.Id & Chr(m_lSeparador) & oGrPlant.Den & Chr(m_lSeparador) & oGrPlant.Codigo & " - " & oGrPlant.Den
                        Next
                    End If
                End If
                
                'Aun sin seleccionar plantilla: Se cargue el grupo de la solicitud
                If m_oPlantillaSeleccionada Is Nothing Then
                    .sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oGrupoSeleccionado.Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oGrupoSeleccionado.Den & Chr(m_lSeparador) & m_oGrupoSeleccionado.Codigo & " - " & m_oGrupoSeleccionado.Den
                End If
                
                Dim ind As Long
                Dim vbm As Variant
                Dim FilasCombo As String
                Dim indi As Long
                Dim b_Igual As Boolean
                
                b_Igual = False
                
                For indi = 1 To m_oProceso.Grupos.Count
                    For ind = 0 To (frmSELGrupo.sdbcGruposDestino.Rows - 1)
                        vbm = .sdbcGruposDestino.GetBookmark(ind)
                        FilasCombo = .sdbcGruposDestino.Columns("COD-DEN").CellValue(vbm)
    
                        If (m_oProceso.Grupos.Item(indi).Codigo & " - " & m_oProceso.Grupos.Item(indi).Den) = FilasCombo Then
                            b_Igual = True
                            Exit For
                        End If
                    Next ind
                    If Not b_Igual Then
                        .sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oProceso.Grupos.Item(indi).Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oProceso.Grupos.Item(indi).Den & Chr(m_lSeparador) & m_oProceso.Grupos.Item(indi).Codigo & " - " & m_oProceso.Grupos.Item(indi).Den
                    End If
                    b_Igual = False
                Next indi
                
                'Grupo Nuevo
                If m_bModifProcPlantilla Then
                    .sdbcGruposDestino.AddItem "" & Chr(m_lSeparador) & m_oGrupoSeleccionado.Codigo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & m_oGrupoSeleccionado.Den & Chr(m_lSeparador) & frmSELGrupo.txtGrupoNuevo
                End If
                
                'Si la solicitud tiene mas de un grupo, se muestra la tercera opci�n
                'El primer grupo de 'Datos Generales' se obvia, se tienen en cuenta unicamente los de desglose
                If ssTabGrupos.Tabs.Count > 1 Then
                    .VisibleOpt3 = True
                Else
                    .VisibleOpt3 = False
                End If
                
                'Pesta�a (grupo de la solicitud) se est� tratando
                .GrupoSeleccionado = ssTabGrupos.selectedItem
                'Linea de donde se abrir� 'frmSELGrupo'
                .Linea = sdbgLineas.AddItemRowIndex(sdbgLineas.Bookmark)
                .Linea = frmSELGrupo.Linea + 1
                If Not m_oGrupoSeleccionado Is Nothing Then
                    If .txtLabelCod = "" Then
                        .txtLabelCod = m_oGrupoSeleccionado.Codigo
                    End If
                    
                    If .txtLabelDen = "" Then
                        .txtLabelDen = m_oGrupoSeleccionado.Den
                    End If
                End If
                
                If Not m_oPlantillaSeleccionada Is Nothing Then
                    'Caso que la plantilla no tenga grupos
                    If m_oPlantillaSeleccionada.Grupos.Count = 0 Then
                        .ComboValue = m_oGrupoSeleccionado.Codigo & " - " & m_oGrupoSeleccionado.Den
                        
                        'Mantener el valor si se ha cambiado antes
                        If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> .ComboValue Then
                            .ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                        End If
                    Else
                        If ssTabGrupos.Tabs.Count = 1 Then
                            'Si la solicitud solo tiene 1 grupo, por defecto el 1� grupo de la plantilla
                            .ComboValue = m_oPlantillaSeleccionada.Grupos.Item(1).Codigo & " - " & m_oPlantillaSeleccionada.Grupos.Item(1).Den
                        
                            'Mantener el valor si se ha cambiado antes
                            If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> .ComboValue Then
                                .ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                            End If
                        Else
                            For k = 1 To m_oPlantillaSeleccionada.Grupos.Count
                                'Se busca si algun grupo de la plantilla tiene el mismo cod o den que el de la solicitud
                                If m_oGrupoSeleccionado.Codigo = m_oPlantillaSeleccionada.Grupos.Item(k).Codigo Or m_oGrupoSeleccionado.Den = m_oPlantillaSeleccionada.Grupos.Item(k).Den Then
                                    .ComboValue = m_oPlantillaSeleccionada.Grupos.Item(k).Codigo & " - " & m_oPlantillaSeleccionada.Grupos.Item(k).Den
                                    
                                    'Mantener el valor si se ha cambiado antes
                                    If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> .ComboValue Then
                                        .ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                                    End If
    
                                    Exit For
                                Else
                                    'Sino, se asigna el de la misma posici�n
                                    If m_oGrupoSeleccionado.Id = m_oPlantillaSeleccionada.Grupos.Item(k).Id Then
                                        .ComboValue = m_oPlantillaSeleccionada.Grupos.Item(k).Codigo & " - " & m_oPlantillaSeleccionada.Grupos.Item(k).Den
                                        
                                        'Mantener el valor si se ha cambiado antes
                                        If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> .ComboValue Then
                                            .ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                                        End If
    
                                        Exit For
                                    End If
                                End If
                            Next k
                            'Caso: Solicitud tiene mas grupos que la plantilla: se asigna el �ltimo grupo de la plantilla
                            If .ComboValue = "" Then
                                .ComboValue = m_oPlantillaSeleccionada.Grupos.Item(m_oPlantillaSeleccionada.Grupos.Count).Codigo & " - " & m_oPlantillaSeleccionada.Grupos.Item(m_oPlantillaSeleccionada.Grupos.Count).Den
                            
                                'Mantener el valor si se ha cambiado antes
                                If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> .ComboValue Then
                                    .ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                                End If
                            End If
                        End If
                    End If
                Else
                    'Si no se ha elegido aun plantilla
                    .ComboValue = m_oGrupoSeleccionado.Codigo & " - " & m_oGrupoSeleccionado.Den
                
                    'Mantener el valor si se ha cambiado antes
                    If sdbgLineas.Columns("GR_DESTINO_PROC").Value <> .ComboValue Then
                        .ComboValue = sdbgLineas.Columns("GR_DESTINO_PROC").Value
                    End If
                End If
                            
                .optTodasLasLineasSolicitud(2).Visible = frmSELGrupo.VisibleOpt3
                .sdbcGruposDestino.Value = .ComboValue
                .optTrasladarUnaLinea(1).caption = Replace(.optTrasladarUnaLinea(1).caption, "xxx", .Linea)
                .optTodasLasLineasPesta�a(0).caption = Replace(.optTodasLasLineasPesta�a(0).caption, "xxx", .GrupoSeleccionado)
                                
                'No permitir volver a cambiar la estructura, una vez trasladados los items
                If Not b_Bloqueo Then .Show vbModal
            End With
            
            Dim i As Long
            Dim h As Long
            Dim m As Long
            Dim j As Long
            Dim IndexAnyadir As Long
            Dim b_CambiarGrupo As Boolean
            Dim oItemOrigen As CItem
            'Guardado de la configuraci�n inicial
            If Not m_oProcesoPlant.Grupos Is Nothing Then
                For i = 1 To m_oProcesoPlant.Grupos.Count
                    m_oProceso.Grupos.Item(i).Codigo = m_oProcesoPlant.Grupos.Item(i).Codigo
                    m_oProceso.Grupos.Item(i).Den = m_oProcesoPlant.Grupos.Item(i).Den
                Next i
            End If
  
                
     'Traslado de los items de un grupo a otro (despues de haber hecho las comprobaciones anteriores)
InicioPorCambio:
            For h = 1 To m_oProceso.Grupos.Count
                b_CambiarGrupo = False
                If Not m_oProceso.Grupos.Item(h).Items Is Nothing Then
                    For m = 1 To m_oProceso.Grupos.Item(h).Items.Count
                        If m_oProceso.Grupos.Item(h).Items.Item(m).grupoCod <> m_oProceso.Grupos.Item(h).Codigo Then
                            b_CambiarGrupo = True
                            
                            For j = 1 To m_oProceso.Grupos.Count
                                If m_oProceso.Grupos.Item(h).Items.Item(m).grupoCod = m_oProceso.Grupos.Item(j).Codigo Then
                                    IndexAnyadir = j
                                    Exit For
                                End If
                            Next j
                            
                            'Grupo nuevo
                            If j > m_oProceso.Grupos.Count Then
                                'Pesta�a nueva en ssTabGrupos con el nuevo grupo
                                ssTabGrupos.Tabs.Add , "G" & m_oProceso.Grupos.Item(h).Items.Item(m).grupoCod, m_oProceso.Grupos.Item(h).Items.Item(m).grupoCod & "-" & m_oProceso.Grupos.Item(h).Items.Item(m).GrupoDen
                                'A�ado el grupo nuevo
                                m_oProceso.Grupos.Add m_oProceso, m_oProceso.Grupos.Item(h).Items.Item(m).grupoCod, m_oProceso.Grupos.Item(h).Items.Item(m).GrupoDen, , lID:=j
                                'Items
                                Set m_oProceso.Grupos.Item(j).Items = oFSGSRaiz.Generar_CItems
                                m_bCambio = True
                                Set oItemOrigen = m_oProceso.Grupos.Item(h).Items.Item(m)
                                m_oProceso.Grupos.Item(j).Items.Add m_oProceso, oItemOrigen.Id, oItemOrigen.DestCod, oItemOrigen.UniCod, oItemOrigen.PagCod, oItemOrigen.Cantidad, oItemOrigen.FechaInicioSuministro, oItemOrigen.FechaFinSuministro, oItemOrigen.ArticuloCod, oItemOrigen.Descr, oItemOrigen.Precio, oItemOrigen.Presupuesto, , , , , , , , oItemOrigen.ProveAct, oItemOrigen.ProveActDen, oItemOrigen.grupoCod, , oItemOrigen.SolicitudId, , , , oItemOrigen.GMN1Cod, oItemOrigen.GMN2Cod, oItemOrigen.GMN3Cod, oItemOrigen.GMN4Cod, , m_oProceso.Grupos.Item(j).Id, oItemOrigen.CopiaCampoArt, , oItemOrigen.GMN4Den, oItemOrigen.IdLineaSolicit, oItemOrigen.IdCampoSolicit, oItemOrigen.UON1Cod, oItemOrigen.UON2Cod, oItemOrigen.UON3Cod
                                m_oProceso.Grupos.Item(h).Items.Remove CStr(oItemOrigen.Id)
                                GoTo InicioPorCambio
                            Else
                                If b_CambiarGrupo Then
                                    If m_oProceso.Grupos.Item(j).Items Is Nothing Then
                                        Set m_oProceso.Grupos.Item(j).Items = oFSGSRaiz.Generar_CItems
                                    End If
                                    m_bCambio = True
                                    Set oItemOrigen = m_oProceso.Grupos.Item(h).Items.Item(m)
                                    m_oProceso.Grupos.Item(j).Items.Add m_oProceso, oItemOrigen.Id, oItemOrigen.DestCod, oItemOrigen.UniCod, oItemOrigen.PagCod, oItemOrigen.Cantidad, oItemOrigen.FechaInicioSuministro, oItemOrigen.FechaFinSuministro, oItemOrigen.ArticuloCod, oItemOrigen.Descr, oItemOrigen.Precio, oItemOrigen.Presupuesto, oItemOrigen.Indice, , , , , , , oItemOrigen.ProveAct, oItemOrigen.ProveActDen, oItemOrigen.grupoCod, , oItemOrigen.SolicitudId, , , , oItemOrigen.GMN1Cod, oItemOrigen.GMN2Cod, oItemOrigen.GMN3Cod, oItemOrigen.GMN4Cod, , m_oProceso.Grupos.Item(j).Id, oItemOrigen.CopiaCampoArt, , oItemOrigen.GMN4Den, oItemOrigen.IdLineaSolicit, oItemOrigen.IdCampoSolicit
                                    m_oProceso.Grupos.Item(h).Items.Remove CStr(oItemOrigen.Id)
                                    GoTo InicioPorCambio
                                End If
                            End If
                        End If
                    Next m
                End If
            Next h

            SSTabGrupos_Click
                    
        Case "FEC_INI", "FEC_FIN"
            With frmCalendar
                Set .ctrDestination = Nothing
                Set .frmDestination = frmSOLAbrirProc
                .sOrigen = sdbgLineas.Columns(sdbgLineas.col).Name
            
                .addtotop = 900 + 360
                .addtoleft = 180
    
                If sdbgLineas.Columns(sdbgLineas.col).Value <> "" Then
                    .Calendar.Value = sdbgLineas.Columns(sdbgLineas.col).Value
                Else
                    .Calendar.Value = Date
                End If
    
                .Show 1
            End With
            
        Case Else
            With sdbgLineas
                If Not m_oAtribEspePlantI Is Nothing And Not .col < 0 Then
                    If Not m_oAtribEspePlantI.Item(CStr(.Columns(.col).Name)) Is Nothing Then
                        Set m_oAtributoI = oFSGSRaiz.Generar_CAtributo
                        Set m_oAtributoI = m_oAtribEspePlantI.Item(CStr(.Columns(.col).Name))
                        If m_oAtributoI.ListaExterna Then
                            Select Case m_oProceso.DefDistribUON
                                Case EnProceso
                                    If Not m_oProceso.DistsNivel3 Is Nothing Then
                                        sUON1 = m_oProceso.DistsNivel3.Item(1).CodUON1
                                        sUON2 = m_oProceso.DistsNivel3.Item(1).CodUON2
                                        sUON3 = m_oProceso.DistsNivel3.Item(1).CodUON3
                                    ElseIf Not m_oProceso.DistsNivel2 Is Nothing Then
                                        sUON1 = m_oProceso.DistsNivel2.Item(1).CodUON1
                                        sUON2 = m_oProceso.DistsNivel2.Item(1).CodUON2
                                        sUON3 = ""
                                    ElseIf Not m_oProceso.DistsNivel1 Is Nothing Then
                                        sUON1 = m_oProceso.DistsNivel1.Item(1).CodUON1
                                        sUON2 = ""
                                        sUON3 = ""
                                    End If
                                Case EnGrupo
                                    If Not m_oProceso.Grupos Is Nothing Then
                                        If Not m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).DistsNivel3 Is Nothing Then
                                            sUON1 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).DistsNivel3.Item(1).CodUON1
                                            sUON2 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).DistsNivel3.Item(1).CodUON2
                                            sUON3 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).DistsNivel3.Item(1).CodUON3
                                        ElseIf Not m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).DistsNivel2 Is Nothing Then
                                            sUON1 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).DistsNivel2.Item(1).CodUON1
                                            sUON2 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).DistsNivel2.Item(1).CodUON2
                                            sUON3 = ""
                                        ElseIf Not m_oProceso.Grupos.Item(1).DistsNivel1 Is Nothing Then
                                            sUON1 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).DistsNivel1.Item(1).CodUON1
                                            sUON2 = ""
                                            sUON3 = ""
                                        End If
                                    End If
                                Case EnItem
                                    sUON1 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items.Item(CStr(.Columns("LINEA").Value)).UON1Cod
                                    sUON2 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items.Item(CStr(.Columns("LINEA").Value)).UON2Cod
                                    sUON3 = m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items.Item(CStr(.Columns("LINEA").Value)).UON3Cod
                            End Select
                             
                            If sUON1 = "" Then
                                'Es obligatorio que indique previamente la distribuci�n de la compra
                                oMensajes.DistribucionObligatoria
                                Exit Sub
                            End If
                            sValAtr = MostrarFormSelAtribListaExterna(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, _
                                                            m_oAtributoI.Cod, m_oAtributoI.Den, NullToDbl0(.Columns(.col).Name), _
                                                            oUsuarioSummit.Cod, oFSGSRaiz, sUON1, sUON2, sUON3, , m_oProceso.AtributosEspecificacion, m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items.Item(CStr(.Columns("LINEA").Value)).AtributosEspecificacion, NullToDbl0(m_oSolicitudSeleccionada.Id))
                            If sValAtr <> "" Then
                                .Columns(.col).Value = sValAtr
                            End If
                        End If
                    End If
                End If
            End With
    End Select
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgLineas_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub

Private Sub sdbgLineas_Change()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 1
    
    bUpdate = True
    
    With sdbgLineas
        Select Case .Columns(.col).Name
            Case "PROVE", "NUM", "GR_DESTINO_PROC", "FEC_INI", "FEC_FIN"
            Case "SEL"
                If GridCheckToBoolean(.Columns("SEL").Value) = True Then
                    If .Columns("CANT").Value = "" Or .Columns("DESCR_ART").Value = "" Then
                        .Columns("SEL").Value = 0
                        Exit Sub
                    End If
                    
                    If Not m_bPermProcMultimaterial Then
                        If .Columns("COD_ART").Value <> "" Then
                            If .Columns("GMN1").Value <> m_sGMN1Cod Or .Columns("GMN2").Value <> m_sGMN2Cod Or _
                               .Columns("GMN3").Value <> m_sGMN3Cod Or .Columns("GMN4").Value <> m_sGMN4Cod Then
                                    .Columns("SEL").Value = 0
                                    Exit Sub
                            End If
                        Else
                            If (.Columns("GMN1").Value <> m_sGMN1Cod And .Columns("GMN1").Value <> "") Or (.Columns("GMN2").Value <> m_sGMN2Cod And .Columns("GMN2").Value <> "") Or _
                               (.Columns("GMN3").Value <> m_sGMN3Cod And .Columns("GMN3").Value <> "") Or (.Columns("GMN4").Value <> m_sGMN4Cod And .Columns("GMN4").Value <> "") Then
                                    .Columns("SEL").Value = 0
                                    Exit Sub
                            End If
                        End If
                    End If
                    If Not m_oGrupoSeleccionado.Items.Item(CStr(.Columns("LINEA").Value)) Is Nothing Then
                        m_oGrupoSeleccionado.Items.Item(CStr(.Columns("LINEA").Value)).eliminado = False
                    End If
                    
                    'edu T98
                    If Not m_oPlantillaSeleccionada Is Nothing Then
                        If m_oPlantillaSeleccionada.AdmiteMaterial(m_oGrupoSeleccionado.Items.Item(CStr(.Columns("LINEA").Value)), m_oPlantillaSeleccionada.Id) = False Then
                            .Columns("SEL").Value = "0"
                            oMensajes.ImposibleEnviarMaterialAProcesoRestriccionPorPlantilla " : " & m_oPlantillaSeleccionada.nombre
                        End If
                    End If
                Else
                    If Not m_oGrupoSeleccionado.Items.Item(CStr(.Columns("LINEA").Value)) Is Nothing Then
                        m_oGrupoSeleccionado.Items.Item(CStr(.Columns("LINEA").Value)).eliminado = True
                    End If
                End If
            Case Else
                'se esta modificando un atriburo de especificacion de la plantilla
                DoEvents
        
                Set m_oAtributoI = Nothing
                Set m_oAtributoI = oFSGSRaiz.Generar_CAtributo
                Set m_oAtributoI = m_oAtribEspePlantI.Item(CStr(sdbgLineas.Columns(.col).Name))
                iLineaLineas = .Bookmark
        End Select
    End With

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "sdbgLineas_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgLineas_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
Dim s As String
Select Case sdbgLineas.Columns("RIESGO").Value
    Case 1
        s = "RiesgoAlto"
    Case 2
        s = "RiesgoMedio"
    Case 3
        s = "RiesgoBajo"
End Select
For i = 0 To iNumCol - 1
    If Right(sdbgLineas.Columns(i).StyleSet, 8) = "amarillo" Then
        sdbgLineas.Columns(i).CellStyleSet s & "amarillo"
    Else
        sdbgLineas.Columns(i).CellStyleSet s
    End If
Next i
End Sub

''' <summary>
''' Evento que se produce al clickear el tab de grupos. Carga el grid para el grupo.
''' </summary>
''' <remarks>Llamada desde: Form_Load       sdbcPlantilla_CloseUp       sdbcPlantilla_Validate; Tiempo m�ximo: 0,2</remarks>
Private Sub SSTabGrupos_Click()
Dim sGrupo As String
Dim oItem As CItem
Dim sCadena As String
Dim sproveedor As String
Dim i As Integer
Dim oAtributoEspecificacion As CAtributo
Dim bExiste As Boolean
Dim j As Integer
Dim bEncontrado As Boolean
Dim iBmAEP As Integer
Dim oGrupoPlant As CGrupo
Dim iRiesgo As Integer
Dim oGMN4 As CGrupoMatNivel4
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 0

    sdbgLineas.Visible = False

    sdbgLineas.RemoveAll
    sdbgAtributos.RemoveAll
    
        
    'Eliminamos las columnas de los atributos de especificacion
    For i = sdbgLineas.Columns.Count - 1 To iNumCol Step -1
        sdbgLineas.Columns.Remove (i)
    Next
        
    If ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key = "" Then Exit Sub

    Set m_oGrupoSeleccionado = Nothing
    sGrupo = Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2)
    Set m_oGrupoSeleccionado = m_oProceso.Grupos.Item(CStr(sGrupo))
    
    
    If Not m_oProceso.Grupos.Item(CStr(sGrupo)).Items Is Nothing Then
    
        Dim restriccion As Boolean

        If Not m_oPlantillaSeleccionada Is Nothing Then
            restriccion = m_oPlantillaSeleccionada.ExisteRestriccionMaterial()
        Else
            restriccion = False
        End If
        For Each oItem In m_oGrupoSeleccionado.Items
            'Se insertan las columnas necesarias para los atributos de especificaci�n de los items de este grupo.
            If Not oItem.AtributosEspecificacion Is Nothing Then
                i = sdbgLineas.Columns.Count
                
                For Each oAtributoEspecificacion In oItem.AtributosEspecificacion
                    bExiste = False
                    For j = iNumCol To sdbgLineas.Columns.Count - 1
                        If sdbgLineas.Columns.Item(j).Name = oAtributoEspecificacion.Atrib Then
                            bExiste = True
                            Exit For
                        End If
                    Next
                    If Not bExiste Then
                        sdbgLineas.Columns.Add i
                        
                        sdbgLineas.Columns(i).Name = oAtributoEspecificacion.Atrib

                        sdbgLineas.Columns(i).caption = oAtributoEspecificacion.Cod
                        sdbgLineas.Columns(i).DataType = 8
                        sdbgLineas.Columns(i).FieldLen = 256
                        
                        '3275
                        If Not m_oAtribEspePlantI Is Nothing Then
                            If m_oAtribEspePlantI.ExisteAtributo(oAtributoEspecificacion.Atrib) Then
                                sdbgLineas.Columns(i).Locked = False
                                sdbgLineas.Columns(i).HeadStyleSet = "apedido"
                            Else
                                sdbgLineas.Columns(i).Locked = True
                                sdbgLineas.Columns(i).StyleSet = "amarillo"
                            End If
                        Else
                            sdbgLineas.Columns(i).Locked = True 'Comportamiento anterior a 3275
                            sdbgLineas.Columns(i).StyleSet = "amarillo" 'Comportamiento anterior a 3275
                        End If
                        
                        sdbgLineas.Columns(i).Style = ssStyleEdit
                        sdbgLineas.Columns(i).Width = 1814
                        
                        If oAtributoEspecificacion.pedido Then
                            sdbgLineas.Columns(i).HeadStyleSet = "apedido"
                        End If
                                                
                        i = i + 1
                    End If
                Next
            End If
        Next
        
        'Se insertan ahora los valores (las filas)

        iBmAEP = 0
        For Each oItem In m_oGrupoSeleccionado.Items
            iRiesgo = 0
            If NullToStr(oItem.ProveAct) = "" Then
                sproveedor = ""
            Else
                sproveedor = oItem.ProveAct & " - " & oItem.ProveActDen
            End If
                
            'quitar marca de seleccion si el material no es de la plantilla
            If oItem.eliminado Then
                sCadena = "0"
            Else
                If restriccion Then
                    If Not m_oPlantillaSeleccionada.AdmiteMaterial(oItem, m_oPlantillaSeleccionada.Id) Then
                        sCadena = "0"
                    Else
                        sCadena = "1"
                    End If
                Else
                    sCadena = "1"
                End If
            End If

            'N�mero de Linea
            sCadena = sCadena & Chr(m_lSeparador) & oItem.SolicitudId
            sCadena = sCadena & Chr(m_lSeparador) & oItem.IdLineaSolicit
            sCadena = sCadena & Chr(m_lSeparador) & oItem.Id
           
           'Grupo Destino Proceso
           If Not m_bPrimVez Then
                If Not m_oProceso.Grupos.Item(CStr(oItem.grupoCod)) Is Nothing Then
                    sCadena = sCadena & Chr(m_lSeparador) & ssTabGrupos.selectedItem
                Else
                    'Grupo Nuevo
                    sCadena = sCadena & Chr(m_lSeparador) & oItem.grupoCod & " - " & oItem.GrupoDen
                End If
           Else
            If Not m_oPlantillaSeleccionada Is Nothing Then
                Set oGrupoPlant = DevolverGrupoPlantillaParaItem(m_oGrupoSeleccionado.Codigo, m_oGrupoSeleccionado.Den, m_oGrupoSeleccionado.Id)
                sCadena = sCadena & Chr(m_lSeparador) & oGrupoPlant.Codigo & " - " & oGrupoPlant.Den
                oItem.grupoCod = oGrupoPlant.Codigo
                oItem.GrupoID = oGrupoPlant.Id
            
            Else
                'Si no se ha elegido aun plantilla, sale la que estaba de antes por defecto: la de la solicitud
                sCadena = sCadena & Chr(m_lSeparador) & m_oGrupoSeleccionado.Codigo & " - " & m_oGrupoSeleccionado.Den
                oItem.grupoCod = m_oGrupoSeleccionado.Codigo
                oItem.GrupoID = m_oGrupoSeleccionado.Id
            End If
          End If
            
            
            
            'Extraemos la famila del material.
On Error GoTo errArrayVacio
            bEncontrado = False
            For i = 0 To UBound(m_arrFamMat, 2)
                If m_arrFamMat(0, i) = oItem.GMN1Cod And m_arrFamMat(1, i) = oItem.GMN2Cod And m_arrFamMat(2, i) = oItem.GMN3Cod And m_arrFamMat(3, i) = oItem.GMN4Cod Then
                    sCadena = sCadena & Chr(m_lSeparador) & oItem.GMN1Cod & "-" & oItem.GMN2Cod & "-" & oItem.GMN3Cod & "-" & oItem.GMN4Cod & "-" & m_arrFamMat(4, i)
                     'Sacamos el riesgo
                    Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
                    oGMN4.GMN1Cod = oItem.GMN1Cod
                    oGMN4.GMN2Cod = oItem.GMN2Cod
                    oGMN4.GMN3Cod = oItem.GMN3Cod
                    oGMN4.Cod = oItem.GMN4Cod
                    iRiesgo = oGMN4.DevolverRiesgo
                    Set oGMN4 = Nothing
                    bEncontrado = True
                    Exit For
                End If
            Next
            If Not bEncontrado Then sCadena = sCadena & Chr(m_lSeparador) & ""
            
            
SinMaterial:
            sCadena = sCadena & Chr(m_lSeparador) & oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr
            sCadena = sCadena & Chr(m_lSeparador) & IIf(oItem.FechaInicioSuministro = TimeValue("00:00"), "", Format(oItem.FechaInicioSuministro, "Short Date"))
            sCadena = sCadena & Chr(m_lSeparador) & IIf(oItem.FechaFinSuministro = TimeValue("00:00"), "", Format(oItem.FechaFinSuministro, "Short Date"))
            sCadena = sCadena & Chr(m_lSeparador) & oItem.Cantidad & Chr(m_lSeparador) & oItem.Precio & Chr(m_lSeparador) & oItem.UniCod
            sCadena = sCadena & Chr(m_lSeparador) & oItem.Presupuesto & Chr(m_lSeparador) & oItem.DestCod & Chr(m_lSeparador) & oItem.PagCod
            sCadena = sCadena & Chr(m_lSeparador) & sproveedor & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oItem.ProveAct
            
            'almacena el material en la grid al que pertenece el art�culo:
            sCadena = sCadena & Chr(m_lSeparador) & oItem.GMN1Cod & Chr(m_lSeparador) & oItem.GMN2Cod & Chr(m_lSeparador) & oItem.GMN3Cod & Chr(m_lSeparador) & oItem.GMN4Cod
            'Tarea 3114: El proveedor es editable si "generar pedido" y si "proveedor vacio"
            sCadena = sCadena & Chr(m_lSeparador) & NullToStr(oItem.ProveAct) & Chr(m_lSeparador) & oItem.grupoCod
            
            '3275
            sCadena = sCadena & Chr(m_lSeparador) & iBmAEP
            sCadena = sCadena & Chr(m_lSeparador) & iRiesgo
            iBmAEP = iBmAEP + 1
            
            If Not oItem.AtributosEspecificacion Is Nothing Then

                For i = iNumCol To sdbgLineas.Columns.Count - 1
                    sCadena = sCadena & Chr(m_lSeparador) & ""
                    
                    For Each oAtributoEspecificacion In oItem.AtributosEspecificacion
                        
                        If sdbgLineas.Columns.Item(i).Name = oAtributoEspecificacion.Atrib Then
                            
                            Select Case oAtributoEspecificacion.Tipo
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                If Not IsNull(oAtributoEspecificacion.valorText) Then
                                    sCadena = sCadena & oAtributoEspecificacion.valorText

                                End If
                            Case TiposDeAtributos.TipoNumerico
                                If Not IsNull(oAtributoEspecificacion.valorNum) Then
                                    sCadena = sCadena & CStr(NullToStr(oAtributoEspecificacion.valorNum))
                                End If
                            Case TiposDeAtributos.TipoFecha
                                If Not IsNull(oAtributoEspecificacion.valorFec) Then
                                    sCadena = sCadena & IIf(oAtributoEspecificacion.valorFec = TimeValue("00:00"), "", Format(oAtributoEspecificacion.valorFec, "Short Date"))
                                End If
                            Case TiposDeAtributos.TipoBoolean
                                If Not IsNull(oAtributoEspecificacion.valorBool) Then
                                    sCadena = sCadena & IIf(oAtributoEspecificacion.valorBool = 1, "S�", "No")

                                End If
                            End Select
                            
                            Exit For
                        End If
                    Next
                Next
                
            End If
            
            sdbgLineas.AddItem sCadena

        Next
        
    End If 'Not m_oProceso.Grupos.Item(CStr(sGrupo)).Items Is Nothing
       
    sdbgLineas.Visible = True
    'Mostraremos los atributos obligatorios de la plantilla al generar ofertas, generar adjudicaciones y emitir pedido directo.  Al abrir proceso no porque se queda en apertura de proceso donde todav�a pueden modificarlos
    If m_iGenerar <> 0 Then MostrarAtributosGrupo
    
    
    Exit Sub
    
errArrayVacio:
    Select Case err.Number
        Case 9
            sCadena = sCadena & Chr(m_lSeparador) & ""
            
            ReDim Preserve m_arrFamMat(4, 0)
            m_arrFamMat(0, 0) = "##"
            m_arrFamMat(1, 0) = "##"
            m_arrFamMat(2, 0) = "##"
            m_arrFamMat(3, 0) = "##"
            m_arrFamMat(4, 0) = "##"
            
            GoTo SinMaterial
    End Select
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "SSTabGrupos_Click", err, Erl, Me, , m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Abrir Proceso
''' </summary>
''' <remarks>Llamada desde: GenerarOfertas  cmdContinuar_Click; Tiempo m�ximo: 0,2</remarks>
Private Sub AbrirProceso()
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim oMonedas As CMonedas
Dim i As Integer
Dim sGMN1 As String
Dim bHayItems As Boolean

Dim oAtributoEspecificacion As CAtributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    If Not m_oProceso Is Nothing Then

        'Si tiene y no puede abrir procesos con items no codificados le mostramos el error
        If Not basParametros.gParametrosGenerales.gbPermAbrirItemsSinArtCod Then

            If Not m_oProceso.Grupos Is Nothing Then
                For Each oGrupo In m_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If oItem.eliminado = False Then
                            'Si tiene y no puede abrir procesos con items no codificados le mostramos el error
                                If oItem.ArticuloCod = "" Then
                                    Screen.MousePointer = vbNormal
                                    oMensajes.ImposibleAbrirProcesoConItemsNoCodificados
                                    Exit Sub
                                End If
                            End If
                        Next
                    End If
                Next
            End If
        End If
    End If

    ' edu T98
    
    Dim restriccion As Boolean
    Dim iItems As Integer
    
    If Not m_oPlantillaSeleccionada Is Nothing Then
        restriccion = m_oPlantillaSeleccionada.ExisteRestriccionMaterial
    Else
        restriccion = False
    End If
    
    iItems = 0
    
    If Not m_oProceso.Grupos Is Nothing Then
        For Each oGrupo In m_oProceso.Grupos
             If Not oGrupo.Items Is Nothing Then
                 For Each oItem In oGrupo.Items
                     iItems = iItems + 1

                    'edu T98 asegurarse que no hay materiales restringidos en la plantilla del proceso destino

                     If restriccion Then
                        If m_oPlantillaSeleccionada.AdmiteMaterial(oItem, m_oPlantillaSeleccionada.Id) = False Then
                            oItem.eliminado = True
                        End If
                     End If

                      If oItem.eliminado = True Then
                        iItems = iItems - 1
                     End If
                 Next
             End If
        Next
      End If
    
     If iItems = 0 Then
        Screen.MousePointer = vbNormal
        oMensajes.SeleccioneMaterial
        Exit Sub
     End If


    ComprobacionAbrirProceso
    If Not m_oProceso Is Nothing Then
        'Ahora con multimaterial lo que tenga el combo no es material de proceso. Sera el del primer articulo seleccionado.
        sGMN1 = ""
        '
        bHayItems = False
        If Not m_oProceso.Grupos Is Nothing Then
            For Each oGrupo In m_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If oItem.eliminado = False Then
                            bHayItems = True
                            Exit For
                        End If
                    Next
                    If bHayItems = True Then
                        Exit For
                    End If
                End If
            Next
         End If
        
         If bHayItems = False Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleAbrirProcesoSinItems
            Exit Sub
        End If
        'Elimina de la colecci�n los art�culos que se hayan deseleccionado:
          If Not m_oProceso.Grupos Is Nothing Then
            For Each oGrupo In m_oProceso.Grupos
                 If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If oItem.eliminado = True Then
                            oGrupo.Items.Remove (CStr(oItem.Id))
                          Else
                            If sGMN1 = "" Then
                                sGMN1 = oItem.GMN1Cod
                            End If
                        End If
                    Next
                End If
            Next
        Else
            sGMN1 = Trim(sdbcMat.Columns("GMN1").Value)
        End If
        
        If Not gParametrosGenerales.gbPMCargarTodosGrupos Then
            'Elimina de la colecci�n los grupos sin art�culos seleccionados y que no vengan de plantilla. Si el grupo viene de plantilla no se elimina
            If Not m_oProceso.Grupos Is Nothing Then
                For i = m_oProceso.Grupos.Count To 1 Step -1
                    Set oGrupo = m_oProceso.Grupos.Item(i)
                    
                    If NullToDbl0(oGrupo.IDPlantilla) = 0 Then
                        If Not oGrupo.Items Is Nothing Then
                            If oGrupo.Items.Count = 0 Then
                                m_oProceso.Grupos.Remove (CStr(oGrupo.Codigo))
                            End If
                        Else
                            m_oProceso.Grupos.Remove (CStr(oGrupo.Codigo))
                        End If
                    End If
                Next
            End If
        End If
        
        'Configura los datos del proceso:
        m_oProceso.Anyo = Year(Now)
        m_oProceso.Den = Mid(Trim(txtProceso.Text), 1, 100) 'Es un texto corto
         m_oProceso.FechaApertura = Now
         m_oProceso.UsuAper = basOptimizacion.gvarCodUsuario
         If Not m_bPermProcMultimaterial Then
            m_oProceso.GMN1Cod = Trim(sdbcMat.Columns("GMN1").Value)
         Else
            m_oProceso.GMN1Cod = sGMN1
        End If
        
        'm_oProceso.GMN2Cod = Trim(sdbcMat.Columns("GMN2").Value)
        'm_oProceso.GMN3Cod = Trim(sdbcMat.Columns("GMN3").Value)
        'm_oProceso.GMN4Cod = Trim(sdbcMat.Columns("GMN4").Value)
        m_oProceso.PermitirAdjDirecta = gParametrosInstalacion.gbSolicProcAdjDir
        'm_oProceso.DefDistribUON = EnProceso
        
        'Responsable del proceso:si el usuario que genera el proceso es comprador se asigna como responsable.Si no el comprador asignado a la solicitud:
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
            Set m_oProceso.responsable = oFSGSRaiz.generar_CComprador
            m_oProceso.responsable.Cod = basOptimizacion.gCodPersonaUsuario
            m_oProceso.responsable.codEqp = basOptimizacion.gCodEqpUsuario
        ElseIf Not m_oSolicitudSeleccionada.comprador Is Nothing Then
            Set m_oProceso.responsable = oFSGSRaiz.generar_CComprador
            m_oProceso.responsable.Cod = m_oSolicitudSeleccionada.comprador.Cod
            m_oProceso.responsable.codEqp = m_oSolicitudSeleccionada.comprador.codEqp
        End If
        
        'Solicitud:si hay items estar� a nivel de item.si no a nivel de proceso
        If Not m_oProceso.Grupos Is Nothing Then
            For Each oGrupo In m_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        m_oProceso.DefSolicitud = EnItem
                    Next
                End If
            Next
        End If
        
        'Actualizamos si hay AtributosEspecificacion
        If Not m_oProceso.AtributosEspecificacion Is Nothing Then
            For Each oAtributoEspecificacion In m_oProceso.AtributosEspecificacion
                oAtributoEspecificacion.AnyoProce = m_oProceso.Anyo
                oAtributoEspecificacion.GMN1Proce = m_oProceso.GMN1Cod
                oAtributoEspecificacion.Cod = m_oProceso.Cod
            Next
        End If
        
        'Actualizamos si hay AtributosEspecificacion
        If Not m_oProceso.Grupos Is Nothing Then
            For Each oGrupo In m_oProceso.Grupos
                If Not oGrupo.AtributosEspecificacion Is Nothing Then
                    For Each oAtributoEspecificacion In oGrupo.AtributosEspecificacion
                        oAtributoEspecificacion.AnyoProce = m_oProceso.Anyo
                        oAtributoEspecificacion.GMN1Proce = m_oProceso.GMN1Cod
                        oAtributoEspecificacion.Cod = m_oProceso.Cod
                    Next
                End If
            Next
        End If
        
        If m_oProceso.DefSolicitud = NoDefinido Then
            m_oProceso.SolicitudId = m_oSolicitudSeleccionada.Id
            m_oProceso.DefSolicitud = EnProceso
        End If
        
        'si no se ha seleccionado una moneda por defecto ponemos la moneda central con su cambio.
        m_oProceso.MonCod = m_oSolicitudSeleccionada.Moneda
        m_oProceso.Cambio = m_oSolicitudSeleccionada.Cambio
        If IsNull(m_oProceso.MonCod) Or m_oProceso.MonCod = "" Then
            Set oMonedas = oFSGSRaiz.Generar_CMonedas
            oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
            If oMonedas.Count > 0 Then
                m_oProceso.MonCod = basParametros.gParametrosGenerales.gsMONCEN
                m_oProceso.Cambio = oMonedas.Item(1).Equiv
            End If
        End If
        
        
        'Se comprueba si falta alg�n dato necesario y se muestra la pantalla que los pide:
        Set frmSOLAbrirFaltan.g_oSolicitudSeleccionada = m_oSolicitudSeleccionada
        Set frmSOLAbrirFaltan.g_oProceso = m_oProceso
        If chkAdjuntos.Value = 1 Then
            frmSOLAbrirFaltan.g_bTraspasarAdjuntos = True
        Else
            frmSOLAbrirFaltan.g_bTraspasarAdjuntos = False
        End If
                
        ' Pasar la plantilla
        
        If Not m_oPlantillaSeleccionada Is Nothing Then
            Set frmSOLAbrirFaltan.g_oPlantillaSeleccionada = m_oPlantillaSeleccionada
            frmSOLAbrirFaltan.g_bGruposPlantillaCoinciden = True
        Else
            Set frmSOLAbrirFaltan.g_oPlantillaSeleccionada = Nothing
            frmSOLAbrirFaltan.g_bGruposPlantillaCoinciden = False
        End If
        
        frmSOLAbrirFaltan.g_bGenerarPedido_EstasGenerando = (optGenerar.Item(3).Value = True Or optGenerar.Item(4).Value = True)
        If (optGenerar.Item(3).Value Or optGenerar.Item(4).Value) And Not sdbgLineas.Columns("PROV").Visible Then
            frmSOLAbrirFaltan.g_bGenerarPedido_VerProve = True
        Else
            frmSOLAbrirFaltan.g_bGenerarPedido_VerProve = False
        End If
        
        'Multi solicitud
        frmSOLAbrirFaltan.g_arSolicitudes = g_arSolicitudes
        
        If optGenerar.Item(0).Value = True Then
            frmSOLAbrirFaltan.g_bSoloAbrirProc = True
            Unload Me
        Else
            frmSOLAbrirFaltan.g_bSoloAbrirProc = False
            Me.Hide
        End If
        Screen.MousePointer = vbNormal
        
        frmSOLAbrirFaltan.Show vbModal
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "AbrirProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Mostrar Detalle Proveedor
''' </summary>
''' <param name="CodProve">Proveedor</param>
''' <remarks>Llamada desde: sdbgLineas_BtnClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub MostrarDetalleProveedor(ByVal CodProve As String)
Dim oProves As CProveedores
Dim oProve As CProveedor
Dim oCon As CContacto
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Trim(CodProve) = "" Then Exit Sub
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    
   oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado m_sIdiProve
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    
    Set frmProveDetalle.g_oProveSeleccionado = oProve
    frmProveDetalle.caption = CodProve & "  " & oProve.Den
    frmProveDetalle.lblCodPortProve = NullToStr(oProve.CodPortal)
    frmProveDetalle.g_bPremium = oProve.EsPremium
    frmProveDetalle.g_bActivo = oProve.EsPremiumActivo
    frmProveDetalle.lblDir = NullToStr(oProve.Direccion)
    frmProveDetalle.lblCp = NullToStr(oProve.cP)
    frmProveDetalle.lblPob = NullToStr(oProve.Poblacion)
    frmProveDetalle.lblPaiCod = NullToStr(oProve.CodPais)
    frmProveDetalle.lblPaiDen = NullToStr(oProve.DenPais)
    frmProveDetalle.lblMonCod = NullToStr(oProve.CodMon)
    frmProveDetalle.lblMonDen = NullToStr(oProve.DenMon)
    frmProveDetalle.lblProviCod = NullToStr(oProve.CodProvi)
    frmProveDetalle.lblProviDen = NullToStr(oProve.DenProvi)
    frmProveDetalle.lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    frmProveDetalle.lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    frmProveDetalle.lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then
        frmProveDetalle.lblCal1Den = oProve.Calif1
    End If
    
    If Trim(oProve.Calif2) <> "" Then
        frmProveDetalle.lblcal2den = oProve.Calif2
    End If
    
    If Trim(oProve.Calif3) <> "" Then
        frmProveDetalle.lblcal3den = oProve.Calif3
    End If
    
    frmProveDetalle.lblCal1Val = DblToStr(oProve.Val1)
    frmProveDetalle.lblCal2Val = DblToStr(oProve.Val2)
    frmProveDetalle.lblCal3Val = DblToStr(oProve.Val3)

    frmProveDetalle.lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        
        frmProveDetalle.sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    
    Next
        
    Set oProves = Nothing
    Set oProve = Nothing
    
    
    frmProveDetalle.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "MostrarDetalleProveedor", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub


Private Sub ComprobarArticuloDelMat()
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProceso Is Nothing Then Exit Sub
    If m_oProceso.Grupos Is Nothing Then Exit Sub

    For Each oGrupo In m_oProceso.Grupos
        If Not oGrupo.Items Is Nothing Then
            For Each oItem In oGrupo.Items
                If Not oItem.eliminado Then
                    If Not IsNull(oItem.ArticuloCod) And Not IsEmpty(oItem.ArticuloCod) Then
                        If oItem.GMN1Cod <> m_sGMN1Cod Or oItem.GMN2Cod <> m_sGMN2Cod Or oItem.GMN3Cod <> m_sGMN3Cod Or oItem.GMN4Cod <> m_sGMN4Cod Then
                            oItem.eliminado = True
                        Else
                            oItem.eliminado = False
                        End If
                    Else
                        If (oItem.GMN1Cod <> m_sGMN1Cod And oItem.GMN1Cod <> "") Or (oItem.GMN2Cod <> m_sGMN2Cod And oItem.GMN2Cod <> "") Or (oItem.GMN3Cod <> m_sGMN3Cod And oItem.GMN3Cod <> "") Or (oItem.GMN4Cod <> m_sGMN4Cod And oItem.GMN4Cod <> "") Then
                            oItem.eliminado = True
                        Else
                            oItem.eliminado = False
                        End If
                    End If
                End If
            Next
            
            If oGrupo.Codigo = m_oGrupoSeleccionado.Codigo Then
                sdbgLineas.MoveFirst
                For i = 0 To sdbgLineas.Rows - 1
                    If GridCheckToBoolean(sdbgLineas.Columns("SEL").Value) = True Then
                        If oGrupo.Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)).eliminado = True Then
                            sdbgLineas.Columns("SEL").Value = "0"
                        End If
                    Else
                        If oGrupo.Items.Item(CStr(sdbgLineas.Columns("LINEA").Value)).eliminado = False Then
                            sdbgLineas.Columns("SEL").Value = "1"
                        End If
                    End If
                    sdbgLineas.MoveNext
                Next i
                sdbgLineas.MoveFirst
            End If
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ComprobarArticuloDelMat", err, Erl, , m_bActivado)
      Exit Sub
   End If


End Sub

''' <summary>Genera las ofertas de un proceso</summary>
''' <remarks>Llamada desde: cmdContinuar_Click ; GenerarAdjudicaciones; Tiempo m�ximo: 0,4</remarks>
''' <revision>LTG 22/05/2012</revision>

Private Sub GenerarOfertas()
    Dim teserror As TipoErrorSummit
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim bHayProveedores As Boolean
    Dim oIasig As IAsignaciones
    Dim oAtribsEspec As CAtributosEspecificacion
    Dim sCadena As String
    Dim sCadenaTmp As String
    Dim oAtribEsp As CAtributoEspecificacion
    Dim ofrmPROCE As frmPROCE
    Dim oIOfertas As iOfertas
    Dim NombreUsuario As String
    Dim bProveFrmSol_EnPantalla As Boolean

    '1- En primer lugar abre el proceso:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AbrirProceso
    
    If m_oProceso Is Nothing Then Exit Sub
    'Si ha habido alg�n error al abrir el proceso no contin�a:
    If m_oProceso.Estado <= 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    'comprueba si hay valores para los proveedores:
    If m_oProceso.DefProveActual = NoDefinido Then
        bHayProveedores = False
    Else
        For Each oGrupo In m_oSolicitudSeleccionada.Grupos
            For Each oCampo In oGrupo.CAMPOS
                If oCampo.CampoGS = TipoCampoGS.Proveedor Then
                    If Not IsNull(oCampo.valorText) Then
                        bHayProveedores = True
                        Exit For
                    End If
                ElseIf ((oCampo.CampoGS = TipoCampoGS.Desglose) Or (IsNull(oCampo.CampoGS) And (oCampo.Tipo = TipoDesglose))) Then
                    bHayProveedores = oCampo.HayProveedorEnDesglose
                End If
                If bHayProveedores = True Then Exit For
            Next
            If bHayProveedores = True Then Exit For
        Next
    End If
    
    bProveFrmSol_EnPantalla = (Me.optGenerar(3).Value Or Me.optGenerar(4).Value)
    
    'Si se esta generando con plantilla validamos los atributos obligatorios
    If Not IsNull(m_oProceso.Plantilla) Then
    
        Set oAtribsEspec = m_oProceso.ValidarAtributosEspObligatorios(TValidacionAtrib.Apertura)
        If Not oAtribsEspec.Count = 0 Then
            Screen.MousePointer = vbNormal
            sCadena = ""
            sCadenaTmp = ""
            For Each oAtribEsp In oAtribsEspec
                sCadenaTmp = oAtribEsp.Den
                Select Case oAtribEsp.ambito
                    Case TipoAmbitoProceso.AmbProceso
                        sCadenaTmp = sCadenaTmp & " " & m_sProceso & vbCrLf
                    Case TipoAmbitoProceso.AmbGrupo
                        sCadenaTmp = sCadenaTmp & " " & m_sGrupo & vbCrLf
                    Case TipoAmbitoProceso.AmbItem
                        sCadenaTmp = sCadenaTmp & " " & m_sItem & vbCrLf
                End Select
                
                If InStr(1, sCadena, sCadenaTmp) = 0 Then
                    sCadena = sCadena & sCadenaTmp
                End If
            Next
            oMensajes.AtributosEspObl sCadena, TValidacionAtrib.Apertura
            
            Set ofrmPROCE = New frmPROCE
            ofrmPROCE.sdbcAnyo = m_oProceso.Anyo
            ofrmPROCE.sdbcGMN1_4Cod = m_oProceso.GMN1Cod
            ofrmPROCE.sdbcProceCod = m_oProceso.Cod
            ofrmPROCE.sdbcProceCod_Validate False
            ofrmPROCE.Show
            Exit Sub
        End If
        
        '3317
        'Validar atributos obligatorios con validaci�n en la adjudicaci�n
        
        Dim oAtribEspec As CAtributoEspecificacion
        Dim sAtributosObligatorios As String
        
        Set oAtribsEspec = m_oProceso.ValidarAtributosEspObligatorios(TValidacionAtrib.TVAdjudicacion)
        If oAtribsEspec.Count > 0 Then
            For Each oAtribEsp In oAtribsEspec
                sAtributosObligatorios = sAtributosObligatorios & oAtribEspec.Den & vbCrLf
            Next
        End If
        Set oAtribsEspec = Nothing
        Set oAtribEspec = Nothing
        If sAtributosObligatorios <> "" Then
            Screen.MousePointer = vbNormal
            Call oMensajes.AtributosEspObl(sAtributosObligatorios, TVAdjudicacion)
            Exit Sub
        End If

    End If
    
    
    '2 -Se valida el proceso.Si en la solicitud no hay proveedores,se generan los proveedores potenciales para el proceso y se llama a la pantalla de selecci�n
    'de proveedores (si tiene permisos de consulta).Si hay proveedores se asignan al proceso:
    If bProveFrmSol_EnPantalla = True Then
        teserror = m_oProceso.Validar(basOptimizacion.gvarCodUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , m_oSolicitudSeleccionada.Id, True)
    ElseIf bHayProveedores = True Then
        teserror = m_oProceso.Validar(basOptimizacion.gvarCodUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , m_oSolicitudSeleccionada.Id)
    Else
        teserror = m_oProceso.Validar(basOptimizacion.gvarCodUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    End If
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If

    If Not bHayProveedores And Not bProveFrmSol_EnPantalla Then
        Screen.MousePointer = vbNormal
        oMensajes.SolicitudSinProveedores
        If m_bVerSelecProve = True Then  'Muestra la pantalla se selecci�n de proveedores
            frmSELPROVE.sdbcAnyo = m_oProceso.Anyo
            frmSELPROVE.sdbcGMN1_4Cod = m_oProceso.GMN1Cod
            frmSELPROVE.GMN1Seleccionado
            frmSELPROVE.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
            frmSELPROVE.sdbcProceCod = m_oProceso.Cod
            frmSELPROVE.Show
            DoEvents
            frmSELPROVE.sdbcProceCod_Validate False
            DoEvents
        End If
        Unload Me
        Exit Sub
    End If
    
    '3 -Validamos la selecci�n de proveedores:
    Set oIasig = m_oProceso
    teserror = oIasig.Validar(basOptimizacion.gvarCodUsuario)
    Set oIasig = Nothing
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    '4 -Generamos las ofertas:
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Administrador Then
        NombreUsuario = ""
    Else
        NombreUsuario = oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
    End If
    
    teserror = m_oProceso.GenerarOfertasDesdeSolicitud(m_oSolicitudSeleccionada.Id, basOptimizacion.gvarCodUsuario _
    , SQLBinaryToBoolean(chkAtrib.Value), NombreUsuario, bProveFrmSol_EnPantalla)

    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    Screen.MousePointer = vbNormal
    
    'Si se estaban generando solo ofertas lanza el mensaje y cierra la pantalla
    If optGenerar.Item(1).Value = True Then
        If m_bVerRecOfe = True Then
            frmMsgboxOptima.sOrigen = "frmSOLAbrirProc"
            frmMsgboxOptima.sMensaje = "OFERTAS"
            frmMsgboxOptima.Show vbModal
            If g_bIrA = True Then
                Screen.MousePointer = vbHourglass
                frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
                frmOFERec.sdbcAnyo = m_oProceso.Anyo
                frmOFERec.sdbcGMN1_4Cod = m_oProceso.GMN1Cod
                frmOFERec.GMN1Seleccionado
                frmOFERec.sdbcProceCod = m_oProceso.Cod
                frmOFERec.sdbcProceCod_Validate False
                DoEvents
                Screen.MousePointer = vbNormal
            End If
        Else
            Screen.MousePointer = vbNormal
            oMensajes.OfertasGeneradasDeSolicitud
        End If
        Unload Me
    Else
        Set oIOfertas = m_oProceso
        oIOfertas.DevolverUltimasOfertasDelProceso basPublic.gParametrosInstalacion.gIdioma
        Set oIOfertas = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Screen.MousePointer = vbNormal
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "GenerarOfertas", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub

Private Sub GenerarAdjudicaciones()
Dim teserror As TipoErrorSummit

    'abre el proceso y genera las ofertas:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    GenerarOfertas
    
    If m_oProceso Is Nothing Then Exit Sub
    If m_oProceso.Estado < TipoEstadoProceso.conofertas Then Exit Sub
    
    'Ahora genera las adjudicaciones:
    Screen.MousePointer = vbHourglass
    
    teserror = m_oProceso.GenerarAdjudicacionesDesdeSolicitud
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    Screen.MousePointer = vbNormal
    
    
    'Si se estaban generando solo adjudicaciones lanza el mensaje y cierra la pantalla
    If optGenerar.Item(2).Value = True Then
        If m_bVerComparativa = True Then
            frmMsgboxOptima.sOrigen = "frmSOLAbrirProc"
            frmMsgboxOptima.sMensaje = "ADJUDICACION"
            frmMsgboxOptima.Show vbModal
            If g_bIrA = True Then
                Screen.MousePointer = vbHourglass
                frmADJ.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
                frmADJ.sdbcAnyo = m_oProceso.Anyo
                frmADJ.sdbcGMN1_4Cod = m_oProceso.GMN1Cod
                frmADJ.GMN1Seleccionado
                frmADJ.sdbcProceCod = m_oProceso.Cod
                DoEvents
                Unload Me
                frmADJ.sdbcProceCod_Validate False
                DoEvents
                frmADJ.SetFocus
                Screen.MousePointer = vbNormal
            Else
                Unload Me
            End If
        Else
            oMensajes.AdjudicacionesGeneradasDeSolicitud
            Unload Me
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "GenerarAdjudicaciones", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub

''' <summary>Genera las adjudicaciones y cierra el proceso</summary>
''' <remarks>Llamada desde: cmdContinuar_Click</remarks>
''' <returns>Booleano indicando si no se producido ning�n error</returns>

Private Function AdjudicarYCerrarProceso() As Boolean
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim sProceso As String
    Dim dPresAbierto As Double
    Dim oAdjs As CAdjsGrupo
    Dim oAdj As CAdjGrupo
    Dim dblAdjudicado As Double
    Dim dblConsumido As Double
    Dim bBloqueoEtapa As Boolean
    Dim bBloqueoCondiciones As Boolean
    Dim oAdjud As CAdjudicacion
    Dim SolicitudId As Variant
    Dim ListaAvisos As String
    Dim oIAdj As IAdjudicaciones
    Dim teserror As TipoErrorSummit
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AdjudicarYCerrarProceso = False
    
    b_ErrorPosicionarTabPedidos = False
    
    If Not m_oProceso Is Nothing Then
        'Si tiene y no puede abrir procesos con items no codificados le mostramos el error
        If Not basParametros.gParametrosGenerales.gbPermAbrirItemsSinArtCod Then
            If Not m_oProceso.Grupos Is Nothing Then
                For Each oGrupo In m_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If oItem.eliminado = False Then
                            'Si tiene y no puede abrir procesos con items no codificados le mostramos el error
                                If oItem.ArticuloCod = "" Then
                                    Screen.MousePointer = vbNormal
                                    oMensajes.ImposibleAbrirProcesoConItemsNoCodificados
                                    Unload Me
                                    Exit Function
                                End If
                            End If
                        Next
                        
                        Set oItem = Nothing
                    End If
                Next
                
                Set oGrupo = Nothing
            End If
        End If
    End If

    'abre el proceso y genera las ofertas:
    GenerarAdjudicaciones
    
    'comprueba que se hayan generado correctamente las adjudicaciones:
    If m_oProceso Is Nothing Then
        Screen.MousePointer = vbNormal
        Unload Me
        Exit Function
    End If
    If m_oProceso.Estado <> TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado And m_oProceso.Estado <> TipoEstadoProceso.PreadjYConObjNotificados Then
        Screen.MousePointer = vbNormal
        Unload Me
        Exit Function
    End If
    
    'Si el proceso es de reuni�n o supera el volumen m�ximo de adj.directa saca un mensaje y no contin�a:
    If m_oProceso.PermitirAdjDirecta = False Then
        sProceso = m_oProceso.Anyo & "/" & m_oProceso.GMN1Cod & "/" & m_oProceso.Cod
        Screen.MousePointer = vbNormal
        oMensajes.ImposibleCerrarProcDesdeSolicit sProceso, basParametros.gParametrosGenerales.gsMONCEN
        Unload Me
        Exit Function
    Else
        dPresAbierto = m_oProceso.DevolverPresupuestoAbierto
        If Not IsNull(dPresAbierto) And NullToDbl0(gParametrosGenerales.gvImporteSolicProcAdjDir) > 0 Then
            If CDbl(dPresAbierto) > gParametrosGenerales.gvImporteSolicProcAdjDir * CDbl(m_oProceso.Cambio) Then
                sProceso = m_oProceso.Anyo & "/" & m_oProceso.GMN1Cod & "/" & m_oProceso.Cod
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleCerrarProcDesdeSolicit sProceso, gParametrosGenerales.gvImporteSolicProcAdjDir
                Unload Me
                Exit Function
            End If
        End If
    End If
    
    'Ahora cierra el proceso y muestra la pantalla de generar pedido directo
    Screen.MousePointer = vbHourglass
        
    Set oAdjs = oFSGSRaiz.Generar_CAdjsGrupo
    oAdjs.CargarAdjudicaciones m_oProceso, , basPublic.gParametrosInstalacion.gIdioma
    For Each oAdj In oAdjs
        dblAdjudicado = dblAdjudicado + oAdj.Adjudicado
        dblConsumido = dblConsumido + oAdj.Consumido
        oAdj.ConsumidoReal = oAdj.Consumido
        oAdj.AdjudicadoReal = oAdj.Adjudicado
        oAdj.AbiertoOfe = oAdj.Abierto
        If Not m_oProceso.Ofertas.Item(oAdj.Prove) Is Nothing Then
            m_oProceso.Ofertas.Item(oAdj.Prove).AdjudicadoSinProc = m_oProceso.Ofertas.Item(oAdj.Prove).AdjudicadoSinProc + oAdj.Adjudicado
        End If
    Next
    
    m_oProceso.CargarGrupos
    If Not m_oProceso.Grupos Is Nothing Then
        For Each oGrupo In m_oProceso.Grupos
            For Each oAdj In oAdjs
                If oAdj.Grupo = oGrupo.Codigo Then
                    oGrupo.AdjudicadoTotal = oGrupo.AdjudicadoTotal + NullToDbl0(oAdj.Adjudicado)
                End If
            Next
        Next
    End If
    
    '1� preadjudica:
    Dim oAdjudicaciones As CAdjudicaciones
    Set oAdjudicaciones = oFSGSRaiz.Generar_CAdjudicaciones
    oAdjudicaciones.CargarAdjudicaciones m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If gParametrosGenerales.gbSolicitudesCompras Then
        bBloqueoEtapa = False
        bBloqueoCondiciones = False
        Dim b As Boolean
        m_oProceso.CargarTodosLosItemsGrupos bActivoSM:=(Not g_oParametrosSM Is Nothing)
        For Each oAdjud In oAdjudicaciones
            If Not m_oProceso.Grupos Is Nothing Then
                b = False
                For Each oGrupo In m_oProceso.Grupos
                    If Not oGrupo.Items.Item(CStr(oAdjud.Id)) Is Nothing Then
                        If Not b Then
                            SolicitudId = oGrupo.Items.Item(CStr(oAdjud.Id)).SolicitudId
                            oGrupo.Items.Item(CStr(oAdjud.Id)).Consumido = oGrupo.Items.Item(CStr(oAdjud.Id)).Presupuesto
                        End If
                        oAdjud.Partida = oGrupo.Items.Item(CStr(oAdjud.Id)).PartidaPres
                        oAdjud.AnyoPartida = oGrupo.Items.Item(CStr(oAdjud.Id)).AnyoPartida
                        b = True
                    End If
                Next
            End If

            'Se comprueban las condiciones de bloqueo por etapa
            If Not bBloqueoEtapa And Not NoHayParametro(SolicitudId) Then
                bBloqueoEtapa = BloqueoEtapa(SolicitudId)
                If bBloqueoEtapa Then
                    oMensajes.MensajeOKOnly 903, Exclamation  'La solicitud se encuentra en una etapa en la que no se permiten adjudicaciones
                    Exit For
                Else
                    'Se comprueban las condiciones de bloqueo por f�rmula
                    bBloqueoCondiciones = BloqueoCondiciones(SolicitudId, ListaAvisos, oAdjudicaciones) 'Or bBloqueoCondiciones
                    If bBloqueoCondiciones Then
                        Exit For
                    End If
                End If
            End If
        Next
        Screen.MousePointer = vbHourglass
        Set oAdjud = Nothing

        If bBloqueoEtapa Or bBloqueoCondiciones Then
            Screen.MousePointer = vbNormal
            Unload Me
            Exit Function
        End If
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Set oIAdj = m_oProceso
    teserror = oIAdj.RealizarAdjudicaciones(oAdjudicaciones, , True, oAdjs, True, dblConsumido, dblAdjudicado, , oUsuarioSummit)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Unload Me
        Exit Function
    End If
    
    'Ahora cierra:
    teserror = m_oProceso.ValidacionYCierre(basOptimizacion.gvarCodUsuario, oAdjs, dblAdjudicado)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Unload Me
        Exit Function
    Else
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
        End If
    End If
    
    AdjudicarYCerrarProceso = True
    
    Set oAdjs = Nothing
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "AdjudicarYCerrarProceso", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

''' <summary>
''' Genera un pedido directo de la solictud. Solo se debe dar los diferentes mensajes de aviso de bloqueo por
''' f�rmula una vez para todo el proceso
''' </summary>
''' <remarks>Llamada desde: cmdContinuar_Click ; Tiempo m�ximo: 0,4</remarks>
Private Sub GenerarPedidosDirectos()
    Dim i As Integer
    Dim oProcesoEliminar As cProceso
    Dim m_oIBaseDatosEdicionEnPRoceso As IBaseDatos
    Dim j As Integer
    Dim TESErrorELiminarProceso As TipoErrorSummit
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Set oProcesoEliminar = m_oProceso
    'Hay SM
    If Not g_oParametrosSM Is Nothing And m_oEmpresas.Count > 0 Then
        If m_oEmpresas.Count = 1 Then   'Solo una empresa que viene cargada de los campos de SM de la solicitud
            Unload frmPEDIDOS
            frmPEDIDOS.m_bProcededeSolicitud = True
            If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then frmPEDIDOS.g_bSolicitudAbono = True
            frmPEDIDOS.g_lIdEmpresa = m_oEmpresas.Item(1).Id
            frmPEDIDOS.sdbcAnyo = m_oProceso.Anyo
            frmPEDIDOS.sdbcGMN1_4Cod = m_oProceso.GMN1Cod
            frmPEDIDOS.GMN1Seleccionado
            frmPEDIDOS.m_bRespetarCombo = True
            frmPEDIDOS.sdbcProceCod = m_oProceso.Cod
                        
            frmPEDIDOS.m_bRespetarCombo = False
            DoEvents
            
            frmPEDIDOS.sdbcProceCod_Validate False
            
            'Paso a la pesta�a todos para que cargue todos los �tems, sino s�lo se emitir�an los del primer grupo
            If frmPEDIDOS.ssTabGrupos.Tabs.Count > 1 Then
                frmPEDIDOS.ssTabGrupos.Tabs(frmPEDIDOS.ssTabGrupos.Tabs.Count).Selected = True
                frmPEDIDOS.SSTabGrupos_Click
            End If
            'Ahora chequea todos los art�culos.
            For i = 0 To frmPEDIDOS.sdbgPedidos.Rows - 1
                frmPEDIDOS.sdbgPedidos.Columns(0).Value = 1
                frmPEDIDOS.sdbgPedidos.col = 0
                frmPEDIDOS.sdbgPedidos_Change
                frmPEDIDOS.sdbgPedidos.MoveNext
            Next i
            
            DoEvents
            If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then
                For j = 1 To frmPEDIDOS.g_oOrdenesTemporales.Count
                    frmPEDIDOS.g_oOrdenesTemporales.Item(j).Abono = True
                Next j
            End If
            If frmPEDIDOS.g_oOrdenesTemporales.Count > 0 Then
                If frmPEDIDOS.g_TipoEmision = PedidoEstandar And frmPEDIDOS.m_oTipoPedido Is Nothing Then
                    frmPEDIDOS.sstabPedidos.Tab = 0
                    frmPEDIDOS.chkPedirTodo.Value = 0
                Else
                    frmPEDIDOS.sstabPedidos.Tab = 1
                End If
            End If
            
            frmPEDIDOS.txtRetenGarantia.Text = m_oProceso.RetencionEnGarantia
            frmPEDIDOS.txtRetenGarantia_LostFocus
            
            If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then
                oProcesoEliminar.Reabrir (oUsuarioSummit.Cod)
                Set m_oIBaseDatosEdicionEnPRoceso = oProcesoEliminar
                TESErrorELiminarProceso = m_oIBaseDatosEdicionEnPRoceso.EliminarDeBaseDatos
                If b_ErrorPosicionarTabPedidos Then
                    Screen.MousePointer = vbNormal
                    Unload Me
                    Exit Sub
                End If
                frmPEDIDOS.sstabPedidos.TabEnabled(0) = False
                frmPEDIDOS.sstabPedidos.Tab = 1
            End If
            Screen.MousePointer = vbNormal
            frmPEDIDOS.SetFocus
            Unload Me
        Else    'Mas de una empresa que vienen cargadas de los campos de SM de la solicitud
            ReDim basPublic.ofrmPedidos(1 To m_oEmpresas.Count)
            
            For i = 1 To m_oEmpresas.Count
                Set basPublic.ofrmPedidos(i) = New frmPEDIDOS
                If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then basPublic.ofrmPedidos(i).g_bSolicitudAbono = True
                basPublic.ofrmPedidos(i).g_lIdEmpresa = m_oEmpresas.Item(i).Id
                basPublic.ofrmPedidos(i).sdbcAnyo = m_oProceso.Anyo
                basPublic.ofrmPedidos(i).sdbcGMN1_4Cod = m_oProceso.GMN1Cod
                basPublic.ofrmPedidos(i).GMN1Seleccionado
                basPublic.ofrmPedidos(i).m_bRespetarCombo = True
                basPublic.ofrmPedidos(i).sdbcProceCod = m_oProceso.Cod
                basPublic.ofrmPedidos(i).m_bRespetarCombo = False
                basPublic.ofrmPedidos(i).m_bProcededeSolicitud = True
                                
                DoEvents
                basPublic.ofrmPedidos(i).sdbcProceCod_Validate False
                
                'Paso a la pesta�a todos para que cargue todos los �tems, sino s�lo se emitir�an los del primer grupo
                If basPublic.ofrmPedidos(i).ssTabGrupos.Tabs.Count > 1 Then
                    basPublic.ofrmPedidos(i).ssTabGrupos.Tabs(basPublic.ofrmPedidos(i).ssTabGrupos.Tabs.Count).Selected = True
                    basPublic.ofrmPedidos(i).SSTabGrupos_Click
                End If
                
                'Ahora chequea todos los art�culos:
                Dim k As Integer
                For k = 0 To basPublic.ofrmPedidos(i).sdbgPedidos.Rows - 1
                    basPublic.ofrmPedidos(i).sdbgPedidos.Columns(0).Value = 1
                    basPublic.ofrmPedidos(i).sdbgPedidos.col = 0
                    basPublic.ofrmPedidos(i).sdbgPedidos_Change
                    basPublic.ofrmPedidos(i).sdbgPedidos.MoveNext
                Next k
                
                DoEvents
                If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then
                    For j = 1 To basPublic.ofrmPedidos(i).g_oOrdenesTemporales.Count
                         basPublic.ofrmPedidos(i).g_oOrdenesTemporales.Item(j).Abono = True
                    Next j
                End If
                If basPublic.ofrmPedidos(i).g_oOrdenesTemporales.Count > 0 Then
                    If basPublic.ofrmPedidos(i).g_TipoEmision = PedidoEstandar And basPublic.ofrmPedidos(i).m_oTipoPedido Is Nothing Then
                        basPublic.ofrmPedidos(i).sstabPedidos.Tab = 0
                        basPublic.ofrmPedidos(i).chkPedirTodo.Value = 0
                    Else
                        basPublic.ofrmPedidos(i).sstabPedidos.Tab = 1
                    End If
                End If
                
                basPublic.ofrmPedidos(i).txtRetenGarantia.Text = m_oProceso.RetencionEnGarantia
                basPublic.ofrmPedidos(i).txtRetenGarantia_LostFocus
                
                If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then
                    oProcesoEliminar.Reabrir (oUsuarioSummit.Cod)
                    Set m_oIBaseDatosEdicionEnPRoceso = oProcesoEliminar
                    TESErrorELiminarProceso = m_oIBaseDatosEdicionEnPRoceso.EliminarDeBaseDatos
                    If b_ErrorPosicionarTabPedidos Then
                        Screen.MousePointer = vbNormal
                        Unload Me
                        Exit Sub
                    End If
                    basPublic.ofrmPedidos(i).sstabPedidos.TabEnabled(0) = False
                    basPublic.ofrmPedidos(i).sstabPedidos.Tab = 1
                End If
            Next
            Screen.MousePointer = vbNormal
            Unload Me
        End If
    Else    'Sin SM o no hay empresa
        Unload frmPEDIDOS
        frmPEDIDOS.g_lIdEmpresa = BuscarEmpresa()
        frmPEDIDOS.m_bProcededeSolicitud = True
        If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then frmPEDIDOS.g_bSolicitudAbono = True
        frmPEDIDOS.sdbcAnyo = m_oProceso.Anyo
        frmPEDIDOS.sdbcGMN1_4Cod = m_oProceso.GMN1Cod
        frmPEDIDOS.GMN1Seleccionado
        frmPEDIDOS.m_bRespetarCombo = True
        frmPEDIDOS.sdbcProceCod = m_oProceso.Cod
        
        frmPEDIDOS.m_bRespetarCombo = False
        DoEvents
        
        frmPEDIDOS.sdbcProceCod_Validate False
        
        'Paso a la pesta�a todos para que cargue todos los �tems, sino s�lo se emitir�an los del primer grupo
        If frmPEDIDOS.ssTabGrupos.Tabs.Count > 1 Then
            frmPEDIDOS.ssTabGrupos.Tabs(frmPEDIDOS.ssTabGrupos.Tabs.Count).Selected = True
            frmPEDIDOS.SSTabGrupos_Click
        End If
        
        'Ahora chequea todos los art�culos:
        For i = 0 To frmPEDIDOS.sdbgPedidos.Rows - 1
            frmPEDIDOS.sdbgPedidos.Columns(0).Value = 1
            frmPEDIDOS.sdbgPedidos.col = 0
            frmPEDIDOS.sdbgPedidos_Change
            frmPEDIDOS.sdbgPedidos.MoveNext
        Next i
        
        If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then
            For j = 1 To frmPEDIDOS.g_oOrdenesTemporales.Count
                frmPEDIDOS.g_oOrdenesTemporales.Item(j).Abono = True
            Next j
            ' Set frmPEDIDOS.g_oOrdenEntrega = oFSGSRaiz.Generar_COrdenEntrega
            ' frmPEDIDOS.g_oOrdenEntrega.Abono = True
        End If
        If frmPEDIDOS.sstabPedidos.TabVisible(1) Then
            If frmPEDIDOS.g_TipoEmision = PedidoEstandar And frmPEDIDOS.m_oTipoPedido Is Nothing Then
                frmPEDIDOS.sstabPedidos.Tab = 0
                frmPEDIDOS.chkPedirTodo.Value = 0
            Else
                frmPEDIDOS.sstabPedidos.Tab = 1
            End If
        End If
                
        frmPEDIDOS.txtRetenGarantia.Text = m_oProceso.RetencionEnGarantia
        frmPEDIDOS.txtRetenGarantia_LostFocus
        
        If m_oSolicitudSeleccionada.Solicitud.Tipo = TipoSolicitud.Abono Then
'            Set frmPEDIDOS.g_oOrdenEntrega = oFSGSRaiz.Generar_COrdenEntrega
            oProcesoEliminar.Reabrir (oUsuarioSummit.Cod)
            Set m_oIBaseDatosEdicionEnPRoceso = oProcesoEliminar
            TESErrorELiminarProceso = m_oIBaseDatosEdicionEnPRoceso.EliminarDeBaseDatos
            If b_ErrorPosicionarTabPedidos Then
                Screen.MousePointer = vbNormal
                Unload Me
                Exit Sub
            End If
            frmPEDIDOS.sstabPedidos.TabEnabled(0) = False
            frmPEDIDOS.sstabPedidos.Tab = 1
        End If
        DoEvents
        Screen.MousePointer = vbNormal
        frmPEDIDOS.SetFocus
        Unload Me
        'GenerarPedidosDirectos
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "GenerarPedidosDirectos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>Enviar a pedido las adjudicaciones creadas</summary>
''' <remarks>Llamada desde: cmdContinuar_Click</remarks>

Private Sub EnviarAPedido()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Unload frmSeguimiento
    
    frmSeguimiento.AbrirBusquedaParaEnvioAPedido m_oProceso
    Screen.MousePointer = vbNormal
    frmSeguimiento.SetFocus
    Unload Me
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "EnviarAPedido", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Devuelve la empresa de la distribuci�n del proceso
''' </summary>
''' <returns>Id de empresa</returns>
''' <remarks>Llamada desde: GenerarPedidoDirecto; Tiempo m�ximo:0,6</remarks>
Private Function BuscarEmpresa() As Long
    Dim oPersona As CPersona
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPersona = oFSGSRaiz.Generar_CPersona
    
    oPersona.UON1 = NullToStr(m_oSolicitudSeleccionada.Peticionario.UON1)
    oPersona.UON2 = NullToStr(m_oSolicitudSeleccionada.Peticionario.UON2)
    oPersona.UON3 = NullToStr(m_oSolicitudSeleccionada.Peticionario.UON3)
    
    BuscarEmpresa = m_oProceso.DevolverEmpresa(True, oPersona.UON1, oPersona.UON2, oPersona.UON3)
        
    Set oPersona = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "BuscarEmpresa", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Sub ComprobacionAbrirProceso()
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim sArticulosaBorrar As String
Dim sArticulosaModificar As String
Dim irespuesta As Integer
Dim oArticulos As CArticulos
        
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    irespuesta = vbYes
    'Si no puede dar de alta art�culos en la apertura de procesos o la integraci�n es en sentido solo salida
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEAltaArticulos)) Is Nothing) Or _
        (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
                
        If Not m_oProceso.Grupos Is Nothing Then
       
            For Each oGrupo In m_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        If Not oItem.eliminado Then
                            If Trim$(oItem.ArticuloCod) = "" Then
                                ''Item No codificado. Si esta aqui sera que lo permite
                            Else
                                ''Buscar el Articulo en la BBDD
                                Set oArticulos = oFSGSRaiz.Generar_CArticulos
                                oArticulos.CargarTodosLosArticulos oItem.ArticuloCod, , True
                            
                                If oArticulos.Count = 0 Then
                                    sArticulosaBorrar = sArticulosaBorrar & oItem.ArticuloCod & " - " & oItem.Descr & Chr(10)
                                    oGrupo.Items.Remove (CStr(oItem.Id))
                                ElseIf oArticulos.Count = 1 Then
                                    'Si la den es distinta y el articulo no generico pero no hay permiso de modificar o es solo importar la int
                                    If oItem.Descr <> oArticulos.Item(1).Den And Not oArticulos.Item(1).Generico Then
                                        sArticulosaModificar = sArticulosaModificar & oItem.ArticuloCod & " - " & oItem.Descr & Chr(10)
                                        oItem.Descr = oArticulos.Item(1).Den
                                    End If
                                End If
                            
                                Set oArticulos = Nothing
                            End If
                        End If
                    Next
                End If
            Next
        
        End If
    End If

    
    If sArticulosaBorrar <> "" And Not (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
        irespuesta = oMensajes.PreguntaAbrirProcesoEliminandoArticulos(sArticulosaBorrar)
        If irespuesta = vbYes Then
            If sArticulosaModificar <> "" And Not (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
                irespuesta = oMensajes.PreguntaAbrirProcesoSinModificarDescripcion(sArticulosaModificar)
            End If
        End If
    ElseIf sArticulosaModificar <> "" And Not (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4)) Then
        irespuesta = oMensajes.PreguntaAbrirProcesoSinModificarDescripcion(sArticulosaModificar)
    End If
       
    If irespuesta <> vbYes Then
        Unload Me
    End If
    
    Screen.MousePointer = vbNormal
                
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ComprobacionAbrirProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Function BloqueoEtapa(IdInstancia As Variant) As Boolean
    Dim oInstancia As CInstancia
    Dim Ador As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.Id = IdInstancia
    Set Ador = oInstancia.ComprobarBloqueoEtapa()
    
    If Not Ador.EOF Then
        BloqueoEtapa = IIf(Ador.Fields("BLOQUEO_ADJUDICACION").Value = 1, False, True)
    Else
        BloqueoEtapa = False
    End If
    
    Ador.Close
    Set Ador = Nothing
    Set oInstancia = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "BloqueoEtapa", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

''' <summary>
''' Se comprueban las condiciones de bloqueo por f�rmula
''' </summary>
''' <param name="IdInstancia">Instancia</param>
''' <param name="ListaAvisos">ByRef. Lista de condiciones de aviso q no cumple</param>
''' <param name="oAdjs">Adjudicaciones a realizar</param>
''' <returns>Si no cumple alguna condici�n de bloqueo. ListaAvisos</returns>
''' <remarks>Llamada desde: GenerarPedidosDirectos; Tiempo m�ximo: 0,1</remarks>
Private Function BloqueoCondiciones(IdInstancia As Variant, ByRef ListaAvisos As String, Optional ByVal oAdjs As CAdjudicaciones) As Boolean
    Dim oInstancia As CInstancia
    Dim iBloqueo As Integer
    Dim bExisteBloqueo As Boolean
    Dim Ador As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.Id = IdInstancia
            
    bExisteBloqueo = False
    
    Set Ador = oInstancia.ComprobarBloqueoCond(TipoBloqueo.Adjudicacion)

    If Ador.EOF Then
        BloqueoCondiciones = False
        Ador.Close
        Set Ador = Nothing
        Set oInstancia = Nothing
        Exit Function
    Else
        While Not Ador.EOF And Not bExisteBloqueo
            iBloqueo = oInstancia.ComprobarBloqueoCondiciones(Ador.Fields("ID").Value, Ador.Fields("FORMULA").Value, , oAdjs, m_oProceso.Anyo, m_oProceso.Cod, m_oProceso.GMN1Cod, , m_oProceso)
            Select Case iBloqueo
                Case 0  'No existe bloqueo
                
                Case 1  'Existe bloqueo
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.Bloqueo (NullToStr(Ador.Fields("MENSAJE_" & gParametrosInstalacion.gIdioma).Value))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
                    If Ador.Fields("TIPO").Value = TipoAvisoBloqueo.Bloquea Then
                        bExisteBloqueo = True
                    End If
                Case 3  'Error al realizar el c�lculo:F�rmula inv�lida.
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
                Case 4  'Error al realizar el c�lculo:Valores incorrectos.
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
            End Select
            Ador.MoveNext
        Wend
    End If
    
    BloqueoCondiciones = bExisteBloqueo

    Ador.Close
    Set Ador = Nothing
    Set oInstancia = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "BloqueoCondiciones", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub CargarPlantillas(ByVal Opcion As Integer)
        Dim oPlantilla As CPlantilla
        Dim oPlantillas As CPlantillas

        Dim oItem As CItem
        Dim admite As Boolean
        Dim oGrupo As Object
        
        Dim rs As ADODB.Recordset
        
        '3279
        Dim PedidoDirecto As Boolean
        Dim bPorDefecto As Boolean
        Dim sNombre As String
        Dim sId As String
        
        Dim Primero As Boolean
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Primero = False
        iorigen = 0
                
        Set oPlantillas = oFSGSRaiz.Generar_CPlantillas
        Set oPlantilla = oFSGSRaiz.Generar_CPlantilla
        
        PedidoDirecto = (m_iGenerar = 3 Or m_iGenerar = 4)
        
        '3275
        'Cada vez que se carga una plantilla, se borran del proceso los atributos de especificacion
        'de la anterior plantilla seleccionada
        EliminarAtribEspePlantillaDelProceso
                
        'Si la opcion a anterior era pedido directo, o la nueva
        'es pedido directo, se borra la plantilla seleccionada
        If (m_iGenerar = 3 Or m_iGenerar = 4) And Not (Opcion = 3 Or Opcion = 4) Then
            sdbcPlantilla.Text = ""
            Set m_oPlantillaSeleccionada = Nothing
        ElseIf m_iGenerar <> 3 And (Opcion = 3 Or Opcion = 4) Then
            sdbcPlantilla.Text = ""
            Set m_oPlantillaSeleccionada = Nothing
        End If

        sdbcPlantilla.RemoveAll
        sdbcPlantilla.AddItem "" & Chr(m_lSeparador) & "..."

        'Si vamos a generar un pedido directo, se cargan las plantillas
        'marcadas para pedido directo (PLANTILLA.PARAPEDIDO = 1), sino, las no marcadas (PLANTILLA.PARAPEDIDO = 0)
        Set rs = oPlantillas.CargarTodasLasPlantillas2(, PedidoDirecto)
        bPorDefecto = False
        While Not rs.EOF
            admite = True
            If rs("RestriccionMaterial").Value > 0 Then
                'Existe Restriccion de Material
                admite = False
                For Each oGrupo In m_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            If oItem.eliminado Then
                            Else
                                If oPlantilla.AdmiteMaterial(oItem, rs("ID").Value) = True Then
                                    admite = True ' admite al menos un material
                                    Exit For
                                End If
                            End If
                        Next
                        If admite Then
                            Exit For
                        End If
                    End If
                Next
            End If
            If admite Then
                ' annadir la plantilla al combo
                sdbcPlantilla.AddItem rs("NOMBRE").Value & Chr(m_lSeparador) & rs("ID").Value

                'Se comprueba si tiene que abrir el proceso desde plantilla
                'En ese caso, se seleccionar�a la primera plantilla del desplegable
                If Not PedidoDirecto Then
                    bPorDefecto = True
                    If m_bRestrAperturaPlantilla And Primero = False Then
                        sNombre = CStr(rs("NOMBRE").Value)
                        sId = CStr(rs("Id").Value)
                        NombrePlantilla1 = sNombre
                        IdPlantilla1 = sId
                        Primero = True                      'Para indicar que ya ha tomado la primera plantilla del desplegable
                    End If
                Else
                    'Seleccionamos la plantilla con material por defecto
                    If rs("PARAPEDIDODEF") And rs("RestriccionMaterial").Value > 0 Then
                        bPorDefecto = True
                        sNombre = CStr(rs("NOMBRE").Value)
                        sId = CStr(rs("Id").Value)
    
                        If m_bRestrAperturaPlantilla Then
                            NombrePlantilla1 = sNombre
                            IdPlantilla1 = sId
                        End If
                    End If
                End If
            End If
            rs.MoveNext
        Wend
        
        rs.Close
        Set rs = Nothing
        'si es pedido  y no se ha seleccionado plantilla por defecto con material
        If bPorDefecto Then
            SeleccionarPlantilla sNombre
        Else
            Set rs = oPlantillas.CargarPlantillaDefectoMaterial(False)
            
            If Not rs.EOF Then
                sNombre = CStr(rs("NOMBRE").Value)
                sId = CStr(rs("Id").Value)
                SeleccionarPlantilla sNombre
                
                If m_bRestrAperturaPlantilla Then
                    NombrePlantilla1 = sNombre
                    IdPlantilla1 = sId
                End If
            End If
        End If
    
    
        PlantillaSeleccionada
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "CargarPlantillas", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub



Private Sub EliminarAtribEspePlantillaDelProceso()

    Dim m_oAtribEspe As CAtributo
    Dim oGrupo As CGrupo
    Dim oItem As CItem
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oAtributoG = Nothing
    Set m_oAtributoI = Nothing
    
    sdbgAtributos.RemoveAll
    
    If Not m_oAtribEspePlantG Is Nothing Then
        For Each m_oAtribEspe In m_oAtribEspePlantG
            If Not m_oProceso.Grupos Is Nothing Then
                For Each oGrupo In m_oProceso.Grupos
                    oGrupo.AtributosEspecificacion.Remove (m_oAtribEspe.Atrib)
                Next
            End If
        Next
        Set m_oAtribEspePlantG = Nothing
    End If
    If Not m_oAtribEspePlantI Is Nothing Then
        For Each m_oAtribEspe In m_oAtribEspePlantI
            If Not m_oProceso.Grupos Is Nothing Then
                For Each oGrupo In m_oProceso.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        For Each oItem In oGrupo.Items
                            oItem.AtributosEspecificacion.Remove (m_oAtribEspe.Atrib)
                        Next
                    End If
                Next
            End If
        Next
        Set m_oAtribEspePlantI = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "EliminarAtribEspePlantillaDelProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


'3275
Private Sub AnyadirAtribEspeObligatoriosPlantillaAProceso()
'A�ade los Atributos de Especificacion de la plantilla al proceso de compra
'tanto a nivel de proceso, como de grupo o de item

    Dim oAtribEsp As CAtributo
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iorigen = 0
        
    'cargamos todos los atributos de especificacion de ambito PROCESO
    m_oPlantillaSeleccionada.CargarTodosLosAtributosEspecificacion ("")
    
    For Each oAtribEsp In m_oPlantillaSeleccionada.AtributosEspecificacion
        If oAtribEsp.Obligatorio = True Then
            
            Select Case oAtribEsp.ambito
    
                Case TipoAmbitoProceso.AmbGrupo
                    If Not m_oProceso.Grupos Is Nothing Then
                        For Each oGrupo In m_oProceso.Grupos
                            If oGrupo.AtributosEspecificacion Is Nothing Then
                                Set oGrupo.AtributosEspecificacion = oFSGSRaiz.Generar_CAtributos
                            End If
                            oGrupo.AtributosEspecificacion.AddAtrEsp m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbGrupo, , , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , oAtribEsp.ListaExterna
                        Next
                        
                        If m_oAtribEspePlantG Is Nothing Then
                            Set m_oAtribEspePlantG = oFSGSRaiz.Generar_CAtributos
                        End If
                        m_oAtribEspePlantG.AddAtrEsp m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbGrupo, , , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , oAtribEsp.ListaExterna
                    End If
                
                Case TipoAmbitoProceso.AmbItem
                    If Not m_oProceso.Grupos Is Nothing Then
                        For Each oGrupo In m_oProceso.Grupos
                            If Not oGrupo.Items Is Nothing Then
                                For Each oItem In oGrupo.Items
                                    If oItem.AtributosEspecificacion Is Nothing Then
                                        Set oItem.AtributosEspecificacion = oFSGSRaiz.Generar_CAtributos
                                    End If
                                    oItem.AtributosEspecificacion.AddAtrEsp m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbItem, , , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , oAtribEsp.ListaExterna
                                Next
                            End If
                        Next
                    End If
                    
                    If m_oAtribEspePlantI Is Nothing Then
                        Set m_oAtribEspePlantI = oFSGSRaiz.Generar_CAtributos
                    End If
                    m_oAtribEspePlantI.AddAtrEsp m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbItem, , , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , oAtribEsp.ListaExterna

            End Select
            
        End If
    Next
    
    'cargamos todos los atributos de especificacion de ambito GRUPO, por cada grupo
    For Each oGrupo In m_oProceso.Grupos
    
        Set m_oPlantillaSeleccionada.AtributosEspecificacion = Nothing
        m_oPlantillaSeleccionada.CargarTodosLosAtributosEspecificacion (oGrupo.Id)
    
        For Each oAtribEsp In m_oPlantillaSeleccionada.AtributosEspecificacion
            If oAtribEsp.Obligatorio = True Then
                
                Select Case oAtribEsp.ambito
                
                    Case TipoAmbitoProceso.AmbGrupo
                    
                        If oGrupo.AtributosEspecificacion Is Nothing Then
                            Set oGrupo.AtributosEspecificacion = oFSGSRaiz.Generar_CAtributos
                        End If
                        oGrupo.AtributosEspecificacion.AddAtrEsp m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbGrupo, oGrupo.Codigo, , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , oAtribEsp.ListaExterna
                        If m_oAtribEspePlantG Is Nothing Then
                             Set m_oAtribEspePlantG = oFSGSRaiz.Generar_CAtributos
                        End If
                        m_oAtribEspePlantG.AddAtrEsp m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbGrupo, , , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , oAtribEsp.ListaExterna

                    Case TipoAmbitoProceso.AmbItem
                        For Each oItem In oGrupo.Items
                            If oItem.AtributosEspecificacion Is Nothing Then
                                Set oItem.AtributosEspecificacion = oFSGSRaiz.Generar_CAtributos
                            End If
                            
                            If m_oAtribEspePlantI Is Nothing Then
                                Set m_oAtribEspePlantI = oFSGSRaiz.Generar_CAtributos
                            End If
                            oItem.AtributosEspecificacion.AddAtrEsp m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbItem, oGrupo.Codigo, , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , oAtribEsp.ListaExterna
                            m_oAtribEspePlantI.AddAtrEsp m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, oAtribEsp.Atrib, TipoAmbitoProceso.AmbItem, , , oAtribEsp.interno, oAtribEsp.pedido, , oAtribEsp.valorNum, oAtribEsp.valorText, oAtribEsp.valorFec, oAtribEsp.valorBool, oAtribEsp.Cod, oAtribEsp.Den, oAtribEsp.Descripcion, oAtribEsp.Tipo, oAtribEsp.Minimo, oAtribEsp.Maximo, oAtribEsp.TipoIntroduccion, , , , , , , , , oAtribEsp.ListaExterna
                        Next
                End Select
            End If
        Next
    Next
    
    If m_iGenerar <> 0 Then MostrarAtributosGrupo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "AnyadirAtribEspeObligatoriosPlantillaAProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub



'3279
Private Sub SeleccionarPlantilla(ByVal nombre As String)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPlantilla.Text = nombre
    sdbcPlantilla_PositionList (sdbcPlantilla.Text)
    sdbcPlantilla_Validate True

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "SeleccionarPlantilla", err, Erl, , m_bActivado)
      Exit Sub
   End If

    
End Sub


'edu pm98
Public Sub SeleccionarMaterial(ByVal Origen As String, Optional ByVal proceso As cProceso, Optional ByVal comprador As Boolean, Optional ByVal multimaterial As Boolean = False, Optional ByVal oPlantillaSeleccionada As CPlantilla)
    Dim restriccion As Boolean
    Dim oPlantillas As CPlantillas
    Dim hay_plantilla As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    restriccion = False

    ' conseguir una plantilla, bien directamente, bien desde el proceso.
    
    If Not IsMissing(oPlantillaSeleccionada) Then
        If Not (oPlantillaSeleccionada Is Nothing) Then
            hay_plantilla = True
        End If
    End If

    If Not hay_plantilla Then
        If Not IsEmpty(proceso.Plantilla) Then
            If Not IsNull(proceso.Plantilla) Then
                Set oPlantillas = oFSGSRaiz.Generar_CPlantillas
                oPlantillas.CargarTodasLasPlantillas proceso.Plantilla, False
                Set oPlantillaSeleccionada = oPlantillas.Item(CStr(proceso.Plantilla))
                hay_plantilla = True
            End If
        End If
    End If
    
    If hay_plantilla Then
        If oPlantillaSeleccionada.ExisteRestriccionMaterial Then
            restriccion = True
        End If
    End If
    
    ' si la plantilla tiene restriccion de material abrir formulario de restriccion de material
    
    If restriccion Then
        Set frmSELMATPlantilla.oPlantillaSeleccionada = oPlantillaSeleccionada
        frmSELMATPlantilla.bPermProcMultiMaterial = multimaterial
        frmSELMATPlantilla.sOrigen = Origen
        If Not IsEmpty(comprador) Then
            frmSELMATPlantilla.bRComprador = comprador
        End If
        frmSELMATPlantilla.Show 1
    Else
        frmSELMAT.bPermProcMultiMaterial = multimaterial
        frmSELMAT.sOrigen = Origen
        If Not IsEmpty(comprador) Then
            frmSELMAT.bRComprador = comprador
        End If
        Set frmSELMAT.oProcesoSeleccionado = proceso
        frmSELMAT.Show 1
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "SeleccionarMaterial", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

'edu pm98
'si llegan argumentos hacer abstraccion del nombre del formulario
'una vez depurada esta funcion debe sustituir a PonerMatSeleccionadoEnCombos

Public Sub PonerMatSeleccionadoNew(Optional oGMN4Seleccionado As CGrupoMatNivel4)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsMissing(oGMN4Seleccionado) Or IsEmpty(oGMN4Seleccionado) Then
        Set g_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    Else
        Set g_oGMN4Seleccionado = oGMN4Seleccionado
    End If
    
    If Not g_oGMN4Seleccionado Is Nothing Then
        lblMat.caption = g_oGMN4Seleccionado.GMN1Cod & " - " & g_oGMN4Seleccionado.GMN2Cod & " - " & g_oGMN4Seleccionado.GMN3Cod & "-" & g_oGMN4Seleccionado.Cod & " - " & g_oGMN4Seleccionado.Den
    Else
        Exit Sub
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "PonerMatSeleccionadoNew", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Establece el proveedor desde la busqueda de proveedor
''' </summary>
''' <remarks>Llamada desde: frmproveBuscar ; Tiempo m�ximo: 0,2</remarks>
Public Sub CargarProveedorConBusqueda()

    Dim oProves As CProveedores
    Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados

If Not frmPROVEBuscar.seleccionarTodos Then
    sdbgLineas.Columns("COD_PROVE").Value = oProves.Item(1).Cod
    sdbgLineas.Columns("PROV").Value = oProves.Item(1).Cod & " - " & oProves.Item(1).Den

    For Each oItem In m_oProceso.Grupos.Item(CStr(Mid(ssTabGrupos.Tabs(ssTabGrupos.selectedItem.Index).key, 2))).Items
        If oItem.grupoCod = sdbgLineas.Columns("GRUPO").Value Then
            If oItem.Id = sdbgLineas.Columns("LINEA").Value Then
                oItem.ProveAct = oProves.Item(1).Cod
                Exit Sub
            End If
        End If
    Next
Else
    'cambiar el proveedor del resto de lineas
        
    Set oItem = m_oGrupoSeleccionado.Items.Item(CStr(sdbgLineas.Columns("LINEA").Value))
    
    Dim vbm As Variant
    Dim i As Integer
        
    vbm = sdbgLineas.Bookmark
    sdbgLineas.MoveFirst
        
    For i = 0 To sdbgLineas.Rows - 1
        m_oGrupoSeleccionado.Items.Item(i + 1).ProveAct = oProves.Item(1).Cod
        sdbgLineas.Columns("PROV").Text = oProves.Item(1).Cod & " - " & oProves.Item(1).Den
        sdbgLineas.Columns("COD_PROVE").Text = oItem.ProveAct
        sdbgLineas.MoveNext
    Next i

    sdbgLineas.Bookmark = vbm
    Set oItem = Nothing
End If
Set frmPROVEBuscar.oProveEncontrados = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "CargarProveedorConBusqueda", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Rellena la variable global 'm_oProceso' con la estructura de grupos de la plantilla seleccionada. Esta variable es el proceso a generar � del q generar
''' oferta � del q generar adjuducaciones � del q generar pedido directo.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sdbcPlantilla_CloseUp;</remarks>
Private Sub GenerarProcesoPlantilla()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim oGrProc As CGrupo
    Dim bVisibleProve As Boolean
    Dim bFinBusquedaProve As Boolean
    Dim oLineaDesglose As CLineaDesglose
    Dim oGrupoPlantilla As CGrupo
    Dim k As Long
    Dim b_encontrado As Boolean

    'carga las l�neas de desglose y genera la estructura del proceso:
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set m_oAtribEspePlantG = Nothing
    Set m_oAtribEspePlantI = Nothing
    Set m_oProceso = oFSGSRaiz.Generar_CProceso
    Set m_oEmpresas = oFSGSRaiz.Generar_CEmpresas
    
    m_oProceso.RetencionEnGarantia = ""
        
    If m_oProceso.Grupos Is Nothing Then Set m_oProceso.Grupos = oFSGSRaiz.Generar_CGrupos
    
    m_oSolicitudSeleccionada.GenerarEstructuraProceso m_oProceso, g_oParametrosSM, m_oEmpresas, m_bRestrMat, m_sIdiTrue, m_sIdiFalse, m_arrMatComp, m_oPlantillaSeleccionada, g_arSolicitudes
    
    'Si la solicitud no ten�a ning�n grupo (aparte del de los datos generales) le a�adimos al proceso los grupos de la plantilla:
    If m_oProceso.Grupos Is Nothing Then
        Set m_oProceso.Grupos = oFSGSRaiz.Generar_CGrupos
        For Each oGrProc In m_oPlantillaSeleccionada.Grupos
            m_oProceso.Grupos.Add m_oProceso, oGrProc.Codigo, oGrProc.Den, oGrProc.Codigo, oGrProc.Descripcion, oGrProc.DefDestino, oGrProc.DestCod, oGrProc.DefFechasSum, oGrProc.FechaInicioSuministro, oGrProc.FechaFinSuministro, oGrProc.DefFormaPago, oGrProc.PagCod, oGrProc.DefProveActual, oGrProc.ProveActual, oGrProc.DefDistribUON, , oGrProc.DefPresAnualTipo1, , oGrProc.DefPresAnualTipo2, , oGrProc.DefPresTipo1, , oGrProc.DefPresTipo2, , oGrProc.DefEspecificaciones, oGrProc.esp, , oGrProc.Items, , , , oGrProc.IDPlantilla, , , , , , , , , oGrProc.AtributosEspecificacion, , , , , , , oGrProc.DistsNivel1, oGrProc.DistsNivel2, oGrProc.DistsNivel3
        Next
    Else
        For Each oGrProc In m_oProceso.Grupos
            oGrProc.IDPlantilla = m_oProceso.Plantilla
        Next
    End If
    
    With sdbgLineas
        'Si el destino,forma de pago y fechas de suministro no est�n a nivel de �tem no se muestran en la grid:
        If m_oProceso.DefDestino <> EnItem Then .Columns("DEST").Visible = False
        
        If m_oProceso.DefFormaPago <> EnItem Then .Columns("PAGO").Visible = False
        
        If m_oProceso.DefFechasSum <> EnItem Then
            .Columns("FEC_INI").Visible = False
            .Columns("FEC_FIN").Visible = False
        Else
            .Columns("FEC_INI").StyleSet = "normal"
            .Columns("FEC_FIN").StyleSet = "normal"
                
            'Edici�n de fechas inicio y fin de suministro
            .Columns("FEC_INI").Style = ssStyleEditButton
            .Columns("FEC_INI").Locked = False
            .Columns("FEC_FIN").Style = ssStyleEditButton
            .Columns("FEC_FIN").Locked = False
        End If
    End With
    
    If g_bGenerarPedido Or g_bPedidoExistente Then
        bVisibleProve = False
        bFinBusquedaProve = False
                       
        For Each oGrupo In m_oSolicitudSeleccionada.Grupos
            For Each oCampo In oGrupo.CAMPOS
                If oCampo.CampoGS = TipoCampoGS.Proveedor Then
                    bVisibleProve = False
                    bFinBusquedaProve = True
                                        
                    Exit For
                ElseIf Not oCampo.LineasDesglose Is Nothing Then
                    For Each oLineaDesglose In oCampo.LineasDesglose
                        If oLineaDesglose.CampoHijo.CampoGS = TipoCampoGS.Proveedor Then
                            bVisibleProve = True
                            bFinBusquedaProve = True
    
                            Exit For
                        End If
                    Next
                    
                    If bFinBusquedaProve Then Exit For
                End If
            Next
            
            If bFinBusquedaProve Then Exit For
        Next
                            
        sdbgLineas.Columns("PROV").Visible = bVisibleProve
    Else
        If m_oProceso.DefProveActual <> EnItem Then
            sdbgLineas.Columns("PROV").Visible = False
        End If
    End If
    
    'Se recorren todos los grupos de la plantilla
    For Each oGrupoPlantilla In m_oPlantillaSeleccionada.Grupos
        'Se recorren todos los grupos del proceso
        b_encontrado = False
        For k = 1 To m_oProceso.Grupos.Count
            If oGrupoPlantilla.Codigo = m_oProceso.Grupos.Item(k).Codigo Then
                b_encontrado = True
                Exit For
            End If
        Next k
        
        'Grupo de la plantilla que se ha de a�adir al proceso aunque este sin items
        If Not b_encontrado Then
            m_oProceso.Grupos.Add m_oProceso, oGrupoPlantilla.Codigo, oGrupoPlantilla.Den, oGrupoPlantilla.Codigo, oGrupoPlantilla.Descripcion, oGrupoPlantilla.DefDestino, oGrupoPlantilla.DestCod, oGrupoPlantilla.DefFechasSum, oGrupoPlantilla.FechaInicioSuministro, oGrupoPlantilla.FechaFinSuministro, oGrupoPlantilla.DefFormaPago, oGrupoPlantilla.PagCod, oGrupoPlantilla.DefProveActual, oGrupoPlantilla.ProveActual, oGrupoPlantilla.DefDistribUON, , oGrupoPlantilla.DefPresAnualTipo1, , oGrupoPlantilla.DefPresAnualTipo2, , oGrupoPlantilla.DefPresTipo1, , oGrupoPlantilla.DefPresTipo2, , oGrupoPlantilla.DefEspecificaciones, oGrupoPlantilla.esp, , oGrupoPlantilla.Items, , , , oGrupoPlantilla.IDPlantilla, , , , , , , , , oGrupoPlantilla.AtributosEspecificacion, , , , , , , oGrupoPlantilla.DistsNivel1, oGrupoPlantilla.DistsNivel2, oGrupoPlantilla.DistsNivel3
        End If
    Next

    'Comprobar que las UONS obtenidas cumplen con el 'Nivel minimo de asignaci�n a unidades organizativas'
    ComprobarNivelMinimo
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "GenerarProcesoPlantilla", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


''' <summary>
''' Comprobar que las UONs obtenidas cumplen con el 'Nivel m�nimo de asignaci�n a unidades organizativas'
'''Si no se cumple, se elimina la distribuci�n que se acaba de asignar y se comprueba la 'DefDistribUON' de m_oProceso
'''Entonces, luego ya se asignar�a la del peticionario
''' </summary>
''' <remarks>Llamada desde: GenerarProcesoCompra, GenerarProcesoPlantilla</remarks>
Private Sub ComprobarNivelMinimo()

Dim oGrupo As CGrupo
Dim oItem As CItem
Dim CambDistGrupo As Boolean
Dim HayItems As Boolean
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oProceso Is Nothing Then Exit Sub
        
    If Not m_oProceso.DistsNivel3 Is Nothing Then               'Nivel 3
        If gParametrosGenerales.giNIVDIST > 3 Then Set m_oProceso.DistsNivel3 = Nothing
    Else
        If Not m_oProceso.DistsNivel2 Is Nothing Then           'Nivel 2
            If gParametrosGenerales.giNIVDIST > 2 Then Set m_oProceso.DistsNivel2 = Nothing
        Else
            If Not m_oProceso.DistsNivel1 Is Nothing Then       'Nivel 1
                If gParametrosGenerales.giNIVDIST > 1 Then Set m_oProceso.DistsNivel1 = Nothing
            End If
        End If
    End If
    
    If Not m_oProceso.Grupos Is Nothing Then
        For Each oGrupo In m_oProceso.Grupos
            If Not oGrupo.DistsNivel3 Is Nothing Then
                If gParametrosGenerales.giNIVDIST > 3 Then Set oGrupo.DistsNivel3 = Nothing
            Else
                If Not oGrupo.DistsNivel2 Is Nothing Then
                    If gParametrosGenerales.giNIVDIST > 2 Then Set oGrupo.DistsNivel2 = Nothing
                Else
                    If Not oGrupo.DistsNivel1 Is Nothing Then
                        If gParametrosGenerales.giNIVDIST > 1 Then Set oGrupo.DistsNivel1 = Nothing
                    End If
                End If
            End If
            
            'Items
            If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    If Not oItem.eliminado Then
                        If Not NoHayParametro(oItem.UON3Cod) Then
                            If gParametrosGenerales.giNIVDIST > 3 Then
                                oItem.UON3Cod = Empty
                                oItem.UON2Cod = Empty
                                oItem.UON1Cod = Empty
                            End If
                        Else
                            If Not NoHayParametro(oItem.UON2Cod) Then
                                If gParametrosGenerales.giNIVDIST > 2 Then
                                    oItem.UON2Cod = Empty
                                    oItem.UON1Cod = Empty
                                End If
                            Else
                                If Not NoHayParametro(oItem.UON1Cod) Then
                                    If gParametrosGenerales.giNIVDIST > 1 Then
                                        oItem.UON1Cod = Empty
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
            End If
        Next
    
        Select Case m_oProceso.DefDistribUON
        
        Case EnProceso
            'Si hay plantilla y es a otro ambiot lo cambiamos
            If Not m_oPlantillaSeleccionada Is Nothing Then
                If m_oPlantillaSeleccionada.DistribUON = EnProceso Then
                    'se deja como est�
                ElseIf m_oPlantillaSeleccionada.DistribUON = EnGrupo Then 'Se pasa a grupo
                        m_oProceso.DefDistribUON = EnGrupo
                        For Each oGrupo In m_oProceso.Grupos
                            'Se recorren todos los grupos del proceso
                            oGrupo.DefDistribUON = True
                            If Not m_oProceso.DistsNivel3 Is Nothing Then
                                oGrupo.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                                oGrupo.DistsNivel3.Add Nothing, m_oProceso.DistsNivel3.Item(1).CodUON1, m_oProceso.DistsNivel3.Item(1).CodUON2, m_oProceso.DistsNivel3.Item(1).CodUON3, 100, , 0
                            ElseIf Not m_oProceso.DistsNivel2 Is Nothing Then
                                Set oGrupo.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                                oGrupo.DistsNivel2.Add Nothing, m_oProceso.DistsNivel2.Item(1).CodUON1, m_oProceso.DistsNivel2.Item(1).CodUON2, 100, , 0
                            ElseIf Not m_oProceso.DistsNivel1 Is Nothing Then
                                Set oGrupo.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel2
                                oGrupo.DistsNivel1.Add Nothing, m_oProceso.DistsNivel1.Item(1).CodUON1, 100, , 0
                            End If
                        Next
                ElseIf m_oPlantillaSeleccionada.DistribUON = EnItem Then
                    m_oProceso.DefDistribUON = EnItem
                    For Each oGrupo In m_oProceso.Grupos
                        If Not oGrupo.Items Is Nothing Then
                            For Each oItem In oGrupo.Items
                                If Not m_oProceso.DistsNivel3 Is Nothing Then          'Nivel 3
                                    oItem.UON1Cod = m_oProceso.DistsNivel3.Item(1).CodUON1
                                    oItem.UON2Cod = m_oProceso.DistsNivel3.Item(1).CodUON2
                                    oItem.UON3Cod = m_oProceso.DistsNivel3.Item(1).CodUON3
                                Else
                                    If Not m_oProceso.DistsNivel2 Is Nothing Then      'Nivel 2
                                        oItem.UON1Cod = m_oProceso.DistsNivel2.Item(1).CodUON1
                                        oItem.UON2Cod = m_oProceso.DistsNivel2.Item(1).CodUON2
                                        oItem.UON3Cod = Null
                                    Else
                                        If Not m_oProceso.DistsNivel1 Is Nothing Then  'Nivel 1
                                            oItem.UON1Cod = m_oProceso.DistsNivel1.Item(1).CodUON1
                                            oItem.UON2Cod = Null
                                            oItem.UON3Cod = Null
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
            End If
            
        Case EnGrupo
            If Not m_oPlantillaSeleccionada Is Nothing Then
                If m_oPlantillaSeleccionada.DistribUON = EnProceso Then
                    'Se borra por si tuviese valor de antes
                    Set m_oProceso.DistsNivel1 = Nothing
                    Set m_oProceso.DistsNivel2 = Nothing
                    Set m_oProceso.DistsNivel3 = Nothing
                    For Each oGrupo In m_oProceso.Grupos

                        If Not oGrupo.DistsNivel3 Is Nothing Or Not oGrupo.DistsNivel2 Is Nothing Or Not oGrupo.DistsNivel1 Is Nothing Then
                            If Not oGrupo.DistsNivel3 Is Nothing Then               'Nivel 3
                                'Comprobamos que esta dsitribucion es valida para todos los art�culos, no la cambiamos
                                If Not ComprobarDistArticulos(oGrupo.DistsNivel3.Item(1).CodUON1, oGrupo.DistsNivel3.Item(1).CodUON2, oGrupo.DistsNivel3.Item(1).CodUON3) Then
                                    Exit Sub
                                Else 'Se cambia a proceso
                                    m_oProceso.DefDistribUON = EnProceso
                                    Set m_oProceso.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                                    m_oProceso.DistsNivel3.Add Nothing, oGrupo.DistsNivel3.Item(1).CodUON1, oGrupo.DistsNivel3.Item(1).CodUON2, oGrupo.DistsNivel3.Item(1).CodUON3, 100, , 0
                                    Exit For
                                End If

                            ElseIf Not oGrupo.DistsNivel2 Is Nothing Then           'Nivel 2
                                'Comprobamos que esta dsitribucion es valida para todos los art�culos, no la cambiamos
                                If Not ComprobarDistArticulos(oGrupo.DistsNivel2.Item(1).CodUON1, oGrupo.DistsNivel2.Item(1).CodUON2) Then
                                    Exit Sub
                                Else
                                    'Se asigna el valor
                                    m_oProceso.DefDistribUON = EnProceso
                                    Set m_oProceso.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                                    m_oProceso.DistsNivel2.Add Nothing, oGrupo.DistsNivel2.Item(1).CodUON1, oGrupo.DistsNivel2.Item(1).CodUON2, 100, , 0
                                    Exit For
                                End If
            
                            ElseIf Not oGrupo.DistsNivel1 Is Nothing Then       'Nivel 1
                                'Comprobamos que esta dsitribucion es valida para todos los art�culos, no la cambiamos
                                If Not ComprobarDistArticulos(oGrupo.DistsNivel1.Item(1).CodUON1) Then
                                    Exit Sub
                                Else
                                    'Se asigna el valor
                                    m_oProceso.DefDistribUON = EnProceso
                                    Set m_oProceso.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                                    m_oProceso.DistsNivel1.Add Nothing, oGrupo.DistsNivel1.Item(1).CodUON1, 100, , 0
                                    Exit For
                                End If
                            End If
                        End If
                        
                    Next
                    For Each oGrupo In m_oProceso.Grupos
                        oGrupo.DefDistribUON = False
                    Next
                'Cada grupo del proceso la UO con el grupo correspondiente en la plantilla que tenga distribuci�n
                'Los grupos que no tengan distribuci�n, se les pone la del peticionario si �ste cumple el nivel m�nimo
                ElseIf m_oPlantillaSeleccionada.DistribUON = EnGrupo Then
                    Exit Sub
                ElseIf m_oPlantillaSeleccionada.DistribUON = EnItem Then
                        m_oProceso.DefDistribUON = EnItem
                        For Each oGrupo In m_oProceso.Grupos
                            'Si la plantilla tiene distribuci�n a nivel de item, dentro de sus grupos, la DefDistribUON ser� False
                            If oGrupo.DefDistribUON Then
                                If oGrupo.Items Is Nothing Then
                                    For Each oItem In oGrupo.Items
                                        'Se borra por si tuviese valor de antes
                                        oItem.UON1Cod = Null
                                        oItem.UON2Cod = Null
                                        oItem.UON3Cod = Null
                                        
                                        If Not oGrupo.DistsNivel3 Is Nothing Then              'Nivel 3
                                            oItem.UON1Cod = oGrupo.DistsNivel3.Item(1).CodUON1
                                            oItem.UON2Cod = oGrupo.DistsNivel3.Item(1).CodUON2
                                            oItem.UON3Cod = oGrupo.DistsNivel3.Item(1).CodUON3
                                            
                                        ElseIf Not oGrupo.DistsNivel2 Is Nothing Then          'Nivel 2
                                            oItem.UON1Cod = oGrupo.DistsNivel2.Item(1).CodUON1
                                            oItem.UON2Cod = oGrupo.DistsNivel2.Item(1).CodUON2
                                            oItem.UON3Cod = Null
                                                
                                        ElseIf Not oGrupo.DistsNivel1 Is Nothing Then      'Nivel 1
                                            oItem.UON1Cod = oGrupo.DistsNivel1.Item(1).CodUON1
                                            oItem.UON2Cod = Null
                                            oItem.UON3Cod = Null
                                                    
                                        End If
                                        
                                    Next
                                End If
                            End If
                        Next
                End If
            Else
                CambDistGrupo = False
                i = 0
                For Each oGrupo In m_oProceso.Grupos
                    If Not oGrupo.DistsNivel1 Is Nothing Or Not oGrupo.DistsNivel2 Is Nothing Or Not oGrupo.DistsNivel3 Is Nothing Then
                        i = i + 1
                    End If
                Next
                
                'Si solo hay en un grupo, si solo uno de ellos tiene UO pero hay mas de un grupo, no es a nivel de grupo. Es a nivel de proceso
                If m_oProceso.Grupos.Count > 1 Then
                    If i = 1 Then CambDistGrupo = True
                End If
                
                'Si entra aqui es porque inicialmente hab�a distribuci�n a nivel de grupo, pero si �stas no han sido v�lidas y han sido borradas, pasa de nivel de grupo a nivel de proceso
                If i = 0 Then CambDistGrupo = True   'i = 0 si ning�n grupo se ha quedado con una distribuci�n v�lida
                
                If CambDistGrupo Then
                    For Each oGrupo In m_oProceso.Grupos
                        oGrupo.DefDistribUON = False
                    Next
                    'Si hay alg�n grupo con distribuci�n a nivel de item, se cambia a nivel de item
                    HayItems = False
                    For Each oGrupo In m_oProceso.Grupos
                        If Not oGrupo.Items Is Nothing Then
                            For Each oItem In oGrupo.Items
                                If Not oItem.eliminado Then
                                    If Not NoHayParametro(oItem.UON1Cod) Or Not NoHayParametro(oItem.UON2Cod) Or Not NoHayParametro(oItem.UON3Cod) Then
                                        HayItems = True
                                        Exit For
                                    End If
                                End If
                            Next
                            If HayItems Then Exit For
                        End If
                    Next
                    
                    'Si no hay ning�n otro grupo a nivel de item, se cambiar� a nivel de proceso
                    If HayItems Then
                        m_oProceso.DefDistribUON = EnItem
                    Else
                        m_oProceso.DefDistribUON = EnProceso
                        
                        'Se asigna los valores de campo del grupo a los del proceso
                        'Se borra por si tuviese valor de antes
                        Set m_oProceso.DistsNivel1 = Nothing
                        Set m_oProceso.DistsNivel2 = Nothing
                        Set m_oProceso.DistsNivel3 = Nothing
                        'Se introducen los del grupo
                        For Each oGrupo In m_oProceso.Grupos
                            If Not oGrupo.DistsNivel1 Is Nothing Or Not oGrupo.DistsNivel2 Is Nothing Or Not oGrupo.DistsNivel3 Is Nothing Then
                                If Not oGrupo.DistsNivel3 Is Nothing Then           'Nivel 3
                                    Set m_oProceso.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                                    m_oProceso.DistsNivel3.Add Nothing, oGrupo.DistsNivel3.Item(1).CodUON1, oGrupo.DistsNivel3.Item(1).CodUON2, oGrupo.DistsNivel3.Item(1).CodUON3, 100, , 0
                                Else
                                    If Not oGrupo.DistsNivel2 Is Nothing Then       'Nivel 2
                                        Set m_oProceso.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                                        m_oProceso.DistsNivel2.Add Nothing, oGrupo.DistsNivel2.Item(1).CodUON1, oGrupo.DistsNivel2.Item(1).CodUON2, 100, , 0
                                    Else                                            'Nivel 1
                                        Set m_oProceso.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                                        m_oProceso.DistsNivel1.Add Nothing, oGrupo.DistsNivel1.Item(1).CodUON1, 100, , 0
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If
            
        Case EnItem
            'Si la plantilla no tiene distr a item, o no hay plantilla voy a revisar los �tems para ver que distr ponemos
            If Not m_oPlantillaSeleccionada Is Nothing Then
                If m_oPlantillaSeleccionada.DistribUON <> EnItem Then
                    CambiarDistrDeItem m_oPlantillaSeleccionada.DistribUON
                End If
            Else
                CambiarDistrDeItem
            End If
        End Select
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ComprobarNivelMinimo", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' Comprobar si todos los �tems tienen la misma distribuci�n, en ese caso a proceso o grupo
''' </summary>
''' <remarks>Llamada desde: ComprobarNivelMinimo</remarks>
Private Sub CambiarDistrDeItem(Optional ByVal DistPlantilla As Variant)
Dim HayItems As Boolean
Dim sDistrib As String
Dim iItems As Integer
Dim iItemsSin As Integer
Dim iItemID As Long
Dim oGrupo As CGrupo
Dim oItem As CItem
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
            HayItems = False
            iItems = 0
            iItemsSin = 0
            sDistrib = "" 'Con esta voy a comparar si todos los �tems tienen la misma dist para pasarla a proceso
            'Queda pendiente comprobar por grupo para que si no puede ser por proceso sea por grupo
            For Each oGrupo In m_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                    For Each oItem In oGrupo.Items
                        iItems = iItems + 1
                        If Not NoHayParametro(oItem.UON1Cod) Or Not NoHayParametro(oItem.UON2Cod) Or Not NoHayParametro(oItem.UON3Cod) Then
                            If sDistrib = "" Then
                                sDistrib = oItem.UON1Cod & "@#@" & NullToStr(oItem.UON2Cod) & "@#@" & NullToStr(oItem.UON3Cod)
                                iItemID = oItem.Id
                            ElseIf sDistrib <> oItem.UON1Cod & "@#@" & NullToStr(oItem.UON2Cod) & "@#@" & NullToStr(oItem.UON3Cod) Then
                                HayItems = True
                                Exit For
                            End If
                        Else
                            iItemsSin = iItemsSin + 1
                            'Compruebo que este art�culo sea v�lido para la distribuci�n que tenemos por si pasa a proceso
                            If sDistrib <> "" Then
                                Dim arrAux() As String
                                Dim oUON As IUon
                                arrAux = Split(sDistrib, "@#@")
                                Select Case UBound(arrAux)
                                Case 1
                                    Set oUON = createUon(arrAux(0), "", "")
                                Case 2
                                    Set oUON = createUon(arrAux(0), arrAux(1), "")
                                Case 3
                                    Set oUON = createUon(arrAux(0), arrAux(1), arrAux(2))
                                End Select
                                If ("" & oItem.ArticuloCod) <> "" Then 'Artic NO codificados-> todos uons. Cumple.
                                    If Not oItem.Articulo.uons.EsDistribuibleUON(oUON) Then
                                        HayItems = True
                                        Exit For
                                    End If
                                End If
                            End If
                        End If
                    Next
                    If HayItems Then Exit For
                End If
            Next
                   
            'Si la distribuci�n estaba a nivel de Item,
            'SI las distribuciones de todos los �tems sin iguales
            If Not HayItems And iItems <> iItemsSin Then
                If Not IsMissing(DistPlantilla) Then
                    If DistPlantilla = TipoDefinicionDatoProceso.EnProceso Then
                        m_oProceso.DefDistribUON = EnProceso
                    ElseIf DistPlantilla = TipoDefinicionDatoProceso.EnGrupo Then
                        m_oProceso.DefDistribUON = EnGrupo
                    ElseIf DistPlantilla = TipoDefinicionDatoProceso.NoDefinido Then
                        m_oProceso.DefDistribUON = EnProceso
                    End If
                Else
                    m_oProceso.DefDistribUON = EnProceso
                End If
                If m_oProceso.DefDistribUON = EnProceso Then
                    'Paso al proceso la distribucion de los items
                    If Not NoHayParametro(m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON3Cod) Then           'Nivel 3
                        Set m_oProceso.DistsNivel1 = Nothing
                        Set m_oProceso.DistsNivel2 = Nothing
                        Set m_oProceso.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                        m_oProceso.DistsNivel3.Add Nothing, m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON1Cod, m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON2Cod, m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON3Cod, 100, , 0
                    ElseIf Not NoHayParametro(m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON2Cod) Then       'Nivel 2
                            Set m_oProceso.DistsNivel1 = Nothing
                            Set m_oProceso.DistsNivel3 = Nothing
                            Set m_oProceso.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                            m_oProceso.DistsNivel2.Add Nothing, m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON1Cod, m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON2Cod, 100, , 0
                    ElseIf Not NoHayParametro(m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON1Cod) Then     'Nivel 1
                            Set m_oProceso.DistsNivel2 = Nothing
                            Set m_oProceso.DistsNivel3 = Nothing
                            Set m_oProceso.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                            m_oProceso.DistsNivel1.Add Nothing, m_oProceso.Grupos.Item(1).Items.Item(CStr(iItemID)).UON1Cod, 100, , 0
                    End If
                ElseIf m_oProceso.DefDistribUON = EnGrupo Then
                    'Paso al grupo la distribucion de los items
                    For Each oGrupo In m_oProceso.Grupos
                        oGrupo.DefDistribUON = True
                        If oGrupo.Items.Count > 0 Then
                            If Not NoHayParametro(oGrupo.Items.Item(CStr(iItemID)).UON3Cod) Then           'Nivel 3
                                Set oGrupo.DistsNivel1 = Nothing
                                Set oGrupo.DistsNivel2 = Nothing
                                Set oGrupo.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                                oGrupo.DistsNivel3.Add Nothing, oGrupo.Items.Item(CStr(iItemID)).UON1Cod, oGrupo.Items.Item(CStr(iItemID)).UON2Cod, oGrupo.Items.Item(CStr(iItemID)).UON3Cod, 100, , 0
                            ElseIf Not NoHayParametro(oGrupo.Items.Item(CStr(iItemID)).UON2Cod) Then       'Nivel 2
                                    Set oGrupo.DistsNivel1 = Nothing
                                    Set oGrupo.DistsNivel3 = Nothing
                                    Set oGrupo.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                                    oGrupo.DistsNivel2.Add Nothing, oGrupo.Items.Item(CStr(iItemID)).UON1Cod, oGrupo.Items.Item(CStr(iItemID)).UON2Cod, 100, , 0
                            ElseIf Not NoHayParametro(oGrupo.Items.Item(CStr(iItemID)).UON1Cod) Then     'Nivel 1
                                    Set oGrupo.DistsNivel2 = Nothing
                                    Set oGrupo.DistsNivel3 = Nothing
                                    Set oGrupo.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                                    oGrupo.DistsNivel1.Add Nothing, oGrupo.Items.Item(CStr(iItemID)).UON1Cod, 100, , 0
                            End If
                        End If
                    Next
                End If
            ElseIf iItems = iItemsSin Then 'ningun item tiene distribucion, pq no cumplia el nivel minimo o no ten�a
                m_oProceso.DefDistribUON = EnProceso
            End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "CambiarDistrDeItem", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
            
''' <summary>
''' Pone en las distribuciones que falten la distribuci�n del peticionario
''' </summary>
''' <remarks>Llamada desde: </remarks>
Private Sub EstablecerDistPeticionario()
Dim NivelPeticionario As Integer
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim oUON As IUon
Dim sUON1 As String
Dim sUON2 As String
Dim sUON3 As String
Dim uons3 As CUnidadesOrgNivel3
Dim uons2 As CUnidadesOrgNivel2
Dim uons1 As CUnidadesOrgNivel1

    'Nivel del Peticionario
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'bMostrar = False
sUON1 = NullToStr(m_oSolicitudSeleccionada.Peticionario.UON1)
sUON2 = NullToStr(m_oSolicitudSeleccionada.Peticionario.UON2)
sUON3 = NullToStr(m_oSolicitudSeleccionada.Peticionario.UON3)

Set oUON = createUon(sUON1, sUON2, sUON3)
Set uons1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
Set uons2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
Set uons3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
'Primero validaremos que la distribuci�n es v�lida para todos los items del proceso
For Each oGrupo In m_oProceso.Grupos
    If Not oGrupo.Items Is Nothing Then
        For Each oItem In oGrupo.Items
            If NoHayParametro(oItem.UON1Cod) Then
                If ("" & oItem.ArticuloCod) <> "" Then 'Artic NO codificados-> todos uons. Cumple.
                    If Not oItem.Articulo.uons.EsDistribuibleUON(oUON) Then
                        'No se traspasa la Distribucion porque no es v�lida para todos los Items
                        Exit Sub
                    End If
                End If
            End If
        Next
    End If
Next


Set oUON = Nothing
Set uons1 = Nothing
Set uons2 = Nothing
Set uons3 = Nothing

'Nivel del Peticionario
If Not IsNull(m_oSolicitudSeleccionada.Peticionario.UON1) And Not IsNull(m_oSolicitudSeleccionada.Peticionario.UON2) And Not IsNull(m_oSolicitudSeleccionada.Peticionario.UON3) Then
    NivelPeticionario = 3
Else
    If Not IsNull(m_oSolicitudSeleccionada.Peticionario.UON1) And Not IsNull(m_oSolicitudSeleccionada.Peticionario.UON2) Then
        NivelPeticionario = 2
    Else
        NivelPeticionario = 1
    End If
End If

'Una vez se tenga el proceso ya definido, para los casos que no se tengan rellenados, se establece la distribuci�n del peticionario si �sta es v�lida
If NivelPeticionario >= gParametrosGenerales.giNIVDIST Then
    Select Case m_oProceso.DefDistribUON
        Case EnProceso
            If m_oProceso.DistsNivel1 Is Nothing And m_oProceso.DistsNivel2 Is Nothing And m_oProceso.DistsNivel3 Is Nothing Then
                Select Case NivelPeticionario
                    Case 1
                        Set m_oProceso.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                        m_oProceso.DistsNivel1.Add Nothing, sUON1, 100, , 0
                    Case 2
                        Set m_oProceso.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                        m_oProceso.DistsNivel2.Add Nothing, sUON1, sUON2, 100, , 0
                    Case 3
                        Set m_oProceso.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                        m_oProceso.DistsNivel3.Add Nothing, sUON1, sUON2, sUON3, 100, , 0
                End Select
            End If
        

        Case EnGrupo
            For Each oGrupo In m_oProceso.Grupos
                If oGrupo.DistsNivel1 Is Nothing And oGrupo.DistsNivel2 Is Nothing And oGrupo.DistsNivel3 Is Nothing Then
                    Select Case NivelPeticionario
                        Case 1
                            Set oGrupo.DistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
                            oGrupo.DistsNivel1.Add Nothing, sUON1, 100, , 0
                        Case 2
                            Set oGrupo.DistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
                            oGrupo.DistsNivel2.Add Nothing, sUON1, sUON2, 100, , 0
                        Case 3
                            Set oGrupo.DistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
                            oGrupo.DistsNivel3.Add Nothing, sUON1, sUON2, sUON3, 100, , 0
                    End Select
                End If
            Next
        
        
        Case EnItem
            For Each oGrupo In m_oProceso.Grupos
                If Not oGrupo.Items Is Nothing Then
                For Each oItem In oGrupo.Items
                    If NoHayParametro(oItem.UON1Cod) And NoHayParametro(oItem.UON2Cod) And NoHayParametro(oItem.UON3Cod) Then
                        Set oUON = createUon(sUON1, sUON2, sUON3)
                        If ("" & oItem.ArticuloCod) = "" Then 'Artic NO codificados-> todos uons. Cumple.
                            Select Case NivelPeticionario
                                Case 1
                                    oItem.UON1Cod = sUON1
                                Case 2
                                    oItem.UON1Cod = sUON1
                                    oItem.UON2Cod = sUON2
                                Case 3
                                    oItem.UON1Cod = sUON1
                                    oItem.UON2Cod = sUON2
                                    oItem.UON3Cod = sUON3
                            End Select
                        ElseIf oItem.Articulo.uons.EsDistribuibleUON(oUON) Then
                            Select Case NivelPeticionario
                                Case 1
                                    oItem.UON1Cod = sUON1
                                Case 2
                                    oItem.UON1Cod = sUON1
                                    oItem.UON2Cod = sUON2
                                Case 3
                                    oItem.UON1Cod = sUON1
                                    oItem.UON2Cod = sUON2
                                    oItem.UON3Cod = sUON3
                            End Select
                        End If
                    End If
                Next
                End If
            Next
    End Select
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "EstablecerDistPeticionario", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
''' <summary>
''' Comprueba si los articulos son validos para una distribucion
''' </summary>
''' <remarks>Llamada desde: GenerarProcesoPlantilla</remarks>
Private Function ComprobarDistArticulos(ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String)
Dim oUON As IUon
Dim oGrupo As CGrupo
Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

Set oUON = createUon(sUON1, sUON2, sUON3)

ComprobarDistArticulos = True
''Primero validaremos que la distribuci�n es v�lida para todos los items del proceso
For Each oGrupo In m_oProceso.Grupos
    If Not oGrupo.Items Is Nothing Then
        If Not oGrupo.Items Is Nothing Then
        For Each oItem In oGrupo.Items
            If ("" & oItem.ArticuloCod) <> "" Then 'Artic NO codificados-> todos uons. Cumple.
                If Not oItem.Articulo.uons.EsDistribuibleUON(oUON) Then
                    'No se traspasa la Distribucion porque no es v�lida para todos los Items
                    ComprobarDistArticulos = False
                    Exit Function
                End If
            End If
        Next
        End If
    End If
Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "ComprobarDistArticulos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


Private Function DevolverGrupoPlantillaParaItem(ByVal sCodGrupo As String, ByVal sDenGrupo As String, ByVal lIdGrupo As Long) As CGrupo
Dim oGrupo As CGrupo
Dim iGrupoPos As Integer
Dim k As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oGrupo = Nothing
If m_oPlantillaSeleccionada.Grupos.Count = 0 Then
    Set oGrupo = m_oProceso.Grupos.Item(sCodGrupo)

Else
    If ssTabGrupos.Tabs.Count = 1 Then
        'Si la solicitud solo tiene 1 grupo, por defecto el 1� grupo de la plantilla
            Set oGrupo = m_oPlantillaSeleccionada.Grupos.Item(1)

    Else
        If ssTabGrupos.Tabs.Count > 1 Then
            For k = 1 To m_oPlantillaSeleccionada.Grupos.Count
                'Se busca si algun grupo de la plantilla tiene el mismo cod o den que el de la solicitud
                If sCodGrupo = m_oPlantillaSeleccionada.Grupos.Item(k).Codigo Or sDenGrupo = m_oPlantillaSeleccionada.Grupos.Item(k).Den Then
                    Set oGrupo = m_oPlantillaSeleccionada.Grupos.Item(k)
                    Exit For
                Else
                'Sino, se asigna el de la misma posici�n
                    If lIdGrupo = m_oPlantillaSeleccionada.Grupos.Item(k).Id Then
                        iGrupoPos = k
                    End If
                End If
            Next k
            If oGrupo Is Nothing Then 'Le asigno el de la posicion si no ha encontrado alguno por cod-den
                If iGrupoPos <> 0 Then
                    Set oGrupo = m_oPlantillaSeleccionada.Grupos.Item(iGrupoPos)
                Else
                    '''La solicitud tiene mas grupos que la plantilla:se asigna el �ltimo grupo de la plantilla
                    '''Set oGrupo = m_oPlantillaSeleccionada.Grupos.Item(m_oPlantillaSeleccionada.Grupos.Count)
                    'Pruebas 32100.3 / 2016 / 62
                    Set oGrupo = m_oProceso.Grupos.Item(sCodGrupo)
                End If
            End If
        End If
    End If
End If
Set DevolverGrupoPlantillaParaItem = oGrupo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSOLAbrirProc", "DevolverGrupoPlantillaParaItem", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function





