VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosAnyadirEtapa 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DAnyadir Etapa"
   ClientHeight    =   3495
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5805
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosAnyadirEtapa.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3495
   ScaleWidth      =   5805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1440
      ScaleHeight     =   420
      ScaleWidth      =   2625
      TabIndex        =   8
      Top             =   2970
      Width           =   2625
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   6
         Top             =   120
         Width           =   1050
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   7
         Top             =   120
         Width           =   1050
      End
   End
   Begin VB.Frame fraBloque 
      BackColor       =   &H00808000&
      Caption         =   "DBloque"
      ForeColor       =   &H00FFFFFF&
      Height          =   2790
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5595
      Begin VB.CheckBox chkEtapaInt 
         BackColor       =   &H00808000&
         Caption         =   "DEtapa Integraci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   330
         MaskColor       =   &H00FFFFFF&
         TabIndex        =   9
         Top             =   2310
         Width           =   2235
      End
      Begin VB.OptionButton optBloque 
         BackColor       =   &H00808000&
         Caption         =   "DNuevo"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   300
         TabIndex        =   3
         Top             =   1110
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.OptionButton optBloque 
         BackColor       =   &H00808000&
         Caption         =   "DExistente"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   300
         TabIndex        =   1
         Top             =   270
         Width           =   1815
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcBloques 
         Height          =   285
         Left            =   780
         TabIndex        =   2
         Top             =   630
         Width           =   4575
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8070
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   8070
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         Enabled         =   0   'False
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgDen 
         Height          =   945
         Left            =   1740
         TabIndex        =   5
         Top             =   1365
         Width           =   3735
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   1
         BorderStyle     =   0
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD_IDIOMA"
         Columns(0).Name =   "COD_IDIOMA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DEN_IDIOMA"
         Columns(1).Name =   "DEN_IDIOMA"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).ForeColor=   16777215
         Columns(1).BackColor=   8421376
         Columns(2).Width=   3200
         Columns(2).Caption=   "VALOR_IDIOMA"
         Columns(2).Name =   "VALOR_IDIOMA"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   50
         _ExtentX        =   6588
         _ExtentY        =   1667
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblNombre 
         BackColor       =   &H00808000&
         Caption         =   "DNombre"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   780
         TabIndex        =   4
         Top             =   1515
         Width           =   1065
      End
   End
End
Attribute VB_Name = "frmFlujosAnyadirEtapa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public lIdWorkFlow As Long
Public oBloque As CBloque

Private oBloques As CBloques
Private m_oIBaseDatos As IBaseDatos
Private lIdBloque_Sel As Long

Private m_sMensajeDenominacion As String

Private oIdiomas As CIdiomas
Private oDenominaciones As CMultiidiomas



Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    
    If sdbgDen.DataChanged Then
        sdbgDen.Update
    End If
    
    If optBloque(0).Value Then
        If lIdBloque_Sel > 0 Then
            Set oBloque = oFSGSRaiz.Generar_CBloque
            oBloque.Id = lIdBloque_Sel
            Set m_oIBaseDatos = oBloque
            teserror = m_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Set m_oIBaseDatos = Nothing
                Exit Sub
            Else
                Set m_oIBaseDatos = Nothing
            End If
        End If
    ElseIf optBloque(1).Value Then
        If oDenominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den = "" Then
            oMensajes.NoValido m_sMensajeDenominacion & " " & oGestorParametros.DevolverIdiomas.Item(basPublic.gParametrosInstalacion.gIdioma).Den
            Exit Sub
        End If
        Set oBloque = oFSGSRaiz.Generar_CBloque
        Set oBloque.Denominaciones = oDenominaciones
    End If
    
    If Not oBloque Is Nothing Then
        oBloque.EtapaInt = BooleanToSQLBinary(chkEtapaInt.Value)
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
        Me.Hide 'Lo descargo en el Formulario que lo ha llamado
    End If
    
End Sub

Private Sub cmdCancelar_Click()
    
    Set oBloque = Nothing
    Me.Hide 'Lo descargo en el Formulario que lo ha llamado
End Sub

Private Sub Form_Load()
    Me.Top = frmFlujos.Top + (frmFlujos.Height / 2) - (Me.Height / 2)
    If Me.Top < 0 Then
        Me.Top = 0
    End If
    Me.Left = frmFlujos.Left + (frmFlujos.Width / 2) - (Me.Width / 2)
    If Me.Left < 0 Then
        Me.Left = 0
    End If
    CargarRecursos
    
    CargarIdiomas
       
End Sub

Private Sub CargarRecursos()
   Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSANYADIRETAPA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1
        Ador.MoveNext
        fraBloque.caption = Ador(0).Value
        Ador.MoveNext
        optBloque(0).caption = Ador(0).Value
        Ador.MoveNext
        optBloque(1).caption = Ador(0).Value
        Ador.MoveNext
        lblNombre.caption = Ador(0).Value '5
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sMensajeDenominacion = Ador(0).Value
        Ador.MoveNext
        chkEtapaInt.caption = Ador(0).Value
        Ador.Close
    End If
End Sub

Private Sub CargarIdiomas()
    Dim oIdioma As CIdioma
    
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    
    For Each oIdioma In oIdiomas
        oDenominaciones.Add oIdioma.Cod, ""
    Next
    Set oIdioma = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oBloque = Nothing
    Set oBloques = Nothing
    Set oIdiomas = Nothing
    Set oDenominaciones = Nothing
    Set m_oIBaseDatos = Nothing
    
End Sub

Private Sub optBloque_Click(Index As Integer)
    sdbcBloques.Enabled = optBloque(0).Value
    sdbgDen.Enabled = optBloque(1).Value
End Sub

Private Sub sdbcBloques_CloseUp()
    If sdbcBloques.Value = "" Then
        lIdBloque_Sel = 0
    Else
        lIdBloque_Sel = sdbcBloques.Value
    End If
   
End Sub

Private Sub sdbcBloques_DropDown()
        
    Screen.MousePointer = vbHourglass

    Set oBloques = oFSGSRaiz.Generar_CBloques
    oBloques.CargarBloques basPublic.gParametrosInstalacion.gIdioma, lIdWorkFlow
        
    sdbcBloques.RemoveAll
    For Each oBloque In oBloques
        If oBloque.Tipo = TipoBloque.Intermedio Then
            sdbcBloques.AddItem oBloque.Id & Chr(9) & oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
        End If
    Next
    Set oBloque = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcBloques_InitColumnProps()
    sdbcBloques.DataFieldList = "Column 0"
    sdbcBloques.DataFieldToDisplay = "Column 1"

End Sub

Private Sub sdbcBloques_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcBloques.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcBloques.Rows - 1
            bm = sdbcBloques.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcBloques.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcBloques.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbgDen_LostFocus()
    If sdbgDen.DataChanged Then
        sdbgDen.Update
    End If
End Sub

Private Sub sdbgDen_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)
    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    Dim r As Integer
    Dim i As Integer
    
    Dim iNumIdiomas As Integer
    Dim m_lDenPointer As Long
        
    '''CargarIdiomas
    If oIdiomas Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If
  
    iNumIdiomas = oIdiomas.Count
    
    'Se mostrar� scroll vertical solo si hay m�s de 4 idiomas
    sdbgDen.ScrollBars = 0      '0-ssScrollBarsNone
    If iNumIdiomas > 4 Then
    sdbgDen.ScrollBars = 2      '2-ssScrollBarsVertical
    End If
    
    If IsNull(StartLocation) Then                   'If the grid is empty then
        If ReadPriorRows Then                           'If moving backwards through grid then
            m_lDenPointer = iNumIdiomas - 1                 'pointer = # of last grid row
        Else                                            'else
            m_lDenPointer = 1                               'pointer = # of first grid row
        End If
    Else                                            'If the grid already has data in it then
        m_lDenPointer = StartLocation                   'pointer = location just before or after the row where data will be added
        If ReadPriorRows Then                           'If moving backwards through grid then
            m_lDenPointer = m_lDenPointer - 1           'move pointer back one row
        Else                                            'else
            m_lDenPointer = m_lDenPointer + 1           'move pointer ahead one row
        End If
    End If
    
    'The pointer (m_lDenPointer) now points to the row of the grid where you will start adding data.
    For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
        If (m_lDenPointer < 0) Or (m_lDenPointer > iNumIdiomas) Then
            Exit For                                        'If the pointer is outside the grid then stop this
        End If
    
        RowBuf.Value(i, 0) = oIdiomas.Item(m_lDenPointer).Cod
        RowBuf.Value(i, 1) = oIdiomas.Item(m_lDenPointer).Den
        RowBuf.Value(i, 2) = oDenominaciones.Item(m_lDenPointer).Den
        RowBuf.Bookmark(i) = m_lDenPointer                  'set the value of the bookmark for the current row in the rowbuffer
    
        If ReadPriorRows Then                               'move the pointer forward or backward, depending
            m_lDenPointer = m_lDenPointer - 1                  'on which way it's supposed to move
        Else
            m_lDenPointer = m_lDenPointer + 1
        End If
        r = r + 1                                           'increment the number of rows read
    Next
    
    RowBuf.RowCount = r                                  'set the size of the row buffer to the number of rows read
End Sub

Private Sub sdbgDen_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    If Not (WriteLocation < 1) And Not (WriteLocation > oDenominaciones.Count) Then
        oDenominaciones.Item(WriteLocation).Den = RowBuf.Value(0, 2)
    End If
End Sub
