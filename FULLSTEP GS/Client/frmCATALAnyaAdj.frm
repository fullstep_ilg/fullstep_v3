VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmCATALAnyaAdj 
   BackColor       =   &H00808000&
   Caption         =   "Cat�logo de aprovisionamiento - A�adir �tems"
   ClientHeight    =   8985
   ClientLeft      =   3330
   ClientTop       =   855
   ClientWidth     =   10155
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATALAnyaAdj.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8985
   ScaleWidth      =   10155
   Begin VB.CheckBox chkTodos 
      Caption         =   "DPedir todos"
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   2040
      Width           =   1455
   End
   Begin VB.PictureBox picDatosPedido 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   855
      Left            =   0
      ScaleHeight     =   855
      ScaleWidth      =   10215
      TabIndex        =   14
      Top             =   0
      Width           =   10215
      Begin VB.Label lblOrgCompra 
         BackStyle       =   0  'Transparent
         Caption         =   "DOrgCompra"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   5400
         TabIndex        =   22
         Top             =   495
         Width           =   1395
      End
      Begin VB.Label lblDOrgCompra 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   6840
         TabIndex        =   21
         Top             =   480
         Width           =   3210
      End
      Begin VB.Label lblEmpresa 
         BackStyle       =   0  'Transparent
         Caption         =   "DEmpresa"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   5400
         TabIndex        =   20
         Top             =   120
         Width           =   1395
      End
      Begin VB.Label lblDEmpresa 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   6840
         TabIndex        =   19
         Top             =   120
         Width           =   3210
      End
      Begin VB.Label lblProve 
         BackStyle       =   0  'Transparent
         Caption         =   "DProveedor"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   495
         Width           =   1395
      End
      Begin VB.Label lblDProve 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1440
         TabIndex        =   17
         Top             =   480
         Width           =   3690
      End
      Begin VB.Label lblPedido 
         BackStyle       =   0  'Transparent
         Caption         =   "DPedido"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   135
         Width           =   1395
      End
      Begin VB.Label lblDPedido 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1440
         TabIndex        =   15
         Top             =   120
         Width           =   3210
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAdjudicaciones 
      Height          =   3585
      Left            =   60
      TabIndex        =   13
      Top             =   2385
      Width           =   10020
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   50
      stylesets.count =   6
      stylesets(0).Name=   "GreyImg"
      stylesets(0).BackColor=   12632256
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCATALAnyaAdj.frx":0CB2
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCATALAnyaAdj.frx":0EF0
      stylesets(2).Name=   "NoSelected"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   16777215
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmCATALAnyaAdj.frx":0F0C
      stylesets(3).Name=   "Selected"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   8388608
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmCATALAnyaAdj.frx":0F28
      stylesets(4).Name=   "Grey"
      stylesets(4).BackColor=   12632256
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmCATALAnyaAdj.frx":0F44
      stylesets(5).Name=   "NoHom"
      stylesets(5).ForeColor=   16777215
      stylesets(5).BackColor=   12632256
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmCATALAnyaAdj.frx":0F60
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns.Count   =   50
      Columns(0).Width=   1058
      Columns(0).Caption=   "DPedir"
      Columns(0).Name =   "SEL"
      Columns(0).Alignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   11
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   847
      Columns(1).Caption=   "Hom."
      Columns(1).Name =   "HOM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   11
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).Style=   2
      Columns(2).Width=   2990
      Columns(2).Caption=   "Art�culo"
      Columns(2).Name =   "ART"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3254
      Columns(3).Caption=   "Proveedor"
      Columns(3).Name =   "PROVE"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Caption=   "DUnidades org."
      Columns(4).Name =   "UON_DEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1455
      Columns(5).Caption=   "Destino"
      Columns(5).Name =   "DEST"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1826
      Columns(6).Caption=   "Cantidad"
      Columns(6).Name =   "CANT"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   5
      Columns(6).NumberFormat=   "Standard"
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   1244
      Columns(7).Caption=   "Unidad"
      Columns(7).Name =   "UNI"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Caption=   "DCant. adj."
      Columns(8).Name =   "CANT_ADJ"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3201
      Columns(9).Caption=   "DCant. pedida"
      Columns(9).Name =   "CANT_PED"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3200
      Columns(10).Caption=   "DImporte adj."
      Columns(10).Name=   "IMP_ADJ"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Caption=   "DImporte pedido"
      Columns(11).Name=   "IMP_PED"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   1588
      Columns(12).Caption=   "P.U."
      Columns(12).Name=   "PU"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   5
      Columns(12).NumberFormat=   "#,##0.0####################"
      Columns(12).FieldLen=   256
      Columns(12).Locked=   -1  'True
      Columns(13).Width=   1376
      Columns(13).Caption=   "MON"
      Columns(13).Name=   "MON"
      Columns(13).Alignment=   2
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(14).Width=   3149
      Columns(14).Caption=   "Suministro"
      Columns(14).Name=   "SUMI"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(14).Locked=   -1  'True
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "ITEM"
      Columns(15).Name=   "ITEM"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "ARTCOD"
      Columns(16).Name=   "ARTCOD"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      Columns(17).Width=   3200
      Columns(17).Visible=   0   'False
      Columns(17).Caption=   "ARTDEN"
      Columns(17).Name=   "ARTDEN"
      Columns(17).DataField=   "Column 17"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      Columns(17).Locked=   -1  'True
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "PROVECOD"
      Columns(18).Name=   "PROVECOD"
      Columns(18).DataField=   "Column 18"
      Columns(18).DataType=   8
      Columns(18).FieldLen=   256
      Columns(18).Locked=   -1  'True
      Columns(19).Width=   3200
      Columns(19).Visible=   0   'False
      Columns(19).Caption=   "PROVEINST"
      Columns(19).Name=   "PROVEINSTWEB"
      Columns(19).DataField=   "Column 19"
      Columns(19).DataType=   8
      Columns(19).FieldLen=   256
      Columns(19).Locked=   -1  'True
      Columns(20).Width=   3200
      Columns(20).Visible=   0   'False
      Columns(20).Caption=   "PRES"
      Columns(20).Name=   "PRES"
      Columns(20).DataField=   "Column 20"
      Columns(20).DataType=   8
      Columns(20).FieldLen=   256
      Columns(20).Locked=   -1  'True
      Columns(21).Width=   3200
      Columns(21).Visible=   0   'False
      Columns(21).Caption=   "SOLICIT"
      Columns(21).Name=   "SOLICIT"
      Columns(21).DataField=   "Column 21"
      Columns(21).DataType=   8
      Columns(21).FieldLen=   256
      Columns(21).Locked=   -1  'True
      Columns(22).Width=   3200
      Columns(22).Visible=   0   'False
      Columns(22).Caption=   "GENERICO"
      Columns(22).Name=   "GENERICO"
      Columns(22).DataField=   "Column 22"
      Columns(22).DataType=   8
      Columns(22).FieldLen=   256
      Columns(22).Locked=   -1  'True
      Columns(23).Width=   3200
      Columns(23).Visible=   0   'False
      Columns(23).Caption=   "TIPORECEPCION"
      Columns(23).Name=   "TIPORECEPCION"
      Columns(23).DataField=   "Column 23"
      Columns(23).DataType=   8
      Columns(23).FieldLen=   256
      Columns(23).Locked=   -1  'True
      Columns(24).Width=   3200
      Columns(24).Visible=   0   'False
      Columns(24).Caption=   "CONCEPTO"
      Columns(24).Name=   "CONCEPTO"
      Columns(24).DataField=   "Column 24"
      Columns(24).DataType=   8
      Columns(24).FieldLen=   256
      Columns(24).Locked=   -1  'True
      Columns(25).Width=   3200
      Columns(25).Visible=   0   'False
      Columns(25).Caption=   "RECEPCIONAR"
      Columns(25).Name=   "RECEPCIONAR"
      Columns(25).DataField=   "Column 25"
      Columns(25).DataType=   8
      Columns(25).FieldLen=   256
      Columns(25).Locked=   -1  'True
      Columns(26).Width=   3200
      Columns(26).Visible=   0   'False
      Columns(26).Caption=   "ALMACENAR"
      Columns(26).Name=   "ALMACENAR"
      Columns(26).DataField=   "Column 26"
      Columns(26).DataType=   8
      Columns(26).FieldLen=   256
      Columns(26).Locked=   -1  'True
      Columns(27).Width=   3200
      Columns(27).Visible=   0   'False
      Columns(27).Caption=   "UON1"
      Columns(27).Name=   "UON1"
      Columns(27).DataField=   "Column 27"
      Columns(27).DataType=   8
      Columns(27).FieldLen=   256
      Columns(27).Locked=   -1  'True
      Columns(28).Width=   3200
      Columns(28).Visible=   0   'False
      Columns(28).Caption=   "UON2"
      Columns(28).Name=   "UON2"
      Columns(28).DataField=   "Column 28"
      Columns(28).DataType=   8
      Columns(28).FieldLen=   256
      Columns(28).Locked=   -1  'True
      Columns(29).Width=   3200
      Columns(29).Visible=   0   'False
      Columns(29).Caption=   "UON3"
      Columns(29).Name=   "UON3"
      Columns(29).DataField=   "Column 29"
      Columns(29).DataType=   8
      Columns(29).FieldLen=   256
      Columns(29).Locked=   -1  'True
      Columns(30).Width=   3200
      Columns(30).Visible=   0   'False
      Columns(30).Caption=   "EMPRESA"
      Columns(30).Name=   "EMPRESA"
      Columns(30).DataField=   "Column 30"
      Columns(30).DataType=   8
      Columns(30).FieldLen=   256
      Columns(30).Locked=   -1  'True
      Columns(31).Width=   3200
      Columns(31).Visible=   0   'False
      Columns(31).Caption=   "ORGCOMPRAS"
      Columns(31).Name=   "ORGCOMPRAS"
      Columns(31).DataField=   "Column 31"
      Columns(31).DataType=   8
      Columns(31).FieldLen=   256
      Columns(31).Locked=   -1  'True
      Columns(32).Width=   3200
      Columns(32).Visible=   0   'False
      Columns(32).Caption=   "GMN1"
      Columns(32).Name=   "GMN1"
      Columns(32).DataField=   "Column 32"
      Columns(32).DataType=   8
      Columns(32).FieldLen=   256
      Columns(32).Locked=   -1  'True
      Columns(33).Width=   3200
      Columns(33).Visible=   0   'False
      Columns(33).Caption=   "GMN2"
      Columns(33).Name=   "GMN2"
      Columns(33).DataField=   "Column 33"
      Columns(33).DataType=   8
      Columns(33).FieldLen=   256
      Columns(33).Locked=   -1  'True
      Columns(34).Width=   3200
      Columns(34).Visible=   0   'False
      Columns(34).Caption=   "GMN3"
      Columns(34).Name=   "GMN3"
      Columns(34).DataField=   "Column 34"
      Columns(34).DataType=   8
      Columns(34).FieldLen=   256
      Columns(34).Locked=   -1  'True
      Columns(35).Width=   3200
      Columns(35).Visible=   0   'False
      Columns(35).Caption=   "GMN4"
      Columns(35).Name=   "GMN4"
      Columns(35).DataField=   "Column 35"
      Columns(35).DataType=   8
      Columns(35).FieldLen=   256
      Columns(35).Locked=   -1  'True
      Columns(36).Width=   3200
      Columns(36).Visible=   0   'False
      Columns(36).Caption=   "COD_EXT"
      Columns(36).Name=   "COD_EXT"
      Columns(36).DataField=   "Column 36"
      Columns(36).DataType=   8
      Columns(36).FieldLen=   256
      Columns(36).Locked=   -1  'True
      Columns(37).Width=   3200
      Columns(37).Visible=   0   'False
      Columns(37).Caption=   "FEC_INI"
      Columns(37).Name=   "FEC_INI"
      Columns(37).DataField=   "Column 37"
      Columns(37).DataType=   8
      Columns(37).FieldLen=   256
      Columns(37).Locked=   -1  'True
      Columns(38).Width=   3200
      Columns(38).Visible=   0   'False
      Columns(38).Caption=   "FEC_FIN"
      Columns(38).Name=   "FEC_FIN"
      Columns(38).DataField=   "Column 38"
      Columns(38).DataType=   8
      Columns(38).FieldLen=   256
      Columns(38).Locked=   -1  'True
      Columns(39).Width=   3200
      Columns(39).Visible=   0   'False
      Columns(39).Caption=   "PREC_VALIDO"
      Columns(39).Name=   "PREC_VALIDO"
      Columns(39).DataField=   "Column 39"
      Columns(39).DataType=   8
      Columns(39).FieldLen=   256
      Columns(39).Locked=   -1  'True
      Columns(40).Width=   3200
      Columns(40).Visible=   0   'False
      Columns(40).Caption=   "MONEDAPROCESO"
      Columns(40).Name=   "MONEDAPROCESO"
      Columns(40).DataField=   "Column 40"
      Columns(40).DataType=   8
      Columns(40).FieldLen=   256
      Columns(40).Locked=   -1  'True
      Columns(41).Width=   3200
      Columns(41).Visible=   0   'False
      Columns(41).Caption=   "OFE_CAMBIO"
      Columns(41).Name=   "OFE_CAMBIO"
      Columns(41).DataField=   "Column 41"
      Columns(41).DataType=   8
      Columns(41).FieldLen=   256
      Columns(41).Locked=   -1  'True
      Columns(42).Width=   3200
      Columns(42).Visible=   0   'False
      Columns(42).Caption=   "GRUPO"
      Columns(42).Name=   "GRUPO"
      Columns(42).DataField=   "Column 42"
      Columns(42).DataType=   8
      Columns(42).FieldLen=   256
      Columns(42).Locked=   -1  'True
      Columns(43).Width=   3200
      Columns(43).Visible=   0   'False
      Columns(43).Caption=   "GRUPO_COD"
      Columns(43).Name=   "GRUPO_COD"
      Columns(43).DataField=   "Column 43"
      Columns(43).DataType=   8
      Columns(43).FieldLen=   256
      Columns(44).Width=   3200
      Columns(44).Visible=   0   'False
      Columns(44).Caption=   "LINEA_SOLICIT"
      Columns(44).Name=   "LINEA_SOLICIT"
      Columns(44).DataField=   "Column 44"
      Columns(44).DataType=   8
      Columns(44).FieldLen=   256
      Columns(44).Locked=   -1  'True
      Columns(45).Width=   3200
      Columns(45).Visible=   0   'False
      Columns(45).Caption=   "CAMPO_SOLICIT"
      Columns(45).Name=   "CAMPO_SOLICIT"
      Columns(45).DataField=   "Column 45"
      Columns(45).DataType=   8
      Columns(45).FieldLen=   256
      Columns(45).Locked=   -1  'True
      Columns(46).Width=   3200
      Columns(46).Visible=   0   'False
      Columns(46).Caption=   "IMPUTACIONES"
      Columns(46).Name=   "IMPUTACIONES"
      Columns(46).DataField=   "Column 46"
      Columns(46).DataType=   8
      Columns(46).FieldLen=   256
      Columns(46).Locked=   -1  'True
      Columns(47).Width=   3200
      Columns(47).Visible=   0   'False
      Columns(47).Caption=   "USARESCALADOS"
      Columns(47).Name=   "USARESCALADOS"
      Columns(47).DataField=   "Column 47"
      Columns(47).DataType=   8
      Columns(47).FieldLen=   256
      Columns(47).Locked=   -1  'True
      Columns(48).Width=   3200
      Columns(48).Visible=   0   'False
      Columns(48).Caption=   "NUMESCADJ"
      Columns(48).Name=   "NUMESCADJ"
      Columns(48).DataField=   "Column 48"
      Columns(48).DataType=   8
      Columns(48).FieldLen=   256
      Columns(48).Locked=   -1  'True
      Columns(49).Width=   3200
      Columns(49).Visible=   0   'False
      Columns(49).Caption=   "ESC"
      Columns(49).Name=   "ESC"
      Columns(49).DataField=   "Column 49"
      Columns(49).DataType=   8
      Columns(49).FieldLen=   256
      Columns(49).Locked=   -1  'True
      _ExtentX        =   17674
      _ExtentY        =   6324
      _StockProps     =   79
      Caption         =   "Seleccione los �tems que desee entre las adjudicaciones y haga ""clic"" en A�adir para continuar..."
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.TabStrip sstabCat 
      Height          =   4335
      Left            =   60
      TabIndex        =   12
      Top             =   1620
      Width           =   10005
      _ExtentX        =   17648
      _ExtentY        =   7646
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   1
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   ""
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   345
      Left            =   5880
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   6120
      Width           =   1185
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&A�adir"
      Height          =   345
      Left            =   4440
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   6120
      Width           =   1305
   End
   Begin VB.CommandButton cmdSeleccionar 
      Caption         =   "&Seleccionar todos"
      Height          =   345
      Left            =   2760
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   6120
      Width           =   1545
   End
   Begin VB.Frame fraProceso 
      BackColor       =   &H00808000&
      Caption         =   "Proceso"
      ForeColor       =   &H8000000E&
      Height          =   675
      Left            =   60
      TabIndex        =   0
      Top             =   840
      Width           =   10005
      Begin VB.CommandButton cmdBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9550
         Picture         =   "frmCATALAnyaAdj.frx":0F7C
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   250
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   800
         TabIndex        =   1
         Top             =   250
         Width           =   900
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1587
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   2500
         TabIndex        =   3
         Top             =   250
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
         Height          =   285
         Left            =   4500
         TabIndex        =   5
         Top             =   250
         Width           =   1065
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1429
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7408
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "F.Reapertura"
         Columns(2).Name =   "FECREAPERT"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
         Height          =   285
         Left            =   5500
         TabIndex        =   6
         Top             =   250
         Width           =   4000
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   6112
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1402
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "F.Reapertura"
         Columns(2).Name =   "FECREAPERT"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7056
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblProceCod 
         BackStyle       =   0  'Transparent
         Caption         =   "Proceso:"
         ForeColor       =   &H8000000E&
         Height          =   180
         Left            =   3700
         TabIndex        =   7
         Top             =   300
         Width           =   855
      End
      Begin VB.Label lblGMN1_4 
         BackStyle       =   0  'Transparent
         Caption         =   "Comm:"
         ForeColor       =   &H8000000E&
         Height          =   225
         Left            =   1900
         TabIndex        =   4
         Top             =   300
         Width           =   525
      End
      Begin VB.Label lblAnyo 
         BackStyle       =   0  'Transparent
         Caption         =   "A�o:"
         ForeColor       =   &H8000000E&
         Height          =   225
         Left            =   150
         TabIndex        =   2
         Top             =   300
         Width           =   570
      End
   End
   Begin VB.Label lblTDOrgCompra 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5760
      TabIndex        =   27
      Top             =   7680
      Width           =   3210
   End
   Begin VB.Label lblTDEmpresa 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5760
      TabIndex        =   26
      Top             =   7200
      Width           =   3210
   End
   Begin VB.Label lblTDProve 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   480
      TabIndex        =   25
      Top             =   7680
      Width           =   3690
   End
   Begin VB.Label lblTDPedido 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   480
      TabIndex        =   24
      Top             =   7200
      Width           =   3210
   End
End
Attribute VB_Name = "frmCATALAnyaAdj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Event OnAnyaAdjSeleccionadas(ByRef oProceso As CProceso, ByRef oLineasNuevas As CLineasCatalogo)

Public g_oOrdenEntrega As COrdenEntrega
Public g_bProcesoPrecargado As Boolean
Public g_bParaPedido As Boolean
Public g_bDesdeSolicitud As Boolean
Public m_bOrigenEnviarPedidoAdj As Boolean

'Variables para func. combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private GMN1RespetarCombo As Boolean
Private GMN1CargarComboDesde As Boolean

'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4

'Proceso seleccionado
Public oProcesoSeleccionado As CProceso
' Coleccion de procesos a cargar
Private oProcesos As CProcesos

Private oIBaseDatos As IBaseDatos
Private m_bEscalados As Boolean
Private m_oLineasAgregadas As Dictionary

'Variables de seguridad
Private m_bRMat As Boolean
Private m_bRAsig As Boolean
Private m_bRCompResp As Boolean
Private m_bREqpAsig As Boolean
Private m_bRUsuAper As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bAnyadirLineasDesdeAdj As Boolean

'Imputaciones
Private m_oArbolCabecera As CParametroSM
Private m_bImputaPedido As Boolean
Private m_bNivelCabe As Boolean

'Variables de idiomas
Private sIdiProceso As String
Private bUnloadForm As Boolean

'Textos
Private m_sPedInv_ArtGasto As String
Private m_sPedGasto_ArtInv As String
Private m_sPedObliAlm_ArtNoAlm As String
Private m_sPedNoAlm_ArtObliAlm As String
Private m_sPedObliRec_ArtNoRec As String
Private m_sPedNoRec_ArtObliRec As String
Private m_sEmpresaIncorrecta As String
Private m_sOrgComprasIncorrecta As String
Private m_sSinPedidoSeleccionado As String
Private m_sCaptionDesdeSeguimiento As String
Private m_sCaptionDesdeCatalogo As String
Private m_sProveedorNoValido As String
Private m_sImputacionIncorrecta As String
Private m_sItemPorImporte As String
Private m_sItemPorCantidad As String

Private Sub Arrange()
    On Error Resume Next
    
    If Me.Width <= 10380 Then
        Me.Width = 10380
    End If
    
    lblOrgCompra.Visible = gParametrosGenerales.gbUsarOrgCompras
    lblDOrgCompra.Visible = gParametrosGenerales.gbUsarOrgCompras
    If Not g_bParaPedido Then
        picDatosPedido.Visible = False
        fraProceso.Top = picDatosPedido.Top
        
        lblTDPedido.Visible = False
        lblTDProve.Visible = False
        lblTDEmpresa.Visible = False
        lblTDOrgCompra.Visible = False
    End If
    picDatosPedido.Enabled = (g_bParaPedido And g_bDesdeSolicitud)
    If Not (g_bParaPedido And g_bDesdeSolicitud) Then
        'Si picDatosPedido no est� habilitado no funcionan los tooltips de los controles que contiene
        'Se ponen unos labels transparentes por encima de estos controles y en la misma posici�n con sus tooltips para poder ofrecer esta funcionalidad
        lblTDPedido.Left = lblDPedido.Left
        lblTDPedido.Top = lblDPedido.Top
        lblTDProve.Left = lblDProve.Left
        lblTDProve.Top = lblDProve.Top
        lblTDEmpresa.Left = lblDEmpresa.Left
        lblTDEmpresa.Top = lblDEmpresa.Top
        lblTDOrgCompra.Left = lblDOrgCompra.Left
        lblTDOrgCompra.Top = lblDOrgCompra.Top
        
        lblTDPedido.BackStyle = BackStyleConstants.cc2BackstyleTransparent
        lblTDProve.BackStyle = BackStyleConstants.cc2BackstyleTransparent
        lblTDEmpresa.BackStyle = BackStyleConstants.cc2BackstyleTransparent
        lblTDOrgCompra.BackStyle = BackStyleConstants.cc2BackstyleTransparent
                
        lblTDPedido.ZOrder 0
        lblTDProve.ZOrder 0
        lblTDEmpresa.ZOrder 0
        lblTDOrgCompra.ZOrder 0
    Else
        lblTDPedido.Visible = False
        lblTDProve.Visible = False
        lblTDEmpresa.Visible = False
        lblTDOrgCompra.Visible = False
    
        'Quitar los labels transparentes
        lblTDPedido.ZOrder 100
        lblTDProve.ZOrder 100
        lblTDEmpresa.ZOrder 100
        lblTDOrgCompra.ZOrder 100
    End If
    If Not (g_bDesdeSolicitud Or m_bOrigenEnviarPedidoAdj) And m_bAnyadirLineasDesdeAdj Then
        fraProceso.Enabled = True
        cmdBuscar.Visible = True
        
        sdbcAnyo.Backcolor = &HFFFFFF
        sdbcGMN1_4Cod.Backcolor = &HFFFFFF
        sdbcProceCod.Backcolor = &HFFFFFF
        sdbcProceDen.Backcolor = &HFFFFFF
    Else
        fraProceso.Enabled = False
        cmdBuscar.Visible = False
        
        sdbcAnyo.Backcolor = &H80000018
        sdbcGMN1_4Cod.Backcolor = &H80000018
        sdbcProceCod.Backcolor = &H80000018
        sdbcProceDen.Backcolor = &H80000018
    End If
    
    
    If g_bDesdeSolicitud Then
        cmdSeleccionar.Visible = False
        cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 180
        cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + 180
    Else
        cmdSeleccionar.Visible = True
        cmdSeleccionar.Left = Me.Width / 3 - 700
        cmdAceptar.Left = cmdSeleccionar.Left + cmdSeleccionar.Width + 180
        cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + 180
    End If

    fraProceso.Width = Me.ScaleWidth - fraProceso.Left - 120
    sstabCat.Top = fraProceso.Top + fraProceso.Height + 120
    sstabCat.Width = fraProceso.Width
    
    sstabCat.Height = Me.ScaleHeight - sstabCat.Top - cmdAceptar.Height - 240
    cmdSeleccionar.Top = sstabCat.Top + sstabCat.Height + 120
    cmdCancelar.Top = cmdSeleccionar.Top
    cmdAceptar.Top = cmdSeleccionar.Top
    With sdbgAdjudicaciones
        If g_bParaPedido And Not g_bDesdeSolicitud Then
            chkTodos.Visible = True
            .Columns("SEL").Visible = True
            chkTodos.Top = sstabCat.Top + 400
            .Top = chkTodos.Top + chkTodos.Height + 90
            .Height = sstabCat.Height - chkTodos.Height - 590
        Else
            chkTodos.Visible = False
            .Columns("SEL").Visible = False
            .Top = sstabCat.Top + 400
            .Height = sstabCat.Height - 500
        End If
        .Width = sstabCat.Width - 30
        
        .Groups(0).Width = .Width
                
        .Columns("ART").Width = .Width * 0.18
        .Columns("DEST").Width = .Width * 0.07
        .Columns("CANT").Width = .Width * 0.12
        .Columns("UNI").Width = .Width * 0.08
        .Columns("PU").Width = .Width * 0.1
        .Columns("SUMI").Width = .Width * 0.18
        If g_bParaPedido Then
            .Columns("UON_DEN").Width = .Width * 0.24
            .Columns("PROVE").Width = 0
        Else
            .Columns("PROVE").Width = .Width * 0.15
            .Columns("UON_DEN").Width = 0
        End If
        
        If Not gParametrosGenerales.gbOblPedidosHom Then
            .Columns("HOM").Width = 0
            If g_bParaPedido Then
                .Columns("UON_DEN").Width = .Width * 0.24
            Else
                .Columns("PROVE").Width = .Width * 0.187
            End If
        End If
    End With
    
    If (m_bOrigenEnviarPedidoAdj = True) Then
        chkTodos.Value = 1
        chkTodos_Click
    End If
End Sub


Public Sub CargarProcesoConBusqueda()

    Set oProcesos = Nothing
    Set oProcesos = frmPROCEBuscar.oProceEncontrados
    
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo = oProcesos.Item(1).Anyo
    bRespetarCombo = True
    sdbcGMN1_4Cod = oProcesos.Item(1).GMN1Cod
    sdbcGMN1_4Cod_Validate False
    sdbcProceCod = oProcesos.Item(1).Cod
    sdbcProceDen = oProcesos.Item(1).Den
    bRespetarCombo = False
    ProcesoSeleccionado

End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALANYA_ADJ, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sCaptionDesdeCatalogo = Ador(0).Value
        Ador.MoveNext
        fraProceso.caption = Ador(0).Value
        lblProceCod.caption = Ador(0).Value & ":"
        sIdiProceso = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("HOM").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("ART").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("PROVE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("DEST").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("UNI").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CANT").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("PU").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("SUMI").caption = Ador(0).Value
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        'A�adido el 24-2-2006
        sdbgAdjudicaciones.Columns("MON").caption = Ador(0).Value
        Ador.MoveNext
        lblPedido.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblProve.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblEmpresa.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblOrgCompra.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("UON_DEN").caption = Ador(0).Value
        Ador.MoveNext
        m_sPedInv_ArtGasto = Ador(0).Value
        Ador.MoveNext
        m_sPedGasto_ArtInv = Ador(0).Value
        Ador.MoveNext
        m_sPedObliAlm_ArtNoAlm = Ador(0).Value
        Ador.MoveNext
        m_sPedNoAlm_ArtObliAlm = Ador(0).Value
        Ador.MoveNext
        m_sPedObliRec_ArtNoRec = Ador(0).Value
        Ador.MoveNext
        m_sPedNoRec_ArtObliRec = Ador(0).Value
        Ador.MoveNext
        m_sEmpresaIncorrecta = Ador(0).Value
        Ador.MoveNext
        m_sOrgComprasIncorrecta = Ador(0).Value
        Ador.MoveNext
        m_sCaptionDesdeSeguimiento = Ador(0).Value
        Ador.MoveNext
        m_sSinPedidoSeleccionado = Ador(0).Value
        Ador.MoveNext
        m_sProveedorNoValido = Ador(0).Value
        Ador.MoveNext
        m_sImputacionIncorrecta = Ador(0).Value
        Ador.MoveNext
        chkTodos.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("SEL").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CANT_ADJ").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CANT_PED").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("IMP_ADJ").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("IMP_PED").caption = Ador(0).Value
        Ador.MoveNext
        m_sItemPorImporte = Ador(0).Value
        Ador.MoveNext
        m_sItemPorCantidad = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub chkTodos_Click()
    chequearTodo chkTodos.Value
    cmdAceptar.Enabled = chkTodos.Value
End Sub

Private Sub chequearTodo(ByVal Value As Boolean)
    LockWindowUpdate Me.hWnd
    With sdbgAdjudicaciones
        .MoveFirst
        Dim i As Integer
        For i = 0 To .Rows - 1
            If .Columns("SEL").Value <> Value Then
                If Not Value Or (Value And FilaSeleccionableParaPedido(.RowBookmark(i))) Then
                    .Columns("SEL").Value = Value
                End If
            End If
            .MoveNext
        Next
        .MoveFirst
    End With
    LockWindowUpdate 0&
End Sub

Private Sub cmdAceptar_Click()
    'Si se viene de solicitud seleccionar todas las l�neas
    If g_bDesdeSolicitud Then
        If sdbgAdjudicaciones.Rows > 0 Then
            Dim i As Integer
            For i = 0 To sdbgAdjudicaciones.Rows - 1
                If FilaSeleccionableParaPedido(sdbgAdjudicaciones.RowBookmark(i)) Then
                    sdbgAdjudicaciones.SelBookmarks.Add sdbgAdjudicaciones.RowBookmark(i)
                End If
            Next
        End If
    End If
    
    AnyadirAdjudicacionesSeleccionadas
End Sub

Private Sub AnyadirAdjudicacionesSeleccionadas()
    Dim oLineasNuevas As CLineasCatalogo
    Dim bAdjuntarArt As Boolean
    Dim bAdjuntarProve As Boolean
    Dim i As Integer
    
    With sdbgAdjudicaciones
        If .Rows > 0 Then
            If Not g_bParaPedido Then
                If ((Not IsNull(.Bookmark)) And (.SelBookmarks.Count = 0)) Then .SelBookmarks.Add .Bookmark
                
                'Si hay articulos codificados miro si pasar las esp
                bAdjuntarArt = False
                bAdjuntarProve = False
                For i = 0 To .SelBookmarks.Count - 1
                    If .Columns("ARTCOD").CellValue(.SelBookmarks.Item(i)) <> "" Then
                        bAdjuntarArt = True
                        Exit For
                    End If
                Next
                If bAdjuntarArt Then
                    bAdjuntarArt = False
                    bAdjuntarProve = False
                    Select Case gParametrosGenerales.giCatalogoEspecArticulo
                        Case 1
                            bAdjuntarArt = True
                        Case 2
                            i = oMensajes.PreguntaAnyadirEspecificaciones(TipoEspecificacion.EspArticulo)
                            If i = vbYes Then
                                bAdjuntarArt = True
                            Else
                                bAdjuntarArt = False
                            End If
                        Case 3
                            bAdjuntarArt = False
                    End Select
                    Select Case gParametrosGenerales.giCatalogoEspecProveArt
                        Case 1
                            bAdjuntarProve = True
                        Case 2
                            i = oMensajes.PreguntaAnyadirEspecificaciones(TipoEspecificacion.EspArticuloProveedor)
                            If i = vbYes Then
                                bAdjuntarProve = True
                            Else
                                bAdjuntarProve = False
                            End If
                        Case 3
                            bAdjuntarProve = False
                    End Select
                End If
            End If
            
            Set oLineasNuevas = oFSGSRaiz.Generar_CLineasCatalogo
            If Not g_bParaPedido Or g_bDesdeSolicitud Then
                'Recorrer las filas seleccionadas
                For i = 0 To .SelBookmarks.Count - 1
                    If Not AgregarLineaColeccion(oLineasNuevas, i, bAdjuntarArt, bAdjuntarProve) Then
                        .SelBookmarks.RemoveAll
                        Exit Sub
                    End If
                Next
                
                .DeleteSelected
            Else
                LockWindowUpdate Me.hWnd
                
                'Coger las filas con el check a 1
                Dim vbm As Variant
                .Update
                For i = .Rows - 1 To 0 Step -1
                    vbm = .AddItemBookmark(i)
                    If .Columns("SEL").CellValue(vbm) Then
                        If Not AgregarLineaColeccion(oLineasNuevas, i, bAdjuntarArt, bAdjuntarProve) Then
                            chequearTodo False
                            Exit Sub
                        End If
                                                                       
                        .RemoveItem .AddItemRowIndex(vbm)
                    End If
                Next
                
                LockWindowUpdate 0&
            End If
        
            RaiseEvent OnAnyaAdjSeleccionadas(oProcesoSeleccionado, oLineasNuevas)
        End If
    End With
    
    Set oLineasNuevas = Nothing
End Sub

''' <summary>Agrega una l�nea de pedido a la colecci��on de l�neas de pedido seleccionadas</summary>
''' <param name="oLineasNuevas">Colecci�n de l�neas a a�adir</param>
''' <param name="vBm">Bookmark de la l�nea a a�adir</param>
''' <param name="bAdjuntarArt">bAdjuntarArt</param>
''' <param name="bAdjuntarProve">bAdjuntarProve</param>
''' <remarks>Llamada desde: AnyadirAdjudicacionesSeleccionadas</remarks>
''' <returns>Booleano indicando si se ha podido a�adir</returns>

Private Function AgregarLineaColeccion(ByRef oLineasNuevas As CLineasCatalogo, ByVal i As Integer, ByVal bAdjuntarArt As Boolean, ByVal bAdjuntarProve As Boolean) As Boolean
    Dim vbm As Variant
    Dim oLineaNueva As CLineaCatalogo
    
    AgregarLineaColeccion = True
    
    With sdbgAdjudicaciones
        If g_bParaPedido Then
            vbm = .AddItemBookmark(i)
        Else
            vbm = .SelBookmarks.Item(i)
        End If
        If Not g_bParaPedido Then 'Solo miramos si el prove esta en portal cuando sea para catalogo
            If gParametrosGenerales.giINSTWEB = ConPortal And .Columns("PROVEINSTWEB").CellValue(vbm) = "" Then
                Screen.MousePointer = vbNormal
                oMensajes.ProveNoEnlazadoPortal .Columns("PROVE").CellValue(vbm)
                AgregarLineaColeccion = False
            End If
        End If
                        
        If .Columns("SOLICIT").CellValue(vbm) <> "" Then
            'Si el estado de la Instancia es distinta que aprobada, miramos si su solicitud permite los pedidos directos:
            Dim oSolicitudes As CInstancias
            Set oSolicitudes = oFSGSRaiz.Generar_CInstancias
            oSolicitudes.BuscarSolicitudes OrdSolicPorId, , .Columns("SOLICIT").CellValue(vbm)
            If Not oSolicitudes Is Nothing Then
                If (oSolicitudes.Count = 1) Then
                        If oSolicitudes.Item(1).Estado < EstadoSolicitud.Pendiente Then
                            oSolicitudes.Item(1).Solicitud.CargarConfiguracion , , , , True
                            If Not oSolicitudes.Item(1).Solicitud.PermitirPedidoDirecto Then
                                Screen.MousePointer = vbNormal
                                oMensajes.NoPermitidaAdjudicacion (.Columns("ART").CellValue(vbm))
                                AgregarLineaColeccion = False
                            End If
                        End If
                End If
            End If
        End If
        'FIN Version >= 30400
        
        Set oLineaNueva = oFSGSRaiz.Generar_CLineaCatalogo
        oLineaNueva.Homologado = .Columns("HOM").CellValue(vbm)
        oLineaNueva.FechaDespublicacion = Null
        oLineaNueva.Anyo = val(sdbcAnyo.Text)
        If g_bParaPedido Then
            oLineaNueva.GMN1Cod = .Columns("GMN1").CellValue(vbm)
            oLineaNueva.GMN2Cod = .Columns("GMN2").CellValue(vbm)
            oLineaNueva.GMN3Cod = .Columns("GMN3").CellValue(vbm)
            oLineaNueva.GMN4Cod = .Columns("GMN4").CellValue(vbm)
            oLineaNueva.Concepto = .Columns("CONCEPTO").CellValue(vbm)
            oLineaNueva.Recepcionable = .Columns("RECEPCIONAR").CellValue(vbm)
            oLineaNueva.Almacenable = .Columns("ALMACENAR").CellValue(vbm)
            oLineaNueva.GrupoID = .Columns("GRUPO").CellValue(vbm)
        Else
            oLineaNueva.GMN1Cod = oProcesoSeleccionado.GMN1Cod
        End If
        oLineaNueva.GMN1Proce = oProcesoSeleccionado.GMN1Cod
        oLineaNueva.ProceCod = val(sdbcProceCod.Text)
        oLineaNueva.Item = val(.Columns("ITEM").CellValue(vbm))
        oLineaNueva.PresupuestoUC = .Columns("PRES").CellValue(vbm)
        oLineaNueva.ArtCod_Interno = .Columns("ARTCOD").CellValue(vbm)
        oLineaNueva.ArtCod_Externo = .Columns("COD_EXT").CellValue(vbm)
        oLineaNueva.ArtDen = .Columns("ARTDEN").CellValue(vbm)
        oLineaNueva.ProveCod = .Columns("PROVECOD").CellValue(vbm)
        oLineaNueva.Destino = .Columns("DEST").CellValue(vbm)
        oLineaNueva.CantidadAdj = .Columns("CANT").CellValue(vbm)
        oLineaNueva.CantidadPedido = .Columns("CANT_PED").CellValue(vbm)
        oLineaNueva.UnidadCompra = .Columns("UNI").CellValue(vbm)
        oLineaNueva.PrecioUC = .Columns("PU").CellValue(vbm)
        oLineaNueva.UnidadPedido = .Columns("UNI").CellValue(vbm)
        oLineaNueva.FactorConversion = 1
        oLineaNueva.CantidadMinima = 0
        oLineaNueva.Solicitud = .Columns("SOLICIT").CellValue(vbm)
        oLineaNueva.SolicitudLinea = .Columns("LINEA_SOLICIT").CellValue(vbm)
        oLineaNueva.SolicitudCampo = .Columns("CAMPO_SOLICIT").CellValue(vbm)
        oLineaNueva.MonOferta = .Columns("MON").CellValue(vbm)
        oLineaNueva.MonProce = .Columns("MONEDAPROCESO").CellValue(vbm)
        oLineaNueva.tipoRecepcion = .Columns("TIPORECEPCION").CellValue(vbm)
        If Not g_oOrdenEntrega Is Nothing Then
            If gParametrosGenerales.gbActPedAbierto And g_oOrdenEntrega.Tipo = TipoPedido.PedidoAbierto Then
                If g_oOrdenEntrega.PedidoAbiertoTipo = TipoEmisionPedido.PedidoAbiertoCantidad Then
                    oLineaNueva.tipoRecepcion = 0
                Else
                    oLineaNueva.tipoRecepcion = 1
                End If
            End If
        End If
        oLineaNueva.DestinoUsuario = True
        If .Columns("FEC_INI").CellValue(vbm) <> "" Then oLineaNueva.FecIni = .Columns("FEC_INI").CellValue(vbm)
        If .Columns("FEC_FIN").CellValue(vbm) <> "" Then oLineaNueva.FecFin = .Columns("FEC_FIN").CellValue(vbm)
        oLineaNueva.PrecioAdj = .Columns("PREC_VALIDO").CellValue(vbm)
        oLineaNueva.CambioOferta = .Columns("OFE_CAMBIO").CellValue(vbm)
        oLineaNueva.grupoCod = .Columns("GRUPO_COD").CellValue(vbm)
        If bAdjuntarArt Then
            If bAdjuntarProve Then
                oLineaNueva.EspAdj = 3
            Else
                oLineaNueva.EspAdj = 1
            End If
        Else
            If bAdjuntarProve Then
                oLineaNueva.EspAdj = 2
            Else
                oLineaNueva.EspAdj = 0
            End If
        End If
        oLineaNueva.UsarEscalados = SQLBinaryToBoolean(.Columns("USARESCALADOS").CellValue(vbm))
        oLineaNueva.NumEscAdjudicados = .Columns("NUMESCADJ").CellValue(vbm)
        If oLineaNueva.UsarEscalados And oLineaNueva.NumEscAdjudicados = 1 Then
            oLineaNueva.IdEscalado = .Columns("ESC").CellValue(vbm)
        End If
        oLineaNueva.PorcenDesvio = gParametrosGenerales.gsDesvioRecepcion
    End With
    
    oLineasNuevas.AddLineaCatalogo oLineaNueva, i
    
    If g_bParaPedido Then
        Dim sCod As String
        sCod = oLineaNueva.GMN1Proce & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oLineaNueva.GMN1Proce))
        sCod = CStr(oLineaNueva.Anyo) & sCod & CStr(oLineaNueva.ProceCod) & CStr(oLineaNueva.Item) & CStr(oLineaNueva.ArtCod_Interno)
        If Not m_oLineasAgregadas.Exists(sCod) Then m_oLineasAgregadas.Add sCod, sCod
    End If
    
    Set oLineaNueva = Nothing
End Function

Private Sub cmdBuscar_Click()
    frmPROCEBuscar.bRDest = False
    frmPROCEBuscar.m_bProveAsigComp = False
    frmPROCEBuscar.m_bProveAsigEqp = False
    frmPROCEBuscar.bRMat = m_bRMat
    frmPROCEBuscar.bRAsig = m_bRAsig
    frmPROCEBuscar.bRCompResponsable = m_bRCompResp
    frmPROCEBuscar.bREqpAsig = m_bREqpAsig
    frmPROCEBuscar.bRUsuAper = m_bRUsuAper
    frmPROCEBuscar.bRUsuUON = m_bRUsuUON
    frmPROCEBuscar.bRUsuDep = m_bRUsuDep
    frmPROCEBuscar.sOrigen = "frmCATALAnyaAdj"
    Set frmPROCEBuscar.g_oOrigen = Me
    frmPROCEBuscar.sdbcAnyo = sdbcAnyo
    frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
    frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
    Dim sProve As String
    If Not g_oOrdenEntrega Is Nothing Then sProve = g_oOrdenEntrega.ProveCod
    frmPROCEBuscar.sProveAdj = sProve
    frmPROCEBuscar.Show 1
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdSeleccionar_Click()
    Dim i As Integer
    
    If g_bParaPedido Then
        chequearTodo True
    Else
        For i = 0 To (sdbgAdjudicaciones.Rows - 1)
            sdbgAdjudicaciones.SelBookmarks.Add i
        Next
    End If
    
    AnyadirAdjudicacionesSeleccionadas
End Sub

Private Sub Form_Load()
    Me.Top = 1500
    Me.Left = 1500
    
    Arrange
    CargarRecursos
    ConfigurarSeguridad
    
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    
    If g_bParaPedido Then
        Me.caption = m_sCaptionDesdeSeguimiento
    Else
        Me.caption = m_sCaptionDesdeCatalogo
    End If
    
    PonerFieldSeparator Me
    CargarAnyos
    
    'Comprobar si hay imputaciones a pedido
    Dim oParamSM As CParametroSM
    If Not g_oParametrosSM Is Nothing Then
        m_bImputaPedido = False
        m_bNivelCabe = False
        
        'De momento se supone un �nico �rbol a nivel de cabecera, cogemos el primero que encontramos de cabecera por defecto.
        For Each oParamSM In g_oParametrosSM
            If oParamSM.ImpPedido <> TipoImputacionEmiPed.NoImputa Then
                m_bImputaPedido = True
                
                If oParamSM.ImpModo = TipoModoImputacionSM.NivelCabecera Then
                    Set m_oArbolCabecera = oParamSM
                    m_bNivelCabe = True
                    Exit For
                End If
            End If
        Next
        Set oParamSM = Nothing
    End If
    
    Set oProcesos = oFSGSRaiz.generar_CProcesos
    Set m_oLineasAgregadas = New Dictionary
    
    ConfigurarGrid
    If g_bParaPedido Then MostrarDatosPedido
    If g_bProcesoPrecargado Then PreCargarProceso
    cmdAceptar.Enabled = Not (g_bParaPedido And Not g_bDesdeSolicitud)
    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Private Sub ConfigurarGrid()
    With sdbgAdjudicaciones
        If Not gParametrosGenerales.gbOblPedidosHom Then
            .Columns("HOM").Visible = False
            .Columns("ART").Width = .Columns("ART").Width + 200
            If g_bParaPedido Then
                .Columns("SEL").Visible = True
                .Columns("PROVE").Visible = False
                .Columns("UON_DEN").Visible = True
            Else
                .Columns("SEL").Visible = False
                .Columns("PROVE").Visible = True
                .Columns("PROVE").Width = .Columns("PROVE").Width + 300
                .Columns("UON_DEN").Visible = False
            End If
        End If
        
        .Columns("CANT").Visible = Not g_bParaPedido
        If g_bParaPedido Then
            If g_oOrdenEntrega.Tipo = PedidoAbierto Then
                If g_oOrdenEntrega.PedidoAbiertoTipo = TipoEmisionPedido.PedidoAbiertoCantidad Then
                    .Columns("CANT_ADJ").Visible = True
                    .Columns("CANT_PED").Visible = True
                    .Columns("IMP_ADJ").Visible = False
                    .Columns("IMP_PED").Visible = False
                    .Columns("UNI").Visible = True
                    .Columns("PU").Visible = True
                Else
                    .Columns("CANT_ADJ").Visible = False
                    .Columns("CANT_PED").Visible = False
                    .Columns("IMP_ADJ").Visible = True
                    .Columns("IMP_PED").Visible = True
                    .Columns("UNI").Visible = False
                    .Columns("PU").Visible = False
                End If
            Else
                .Columns("CANT_ADJ").Visible = True
                .Columns("CANT_PED").Visible = True
                .Columns("IMP_ADJ").Visible = False
                .Columns("IMP_PED").Visible = False
                .Columns("UNI").Visible = True
                .Columns("PU").Visible = True
            End If
        Else
            .Columns("CANT_ADJ").Visible = False
            .Columns("CANT_PED").Visible = False
            .Columns("IMP_ADJ").Visible = False
            .Columns("IMP_PED").Visible = False
            .Columns("UNI").Visible = True
            .Columns("PU").Visible = True
        End If
    End With
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Private Sub ConfigurarSeguridad()
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestMatComp)) Is Nothing) Then
            m_bRMat = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestAsignado)) Is Nothing) Then
            m_bRAsig = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestResponsable)) Is Nothing) Then
            m_bRCompResp = True
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestEqpAsignado)) Is Nothing) Then
            m_bREqpAsig = True
        End If
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuAper)) Is Nothing) Then
        m_bRUsuAper = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuUON)) Is Nothing) Then
        m_bRUsuUON = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuDep)) Is Nothing) Then
        m_bRUsuDep = True
    End If
    m_bAnyadirLineasDesdeAdj = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGAnyadirLineasDesdeAdj)) Is Nothing)
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next
    If sdbcAnyo.Text = "" Then
        sdbcAnyo.Text = iAnyoActual
        If Not oProcesoSeleccionado Is Nothing Then
            If oProcesoSeleccionado.Anyo > 0 Then
                sdbcAnyo.Text = oProcesoSeleccionado.Anyo
            End If
        End If
    End If
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub

Private Sub Form_Resize()
    'Diferenciar cuando la columna Hom est� visible y no dependiendo del parametro general
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oGruposMN1 = Nothing
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    Set oProcesoSeleccionado = Nothing
    Set oProcesos = Nothing
    Set oIBaseDatos = Nothing
    Set m_oArbolCabecera = Nothing
    Set m_oLineasAgregadas = Nothing
    If bUnloadForm Then
        bUnloadForm = False
        Unload Me
    End If
End Sub

Private Sub sdbcAnyo_Click()
    sdbcProceCod = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    sdbcProceDen.RemoveAll
End Sub

Private Sub sdbcGMN1_4Cod_Change()
    
    If Not GMN1RespetarCombo Then
    
        GMN1CargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
'        Accion = ACCProceCon
        sdbcProceCod.Text = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.Text = ""
        sdbcProceDen.RemoveAll
    End If

End Sub

Private Sub sdbcGMN1_4Cod_Click()
    If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod = ""
    End If
End Sub


Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    GMN1CargarComboDesde = False
    
    Set oProcesoSeleccionado = Nothing
    LimpiarDatosProceso
'    Accion = ACCProceCon
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceDen.Text = ""

End Sub

Public Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
'    Set oGMN2Seleccionado = Nothing
'    Set oGMN3Seleccionado = Nothing
'    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
        
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If GMN1CargarComboDesde Then
    
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
             oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcGMN1_4Cod_InitColumnProps()
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , True, False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
            GMN1CargarComboDesde = False
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
       oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
            GMN1CargarComboDesde = False
        End If
    
    End If
    
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProceCod_Change()
    
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcProceDen.Text = ""
        Set oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        bRespetarCombo = False
        bCargarComboDesde = True
    End If

End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceCod = ""
    End If
End Sub


Private Sub sdbcProceCod_CloseUp()

    If sdbcProceCod.Value = "..." Or sdbcProceCod = "" Then
        sdbcProceCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False

End Sub

Private Sub sdbcProceCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sProve As String
    Dim bPedidoAbierto As Boolean
           
    sdbcProceCod.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    Set oProcesoSeleccionado = Nothing

    If Not g_oOrdenEntrega Is Nothing Then
        sProve = g_oOrdenEntrega.ProveCod
        bPedidoAbierto = (g_oOrdenEntrega.Tipo = TipoPedido.PedidoAbierto)
    End If
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosConAdjudicaciones gParametrosInstalacion.giCargaMaximaCombos, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Text, sdbcGMN1_4Cod.Text, val(sdbcProceCod.Text), , False, _
            basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, _
            basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, sProve, Not g_bParaPedido, bPedidoAbierto
    Else
        oProcesos.CargarTodosLosProcesosConAdjudicaciones gParametrosInstalacion.giCargaMaximaCombos, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Text, sdbcGMN1_4Cod.Text, , , False, _
            basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, _
            basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, sProve, Not g_bParaPedido, bPedidoAbierto
    End If

    Codigos = oProcesos.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & ""
    Next

    If Not oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProceCod_InitColumnProps()
    
    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbcProceCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceCod.Rows - 1
            bm = sdbcProceCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Public Sub sdbcProceCod_Validate(Cancel As Boolean)
    If sdbcProceCod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
        Set oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        Exit Sub
    End If
        
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    Dim sProve As String
    If Not g_oOrdenEntrega Is Nothing Then sProve = g_oOrdenEntrega.ProveCod
    oProcesos.CargarTodosLosProcesosConAdjudicaciones 1, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, val(sdbcProceCod), , True, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, _
        m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, _
        basOptimizacion.gCodDepUsuario, sProve, Not g_bParaPedido
        
    If oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
        Set oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
    Else
        bRespetarCombo = True
        sdbcProceDen.Text = oProcesos.Item(1).Den
        
        sdbcProceCod.Columns(0).Text = sdbcProceCod.Text
        sdbcProceCod.Columns(1).Text = sdbcProceDen.Text
        
        bRespetarCombo = False
        Set oProcesoSeleccionado = Nothing
        Set oProcesoSeleccionado = oProcesos.Item(1)
        bCargarComboDesde = False
        ProcesoSeleccionado
    End If

End Sub

Private Sub ProcesoSeleccionado()
    Dim adoRecordset As Ador.Recordset
    Dim sCod As String
    Dim iNumGrupos As Integer
    
    LimpiarDatosProceso
        
    sdbgAdjudicaciones.Visible = True
          
    sCod = sdbcGMN1_4Cod.Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Value))
    If sCod = "" Then
        sstabCat.Visible = False
        Exit Sub
    End If
        
    sCod = CStr(sdbcAnyo.Value) & sCod & sdbcProceCod.Value
    If sCod = "" Then
        sstabCat.Visible = False
        Exit Sub
    End If
    sstabCat.Visible = True
    Screen.MousePointer = vbHourglass
        
    oProcesos.CargarDatosGeneralesProceso val(sdbcAnyo.Value), sdbcGMN1_4Cod.Value, val(sdbcProceCod.Value)
    If Not g_bProcesoPrecargado Then
        Set oProcesoSeleccionado = oProcesos.Item(sCod)
    End If
    If oProcesoSeleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
               
    If sstabCat.Tabs.Count > 0 Then
        sstabCat.Tabs(1).caption = ""
        sstabCat.Tabs.Remove 1
    End If
    iNumGrupos = 0
    
    m_bEscalados = False
    Set adoRecordset = oProcesoSeleccionado.DevolverGruposConAdjudicaciones
    While Not adoRecordset.EOF
        If adoRecordset.Fields("ESCALADOS").Value = 1 Then m_bEscalados = True
        
        iNumGrupos = iNumGrupos + 1
        sstabCat.Tabs.Add iNumGrupos, "A" & adoRecordset.Fields(0).Value, adoRecordset.Fields(0).Value & " - " & adoRecordset.Fields(1).Value
        sstabCat.Tabs(iNumGrupos).caption = adoRecordset.Fields(0).Value & " - " & adoRecordset.Fields(1).Value 'jul
        adoRecordset.MoveNext
    Wend
    adoRecordset.Close
    Set adoRecordset = Nothing
        
    If (iNumGrupos > 1) Then
        sstabCat.Tabs.Add iNumGrupos + 1
        sstabCat.Tabs(sstabCat.Tabs.Count).caption = oMensajes.CargarTextoMensaje(318)
    End If
        
    If (iNumGrupos > 0) Then
        sstabCat.Tabs(1).Selected = True
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub LimpiarDatosProceso()
    Dim i As Integer
    
    'Borra los tabs de los grupos y el de ALL
    i = sstabCat.Tabs.Count
    While i > 1
        sstabCat.Tabs.Remove i
        i = sstabCat.Tabs.Count
    Wend
    If sstabCat.Tabs.Count > 0 Then
        sstabCat.Tabs(1).caption = ""
    End If
    sdbgAdjudicaciones.RemoveAll
End Sub



Private Sub sdbcProceDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        
        sdbcProceCod.Text = ""
        Set oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        bRespetarCombo = False
        
        bCargarComboDesde = True
    
    End If

End Sub

Private Sub sdbcProceDen_Click()
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen.Text = ""
        sdbcProceCod.Text = ""
    End If
End Sub

Private Sub sdbcProceDen_CloseUp()
    If sdbcProceDen.Value = "....." Or sdbcProceDen = "" Then
        sdbcProceDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
End Sub

Private Sub sdbcProceDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sProve As String
    
    sdbcProceDen.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProcesoSeleccionado = Nothing
    
    If Not g_oOrdenEntrega Is Nothing Then sProve = g_oOrdenEntrega.ProveCod
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosConAdjudicaciones gParametrosInstalacion.giCargaMaximaCombos, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , val(sdbcProceDen), False, basOptimizacion.gCodEqpUsuario, _
            basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, _
            basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, sProve, Not g_bParaPedido
    Else
        oProcesos.CargarTodosLosProcesosConAdjudicaciones gParametrosInstalacion.giCargaMaximaCombos, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , , False, basOptimizacion.gCodEqpUsuario, _
            basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, _
            basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, sProve, Not g_bParaPedido
    End If

    Codigos = oProcesos.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i) & Chr(m_lSeparador) & ""
    Next
    
    If Not oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceDen_InitColumnProps()
    
    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbcProceDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceDen.Rows - 1
            bm = sdbcProceDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbgAdjudicaciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgAdjudicaciones_Change()
    'Se pone aqu� este control y no en el RowColChange bloqueando la columna porque por ej. para el caso de pasar de un proceso con 1 l�nea no seleccionable
    'a otro con una l�nea seleccionable la columna se queda bloqueada y al rev�s.
    With sdbgAdjudicaciones
        If .Columns(.col).Name = "SEL" Then
            If Not FilaSeleccionableParaPedido(.RowBookmark(.Row)) Then .Columns(.col).Value = False
            
            cmdAceptar.Enabled = HayFilasChequeadas
        End If
    End With
End Sub

Private Sub sdbgAdjudicaciones_HeadClick(ByVal ColIndex As Integer)
    Dim udtOrdenar As TipoOrdenacionAdjs
    Dim sTexto As String
    Dim adoresAdo As Ador.Recordset
    Dim sProve As String
    
    Select Case ColIndex
        Case 0
            udtOrdenar = OrdAdjPorDescr
        Case 1
            udtOrdenar = OrdAdjPorDescr
        Case 2
            udtOrdenar = OrdAdjPorCodProve
        Case 3
            udtOrdenar = OrdAdjPorDest
        Case 4
            udtOrdenar = OrdAdjPorCant
        Case 5
            udtOrdenar = OrdAdjPorUni
        Case 6
            udtOrdenar = OrdAdjPorPrec
        Case 7
            udtOrdenar = OrdAdjPorMon
        Case 8
            udtOrdenar = OrdAdjPorFecIni
        Case Else
            udtOrdenar = OrdAdjPorDescr
    End Select

    sTexto = sstabCat.selectedItem.caption
    If (sTexto = oMensajes.CargarTextoMensaje(318)) Then
        If Not g_oOrdenEntrega Is Nothing Then sProve = g_oOrdenEntrega.ProveCod
        Set adoresAdo = oProcesoSeleccionado.DevolverAdjudicacionesParaCatalogo(, udtOrdenar, sProve, g_bParaPedido, g_bParaPedido, Not g_bParaPedido, (gParametrosGenerales.gsAccesoFSSM = AccesoFSSM And g_bParaPedido), m_bEscalados)
    Else
        If sTexto <> "" Then
            sTexto = Left(sTexto, InStr(sTexto, " ") - 1)
            If Not g_oOrdenEntrega Is Nothing Then sProve = g_oOrdenEntrega.ProveCod
            Set adoresAdo = oProcesoSeleccionado.DevolverAdjudicacionesParaCatalogo(sTexto, udtOrdenar, sProve, g_bParaPedido, g_bParaPedido, Not g_bParaPedido, (gParametrosGenerales.gsAccesoFSSM = AccesoFSSM And g_bParaPedido), m_bEscalados)
        End If
    End If
        
    sdbgAdjudicaciones.RemoveAll
    sdbgAdjudicaciones.Visible = True
        
    If adoresAdo Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    While Not adoresAdo.EOF
        AgregarLineaGrid sTexto, adoresAdo
        
        adoresAdo.MoveNext
    Wend
    
    adoresAdo.Close
    Set adoresAdo = Nothing
    
    sdbgAdjudicaciones.MoveFirst
     
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Comprueba si es valida y de serlo la mete en el grid
''' </summary>
''' <param name="sTexto">Grupo q estes cargando</param>
''' <param name="adoresAdo">datos de la linea q podr�a ser agregada al grid si es valida</param>
''' <remarks>Llamada desde: sstabCat_Click   sdbgAdjudicaciones_HeadClick; Tiempo m�ximo: 0,2</remarks>
Private Sub AgregarLineaGrid(ByVal sTexto As String, ByVal adoresAdo As Ador.Recordset)
    Dim oEmp As CEmpresa
    Dim sEmpresa As String
    Dim sImputaciones As String
    Dim sOrgCompras As String
    Dim bHom As Boolean
    Dim dblPrecioUnidad As Double
    Dim sInstWeb As String
    Dim str_linea As String 'Se a�ade para manipular mejor la adiccion de lineas al grid.
    Dim bAdd As Boolean
    Dim vImpPed As Variant
    Dim vImpAdj As Variant
    
    With adoresAdo
        'Comprobar que no se haya agregado ya al pedido
        Dim sCod As String
        sCod = oProcesoSeleccionado.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oProcesoSeleccionado.GMN1Cod))
        sCod = CStr(val(sdbcAnyo.Text)) & sCod & CStr(val(sdbcProceCod.Text)) & CStr(val(.Fields("ID").Value)) & NullToStr(.Fields("ART").Value)
        If m_oLineasAgregadas.Exists(sCod) Then Exit Sub
        
        If g_bParaPedido And Not g_oOrdenEntrega Is Nothing Then 'Si viene de solicitud al principio, cuando se cargan las l�neas, no se tiene un pedido seleccionado
            If Not oProcesoSeleccionado.Grupos Is Nothing Then
                If Not oProcesoSeleccionado.Grupos.Item(CStr(.Fields("GRUPO_COD").Value)) Is Nothing Then
                    If Not oProcesoSeleccionado.Grupos.Item(CStr(.Fields("GRUPO_COD").Value)).Items Is Nothing Then
                        If Not oProcesoSeleccionado.Grupos.Item(CStr(.Fields("GRUPO_COD").Value)).Items.Item(CStr(.Fields("ID").Value)) Is Nothing Then
                            'Comprobamos que es valido para la empresa
                            If g_oOrdenEntrega.Empresa <> 0 Then
                                If Not oProcesoSeleccionado.Grupos.Item(CStr(.Fields("GRUPO_COD").Value)).Items.Item(CStr(.Fields("ID").Value)).Empresas Is Nothing Then
                                    sEmpresa = "0"
                                    sOrgCompras = "0"
                                    For Each oEmp In oProcesoSeleccionado.Grupos.Item(CStr(.Fields("GRUPO_COD").Value)).Items.Item(CStr(.Fields("ID").Value)).Empresas
                                        If oEmp.Id = g_oOrdenEntrega.Empresa And oEmp.NIF = .Fields("ART").Value Then
                                            sEmpresa = "1"
                                            If gParametrosGenerales.gbUsarOrgCompras Then
                                                If g_oOrdenEntrega.OrgCompras <> "" Then
                                                    If oEmp.Den = g_oOrdenEntrega.OrgCompras And oEmp.NIF = .Fields("ART").Value Then
                                                        sOrgCompras = "1"
                                                    Else
                                                        sOrgCompras = "0"
                                                    End If
                                                Else
                                                    sOrgCompras = "1"
                                                End If
                                            Else
                                                sOrgCompras = "1"
                                            End If
                                            Exit For
                                        End If
                                    Next
                                    
                                    Set oEmp = Nothing
                                Else
                                    sEmpresa = "0"
                                End If
                            Else
                                sEmpresa = "1"
                            End If
                        End If
                    End If
                End If
            End If
            
            'Comprobar imputaciones
            sImputaciones = "1"
            If (g_bDesdeSolicitud Or NullToDbl0(.Fields("SOLICIT").Value) <> 0) And m_bImputaPedido And m_bNivelCabe Then
                If Not g_oOrdenEntrega.Imputaciones Is Nothing And Not oProcesoSeleccionado.Grupos.Item(CStr(.Fields("GRUPO_COD").Value)).Items.Item(CStr(.Fields("ID").Value)).Imputaciones Is Nothing Then
                    Dim oImputacion As Cimputacion
                    For Each oImputacion In oProcesoSeleccionado.Grupos.Item(CStr(.Fields("GRUPO_COD").Value)).Items.Item(CStr(.Fields("ID").Value)).Imputaciones
                        If Not g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5) Is Nothing Then
                            'Comprobaci�n de centro de coste y contrato de imputaciones
                            If oImputacion.CC1 <> g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5).CC1 Or oImputacion.CC2 <> g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5).CC2 Or _
                                oImputacion.CC3 <> g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5).CC3 Or oImputacion.CC4 <> g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5).CC4 Or _
                                oImputacion.Pres1 <> g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5).Pres1 Or oImputacion.Pres2 <> g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5).Pres2 Or _
                                oImputacion.Pres3 <> g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5).Pres3 Or oImputacion.Pres4 <> g_oOrdenEntrega.Imputaciones.Item(oImputacion.PRES5).Pres4 Then
                                sImputaciones = "0"
                            End If
                        End If
                    Next
                End If
            End If
        End If
        
        bHom = IIf(.Fields(0).Value = 1, 1, 0)
        dblPrecioUnidad = adoresAdo.Fields("PRECIO").Value
        If gParametrosGenerales.giINSTWEB <> SinWeb Then
            sInstWeb = NullToStr(.Fields("PROVE_INSTWEB").Value)
        Else
            sInstWeb = "$"
        End If
        
        'Importes
        If .Fields("IMPORTE_PED").Value = 0 Then
            vImpPed = ""
        Else
            vImpPed = .Fields("IMPORTE_PED").Value
        End If
        If .Fields("ADJUDICADO").Value = 0 Then
            vImpAdj = ""
        Else
            vImpAdj = .Fields("ADJUDICADO").Value
        End If
        
        bAdd = False
        If .Fields("ES_CENTRAL").Value = 1 Then 'Indica que es un articulo hijo de un articulo central(que es el que se ha negociado)
            Dim CantAdj As Double
            CantAdj = oProcesoSeleccionado.DevolverAdjudicacionItemCentral(.Fields("ID").Value, .Fields("ART").Value, .Fields("PROVECOD").Value)
            
            If CantAdj <> -1 Then bAdd = True
        Else
            CantAdj = NullToDbl0(.Fields("CANT_ADJ").Value)
            bAdd = True
        End If
        
        If bAdd Then
            str_linea = False & Chr(m_lSeparador)
            str_linea = str_linea & bHom & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("ART").Value & " - " & .Fields("DESCR").Value & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("PROVECOD").Value & " - " & .Fields("PROVEDEN").Value & Chr(m_lSeparador)
            If g_bParaPedido Then
                str_linea = str_linea & NullToStr(.Fields("UON_DEN").Value) & Chr(m_lSeparador)
            Else
                str_linea = str_linea & "" & Chr(m_lSeparador)
            End If
            str_linea = str_linea & .Fields("DEST").Value & Chr(m_lSeparador)
            str_linea = str_linea & CantAdj & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("UNI").Value & Chr(m_lSeparador)
            str_linea = str_linea & NullToDbl0(.Fields("CANT_ADJ").Value) & Chr(m_lSeparador)
            str_linea = str_linea & NullToDbl0(.Fields("CANT_PED").Value) & Chr(m_lSeparador)
            str_linea = str_linea & vImpAdj & Chr(m_lSeparador)
            str_linea = str_linea & vImpPed & Chr(m_lSeparador)
            str_linea = str_linea & dblPrecioUnidad & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("MON")) & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("FECINI").Value & " - " & .Fields("FECFIN").Value & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("ID").Value & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("ART").Value & Chr(m_lSeparador)
            str_linea = str_linea & adoresAdo.Fields("DESCR").Value & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("PROVECOD").Value & Chr(m_lSeparador)
            str_linea = str_linea & sInstWeb & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("PRES").Value) & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("SOLICIT").Value) & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("TIPORECEPCION").Value)
            If g_bParaPedido Then
                str_linea = str_linea & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("CONCEPTO").Value) & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("RECEPCIONAR").Value) & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("ALMACENAR").Value) & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("UON1").Value) & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("UON2").Value) & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("UON3").Value) & Chr(m_lSeparador)
                str_linea = str_linea & sEmpresa & Chr(m_lSeparador) & sOrgCompras & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("GMN1").Value) & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("GMN2").Value) & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("GMN3").Value) & Chr(m_lSeparador)
                str_linea = str_linea & NullToStr(.Fields("GMN4").Value) & Chr(m_lSeparador)
            End If
            str_linea = str_linea & NullToStr(.Fields("COD_EXT").Value) & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("FECINI").Value & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("FECFIN").Value & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("PRECIO").Value) & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("MONEDAPROCESO").Value) & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("OFE_CAMBIO").Value) & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("GRUPO").Value)
            If g_bParaPedido Then str_linea = str_linea & Chr(m_lSeparador) & NullToStr(.Fields("GRUPO_COD").Value) & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("LINEA_SOLICIT").Value) & Chr(m_lSeparador)
            str_linea = str_linea & NullToStr(.Fields("CAMPO_SOLICIT").Value) & Chr(m_lSeparador)
            str_linea = str_linea & sImputaciones & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("ESCALADOS").Value & Chr(m_lSeparador)
            str_linea = str_linea & NullToDbl0(.Fields("NUMESCADJ").Value) & Chr(m_lSeparador)
            str_linea = str_linea & .Fields("ESC").Value
            
            sdbgAdjudicaciones.AddItem str_linea
        End If
    End With
End Sub

Private Sub sdbgAdjudicaciones_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iVisibleRow As Integer
    Dim iVisibleCol As Integer
    Dim vBmk As Variant
    Dim sTooltip As String
        
    If g_bParaPedido Then
        With sdbgAdjudicaciones
            If .WhereIs(X, Y) = ssWhereIsData Then
                'Get the Visible Row Number
                iVisibleRow = .RowContaining(Y)
                iVisibleCol = .ColContaining(X)
                
                'Get the bookmark of the row
                vBmk = .RowBookmark(iVisibleRow)
                                               
                If Not g_oOrdenEntrega Is Nothing Then
                    'Comprobar empresa
                    If g_oOrdenEntrega.Empresa = 0 Or (g_oOrdenEntrega.Empresa <> 0 And .Columns("EMPRESA").CellValue(vBmk) = "0") Then AddTooltip sTooltip, m_sEmpresaIncorrecta
                    
                    'Comprobar Org. compras
                    If gParametrosGenerales.gbUsarOrgCompras And (.Columns("ORGCOMPRAS").CellText(vBmk) = "0" Or .Columns("ORGCOMPRAS").CellText(vBmk) = "") Then AddTooltip sTooltip, m_sOrgComprasIncorrecta
                    
                    'Comprobar el proveedor
                    If .Columns("PROVECOD").Value <> g_oOrdenEntrega.ProveCod Then AddTooltip sTooltip, m_sProveedorNoValido
                                        
                    If Not g_oOrdenEntrega.TipoDePedido Is Nothing Then
                        'Comprobar tipo de pedido
                        If g_oOrdenEntrega.TipoDePedido.CodConcep = Inversion And .Columns("CONCEPTO").CellText(vBmk) = TipoConcepto.Gasto Then AddTooltip sTooltip, m_sPedInv_ArtGasto
                        If g_oOrdenEntrega.TipoDePedido.CodConcep = Gasto And .Columns("CONCEPTO").CellText(vBmk) = TipoConcepto.Inversion Then AddTooltip sTooltip, m_sPedGasto_ArtInv
                        If g_oOrdenEntrega.TipoDePedido.CodAlmac = ObligatorioAlmacenar And .Columns("ALMACENAR").CellText(vBmk) = TipoArtAlmacenable.NoAlmacenable Then AddTooltip sTooltip, m_sPedObliAlm_ArtNoAlm
                        If g_oOrdenEntrega.TipoDePedido.CodAlmac = NoAlmacenable And .Columns("ALMACENAR").CellText(vBmk) = TipoArtAlmacenable.ObligatorioAlmacenar Then AddTooltip sTooltip, m_sPedNoAlm_ArtObliAlm
                        If g_oOrdenEntrega.TipoDePedido.CodRecep = ObligatorioRececpionar And .Columns("RECEPCIONAR").CellText(vBmk) = TipoArtRecepcionable.NoRececpionar Then AddTooltip sTooltip, m_sPedObliRec_ArtNoRec
                        If g_oOrdenEntrega.TipoDePedido.CodRecep = NoRececpionar And .Columns("RECEPCIONAR").CellText(vBmk) = TipoArtRecepcionable.ObligatorioRececpionar Then AddTooltip sTooltip, m_sPedNoRec_ArtObliRec
                    End If
                    
                    'Comprobar partida presupuestaria
                    If ((g_bDesdeSolicitud Or NullToDbl0(.Columns("SOLICIT").Value) <> 0) And m_bImputaPedido And m_bNivelCabe) And (.Columns("IMPUTACIONES").Value = "0") Then
                        m_sImputacionIncorrecta = Replace(m_sImputacionIncorrecta, "@ARBOL", """" & m_oArbolCabecera.NomNivelImputacion & """")
                        AddTooltip sTooltip, m_sImputacionIncorrecta
                    End If
                    
                    'Comprobar tipo de recepci�n
                    If gParametrosGenerales.gbActPedAbierto And g_oOrdenEntrega.Tipo = TipoPedido.PedidoAbierto Then
                        If g_oOrdenEntrega.PedidoAbiertoTipo = TipoEmisionPedido.PedidoAbiertoCantidad And .Columns("TIPORECEPCION").Value = 1 Then
                            AddTooltip sTooltip, m_sItemPorImporte
                        End If
                    End If
                Else
                    AddTooltip sTooltip, m_sSinPedidoSeleccionado
                End If
                
                If sTooltip <> "" Then sTooltip = sTooltip & " * "
                
                .ToolTipText = sTooltip
            End If
        End With
    End If
End Sub

''' <summary>Comprueba si una fila se puede adjudicar a un pedido</summary>
''' <params name="vBm">Bookmark de la fila actual</params>
''' <remarks>Llamada desde: sdbgAdjudicaciones_MouseMove</remarks>

Private Function FilaSeleccionableParaPedido(ByVal vbm As Variant) As Boolean
    Dim bBloqueo As Boolean
    
    With sdbgAdjudicaciones
        '1- Comprobamos que se cumplen las condiciones de org de compras y empresa
        If .Columns("UON_DEN").CellValue(vbm) <> "" Then
            'si el art�culo no est� en todas las uons
            If .Columns("EMPRESA").CellValue(vbm) = "1" Then
                'coincide empresa
                If gParametrosGenerales.gbUsarOrgCompras Then
                    bBloqueo = (.Columns("ORGCOMPRAS").CellValue(vbm) = "0")
                End If
            Else
                'no coincide empresa
                bBloqueo = True
            End If
        End If
        
        '2- Comprobar el proveedor
        If Not bBloqueo Then bBloqueo = (.Columns("PROVECOD").CellValue(vbm) <> g_oOrdenEntrega.ProveCod)
        
        '3- Comprobar el tipo de pedido
        If Not bBloqueo Then
            If Not g_oOrdenEntrega Is Nothing Then
                If Not g_oOrdenEntrega.TipoDePedido Is Nothing Then
                    bBloqueo = (.Columns("CONCEPTO").CellValue(vbm) = TipoConcepto.Gasto And g_oOrdenEntrega.TipoDePedido.CodConcep = Inversion) Or _
                        (.Columns("CONCEPTO").CellValue(vbm) = TipoConcepto.Inversion And g_oOrdenEntrega.TipoDePedido.CodConcep = Gasto) Or _
                        (.Columns("RECEPCIONAR").CellValue(vbm) = NoRececpionar And g_oOrdenEntrega.TipoDePedido.CodRecep = ObligatorioRececpionar) Or _
                        (.Columns("RECEPCIONAR").CellValue(vbm) = ObligatorioAlmacenar And g_oOrdenEntrega.TipoDePedido.CodRecep = NoAlmacenable) Or _
                        (.Columns("ALMACENAR").CellValue(vbm) = NoAlmacenable And g_oOrdenEntrega.TipoDePedido.CodAlmac = ObligatorioAlmacenar) Or _
                        (.Columns("ALMACENAR").CellValue(vbm) = ObligatorioAlmacenar And g_oOrdenEntrega.TipoDePedido.CodAlmac = NoAlmacenable)
                End If
            Else
                'Si viene de solicitud, al principio todav�a no hay un pedido seleccionado y las filas deben aparecer bloqueadas
                bBloqueo = True
            End If
        End If
        
        '4- Validar las imputaciones al SM
        If Not bBloqueo And ((g_bDesdeSolicitud Or NullToDbl0(.Columns("SOLICIT").CellValue(vbm)) <> 0) And m_bImputaPedido And m_bNivelCabe) Then
            bBloqueo = (.Columns("IMPUTACIONES").CellValue(vbm) = "0")
        End If
        
        '5- Validar el tipo de recepci�n
        If Not g_oOrdenEntrega Is Nothing Then
            If gParametrosGenerales.gbActPedAbierto And g_oOrdenEntrega.Tipo = TipoPedido.PedidoAbierto Then
                If g_oOrdenEntrega.PedidoAbiertoTipo = TipoEmisionPedido.PedidoAbiertoCantidad And .Columns("TIPORECEPCION").CellValue(vbm) = 1 Then
                    bBloqueo = True
                End If
            End If

        End If
        
        
    End With
    
    FilaSeleccionableParaPedido = Not bBloqueo
End Function

''' <summary>Devuelve se hay alguna fila chequeda</summary>
''' <remarks>Llamada desde: sdbgAdjudicaciones_MouseMove</remarks>

Private Function HayFilasChequeadas() As Boolean
    Dim i As Integer
    Dim bHay As Boolean
    
    bHay = False
    
    With sdbgAdjudicaciones
        .Update
        For i = 0 To .Rows - 1
            If .Columns("SEL").CellValue(.RowBookmark(i)) Then
                bHay = True
                Exit For
            End If
        Next
    End With
    
    HayFilasChequeadas = bHay
End Function

''' <summary>A�ade un texto a la cadena de tooltips del grid de adjudicaciones</summary>
''' <remarks>Llamada desde: sdbgAdjudicaciones_MouseMove</remarks>

Private Sub AddTooltip(ByRef sTooltip As String, ByVal sText As String)
    If sTooltip = "" Then
        sTooltip = " * "
    ElseIf sTooltip <> " * " Then
        sTooltip = sTooltip & " / "
    End If
    
    sTooltip = sTooltip & sText
End Sub

Private Sub sdbgAdjudicaciones_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If gParametrosGenerales.gbOblPedidosHom And sdbgAdjudicaciones.Columns.Item("HOM").Value = "False" Then
        oMensajes.ArticuloNoHomologado
'        sdbgAdjudicaciones.ActiveRowStyleSet = "NoSelected"
        sdbgAdjudicaciones.SelBookmarks.Remove 0
    Else
'        sdbgAdjudicaciones.ActiveRowStyleSet = "Selected"
    End If
End Sub

Private Sub sdbgAdjudicaciones_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    
    With sdbgAdjudicaciones
        If gParametrosGenerales.gbOblPedidosHom And .Columns("HOM").Value = "False" Then
            For i = 0 To .Cols - 1
                .Columns(i).CellStyleSet "NoHom"
            Next i
        End If
        If g_bParaPedido Then
            If Not FilaSeleccionableParaPedido(Bookmark) Then
                For i = 0 To .Cols - 1
                    If .Columns(i).Name = "ART" Then
                        .Columns(i).CellStyleSet "GreyImg"
                    Else
                        .Columns(i).CellStyleSet "Grey"
                    End If
                Next i
            End If
        End If
    End With
End Sub

Private Sub sdbgAdjudicaciones_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
    If g_bParaPedido Then
        If Not FilaSeleccionableParaPedido(sdbgAdjudicaciones.Bookmark) Then Cancel = True
    End If
End Sub

''' <summary>
''' Carga el grid sdbgAdjudicaciones
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sstabCat_Click()
    Dim sTexto As String
    Dim adoresAdo As Ador.Recordset
    Dim sProve As String
    
    If oProcesoSeleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    sTexto = sstabCat.selectedItem.caption
    If (sTexto = oMensajes.CargarTextoMensaje(318)) Then
        If Not g_oOrdenEntrega Is Nothing Then sProve = g_oOrdenEntrega.ProveCod
        Set adoresAdo = oProcesoSeleccionado.DevolverAdjudicacionesParaCatalogo(, , sProve, g_bParaPedido, g_bParaPedido, Not g_bParaPedido, (gParametrosGenerales.gsAccesoFSSM = AccesoFSSM And g_bParaPedido), m_bEscalados)
    Else
        sTexto = Left(sTexto, InStr(sTexto, " ") - 1)
        If Not g_oOrdenEntrega Is Nothing Then sProve = g_oOrdenEntrega.ProveCod
        Set adoresAdo = oProcesoSeleccionado.DevolverAdjudicacionesParaCatalogo(sTexto, , sProve, g_bParaPedido, g_bParaPedido, Not g_bParaPedido, (gParametrosGenerales.gsAccesoFSSM = AccesoFSSM And g_bParaPedido), m_bEscalados)
    End If

    sdbgAdjudicaciones.RemoveAll
    sdbgAdjudicaciones.Visible = True
        
    If adoresAdo Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    While Not adoresAdo.EOF
        AgregarLineaGrid sTexto, adoresAdo
            
        adoresAdo.MoveNext
    Wend
    
    If g_bParaPedido And chkTodos.Value Then chequearTodo True
    
    adoresAdo.Close
    Set adoresAdo = Nothing
    
    sdbgAdjudicaciones.MoveFirst
     
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Cargamos el formulario con los datos de un proceso que se ha pasado desde otro formulario
''' </summary>
''' <remarks>Llamada desde: frmCATALAnyaAdj; Tiempo m�ximo:0,1</remarks>
Public Sub PreCargarProceso()
    Dim oProceso As CProceso
    Set oProceso = oProcesoSeleccionado
    sdbcAnyo = oProceso.Anyo
    bRespetarCombo = True
    GMN1RespetarCombo = True
    sdbcProceCod = oProceso.Cod
    sdbcProceDen = oProceso.Den
    sdbcGMN1_4Cod = oProceso.GMN1Cod
    sdbcGMN1_4Cod_Validate False
    ProcesoSeleccionado
    bRespetarCombo = False
    GMN1RespetarCombo = False
    g_bProcesoPrecargado = False
    bUnloadForm = True
End Sub

''' <summary>Muestra los datos del pedido</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub MostrarDatosPedido()
    Dim oEmpresa As CEmpresa
    
    If Not g_oOrdenEntrega Is Nothing Then
        lblDPedido.caption = g_oOrdenEntrega.Anyo & "/" & g_oOrdenEntrega.NumPedido & "/" & g_oOrdenEntrega.Numero
        lblDPedido.ToolTipText = lblDPedido.caption
        lblDProve.caption = g_oOrdenEntrega.ProveCod & " - " & g_oOrdenEntrega.ProveDen
        lblDProve.ToolTipText = lblDProve.caption
        lblDOrgCompra.caption = g_oOrdenEntrega.OrgComprasDen
        lblDOrgCompra.ToolTipText = lblDOrgCompra.caption
        
        Set oEmpresa = oFSGSRaiz.Generar_CEmpresa
        oEmpresa.Id = g_oOrdenEntrega.Empresa
        oEmpresa.cargarDatosEmpresa
        lblDEmpresa.caption = oEmpresa.NIF & " - " & g_oOrdenEntrega.EmpresaDen
        lblDEmpresa.ToolTipText = lblDEmpresa.caption
        Set oEmpresa = Nothing
        
        'Tooltips labels transparentes
        lblTDPedido.ToolTipText = lblDPedido.ToolTipText
        lblTDProve.ToolTipText = lblDProve.ToolTipText
        lblTDEmpresa.ToolTipText = lblDEmpresa.ToolTipText
        lblTDOrgCompra.ToolTipText = lblDOrgCompra.ToolTipText
    End If
End Sub

