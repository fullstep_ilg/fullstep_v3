VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmActualizarAhorros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de ahorros"
   ClientHeight    =   5220
   ClientLeft      =   975
   ClientTop       =   2700
   ClientWidth     =   7410
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActualizarAhorros.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   7410
   Begin VB.Frame Frame4 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   60
      TabIndex        =   38
      Top             =   0
      Width           =   7275
      Begin VB.CommandButton cmdDetalle 
         Caption         =   "&Detalle"
         Height          =   315
         Left            =   3900
         TabIndex        =   1
         Top             =   180
         Width           =   915
      End
      Begin VB.CommandButton cmdActualizar 
         Caption         =   "&Actualizar ahorros"
         Height          =   315
         Left            =   4980
         TabIndex        =   2
         Top             =   180
         Width           =   2175
      End
      Begin VB.Label lblFechaAct 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   60
         TabIndex        =   0
         Top             =   180
         Width           =   3795
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   1380
      TabIndex        =   31
      Top             =   4860
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   60
      TabIndex        =   30
      Top             =   4860
      Width           =   1215
   End
   Begin VB.PictureBox picTarea 
      BorderStyle     =   0  'None
      Height          =   4215
      Left            =   0
      ScaleHeight     =   4215
      ScaleWidth      =   7395
      TabIndex        =   39
      Top             =   600
      Width           =   7395
      Begin VB.Frame fraDuracion 
         Caption         =   "Duraci�n"
         Height          =   1095
         Left            =   60
         TabIndex        =   54
         Top             =   3060
         Width           =   7275
         Begin VB.TextBox txtDur 
            Height          =   315
            Left            =   1980
            TabIndex        =   28
            Top             =   360
            Width           =   1335
         End
         Begin VB.CommandButton cmdCalDur 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3390
            Picture         =   "frmActualizarAhorros.frx":0CB2
            Style           =   1  'Graphical
            TabIndex        =   29
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   360
            Width           =   315
         End
         Begin VB.Label lblDur 
            Caption         =   "Fecha de comienzo:"
            Height          =   255
            Left            =   300
            TabIndex        =   55
            Top             =   420
            Width           =   1575
         End
      End
      Begin VB.Frame fraDiaria 
         Caption         =   "Frecuencia diaria"
         Height          =   1335
         Left            =   60
         TabIndex        =   52
         Top             =   1710
         Width           =   7275
         Begin VB.ComboBox cmbFrec 
            Height          =   315
            Left            =   2820
            Style           =   2  'Dropdown List
            TabIndex        =   26
            Top             =   780
            Width           =   1215
         End
         Begin VB.OptionButton optFrec 
            Caption         =   "Una vez a las"
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   22
            Top             =   300
            Width           =   1755
         End
         Begin VB.TextBox txtFrec 
            Height          =   315
            Left            =   2070
            MaxLength       =   4
            TabIndex        =   25
            Text            =   "1"
            Top             =   780
            Width           =   405
         End
         Begin VB.OptionButton optFrec 
            Caption         =   "Cada"
            Height          =   255
            Index           =   1
            Left            =   180
            TabIndex        =   24
            Top             =   780
            Width           =   1755
         End
         Begin MSComCtl2.UpDown updFrec 
            Height          =   315
            Left            =   2490
            TabIndex        =   56
            Top             =   780
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   556
            _Version        =   393216
            Value           =   1
            BuddyControl    =   "txtFrec"
            BuddyDispid     =   196623
            OrigLeft        =   2490
            OrigTop         =   780
            OrigRight       =   2730
            OrigBottom      =   1095
            Max             =   24
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin MSComCtl2.DTPicker dtpFrec 
            Height          =   315
            Index           =   0
            Left            =   2040
            TabIndex        =   23
            Top             =   270
            Width           =   1275
            _ExtentX        =   2249
            _ExtentY        =   556
            _Version        =   393216
            Format          =   137035778
            CurrentDate     =   0.649166666666667
            MaxDate         =   0.999988425925926
            MinDate         =   0
         End
         Begin MSComCtl2.DTPicker dtpFrec 
            Height          =   315
            Index           =   1
            Left            =   5640
            TabIndex        =   27
            Top             =   780
            Width           =   1275
            _ExtentX        =   2249
            _ExtentY        =   556
            _Version        =   393216
            Format          =   137035778
            CurrentDate     =   0.461238425925926
            MaxDate         =   0.999988425925926
            MinDate         =   0
         End
         Begin VB.Label lblFrec 
            Caption         =   "Comenzando a las"
            Height          =   255
            Left            =   4200
            TabIndex        =   53
            Top             =   840
            Width           =   1395
         End
      End
      Begin VB.CheckBox chkActivarTarea 
         Caption         =   "Activar tarea de actualizaci�n de ahorros"
         Height          =   195
         Left            =   120
         TabIndex        =   32
         Top             =   30
         Width           =   3615
      End
      Begin VB.Frame fraFrecuencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1635
         Left            =   60
         TabIndex        =   40
         Top             =   30
         Width           =   7275
         Begin VB.PictureBox picMeses 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1035
            Left            =   1740
            ScaleHeight     =   1035
            ScaleWidth      =   5445
            TabIndex        =   41
            Top             =   360
            Visible         =   0   'False
            Width           =   5445
            Begin VB.ComboBox cmbMeses 
               Height          =   315
               Index           =   1
               Left            =   1710
               Style           =   2  'Dropdown List
               TabIndex        =   20
               Top             =   630
               Width           =   1365
            End
            Begin VB.ComboBox cmbMeses 
               Height          =   315
               Index           =   0
               ItemData        =   "frmActualizarAhorros.frx":123C
               Left            =   780
               List            =   "frmActualizarAhorros.frx":123E
               Style           =   2  'Dropdown List
               TabIndex        =   19
               Top             =   630
               Width           =   855
            End
            Begin VB.TextBox txtMeses 
               Height          =   315
               Index           =   1
               Left            =   2370
               MaxLength       =   2
               TabIndex        =   17
               Text            =   "1"
               Top             =   150
               Width           =   435
            End
            Begin VB.OptionButton optMeses 
               Caption         =   "D�a"
               Height          =   255
               Index           =   0
               Left            =   0
               TabIndex        =   15
               Top             =   210
               Width           =   705
            End
            Begin VB.TextBox txtMeses 
               Height          =   315
               Index           =   2
               Left            =   3975
               MaxLength       =   2
               TabIndex        =   21
               Text            =   "1"
               Top             =   630
               Width           =   435
            End
            Begin VB.OptionButton optMeses 
               Caption         =   "El"
               Height          =   255
               Index           =   1
               Left            =   0
               TabIndex        =   18
               Top             =   660
               Width           =   705
            End
            Begin MSComCtl2.UpDown UpdMeses 
               Height          =   315
               Index           =   0
               Left            =   1215
               TabIndex        =   35
               Top             =   150
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   556
               _Version        =   393216
               Value           =   1
               BuddyControl    =   "txtMeses(0)"
               BuddyDispid     =   196631
               BuddyIndex      =   0
               OrigLeft        =   1200
               OrigTop         =   150
               OrigRight       =   1440
               OrigBottom      =   465
               Max             =   31
               Min             =   1
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin MSComCtl2.UpDown UpdMeses 
               Height          =   315
               Index           =   1
               Left            =   2805
               TabIndex        =   36
               Top             =   150
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   556
               _Version        =   393216
               Value           =   1
               BuddyControl    =   "txtMeses(1)"
               BuddyDispid     =   196631
               BuddyIndex      =   1
               OrigLeft        =   2640
               OrigTop         =   150
               OrigRight       =   2880
               OrigBottom      =   465
               Max             =   99
               Min             =   1
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin MSComCtl2.UpDown UpdMeses 
               Height          =   315
               Index           =   2
               Left            =   4410
               TabIndex        =   37
               Top             =   630
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   556
               _Version        =   393216
               Value           =   1
               BuddyControl    =   "txtMeses(2)"
               BuddyDispid     =   196631
               BuddyIndex      =   2
               OrigLeft        =   3960
               OrigTop         =   600
               OrigRight       =   4200
               OrigBottom      =   915
               Max             =   99
               Min             =   1
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin VB.TextBox txtMeses 
               Height          =   315
               Index           =   0
               Left            =   780
               MaxLength       =   2
               TabIndex        =   16
               Text            =   "1"
               Top             =   150
               Width           =   435
            End
            Begin VB.Label lblMeses 
               Alignment       =   2  'Center
               Caption         =   "von jedem"
               Height          =   255
               Index           =   0
               Left            =   1470
               TabIndex        =   45
               Top             =   210
               Width           =   870
            End
            Begin VB.Label lblMeses 
               Caption         =   "mes(es)"
               Height          =   255
               Index           =   1
               Left            =   3120
               TabIndex        =   44
               Top             =   210
               Width           =   1005
            End
            Begin VB.Label lblMeses 
               Alignment       =   2  'Center
               Caption         =   "von jedem"
               Height          =   255
               Index           =   2
               Left            =   3090
               TabIndex        =   43
               Top             =   690
               Width           =   840
            End
            Begin VB.Label lblMeses 
               Caption         =   "mes(es)"
               Height          =   255
               Index           =   3
               Left            =   4695
               TabIndex        =   42
               Top             =   690
               Width           =   675
            End
         End
         Begin VB.PictureBox picSemanas 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1275
            Left            =   1950
            ScaleHeight     =   1275
            ScaleWidth      =   4275
            TabIndex        =   49
            Top             =   240
            Visible         =   0   'False
            Width           =   4275
            Begin MSComCtl2.UpDown updSemanas 
               Height          =   315
               Left            =   1456
               TabIndex        =   34
               Top             =   150
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   556
               _Version        =   393216
               Value           =   1
               BuddyControl    =   "txtSemanas"
               BuddyDispid     =   196637
               OrigLeft        =   1560
               OrigTop         =   180
               OrigRight       =   1800
               OrigBottom      =   495
               Max             =   52
               Min             =   1
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin VB.TextBox txtSemanas 
               Height          =   315
               Left            =   1020
               MaxLength       =   2
               TabIndex        =   6
               Text            =   "1"
               Top             =   150
               Width           =   435
            End
            Begin VB.CheckBox chkSemanas 
               Caption         =   "Lun"
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   7
               Top             =   660
               Width           =   795
            End
            Begin VB.CheckBox chkSemanas 
               Caption         =   "Mar"
               Height          =   195
               Index           =   1
               Left            =   960
               TabIndex        =   8
               Top             =   660
               Width           =   795
            End
            Begin VB.CheckBox chkSemanas 
               Caption         =   "Mie"
               Height          =   195
               Index           =   2
               Left            =   1770
               TabIndex        =   9
               Top             =   660
               Width           =   795
            End
            Begin VB.CheckBox chkSemanas 
               Caption         =   "Jue"
               Height          =   195
               Index           =   3
               Left            =   2580
               TabIndex        =   10
               Top             =   660
               Width           =   795
            End
            Begin VB.CheckBox chkSemanas 
               Caption         =   "Vie"
               Height          =   195
               Index           =   4
               Left            =   3390
               TabIndex        =   11
               Top             =   660
               Width           =   795
            End
            Begin VB.CheckBox chkSemanas 
               Caption         =   "Sab"
               Height          =   195
               Index           =   5
               Left            =   120
               TabIndex        =   12
               Top             =   960
               Width           =   795
            End
            Begin VB.CheckBox chkSemanas 
               Caption         =   "Dom"
               Height          =   195
               Index           =   6
               Left            =   960
               TabIndex        =   13
               Top             =   960
               Value           =   1  'Checked
               Width           =   795
            End
            Begin VB.Label lblSemanas 
               Caption         =   "semana(s) el"
               Height          =   255
               Index           =   1
               Left            =   1860
               TabIndex        =   51
               Top             =   180
               Width           =   1365
            End
            Begin VB.Label lblSemanas 
               Caption         =   "Cada"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   50
               Top             =   180
               Width           =   735
            End
         End
         Begin VB.PictureBox picDias 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   855
            Left            =   2280
            ScaleHeight     =   855
            ScaleWidth      =   3315
            TabIndex        =   46
            Top             =   420
            Width           =   3315
            Begin VB.TextBox txtDias 
               BeginProperty DataFormat 
                  Type            =   0
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
               Height          =   315
               Left            =   900
               MaxLength       =   3
               TabIndex        =   4
               Text            =   "1"
               Top             =   180
               Width           =   435
            End
            Begin MSComCtl2.UpDown updDias 
               Height          =   315
               Left            =   1335
               TabIndex        =   33
               Top             =   180
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   556
               _Version        =   393216
               Value           =   1
               BuddyControl    =   "txtDias"
               BuddyDispid     =   196641
               OrigLeft        =   1335
               OrigTop         =   180
               OrigRight       =   1575
               OrigBottom      =   495
               Max             =   366
               Min             =   1
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin VB.Label lblDias 
               Caption         =   "Cada"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   48
               Top             =   240
               Width           =   645
            End
            Begin VB.Label lblDias 
               Caption         =   "dia(s)"
               Height          =   255
               Index           =   1
               Left            =   1680
               TabIndex        =   47
               Top             =   240
               Width           =   735
            End
         End
         Begin VB.OptionButton optSemanal 
            Caption         =   "Semanal"
            Height          =   255
            Left            =   180
            TabIndex        =   5
            Top             =   750
            Width           =   1425
         End
         Begin VB.OptionButton optDiaria 
            Caption         =   "Diaria"
            Height          =   255
            Left            =   180
            TabIndex        =   3
            Top             =   360
            Width           =   1425
         End
         Begin VB.OptionButton optMensual 
            Caption         =   "Mensual"
            Height          =   255
            Left            =   180
            TabIndex        =   14
            Top             =   1110
            Width           =   1425
         End
      End
   End
End
Attribute VB_Name = "frmActualizarAhorros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_oJob As CJob
Private m_oProcesosPendientes As CProcesos
Private m_lNumPendientes As Long

Private m_sIdioma(1 To 16) As String
Private m_sUltimaAct As String
Private m_bModificar As Boolean


Private Sub cmbFrec_Validate(Cancel As Boolean)
If cmbFrec.ListIndex = 1 Then
    updFrec.Max = 24
    txtFrec.MaxLength = 2
Else
    updFrec.Max = 1440
    txtFrec.MaxLength = 4
End If
End Sub

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit

m_oJob.Enabled = SQLBinaryToBoolean(chkActivarTarea.Value)
If optDiaria.Value Then
    If Not IsNumeric(txtDias.Text) Then
        oMensajes.NoValido 143
        If Me.Visible Then txtDias.SetFocus
        Exit Sub
    End If
    m_oJob.Tipo = Diario
    m_oJob.Intervalo = txtDias.Text
ElseIf optSemanal.Value Then
    If Not IsNumeric(txtSemanas.Text) Then
        oMensajes.NoValido 144
        Exit Sub
    End If
    If DevolverValorInterSemanal = 0 Then
        oMensajes.MensajeOKOnly 579
        Exit Sub
    End If
    m_oJob.Tipo = Semanal
    m_oJob.Intervalo = txtSemanas.Text
    m_oJob.IntervSemanal = DevolverValorInterSemanal
ElseIf optMensual.Value Then
    If optMeses(0).Value Then
        If Not IsNumeric(txtMeses(0).Text) Then
            oMensajes.NoValido 145
            If Me.Visible Then txtMeses(0).SetFocus
            Exit Sub
        End If
        If Not IsNumeric(txtMeses(1).Text) Then
            oMensajes.NoValido 146
            If Me.Visible Then txtMeses(1).SetFocus
            Exit Sub
        End If
        m_oJob.Tipo = Mensual
        m_oJob.Intervalo = txtMeses(1).Text
        m_oJob.IntervMensual = txtMeses(0).Text
    Else
        If Not IsNumeric(txtMeses(2).Text) Then
            oMensajes.NoValido 146
            If Me.Visible Then txtMeses(2).SetFocus
            Exit Sub
        End If
        m_oJob.Tipo = MensualRelativo
        m_oJob.Intervalo = txtMeses(2).Text
        m_oJob.IntervMensualRelativo = cmbMeses(1).ListIndex + 1
        m_oJob.RelativoMens = 2 ^ cmbMeses(0).ListIndex
    End If
End If

If optFrec(0).Value Then
    m_oJob.Frecuencia = FUnaVez
    m_oJob.DiaHoraComienzo = dtpFrec(0).Value
Else
    If Not IsNumeric(txtFrec.Text) Then
        oMensajes.NoValido fraDiaria.caption
        If Me.Visible Then txtFrec.SetFocus
        Exit Sub
    End If
    m_oJob.Frecuencia = 2 ^ (cmbFrec.ListIndex + 2)
    m_oJob.IntervDia = txtFrec.Text
    m_oJob.DiaHoraComienzo = dtpFrec(1).Value
End If
If Not IsDate(txtDur.Text) Then
    oMensajes.NoValido Left(lblDur.caption, Len(lblDur.caption) - 1)
    If Me.Visible Then txtDur.SetFocus
    Exit Sub
End If
m_oJob.FechaComienzo = txtDur.Text

teserror = m_oJob.ModificarJob
If teserror.NumError <> TESnoerror Then
    TratarError teserror
    Exit Sub
Else
    basSeguridad.RegistrarAccion ACCAhorrosModJob, "Type: " & m_oJob.Tipo & " Enabled: " & BooleanToSQLBinary(m_oJob.Enabled)
    Unload Me
End If
End Sub

Private Sub cmdActualizar_Click()
    Dim iRes As Integer

    m_lNumPendientes = m_oProcesosPendientes.CargarProcesosPendientesDeCalcularAhorros
    
    If m_lNumPendientes > 0 Then
        iRes = oMensajes.PreguntaCalcularAhorros
        If iRes = vbNo Then
            Exit Sub
        End If
    Else
        oMensajes.MensajeOKOnly 581
        Exit Sub
    End If
    
    MostrarFormEsperaVirtual oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oMensajes, oFSGSRaiz, gLongitudesDeCodigos, m_oProcesosPendientes, m_lNumPendientes
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalDur_Click()
    AbrirFormCalendar Me, txtDur
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub cmdDetalle_Click()
    MostrarFormAhorrosPend oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, "PENDIENTES", oFSGSRaiz, gLongitudesDeCodigos, oMensajes
End Sub

Private Sub Form_Load()
Dim I As Integer

    Me.Height = 5625
    Me.Width = 7530
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    
    ConfigurarSeguridad
    
    'Inicializo los controles y cargo los combos
    cmbMeses(0).AddItem m_sIdioma(1), 0
    cmbMeses(0).AddItem m_sIdioma(2), 1
    cmbMeses(0).AddItem m_sIdioma(3), 2
    cmbMeses(0).AddItem m_sIdioma(4), 3
    cmbMeses(0).AddItem m_sIdioma(5), 4
    cmbMeses(0).ListIndex = 0
    For I = 6 To 12
        cmbMeses(1).AddItem m_sIdioma(I), I - 6
    Next
    cmbMeses(1).AddItem optMeses(0).caption, 7
    cmbMeses(1).AddItem m_sIdioma(13), 8
    cmbMeses(1).AddItem m_sIdioma(14), 9
    cmbMeses(1).ListIndex = 0
    cmbFrec.AddItem m_sIdioma(16), 0
    cmbFrec.AddItem m_sIdioma(15), 1
    cmbFrec.ListIndex = 1
    txtDur.Text = Date
    
    If Not m_bModificar Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        picTarea.Enabled = False
        Me.Height = 5250
    End If

    Set m_oProcesosPendientes = oFSGSRaiz.generar_CProcesos
    
    MostrarConfiguracionDelJob
    
End Sub
Private Sub MostrarConfiguracionDelJob()
Dim I As Integer

    Set m_oJob = oFSGSRaiz.Generar_CJob
    m_oJob.LeerDatos
    
    lblFechaAct.caption = m_sUltimaAct & ":"
    If Not IsNull(m_oJob.FechaUltimaAct) Then
        lblFechaAct.caption = m_sUltimaAct & " " & m_oJob.FechaUltimaAct
    End If
    chkActivarTarea.Value = BooleanToSQLBinary(m_oJob.Enabled)
    picDias.Visible = False
    picSemanas.Visible = False
    picMeses.Visible = False
    Select Case m_oJob.Tipo
    Case Diario
        optDiaria.Value = True
        picDias.Visible = True
        txtDias.Text = m_oJob.Intervalo
        
    Case Semanal
        optSemanal.Value = True
        RellenarCheckSemana
        picSemanas.Visible = True
        txtSemanas.Text = m_oJob.Intervalo
        
    Case Mensual
        optMensual.Value = True
        picMeses.Visible = True
        optMeses(0).Value = True
        txtMeses(0).Text = m_oJob.IntervMensual
        txtMeses(1).Text = m_oJob.Intervalo
        
    Case MensualRelativo
        optMensual.Value = True
        picMeses.Visible = True
        optMeses(1).Value = True
        cmbMeses(0).ListIndex = Log2(m_oJob.RelativoMens)
        cmbMeses(1).ListIndex = m_oJob.IntervMensualRelativo - 1
        txtMeses(2).Text = m_oJob.Intervalo
    End Select
    dtpFrec(1).Value = m_oJob.DiaHoraComienzo
    dtpFrec(0).Value = m_oJob.DiaHoraComienzo
    txtDur.Text = m_oJob.FechaComienzo
    If m_oJob.Tipo <> noExiste Then
        If m_oJob.Frecuencia = FUnaVez Then
            optFrec(0).Value = True
        Else
            optFrec(1).Value = True
            txtFrec.Text = m_oJob.IntervDia
            cmbFrec.ListIndex = Log2(m_oJob.Frecuencia) - 2
        End If

    Else
        chkActivarTarea = vbUnchecked
        optSemanal.Value = True
        For I = 0 To 6
            chkSemanas(I).Value = vbUnchecked
        Next
        picSemanas.Visible = True
        chkSemanas(6).Value = vbChecked
        txtSemanas.Text = 1
        optFrec(0).Value = True
    End If
        
End Sub
Private Sub ConfigurarSeguridad()
    m_bModificar = True
    
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.AhorrosModifFrec)) Is Nothing) Then
        m_bModificar = False
    End If
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim I As Integer

    On Error Resume Next
      
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ActualizarAhorros, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value
        Ador.MoveNext
        m_sUltimaAct = Ador(0).Value
        Ador.MoveNext
        cmdDetalle.caption = Ador(0).Value
        Ador.MoveNext
        cmdActualizar.caption = Ador(0).Value
        Ador.MoveNext
        chkActivarTarea.caption = Ador(0).Value
        Ador.MoveNext
        optDiaria.caption = Ador(0).Value
        Ador.MoveNext
        optSemanal.caption = Ador(0).Value
        Ador.MoveNext
        optMensual.caption = Ador(0).Value
        Ador.MoveNext
        lblDias(0).caption = Ador(0).Value
        lblSemanas(0).caption = Ador(0).Value
        optFrec(1).caption = Ador(0).Value
        Ador.MoveNext
        lblDias(1).caption = Ador(0).Value '10
        Ador.MoveNext
        lblSemanas(1).caption = Ador(0).Value
        Ador.MoveNext
        chkSemanas(0).caption = Ador(0).Value
        Ador.MoveNext
        chkSemanas(1).caption = Ador(0).Value
        Ador.MoveNext
        chkSemanas(2).caption = Ador(0).Value
        Ador.MoveNext
        chkSemanas(3).caption = Ador(0).Value
        Ador.MoveNext
        chkSemanas(4).caption = Ador(0).Value
        Ador.MoveNext
        chkSemanas(5).caption = Ador(0).Value
        Ador.MoveNext
        chkSemanas(6).caption = Ador(0).Value
        Ador.MoveNext
        optMeses(0).caption = Ador(0).Value
        Ador.MoveNext
        lblMeses(0).caption = Ador(0).Value '20
        lblMeses(2).caption = Ador(0).Value
        Ador.MoveNext
        lblMeses(1).caption = Ador(0).Value
        lblMeses(3).caption = Ador(0).Value
        Ador.MoveNext
        optMeses(1).caption = Ador(0).Value '22
        Ador.MoveNext
        For I = 1 To 16
            m_sIdioma(I) = Ador(0).Value
            Ador.MoveNext
        Next
        fraDiaria.caption = Ador(0).Value '39
        Ador.MoveNext
        optFrec(0).caption = Ador(0).Value
        Ador.MoveNext
        optFrec(1).caption = Ador(0).Value
        Ador.MoveNext
        lblFrec.caption = Ador(0).Value
        Ador.MoveNext
        fraDuracion.caption = Ador(0).Value
        Ador.MoveNext
        lblDur.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing

End Sub

Private Sub RellenarCheckSemana()
Dim I As Integer

For I = 0 To 6
    chkSemanas(I).Value = vbUnchecked
Next

If BinaryComp(m_oJob.IntervSemanal, IntervaloSemanal.sTodos) = IntervaloSemanal.sTodos Then
    For I = 0 To 6
        chkSemanas(I).Value = vbChecked
    Next
    Exit Sub
End If
If BinaryComp(m_oJob.IntervSemanal, IntervaloSemanal.SLunes) = IntervaloSemanal.SLunes Then chkSemanas(0).Value = vbChecked
If BinaryComp(m_oJob.IntervSemanal, IntervaloSemanal.SMartes) = IntervaloSemanal.SMartes Then chkSemanas(1).Value = vbChecked
If BinaryComp(m_oJob.IntervSemanal, IntervaloSemanal.SMiercoles) = IntervaloSemanal.SMiercoles Then chkSemanas(2).Value = vbChecked
If BinaryComp(m_oJob.IntervSemanal, IntervaloSemanal.SJueves) = IntervaloSemanal.SJueves Then chkSemanas(3).Value = vbChecked
If BinaryComp(m_oJob.IntervSemanal, IntervaloSemanal.SViernes) = IntervaloSemanal.SViernes Then chkSemanas(4).Value = vbChecked
If BinaryComp(m_oJob.IntervSemanal, IntervaloSemanal.SSabado) = IntervaloSemanal.SSabado Then chkSemanas(5).Value = vbChecked
If BinaryComp(m_oJob.IntervSemanal, IntervaloSemanal.SDomingo) = IntervaloSemanal.SDomingo Then chkSemanas(6).Value = vbChecked


End Sub

Private Sub Form_Unload(Cancel As Integer)
Set m_oJob = Nothing
Set m_oProcesosPendientes = Nothing
m_lNumPendientes = 0
m_bModificar = True

End Sub

Private Sub optDiaria_Click()
If picDias.Visible = False Then
    picDias.Visible = True
    picSemanas.Visible = False
    picMeses.Visible = False
End If
End Sub

Private Sub optMeses_Click(Index As Integer)
If optMeses(0).Value = True Then
    txtMeses(0).Enabled = True
    UpdMeses(0).Enabled = True
    txtMeses(1).Enabled = True
    UpdMeses(1).Enabled = True
    cmbMeses(0).Enabled = False
    cmbMeses(1).Enabled = False
    txtMeses(2).Enabled = False
    UpdMeses(2).Enabled = False
Else
    cmbMeses(0).Enabled = True
    cmbMeses(1).Enabled = True
    txtMeses(2).Enabled = True
    UpdMeses(2).Enabled = True
    txtMeses(0).Enabled = False
    UpdMeses(0).Enabled = False
    txtMeses(1).Enabled = False
    UpdMeses(1).Enabled = False
End If
End Sub

Private Sub optSemanal_Click()
    If picSemanas.Visible = False Then
        picDias.Visible = False
        picSemanas.Visible = True
        picMeses.Visible = False
    End If
End Sub
Private Sub optMensual_Click()
    If picMeses.Visible = False Then
        picDias.Visible = False
        picSemanas.Visible = False
        picMeses.Visible = True
        If optMeses(0).Value = False And optMeses(1).Value = False Then
             optMeses(0).Value = True
        End If
    End If

End Sub

Private Sub optFrec_Click(Index As Integer)

    If optFrec(0).Value = True Then
        txtFrec.Enabled = False
        updFrec.Enabled = False
        cmbFrec.Enabled = False
        dtpFrec(1).Enabled = False
        dtpFrec(0).Enabled = True
    Else
        txtFrec.Enabled = True
        updFrec.Enabled = True
        cmbFrec.Enabled = True
        dtpFrec(1).Enabled = True
        dtpFrec(0).Enabled = False
    End If
End Sub

Private Function Log2(ByVal X As Double) As Long
  
  Log2 = CLng(Log(X) / Log(2))
  
End Function


Private Sub txtDias_Validate(Cancel As Boolean)
    If Trim(txtDias.Text = "") Then txtDias.Text = 1
    If Not IsNumeric(txtDias.Text) Then
        oMensajes.NoValido 143
        txtDias.Text = 1
        Exit Sub
    ElseIf txtDias.Text < 1 Then
        oMensajes.NoValido 143
        txtDias.Text = 1
        Exit Sub
    Else
        If CInt(txtDias.Text) > 366 Then
            txtDias.Text = 366
        Else
            txtDias.Text = CInt(txtDias.Text)
        End If
    End If
        
End Sub

Private Sub txtFrec_Validate(Cancel As Boolean)
If txtFrec.Text = "" Then txtFrec.Text = 1
If Not IsNumeric(txtFrec.Text) Then
    oMensajes.NoValido fraDiaria.caption
    txtFrec.Text = 1
    Exit Sub
ElseIf txtFrec.Text < 1 Then
    oMensajes.NoValido fraDiaria.caption
    txtFrec.Text = 1
    Exit Sub
Else
    If cmbFrec.ListIndex = 1 And CInt(txtFrec.Text) > 24 Then
        txtFrec.Text = 24
    End If
    If cmbFrec.ListIndex = 0 And CInt(txtFrec.Text) > 1440 Then
        txtFrec.Text = 1440
    End If

    txtFrec.Text = CInt(txtFrec.Text)
End If

End Sub

Private Sub txtMeses_Validate(Index As Integer, Cancel As Boolean)
Dim iMens As Integer
If txtMeses(Index).Text = "" Then txtMeses(Index).Text = 1
If Not IsNumeric(txtMeses(Index).Text) Then
    If Index = 0 Then
        iMens = 145
    Else
        iMens = 146
    End If
    oMensajes.NoValido iMens
    txtMeses(Index).Text = 1
    Exit Sub
ElseIf txtMeses(Index).Text < 1 Then
    If Index = 0 Then
        iMens = 145
    Else
        iMens = 146
    End If
    oMensajes.NoValido iMens
    txtMeses(Index).Text = 1
    Exit Sub
Else
    If Index = 0 Then
        If CInt(txtMeses(0).Text) > 31 Then
            txtMeses(Index).Text = 31
        End If
    Else
        If CInt(txtMeses(Index).Text) > 99 Then
            txtMeses(Index).Text = 99
        End If
    End If
    txtMeses(Index).Text = CLng(txtMeses(Index).Text)
End If

End Sub

Private Sub txtSemanas_Validate(Cancel As Boolean)
    If txtSemanas.Text = "" Then txtSemanas.Text = 1
    
    If Not IsNumeric(txtSemanas.Text) Then
        oMensajes.NoValido 144
        txtSemanas.Text = 1
        Exit Sub
    ElseIf txtSemanas.Text < 1 Then
        oMensajes.NoValido 144
        txtSemanas.Text = 1
        Exit Sub
    Else
        If CInt(txtSemanas.Text) > 52 Then
            txtSemanas.Text = 52
        Else
            txtSemanas.Text = CInt(txtSemanas.Text)
        End If
    End If
End Sub
Private Function DevolverValorInterSemanal() As Long
Dim I As Integer
Dim lRes As Long

lRes = 0
If chkSemanas(6).Value = vbChecked Then
    lRes = 1
End If
For I = 1 To 6
    If chkSemanas(I - 1).Value = vbChecked Then
        lRes = lRes + (2 ^ I)
    End If
Next
DevolverValorInterSemanal = lRes
End Function

