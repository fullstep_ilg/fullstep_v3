VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstFormularios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de formularios"
   ClientHeight    =   2895
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6165
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstFormularios.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2895
   ScaleWidth      =   6165
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab SSTabForm 
      Height          =   2415
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   6120
      _ExtentX        =   10795
      _ExtentY        =   4260
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "DSelecci�n"
      TabPicture(0)   =   "frmLstFormularios.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frmFiltro"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "DOpciones"
      TabPicture(1)   =   "frmLstFormularios.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   1785
         Left            =   -74860
         TabIndex        =   8
         Top             =   510
         Width           =   5895
         Begin VB.CheckBox chkDesglose 
            Caption         =   "DObtener desglose"
            Height          =   255
            Left            =   120
            TabIndex        =   5
            Top             =   520
            Width           =   2865
         End
         Begin VB.CheckBox chkDetalleCampo 
            Caption         =   "DDetalle del campo"
            Height          =   195
            Left            =   120
            TabIndex        =   6
            Top             =   240
            Visible         =   0   'False
            Width           =   2535
         End
      End
      Begin VB.Frame frmFiltro 
         Height          =   1785
         Left            =   140
         TabIndex        =   7
         Top             =   510
         Width           =   5895
         Begin VB.OptionButton optForm 
            Caption         =   "DFormulario:"
            Height          =   255
            Left            =   80
            TabIndex        =   1
            Top             =   1110
            Width           =   1335
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Listado completo+"
            Height          =   195
            Left            =   80
            TabIndex        =   0
            Top             =   495
            Value           =   -1  'True
            Width           =   4230
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcFormulario 
            Height          =   285
            Left            =   1665
            TabIndex        =   2
            Top             =   1110
            Width           =   4035
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   8229
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   7117
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
   End
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      Height          =   375
      Left            =   4780
      TabIndex        =   3
      Top             =   2480
      Width           =   1335
   End
End
Attribute VB_Name = "frmLstFormularios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_sOrigen As String

'Variables privadas
Private m_sIdiForm As String
Private m_sIdiGrupo As String
Private m_sIdiTodos As String
Private txtPag As String
Private txtDe As String
Private txtSeleccion As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sIdiFecha(17) As String
Private m_sIdiCampo As String
Private m_sIdiValor As String
Private m_sIdiDesglose As String

Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LST_FORMULARIO, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value '1 Listado de formularios
        Ador.MoveNext
        optForm.caption = Ador(0).Value & ":"   '2 Formulario
        m_sIdiForm = Ador(0).Value & ":"
        Ador.MoveNext
        optTodos.caption = Ador(0).Value   '3 Listado completo
        Ador.MoveNext
        chkDetalleCampo.caption = Ador(0).Value   '4 Detalle de campo
        Ador.MoveNext
        chkDesglose.caption = Ador(0).Value   '5 Mostrar desgloses
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value '6 &Obtener
        Ador.MoveNext
        m_sIdiGrupo = Ador(0).Value '7 Grupo
        Ador.MoveNext
        txtPag = Ador(0).Value   '8 P�g.
        Ador.MoveNext
        txtDe = Ador(0).Value   '9 De
        Ador.MoveNext
        txtSeleccion = Ador(0).Value & ":"  '10 Selecci�n
        SSTabForm.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTabForm.TabCaption(1) = Ador(0).Value  '11 Opciones
        Ador.MoveNext
        m_sIdiTodos = Ador(0).Value '12 Todos los formularios
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value  '13 S�
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value  '14 No
        
        For i = 1 To 17
            Ador.MoveNext
            m_sIdiFecha(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sIdiCampo = Ador(0).Value
        Ador.MoveNext
        m_sIdiValor = Ador(0).Value
        Ador.MoveNext
        m_sIdiDesglose = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


Private Sub cmdObtener_Click()
    ObtenerListado
End Sub

Private Sub Form_Load()
    Me.Width = 6255
    Me.Height = 3375
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If g_sOrigen = "frmFormularios" Then
        If frmFormularios.sdbcFormulario.Text <> "" Then
            optForm.Value = True
            sdbcFormulario.Text = frmFormularios.sdbcFormulario.Text
            sdbcFormulario.Columns("ID").Value = frmFormularios.sdbcFormulario.Columns("ID").Value
            sdbcFormulario.Columns("COD").Value = frmFormularios.sdbcFormulario.Columns("COD").Value
        End If
    End If
    
    cmdObtener.Enabled = True
End Sub


Private Sub ObtenerListado()
    Dim SelectionText As String
    Dim sTitulo As String
    Dim oReport As CRAXDRT.Report
    Dim Srpt As CRAXDRT.Report
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim pv As Preview
    Dim adoresDG As Ador.Recordset
    Dim SubListado As CRAXDRT.Report
    Dim adoresSub As Ador.Recordset
    
    ''' * Objetivo: Obtener un listado de la estructura de la organizaci�n
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
   
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptFormularios.rpt"
   
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
       
    If optForm.Value = True Then
        If sdbcFormulario.Text = "" Then
            oMensajes.SeleccionarFormulario
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
                                                            
    
    SelectionText = ""
    If optTodos.Value = True Then
        SelectionText = m_sIdiTodos
    Else
        SelectionText = m_sIdiForm & " " & sdbcFormulario.Text
    End If

    sTitulo = Me.caption
      
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTITULO")).Text = """" & sTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSELECCION")).Text = """" & txtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFormulario")).Text = """" & m_sIdiForm & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & m_sIdiGrupo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSi")).Text = """" & m_sIdiTrue & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNo")).Text = """" & m_sIdiFalse & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFechaAlta")).Text = """" & m_sIdiFecha(1) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUnDiaDespues")).Text = """" & m_sIdiFecha(2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDosDiasDespues")).Text = """" & m_sIdiFecha(3) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTresDiasDespues")).Text = """" & m_sIdiFecha(4) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCuatroDiasDespues")).Text = """" & m_sIdiFecha(5) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCincoDiasDespues")).Text = """" & m_sIdiFecha(6) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeisDiasDespues")).Text = """" & m_sIdiFecha(7) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUnaSemanaDespues")).Text = """" & m_sIdiFecha(8) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDosSemanaDespues")).Text = """" & m_sIdiFecha(9) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTresSemanaDespues")).Text = """" & m_sIdiFecha(10) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUnMesDespues")).Text = """" & m_sIdiFecha(11) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDosMesDespues")).Text = """" & m_sIdiFecha(12) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTresMesDespues")).Text = """" & m_sIdiFecha(13) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCuatroMesDespues")).Text = """" & m_sIdiFecha(14) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCincoMesDespues")).Text = """" & m_sIdiFecha(15) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeisMesDespues")).Text = """" & m_sIdiFecha(16) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUnAnyoDespues")).Text = """" & m_sIdiFecha(17) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCampo")).Text = """" & m_sIdiCampo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtValor")).Text = """" & m_sIdiValor & """"
        
    If optForm.Value = True Then
        Set adoresDG = oGestorInformes.ListadoFormularios(sdbcFormulario.Columns(0).Value)
    Else
        Set adoresDG = oGestorInformes.ListadoFormularios()
    End If
    
    If Not adoresDG Is Nothing Then
        oReport.Database.SetDataSource adoresDG
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    
    'subreport de desglose:
    If chkDesglose.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DESGLOSE")).Text = """S"""
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DESGLOSE")).Text = """N"""
    End If
    
    Set SubListado = oReport.OpenSubreport("rptSubFormularios")
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_sIdiTrue & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_sIdiFalse & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFechaAlta")).Text = """" & m_sIdiFecha(1) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnDiaDespues")).Text = """" & m_sIdiFecha(2) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDosDiasDespues")).Text = """" & m_sIdiFecha(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTresDiasDespues")).Text = """" & m_sIdiFecha(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCuatroDiasDespues")).Text = """" & m_sIdiFecha(5) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCincoDiasDespues")).Text = """" & m_sIdiFecha(6) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSeisDiasDespues")).Text = """" & m_sIdiFecha(7) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnaSemanaDespues")).Text = """" & m_sIdiFecha(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDosSemanaDespues")).Text = """" & m_sIdiFecha(9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTresSemanaDespues")).Text = """" & m_sIdiFecha(10) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnMesDespues")).Text = """" & m_sIdiFecha(11) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDosMesDespues")).Text = """" & m_sIdiFecha(12) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTresMesDespues")).Text = """" & m_sIdiFecha(13) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCuatroMesDespues")).Text = """" & m_sIdiFecha(14) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCincoMesDespues")).Text = """" & m_sIdiFecha(15) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSeisMesDespues")).Text = """" & m_sIdiFecha(16) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnAnyoDespues")).Text = """" & m_sIdiFecha(17) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDesglose")).Text = """" & m_sIdiDesglose & """"
    
    If optForm.Value = True Then
        Set adoresSub = oGestorInformes.ListadoSubCampos(sdbcFormulario.Columns(0).Value)
    Else
        Set adoresSub = oGestorInformes.ListadoSubCampos()
    End If

    If Not adoresSub Is Nothing Then
        SubListado.Database.SetDataSource adoresSub
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DESGLOSE")).Text = """N"""
    End If
        
    
    'Muestra el report:
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    pv.Show
    
    Set adoresDG = Nothing
    Set adoresSub = Nothing
    Set SubListado = Nothing
    
    Unload Me
    Screen.MousePointer = vbNormal

End Sub

Private Sub optTodos_Click()
    Me.sdbcFormulario.Text = ""
End Sub

Private Sub sdbcFormulario_Change()
    If Not m_bRespetarCombo Then
        If sdbcFormulario <> "" Then
            m_bCargarComboDesde = True
        End If
    End If
End Sub

Private Sub sdbcFormulario_CloseUp()
    m_bCargarComboDesde = False
    optForm.Value = True
End Sub

Private Sub sdbcFormulario_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim oForm As CFormulario
    Dim oFormularios As CFormularios
    
    Screen.MousePointer = vbHourglass
    
    Set oFormularios = Nothing
    Set oFormularios = oFSGSRaiz.Generar_CFormularios
    
    sdbcFormulario.RemoveAll
    
    If m_bCargarComboDesde Then
        oFormularios.CargarTodosFormularios Trim(sdbcFormulario.Text)
    Else
        oFormularios.CargarTodosFormularios
    End If
    
    For Each oForm In oFormularios
        sdbcFormulario.AddItem oForm.Id & Chr(m_lSeparador) & oForm.Den
    Next
    
    Set oFormularios = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFormulario_InitColumnProps()
    sdbcFormulario.DataFieldList = "Column 1"
    sdbcFormulario.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbcFormulario_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcFormulario.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcFormulario.Rows - 1
            bm = sdbcFormulario.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcFormulario.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcFormulario.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


