VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCONTRWizardItem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de items del proceso:"
   ClientHeight    =   5055
   ClientLeft      =   2700
   ClientTop       =   1335
   ClientWidth     =   8925
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONTRWizardItem.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   8925
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2640
      TabIndex        =   2
      Top             =   4680
      Width           =   1095
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "&Finalizar"
      Default         =   -1  'True
      Height          =   315
      Left            =   4980
      TabIndex        =   1
      Top             =   4680
      Width           =   1095
   End
   Begin VB.CommandButton cmdVolver 
      Caption         =   "<<             "
      Height          =   315
      Left            =   3810
      TabIndex        =   0
      Top             =   4680
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgContrato 
      Height          =   3675
      Left            =   180
      TabIndex        =   4
      Top             =   840
      Width           =   8625
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Row.Count       =   2
      Col.Count       =   11
      Row(0).Col(1)   =   "01"
      Row(0).Col(2)   =   "Electricidad central  1"
      Row(0).Col(3)   =   "ST"
      Row(0).Col(4)   =   "300.000,00"
      Row(0).Col(5)   =   "k/a"
      Row(0).Col(6)   =   "0,005"
      Row(0).Col(7)   =   "1500,00"
      Row(0).Col(8)   =   "03"
      Row(1).Col(1)   =   "02"
      Row(1).Col(2)   =   "Electricidad central 2"
      Row(1).Col(3)   =   "ST"
      Row(1).Col(4)   =   "200.000,00"
      Row(1).Col(5)   =   "k/a"
      Row(1).Col(6)   =   "0,0045"
      Row(1).Col(7)   =   "900,00"
      Row(1).Col(8)   =   "03"
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCONTRWizardItem.frx":0CB2
      stylesets(1).Name=   "ITEMCERRADO"
      stylesets(1).ForeColor=   0
      stylesets(1).BackColor=   16777152
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCONTRWizardItem.frx":0CCE
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   53
      ActiveRowStyleSet=   "Normal"
      Columns.Count   =   11
      Columns(0).Width=   900
      Columns(0).Name =   "INC"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   1535
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3254
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777215
      Columns(3).Width=   820
      Columns(3).Caption=   "Dest"
      Columns(3).Name =   "DEST"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   1614
      Columns(4).Caption=   "Cantidad"
      Columns(4).Name =   "CANT"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16777215
      Columns(5).Width=   820
      Columns(5).Caption=   "Uni"
      Columns(5).Name =   "UNI"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   16777215
      Columns(6).Width=   2143
      Columns(6).Caption=   "Precio unitario"
      Columns(6).Name =   "PREC"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   0
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).ButtonsAlways=   -1  'True
      Columns(6).HasBackColor=   -1  'True
      Columns(6).BackColor=   16777215
      Columns(7).Width=   2249
      Columns(7).Caption=   "Importe"
      Columns(7).Name =   "IMP"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   0
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(7).HasBackColor=   -1  'True
      Columns(7).BackColor=   16777215
      Columns(8).Width=   900
      Columns(8).Caption=   "Pago"
      Columns(8).Name =   "PAG"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(8).HasBackColor=   -1  'True
      Columns(8).BackColor=   16777215
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID"
      Columns(9).Name =   "ID"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "GRUPO"
      Columns(10).Name=   "GRUPO"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      _ExtentX        =   15214
      _ExtentY        =   6482
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TabStrip sstabGrupos 
      Height          =   4170
      Left            =   60
      TabIndex        =   5
      Top             =   420
      Width           =   8835
      _ExtentX        =   15584
      _ExtentY        =   7355
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "DGeneral"
            Key             =   "General"
            Object.Tag             =   "General"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblSel 
      Caption         =   "Seleccione los items que desea incluir en el nuevo contrato:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   60
      TabIndex        =   3
      Top             =   90
      Width           =   6225
   End
End
Attribute VB_Name = "frmCONTRWizardItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_sFormatoNumber As String
Private m_sIdiMensaje  As String
Private m_sIdiCaption  As String
Private m_sALL As String
Private m_bVolver As Boolean
Private m_oProceso As CProceso
Private m_oAllItemsAdjudicados As CItems
Private m_oAllAdjudicaciones As CAdjudicaciones
Public g_sCodProve As String

Private Sub cmdCancelar_Click()
    Unload frmCONTRWizard
    Unload Me
End Sub

''' <summary>
''' Construye una cadena con los items seleccionador para enviar a la llamada web (Alta de contrato)
''' </summary>
''' <remarks>Tiempo m�ximo:0,3seg.</remarks>
Private Sub cmdContinuar_Click()
Dim cont As Integer
Dim oItems As CItems
Dim oItem As CItem
Dim oGrupo As CGrupo
Dim sCadenaItems As String
Dim dFechaInicio As Date
Dim dFechaFin As Date
Set oItems = oFSGSRaiz.Generar_CItems
cont = 0
sCadenaItems = ""
dFechaInicio = Date

For Each oGrupo In m_oProceso.Grupos
    If Not oGrupo.Items Is Nothing Then
        For Each oItem In oGrupo.Items
            If oItem.Cerrado Then
                oItems.AnyadirItemACol oItem
                If oItem.FechaInicioSuministro < dFechaInicio Then dFechaInicio = oItem.FechaInicioSuministro
                If oItem.FechaFinSuministro > dFechaFin Then dFechaFin = oItem.FechaFinSuministro
                If sCadenaItems <> "" Then sCadenaItems = sCadenaItems & "_"
                sCadenaItems = sCadenaItems & oItem.Id
            End If
        Next
    End If
Next
If oItems.Count = 0 Then
    MsgBox m_sIdiMensaje, vbExclamation, "FULLSTEP"
    Exit Sub
End If

frmCONTRWizard.g_sCadenaItems = sCadenaItems
frmCONTRWizard.g_dFechaInicioSuministro = dFechaInicio
frmCONTRWizard.g_dFechaFinSuministro = dFechaFin
Set frmCONTRWizard.g_oItemsProce = oItems
Set oItems = Nothing

frmCONTRWizard.WizardContrato
Unload Me

End Sub

Private Sub cmdVolver_Click()
    m_bVolver = True
    
    Unload Me
    frmCONTRWizard.Show
End Sub

''' <summary>
''' Carga la informacion del formulario
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo:0,4seg.</remarks>
Private Sub Form_Load()
    Dim i As Integer

    Me.Height = 5475
    Me.Width = 9045
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    'Obtiene el n� de decimales a mostrar en esta pantalla
    m_sFormatoNumber = "#,##0."
    For i = 1 To gParametrosInstalacion.giNumDecimalesComp
        m_sFormatoNumber = m_sFormatoNumber & "0"
    Next i
    
    CargarRecursos
    
    Set m_oProceso = frmCONTRWizard.m_oProcesoSeleccionado
    m_bVolver = False
    
    PonerFieldSeparator Me
    
    CargarTabs
End Sub
''' <summary>
''' Descarga las variables
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo:0seg.</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Set m_oAllItemsAdjudicados = Nothing
    Set m_oAllAdjudicaciones = Nothing
    Set m_oProceso = Nothing
    g_sCodProve = ""
    If Not m_bVolver Then
        Unload frmCONTRWizard
    End If
End Sub

Private Sub sdbgContrato_change()
    sdbgContrato.Update
    If sdbgContrato.Columns("INC").Value = "1" Or sdbgContrato.Columns("INC").Value = "-1" Then
        If Not m_oAllItemsAdjudicados Is Nothing Then
            m_oAllItemsAdjudicados.Item(sdbgContrato.Columns("ID").Value).Cerrado = True
        End If
        m_oProceso.Grupos.Item(sdbgContrato.Columns("GRUPO").Value).Items.Item(sdbgContrato.Columns("ID").Value).Cerrado = True
    Else
        If Not m_oAllItemsAdjudicados Is Nothing Then
            m_oAllItemsAdjudicados.Item(sdbgContrato.Columns("ID").Value).Cerrado = False
        End If
        m_oProceso.Grupos.Item(sdbgContrato.Columns("GRUPO").Value).Items.Item(sdbgContrato.Columns("ID").Value).Cerrado = False
    End If
End Sub

''' <summary>
''' Carga en los tab los distintos grupos del proceso
''' </summary>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,35</remarks>
Private Sub CargarTabs()
    Dim i As Integer
    Dim oGrupos As CGrupos
    Dim Ador As Ador.Recordset

    m_oProceso.CargarTodosLosGrupos bSinpres:=True
    Set oGrupos = oFSGSRaiz.Generar_CGrupos
    Set Ador = oGrupos.DevolverGruposConAdjudicaciones(m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, frmCONTRWizard.g_oProveSeleccionado.Cod)
    
    'Primero borro los tabs
    i = sstabGrupos.Tabs.Count
    While i > 0
        sstabGrupos.Tabs.Remove i
        i = sstabGrupos.Tabs.Count
    Wend

    'Relleno los tabs con los grupos del proceso
    If Not Ador.EOF Then
        i = 0
        While Not Ador.EOF
            i = i + 1
            sstabGrupos.Tabs.Add i, "A" & Ador(0).Value, Ador(0).Value & " - " & Ador(1).Value
            sstabGrupos.Tabs(i).Tag = Ador(0).Value
            m_oProceso.Grupos.Item(Ador(0).Value).CargarItemsAdjudicadosAProveedor (g_sCodProve)
            Ador.MoveNext
        Wend
        Ador.Close
        Set Ador = Nothing
        'Ahora a�ade el tab para todos los items (ALL)
        Set m_oAllItemsAdjudicados = Nothing
        Set m_oAllAdjudicaciones = Nothing
        If i > 1 Then
            i = i + 1
            sstabGrupos.Tabs.Add i, "ALL", m_sALL
            sstabGrupos.Tabs(i).Tag = "ALL"
            
            Set m_oAllItemsAdjudicados = oFSGSRaiz.Generar_CItems
            Set m_oAllAdjudicaciones = m_oAllItemsAdjudicados.CargarItemsAdjudicadosAProveedor(m_oProceso.Anyo, m_oProceso.GMN1Cod, m_oProceso.Cod, g_sCodProve, m_oProceso.Cambio)
        End If
    End If
    
    sstabGrupos.TabIndex = 1
    sstabGrupos.Tag = sstabGrupos.selectedItem.Tag
    CargarGrid (sstabGrupos.Tabs(1).Tag)
End Sub

''' <summary>
''' Carga en la grid con los items del grupo
''' </summary>
''' <remarks>Llamada desde:CargaTabs//SSTabGrupos_Click; Tiempo m�ximo:0,35</remarks>
Private Sub CargarGrid(Optional ByVal sGrupo As String)
Dim oItem As CItem
Dim iInc As Integer
Dim dCantidadAdj As Double
Dim sCod As String
Dim oAdj As CAdjudicacion
Dim sLinea As String

If sGrupo <> "" Then
    If m_oProceso.Grupos.Item(sGrupo) Is Nothing Then Exit Sub
    If m_oProceso.Grupos.Item(sGrupo).Items Is Nothing Then Exit Sub
    If m_oProceso.Grupos.Item(sGrupo).Adjudicaciones Is Nothing Then Exit Sub
    
    sdbgContrato.RemoveAll
    With m_oProceso.Grupos.Item(sGrupo)
        For Each oItem In .Items
            If oItem.Cerrado = False Then
                iInc = 0
            Else
                iInc = 1
            End If
            sCod = g_sCodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(g_sCodProve))
            sCod = CStr(oItem.Id) & sCod
            If Not .Adjudicaciones.Item(sCod) Is Nothing Then
                Set oAdj = .Adjudicaciones.Item(sCod)
                dCantidadAdj = (oAdj.Porcentaje / 100) * oItem.Cantidad
                sLinea = CStr(iInc) & Chr(m_lSeparador) & oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador)
                sLinea = sLinea & oItem.DestCod & Chr(m_lSeparador) & FormateoNumerico(dCantidadAdj, m_sFormatoNumber) & Chr(m_lSeparador) & oItem.UniCod & Chr(m_lSeparador)
                sLinea = sLinea & FormateoNumerico(oAdj.Precio, m_sFormatoNumber) & Chr(m_lSeparador) & FormateoNumerico(oItem.ImporteAdj, m_sFormatoNumber) & Chr(m_lSeparador) & oItem.PagCod & Chr(m_lSeparador) & oItem.Id & Chr(m_lSeparador) & oItem.GrupoCod
                sdbgContrato.AddItem sLinea
            End If
        Next
    End With
Else
    If m_oAllItemsAdjudicados Is Nothing Then Exit Sub
    If m_oAllAdjudicaciones Is Nothing Then Exit Sub
    
    sdbgContrato.RemoveAll
    For Each oItem In m_oAllItemsAdjudicados
        If oItem.Cerrado = False Then
            iInc = 0
        Else
            iInc = 1
        End If
        sCod = g_sCodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(g_sCodProve))
        sCod = CStr(oItem.Id) & sCod
        If Not m_oAllAdjudicaciones.Item(sCod) Is Nothing Then
            Set oAdj = m_oAllAdjudicaciones.Item(sCod)
            dCantidadAdj = (oAdj.Porcentaje / 100) * oItem.Cantidad
            sLinea = CStr(iInc) & Chr(m_lSeparador) & oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador)
            sLinea = sLinea & oItem.DestCod & Chr(m_lSeparador) & FormateoNumerico(dCantidadAdj, m_sFormatoNumber) & Chr(m_lSeparador) & oItem.UniCod & Chr(m_lSeparador)
            sLinea = sLinea & FormateoNumerico(oAdj.Precio, m_sFormatoNumber) & Chr(m_lSeparador) & FormateoNumerico(oItem.ImporteAdj, m_sFormatoNumber) & Chr(m_lSeparador) & oItem.PagCod & Chr(m_lSeparador) & oItem.Id & Chr(m_lSeparador) & oItem.GrupoCod
            sdbgContrato.AddItem sLinea
        End If
    Next
    
End If
End Sub

Private Sub SSTabGrupos_Click()
    'Si seleccionamos el mismo tab que en el que nos encontramos:
    If sstabGrupos.selectedItem.Tag = sstabGrupos.Tag Then Exit Sub
    
    'bloqueo el tab hasta que acabe la carga
    sstabGrupos.Enabled = False
    Me.Enabled = False
    
    'sino cambiamos de tab:
    If sstabGrupos.selectedItem.Tag = "ALL" Then
        CargarGrid
    Else
        CargarGrid sstabGrupos.selectedItem.Tag
    End If
    
    'una vez que est� cargado vuelve a habilitar el tab
    sstabGrupos.Enabled = True
    Me.Enabled = True
    
    sstabGrupos.Tag = sstabGrupos.selectedItem.Tag
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONTR_WIZARDITEM, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value '1
        m_sIdiCaption = Ador(0).Value
        Ador.MoveNext
        lblSel.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdContinuar.caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiMensaje = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContrato.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        m_sALL = Ador(0).Value
        Ador.Close
    End If

    Set Ador = Nothing

    cmdVolver.caption = " <<      "
End Sub
