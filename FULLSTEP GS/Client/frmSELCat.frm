VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELCat 
   BackColor       =   &H00808000&
   Caption         =   "DSelecci�n de categor�as"
   ClientHeight    =   4965
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5250
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELCat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4965
   ScaleWidth      =   5250
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   3510
      ScaleHeight     =   735
      ScaleWidth      =   1575
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   1635
      Begin VB.Label lblDato2 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Label3"
         Height          =   195
         Left            =   60
         TabIndex        =   6
         Top             =   480
         Width           =   1500
      End
      Begin VB.Label lblDato1 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Label2"
         Height          =   195
         Left            =   60
         TabIndex        =   5
         Top             =   255
         Width           =   1500
      End
      Begin VB.Label lblDescDatos 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Datos adicionales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   4
         Top             =   0
         Width           =   1485
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   2685
      TabIndex        =   2
      Top             =   4560
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1500
      TabIndex        =   1
      Top             =   4560
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrCat 
      Height          =   4335
      Left            =   150
      TabIndex        =   0
      Top             =   120
      Width           =   4995
      _ExtentX        =   8811
      _ExtentY        =   7646
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":0CB2
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":0DC4
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":0ED6
            Key             =   "RAIZ"
            Object.Tag             =   "RAIZ"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":19A0
            Key             =   "CANDADO"
            Object.Tag             =   "CANDADO"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":1DF2
            Key             =   "Baja"
            Object.Tag             =   "Baja"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":2195
            Key             =   "Marcado"
            Object.Tag             =   "Marcado"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":2532
            Key             =   "CANMarca"
            Object.Tag             =   "CANMarca"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":2984
            Key             =   "CAT_INT"
            Object.Tag             =   "CAT_INT"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELCat.frx":2FAE
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSELCat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oCatsN1 As CCategoriaSN1
Private sIdioma(1) As String
Private sIdiCategoria As String

' Variables d   e restricciones
Public bRUsuAprov As Boolean
Public Copia As Long
Public CopiaNivel As Integer
Public bVerBajas As Boolean
' Variables para interactuar con otros forms
Public sOrigen As String
Public g_sCategorias As String 'Variable para guardar las categorias seleccionadas
' Variable de control de flujo
Public Accion As accionessummit

Private iNivel As Integer

Public bProvMat As Boolean
'Variable para pasar al formulario un texto diferente para el caption
Public g_sFormCaption As String

Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean

Private m_sMsgError As String
Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim ACN1Selecc As Variant
Dim ACN2Selecc As Variant
Dim ACN3Selecc As Variant
Dim ACN4Selecc As Variant
Dim ACN5Selecc As Variant
Dim sRama As String
Dim iSeguridadCat As Integer
Dim irespuesta As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwEstrCat.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    
    If nodx.Tag = "Raiz" Then
        If sOrigen = "CopiarSeg" Or _
           sOrigen = "frmSeguimiento" Or _
           sOrigen = "frmCatalogo" Or _
           sOrigen = "frmLstPedidos" Or _
           sOrigen = "frmLstCATSeguridad" Or _
           sOrigen = "frmLstCatalogo" Or _
           sOrigen = "frmCATLINBuscar" Or _
           sOrigen = "frmRESREU" Or _
           sOrigen = "frmADJ" Then
            oMensajes.DebeElegirUnaCategoria
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    If sOrigen = "frmSeguimiento" Then
            Select Case Left(nodx.Tag, 4)
                Case "CAT1"
                    With frmSeguimiento
                        .ACN1Seleccionada = DevolverId(nodx)
                         ACN1Selecc = nodx
                        .ACN2Seleccionada = 0
                        .ACN3Seleccionada = 0
                        .ACN4Seleccionada = 0
                        .ACN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc
                    End With
                    
                Case "CAT2"
                    With frmSeguimiento
                        .ACN1Seleccionada = DevolverId(nodx.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent)
                        .ACN2Seleccionada = DevolverId(nodx)
                         ACN2Selecc = nodx
                        .ACN3Seleccionada = 0
                        .ACN4Seleccionada = 0
                        .ACN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc
                    End With
                    
                Case "CAT3"
                    With frmSeguimiento
                        .ACN1Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent)
                        .ACN2Seleccionada = DevolverId(nodx.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent)
                        .ACN3Seleccionada = DevolverId(nodx)
                         ACN3Selecc = nodx
                        .ACN4Seleccionada = 0
                        .ACN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc
                    End With
                    
                Case "CAT4"
                    With frmSeguimiento
                        .ACN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .ACN2Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent)
                        .ACN3Seleccionada = DevolverId(nodx.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent)
                        .ACN4Seleccionada = DevolverId(nodx)
                         ACN4Selecc = nodx
                        .ACN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc
                    End With
                    
                Case "CAT5"
                    With frmSeguimiento
                        .ACN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                        .ACN2Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .ACN3Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent.Parent)
                        .ACN4Seleccionada = DevolverId(nodx.Parent)
                         ACN4Selecc = DevolverCod(nodx.Parent)
                        .ACN5Seleccionada = DevolverId(nodx)
                         ACN5Selecc = nodx
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc & " - " & ACN5Selecc
                    End With
            End Select
    End If
    
    If sOrigen = "frmLstPedidos" Then
            Select Case Left(nodx.Tag, 4)
                Case "CAT1"
                    With frmLstPedidos
                        .ACN1Seleccionada = DevolverId(nodx)
                         ACN1Selecc = nodx
                        .ACN2Seleccionada = 0
                        .ACN3Seleccionada = 0
                        .ACN4Seleccionada = 0
                        .ACN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc
                    End With
                    
                Case "CAT2"
                    With frmLstPedidos
                        .ACN1Seleccionada = DevolverId(nodx.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent)
                        .ACN2Seleccionada = DevolverId(nodx)
                         ACN2Selecc = nodx
                        .ACN3Seleccionada = 0
                        .ACN4Seleccionada = 0
                        .ACN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc
                    End With
                    
                Case "CAT3"
                    With frmLstPedidos
                        .ACN1Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent)
                        .ACN2Seleccionada = DevolverId(nodx.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent)
                        .ACN3Seleccionada = DevolverId(nodx)
                         ACN3Selecc = nodx
                        .ACN4Seleccionada = 0
                        .ACN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc
                    End With
                    
                Case "CAT4"
                    With frmLstPedidos
                        .ACN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .ACN2Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent)
                        .ACN3Seleccionada = DevolverId(nodx.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent)
                        .ACN4Seleccionada = DevolverId(nodx)
                         ACN4Selecc = nodx
                        .ACN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc
                    End With
                    
                Case "CAT5"
                    With frmLstPedidos
                        .ACN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                        .ACN2Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .ACN3Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent.Parent)
                        .ACN4Seleccionada = DevolverId(nodx.Parent)
                         ACN4Selecc = DevolverCod(nodx.Parent)
                        .ACN5Seleccionada = DevolverId(nodx)
                         ACN5Selecc = nodx
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc & " - " & ACN5Selecc
                    End With
            End Select
    End If
    
    If sOrigen = "frmLstCATSeguridad" Then
        If nodx.Image <> "CANDADO" And nodx.Image <> "CANMarca" Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiCategoria
            Exit Sub
        End If
            With frmLstCATSeguridad
                Select Case Left(nodx.Tag, 4)
                    Case "CAT1"
                            .ACN1Seleccionada = DevolverId(nodx)
                            .ACN2Seleccionada = 0
                            .ACN3Seleccionada = 0
                            .ACN4Seleccionada = 0
                            .ACN5Seleccionada = 0
                            .lSeguridad = DevolverSeguridadCategoria(nodx)
                            .lblCategoria = nodx.Text
                            .lCategoria = DevolverId(nodx)
                            .lNivelCat = 1
                    Case "CAT2"
                            .ACN1Seleccionada = DevolverId(nodx.Parent)
                             ACN1Selecc = DevolverCod(nodx.Parent)
                            .ACN2Seleccionada = DevolverId(nodx)
                             ACN2Selecc = nodx.Text
                            .ACN3Seleccionada = 0
                            .ACN4Seleccionada = 0
                            .ACN5Seleccionada = 0
                            .lSeguridad = DevolverSeguridadCategoria(nodx)
                            .lblCategoria = ACN1Selecc & " - " & ACN2Selecc
                            .lCategoria = DevolverId(nodx)
                            .lNivelCat = 2
                    Case "CAT3"
                            .ACN1Seleccionada = DevolverId(nodx.Parent.Parent)
                             ACN1Selecc = DevolverCod(nodx.Parent.Parent)
                            .ACN2Seleccionada = DevolverId(nodx.Parent)
                             ACN2Selecc = DevolverCod(nodx.Parent)
                            .ACN3Seleccionada = DevolverId(nodx)
                             ACN3Selecc = nodx.Text
                            .ACN4Seleccionada = 0
                            .ACN5Seleccionada = 0
                            .lSeguridad = DevolverSeguridadCategoria(nodx)
                            .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc
                            .lCategoria = DevolverId(nodx)
                            .lNivelCat = 3
                    Case "CAT4"
                            .ACN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                             ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                            .ACN2Seleccionada = DevolverId(nodx.Parent.Parent)
                             ACN2Selecc = DevolverCod(nodx.Parent.Parent)
                            .ACN3Seleccionada = DevolverId(nodx.Parent)
                             ACN3Selecc = DevolverCod(nodx.Parent)
                            .ACN4Seleccionada = DevolverId(nodx)
                             ACN4Selecc = nodx.Text
                            .ACN5Seleccionada = 0
                            .lSeguridad = DevolverSeguridadCategoria(nodx)
                            .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc
                            .lCategoria = DevolverId(nodx)
                            .lNivelCat = 4
                    Case "CAT5"
                            .ACN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent.Parent)
                             ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                            .ACN2Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                             ACN2Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                            .ACN3Seleccionada = DevolverId(nodx.Parent.Parent)
                             ACN3Selecc = DevolverCod(nodx.Parent.Parent)
                            .ACN4Seleccionada = DevolverId(nodx.Parent)
                             ACN4Selecc = DevolverCod(nodx.Parent)
                            .ACN5Seleccionada = DevolverId(nodx)
                             ACN5Selecc = nodx.Text
                             .lSeguridad = DevolverSeguridadCategoria(nodx)
                            .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc & " - " & ACN5Selecc
                            .lCategoria = DevolverId(nodx)
                            .lNivelCat = 5
                End Select
            End With
    End If
    
    
    If sOrigen = "frmCATLINBuscar" Then
    
            Select Case Left(nodx.Tag, 4)
                Case "CAT1"
                    With frmCATLINBuscar
                        .iCAT1Seleccionada = DevolverId(nodx)
                         ACN1Selecc = nodx
                        .iCAT2Seleccionada = 0
                        .iCAT3Seleccionada = 0
                        .iCAT4Seleccionada = 0
                        .iCAT5Seleccionada = 0
                        .txtEstCat = ACN1Selecc
                    End With
                    
                Case "CAT2"
                    With frmCATLINBuscar
                        .iCAT1Seleccionada = DevolverId(nodx.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent)
                        .iCAT2Seleccionada = DevolverId(nodx)
                         ACN2Selecc = nodx
                        .iCAT3Seleccionada = 0
                        .iCAT4Seleccionada = 0
                        .iCAT5Seleccionada = 0
                        .txtEstCat = ACN1Selecc & " - " & ACN2Selecc
                    End With
                    
                    
                Case "CAT3"
                    With frmCATLINBuscar
                        .iCAT1Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent)
                        .iCAT2Seleccionada = DevolverId(nodx.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent)
                        .iCAT3Seleccionada = DevolverId(nodx)
                         ACN3Selecc = nodx
                        .iCAT4Seleccionada = 0
                        .iCAT5Seleccionada = 0
                        .txtEstCat = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc
                    End With
                    
                Case "CAT4"
                    With frmCATLINBuscar
                        .iCAT1Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .iCAT2Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent)
                        .iCAT3Seleccionada = DevolverId(nodx.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent)
                        .iCAT4Seleccionada = DevolverId(nodx)
                         ACN4Selecc = nodx
                        .iCAT5Seleccionada = 0
                        .txtEstCat = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc
                    End With
                    
                Case "CAT5"
                    With frmCATLINBuscar
                        .iCAT1Seleccionada = DevolverId(nodx.Parent.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                        .iCAT2Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .iCAT3Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent.Parent)
                        .iCAT4Seleccionada = DevolverId(nodx.Parent)
                         ACN4Selecc = DevolverCod(nodx.Parent)
                        .iCAT5Seleccionada = DevolverId(nodx)
                         ACN5Selecc = nodx
                        .txtEstCat = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc & " - " & ACN5Selecc
                    End With
            End Select
                    
    End If
    
    If sOrigen = "frmCatalogo" Then
            'S�lo pueden elegir categor�as hoja terminales
            If nodx.Children <> 0 Then
                Screen.MousePointer = vbNormal
                oMensajes.CategoriaNodoHoja
                Exit Sub
            End If
            Select Case Left(nodx.Tag, 4)
            
                Case "CAT1"
                    If nodx.Image <> "CANDADO" And nodx.Image <> "CANMArca" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.CategoriaConSeguridad
                        Exit Sub
                    End If
                    irespuesta = oMensajes.CambiarLineasDeCategoria()
                    If irespuesta = vbNo Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    With frmCatalogo
                        .CAT1Seleccionada = DevolverId(nodx)
                        .CAT2Seleccionada = Null
                        .CAT3Seleccionada = Null
                        .CAT4Seleccionada = Null
                        .CAT5Seleccionada = Null
                        .SeguridadCat = DevolverSeguridadCategoria(nodx)
                    End With
                    
                Case "CAT2"
                    If nodx.Image <> "CANDADO" And nodx.Image <> "CANMArca" _
                        And nodx.Parent.Image <> "CANDADO" And nodx.Parent.Image <> "CANMArca" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.CategoriaConSeguridad
                        Exit Sub
                    End If
                    irespuesta = oMensajes.CambiarLineasDeCategoria()
                    If irespuesta = vbNo Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    With frmCatalogo
                        .CAT1Seleccionada = DevolverId(nodx.Parent)
                        .CAT2Seleccionada = DevolverId(nodx)
                        .CAT3Seleccionada = Null
                        .CAT4Seleccionada = Null
                        .CAT5Seleccionada = Null
                        iSeguridadCat = DevolverSeguridadCategoria(nodx)
                        If iSeguridadCat = 0 Then
                            iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent)
                        End If
                        .SeguridadCat = iSeguridadCat
                    End With
                    
                    
                Case "CAT3"
                    
                    If nodx.Image <> "CANDADO" And nodx.Parent.Image <> "CANDADO" And nodx.Parent.Parent.Image <> "CANDADO" _
                       And nodx.Image <> "CANMarca" And nodx.Parent.Image <> "CANMarca" And nodx.Parent.Parent.Image <> "CANMarca" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.CategoriaConSeguridad
                        Exit Sub
                    End If
                    irespuesta = oMensajes.CambiarLineasDeCategoria()
                    If irespuesta = vbNo Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    With frmCatalogo
                        .CAT1Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent)
                        .CAT2Seleccionada = DevolverId(nodx.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent)
                        .CAT3Seleccionada = DevolverId(nodx)
                         ACN3Selecc = nodx
                        .CAT4Seleccionada = Null
                        .CAT5Seleccionada = Null
                        iSeguridadCat = DevolverSeguridadCategoria(nodx)
                        If iSeguridadCat = 0 Then
                            iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent)
                            If iSeguridadCat = 0 Then
                                iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent)
                            End If
                        End If
                        .SeguridadCat = iSeguridadCat
                    End With
                    
                Case "CAT4"
                    If nodx.Image <> "CANDADO" And nodx.Parent.Image <> "CANDADO" And nodx.Parent.Parent.Image <> "CANDADO" And nodx.Parent.Parent.Parent.Image <> "CANDADO" And _
                       nodx.Image <> "CANMarca" And nodx.Parent.Image <> "CANMarca" And nodx.Parent.Parent.Image <> "CANMarca" And nodx.Parent.Parent.Parent.Image <> "CANMarca" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.CategoriaConSeguridad
                        Exit Sub
                    End If
                    irespuesta = oMensajes.CambiarLineasDeCategoria()
                    If irespuesta = vbNo Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    With frmCatalogo
                        .CAT1Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                        .CAT2Seleccionada = DevolverId(nodx.Parent.Parent)
                        .CAT3Seleccionada = DevolverId(nodx.Parent)
                        .CAT4Seleccionada = DevolverId(nodx)
                        .CAT5Seleccionada = Null
                        iSeguridadCat = DevolverSeguridadCategoria(nodx)
                        If iSeguridadCat = 0 Then
                            iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent)
                            If iSeguridadCat = 0 Then
                                iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent)
                                If iSeguridadCat = 0 Then
                                    iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent.Parent)
                                End If
                            End If
                        End If
                        .SeguridadCat = iSeguridadCat
                    End With
                    
                Case "CAT5"
                    
                    If nodx.Image <> "CANDADO" And nodx.Parent.Image <> "CANDADO" And nodx.Parent.Parent.Image <> "CANDADO" And nodx.Parent.Parent.Parent.Image <> "CANDADO" And nodx.Parent.Parent.Parent.Parent.Image <> "CANDADO" And _
                       nodx.Image <> "CANMarca" And nodx.Parent.Image <> "CANMarca" And nodx.Parent.Parent.Image <> "CANMarca" And nodx.Parent.Parent.Parent.Image <> "CANMarca" And nodx.Parent.Parent.Parent.Parent.Image <> "CANMarca" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.CategoriaConSeguridad
                        Exit Sub
                    End If
                    irespuesta = oMensajes.CambiarLineasDeCategoria()
                    If irespuesta = vbNo Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    With frmCatalogo
                        .CAT1Seleccionada = DevolverId(nodx.Parent.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                        .CAT2Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .CAT3Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent.Parent)
                        .CAT4Seleccionada = DevolverId(nodx.Parent)
                         ACN4Selecc = DevolverCod(nodx.Parent)
                        .CAT5Seleccionada = DevolverId(nodx)
                         ACN5Selecc = nodx
                        iSeguridadCat = DevolverSeguridadCategoria(nodx)
                        If iSeguridadCat = 0 Then
                            iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent)
                            If iSeguridadCat = 0 Then
                                iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent)
                                If iSeguridadCat = 0 Then
                                    iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent.Parent)
                                    If iSeguridadCat = 0 Then
                                        iSeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent.Parent.Parent)
                                    End If
                                End If
                            End If
                        End If
                        .SeguridadCat = iSeguridadCat
                    End With
            End Select
        frmCatalogo.bContinuarCambioCat = True
    End If
    
    If sOrigen = "CopiarSeg" Then
        If nodx.Image = "CANDADO" Or nodx.Image = "CANMarca" Then
            sRama = DevolverNombreRama(nodx)
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleConfigurarSeguridad sRama
            Exit Sub
        End If
        sRama = ComprobarSeguridadDeRama(nodx)
        If sRama <> "" And sRama <> "subir" Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleConfigurarSeguridad sRama
            Exit Sub
        End If
        
        With frmCatalogo
            .CopiarACategoria = DevolverId(nodx)
            Select Case Left(nodx.Tag, 4)
            Case "CAT1"
                .CopiarANivel = 1
                .CAT1Seleccionada = DevolverCod(nodx)

            Case "CAT2"
                .CopiarANivel = 2
                .CAT1Seleccionada = DevolverCod(nodx.Parent)
                .CAT2Seleccionada = DevolverCod(nodx)
            Case "CAT3"
                .CopiarANivel = 3
                .CAT1Seleccionada = DevolverCod(nodx.Parent.Parent)
                .CAT2Seleccionada = DevolverCod(nodx.Parent)
                .CAT3Seleccionada = DevolverCod(nodx)
            Case "CAT4"
                .CopiarANivel = 4
                .CAT1Seleccionada = DevolverCod(nodx.Parent.Parent.Parent)
                .CAT2Seleccionada = DevolverCod(nodx.Parent.Parent)
                .CAT3Seleccionada = DevolverCod(nodx.Parent)
                .CAT4Seleccionada = DevolverCod(nodx)
            Case "CAT5"
                frmCatalogo.CopiarANivel = 5
                .CAT1Seleccionada = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                .CAT2Seleccionada = DevolverCod(nodx.Parent.Parent.Parent)
                .CAT3Seleccionada = DevolverCod(nodx.Parent.Parent)
                .CAT4Seleccionada = DevolverCod(nodx.Parent)
                .CAT5Seleccionada = DevolverCod(nodx)
            End Select
            If sRama = "subir" Then
                .SubirSeguridad = True
            Else
                .SubirSeguridad = False
            End If
        End With
    End If
    
    If sOrigen = "frmLstCatalogo" Then
        With frmLstCatalogo
            Select Case Left(nodx.Tag, 4)
                Case "CAT1"
                        .CATN1Seleccionada = DevolverId(nodx)
                         ACN1Selecc = nodx.Text
                        .CATN2Seleccionada = 0
                        .CATN3Seleccionada = 0
                        .CATN4Seleccionada = 0
                        .CATN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc
                    
                Case "CAT2"
                        .CATN1Seleccionada = DevolverId(nodx.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent)
                        .CATN2Seleccionada = DevolverId(nodx)
                         ACN2Selecc = nodx
                        .CATN3Seleccionada = 0
                        .CATN4Seleccionada = 0
                        .CATN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc
                    
                Case "CAT3"
                        .CATN1Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent)
                        .CATN2Seleccionada = DevolverId(nodx.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent)
                        .CATN3Seleccionada = DevolverId(nodx)
                         ACN3Selecc = nodx
                        .CATN4Seleccionada = 0
                        .CATN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc
                    
                Case "CAT4"
                        .CATN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .CATN2Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent)
                        .CATN3Seleccionada = DevolverId(nodx.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent)
                        .CATN4Seleccionada = DevolverId(nodx)
                         ACN4Selecc = nodx
                        .CATN5Seleccionada = 0
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc
                    
                Case "CAT5"
                        .CATN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent.Parent)
                         ACN1Selecc = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                        .CATN2Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                         ACN2Selecc = DevolverCod(nodx.Parent.Parent.Parent)
                        .CATN3Seleccionada = DevolverId(nodx.Parent.Parent)
                         ACN3Selecc = DevolverCod(nodx.Parent.Parent)
                        .CATN4Seleccionada = DevolverId(nodx.Parent)
                         ACN4Selecc = DevolverCod(nodx.Parent)
                        .CATN5Seleccionada = DevolverId(nodx)
                         ACN5Selecc = nodx
                        .lblCategoria = ACN1Selecc & " - " & ACN2Selecc & " - " & ACN3Selecc & " - " & ACN4Selecc & " - " & ACN5Selecc
            End Select
        
            If nodx.Children > 0 Then
                .bCatHijos = True
            End If
        End With
    End If
    
    If sOrigen = "frmRESREU" Or sOrigen = "frmADJ" Then
        'Si es baja l�gica o marcado para baja l�gica, no se pueden a�adir art�culos
        If nodx.Image = "Marcado" Or nodx.Image = "Baja" Or nodx.Image = "CANMarca" Then
            Screen.MousePointer = vbNormal
            oMensajes.CategoriaBajaLogica
            Exit Sub
        End If
        'Si no es el �ltimo nivel, no se pueden a�adir art�culos
        If nodx.Children <> 0 Then
            Screen.MousePointer = vbNormal
            oMensajes.CategoriaNodoHoja
            Exit Sub
        End If
    
        Select Case sOrigen
            Case "frmRESREU"
                With frmRESREU
                    Set .g_oNodx = nodx
                    .bContinuarCambioCat = True
                End With
            Case "frmADJ"
                With frmADJ
                    Set .g_oNodx = nodx
                    .bContinuarCambioCat = True
                End With
        End Select
    End If
    
    Screen.MousePointer = vbNormal
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sOrigen = "frmCatalogo" Then
        frmCatalogo.bContinuarCambioCat = False
    End If
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELCAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sIdioma(1) = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value '8
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiCategoria = Ador(0).Value
        Ador.MoveNext
        lblDescDatos.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub


Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
    
    Dim sCaption As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    CargarRecursos
    
    Me.Height = 5385
    Me.Width = 5355
    
    sCaption = Me.caption
        
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    DoEvents
    
    GenerarEstructuraCat False
         
    Me.caption = sCaption
    
    If g_sFormCaption <> "" Then
        Me.caption = g_sFormCaption
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 1200 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    tvwEstrCat.Height = Me.Height - 1030
    tvwEstrCat.Width = Me.Width - 360
    cmdAceptar.Top = Me.Height - 825
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (tvwEstrCat.Width / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (tvwEstrCat.Width / 2) + 300
    
    picDatos.Left = tvwEstrCat.Width - 1485
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oCatsN1 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function

    'posici�n del primer -%$
    iPosicion = InStr(6, node.Tag, "-%$")
    DevolverCod = Mid(node.Tag, 6, iPosicion - 6)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion1 As Integer
Dim iPosicion2 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function
    
    'posici�n del primer -%$,justo �ntes del ID
    iPosicion1 = InStr(6, node.Tag, "-%$")
    iPosicion1 = iPosicion1 + 3
    'posici�n del segundo -%$
    iPosicion2 = InStr(iPosicion1, node.Tag, "-%$")
    If iPosicion2 = 0 Then
        DevolverId = val(Right(node.Tag, Len(node.Tag) - (iPosicion1 - 1)))
    Else
        DevolverId = val(Mid(node.Tag, iPosicion1, iPosicion2 - iPosicion1))
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "DevolverId", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function


Public Sub GenerarEstructuraCat(ByVal bOrdenadoPorDen As Boolean)
Dim oCATN1 As CCategoriaN1
Dim oCatN2 As CCategoriaN2
Dim oCatN3 As CCategoriaN3
Dim oCatN4 As CCategoriaN4
Dim oCatN5 As CCategoriaN5

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Dim nodx As MSComctlLib.node
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
tvwEstrCat.Nodes.clear

Set nodx = tvwEstrCat.Nodes.Add(, , "Raiz", sIdioma(1), "RAIZ")
nodx.Tag = "Raiz"
nodx.Expanded = True

'Generamos una estructura diferente en funci�n del formulario de origen
'Para el caso de frmRESREU y frmADJ hay que hacerla mim�tica a la del treeview tvwCategorias de frmCatalogo
If sOrigen <> "frmRESREU" And sOrigen <> "frmADJ" Then
    Set oCatsN1 = oFSGSRaiz.Generar_CCategoriasN1
    If Not bRUsuAprov Then
        If bProvMat Then
            oCatsN1.GenerarEstructuraCategoriasDeUsuario oUsuarioSummit.Persona.Cod, bVerBajas, bProvMat
        Else
            oCatsN1.GenerarEstructuraCategorias bVerBajas, bOrdenadoPorDen
        End If
    Else
    '    oCatsN1.GenerarEstructuraCategoriasdeusuarios False, bOrdenadoPorDen, oUsuarioSummit.persona
         oCatsN1.GenerarEstructuraCategoriasDeUsuario oUsuarioSummit.Persona.Cod, bVerBajas
    End If
    
    If Not oCatsN1 Is Nothing Then
           
      For Each oCATN1 In oCatsN1
        
        scod1 = CStr(oCATN1.Id) & Mid$("                         ", 1, 10 - Len(CStr(oCATN1.Id)))
        Set nodx = tvwEstrCat.Nodes.Add("Raiz", tvwChild, "CAT1" & scod1, oCATN1.Cod & " - " & oCATN1.Den, "ACT")
        nodx.Tag = "CAT1-" & oCATN1.Cod & "-%$" & oCATN1.Id
        If oCATN1.Seguridad <> 0 Then
            If oCATN1.BajaLogica = MarcadoParaBaja And bVerBajas Then
                nodx.Image = "CANMarca"
            Else
                nodx.Image = "CANDADO"
            End If
            nodx.Tag = nodx.Tag & "-%$SEG" & oCATN1.Seguridad
        Else
            Select Case oCATN1.BajaLogica
            Case MarcadoParaBaja
                nodx.Image = "Marcado"
            Case BajaLogica
                nodx.Image = "Baja"
            End Select
        End If
        
        nodx.Expanded = False
        
        
        If Not oCATN1.CategoriasN2 Is Nothing Then
        
            For Each oCatN2 In oCATN1.CategoriasN2
                scod2 = CStr(oCatN2.Id) & Mid$("                         ", 1, 10 - Len(CStr(oCatN2.Id)))
                Set nodx = tvwEstrCat.Nodes.Add("CAT1" & scod1, tvwChild, "CAT2" & scod1 & scod2, oCatN2.Cod & " - " & oCatN2.Den, "ACT")
                nodx.Tag = "CAT2-" & oCatN2.Cod & "-%$" & oCatN2.Id
                If oCatN2.Seguridad <> 0 Then
                    If oCatN2.BajaLogica = MarcadoParaBaja And bVerBajas Then
                        nodx.Image = "CANMarca"
                    Else
                        nodx.Image = "CANDADO"
                    End If
                    nodx.Tag = nodx.Tag & "-%$SEG" & oCatN2.Seguridad
                Else
                    Select Case oCatN2.BajaLogica
                    Case MarcadoParaBaja
                        nodx.Image = "Marcado"
                    Case BajaLogica
                        nodx.Image = "Baja"
                    End Select
                
                End If
                
                nodx.Expanded = False
                
                If Not oCatN2.CategoriasN3 Is Nothing Then
                    For Each oCatN3 In oCatN2.CategoriasN3
                        scod3 = CStr(oCatN3.Id) & Mid$("                         ", 1, 10 - Len(CStr(oCatN3.Id)))
                        Set nodx = tvwEstrCat.Nodes.Add("CAT2" & scod1 & scod2, tvwChild, "CAT3" & scod1 & scod2 & scod3, oCatN3.Cod & " - " & oCatN3.Den, "ACT")
                        nodx.Tag = "CAT3-" & oCatN3.Cod & "-%$" & oCatN3.Id
                        If oCatN3.Seguridad <> 0 Then
                            If oCatN3.BajaLogica = MarcadoParaBaja And bVerBajas Then
                                nodx.Image = "CANMarca"
                            Else
                                nodx.Image = "CANDADO"
                            End If
                            nodx.Tag = nodx.Tag & "-%$SEG" & oCatN3.Seguridad
                        Else
                            Select Case oCatN3.BajaLogica
                            Case MarcadoParaBaja
                                nodx.Image = "Marcado"
                            Case BajaLogica
                                nodx.Image = "Baja"
                            End Select
                        End If
                        
                        nodx.Expanded = False
                        
                        If Not oCatN3.CategoriasN4 Is Nothing Then
                        
                            For Each oCatN4 In oCatN3.CategoriasN4
                                scod4 = CStr(oCatN4.Id) & Mid$("                         ", 1, 10 - Len(CStr(oCatN4.Id)))
                                Set nodx = tvwEstrCat.Nodes.Add("CAT3" & scod1 & scod2 & scod3, tvwChild, "CAT4" & scod1 & scod2 & scod3 & scod4, oCatN4.Cod & " - " & oCatN4.Den, "ACT")
                                nodx.Tag = "CAT4-" & oCatN4.Cod & "-%$" & oCatN4.Id
                                If oCatN4.Seguridad <> 0 Then
                                    If oCatN4.BajaLogica = MarcadoParaBaja And bVerBajas Then
                                        nodx.Image = "CANMarca"
                                    Else
                                        nodx.Image = "CANDADO"
                                    End If
                                    nodx.Tag = nodx.Tag & "-%$SEG" & oCatN4.Seguridad
                                Else
                                    Select Case oCatN4.BajaLogica
                                    Case MarcadoParaBaja
                                        nodx.Image = "Marcado"
                                    Case BajaLogica
                                        nodx.Image = "Baja"
                                    End Select
                                End If
                                
                                nodx.Expanded = False
                                
                                If Not oCatN4.CategoriasN5 Is Nothing Then
                                
                                    For Each oCatN5 In oCatN4.CategoriasN5
                                        scod5 = CStr(oCatN5.Id) & Mid$("                         ", 1, 10 - Len(CStr(oCatN5.Id)))
                                        Set nodx = tvwEstrCat.Nodes.Add("CAT4" & scod1 & scod2 & scod3 & scod4, tvwChild, "CAT5" & scod1 & scod2 & scod3 & scod4 & scod5, oCatN5.Cod & " - " & oCatN5.Den, "ACT")
                                        nodx.Tag = "CAT5-" & oCatN5.Cod & "-%$" & oCatN5.Id
                                        If oCatN5.Seguridad <> 0 Then
                                            If oCatN5.BajaLogica = MarcadoParaBaja And bVerBajas Then
                                                nodx.Image = "CANMarca"
                                            Else
                                                nodx.Image = "CANDADO"
                                            End If
                                            nodx.Tag = nodx.Tag & "-%$SEG" & oCatN5.Seguridad
                                        Else
                                            Select Case oCatN5.BajaLogica
                                            Case MarcadoParaBaja
                                                nodx.Image = "Marcado"
                                            Case BajaLogica
                                                nodx.Image = "Baja"
                                            End Select
                                        End If
                                        
                                        nodx.Expanded = False
                                    Next
                                    
                                End If
                            Next
                        End If
                    Next
                End If
            Next
        End If
        Next
    End If
Else
    'Todas las categorias
    Set oCatsN1 = oFSGSRaiz.Generar_CCategoriasN1
    If bRUsuAprov Then
        oCatsN1.GenerarEstructuraCategoriasDeUsuario oUsuarioSummit.Persona.Cod, bVerBajas
    Else
        oCatsN1.GenerarEstructuraCategorias bVerBajas, False
    End If
    If Not oCatsN1 Is Nothing Then
        For Each oCATN1 In oCatsN1
            scod1 = oCATN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(oCATN1.Cod))
            Set nodx = tvwEstrCat.Nodes.Add("Raiz", tvwChild, "CAT1" & scod1, oCATN1.Cod & " - " & oCATN1.Den, "ACT")
            nodx.Tag = "CAT1-" & oCATN1.Cod & "-%$" & oCATN1.Id
    
            If bVerBajas Then
                If oCATN1.BajaLogica = 2 Then nodx.Image = "Baja"
            End If
            If oCATN1.Seguridad <> 0 Then
                If oCATN1.BajaLogica = 1 Then
                    nodx.Image = "CANMarca"
                Else
                    If oCATN1.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                        nodx.Image = "CAT_INT"
                    Else
                        nodx.Image = "CANDADO"
                    End If
                End If
                nodx.Tag = nodx.Tag & "-%$SEG" & oCATN1.Seguridad
            Else
                If oCATN1.BajaLogica = 1 Then nodx.Image = "Marcado"
            End If
                
            For Each oCatN2 In oCATN1.CategoriasN2
                scod2 = oCatN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(oCatN2.Cod))
                Set nodx = tvwEstrCat.Nodes.Add("CAT1" & scod1, tvwChild, "CAT2" & scod1 & scod2, oCatN2.Cod & " - " & oCatN2.Den, "ACT")
                nodx.Tag = "CAT2-" & oCatN2.Cod & "-%$" & oCatN2.Id
                If oCatN2.Seguridad <> 0 Then
                    If oCatN2.BajaLogica = 1 Then
                        nodx.Image = "Baja"
                    Else
                        If oCatN2.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                            nodx.Image = "CAT_INT"
                        Else
                            nodx.Image = "CANDADO"
                        End If
                    End If
                    nodx.Tag = nodx.Tag & "-%$SEG" & oCatN2.Seguridad
                Else
                    If oCatN2.BajaLogica = 1 Then nodx.Image = "Marcado"
                End If
                If bVerBajas Then
                    If oCatN2.BajaLogica = 2 Then nodx.Image = "Baja"
                End If
                
                For Each oCatN3 In oCatN2.CategoriasN3
                    scod3 = oCatN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(oCatN3.Cod))
                    Set nodx = tvwEstrCat.Nodes.Add("CAT2" & scod1 & scod2, tvwChild, "CAT3" & scod1 & scod2 & scod3, oCatN3.Cod & " - " & oCatN3.Den, "ACT")
                    nodx.Tag = "CAT3-" & oCatN3.Cod & "-%$" & oCatN3.Id
                    If oCatN3.Seguridad <> 0 Then
                        If oCatN3.BajaLogica = 1 Then
                            nodx.Image = "Baja"
                        Else
                            If oCatN3.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                                nodx.Image = "CAT_INT"
                            Else
                                nodx.Image = "CANDADO"
                            End If
                        End If
                        nodx.Tag = nodx.Tag & "-%$SEG" & oCatN3.Seguridad
                    Else
                        If oCatN3.BajaLogica = 1 Then nodx.Image = "Marcado"
                    End If
                    If bVerBajas Then
                        If oCatN3.BajaLogica = 2 Then nodx.Image = "Baja"
                    End If
                    
                    For Each oCatN4 In oCatN3.CategoriasN4
                        scod4 = oCatN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(oCatN4.Cod))
                        Set nodx = tvwEstrCat.Nodes.Add("CAT3" & scod1 & scod2 & scod3, tvwChild, "CAT4" & scod1 & scod2 & scod3 & scod4, oCatN4.Cod & " - " & oCatN4.Den, "ACT")
                        nodx.Tag = "CAT4-" & oCatN4.Cod & "-%$" & oCatN4.Id
                        If oCatN4.Seguridad <> 0 Then
                            If oCatN4.BajaLogica = 1 Then
                                nodx.Image = "Baja"
                            Else
                                If oCatN4.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                                    nodx.Image = "CAT_INT"
                                Else
                                    nodx.Image = "CANDADO"
                                End If
                            End If
                            nodx.Tag = nodx.Tag & "-%$SEG" & oCatN4.Seguridad
                        Else
                            If oCatN4.BajaLogica = 1 Then nodx.Image = "Marcado"
                        End If
                        If bVerBajas Then
                            If oCatN4.BajaLogica = 2 Then nodx.Image = "Baja"
                        End If
                        
                        For Each oCatN5 In oCatN4.CategoriasN5
                            scod5 = oCatN5.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(oCatN5.Cod))
                            Set nodx = tvwEstrCat.Nodes.Add("CAT4" & scod1 & scod2 & scod3 & scod4, tvwChild, "CAT5" & scod1 & scod2 & scod3 & scod4 & scod5, oCatN5.Cod & " - " & oCatN5.Den, "ACT")
                            nodx.Tag = "CAT5-" & oCatN5.Cod & "-%$" & oCatN5.Id
                            If oCatN5.Seguridad <> 0 Then
                                If oCatN5.BajaLogica = 1 Then
                                    nodx.Image = "Baja"
                                Else
                                    If oCatN5.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                                        nodx.Image = "CAT_INT"
                                    Else
                                        nodx.Image = "CANDADO"
                                    End If
                                End If
                                nodx.Tag = nodx.Tag & "-%$SEG" & oCatN5.Seguridad
                            Else
                                If oCatN5.BajaLogica = 1 Then nodx.Image = "Marcado"
                            End If
                            If bVerBajas Then
                                If oCatN5.BajaLogica = 2 Then nodx.Image = "Baja"
                            End If
                        Next
                    Next
                Next
            Next
        Next
    End If
End If

    Exit Sub

ERROR_Frm:
    Set nodx = Nothing
    Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "GenerarEstructuraCat", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If
    
End Sub



Public Function DevolverSeguridadCategoria(ByVal node As MSComctlLib.node) As Variant
Dim sSegur As String
Dim iPos As Integer
Dim sTag As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sTag = node.Tag
    iPos = InStr(1, sTag, "-%$SEG")
    If iPos <> 0 Then
        sTag = Right(sTag, Len(sTag) - iPos)
        sSegur = Right(sTag, Len(sTag) - 5)
    End If
    
    If sSegur <> "" Then
        DevolverSeguridadCategoria = CLng(sSegur)
    Else
        DevolverSeguridadCategoria = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "DevolverSeguridadCategoria", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function DevolverNombreRama(ByVal nodx As node) As String
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim oCatNivel1 As CCategoriaN1
Dim oCatNivel2 As CCategoriaN2
Dim oCatNivel3 As CCategoriaN3
Dim oCatNivel4 As CCategoriaN4
Dim oCatNivel5 As CCategoriaN5

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
DevolverNombreRama = ""
Select Case Left(nodx.Tag, 4)
    Case "CAT1"
        scod1 = DevolverCod(nodx)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        Set oCatNivel1 = oCatsN1.Item(scod1)
        DevolverNombreRama = oCatNivel1.Cod & " - " & oCatNivel1.Den
        
    Case "CAT2"
        scod1 = DevolverCod(nodx.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        Set oCatNivel2 = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
        DevolverNombreRama = oCatNivel2.Cat1Cod & " - " & oCatNivel2.Cod & " - " & oCatNivel2.Den
        
    Case "CAT3"
        scod1 = DevolverCod(nodx.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        Set oCatNivel3 = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
        DevolverNombreRama = oCatNivel3.Cat1Cod & " - " & oCatNivel3.Cat2Cod & " - " & oCatNivel3.Cod & " - " & oCatNivel3.Den
    
    Case "CAT4"
        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx.Parent)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        scod4 = DevolverCod(nodx)
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
        
        'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
        Set oCatNivel4 = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
        DevolverNombreRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
    
    Case "CAT5"
        scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent.Parent.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx.Parent.Parent)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        scod4 = DevolverCod(nodx.Parent)
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
        scod5 = DevolverCod(nodx)
        scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
        
        Set oCatNivel5 = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5)
        DevolverNombreRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "DevolverNombreRama", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function ComprobarSeguridadDeRama(ByVal nodx As node) As String
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oCatNivel1 As CCategoriaN1
Dim oCatNivel2 As CCategoriaN2
Dim oCatNivel3 As CCategoriaN3
Dim oCatNivel4 As CCategoriaN4
Dim oCatNivel5 As CCategoriaN5

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
ComprobarSeguridadDeRama = ""
'Set oCatSNivel1 = oFSGSRaiz.Generar_CCategoriasN1
'If bRUsuAprov Then
'    oCatSNivel1.GenerarEstructuraCategoriasDeUsuario oUsuarioSummit.persona.cod, True
'Else
'    oCatSNivel1.GenerarEstructuraCategorias True
'End If
'No dejo que haya bjas logias con seguridad
Select Case Left(nodx.Tag, 4)
    Case "CAT1"
        scod1 = DevolverCod(nodx)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        Set oCatNivel1 = oCatsN1.Item(scod1)
        
        For Each oCatNivel2 In oCatNivel1.CategoriasN2
            If oCatNivel2.Seguridad <> 0 Then
                If oCatNivel2.Id = Copia And CopiaNivel = 2 Then
                    'Comprobar que todos los dem�s hermanos  con sus hijos no tienen seguridad
                    If Not ComprobarSeguridadDeHermanos(2, oCatNivel2.Cat1Cod, oCatNivel2.Cod) Then
                        ComprobarSeguridadDeRama = "subir"
                    Else
                        ComprobarSeguridadDeRama = oCatNivel2.Cat1Cod & " - " & oCatNivel2.Cod & " - " & oCatNivel2.Den
                    End If
                Else
                    ComprobarSeguridadDeRama = oCatNivel2.Cat1Cod & " - " & oCatNivel2.Cod & " - " & oCatNivel2.Den
                End If
                Set oCatNivel1 = Nothing
                Set oCatNivel2 = Nothing
                Exit Function
            End If
            For Each oCatNivel3 In oCatNivel2.CategoriasN3
                If oCatNivel3.Seguridad <> 0 Then
                    If oCatNivel3.Id = Copia And CopiaNivel = 3 Then
                        If Not ComprobarSeguridadDeHermanos(3, oCatNivel3.Cat1Cod, oCatNivel3.Cat2Cod, oCatNivel3.Cod) Then
                            ComprobarSeguridadDeRama = "subir"
                        Else
                            ComprobarSeguridadDeRama = oCatNivel3.Cat1Cod & " - " & oCatNivel3.Cat2Cod & " - " & oCatNivel3.Cod & " - " & oCatNivel3.Den
                        End If
                    Else
                        ComprobarSeguridadDeRama = oCatNivel3.Cat1Cod & " - " & oCatNivel3.Cat2Cod & " - " & oCatNivel3.Cod & " - " & oCatNivel3.Den
                    End If
                    Set oCatNivel1 = Nothing
                    Set oCatNivel2 = Nothing
                    Set oCatNivel3 = Nothing
                    Exit Function
                End If
                For Each oCatNivel4 In oCatNivel3.CategoriasN4
                    If oCatNivel4.Seguridad <> 0 Then
                        If oCatNivel4.Id = Copia And CopiaNivel = 4 Then
                            If Not ComprobarSeguridadDeHermanos(4, oCatNivel4.Cat1Cod, oCatNivel4.Cat2Cod, oCatNivel4.CAT3cod, oCatNivel4.Cod) Then
                                ComprobarSeguridadDeRama = "subir"
                            Else
                                ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                            End If
                        Else
                            ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                        End If
                        Set oCatNivel1 = Nothing
                        Set oCatNivel2 = Nothing
                        Set oCatNivel3 = Nothing
                        Set oCatNivel4 = Nothing
                        Exit Function
                    End If
                    For Each oCatNivel5 In oCatNivel4.CategoriasN5
                        If oCatNivel5.Seguridad <> 0 Then
                            If oCatNivel5.Id = Copia And CopiaNivel = 5 Then
                                If Not ComprobarSeguridadDeHermanos(5, oCatNivel5.Cat1Cod, oCatNivel5.Cat2Cod, oCatNivel5.CAT3cod, oCatNivel5.CAT4cod, oCatNivel5.Cod) Then
                                    ComprobarSeguridadDeRama = "subir"
                                Else
                                    ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                                End If
                            Else
                                ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                            End If
                            Set oCatNivel1 = Nothing
                            Set oCatNivel2 = Nothing
                            Set oCatNivel3 = Nothing
                            Set oCatNivel4 = Nothing
                            Set oCatNivel5 = Nothing
                            Exit Function
                        End If
                    Next '5
                Next '4
            Next '3
        Next '2

        
    Case "CAT2"
        If nodx.Parent.Image = "CANDADO" Or nodx.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent) & " - " & DevolverDen(nodx.Parent)
            Exit Function
        Else
            
        End If
        scod1 = DevolverCod(nodx.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        Set oCatNivel2 = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
        
        For Each oCatNivel3 In oCatNivel2.CategoriasN3
            If oCatNivel3.Seguridad <> 0 Then
                If oCatNivel3.Id = Copia And CopiaNivel = 3 Then
                    If Not ComprobarSeguridadDeHermanos(3, oCatNivel3.Cat1Cod, oCatNivel3.Cat2Cod, oCatNivel3.Cod) Then
                        ComprobarSeguridadDeRama = "subir"
                    Else
                        ComprobarSeguridadDeRama = oCatNivel3.Cat1Cod & " - " & oCatNivel3.Cat2Cod & " - " & oCatNivel3.Cod & " - " & oCatNivel3.Den
                    End If
                Else
                    ComprobarSeguridadDeRama = oCatNivel3.Cat1Cod & " - " & oCatNivel3.Cat2Cod & " - " & oCatNivel3.Cod & " - " & oCatNivel3.Den
                End If
                Set oCatNivel1 = Nothing
                Set oCatNivel2 = Nothing
                Set oCatNivel3 = Nothing
                Exit Function
            End If
            For Each oCatNivel4 In oCatNivel3.CategoriasN4
                If oCatNivel4.Seguridad <> 0 Then
                    If oCatNivel4.Id = Copia And CopiaNivel = 4 Then
                        If Not ComprobarSeguridadDeHermanos(4, oCatNivel4.Cat1Cod, oCatNivel4.Cat2Cod, oCatNivel4.CAT3cod, oCatNivel4.Cod) Then
                            ComprobarSeguridadDeRama = "subir"
                        Else
                            ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                        End If
                    Else
                        ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                    End If
                    Set oCatNivel1 = Nothing
                    Set oCatNivel2 = Nothing
                    Set oCatNivel3 = Nothing
                    Set oCatNivel4 = Nothing
                    Exit Function
                End If
                For Each oCatNivel5 In oCatNivel4.CategoriasN5
                    If oCatNivel5.Seguridad <> 0 Then
                        If oCatNivel5.Id = Copia And CopiaNivel = 5 Then
                            If Not ComprobarSeguridadDeHermanos(5, oCatNivel5.Cat1Cod, oCatNivel5.Cat2Cod, oCatNivel5.CAT3cod, oCatNivel5.CAT4cod, oCatNivel5.Cod) Then
                                ComprobarSeguridadDeRama = "subir"
                            Else
                                ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                            End If
                        Else
                            ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                        End If
                        Set oCatNivel1 = Nothing
                        Set oCatNivel2 = Nothing
                        Set oCatNivel3 = Nothing
                        Set oCatNivel4 = Nothing
                        Set oCatNivel5 = Nothing
                        Exit Function
                    End If
                Next '5
            Next '4
        Next '3
    
    Case "CAT3"
        If nodx.Parent.Image = "CANDADO" Or nodx.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverDen(nodx.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Image = "CANDADO" Or nodx.Parent.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent)
            Exit Function
        End If

        
        scod1 = DevolverCod(nodx.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        Set oCatNivel3 = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
        
        For Each oCatNivel4 In oCatNivel3.CategoriasN4
            If oCatNivel4.Seguridad <> 0 Then
                If oCatNivel4.Id = Copia And CopiaNivel = 4 Then
                    If Not ComprobarSeguridadDeHermanos(4, oCatNivel4.Cat1Cod, oCatNivel4.Cat2Cod, oCatNivel4.CAT3cod, oCatNivel4.Cod) Then
                        ComprobarSeguridadDeRama = "subir"
                    Else
                        ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                    End If
                Else
                    ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                End If
                Set oCatNivel1 = Nothing
                Set oCatNivel2 = Nothing
                Set oCatNivel3 = Nothing
                Set oCatNivel4 = Nothing
                Exit Function
            End If
            For Each oCatNivel5 In oCatNivel4.CategoriasN5
                If oCatNivel5.Seguridad <> 0 Then
                    If oCatNivel5.Id = Copia And CopiaNivel = 5 Then
                        If Not ComprobarSeguridadDeHermanos(5, oCatNivel5.Cat1Cod, oCatNivel5.Cat2Cod, oCatNivel5.CAT3cod, oCatNivel5.CAT4cod, oCatNivel5.Cod) Then
                            ComprobarSeguridadDeRama = "subir"
                        Else
                            ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                        End If
                    Else
                        ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                    End If
                    Set oCatNivel1 = Nothing
                    Set oCatNivel2 = Nothing
                    Set oCatNivel3 = Nothing
                    Set oCatNivel4 = Nothing
                    Set oCatNivel5 = Nothing
                    Exit Function
                End If
            Next '5
        Next '4
    
    Case "CAT4"
        If nodx.Parent.Image = "CANDADO" Or nodx.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverDen(nodx.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Image = "CANDADO" Or nodx.Parent.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent) & " - " & (nodx.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Parent.Image = "CANDADO" Or nodx.Parent.Parent.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent.Parent)
            Exit Function
        End If
      
        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx.Parent)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        scod4 = DevolverCod(nodx)
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
        
        'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
        Set oCatNivel4 = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
        
        For Each oCatNivel5 In oCatNivel4.CategoriasN5
            If oCatNivel5.Seguridad <> 0 Then
                If oCatNivel5.Id = Copia And CopiaNivel = 5 Then
                    If Not ComprobarSeguridadDeHermanos(5, oCatNivel5.Cat1Cod, oCatNivel5.Cat2Cod, oCatNivel5.CAT3cod, oCatNivel5.CAT4cod, oCatNivel5.Cod) Then
                        ComprobarSeguridadDeRama = "subir"
                    Else
                        ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                    End If
                Else
                    ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                End If
                Set oCatNivel1 = Nothing
                Set oCatNivel2 = Nothing
                Set oCatNivel3 = Nothing
                Set oCatNivel4 = Nothing
                Set oCatNivel5 = Nothing
                Exit Function
            End If
        Next '5
    
    
    Case "CAT5"
        If nodx.Parent.Image = "CANDADO" Or nodx.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverDen(nodx.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Image = "CANDADO" Or nodx.Parent.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent.Parent) & " - " & (nodx.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Parent.Image = "CANDADO" Or nodx.Parent.Parent.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Parent.Parent.Image = "CANDADO" Or nodx.Parent.Parent.Parent.Parent.Image = "CANMarca" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent.Parent.Parent)
            Exit Function
        End If
        
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "ComprobarSeguridadDeRama", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Public Function DevolverDen(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function

    'posici�n del gui�n
    iPosicion = InStr(1, node.Text, "-")
    DevolverDen = Mid(node.Text, iPosicion + 2, Len(node.Text) - iPosicion)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "DevolverDen", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function ComprobarSeguridadDeHermanos(ByVal Nivel As Integer, ByVal Cod1 As String, Optional ByVal Cod2 As String, Optional ByVal Cod3 As String, Optional ByVal Cod4 As String, Optional ByVal Cod5 As String) As Boolean
Dim oCatN2 As CCategoriaN2
Dim oCatN3 As CCategoriaN3
Dim oCatN4 As CCategoriaN4
Dim oCatN5 As CCategoriaN5

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
ComprobarSeguridadDeHermanos = False
Select Case Nivel
Case 2
    Cod1 = Cod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(Cod1))

    For Each oCatN2 In oCatsN1.Item(Cod1).CategoriasN2
        If oCatN2.Cod <> Cod2 Then
            If oCatN2.Seguridad <> 0 Then
                ComprobarSeguridadDeHermanos = True
                Set oCatN2 = Nothing
                Exit Function
            End If
            For Each oCatN3 In oCatN2.CategoriasN3
                If oCatN3.Seguridad <> 0 Then
                    ComprobarSeguridadDeHermanos = True
                    Set oCatN2 = Nothing
                    Set oCatN3 = Nothing
                    Exit Function
                End If
                For Each oCatN4 In oCatN3.CategoriasN4
                    If oCatN4.Seguridad <> 0 Then
                        ComprobarSeguridadDeHermanos = True
                        Set oCatN2 = Nothing
                        Set oCatN3 = Nothing
                        Set oCatN4 = Nothing
                        Exit Function
                    End If
                    For Each oCatN5 In oCatN4.CategoriasN5
                        If oCatN5.Seguridad <> 0 Then
                            ComprobarSeguridadDeHermanos = True
                            Set oCatN2 = Nothing
                            Set oCatN3 = Nothing
                            Set oCatN4 = Nothing
                            Set oCatN5 = Nothing
                            Exit Function
                        End If
                    Next
                Next
            Next
        End If
    Next
Case 3
    Cod1 = Cod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(Cod1))
    Cod2 = Cod1 & Cod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(Cod2))
    For Each oCatN3 In oCatsN1.Item(Cod1).CategoriasN2.Item(Cod2).CategoriasN3
        If oCatN3.Cod <> Cod3 Then
    
            If oCatN3.Seguridad <> 0 Then
                ComprobarSeguridadDeHermanos = True
                Set oCatN2 = Nothing
                Set oCatN3 = Nothing
                Exit Function
            End If
            For Each oCatN4 In oCatN3.CategoriasN4
                If oCatN4.Seguridad <> 0 Then
                    ComprobarSeguridadDeHermanos = True
                    Set oCatN2 = Nothing
                    Set oCatN3 = Nothing
                    Set oCatN4 = Nothing
                    Exit Function
                End If
                For Each oCatN5 In oCatN4.CategoriasN5
                    If oCatN5.Seguridad <> 0 Then
                        ComprobarSeguridadDeHermanos = True
                        Set oCatN2 = Nothing
                        Set oCatN3 = Nothing
                        Set oCatN4 = Nothing
                        Set oCatN5 = Nothing
                        Exit Function
                    End If
                Next
            Next
        End If
    Next
Case 4
    Cod1 = Cod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(Cod1))
    Cod2 = Cod1 & Cod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(Cod2))
    Cod3 = Cod2 & Cod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(Cod3))
    For Each oCatN4 In oCatsN1.Item(Cod1).CategoriasN2.Item(Cod2).CategoriasN3.Item(Cod3).CategoriasN4
        If oCatN4.Cod <> Cod4 Then
            If oCatN4.Seguridad <> 0 Then
                ComprobarSeguridadDeHermanos = True
                Set oCatN4 = Nothing
                Exit Function
            End If
            For Each oCatN5 In oCatN4.CategoriasN5
                If oCatN5.Seguridad <> 0 Then
                    ComprobarSeguridadDeHermanos = True
                    Set oCatN4 = Nothing
                    Set oCatN5 = Nothing
                    Exit Function
                End If
            Next
        End If
    Next

Case 5
    Cod1 = Cod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(Cod1))
    Cod2 = Cod1 & Cod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(Cod2))
    Cod3 = Cod2 & Cod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(Cod3))
    Cod4 = Cod3 & Cod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(Cod4))
    For Each oCatN5 In oCatsN1.Item(Cod1).CategoriasN2.Item(Cod2).CategoriasN3.Item(Cod3).CategoriasN4.Item(Cod4).CategoriasN5
        If oCatN5.Cod <> Cod5 Then
            If oCatN5.Seguridad <> 0 Then
                ComprobarSeguridadDeHermanos = True
                Set oCatN5 = Nothing
                Exit Function
            End If
        End If
    Next

End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "ComprobarSeguridadDeHermanos", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Function
   End If
End Function


Private Sub tvwEstrCat_NodeClick(ByVal node As MSComctlLib.node)
    Dim nodx As MSComctlLib.node
    Dim oCat As Object
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picDatos.Visible = False
    lblDato1.caption = ""
    lblDato2.caption = ""
    'Muestra los campos personalizados en el caso de que tenga:
    If node.Tag <> "Raiz" Then
        Set nodx = tvwEstrCat.selectedItem
        If nodx Is Nothing Then Exit Sub
        
        'Campos personalizados
        If nodx.Children = 0 Then
            Set oCat = DevolverCategoria
            If oCat Is Nothing Then Exit Sub
            If oCat.CamposPersonalizados Is Nothing Then
                Set oCat.CamposPersonalizados = oFSGSRaiz.Generar_CCampos
                oCat.CamposPersonalizados.CargarDatos oCat.Id, iNivel
            End If
            
            If Not oCat.CamposPersonalizados Is Nothing Then
                If Not oCat.CamposPersonalizados.Item(basParametros.gParametrosGenerales.gIdioma & "1") Is Nothing Then
                    lblDato1.caption = oCat.CamposPersonalizados.Item(basParametros.gParametrosGenerales.gIdioma & "1").Den
                    lblDato1.ToolTipText = oCat.CamposPersonalizados.Item(basParametros.gParametrosGenerales.gIdioma & "1").Den
                End If
                If Not oCat.CamposPersonalizados.Item(basParametros.gParametrosGenerales.gIdioma & "2") Is Nothing Then
                    lblDato2.caption = oCat.CamposPersonalizados.Item(basParametros.gParametrosGenerales.gIdioma & "2").Den
                    lblDato2.ToolTipText = oCat.CamposPersonalizados.Item(basParametros.gParametrosGenerales.gIdioma & "2").Den
                End If
                If sOrigen <> "frmADJ" And sOrigen <> "frmRESREU" Then
                    If lblDato1.caption <> "" Or lblDato2.caption <> "" Then
                        picDatos.Visible = True
                    End If
                End If
            End If
            
            Set oCat = Nothing
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "tvwEstrCat_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Function DevolverCategoria() As Object
Dim oCat As Object
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim nodx As MSComctlLib.node
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set nodx = tvwEstrCat.selectedItem
        If nodx Is Nothing Then
            Set DevolverCategoria = Nothing
            Exit Function
        End If
        
        Select Case Left(nodx.Tag, 4)
            Case "CAT1"
                    iNivel = 1
                    scod1 = DevolverCod(nodx)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    Set oCat = oCatsN1.Item(scod1)
            Case "CAT2"
                    iNivel = 2
                    scod1 = DevolverCod(nodx.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nodx)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    Set oCat = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
            Case "CAT3"
                    iNivel = 3
                    scod1 = DevolverCod(nodx.Parent.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nodx.Parent)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    scod3 = DevolverCod(nodx)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    Set oCat = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
            Case "CAT4"
                    iNivel = 4
                    scod1 = DevolverCod(nodx.Parent.Parent.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nodx.Parent.Parent)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    scod3 = DevolverCod(nodx.Parent)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    scod4 = DevolverCod(nodx)
                    scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                    Set oCat = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
            Case "CAT5"
                    iNivel = 5
                    scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nodx.Parent.Parent.Parent)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    scod3 = DevolverCod(nodx.Parent.Parent)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    scod4 = DevolverCod(nodx.Parent)
                    scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                    scod5 = DevolverCod(nodx)
                    scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
                    Set oCat = oCatsN1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5)
        End Select

        Set DevolverCategoria = oCat
        Set oCat = Nothing
        Set nodx = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "DevolverCategoria", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Sub FinalizarSesionMail()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oIdsMail.FuerzaFinalizeSmtpClient
    Set oIdsMail = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCat", "FinalizarSesionMail", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

