Attribute VB_Name = "basErrores"
Option Explicit
'********************* TIPOS DE ERRORES EN SUMMIT *********
' En la clase CTiposDeErrores del servidor

'******************** ESTRUCUTURA DE RETORNO DE ERROR *******
' En la clase CTiposDeErrores del servidor


' ******************** METODO DE TRATAMIENTO DE ERRORES ********
''' <summary>
''' Funcion que trata errores de BD para mostrar el mensaje adecuado en cada momento
''' </summary>
''' <param name="TESErr">Error summit que especifica el numero para mostrar el mensaje adecuado </param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:(hay cientos de llamadas); Tiempo m�ximo:0 </remarks>
''' <remarks>Revisado por: auv. 09/11/2011</remarks>
Public Sub TratarError(TESErr As TipoErrorSummit)
    FSGSLibrary.TratarError TESErr, oMensajes, gParametrosGenerales
End Sub

Public Sub TratarErrorBrowse(ByVal Num As Long, ByVal Descr As String)

    Select Case Num
    Case 76
        oMensajes.MensajeOKOnly 576
    Case 419
        oMensajes.PermisoDenegadoRuta
    Case 68
         oMensajes.MensajeOKOnly 578
    Case Else
        oMensajes.OtroError CStr(Num), Descr
    End Select

End Sub

''' <summary>
''' Grabar un error en una comunicaci�n
''' </summary>
''' <param name="Pagina">Funci�n o pantalla q da el error</param>
''' <param name="NumError">Numero de Error VB. El Id de la tabla va en teserror.arg1</param>
''' <param name="Message">Mensaje del error VB</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmAdjAnya/cmdAceptar_Click frmObjAnya/cmdAceptar_Click frmOfePetAnya/cmdAceptar_Click
'''     basUtilidades/EnviarMensaje     basUtilidades/GrabarEnviarMensaje ; Tiempo m�ximo: 0,2</remarks>
Public Function GrabarError(ByVal Pagina As String, ByVal NumError As String, ByVal Message As String) As TipoErrorSummit
    GrabarError = oFSGSRaiz.GrabarError(Pagina, NumError, Message)
End Function
