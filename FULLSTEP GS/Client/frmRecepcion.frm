VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmRecepcion 
   Caption         =   "dRecepci�n de orden de entrega"
   ClientHeight    =   8115
   ClientLeft      =   2235
   ClientTop       =   2550
   ClientWidth     =   12615
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRecepcion.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8115
   ScaleWidth      =   12615
   Begin VB.Frame FraLineas 
      Height          =   6915
      Left            =   0
      TabIndex        =   15
      Top             =   720
      Width           =   11805
      Begin SSDataWidgets_B.SSDBDropDown sdbddAlmacenes 
         Height          =   1065
         Left            =   390
         TabIndex        =   27
         Top             =   450
         Width           =   3735
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1826
         Columns(0).Caption=   "DC�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4
         Columns(1).Width=   4286
         Columns(1).Caption=   "DDenominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   100
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   1879
         _StockProps     =   77
      End
      Begin VB.PictureBox PicDatos 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   2220
         Left            =   120
         ScaleHeight     =   2220
         ScaleWidth      =   11640
         TabIndex        =   20
         Top             =   4590
         Width           =   11640
         Begin VB.Frame fraBloqueoFactura 
            Height          =   675
            Left            =   8460
            TabIndex        =   30
            Top             =   0
            Width           =   3135
            Begin VB.Label lblBloqueoFactura 
               BackStyle       =   0  'Transparent
               Caption         =   "� albaran bloqueado para facturacion !"
               ForeColor       =   &H000000FF&
               Height          =   225
               Left            =   150
               TabIndex        =   31
               Top             =   270
               Width           =   2865
            End
         End
         Begin VB.OptionButton optCompleto 
            Caption         =   "Completamente recibido y cerrado"
            Height          =   225
            Left            =   5400
            TabIndex        =   26
            Top             =   540
            Width           =   3045
         End
         Begin VB.CheckBox chkEnviar 
            Caption         =   "DComunicar a proveedor"
            Height          =   225
            Left            =   1395
            TabIndex        =   7
            Top             =   510
            Width           =   3495
         End
         Begin VB.TextBox txtAlbaran 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   1395
            MaxLength       =   100
            TabIndex        =   4
            Top             =   120
            Width           =   3450
         End
         Begin VB.OptionButton optRecKo 
            Caption         =   "REcepci�n incorrecta"
            Height          =   225
            Left            =   5400
            TabIndex        =   6
            Top             =   277
            Width           =   3045
         End
         Begin VB.OptionButton optRecOk 
            Caption         =   "REcepci�n correcta"
            Height          =   225
            Left            =   5400
            TabIndex        =   5
            Top             =   0
            Value           =   -1  'True
            Width           =   3045
         End
         Begin VB.TextBox txtComent 
            Height          =   1335
            Left            =   1395
            MaxLength       =   255
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   8
            Text            =   "frmRecepcion.frx":014A
            Top             =   825
            Width           =   10020
         End
         Begin VB.Label LblAlbaran 
            BackStyle       =   0  'Transparent
            Caption         =   "DAlbar�n :"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   120
            TabIndex        =   22
            Top             =   150
            Width           =   1100
         End
         Begin VB.Label LblComent 
            BackStyle       =   0  'Transparent
            Caption         =   "Observaciones :"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   120
            TabIndex        =   21
            Top             =   510
            Width           =   1185
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgPrecios 
         Height          =   4455
         Left            =   0
         TabIndex        =   19
         Top             =   120
         Width           =   11760
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         Col.Count       =   21
         stylesets.count =   4
         stylesets(0).Name=   "Headers"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmRecepcion.frx":0150
         stylesets(1).Name=   "gris"
         stylesets(1).ForeColor=   -2147483633
         stylesets(1).BackColor=   -2147483633
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmRecepcion.frx":016C
         stylesets(2).Name=   "Normal"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmRecepcion.frx":0188
         stylesets(3).Name=   "ITEMCERRADO"
         stylesets(3).ForeColor=   16777215
         stylesets(3).BackColor=   9408399
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmRecepcion.frx":01A4
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         BalloonHelp     =   0   'False
         MaxSelectedRows =   1
         HeadStyleSet    =   "Headers"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ActiveRowStyleSet=   "Normal"
         SplitterVisible =   -1  'True
         Columns.Count   =   21
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).CaptionAlignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1138
         Columns(1).Name =   "NUM"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777152
         Columns(2).Width=   5239
         Columns(2).Caption=   "DArt�culo"
         Columns(2).Name =   "DEN"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   50
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16777152
         Columns(2).StyleSet=   "Normal"
         Columns(3).Width=   1561
         Columns(3).Caption=   "DDest"
         Columns(3).Name =   "DEST"
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16777152
         Columns(3).StyleSet=   "Normal"
         Columns(4).Width=   2170
         Columns(4).Caption=   "dF.Entrega"
         Columns(4).Name =   "INI"
         Columns(4).Alignment=   2
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   16777152
         Columns(4).StyleSet=   "Normal"
         Columns(5).Width=   1561
         Columns(5).Caption=   "dUni"
         Columns(5).Name =   "UNI"
         Columns(5).CaptionAlignment=   2
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   16777152
         Columns(5).StyleSet=   "Normal"
         Columns(6).Width=   2170
         Columns(6).Caption=   "dPrecio"
         Columns(6).Name =   "PRECIO"
         Columns(6).Alignment=   1
         Columns(6).CaptionAlignment=   2
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).NumberFormat=   "Standard"
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   16777152
         Columns(6).StyleSet=   "Normal"
         Columns(7).Width=   2170
         Columns(7).Caption=   "dC.Pedida"
         Columns(7).Name =   "CANT_PED"
         Columns(7).Alignment=   1
         Columns(7).CaptionAlignment=   2
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).NumberFormat=   "#,##0.00####################"
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   16777152
         Columns(7).StyleSet=   "Normal"
         Columns(8).Width=   2461
         Columns(8).Caption=   "dImp.Pedido"
         Columns(8).Name =   "IMPORTE_PED"
         Columns(8).Alignment=   1
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   16777152
         Columns(9).Width=   1561
         Columns(9).Caption=   "Desv.R"
         Columns(9).Name =   "DESVIO"
         Columns(9).Alignment=   1
         Columns(9).CaptionAlignment=   0
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(9).HasBackColor=   -1  'True
         Columns(9).BackColor=   16777152
         Columns(10).Width=   2725
         Columns(10).Caption=   "dCantidadRecibida"
         Columns(10).Name=   "CANT_RECIBIDA"
         Columns(10).Alignment=   1
         Columns(10).CaptionAlignment=   2
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).NumberFormat=   "#,##0.00####################"
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(10).HasBackColor=   -1  'True
         Columns(10).BackColor=   11007475
         Columns(10).StyleSet=   "Normal"
         Columns(11).Width=   2646
         Columns(11).Caption=   "dImporteRecibido"
         Columns(11).Name=   "IMPORTE_RECIBIDO"
         Columns(11).Alignment=   1
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Locked=   -1  'True
         Columns(11).HasBackColor=   -1  'True
         Columns(11).BackColor=   11007475
         Columns(12).Width=   2170
         Columns(12).Caption=   "dC.En esta R."
         Columns(12).Name=   "CANT_RECEPCIONADA"
         Columns(12).Alignment=   1
         Columns(12).CaptionAlignment=   2
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).NumberFormat=   "#,##0.00####################"
         Columns(12).FieldLen=   256
         Columns(12).HasBackColor=   -1  'True
         Columns(12).BackColor=   16777215
         Columns(13).Width=   2752
         Columns(13).Caption=   "dImp.en esta Recep"
         Columns(13).Name=   "IMPORTE_RECEPCIONADO"
         Columns(13).Alignment=   1
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).HasBackColor=   -1  'True
         Columns(13).BackColor=   16777215
         Columns(14).Width=   2752
         Columns(14).Caption=   "ALMACEN"
         Columns(14).Name=   "ALMACEN"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "ALM_ID"
         Columns(15).Name=   "ALM_ID"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   2752
         Columns(16).Caption=   "ACTIVO"
         Columns(16).Name=   "ACTIVO"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         Columns(16).Style=   1
         Columns(17).Width=   3200
         Columns(17).Visible=   0   'False
         Columns(17).Caption=   "CENTRO"
         Columns(17).Name=   "CENTRO"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(18).Width=   3200
         Columns(18).Visible=   0   'False
         Columns(18).Caption=   "IM_RECEPAUTO"
         Columns(18).Name=   "IM_RECEPAUTO"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   8
         Columns(18).FieldLen=   256
         Columns(19).Width=   3200
         Columns(19).Visible=   0   'False
         Columns(19).Caption=   "TIPO_RECEPCION"
         Columns(19).Name=   "TIPORECEPCION"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(20).Width=   3200
         Columns(20).Caption=   "DSalida autom. almac�n"
         Columns(20).Name=   "SALIDA_ALMACEN"
         Columns(20).DataField=   "Column 20"
         Columns(20).DataType=   11
         Columns(20).FieldLen=   256
         Columns(20).Style=   2
         _ExtentX        =   20743
         _ExtentY        =   7858
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frafec 
      Height          =   735
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   11775
      Begin VB.Frame fraFechaContable 
         BorderStyle     =   0  'None
         Height          =   300
         Left            =   4320
         TabIndex        =   37
         Top             =   275
         Width           =   2895
         Begin VB.TextBox txtFechaContable 
            Height          =   285
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   39
            Top             =   0
            Width           =   1000
         End
         Begin VB.CommandButton cmdCalendarCon 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2450
            Picture         =   "frmRecepcion.frx":01C0
            Style           =   1  'Graphical
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   0
            Width           =   315
         End
         Begin VB.Label lblFecCon 
            Caption         =   "dFecha contable:"
            Height          =   255
            Left            =   0
            TabIndex        =   40
            Top             =   10
            Width           =   1500
         End
      End
      Begin VB.Frame fraNumERP 
         BorderStyle     =   0  'None
         Height          =   525
         Left            =   4440
         TabIndex        =   32
         Top             =   120
         Width           =   3615
         Begin VB.TextBox TxtNumeroSap 
            Height          =   285
            Left            =   1770
            TabIndex        =   33
            Top             =   150
            Width           =   1755
         End
         Begin VB.Label lblNumeroSap 
            Caption         =   "N�mero ERP:"
            Height          =   345
            Left            =   0
            TabIndex        =   34
            Top             =   180
            Width           =   1785
         End
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcFecRec 
         Height          =   285
         Left            =   1920
         TabIndex        =   1
         Top             =   240
         Width           =   1935
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3413
         Columns(0).Caption=   "Fecha"
         Columns(0).Name =   "FECHA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3201
         Columns(1).Caption=   "N�mero ERP"
         Columns(1).Name =   "NUMERP"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   3
         Columns(2).FieldLen=   256
         _ExtentX        =   3413
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Frame FraInt 
         Height          =   735
         Left            =   11010
         TabIndex        =   23
         Top             =   0
         Width           =   735
         Begin VB.PictureBox PicIntegracion 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   60
            Picture         =   "frmRecepcion.frx":04D2
            ScaleHeight     =   255
            ScaleWidth      =   330
            TabIndex        =   25
            Top             =   240
            Width           =   325
         End
         Begin VB.TextBox txtEstIntNormal 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   390
            Locked          =   -1  'True
            MaxLength       =   20
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   255
            Width           =   235
         End
      End
      Begin VB.CheckBox chkTotal 
         Caption         =   "100%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   10890
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   700
      End
      Begin VB.CommandButton cmdCalendarRec 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3900
         Picture         =   "frmRecepcion.frx":0AEC
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecRec 
         Height          =   285
         Left            =   1920
         TabIndex        =   2
         Top             =   255
         Width           =   1980
      End
      Begin VB.Label lblImporteTotalAlbaranDato 
         Alignment       =   1  'Right Justify
         Caption         =   "0,0"
         Height          =   255
         Left            =   9840
         TabIndex        =   36
         Top             =   240
         Width           =   1650
      End
      Begin VB.Label lblImporteTotalAlbaran 
         Caption         =   "DImporte Total albaran:"
         Height          =   255
         Left            =   8160
         TabIndex        =   35
         Top             =   270
         Width           =   1770
      End
      Begin VB.Label lblReceptor 
         Caption         =   "DRecepci�n realizada por"
         Height          =   255
         Left            =   10770
         TabIndex        =   28
         Top             =   270
         Width           =   3690
      End
      Begin VB.Label lblFecRec 
         Caption         =   "dFecha recepci�n:"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   270
         Width           =   1710
      End
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   30
      ScaleHeight     =   510
      ScaleWidth      =   9255
      TabIndex        =   0
      Top             =   7680
      Width           =   9255
      Begin VB.CommandButton cmdBloquear 
         Caption         =   "DesBloquear facturaci�n"
         Height          =   345
         Left            =   3480
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   90
         Width           =   1935
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         Enabled         =   0   'False
         Height          =   345
         Left            =   0
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Enabled         =   0   'False
         Height          =   345
         Left            =   1155
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2310
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
   End
   Begin VB.PictureBox picEdit 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   0
      ScaleHeight     =   480
      ScaleWidth      =   13155
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   7680
      Visible         =   0   'False
      Width           =   13155
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   345
         Left            =   5925
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   345
         Left            =   4800
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   75
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmRecepcion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables para func. combos
Public bRespetarCombo As Boolean
Public bCargarComboDesde As Boolean

'Variable de control de flujo de proceso
Public Accion As accionessummit

'Variables de seguridad
Public bMod As Boolean
Public bRMat As Boolean

Private oLinea As CLineaRecepcion

' Interface para obener las ofertas de un proveedor
Public oOrdenEntrega As COrdenEntrega
Private oRecepSeleccionada As CRecepcion
Private oRecepciones As cRecepciones
Private oIBaseDatos As IBaseDatos
Public g_oActivo As CActivo

Private m_bValidaAlm As Boolean
'Contiene el c�digo de la persona conectada, si administrador = ""
Private sPersona As String
''' INTEGRACION, c�digo del usuario FSGS
Private sUsuario As String
'Multilenguaje
Private sIdiFecRec  As String
Private sIdiFecContable As String
Private sIdiCantRec  As String
Private sIdiAlbaran  As String
Private m_sIdiSincronizado As String
Private m_sIdiNoSincronizado As String
Private m_sIdiCentroC As String
Private m_sCerrado As String
Private m_sBloquea As String

Private m_bCabeceraBloqueo As Boolean

Public bActualizaSeguimiento As Boolean
Public bCargarFechaMasReciente As Boolean
Private m_lIdFechaMax As Long

Private m_sIdiReceptor As String
Private m_sIdiAdm As String

Private bAlgunaModificacion As Boolean

Private m_bError As Boolean

Private m_sTxtBloquear As String
Private m_sTxtDesBloquear As String
Private m_bModoConsulta As Boolean
Private bAlta As Boolean

''' <summary>
''' Muestra los datos de la recepci�n seleccionada
''' </summary>
''' <param name="Id">OPCIONAL. Id de la ultima recepcion</param>
''' <remarks>Llamada desde:frmRececpcion.sdbcFecRec_CloseUp, frmRecepcion.sdbcFecRec_Validate </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub RecepSeleccionada(Optional ByVal Id As Long)
    Dim sReceptor As String
    Dim bAlgunoNoRecepAutom As Boolean
        
    If bCargarFechaMasReciente Then
        Set oRecepSeleccionada = oRecepciones.Item(CStr(Id))
    Else
        Set oRecepSeleccionada = oRecepciones.Item(CStr(sdbcFecRec.Columns("ID").Value))
    End If
    
    If oRecepSeleccionada Is Nothing Then
        sdbcFecRec = ""
        Exit Sub
    End If
    
    If (oRecepSeleccionada.BloqueoFactura) Then
        cmdBloquear.caption = m_sTxtDesBloquear
        fraBloqueoFactura.Visible = (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM)
    Else
        cmdBloquear.caption = m_sTxtBloquear
        fraBloqueoFactura.Visible = False
    End If
    
    ''' INTEGRACION
    If oRecepSeleccionada.TipoOrden = Aprovisionamiento Then
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Aprov) Then
            If oRecepSeleccionada.DevolverEstadoIntegracion(EntidadIntegracion.Rec_Aprov) Then
                txtEstIntNormal.Backcolor = RGB(212, 208, 200)
                txtEstIntNormal.ToolTipText = m_sIdiSincronizado
            Else
                txtEstIntNormal.Backcolor = RGB(255, 0, 0)
                txtEstIntNormal.ToolTipText = m_sIdiNoSincronizado
            End If
            FraInt.Visible = True
        Else
            FraInt.Visible = False
        End If
    ElseIf oRecepSeleccionada.TipoOrden = Directo Then
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Directo) Then
            If oRecepSeleccionada.DevolverEstadoIntegracion(EntidadIntegracion.Rec_Directo) Then
                txtEstIntNormal.Backcolor = RGB(212, 208, 200)
                txtEstIntNormal.ToolTipText = m_sIdiSincronizado
            Else
                txtEstIntNormal.Backcolor = RGB(255, 0, 0)
                txtEstIntNormal.ToolTipText = m_sIdiNoSincronizado
            End If
            FraInt.Visible = True
        Else
            FraInt.Visible = False
        End If
    ElseIf oRecepSeleccionada.TipoOrden = PedidosPM Then
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Aprov) _
        Or basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Directo) Then
            If oRecepSeleccionada.DevolverEstadoIntegracion(EntidadIntegracion.Rec_Aprov) _
            Or oRecepSeleccionada.DevolverEstadoIntegracion(EntidadIntegracion.Rec_Directo) Then
                txtEstIntNormal.Backcolor = RGB(212, 208, 200)
                txtEstIntNormal.ToolTipText = m_sIdiSincronizado
            Else
                txtEstIntNormal.Backcolor = RGB(255, 0, 0)
                txtEstIntNormal.ToolTipText = m_sIdiNoSincronizado
            End If
            FraInt.Visible = True
        Else
            FraInt.Visible = False
        End If
    End If
    
    txtalbaran = oRecepSeleccionada.albaran
    txtComent = NullToStr(oRecepSeleccionada.coment)
    TxtNumeroSap.Text = NullToStr(oRecepSeleccionada.NumErp)
    TxtNumeroSap.Locked = True
    txtFechaContable.Text = NullToStr(oRecepSeleccionada.FechaContable)
    If oRecepSeleccionada.OrdenEstado = TipoEstadoOrdenEntrega.RecibidoYCerrado And oRecepSeleccionada.Completo Then
        optCompleto.Value = True
    Else
        If oRecepSeleccionada.Correcto Then
            optRecOk.Value = True
        Else
            optRecKo.Value = True
        End If
    End If
    
    sdbgPrecios.RemoveAll
    
    If bMod Then
        If oRecepSeleccionada.OrdenEstado = RecibidoYCerrado Or oRecepSeleccionada.OrdenEstado = Anulado Then
            cmdA�adir.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
        Else
            cmdA�adir.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
        End If
    End If
            
    bAlgunoNoRecepAutom = oRecepSeleccionada.CargarLineasRecepcion(bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, oOrdenEntrega.Empresa)
        
    If bAlgunoNoRecepAutom = False Then
        Me.cmdA�adir.Enabled = False
        Me.cmdModificar.Enabled = False
    End If
    
    CargarGridConRecep
    
    If oRecepSeleccionada.Receptor = "" Then
        sReceptor = m_sIdiAdm
    Else
        sReceptor = oRecepSeleccionada.Receptor
    End If
    Me.lblReceptor.caption = m_sIdiReceptor & ":  " & sReceptor
        
End Sub

''' <summary>
''' Pone los controles de pantalla en modo no editable
''' </summary>
''' <remarks>Llamada desde: Form_Load    cmdAceptar_Click       cmdCancelar_Click       cmdEliminar_Click; Tiempo m�ximo: 0</remarks>
Private Sub Modo_Consulta()

    cmdCalendarRec.Visible = False
    txtFecRec.Text = ""
    txtFecRec.Visible = False
    sdbcFecRec.Visible = True
    
    cmdCalendarCon.Visible = False
    txtFechaContable.Locked = True
    
    picDatos.Enabled = False
    txtalbaran.Enabled = True
    txtComent.Enabled = True
    lblReceptor.Visible = True
    sdbgPrecios.AllowUpdate = False
    picEdit.Visible = False
    picNavigate.Visible = True
    chkTotal.Visible = False
    TxtNumeroSap.Locked = True
    
    
    Accion = ACCRecepcionPedConsulta
    m_bModoConsulta = True
End Sub

''' <summary>
''' Pone los controles de pantalla en modo editable
''' </summary>
''' <remarks>Llamada desde: cmdModificar_Click; Tiempo m�ximo: 0</remarks>
Private Sub Modo_Edicion()

    txtFecRec.Text = sdbcFecRec.Value
    txtFecRec.Visible = True
    sdbcFecRec.Visible = False
    
    txtFechaContable.Locked = False
    cmdCalendarCon.Visible = True
    
    lblReceptor.Visible = True
    cmdCalendarRec.Visible = True
    picDatos.Enabled = True
    picEdit.Visible = True
    picNavigate.Visible = False
    chkTotal.Visible = True
    sdbgPrecios.AllowUpdate = True
    Accion = ACCRecepcionPedMOD
    'INTEGRACION
    FraInt.Visible = False
    TxtNumeroSap.Locked = True
    
    m_bModoConsulta = False
End Sub
''' <summary>
''' Coloca los controles en modo edici�n de una recepcion nueva y carga las lineas de pedido
''' </summary>
''' <param name="estadoOrden">Estado del pedido</param>
''' <remarks>Llamada desde:frmRececpcion.cmdA�adir_Click, frmRecepcion.Form_load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Function Modo_Recepcion(ByVal estadoOrden As Integer) As Boolean
Dim oNoVigentes As CImputaciones
Dim oImp As Cimputacion
Dim sMensaje As String
Dim iBloquear As Integer
Dim irespuesta As Integer
Dim sReceptor As String
    
    Modo_Recepcion = True
    Set oRecepSeleccionada = oFSGSRaiz.Generar_crecepcion
    
    oRecepSeleccionada.Orden = oOrdenEntrega.Id
    oRecepSeleccionada.pedido = oOrdenEntrega.idPedido
    oRecepSeleccionada.TipoOrden = oOrdenEntrega.Tipo
    oRecepSeleccionada.OrdenEstado = estadoOrden
    oRecepSeleccionada.OrdenIncorrecta = oOrdenEntrega.Incorrecta
    oRecepSeleccionada.CodProve = oOrdenEntrega.ProveCod
    oRecepSeleccionada.FechaRec = Now
    If gParametrosGenerales.gbMostrarFechaContable Then oRecepSeleccionada.FechaContable = Date
    
    oRecepSeleccionada.CargarLineasPedido bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, oOrdenEntrega.Empresa
    
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        If g_oParametrosSM.Count > 0 Then
            'Valido la vigencia antes de a�adir
            Set oNoVigentes = oRecepSeleccionada.ValidarVigenciaPartidas(, True)
            If Not oNoVigentes Is Nothing Then
                sMensaje = ""
                For Each oImp In oNoVigentes
                    If iBloquear <> 2 Then  'Bloquear tendr� 2 si bloquear
                        iBloquear = g_oParametrosSM.Item(oImp.PRES5).Vigencia
                    End If
                    sMensaje = sMensaje & g_oParametrosSM.Item(oImp.PRES5).NomNivelImputacion & ": " & oImp.PresDen & " (" & oImp.FecInicio & " - " & oImp.FecFin & ") " & IIf(oImp.Cerrado, m_sCerrado, "") & IIf(g_oParametrosSM.Item(oImp.PRES5).Vigencia = 2, "�" & m_sBloquea & "!", "") & vbCrLf
                Next
                irespuesta = oMensajes.ImputacionesSMNoVigentes(IIf(m_bCabeceraBloqueo, 1, 4), sMensaje, IIf(iBloquear = 2 And m_bCabeceraBloqueo, False, True))
                If irespuesta = vbNo Then
                    Modo_Recepcion = False
                    Exit Function
                End If
            End If
            Set oNoVigentes = Nothing
        End If
    End If

    txtFecRec.Visible = True
    sdbcFecRec.Visible = False
    lblReceptor.Visible = True
    cmdCalendarRec.Visible = True
    
    txtFechaContable.Locked = False
    cmdCalendarCon.Visible = True
    
    chkTotal.Visible = True
    picEdit.Visible = True
    picNavigate.Visible = False
    
    txtalbaran.Text = ""
    txtComent.Text = ""
    chkEnviar.Value = vbUnchecked
    txtFecRec.Text = oRecepSeleccionada.FechaRec
    txtFechaContable.Text = NullToStr(oRecepSeleccionada.FechaContable)
    
    txtFecRec.Enabled = True
    txtalbaran.Enabled = True
    txtComent.Enabled = True
    picDatos.Enabled = True
    optRecOk.Value = True
    sdbgPrecios.RemoveAll
    
    If oRecepSeleccionada.Receptor = "" Then
        sReceptor = m_sIdiAdm
    Else
        sReceptor = oRecepSeleccionada.Receptor
    End If
    Me.lblReceptor.caption = m_sIdiReceptor & ":  " & sReceptor
    
    CargarGridConRecep
    
    sdbgPrecios.AllowUpdate = True
    
    Accion = ACCRecepcionPedAnya
    ''' INTEGRACION
    FraInt.Visible = False
        
End Function

''' <summary>
''' Carga el grid sdbgPrecios con las lineas de la recepci�n
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: RecepSeleccionada, Modo_Recepcion; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisado por ngo</remarks>
Private Sub CargarGridConRecep()
    Dim oLinea As CLineaRecepcion
    Dim oParamSM As CParametroSM
    Dim oImputacion As Cimputacion
    Dim sLinea As String
    Dim sCodCC As String
    Dim sCodPres As String
    Dim dImporteRecep As Double
    
    dImporteRecep = 0
    '''limpiamos grid
    'oRecepSeleccionada.CargarLineasRecepcion bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, oOrdenEntrega.Empresa
    '''Calculamos el total recepcionado
    For Each oLinea In oRecepSeleccionada.Lineas
        sLinea = oLinea.IdLineaPed
        Dim importePedido As Double
        Dim cantRecibida As Double
        Dim importerecibido As Double
        Dim cantRecepcionada As Double
        Dim importerecepcionado As Double
                        
        importePedido = oLinea.importePedido
        If Not bAlta Then
            'es una recepci�n a modificar entonces tenemos que restarle a la cantidad recibida hasta el momento la de la nueva recepci�n ya que si no la computamos dos veces
            'lo mismo para el importe
            cantRecibida = oLinea.cantRecibida - oLinea.cantRecepcionada
            importerecibido = oLinea.importerecibido - oLinea.importerecepcionado
            cantRecepcionada = oLinea.cantRecepcionada
            importerecepcionado = oLinea.importerecepcionado
        Else
            'Si se trata de una recepcion nueva
            cantRecibida = oLinea.cantRecibida
            importerecibido = oLinea.importerecibido
            cantRecepcionada = oLinea.CantidadPendienteRecepcionar
            importerecepcionado = oLinea.ImportePendienteRecepcionar
        End If
        
        If oLinea.Num < 10 Then
            sLinea = sLinea & Chr(m_lSeparador) & "00" & CStr(oLinea.Num)
        ElseIf oLinea.Num < 100 Then
            sLinea = sLinea & Chr(m_lSeparador) & "0" & CStr(oLinea.Num)
        Else
            sLinea = sLinea & Chr(m_lSeparador) & CStr(oLinea.Num)
        End If
            
        If IsNull(oLinea.ArticuloCod) Then
            sLinea = sLinea & Chr(m_lSeparador) & oLinea.Descr
        Else
            sLinea = sLinea & Chr(m_lSeparador) & (oLinea.ArticuloCod) & " - " & oLinea.Descr
        End If
        sLinea = sLinea & Chr(m_lSeparador) & oLinea.DestCod & Chr(m_lSeparador) & NullToStr(oLinea.FechaEntrega) & Chr(m_lSeparador) & oLinea.UniCod & Chr(m_lSeparador) _
                & CDec(StrToDbl0(oLinea.Precio) * oOrdenEntrega.Cambio) & Chr(m_lSeparador) & oLinea.CantPedida & Chr(m_lSeparador) _
                & FormateoNumerico(oLinea.importePedido * oOrdenEntrega.Cambio, "#,##0.00") & Chr(m_lSeparador) & _
                NullToDbl0(oLinea.Desvio) & "%" & Chr(m_lSeparador) & NullToStr(cantRecibida) & Chr(m_lSeparador) & _
                FormateoNumerico(importerecibido * oOrdenEntrega.Cambio, "#,##0.00") & Chr(m_lSeparador) & FormateoNumerico(cantRecepcionada, "#,##0.00") & _
                Chr(m_lSeparador) & FormateoNumerico(importerecepcionado * oOrdenEntrega.Cambio, "#,##0.00")
        If oLinea.Almacen.Id <> 0 Then
            sLinea = sLinea & Chr(m_lSeparador) & oLinea.Almacen.Cod & " - " & oLinea.Almacen.Den & Chr(m_lSeparador) & oLinea.Almacen.Id
        Else
            sLinea = sLinea & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        End If
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            If oLinea.Activo.Id <> 0 Then
                sLinea = sLinea & Chr(m_lSeparador) & oLinea.Activo.Cod & " - " & oLinea.Activo.Den
            Else
                sLinea = sLinea & Chr(m_lSeparador) & ""
            End If
        Else
            sLinea = sLinea & Chr(m_lSeparador) & ""
        End If
        
        If IsNull(oLinea.Centro) Then
            sLinea = sLinea & Chr(m_lSeparador) & ""
        Else
            sLinea = sLinea & Chr(m_lSeparador) & NullToStr(oLinea.Centro)
        End If
        sLinea = sLinea & Chr(m_lSeparador) & BooleanToSQLBinary(oLinea.RecepAutom)
        sLinea = sLinea & Chr(m_lSeparador) & oLinea.tipoRecepcion
        sLinea = sLinea & Chr(m_lSeparador) & CStr(oLinea.SalidaAlmacen)
        If Not g_oParametrosSM Is Nothing Then
            For Each oParamSM In g_oParametrosSM
                If oParamSM.ImpPedido <> 0 Then
                    If oLinea.Imputaciones Is Nothing Then
                        sLinea = sLinea & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    Else
                        Set oImputacion = oLinea.Imputaciones.Item(oParamSM.PRES5)
                        If oImputacion Is Nothing Then
                            sLinea = sLinea & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                        Else
                            sCodCC = DevolverCodCC(oImputacion.CC1, oImputacion.CC2, oImputacion.CC3, oImputacion.CC4)
                            sCodPres = DevolverCodCC(oImputacion.Pres1, oImputacion.Pres2, oImputacion.Pres3, oImputacion.Pres4)
                            sLinea = sLinea & Chr(m_lSeparador) & sCodCC & " - " & oImputacion.CCDen & Chr(m_lSeparador) & sCodPres & " - " & oImputacion.PresDen
                        End If
                    End If
                End If
            Next
        End If
        
        sdbgPrecios.AddItem sLinea
        If oLinea.isRecepcionCantidad Then
            dImporteRecep = dImporteRecep + (CDec(StrToDbl0(oLinea.Precio)) * NullToDbl0(cantRecepcionada))
            oLinea.importerecepcionado = importerecepcionado
            oLinea.cantRecepcionada = cantRecepcionada
        Else
            dImporteRecep = dImporteRecep + oLinea.importerecepcionado
            oLinea.cantRecepcionada = cantRecepcionada
            oLinea.importerecepcionado = importerecepcionado
        End If
        
    Next
    
    
    Me.lblImporteTotalAlbaranDato.caption = FormateoNumerico(dImporteRecep * oOrdenEntrega.Cambio, "#,##0.00") & " " & oOrdenEntrega.Moneda   ' FormateoNumerico(dImporteRecep, "#,##0.00") & " " & oOrdenEntrega.Moneda
    
    mostrarOcultarColumnas
Set oLinea = Nothing
End Sub

Private Sub chkTotal_Click()
Dim i As Integer
Dim iCant As Double
Dim iRdo As Double

If chkTotal.Value = vbChecked Then
    sdbgPrecios.MoveFirst
    For i = 0 To sdbgPrecios.Rows - 1
        If sdbgPrecios.Columns("CANT_RECEPCIONADA").Value = "" Or IsNull(sdbgPrecios.Columns("CANT_RECEPCIONADA").Value) Then
            iCant = 0
        Else
            iCant = sdbgPrecios.Columns("CANT_RECEPCIONADA").Value
        End If
        iRdo = CDbl(sdbgPrecios.Columns("CANT_PED").Value) - iCant
        
        If (CDbl(sdbgPrecios.Columns("CANT_PED").Value) < 0 And iRdo < 0) Or (CDbl(sdbgPrecios.Columns("CANT_PED").Value) > 0 And iRdo > 0) Then
            sdbgPrecios.Columns("CANT_RECIBIDA").Value = iRdo
            oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).cantRecibida = iRdo
        End If
        sdbgPrecios.MoveNext
    Next
Else
    sdbgPrecios.MoveFirst
    For i = 0 To sdbgPrecios.Rows - 1
        sdbgPrecios.Columns("CANT_RECIBIDA").Value = Null
        oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).cantRecibida = iRdo
        sdbgPrecios.MoveNext
    Next
End If
sdbgPrecios.Update

End Sub
''' <summary>Hace las comprobaciones oportunas y graba la recepci�n</summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisada por ngo</remarks>
Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim iNulos As Integer
Dim iTot_recepcionada As Double
Dim iTot_recibida As Double
Dim iTot_pedida As Double
Dim irespuesta As Integer
Dim bCierre As Boolean
Dim blnHayPedidoAbierto As Boolean
Dim bExiste As Boolean
Dim oNoVigentes As CImputaciones
Dim oImp As Cimputacion
Dim sMensaje As String
Dim iBloquear As Integer
Dim bFechaAnt As Boolean
Dim oLin As CLineaRecepcion

Dim bAlgunDesvio As Boolean
Dim bLineasComplConDesvio As Boolean

    If Not IsDate(txtFecRec.Text) Then
        oMensajes.NoValido sIdiFecRec
        If Me.Visible Then txtFecRec.SetFocus
        Exit Sub
    End If
    
    If gParametrosGenerales.gbMostrarFechaContable Then
    
        If Not IsDate(txtFechaContable.Text) Then
            oMensajes.NoValido sIdiFecContable
            If Me.Visible Then txtFechaContable.SetFocus
            Exit Sub
        End If

    End If
    
    If gParametrosGenerales.gbOblPedDirFecRecep Then
        bFechaAnt = False
        If CDate(txtFecRec.Text) <> oRecepSeleccionada.FechaRec And Accion <> ACCRecepcionPedAnya Then
            bFechaAnt = True
        ElseIf Accion = ACCRecepcionPedAnya Then
            bFechaAnt = True
        End If
        If bFechaAnt And CDate(Format(txtFecRec.Text, "Short date")) < Date Then
            oMensajes.MensajeOKOnly 1111, Critical
            If Me.Visible Then txtFecRec.SetFocus
            Exit Sub
        End If
    End If
    
    If sdbgPrecios.DataChanged Then
        m_bError = False
        sdbgPrecios.Update
        If m_bError Then Exit Sub
    End If
     'Si est�n a�adiendo y han modificado la fecha de recepcion valido de nuevo la vigencia
     'Si es modificaci�n y hay imputaci�n por linea valido pq podr�an meter cantidades para lineas vigentes aunque haya otras no vigentes
    If (Format(txtFecRec.Text, "Short date") <> Format(Date, "Short date") And Accion = ACCRecepcionPedAnya) Or Not m_bCabeceraBloqueo Then
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            If g_oParametrosSM.Count > 0 Then
             Set oNoVigentes = oRecepSeleccionada.ValidarVigenciaPartidas(Format(txtFecRec.Text, "Short date"))
             If Not oNoVigentes Is Nothing Then
                 sMensaje = ""
                 For Each oImp In oNoVigentes
                     If iBloquear <> 2 Then
                         iBloquear = g_oParametrosSM.Item(oImp.PRES5).Vigencia
                     End If
                     sMensaje = sMensaje & g_oParametrosSM.Item(oImp.PRES5).NomNivelImputacion & ": " & oImp.PresDen & " (" & oImp.FecInicio & " - " & oImp.FecFin & ") " & IIf(oImp.Cerrado, m_sCerrado, "") & IIf(g_oParametrosSM.Item(oImp.PRES5).Vigencia = 2, "�" & m_sBloquea & "!", "") & vbCrLf
                 Next
                 If iBloquear = 2 Then 'Si no hay bloqueo no muestro el mensaje pq ya se ha mostrado antes
                    irespuesta = oMensajes.ImputacionesSMNoVigentes(IIf(Accion = ACCRecepcionPedAnya, 1, 3), sMensaje, IIf(iBloquear = 2, False, True))
                    If irespuesta = vbNo Then
                        Exit Sub
                    End If
                End If
             End If
             Set oNoVigentes = Nothing
            End If
         End If
    End If
    
    blnHayPedidoAbierto = False
    
    
    If txtalbaran = "" Then
        oMensajes.FaltanDatos Left(sIdiAlbaran, Len(sIdiAlbaran) - 1)
        If Me.Visible Then txtalbaran.SetFocus
        Exit Sub
    End If
    iNulos = 0
    iTot_pedida = 0
    iTot_recibida = 0
    iTot_recepcionada = 0
    
    'Miro a ver si hay desvios, de haberlos hay q mirar linea a linea, no valen sumatorios para saber si se completa o no
    bAlgunDesvio = False
    bLineasComplConDesvio = True
    
    ' hay que recorrer la colecci�n en vez de la grid, por si hay restricciones
    For Each oLinea In oRecepSeleccionada.Lineas
        
        If IsNull(oLinea.cantRecibida) And IsNull(oLinea.importerecibido) Then
            iNulos = iNulos + 1
            iTot_recepcionada = iTot_recepcionada + oLinea.cantRecepcionada
        Else
            'Si el parametro de control de la cantidad recepcionada est� activado no se le permitir� meter 0.
            If gParametrosGenerales.gbOblPedDirCantRecep Then
                If oLinea.cantRecibida = 0 Then
                    oMensajes.MensajeOKOnly 1110, Exclamation
                    PosicionarEnGrid IIf(IsNull(oLinea.ArticuloCod), oLinea.Descr, (oLinea.ArticuloCod & " - " & oLinea.Descr)), 9 'Col 9 Almacen                    Exit Sub
                    Exit Sub
                End If
            End If
            If oLinea.isRecepcionCantidad Then
                iTot_recibida = iTot_recibida + oLinea.cantRecepcionada
                iTot_recepcionada = iTot_recepcionada + oLinea.cantRecepcionada
            End If
        End If
        
        If NullToDbl0(oLinea.Desvio) > 0 Then
            bAlgunDesvio = True
        End If
        If oLinea.isRecepcionCantidad Then
            If ((oLinea.CantPedida < 0) And (oLinea.CantPedida < (NullToDbl0(oLinea.cantRecibida) + oLinea.cantRecepcionada))) Then
                bLineasComplConDesvio = False
            ElseIf ((oLinea.CantPedida > 0) And (oLinea.CantPedida > (NullToDbl0(oLinea.cantRecibida) + oLinea.cantRecepcionada))) Then
                bLineasComplConDesvio = False
            End If
        Else
            If (oLinea.importePedido < 0) And (oLinea.importePedido < NullToDbl0(oLinea.importerecibido) + oLinea.importerecepcionado) Then
                bLineasComplConDesvio = False
            ElseIf (oLinea.importePedido > 0) And oLinea.importerecibido > NullToDbl0(oLinea.importerecibido) + oLinea.importerecepcionado Then
                bLineasComplConDesvio = False
            End If
        End If
        
        If oLinea.PedidoAbierto = 1 Then
            'Si alguna de las lineas es de un pedido abierto no se cerrara el pedido al recepcionar, se queda con estado En Recepcion
            blnHayPedidoAbierto = True
        End If
        'Valida que el almacen est� si el articulo es de almacen obligatorio o el pedido es de almacen obligatorio
        If oLinea.ArtAlmacenar = ObligatorioAlmacenar Or oOrdenEntrega.TipoDePedido.CodAlmac = ObligatorioAlmacenar Then
            bExiste = True
            If oLinea.Almacen Is Nothing Then
                bExiste = False
            ElseIf oLinea.Almacen.Id = 0 Then
                bExiste = False
            End If
            If Not bExiste Then
                oMensajes.NoValido sdbgPrecios.Columns("ALMACEN").caption
                PosicionarEnGrid IIf(IsNull(oLinea.ArticuloCod), oLinea.Descr, (oLinea.ArticuloCod & " - " & oLinea.Descr)), 9 'Col 9 Almacen
                Exit Sub
            End If
        End If

        'Valida que el activo est� si el articulo es de inversi�n
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            If (oOrdenEntrega.TipoDePedido.CodConcep = Ambos And oLinea.ArtConcepto = Inversion) Or oOrdenEntrega.TipoDePedido.CodConcep = Inversion Then
                bExiste = True
                If oLinea.Activo Is Nothing Then
                    bExiste = False
                ElseIf oLinea.Activo.Id = 0 Then
                    bExiste = False
                End If
                If Not bExiste Then
                    If oOrdenEntrega.TipoDePedido.CodConcep = Inversion Then
                        oMensajes.InformarActivo_PedInv (oLinea.Descr)
                    End If
                    If oOrdenEntrega.TipoDePedido.CodConcep = Ambos And oLinea.ArtConcepto = Inversion Then
                        oMensajes.InformarActivo (oLinea.Descr)
                    End If
                    PosicionarEnGrid IIf(IsNull(oLinea.ArticuloCod), oLinea.Descr, (oLinea.ArticuloCod & " - " & oLinea.Descr)), 11 'Col 9 aCTIVO
                    Exit Sub
                End If
            End If
        End If
            
    Next
    
    If iNulos = oRecepSeleccionada.Lineas.Count Then
       oMensajes.FaltanDatos sIdiCantRec
        Exit Sub
    End If
    
    Dim arrIdOrdenes() As Long
    ReDim arrIdOrdenes(0)
    Dim arrIdLineas() As Long
    ReDim arrIdLineas(0)
    Dim arrCantLineas() As Double
    ReDim arrCantLineas(0)

    Dim indiceLineas As Integer
    indiceLineas = -1

    For Each oLin In oRecepSeleccionada.Lineas
        ReDim Preserve arrIdLineas(indiceLineas + 1)
        ReDim Preserve arrIdOrdenes(indiceLineas + 1)
        ReDim Preserve arrCantLineas(indiceLineas + 1)
        arrIdLineas(indiceLineas + 1) = oLin.IdLineaPed
        arrIdOrdenes(indiceLineas + 1) = oRecepSeleccionada.Orden
        arrCantLineas(indiceLineas + 1) = oLin.cantRecepcionada
        indiceLineas = indiceLineas + 1
    Next

    ''Validaciones de integraci�n:
    If ValidacionesERP(oOrdenEntrega.Empresa, oOrdenEntrega.OrgCompras, txtalbaran, txtComent.Text, arrIdLineas, arrIdOrdenes, arrCantLineas, txtFechaContable.Text) = False Then
        ''Sale sin hacer la recepci�n
        Exit Sub
    End If

    ''COMPROBAR SI SE HA RECEPCIONADO EL TOTAL DEL PEDIDO
    If (Not bAlgunDesvio And oRecepSeleccionada.getTotalPedido = oRecepSeleccionada.getTotalRecepcionado() + oRecepSeleccionada.getTotalrecibido) _
    Or (bAlgunDesvio And bLineasComplConDesvio) Then
        If oRecepSeleccionada.OrdenEstado < RecibidoYCerrado Then
            irespuesta = oMensajes.PreguntaEstadoCerrado(oOrdenEntrega.Anyo & "/" & oOrdenEntrega.NumPedido & "/" & oOrdenEntrega.Numero)
            bCierre = True
            If irespuesta = vbYes Then
                optCompleto.Value = True
            End If
        Else
            irespuesta = vbYes
        End If
    Else
        If optCompleto.Value = True Then
            bCierre = True
            irespuesta = vbYes
        Else
            bCierre = False
            If oOrdenEntrega.Estado = RecibidoYCerrado Then
                irespuesta = oMensajes.MensajeYesNo(698)
            Else
                irespuesta = vbYes
            End If
        End If
        
    End If
    
   ' If Not blnHayPedidoAbierto Then
   ''EPB REvisar esto     oOrdenEntrega.Estado = RecibidoYCerrado
   ' End If
    If irespuesta = vbYes Then
        oRecepSeleccionada.FechaRec = CDate(txtFecRec.Text)
        If gParametrosGenerales.gbMostrarFechaContable Then
            If Trim(txtFechaContable.Text) = "" Then
                oRecepSeleccionada.FechaContable = Null
            Else
                oRecepSeleccionada.FechaContable = CDate(txtFechaContable.Text)
            End If
        End If
        
        oRecepSeleccionada.albaran = txtalbaran.Text
        oRecepSeleccionada.Completo = False
        If optRecOk.Value Then
            oRecepSeleccionada.Correcto = True
            If blnHayPedidoAbierto Then
                oRecepSeleccionada.Completo = False
            End If
        ElseIf optRecKo.Value Then
            oRecepSeleccionada.Correcto = False
        Else
            oRecepSeleccionada.Completo = True
            oRecepSeleccionada.Correcto = True
        End If
        
        oRecepSeleccionada.coment = StrToNull(txtComent.Text)
        
        Select Case Accion
            
            Case ACCRecepcionPedAnya
                oRecepSeleccionada.Persona = sPersona
                oRecepSeleccionada.Usuario = sUsuario
                oRecepSeleccionada.Moneda = oOrdenEntrega.Moneda
                oRecepSeleccionada.Cambio = oOrdenEntrega.Cambio
                oRecepSeleccionada.NumErp = TxtNumeroSap.Text
                Set oIBaseDatos = oRecepSeleccionada
                teserror = oIBaseDatos.AnyadirABaseDatos
                
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Exit Sub
                Else
                    basSeguridad.RegistrarAccion ACCRecepcionPedAnya, "Orden :" & oOrdenEntrega.Anyo & "/" & oRecepSeleccionada.pedido & "/" & oRecepSeleccionada.Orden & " Fecha Recepci�n : " & oRecepSeleccionada.FechaRec & " Estado :" & oRecepSeleccionada.OrdenEstado
                End If
                '''tras la recepcion actualizamos balta para que nos muestre bien los datos de la recepcion
                bAlta = False
            Case ACCRecepcionPedMOD
                            
                Screen.MousePointer = vbHourglass
                oRecepSeleccionada.Persona = sPersona
                oRecepSeleccionada.Usuario = sUsuario
                oRecepSeleccionada.Moneda = oOrdenEntrega.Moneda
                oRecepSeleccionada.Cambio = oOrdenEntrega.Cambio
                oRecepSeleccionada.NumErp = TxtNumeroSap.Text
                Set oIBaseDatos = oRecepSeleccionada
                teserror = oIBaseDatos.FinalizarEdicionModificando
                Screen.MousePointer = vbNormal
                
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Exit Sub
                End If
                basSeguridad.RegistrarAccion ACCRecepcionPedMOD, "Orden :" & oOrdenEntrega.Anyo & "/" & oRecepSeleccionada.pedido & "/" & oRecepSeleccionada.Orden & " Fecha Recepci�n : " & oRecepSeleccionada.FechaRec & " Estado :" & oRecepSeleccionada.OrdenEstado
            
        End Select
        
        bAlgunaModificacion = True
        
        If chkEnviar.Value = vbChecked Then
            If optRecOk.Value Or optCompleto.Value Then
                frmNotificaPedido.TipoComu = 2
            Else
               frmNotificaPedido.TipoComu = 3
            End If
            Set frmNotificaPedido.oOrdenEntrega = oOrdenEntrega
            frmNotificaPedido.Cierre = bCierre
            frmNotificaPedido.CodProve = oOrdenEntrega.ProveCod
            frmNotificaPedido.DenProve = oOrdenEntrega.ProveDen
            frmNotificaPedido.pedido = oOrdenEntrega.idPedido
            frmNotificaPedido.Orden = oOrdenEntrega.Id
            frmNotificaPedido.TipoPedi = oOrdenEntrega.Tipo
            MDI.MostrarFormulario frmNotificaPedido, True
            frmNotificaPedido.Show
        End If
        If Not bCierre Then
            sdbcFecRec.Columns("ID").Value = oRecepSeleccionada.Id
            sdbcFecRec.Text = oRecepSeleccionada.FechaRec
            sdbcFecRec_Validate False
            bCargarComboDesde = False
            Modo_Consulta
        Else
            If chkEnviar.Value = vbUnchecked Then
                Unload Me
            End If
        End If

    End If  'YES a Respuesta

End Sub

''' <summary>
''' Pone la pantalla en modo recepcionar una nueva recepcion
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdA�adir_Click()
    bAlta = True
    Me.fraBloqueoFactura.Visible = False
        
    Me.TxtNumeroSap.Text = ""
    
    If Not Modo_Recepcion(oRecepSeleccionada.OrdenEstado) Then
        RecepSeleccionada
    End If
    bAlta = False
End Sub

''' <summary>
''' Cambia la opci�n de "Bloqueo de Factura" para la recepci�n en bbdd y reconfigura la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdBloquear_Click()
    If oRecepSeleccionada.BloqueoFactura Then
        oRecepSeleccionada.BloqueoFactura = False
        Me.cmdBloquear.caption = m_sTxtBloquear
        Me.fraBloqueoFactura.Visible = False
    Else
        oRecepSeleccionada.BloqueoFactura = True
        Me.cmdBloquear.caption = m_sTxtDesBloquear
        Me.fraBloqueoFactura.Visible = True
    End If
    
    oRecepSeleccionada.ModificarBloqueoFactura
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendarRec_Click()
    AbrirFormCalendar frmRecepGral, txtFecRec
End Sub

Private Sub cmdCalendarCon_Click()
    If Not txtFechaContable = "" And Not IsDate(txtFechaContable.Text) Then
        oMensajes.NoValido sIdiFecContable
        If Me.Visible Then txtFechaContable.SetFocus
        Exit Sub
    End If
    AbrirFormCalendar frmRecepGral, txtFechaContable
End Sub

Private Sub cmdCancelar_Click()
If ACCRecepcionPedAnya Then
    Unload Me
Else
    sdbgPrecios.CancelUpdate
    sdbgPrecios.DataChanged = False
    
    If Not oRecepSeleccionada Is Nothing Then
        oIBaseDatos.CancelarEdicion
        Set oIBaseDatos = Nothing
    End If
    Modo_Consulta
    
End If
End Sub
''' <summary>
''' Elimina una recepci�n
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdEliminar_Click()
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim oImp As Cimputacion
Dim oNoVigentes As CImputaciones
Dim sMensaje As String
Dim iBloquear As Integer
    
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        If g_oParametrosSM.Count > 0 Then
        'Valido la vigencia antes de modificar
        Set oNoVigentes = oRecepSeleccionada.ValidarVigenciaPartidas(, , True)
        If Not oNoVigentes Is Nothing Then
            sMensaje = ""
            For Each oImp In oNoVigentes
                If iBloquear <> 2 Then
                    iBloquear = g_oParametrosSM.Item(oImp.PRES5).Vigencia
                End If
                sMensaje = sMensaje & g_oParametrosSM.Item(oImp.PRES5).NomNivelImputacion & ": " & oImp.PresDen & " (" & oImp.FecInicio & " - " & oImp.FecFin & ") " & IIf(oImp.Cerrado, m_sCerrado, "") & IIf(g_oParametrosSM.Item(oImp.PRES5).Vigencia = 2, "�" & m_sBloquea & "!", "") & vbCrLf
            Next
            irespuesta = oMensajes.ImputacionesSMNoVigentes(2, sMensaje, IIf(iBloquear = 2, False, True))
            If irespuesta = vbNo Then
                Exit Sub
            End If
        End If
        Set oNoVigentes = Nothing
        End If
    End If
    
    irespuesta = oMensajes.PreguntaEliminar(Me.caption)
    
    If irespuesta = vbYes Then

        
        Set oIBaseDatos = oRecepSeleccionada
        oRecepSeleccionada.Persona = sPersona
        oRecepSeleccionada.Usuario = sUsuario
        oRecepSeleccionada.Moneda = oOrdenEntrega.Moneda
        oRecepSeleccionada.Cambio = oOrdenEntrega.Cambio
        teserror = oIBaseDatos.EliminarDeBaseDatos
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            bAlgunaModificacion = True
        
            basSeguridad.RegistrarAccion ACCRecepcionPedEli, "Orden :" & oOrdenEntrega.Anyo & "/" & oRecepSeleccionada.pedido & "/" & oRecepSeleccionada.Orden & " Fecha Recepci�n : " & oRecepSeleccionada.FechaRec & " Estado :" & oRecepSeleccionada.OrdenEstado
            Set oRecepciones = oOrdenEntrega.DevolverTodasLasRecepciones(, True)
            If oRecepciones.Count > 0 Then
            ' por si borramos todas las recepciones
                sdbcFecRec.Columns("ID").Value = oRecepciones.Item(1).Id
                sdbcFecRec.Text = oRecepciones.Item(1).FechaRec
                sdbcFecRec_Validate False
                bCargarComboDesde = False
                Modo_Consulta
            Else
                Unload Me
            End If
                    
        End If
        
    End If

End Sub

Private Sub cmdModificar_Click()
Dim teserror As TipoErrorSummit
Dim oImp As Cimputacion
Dim oNoVigentes As CImputaciones
Dim sMensaje As String
Dim iBloquear As Integer
Dim irespuesta As Integer

    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        If g_oParametrosSM.Count > 0 Then
        'Valido la vigencia antes de modificar, s�lo aviso y dejo entrar porque podr�a haber lineas vigentes
        Set oNoVigentes = oRecepSeleccionada.ValidarVigenciaPartidas(, True)
        If Not oNoVigentes Is Nothing Then
            sMensaje = ""
            For Each oImp In oNoVigentes
                If iBloquear <> 2 Then
                    iBloquear = g_oParametrosSM.Item(oImp.PRES5).Vigencia
                End If
                sMensaje = sMensaje & g_oParametrosSM.Item(oImp.PRES5).NomNivelImputacion & ": " & oImp.PresDen & " (" & oImp.FecInicio & " - " & oImp.FecFin & ") " & IIf(oImp.Cerrado, m_sCerrado, "") & IIf(g_oParametrosSM.Item(oImp.PRES5).Vigencia = 2, "�" & m_sBloquea & "!", "") & vbCrLf
            Next
            irespuesta = oMensajes.ImputacionesSMNoVigentes(IIf(m_bCabeceraBloqueo, 3, 5), sMensaje, IIf(iBloquear = 2 And m_bCabeceraBloqueo, False, True))
            If irespuesta = vbNo Then
                Exit Sub
            End If
        End If
        Set oNoVigentes = Nothing
        End If
    End If

    
    Set oIBaseDatos = oRecepSeleccionada
    
    teserror = oIBaseDatos.IniciarEdicion
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    Modo_Edicion
    
    End Sub


Private Sub Form_Activate()
Unload frmNotificaPedido
End Sub
''' <summary>
''' Carga la pantalla
''' </summary>
''' <remarks>Llamada desde: frmRecepGral y FrmSeguimiento ; Tiempo m�ximo: 0,3</remarks>
Private Sub Form_Load()
    'En principio suponemos que es una nueva recepcion
    bAlta = True
    
    Me.Height = 8520
    Me.Width = 11970
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        sPersona = oUsuarioSummit.Persona.Cod
        sUsuario = basOptimizacion.gvarCodUsuario 'integracion
    Else
        sPersona = ""
        sUsuario = ""
    End If
    
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        m_bCabeceraBloqueo = False
    End If
    ConfigurarNumERP
    ConfigurarGrid
    Arrange
    
    fraBloqueoFactura.Visible = False
    
    If gParametrosGenerales.gbMostrarFechaContable Then
        fraFechaContable.Visible = True
    Else
        fraFechaContable.Visible = False
    End If
    
    If Accion = ACCRecepcionPedAnya Then
        If Not Modo_Recepcion(oOrdenEntrega.Estado) Then
            bAlta = False
            Modo_Consulta
        End If
    Else
        bAlta = False
        Modo_Consulta
    End If
    
    bAlgunaModificacion = False
    
    If bCargarFechaMasReciente Then bActualizaSeguimiento = True
    
    Me.cmdBloquear.Visible = (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM)
    
    Me.TxtNumeroSap.Locked = True
End Sub

''' <summary>
''' Configura las columnas activo y almacen visibles si procede y a�ade dinamicamente las columnas de imputaci�n SM
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisada por ngo</remarks>
Private Sub ConfigurarNumERP()

If gParametrosGenerales.gbCodPersonalizRecep Then
    If oOrdenEntrega.Tipo = Aprovisionamiento Then
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Aprov) Or _
            basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Aprov) Then
            fraNumERP.Visible = True
            sdbcFecRec.Columns("NUMERP").Visible = True
        Else
            fraNumERP.Visible = False
            sdbcFecRec.Columns("NUMERP").Visible = False
        End If
    Else
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Directo) Or _
            basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Directo) Then
            fraNumERP.Visible = True
            sdbcFecRec.Columns("NUMERP").Visible = True
        Else
            fraNumERP.Visible = False
            sdbcFecRec.Columns("NUMERP").Visible = False
        End If
    
    End If
Else
    fraNumERP.Visible = False
    sdbcFecRec.Columns("NUMERP").Visible = False
End If
End Sub

''' <summary>Configura las columnas activo y almacen visibles si procede y a�ade dinamicamente las columnas de imputaci�n SM</summary>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisada por ngo</remarks>
Private Sub ConfigurarGrid()
    Dim oParamSM As CParametroSM
    Dim i As Integer
    Dim oColumn As SSDataWidgets_B.Column

    If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.SinAcceso Then
        sdbgPrecios.Columns("ACTIVO").Visible = False
    Else
        If oOrdenEntrega.TipoDePedido.CodConcep = Gasto Then
            sdbgPrecios.Columns("ACTIVO").Visible = False
        End If
    End If
    If oOrdenEntrega.TipoDePedido.CodAlmac = NoAlmacenable Then
        sdbgPrecios.Columns("ALMACEN").Visible = False
    Else
        sdbddAlmacenes.AddItem ""
        sdbgPrecios.Columns("ALMACEN").DropDownHwnd = sdbddAlmacenes.hWnd
    End If
    sdbgPrecios.Columns("SALIDA_ALMACEN").Visible = gParametrosGenerales.gbMostrarRecepSalidaAlmacen

    i = sdbgPrecios.Columns.Count
    'Si hay parametros de SM a nivel linea configuro las l�neas
    If Not g_oParametrosSM Is Nothing Then
        For Each oParamSM In g_oParametrosSM
            If oParamSM.ImpModo = 0 And oParamSM.Vigencia = 2 Then m_bCabeceraBloqueo = True
            If oParamSM.ImpPedido <> 0 Then
                DoEvents
                sdbgPrecios.Columns.Add i
                DoEvents
                Set oColumn = sdbgPrecios.Columns(i)
                oColumn.Name = "SM_CC_" & CStr(oParamSM.PRES5)  'Columna para el CC
                oColumn.CaptionAlignment = ssColCapAlignCenter
                oColumn.Alignment = ssCaptionAlignmentLeft
                oColumn.Style = ssStyleEdit
                oColumn.FieldLen = 100
                oColumn.caption = m_sIdiCentroC
                oColumn.Visible = True
                oColumn.Locked = True
                Set oColumn = Nothing
                i = i + 1
                DoEvents
                sdbgPrecios.Columns.Add i
                DoEvents
                Set oColumn = sdbgPrecios.Columns(i)
                oColumn.Name = "SM_PA_" & CStr(oParamSM.PRES5)   'Columna para el CC
                oColumn.CaptionAlignment = ssColCapAlignCenter
                oColumn.Alignment = ssCaptionAlignmentLeft
                oColumn.Style = ssStyleEdit
                oColumn.FieldLen = 100
                oColumn.caption = oParamSM.NomNivelImputacion
                oColumn.Visible = True
                oColumn.Locked = True
                Set oColumn = Nothing
                i = i + 1
            End If
        Next
    End If
End Sub
Private Sub ConfigurarSeguridad()

If Not bMod Then
    cmdA�adir.Enabled = False
    cmdEliminar.Enabled = False
    cmdModificar.Enabled = False
End If
End Sub
''' <summary>
''' Carga los idiomas del formulario.
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo=0,2</remarks>
Private Sub CargarRecursos()
    
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_RECEPCION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        'Caption = Ador(0).Value '1
        Ador.MoveNext
        lblFecRec = Ador(0).Value & ":"
        sIdiFecRec = Ador(0).Value
        Ador.MoveNext
        sdbcFecRec.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("DEST").caption = Ador(0).Value '5
        Ador.MoveNext
        sdbgPrecios.Columns("UNI").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("INI").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("PRECIO").caption = Ador(0).Value & " (" & oOrdenEntrega.Moneda & ")"
        Ador.MoveNext
        sdbgPrecios.Columns("CANT_PED").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("CANT_RECIBIDA").caption = Ador(0).Value
        Ador.MoveNext
        sIdiCantRec = Ador(0).Value
        Ador.MoveNext
        lblAlbaran = Ador(0).Value
        sIdiAlbaran = Ador(0).Value
        Ador.MoveNext
        optRecOk.caption = Ador(0).Value
        Ador.MoveNext
        optRecKo.caption = Ador(0).Value
        Ador.MoveNext
        optCompleto.caption = Ador(0).Value
        Ador.MoveNext
        lblComent = Ador(0).Value
        Ador.MoveNext
        chkEnviar.caption = Ador(0).Value
        Ador.MoveNext
        
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiSincronizado = Ador(0).Value
        Ador.MoveNext
        m_sIdiNoSincronizado = Ador(0).Value
        Ador.MoveNext
        m_sIdiCentroC = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("ALMACEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("ACTIVO").caption = Ador(0).Value
        Ador.MoveNext
        m_sCerrado = Ador(0).Value
        Ador.MoveNext
        m_sBloquea = Ador(0).Value
        Ador.MoveNext
        sdbddAlmacenes.Columns("COD").caption = Ador(0).Value '30
        Ador.MoveNext
        sdbddAlmacenes.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiReceptor = Ador(0).Value
        Ador.MoveNext
        m_sIdiAdm = Ador(0).Value
        Ador.MoveNext
        m_sTxtBloquear = Ador(0).Value
        Ador.MoveNext
        m_sTxtDesBloquear = Ador(0).Value
        Ador.MoveNext
        Me.lblBloqueoFactura.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("DESVIO").caption = Ador(0).Value
        Ador.MoveNext
        Me.lblImporteTotalAlbaran.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").caption = Ador(0).Value & " (" & oOrdenEntrega.Moneda & ")"
        Ador.MoveNext
        'C.en esta R. substituye a cantidad en esta recepci�n (tarea 3050)
        sdbgPrecios.Columns("CANT_RECEPCIONADA").caption = Ador(0).Value
        Ador.MoveNext
        'Imp.en esta R. substituye a Importe en esta Recepci�n (")
        sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").caption = Ador(0).Value & " (" & oOrdenEntrega.Moneda & ")"
        Ador.MoveNext
        sdbgPrecios.Columns("IMPORTE_PED").caption = Ador(0).Value & " (" & oOrdenEntrega.Moneda & ")"
        Ador.MoveNext
        sdbgPrecios.Columns("IMPORTE_RECIBIDO").caption = Ador(0).Value & " (" & oOrdenEntrega.Moneda & ")"
        'Ador.MoveNext
        'sdbgPrecios.Columns("SALIDA_ALMACEN").caption = Ador(0).Value
        Ador.MoveNext
        lblFecCon = Ador(0).Value & ":"
        sIdiFecContable = Ador(0).Value
                
        Ador.Close
    End If

    Set Ador = Nothing
    
    lblNumeroSap.caption = gParametrosGenerales.gsNomCodPersonalizRecep
    sdbcFecRec.Columns("NUMERP").caption = gParametrosGenerales.gsNomCodPersonalizRecep
    
End Sub


Private Sub Form_Resize()
    Arrange
End Sub
''' <summary>
''' Al cerrar la pantalla libera variables de modulo y si se ha venido desde frmRecepGral, recarga frmRecepGral
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Unload(Cancel As Integer)

If bActualizaSeguimiento Then
    
    If bAlgunaModificacion Then
        frmSeguimiento.TrasRecepcionarActualizar oOrdenEntrega.Id
    End If
End If

bActualizaSeguimiento = False

Set oOrdenEntrega = Nothing
Set oRecepSeleccionada = Nothing
Set oRecepciones = Nothing
Set oIBaseDatos = Nothing

Accion = 0

Me.Visible = False
bCargarFechaMasReciente = False
End Sub


Private Sub sdbcFecRec_Change()
 
            
    If Not bRespetarCombo Then
        txtFechaContable.Text = ""
        txtalbaran.Text = ""
        txtComent.Text = ""
        TxtNumeroSap.Text = ""
        sdbgPrecios.RemoveAll
        
        
        If sdbcFecRec.Value <> "" Then
            bCargarComboDesde = True
        Else
            bCargarComboDesde = False
        End If
        
        Set oRecepSeleccionada = Nothing
        
    End If

End Sub

Private Sub sdbcFecRec_CloseUp()
    
    If sdbcFecRec.Value = "" Then
        Exit Sub
    End If
    
    sdbcFecRec.Text = sdbcFecRec.Columns("FECHA").Text
    RecepSeleccionada
    bCargarComboDesde = False
    

End Sub
''' <summary>
''' Cargar el combo de recepciones
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Public Sub sdbcFecRec_DropDown()
Dim oRec As CRecepcion
Dim sFecha As String

    Set oRecepciones = Nothing
    
    sdbcFecRec.RemoveAll
        
    Screen.MousePointer = vbHourglass
    If bCargarComboDesde Then
            
        If IsDate(sdbcFecRec) Then
            Set oRecepciones = oOrdenEntrega.DevolverTodasLasRecepciones(CDate(Format(sdbcFecRec.Text, "Short date")))
        End If
    Else
        Set oRecepciones = oOrdenEntrega.DevolverTodasLasRecepciones()
        
    End If
    
    m_lIdFechaMax = 0
    For Each oRec In oRecepciones
        sdbcFecRec.AddItem oRec.FechaRec & Chr(m_lSeparador) & oRec.NumErp & Chr(m_lSeparador) & oRec.Id
        
        If bCargarFechaMasReciente And (m_lIdFechaMax = 0) Then
            m_lIdFechaMax = oRec.Id
            sFecha = oRec.FechaRec
        End If
    Next
    
    If bCargarFechaMasReciente And Not (m_lIdFechaMax = 0) Then
        sdbcFecRec.Columns("ID").Value = m_lIdFechaMax
        sdbcFecRec.Text = sFecha
    End If
    
    Screen.MousePointer = vbNormal
    

End Sub

Private Sub sdbcFecRec_InitColumnProps()
    
    sdbcFecRec.DataFieldList = "Column 0"
    sdbcFecRec.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcFecRec_PositionList(ByVal Text As String)
 ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcFecRec.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcFecRec.Rows - 1
            bm = sdbcFecRec.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcFecRec.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcFecRec.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    

End Sub
''' <summary>
''' Selecciona una recepci�n y la carga
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Public Sub sdbcFecRec_Validate(Cancel As Boolean)
    
    If bCargarFechaMasReciente Then
        Screen.MousePointer = vbHourglass
    Else
        If Trim(sdbcFecRec.Text = "") Then Exit Sub
        
        If Not IsDate(sdbcFecRec) Then
            oMensajes.NoValida sIdiFecRec
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        
        Set oRecepciones = oOrdenEntrega.DevolverTodasLasRecepciones(CDate(sdbcFecRec.Text))
    End If
    
    If oRecepciones Is Nothing Then
        sdbcFecRec.Text = ""
        oMensajes.NoValida sIdiFecRec
    Else
        RecepSeleccionada m_lIdFechaMax
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub



Private Sub sdbgPrecios_BtnClick()
Dim oLinea As CLineaRecepcion
Dim oImp As Cimputacion
Dim bExiste As Boolean

    If sdbgPrecios.Columns(sdbgPrecios.col).Name = "ACTIVO" And gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        Set oLinea = oRecepSeleccionada.Lineas.Item(sdbgPrecios.Columns("ID").Value)
        If oLinea.Imputaciones.Count > 0 Then
            For Each oImp In oLinea.Imputaciones
                If oImp.CC1 <> "" Then
                    frmSelActivo.g_sUON1 = oImp.CC1
                    frmSelActivo.g_sUON2 = oImp.CC2
                    frmSelActivo.g_sUON3 = oImp.CC3
                    frmSelActivo.g_sUON4 = oImp.CC4
                    Exit For
                End If
            Next
        End If
        frmSelActivo.g_sOrigen = "frmRecepcion"
        frmSelActivo.g_lEmpresa = oOrdenEntrega.Empresa
        frmSelActivo.Show 1
        If g_oActivo Is Nothing Then
            bExiste = False
        ElseIf g_oActivo.Id = 0 Then
            bExiste = False
        Else
            bExiste = True
        End If
        bRespetarCombo = True
        If bExiste Then
            Set oLinea.Activo = g_oActivo
            sdbgPrecios.Columns("ACTIVO").Value = oLinea.Activo.Cod
        Else
            Set oLinea.Activo = Nothing
            Set oLinea.Activo = oFSGSRaiz.Generar_CActivo
            sdbgPrecios.Columns("ACTIVO").Value = ""
        End If
        bRespetarCombo = False
        
                
    End If
End Sub

''' <summary>Establece la posici�n y el tama�o de los controles del formulario</summary>
''' <remarks>Llamada desde: Form_load from_resize; Tiempo m�ximo: 0</remarks>
Private Sub Arrange()

On Error Resume Next


        Frafec.Width = Me.Width - 135
        
        FraLineas.Width = Me.Width - 135
        chkTotal.Left = Me.Width - 1110
        lblReceptor.Left = Me.Width - 4070
        
        fraFechaContable.Left = cmdCalendarRec.Left + (cmdCalendarRec.Width) * 2
        fraNumERP.Left = fraFechaContable.Left + fraFechaContable.Width + 300
        lblImporteTotalAlbaran.Left = fraNumERP.Left + fraNumERP.Width + 300
        lblImporteTotalAlbaran.Left = fraNumERP.Left + fraNumERP.Width + 300
        lblImporteTotalAlbaranDato.Left = lblImporteTotalAlbaran.Left + lblImporteTotalAlbaran.Width
        
        
        FraInt.Left = Frafec.Width - 1095
        
        FraLineas.Height = Me.Height - (1215 + 400)
         
         
        sdbgPrecios.Width = Me.Width - 195
                
        txtComent.Width = Me.Width - 1800
        
        sdbgPrecios.Height = FraLineas.Height - (picDatos.Height + 300)
        sdbgPrecios.SplitterVisible = True
        sdbgPrecios.SplitterPos = 2
        
        If Me.Width <= 11970 Then
            sdbgPrecios.Columns("NUM").Width = sdbgPrecios.Width * 5.4 / 100
            If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM And sdbgPrecios.Columns("ALMACEN").Visible = False And sdbgPrecios.Columns("ACTIVO").Visible = False Then
                sdbgPrecios.Columns("DEN").Width = sdbgPrecios.Width * 24 / 100
            Else
                sdbgPrecios.Columns("DEN").Width = sdbgPrecios.Width * 18 / 100
            End If
            sdbgPrecios.Columns("DEST").Width = sdbgPrecios.Width * 7.5 / 100
            sdbgPrecios.Columns("UNI").Width = sdbgPrecios.Width * 7.5 / 100
            sdbgPrecios.Columns("PRECIO").Width = sdbgPrecios.Width * 10.5 / 100
            sdbgPrecios.Columns("INI").Width = sdbgPrecios.Width * 10.5 / 100
            sdbgPrecios.Columns("CANT_PED").Width = sdbgPrecios.Width * 10.5 / 100
            sdbgPrecios.Columns("CANT_RECEPCIONADA").Width = sdbgPrecios.Width * 10.5 / 100
            sdbgPrecios.Columns("CANT_RECIBIDA").Width = sdbgPrecios.Width * 13 / 100
        Else
            sdbgPrecios.Columns("NUM").Width = 645
            sdbgPrecios.Columns("DEN").Width = 2970
            sdbgPrecios.Columns("DEST").Width = 885
            sdbgPrecios.Columns("UNI").Width = 885
            sdbgPrecios.Columns("PRECIO").Width = 1230
            sdbgPrecios.Columns("INI").Width = 1230
            sdbgPrecios.Columns("CANT_PED").Width = 1230
            sdbgPrecios.Columns("CANT_RECEPCIONADA").Width = 1230
            sdbgPrecios.Columns("CANT_RECIBIDA").Width = 1545
        End If
        picDatos.Top = sdbgPrecios.Height + 195
        picDatos.Width = Me.Width - 375
        picEdit.Top = Frafec.Height + FraLineas.Height
        cmdAceptar.Left = Me.Width / 2 - 1200
        cmdCancelar.Left = cmdAceptar.Left + 1215
        picNavigate.Top = Frafec.Height + FraLineas.Height
        picEdit.Width = picDatos.Width
        picNavigate.Width = picDatos.Width
    
End Sub

''' <summary>
''' DEvuelve el c�digo de la UON que es centro de coste
''' </summary>
''' <param name="CC1">C�digo UON 1</param>
''' <param name="CC2">C�digo UON 2</param>
''' <param name="CC3">C�digo UON 3</param>
''' <param name="CC4">C�digo UON 4</param>
''' <returns>String, c�digo de la UON que es centro de coste</returns>
''' <remarks>Llamada desde: CargarGridConRecep; Tiempo m�ximo: 0,1</remarks>
Private Function DevolverCodCC(ByVal CC1 As String, ByVal CC2 As String, ByVal CC3 As String, ByVal CC4 As String) As String
If CC4 <> "" Then
    DevolverCodCC = CC4
ElseIf CC3 <> "" Then
    DevolverCodCC = CC3
ElseIf CC2 <> "" Then
    DevolverCodCC = CC2
ElseIf CC1 <> "" Then
    DevolverCodCC = CC1
Else
    DevolverCodCC = ""
End If
End Function
Private Sub sdbddAlmacenes_CloseUp()
    If sdbddAlmacenes.Columns("ID").Value = "" Then Exit Sub
    
    If oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen Is Nothing Then Set oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen = oFSGSRaiz.Generar_CAlmacen
    oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen.Id = sdbddAlmacenes.Columns("ID").Value
    oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen.Cod = sdbddAlmacenes.Columns("COD").Value
    oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen.Den = sdbddAlmacenes.Columns("DEN").Value
    sdbgPrecios.Columns("ALMACEN").Value = sdbddAlmacenes.Columns("COD").Value & " - " & sdbddAlmacenes.Columns("DEN").Value
    sdbgPrecios.Columns("ALM_ID").Value = sdbddAlmacenes.Columns("ID").Value
    m_bValidaAlm = True


End Sub
''' <summary>
''' Carga el listado de almacenes
''' </summary>
''' <param name="">
''' <returns></returns>
''' <remarks>Llamada desde: Evento sistema; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisado por ngo</remarks>
Private Sub sdbddAlmacenes_DropDown()
Dim oAlmacenes As CAlmacenes
Dim oAlm As CAlmacen

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    
    sdbddAlmacenes.RemoveAll
    Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
    If gParametrosGenerales.gbUsarOrgCompras And oOrdenEntrega.Tipo = TipoPedido.Directo Then
          oAlmacenes.CargarAlmacenesCentro (sdbgPrecios.Columns("CENTRO").Value)
    Else
          oAlmacenes.CargarAlmacenesDestino (sdbgPrecios.Columns("DEST").Value)
    End If
    
    If oAlmacenes.Count = 0 Then
        sdbddAlmacenes.RemoveAll
        Screen.MousePointer = vbNormal
        sdbddAlmacenes.AddItem ""
        sdbgPrecios.ActiveCell.SelStart = 0
        sdbgPrecios.ActiveCell.SelLength = Len(sdbgPrecios.ActiveCell.Text)
    Else
        For Each oAlm In oAlmacenes
            sdbddAlmacenes.AddItem oAlm.Cod & Chr(m_lSeparador) & oAlm.Den & Chr(m_lSeparador) & oAlm.Id
        Next
        
        If sdbddAlmacenes.Rows = 0 Then
            sdbddAlmacenes.AddItem ""
        End If
        
        Set oAlm = Nothing
        Set oAlmacenes = Nothing
        
        sdbgPrecios.ActiveCell.SelStart = 0
        sdbgPrecios.ActiveCell.SelLength = Len(sdbgPrecios.ActiveCell.Text)
    End If
    Set oAlmacenes = Nothing
End Sub

Private Sub sdbddAlmacenes_InitColumnProps()
    
    sdbddAlmacenes.DataFieldList = "Column 0"
    sdbddAlmacenes.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbddAlmacenes_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddAlmacenes.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddAlmacenes.Rows - 1
            bm = sdbddAlmacenes.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddAlmacenes.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddAlmacenes.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbddAlmacenes_ValidateList(Text As String, RtnPassed As Integer)



    ''' * Objetivo: Validar la seleccion (nulo o existente)
    

    If m_bValidaAlm = True Then Exit Sub
    
    If Text = "" Then
        RtnPassed = True 'Si lo dejan vacio pongo la property a nothing
        Set oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen = Nothing
        Set oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen = New CAlmacen
        sdbgPrecios.Columns("ALM_ID").Value = ""
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddAlmacenes.Columns("COD").Value = Text Then
            RtnPassed = True
            Exit Sub
        End If
        
        If ValidarAlmacen(Text) Then
            RtnPassed = True
        End If
        
    End If
End Sub
''' <summary>
''' Valida que el texto pasado es un almacen v�lido para la linea
''' </summary>
''' <param name="sText">C�digo de almacen</param>
''' <returns>Boolean, true es valido, false no lo es</returns>
''' <remarks>Llamada desde: CargarGridConRecep; Tiempo m�ximo: 0,1</remarks>
Private Function ValidarAlmacen(ByVal sText As String) As Boolean
Dim oAlmacenes As CAlmacenes

    Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
    
    If Not (oAlmacenes.CargarTodosLosAlmacenes(UCase(sText), , , False).EOF) Then
        If oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen Is Nothing Then Set oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen = oFSGSRaiz.Generar_CAlmacen
        oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen.Id = oAlmacenes.Item(1).Id
        oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen.Cod = oAlmacenes.Item(1).Cod
        oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen.Den = oAlmacenes.Item(1).Den
        sdbgPrecios.Columns("ALMACEN").Value = oAlmacenes.Item(1).Cod & " - " & oAlmacenes.Item(1).Den
        sdbgPrecios.Columns("ALM_ID").Value = oAlmacenes.Item(1).Id
        m_bValidaAlm = True
        ValidarAlmacen = True
    Else
        sdbgPrecios.Columns("ALMACEN").Value = ""
        sdbgPrecios.Columns("ALM_ID").Value = ""
        Set oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen = Nothing
        Set oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).Almacen = New CAlmacen
        m_bValidaAlm = False
        ValidarAlmacen = False
    End If
    Set oAlmacenes = Nothing

End Function
''' <summary>Si se ha modificado el almac�n, se vac�a el ID</summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisada por ngo</remarks>
Private Sub sdbgPrecios_Change()
If sdbgPrecios.col = -1 Then Exit Sub
'Si modifican el almacen vacio el ID para luego validarlo
    If sdbgPrecios.Columns(sdbgPrecios.col).Name = "ALMACEN" Then
        sdbgPrecios.Columns("ALM_ID").Value = ""
        m_bValidaAlm = False
    End If
End Sub
''' <summary>
''' Evento click
''' </summary>
''' <param name="">
''' <returns></returns>
''' <remarks>Llamada desde: Evento sistema; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisado por ngo</remarks>
Public Sub sdbgPrecios_Click()
If sdbgPrecios.col = -1 Then Exit Sub
    
    If sdbgPrecios.Columns(sdbgPrecios.col).Name = "ALMACEN" Then
        sdbddAlmacenes.DroppedDown = True
    End If

End Sub
''' <summary>
''' Validaciones previas
''' </summary>
''' <param name="Cancel">cancel</param>
''' <returns></returns>
''' <remarks>Llamada desde: ; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisada por ngo</remarks>
Private Sub sdbgPrecios_BeforeRowColChange(Cancel As Integer)
    Dim iNumDecimales As Integer
    
    Set oLinea = oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value))
    
    If sdbgPrecios.DataChanged Then
        If sdbgPrecios.col = -1 Then Exit Sub
        If bRespetarCombo Then Exit Sub
        
        If sdbgPrecios.Columns(sdbgPrecios.col).Name = "CANT_RECEPCIONADA" Then
            If sdbgPrecios.Columns("CANT_RECEPCIONADA").Value = "" Then
                sdbgPrecios.Columns("CANT_RECEPCIONADA").Value = 0
                oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).cantRecepcionada = 0
                'si da aceptar le dira que faltan datos.
                'Exit Sub
            Else
                'Validamos q solo se introduzcan cantidades validas
                If Not IsNumeric(sdbgPrecios.Columns("CANT_RECEPCIONADA").Text) Then
                    oMensajes.NoValido sdbgPrecios.Columns("CANT_RECEPCIONADA").caption
                    Cancel = True
                    m_bError = True
                    Exit Sub
                Else
                    'Si el parametro de control de la cantidad recepcionada est� activado no se le permitir� meter 0.
                    If gParametrosGenerales.gbOblPedDirCantRecep Then
                        If CDbl(sdbgPrecios.Columns("CANT_RECEPCIONADA").Text) = 0 Then
                            oMensajes.MensajeOKOnly 1110, Exclamation
                            sdbgPrecios.Columns("CANT_RECIBIDA").Value = ""
                            Cancel = True
                            m_bError = True
                            Exit Sub
                        End If
                    End If
                    'Tratamos el numero maximo de decimales de la cantidad del pedido
                    iNumDecimales = ValidarNumeroMaximoDecimales(sdbgPrecios.Columns("UNI").Text, sdbgPrecios.Columns("CANT_RECEPCIONADA").Text)
                    If iNumDecimales <> -1 Then
                        oMensajes.MensajeMaximoDecimales 1125, iNumDecimales, sdbgPrecios.Columns("UNI").Text
                        If Me.Visible Then sdbgPrecios.SetFocus
                        Cancel = True
                        m_bError = True
                        Exit Sub
                    End If
                End If
            End If
            'Validamos la concordancia entre las casillas
            If sdbgPrecios.Columns("CANT_RECEPCIONADA").Value <> "-" Then
                '''Recepcion por cantidad
                If oLinea.isRecepcionCantidad Then
                    If CDbl(sdbgPrecios.Columns("CANT_PED").Value) > 0 Then
                         'Cantidades positivas
                         'No se pueden recepcionar cantidades negativas
                         If CDbl(sdbgPrecios.Columns("CANT_RECEPCIONADA").Value) < 0 Then
                             oMensajes.ImposibleRecepcionarNegativos
                             Cancel = True
                             m_bError = True
                             Exit Sub
                         End If
                         oLinea.cantRecepcionada = sdbgPrecios.Columns("CANT_RECEPCIONADA").Value
                         If CDbl(StrToDbl0(sdbgPrecios.Columns("CANT_RECIBIDA").Value)) + CDbl(sdbgPrecios.Columns("CANT_RECEPCIONADA").Value) > oLinea.CantPedida + (Left(sdbgPrecios.Columns("DESVIO").Value, Len(sdbgPrecios.Columns("DESVIO").Value) - 1) / 100) * CDbl(sdbgPrecios.Columns("CANT_PED").Value) Then
                         'If CDbl(sdbgPrecios.Columns("CANT_RECIBIDA").Value) + CDbl(sdbgPrecios.Columns("CANT_RECEPCIONADA").Value) > CDbl(sdbgPrecios.Columns("CANT_PED").Value) + (Left(sdbgPrecios.Columns("DESVIO").Value, Len(sdbgPrecios.Columns("DESVIO").Value) - 1) / 100) * CDbl(sdbgPrecios.Columns("CANT_PED").Value) Then
                             oMensajes.SuperadaCantidadPedida
                             Cancel = True
                             m_bError = True
                             Exit Sub
                         Else
                             oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).cantRecepcionada = CDbl(sdbgPrecios.Columns("CANT_RECEPCIONADA").Value)
                             Me.lblImporteTotalAlbaranDato.caption = FormateoNumerico(Me.getTotalAlbaran * oOrdenEntrega.Cambio, "#,##0.00") & " " & NullToStr(oOrdenEntrega.Moneda)
                             sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Value = FormateoNumerico(CDbl(sdbgPrecios.Columns("PRECIO").Value) * CDbl(sdbgPrecios.Columns("CANT_RECEPCIONADA").Value), "#,##0.00")
                         End If
                     Else
                         If (CDbl(sdbgPrecios.Columns("CANT_PED").Value) < 0) And (CDbl(sdbgPrecios.Columns("CANT_RECEPCIONADA").Value) > 0) Then
                             'No puede haber devoluciones y recibir cantidades positivas
                                 oMensajes.SuperadaCantidadPedida
                                 Cancel = True
                                 m_bError = True
                                 Exit Sub
                         End If
                         If CDbl(sdbgPrecios.Columns("CANT_RECIBIDA").Value) + sdbgPrecios.Columns("CANT_RECEPCIONADA").Value < oLinea.importePedido Then
                             oMensajes.SuperadaCantidadPedida
                             Cancel = True
                             m_bError = True
                             Exit Sub
                         Else
                             oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).cantRecepcionada = CDbl(sdbgPrecios.Columns("CANT_RECEPCIONADA").Value)
                             Me.lblImporteTotalAlbaranDato.caption = FormateoNumerico(Me.getTotalAlbaran * oOrdenEntrega.Cambio, "#,##0.00") & " " & NullToStr(oOrdenEntrega.Moneda)
                             sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Value = CDbl(sdbgPrecios.Columns("PRECIO").Value) * (sdbgPrecios.Columns("CANT_RECIBIDA").Value)
                         End If
                    End If
                End If
            End If
        End If
        'Validar Recepcion por importe
        If oLinea.isRecepcionImporte Then
            If sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Value = "" Then
                sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Value = 0
                oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value)).importerecepcionado = 0
                Exit Sub
            End If
            If Not IsNumeric(sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Text) Then
                oMensajes.NoValido sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").caption
                Cancel = True
                m_bError = True
                Exit Sub
            End If
            If CDbl(sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Value) < 0 Then
                Cancel = True
                m_bError = True
                Exit Sub
            End If
            If (CDbl(sdbgPrecios.Columns("IMPORTE_RECIBIDO").Value) + sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Value) > oLinea.importePedido + (Left(sdbgPrecios.Columns("DESVIO").Value, Len(sdbgPrecios.Columns("DESVIO").Value) - 1) / 100) * oLinea.importePedido Then
                oMensajes.SuperadoImportePedido
                Cancel = True
                m_bError = True
                Exit Sub
            Else
                Dim importe As Double
                importe = CDbl(sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Text)
                oLinea.importerecepcionado = importe / oOrdenEntrega.Cambio
                Me.lblImporteTotalAlbaranDato.caption = FormateoNumerico(Me.getTotalAlbaran * oOrdenEntrega.Cambio, "#,##0.00") & " " & NullToStr(oOrdenEntrega.Moneda)
                sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Text = FormateoNumerico(importe, "#,##0.00")
            End If
        End If
        
        'Valido el almacen
        If sdbgPrecios.Columns(sdbgPrecios.col).Name = "ALMACEN" Then
            If sdbgPrecios.Columns("ALMACEN").Value <> "" And sdbgPrecios.Columns("ALM_ID").Value = "" Then
                If Not ValidarAlmacen(sdbgPrecios.Columns("ALMACEN").Value) Then
                    oMensajes.NoValido sdbgPrecios.Columns("ALMACEN").caption
                    Cancel = True
                    m_bError = True
                End If
            End If
        End If
        
        'Valido el activo, contra los cc de las imputaciones si hay y sino contra la empresa del pedido
        If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
            If sdbgPrecios.Columns(sdbgPrecios.col).Name = "ACTIVO" Then
                Set oLinea = oRecepSeleccionada.Lineas.Item(sdbgPrecios.Columns("ID").Value)
                If sdbgPrecios.Columns("ACTIVO").Value = "" Then
                    Set oLinea.Activo = Nothing
                    Set oLinea.Activo = oFSGSRaiz.Generar_CActivo
                Else
                    'Si el activo es distinto lo valido por c�digo
                    If oLinea.Activo.Cod & " - " & oLinea.Activo.Den <> sdbgPrecios.Columns("ACTIVO").Value Then
                        If oLinea.ValidarActivo(oOrdenEntrega.Empresa, IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), sdbgPrecios.Columns("ACTIVO").Value) Then
                            sdbgPrecios.Columns("ACTIVO").Value = oLinea.Activo.Cod & " - " & oLinea.Activo.Den
                        Else
                            oMensajes.NoValida sdbgPrecios.Columns("ACTIVO").caption
                            Cancel = True
                            m_bError = True
                        End If
                    End If
                End If
                Set oLinea = Nothing
            End If
        End If
        
        If sdbgPrecios.Columns(sdbgPrecios.col).Name = "SALIDA_ALMACEN" Then oRecepSeleccionada.Lineas.Item(sdbgPrecios.Columns("ID").Value).SalidaAlmacen = sdbgPrecios.Columns("SALIDA_ALMACEN").Value
    End If
End Sub
''' <summary>
''' Bloquea las columnas ACTIVO y ALMACEN si se cumplen las condiciones
''' </summary>
''' <param name="LastRow">LastRow</param>
''' <param name="LastCol">LastCol</param>
''' <returns></returns>
''' <remarks>Llamada desde: ; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisada por ngo</remarks>
Private Sub sdbgPrecios_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim oLinea As CLineaRecepcion

If sdbgPrecios.col = -1 Then Exit Sub
'Si el art�culo de la linea es no almacenable bloqueo la columna almacen
If sdbgPrecios.Columns(sdbgPrecios.col).Name = "ALMACEN" Then
    Set oLinea = oRecepSeleccionada.Lineas.Item(sdbgPrecios.Columns("ID").Value)
    If Not IsNull(oLinea.ArticuloCod) Then
        If oLinea.ArtAlmacenar = NoAlmacenable Then
            sdbgPrecios.Columns("ALMACEN").Locked = True
        End If
    End If
    Set oLinea = Nothing
End If

'Si el art�culo de la linea es de gasto bloqueo la columna activo
If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
    If sdbgPrecios.Columns(sdbgPrecios.col).Name = "ACTIVO" Then
        Set oLinea = oRecepSeleccionada.Lineas.Item(sdbgPrecios.Columns("ID").Value)
        If Not IsNull(oLinea.ArticuloCod) Then
            If oLinea.ArtConcepto = Gasto Then
                sdbgPrecios.Columns("ACTIVO").Locked = True
            End If
        End If
        Set oLinea = Nothing
    End If
End If

'Si la linea es de planes de entrega automaticos no se permite recepcioones de la linea
If sdbgPrecios.Columns(sdbgPrecios.col).Name = "CANT_RECEPCIONADA" Then
    Set oLinea = oRecepSeleccionada.Lineas.Item(sdbgPrecios.Columns("ID").Value)
    If (oLinea.RecepAutom) Then
        sdbgPrecios.Columns("CANT_RECEPCIONADA").Locked = True
    Else
        sdbgPrecios.Columns("CANT_RECEPCIONADA").Locked = False
    End If
    Set oLinea = Nothing
End If

''' Formateo del grid
formatearGridLock

End Sub
''' <summary>
''' Posiciona el grid sdbgPrecios en la fila que coincide con el parametro sArt y que tiene la columna iCol vacia
''' </summary>
''' <param name="sArt">Den de articulo, segun sale en el grid</param>
''' <param name="iCol">Columna</param>
 ''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo: 0,1</remarks>
Private Sub PosicionarEnGrid(ByVal sArt As String, ByVal iCol As Integer)
Dim vbm As Variant
Dim i As Integer

If sArt = "" Then Exit Sub
If iCol = -1 Then Exit Sub

For i = 0 To sdbgPrecios.Rows - 1
    vbm = sdbgPrecios.AddItemBookmark(i)
    If sdbgPrecios.Columns("DEN").CellValue(vbm) = sArt And sdbgPrecios.Columns(iCol).CellValue(vbm) = "" Then
        sdbgPrecios.col = iCol '9 - ALMACEN, 11 - Activo
        sdbgPrecios.Bookmark = vbm
        Exit For
    End If
Next i
If Me.Visible Then sdbgPrecios.SetFocus
End Sub

'''<summary>Funci�n que realiza las validaciones espec�ficas de cada ERP (mediante llamada a la mapper correspondiente) antes de emitir el pedido</summary>
'''<param name="Empresa">Identificador de la empresa del pedido</param>
'''<param name="OrgCompras">Organizaci�n de compras del pedido</param>
'''<param name="sAlbaran">Albar�n</param>
'''<param name="sObservaciones">Comentarios</param>
'''<returns></returns>
'''<remarks>Llamada desde: FSGSClient.frmRecepcion.cmdAceptar_Click() Tiempo m�ximo: 0,2</remarks>
'''<revision>ngo 26/01/2012</revision>
Private Function ValidacionesERP(ByVal Empresa As Long, ByRef OrgCompras As String, ByVal sAlbaran As String, ByVal sObservaciones As String, ByRef arrIdLineas() As Long, ByRef arrIdOrden() As Long, ByRef arrCantLineas() As Double, ByVal sFecContable As String) As Boolean
Dim Maper As Object
Dim sMapper As String
Dim iNumError As Integer
Dim auxERP As FSGSServer.CERPInt
Dim TextosGS As cTextosGS
Dim Modulo As Integer

If gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Directo) = True Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Aprov) = True Then
    ValidacionesERP = False
    ''En primer lugar obtiene el nombre de la mapper:
    Set auxERP = oFSGSRaiz.Generar_CERPInt
    sMapper = auxERP.ObtenerMapper(OrgCompras, Empresa)
    
    On Error Resume Next
    Set Maper = CreateObject(sMapper & ".clsInPedidosDirectos")
    On Error GoTo 0

    If Not Maper Is Nothing Then
        On Error GoTo NoFnTratarLinea
        Dim sError As String
        If Not Maper.TratarRecepcion(iNumError, sError, sAlbaran, sObservaciones, arrIdLineas, arrIdOrden, arrCantLineas, sFecContable) Then
            If iNumError = -1000 Then
                Exit Function
            End If
            
            Set TextosGS = oFSGSRaiz.Generar_CTextosGS
            Modulo = MapperModuloMensaje.Recepcion
            Call oMensajes.MapperTextosGS(TextosGS.MensajeError(Modulo, iNumError, basPublic.gParametrosInstalacion.gIdioma, sError))
            Exit Function
        Else
           ValidacionesERP = True
        End If
    
NoFnTratarLinea:
    End If
    
    ValidacionesERP = True
Else ''Si no est� en sentido salida, no hay que validar
    ValidacionesERP = True
End If

Set Maper = Nothing
Set auxERP = Nothing
Set TextosGS = Nothing

End Function


Private Sub formatearGridLock()
    Set oLinea = oRecepSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ID").Value))
    If sdbgPrecios.Columns("TIPORECEPCION").Value Then
        sdbgPrecios.Columns("CANT_RECEPCIONADA").Locked = True
        sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Locked = False
    Else
        sdbgPrecios.Columns("CANT_RECEPCIONADA").Locked = False
        sdbgPrecios.Columns("IMPORTE_RECEPCIONADO").Locked = True
    End If
End Sub

Public Sub mostrarOcultarColumnas()
    Dim iLineasRecepcionCantidad As Integer
    For Each oLinea In oRecepSeleccionada.Lineas
        If oLinea.isRecepcionCantidad Then
            iLineasRecepcionCantidad = iLineasRecepcionCantidad + 1
        End If
    Next
    If iLineasRecepcionCantidad = 0 Then
        sdbgPrecios.Columns("PRECIO").Visible = False
        sdbgPrecios.Columns("CANT_PED").Visible = False
        sdbgPrecios.Columns("CANT_RECIBIDA").Visible = False
        sdbgPrecios.Columns("CANT_RECEPCIONADA").Visible = False
    End If
End Sub
Public Function getTotalAlbaran() As Double
    Dim importe As Double
    Dim miLinea As CLineaRecepcion
    importe = 0
    For Each miLinea In oRecepSeleccionada.Lineas
            importe = importe + miLinea.importerecepcionado
    Next
    getTotalAlbaran = importe
End Function

Private Sub sdbgPrecios_RowLoaded(ByVal Bookmark As Variant)
    If sdbgPrecios.Columns("TIPORECEPCION").Value = 1 Then
        sdbgPrecios.Columns("PRECIO").CellStyleSet "gris", sdbgPrecios.Row
        sdbgPrecios.Columns("CANT_PED").CellStyleSet "gris", sdbgPrecios.Row
        sdbgPrecios.Columns("CANT_RECIBIDA").CellStyleSet "gris", sdbgPrecios.Row
        sdbgPrecios.Columns("CANT_RECEPCIONADA").CellStyleSet "gris", sdbgPrecios.Row
    End If
End Sub

