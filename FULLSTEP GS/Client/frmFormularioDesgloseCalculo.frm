VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFormularioDesgloseCalculo 
   BackColor       =   &H00808000&
   Caption         =   "DF�rmula para el campo de xxx"
   ClientHeight    =   3810
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10875
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormularioDesgloseCalculo.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3810
   ScaleWidth      =   10875
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSimular 
      Caption         =   "DSimulaci�n"
      Height          =   315
      Left            =   100
      TabIndex        =   8
      Top             =   3420
      Width           =   1500
   End
   Begin VB.CommandButton cmdAyuda 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   100
      Picture         =   "frmFormularioDesgloseCalculo.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1880
      Width           =   350
   End
   Begin VB.TextBox txtFormula 
      Height          =   285
      Left            =   2160
      TabIndex        =   7
      Top             =   2960
      Width           =   8610
   End
   Begin VB.CommandButton cmdSubir 
      Height          =   300
      Left            =   100
      Picture         =   "frmFormularioDesgloseCalculo.frx":0EE3
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   360
      UseMaskColor    =   -1  'True
      Width           =   350
   End
   Begin VB.CommandButton cmdBajar 
      Height          =   300
      Left            =   100
      Picture         =   "frmFormularioDesgloseCalculo.frx":1225
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   740
      UseMaskColor    =   -1  'True
      Width           =   350
   End
   Begin VB.CommandButton cmdAnyaCampo 
      Height          =   300
      Left            =   100
      Picture         =   "frmFormularioDesgloseCalculo.frx":1567
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1120
      Width           =   350
   End
   Begin VB.CommandButton cmdElimCampo 
      Height          =   300
      Left            =   100
      Picture         =   "frmFormularioDesgloseCalculo.frx":15E9
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1500
      Width           =   350
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTipo 
      Height          =   975
      Left            =   6120
      TabIndex        =   0
      Top             =   0
      Width           =   2295
      DataFieldList   =   "Column 1"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3757
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1720
      _StockProps     =   77
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCalculados 
      Height          =   2415
      Left            =   495
      TabIndex        =   6
      Top             =   360
      Width           =   10300
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   9
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormularioDesgloseCalculo.frx":167B
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID_GRUPO"
      Columns(1).Name =   "ID_GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1429
      Columns(2).Caption=   "ID_CALCULO"
      Columns(2).Name =   "ID_CALCULO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   13619151
      Columns(3).Width=   3863
      Columns(3).Caption=   "NOMBRE"
      Columns(3).Name =   "NOMBRE"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1931
      Columns(4).Caption=   "TIPO"
      Columns(4).Name =   "TIPO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   3
      Columns(5).Width=   8546
      Columns(5).Caption=   "FORMULA"
      Columns(5).Name =   "FORMULA"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID_TIPO"
      Columns(6).Name =   "ID_TIPO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "ID_ORIGEN"
      Columns(7).Name =   "ID_ORIGEN"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "ES_IMPORTE_TOTAL"
      Columns(8).Name =   "ES_IMPORTE_TOTAL"
      Columns(8).Alignment=   2
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      _ExtentX        =   18168
      _ExtentY        =   4260
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblFormula 
      BackColor       =   &H00808000&
      Caption         =   "DF�rmula para el campo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   105
      TabIndex        =   10
      Top             =   3000
      Width           =   2055
   End
   Begin VB.Label lblCampos 
      BackColor       =   &H00808000&
      Caption         =   "DCampos num�ricos de las l�neas de desglose"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   100
      TabIndex        =   9
      Top             =   80
      Width           =   8175
   End
End
Attribute VB_Name = "frmFormularioDesgloseCalculo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_bModif As Boolean
Public g_oCampoDesglose As CFormItem
Public g_oCampo As CFormItem
Public g_sOrigen As String
Private m_bUpdate As Boolean
Private m_bExit As Boolean
'variables de idiomas:
Private m_sIdiTipo(2) As String
Private m_sIdiErrorFormula(12) As String

Private Sub cmdAnyaCampo_Click()
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    
    'A�ade una fila a la grid:
    sdbgCalculados.AddNew
    
    sdbgCalculados.Scroll 0, sdbgCalculados.Rows - sdbgCalculados.Row

    If sdbgCalculados.VisibleRows > 0 Then
        If sdbgCalculados.VisibleRows >= sdbgCalculados.Rows Then
            sdbgCalculados.Row = sdbgCalculados.Rows
        Else
            sdbgCalculados.Row = sdbgCalculados.Rows - (sdbgCalculados.Rows - sdbgCalculados.VisibleRows) - 1
        End If
    End If
            
    sdbgCalculados.Columns("ID_CALCULO").Value = "Y" & g_oCampoDesglose.Grupo.Formulario.ObtenerIDCalculo(g_oCampoDesglose.Grupo.Id)

    If Me.Visible Then sdbgCalculados.SetFocus
End Sub
Private Sub cmdAyuda_Click()
    'Muestra la ayuda:
    If sdbgCalculados.DataChanged = True Then
        sdbgCalculados.Update
    End If
    
    MostrarFormSOLAyudaCalculos oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub
Private Sub cmdBajar_Click()
    Dim i As Integer
    Dim arrValores() As Variant
    Dim arrValores2() As Variant
    
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    If sdbgCalculados.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgCalculados.AddItemRowIndex(sdbgCalculados.SelBookmarks.Item(0)) = sdbgCalculados.Rows - 1 Then Exit Sub
    
    m_bUpdate = False
    
    ReDim arrValores(sdbgCalculados.Columns.Count - 1)
    ReDim arrValores2(sdbgCalculados.Columns.Count - 1)
   
    For i = 0 To sdbgCalculados.Columns.Count - 1
        arrValores(i) = sdbgCalculados.Columns(i).Value
    Next i
    sdbgCalculados.MoveNext
        
    For i = 0 To sdbgCalculados.Columns.Count - 1
        arrValores2(i) = sdbgCalculados.Columns(i).Value
        sdbgCalculados.Columns(i).Value = arrValores(i)
    Next i
    
    sdbgCalculados.MovePrevious
    
    For i = 0 To sdbgCalculados.Columns.Count - 1
        sdbgCalculados.Columns(i).Value = arrValores2(i)
    Next i
        
    sdbgCalculados.SelBookmarks.RemoveAll
    sdbgCalculados.MoveNext
    sdbgCalculados.SelBookmarks.Add sdbgCalculados.Bookmark
        
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
    
    m_bUpdate = True
End Sub
Private Sub cmdElimCampo_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim i As Integer
    Dim aIdentificadores As Variant
    Dim aBookmarks As Variant
    Dim vbm As Variant
    Dim j As Integer

On Error GoTo Cancelar:
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    If sdbgCalculados.Rows = 0 Then Exit Sub
    If sdbgCalculados.SelBookmarks.Count = 0 Then sdbgCalculados.SelBookmarks.Add sdbgCalculados.Bookmark
    
    If CampoEnFormula(sdbgCalculados.Columns("ID_CALCULO").Value) Then
        oMensajes.BloqueoBorrarCampoFormula
        Exit Sub
    End If
    
    If sdbgCalculados.Columns("ID").Value = "" Then
        sdbgCalculados.CancelUpdate
        sdbgCalculados.DataChanged = False
    Else
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCalculados.Columns("NOMBRE").Value)
        If irespuesta = vbNo Then Exit Sub
    
        Screen.MousePointer = vbHourglass
        
        'Elimina de base de datos:
        ReDim aIdentificadores(sdbgCalculados.SelBookmarks.Count)
        ReDim aBookmarks(sdbgCalculados.SelBookmarks.Count)
    
        i = 0
        While i < sdbgCalculados.SelBookmarks.Count
            sdbgCalculados.Bookmark = sdbgCalculados.SelBookmarks(i)
            aIdentificadores(i + 1) = sdbgCalculados.Columns("ID").Value
            aBookmarks(i + 1) = sdbgCalculados.SelBookmarks(i)
            i = i + 1
        Wend
        
        udtTeserror = g_oCampoDesglose.Formulas.EliminarCamposDeBaseDatos(aIdentificadores)
    
        If udtTeserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError udtTeserror
        Else
            'Elimina los campos de la colecci�n:
            For i = 1 To UBound(aIdentificadores)
                 g_oCampoDesglose.Formulas.Remove (CStr(aIdentificadores(i)))
                 basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemEliminar, aIdentificadores(i)
            Next i
            
            'Elimina los campos de la grid:
            For i = 1 To UBound(aBookmarks)
                sdbgCalculados.RemoveItem (sdbgCalculados.AddItemRowIndex(aBookmarks(i)))
            Next i
            
            'Se posiciona en la fila correspondiente:
            If sdbgCalculados.Rows > 0 Then
                If IsEmpty(sdbgCalculados.RowBookmark(sdbgCalculados.Row)) Then
                    sdbgCalculados.Bookmark = sdbgCalculados.RowBookmark(sdbgCalculados.Row - 1)
                Else
                    sdbgCalculados.Bookmark = sdbgCalculados.RowBookmark(sdbgCalculados.Row)
                End If
            End If
            
            'Si se le llama desde la pantalla de desglose elimina la fila de la grid:
            If g_sOrigen = "frmDesglose" Then
                frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Desglose.Remove (CStr(aIdentificadores(1)))
                For j = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                    vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(j)
                    If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("ID").CellValue(vbm) = aIdentificadores(1) Then
                        frmFormularios.g_ofrmDesglose.sdbgCampos.RemoveItem (frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemRowIndex(vbm))
                        Exit For
                    End If
                Next j
            End If
        End If
    End If
    
    sdbgCalculados.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgCalculados.SetFocus

    Screen.MousePointer = vbNormal

    Exit Sub
Cancelar:
    Screen.MousePointer = vbNormal
End Sub
Private Sub cmdSimular_Click()
    Dim teserror As TipoErrorSummit

    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
      
    'Si se ha cambiado la f�rmula del campo la guarda en BD antes de hacer a simulaci�n:
    If NullToStr(g_oCampo.Formula) <> Trim(txtFormula.Text) Then
        If Trim(txtFormula.Text) = "" Then
            g_oCampo.Formula = Null
        Else
            If ComprobarFormula() = False Then
                If Me.Visible Then txtFormula.SetFocus
                Exit Sub
            Else
                g_oCampo.Formula = Trim(txtFormula.Text)
            End If
        End If
        
        'Almacena en BD la f�rmula:
        teserror = g_oCampo.ModificarDefinicionCalculo
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            If Me.Visible Then txtFormula.SetFocus
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemModif, "Id" & g_oCampo.Id
        End If
    End If
    
    'Llama al formulario de las l�neas de desglose:
    frmDesgloseValores.g_sOrigen = "frmFormularioDesgloseCalculo"
    Set frmDesgloseValores.g_oCampoDesglose = g_oCampoDesglose
    Set frmDesgloseValores.g_oCampoFormula = g_oCampo
    
    frmDesgloseValores.Show vbModal
End Sub
Private Sub cmdSubir_Click()
    Dim i As Integer
    Dim arrValores() As Variant
    Dim arrValores2() As Variant
    
    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    If sdbgCalculados.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgCalculados.AddItemRowIndex(sdbgCalculados.SelBookmarks.Item(0)) = 0 Then Exit Sub
    
    m_bUpdate = False
    
    ReDim arrValores(sdbgCalculados.Columns.Count - 1)
    ReDim arrValores2(sdbgCalculados.Columns.Count - 1)
    
    For i = 0 To sdbgCalculados.Columns.Count - 1
        arrValores(i) = sdbgCalculados.Columns(i).Value
    Next i

    sdbgCalculados.MovePrevious
        
    For i = 0 To sdbgCalculados.Columns.Count - 1
        arrValores2(i) = sdbgCalculados.Columns(i).Value
        sdbgCalculados.Columns(i).Value = arrValores(i)
    Next i
    
    sdbgCalculados.MoveNext
    
    For i = 0 To sdbgCalculados.Columns.Count - 1
        sdbgCalculados.Columns(i).Value = arrValores2(i)
    Next i
    
    sdbgCalculados.SelBookmarks.RemoveAll
    sdbgCalculados.MovePrevious
    sdbgCalculados.SelBookmarks.Add sdbgCalculados.Bookmark
        
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
    
    m_bUpdate = True
End Sub
Private Sub Form_Load()
    Dim i As Integer

    Me.Height = 4320
    Me.Width = 11000
    
    CargarRecursos
        
    PonerFieldSeparator Me
    
    If g_bModif = False Then
        cmdAnyaCampo.Visible = False
        cmdAyuda.Visible = False
        cmdBajar.Visible = False
        cmdElimCampo.Visible = False
        cmdSubir.Visible = False
        
        cmdSimular.Left = 120
        sdbgCalculados.Left = 120
        For i = 0 To sdbgCalculados.Columns.Count - 1
            sdbgCalculados.Columns(i).Locked = True
        Next i
        
        txtFormula.Locked = True
    End If
    
    If frmFormularioCampoCalculado.g_sOrigen = "frmSolicitudDetalle" Then
        cmdSimular.Visible = False
    End If
    
    If g_sOrigen = "frmDesglose" Then
        lblFormula.Visible = False
        txtFormula.Visible = False
        cmdSimular.Visible = False
    End If
    
    'Carga los campos calculados del desglose
    CargarCamposCalculados
    
    sdbddTipo.AddItem ""
    sdbgCalculados.Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
    
    If g_sOrigen <> "frmDesglose" Then
        txtFormula.Text = NullToStr(g_oCampo.Formula)
    End If
    
    m_bUpdate = True
End Sub
Private Sub Form_Resize()
    If Me.Width < 1000 Then Exit Sub
    If Me.Height < 2000 Then Exit Sub
    
    If g_bModif = True Then
        sdbgCalculados.Width = Me.Width - 700
    Else
        sdbgCalculados.Width = Me.Width - 325
    End If
    
    sdbgCalculados.Columns("ID_CALCULO").Width = sdbgCalculados.Width * 0.05
    sdbgCalculados.Columns("NOMBRE").Width = sdbgCalculados.Width * 0.15
    sdbgCalculados.Columns("TIPO").Width = sdbgCalculados.Width * 0.1
    sdbgCalculados.Columns("FORMULA").Width = sdbgCalculados.Width * 0.5
    sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Width = sdbgCalculados.Width * 0.15
    
    If g_sOrigen = "frmDesglose" Then
        sdbgCalculados.Height = Me.Height - 950
    Else
        If frmFormularioCampoCalculado.g_sOrigen = "frmSolicitudDetalle" Then
            sdbgCalculados.Height = Me.Height - 1500
        Else
            sdbgCalculados.Height = Me.Height - 1905
        End If
        
        lblFormula.Top = sdbgCalculados.Top + sdbgCalculados.Height + 200
        txtFormula.Top = lblFormula.Top - 40
        cmdSimular.Top = txtFormula.Top + txtFormula.Height + 175
    End If
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DESGLOSE_CALCULADOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        If g_sOrigen <> "frmDesglose" Then
            Me.caption = Ador(0).Value & " " & g_oCampo.Grupo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & " " & g_oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den           '1 F�rmula para el campo de desglose:
        End If
        Ador.MoveNext
        lblCampos.caption = Ador(0).Value '2 Campos num�ricos de las l�neas de desglose
        Ador.MoveNext
        sdbgCalculados.Columns("ID_CALCULO").caption = Ador(0).Value '3 Id.
        Ador.MoveNext
        sdbgCalculados.Columns("NOMBRE").caption = Ador(0).Value '4 Nombre
        Ador.MoveNext
        sdbgCalculados.Columns("TIPO").caption = Ador(0).Value '5 Tipo
        Ador.MoveNext
        sdbgCalculados.Columns("FORMULA").caption = Ador(0).Value '6 F�rmula
        Ador.MoveNext
        lblFormula.caption = Ador(0).Value '7 F�rmula para el campo:
        Ador.MoveNext
        m_sIdiTipo(1) = Ador(0).Value '8 Num�rico
        Ador.MoveNext
        m_sIdiTipo(2) = Ador(0).Value '9 Calculado
        
        For i = 1 To 11
            Ador.MoveNext
            m_sIdiErrorFormula(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        cmdSimular.caption = Ador(0).Value '13 &Simular
        
        Ador.MoveNext
        If g_sOrigen = "frmDesglose" Then
            Me.caption = Ador(0).Value & " " & g_oCampoDesglose.Grupo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & " " & g_oCampoDesglose.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den           '14 F�rmulas para el campo de desglose:
        End If
        
        Ador.MoveNext
        sdbgCalculados.Columns("ES_IMPORTE_TOTAL").caption = Ador(0).Value '23 Importe total l�nea
        Ador.MoveNext
        m_sIdiErrorFormula(12) = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Dim teserror As TipoErrorSummit

    If sdbgCalculados.DataChanged = True Then sdbgCalculados.Update
    
    'Si se ha cambiado la f�rmula del campo:
    If g_sOrigen <> "frmDesglose" Then
        If NullToStr(g_oCampo.Formula) <> Trim(txtFormula.Text) Then
            m_bExit = True
            
            If Trim(txtFormula.Text) = "" Then
                g_oCampo.Formula = Null
            Else
                If ComprobarFormula() = False Then
                    If Me.Visible Then txtFormula.SetFocus
                    Cancel = True
                    m_bExit = False
                    Exit Sub
                Else
                    g_oCampo.Formula = Trim(txtFormula.Text)
                End If
            End If
            
            'Almacena en BD la f�rmula:
            teserror = g_oCampo.ModificarDefinicionCalculo
            If teserror.NumError <> TESnoerror Then
                m_bExit = False
                basErrores.TratarError teserror
                If Me.Visible Then txtFormula.SetFocus
                Exit Sub
            Else
                ''' Registro de acciones
                basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemModif, "Id" & g_oCampo.Id
            End If
            
            m_bExit = False
        End If
    End If
    
    If g_sOrigen <> "frmDesglose" Then
        If Trim(txtFormula.Text) = "" And sdbgCalculados.Rows > 0 Then
            oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
            Cancel = True
            m_bExit = False
            If Me.Visible Then txtFormula.SetFocus
            Exit Sub
        End If
    End If
    
    g_bModif = False
    g_sOrigen = ""
    Set g_oCampoDesglose = Nothing
    Set g_oCampo = Nothing
End Sub
Private Sub GuardarOrdenCampos()
    Dim oCampos As CFormItems
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim vbm As Variant
    Dim oCampo As CFormItem

    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:

    Set oCampos = oFSGSRaiz.Generar_CFormCampos

    For i = 0 To sdbgCalculados.Rows - 1
        vbm = sdbgCalculados.AddItemBookmark(i)

        'Guarda en BD solo el orden de los campos que han cambiado
        If NullToDbl0(g_oCampoDesglose.Formulas.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).OrdenCalculo) <> i + 1 Then
            g_oCampoDesglose.Formulas.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).OrdenCalculo = i + 1

            oCampos.Add sdbgCalculados.Columns("ID").CellValue(vbm), g_oCampoDesglose.Grupo, g_oCampoDesglose.Formulas.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).Denominaciones, , , , , , , , , , , , , , , , , , , , , , , , , , , i + 1
        End If
    Next i

    teserror = oCampos.GuardarOrdenCalculoCampos

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        For Each oCampo In oCampos
            g_oCampoDesglose.Formulas.Item(CStr(oCampo.Id)).FECACT = oCampo.FECACT
        Next
    End If

    Set oCampos = Nothing
End Sub
Private Sub CargarCamposCalculados()
    Dim sCadena As String
    Dim oCampo As CFormItem

    'Carga los campos del desglose que sean num�ricos o de c�lculo:
    sdbgCalculados.RemoveAll
    
    g_oCampoDesglose.CargarCamposCalculadosDesglose
    
    For Each oCampo In g_oCampoDesglose.Formulas
        sCadena = oCampo.Id & Chr(m_lSeparador) & oCampo.Grupo.Id & Chr(m_lSeparador) & oCampo.IdCalculo & Chr(m_lSeparador) & oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
                    
        'Tipo:
        If oCampo.TipoPredef = TipoCampoPredefinido.Calculado Then
            sCadena = sCadena & Chr(m_lSeparador) & m_sIdiTipo(2) & Chr(m_lSeparador) & oCampo.Formula & Chr(m_lSeparador) & "2"
        Else 'Num�rico
            sCadena = sCadena & Chr(m_lSeparador) & m_sIdiTipo(1) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "1"
        End If
        sCadena = sCadena & Chr(m_lSeparador) & Chr(m_lSeparador) & IIf(oCampo.EsImporteTotal, "1", "0")
        sdbgCalculados.AddItem sCadena
    Next
End Sub
Private Sub sdbddTipo_CloseUp()
    If sdbgCalculados.Columns("ID_TIPO").Value = sdbddTipo.Columns("ID").Value Then Exit Sub
    
    sdbgCalculados.Columns("ID_TIPO").Value = sdbddTipo.Columns("ID").Value
    sdbgCalculados.Columns("FORMULA").Value = ""
End Sub
Private Sub sdbddTipo_DropDown()
    ''' * Objetivo: Cargar combo con la coleccion de formas de pago
    If Not g_bModif Then
        sdbddTipo.DroppedDown = False
        Exit Sub
    End If

    sdbddTipo.RemoveAll
    
    If sdbgCalculados.IsAddRow Then
        sdbddTipo.AddItem "1" & Chr(m_lSeparador) & m_sIdiTipo(1)  'Num�rico
    End If
    sdbddTipo.AddItem "2" & Chr(m_lSeparador) & m_sIdiTipo(2)  'Calculado
        
    If sdbddTipo.Rows = 0 Then sdbddTipo.AddItem ""
    
    sdbgCalculados.ActiveCell.SelStart = 0
    sdbgCalculados.ActiveCell.SelLength = Len(sdbgCalculados.ActiveCell.Value)
End Sub
Private Sub sdbddTipo_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    sdbddTipo.DataFieldList = "Column 1"
    sdbddTipo.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbddTipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTipo.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddTipo.Rows - 1
            bm = sdbddTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTipo.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbddTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub sdbgCalculados_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    If Not m_bUpdate Then Exit Sub
    
    If sdbgCalculados.Columns(ColIndex).Name <> "FORMULA" Then Exit Sub
    If sdbgCalculados.Columns(ColIndex).Value = "" Then Exit Sub
    
    If ComprobarFormula(ColIndex) = False Then
        Cancel = True
    End If
End Sub
Private Sub sdbgCalculados_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oCampo As CFormItem
    Dim oCampos As CFormItems
    Dim oIdi As CIdioma
    Dim i As Integer
    Dim vbm As Variant
    
    If Not m_bUpdate Then Exit Sub
    
    If sdbgCalculados.Columns("NOMBRE").Value = "" Then
        oMensajes.NoValido sdbgCalculados.Columns("NOMBRE").caption
        Cancel = True
        Exit Sub
    End If
    
    If sdbgCalculados.Columns("ID_TIPO").Value = "" Then
        oMensajes.NoValido sdbgCalculados.Columns("TIPO").caption
        Cancel = True
        Exit Sub
    ElseIf sdbgCalculados.Columns("ID_TIPO").Value = "2" Then 'Es un campo calculado normal
        If sdbgCalculados.Columns("FORMULA").Value = "" Then
            oMensajes.FaltaFormula
            Cancel = True
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    If sdbgCalculados.IsAddRow Then
        'Es una inserci�n:
        Set oCampo = oFSGSRaiz.Generar_CFormCampo
        
        oCampo.CampoGS = Null
        oCampo.idAtrib = Null
        Set oCampo.CampoPadre = g_oCampoDesglose
        oCampo.EsSubCampo = True
        oCampo.Tipo = TipoNumerico
        oCampo.TipoIntroduccion = IntroLibre
        If sdbgCalculados.Columns("ID_TIPO").Value = "1" Then
            oCampo.TipoPredef = TipoCampoPredefinido.Normal
        Else
            oCampo.TipoPredef = TipoCampoPredefinido.Calculado
        End If
        
        Set oCampo.Grupo = g_oCampoDesglose.Grupo
        
        Set oCampo.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdi In frmFormularios.m_oIdiomas
            oCampo.Denominaciones.Add oIdi.Cod, sdbgCalculados.Columns("NOMBRE").Value
        Next
        
        oCampo.IdCalculo = sdbgCalculados.Columns("ID_CALCULO").Value
        oCampo.OrdenCalculo = sdbgCalculados.Row + 1
        If sdbgCalculados.Columns("ID_TIPO").Value = "2" Then
            oCampo.Formula = sdbgCalculados.Columns("FORMULA").Value
            oCampo.OrigenCalcDesglose = oCampo.CampoPadre.Id
        Else
            oCampo.Formula = Null
            oCampo.OrigenCalcDesglose = Null
        End If
        oCampo.OrigenCalcDesglose = Null
        oCampo.EsImporteTotal = StrToDbl0(sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Value)
        
        Set oIBaseDatos = oCampo
        teserror = oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            sdbgCalculados.CancelUpdate
            If Me.Visible Then sdbgCalculados.SetFocus
            sdbgCalculados.DataChanged = False
        Else
            sdbgCalculados.Columns("ID").Value = oCampo.Id
            
            g_oCampoDesglose.Formulas.Add oCampo.Id, oCampo.Grupo, oCampo.Denominaciones, , oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , , , oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FECACT, , g_oCampoDesglose, True, , , , , , , , oCampo.IdCalculo, oCampo.Formula, oCampo.OrdenCalculo, oCampo.EsImporteTotal, oCampo.OrigenCalcDesglose
            
            ''' Registro de acciones
            basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAnyadir, "Id" & sdbgCalculados.Columns("ID").Value
            
            'si se le llama desde la pantalla de desglose hace la modificaci�n en la grid:
            If g_sOrigen = "frmDesglose" Then
                Set oCampos = oFSGSRaiz.Generar_CFormCampos
                oCampos.Add oCampo.Id, frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, , , oCampo.FECACT, , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, oCampo.EsSubCampo, , , , , , , , oCampo.IdCalculo, oCampo.Formula, oCampo.OrdenCalculo, oCampo.EsImporteTotal, oCampo.OrigenCalcDesglose
                frmFormularios.g_ofrmDesglose.AnyadirCampos oCampos
                Set oCampos = Nothing
                RegistrarAccion ACCDesgloseCampoAnyadir, "Id:" & oCampo.Id
                Set oCampos = Nothing
            End If
        End If
    Else
        'Es una modificaci�n:
        Set oCampo = g_oCampoDesglose.Formulas.Item(CStr(sdbgCalculados.Columns("ID").Value))
        oCampo.Formula = sdbgCalculados.Columns("FORMULA").Value
        oCampo.EsImporteTotal = sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Value
        If sdbgCalculados.Columns("ID_TIPO").Value = "1" Then
            oCampo.TipoPredef = TipoCampoPredefinido.Normal
            oCampo.OrigenCalcDesglose = Null
        Else
            oCampo.TipoPredef = TipoCampoPredefinido.Calculado
            oCampo.OrigenCalcDesglose = oCampo.CampoPadre.Id
        End If
        
        teserror = oCampo.ModificarDefinicionCalculo

        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            sdbgCalculados.CancelUpdate
            If Me.Visible Then sdbgCalculados.SetFocus
            sdbgCalculados.DataChanged = False
        Else
            ''' Registro de acciones
            basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemModif, "Id" & sdbgCalculados.Columns("ID").Value
            
            'si se le llama desde la pantalla de desglose hace la modificaci�n en la grid:
            If g_sOrigen = "frmDesglose" Then
                frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Desglose.Item(CStr(oCampo.Id)).TipoIntroduccion = oCampo.TipoIntroduccion
                For i = 0 To frmFormularios.g_ofrmDesglose.sdbgCampos.Rows - 1
                    vbm = frmFormularios.g_ofrmDesglose.sdbgCampos.AddItemBookmark(i)
                    If frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("ID").CellValue(vbm) = oCampo.Id Then
                        frmFormularios.g_ofrmDesglose.sdbgCampos.Bookmark = vbm
                        frmFormularios.g_ofrmDesglose.sdbgCampos.Columns("TIPO").Value = oCampo.TipoPredef
                        frmFormularios.g_ofrmDesglose.sdbgCampos.Update
                        Exit For
                    End If
                Next i
            End If
        End If
    End If

    Set oCampo = Nothing
    Set oIBaseDatos = Nothing
    
    Screen.MousePointer = vbNormal
End Sub
''' <summary>
''' Mira si existe el campo que se pasa como parametro dentro de las formulas de los campos del objeto(que es lo que se ve en la grid)
''' </summary>
''' <returns>Devulve si el campo est� en una formula o no</returns>
Private Function CampoEnFormula(ByVal sCampo As String) As Boolean
    Dim oCampo As CFormItem
    Dim i As Integer
    Dim s As String
    Dim b As Boolean
    b = False
    For Each oCampo In g_oCampoDesglose.Formulas
        'Sacamos la posicion que tiene en la f�rmula el campo
        i = InStr(NullToStr(oCampo.Formula), sCampo)
        If i > 0 Then
            'Recogemos el siguiemte caractrer de la f�rmula en caso de que lo hubiera
            s = Mid(oCampo.Formula, i + Len(sCampo), 1)
            'Si lo hay miramos a ver si es un caracter numerico y en caso de que lo sea no es el campo a buscar
            'Ej: Busco Y1, encuentro Y11 (que contiene Y1), si el siguiemte caracter es num�rico (1) no es el campo a buscar
            If Not IsNumeric(s) Then
                b = True
                Exit For
            End If
        End If
    Next
    CampoEnFormula = b
End Function

Private Sub sdbgCalculados_Change()
    Dim vbm As Variant
    Dim i As Integer
    Dim oCampo As CFormItem
    Dim sGrupos As String
    
    If sdbgCalculados.col < 0 Then Exit Sub
    
    If sdbgCalculados.Columns(sdbgCalculados.col).Name = "ES_IMPORTE_TOTAL" Then
        If Not sdbgCalculados.Columns("ID").Value = "" Then
            If g_oCampoDesglose.Formulas.Item(sdbgCalculados.Columns("ID").Value).CampoEnUso() Then
                If sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Value = "0" Then
                    sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Value = "-1"
                Else
                    sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Value = "0"
                End If
                Exit Sub
            End If
        End If
        If GridCheckToBoolean(sdbgCalculados.Columns(sdbgCalculados.col).Value) = True Then
            For i = 0 To sdbgCalculados.Rows - 1
                vbm = sdbgCalculados.AddItemBookmark(i)
                For Each oCampo In g_oCampoDesglose.Formulas
                    If oCampo.Id <> sdbgCalculados.Columns("ID").Value Then
                        If oCampo.EsImporteTotal Then
                            If oCampo.CampoEnUso(oCampo.Id) Then
                                sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Value = False
                                oMensajes.BloqueoModifImporteWorkflow
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            Next i
            'Si se hab�a indicado otro campo como importe del workflow se quita:
            For i = 0 To sdbgCalculados.Rows - 1
                vbm = sdbgCalculados.AddItemBookmark(i)
                If sdbgCalculados.Columns("ID").CellValue(vbm) <> sdbgCalculados.Columns("ID").Value Then
                    If GridCheckToBoolean(sdbgCalculados.Columns("ES_IMPORTE_TOTAL").CellValue(vbm)) = True Then
                        sdbgCalculados.Bookmark = vbm
                        sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Value = "0"
                        sdbgCalculados.Update
                        Exit For
                    End If
                End If
            Next i
        End If
    End If
End Sub

Private Sub sdbgCalculados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim b As Boolean
    If g_bModif = False Then
        sdbgCalculados.Columns("TIPO").Style = ssStyleEdit
        Exit Sub
    End If
    
    If sdbgCalculados.IsAddRow Then
        sdbgCalculados.Columns("NOMBRE").Locked = False
    Else
        sdbgCalculados.Columns("NOMBRE").Locked = True
    End If
    If sdbgCalculados.Columns("ID_TIPO").Value = "1" Then
        sdbgCalculados.Columns("FORMULA").Locked = True
    Else
        sdbgCalculados.Columns("FORMULA").Locked = False
    End If
            
    If sdbgCalculados.IsAddRow Then
        sdbgCalculados.Columns("TIPO").Style = ssStyleComboBox
        sdbgCalculados.Columns("TIPO").Locked = False
        sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Locked = False
    Else 'Si el campo esta en uso en una instancia no se puede modificar
        If Not g_oCampo Is Nothing Then
            b = g_oCampo.CampoEnUso(CLng(sdbgCalculados.Columns("ID").Value))
        Else
            b = g_oCampoDesglose.CampoEnUso(CLng(sdbgCalculados.Columns("ID").Value))
        End If
        
        If b Then
            sdbgCalculados.Columns("TIPO").Locked = True
            sdbgCalculados.Columns("TIPO").Style = ssStyleEdit
            sdbgCalculados.Columns("FORMULA").Locked = True
            sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Locked = True
        Else
            sdbgCalculados.Columns("ES_IMPORTE_TOTAL").Locked = False
            If g_oCampoDesglose.Formulas.Item(CStr(sdbgCalculados.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.Calculado _
               Or g_oCampoDesglose.Formulas.Item(CStr(sdbgCalculados.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.Normal Then
                sdbgCalculados.Columns("TIPO").Style = ssStyleComboBox
                sdbgCalculados.Columns("TIPO").Locked = False
            Else
                sdbgCalculados.Columns("TIPO").Style = ssStyleEdit
                sdbgCalculados.Columns("TIPO").Locked = True
            End If
        End If
    End If
End Sub
Private Function ComprobarFormula(Optional ByVal ColIndex As Integer) As Boolean
    Dim sVariables() As String
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim sCaracter As String
    Dim i As Integer
    Dim vbm As Variant
    Dim iHasta As Integer
    
    Set iEq = New USPExpression
    
    If sdbgCalculados.Rows > 0 Then
    
    ReDim sVariables(sdbgCalculados.Rows - 1)
    
    If sdbgCalculados.IsAddRow Then
        iHasta = sdbgCalculados.Rows - 2
    Else
        iHasta = sdbgCalculados.Rows - 1
    End If
    
    For i = 0 To iHasta
        vbm = sdbgCalculados.AddItemBookmark(i)
        sVariables(i) = sdbgCalculados.Columns("ID_CALCULO").CellValue(vbm)
    Next i
    
    If ColIndex <> 0 Then
        lIndex = iEq.Parse(sdbgCalculados.Columns(ColIndex).Value, sVariables, lErrCode)
    Else
        lIndex = iEq.Parse(Trim(txtFormula.Text), sVariables, lErrCode)
    End If

    If lErrCode <> USPEX_NO_ERROR Then
        ' Parsing error handler
        Select Case lErrCode
            Case USPEX_DIVISION_BY_ZERO
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(1))
            Case USPEX_EMPTY_EXPRESSION
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
            Case USPEX_MISSING_OPERATOR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(3))
            Case USPEX_SYNTAX_ERROR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(4))
            Case USPEX_UNKNOWN_FUNCTION
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(5))
            Case USPEX_UNKNOWN_OPERATOR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(6))
            Case USPEX_WRONG_PARAMS_NUMBER
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(7))
            Case USPEX_UNKNOWN_IDENTIFIER
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
            Case USPEX_UNKNOWN_VAR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(9))
            Case USPEX_VARIABLES_NOTUNIQUE
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(10))
            Case USPEX_UNBALANCED_PAREN
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(12))
            Case Else
                sCaracter = Mid(sdbgCalculados.Columns(ColIndex).Value, lIndex + 1)
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(11) & vbCrLf & sCaracter)
        End Select
        
        ComprobarFormula = False
    Else
        ComprobarFormula = True
    End If
    
    Set iEq = Nothing
    Else
        oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
    End If
End Function
Private Sub txtFormula_LostFocus()
    If Trim(txtFormula.Text) = "" Then Exit Sub
    If m_bExit = True Then Exit Sub
    
    If ComprobarFormula() = False Then
        If Me.Visible Then txtFormula.SetFocus
    End If
End Sub
