VERSION 5.00
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmADJAdjuntosItem 
   BackColor       =   &H00808000&
   Caption         =   "DProveedor"
   ClientHeight    =   6990
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8055
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADJAdjuntosItem.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6990
   ScaleWidth      =   8055
   Begin VB.CommandButton cmdAbrirAdjuntos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   7560
      Picture         =   "frmADJAdjuntosItem.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   6615
      UseMaskColor    =   -1  'True
      Width           =   420
   End
   Begin VB.CommandButton cmdSalvarAdjuntos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   7080
      Picture         =   "frmADJAdjuntosItem.frx":01C6
      Style           =   1  'Graphical
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   6615
      UseMaskColor    =   -1  'True
      Width           =   420
   End
   Begin VB.TextBox txtObsAdjun 
      Height          =   1600
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   2320
      Width           =   7875
   End
   Begin VB.TextBox txtObsPrecio 
      Height          =   1600
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   360
      Width           =   7875
   End
   Begin MSComDlg.CommonDialog cmmdAdjun 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin UltraGrid.SSUltraGrid sdbgAdjuntos 
      Height          =   2250
      Left            =   120
      TabIndex        =   3
      Top             =   4280
      Width           =   7875
      _ExtentX        =   13891
      _ExtentY        =   3969
      _Version        =   131072
      GridFlags       =   17040384
      LayoutFlags     =   72613888
      Bands           =   "frmADJAdjuntosItem.frx":0247
      Override        =   "frmADJAdjuntosItem.frx":03CD
      Appearance      =   "frmADJAdjuntosItem.frx":0423
      Caption         =   "sdbgAdjuntos"
   End
   Begin VB.Label lblAdjun 
      BackColor       =   &H00808000&
      Caption         =   "DArchivos adjuntos"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   4040
      Width           =   7695
   End
   Begin VB.Label lblComentAdjun 
      BackColor       =   &H00808000&
      Caption         =   "DComentarios asociados a los archivos adjuntos"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   6
      Top             =   2080
      Width           =   8175
   End
   Begin VB.Label lblComentPrecio 
      BackColor       =   &H00808000&
      Caption         =   "DComentarios asociados al precio"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7935
   End
End
Attribute VB_Name = "frmADJAdjuntosItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oOrigen As Form
Public g_oPrecio As CPrecioItem
Public g_sCaption As String
Public g_bAdjunVisible As Boolean

Private sayFileNames() As String
Private m_sIdiGuardar As String
Private m_sIdiTipoOrig As String
Private m_sIdiElArchivo As String
Private m_sIdiComent As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
Private Sub cmdAbrirAdjuntos_Click()
    Dim oAdjun As CAdjunto
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    
    If sdbgAdjuntos.ActiveRow Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    Else
        If sdbgAdjuntos.Selected.Rows.Count = 0 Then sdbgAdjuntos.Selected.Rows.Add sdbgAdjuntos.ActiveRow
    End If
    
    Set oAdjun = g_oPrecio.Adjuntos.Item(CStr(sdbgAdjuntos.ActiveRow.Cells("ID").Value))
    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & oAdjun.nombre
    sFileTitle = sdbgAdjuntos.ActiveRow.Cells("NOMBRE").Value
    
    If oAdjun.DataSize = 0 Then
        oMensajes.NoValido m_sIdiElArchivo & " " & oAdjun.nombre
        Set oAdjun = Nothing
        Exit Sub
    End If
    
    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing
    
    Set oAdjun.PrecioItem = g_oPrecio
    Set oAdjun.Oferta = g_oPrecio.Oferta
    teserror = oAdjun.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oAdjun = Nothing
        Exit Sub
    End If
    
    oAdjun.LeerAdjuntoOferta oAdjun.Id, oAdjun.DataSize, sFileName
    
    sayFileNames(UBound(sayFileNames)) = sFileName
    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
    
    'Lanzamos la aplicacion
    ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1

Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If

    Set oAdjun = Nothing
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number = 70 Then
        Resume Next
    ElseIf err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntosItem", "cmdAbrirAdjuntos_Click", err, Erl, Me)
        GoTo Cancelar
        Exit Sub
    End If
End Sub

Private Sub cmdSalvarAdjuntos_Click()
    Dim oAdjun As CAdjunto
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    
    If sdbgAdjuntos.ActiveRow Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    Else
        If sdbgAdjuntos.Selected.Rows.Count = 0 Then sdbgAdjuntos.Selected.Rows.Add sdbgAdjuntos.ActiveRow
    End If
    
    Set oAdjun = g_oPrecio.Adjuntos.Item(CStr(sdbgAdjuntos.ActiveRow.Cells("ID").Value))
 
    If oAdjun.DataSize = 0 Then
        oMensajes.NoValido m_sIdiElArchivo & " " & oAdjun.nombre
        Set oAdjun = Nothing
        Exit Sub
    End If
    
    cmmdAdjun.DialogTitle = m_sIdiGuardar
    cmmdAdjun.CancelError = True
    cmmdAdjun.Filter = m_sIdiTipoOrig & "|*.*"
    cmmdAdjun.filename = oAdjun.nombre
    cmmdAdjun.ShowSave

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle
    If sFileTitle = "" Then
        oMensajes.NoValido m_sIdiElArchivo
        Exit Sub
    End If

    Set oAdjun.Oferta = g_oPrecio.Oferta
    Set oAdjun.PrecioItem = g_oPrecio
    teserror = oAdjun.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oAdjun = Nothing
        Exit Sub
    End If
                
    oAdjun.LeerAdjuntoOferta oAdjun.Id, oAdjun.DataSize, sFileName

Cancelar:
    On Error Resume Next

    Set oAdjun = Nothing

    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number = 70 Then
        Resume Next
    ElseIf err.Number <> 0 Then
        If err.Number <> 32755 Then '32755 = han pulsado cancel
            m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntosItem", "cmdSalvarAdjuntos_Click", err, Erl, Me)
        End If
        GoTo Cancelar
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntosItem", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Width = 8175
    m_bDescargarFrm = False
    m_bActivado = False
    If g_bAdjunVisible = True Then
        Me.Height = 7395
        
        lblComentAdjun.Visible = True
        txtObsAdjun.Visible = True
        lblAdjun.Visible = True
        sdbgAdjuntos.Visible = True
        cmdSalvarAdjuntos.Visible = True
        cmdAbrirAdjuntos.Visible = True
    Else
        Me.Height = 4395
        
        lblComentAdjun.Visible = False
        txtObsAdjun.Visible = False
        lblAdjun.Visible = False
        sdbgAdjuntos.Visible = False
        cmdSalvarAdjuntos.Visible = False
        cmdAbrirAdjuntos.Visible = False
    End If
        
    ReDim sayFileNames(0)
    
    CargarRecursos
    
    MostrarAdjuntosYComentarios
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntosItem", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub


Private Sub Form_Resize()

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADJ_ADJUNTOS_ITEM, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        lblAdjun.caption = Ador(0).Value   '1 Archivos adjuntos:
        Ador.MoveNext
        'sdbgAdjuntos.ColumnHeaders(1).Text = Ador(0).Value  '2 Archivo
        Ador.MoveNext
        m_sIdiComent = Ador(0).Value '3 Comentario
        Ador.MoveNext
        m_sIdiElArchivo = Ador.Fields.Item(0).Value '4 Archivo
        Ador.MoveNext
        m_sIdiTipoOrig = Ador.Fields.Item(0).Value  '5 Tipo original
        Ador.MoveNext
        m_sIdiGuardar = Ador.Fields.Item(0).Value   '6 Guardar archivo adjunto
        Ador.MoveNext
        lblComentPrecio.caption = Ador(0).Value  '7  Comentarios asociados al precio:
        Ador.MoveNext
        lblComentAdjun.caption = Ador(0).Value   '8  Comentarios asociados a los archivos adjuntos:
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    Me.caption = g_sCaption
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Arrange()
    'Redimensiona el formulario
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Width < 2000 Then Exit Sub
    If Me.Height < 2000 Then Exit Sub
    
    txtObsPrecio.Width = Me.Width - 300
    txtObsAdjun.Width = Me.Width - 300
    sdbgAdjuntos.Width = Me.Width - 300
    
    If sdbgAdjuntos.Bands.Count > 0 Then
        sdbgAdjuntos.Bands(0).Columns("NOMBRE").Width = sdbgAdjuntos.Width * (30 / 100)
        sdbgAdjuntos.Bands(0).Columns("COMENT").Width = sdbgAdjuntos.Width * (62 / 100)
    End If
    
    If g_bAdjunVisible = True Then
        txtObsPrecio.Height = Me.Height / 4 - 248
    Else
        txtObsPrecio.Height = Me.Height - 900
    End If
    
    txtObsAdjun.Height = txtObsPrecio.Height
    sdbgAdjuntos.Height = Me.Height / 3 - 215
    
    lblComentAdjun.Top = txtObsPrecio.Top + txtObsPrecio.Height + 120
    txtObsAdjun.Top = lblComentAdjun.Top + 240
    
    lblAdjun.Top = txtObsAdjun.Top + txtObsAdjun.Height + 120
    sdbgAdjuntos.Top = lblAdjun.Top + 240
    
    cmdAbrirAdjuntos.Top = sdbgAdjuntos.Top + sdbgAdjuntos.Height + 85
    cmdSalvarAdjuntos.Top = cmdAbrirAdjuntos.Top
    cmdAbrirAdjuntos.Left = Me.Width - 615
    cmdSalvarAdjuntos.Left = Me.Width - 1095
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_sCaption = ""
    Set g_oPrecio = Nothing
    
    g_oOrigen.PopUpDescargarAdjuntosItem
    Set g_oOrigen = Nothing
    
    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntosItem", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub MostrarAdjuntosYComentarios()
    Dim oAdjunto As CAdjunto
    Dim Ador As ADODB.Recordset
    
    'Carga los adjuntos y los comentarios asociados al item
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtObsAdjun.Text = ""
    txtObsPrecio.Text = ""
    
    If g_oPrecio Is Nothing Then Exit Sub
                
    'comentarios del precio:
    Select Case g_oPrecio.Usar
        Case 1
            If Not IsNull(g_oPrecio.Precio1Obs) Then
                txtObsPrecio.Text = g_oPrecio.Precio1Obs
            End If
            
        Case 2
            If Not IsNull(g_oPrecio.Precio2Obs) Then
                txtObsPrecio.Text = g_oPrecio.Precio2Obs
            End If
        
        Case 3
            If Not IsNull(g_oPrecio.Precio3Obs) Then
                txtObsPrecio.Text = g_oPrecio.Precio3Obs
            End If
    End Select
        
    'Carga los adjuntos:
    g_oPrecio.CargarAdjuntos
    
    'Observaciones de los adjuntos:
    txtObsAdjun.Text = NullToStr(g_oPrecio.ObsAdjun)
    
    'Carga los adjuntos:
    Set Ador = New Ador.Recordset
    Ador.Fields.Append "ID", adBigInt
    Ador.Fields.Append "NOMBRE", adVarChar, 200
    Ador.Fields.Append "COMENT", adVarChar, 8000
    Ador.Fields.Append "BTN", adVarChar, 10
    
    Ador.Open
    
    If Not g_oPrecio.Adjuntos Is Nothing Then
        For Each oAdjunto In g_oPrecio.Adjuntos
            Ador.AddNew
            Ador("ID").Value = oAdjunto.Id
            Ador("NOMBRE").Value = oAdjunto.nombre
            Ador("COMENT").Value = NullToStr(oAdjunto.Comentario)
            Ador("BTN").Value = "..."
        Next
    End If
    
    Set sdbgAdjuntos.DataSource = Ador
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntosItem", "MostrarAdjuntosYComentarios", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjuntos_ClickCellButton(ByVal Cell As UltraGrid.SSCell)
    'Accedemos a la pantalla de comentarios de la adjudicación
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If NullToStr(Cell.Row.Cells("COMENT").Value) = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    frmAdjComent.g_sOrigen = "frmADJAdjuntos"
    frmAdjComent.g_bRespetarCombo = True
    frmAdjComent.txtComentario.Text = NullToStr(Cell.Row.Cells("COMENT").Value)
    frmAdjComent.g_bRespetarCombo = False
    frmAdjComent.txtComentario.Locked = True
    frmAdjComent.txtComentario.BackColor = RGB(255, 255, 200)
    frmAdjComent.lstvwEsp.Visible = False
    frmAdjComent.picBotones.Visible = False
    frmAdjComent.caption = Me.caption
    
    Screen.MousePointer = vbNormal
    frmAdjComent.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntosItem", "sdbgAdjuntos_ClickCellButton", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgAdjuntos_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oPrecio.Adjuntos Is Nothing Then Exit Sub
    
    With Layout.Appearances.Add("Gris")
        .BackColor = RGB(223, 223, 223)
    End With
    
    sdbgAdjuntos.Bands(0).Columns("NOMBRE").Activation = UltraGrid.ssActivationActivateOnly
    sdbgAdjuntos.Bands(0).Columns("COMENT").Activation = UltraGrid.ssActivationActivateOnly
    sdbgAdjuntos.Bands(0).Columns("ID").Hidden = True
     
    sdbgAdjuntos.Bands(0).Columns("NOMBRE").Header.caption = m_sIdiElArchivo
    sdbgAdjuntos.Bands(0).Columns("COMENT").Header.caption = m_sIdiComent
    sdbgAdjuntos.Bands(0).Columns("BTN").Header.caption = " "
    
    sdbgAdjuntos.Bands(0).Columns("BTN").Style = UltraGrid.ssStyleButton
    sdbgAdjuntos.Bands(0).Columns("BTN").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
    sdbgAdjuntos.Bands(0).Columns("BTN").Width = 260
    
    sdbgAdjuntos.Override.SelectTypeRow = ssSelectTypeSingle
    sdbgAdjuntos.Override.SelectTypeCol = ssSelectTypeNone
    
    sdbgAdjuntos.ActiveRow = sdbgAdjuntos.GetRow(ssChildRowFirst)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntosItem", "sdbgAdjuntos_InitializeLayout", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


