VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSOLConfigEstados 
   Caption         =   "DEstados de las acciones de la no conformidad"
   ClientHeight    =   4410
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8640
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLConfigEstados.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4410
   ScaleWidth      =   8640
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   8640
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   3855
      Width           =   8640
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   7500
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   120
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   1260
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdAnyadir 
         Caption         =   "&A�adir"
         Height          =   345
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2400
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgEstados 
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8415
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "DCodigo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   20
      _ExtentX        =   14843
      _ExtentY        =   6588
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSOLConfigEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_oSolicitud As CSolicitud
Public g_bModif As Boolean

'Variables privadas
Private m_oIdiomas As CIdiomas
Private m_bModoEdicion As Boolean

'Variables de idiomas
Private m_sIdiConsulta As String
Private m_sIdiEdicion As String
Private m_sIdiDescripcion As String

Private m_oIBAseDatosEnEdicion As IBaseDatos
Private Accion As accionessummit
Private m_oEstadoEnEdicion As CSolicitEstado

Private m_bError As Boolean
Private m_bAnyaError As Boolean
Private m_bModError As Boolean

''' <summary>
''' Carga los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLCONFIG_ESTADOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value & " "    '1 Estados de las acciones de la no conformidad
        Ador.MoveNext
        sdbgEstados.Columns("COD").caption = Ador(0).Value '2 C�digo
        Ador.MoveNext
        m_sIdiDescripcion = Ador(0).Value '4 Descripci�n
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value  '5 Eliminar
        Ador.MoveNext
        cmdAnyadir.caption = Ador(0).Value  ' 6 A�adir
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value  '7 Actualizar
        Ador.MoveNext
        m_sIdiConsulta = Ador(0).Value  '8 Consulta
        Ador.MoveNext
        m_sIdiEdicion = Ador(0).Value   '9 Edici�n
        cmdModoEdicion.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value  '10 Deshacer

        Ador.Close
    
    End If

    Set Ador = Nothing

End Sub

Private Sub cmdAnyadir_Click()
''' * Objetivo: Situarnos en la fila de adicion
    cmdEliminar.Enabled = False
    cmdAnyadir.Enabled = False
    cmdDeshacer.Enabled = True
    
    Accion = ACCSolEstadosAnyadir
    sdbgEstados.Scroll 0, sdbgEstados.Rows - sdbgEstados.Row
    
    If sdbgEstados.VisibleRows > 0 Then
        If sdbgEstados.VisibleRows > sdbgEstados.Rows Then
            sdbgEstados.Row = sdbgEstados.Rows
        Else
            sdbgEstados.Row = sdbgEstados.Rows - (sdbgEstados.Rows - sdbgEstados.VisibleRows) - 1
        End If
    End If
    
    If Me.Visible Then sdbgEstados.SetFocus
End Sub

Private Sub cmdDeshacer_Click()
''' * Objetivo: Deshacer la edicion en el estado actual

    sdbgEstados.CancelUpdate
    sdbgEstados.DataChanged = False

    If Not m_oEstadoEnEdicion Is Nothing Then
        m_oIBAseDatosEnEdicion.CancelarEdicion
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oEstadoEnEdicion = Nothing
    End If

    cmdAnyadir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False

    Accion = ACCSolEstadosConsulta
End Sub

Private Sub cmdEliminar_Click()
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit
Dim vbm As Variant
  
    If sdbgEstados.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    If sdbgEstados.SelBookmarks.Count = 0 Then
        vbm = sdbgEstados.GetBookmark(0)
        sdbgEstados.Bookmark = vbm
        sdbgEstados.SelBookmarks.Add sdbgEstados.Bookmark
    End If

    If sdbgEstados.SelBookmarks.Count = 0 Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminar(sdbgEstados.Columns("COD").Value & " (" & sdbgEstados.Columns(gParametrosInstalacion.gIdioma).Value & ")")

    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set m_oEstadoEnEdicion = g_oSolicitud.Estados.Item(CStr(sdbgEstados.Columns("COD").Value))

    Set m_oIBAseDatosEnEdicion = m_oEstadoEnEdicion
    udtTeserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos()

    If udtTeserror.NumError <> TESnoerror Then
        TratarError udtTeserror
        Screen.MousePointer = vbNormal
        
        If Me.Visible Then sdbgEstados.SetFocus
        Exit Sub

    Else
        g_oSolicitud.Estados.Remove (CStr(sdbgEstados.Columns("COD").Value))
        If sdbgEstados.AddItemRowIndex(sdbgEstados.Bookmark) > -1 Then
            sdbgEstados.RemoveItem (sdbgEstados.AddItemRowIndex(sdbgEstados.Bookmark))
        Else
            sdbgEstados.RemoveItem (0)
        End If
        If IsEmpty(sdbgEstados.GetBookmark(0)) Then
            sdbgEstados.Bookmark = sdbgEstados.GetBookmark(-1)
        Else
            sdbgEstados.Bookmark = sdbgEstados.GetBookmark(0)
        End If
    End If

    Set m_oIBAseDatosEnEdicion = Nothing
    Set m_oEstadoEnEdicion = Nothing


    sdbgEstados.SelBookmarks.RemoveAll
    
    
    If sdbgEstados.Rows = 0 Then
        cmdEliminar.Enabled = False
    Else
        cmdEliminar.Enabled = True
    End If
    
    Screen.MousePointer = vbNormal
    Accion = ACCSolEstadosConsulta

End Sub


Private Sub cmdModoEdicion_Click()
''' * Objetivo: Cambiar entre modo de edicion y modo de consulta
  
    If Not m_bModoEdicion Then
        sdbgEstados.AllowAddNew = True
        sdbgEstados.AllowUpdate = True
        sdbgEstados.AllowDelete = False
        
        cmdModoEdicion.caption = m_sIdiConsulta
        
        cmdRestaurar.Visible = False
        cmdAnyadir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
            
        m_bModoEdicion = True
        
        Accion = ACCSolEstadosConsulta
        
    Else
                
        If sdbgEstados.DataChanged = True Then
            sdbgEstados.Update
            If m_bError Then
                Exit Sub
            End If
        End If

        sdbgEstados.AllowAddNew = False
        sdbgEstados.AllowUpdate = False
        sdbgEstados.AllowDelete = False
                
        cmdAnyadir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdRestaurar.Visible = True
    
        cmdModoEdicion.caption = m_sIdiEdicion
        
        cmdRestaurar_Click
        
        m_bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgEstados.SetFocus
End Sub

Private Sub cmdRestaurar_Click()

    CargarEstados

End Sub

Private Sub Form_Load()
Dim i As Integer
Dim iPosition As Integer
Dim oIdioma As CIdioma

    Me.Width = 8760
    Me.Height = 4920
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    m_bModoEdicion = False
    
    If g_bModif = False Then
        Me.cmdModoEdicion.Visible = False
    End If
    
    Accion = ACCSolEstadosConsulta
    
    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    i = sdbgEstados.Columns.Count
    iPosition = 1
    sdbgEstados.Columns.Add i
    sdbgEstados.Columns(i).Name = gParametrosInstalacion.gIdioma
    sdbgEstados.Columns(i).Style = ssStyleEdit
    sdbgEstados.Columns(i).Position = iPosition
    sdbgEstados.Columns(i).FieldLen = 100
    
    If frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("MULT").Value = "1" Then
        sdbgEstados.Columns(i).caption = m_oIdiomas.Item(CStr(gParametrosInstalacion.gIdioma)).Den
        'A�ade el resto de columnas para todos los idiomas:
        For Each oIdioma In m_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                i = i + 1
                iPosition = iPosition + 1
                sdbgEstados.Columns.Add i
                sdbgEstados.Columns(i).Name = oIdioma.Cod
                sdbgEstados.Columns(i).caption = oIdioma.Den
                sdbgEstados.Columns(i).Style = ssStyleEdit
                sdbgEstados.Columns(i).Position = iPosition
                sdbgEstados.Columns(i).FieldLen = 100
            End If
        Next
    Else
        sdbgEstados.Columns(i).caption = m_sIdiDescripcion
    End If
    
    sdbgEstados.AllowUpdate = False
    
    'Carga los estados de la no conformidad
    CargarEstados
    
End Sub

''' <summary>
''' Al cambiar de tama�o la pantalla, debe cambiar de tama�o los grids y reposionarse todo.
''' </summary>
''' <remarks>Llamada desde: evento de sistema; Tiempo m�ximo: 0</remarks>
Private Sub Form_Resize()
Dim oIdioma As CIdioma
Dim dblWGrid As Double

    If Me.Width < 1500 Then Exit Sub
    If Me.Height < 1300 Then Exit Sub
    
    sdbgEstados.Width = Me.Width - 345
    sdbgEstados.Height = Me.Height - 1185
    picNavigate.Top = sdbgEstados.Top + sdbgEstados.Height
    cmdModoEdicion.Left = Me.Width - 1260
     
    sdbgEstados.Columns("COD").Width = sdbgEstados.Width * 0.15
    dblWGrid = sdbgEstados.Width - sdbgEstados.Columns("COD").Width - 600

    If sdbgEstados.Columns.Count > 2 Then  'si ya se han insertado las columnas de idiomas:
        If frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("MULT").Value = "1" Then
            If Not m_oIdiomas Is Nothing Then
                For Each oIdioma In m_oIdiomas
                    sdbgEstados.Columns(oIdioma.Cod).Width = dblWGrid / m_oIdiomas.Count
                Next
            End If
        Else
            sdbgEstados.Columns(gParametrosInstalacion.gIdioma).Width = dblWGrid
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oIdiomas = Nothing
    Set g_oSolicitud = Nothing
    
    If Not m_oIBAseDatosEnEdicion Is Nothing Then
        m_oIBAseDatosEnEdicion.CancelarEdicion
        Set m_oIBAseDatosEnEdicion = Nothing
    End If
    
    If Not ((IsNull(frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("NUM_ESTADOS").Value) And Me.sdbgEstados.Rows = 0) Or (NullToDbl0(frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("NUM_ESTADOS").Value) = Me.sdbgEstados.Rows)) Then
        frmSOLConfiguracion.m_bRespetar = True
        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("NUM_ESTADOS").Value = Me.sdbgEstados.Rows
        frmSOLConfiguracion.ssSOLConfig.Update
        frmSOLConfiguracion.m_bRespetar = False
    End If
            
End Sub

''' <summary>
''' Carga del grid
''' </summary>
''' <remarks>Llamada desde: Form_Load   cmdRestaurar_Click; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarEstados()
Dim oEstado As CSolicitEstado
Dim sCadena As String
Dim oCol As SSDataWidgets_B.Column

    'Carga la grid con los estados de las acciones:
    Screen.MousePointer = vbHourglass
    sdbgEstados.RemoveAll
    
    g_oSolicitud.CargarEstadosNoConformidad
    
    If Not g_oSolicitud.Estados Is Nothing Then
        For Each oEstado In g_oSolicitud.Estados
            sCadena = oEstado.Codigo & Chr(m_lSeparador) & oEstado.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
            
            If frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("MULT").Value = "1" Then
                'Carga tb el resto de idiomas
                For Each oCol In sdbgEstados.Columns
                    If oCol.Name <> gParametrosInstalacion.gIdioma And oCol.Name <> "COD" Then
                        If Not oEstado.Denominaciones.Item(CStr(oCol.Name)) Is Nothing Then
                            sCadena = sCadena & Chr(m_lSeparador) & oEstado.Denominaciones.Item(CStr(oCol.Name)).Den
                        End If
                    End If
                Next
            End If
            sdbgEstados.AddItem sCadena
        Next
        
        sdbgEstados.MoveFirst
    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgEstados_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgEstados.Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(sdbgEstados.GetBookmark(0)) Then
        sdbgEstados.Bookmark = sdbgEstados.GetBookmark(-1)
    Else
        sdbgEstados.Bookmark = sdbgEstados.GetBookmark(0)
    End If
    If Me.Visible Then sdbgEstados.SetFocus
End Sub

Private Sub sdbgEstados_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la situacion normal
    
    RtnDispErrMsg = 0
    
    If m_bAnyaError = False Then
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
    
    If IsEmpty(sdbgEstados.GetBookmark(0)) Then
        sdbgEstados.Bookmark = sdbgEstados.GetBookmark(-1)
    Else
        sdbgEstados.Bookmark = sdbgEstados.GetBookmark(0)
    End If
End Sub


Private Sub sdbgEstados_AfterUpdate(RtnDispErrMsg As Integer)
''' * Objetivo: Actualizar la fila en edicion
''' * Recibe: Buffer con los datos y bookmark de la fila

    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False Then
        cmdAnyadir.Visible = True
        cmdEliminar.Visible = True
    End If
    
    Set m_oEstadoEnEdicion = Nothing

End Sub

Private Sub sdbgEstados_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim teserror As TipoErrorSummit

    Cancel = False
    m_bError = False
    
    If sdbgEstados.IsAddRow Then Exit Sub

    If m_oEstadoEnEdicion Is Nothing Then Exit Sub
    
    'Si no se ha modificado la columna del c�digo no hace nada:
    If ColIndex <> 0 Then Exit Sub
    
    cmdDeshacer.Enabled = True
    
    If Trim(sdbgEstados.Columns("COD").Value) = "" Then
        oMensajes.NoValido sdbgEstados.Columns("COD").caption
        Cancel = True
        GoTo Salir
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set m_oEstadoEnEdicion.Solicitud = g_oSolicitud
    Set m_oIBAseDatosEnEdicion = m_oEstadoEnEdicion

    teserror = m_oIBAseDatosEnEdicion.CambiarCodigo(sdbgEstados.Columns("COD").Value)
                
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        If Me.Visible Then sdbgEstados.SetFocus
        m_bModError = True
        sdbgEstados.DataChanged = False
        m_bError = True
    Else
        cmdDeshacer.Enabled = False
        g_oSolicitud.Estados.Remove (OldValue)
        g_oSolicitud.Estados.Add g_oSolicitud, m_oEstadoEnEdicion.Codigo, m_oEstadoEnEdicion.Denominaciones, m_oEstadoEnEdicion.FECACT
    End If
        
    Set m_oIBAseDatosEnEdicion = Nothing
    
    If Accion <> ACCSolEstadosModificar Then
        Set m_oEstadoEnEdicion = Nothing
    End If
    
    Screen.MousePointer = vbNormal
    
    Exit Sub

Salir:

    If Me.Visible Then sdbgEstados.SetFocus
    m_bError = True
End Sub

Private Sub sdbgEstados_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim oDen As CMultiidiomas
Dim oIdioma As CIdioma

    Cancel = False
    m_bError = False
    
    If Not sdbgEstados.IsAddRow Then
        If m_oEstadoEnEdicion Is Nothing Then Exit Sub
    End If
    cmdDeshacer.Enabled = True
    
    If Trim(sdbgEstados.Columns("COD").Value) = "" Then
        oMensajes.NoValido Me.sdbgEstados.Columns("COD").caption
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgEstados.Columns(gParametrosInstalacion.gIdioma).Value) = "" Then
        oMensajes.NoValido sdbgEstados.Columns(gParametrosInstalacion.gIdioma).caption
        Cancel = True
        GoTo Salir
    End If
    
    Screen.MousePointer = vbHourglass
    
    'ahora guarda en BD:
    If sdbgEstados.IsAddRow Then
        Set m_oEstadoEnEdicion = oFSGSRaiz.Generar_CSolicitEstado
    End If
    
    m_oEstadoEnEdicion.Codigo = sdbgEstados.Columns("COD").Value
    Set oDen = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In m_oIdiomas
        If frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("MULT").Value = "1" Then
            oDen.Add oIdioma.Cod, sdbgEstados.Columns(oIdioma.Cod).Value
        Else
            oDen.Add oIdioma.Cod, sdbgEstados.Columns(gParametrosInstalacion.gIdioma).Value
        End If
    Next
    Set m_oEstadoEnEdicion.Denominaciones = oDen
    Set m_oEstadoEnEdicion.Solicitud = g_oSolicitud
    
    Set m_oIBAseDatosEnEdicion = m_oEstadoEnEdicion
    
    If sdbgEstados.IsAddRow Then
        teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            m_bAnyaError = True
            cmdDeshacer_Click
            If Me.Visible Then sdbgEstados.SetFocus
            GoTo Salir
        Else
            basSeguridad.RegistrarAccion accionessummit.ACCSolEstadosAnyadir, "Cod:" & m_oEstadoEnEdicion.Codigo
        End If
        
        g_oSolicitud.Estados.Add g_oSolicitud, m_oEstadoEnEdicion.Codigo, m_oEstadoEnEdicion.Denominaciones, m_oEstadoEnEdicion.FECACT
        m_bAnyaError = False
    
    Else
        teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
                
        If teserror.NumError <> TESnoerror Then
          If teserror.NumError = TESDatoNoValido Then
            If Me.Visible Then sdbgEstados.SetFocus
            m_bModError = True
            sdbgEstados.DataChanged = False
            m_bError = True
          
           Else
              TratarError teserror
              If Me.Visible Then sdbgEstados.SetFocus
              m_bModError = True
              sdbgEstados.DataChanged = False
              m_bError = True
          End If
        Else
          ''' Registro de acciones
           basSeguridad.RegistrarAccion ACCSolEstadosModificar, "Id:" & m_oEstadoEnEdicion.Codigo
           cmdDeshacer.Enabled = False
        End If
            
        Set m_oIBAseDatosEnEdicion = Nothing
        
    End If
    
    Accion = ACCSolEstadosConsulta
    
    Screen.MousePointer = vbNormal
    
    Exit Sub

Salir:

    If Me.Visible Then sdbgEstados.SetFocus
    m_bError = True
End Sub


Private Sub sdbgEstados_Change()
    Dim teserror As TipoErrorSummit

    If cmdDeshacer.Enabled = False Then
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    End If

    If Not sdbgEstados.IsAddRow Then
        If sdbgEstados.Col <> 0 Then
            Set m_oEstadoEnEdicion = Nothing
            Set m_oEstadoEnEdicion = g_oSolicitud.Estados.Item(CStr(sdbgEstados.Columns("COD").Value))
        Else
            If m_oEstadoEnEdicion Is Nothing Then Exit Sub
        End If
        
        Set m_oIBAseDatosEnEdicion = m_oEstadoEnEdicion
            
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgEstados.SetFocus
        Else
            If sdbgEstados.Col <> 0 Then
                Accion = ACCSolEstadosModificar
            End If
        End If
    End If
            
        
End Sub
Private Sub sdbgEstados_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

    If KeyCode = vbKeyDelete Then

        Exit Sub

    End If
End Sub

Private Sub sdbgEstados_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
                                    
        If sdbgEstados.DataChanged = False Then
                
            sdbgEstados.CancelUpdate
            sdbgEstados.DataChanged = False
                
            If Not m_oEstadoEnEdicion Is Nothing Then
                m_oIBAseDatosEnEdicion.CancelarEdicion
                Set m_oIBAseDatosEnEdicion = Nothing
                Set m_oEstadoEnEdicion = Nothing
            End If
               
            If Not sdbgEstados.IsAddRow Then
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
            Else
                cmdAnyadir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
            End If
                   
        End If
          
    End If

End Sub


Private Sub sdbgEstados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
''' * Objetivo: Configurar la fila segun sea o no la fila de adicion
    
    If m_bModoEdicion = True Then
        Set m_oEstadoEnEdicion = Nothing
        Set m_oEstadoEnEdicion = g_oSolicitud.Estados.Item(CStr(sdbgEstados.Columns("COD").Value))
    End If
    
    If Not sdbgEstados.IsAddRow Then
        If sdbgEstados.DataChanged = False Then
            cmdAnyadir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
    Else
        If sdbgEstados.DataChanged = True Then cmdDeshacer.Enabled = True
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgEstados.Row) Then
                sdbgEstados.Col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False

    End If
End Sub


