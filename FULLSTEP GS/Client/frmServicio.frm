VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmServicio 
   BackColor       =   &H00808000&
   Caption         =   "DFormulario"
   ClientHeight    =   7785
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9210
   Icon            =   "frmServicio.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7785
   ScaleWidth      =   9210
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "DCancelar"
      Height          =   375
      Left            =   4560
      TabIndex        =   7
      Top             =   7080
      Width           =   1575
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "DAceptar"
      Height          =   375
      Left            =   2400
      TabIndex        =   6
      Top             =   7080
      Width           =   1575
   End
   Begin VB.TextBox txtURL 
      Height          =   285
      Left            =   1560
      TabIndex        =   2
      Top             =   1300
      Width           =   4695
   End
   Begin VB.Frame fraServicio 
      BackColor       =   &H00808000&
      Height          =   6735
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   8535
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   1320
         TabIndex        =   18
         Top             =   360
         Width           =   4635
      End
      Begin TabDlg.SSTab tbGrid 
         Height          =   4695
         Left            =   240
         TabIndex        =   10
         Top             =   1800
         Width           =   8055
         _ExtentX        =   14208
         _ExtentY        =   8281
         _Version        =   393216
         Tabs            =   2
         TabHeight       =   520
         TabCaption(0)   =   "DPar�metros de entrada"
         TabPicture(0)   =   "frmServicio.frx":0CB2
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraGridEntrada"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "DPar�metros de salida"
         TabPicture(1)   =   "frmServicio.frx":0CCE
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraGridSalida"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraGridSalida 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H00FFFFFF&
            Height          =   3615
            Left            =   -74760
            TabIndex        =   15
            Top             =   480
            Width           =   7575
            Begin SSDataWidgets_B.SSDBDropDown sdbddCampoSal 
               Height          =   855
               Left            =   3600
               TabIndex        =   19
               Top             =   2040
               Width           =   3495
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "TIPO"
               Columns(0).Name =   "TIPO"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "COD"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   6165
               Columns(2).Caption=   "DESC"
               Columns(2).Name =   "DESC"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   6174
               _ExtentY        =   1508
               _StockProps     =   77
            End
            Begin SSDataWidgets_B.SSDBDropDown sdbddGrupoSal 
               Height          =   855
               Left            =   480
               TabIndex        =   20
               Top             =   2040
               Width           =   2205
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3784
               Columns(1).Caption=   "DESC"
               Columns(1).Name =   "DESC"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3881
               _ExtentY        =   1508
               _StockProps     =   77
            End
            Begin SSDataWidgets_B.SSDBGrid sdbgParamSal 
               Height          =   3255
               Left            =   240
               TabIndex        =   16
               Top             =   240
               Width           =   7095
               ScrollBars      =   2
               _Version        =   196617
               DataMode        =   2
               Col.Count       =   10
               stylesets.count =   3
               stylesets(0).Name=   "Gris"
               stylesets(0).BackColor=   -2147483633
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmServicio.frx":0CEA
               stylesets(1).Name=   "Normal"
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmServicio.frx":0D06
               stylesets(2).Name=   "ActiveRow"
               stylesets(2).BackColor=   8388608
               stylesets(2).HasFont=   -1  'True
               BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(2).Picture=   "frmServicio.frx":0D22
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   10
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2646
               Columns(1).Caption=   "DPar�metros"
               Columns(1).Name =   "PARAMETROS"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "TIPO"
               Columns(2).Name =   "TIPO"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "DDestino solicitud"
               Columns(3).Name =   "DESTINO"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   11
               Columns(3).FieldLen=   256
               Columns(3).Style=   2
               Columns(4).Width=   3200
               Columns(4).Visible=   0   'False
               Columns(4).Caption=   "ID_GRUPO"
               Columns(4).Name =   "ID_GRUPO"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(5).Width=   3122
               Columns(5).Caption=   "DGrupo"
               Columns(5).Name =   "GRUPO"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(5).Style=   3
               Columns(6).Width=   3200
               Columns(6).Visible=   0   'False
               Columns(6).Caption=   "TIPO_CAMPO"
               Columns(6).Name =   "TIPO_CAMPO"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               Columns(7).Width=   3200
               Columns(7).Visible=   0   'False
               Columns(7).Caption=   "ID_CAMPO"
               Columns(7).Name =   "ID_CAMPO"
               Columns(7).DataField=   "Column 7"
               Columns(7).DataType=   8
               Columns(7).FieldLen=   256
               Columns(8).Width=   2884
               Columns(8).Caption=   "DCampo"
               Columns(8).Name =   "CAMPO"
               Columns(8).DataField=   "Column 8"
               Columns(8).DataType=   8
               Columns(8).FieldLen=   256
               Columns(8).Style=   3
               Columns(9).Width=   1614
               Columns(9).Caption=   "DIndicador Error"
               Columns(9).Name =   "ERROR"
               Columns(9).DataField=   "Column 9"
               Columns(9).DataType=   11
               Columns(9).FieldLen=   256
               Columns(9).Style=   2
               _ExtentX        =   12515
               _ExtentY        =   5741
               _StockProps     =   79
               BackColor       =   -2147483634
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraGridEntrada 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H00FFFFFF&
            Height          =   3615
            Left            =   240
            TabIndex        =   11
            Top             =   480
            Width           =   7575
            Begin SSDataWidgets_B.SSDBDropDown sdbddCampoEnt 
               Height          =   855
               Left            =   4080
               TabIndex        =   12
               Top             =   2040
               Width           =   3500
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "TIPO"
               Columns(0).Name =   "TIPO"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "COD"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   6165
               Columns(2).Caption=   "DESC"
               Columns(2).Name =   "DESC"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   6174
               _ExtentY        =   1508
               _StockProps     =   77
            End
            Begin SSDataWidgets_B.SSDBDropDown sdbddGrupoEnt 
               Height          =   855
               Left            =   840
               TabIndex        =   13
               Top             =   2040
               Width           =   2200
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3784
               Columns(1).Caption=   "DESC"
               Columns(1).Name =   "DESC"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3881
               _ExtentY        =   1508
               _StockProps     =   77
            End
            Begin SSDataWidgets_B.SSDBGrid sdbgParamEnt 
               Height          =   3255
               Left            =   240
               TabIndex        =   14
               Top             =   240
               Width           =   7110
               ScrollBars      =   2
               _Version        =   196617
               DataMode        =   2
               Col.Count       =   11
               stylesets.count =   3
               stylesets(0).Name=   "Gris"
               stylesets(0).BackColor=   -2147483633
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmServicio.frx":0D3E
               stylesets(1).Name=   "Normal"
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmServicio.frx":0D5A
               stylesets(2).Name=   "ActiveRow"
               stylesets(2).ForeColor=   16777215
               stylesets(2).BackColor=   8388608
               stylesets(2).HasFont=   -1  'True
               BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(2).Picture=   "frmServicio.frx":0D76
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   11
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2646
               Columns(1).Caption=   "DPar�metros"
               Columns(1).Name =   "PARAMETROS"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "TIPO"
               Columns(2).Name =   "TIPO"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "CAMPO_EXTERNO"
               Columns(3).Name =   "CAMPO_EXTERNO"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   1429
               Columns(4).Caption=   "DDirecto"
               Columns(4).Name =   "DIRECTO"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   11
               Columns(4).FieldLen=   256
               Columns(4).Style=   2
               Columns(5).Width=   2117
               Columns(5).Caption=   "DValor"
               Columns(5).Name =   "VALOR"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(6).Width=   3200
               Columns(6).Visible=   0   'False
               Columns(6).Caption=   "ID_GRUPO"
               Columns(6).Name =   "ID_GRUPO"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               Columns(7).Width=   2646
               Columns(7).Caption=   "DGrupo"
               Columns(7).Name =   "GRUPO"
               Columns(7).DataField=   "Column 7"
               Columns(7).DataType=   8
               Columns(7).FieldLen=   256
               Columns(7).Style=   3
               Columns(8).Width=   3200
               Columns(8).Visible=   0   'False
               Columns(8).Caption=   "TIPO_CAMPO"
               Columns(8).Name =   "TIPO_CAMPO"
               Columns(8).DataField=   "Column 8"
               Columns(8).DataType=   8
               Columns(8).FieldLen=   256
               Columns(9).Width=   3200
               Columns(9).Visible=   0   'False
               Columns(9).Caption=   "ID_CAMPO"
               Columns(9).Name =   "ID_CAMPO"
               Columns(9).DataField=   "Column 9"
               Columns(9).DataType=   8
               Columns(9).FieldLen=   256
               Columns(10).Width=   2646
               Columns(10).Caption=   "DCampo"
               Columns(10).Name=   "CAMPO"
               Columns(10).DataField=   "Column 10"
               Columns(10).DataType=   8
               Columns(10).FieldLen=   256
               _ExtentX        =   12541
               _ExtentY        =   5741
               _StockProps     =   79
               BackColor       =   -2147483634
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
               Height          =   915
               Left            =   0
               TabIndex        =   21
               Top             =   0
               Width           =   2025
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               stylesets.count =   1
               stylesets(0).Name=   "Normal"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmServicio.frx":0D92
               DividerStyle    =   3
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   5318
               Columns(0).Name =   "NOMBRE"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "ID"
               Columns(1).Name =   "ID"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3572
               _ExtentY        =   1614
               _StockProps     =   77
               BackColor       =   -2147483633
            End
         End
      End
      Begin VB.Frame fraGrid 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H00FFFFFF&
         Height          =   3615
         Left            =   480
         TabIndex        =   9
         Top             =   2400
         Width           =   7575
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcTipo 
         Height          =   285
         Left            =   1320
         TabIndex        =   8
         Top             =   750
         Visible         =   0   'False
         Width           =   2055
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3625
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.CommandButton cmdCargar 
         Caption         =   "DCargar par�metros"
         Height          =   315
         Left            =   6360
         TabIndex        =   5
         Top             =   1200
         Width           =   1815
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgDen 
         Height          =   735
         Left            =   1320
         TabIndex        =   17
         Top             =   360
         Visible         =   0   'False
         Width           =   4935
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2170
         Columns(0).Caption=   "IDI"
         Columns(0).Name =   "IDI"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   14671839
         Columns(1).Width=   5477
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "COD_IDI"
         Columns(2).Name =   "COD_IDI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8705
         _ExtentY        =   1296
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblUrl 
         BackColor       =   &H00808000&
         Caption         =   "DURL"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label lblTipo 
         BackColor       =   &H00808000&
         Caption         =   "DTipo"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   750
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblNombre 
         BackColor       =   &H00808000&
         Caption         =   "DNombre"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmServicio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public lIdFormulario As Long
Public lGrupoFormulario As Long
Public g_Instancia As Long
Public g_sOrigen As String
Public g_oIdiomas As CIdiomas
Public g_multiidioma As Boolean
Public iServAntiguo As Integer
Public lCampoForm As Long

Private m_sCampoGenerico As String
Private m_arCamposGenericos(0 To 10) As String 'Prototipo 3048 Originalmente (2 To 6). Demo Aje / 2015 /54 solo peticionario
Private m_desglose As String
Private m_bMultiIdioma As Boolean
Private m_sIdiForm As String
Private m_sIdiCaption As String
Private m_sIdiNombre As String
Private oServicio As CServicio
Private miParam As Collection
Private errorString As String, errorInd As String, errorFalta As String, errorCampo As String
Private m_sMensaje(1 To 3) As String 'No pasa la validadion de tipo para el Param Entrada: Mensaje identificativo
Private m_sIdiTrue As String 'Multiidioma S�: Param Entrada booolean
Private m_sIdiFalse As String 'Multiidioma No: Param Entrada booolean

Private m_bCancel As Boolean 'No pasa la validadion de tipo el Param Entrada
Private m_bRowLoaded As Boolean 'Ya has puesto la aparencia de los campos tipo Param Entrada: No volver a tocar tipo_campo solo cellstyle. La 1ra vez inicializa tipo_campo.
Private m_bRowIsNew As Boolean 'Si estas cargando un campo del formulario o creando un campo del formulario

''' <summary>
''' Carga la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    CargarRecursos
    PonerFieldSeparator Me
    Set oServicio = oFSGSRaiz.Generar_CServicio
    m_bMultiIdioma = g_multiidioma
    sdbddGrupoEnt.AddItem 0 & Chr(m_lSeparador) & ""
    sdbddCampoEnt.AddItem 0 & Chr(m_lSeparador) & ""
    sdbddGrupoSal.AddItem 0 & Chr(m_lSeparador) & ""
    sdbddCampoSal.AddItem 0 & Chr(m_lSeparador) & ""
    sdbddValor.AddItem ""
    sdbddValor.Columns(0).Width = Me.sdbgParamEnt.Columns("VALOR").Width
    Select Case g_sOrigen
        Case "frmFormularios"
            Me.caption = m_sIdiForm & " " & frmFormularios.g_oFormSeleccionado.Den & " " & m_sIdiCaption
        Case "frmDesglose"
            Me.caption = NullToStr(frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Denominaciones.Item(oUsuarioSummit.idioma).Den) & " " & m_sIdiCaption
    End Select
    CargarDatos
    If g_Instancia <> 0 Then
        txtNombre.Locked = True
        sdbgDen.Enabled = False
        txtURL.Locked = True
        cmdCargar.Enabled = False
        sdbgParamEnt.Enabled = False
        sdbgParamSal.Enabled = False
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdCancelar_Click()
    Set miParam = Nothing
    Unload Me
End Sub

''' <summary>
''' Proporcionada una url de servicio, cargar sus parametros
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdCargar_Click()
    Dim linkUrl As String
            
    linkUrl = txtURL.Text
    
    'Llamada al webservice
    Set miParam = DefinicionWebService(linkUrl)
    If miParam Is Nothing Then
        Exit Sub
    End If
    
    m_bRowIsNew = True
    m_bRowLoaded = False
    
    'Saco los parametros
    CargarGridParametros
    
    m_bRowLoaded = True
End Sub

Private Function DefinicionWebService(linkUrl As String) As Collection
    
    Dim ParamServicio(4) As String '0:Id/1:numParam/2:sentido(0-entrada,1-salida)/3:nombre/4:tipo
    Dim oParams As Collection
    Dim strUrl As String, strWsdl As String, metodo As String
    Dim Num As Integer, j As Integer, icontador As Integer, noGrabar As Integer, inum As Integer
    Dim numParametro As Integer
    Dim o As Object
    Dim Parametros As Object
    Dim parametro As Object
    Dim tipoParam As String, nombreParam As String, claseSalida As String
    
    Dim cliente As System.WebClient
    Set cliente = New System.WebClient
    Dim contenidoDefinicionServicio As String
    Dim contenidoEnXml As Object
    Set contenidoEnXml = CreateObject("MSXML2.DOMDocument")
    
    Num = InStr(linkUrl, ".asmx")
    If Num = 0 Then
        oMensajes.WebServiceIncorrecto
        Exit Function
    End If
    strUrl = Left$(linkUrl, Num + 4)
    'Si la url esta mal metida (sin especificar el metodo del webservice) no le dejo continuar
    If Len(linkUrl) > (Num + 5) Then
        metodo = Right$(linkUrl, Len(linkUrl) - (Num + 5))
    Else
        oMensajes.WebServiceIncorrecto
        Exit Function
    End If
    
    strWsdl = strUrl & "?wsdl"
    contenidoDefinicionServicio = cliente.DownloadString(strWsdl)
    contenidoEnXml.loadXML (contenidoDefinicionServicio)
    Set o = contenidoEnXml.documentElement.selectNodes("//wsdl:definitions/wsdl:types/s:schema/s:element")
    
    inum = 0
    Set oParams = New Collection
    For icontador = 0 To o.Length - 1
        'param�tros de entrada
        If o.Item(icontador).Attributes(0).Value = metodo Then
            Set Parametros = o.Item(icontador).selectNodes("s:complexType/s:sequence/s:element")
            For numParametro = 0 To Parametros.Length - 1
                Set parametro = Parametros.Item(numParametro)
                For j = 0 To parametro.Attributes.Length - 1
                    If parametro.Attributes(j).Name = "name" Then
                        nombreParam = parametro.Attributes(j).Value
                    End If
                    If parametro.Attributes(j).Name = "type" Then
                        tipoParam = Right$(parametro.Attributes(j).Value, Len(parametro.Attributes(j).Value) - 2)
                    End If
                Next
                ParamServicio(0) = CStr(inum)
                ParamServicio(1) = CStr(numParametro)
                ParamServicio(2) = CStr(0)
                ParamServicio(3) = nombreParam
                ParamServicio(4) = tipoParam
                oParams.Add ParamServicio, ParamServicio(0)
                inum = inum + 1
            Next
        End If
        'param�tros de salida
        If o.Item(icontador).Attributes(0).NodeValue = (metodo & "Response") Then
            Set Parametros = o.Item(icontador).selectNodes("s:complexType/s:sequence/s:element")
            For numParametro = 0 To Parametros.Length - 1
                Set parametro = Parametros.Item(numParametro)
                For j = 0 To parametro.Attributes.Length - 1
                    If parametro.Attributes(j).Name = "type" Then
                        noGrabar = 0
                        Num = InStr(parametro.Attributes(j).Value, "tns:")
                        If Num = 0 Then
                            tipoParam = Right$(parametro.Attributes(j).Value, Len(parametro.Attributes(j).Value) - 2)
                        Else
                            claseSalida = Right$(parametro.Attributes(j).Value, Len(parametro.Attributes(j).Value) - 4)
                            noGrabar = 1
                            Exit For
                        End If
                    End If
                    If parametro.Attributes(j).Name = "name" Then
                        Num = InStr(parametro.Attributes(j).Value, "Result")
                        nombreParam = Left$(parametro.Attributes(j).Value, Num - 1)
                    End If
                Next
                If noGrabar = 0 Then
                    ParamServicio(0) = CStr(inum)
                    ParamServicio(1) = CStr(numParametro)
                    ParamServicio(2) = CStr(1)
                    ParamServicio(3) = nombreParam
                    ParamServicio(4) = tipoParam
                    oParams.Add ParamServicio, ParamServicio(0)
                    inum = inum + 1
                End If
            Next
        End If
    Next
    Set o = contenidoEnXml.documentElement.selectNodes("//wsdl:definitions/wsdl:types/s:schema/s:complexType")
    For icontador = 0 To o.Length - 1
        'param�tros de salida (dentro de un objeto)
        If o.Item(icontador).Attributes(0).NodeValue = claseSalida Then
            Set Parametros = o.Item(icontador).selectNodes("s:sequence/s:element")
            For numParametro = 0 To Parametros.Length - 1
                Set parametro = Parametros.Item(numParametro)
                For j = 0 To parametro.Attributes.Length - 1
                    If parametro.Attributes(j).Name = "type" Then
                        tipoParam = Right$(parametro.Attributes(j).Value, Len(parametro.Attributes(j).Value) - 2)
                    End If
                    If parametro.Attributes(j).Name = "name" Then
                        nombreParam = parametro.Attributes(j).Value
                    End If
                Next
                ParamServicio(0) = CStr(inum)
                ParamServicio(1) = CStr(numParametro)
                ParamServicio(2) = CStr(1)
                ParamServicio(3) = nombreParam
                ParamServicio(4) = tipoParam
                oParams.Add ParamServicio, ParamServicio(0)
                inum = inum + 1
            Next
        End If
    Next
    Set DefinicionWebService = oParams
End Function

''' <summary>
''' Grabar el servicio
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
 
    Dim udtTeserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oIdioma As CIdioma
    Dim oDenominaciones As CMultiidiomas
    Dim oCampo As CFormItem
    Dim i As Integer
    Dim vbm As Variant
    Dim oCampos As CFormItems
    Dim sValor As String
    
    Dim errorEncontrado As Boolean
    Dim oParametros As CParametrosExternos
    
    LockWindowUpdate Me.hWnd
    
    'Si hay cambios sin guardar
    If Me.sdbgParamEnt.DataChanged = True Then
        sdbgParamEnt.Update
    End If
    If m_bCancel Then
        LockWindowUpdate 0&
        Exit Sub
    End If
    
    'Compruebo que todo est� rellenado
    If g_Instancia <> 0 Then
        Set miParam = Nothing
        LockWindowUpdate 0&
        Unload Me
        Exit Sub
    End If
    If m_bMultiIdioma = True Then
        'Tiene que introducir la denominaci�n por lo menos en el idioma de la aplicaci�n:
        sdbgDen.MoveFirst
        For i = 0 To sdbgDen.Rows - 1
            vbm = sdbgDen.AddItemBookmark(i)
            If sdbgDen.Columns("DEN").CellValue(vbm) = "" Then
                LockWindowUpdate 0&
                oMensajes.NoValido sdbgDen.Columns("IDI").CellValue(vbm)
                Exit Sub
            End If
        Next i
    Else
        If Trim(txtNombre.Text) = "" Then
            oMensajes.NoValido m_sIdiNombre
            If Me.Visible Then txtNombre.SetFocus
            LockWindowUpdate 0&
            Exit Sub
        End If
    End If
    If txtURL.Text = "" Then
        oMensajes.NoValido lblURL.caption
        If Me.Visible Then txtURL.SetFocus
        LockWindowUpdate 0&
        Exit Sub
    End If
    errorEncontrado = False
    sdbgParamSal.Update
    For i = 0 To sdbgParamSal.Rows - 1
        vbm = sdbgParamSal.AddItemBookmark(i)
        If sdbgParamSal.Columns("ERROR").CellValue(vbm) = True Then
            errorEncontrado = True
        ElseIf sdbgParamSal.Columns("ERROR").CellValue(vbm) = False Then
            If sdbgParamSal.Columns("ID_CAMPO").CellValue(vbm) = "" Then
                oMensajes.NoValido (errorCampo & " " & sdbgParamSal.Columns("PARAMETROS").CellValue(vbm))
                LockWindowUpdate 0&
                Exit Sub
            End If
        End If
    Next i
    If errorEncontrado = False Then
        oMensajes.NoValido errorFalta
        LockWindowUpdate 0&
        Exit Sub
    End If
    
    'Inserto el servicio en la BD
    Screen.MousePointer = vbHourglass
    
    'Cojo la URL del servicio
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    oCampo.Tipo = TipoCampoPredefinido.Servicio
    oCampo.Subtipo = TipoTextoMedio
    Set oServicio = oFSGSRaiz.Generar_CServicio
    oServicio.URL = txtURL.Text
    udtTeserror = oServicio.AnyadirServicio(iServAntiguo)
    'Cojo el nombre del servicio
    oCampo.Servicio = oServicio.Id
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    
    If m_bMultiIdioma Then
        For Each oIdioma In g_oIdiomas
            For i = 0 To sdbgDen.Rows - 1
                vbm = sdbgDen.AddItemBookmark(i)
                If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                    oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                    Exit For
                End If
            Next i
        Next
    Else
        oDenominaciones.Add oUsuarioSummit.idioma, Trim(txtNombre.Text)
    End If
    
    Set oServicio.Denominaciones = oDenominaciones
    Set oCampo.Denominaciones = oDenominaciones
    udtTeserror = oServicio.AnyadirDenAServicio(iServAntiguo, oServicio.Id)
    'Cojo los parametros del servicio
    Set oParametros = oFSGSRaiz.Generar_CParametrosExternos
    sdbgParamEnt.MoveFirst
    For i = 0 To sdbgParamEnt.Rows - 1
        vbm = sdbgParamEnt.AddItemBookmark(i)
        If CInt(sdbgParamEnt.Columns("TIPO").CellValue(vbm)) = 4 Then
            sValor = IIf(sdbgParamEnt.Columns("VALOR").CellValue(vbm) = m_sIdiTrue, 1, 0)
        Else
            sValor = sdbgParamEnt.Columns("VALOR").CellValue(vbm)
        End If
        oParametros.Add 1, Trim(sdbgParamEnt.Columns("PARAMETROS").CellValue(vbm)), CInt(sdbgParamEnt.Columns("TIPO").CellValue(vbm)), _
                         CLng(sdbgParamEnt.Columns("ID_GRUPO").CellValue(vbm)), CInt(sdbgParamEnt.Columns("TIPO_CAMPO").CellValue(vbm)), _
                         CLng(sdbgParamEnt.Columns("ID_CAMPO").CellValue(vbm)), CBool(sdbgParamEnt.Columns("DIRECTO").CellValue(vbm)), , _
                         sValor, CInt(sdbgParamEnt.Columns("ID").CellValue(vbm))
    Next i
    sdbgParamSal.MoveFirst
    For i = 0 To sdbgParamSal.Rows - 1
        vbm = sdbgParamSal.AddItemBookmark(i)
        oParametros.Add 2, Trim(sdbgParamSal.Columns("PARAMETROS").CellValue(vbm)), CInt(sdbgParamSal.Columns("TIPO").CellValue(vbm)), _
                            CLng(sdbgParamSal.Columns("ID_GRUPO").CellValue(vbm)), CInt(sdbgParamSal.Columns("TIPO_CAMPO").CellValue(vbm)), _
                            CLng(sdbgParamSal.Columns("ID_CAMPO").CellValue(vbm)), , CBool(sdbgParamSal.Columns("ERROR").CellValue(vbm)), , _
                            CInt(sdbgParamSal.Columns("ID").CellValue(vbm))
    Next i
    Set oServicio.Parametros = oParametros
    If g_sOrigen = "frmFormularios" Then
        Set oCampo.Grupo = frmFormularios.g_oGrupoSeleccionado
    Else
        Set oCampo.Grupo = frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Grupo
    End If
    oCampo.TipoPredef = TipoCampoPredefinido.Servicio
    If g_sOrigen = "frmDesglose" Then
        Set oCampo.CampoPadre = frmFormularios.g_ofrmDesglose.g_oCampoDesglose
        oCampo.EsSubCampo = True
    Else
        oCampo.EsSubCampo = False
    End If
    If lCampoForm = 0 Then
        Set oIBaseDatos = oCampo
        udtTeserror = oIBaseDatos.AnyadirABaseDatos
        udtTeserror = oServicio.AnyadirParamAServicio(iServAntiguo, oServicio.Id, oCampo.Id)
        
        If udtTeserror.NumError = TESnoerror Then
            Set oCampos = oFSGSRaiz.Generar_CFormCampos
            If g_sOrigen = "frmFormularios" Then
                'Lo a�ade a la colecci�n y grid de formularios
                oCampos.Add oCampo.Id, frmFormularios.g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, , , oCampo.FECACT, , , , , , , , , , , , , , , , oCampo.MostrarPOPUP, , , , , oCampo.MaxLength, , , , , , , , , , oCampo.RecordarValores, oCampo.IDListaCampoPadre, , , oCampo.Servicio
                frmFormularios.AnyadirCampos oCampos
                Set oCampos = Nothing
                RegistrarAccion ACCFormItemAnyadir, "Id:" & oCampo.Id
            Else
                'Lo a�ade a la colecci�n y grid de desglose:
                oCampos.Add oCampo.Id, frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Grupo, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, , , oCampo.FECACT, , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, oCampo.EsSubCampo, , , , , , , , , , , , , , , , , , oCampo.MaxLength, , , , , , , , , , , oCampo.IDListaCampoPadre, , , oCampo.Servicio
                frmFormularios.g_ofrmDesglose.AnyadirCampos oCampos
                Set oCampos = Nothing
                RegistrarAccion ACCDesgloseCampoAnyadir, "Id:" & oCampo.Id
            End If
        Else
            LockWindowUpdate 0&
            Screen.MousePointer = vbNormal
            TratarError udtTeserror
            Exit Sub
        End If
    Else
        udtTeserror = oServicio.ActualizarFormCampo(lCampoForm)
        udtTeserror = oServicio.AnyadirParamAServicio(iServAntiguo, oServicio.Id, , lCampoForm)
        Dim oMulti As CMultiidioma
        If g_sOrigen = "frmFormularios" Then
            frmFormularios.g_bUpdate = False
            For Each oMulti In oDenominaciones
                frmFormularios.sdbgCampos.Columns(oMulti.Cod).Value = oMulti.Den
            Next
            frmFormularios.sdbgCampos.Update
            frmFormularios.g_bUpdate = True
            RegistrarAccion ACCFormItemModif, "Id:" & lCampoForm
        End If
        If udtTeserror.NumError <> TESnoerror Then
            LockWindowUpdate 0&
            Screen.MousePointer = vbNormal
            TratarError udtTeserror
            Exit Sub
        End If
    End If
    
    Set oCampo = Nothing
    frmFormularios.g_Accion = ACCFormularioCons
    Set oIBaseDatos = Nothing
    Set oDenominaciones = Nothing
    
    LockWindowUpdate 0&
    
    Screen.MousePointer = vbNormal
        
    Unload Me
End Sub
 
''' <summary>
''' Establece el aspecto del grid Param Entrada
''' </summary>
''' <remarks>Llamada desde: sdbgParamEnt_RowLoaded ; Tiempo m�ximo: 0,2</remarks>
Private Sub comprobarDirecto()
    With sdbgParamEnt
        If sdbgParamEnt.Columns("DIRECTO").Value = True Then
            DirectoColumnasSi

        ElseIf sdbgParamEnt.Columns("CAMPO_EXTERNO").Value = True Or _
        sdbgParamEnt.Columns("CAMPO_EXTERNO").Value = -1 Or _
        sdbgParamEnt.Columns("CAMPO_EXTERNO").Value = "True" Then
            .Columns("PARAMETROS").Locked = True
            .Columns("PARAMETROS").CellStyleSet "Gris", sdbgParamEnt.Row
    
            .Columns("DIRECTO").Locked = False
            .Columns("DIRECTO").CellStyleSet "Normal", sdbgParamEnt.Row
            If Not m_bRowLoaded Then
                If m_bRowIsNew Then
                    .Columns("TIPO_CAMPO").Value = TipoCampo.CampoDeFormulario * 1000
                Else
                    .Columns("TIPO_CAMPO").Value = TipoCampo.CampoDeFormulario
                End If
            End If
            
            .Columns("VALOR").Locked = True
            .Columns("VALOR").CellStyleSet "Gris", sdbgParamEnt.Row
        
            .Columns("GRUPO").Locked = False
            .Columns("GRUPO").CellStyleSet "Normal", sdbgParamEnt.Row
            
            If .Columns("TIPO_CAMPO").Value = TipoCampo.CampoDeFormulario * 1000 Then
                .Columns("CAMPO").Locked = True
                .Columns("CAMPO").CellStyleSet "Gris", sdbgParamEnt.Row
            Else
                .Columns("CAMPO").Locked = False
                .Columns("CAMPO").CellStyleSet "Normal", sdbgParamEnt.Row
            End If

        Else
            .Columns("VALOR").Value = ""
            .Columns("VALOR").Locked = True
            .Columns("VALOR").DropDownHwnd = 0
            .Columns("VALOR").CellStyleSet "Gris", sdbgParamEnt.Row
            If Not m_bRowLoaded Then
                .Columns("TIPO_CAMPO").Value = TipoCampo.CampoDeFormulario
            End If
            
            .Columns("GRUPO").Locked = False
            .Columns("GRUPO").CellStyleSet "Normal", sdbgParamEnt.Row
            
            .Columns("CAMPO").Locked = False
            .Columns("CAMPO").CellStyleSet "Normal", sdbgParamEnt.Row

        End If
    End With
    
End Sub

''' <summary>
''' Descarga la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
    m_bRowLoaded = False
    m_bRowIsNew = False
End Sub

Private Sub sdbddCampoEnt_CloseUp()
    If sdbgParamEnt.Columns("CAMPO").Value = "" Then Exit Sub
    sdbgParamEnt.Columns("TIPO_CAMPO").Value = sdbddCampoEnt.Columns(0).Value
    sdbgParamEnt.Columns("ID_CAMPO").Value = sdbddCampoEnt.Columns(1).Value
    sdbgParamEnt.Columns("CAMPO").Value = sdbddCampoEnt.Columns(2).Value
End Sub

Private Sub sdbddCampoEnt_DropDown()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim cDesglose As CFormItem
    Dim sDenCampo As String
    Dim iTipoCampo As TipoCampo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbddCampoEnt.RemoveAll
    If iTipoCampo = TipoCampo.CampoDeFormulario Then
    End If
    If sdbgParamEnt.Columns("ID_GRUPO").Value <> "" Then
        If CLng(sdbgParamEnt.Columns("ID_GRUPO").Value) > 0 Then
            iTipoCampo = TipoCampo.CampoDeFormulario
        End If
    End If
    If iTipoCampo = TipoCampo.CampoDeFormulario Then
        Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
        oGrupo.Id = sdbgParamEnt.Columns("ID_GRUPO").Value
        Set oGrupo.Formulario = oFSGSRaiz.Generar_CFormulario 'Lo pongo para enga�ar al objeto y que cargue los datos de FORM_CAMPO
        oGrupo.CargarTodosLosCampos
        For Each oCampo In oGrupo.Campos
            If g_sOrigen = "frmDesglose" And oCampo.Tipo = TipoDesglose And sdbgParamEnt.Columns("ID_GRUPO").Value = lGrupoFormulario Then
                frmFormularios.g_ofrmDesglose.g_oCampoDesglose.CargarDesglose
                For Each cDesglose In frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Desglose
                    If tipoGeneral(cDesglose.Tipo) = sdbgParamEnt.Columns("TIPO").Value Then 'Cargo s�lo los del mismo tipo que el par�metro
                        For i = 1 To cDesglose.Denominaciones.Count
                        If cDesglose.Denominaciones.Item(i).Cod = oUsuarioSummit.idioma Then
                            sDenCampo = "(" & m_desglose & ") " & cDesglose.Denominaciones.Item(i).Den
                            Exit For
                        End If
                    Next
                    sdbddCampoEnt.AddItem TipoCampo.CampoDeFormulario & Chr(m_lSeparador) & cDesglose.Id & Chr(m_lSeparador) & sDenCampo
                    End If
                Next
            Else
                If tipoGeneral(oCampo.Tipo) = sdbgParamEnt.Columns("TIPO").Value And oCampo.CampoPadre Is Nothing Then
                    For i = 1 To oCampo.Denominaciones.Count
                        If oCampo.Denominaciones.Item(i).Cod = oUsuarioSummit.idioma Then
                            sDenCampo = oCampo.Denominaciones.Item(i).Den
                            Exit For
                        End If
                    Next
                    sdbddCampoEnt.AddItem TipoCampo.CampoDeFormulario & Chr(m_lSeparador) & oCampo.Id & Chr(m_lSeparador) & sDenCampo
                End If
            End If
        Next
    Else
        Select Case sdbgParamEnt.Columns("TIPO").Value
            Case "4" 'boolean
            Case "3" 'dateTime
                sdbddCampoEnt.AddItem TipoCampo.FechaInicio & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(5) 'Fecha de inicio
                sdbddCampoEnt.AddItem TipoCampo.FechaExpiracion & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(6) 'Fecha de expiracion
            Case "2"  '"int", "double", "long", "decimal", "short", "unsignedByte"
                sdbddCampoEnt.AddItem TipoCampo.Contacto & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(3) 'Contacto
                sdbddCampoEnt.AddItem TipoCampo.Empresa & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(4) 'Empresa
                sdbddCampoEnt.AddItem TipoCampo.MostrarAlerta & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(7) 'Mostrar alerta
                sdbddCampoEnt.AddItem TipoCampo.EnviarMail & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(8) 'Enviar email
                sdbddCampoEnt.AddItem TipoCampo.ImporteDesde & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(9) 'Importe desde
                sdbddCampoEnt.AddItem TipoCampo.ImporteHasta & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(10) 'Importe hasta
            Case Else '"string"
                sdbddCampoEnt.AddItem TipoCampo.Peticionario & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(0) 'Peticionario
                sdbddCampoEnt.AddItem TipoCampo.Moneda & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(1) 'Moneda
                sdbddCampoEnt.AddItem TipoCampo.Proveedor & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(2) 'Proveedor
        End Select
        
        
        If sdbgParamEnt.Columns("TIPO").Value = "6" Then 'String
            
        End If
        
        
        
    End If
    If sdbddCampoEnt.Rows = 0 Then
        sdbddCampoEnt.AddItem 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & ""
    End If
    
    Set oGrupo = Nothing
    Set oCampo = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddCampoEnt_InitColumnProps()
    sdbddCampoEnt.DataFieldList = "Column 2"
End Sub

''' <summary>
''' Valida q el dato directo suministrado sea del tipo correcto
''' </summary>
''' <param name="Cancel">Cancel el cambio de fila/columna</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgParamEnt_BeforeRowColChange(Cancel As Integer)
    If sdbgParamEnt.col = -1 Then Exit Sub

    m_bCancel = False

    Select Case sdbgParamEnt.Columns(sdbgParamEnt.col).Name
        Case "VALOR"
            If sdbgParamEnt.Columns("VALOR").Value <> "" Then
                Select Case sdbgParamEnt.Columns("TIPO").Value
                Case "4" 'boolean
                    If UCase(sdbgParamEnt.Columns("VALOR").Value) <> UCase(m_sIdiTrue) And UCase(sdbgParamEnt.Columns("VALOR").Value) <> UCase(m_sIdiFalse) Then
                        oMensajes.NoValido sdbgParamEnt.Columns("VALOR").caption & " " & m_sMensaje(3)
                        Cancel = True
                        m_bCancel = True
                    End If
                Case "3" 'dateTime
                    If Not IsDate(sdbgParamEnt.Columns("VALOR").Value) Then
                        oMensajes.NoValido sdbgParamEnt.Columns("VALOR").caption & " " & m_sMensaje(2)
                        Cancel = True
                        m_bCancel = True
                    End If
                Case "2"  '"int", "double", "long", "decimal", "short", "unsignedByte"
                    If Not IsNumeric(sdbgParamEnt.Columns("VALOR").Value) Then
                        oMensajes.NoValido sdbgParamEnt.Columns("VALOR").caption & " " & m_sMensaje(1)
                        Cancel = True
                        m_bCancel = True
                    End If
                Case Else '"string"
                    m_bCancel = False
                End Select
            End If
    End Select
End Sub

Private Sub sdbgParamEnt_BtnClick()
    If sdbgParamEnt.Columns("VALOR").Value <> "" Then
        If Not IsDate(sdbgParamEnt.Columns("VALOR").Value) Then
            oMensajes.NoValido sdbgParamEnt.Columns("VALOR").caption & " " & m_sMensaje(2)
            m_bCancel = True
            Exit Sub
        End If
    End If

    If sdbgParamEnt.Columns("TIPO").Value = "3" Then 'dateTime
        Set frmCalendar.frmDestination = frmServicio
        Set frmCalendar.ctrDestination = Nothing
        frmCalendar.addtotop = 900 + 360
        frmCalendar.addtoleft = 180
        If sdbgParamEnt.Columns("VALOR").Value <> "" Then
            frmCalendar.Calendar.Value = sdbgParamEnt.Columns("VALOR").Value
        Else
            frmCalendar.Calendar.Value = Date
        End If
        frmCalendar.Show vbModal
    End If
End Sub

''' <summary>
''' Si has cambiado dato DIRECTO o GRUPO has de cambiar la aparencia y/o limpiar datos asociados
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgParamEnt_Change()
    Select Case sdbgParamEnt.Columns(sdbgParamEnt.col).Name
        Case "DIRECTO"
            With sdbgParamEnt
                If .Columns("DIRECTO").Value = True Then
                    DirectoColumnasSi
                Else
                    DirectoColumnasNo
                End If
            End With

        Case "GRUPO"
            sdbgParamEnt.Columns("ID_CAMPO").Value = 0
            sdbgParamEnt.Columns("CAMPO").Value = ""
    End Select
End Sub

''' <summary>
''' Cambiar la aparencia y/o establacer estilos para datos asociados
''' </summary>
''' <param name="LastRow">fila de la q vienes</param>
''' <param name="LastCol">columna de la q vienes</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgParamEnt_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgParamEnt.col < 0 Then
        Exit Sub
    End If

    Select Case sdbgParamEnt.Columns(sdbgParamEnt.col).Name
        Case "VALOR"
            sdbgParamEnt.Columns("VALOR").DropDownHwnd = 0
            Select Case sdbgParamEnt.Columns("TIPO").Value
            Case "4" 'boolean
                sdbgParamEnt.Columns("VALOR").Style = ssStyleComboBox
                sdbddValor.RemoveAll
                sdbddValor.AddItem ""
                sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                sdbgParamEnt.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
            Case "3" 'dateTime
                sdbgParamEnt.Columns("VALOR").Style = ssStyleEditButton
            Case "2"  '"int", "double", "long", "decimal", "short", "unsignedByte"
                sdbgParamEnt.Columns("VALOR").Style = ssStyleEdit
            Case Else '"string"
                sdbgParamEnt.Columns("VALOR").Style = ssStyleEdit
            End Select
        Case "GRUPO", "CAMPO"
            If sdbgParamEnt.Columns("TIPO_CAMPO").Value <> "" Then
                If CInt(sdbgParamEnt.Columns("TIPO_CAMPO").Value) <= TipoCampo.CampoDeFormulario Or _
                CInt(sdbgParamEnt.Columns("TIPO_CAMPO").Value) = TipoCampo.Peticionario Or _
                CInt(sdbgParamEnt.Columns("TIPO_CAMPO").Value) = TipoCampo.DepPeticionario Or _
                CInt(sdbgParamEnt.Columns("TIPO_CAMPO").Value) = TipoCampo.UONPeticionario Or _
                CInt(sdbgParamEnt.Columns("TIPO_CAMPO").Value) = TipoCampo.ImporteAdjudicado Or _
                CInt(sdbgParamEnt.Columns("TIPO_CAMPO").Value) = TipoCampo.ImporteSolicitud Then
                    sdbgParamEnt.Columns("GRUPO").CellStyleSet "Normal", sdbgParamEnt.Row
                    sdbgParamEnt.Columns("GRUPO").Locked = False
                    sdbgParamEnt.Columns("GRUPO").DropDownHwnd = sdbddGrupoEnt.hWnd
                    
                    sdbgParamEnt.Columns("CAMPO").CellStyleSet "Normal", sdbgParamEnt.Row
                    sdbgParamEnt.Columns("CAMPO").Locked = False
                    sdbgParamEnt.Columns("CAMPO").DropDownHwnd = sdbddCampoEnt.hWnd
                    
                    sdbgParamEnt.Columns("VALOR").Value = ""
                    sdbgParamEnt.Columns("VALOR").Locked = True
                    sdbgParamEnt.Columns("VALOR").CellStyleSet "Gris", sdbgParamEnt.Row
                    
                ElseIf CInt(sdbgParamEnt.Columns("TIPO_CAMPO").Value) = TipoCampo.CampoDeFormulario * 1000 Then
                    sdbgParamEnt.Columns("GRUPO").CellStyleSet "Normal", sdbgParamEnt.Row
                    sdbgParamEnt.Columns("GRUPO").Locked = False
                    sdbgParamEnt.Columns("GRUPO").DropDownHwnd = sdbddGrupoEnt.hWnd
                    
                    sdbgParamEnt.Columns("CAMPO").CellStyleSet "Gris", sdbgParamEnt.Row
                    sdbgParamEnt.Columns("CAMPO").Locked = True
                    sdbgParamEnt.Columns("CAMPO").DropDownHwnd = 0
                Else
                    DirectoColumnasSi
                End If
            End If
    End Select
End Sub

Private Sub sdbgParamEnt_RowLoaded(ByVal Bookmark As Variant)
    comprobarDirecto
End Sub

Private Sub comprobarError()
    With sdbgParamSal
        If .Columns("ERROR").Value = True Then
            .Columns("DESTINO").Value = 0
            .Columns("TIPO_CAMPO").Value = 0
            .Columns("ID_GRUPO").Value = 0
            .Columns("GRUPO").Value = ""
            .Columns("GRUPO").DropDownHwnd = 0
            .Columns("GRUPO").Locked = True
            .Columns("GRUPO").CellStyleSet "Gris", sdbgParamSal.Row
            .Columns("ID_CAMPO").Value = 0
            .Columns("CAMPO").Value = ""
            .Columns("CAMPO").DropDownHwnd = 0
            .Columns("CAMPO").Locked = True
            .Columns("CAMPO").CellStyleSet "Gris", sdbgParamSal.Row
        Else
            .Columns("ERROR").Value = False
            .Columns("TIPO_CAMPO").Value = TipoCampo.CampoDeFormulario
            .Columns("GRUPO").Locked = False
            .Columns("GRUPO").CellStyleSet "Normal", sdbgParamSal.Row
            .Columns("CAMPO").Locked = False
            .Columns("CAMPO").DropDownHwnd = sdbddCampoSal.hWnd
            .Columns("CAMPO").CellStyleSet "Normal", sdbgParamSal.Row
        End If
    End With
End Sub

''' <summary>
''' Tras decidir entre Campo Generico y Datos de pantalla, se pasan todos los datos al grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddGrupoEnt_CloseUp()

    If sdbgParamEnt.Columns("GRUPO").Value = "" Then Exit Sub
    sdbgParamEnt.Columns("ID_GRUPO").Value = sdbddGrupoEnt.Columns(0).Value
    sdbgParamEnt.Columns("GRUPO").Value = sdbddGrupoEnt.Columns(1).Value
        
    sdbgParamEnt.Columns("TIPO_CAMPO").Value = TipoCampo.CampoDeFormulario
End Sub


Private Sub sdbddGrupoEnt_DropDown()
    Dim oFormulario As CFormulario
    Dim oGrupo As CFormGrupo
    Dim sDenGrupo As String
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    sdbddGrupoEnt.RemoveAll
    'Cargar combo
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = lIdFormulario
    oFormulario.CargarTodosLosGrupos
       
    sdbddGrupoEnt.AddItem 0 & Chr(m_lSeparador) & m_sCampoGenerico
   
    For Each oGrupo In oFormulario.Grupos
        For i = 1 To oGrupo.Denominaciones.Count
            If oGrupo.Denominaciones.Item(i).Cod = oUsuarioSummit.idioma Then
                sDenGrupo = oGrupo.Denominaciones.Item(i).Den
                Exit For
            End If
        Next
        sdbddGrupoEnt.AddItem oGrupo.Id & Chr(m_lSeparador) & sDenGrupo
    Next
    Set oFormulario = Nothing
    Set oGrupo = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbddGrupoEnt_InitColumnProps()
    sdbddGrupoEnt.DataFieldList = "Column 0"
    sdbddGrupoEnt.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbgParamSal_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim i As Integer
    Dim vbm As Variant
    
    With sdbgParamSal
        Select Case .Columns(ColIndex).Name
            Case "ERROR"
                'Reemplazo el valor de GRUPO por el del desplegable para que no cambie el texto por el ID
                'al checkear/descheckear el indicador de error
                For i = 0 To sdbgParamSal.Rows - 1
                    vbm = sdbgParamSal.AddItemBookmark(i)
                    If sdbgParamSal.Columns("GRUPO").CellValue(vbm) <> "" Then
                        sdbgParamSal.Columns("GRUPO").Value = sdbddGrupoSal.Columns(1).Value
                    End If
                Next i
                'Comprobar que no haya otro par�metro chequeado como ind. de error
                If .Columns("ERROR").Value = True And OldValue = False Then 'si quiero poner a true compruebo que no haya otra fila
                    For i = 0 To sdbgParamSal.Rows - 1
                        vbm = sdbgParamSal.AddItemBookmark(i)
                        If sdbgParamSal.Columns("ERROR").CellValue(vbm) = True Then
                            MsgBox (errorInd)
                            sdbgParamSal.Columns(sdbgParamSal.col).Value = OldValue
                        End If
                    Next i
                    If Not .Columns("TIPO").Value = "6" Then
                        MsgBox (errorString)
                        sdbgParamSal.Columns(sdbgParamSal.col).Value = False
                    End If
                End If
        End Select
    End With
End Sub

Private Sub sdbgParamSal_Change()
    sdbgParamSal.Update
    comprobarError
    Select Case sdbgParamSal.Columns(sdbgParamSal.col).Name
        Case "GRUPO"
            sdbgParamSal.Columns("ID_CAMPO").Value = 0
            sdbgParamSal.Columns("CAMPO").Value = ""
    End Select
End Sub

Private Sub sdbgParamSal_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgParamSal.col < 0 Then
        Exit Sub
    End If
    sdbgParamSal.Update
    comprobarError
    With sdbgParamSal
        Select Case .Columns(sdbgParamSal.col).Name
            Case "GRUPO", "CAMPO"
                If .Columns("TIPO_CAMPO").Value <> "" Then
                    If CInt(.Columns("TIPO_CAMPO").Value) <= TipoCampo.CampoDeFormulario Then
                        .Columns("GRUPO").CellStyleSet "Normal", sdbgParamSal.Row
                        .Columns("GRUPO").Locked = False
                        .Columns("GRUPO").DropDownHwnd = sdbddGrupoSal.hWnd
                        .Columns("CAMPO").CellStyleSet "Normal", sdbgParamSal.Row
                        .Columns("CAMPO").Locked = False
                        .Columns("CAMPO").DropDownHwnd = sdbddCampoSal.hWnd
                    Else
                        .Columns("GRUPO").CellStyleSet "Gris", sdbgParamSal.Row
                        .Columns("GRUPO").Locked = True
                        .Columns("GRUPO").DropDownHwnd = 0
                        .Columns("CAMPO").CellStyleSet "Gris", sdbgParamSal.Row
                        .Columns("CAMPO").Locked = True
                        .Columns("CAMPO").DropDownHwnd = 0
                    End If
                End If
        End Select
    End With
End Sub

Private Sub sdbgParamSal_RowLoaded(ByVal Bookmark As Variant)
    comprobarError
End Sub

Private Sub sdbddGrupoSal_CloseUp()
    If sdbgParamSal.Columns("GRUPO").Value = "" Then Exit Sub
    sdbgParamSal.Columns("ID_GRUPO").Value = sdbddGrupoSal.Columns(0).Value
    sdbgParamSal.Columns("GRUPO").Value = sdbddGrupoSal.Columns(1).Value
End Sub

Private Sub sdbddGrupoSal_DropDown()
    Dim oFormulario As CFormulario
    Dim oGrupo As CFormGrupo
    Dim sDenGrupo As String
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    sdbddGrupoSal.RemoveAll
    'Cargar combo
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = lIdFormulario
    oFormulario.CargarTodosLosGrupos
   
    For Each oGrupo In oFormulario.Grupos
        For i = 1 To oGrupo.Denominaciones.Count
            If oGrupo.Denominaciones.Item(i).Cod = oUsuarioSummit.idioma Then
                sDenGrupo = oGrupo.Denominaciones.Item(i).Den
                Exit For
            End If
        Next
        sdbddGrupoSal.AddItem oGrupo.Id & Chr(m_lSeparador) & sDenGrupo
    Next
    Set oFormulario = Nothing
    Set oGrupo = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddGrupoSal_InitColumnProps()
    sdbddGrupoSal.DataFieldList = "Column 0"
    sdbddGrupoSal.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddCampoSal_CloseUp()
    If sdbgParamSal.Columns("CAMPO").Value = "" Then Exit Sub
    sdbgParamSal.Columns("TIPO_CAMPO").Value = sdbddCampoSal.Columns(0).Value
    sdbgParamSal.Columns("ID_CAMPO").Value = sdbddCampoSal.Columns(1).Value
    sdbgParamSal.Columns("CAMPO").Value = sdbddCampoSal.Columns(2).Value
End Sub

Private Sub sdbddCampoSal_DropDown()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim cDesglose As CFormItem
    Dim sDenCampo As String
    Dim iTipoCampo As TipoCampo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbddCampoSal.RemoveAll
    If iTipoCampo = TipoCampo.CampoDeFormulario Then
    End If
    If sdbgParamSal.Columns("ID_GRUPO").Value <> "" Then
        If CLng(sdbgParamSal.Columns("ID_GRUPO").Value) > 0 Then
            iTipoCampo = TipoCampo.CampoDeFormulario
        End If
    End If
    If iTipoCampo = TipoCampo.CampoDeFormulario Then
        Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
        oGrupo.Id = sdbgParamSal.Columns("ID_GRUPO").Value
        Set oGrupo.Formulario = oFSGSRaiz.Generar_CFormulario 'Lo pongo para enga�ar al objeto y que cargue los datos de FORM_CAMPO
        oGrupo.CargarTodosLosCampos
        For Each oCampo In oGrupo.Campos
            If g_sOrigen = "frmDesglose" And oCampo.Tipo = TipoDesglose And sdbgParamSal.Columns("ID_GRUPO").Value = lGrupoFormulario Then
                frmFormularios.g_ofrmDesglose.g_oCampoDesglose.CargarDesglose
                For Each cDesglose In frmFormularios.g_ofrmDesglose.g_oCampoDesglose.Desglose
                    If tipoGeneral(cDesglose.Tipo) = sdbgParamSal.Columns("TIPO").Value Then 'Cargo s�lo los del mismo tipo que el par�metro
                        For i = 1 To cDesglose.Denominaciones.Count
                        If cDesglose.Denominaciones.Item(i).Cod = oUsuarioSummit.idioma Then
                            sDenCampo = "(" & m_desglose & ") " & cDesglose.Denominaciones.Item(i).Den
                            Exit For
                        End If
                    Next
                    sdbddCampoSal.AddItem TipoCampo.CampoDeFormulario & Chr(m_lSeparador) & cDesglose.Id & Chr(m_lSeparador) & sDenCampo
                    End If
                Next
            Else
                If tipoGeneral(oCampo.Tipo) = sdbgParamSal.Columns("TIPO").Value And oCampo.CampoPadre Is Nothing Then
                    For i = 1 To oCampo.Denominaciones.Count
                        If oCampo.Denominaciones.Item(i).Cod = oUsuarioSummit.idioma Then
                            sDenCampo = oCampo.Denominaciones.Item(i).Den
                            Exit For
                        End If
                    Next
                    sdbddCampoSal.AddItem TipoCampo.CampoDeFormulario & Chr(m_lSeparador) & oCampo.Id & Chr(m_lSeparador) & sDenCampo
                End If
            End If
        Next
    End If
    If sdbddCampoSal.Rows = 0 Then
        sdbddCampoSal.AddItem 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & ""
    End If
    
    Set oGrupo = Nothing
    Set oCampo = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddCampoSal_InitColumnProps()
    sdbddCampoSal.DataFieldList = "Column 2"
End Sub

Private Function tipoParametro(Tipo As String) As String
    Select Case Tipo
        Case "string"
            Tipo = "6"
        Case "int", "double", "long", "decimal", "short", "unsignedByte"
            Tipo = "2"
        Case "dateTime"
            Tipo = "3"
        Case "boolean"
            Tipo = "4"
    End Select
    tipoParametro = Tipo
End Function

Private Function tipoGeneral(Tipo As String) As String
    Select Case Tipo
        Case "1", "5", "6", "7"
            Tipo = "6"
        Case Default
            Tipo = Tipo
    End Select
    tipoGeneral = Tipo
End Function

''' <summary>
''' Carga los grids de Param Entrada y Salida
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarDatos()
    'Carga las columnas de la grid de valores dependiendo si es multiidioma o no
    If m_bMultiIdioma = True Then
        'Es multiidioma,muestra la grid en lugar del texbox del nombre:
        sdbgDen.Visible = True
        txtNombre.Visible = False
        CargarGridDenIdiomas (iServAntiguo)
    Else
        'Se muestra solo el textbox del nombre,no la grid
        sdbgDen.Visible = False
        txtNombre.Visible = True
        txtNombre.Text = oServicio.SacarDenIdiomaServicio(iServAntiguo, oUsuarioSummit.idioma)
    End If
    txtURL.Text = oServicio.SacarURLServicio(iServAntiguo)
    If txtURL.Text <> "" Then
        txtURL.Locked = True
        cmdCargar.Enabled = False
        
        m_bRowIsNew = (lCampoForm = 0)
        m_bRowLoaded = False
        
        CargarGridParametros
        
        m_bRowLoaded = True
    End If
End Sub

Private Sub CargarGridDenIdiomas(piID As Integer)
    Dim oIdioma As CIdioma

    'Carga las denominaciones en todos los idiomas:
    For Each oIdioma In g_oIdiomas
        sdbgDen.AddItem oIdioma.Den & Chr(m_lSeparador) & oServicio.SacarDenIdiomaServicio(piID, oIdioma.Cod) & Chr(m_lSeparador) & oIdioma.Cod
    Next
    sdbgDen.MoveFirst
End Sub

''' <summary>
''' Carga los grids de Param Entrada y Salida
''' </summary>
''' <remarks>Llamada desde: cmdCargar_Click    CargarDatos; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridParametros()
    
    Dim i As Integer
    Dim indError As Boolean
    Dim Ador As Ador.Recordset
    Dim sFila As String
    
    sdbgParamEnt.RemoveAll
    sdbgParamSal.RemoveAll
    'Carga la grid con los parametros del webservice
    If iServAntiguo = 0 Then
        If miParam.Count < 1 Then
            oMensajes.WebServiceIncorrecto
            Exit Sub
        Else
            For i = 1 To miParam.Count
                If miParam.Item(i)(2) = 0 Then
                    sdbgParamEnt.AddItem miParam.Item(i)(1) & Chr(m_lSeparador) & miParam.Item(i)(3) & Chr(m_lSeparador) & tipoParametro(CStr(miParam.Item(i)(4))) & _
                        Chr(m_lSeparador) & True & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & 0 & _
                        Chr(m_lSeparador) & "" & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & ""
                ElseIf miParam.Item(i)(2) = 1 Then
                    sdbgParamSal.AddItem miParam.Item(i)(1) & Chr(m_lSeparador) & miParam.Item(i)(3) & Chr(m_lSeparador) & tipoParametro(CStr(miParam.Item(i)(4))) & _
                        Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & 0 & _
                        Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & False
                End If
            Next
        End If
    Else
        If lCampoForm = 0 Then
            'Carga la grid desde BD con los parametros (sin campos ni valores)
            Set Ador = oServicio.SacarParamServicio(iServAntiguo)
            If Not Ador Is Nothing Then
                Ador.MoveFirst
                Do While Not Ador.EOF
                    If Ador.Fields("SENTIDO").Value = 1 Then
                        'Parametro de entrada (ID, Nombre, Tipo, Campo_externo=True)
                        sdbgParamEnt.AddItem Ador.Fields("ID").Value & Chr(m_lSeparador) & NullToStr(Ador.Fields("DEN").Value) & Chr(m_lSeparador) & _
                                            Ador.Fields("TIPO").Value & Chr(m_lSeparador) & True & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & "" & _
                                            Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & ""
                    ElseIf Ador.Fields("SENTIDO").Value = 2 Then
                        If Ador.Fields("INDICADOR_ERROR").Value = "1" Then
                            indError = True
                        Else
                            indError = False
                        End If
                        'Parametro de salida (ID, Nombre, Tipo, Indicador_Error)
                        sdbgParamSal.AddItem Ador.Fields("ID").Value & Chr(m_lSeparador) & NullToStr(Ador.Fields("DEN").Value) & Chr(m_lSeparador) & _
                                            Ador.Fields("TIPO").Value & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & _
                                            "" & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & indError
                    End If
                    Ador.MoveNext
                Loop
                Ador.Close
                Set Ador = Nothing
            End If
        Else
            'Carga la grid desde BD con los parametros, campos y valores
            Set Ador = oServicio.SacarDatosServicio(lCampoForm)
            If Not Ador Is Nothing Then
                Ador.MoveFirst
                Do While Not Ador.EOF
                    Dim pIDGrupo, pIDCampo As Long
                    Dim pGrupo, pCampo As String
                    pIDCampo = Ador.Fields("CAMPO").Value
                    pIDGrupo = oServicio.sacarIDGrupo(pIDCampo)
                    pGrupo = oServicio.DenGrupoIdioma(pIDGrupo, oUsuarioSummit.idioma)
                    pCampo = oServicio.DenCampoIdioma(pIDCampo, oUsuarioSummit.idioma)
                    If Ador.Fields("SENTIDO").Value = 1 Then
                        Select Case Ador.Fields("TIPO_CAMPO").Value
                            Case TipoCampo.Peticionario
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(0)
                            Case TipoCampo.Moneda
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(1)
                            Case TipoCampo.Proveedor
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(2)
                            Case TipoCampo.Contacto
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(3)
                            Case TipoCampo.Empresa
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(4)
                            Case TipoCampo.FechaInicio
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(5)
                            Case TipoCampo.FechaExpiracion
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(6)
                            Case TipoCampo.MostrarAlerta
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(7)
                            Case TipoCampo.EnviarMail
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(8)
                            Case TipoCampo.ImporteDesde
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(9)
                            Case TipoCampo.ImporteHasta
                                pGrupo = m_sCampoGenerico
                                pCampo = m_arCamposGenericos(10)
                            Case TipoCampo.CampoDeFormulario
                                'Sin cambios
                            Case Else
                                pGrupo = ""
                                pCampo = ""
                        End Select
                        
                        If Ador.Fields("TIPO_CAMPO").Value = TipoCampo.Peticionario Then
                            pGrupo = m_sCampoGenerico
                            pCampo = m_arCamposGenericos(0)
                        End If
                    
                        'Parametro de entrada (ID, Nombre, Tipo, Campo_externo=True)
                        sFila = Ador.Fields("ID").Value & Chr(m_lSeparador) & NullToStr(Ador.Fields("DEN").Value) & Chr(m_lSeparador) & _
                                            Ador.Fields("TIPO").Value & Chr(m_lSeparador) & True & Chr(m_lSeparador) & Ador.Fields("DIRECTO").Value
                        If Ador.Fields("DIRECTO").Value = 1 Then
                            Select Case Ador.Fields("TIPO").Value
                            Case "4" 'boolean
                                sFila = sFila & Chr(m_lSeparador) & IIf(NullToDbl0(Ador.Fields("VALOR_BOOL").Value) = 0, m_sIdiFalse, m_sIdiTrue)
                            Case "3" 'dateTime
                                sFila = sFila & Chr(m_lSeparador) & Ador.Fields("VALOR_FEC").Value
                            Case "2"  '"int", "double", "long", "decimal", "short", "unsignedByte"
                                sFila = sFila & Chr(m_lSeparador) & Ador.Fields("VALOR_NUM").Value
                            Case Else '"string"
                                sFila = sFila & Chr(m_lSeparador) & Ador.Fields("VALOR_TEXT").Value
                            End Select
                        Else
                            sFila = sFila & Chr(m_lSeparador) & ""
                        End If
                        sFila = sFila & Chr(m_lSeparador) & pIDGrupo & Chr(m_lSeparador) & NullToStr(pGrupo) & Chr(m_lSeparador) & _
                                            Ador.Fields("TIPO_CAMPO").Value & Chr(m_lSeparador) & pIDCampo & Chr(m_lSeparador) & NullToStr(pCampo)
                        sdbgParamEnt.AddItem sFila
                        
                    ElseIf Ador.Fields("SENTIDO").Value = 2 Then
                        If Ador.Fields("INDICADOR_ERROR").Value = "1" Then
                            indError = True
                        Else
                            indError = False
                        End If
                        'Parametro de salida (ID, Nombre, Tipo, Indicador_Error)
                        sdbgParamSal.AddItem Ador.Fields("ID").Value & Chr(m_lSeparador) & NullToStr(Ador.Fields("DEN").Value) & Chr(m_lSeparador) & _
                                            Ador.Fields("TIPO").Value & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & pIDGrupo & Chr(m_lSeparador) & _
                                            NullToStr(pGrupo) & Chr(m_lSeparador) & Ador.Fields("TIPO_CAMPO").Value & Chr(m_lSeparador) & _
                                            pIDCampo & Chr(m_lSeparador) & NullToStr(pCampo) & Chr(m_lSeparador) & indError
                    End If
                    Ador.MoveNext
                Loop
                Ador.Close
                Set Ador = Nothing
            End If
        End If
    End If
End Sub

''' <summary>
''' Cargar los texto de pantalla
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SERVICIO, oUsuarioSummit.idioma)
    If Not Ador Is Nothing Then
        m_sIdiForm = Ador(0).Value   '1 Formulario
        Ador.MoveNext
        lblNombre.caption = Ador(0).Value & ":" '2 Nombre
        m_sIdiNombre = Ador(0).Value
        Ador.MoveNext
        lblURL.caption = Ador(0).Value & ":" '3 URL
        Ador.MoveNext
        tbGrid.TabCaption(0) = Ador(0).Value '4 Par�metros de entrada
        Ador.MoveNext
        tbGrid.TabCaption(1) = Ador(0).Value '5 Par�metros de salida
        Ador.MoveNext
        cmdCargar.caption = Ador(0).Value   '6 Cargar par�metros
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '7 Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '8 Cancelar
        Ador.MoveNext
        m_desglose = Ador(0).Value '9 Desglose
        Ador.MoveNext
        m_sCampoGenerico = Ador(0).Value '10 Campo gen�rico:
        Ador.MoveNext
        m_arCamposGenericos(0) = Ador(0).Value '11 Peticionario
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        errorString = Ador(0).Value '16 Para ser indicador de error el par�metro tiene que ser de tipo string.
        Ador.MoveNext
        errorInd = Ador(0).Value '17 Ya hay un par�metro con indicador de error
        Ador.MoveNext
        errorFalta = Ador(0).Value '18 Uno de los par�metros tiene que ser el indicador de error
        Ador.MoveNext
        errorCampo = Ador(0).Value '19 Debe seleccionar el campo de formulario para
        Ador.MoveNext
        sdbgParamEnt.Columns("PARAMETROS").caption = Ador(0).Value '20 Par�metros
        sdbgParamSal.Columns("PARAMETROS").caption = Ador(0).Value '20 Par�metros
        Ador.MoveNext
        sdbgParamEnt.Columns("DIRECTO").caption = Ador(0).Value '21 Directo
        Ador.MoveNext
        sdbgParamEnt.Columns("GRUPO").caption = Ador(0).Value '22 Grupo
        sdbgParamSal.Columns("GRUPO").caption = Ador(0).Value '22 Grupo
        Ador.MoveNext
        sdbgParamEnt.Columns("CAMPO").caption = Ador(0).Value '23 Campo
        sdbgParamSal.Columns("CAMPO").caption = Ador(0).Value '23 Campo
        Ador.MoveNext
        sdbgParamSal.Columns("ERROR").caption = Ador(0).Value '24 Indicador Error
        Ador.MoveNext
        sdbgParamEnt.Columns("VALOR").caption = Ador(0).Value '25 Valor
        Ador.MoveNext
        m_sIdiCaption = Ador(0).Value '26 A�adir campo
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value '21 Si
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value '22 No
        For i = 1 To 3
            Ador.MoveNext
            m_sMensaje(i) = Ador(0).Value
        Next i
                
        For i = 1 To 10
            Ador.MoveNext
            m_arCamposGenericos(i) = Ador(0).Value
        Next i
        
        Ador.Close
    End If
    Set Ador = Nothing
End Sub

''' <summary>
''' Muestra los valores de un boolean
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddValor_DropDown()
    sdbddValor.RemoveAll
    sdbddValor.AddItem m_sIdiTrue
    sdbddValor.AddItem m_sIdiFalse
End Sub

''' <summary>
''' Inicializar el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>
''' * Objetivo: Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">Text sdbddValor</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddValor_PositionList(ByVal Text As String)
        
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                Me.sdbgParamEnt.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>
''' Cambiar la aparencia del grid pq has establecido q es un param directo
''' </summary>
''' <remarks>Llamada desde: comprobarDirecto         sdbgParamEnt_Change        sdbgParamEnt_RowColChange; Tiempo m�ximo: 0,2</remarks>
Private Sub DirectoColumnasSi()
    With Me.sdbgParamEnt
        .Columns("VALOR").Locked = False
        .Columns("VALOR").DropDownHwnd = 0
        Select Case sdbgParamEnt.Columns("TIPO").Value
        Case "4" 'boolean
            .Columns("VALOR").Style = ssStyleComboBox
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
            .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
        Case "3" 'dateTime
            .Columns("VALOR").Style = ssStyleEditButton
        Case "2"  '"int", "double", "long", "decimal", "short", "unsignedByte"
            .Columns("VALOR").Style = ssStyleEdit
        Case Else '"string"
            .Columns("VALOR").Style = ssStyleEdit
        End Select
        
        .Columns("VALOR").CellStyleSet "Normal", sdbgParamEnt.Row
        .Columns("TIPO_CAMPO").Value = TipoCampo.ValorEstatico
        
        .Columns("ID_GRUPO").Value = 0
        .Columns("GRUPO").Value = ""
        .Columns("GRUPO").DropDownHwnd = 0
        .Columns("GRUPO").Locked = True
        .Columns("GRUPO").CellStyleSet "Gris", sdbgParamEnt.Row
        
        .Columns("ID_CAMPO").Value = 0
        .Columns("CAMPO").Value = ""
        .Columns("CAMPO").DropDownHwnd = 0
        .Columns("CAMPO").Locked = True
        .Columns("CAMPO").CellStyleSet "Gris", sdbgParamEnt.Row
    End With
End Sub

''' <summary>
''' Cambiar la aparencia del grid pq has establecido q no es un param directo
''' </summary>
''' <remarks>Llamada desde: sdbgParamEnt_Change ; Tiempo m�ximo: 0,2</remarks>
Private Sub DirectoColumnasNo()
    With Me.sdbgParamEnt
        .Columns("VALOR").Value = ""
        .Columns("VALOR").Locked = True
        .Columns("VALOR").CellStyleSet "Gris", sdbgParamEnt.Row
        .Columns("VALOR").DropDownHwnd = 0
        
        .Columns("GRUPO").Locked = False
        .Columns("GRUPO").CellStyleSet "Normal", sdbgParamEnt.Row
        
        .Columns("ID_CAMPO").Value = 0
        .Columns("CAMPO").Value = ""
        .Columns("CAMPO").Locked = False
        .Columns("CAMPO").DropDownHwnd = 0
        .Columns("CAMPO").CellStyleSet "Normal", sdbgParamEnt.Row
        
        .Columns("TIPO_CAMPO").Value = TipoCampo.CampoDeFormulario
    End With
End Sub
