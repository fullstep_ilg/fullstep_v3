VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{1E673DEF-C82F-4FBB-BAB9-77B0634E8F4D}#1.0#0"; "ProceSelector.ocx"
Begin VB.Form frmPROCEClonar 
   BackColor       =   &H00808000&
   Caption         =   "Copiar proceso"
   ClientHeight    =   7455
   ClientLeft      =   525
   ClientTop       =   945
   ClientWidth     =   9690
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEClonar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   7455
   ScaleWidth      =   9690
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   9690
      TabIndex        =   37
      Top             =   7035
      Width           =   9690
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   4943
         TabIndex        =   28
         Top             =   45
         Width           =   1095
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   3653
         TabIndex        =   27
         Top             =   45
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Caption         =   "Datos generales"
      ForeColor       =   &H00FFFFFF&
      Height          =   1605
      Left            =   158
      TabIndex        =   36
      Top             =   2760
      Width           =   9375
      Begin VB.CheckBox chkPersonas 
         BackColor       =   &H00808000&
         Caption         =   "Personas"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   270
         TabIndex        =   9
         Top             =   1230
         Value           =   1  'Checked
         Width           =   2715
      End
      Begin VB.CheckBox chkPresAnu1 
         BackColor       =   &H00808000&
         Caption         =   "Presupuestos anuales tipo 1"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   3420
         TabIndex        =   10
         Top             =   225
         Value           =   1  'Checked
         Width           =   2715
      End
      Begin VB.CheckBox chkPres1 
         BackColor       =   &H00808000&
         Caption         =   "Presupuestos tipo 1"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   3420
         TabIndex        =   12
         Top             =   890
         Value           =   1  'Checked
         Width           =   2715
      End
      Begin VB.CheckBox chkPres2 
         BackColor       =   &H00808000&
         Caption         =   "Presupuestos tipo 2"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   3420
         TabIndex        =   13
         Top             =   1230
         Value           =   1  'Checked
         Width           =   2715
      End
      Begin VB.CheckBox chkProvesAsig 
         BackColor       =   &H00808000&
         Caption         =   "Proveedores asignados"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   6615
         TabIndex        =   17
         Top             =   1230
         Width           =   2475
      End
      Begin VB.CheckBox chkEspec 
         BackColor       =   &H00808000&
         Caption         =   "Especificaciones"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   6615
         TabIndex        =   16
         Top             =   890
         Value           =   1  'Checked
         Width           =   2475
      End
      Begin VB.CheckBox chkUON 
         BackColor       =   &H00808000&
         Caption         =   "Distribuci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   6615
         TabIndex        =   15
         Top             =   550
         Value           =   1  'Checked
         Width           =   2475
      End
      Begin VB.CheckBox chkPresAnu2 
         BackColor       =   &H00808000&
         Caption         =   "Presupuestos anuales tipo 2"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   3420
         TabIndex        =   11
         Top             =   550
         Value           =   1  'Checked
         Width           =   2715
      End
      Begin VB.CheckBox chkAtrib 
         BackColor       =   &H00808000&
         Caption         =   "Atributos"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   270
         TabIndex        =   6
         Top             =   225
         Value           =   1  'Checked
         Width           =   2715
      End
      Begin VB.CheckBox chkVistas 
         BackColor       =   &H00808000&
         Caption         =   "Vistas de plantillas"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   0
         Left            =   765
         TabIndex        =   7
         Top             =   587
         Value           =   1  'Checked
         Width           =   2580
      End
      Begin VB.CheckBox chkVistas 
         BackColor       =   &H00808000&
         Caption         =   "Vistas de responsable"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   1
         Left            =   765
         TabIndex        =   8
         Top             =   927
         Width           =   2580
      End
      Begin VB.CheckBox chkExcluidos 
         BackColor       =   &H00808000&
         Caption         =   "Items que han sido excluidos"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   6615
         TabIndex        =   14
         Top             =   240
         Width           =   2715
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00808000&
      Caption         =   "Presupuesto de los �tems"
      ForeColor       =   &H00FFFFFF&
      Height          =   1455
      Left            =   158
      TabIndex        =   33
      Top             =   5535
      Width           =   9375
      Begin VB.Frame Frame4 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   870
         Left            =   5220
         TabIndex        =   34
         Top             =   360
         Width           =   4020
         Begin VB.OptionButton optFrame4 
            BackColor       =   &H00808000&
            Caption         =   "Media ponderada"
            ForeColor       =   &H00FFFFFF&
            Height          =   340
            Index           =   3
            Left            =   2070
            TabIndex        =   25
            Top             =   495
            Width           =   1900
         End
         Begin VB.OptionButton optFrame4 
            BackColor       =   &H00808000&
            Caption         =   "Media simple"
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   2
            Left            =   2070
            TabIndex        =   24
            Top             =   225
            Width           =   1900
         End
         Begin VB.OptionButton optFrame4 
            BackColor       =   &H00808000&
            Caption         =   "Precio m�s caro"
            ForeColor       =   &H00FFFFFF&
            Height          =   340
            Index           =   1
            Left            =   225
            TabIndex        =   23
            Top             =   495
            Width           =   1770
         End
         Begin VB.OptionButton optFrame4 
            BackColor       =   &H00808000&
            Caption         =   "Precio m�s barato"
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   0
            Left            =   225
            TabIndex        =   22
            Top             =   225
            Value           =   -1  'True
            Width           =   1770
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00FFFFFF&
            X1              =   45
            X2              =   45
            Y1              =   45
            Y2              =   720
         End
         Begin VB.Label lblframe4 
            BackColor       =   &H00808000&
            Caption         =   "Si la adjudicaci�n es compartida:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   135
            TabIndex        =   35
            Top             =   0
            Width           =   3795
         End
      End
      Begin VB.OptionButton optPresItem 
         BackColor       =   &H00808000&
         Caption         =   "Actualizar los presupuestos seg�n la �ltima adjudicaci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   270
         TabIndex        =   21
         Top             =   630
         Value           =   -1  'True
         Width           =   4875
      End
      Begin VB.OptionButton optPresItem 
         BackColor       =   &H00808000&
         Caption         =   "Mantener los presupuestos del proceso original"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   2
         Left            =   270
         TabIndex        =   26
         Top             =   1035
         Width           =   4875
      End
      Begin VB.OptionButton optPresItem 
         BackColor       =   &H00808000&
         Caption         =   "Actualizar los presupuestos seg�n la planificaci�n anual"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   0
         Left            =   270
         TabIndex        =   20
         Top             =   225
         Width           =   4875
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Caption         =   "Fechas"
      ForeColor       =   &H00FFFFFF&
      Height          =   1050
      Left            =   158
      TabIndex        =   32
      Top             =   4410
      Width           =   9375
      Begin VB.OptionButton optFechas 
         BackColor       =   &H00808000&
         Caption         =   "Mantener las fechas del proceso original"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   270
         TabIndex        =   19
         Top             =   630
         Width           =   8790
      End
      Begin VB.OptionButton optFechas 
         BackColor       =   &H00808000&
         Caption         =   "Actualizar todas las fechas en base a la fecha de apertura actual"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   270
         TabIndex        =   18
         Top             =   225
         Value           =   -1  'True
         Width           =   8790
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
      Height          =   285
      Left            =   2040
      TabIndex        =   1
      Top             =   180
      Width           =   945
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1640
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3228
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   1667
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
      Height          =   285
      Left            =   5300
      TabIndex        =   4
      Top             =   180
      Width           =   4215
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   6615
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2064
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "FECREAPERT"
      Columns(2).Name =   "FECREAPERT"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   7435
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
      Height          =   285
      Left            =   4100
      TabIndex        =   3
      Top             =   180
      Width           =   1185
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1826
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4710
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "FECREAPERT"
      Columns(2).Name =   "FECREAPERT"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   2090
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgGrupos 
      Height          =   2025
      Left            =   120
      TabIndex        =   5
      Top             =   720
      Width           =   9480
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      GroupHeaders    =   0   'False
      Col.Count       =   6
      stylesets.count =   2
      stylesets(0).Name=   "Gris"
      stylesets(0).BackColor=   12632256
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPROCEClonar.frx":0CB2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPROCEClonar.frx":0CCE
      DividerType     =   0
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   0
      PictureButton   =   "frmPROCEClonar.frx":0CEA
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   1535
      Columns(0).Caption=   "Incluir"
      Columns(0).Name =   "INC"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   2566
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   3
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   10160
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   50
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1984
      Columns(3).Caption=   "Items"
      Columns(3).Name =   "ITEMS"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "Escalados"
      Columns(4).Name =   "ESC"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID"
      Columns(5).Name =   "ID"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   16722
      _ExtentY        =   3572
      _StockProps     =   79
      Caption         =   "Seleccione los grupos / items que desea incluir en el proceso a copiar"
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SelectorDeProcesos.ProceSelector ProceSelector1 
      Height          =   315
      Left            =   3015
      TabIndex        =   2
      Top             =   195
      Width           =   315
      _ExtentX        =   556
      _ExtentY        =   556
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
      Height          =   285
      Left            =   480
      TabIndex        =   0
      Top             =   180
      Width           =   900
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   1693
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1587
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00FFFFFF&
      FillColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   45
      Top             =   120
      Width           =   9615
   End
   Begin VB.Label lblAnyo 
      BackColor       =   &H00808000&
      Caption         =   "A�o:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   31
      Top             =   225
      Width           =   405
   End
   Begin VB.Label lblGMN1_4 
      BackColor       =   &H00808000&
      Caption         =   "Comm:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1500
      TabIndex        =   30
      Top             =   225
      Width           =   540
   End
   Begin VB.Label lblProce 
      BackColor       =   &H00808000&
      Caption         =   "Proce:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3405
      TabIndex        =   29
      Top             =   240
      Width           =   675
   End
End
Attribute VB_Name = "frmPROCEClonar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables para func. combos
Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean
Private m_bGMN1RespetarCombo As Boolean
Private m_bGMN1CargarComboDesde As Boolean
Private m_oGruposMN1 As CGruposMatNivel1

Private m_oProcesos As CProcesos
Private m_oProcesoSeleccionado As cProceso

'Restricciones
Public g_bRMat As Boolean
Public g_bRAsig As Boolean
Public g_bRCompResp As Boolean
Public g_bREqpAsig As Boolean
Public g_bRUsuAper As Boolean
Public g_bRUsuUON As Boolean
Public g_bRUsuDep As Boolean
Public g_bSoloInvitado As Boolean 'Si es true el usuario no tiene acceso a frmproce, solo como invitado
Public g_bRPerfUON As Boolean

Public g_sProceso As String 'Proceso que nos viene de frmAvisoDespublicacion.frm para Renegociar el contrato
Public g_lContratoOrigen As Long
'Idiomas
Private m_sIdiProceso As String
Private m_sIdiCodigo As String

'Total de grupos a los que se quiere a�adir los items
Private m_iAnyaItemsGrupo As Integer

'Alguno de los presupuestos o la distrib est� a nivel de �tem
Private m_bPresEnItem As Boolean
'Alguno de los presupuestos o la distrib est� a nivel de proceso
Private m_bPresEnProce As Boolean

Private m_bRespetarChecks As Boolean

Private m_bUnloaded As Boolean

Private Sub DeseleccionarTodosLosItems()
Dim lngGrupo As Long

    sdbgGrupos.MoveFirst
    For lngGrupo = 1 To sdbgGrupos.Rows
        sdbgGrupos.Columns("ITEMS").Value = "0"
        sdbgGrupos.MoveNext
    Next
    
End Sub

Private Function HayAlgunItemSeleccionado() As Boolean
Dim lngItemSel As Long
Dim vbm As Variant
Dim i As Integer

    lngItemSel = 0
    For i = 0 To sdbgGrupos.Rows - 1
        vbm = sdbgGrupos.AddItemBookmark(i)
        If sdbgGrupos.Columns("ITEMS").CellValue(vbm) <> "0" Then
            lngItemSel = lngItemSel + 1
        End If

    Next

    If lngItemSel = sdbgGrupos.Rows Then
        HayAlgunItemSeleccionado = False
    Else
        If lngItemSel = 0 Then
            HayAlgunItemSeleccionado = False
        Else
            HayAlgunItemSeleccionado = True
        End If
    End If

End Function


''' <summary>
''' Determinar si se puede copiar o no la plantilla
''' </summary>
''' <returns>Si es chequeable por haber plantilla o no el check correspondiente</returns>
''' <remarks>Llamada desde: ProcesoSeleccionado         sdbgGrupos_AfterUpdate; Tiempo m�ximo: 0,2</remarks>
Private Function HabilitarVistasPlantilla() As Boolean

    If IsNull(m_oProcesoSeleccionado.Plantilla) Then
        HabilitarVistasPlantilla = False
        Exit Function
    End If

    HabilitarVistasPlantilla = True

    
End Function

Private Sub ProcesoSeleccionado()
Dim oGrupo As CGrupo
Dim bMostrarEscalados As Boolean

    If sdbcProceCod.Value = "" Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    LimpiarDatosProceso
    
    Set m_oProcesoSeleccionado = oFSGSRaiz.Generar_CProceso
    
    m_oProcesoSeleccionado.Anyo = sdbcAnyo.Value
    m_oProcesoSeleccionado.GMN1Cod = sdbcGMN1_4Cod.Value
    m_oProcesoSeleccionado.Cod = sdbcProceCod.Value
    
    m_oProcesoSeleccionado.CargarDatosGeneralesProceso frmPROCE.g_bRUOPresAnuTipo1, frmPROCE.g_bRUOPresAnuTipo2, frmPROCE.g_bRUOPresTipo1, frmPROCE.g_bRUOPresTipo2, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, frmPROCE.g_bRUO

    If m_oProcesoSeleccionado.AdminPublica Then
        chkProvesAsig.Visible = False
    Else
        chkProvesAsig.Visible = True
    End If
        
    For Each oGrupo In m_oProcesoSeleccionado.Grupos
        oGrupo.CargarEscalados
        If oGrupo.UsarEscalados Then
            bMostrarEscalados = True
        End If
    Next
    If bMostrarEscalados Then
        sdbgGrupos.Columns("ESC").Visible = True
        sdbgGrupos.Columns(2).Width = sdbgGrupos.Width * 0.46
        sdbgGrupos.Columns(3).Width = sdbgGrupos.Width * 0.12
        sdbgGrupos.Columns(4).Width = sdbgGrupos.Width * 0.12
    Else
        sdbgGrupos.Columns("ESC").Visible = False
        sdbgGrupos.Columns(2).Width = sdbgGrupos.Width * 0.58
        sdbgGrupos.Columns(3).Width = sdbgGrupos.Width * 0.12
        sdbgGrupos.Columns(4).Width = 0
    End If
        
    'Grid de grupos
    For Each oGrupo In m_oProcesoSeleccionado.Grupos
        sdbgGrupos.AddItem "0" & Chr(m_lSeparador) & oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & oGrupo.Id
        
        
    Next
    
    'PROCESO
    ' Si la distribuci�n o alguno de los presupuestos est� definido a nivel de proceso
    ' se seleccionan todos los grupos (sin �tems). Tanto si son obligatorios como si no.
    If m_oProcesoSeleccionado.DefDistribUON = EnProceso Or (gParametrosGenerales.gbUsarPres1 And m_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso) Or (gParametrosGenerales.gbUsarPres2 And m_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso) Or (gParametrosGenerales.gbUsarPres3 And m_oProcesoSeleccionado.DefPresTipo1 = EnProceso) Or (gParametrosGenerales.gbUsarPres4 And m_oProcesoSeleccionado.DefPresTipo2 = EnProceso) Then
        m_bPresEnProce = True
        SeleccionarTodosLosGrupos
    End If
If sdbgGrupos.Columns(3).Visible Then
Else

End If
    
    
    'GRUPO
    ' Si alg�n presupuesto o la distribuci�n est�n a nivel de grupo,
    ' se marcan los grupos para los que haya definido alg�n pres o distrib (sin items)
    ' y se marcan los grupos con items para los que no est� definido
    If m_oProcesoSeleccionado.DefDistribUON = EnGrupo Then
        sdbgGrupos.MoveFirst
        For Each oGrupo In m_oProcesoSeleccionado.Grupos
            If oGrupo.DefDistribUON Then
                sdbgGrupos.Columns("INC").Value = "-1"
            Else
                sdbgGrupos.Columns("INC").Value = "-1"
                sdbgGrupos.Columns("ITEMS").Value = "-1"
            End If
            sdbgGrupos.MoveNext
        Next
    End If
    If gParametrosGenerales.gbUsarPres1 Then
        If m_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo Then
            sdbgGrupos.MoveFirst
            For Each oGrupo In m_oProcesoSeleccionado.Grupos
                If oGrupo.DefPresAnualTipo1 Then
                    sdbgGrupos.Columns("INC").Value = "-1"
                Else
                    sdbgGrupos.Columns("INC").Value = "-1"
                    sdbgGrupos.Columns("ITEMS").Value = "-1"
                End If
                sdbgGrupos.MoveNext
            Next
        End If
    End If
    If gParametrosGenerales.gbUsarPres2 Then
        If m_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo Then
            sdbgGrupos.MoveFirst
            For Each oGrupo In m_oProcesoSeleccionado.Grupos
                If oGrupo.DefPresAnualTipo2 Then
                    sdbgGrupos.Columns("INC").Value = "-1"
                Else
                    sdbgGrupos.Columns("INC").Value = "-1"
                    sdbgGrupos.Columns("ITEMS").Value = "-1"
                End If
                sdbgGrupos.MoveNext
            Next
        End If
    End If
    If gParametrosGenerales.gbUsarPres3 Then
        If m_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then
            sdbgGrupos.MoveFirst
            For Each oGrupo In m_oProcesoSeleccionado.Grupos
                If oGrupo.DefPresTipo1 Then
                    sdbgGrupos.Columns("INC").Value = "-1"
                Else
                    sdbgGrupos.Columns("INC").Value = "-1"
                    sdbgGrupos.Columns("ITEMS").Value = "-1"
                End If
                sdbgGrupos.MoveNext
            Next
        End If
    End If
    If gParametrosGenerales.gbUsarPres4 Then
        If m_oProcesoSeleccionado.DefPresTipo2 = EnGrupo Then
            sdbgGrupos.MoveFirst
            For Each oGrupo In m_oProcesoSeleccionado.Grupos
                If oGrupo.DefPresTipo2 Then
                    sdbgGrupos.Columns("INC").Value = "-1"
                Else
                    sdbgGrupos.Columns("INC").Value = "-1"
                    sdbgGrupos.Columns("ITEMS").Value = "-1"
                End If
                sdbgGrupos.MoveNext
            Next
        End If
    End If
    
    'ITEM
    'Si alguno de los presupuestos o la distribuci�n est�n a nivel de �tem,
    'se marcan los grupos con sus �tems.Si no es obligatorio no se marca nada
    chkUON.Value = vbChecked
    If m_oProcesoSeleccionado.DefDistribUON = EnItem Then
        m_bPresEnItem = True
        SeleccionarTodosLosItems
        '''No tiene sentido no dejar desseleccionarlo
        '''sdbgGrupos.Columns("ITEMS").Locked = True
    End If
    '''No se deja deseleccionar ning�n grupo - No tiene sentido
    '''sdbgGrupos.Columns("INC").Locked = True
    
    If gParametrosGenerales.gbUsarPres1 Then
        If gParametrosGenerales.gbOBLPP Then
            chkPresAnu1.Value = vbChecked
    
            If Not m_bPresEnItem Then
                If m_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem Then
                    m_bPresEnItem = True
                    SeleccionarTodosLosItems
                    '''sdbgGrupos.Columns("ITEMS").Locked = True
                End If
            End If
            '''No se deja deseleccionar ning�n grupo
            '''sdbgGrupos.Columns("INC").Locked = True
        End If
    End If
    If gParametrosGenerales.gbUsarPres2 Then
        If gParametrosGenerales.gbOBLPC Then
            chkPresAnu2.Value = vbChecked
            If Not m_bPresEnItem Then
                If m_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem Then
                    m_bPresEnItem = True
                    SeleccionarTodosLosItems
                    '''sdbgGrupos.Columns("ITEMS").Locked = True
                End If
            End If
            'No se deja deseleccionar ning�n grupo
            '''sdbgGrupos.Columns("INC").Locked = True
        End If
    End If
    If gParametrosGenerales.gbUsarPres3 Then
        If gParametrosGenerales.gbOBLPres3 Then
            chkPres1.Value = vbChecked
    
            If Not m_bPresEnItem Then
                If m_oProcesoSeleccionado.DefPresTipo1 = EnItem Then
                    m_bPresEnItem = True
                    SeleccionarTodosLosItems
                    '''sdbgGrupos.Columns("ITEMS").Locked = True
                End If
            End If
            'No se deja deseleccionar ning�n grupo
            '''sdbgGrupos.Columns("INC").Locked = True
        End If
    End If
    If gParametrosGenerales.gbUsarPres4 Then
        If gParametrosGenerales.gbOBLPres4 Then
            chkPres2.Value = vbChecked
            If Not m_bPresEnItem Then
                If m_oProcesoSeleccionado.DefPresTipo2 = EnItem Then
                    m_bPresEnItem = True
                    SeleccionarTodosLosItems
                    '''sdbgGrupos.Columns("ITEMS").Locked = True
                End If
            End If
            'No se deja deseleccionar ning�n grupo
            '''sdbgGrupos.Columns("INC").Locked = True
        End If
    End If
        
    sdbgGrupos.Update
        
    chkVistas(0).Enabled = HabilitarVistasPlantilla
    If chkVistas(0).Enabled = False Then
        chkVistas(0).Value = vbUnchecked
    Else
        chkVistas(0).Value = vbChecked
    End If
    chkVistas(1).Enabled = m_oProcesoSeleccionado.ExistenVistasDeResponsable
    If chkVistas(1).Enabled = False Then chkVistas(1).Value = vbUnchecked
    
    chkExcluidos.Enabled = m_oProcesoSeleccionado.ExistenItemsExcluidos
    If chkExcluidos.Enabled = False Then chkExcluidos.Value = vbUnchecked
    
    
    If m_oProcesoSeleccionado.Estado >= Conproveasignados And m_oProcesoSeleccionado.AdminPublica = False Then
        chkProvesAsig.Visible = True
        'Si hay alg�n item seleccionado lo habilito
        If HayItemsSeleccionados Then
            chkProvesAsig.Enabled = True
        Else
            chkProvesAsig.Enabled = False
        End If
    Else
        chkProvesAsig.Visible = False
    End If
    If Not frmPROCE.g_bVal Then chkProvesAsig.Visible = False 'Si no hay permiso de validaci�n
    
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub SeleccionarTodosLosItems()
Dim lngGrupos As Long

    sdbgGrupos.MoveFirst
    For lngGrupos = 1 To sdbgGrupos.Rows
        sdbgGrupos.Columns("INC").Value = "-1"
        sdbgGrupos.Columns("ITEMS").Value = "-1"
        
        If sdbgGrupos.Columns("ESC").Visible Then
            'Comprobar si hay items con cantidad o preupuesto unitario a null y en ese caso, obligamos
            ' a utilizar los escalados
            If m_oProcesoSeleccionado.Grupos.Item(CStr(sdbgGrupos.Columns("COD").Value)).HayItemsANull Then
                sdbgGrupos.Columns("ESC").Value = True
            End If
        End If
        sdbgGrupos.MoveNext
    Next

End Sub

Private Sub SeleccionarTodosLosGrupos()
Dim lngGrupos As Long

    sdbgGrupos.MoveFirst
    For lngGrupos = 1 To sdbgGrupos.Rows
        sdbgGrupos.Columns("INC").Value = "-1"
        sdbgGrupos.MoveNext
    Next

End Sub

Private Sub chkAtrib_Click()
    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkAtrib.Value = vbUnchecked
            chkVistas(0).Value = vbUnchecked
            chkVistas(1).Value = vbUnchecked
        Else
            If chkAtrib.Value = vbUnchecked Then
                chkVistas(0).Value = vbUnchecked
                chkVistas(1).Value = vbUnchecked
            End If
        End If
    End If
End Sub
Private Sub chkVistas_Click(Index As Integer)
If Not m_bRespetarChecks Then
    If sdbcProceCod.Value = "" Then
        chkVistas(Index).Value = vbUnchecked
        Exit Sub
    Else
        If chkAtrib.Value = vbUnchecked Then
            chkVistas(Index).Value = vbUnchecked
            Exit Sub
        End If
    End If
    If chkVistas(Index).Value = vbChecked Then
        If Index = 1 Then
            chkVistas(0).Value = vbUnchecked
        Else
            chkVistas(1).Value = vbUnchecked
        End If
    End If
End If
End Sub

Private Sub chkPersonas_Click()
    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkPersonas.Value = vbUnchecked
        End If
    End If
End Sub

Private Sub chkPresAnu1_Click()

    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkPresAnu1.Value = vbUnchecked
        Else
            If gParametrosGenerales.gbOBLPP Then
                If chkPresAnu1.Value = vbUnchecked Then
                    m_bRespetarChecks = True
                    chkPresAnu1.Value = vbChecked
                    m_bRespetarChecks = False
                End If
            End If
        End If
    End If

End Sub


Private Sub chkPresAnu2_Click()

    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkPresAnu2.Value = vbUnchecked
        Else
            If gParametrosGenerales.gbOBLPC Then
                If chkPresAnu2.Value = vbUnchecked Then
                    m_bRespetarChecks = True
                    chkPresAnu2.Value = vbChecked
                    m_bRespetarChecks = False
                End If
            End If
        End If
    End If

End Sub

Private Sub chkPres1_Click()

    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkPres1.Value = vbUnchecked
        Else
            If gParametrosGenerales.gbOBLPres3 Then
                If chkPres1.Value = vbUnchecked Then
                    m_bRespetarChecks = True
                    chkPres1.Value = vbChecked
                    m_bRespetarChecks = False
                End If
            End If
        End If
    End If

End Sub

Private Sub chkPres2_Click()

    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkPres2.Value = vbUnchecked
        Else
            If gParametrosGenerales.gbOBLPres4 Then
                If chkPres2.Value = vbUnchecked Then
                    m_bRespetarChecks = True
                    chkPres2.Value = vbChecked
                    m_bRespetarChecks = False
                End If
            End If
        End If
    End If
End Sub

Private Sub chkExcluidos_Click()
    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkExcluidos.Value = vbUnchecked
        Else
            If Not HayItemsSeleccionados Then
                chkExcluidos.Value = vbUnchecked
            End If
        End If
    End If
End Sub
Private Sub chkUON_Click()

    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkUON.Value = vbUnchecked
        Else
            If chkUON.Value = vbUnchecked Then
                m_bRespetarChecks = True
                chkUON.Value = vbChecked
                m_bRespetarChecks = False
            End If
        End If
    End If
End Sub

Private Sub chkEspec_Click()
    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkEspec.Value = vbUnchecked
        End If
    End If
End Sub

Private Sub chkProvesAsig_Click()
    If Not m_bRespetarChecks Then
        If sdbcProceCod.Value = "" Then
            chkProvesAsig.Value = vbUnchecked
        End If
    End If
End Sub

Private Sub optPresItem_Click(Index As Integer)
If optPresItem(Index).Value = True Then
    If Index = 1 Then
        If Frame4.Visible = False Then
            Frame4.Visible = True
            optFrame4(0).Value = True
        End If
    Else
        Frame4.Visible = False
    End If
End If
End Sub

''' <summary>
''' hace la copia
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
Dim udtTeserror As TipoErrorSummit
Dim oProce As cProceso
Dim i As Integer
Dim vGrupos() As Variant
Dim bGrupos As Boolean
Dim bItems As Boolean
Dim bEscalados As Boolean
Dim irespuesta As Integer
Dim bGuardar As Boolean
Dim iGrupos As Integer
Dim vbm As Variant
Dim iPresItem As Byte
Dim iTipoPrecio As Byte

    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    Set oProce = oFSGSRaiz.Generar_CProceso
    oProce.Anyo = Year(Date)
    oProce.GMN1Cod = m_oProcesoSeleccionado.GMN1Cod
    If frmPROCE.g_bRAperMatComp Then
        udtTeserror = oProce.DevolverPosibleCodigo(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    Else
        udtTeserror = oProce.DevolverPosibleCodigo
    End If
    Set oProce = Nothing
    If (udtTeserror.NumError <> TESnoerror) Then
        basErrores.TratarError udtTeserror
        Exit Sub
    End If
    
    ReDim Preserve vGrupos(0 To 3, 0)
    
    iGrupos = 0
    For i = 0 To sdbgGrupos.Rows - 1
        vbm = sdbgGrupos.AddItemBookmark(i)
        If sdbgGrupos.Columns("INC").CellValue(vbm) <> "0" Then
            bGrupos = True
            ReDim Preserve vGrupos(0 To 3, iGrupos)
            vGrupos(0, iGrupos) = sdbgGrupos.Columns("COD").CellValue(vbm)
            vGrupos(2, iGrupos) = sdbgGrupos.Columns("ID").CellValue(vbm)
            
            If sdbgGrupos.Columns("ITEMS").CellValue(vbm) <> "0" Then
                bItems = True
                vGrupos(1, iGrupos) = 1
            Else
                If chkProvesAsig.Value = vbChecked Then
                    oMensajes.ImposibleCopiarProvesAsigAProce
                    Exit Sub
                Else
                    vGrupos(1, iGrupos) = 0
                End If
            End If
            
            If sdbgGrupos.Columns("ESC").CellValue(vbm) <> "0" Then
                bEscalados = True
                vGrupos(3, iGrupos) = 1
            Else
                vGrupos(3, iGrupos) = 0
            End If
            
            iGrupos = iGrupos + 1
        End If
    Next
    
    If Not bGrupos Then
        irespuesta = oMensajes.PreguntaCopiarProceSinGrupo
        If irespuesta = vbNo Then Exit Sub
        If gParametrosInstalacion.gsGrupoCod = "" Then
            If gParametrosGenerales.gsGrupoCod = "" Then
                oMensajes.ImposibleCopiarProceNoExisteGrupoDefecto
                Exit Sub
            Else
                gParametrosInstalacion.gsGrupoCod = gParametrosGenerales.gsGrupoCod
                bGuardar = True
            End If
        End If
        If gParametrosInstalacion.gsGrupoDen = "" Then
            If gParametrosGenerales.gsGrupoDen = "" Then
                oMensajes.ImposibleCopiarProceNoExisteGrupoDefecto
                Exit Sub
            Else
                gParametrosInstalacion.gsGrupoDen = gParametrosGenerales.gsGrupoDen
                bGuardar = True
            End If
        End If
        If gParametrosInstalacion.gsDestino = "" Then
            If gParametrosGenerales.gsDESTDEF <> "" Then
                gParametrosInstalacion.gsDestino = gParametrosGenerales.gsDESTDEF
                bGuardar = True
            End If
        End If
        
        If bGuardar Then
            g_GuardarParametrosIns = True
        End If
        
        bGuardar = False
        
        'Si vamos a a�adir el grupo por defecto pasamos el destino de la
        'instalaci�n
        ReDim Preserve vGrupos(0 To 3, 0)
        vGrupos(0, 0) = gParametrosInstalacion.gsDestino
    End If
    
    Screen.MousePointer = vbHourglass
    Set oProce = m_oProcesoSeleccionado
    oProce.UsuAper = basOptimizacion.gvarCodUsuario
    'Si el usuario no tiene permiso de modificar las opciones de subasta el proceso no puede ser de subasta
    If Not frmPROCE.g_bModifSubasta Then
        If oProce.ModoSubasta Then
            oProce.ModoSubasta = False
            oProce.Tipo = 0
            oProce.modo = TipoAmbitoProceso.AmbItem
            oProce.PublicarFec = Null
            oProce.MinutosEspera = 0
            oProce.FecApeSobre = Null
            oProce.MinJaponesa = Null
            oProce.MuestraProveedor = False
            oProce.MuestraPrecioPuja = False
            oProce.MuestraDetalle = False
            oProce.VerDesdePrimeraPuja = False
            oProce.BajadaMinimaPujas = False
            oProce.BajMinGanadorTipo = 0
            oProce.BajMinProve = False
            oProce.BajMinProveTipo = 0
            oProce.NotifAutom = False
            Set oProce.TextoFin = Nothing
            oProce.NotificarEventos = 0
            oProce.FechaInicioSubasta = Null
            oProce.PrecSalida = Null
            oProce.MinPujGanador = Null
            oProce.MinPujProve = Null
            Set oProce.TextoFin = Nothing
        End If
    End If
    
    'Si el usuario que abre el proceso es un comprador lo asignamos como "responsable del proceso"
    If basOptimizacion.gTipoDeUsuario = comprador Then
        Set oProce.responsable = oFSGSRaiz.generar_CComprador
        oProce.responsable.Cod = basOptimizacion.gCodCompradorUsuario
        oProce.responsable.codEqp = basOptimizacion.gCodEqpUsuario
    Else
        Set oProce.responsable = Nothing
    End If
        
    If optPresItem(0).Value = True Then iPresItem = 0
    If optPresItem(1).Value = True Then
        iPresItem = 1
        If optFrame4(0).Value Then iTipoPrecio = 0
        If optFrame4(1).Value Then iTipoPrecio = 1
        If optFrame4(2).Value Then iTipoPrecio = 2
        If optFrame4(3).Value Then iTipoPrecio = 3
    End If
    If optPresItem(2).Value = True Then iPresItem = 2
                          
    oProce.ContratoOrigen = g_lContratoOrigen
            
    udtTeserror = oProce.CopiarProceso(basOptimizacion.gvarCodUsuario, chkAtrib.Value, chkPersonas.Value, chkUON.Value, chkPresAnu1.Value, chkPresAnu2.Value, chkPres1.Value, chkPres2.Value, chkEspec.Value, chkProvesAsig.Value, bGrupos, bItems, vGrupos, chkVistas(0).Value, chkVistas(1).Value, chkExcluidos.Value, optFechas(0).Value, iPresItem, iTipoPrecio, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, basOptimizacion.gCodPersonaUsuario, bEscalados)
    If udtTeserror.NumError <> TESnoerror And udtTeserror.NumError <> TESCambioUnidadMedida _
    And udtTeserror.NumError <> TESProcesoNoCumpleNivelMinimoUO And udtTeserror.NumError <> TESProcesoNoCumpleNivelMinimoUOProve _
    And udtTeserror.NumError <> TESProcesoNoCumpleIgnorar Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError udtTeserror
        Set oProce = Nothing
        Exit Sub
    Else
        RegistrarAccion AccionesSummit.AccProceAnya, "Anyo:" & CStr(oProce.Anyo) & "GMN1:" & oProce.GMN1Cod & "COD:" & oProce.Cod
    End If
    Screen.MousePointer = vbNormal

    Unload Me
    oMensajes.ProcesoCopiadoOK oProce.Anyo & "/" & oProce.GMN1Cod & "/" & oProce.Cod, udtTeserror.NumError, gParametrosGenerales.giNIVDIST
    If basParametros.gParametrosGenerales.gbOBLUnidadMedida And udtTeserror.NumError = TESCambioUnidadMedida Then
        oMensajes.CambioUnidadesItems
    End If
        
    frmPROCE.sdbcAnyo.Value = oProce.Anyo
    frmPROCE.sdbcGMN1_4Cod.Value = oProce.GMN1Cod
    frmPROCE.sdbcGMN1_4Cod.Columns(0).Text = ""
    frmPROCE.sdbcGMN1_4Cod_Validate False
    frmPROCE.ProceSelector1.Seleccion = 1
    frmPROCE.sdbcProceCod.Value = oProce.Cod
    frmPROCE.sdbcProceCod_Validate False
                            
    Set oProce = Nothing

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Deactivate()
    If m_bUnloaded = False Then
        If Me.Visible Then Me.SetFocus
    End If
End Sub

''' <summary>
''' Nos carga el formulario
''' </summary>
''' <remarks>Tiempo m�ximo:0,4seg.</remarks>
Private Sub Form_Load()

    m_bUnloaded = False
    'Me.Top = 3000
    'Me.Left = 3000
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Height = 7875
    Me.Width = 9840
    
    CargarRecursos

    PonerFieldSeparator Me
    
    CargarAnyos
    
    Set m_oProcesos = oFSGSRaiz.generar_CProcesos
    
    m_iAnyaItemsGrupo = 0
    
    If Not gParametrosGenerales.gbUsarPres1 And Not gParametrosGenerales.gbUsarPres2 And Not gParametrosGenerales.gbUsarPres3 And Not gParametrosGenerales.gbUsarPres4 Then
        chkExcluidos.Left = chkPresAnu1.Left
        chkUON.Left = chkPresAnu2.Left
        chkEspec.Left = chkPres1.Left
        chkProvesAsig.Left = chkPres2.Left
        chkPresAnu1.Visible = False
        chkPresAnu2.Visible = False
        chkPres1.Visible = False
        chkPres2.Visible = False
    Else
        If Not gParametrosGenerales.gbUsarPres1 Then
            chkPresAnu1.Visible = False
            chkProvesAsig.Top = chkEspec.Top
            chkEspec.Top = chkUON.Top
            chkUON.Top = chkExcluidos.Top
            chkExcluidos.Top = chkPres2.Top
            chkExcluidos.Left = chkPres2.Left
            chkPres2.Top = chkPres1.Top
            chkPres1.Top = chkPresAnu2.Top
            chkPresAnu2.Top = chkPresAnu1.Top
        End If
        If Not gParametrosGenerales.gbUsarPres2 Then
            chkPresAnu2.Visible = False
            chkProvesAsig.Top = chkEspec.Top
            chkEspec.Top = chkUON.Top
            chkUON.Top = chkExcluidos.Top
            chkExcluidos.Top = chkPres2.Top
            chkExcluidos.Left = chkPres2.Left
            chkPres2.Top = chkPres1.Top
            chkPres1.Top = chkPresAnu2.Top
        End If
        If Not gParametrosGenerales.gbUsarPres3 Then
            chkPres1.Visible = False
            chkProvesAsig.Top = chkEspec.Top
            chkEspec.Top = chkUON.Top
            chkUON.Top = chkExcluidos.Top
            chkExcluidos.Top = chkPres2.Top
            chkExcluidos.Left = chkPres2.Left
            chkPres2.Top = chkPres1.Top
        End If
        If Not gParametrosGenerales.gbUsarPres4 Then
            chkPres2.Visible = False
            chkProvesAsig.Top = chkEspec.Top
            chkEspec.Top = chkUON.Top
            chkUON.Top = chkExcluidos.Top
            chkExcluidos.Top = chkPres2.Top
            chkExcluidos.Left = chkPres2.Left
        End If
    End If
    

    If g_sProceso <> "" Then
        CargarProcesoContrato
    End If
    
End Sub

Private Sub Form_Resize()
On Error Resume Next
picEdit.Width = Me.Width
cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 120
cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + 120
sdbgGrupos.Width = Me.Width - 360
sdbgGrupos.Height = Me.Height - sdbgGrupos.Top - Frame1.Height - Frame2.Height - Frame3.Height - picEdit.Height - 870
Frame1.Top = sdbgGrupos.Top + sdbgGrupos.Height + 135
Frame2.Top = Frame1.Top + Frame1.Height + 135
Frame3.Top = Frame2.Top + Frame2.Height + 135
sdbgGrupos.Columns(0).Width = sdbgGrupos.Width * 0.1
sdbgGrupos.Columns(1).Width = sdbgGrupos.Width * 0.17
If sdbgGrupos.Columns(4).Visible Then
    sdbgGrupos.Columns(2).Width = sdbgGrupos.Width * 0.46
    sdbgGrupos.Columns(3).Width = sdbgGrupos.Width * 0.12
    sdbgGrupos.Columns(4).Width = sdbgGrupos.Width * 0.12
Else
    sdbgGrupos.Columns(2).Width = sdbgGrupos.Width * 0.58
    sdbgGrupos.Columns(3).Width = sdbgGrupos.Width * 0.12
    sdbgGrupos.Columns(4).Width = 0

End If
End Sub

Private Sub Form_Unload(Cancel As Integer)

    m_bUnloaded = True
    Set m_oProcesoSeleccionado = Nothing
    Set m_oProcesos = Nothing
    m_bPresEnProce = False
    m_bPresEnItem = False
    Me.Visible = False
    
End Sub


Private Sub ProceSelector1_Click(ByVal Opcion As SelectorDeProcesos.PSSeleccion)
    sdbcProceCod.Value = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    sdbcProceDen.RemoveAll
End Sub

Private Sub sdbcAnyo_Click()
    sdbcProceCod.Value = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    sdbcProceDen.RemoveAll
End Sub

Private Sub CargarAnyos()
'**************************************************************************
' Carga los a�os en el combo de a�os del proceso. Desde el actual menos 10
' hasta el actual m�s 10.
'**************************************************************************
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)

    For iInd = iAnyoActual - 10 To iAnyoActual + 10

        sdbcAnyo.AddItem iInd

    Next

    sdbcAnyo.Value = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7

End Sub

Private Sub sdbcGMN1_4Cod_Change()
    If Not m_bGMN1RespetarCombo Then
        m_bGMN1CargarComboDesde = True
        Set m_oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        sdbcProceCod.Value = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.Value = ""
        sdbcProceDen.RemoveAll
    End If
End Sub

Private Sub sdbcGMN1_4Cod_Click()
    If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod.Value = ""
    End If
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Value = ""
        Exit Sub
    End If

    If sdbcGMN1_4Cod.Value = "" Then Exit Sub

    m_bGMN1RespetarCombo = True
    sdbcGMN1_4Cod.Value = sdbcGMN1_4Cod.Columns(0).Value
    m_bGMN1RespetarCombo = False

    m_bGMN1CargarComboDesde = False

    Set m_oProcesoSeleccionado = Nothing
    LimpiarDatosProceso
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceDen.Value = ""

End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer

    Screen.MousePointer = vbHourglass

    sdbcGMN1_4Cod.RemoveAll

    Set m_oGruposMN1 = Nothing
    Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1

    If m_bGMN1CargarComboDesde Then

        If g_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod.Value), , , False, , g_bRUsuAper, g_bRCompResp, basOptimizacion.gCodPersonaUsuario, , sdbcAnyo.Value)
        Else
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod.Value), , , False
        End If
    Else
        If g_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , g_bRUsuAper, g_bRCompResp, basOptimizacion.gCodPersonaUsuario, , sdbcAnyo.Value)
        Else
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
             m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If

    Codigos = m_oGruposMN1.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next

    If m_bGMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Value)
    sdbcGMN1_4Cod.Refresh

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcGMN1_4Cod.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
Dim oIBaseDatos As IBaseDatos
Dim oGMN1 As CGrupoMatNivel1
Dim oIMAsig As IMaterialAsignado
Dim scod1 As String


    If sdbcGMN1_4Cod.Value = "" Then Exit Sub

    If sdbcGMN1_4Cod.Value = sdbcGMN1_4Cod.Columns(0).Value Then
        Exit Sub
    End If

    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If g_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , True, False, , g_bRUsuAper, g_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        scod1 = sdbcGMN1_4Cod.Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Value))
        If m_oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Value = ""
        Else
            m_bGMN1CargarComboDesde = False
        End If

    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1

        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos

        If Not bExiste Then
            sdbcGMN1_4Cod.Value = ""
        Else
            m_bGMN1CargarComboDesde = False
        End If

    End If
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set m_oGruposMN1 = Nothing
    Set oIMAsig = Nothing

    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProceCod_Change()
    
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcProceDen.Value = ""
        Set m_oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
    End If

End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceCod.Value = ""
    End If
End Sub

Private Sub sdbcProceCod_CloseUp()
    If sdbcProceCod.Value = "..." Or sdbcProceCod.Value = "" Then
        sdbcProceCod.Value = ""
        Exit Sub
    End If

    m_bRespetarCombo = True
    sdbcProceDen.Value = sdbcProceCod.Columns(1).Value
    sdbcProceCod.Value = sdbcProceCod.Columns(0).Value
    m_bRespetarCombo = False

    ProcesoSeleccionado
    m_bCargarComboDesde = False
End Sub


Private Sub sdbcProceCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim udtDesdeEst As TipoEstadoProceso
    Dim udtHastaEst As TipoEstadoProceso
    Dim bCerrado As Boolean
    Dim lIdPerfil As Long

    sdbcProceCod.RemoveAll

    If sdbcGMN1_4Cod.Value = "" Then Exit Sub

    Screen.MousePointer = vbHourglass

    Set m_oProcesoSeleccionado = Nothing
    bCerrado = False
    
    Select Case ProceSelector1.Seleccion

        Case PSSeleccion.PSPendientes
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.ConItemsSinValidar

        Case PSSeleccion.PSAbiertos
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.PreadjYConObjNotificados

        Case PSSeleccion.PSParcialCerrados
            udtDesdeEst = TipoEstadoProceso.ParcialmenteCerrado
            udtHastaEst = TipoEstadoProceso.ParcialmenteCerrado

        Case PSSeleccion.PSCerrados
            udtDesdeEst = TipoEstadoProceso.conadjudicaciones
            udtHastaEst = TipoEstadoProceso.Cerrado
            bCerrado = True
        
        Case PSSeleccion.PSTodos
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If m_bCargarComboDesde Then
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtDesdeEst, udtHastaEst, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod, , , , val(sdbcProceCod.Value), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, g_bRMat, g_bRAsig, g_bRCompResp, g_bREqpAsig, g_bRUsuAper, g_bRUsuUON, g_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , bCerrado, True, g_bSoloInvitado, , g_bRPerfUON, lIdPerfil
    Else
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtDesdeEst, udtHastaEst, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, g_bRMat, g_bRAsig, g_bRCompResp, g_bREqpAsig, g_bRUsuAper, g_bRUsuUON, g_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , bCerrado, True, g_bSoloInvitado, , g_bRPerfUON, lIdPerfil
    End If


    If ProceSelector1.Seleccion = PSSeleccion.PSCerrados Then
        Codigos = m_oProcesos.DevolverLosCodigos(True)
        sdbcProceCod.Columns("FecReapeRT").Width = 1100
        sdbcProceCod.Columns("FecReapeRT").Visible = True
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProceCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Id(i) & Chr(m_lSeparador) & m_oProcesos.Item(i + 1).FechaReapertura
        Next
    Else
        Codigos = m_oProcesos.DevolverLosCodigos
        sdbcProceCod.Columns("FecReapeRT").Visible = False
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProceCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & ""
        Next
    End If

    If Not m_oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Value)
    sdbcProceCod.Refresh

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProceCod_InitColumnProps()
    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProceCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcProceCod.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcProceCod.Rows - 1
            bm = sdbcProceCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Sub sdbcProceCod_Validate(Cancel As Boolean)
    Dim udtDesdeEst As TipoEstadoProceso
    Dim udtHastaEst As TipoEstadoProceso
    Dim bCerrado As Boolean
    Dim lIdPerfil As Long

    If sdbcProceCod.Value = "" Then Exit Sub

    If sdbcGMN1_4Cod.Value = "" Then
        sdbcProceCod.Value = ""
        Screen.MousePointer = vbNormal
        Set m_oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        Exit Sub
    End If

    If Not IsNumeric(sdbcProceCod.Value) Then
        sdbcProceCod.Value = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sIdiCodigo
        Set m_oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        Exit Sub
    End If

    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Value = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sIdiProceso
        Set m_oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        Exit Sub
    End If

    If sdbcProceCod.Value = sdbcProceCod.Columns(0).Value Then
        m_bRespetarCombo = True
        sdbcProceDen.Value = sdbcProceCod.Columns(1).Value
        m_bRespetarCombo = False
        If m_oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    If sdbcProceCod.Value = sdbcProceDen.Columns(1).Value Then
        m_bRespetarCombo = True
        sdbcProceDen.Value = sdbcProceDen.Columns(0).Value
        m_bRespetarCombo = False
        If m_oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    bCerrado = False

    ''' Solo continuamos si existe el proceso
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.ConItemsSinValidar
        Case PSSeleccion.PSAbiertos
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtDesdeEst = TipoEstadoProceso.ParcialmenteCerrado
            udtHastaEst = TipoEstadoProceso.ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtDesdeEst = TipoEstadoProceso.conadjudicaciones
            udtHastaEst = TipoEstadoProceso.Cerrado
            bCerrado = True
        Case PSSeleccion.PSTodos
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        
    m_oProcesos.CargarTodosLosProcesosDesde 1, udtDesdeEst, udtHastaEst, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , True, False, basOptimizacion.gCodEqpUsuario _
                , basOptimizacion.gCodCompradorUsuario, g_bRMat, g_bRAsig, g_bRCompResp, g_bREqpAsig, g_bRUsuAper, g_bRUsuUON, g_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario _
                , , , bCerrado, True, g_bSoloInvitado, , g_bRPerfUON, lIdPerfil

    If m_oProcesos.Count = 0 Then
        sdbcProceCod.Value = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sIdiProceso
        Set m_oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
    Else
        m_bRespetarCombo = True
        sdbcProceDen.Value = m_oProcesos.Item(1).Den

        sdbcProceCod.Columns(0).Value = sdbcProceCod.Value
        sdbcProceCod.Columns(1).Value = sdbcProceDen.Value

        m_bRespetarCombo = False
        Set m_oProcesoSeleccionado = Nothing
        Set m_oProcesoSeleccionado = m_oProcesos.Item(1)
        m_bCargarComboDesde = False
        ProcesoSeleccionado
    End If
        
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProceDen_Change()
    
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcProceCod.Value = ""
        Set m_oProcesoSeleccionado = Nothing
        LimpiarDatosProceso
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
    End If

End Sub

Private Sub sdbcProceDen_Click()
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen.Value = ""
        sdbcProceCod.Value = ""
    End If
End Sub

Private Sub sdbcProceDen_CloseUp()
    If sdbcProceDen.Value = "....." Or sdbcProceDen = "" Then
        sdbcProceDen.Value = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcProceCod.Value = sdbcProceDen.Columns(1).Value
    sdbcProceDen.Value = sdbcProceDen.Columns(0).Value
    m_bRespetarCombo = False
    
    ProcesoSeleccionado
    m_bCargarComboDesde = False

End Sub


Private Sub sdbcProceDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim udtDesdeEst As TipoEstadoProceso
    Dim udtHastaEst As TipoEstadoProceso
    Dim bCerrado As Boolean
    Dim lIdPerfil As Long

    sdbcProceDen.RemoveAll
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set m_oProcesoSeleccionado = Nothing
    bCerrado = False
    
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.ConItemsSinValidar
        Case PSSeleccion.PSAbiertos
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtDesdeEst = TipoEstadoProceso.ParcialmenteCerrado
            udtHastaEst = TipoEstadoProceso.ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtDesdeEst = TipoEstadoProceso.conadjudicaciones
            udtHastaEst = TipoEstadoProceso.Cerrado
            bCerrado = True
        Case PSSeleccion.PSTodos
            udtDesdeEst = TipoEstadoProceso.sinitems
            udtHastaEst = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If m_bCargarComboDesde Then
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtDesdeEst, udtHastaEst, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , , , , sdbcProceDen, False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, g_bRMat, g_bRAsig, g_bRCompResp, g_bREqpAsig, g_bRUsuAper, g_bRUsuUON, g_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , bCerrado, True, g_bSoloInvitado, , g_bRPerfUON, lIdPerfil
    Else
        m_oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtDesdeEst, udtHastaEst, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, g_bRMat, g_bRAsig, g_bRCompResp, g_bREqpAsig, g_bRUsuAper, g_bRUsuUON, g_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , bCerrado, True, g_bSoloInvitado, , g_bRPerfUON, lIdPerfil
    End If
        
    If ProceSelector1.Seleccion = PSSeleccion.PSCerrados Then
        Codigos = m_oProcesos.DevolverLosCodigos(True)
        sdbcProceDen.Columns("FECREAPERT").Width = 1100
        sdbcProceDen.Columns("FECREAPERT").Visible = True
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProceDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Id(i) & Chr(m_lSeparador) & m_oProcesos.Item(i + 1).FechaReapertura
        Next
    Else
        Codigos = m_oProcesos.DevolverLosCodigos
        sdbcProceDen.Columns("FECREAPERT").Visible = False
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProceDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i) & Chr(m_lSeparador) & ""
        Next
    End If
    
    
    'If m_bCargarComboDesde And Not m_oProcesos.EOF Then
    If Not m_oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Value)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal


End Sub

Private Sub sdbcProceDen_InitColumnProps()
    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProceDen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProceDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceDen.Rows - 1
            bm = sdbcProceDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub LimpiarDatosProceso()

    sdbgGrupos.RemoveAll
    m_bRespetarChecks = True
    
    chkAtrib.Value = vbChecked
    chkVistas(0).Value = vbUnchecked
    chkVistas(1).Value = vbUnchecked
    chkPersonas.Value = vbChecked
    
    chkPresAnu1.Value = vbUnchecked
    chkPresAnu2.Value = vbUnchecked
    chkPres1.Value = vbUnchecked
    chkPres2.Value = vbUnchecked
    If gParametrosGenerales.gbUsarPres1 Then chkPresAnu1.Value = vbChecked
    If gParametrosGenerales.gbUsarPres2 Then chkPresAnu2.Value = vbChecked
    If gParametrosGenerales.gbUsarPres3 Then chkPres1.Value = vbChecked
    If gParametrosGenerales.gbUsarPres4 Then chkPres2.Value = vbChecked
    
    chkExcluidos.Value = vbUnchecked
    chkEspec.Value = vbChecked
    chkUON.Value = vbChecked
    chkProvesAsig.Value = vbUnchecked

    optFechas(0).Value = True
    optFechas(1).Value = False
    
    optPresItem(0).Value = False
    optPresItem(1).Value = True
    optFrame4(0).Value = True
    optFrame4(1).Value = False
    optFrame4(2).Value = False
    optFrame4(3).Value = False
    optPresItem(2).Value = False
    
    sdbgGrupos.Columns("INC").Locked = False
    sdbgGrupos.Columns("ITEMS").Locked = False
    
    m_bRespetarChecks = False

End Sub

Private Sub sdbgGrupos_AfterUpdate(RtnDispErrMsg As Integer)
    chkVistas(0).Enabled = HabilitarVistasPlantilla
    If chkVistas(0).Enabled = False Then chkVistas(0).Value = vbUnchecked
    'Si no copia todos los grupos se puede copiar las vistas de la plantilla pero el proceso no queda como de plantilla
    
End Sub

Private Sub sdbgGrupos_Change()
Dim bDist As Boolean
Dim bPresAnu1 As Boolean
Dim bPresAnu2 As Boolean
Dim bPres1 As Boolean
Dim bPres2 As Boolean
Dim sCadena As String
Dim vBookmark As Variant 'almacena la fila actual

    If sdbgGrupos.Col = 0 Then 'Incluir GRUPO
        'Si se deselecciona un grupo y la distrib o alguno de los presup est�
        'a nivel de proceso se avisa de que no se copiar� el dato
        If sdbgGrupos.Columns("INC").Value = "0" Then
            If m_bPresEnProce Then
                If m_oProcesoSeleccionado.DefDistribUON = EnProceso And chkUON.Value = vbChecked Then
                    bDist = True
                End If
                If gParametrosGenerales.gbUsarPres1 Then
                    If m_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso And chkPresAnu1.Value = vbChecked Then
                        bPresAnu1 = True
                    End If
                End If
                If gParametrosGenerales.gbUsarPres2 Then
                    If m_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso And chkPresAnu2.Value = vbChecked Then
                        bPresAnu2 = True
                    End If
                End If
                If gParametrosGenerales.gbUsarPres3 Then
                    If m_oProcesoSeleccionado.DefPresTipo1 = EnProceso And chkPres1.Value = vbChecked Then
                        bPres1 = True
                    End If
                End If
                If gParametrosGenerales.gbUsarPres4 Then
                    If m_oProcesoSeleccionado.DefPresTipo2 = EnProceso And chkPres2.Value = vbChecked Then
                        bPres2 = True
                    End If
                End If
                If bDist Or bPresAnu1 Or bPresAnu2 Or bPres1 Or bPres2 Then
                    oMensajes.AvisoRevisarPresDeProce gParametrosGenerales.gsPlurPres1, gParametrosGenerales.gsPlurPres2, gParametrosGenerales.gsPlurPres3, _
                                                      gParametrosGenerales.gsPlurPres4, bDist, bPresAnu1, bPresAnu2, bPres1, bPres2
                End If
            End If
            sdbgGrupos.Columns("ITEMS").Value = "0"
            vBookmark = sdbgGrupos.RowBookmark(sdbgGrupos.Row)
            'comprobrar si no hay ning�n item seleccionado
            If Not HayAlgunItemSeleccionado Then
                chkProvesAsig.Value = vbUnchecked
                chkProvesAsig.Enabled = False
            End If
            sdbgGrupos.Bookmark = vBookmark
        End If

    End If
    
    If sdbgGrupos.Col = 3 Then 'ITEMS
        If sdbgGrupos.Columns("INC").Value <> "-1" Then
            sdbgGrupos.Columns("ITEMS").Value = "0"
        Else
            If sdbgGrupos.Columns("ITEMS").Value = "-1" Then
                chkProvesAsig.Enabled = True
            Else
                vBookmark = sdbgGrupos.RowBookmark(sdbgGrupos.Row)
                'comprobrar si no hay ning�n item seleccionado
                If HayAlgunItemSeleccionado Then
                    chkProvesAsig.Enabled = True
                Else
                    chkProvesAsig.Value = vbUnchecked
                    chkProvesAsig.Enabled = False
                End If
                sdbgGrupos.Bookmark = vBookmark
            End If
            ' Al seleccionar un �tem se miran los presup y ditrib
            ' si alguno est� a nivel de proceso y su check est�
            ' seleccionado se marcan todos los �tems. Y al deseleccionar se deseleccionan todos.
            If m_oProcesoSeleccionado.DefDistribUON = EnProceso And chkUON.Value = vbChecked Then
                bDist = True
                sCadena = chkUON.caption
            End If
            If gParametrosGenerales.gbUsarPres1 Then
                If m_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso And chkPresAnu1.Value = vbChecked Then
                    bPresAnu1 = True
                    If sCadena = "" Then
                        sCadena = chkPresAnu1.caption
                    Else
                        sCadena = sCadena & "," & chkPresAnu1.caption
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres2 Then
                If m_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso And chkPresAnu2.Value = vbChecked Then
                    bPresAnu2 = True
                    If sCadena = "" Then
                        sCadena = chkPresAnu2.caption
                    Else
                        sCadena = sCadena & "," & chkPresAnu2.caption
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres3 Then
                If m_oProcesoSeleccionado.DefPresTipo1 = EnProceso And chkPres1.Value = vbChecked Then
                    bPres1 = True
                    If sCadena = "" Then
                        sCadena = chkPres1.caption
                    Else
                        sCadena = sCadena & "," & chkPres1.caption
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres4 Then
                If m_oProcesoSeleccionado.DefPresTipo2 = EnProceso And chkPres2.Value = vbChecked Then
                    bPres2 = True
                    If sCadena = "" Then
                        sCadena = chkPres2.caption
                    Else
                        sCadena = sCadena & "," & chkPres2.caption
                    End If
                End If
            End If
            If bDist Or bPresAnu1 Or bPresAnu2 Or bPres1 Or bPres2 Then
                If sdbgGrupos.Columns("ITEMS").Value = "-1" Then
                    If sdbgGrupos.Rows > 1 Then
                        'mensaje seleccionar todos
                        oMensajes.SelDesSelItemsProceClonar True, sCadena
                    End If
                    vBookmark = sdbgGrupos.RowBookmark(sdbgGrupos.Row)
                    SeleccionarTodosLosItems
                    sdbgGrupos.Bookmark = vBookmark
                Else
                    If sdbgGrupos.Rows > 1 Then
                        'mensaje deseleccionar todos
                        oMensajes.SelDesSelItemsProceClonar False, sCadena
                    End If
                    chkProvesAsig.Value = vbUnchecked
                    chkProvesAsig.Enabled = False
                    vBookmark = sdbgGrupos.RowBookmark(sdbgGrupos.Row)
                    DeseleccionarTodosLosItems
                    sdbgGrupos.Bookmark = vBookmark
                End If
            End If
        End If
        
        'Comprobar si hay items con cantidad o preupuesto unitario a null y en ese caso, obligamos
        ' a utilizar los escalados
        If m_oProcesoSeleccionado.Grupos.Item(CStr(sdbgGrupos.Columns("COD").Value)).HayItemsANull Then
            sdbgGrupos.Columns("ESC").Value = True
        End If
    
    End If
    If sdbgGrupos.Col = 4 Then 'ESCALADOS
        If sdbgGrupos.Columns("ESC").Value = True Then
            If Not m_oProcesoSeleccionado.Grupos.Item(CStr(sdbgGrupos.Columns("COD").Value)).UsarEscalados <> 0 Then
                sdbgGrupos.Columns("ESC").Value = False
                'El grupo no tiene escalados
            End If
        End If
        'Comprobar si hay items con cantidad o preupuesto unitario a null y en ese caso, obligamos
        ' a utilizar los escalados
        If m_oProcesoSeleccionado.Grupos.Item(CStr(sdbgGrupos.Columns("COD").Value)).HayItemsANull Then
            sdbgGrupos.Columns("ESC").Value = True
        End If
        
        
    End If
    
    sdbgGrupos.Update
    
End Sub


Private Sub CargarRecursos()
Dim Adores As Ador.Recordset

    On Error Resume Next

    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCECLONAR, basPublic.gParametrosInstalacion.gIdioma)

    If Not Adores Is Nothing Then

        caption = Adores(0).Value
        Adores.MoveNext
        lblAnyo.caption = Adores(0).Value
        Adores.MoveNext
        lblProce.caption = Adores(0).Value & ":"
        m_sIdiProceso = Adores(0).Value
        Adores.MoveNext
        sdbgGrupos.caption = Adores(0).Value
        Adores.MoveNext
        sdbgGrupos.Columns("INC").caption = Adores(0).Value
        Adores.MoveNext
        sdbgGrupos.Columns("COD").caption = Adores(0).Value
        m_sIdiCodigo = Adores(0).Value
        Adores.MoveNext
        sdbgGrupos.Columns("DEN").caption = Adores(0).Value
        sdbcProceCod.Columns("DEN").caption = Adores(0).Value
        sdbcProceDen.Columns("COD").caption = Adores(0).Value
        sdbcGMN1_4Cod.Columns("DEN").caption = Adores(0).Value
        Adores.MoveNext
        sdbgGrupos.Columns("ITEMS").caption = Adores(0).Value
        Adores.MoveNext
        chkAtrib.caption = Adores(0).Value
        Adores.MoveNext
        chkPersonas.caption = Adores(0).Value
        Adores.MoveNext
        chkUON.caption = Adores(0).Value
        Adores.MoveNext
        chkEspec.caption = Adores(0).Value
        Adores.MoveNext
        chkProvesAsig.caption = Adores(0).Value
        Adores.MoveNext
        cmdAceptar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        sdbcProceCod.Columns("COD").caption = Adores(0).Value
        sdbcProceDen.Columns("DEN").caption = Adores(0).Value
        sdbcGMN1_4Cod.Columns("COD").caption = Adores(0).Value
        Adores.MoveNext
        sdbcProceCod.Columns("FECREAPERT").caption = Adores(0).Value
        sdbcProceDen.Columns("FECREAPERT").caption = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.AbrevParaPendientes = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.AbrevParaAbiertos = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.AbrevParaCerrados = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.AbrevParaTodos = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.DenParaPendientes = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.DenParaAbiertos = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.DenParaCerrados = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.DenParaTodos = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.AbrevParaParcialCerrados = Adores(0).Value
        Adores.MoveNext
        ProceSelector1.DenParaParcialCerrados = Adores(0).Value
        Adores.MoveNext
        Frame1.caption = Adores(0).Value
        Adores.MoveNext
        chkVistas(0).caption = Adores(0).Value
        Adores.MoveNext
        chkVistas(1).caption = Adores(0).Value
        Adores.MoveNext
        chkExcluidos.caption = Adores(0).Value
        Adores.MoveNext
        Frame2.caption = Adores(0).Value
        Adores.MoveNext
        optFechas(0).caption = Adores(0).Value
        Adores.MoveNext
        optFechas(1).caption = Adores(0).Value
        Adores.MoveNext
        Frame3.caption = Adores(0).Value
        Adores.MoveNext
        optPresItem(0).caption = Adores(0).Value
        Adores.MoveNext
        optPresItem(1).caption = Adores(0).Value
        Adores.MoveNext
        optPresItem(2).caption = Adores(0).Value
        Adores.MoveNext
        lblframe4.caption = Adores(0).Value
        Adores.MoveNext
        optFrame4(0).caption = Adores(0).Value
        Adores.MoveNext
        optFrame4(1).caption = Adores(0).Value
        Adores.MoveNext
        optFrame4(2).caption = Adores(0).Value
        Adores.MoveNext
        optFrame4(3).caption = Adores(0).Value
        Adores.MoveNext
        Me.sdbgGrupos.Columns("ESC").caption = Adores(0).Value
        

        Adores.Close

    End If

    Set Adores = Nothing
    lblGMN1_4.caption = gParametrosGenerales.gsABR_GMN1 & ":"
    chkPresAnu1.caption = gParametrosGenerales.gsPlurPres1
    chkPresAnu2.caption = gParametrosGenerales.gsPlurPres2
    chkPres1.caption = gParametrosGenerales.gsPlurPres3
    chkPres2.caption = gParametrosGenerales.gsPlurPres4
    
End Sub

Private Sub sdbgGrupos_RowLoaded(ByVal Bookmark As Variant)
    If sdbgGrupos.Columns("INC").Value <> "-1" Then
        sdbgGrupos.Columns("ITEMS").CellStyleSet "Gris"
    Else
        sdbgGrupos.Columns("ITEMS").CellStyleSet "Normal"
    End If
    
    
    If sdbgGrupos.Columns("ESC").Visible Then
        If Not m_oProcesoSeleccionado.Grupos.Item(sdbgGrupos.Columns("COD").CellValue(Bookmark)).UsarEscalados <> 0 Then
            'El grupo no tiene escalados
            sdbgGrupos.Columns("ESC").CellStyleSet "Gris"
        End If
        
    End If
    
    
    
End Sub

Private Function HayItemsSeleccionados() As Boolean
Dim i As Integer
Dim vbm As Variant

    HayItemsSeleccionados = False
    For i = 0 To sdbgGrupos.Rows - 1
        vbm = sdbgGrupos.AddItemBookmark(i)
        If sdbgGrupos.Columns("ITEMS").CellValue(vbm) <> "0" Then
            HayItemsSeleccionados = True
            Exit For
        End If
    Next
    

End Function

''' <summary>
''' Nos selecciona el proceso que viene desde frmAvisoDespublicacion.frm para ser copiado
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo:0,1seg.</remarks>
Private Sub CargarProcesoContrato()
Dim sAux() As String
    
    ProceSelector1.Seleccion = PSCerrados
    sAux = Split(g_sProceso, "/")
    Me.sdbcAnyo.Value = sAux(0)
    Me.sdbcGMN1_4Cod.Value = sAux(1)
    m_bRespetarCombo = True
    Me.sdbcProceCod.Value = sAux(2)
    sdbcProceCod_Validate (True)

End Sub

