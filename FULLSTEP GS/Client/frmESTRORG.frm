VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmESTRORG 
   Caption         =   "Organigrama"
   ClientHeight    =   5295
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8820
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRORG.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5295
   ScaleMode       =   0  'User
   ScaleWidth      =   8820
   Begin TabDlg.SSTab sstabEstrorg 
      Height          =   5175
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8640
      _ExtentX        =   15240
      _ExtentY        =   9128
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Organigrama"
      TabPicture(0)   =   "frmESTRORG.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "tvwestrorg"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "picNavigate"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "chkBajaLog"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Destinos"
      TabPicture(1)   =   "frmESTRORG.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblUO"
      Tab(1).Control(1)=   "sdbddDestinos"
      Tab(1).Control(2)=   "sdbddPaises"
      Tab(1).Control(3)=   "sdbddProvincias"
      Tab(1).Control(4)=   "sdbgDestinos"
      Tab(1).Control(5)=   "Picture1"
      Tab(1).ControlCount=   6
      Begin VB.CheckBox chkBajaLog 
         Caption         =   "DVer bajas l�gicas"
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   400
         Width           =   2415
      End
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         ScaleHeight     =   375
         ScaleWidth      =   8385
         TabIndex        =   18
         Top             =   4680
         Width           =   8385
         Begin VB.CommandButton cmdReubicar 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Reubicar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2250
            TabIndex        =   25
            Top             =   30
            Width           =   1125
         End
         Begin VB.CommandButton cmdBajaLog 
            Caption         =   "DBaja l�gica"
            Height          =   345
            Left            =   4620
            TabIndex        =   24
            Top             =   30
            Width           =   1485
         End
         Begin VB.CommandButton cmdA�adir 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&A�adir"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   2
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdEli 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Eliminar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3495
            TabIndex        =   4
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdModif 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Modificar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1125
            TabIndex        =   3
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Restaurar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   6225
            TabIndex        =   5
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdBuscar 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Buscar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   7350
            TabIndex        =   6
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdListado 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Listado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   8520
            TabIndex        =   7
            Top             =   30
            Width           =   1005
         End
      End
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -74880
         ScaleHeight     =   375
         ScaleWidth      =   7740
         TabIndex        =   17
         Top             =   4620
         Width           =   7745
         Begin VB.CommandButton cmdListadoDest 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Listado+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2280
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliminarDest 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Eliminar+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1140
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdA�adirDest 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&A�adir+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdModoEdicion 
            BackColor       =   &H00C9D2D6&
            Caption         =   "&Edici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   6740
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdCodigo 
            BackColor       =   &H00C9D2D6&
            Caption         =   "C�&digo+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3480
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdDeshacer 
            Caption         =   "&Deshacer"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2280
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurarDest 
            Caption         =   "&Restaurar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1140
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdFiltrar 
            Caption         =   "&Filtrar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   0
            Width           =   1005
         End
      End
      Begin MSComctlLib.TreeView tvwestrorg 
         Height          =   3935
         Left            =   120
         TabIndex        =   1
         Top             =   680
         Width           =   8385
         _ExtentX        =   14790
         _ExtentY        =   6932
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgDestinos 
         Height          =   3675
         Left            =   -75000
         TabIndex        =   8
         Top             =   720
         Width           =   11685
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   17
         stylesets.count =   6
         stylesets(0).Name=   "IntOK"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   -2147483633
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmESTRORG.frx":0182
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmESTRORG.frx":019E
         stylesets(2).Name=   "IntHeader"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmESTRORG.frx":01BA
         stylesets(2).AlignmentText=   0
         stylesets(2).AlignmentPicture=   0
         stylesets(3).Name=   "IntKO"
         stylesets(3).ForeColor=   16777215
         stylesets(3).BackColor=   255
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmESTRORG.frx":07E4
         stylesets(3).AlignmentText=   0
         stylesets(4).Name=   "UONInf"
         stylesets(4).BackColor=   13172735
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmESTRORG.frx":0800
         stylesets(5).Name=   "styAzul"
         stylesets(5).BackColor=   16777156
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmESTRORG.frx":081C
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         MaxSelectedRows =   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   17
         Columns(0).Width=   635
         Columns(0).Name =   "INT"
         Columns(0).Alignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HeadStyleSet=   "IntHeader"
         Columns(0).StyleSet=   "IntOK"
         Columns(1).Width=   1323
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   3
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777152
         Columns(2).Width=   1482
         Columns(2).Caption=   "Gen�rico"
         Columns(2).Name =   "GEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   11
         Columns(2).FieldLen=   256
         Columns(2).Style=   2
         Columns(3).Width=   5345
         Columns(3).Caption=   "Direcci�n"
         Columns(3).Name =   "DIR"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   255
         Columns(4).Width=   3572
         Columns(4).Caption=   "Poblaci�n"
         Columns(4).Name =   "POB"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   100
         Columns(5).Width=   1455
         Columns(5).Caption=   "CP"
         Columns(5).Name =   "CP"
         Columns(5).CaptionAlignment=   0
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   20
         Columns(6).Width=   1244
         Columns(6).Caption=   "Pa�s"
         Columns(6).Name =   "PAI"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   3
         Columns(7).Width=   1429
         Columns(7).Caption=   "Provincia"
         Columns(7).Name =   "PROVI"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   3
         Columns(8).Width=   2672
         Columns(8).Caption=   "TFNO"
         Columns(8).Name =   "TFNO"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   2672
         Columns(9).Caption=   "FAX"
         Columns(9).Name =   "FAX"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   4419
         Columns(10).Caption=   "EMAIL"
         Columns(10).Name=   "EMAIL"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   1640
         Columns(11).Caption=   "Almacenes"
         Columns(11).Name=   "ALMAC"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Style=   4
         Columns(11).ButtonsAlways=   -1  'True
         Columns(12).Width=   1402
         Columns(12).Caption=   "U.O"
         Columns(12).Name=   "U.O"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(12).HasBackColor=   -1  'True
         Columns(12).BackColor=   16777152
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "UON1"
         Columns(13).Name=   "UON1"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "UON2"
         Columns(14).Name=   "UON2"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "UON3"
         Columns(15).Name=   "UON3"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   3200
         Columns(16).Visible=   0   'False
         Columns(16).Caption=   "UON4"
         Columns(16).Name=   "UON4"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         _ExtentX        =   20611
         _ExtentY        =   6482
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddProvincias 
         Height          =   1575
         Left            =   -71760
         TabIndex        =   19
         Top             =   840
         Width           =   3855
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         MaxDropDownItems=   10
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1085
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   3
         Columns(1).Width=   4868
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   50
         _ExtentX        =   6800
         _ExtentY        =   2778
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddPaises 
         Height          =   1575
         Left            =   -74160
         TabIndex        =   20
         Top             =   1560
         Width           =   3855
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         MaxDropDownItems=   10
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1085
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   3
         Columns(1).Width=   4868
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   50
         _ExtentX        =   6800
         _ExtentY        =   2778
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddDestinos 
         Height          =   1260
         Left            =   -75120
         TabIndex        =   21
         Top             =   1260
         Width           =   7350
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelColorFace  =   12632256
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   185
         Columns.Count   =   9
         Columns(0).Width=   1376
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777152
         Columns(1).Width=   2487
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN1"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Direcci�n"
         Columns(2).Name =   "DIR"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2090
         Columns(3).Caption=   "Poblaci�n"
         Columns(3).Name =   "POB"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1138
         Columns(4).Caption=   "CP"
         Columns(4).Name =   "CP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   926
         Columns(5).Caption=   "Pa�s"
         Columns(5).Name =   "PAI"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1402
         Columns(6).Caption=   "Provincia"
         Columns(6).Name =   "PROVI"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "DEN2"
         Columns(7).Name =   "DEN2"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "DEN3"
         Columns(8).Name =   "DEN3"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   12965
         _ExtentY        =   2222
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblUO 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -74880
         TabIndex        =   22
         Top             =   360
         Width           =   7735
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   18
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0838
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":08E8
            Key             =   "DepartamentoBaja"
            Object.Tag             =   "DepartamentoBaja"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0C3A
            Key             =   "PerBaja"
            Object.Tag             =   "PerBaja"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0F5E
            Key             =   "PerCestaBaja"
            Object.Tag             =   "PerCestaBaja"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":1282
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":1332
            Key             =   "UON1BAJA"
            Object.Tag             =   "UON1BAJA"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":1684
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":1745
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":1AD8
            Key             =   "UON3BAJA"
            Object.Tag             =   "UON3BAJA"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":1E2A
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":1EDA
            Key             =   "UON2BAJA"
            Object.Tag             =   "UON2BAJA"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":222C
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":22CB
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":2336
            Key             =   "PerCesta"
            Object.Tag             =   "PerCesta"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":23C1
            Key             =   "SELLO"
            Object.Tag             =   "SELLO"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":243D
            Key             =   "SELLOBAJA"
            Object.Tag             =   "SELLOBAJA"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":27C4
            Key             =   "UON4"
            Object.Tag             =   "UON4"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":2857
            Key             =   "UON4BAJA"
            Object.Tag             =   "UON4BAJA"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmESTRORG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Public bSustituirTodas As Boolean
'Variables de optimizacion
Private bEstcmdEli As Boolean
Private bEstcmdA�adir As Boolean
' Variables de restricciones
Private m_bModifEstr As Boolean
Private m_bModifCodigo As Boolean
Private m_bModifCodigoDest As Boolean
Private m_bPermitGenerico As Boolean
Private m_bModifPersonas As Boolean 'Metemos esta vble porque segun la integraci�n se podr�an modificar las personas y no las UO
Public bRUO As Boolean
Public bRDep As Boolean
Private m_bConDestinos As Boolean
Private m_bModifDestinos As Boolean
Private m_bAsigDestinos As Boolean

' Listado de Organigrama y Personas
Public sOrdenListadoDen As String

' Variables para interactuar con otros forms
Public oUON0 As CUnidadOrgNivel0
Public oUON1Seleccionada As CUnidadOrgNivel1
Public oUON2Seleccionada As CUnidadOrgNivel2
Public oUON3Seleccionada As CUnidadOrgNivel3
Public oUON4Seleccionada As CUnidadOrgNivel4
Public oDepAsocSeleccionado As CDepAsociado
Public oPersonaSeleccionada As CPersona
Public oIBaseDatos As IBaseDatos
Public m_bMostrarCC As Boolean

' Variables para los cambios de c�digo
Public g_sCodigoNuevo As String

' Variable de control de flujo
Public Accion As AccionesSummit

Public g_sSustituto As String
Public g_sSustitutoEnCat As String
Public g_sSustUON1 As String
Public g_sSustUON2 As String
Public g_sSustUON3 As String
Public g_sSustDep As String

'Variables de idiomas:
Private m_sIdiUO As String
Private m_sIdiDept As String
Private m_sIdiPers As String
Private m_sIdiCod As String
Private m_sIdiA�adir As String
Private m_sIdiDeptCod As String
Private m_sIdiPersCod As String
Private m_sIdiDet As String
Private m_sIdiModif As String
Private m_sIdiModifDept As String
Private m_sIdiCodP As String
Private m_sDesBajaLog As String
Private m_sBajaLog As String
Private m_sNoHayEmpresaAsociada As String 'Mensaje cuando se quiere mostrar la gesti�n de centros de coste y no hay empresa asociada

'Parte Destinos
Private oDestinos As CDestinos
Private m_sUON As String
Public g_vUON1 As Variant
Public g_vUON2 As Variant
Public g_vUON3 As Variant
Public g_vUON4 As Variant

''' Destino en edicion

Private m_oDestinoEnEdicion As CDestino
Private oIBAseDatosEnEdicion As IBaseDatos

''' Listado de Destinos
Public sOrdenListado As String

''' Control de errores
Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bValError As Boolean

''' Variables de control
Private m_bCambioGeneral As Boolean
Private m_bCambioInstala As Boolean
Private m_bDesactivarTabClick As Boolean

'Public Accion As accionessummit
Private m_bModoEdicion As Boolean

Public g_bCodigoCancelar As Boolean
Private m_bCargarComboDesdePai As Boolean
Private m_bCargarComboDesdeProvi As Boolean
Private m_sPais As String

''' Coleccion de paises
Private m_oPaises As CPaises

''' Coleccion de provincias
Private m_oProvincias As CProvincias


' MULTILENGUAJE
Private m_asFrmTitulo(6) As String  'Captions form
Private m_sOrdenando As String   'Caption form ordenando
Private m_sConsulta As String    'Caption bot�n consulta
Private m_sEdicion As String     'Caption bot�n edici�n
Private m_asMensajes(4) As String  'literales para basmensajes
Private m_scodDest As String
Private m_sIdiSincronizado As String 'literales para estado integraci�n
Private m_sIdiNoSincronizado As String

Private m_oIdiomas As CIdiomas
Public m_Reubicar As Boolean


' variable den_general: Va a recoger "DEN_SPA","DEN_ENG","DEN_GER","DEN_FRA"
  Public den_general As String
  Public PosIniIdiomas As Integer  ' PosIniIdiomas = Primera columna del idioma (Ser� 2. La (0), COD y empezar�an los idiomas)
  Public PosFinIdiomas As Integer  ' PosFinIdiomas = Cuenta cuantos idiomas va a haber y asi se sabe hasta que columna llegan los idiomas

Public g_SelNode As MSComctlLib.node 'Nodo para recoger el recibido en frmESTRORG.tvwestrorg_NodeClick
Public m_bGestionarCCVisible As Boolean 'BLP
Public m_bConsultarCCVisible As Boolean 'BLP
Private m_sIdiConsultarCC As String

Private m_arDestinosDeHijos() As String

Private m_sValidarDestGenerico As String
'************** Metodos privados de la clase *********
Public Sub ConfigurarInterfazSeguridad(ByVal nodx As node)

''' <summary>
''' Muestra las pesta�as y los men�s en funci�n del tipo de usuario
''' </summary>
''' <param name="nodx">Nodo seleccionado del �rbol</param>
''' <remarks>   frmESTRORG
'''             frmCATSeguridad
'''             frmESTRCOMP
'''             frmSOLConfiguracion
'''             frmESTRCOMPMatPorCom
'''             frmESTRMAT
'''             frmCatalogo
'''             frmESTRMATBuscar
'''             frmESTRORGBuscarDep
'''             frmESTRORGBuscarUO
'''             frmESTRORGBuscarPersona
'''             frmCATLINBuscar
'''             frmPROVEMatPorProve
'''             frmPresupuestos1
'''             frmPresupuestos2
'''             frmPresupuestos3
'''             frmPresupuestos4
'''             frmPREAsig
'''             frmItemsWizard4</remarks>



If Not nodx Is Nothing Then
        
    cmdBajaLog.Enabled = False
    
    Select Case Left(nodx.Tag, 4)
 
    Case "PER0", "PER1", "PER2", "PER3"
            
           cmdA�adir.Enabled = False
           sstabEstrorg.TabVisible(1) = False
                      
           If m_bModifPersonas Then

                cmdEli.Enabled = True
                
                If nodx.Image = "PerCestaBaja" Or nodx.Image = "PerBaja" Then
                    MDI.mnuPopUpEstrOrgBajaLogPer.Visible = False
                    MDI.mnuPopUpEstrOrgDesBajaLogPer.Visible = True
                    MDI.mnuPopUpEstrOrgModPer.Visible = False
                    MDI.mnuPopUpEstrOrgCamCodPer.Visible = False
                    MDI.mnuPopUpEstrOrgReuPer.Visible = False
                    cmdModif.Enabled = False
                    cmdReubicar.Enabled = False
                    
                    If InStr(1, nodx.Parent.Parent.Image, "BAJA") Then
                        MDI.mnuPopUpEstrOrgDesBajaLogPer.Visible = False
                        MDI.mnuPopUpEstrOrgReuPer.Visible = False
                    End If
                    
                Else
                    MDI.mnuPopUpEstrOrgBajaLogPer.Visible = True
                    MDI.mnuPopUpEstrOrgDesBajaLogPer.Visible = False
                    MDI.mnuPopUpEstrOrgModPer.Visible = True
                    MDI.mnuPopUpEstrOrgCamCodPer.Visible = True
                    MDI.mnuPopUpEstrOrgReuPer.Visible = True
                    cmdBajaLog.Enabled = True
                    cmdModif.Enabled = True
                    cmdReubicar.Enabled = True
                End If
                
                MDI.mnuPopUpEstrOrgEliPer.Visible = True
                 
           Else
                cmdModif.Enabled = False
                cmdEli.Enabled = False
                cmdReubicar.Enabled = False
                MDI.mnuPopUpEstrOrgModPer.Visible = False
                MDI.mnuPopUpEstrOrgEliPer.Visible = False
                MDI.mnuPopUpEstrOrgPerSep.Visible = False
                MDI.mnuPopUpEstrOrgBajaLogPer.Visible = False
                MDI.mnuPopUpEstrOrgDesBajaLogPer.Visible = False
                MDI.mnuPopUpEstrOrgReuPer.Visible = False
           End If
            
           MDI.mnuPopUpEstrOrgCamCodPer.Visible = m_bModifCodigo
    
    Case "DEP0", "DEP1", "DEP2", "DEP3"
    
            sstabEstrorg.TabVisible(1) = False
           If m_bModifPersonas Then
                If nodx.Image <> "DepartamentoBaja" Then
                    cmdA�adir.Enabled = True
                    MDI.mnuPopUpEstrOrgDepart(1).Visible = True
                Else
                    cmdA�adir.Enabled = False
                    MDI.mnuPopUpEstrOrgDepart(1).Visible = False
                End If
           Else
                cmdA�adir.Enabled = False
                MDI.mnuPopUpEstrOrgDepart(1).Visible = False
           End If
           
           If m_bModifEstr Then
           
                If nodx.Image <> "DepartamentoBaja" Then
                    cmdModif.Enabled = True
                    cmdReubicar.Enabled = False
                    If nodx.Children <> 0 Then
                        cmdEli.Enabled = False
                        'MDI.mnuPopUpEstrOrgEliDep.Enabled = False
                        MDI.mnuPopUpEstrOrgDepart(4).Enabled = False
                        MDI.mnuPopUpEstrOrgDepart(4).Visible = True
                    Else
                        cmdEli.Enabled = True
                        'MDI.mnuPopUpEstrOrgEliDep.Enabled = True
                        MDI.mnuPopUpEstrOrgDepart(4).Enabled = True
                        MDI.mnuPopUpEstrOrgDepart(4).Visible = True
                    End If
                    
                    MDI.mnuPopUpEstrOrgModPer.Enabled = True
                    MDI.mnuPopUpEstrOrgDepart(3).Visible = True
                    MDI.mnuPopUpEstrOrgDepart(2).Visible = True
                    MDI.mnuPopUpEstrOrgDepart(6).Visible = True
                    MDI.mnuPopUpEstrOrgDepart(5).Visible = m_bModifCodigo

                Else
                    cmdModif.Enabled = False
                    cmdEli.Enabled = False
                    cmdReubicar.Enabled = False
                    MDI.mnuPopUpEstrOrgDepart(3).Visible = False
                    MDI.mnuPopUpEstrOrgDepart(4).Visible = False
                    MDI.mnuPopUpEstrOrgDepart(2).Visible = False
                    MDI.mnuPopUpEstrOrgDepart(6).Visible = False
                    MDI.mnuPopUpEstrOrgDepart(5).Visible = False

                End If
           Else
                cmdModif.Enabled = False
                cmdEli.Enabled = False
                cmdReubicar.Enabled = False
                MDI.mnuPopUpEstrOrgDepart(3).Visible = False
                MDI.mnuPopUpEstrOrgDepart(4).Visible = False
                MDI.mnuPopUpEstrOrgDepart(2).Visible = False
                MDI.mnuPopUpEstrOrgDepart(6).Visible = False
                MDI.mnuPopUpEstrOrgDepart(5).Visible = False
                
           End If
        
           MDI.mnuPopUpEstrOrgDepart(5).Visible = m_bModifCodigo
           
    Case "UON4"
        'sstabEstrorg.TabVisible(1) = False
        
        If m_bModifEstr Then
            If nodx.Image <> "UON4BAJA" And nodx.Image <> "SELLOBAJA" Then
            
'                MDI.mnuPopUpEstrOrgNiv4(1).Enabled = True
'                MDI.mnuPopUpEstrOrgNiv4(2).Enabled = True
                MDI.mnuPopUpEstrOrgNiv4(3).Enabled = True
                MDI.mnuPopUpEstrOrgNiv4(4).Enabled = True
                MDI.mnuPopUpEstrOrgNiv4(5).Enabled = True
                MDI.mnuPopUpEstrOrgNiv4(6).Enabled = True

                cmdA�adir.Enabled = False
                cmdModif.Enabled = True
'                If nodx.Children <> 0 Then
'                    cmdEli.Enabled = False
'                    MDI.mnuPopUpEstrOrgNiv4(2).Enabled = False
'                Else
                    cmdEli.Enabled = True
'                    MDI.mnuPopUpEstrOrgNiv4(2).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv4(4).Enabled = True
                'End If
                
                NodoSeleccionado nodx
'                If oUON4Seleccionada.SePuedeDarBajaUON4 Then
'                    MDI.mnuPopUpEstrOrgNiv4(3).Visible = True 'Baja Logica
                    MDI.mnuPopUpEstrOrgNiv4(5).Visible = True 'Baja Logica
                    cmdBajaLog.Enabled = True
'                Else
'                    MDI.mnuPopUpEstrOrgNiv4(3).Visible = False
'                    cmdBajaLog.Enabled = False
'
'
'                End If
'                MDI.mnuPopUpEstrOrgNiv4(4).Visible = False ' Deshacer baja logica
                MDI.mnuPopUpEstrOrgNiv4(6).Visible = False ' Deshacer baja logica

            Else
                
                cmdA�adir.Enabled = False
                cmdModif.Enabled = False
'                If nodx.Children <> 0 Then
'                    cmdEli.Enabled = False
'                    'MDI.mnuPopUpEstrOrgNiv4(4).Enabled = False
'                Else
                    cmdEli.Enabled = True
'                    MDI.mnuPopUpEstrOrgNiv4(2).Enabled = True ' Eliminar
                    MDI.mnuPopUpEstrOrgNiv4(4).Enabled = True ' Eliminar
                'End If
                
                If InStr(1, nodx.Parent.Image, "BAJA") Then
'                    MDI.mnuPopUpEstrOrgNiv4(1).Visible = False 'Modificar
'                    MDI.mnuPopUpEstrOrgNiv4(3).Visible = False  'Eliminar
'                    MDI.mnuPopUpEstrOrgNiv4(4).Visible = False    ' Deshacer baja logica
                    MDI.mnuPopUpEstrOrgNiv4(3).Visible = False 'Modificar
                    MDI.mnuPopUpEstrOrgNiv4(5).Visible = False  'Eliminar
                    MDI.mnuPopUpEstrOrgNiv4(6).Visible = False    ' Deshacer baja logica
                Else
'                    MDI.mnuPopUpEstrOrgNiv4(3).Visible = False ' Deshacer baja logica
'                    MDI.mnuPopUpEstrOrgNiv4(4).Visible = True ' Deshacer baja logica
                    MDI.mnuPopUpEstrOrgNiv4(5).Visible = False ' Deshacer baja logica
                    MDI.mnuPopUpEstrOrgNiv4(6).Visible = True ' Deshacer baja logica
                End If

                cmdBajaLog.Enabled = False
            
            
            End If

            
        Else
            
'            MDI.mnuPopUpEstrOrgNiv4(1).Visible = False
'            MDI.mnuPopUpEstrOrgNiv4(2).Visible = False
'            MDI.mnuPopUpEstrOrgNiv4(3).Visible = False
'            MDI.mnuPopUpEstrOrgNiv4(4).Visible = False
            MDI.mnuPopUpEstrOrgNiv4(3).Visible = False
            MDI.mnuPopUpEstrOrgNiv4(4).Visible = False
            MDI.mnuPopUpEstrOrgNiv4(5).Visible = False
            MDI.mnuPopUpEstrOrgNiv4(6).Visible = False
            
            cmdA�adir.Enabled = False
            cmdModif.Enabled = False
            cmdEli.Enabled = False
'            MDI.mnuPopUpEstrOrgNiv3(5).Visible = False
'            MDI.mnuPopUpEstrOrgNiv3(6).Visible = False
            cmdBajaLog.Enabled = False
            
        End If

        
        'Gestion Centro de Coste
        If m_bGestionarCCVisible Or m_bConsultarCCVisible Then
            If nodx.Image <> "UON4BAJA" And nodx.Image <> "SELLOBAJA" Then
                MDI.mnuPopUpEstrOrgNiv4(1).Enabled = True
                MDI.mnuPopUpEstrOrgNiv4(2).Enabled = True
                'Ocultamos la linea que hay en el �ndice 7 si los cuatro elementos siguientes est�n ocultos
                If Not MDI.mnuPopUpEstrOrgNiv4(3).Visible And Not MDI.mnuPopUpEstrOrgNiv4(4).Visible And Not MDI.mnuPopUpEstrOrgNiv4(5).Visible And Not MDI.mnuPopUpEstrOrgNiv4(6).Visible Then
                    MDI.mnuPopUpEstrOrgNiv4(7).Visible = False
                End If
            Else
                If InStr(1, nodx.Parent.Image, "BAJA") Then
                    MDI.mnuPopUpEstrOrgNiv4(1).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv4(2).Enabled = False
                End If
            End If
        Else
'            MDI.mnuPopUpEstrOrgNiv4(1).Enabled = False
'            MDI.mnuPopUpEstrOrgNiv4(2).Enabled = False
            MDI.mnuPopUpEstrOrgNiv4(1).Visible = False
            MDI.mnuPopUpEstrOrgNiv4(2).Visible = False
            'Ocultamos la linea que hay en el �ndice 7 si los cuatro elementos siguientes est�n ocultos
            If Not MDI.mnuPopUpEstrOrgNiv4(3).Visible And Not MDI.mnuPopUpEstrOrgNiv4(4).Visible And Not MDI.mnuPopUpEstrOrgNiv4(5).Visible And Not MDI.mnuPopUpEstrOrgNiv4(6).Visible Then
                MDI.mnuPopUpEstrOrgNiv4(7).Visible = False
            End If
        End If
'        If oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador Then
'            MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
'        End If
    
    Case "UON3"
        
        If m_bModifEstr Then
            
            'Hago visibles las opciones del menu en funcion de si esta o no dado de baja
            If nodx.Image = "UON3BAJA" Or nodx.Image = "SELLOBAJA" Then
                MDI.mnuPopUpEstrOrgNiv3(1).Visible = False
                MDI.mnuPopUpEstrOrgNiv3(2).Visible = False
'                MDI.mnuPopUpEstrOrgNiv3(3).Visible = False
'                MDI.mnuPopUpEstrOrgNiv3(4).Visible = False
'                MDI.mnuPopUpEstrOrgNiv3(5).Visible = True
'                MDI.mnuPopUpEstrOrgNiv3(6).Visible = False
'                MDI.mnuPopUpEstrOrgNiv3(7).Visible = True
'                MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
'                'MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
                MDI.mnuPopUpEstrOrgNiv3(4).Visible = False
                MDI.mnuPopUpEstrOrgNiv3(5).Visible = False
                MDI.mnuPopUpEstrOrgNiv3(6).Visible = True
                MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
                MDI.mnuPopUpEstrOrgNiv3(8).Visible = True
                MDI.mnuPopUpEstrOrgNiv3(9).Visible = False

                cmdReubicar.Enabled = False
                
                If InStr(1, nodx.Parent.Image, "BAJA") Then
'                    MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
                    MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
                Else
'                    MDI.mnuPopUpEstrOrgNiv3(7).Visible = True
                    MDI.mnuPopUpEstrOrgNiv3(8).Visible = True
                End If

            Else
                If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
                    MDI.mnuPopUpEstrOrgNiv3(1).Visible = True
                Else
                    MDI.mnuPopUpEstrOrgNiv3(1).Visible = False
                End If
                
                MDI.mnuPopUpEstrOrgNiv3(2).Visible = True
'                MDI.mnuPopUpEstrOrgNiv3(3).Visible = True
'                MDI.mnuPopUpEstrOrgNiv3(4).Visible = True
'                MDI.mnuPopUpEstrOrgNiv3(5).Visible = True
'                MDI.mnuPopUpEstrOrgNiv3(6).Visible = True
'                MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
'                MDI.mnuPopUpEstrOrgNiv3(8).Visible = True
'                MDI.mnuPopUpEstrOrgNiv3(9).Visible = True
                MDI.mnuPopUpEstrOrgNiv3(4).Visible = True
                MDI.mnuPopUpEstrOrgNiv3(5).Visible = True
                MDI.mnuPopUpEstrOrgNiv3(6).Visible = True
                MDI.mnuPopUpEstrOrgNiv3(7).Visible = True
                MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
                MDI.mnuPopUpEstrOrgNiv3(9).Visible = m_bModifCodigo
                MDI.mnuPopUpEstrOrgNiv3(10).Visible = True
                cmdReubicar.Enabled = False
            End If
            
            If bRDep Then
                
                cmdA�adir.Enabled = False
                cmdModif.Enabled = False
                cmdEli.Enabled = False
               
                MDI.mnuPopUpEstrOrgNiv3(1).Enabled = False
'                MDI.mnuPopUpEstrOrgNiv3(4).Enabled = False
'                MDI.mnuPopUpEstrOrgNiv3(3).Enabled = False
                MDI.mnuPopUpEstrOrgNiv3(5).Enabled = False
                MDI.mnuPopUpEstrOrgNiv3(4).Enabled = False

                If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
'                    MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
                    MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
                End If
                
'                MDI.mnuPopUpEstrOrgNiv3(5).Enabled = False
                MDI.mnuPopUpEstrOrgNiv3(6).Enabled = False
                cmdBajaLog.Enabled = False
                        
                Exit Sub
            End If
            
            If bRUO Then
                If nodx.Image <> "UON3BAJA" And nodx.Image <> "SELLOBAJA" Then
                
                    cmdA�adir.Enabled = True
                    cmdModif.Enabled = True
                    MDI.mnuPopUpEstrOrgNiv3(1).Enabled = True
'                    MDI.mnuPopUpEstrOrgNiv3(3).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv3(4).Enabled = True

                    If nodx.Children > 0 Then
                        cmdEli.Enabled = False
                        'MDI.mnuPopUpEstrOrgEliNivel3 = False
'                        MDI.mnuPopUpEstrOrgNiv3(4).Visible = False
                        MDI.mnuPopUpEstrOrgNiv3(5).Visible = False
                    Else
                        cmdEli.Enabled = True
                        'MDI.mnuPopUpEstrOrgEliNivel3 = True
'                        MDI.mnuPopUpEstrOrgNiv3(4).Visible = True
                        MDI.mnuPopUpEstrOrgNiv3(5).Visible = True
                    End If
                    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
                        'MDI.mnuPopUpEstrOrgCamCodNiv3.Visible = False
'                        MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
                        MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
                    End If
                            
                    NodoSeleccionado nodx
                    If oUON3Seleccionada.SePuedeDarBajaUON3 Then
'                        MDI.mnuPopUpEstrOrgNiv3(5).Visible = True
                        MDI.mnuPopUpEstrOrgNiv3(6).Visible = True
                        cmdBajaLog.Enabled = True
                    Else
'                        MDI.mnuPopUpEstrOrgNiv3(5).Visible = False
                        MDI.mnuPopUpEstrOrgNiv3(6).Visible = False
                        cmdBajaLog.Enabled = False
                    End If
                Else
                    cmdA�adir.Enabled = False
                    cmdModif.Enabled = False
                    
                    If nodx.Children > 0 Then
                        cmdEli.Enabled = False
                        'MDI.mnuPopUpEstrOrgEliNivel3 = False
'                        MDI.mnuPopUpEstrOrgNiv3(4).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv3(5).Enabled = False
                    Else
                        cmdEli.Enabled = True
                        'MDI.mnuPopUpEstrOrgEliNivel3 = True
'                        MDI.mnuPopUpEstrOrgNiv3(4).Enabled = True
                        MDI.mnuPopUpEstrOrgNiv3(5).Enabled = True
                    End If
                    
                    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
                        'MDI.mnuPopUpEstrOrgCamCodNiv3.Visible = False
'                        MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
                        MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
                    End If
                            
                    If InStr(1, nodx.Parent.Image, "BAJA") Then
'                        MDI.mnuPopUpEstrOrgNiv3(6).Visible = False
                        MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
                    Else
'                        MDI.mnuPopUpEstrOrgNiv3(6).Visible = True
                        MDI.mnuPopUpEstrOrgNiv3(7).Visible = True
                    End If
            
'                    MDI.mnuPopUpEstrOrgNiv3(6).Visible = True
                    cmdBajaLog.Enabled = False
                
                End If
                
                Exit Sub
            End If
        
            'MDI.mnuPopUpEstrOrgAnyaDepNivel3.Enabled = True
            'MDI.mnuPopUpEstrOrgModNivel3.Enabled = True
            'MDI.mnuPopUpEstrOrgEliNivel3.Enabled = True
            
            If nodx.Image <> "UON3BAJA" And nodx.Image <> "SELLOBAJA" Then
            
                MDI.mnuPopUpEstrOrgNiv3(2).Enabled = True
'                MDI.mnuPopUpEstrOrgNiv3(4).Enabled = True
'                MDI.mnuPopUpEstrOrgNiv3(5).Enabled = True
                MDI.mnuPopUpEstrOrgNiv3(5).Enabled = True
                MDI.mnuPopUpEstrOrgNiv3(6).Enabled = True
                
                cmdA�adir.Enabled = True
                cmdModif.Enabled = True
                If nodx.Children <> 0 Then
                    cmdEli.Enabled = False
'                    MDI.mnuPopUpEstrOrgNiv3(5).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv3(6).Enabled = False
                Else
                    cmdEli.Enabled = True
'                    MDI.mnuPopUpEstrOrgNiv3(5).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv3(6).Enabled = True
                End If
                
                
                
                NodoSeleccionado nodx
                If oUON3Seleccionada.SePuedeDarBajaUON3 Then
'                    MDI.mnuPopUpEstrOrgNiv3(6).Visible = True
'                    MDI.mnuPopUpEstrOrgNiv3(6).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv3(7).Visible = True
                    MDI.mnuPopUpEstrOrgNiv3(7).Enabled = True
                    cmdBajaLog.Enabled = True
                Else
'                    MDI.mnuPopUpEstrOrgNiv3(6).Visible = False
'                    MDI.mnuPopUpEstrOrgNiv3(6).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
                    MDI.mnuPopUpEstrOrgNiv3(7).Enabled = False
                    cmdBajaLog.Enabled = False
                
                    
                End If
                'MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
                
            Else
                
                cmdA�adir.Enabled = False
                cmdModif.Enabled = False
                If nodx.Children <> 0 Then
                    cmdEli.Enabled = False
'                    MDI.mnuPopUpEstrOrgNiv3(5).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv3(6).Enabled = False
                Else
                    cmdEli.Enabled = True
'                    MDI.mnuPopUpEstrOrgNiv3(5).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv3(6).Enabled = True
                End If
                
                If InStr(1, nodx.Parent.Image, "BAJA") Then
'                    MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
                    MDI.mnuPopUpEstrOrgNiv3(9).Visible = False
                Else
'                    MDI.mnuPopUpEstrOrgNiv3(8).Visible = True
                    MDI.mnuPopUpEstrOrgNiv3(9).Visible = m_bModifCodigo
                End If

                cmdBajaLog.Enabled = False
            
            
            End If

            MDI.mnuPopUpEstrOrgNiv3(9).Visible = m_bModifCodigo
        Else
            
            MDI.mnuPopUpEstrOrgNiv3(1).Visible = False
            MDI.mnuPopUpEstrOrgNiv3(2).Visible = False
'            MDI.mnuPopUpEstrOrgNiv3(3).Visible = False
'            MDI.mnuPopUpEstrOrgNiv3(4).Visible = False
'            MDI.mnuPopUpEstrOrgNiv3(8).Visible = False
'            MDI.mnuPopUpEstrOrgNiv3(9).Visible = False
            MDI.mnuPopUpEstrOrgNiv3(4).Visible = False
            MDI.mnuPopUpEstrOrgNiv3(5).Visible = False
            MDI.mnuPopUpEstrOrgNiv3(9).Visible = False
            MDI.mnuPopUpEstrOrgNiv3(10).Visible = False
            
            cmdA�adir.Enabled = False
            cmdModif.Enabled = False
            cmdEli.Enabled = False
'            MDI.mnuPopUpEstrOrgNiv3(5).Visible = False
'            MDI.mnuPopUpEstrOrgNiv3(6).Visible = False
            MDI.mnuPopUpEstrOrgNiv3(6).Visible = False
            MDI.mnuPopUpEstrOrgNiv3(7).Visible = False
            cmdBajaLog.Enabled = False

        End If
        
        'BLP
        'Gestion Centro de Coste
        If m_bGestionarCCVisible Or m_bConsultarCCVisible Then
            If nodx.Image = "UON3BAJA" Or nodx.Image = "SELLOBAJA" Then
                MDI.mnuPopUpEstrOrgNiv3(3).Enabled = False
            Else
                MDI.mnuPopUpEstrOrgNiv3(3).Visible = True
            End If
            MDI.mnuPopUpEstrOrgNiv3(4).Visible = True 'L�nea divisoria
        Else
            MDI.mnuPopUpEstrOrgNiv3(3).Visible = False
            If Not MDI.mnuPopUpEstrOrgNiv3(2).Visible And Not MDI.mnuPopUpEstrOrgNiv3(1).Visible Then
                MDI.mnuPopUpEstrOrgNiv3(4).Visible = False
            End If
        End If
         
        
    Case "UON2"
              
         If m_bModifEstr Then
            
            If nodx.Image = "UON2BAJA" Or nodx.Image = "SELLOBAJA" Then
                MDI.mnuPopUpEstrOrgNiv2(1).Visible = False
                MDI.mnuPopUpEstrOrgNiv2(2).Visible = False
'                MDI.mnuPopUpEstrOrgNiv2(3).Visible = False
'                MDI.mnuPopUpEstrOrgNiv2(4).Visible = False
'                MDI.mnuPopUpEstrOrgNiv2(5).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(6).Visible = False
'                'MDI.mnuPopUpEstrOrgNiv2(7).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(8).Visible = False
'                MDI.mnuPopUpEstrOrgNiv2(9).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(10).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(4).Visible = False
                MDI.mnuPopUpEstrOrgNiv2(5).Visible = False
                MDI.mnuPopUpEstrOrgNiv2(6).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(7).Visible = False
                'MDI.mnuPopUpEstrOrgNiv2(8).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(9).Visible = False
                MDI.mnuPopUpEstrOrgNiv2(10).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(11).Visible = True
                If InStr(1, nodx.Parent.Image, "BAJA") Then
'                    MDI.mnuPopUpEstrOrgNiv2(7).Visible = False
                    MDI.mnuPopUpEstrOrgNiv2(8).Visible = False
                Else
'                    MDI.mnuPopUpEstrOrgNiv2(7).Visible = True
                    MDI.mnuPopUpEstrOrgNiv2(8).Visible = True
                End If

            Else
                MDI.mnuPopUpEstrOrgNiv2(1).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(2).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(3).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(4).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(5).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(6).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(7).Visible = False
'                MDI.mnuPopUpEstrOrgNiv2(8).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(9).Visible = True
'                MDI.mnuPopUpEstrOrgNiv2(10).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(4).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(5).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(6).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(7).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(8).Visible = False
                MDI.mnuPopUpEstrOrgNiv2(9).Visible = m_bModifCodigo
                MDI.mnuPopUpEstrOrgNiv2(10).Visible = True
                MDI.mnuPopUpEstrOrgNiv2(11).Visible = True

            End If
            
            If bRDep Then
                   
                cmdA�adir.Enabled = False
                cmdModif.Enabled = False
                cmdEli.Enabled = False
                
                MDI.mnuPopUpEstrOrgNiv2(1).Enabled = False
                MDI.mnuPopUpEstrOrgNiv2(2).Enabled = False
'                MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
'                MDI.mnuPopUpEstrOrgNiv2(4).Enabled = False
'                MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                MDI.mnuPopUpEstrOrgNiv2(7).Enabled = False
                MDI.mnuPopUpEstrOrgNiv2(9).Visible = m_bModifCodigo

                Exit Sub
            
            End If
               
            If bRUO Then
                If nodx.Image <> "UON2BAJA" And nodx.Image <> "SELLOBAJA" Then
                    'Tenemos que ver si la del usuario esta por encima de la seleccionada
                    If Trim(basOptimizacion.gUON2Usuario) = "" Then
                        ' El usuario tiene permisos sobre la UON2 seleccionada
                        cmdA�adir.Enabled = True
                        cmdModif.Enabled = True
                       
                        MDI.mnuPopUpEstrOrgNiv2(2).Enabled = True
'                        MDI.mnuPopUpEstrOrgNiv2(4).Enabled = True
                        MDI.mnuPopUpEstrOrgNiv2(5).Enabled = True
                        
                        If nodx.Children <> 0 Then
                            cmdEli.Enabled = False
'                            MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                        Else
                            cmdEli.Enabled = False
'                            MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                        End If
                        
                        NodoSeleccionado nodx
                        If oUON2Seleccionada.SePuedeDarBajaUON2 Then
'                            MDI.mnuPopUpEstrOrgNiv2(6).Enabled = True
                            MDI.mnuPopUpEstrOrgNiv2(7).Enabled = True
                            cmdBajaLog.Enabled = True
                        Else
'                            MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(7).Enabled = False
                            cmdBajaLog.Enabled = False
                        End If
    
                    Else
                        ' Puede ser de nivel 2 o 3
                        If Trim(basOptimizacion.gUON3Usuario) = "" Then
                            'Es la misma
                            cmdA�adir.Enabled = True
                            cmdModif.Enabled = True
                            cmdEli.Enabled = False
                            
                            MDI.mnuPopUpEstrOrgNiv2(2).Enabled = True
'                            MDI.mnuPopUpEstrOrgNiv2(4).Enabled = True
'                            MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(5).Enabled = True
                            MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                            
                        Else
                            'Es de nivel 3
                            cmdA�adir.Enabled = False
                            cmdModif.Enabled = False
                            cmdEli.Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(1).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(2).Enabled = False
'                            MDI.mnuPopUpEstrOrgNiv2(4).Enabled = False
'                            MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                            
                        End If
                        MDI.mnuPopUpEstrOrgNiv2(9).Visible = m_bModifCodigo
    
                        NodoSeleccionado nodx
                        If oUON2Seleccionada.SePuedeDarBajaUON2 Then
'                            MDI.mnuPopUpEstrOrgNiv2(6).Enabled = True
                            MDI.mnuPopUpEstrOrgNiv2(7).Enabled = True
                            cmdBajaLog.Enabled = True
                        Else
'                            MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv2(7).Enabled = False
                            cmdBajaLog.Enabled = False
                        End If
    
                        Exit Sub
                    End If
                Else
                    'Tenemos que ver si la del usuario esta por encima de la seleccionada
                    ' El usuario tiene permisos sobre la UON2 seleccionada
                    cmdA�adir.Enabled = False
                    cmdModif.Enabled = False
                    
                    MDI.mnuPopUpEstrOrgNiv2(2).Enabled = False
'                    MDI.mnuPopUpEstrOrgNiv2(4).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                    
                    If nodx.Children <> 0 Then
                        cmdEli.Enabled = False
                        'MDI.mnuPopUpEstrOrgEliNivel2.Enabled = False
'                        MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False

                    Else
                        cmdEli.Enabled = False
'                        MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                    End If
                    
                    MDI.mnuPopUpEstrOrgNiv2(6).Visible = False
                    cmdBajaLog.Enabled = False

                    MDI.mnuPopUpEstrOrgNiv2(9).Visible = m_bModifCodigo

'                    MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv2(7).Enabled = False
                    cmdBajaLog.Enabled = False
'                    MDI.mnuPopUpEstrOrgNiv2(7).Visible = True
                    MDI.mnuPopUpEstrOrgNiv2(8).Visible = True
                    
                    Exit Sub
                
                End If
            End If
            
            If nodx.Image <> "UON2BAJA" And nodx.Image <> "SELLOBAJA" Then
                MDI.mnuPopUpEstrOrgNiv2(2).Enabled = True
                MDI.mnuPopUpEstrOrgNiv2(1).Enabled = True
'                MDI.mnuPopUpEstrOrgNiv2(4).Enabled = True
                MDI.mnuPopUpEstrOrgNiv2(5).Enabled = True
                
                cmdA�adir.Enabled = True
                cmdModif.Enabled = True
                If nodx.Children = 0 Then
'                    MDI.mnuPopUpEstrOrgNiv2(5).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv2(6).Enabled = True
                    cmdEli.Enabled = True
                Else
'                    MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                    cmdEli.Enabled = False
                End If
                
                NodoSeleccionado nodx
                
                If oUON2Seleccionada.SePuedeDarBajaUON2 Then
'                    MDI.mnuPopUpEstrOrgNiv2(6).Visible = True
                    MDI.mnuPopUpEstrOrgNiv2(7).Visible = True
                    cmdBajaLog.Enabled = True
                Else
'                    MDI.mnuPopUpEstrOrgNiv2(6).Visible = False
                    MDI.mnuPopUpEstrOrgNiv2(7).Visible = False
                    cmdBajaLog.Enabled = False
                End If
            Else
            
                MDI.mnuPopUpEstrOrgNiv2(2).Enabled = False
                MDI.mnuPopUpEstrOrgNiv2(1).Enabled = False
'                MDI.mnuPopUpEstrOrgNiv2(4).Enabled = False
                MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                
                cmdA�adir.Enabled = False
                cmdModif.Enabled = False
                If nodx.Children = 0 Then
'                    MDI.mnuPopUpEstrOrgNiv2(5).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv2(6).Enabled = True
                    cmdEli.Enabled = True
                Else
'                    MDI.mnuPopUpEstrOrgNiv2(5).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv2(6).Enabled = False
                    cmdEli.Enabled = False
                End If
                
'                MDI.mnuPopUpEstrOrgNiv2(6).Visible = False
                MDI.mnuPopUpEstrOrgNiv2(7).Visible = False
                cmdBajaLog.Enabled = False
                
                If InStr(1, nodx.Parent.Image, "BAJA") Then
'                    MDI.mnuPopUpEstrOrgNiv2(7).Visible = False
                    MDI.mnuPopUpEstrOrgNiv2(8).Visible = False
                Else
'                    MDI.mnuPopUpEstrOrgNiv2(7).Visible = True
                    MDI.mnuPopUpEstrOrgNiv2(8).Visible = True
                End If

                'MDI.mnuPopUpEstrOrgNiv2(7).Visible = True
                
            
            End If
            
            MDI.mnuPopUpEstrOrgNiv2(9).Visible = m_bModifCodigo
        Else
                        
            MDI.mnuPopUpEstrOrgNiv2(2).Visible = False
            MDI.mnuPopUpEstrOrgNiv2(1).Visible = False
'            MDI.mnuPopUpEstrOrgNiv2(5).Visible = False
'            MDI.mnuPopUpEstrOrgNiv2(4).Visible = False
'            MDI.mnuPopUpEstrOrgNiv2(3).Visible = False
'            MDI.mnuPopUpEstrOrgNiv2(9).Visible = False
            MDI.mnuPopUpEstrOrgNiv2(6).Visible = False
            MDI.mnuPopUpEstrOrgNiv2(5).Visible = False
            MDI.mnuPopUpEstrOrgNiv2(4).Visible = False
            MDI.mnuPopUpEstrOrgNiv2(10).Visible = False

            cmdA�adir.Enabled = False
            cmdModif.Enabled = False
            cmdEli.Enabled = False
            
'            MDI.mnuPopUpEstrOrgNiv2(6).Visible = False
'            MDI.mnuPopUpEstrOrgNiv2(7).Visible = False
            MDI.mnuPopUpEstrOrgNiv2(7).Visible = False
            MDI.mnuPopUpEstrOrgNiv2(8).Visible = False
            cmdBajaLog.Enabled = False
            
            'no puedes cambiar c�digo cuando NO
            'tienes permiso para modificar la estructura (m_bModifEstr = TRUE)
            MDI.mnuPopUpEstrOrgNiv2(9).Visible = False
        End If
                  
        ' Configuramos los menus en caso de que haya menos niveles
        If gParametrosGenerales.giNEO <= 2 Then
            cmdA�adir.Enabled = False
            MDI.mnuPopUpEstrOrgNiv2(1).Visible = False
        End If
        
        'Gestion Centro de Coste
        If m_bGestionarCCVisible Or m_bConsultarCCVisible Then
            If nodx.Image = "UON2BAJA" Or nodx.Image = "SELLOBAJA" Then
                MDI.mnuPopUpEstrOrgNiv2(3).Enabled = False
            Else
                MDI.mnuPopUpEstrOrgNiv2(3).Visible = True
            End If
            MDI.mnuPopUpEstrOrgNiv2(4).Visible = True 'L�nea divisoria
        Else
            MDI.mnuPopUpEstrOrgNiv2(3).Visible = False
            If Not MDI.mnuPopUpEstrOrgNiv2(2).Visible And Not MDI.mnuPopUpEstrOrgNiv2(1).Visible Then
                MDI.mnuPopUpEstrOrgNiv2(4).Visible = False
            End If
        End If

    Case "UON1"
           
        If m_bModifEstr Then
        
            If nodx.Image = "UON1BAJA" Or nodx.Image = "SELLOBAJA" Then
                MDI.mnuPopUpEstrOrgNiv1(1).Visible = False
                MDI.mnuPopUpEstrOrgNiv1(2).Visible = False
'                MDI.mnuPopUpEstrOrgNiv1(3).Visible = False
'                MDI.mnuPopUpEstrOrgNiv1(4).Visible = False
'                MDI.mnuPopUpEstrOrgNiv1(5).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(6).Visible = False
'                'MDI.mnuPopUpEstrOrgNiv1(7).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(8).Visible = False
'                MDI.mnuPopUpEstrOrgNiv1(9).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(10).Visible = True
'                If InStr(1, nodx.Parent.Image, "BAJA") Then
'                    MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
'                Else
'                    MDI.mnuPopUpEstrOrgNiv1(7).Visible = True
'                End If
                MDI.mnuPopUpEstrOrgNiv1(4).Visible = False
                MDI.mnuPopUpEstrOrgNiv1(5).Visible = False
                MDI.mnuPopUpEstrOrgNiv1(6).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                'MDI.mnuPopUpEstrOrgNiv1(8).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(9).Visible = False
                MDI.mnuPopUpEstrOrgNiv1(10).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(11).Visible = True
                If InStr(1, nodx.Parent.Image, "BAJA") Then
                    MDI.mnuPopUpEstrOrgNiv1(8).Visible = False
                Else
                    MDI.mnuPopUpEstrOrgNiv1(8).Visible = True
                End If

            Else
                MDI.mnuPopUpEstrOrgNiv1(1).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(2).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(3).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(4).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(5).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(8).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(9).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(10).Visible = True
'                MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                MDI.mnuPopUpEstrOrgNiv1(4).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(5).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(6).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(9).Visible = m_bModifCodigo
                MDI.mnuPopUpEstrOrgNiv1(10).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(11).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(8).Visible = False
            
            End If

        
            If bRDep Then
                cmdA�adir.Enabled = False
                MDI.mnuPopUpEstrOrgNiv1(2).Enabled = False
                MDI.mnuPopUpEstrOrgNiv1(1).Enabled = False
'                MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False
'                MDI.mnuPopUpEstrOrgNiv1(4).Enabled = False
                MDI.mnuPopUpEstrOrgNiv1(6).Enabled = False
                MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False

                cmdModif.Enabled = False
                cmdEli.Enabled = False
                MDI.mnuPopUpEstrOrgNiv1(9).Visible = m_bModifCodigo
                            
'                MDI.mnuPopUpEstrOrgNiv1(6).Visible = False
                MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                cmdBajaLog.Enabled = False
                
                Exit Sub
            End If
            
            If bRUO Then
                
                'Si el nivel del usuario es inferior no puede hacer nada
                If nodx.Image <> "UON1BAJA" And nodx.Image <> "SELLOBAJA" Then
                
                    If Trim(basOptimizacion.gUON2Usuario) <> "" Then
                           
                        cmdA�adir.Enabled = False
                        MDI.mnuPopUpEstrOrgNiv1(2).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv1(1).Enabled = False
'                        MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False
'                        MDI.mnuPopUpEstrOrgNiv1(4).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv1(6).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False

                        cmdModif.Enabled = False
                        cmdEli.Enabled = False
'                        MDI.mnuPopUpEstrOrgNiv1(6).Visible = False
                        MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                        cmdBajaLog.Enabled = False
                           
                    Else
                        MDI.mnuPopUpEstrOrgNiv1(2).Enabled = True
                        MDI.mnuPopUpEstrOrgNiv1(1).Enabled = True
'                        MDI.mnuPopUpEstrOrgNiv1(4).Enabled = True
                        MDI.mnuPopUpEstrOrgNiv1(5).Enabled = True

                        cmdA�adir.Enabled = True
                        cmdModif.Enabled = True
                        If nodx.Children = 0 Then
'                            MDI.mnuPopUpEstrOrgNiv1(5).Enabled = True
                            MDI.mnuPopUpEstrOrgNiv1(6).Enabled = True
                            cmdEli.Enabled = True
                        Else
'                            MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv1(6).Enabled = False
                            cmdEli.Enabled = False
                        End If
                        
                    End If
                    MDI.mnuPopUpEstrOrgNiv1(9).Visible = m_bModifCodigo
                    
                    NodoSeleccionado nodx
    
                    If oUON1Seleccionada.SePuedeDarBajaUON1 Then
'                        MDI.mnuPopUpEstrOrgNiv1(6).Visible = True
                        MDI.mnuPopUpEstrOrgNiv1(7).Visible = True
                        cmdBajaLog.Enabled = True
                    Else
'                        MDI.mnuPopUpEstrOrgNiv1(6).Visible = False
                        MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                        cmdBajaLog.Enabled = False
                    End If
    
            
                    Exit Sub
                Else
                
                    If Trim(basOptimizacion.gUON2Usuario) <> "" Then
                           
                        cmdA�adir.Enabled = False
                        MDI.mnuPopUpEstrOrgNiv1(2).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv1(1).Enabled = False

'                        MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False
'                        MDI.mnuPopUpEstrOrgNiv1(4).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv1(6).Enabled = False
                        MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False
                        
                        cmdModif.Enabled = False
                        cmdEli.Enabled = False
'                        MDI.mnuPopUpEstrOrgNiv1(6).Visible = False
                        MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                        cmdBajaLog.Enabled = False
'                        MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                        MDI.mnuPopUpEstrOrgNiv1(8).Visible = False
                        
                    Else
                        If nodx.Children = 0 Then
'                            MDI.mnuPopUpEstrOrgNiv1(5).Enabled = True
                            MDI.mnuPopUpEstrOrgNiv1(6).Enabled = True
                            cmdEli.Enabled = True
                        Else
'                            MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False
                            MDI.mnuPopUpEstrOrgNiv1(6).Enabled = False
                            cmdEli.Enabled = False
                        End If
                    End If
            
                    Exit Sub
                
                End If
            End If
            
            If nodx.Image <> "UON1BAJA" And nodx.Image <> "SELLOBAJA" Then
            
                MDI.mnuPopUpEstrOrgNiv1(2).Enabled = True
                MDI.mnuPopUpEstrOrgNiv1(1).Enabled = True
'                MDI.mnuPopUpEstrOrgNiv1(4).Enabled = True
                MDI.mnuPopUpEstrOrgNiv1(5).Enabled = True
                
                cmdModif.Enabled = True
                cmdA�adir.Enabled = True
                If nodx.Children = 0 Then
'                    MDI.mnuPopUpEstrOrgNiv1(5).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv1(6).Enabled = True
                    cmdEli.Enabled = True
                Else
'                    MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv1(6).Enabled = False
                    cmdEli.Enabled = False
                End If
                            
                NodoSeleccionado nodx
    
                If oUON1Seleccionada.SePuedeDarBajaUON1 Then
'                    MDI.mnuPopUpEstrOrgNiv1(6).Visible = True
                    MDI.mnuPopUpEstrOrgNiv1(7).Visible = True
                    cmdBajaLog.Enabled = True
                Else
'                    MDI.mnuPopUpEstrOrgNiv1(6).Visible = False
                    MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                    cmdBajaLog.Enabled = False
                
                End If
            Else
            
'                MDI.mnuPopUpEstrOrgNiv1(5).Visible = True
                MDI.mnuPopUpEstrOrgNiv1(6).Visible = True
                If nodx.Children = 0 Then
'                    MDI.mnuPopUpEstrOrgNiv1(5).Enabled = True
                    MDI.mnuPopUpEstrOrgNiv1(6).Enabled = True
                    cmdEli.Enabled = True
                Else
'                    MDI.mnuPopUpEstrOrgNiv1(5).Enabled = False
                    MDI.mnuPopUpEstrOrgNiv1(6).Enabled = False
                    cmdEli.Enabled = False
                End If
                    If InStr(1, nodx.Parent.Image, "BAJA") Then
'                        MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
                        MDI.mnuPopUpEstrOrgNiv1(8).Visible = False
                    Else
'                        MDI.mnuPopUpEstrOrgNiv1(7).Visible = True
                        MDI.mnuPopUpEstrOrgNiv1(8).Visible = True
                    End If
               
                    'MDI.mnuPopUpEstrOrgNiv1(7).Visible = True
            End If

            MDI.mnuPopUpEstrOrgNiv1(9).Visible = m_bModifCodigo
        Else
                    
            cmdA�adir.Enabled = False
            MDI.mnuPopUpEstrOrgNiv1(2).Visible = False
            MDI.mnuPopUpEstrOrgNiv1(1).Visible = False
'            MDI.mnuPopUpEstrOrgNiv1(5).Visible = False
'            MDI.mnuPopUpEstrOrgNiv1(4).Visible = False
'            MDI.mnuPopUpEstrOrgNiv1(3).Visible = False
'            MDI.mnuPopUpEstrOrgNiv1(9).Visible = False
            MDI.mnuPopUpEstrOrgNiv1(6).Visible = False
            MDI.mnuPopUpEstrOrgNiv1(5).Visible = False
            MDI.mnuPopUpEstrOrgNiv1(4).Visible = False
            MDI.mnuPopUpEstrOrgNiv1(10).Visible = False
            
            cmdModif.Enabled = False
            cmdEli.Enabled = False
            
'            MDI.mnuPopUpEstrOrgNiv1(6).Visible = False
            MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
            cmdBajaLog.Enabled = False

'            MDI.mnuPopUpEstrOrgNiv1(7).Visible = False
            MDI.mnuPopUpEstrOrgNiv1(8).Visible = False
        
            'no puedes cambiar c�digo cuando NO
            'tienes permiso para modificar la estructura (m_bModifEstr = TRUE)
            MDI.mnuPopUpEstrOrgNiv1(9).Visible = False
        End If

                 
        ' Configuramos los menus en caso de que haya menos niveles
        If gParametrosGenerales.giNEO <= 1 Then
            cmdA�adir.Enabled = False
            MDI.mnuPopUpEstrOrgNiv1(1).Visible = False

        End If
        
        'Gestion Centro de Coste
        If m_bGestionarCCVisible Or m_bConsultarCCVisible Then
            If nodx.Image = "UON1BAJA" Or nodx.Image = "SELLOBAJA" Then
                MDI.mnuPopUpEstrOrgNiv1(3).Enabled = False
            Else
                MDI.mnuPopUpEstrOrgNiv1(3).Visible = True
            End If
            MDI.mnuPopUpEstrOrgNiv1(4).Visible = True 'L�nea divisoria
        Else
            MDI.mnuPopUpEstrOrgNiv1(3).Visible = False
            If Not MDI.mnuPopUpEstrOrgNiv1(2).Visible And Not MDI.mnuPopUpEstrOrgNiv1(1).Visible Then
                MDI.mnuPopUpEstrOrgNiv1(4).Visible = False
            End If
        End If

                         
    Case "UON0"
        
        cmdModif.Enabled = False
        cmdEli.Enabled = False
        cmdReubicar.Enabled = False
        
        
        If m_bModifEstr Then
            If bRDep Then
                cmdA�adir.Enabled = False
                MDI.mnuPopUpEstrOrgNiv0(2) = False
                MDI.mnuPopUpEstrOrgNiv0(1) = False
                Exit Sub
            Else
                If bRUO Then
                    If Trim(basOptimizacion.gUON1Usuario) = "" And Trim(basOptimizacion.gUON2Usuario) = "" And Trim(basOptimizacion.gUON3Usuario) = "" Then
                        cmdA�adir.Enabled = True
                        MDI.mnuPopUpEstrOrgNiv0(2) = True
                        MDI.mnuPopUpEstrOrgNiv0(1) = True
                    Else
                        cmdA�adir.Enabled = False
                        MDI.mnuPopUpEstrOrgNiv0(2) = False
                        MDI.mnuPopUpEstrOrgNiv0(1) = False
                    End If
                    Exit Sub
                End If
            End If
            cmdA�adir.Enabled = True
            MDI.mnuPopUpEstrOrgNiv0(2) = True
            MDI.mnuPopUpEstrOrgNiv0(1) = True
        Else
            cmdA�adir.Enabled = False
            MDI.mnuPopUpEstrOrgNiv0(2) = False
            MDI.mnuPopUpEstrOrgNiv0(1) = False
            MDI.mnuPopUpEstrOrgNiv0(3).Visible = False
        End If
                
 End Select

End If
           
End Sub

Private Sub NodoSeleccionado(ByRef nodx As node)
Dim teserror As TipoErrorSummit

If Not nodx Is Nothing Then
    
    Set oUON1Seleccionada = Nothing
    Set oUON2Seleccionada = Nothing
    Set oUON3Seleccionada = Nothing
    Set oUON4Seleccionada = Nothing
    Set oDepAsocSeleccionado = Nothing
    Set oPersonaSeleccionada = Nothing
    
    Select Case Left(nodx.Tag, 4)
    
    Case "DEP0", "DEP1", "DEP2", "DEP3"
        Set oDepAsocSeleccionado = oFSGSRaiz.generar_CDepAsociado
        oDepAsocSeleccionado.Cod = DevolverCod(nodx)
        
        If nodx.Parent.Parent Is Nothing Then
            ' Es un departamento del nivel 0
        Else
            If nodx.Parent.Parent.Parent Is Nothing Then
                oDepAsocSeleccionado.CodUON1 = DevolverCod(nodx.Parent)
            Else
                If nodx.Parent.Parent.Parent.Parent Is Nothing Then
                    oDepAsocSeleccionado.CodUON2 = DevolverCod(nodx.Parent)
                    oDepAsocSeleccionado.CodUON1 = DevolverCod(nodx.Parent.Parent)
                Else
                    oDepAsocSeleccionado.CodUON3 = DevolverCod(nodx.Parent)
                    oDepAsocSeleccionado.CodUON2 = DevolverCod(nodx.Parent.Parent)
                    oDepAsocSeleccionado.CodUON1 = DevolverCod(nodx.Parent.Parent.Parent)
                End If
            End If
        End If
    
    Case "UON1"
            
            Set oUON1Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel1
            oUON1Seleccionada.Cod = DevolverCod(nodx)
            oUON1Seleccionada.Den = DevolverDen(nodx)
            g_vUON1 = DevolverCod(nodx)
            g_vUON2 = Null
            g_vUON3 = Null
            g_vUON4 = Null
            
    Case "UON2"
            
            Set oUON2Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel2
            oUON2Seleccionada.Cod = DevolverCod(nodx)
            oUON2Seleccionada.Den = DevolverDen(nodx)
            oUON2Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent)
            g_vUON2 = DevolverCod(nodx)
            g_vUON1 = DevolverCod(nodx.Parent)
            g_vUON3 = Null
            g_vUON4 = Null
            
    Case "UON3"
            
            Set oUON3Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel3
            oUON3Seleccionada.Cod = DevolverCod(nodx)
            oUON3Seleccionada.Den = DevolverDen(nodx)
            oUON3Seleccionada.CodUnidadOrgNivel2 = DevolverCod(nodx.Parent)
            oUON3Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent.Parent)
            g_vUON3 = DevolverCod(nodx)
            g_vUON2 = DevolverCod(nodx.Parent)
            g_vUON1 = DevolverCod(nodx.Parent.Parent)
            g_vUON4 = Null
    
    Case "UON4"
            
            Set oUON4Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel4
            oUON4Seleccionada.Cod = DevolverCod(nodx)
            oUON4Seleccionada.Den = DevolverDen(nodx)
            oUON4Seleccionada.CodUnidadOrgNivel3 = DevolverCod(nodx.Parent)
            oUON4Seleccionada.CodUnidadOrgNivel2 = DevolverCod(nodx.Parent.Parent)
            oUON4Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent.Parent.Parent)
            g_vUON4 = DevolverCod(nodx)
            g_vUON3 = DevolverCod(nodx.Parent)
            g_vUON2 = DevolverCod(nodx.Parent.Parent)
            g_vUON1 = DevolverCod(nodx.Parent.Parent.Parent)
           
    Case "PER0"
               
            Set oPersonaSeleccionada = Nothing
            Set oPersonaSeleccionada = oFSGSRaiz.Generar_CPersona
            oPersonaSeleccionada.Cod = DevolverCod(nodx)
            oPersonaSeleccionada.CodDep = DevolverCod(nodx.Parent)
            Set oIBaseDatos = oPersonaSeleccionada
            teserror = oIBaseDatos.IniciarEdicion
            
            If Not teserror.NumError = TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If

            
            
    Case "PER1"
       
            Set oPersonaSeleccionada = Nothing
            Set oPersonaSeleccionada = oFSGSRaiz.Generar_CPersona
            oPersonaSeleccionada.Cod = DevolverCod(nodx)
            oPersonaSeleccionada.UON1 = DevolverCod(nodx.Parent.Parent)
            oPersonaSeleccionada.CodDep = DevolverCod(nodx.Parent)
            Set oIBaseDatos = oPersonaSeleccionada
            teserror = oIBaseDatos.IniciarEdicion
            
            If Not teserror.NumError = TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If
            
    Case "PER2"
               
            Set oPersonaSeleccionada = Nothing
            Set oPersonaSeleccionada = oFSGSRaiz.Generar_CPersona
            oPersonaSeleccionada.Cod = DevolverCod(nodx)
            oPersonaSeleccionada.UON1 = DevolverCod(nodx.Parent.Parent.Parent)
            oPersonaSeleccionada.UON2 = DevolverCod(nodx.Parent.Parent)
            oPersonaSeleccionada.CodDep = DevolverCod(nodx.Parent)
            Set oIBaseDatos = oPersonaSeleccionada
            teserror = oIBaseDatos.IniciarEdicion
            
            If Not teserror.NumError = TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If
    
    Case "PER3"
               
            Set oPersonaSeleccionada = Nothing
            Set oPersonaSeleccionada = oFSGSRaiz.Generar_CPersona
            oPersonaSeleccionada.Cod = DevolverCod(nodx)
            oPersonaSeleccionada.UON1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
            oPersonaSeleccionada.UON2 = DevolverCod(nodx.Parent.Parent.Parent)
            oPersonaSeleccionada.UON3 = DevolverCod(nodx.Parent.Parent)
            oPersonaSeleccionada.CodDep = DevolverCod(nodx.Parent)
            Set oIBaseDatos = oPersonaSeleccionada
            teserror = oIBaseDatos.IniciarEdicion
            
            If Not teserror.NumError = TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If
    
    End Select
End If

End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    m_bGestionarCCVisible = False 'BLP
    m_bModifEstr = False
    m_bModifPersonas = False
    
    'Establezco si se puede modificar o no la estrorg y personas
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGModificar)) Is Nothing) Then
        m_bModifEstr = True
        m_bModifPersonas = True
    Else
        cmdA�adir.Visible = False
        cmdModif.Visible = False
        cmdEli.Visible = False
        cmdReubicar.Visible = False
        cmdBajaLog.Visible = False
        
        cmdRestaurar.Left = tvwestrorg.Left
        cmdBuscar.Left = cmdRestaurar.Left + cmdRestaurar.Width + 100
        cmdListado.Left = cmdBuscar.Left + cmdBuscar.Width + 100
    End If
    
    m_bModifCodigo = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGModificarCodigo)) Is Nothing)
    m_bModifCodigoDest = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGDESTModificarCodigo)) Is Nothing)
    m_bPermitGenerico = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGDESTPermitGenerico)) Is Nothing)
    
    'Si hay integraci�n solo entrada en EstrOrg y/o Usuarios no deja modificar
    If m_bModifEstr And gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.UnidadesOrganizativas) And gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Usuarios) Then
        m_bModifEstr = False
        m_bModifPersonas = False
        
        cmdA�adir.Visible = False
        cmdModif.Visible = False
        cmdEli.Visible = False
        cmdReubicar.Visible = False
        cmdBajaLog.Visible = False
        cmdRestaurar.Left = tvwestrorg.Left
        cmdBuscar.Left = cmdRestaurar.Left + cmdRestaurar.Width + 100
        cmdListado.Left = cmdBuscar.Left + cmdBuscar.Width + 100
    ElseIf m_bModifEstr And gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.UnidadesOrganizativas) Then
        m_bModifEstr = False
    ElseIf m_bModifPersonas And gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Usuarios) Then
        m_bModifPersonas = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGConsultarDest)) Is Nothing) Then
        m_bConDestinos = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGModificarDest)) Is Nothing) Then
        m_bModifDestinos = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGAsignarDest)) Is Nothing) Then
         m_bAsigDestinos = True
    End If

    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGGestionarCC)) Is Nothing) Then
        m_bGestionarCCVisible = True
    End If
    
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGConsultarCC)) Is Nothing) Then
        m_bConsultarCCVisible = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestUO)) Is Nothing) Then
        bRUO = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestDep)) Is Nothing) Then
        bRDep = True
    End If

    ''' Integraci�n, si el sentido es solo entrada no se pueden modificar destinos
    If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Dest) Then
        m_bModifDestinos = False
    End If
End Sub


Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Departamentos asociados
Dim oDepsAsocN0 As CDepAsociados
Dim oDepsAsocN1 As CDepAsociados
Dim oDepsAsocN2 As CDepAsociados
Dim oDepsAsocN3 As CDepAsociados
Dim oDepAsoc As CDepAsociado

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
Dim oUnidadesorgN4 As CUnidadesOrgNivel4

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
Dim oUON4 As CUnidadOrgNivel4

' Personas
Dim oPersN0 As CPersonas
Dim oPersN1 As CPersonas
Dim oPersN2 As CPersonas
Dim oPersN3 As CPersonas
Dim oPer As CPersona

' Otras
Dim nodx As node
Dim varCodPersona As Variant
Dim sKeyImage As String

    
    sOrdenListadoDen = bOrdenadoPorDen

    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    Set oUnidadesorgN4 = oFSGSRaiz.Generar_CUnidadesOrgNivel4
     
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
         
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And (bRUO Or bRDep) Then
        
        varCodPersona = oUsuarioSummit.Persona.Cod
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False, chkBajaLog.Value
        
        ' Cargamos las personas  de esos departamentos.
        
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, chkBajaLog.Value
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , chkBajaLog.Value
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , chkBajaLog.Value
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, chkBajaLog.Value
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
                    oUnidadesorgN4.CargarTodasLasUnidadesOrgNivel4 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                End If
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
                    
                Next
        End Select
        
        
    Else
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False, chkBajaLog.Value
        oPersN0.CargarTodasLasPersonas , , , , 0, Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                oPersN1.CargarTodasLasPersonas , , , , 1, Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                oPersN2.CargarTodasLasPersonas , , , , 2, Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
                oPersN1.CargarTodasLasPersonas , , , , 1, Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , chkBajaLog.Value
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, , , , , chkBajaLog.Value
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen, , , , , chkBajaLog.Value
                If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
                    oUnidadesorgN4.CargarTodasLasUnidadesOrgNivel4 , , , , , , , , , , , , , , chkBajaLog.Value
                End If
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True, chkBajaLog.Value
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False, chkBajaLog.Value
                oPersN1.CargarTodasLasPersonas , , , , 1, Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
                oPersN2.CargarTodasLasPersonas , , , , 2, Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
                oPersN3.CargarTodasLasPersonas , , , , 3, Not bOrdenadoPorDen, , bOrdenadoPorDen, , False, , , , chkBajaLog.Value
                
        End Select
        
        
    End If
    
        
        
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        If Not IsNull(oUON1.idEmpresa) Then
            If oUON1.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            Else
                sKeyImage = "SELLO"
            End If
        Else
            If oUON1.BajaLog = 1 Then
                sKeyImage = "UON1BAJA"
            Else
                sKeyImage = "UON1"
            End If
        End If
        
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, sKeyImage)
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        If Not IsNull(oUON2.idEmpresa) Then
            If oUON2.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            Else
                sKeyImage = "SELLO"
            End If
        Else
            If oUON2.BajaLog = 1 Then
                sKeyImage = "UON2BAJA"
            Else
                sKeyImage = "UON2"
            End If
        End If
        
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, sKeyImage)
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        If Not IsNull(oUON3.idEmpresa) Then
            If oUON3.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            Else
                sKeyImage = "SELLO"
            End If

        Else
            If oUON3.BajaLog = 1 Then
                sKeyImage = "UON3BAJA"
            Else
                sKeyImage = "UON3"
            End If
        End If
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, sKeyImage)
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
    For Each oUON4 In oUnidadesorgN4
        
        scod1 = oUON4.CodUnidadOrgNivel1 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON4.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON4.CodUnidadOrgNivel2 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON4.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON4.CodUnidadOrgNivel3 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON4.CodUnidadOrgNivel3))
        scod4 = scod3 & oUON4.Cod & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(oUON4.Cod))
        If Not IsNull(oUON4.idEmpresa) Then
            If oUON4.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            Else
                sKeyImage = "SELLO"
            End If

        Else
            If oUON4.BajaLog = 1 Then
                sKeyImage = "UON4BAJA"
            Else
                sKeyImage = "UON4"
            End If
        End If
        Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "UON4" & scod4, CStr(oUON4.Cod) & " - " & oUON4.Den, sKeyImage)
        nodx.Tag = "UON4" & CStr(oUON4.Cod)
            
    Next
    
    
    
    ' Departamentos
    
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And (bRUO Or bRDep) Then
    
        For Each oDepAsoc In oDepsAsocN0
            
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            If oDepAsoc.BajaLog = 1 Then
                Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "DepartamentoBaja")
            Else
                Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            End If
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.codEqp) Then
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCestaBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                    End If
                Else
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
            Next
        
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            If oDepAsoc.BajaLog = 1 Then
                Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "DepartamentoBaja")
            Else
                Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            End If
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.codEqp) Then
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCestaBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                    End If
                Else
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
            Next
        
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            If oDepAsoc.BajaLog = 1 Then
                Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "DepartamentoBaja")
            Else
                Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            End If
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.codEqp) Then
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCestaBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                    End If
                Else
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
            Next
       
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            If oDepAsoc.BajaLog = 1 Then
                Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "DepartamentoBaja")
            Else
                Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            End If
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.codEqp) Then
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCestaBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                    End If
                Else
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        Next
    
    Else
    
        'Departamentos
            
            For Each oDepAsoc In oDepsAsocN0
            
                scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                If oDepAsoc.BajaLog = 1 Then
                    Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "DepartamentoBaja")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                End If
                nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            
            Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            If oDepAsoc.BajaLog = 1 Then
                Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "DepartamentoBaja")
            Else
                Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            End If
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                            
            If oDepAsoc.BajaLog = 1 Then
                Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "DepartamentoBaja")
            Else
                Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            End If
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            If oDepAsoc.BajaLog = 1 Then
                Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "DepartamentoBaja")
            Else
                Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            End If
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            
        Next
                
            'Personas
            
            For Each oPer In oPersN0
                scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not IsNull(oPer.codEqp) Then
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerCestaBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                    End If
                Else
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
            Next
            
            For Each oPer In oPersN1
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not IsNull(oPer.codEqp) Then
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCestaBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                    End If
                Else
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
                
            Next
            
            For Each oPer In oPersN2
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not IsNull(oPer.codEqp) Then
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCestaBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                    End If
                Else
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
            Next
            
            For Each oPer In oPersN3
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
                scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not IsNull(oPer.codEqp) Then
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCestaBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                    End If
                Else
                    If oPer.BajaLog = 1 Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oUON4 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    Set oUnidadesorgN4 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
End Sub

Private Sub chkBajaLog_Click()
    sstabEstrorg.TabVisible(1) = False
    tvwestrorg.Nodes.clear
    
    GenerarEstructuraOrg False
    
    tvwestrorg.Nodes.Item("UON0").Selected = True
    ConfigurarInterfazSeguridad tvwestrorg.Nodes.Item("UON0")
    
    sstabEstrorg.TabVisible(1) = False
    
End Sub

Public Sub cmdA�adir_Click()

Dim nodx As node
    
    Set nodx = tvwestrorg.selectedItem
    
    NodoSeleccionado nodx
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 4)
            
            Case "UON0"
                        Accion = ACCUON1Anya

                        frmESTRORGAnyadir.optUO.caption = m_sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON1)
                        frmESTRORGAnyadir.Show vbModal
                        
            Case "UON1"
                        Accion = ACCUON2Anya
                        Set frmESTRORGDetalle.m_oUnidadesOrgN1 = oUON1Seleccionada
                        frmESTRORGDetalle.mCodUON1 = oUON1Seleccionada.Cod
                        
                        frmESTRORGAnyadir.optUO.caption = m_sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON2)
                        frmESTRORGAnyadir.Show vbModal
                        
            Case "UON2"
                        Accion = ACCUON3Anya
                        
                        Set frmESTRORGDetalle.m_oUnidadesOrgN2 = oUON2Seleccionada
                        frmESTRORGDetalle.mCodUON2 = oUON2Seleccionada.Cod
                        
                        frmESTRORGAnyadir.optUO.caption = m_sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON3)
                        frmESTRORGAnyadir.Show vbModal
                        
            Case "UON3"
                        'Comprobar si ya existe asignado un Centro de coste
                        If (gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM) Then
                                Accion = ACCUON4Anya
                                Set frmESTRORGDetalle.m_oUnidadesOrgN3 = oUON3Seleccionada
                                frmESTRORGDetalle.mCodUON3 = oUON3Seleccionada.Cod
                                
                                frmESTRORGAnyadir.optUO.caption = m_sIdiA�adir & " " & gParametrosGenerales.gsDEN_UON4
                                frmESTRORGAnyadir.Show vbModal
                        Else
                                mnuPopUpAnyadirDep
                        End If
                        
            Case "DEP0"
                        Accion = ACCPerAnya
                        Set oPersonaSeleccionada = oFSGSRaiz.Generar_CPersona
                        Set oIBaseDatos = oPersonaSeleccionada
                        oPersonaSeleccionada.CodDep = DevolverCod(nodx)
                        frmESTRORGPersona.Show 1
                        
            Case "DEP1"
                        Accion = ACCPerAnya
                        Set oPersonaSeleccionada = oFSGSRaiz.Generar_CPersona
                        Set oIBaseDatos = oPersonaSeleccionada
                        oPersonaSeleccionada.CodDep = DevolverCod(nodx)
                        oPersonaSeleccionada.UON1 = DevolverCod(nodx.Parent)
                        frmESTRORGPersona.Show 1
                       
            Case "DEP2"
                        Accion = ACCPerAnya
                        Set oPersonaSeleccionada = oFSGSRaiz.Generar_CPersona
                        Set oIBaseDatos = oPersonaSeleccionada
                        oPersonaSeleccionada.CodDep = DevolverCod(nodx)
                        oPersonaSeleccionada.UON2 = DevolverCod(nodx.Parent)
                        oPersonaSeleccionada.UON1 = DevolverCod(nodx.Parent.Parent)
                        frmESTRORGPersona.Show 1
                        
            Case "DEP3"
                        Accion = ACCPerAnya
                        Set oPersonaSeleccionada = oFSGSRaiz.Generar_CPersona
                        Set oIBaseDatos = oPersonaSeleccionada
                        oPersonaSeleccionada.CodDep = DevolverCod(nodx)
                        oPersonaSeleccionada.UON3 = DevolverCod(nodx.Parent)
                        oPersonaSeleccionada.UON2 = DevolverCod(nodx.Parent.Parent)
                        oPersonaSeleccionada.UON1 = DevolverCod(nodx.Parent.Parent.Parent)
                        frmESTRORGPersona.Show 1
                        
        
        End Select
    End If

End Sub

''' Revisado por: blp. Fecha: 29/10/2012
''' Inicia el proceso de baja l�gica de un usuario
''' Llamada desde Evento click en el control mnuPopUpEstrOrgBajaLogPer, que cuelga de mnuPopUpEstrOrgPer en el MDI. M�x 0,1 seg.
Public Sub cmdBajaLog_Click()

Dim nodx As node
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim bQA As Boolean
Dim bPM As Boolean
Dim sSustitutoEliminado As String
                
Dim btipoPres1 As Boolean
Dim btipoPres2 As Boolean
Dim btipoPres3 As Boolean
Dim btipoPres4 As Boolean
                
    'Da de baja l�gica la persona seleccionada
    
    teserror.NumError = TESnoerror
    
    Set nodx = tvwestrorg.selectedItem

    NodoSeleccionado nodx
    
    If nodx Is Nothing Then Exit Sub
    
'    Set oUON1Seleccionada = Nothing
'    Set oUON2Seleccionada = Nothing
'    Set oUON3Seleccionada = Nothing
'    Set oDepAsocSeleccionado = Nothing
'    Set oPersonaSeleccionada = Nothing

    If Not oPersonaSeleccionada Is Nothing Then
    
    g_sSustUON1 = ""
    g_sSustUON2 = ""
    g_sSustUON3 = ""
    g_sSustDep = ""
    g_sSustituto = ""
    g_sSustitutoEnCat = ""
    
        Accion = ACCPerBajaLog
        
        If teserror.NumError = TESnoerror Then
            irespuesta = oMensajes.PreguntaBajaLogica(CStr(oPersonaSeleccionada.Cod) & " (" & oPersonaSeleccionada.Apellidos & " " & oPersonaSeleccionada.nombre & ")")
            If irespuesta = vbYes Then
                oPersonaSeleccionada.CargarTodosLosDatos True   'Comprueba si es un usuario del EP o del GS.
                
                If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso And oPersonaSeleccionada.AccesoFSQA = True Then
                    bQA = True
                End If
                If (gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso And oPersonaSeleccionada.AccesoFSPM = True) _
                Or (gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.SinAcceso And oPersonaSeleccionada.AccesoContratos = True) _
                Or (gParametrosGenerales.gsAccesoFSIM <> TipoAccesoFSIM.SinAcceso And oPersonaSeleccionada.AccesoFSIM = True) Then
                    bPM = True
                End If
                If oPersonaSeleccionada.AccesoFSEP = True Or Not IsEmpty(oPersonaSeleccionada.Usuario) Or bQA = True Or bPM = True Then
                    'ahora si es un usuario del GS o del EP o de PM o QA lo pregunta otra vez
                    irespuesta = oMensajes.PreguntaBajaLogicaAcc(oPersonaSeleccionada.AccesoFSEP, bQA, bPM)
                    If irespuesta = vbNo Then Exit Sub
                End If
                
                'Aqu� comprueba si la persona a dar de baja se encuentra como aprobador o persona asignada en el PM o QA
                'Si es as� muestra el organigrama para seleccionar otra persona que lo sustituya en esos flujos
                If oPersonaSeleccionada.EsAprobadorPM = True Then
                    frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_PER"
                    frmSOLPersonaSustituir.g_sCodPersona = oPersonaSeleccionada.Cod
                    frmSOLPersonaSustituir.g_sDenominacion = NullToStr(oPersonaSeleccionada.nombre) & " " & oPersonaSeleccionada.Apellidos
                    frmSOLPersonaSustituir.Show vbModal
                    If g_sSustituto = "" Then  'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
                        Exit Sub
                    Else
                        oPersonaSeleccionada.SustitutoEnPM = g_sSustituto
                    End If
                End If
            
                If oPersonaSeleccionada.EsAprobadorCatalogo = True Then
                    frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_PER_CAT"
                    frmSOLPersonaSustituir.g_sCodPersona = oPersonaSeleccionada.Cod
                    frmSOLPersonaSustituir.g_sDenominacion = NullToStr(oPersonaSeleccionada.nombre) & " " & oPersonaSeleccionada.Apellidos
                    frmSOLPersonaSustituir.Show vbModal
                    If g_sSustitutoEnCat = "" Then  'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
                        Exit Sub
                    Else
                        oPersonaSeleccionada.SustitutoEnCatSeguridad = g_sSustitutoEnCat
                    End If
                End If
                
                sSustitutoEliminado = oPersonaSeleccionada.ListarSustituidosPor
                If sSustitutoEliminado <> "" Then
                    oMensajes.NoEliminarPersonaPorSerSustituto sSustitutoEliminado, True
                    Exit Sub
                End If
            
                'Ahora da de baja l�gica la persona:
                Screen.MousePointer = vbHourglass
                teserror = oPersonaSeleccionada.DarBajaLogica(bSustituirTodas)
                
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                Else
                    If chkBajaLog.Value = vbChecked Then
                        'Marca la persona como de baja l�gica.
                        If Not IsNull(oPersonaSeleccionada.codEqp) Then
                            nodx.Image = "PerCestaBaja"
                        Else
                            nodx.Image = "PerBaja"
                        End If
                        cmdBajaLog.Enabled = False
                        cmdModif.Enabled = False
                        
                    Else
                        EliminarPersonaDeEstructura
                    End If
                    RegistrarAccion AccionesSummit.ACCPerBajaLog, "Cod:" & CStr(oPersonaSeleccionada.Cod)
                End If
            End If
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            oIBaseDatos.CancelarEdicion
            Set oIBaseDatos = Nothing
        End If
    
    End If
    
    If Not oUON1Seleccionada Is Nothing Then
        irespuesta = oMensajes.PreguntaBajaLogicaUON(CStr(nodx.Text))
        If irespuesta = vbYes Then
        
            oUON1Seleccionada.ComprobarPresupuestos btipoPres1, btipoPres2, btipoPres3, btipoPres4
            irespuesta = vbYes
            If btipoPres1 Or btipoPres2 Or btipoPres3 Or btipoPres4 Then
                irespuesta = oMensajes.PreguntaBajaLogicaUONConPresupuestos(btipoPres1, btipoPres2, btipoPres3, btipoPres4, gParametrosGenerales.gsPlurPres1, _
                                        gParametrosGenerales.gsPlurPres2, gParametrosGenerales.gsPlurPres3, gParametrosGenerales.gsPlurPres4)
            End If
        
            If irespuesta = vbYes Then
        
                teserror = oUON1Seleccionada.DarBajaLogica
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                Else
                    If chkBajaLog.Value = vbChecked Then
                        'Marca la persona como de baja l�gica.
                        If nodx.Image = "UON1" Then
                            nodx.Image = "UON1BAJA"
                        Else
                            nodx.Image = "SELLOBAJA"
                        End If
                        cmdBajaLog.Enabled = False
                        cmdModif.Enabled = False
                        
                        CheckearArbol nodx
                    Else
                        'EliminarDepartamentoDeEstructura
                        EliminarUONDeEstructura
                        'Habria que recargar el arbol
                    End If
                    'RegistrarAccion accionessummit.ACCPerBajaLog, "Cod:" & CStr(oPersonaSeleccionada.Cod)
                End If
            End If
        End If
    
    End If

    If Not oUON2Seleccionada Is Nothing Then
    
        irespuesta = oMensajes.PreguntaBajaLogicaUON(CStr(nodx.Text))
        If irespuesta = vbYes Then
        
            oUON2Seleccionada.ComprobarPresupuestos btipoPres1, btipoPres2, btipoPres3, btipoPres4
            irespuesta = vbYes
            If btipoPres1 Or btipoPres2 Or btipoPres3 Or btipoPres4 Then
                irespuesta = oMensajes.PreguntaBajaLogicaUONConPresupuestos(btipoPres1, btipoPres2, btipoPres3, btipoPres4, gParametrosGenerales.gsPlurPres1, _
                                        gParametrosGenerales.gsPlurPres2, gParametrosGenerales.gsPlurPres3, gParametrosGenerales.gsPlurPres4)
            End If
        
            If irespuesta = vbYes Then
            
                teserror = oUON2Seleccionada.DarBajaLogica
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror

                Else
                    If chkBajaLog.Value = vbChecked Then
                        'Marca la persona como de baja l�gica.
                        If nodx.Image = "UON2" Then
                            nodx.Image = "UON2BAJA"
                        Else
                            nodx.Image = "SELLOBAJA"
                        End If
                        cmdBajaLog.Enabled = False
                        cmdModif.Enabled = False
                        CheckearArbol nodx
                    Else
                        EliminarUONDeEstructura
                        'Habria que recargar el arbol
                    End If
                    'RegistrarAccion accionessummit.ACCPerBajaLog, "Cod:" & CStr(oPersonaSeleccionada.Cod)
                End If
                
            End If
        End If
    
    End If

    If Not oUON3Seleccionada Is Nothing Then
        irespuesta = oMensajes.PreguntaBajaLogicaUON(CStr(nodx.Text))
        If irespuesta = vbYes Then
            oUON3Seleccionada.ComprobarPresupuestos btipoPres1, btipoPres2, btipoPres3, btipoPres4
            irespuesta = vbYes
            If btipoPres1 Or btipoPres2 Or btipoPres3 Or btipoPres4 Then
                irespuesta = oMensajes.PreguntaBajaLogicaUONConPresupuestos(btipoPres1, btipoPres2, btipoPres3, btipoPres4, gParametrosGenerales.gsPlurPres1, _
                                        gParametrosGenerales.gsPlurPres2, gParametrosGenerales.gsPlurPres3, gParametrosGenerales.gsPlurPres4)
            End If
            
            If irespuesta = vbYes Then
                teserror = oUON3Seleccionada.DarBajaLogica
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                Else
                    If chkBajaLog.Value = vbChecked Then
                        'Marca la persona como de baja l�gica.
                        If nodx.Image = "UON3" Then
                            nodx.Image = "UON3BAJA"
                        Else
                            nodx.Image = "SELLOBAJA"
                        End If
                        cmdBajaLog.Enabled = False
                        cmdModif.Enabled = False
                        CheckearArbol nodx
                    Else
                        EliminarUONDeEstructura
                        'Habr�a que recargar el �rbol
                    End If
                    'RegistrarAccion accionessummit.ACCPerBajaLog, "Cod:" & CStr(oPersonaSeleccionada.Cod)
                End If
            End If
        End If
    End If
    
    If Not oUON4Seleccionada Is Nothing Then
        irespuesta = oMensajes.PreguntaBajaLogicaUON4(CStr(nodx.Text))
        
        If irespuesta = vbYes Then
            teserror = oUON4Seleccionada.DarBajaLogica
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
            Else
                If chkBajaLog.Value = vbChecked Then
                    'Marca la persona como de baja l�gica.
                    If nodx.Image = "UON4" Then
                        nodx.Image = "UON4BAJA"
                    End If
                    cmdBajaLog.Enabled = False
                    cmdModif.Enabled = False
                    CheckearArbol nodx
                Else
                    EliminarUONDeEstructura
                    'Habria que recargar el arbol
                End If
            End If

        End If
    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdBuscar_Click()
    Dim iTipoBusqueda As Integer
    
    iTipoBusqueda = MostrarFormESTRORGBuscar(oGestorIdiomas, gParametrosInstalacion.gIdioma, gParametrosGenerales.giNEO)
    
    Select Case iTipoBusqueda
        Case 1
            frmESTRORGBuscarPersona.Show 1
        Case 2
            frmESTRORGBuscarUO.Show 1
        Case 3
            frmESTRORGBuscarDep.Show 1
    End Select
End Sub

Public Sub cmdEli_Click()
Dim nodx As node
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim sCodigoEliminado As String, sSustitutoEliminado As String

teserror.NumError = TESnoerror

Set nodx = tvwestrorg.selectedItem

NodoSeleccionado nodx

g_sSustUON1 = ""
g_sSustUON2 = ""
g_sSustUON3 = ""
g_sSustDep = ""
g_sSustituto = ""

If Not nodx Is Nothing Then
     
    Screen.MousePointer = vbHourglass
   
    Select Case Left(nodx.Tag, 4)
        
        Case "UON1"
            Accion = ACCUON1Eli
            oUON1Seleccionada.CargarEmpresa
            If Not IsNull(oUON1Seleccionada.idEmpresa) Then
                If oUON1Seleccionada.ImposibleEliminar Then
                    Screen.MousePointer = vbNormal
                    oMensajes.ImposibleEliminarUON
                    Exit Sub
                End If
            End If
            
            oUON1Seleccionada.Usuario = basOptimizacion.gvarCodUsuario
            Set oIBaseDatos = oUON1Seleccionada
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiUO & " " & CStr(oUON1Seleccionada.Cod) & " (" & oUON1Seleccionada.Den & ")")
                If irespuesta = vbYes Then
                    'Aqu� comprueba si la UO a dar de baja se encuentra como aprobador en el PM
                    'Si es as� muestra el organigrama para seleccionar otra UO,dep o persona que lo sustituya en esos flujos
                    If oUON1Seleccionada.ExisteEnFlujoPM = True Then
                        Screen.MousePointer = vbNormal
                        frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_UON"
                        frmSOLPersonaSustituir.g_sDenominacion = m_sIdiUO & " " & CStr(oUON1Seleccionada.Cod) & " (" & oUON1Seleccionada.Den & ")"
                        frmSOLPersonaSustituir.g_sUON1 = oUON1Seleccionada.Cod
                        frmSOLPersonaSustituir.Show vbModal
                        If g_sSustituto = "" And g_sSustUON1 = "" And g_sSustUON2 = "" And g_sSustUON3 = "" And g_sSustDep = "" Then 'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
                            Exit Sub
                        End If
                    End If
                    
                    oUON1Seleccionada.UON1SustPM = StrToNull(g_sSustUON1)
                    oUON1Seleccionada.UON2SustPM = StrToNull(g_sSustUON2)
                    oUON1Seleccionada.UON3SustPM = StrToNull(g_sSustUON3)
                    oUON1Seleccionada.DEPSustPM = StrToNull(g_sSustDep)
                    oUON1Seleccionada.PerSustPM = StrToNull(g_sSustituto)
                            
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        TratarError teserror
                    Else
                       Screen.MousePointer = vbHourglass
                       sCodigoEliminado = oUON1Seleccionada.Cod
                       EliminarUODeEstructura
                       RegistrarAccion AccionesSummit.ACCUON1Eli, "Cod:" & CStr(sCodigoEliminado)
                    End If
                End If
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If
    
    
        Case "UON2"
            
            Accion = ACCUON2Eli
            
            oUON2Seleccionada.CargarEmpresa
            If Not IsNull(oUON2Seleccionada.idEmpresa) Then
                If oUON2Seleccionada.ImposibleEliminar Then
                    oMensajes.ImposibleEliminarUON
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
            
            oUON2Seleccionada.Usuario = basOptimizacion.gvarCodUsuario
            Set oIBaseDatos = oUON2Seleccionada
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiUO & " " & CStr(oUON2Seleccionada.Cod) & " (" & oUON2Seleccionada.Den & ")")
                If irespuesta = vbYes Then
                    'Aqu� comprueba si la UO a dar de baja se encuentra como aprobador en el PM
                    'Si es as� muestra el organigrama para seleccionar otra UO,dep o persona que lo sustituya en esos flujos
                    If oUON2Seleccionada.ExisteEnFlujoPM = True Then
                        Screen.MousePointer = vbNormal
                        frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_UON"
                        frmSOLPersonaSustituir.g_sDenominacion = m_sIdiUO & " " & CStr(oUON2Seleccionada.Cod) & " (" & oUON2Seleccionada.Den & ")"
                        frmSOLPersonaSustituir.g_sUON1 = oUON2Seleccionada.CodUnidadOrgNivel1
                        frmSOLPersonaSustituir.g_sUON2 = oUON2Seleccionada.Cod
                        frmSOLPersonaSustituir.Show vbModal
                        If g_sSustituto = "" And g_sSustUON1 = "" And g_sSustUON2 = "" And g_sSustUON3 = "" And g_sSustDep = "" Then 'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
                            Exit Sub
                        End If
                    End If
                    
                    oUON2Seleccionada.UON1SustPM = StrToNull(g_sSustUON1)
                    oUON2Seleccionada.UON2SustPM = StrToNull(g_sSustUON2)
                    oUON2Seleccionada.UON3SustPM = StrToNull(g_sSustUON3)
                    oUON2Seleccionada.DEPSustPM = StrToNull(g_sSustDep)
                    oUON2Seleccionada.PerSustPM = StrToNull(g_sSustituto)
                            
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        TratarError teserror
                    Else
                       sCodigoEliminado = oUON2Seleccionada.Cod
                       EliminarUODeEstructura
                       RegistrarAccion AccionesSummit.ACCUON2Eli, "Cod:" & CStr(sCodigoEliminado)
                    End If
                End If
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If
    
        Case "UON3"
            
            Accion = ACCUON3Eli
            oUON3Seleccionada.CargarEmpresa
            If Not IsNull(oUON3Seleccionada.idEmpresa) Then
                If oUON3Seleccionada.ImposibleEliminar Then
                    oMensajes.ImposibleEliminarUON
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
            
            oUON3Seleccionada.Usuario = basOptimizacion.gvarCodUsuario
            Set oIBaseDatos = oUON3Seleccionada
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiUO & " " & CStr(oUON3Seleccionada.Cod) & " (" & oUON3Seleccionada.Den & ")")
                If irespuesta = vbYes Then
                    'Aqu� comprueba si la UO a dar de baja se encuentra como aprobador en el PM
                    'Si es as� muestra el organigrama para seleccionar otra UO,dep o persona que lo sustituya en esos flujos
                    If oUON3Seleccionada.ExisteEnFlujoPM = True Then
                        Screen.MousePointer = vbNormal
                        frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_UON"
                        frmSOLPersonaSustituir.g_sDenominacion = m_sIdiUO & " " & CStr(oUON3Seleccionada.Cod) & " (" & oUON3Seleccionada.Den & ")"
                        frmSOLPersonaSustituir.g_sUON1 = oUON3Seleccionada.CodUnidadOrgNivel1
                        frmSOLPersonaSustituir.g_sUON2 = oUON3Seleccionada.CodUnidadOrgNivel2
                        frmSOLPersonaSustituir.g_sUON3 = oUON3Seleccionada.Cod
                        frmSOLPersonaSustituir.Show vbModal
                        If g_sSustituto = "" And g_sSustUON1 = "" And g_sSustUON2 = "" And g_sSustUON3 = "" And g_sSustDep = "" Then 'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
                            Exit Sub
                        End If
                    End If
                    
                    oUON3Seleccionada.UON1SustPM = StrToNull(g_sSustUON1)
                    oUON3Seleccionada.UON2SustPM = StrToNull(g_sSustUON2)
                    oUON3Seleccionada.UON3SustPM = StrToNull(g_sSustUON3)
                    oUON3Seleccionada.DEPSustPM = StrToNull(g_sSustDep)
                    oUON3Seleccionada.PerSustPM = StrToNull(g_sSustituto)
                            
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        TratarError teserror
                    Else
                       sCodigoEliminado = oUON3Seleccionada.Cod
                       EliminarUODeEstructura
                       RegistrarAccion AccionesSummit.ACCUON3Eli, "Cod:" & CStr(sCodigoEliminado)
                    End If
                End If
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If
    
    Case "UON4"
            
                Accion = ACCUON4Eli
                oUON4Seleccionada.CargarEmpresa
                If Not IsNull(oUON4Seleccionada.idEmpresa) Then
                    If oUON4Seleccionada.ImposibleEliminar Then
                        oMensajes.ImposibleEliminarUON
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If

                oUON4Seleccionada.Usuario = basOptimizacion.gvarCodUsuario
                Set oIBaseDatos = oUON4Seleccionada
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError = TESnoerror Then
                    irespuesta = oMensajes.PreguntaEliminar(gParametrosGenerales.gsDEN_UON4 & ": " & CStr(oUON4Seleccionada.Cod) & " (" & oUON4Seleccionada.Den & ")")
                    If irespuesta = vbYes Then
'                        'Aqu� comprueba si la UO a dar de baja se encuentra como aprobador en el PM
'                        'Si es as� muestra el organigrama para seleccionar otra UO,dep o persona que lo sustituya en esos flujos
'                        If oUON4Seleccionada.ExisteEnFlujoPM = True Then
'                            Screen.MousePointer = vbNormal
'                            frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_UON"
'                            frmSOLPersonaSustituir.g_sDenominacion = m_sIdiUO & " " & CStr(oUON3Seleccionada.Cod) & " (" & oUON3Seleccionada.Den & ")"
'                            frmSOLPersonaSustituir.g_sUON1 = oUON3Seleccionada.CodUnidadOrgNivel1
'                            frmSOLPersonaSustituir.g_sUON2 = oUON3Seleccionada.CodUnidadOrgNivel2
'                            frmSOLPersonaSustituir.g_sUON3 = oUON3Seleccionada.Cod
'                            frmSOLPersonaSustituir.Show vbModal
'                            If g_sSustituto = "" And g_sSustUON1 = "" And g_sSustUON2 = "" And g_sSustUON3 = "" And g_sSustDep = "" Then 'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
'                                Exit Sub
'                            End If
'                        End If
                        
'                        oUON4Seleccionada.UON1SustPM = StrToNull(g_sSustUON1)
'                        oUON4Seleccionada.UON2SustPM = StrToNull(g_sSustUON2)
'                        oUON4Seleccionada.UON3SustPM = StrToNull(g_sSustUON3)
                                
                        Screen.MousePointer = vbHourglass
                        teserror = oIBaseDatos.EliminarDeBaseDatos
                        If teserror.NumError <> TESnoerror Then
                            Screen.MousePointer = vbNormal
                            TratarError teserror
                        Else
                           sCodigoEliminado = oUON4Seleccionada.Cod
                           EliminarUODeEstructura
                           RegistrarAccion AccionesSummit.ACCUON4Eli, "Cod:" & CStr(sCodigoEliminado)
                        End If
                    End If
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                End If
  
    Case "DEP0", "DEP1", "DEP2", "DEP3"
        
            Accion = ACCDepEli
            
            Set oIBaseDatos = oDepAsocSeleccionado
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiDept & " " & CStr(oDepAsocSeleccionado.Cod) & " (" & oDepAsocSeleccionado.Den & ")")
                If irespuesta = vbYes Then
                    'Aqu� comprueba si la UO a dar de baja se encuentra como aprobador en el PM
                    'Si es as� muestra el organigrama para seleccionar otra UO,dep o persona que lo sustituya en esos flujos
                    If oDepAsocSeleccionado.ExisteEnFlujoPM = True Then
                        Screen.MousePointer = vbNormal
                        frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_UON"
                        frmSOLPersonaSustituir.g_sDenominacion = m_sIdiDept & " " & CStr(oDepAsocSeleccionado.Cod) & " (" & oDepAsocSeleccionado.Den & ")"
                        frmSOLPersonaSustituir.g_sUON1 = oDepAsocSeleccionado.CodUON1
                        frmSOLPersonaSustituir.g_sUON2 = oDepAsocSeleccionado.CodUON2
                        frmSOLPersonaSustituir.g_sUON3 = oDepAsocSeleccionado.CodUON3
                        frmSOLPersonaSustituir.g_sDEP = oDepAsocSeleccionado.Cod
                        frmSOLPersonaSustituir.Show vbModal
                        If g_sSustituto = "" And g_sSustUON1 = "" And g_sSustUON2 = "" And g_sSustUON3 = "" And g_sSustDep = "" Then 'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
                            Exit Sub
                        End If
                    End If
                    
                    oDepAsocSeleccionado.UON1SustPM = StrToNull(g_sSustUON1)
                    oDepAsocSeleccionado.UON2SustPM = StrToNull(g_sSustUON2)
                    oDepAsocSeleccionado.UON3SustPM = StrToNull(g_sSustUON3)
                    oDepAsocSeleccionado.DEPSustPM = StrToNull(g_sSustDep)
                    oDepAsocSeleccionado.PerSustPM = StrToNull(g_sSustituto)
                            
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.FinalizarEdicionEliminando
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        TratarError teserror
                    Else
                        sCodigoEliminado = oDepAsocSeleccionado.Cod
                        EliminarDepDeEstructura
                        RegistrarAccion AccionesSummit.ACCDepEli, "Cod:" & CStr(sCodigoEliminado)
                    End If
                End If
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If
    
        Case "PER0", "PER1", "PER2", "PER3"
                            
            Accion = ACCPerEli
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiPers & " " & CStr(oPersonaSeleccionada.Cod) & " (" & oPersonaSeleccionada.Apellidos & " " & oPersonaSeleccionada.nombre & ")")
                If irespuesta = vbYes Then
                    'Aqu� comprueba si la persona a dar de baja se encuentra como aprobador o persona asignada en el PM o QA
                    'Si es as� muestra el organigrama para seleccionar otra persona que lo sustituya en esos flujos
                    If oPersonaSeleccionada.EsAprobadorPM = True Then
                        Screen.MousePointer = vbNormal
                        frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_PER"
                        frmSOLPersonaSustituir.g_sCodPersona = oPersonaSeleccionada.Cod
                        frmSOLPersonaSustituir.g_sDenominacion = NullToStr(oPersonaSeleccionada.nombre) & " " & oPersonaSeleccionada.Apellidos
                        frmSOLPersonaSustituir.Show vbModal
                        If g_sSustituto = "" Then  'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
                            Exit Sub
                        Else
                            oPersonaSeleccionada.SustitutoEnPM = g_sSustituto
                        End If
                    End If
                    
                    If oPersonaSeleccionada.EsAprobadorCatalogo = True Then
                        Screen.MousePointer = vbNormal
                        frmSOLPersonaSustituir.g_sOrigen = "frmESTRORG_PER_CAT"
                        frmSOLPersonaSustituir.g_sCodPersona = oPersonaSeleccionada.Cod
                        frmSOLPersonaSustituir.g_sDenominacion = NullToStr(oPersonaSeleccionada.nombre) & " " & oPersonaSeleccionada.Apellidos
                        frmSOLPersonaSustituir.Show vbModal
                        If g_sSustitutoEnCat = "" Then  'Si ha cancelado la selecci�n del sustituto en el workflow no hace nada
                            Exit Sub
                        Else
                            oPersonaSeleccionada.SustitutoEnCatSeguridad = g_sSustitutoEnCat
                        End If
                    End If
                    
                    sSustitutoEliminado = oPersonaSeleccionada.ListarSustituidosPor
                    If sSustitutoEliminado <> "" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoEliminarPersonaPorSerSustituto sSustitutoEliminado, False
                        Exit Sub
                    End If
                    
            
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        TratarError teserror
                    Else
                        EliminarPersonaDeEstructura
                        RegistrarAccion AccionesSummit.ACCPerEli, "Cod:" & CStr(oPersonaSeleccionada.Cod)
                    End If
                End If
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
            End If
                    
    End Select
    
    Screen.MousePointer = vbNormal
    
End If
            
End Sub

''' <summary>Cambio de c�digo</summary>
''' <remarks>Llamada desde form_load. Tiempo m�ximo < 0,01 seg.</remarks>
''' <revision>LTG 12/01/2012</revision>

Public Sub CambiarCodigo()
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim sCod As String
    Dim teserror As TipoErrorSummit
    Dim nodx As node
    Dim iResp As Integer
    
    Set nodx = tvwestrorg.selectedItem
    
    NodoSeleccionado nodx
    
    Screen.MousePointer = vbHourglass
    
    Select Case Left(nodx.Tag, 4)
    
    Case "UON1"
    
        Set oIBaseDatos = Nothing
        Set oIBaseDatos = oUON1Seleccionada
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then tvwestrorg.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = gParametrosGenerales.gsDEN_UON1 & " " & m_sIdiCodP
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodUON1
        frmMODCOD.txtCodAct.Text = oUON1Seleccionada.Cod
        Set frmMODCOD.fOrigen = frmESTRORG
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If g_sCodigoNuevo = "" Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(oUON1Seleccionada.Cod) Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        nodx.Text = Trim(g_sCodigoNuevo) & " - " & oUON1Seleccionada.Den
        scod1 = Trim(g_sCodigoNuevo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(Trim(g_sCodigoNuevo)))
        nodx.key = "UON1" & scod1
        nodx.Tag = Left(nodx.Tag, 4) & Trim(g_sCodigoNuevo)
                                        
    Case "UON2"
        
        Set oIBaseDatos = Nothing
        Set oIBaseDatos = oUON2Seleccionada
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then tvwestrorg.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = gParametrosGenerales.gsDEN_UON2 & " " & m_sIdiCodP
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodUON2
        frmMODCOD.txtCodAct.Text = oUON2Seleccionada.Cod
        Set frmMODCOD.fOrigen = frmESTRORG
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If g_sCodigoNuevo = "" Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(oUON2Seleccionada.Cod) Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
        nodx.Text = Trim(g_sCodigoNuevo) & " - " & oUON2Seleccionada.Den
        scod1 = oUON2Seleccionada.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2Seleccionada.CodUnidadOrgNivel1))
        scod2 = scod1 & Trim(g_sCodigoNuevo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(Trim(g_sCodigoNuevo)))
        nodx.key = "UON2" & scod2
        nodx.Tag = Left(nodx.Tag, 4) & Trim(g_sCodigoNuevo)
                                                                                        
    Case "UON3"
        
        Set oIBaseDatos = Nothing
        Set oIBaseDatos = oUON3Seleccionada
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then tvwestrorg.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = gParametrosGenerales.gsDEN_UON3 & " (C�digo)"
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodUON3
        frmMODCOD.txtCodAct.Text = oUON3Seleccionada.Cod
        Set frmMODCOD.fOrigen = frmESTRORG
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If g_sCodigoNuevo = "" Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(oUON3Seleccionada.Cod) Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
        nodx.Text = Trim(g_sCodigoNuevo) & " - " & oUON3Seleccionada.Den
        scod1 = oUON3Seleccionada.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3Seleccionada.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3Seleccionada.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3Seleccionada.CodUnidadOrgNivel2))
        scod3 = scod2 & Trim(g_sCodigoNuevo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(Trim(g_sCodigoNuevo)))
        nodx.key = "UON3" & scod3
        nodx.Tag = Left(nodx.Tag, 4) & Trim(g_sCodigoNuevo)
                                                        
    Case "DEP0", "DEP1", "DEP2", "DEP3"
        
        Set oIBaseDatos = Nothing
        Set oIBaseDatos = oDepAsocSeleccionado
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then tvwestrorg.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = m_sIdiDeptCod
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodDEP
        frmMODCOD.txtCodAct.Text = oDepAsocSeleccionado.Cod
        Set frmMODCOD.fOrigen = frmESTRORG
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If g_sCodigoNuevo = "" Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(oDepAsocSeleccionado.Cod) Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.NumError <> TESnoerror Then
            If teserror.NumError = TESTimeout Then
                Screen.MousePointer = vbNormal
                oMensajes.TimeoutCambioCodigoOnline
            Else
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
            End If
            
            Exit Sub
        End If

        ''' Actualizar datos
        cmdRestaurar_Click
        Select Case Left(nodx.Tag, 4)
        Case "DEP0"
            sCod = Trim(g_sCodigoNuevo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(Trim(g_sCodigoNuevo)))
        
        Case "DEP1"
            scod1 = oDepAsocSeleccionado.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsocSeleccionado.CodUON1))
            sCod = scod1 & Trim(g_sCodigoNuevo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(Trim(g_sCodigoNuevo)))
        
        Case "DEP2"
            scod1 = oDepAsocSeleccionado.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsocSeleccionado.CodUON1))
            scod2 = scod1 & oDepAsocSeleccionado.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsocSeleccionado.CodUON2))
            sCod = scod2 & Trim(g_sCodigoNuevo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(Trim(g_sCodigoNuevo)))
        
        Case "DEP3"
            scod1 = oDepAsocSeleccionado.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsocSeleccionado.CodUON1))
            scod2 = scod1 & oDepAsocSeleccionado.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsocSeleccionado.CodUON2))
            scod3 = scod2 & oDepAsocSeleccionado.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsocSeleccionado.CodUON3))
            sCod = scod3 & Trim(g_sCodigoNuevo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(Trim(g_sCodigoNuevo)))
        End Select
        Set nodx = tvwestrorg.Nodes.Item("DEPA" & sCod)
        Set tvwestrorg.selectedItem = nodx
        NodoSeleccionado nodx

'        nodx.Text = Trim(g_sCodigoNuevo) & " - " & oDepAsocSeleccionado.Den
'        nodx.key = "DEPA" & scod
'        nodx.Tag = Left(nodx.Tag, 4) & Trim(g_sCodigoNuevo)
        
    Case "PER0", "PER1", "PER2", "PER3"
    
        Set oIBaseDatos = Nothing
        Set oIBaseDatos = oPersonaSeleccionada
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then tvwestrorg.SetFocus
            Exit Sub
        End If
                
        frmMODCOD.caption = m_sIdiPersCod
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodPER
        frmMODCOD.txtCodAct.Text = oPersonaSeleccionada.Cod
        Set frmMODCOD.fOrigen = frmESTRORG
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If g_sCodigoNuevo = "" Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(oPersonaSeleccionada.Cod) Then
            oMensajes.NoValido m_sIdiCod
            Exit Sub
        End If
                
        iResp = oMensajes.PreguntarCambiarCodigoPersona()
        If iResp = vbNo Then Exit Sub

        'Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n).
        oPersonaSeleccionada.Usuario = basOptimizacion.gvarCodUsuario
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If
    End Select
    
    Screen.MousePointer = vbNormal
    
End Sub
Public Sub cmdDet_Click()
Dim nodx As node

Set nodx = tvwestrorg.selectedItem

NodoSeleccionado nodx

If Not nodx Is Nothing Then
        
Screen.MousePointer = vbHourglass

Select Case Left(nodx.Tag, 4)
        
    Case "UON1"
            
            Accion = ACCUON1Det
            Set oIBaseDatos = Nothing
            Set oIBaseDatos = oUON1Seleccionada
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                oUON1Seleccionada.CargarEmpresa
                frmESTRORGDetalle.caption = m_sIdiDet
                frmESTRORGDetalle.txtCod = oUON1Seleccionada.Cod
                frmESTRORGDetalle.txtDen = oUON1Seleccionada.Den
                
                Set frmESTRORGDetalle.m_oUnidadesOrgN1 = oUON1Seleccionada
                frmESTRORGDetalle.mCodUON1 = oUON1Seleccionada.Cod
                
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
                
            End If
    
    Case "UON2"
            
            Accion = ACCUON2DET
            Set oIBaseDatos = Nothing
            Set oIBaseDatos = oUON2Seleccionada
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            
                oUON2Seleccionada.CargarEmpresa
                frmESTRORGDetalle.caption = m_sIdiDet
                frmESTRORGDetalle.txtCod = oUON2Seleccionada.Cod
                frmESTRORGDetalle.txtDen = oUON2Seleccionada.Den
                
                Set frmESTRORGDetalle.m_oUnidadesOrgN2 = oUON2Seleccionada
                frmESTRORGDetalle.mCodUON2 = oUON2Seleccionada.Cod
                frmESTRORGDetalle.mCodUON1 = oUON2Seleccionada.CodUnidadOrgNivel1
                
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
                       
            End If
    
    Case "UON3"
            
            Accion = ACCUON3DET
            Set oIBaseDatos = Nothing
            Set oIBaseDatos = oUON3Seleccionada
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            
                oUON3Seleccionada.CargarEmpresa
                frmESTRORGDetalle.caption = m_sIdiDet
                frmESTRORGDetalle.txtCod = oUON3Seleccionada.Cod
                frmESTRORGDetalle.txtDen = oUON3Seleccionada.Den
                
                Set frmESTRORGDetalle.m_oUnidadesOrgN3 = oUON3Seleccionada
                frmESTRORGDetalle.mCodUON3 = oUON3Seleccionada.Cod
                frmESTRORGDetalle.mCodUON2 = oUON3Seleccionada.CodUnidadOrgNivel2
                frmESTRORGDetalle.mCodUON1 = oUON3Seleccionada.CodUnidadOrgNivel1
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
                       
            End If
    Case "UON4"
            
            Accion = ACCUON4DET
            Set oIBaseDatos = Nothing
            Set oIBaseDatos = oUON4Seleccionada
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
            
                oUON4Seleccionada.CargarEmpresa
                frmESTRORGDetalle.caption = m_sIdiDet
                frmESTRORGDetalle.txtCod = oUON4Seleccionada.Cod
                frmESTRORGDetalle.txtDen = oUON4Seleccionada.Den
                
                Set frmESTRORGDetalle.m_oUnidadesOrgN4 = oUON4Seleccionada
                frmESTRORGDetalle.mCodUON4 = oUON4Seleccionada.Cod
                frmESTRORGDetalle.mCodUON3 = oUON4Seleccionada.CodUnidadOrgNivel3
                frmESTRORGDetalle.mCodUON2 = oUON4Seleccionada.CodUnidadOrgNivel2
                frmESTRORGDetalle.mCodUON1 = oUON4Seleccionada.CodUnidadOrgNivel1
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
                       
            End If
    
    Case "DEP0", "DEP1", "DEP2", "DEP3"
            
           Accion = ACCDepDet
            
           Set oIBaseDatos = oDepAsocSeleccionado
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                
                frmESTRORGDetalle.caption = m_sIdiDet
                frmESTRORGDetalle.txtCod = oDepAsocSeleccionado.Cod
                frmESTRORGDetalle.txtDen = oDepAsocSeleccionado.Den
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
                
            End If
    
    Case "PER0", "PER1", "PER2", "PER3"
        
            Me.Accion = ACCPerDet
    
            Set oIBaseDatos = oPersonaSeleccionada
     
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                frmESTRORGPersona.caption = m_sIdiDet
                frmESTRORGPersona.txtCod = oPersonaSeleccionada.Cod
                frmESTRORGPersona.txtNom = NullToStr(oPersonaSeleccionada.nombre)
                frmESTRORGPersona.txtApel = oPersonaSeleccionada.Apellidos
                frmESTRORGPersona.txtCargo = NullToStr(oPersonaSeleccionada.Cargo)
                frmESTRORGPersona.txtFax = NullToStr(oPersonaSeleccionada.Fax)
                frmESTRORGPersona.txtMail = NullToStr(oPersonaSeleccionada.mail)
                frmESTRORGPersona.txtTfno = NullToStr(oPersonaSeleccionada.Tfno)
                frmESTRORGPersona.txtTfno2 = NullToStr(oPersonaSeleccionada.Tfno2)
                frmESTRORGPersona.sdbcEquipo.Text = NullToStr(oPersonaSeleccionada.codEqp)
                If frmESTRORGPersona.sdbcEquipo.Text <> "" Then
                    frmESTRORGPersona.chkFunCompra.Value = vbChecked
                End If
                Screen.MousePointer = vbNormal
                frmESTRORGPersona.Show 1
                
            End If
    
End Select

End If

End Sub
Public Sub mnuPopUpAnyadirDep()
    Accion = ACCDepAnya
    frmESTRORGAnyaDep.Show 1
End Sub
Public Sub mnuPopUpAnyadirNivel3()
    
    Accion = ACCUON3Anya
    
    frmESTRORGDetalle.caption = m_sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON3)
    frmESTRORGDetalle.mCodUON2 = DevolverCod(tvwestrorg.selectedItem)
    frmESTRORGDetalle.Show 1

       
End Sub
Public Sub mnuPopUpAnyadirNivel2()
    
    Accion = ACCUON2Anya

    frmESTRORGDetalle.caption = m_sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON2)
    frmESTRORGDetalle.mCodUON1 = DevolverCod(tvwestrorg.selectedItem)
    frmESTRORGDetalle.Show 1
    
End Sub
Public Sub mnuPopUpAnyadirNivel1()
    
    Accion = ACCUON1Anya
        
    frmESTRORGDetalle.caption = m_sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON1)
    frmESTRORGDetalle.Show 1
    
End Sub

Private Sub cmdlistado_Click()
Dim nodx As node
    ''' * Objetivo: Obtener un listado del organigrama
    frmLstESTRORG.WindowState = vbNormal
    frmLstESTRORG.opOrdODen.Value = sOrdenListadoDen
    frmLstESTRORG.opOrdPNom.Value = sOrdenListadoDen
    frmLstESTRORG.chkBajaLog.Value = chkBajaLog.Value
    frmLstESTRORG.chkBajaLogOrg.Value = chkBajaLog.Value
    
    Set nodx = tvwestrorg.selectedItem
    NodoSeleccionado nodx
    If nodx Is Nothing Then
       frmLstESTRORG.opTodos = True
    Else
        If Mid(nodx.Tag, 4, 1) = "0" Then
            frmLstESTRORG.opTodos = True
        Else
            Screen.MousePointer = vbHourglass
            Select Case Left(nodx.Tag, 4)
            Case "UON1"
                frmLstESTRORG.txtUO = oUON1Seleccionada.Cod
                frmLstESTRORG.sUON1 = oUON1Seleccionada.Cod
            Case "UON2"
                frmLstESTRORG.txtUO = oUON2Seleccionada.CodUnidadOrgNivel1 & " - " & oUON2Seleccionada.Cod
                frmLstESTRORG.sUON1 = oUON2Seleccionada.CodUnidadOrgNivel1
                frmLstESTRORG.sUON2 = oUON2Seleccionada.Cod
            Case "UON3"
                frmLstESTRORG.txtUO = oUON3Seleccionada.CodUnidadOrgNivel1 & " - " & oUON3Seleccionada.CodUnidadOrgNivel2 & " - " & oUON3Seleccionada.Cod
                frmLstESTRORG.sUON1 = oUON3Seleccionada.CodUnidadOrgNivel1
                frmLstESTRORG.sUON2 = oUON3Seleccionada.CodUnidadOrgNivel2
                frmLstESTRORG.sUON3 = oUON3Seleccionada.Cod
            Case "UON4"
                frmLstESTRORG.txtUO = oUON4Seleccionada.CodUnidadOrgNivel1 & " - " & oUON4Seleccionada.CodUnidadOrgNivel2 & " - " & oUON4Seleccionada.CodUnidadOrgNivel3 & " - " & oUON4Seleccionada.Cod
                frmLstESTRORG.sUON1 = oUON4Seleccionada.CodUnidadOrgNivel1
                frmLstESTRORG.sUON2 = oUON4Seleccionada.CodUnidadOrgNivel2
                frmLstESTRORG.sUON3 = oUON4Seleccionada.CodUnidadOrgNivel3
                frmLstESTRORG.sUON4 = oUON4Seleccionada.Cod
                
            End Select
            Screen.MousePointer = vbNormal
        End If
    End If
   
    frmLstESTRORG.Show 1
   
End Sub

Public Sub cmdModif_Click()

Dim nodx As node
Dim teserror As TipoErrorSummit

Screen.MousePointer = vbHourglass
teserror.NumError = TESnoerror

Set nodx = tvwestrorg.selectedItem

NodoSeleccionado nodx

If nodx Is Nothing Then
    Screen.MousePointer = vbNormal
    Exit Sub
End If

Select Case Left(nodx.Tag, 4)
        
    Case "UON1"
            
            Accion = ACCUON1Mod
            
            Set oIBaseDatos = oUON1Seleccionada
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                oUON1Seleccionada.CargarEmpresa
                
                frmESTRORGDetalle.caption = m_sIdiModif & " " & LCase(gParametrosGenerales.gsDEN_UON1)
                frmESTRORGDetalle.txtCod = oUON1Seleccionada.Cod
                frmESTRORGDetalle.txtDen = oUON1Seleccionada.Den
                             
                Set frmESTRORGDetalle.m_oUnidadesOrgN1 = oUON1Seleccionada
                frmESTRORGDetalle.mCodUON1 = oUON1Seleccionada.Cod
                
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
            End If
    
    
    Case "UON2"
            
            Accion = ACCUON2MOD
            
            Set oIBaseDatos = oUON2Seleccionada
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                oUON2Seleccionada.CargarEmpresa
                frmESTRORGDetalle.caption = m_sIdiModif & " " & LCase(gParametrosGenerales.gsDEN_UON2)
                frmESTRORGDetalle.txtCod = oUON2Seleccionada.Cod
                frmESTRORGDetalle.txtDen = oUON2Seleccionada.Den
                frmESTRORGDetalle.mCodUON1 = oUON2Seleccionada.CodUnidadOrgNivel1
                
                Set frmESTRORGDetalle.m_oUnidadesOrgN2 = oUON2Seleccionada
                frmESTRORGDetalle.mCodUON2 = oUON2Seleccionada.Cod
                
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
            
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
            End If
    
    Case "UON3"
            
            Accion = ACCUON3MOD
            
            Set oIBaseDatos = oUON3Seleccionada
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                oUON3Seleccionada.CargarEmpresa
                
                frmESTRORGDetalle.caption = m_sIdiModif & " " & LCase(gParametrosGenerales.gsDEN_UON3)
                frmESTRORGDetalle.txtCod = oUON3Seleccionada.Cod
                frmESTRORGDetalle.txtDen = oUON3Seleccionada.Den
                frmESTRORGDetalle.mCodUON1 = oUON3Seleccionada.CodUnidadOrgNivel1
                frmESTRORGDetalle.mCodUON2 = oUON3Seleccionada.CodUnidadOrgNivel2
                
                Set frmESTRORGDetalle.m_oUnidadesOrgN3 = oUON3Seleccionada
                frmESTRORGDetalle.mCodUON3 = oUON3Seleccionada.Cod
                
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
            End If
    
    Case "UON4"
            Accion = ACCUON4MOD
            
            Set oIBaseDatos = oUON4Seleccionada
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                oUON4Seleccionada.CargarEmpresa
                frmESTRORGDetalle.caption = m_sIdiModif & " " & gParametrosGenerales.gsDEN_UON4
                frmESTRORGDetalle.txtCod.Text = oUON4Seleccionada.Cod
                frmESTRORGDetalle.txtDen.Text = oUON4Seleccionada.Den
                frmESTRORGDetalle.mCodUON1 = oUON4Seleccionada.CodUnidadOrgNivel1
                frmESTRORGDetalle.mCodUON2 = oUON4Seleccionada.CodUnidadOrgNivel2
                frmESTRORGDetalle.mCodUON3 = oUON4Seleccionada.CodUnidadOrgNivel3
                                
                Set frmESTRORGDetalle.m_oUnidadesOrgN4 = oUON4Seleccionada
                frmESTRORGDetalle.mCodUON4 = oUON4Seleccionada.Cod
                
                Screen.MousePointer = vbNormal
                frmESTRORGDetalle.Show 1
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
            End If
    
    Case "DEP0", "DEP1", "DEP2", "DEP3"
            
            ' Comprobamos que no haya ningun departamento fuera de su uorg.
            ' si hubiera alguno no le dejamos modificarlo
            Accion = ACCDepMod
            
            If bRUO Then
                 If Not oDepAsocSeleccionado.EsModificablePorLaPersona(oUsuarioSummit.Persona) Then
                    Screen.MousePointer = vbNormal
                    oMensajes.PermisoDenegadoModDep
                    Exit Sub
                End If
            End If
     
            
        Set oIBaseDatos = oDepAsocSeleccionado
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError = TESnoerror Then
            frmESTRORGDetalle.caption = m_sIdiModifDept
            Screen.MousePointer = vbNormal
            frmESTRORGDetalle.Show 1
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            Set oIBaseDatos = Nothing
        End If
            
     Case "PER0", "PER1", "PER2", "PER3"
            
            Accion = ACCPerMod
            Set oIBaseDatos = oPersonaSeleccionada
            
            teserror = oIBaseDatos.IniciarEdicion
            
            
            If teserror.NumError = TESnoerror Then
                Screen.MousePointer = vbNormal
                frmESTRORGPersona.Show 1
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
            End If
            
End Select

Screen.MousePointer = vbNormal

End Sub



Private Sub cmdRestaurar_Click()
    sstabEstrorg.TabVisible(1) = False
    tvwestrorg.Nodes.clear
    GenerarEstructuraOrg Not MDI.mnuPopUpEstrOrgNiv0(4).Checked
    
    tvwestrorg.Nodes.Item("UON0").Selected = True
    ConfigurarInterfazSeguridad tvwestrorg.Nodes.Item("UON0")

    sstabEstrorg.TabVisible(1) = False

    
End Sub

Public Sub cmdReubicar_Click()


Dim nodx As node
Dim teserror As TipoErrorSummit

teserror.NumError = TESnoerror

Set nodx = tvwestrorg.selectedItem

NodoSeleccionado nodx

frmSELDEP.m_bModif = True
frmSELDEP.sOrigen = "frmESTRORG"
frmSELDEP.Show vbModal

If m_Reubicar Then

    teserror = oPersonaSeleccionada.Reubicar(oPersonaSeleccionada.Cod, frmSELDEP.sCodDep, frmSELDEP.sUON1, frmSELDEP.sUON2, frmSELDEP.sUON3)
            
    If Not teserror.NumError = TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
    Else
    'Comprobamos si tiene el check de realizar el cambio en los procesos
        If frmSELDEP.chkTraslados Then
            teserror = oPersonaSeleccionada.TrasladarProcesosPersona(oPersonaSeleccionada.Cod, frmSELDEP.sCodDep, frmSELDEP.sUON1, frmSELDEP.sUON2, frmSELDEP.sUON3)
            If Not teserror.NumError = TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
            End If
        End If
        
        ' Si el usuario tiene la restricci�n de UO para presupuestos se eliminan los presupuestos favoritos
        Dim oUsuario As CUsuario
        oPersonaSeleccionada.CargarTodosLosDatos (True)
        If Not IsEmpty(oPersonaSeleccionada.Usuario) Then
            Set oUsuario = oFSGSRaiz.generar_cusuario
            oUsuario.Cod = oPersonaSeleccionada.Usuario
            oUsuario.ExpandirUsuario
            If oUsuario.FSWSPresUO Then oUsuario.EliminarPresupuestosFavoritosRestrUO
        End If
    End If

    m_Reubicar = False
    
End If

Unload frmSELDEP

cmdRestaurar_Click

End Sub

Private Sub Form_Load()
Dim oIdi As CIdioma
Dim c As Integer

Me.Height = 5400
Me.Width = 9825
    
If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If

Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, True, False)

Dim contador As Integer 'Definici�n variable
contador = 0            ' Inicializaci�n variable

For Each oIdi In m_oIdiomas
    If (den_general <> "") Then
        den_general = den_general & ","  'el �ltimo idioma introducido no lleve coma
    End If
    den_general = den_general & "DEN_" & oIdi.Cod ' va a�adiendo DEN_SPA, DE_ENG...con todos los idiomas que haya
    contador = contador + 1   ' contador sale del bucle con el n�mero total de idiomas que hay
Next


'Inicializaci�n PosIniIdiomas, PosFinIdiomas
PosIniIdiomas = 3
PosFinIdiomas = PosIniIdiomas + (contador - 1)


' Cargar los captions dependiendo del idioma
CargarRecursos

    PonerFieldSeparator Me

' Configurar la seguridad
ConfigurarSeguridad


'A�adir columnas din�micamente
'A�ado las columnas del n�mero de idiomas que sean
c = PosIniIdiomas

'Reubico las columnas para que se vayan a�adiendo detr�s del �ltimo idioma

sdbgDestinos.Columns.Add sdbgDestinos.Columns.Count
sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Name = "DEN_" & gParametrosInstalacion.gIdioma
sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).caption = m_oIdiomas.Item(gParametrosInstalacion.gIdioma).Den
sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Style = ssStyleEdit
sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Position = c
If m_bModifDestinos = False Then
    sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Locked = True
End If
For Each oIdi In m_oIdiomas
    If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
    c = c + 1
    sdbgDestinos.Columns.Add sdbgDestinos.Columns.Count
    sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Name = "DEN_" & oIdi.Cod
    sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).caption = oIdi.Den
    sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Style = ssStyleEdit
    sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).ButtonsAlways = False
    sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Position = c
    sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Visible = True
    If m_bModifDestinos = False Then
        sdbgDestinos.Columns(sdbgDestinos.Columns.Count - 1).Locked = True
    End If
    End If
Next


' Generar la estructura de la organizacion en el treeview
GenerarEstructuraOrg (False)

tvwestrorg.Nodes.Item("UON0").Selected = True
ConfigurarInterfazSeguridad tvwestrorg.Nodes.Item("UON0")

sstabEstrorg.TabVisible(1) = False

Set oDestinos = Nothing
Set oDestinos = oFSGSRaiz.Generar_CDestinos
  
Show
DoEvents

sdbddPaises.AddItem "" & Chr(m_lSeparador) & ""
sdbddProvincias.AddItem "" & Chr(m_lSeparador) & ""
Set m_oPaises = oFSGSRaiz.Generar_CPaises

m_oPaises.CargarTodosLosPaises , , , , , False

sdbgDestinos.AddItem "" & Chr(m_lSeparador) & ""
If m_bModifDestinos Then
   sdbgDestinos.Columns("PAI").DropDownHwnd = sdbddPaises.hWnd
   sdbgDestinos.Columns("PROVI").DropDownHwnd = sdbddProvincias.hWnd
End If
sdbgDestinos.Columns("GEN").Locked = Not m_bPermitGenerico

sdbgDestinos.SplitterVisible = True
sdbgDestinos.SplitterPos = 2

m_bModoEdicion = False
Accion = ACCDestCon
  
sOrdenListado = "COD"

End Sub

Private Sub Form_Resize()
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    If Width >= 280 Then sstabEstrorg.Width = Me.Width - 280
    If Height >= 600 Then sstabEstrorg.Height = Me.Height - 555
    
    If Height >= 1800 Then
        tvwestrorg.Height = Me.Height - 1795
        sdbgDestinos.Height = Me.Height - 1800
    End If
    
    If Me.Width >= 525 Then
        tvwestrorg.Width = sstabEstrorg.Width - 255
        sdbgDestinos.Width = Me.Width - 590
    End If
    
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest) Then
        sdbgDestinos.Columns("INT").Visible = True
    Else
        sdbgDestinos.Columns("INT").Visible = False
    End If
            
    lblUO.Top = sdbgDestinos.Top - 350
    picNavigate.Top = tvwestrorg.Top + tvwestrorg.Height + 65
    picNavigate.Left = tvwestrorg.Left
    picNavigate.Width = tvwestrorg.Width
    Picture1.Top = sdbgDestinos.Top + sdbgDestinos.Height + 75
    Picture1.Width = sdbgDestinos.Width
    lblUO.Width = sdbgDestinos.Width
    
    cmdModoEdicion.Left = Picture1.Width - cmdModoEdicion.Width


End Sub

Private Sub Form_Unload(Cancel As Integer)
    Unload frmESTRORGAnyaDep
    Unload frmESTRORGBuscarPersona
    Unload frmESTRORGBuscarUO
    Unload frmESTRORGBuscarDep
    Unload frmESTRORGDetalle
    Unload frmESTRORGPersona
    Set oUON1Seleccionada = Nothing
    Set oUON2Seleccionada = Nothing
    Set oUON3Seleccionada = Nothing
    Set oUON4Seleccionada = Nothing
    Set m_oDestinoEnEdicion = Nothing
    Set m_oPaises = Nothing
    Set m_oProvincias = Nothing
    Set oIBaseDatos = Nothing
    Set oPersonaSeleccionada = Nothing

    g_sSustituto = ""
    g_sSustitutoEnCat = ""
    g_sSustUON1 = ""
    g_sSustUON2 = ""
    g_sSustUON3 = ""
    g_sSustDep = ""

    Me.Visible = False
End Sub

Public Sub sdbgDestinos_BtnClick()

frmESTRORGDestAlm.g_sDestino = sdbgDestinos.Columns("COD").Value
frmESTRORGDestAlm.g_bModoEdicion = m_bModoEdicion
frmESTRORGDestAlm.g_bModif = m_bModifDestinos
frmESTRORGDestAlm.Show vbModal

End Sub


Private Sub sdbgDestinos_RowLoaded(ByVal Bookmark As Variant)

Dim IndexIdi As Integer 'Indice para recorrer
Dim vlb As Boolean
If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest) Then
    If sdbgDestinos.Columns("INT").Value = "" Or sdbgDestinos.Columns("INT").Value = CStr(EstadoIntegracion.recibidocorrecto) Then
        sdbgDestinos.Columns("INT").Value = "  " & m_sIdiSincronizado
        sdbgDestinos.Columns("INT").CellStyleSet "IntOK", Bookmark
    Else
        sdbgDestinos.Columns("INT").Value = "   " & m_sIdiNoSincronizado
        sdbgDestinos.Columns("INT").CellStyleSet "IntKO", Bookmark
    End If
End If

vlb = False
Select Case m_sUON
    Case "UON0"
        If sdbgDestinos.Columns("UON1").Text <> "" Then
            vlb = True
        End If
    Case "UON1"
        If sdbgDestinos.Columns("UON2").Text <> "" Then
            vlb = True
        End If
        
    Case "UON2"
        If sdbgDestinos.Columns("UON3").Text <> "" Then
            vlb = True
        End If
        
    Case "UON3"
        If sdbgDestinos.Columns("UON4").Text <> "" Then
            vlb = True
        End If
End Select

If vlb Then
    For IndexIdi = 1 To (PosFinIdiomas + 5) '(PosFinIdiomas + 5) es la posici�n de PROVI
        If sdbgDestinos.Columns(IndexIdi).Name <> "GEN" Then
            sdbgDestinos.Columns(IndexIdi).CellStyleSet ("UONInf")
        End If
    Next
End If
If Not m_bPermitGenerico Then
    sdbgDestinos.Columns("GEN").CellStyleSet "styAzul"
End If
End Sub

Private Sub sstabEstrorg_Click(PreviousTab As Integer)
Dim nodx As node
Dim Dest As CDestino
Dim sDest As String
Dim oIdi As CIdioma

Form_Resize

If m_bDesactivarTabClick = True Then Exit Sub

If PreviousTab = 1 Then

    If m_bModoEdicion Then
        m_bDesactivarTabClick = True
        sstabEstrorg.Tab = 1
        m_bDesactivarTabClick = False
        Exit Sub
    End If
        
    cmdModoEdicion.Enabled = False
    cmdEli.Enabled = bEstcmdEli
    cmdRestaurarDest.Enabled = False
    cmdRestaurar.Enabled = True
    cmdA�adir.Enabled = bEstcmdA�adir
    cmdListado.Enabled = True
    cmdListadoDest.Enabled = False
    
    frmESTRORG.caption = m_asFrmTitulo(6)
    
Else

    sdbgDestinos.RemoveAll
    Set nodx = tvwestrorg.selectedItem
    lblUO.caption = tvwestrorg.selectedItem
    cmdModoEdicion.Enabled = True
    bEstcmdEli = cmdEli.Enabled
    cmdEli.Enabled = False
    cmdRestaurarDest.Enabled = True
    cmdRestaurar.Enabled = False
    bEstcmdA�adir = cmdA�adir.Enabled
    cmdA�adir.Enabled = False
    cmdListado.Enabled = False
    cmdListadoDest.Enabled = True

    g_vUON1 = Null
    g_vUON2 = Null
    g_vUON3 = Null
    g_vUON4 = Null
    
    NodoSeleccionado nodx
    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
    
    'vble para controllar los de nivel inf
    m_sUON = Left(nodx.Tag, 4)
    
    
' Reordenaci�n para que se muestre ordenado seg�n vista GS
    For Each Dest In oDestinos
        sDest = Dest.EstadoIntegracion & Chr(m_lSeparador) & Dest.Cod & Chr(m_lSeparador) & Dest.Generico
        sDest = sDest & Chr(m_lSeparador) & Dest.dir & Chr(m_lSeparador) & Dest.Pob & Chr(m_lSeparador) & Dest.cP & Chr(m_lSeparador) & _
        Dest.Pais & Chr(m_lSeparador) & Dest.Provi & Chr(m_lSeparador) & _
        Dest.Tfno & Chr(m_lSeparador) & Dest.Fax & Chr(m_lSeparador) & Dest.Email & Chr(m_lSeparador) & _
        " " & Chr(m_lSeparador) & Dest.UONString & Chr(m_lSeparador) & _
        Dest.UON1 & Chr(m_lSeparador) & Dest.UON2 & Chr(m_lSeparador) & Dest.UON3 & Chr(m_lSeparador) & Dest.UON4
        sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        For Each oIdi In m_oIdiomas
            If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(oIdi.Cod).Den
            End If
        Next
        sdbgDestinos.AddItem sDest
    Next

    
     
    
    frmESTRORG.caption = m_asFrmTitulo(1)
    
End If

    sdbgDestinos.AllowAddNew = False
    sdbgDestinos.AllowUpdate = False
    sdbgDestinos.AllowDelete = False
    sdbgDestinos.ReBind
  

End Sub

Private Sub tvwestrorg_Collapse(ByVal node As MSComctlLib.node)
ConfigurarInterfazSeguridad node
End Sub

Private Sub tvwEstrOrg_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nodx As node

If Button = 2 Then

    Set nodx = tvwestrorg.selectedItem
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 4)
            
            Case "UON0":
                            MDI.mnuPopUpEstrOrgNiv0(1).caption = m_sIdiA�adir & " " & gParametrosGenerales.gsDEN_UON1
                            PopupMenu MDI.mnuPopUpEstrOrgNivel0
                            
            Case "UON1":
                            MDI.mnuPopUpEstrOrgNiv1(1).caption = m_sIdiA�adir & " " & gParametrosGenerales.gsDEN_UON2
                            'Si se modifica la integraci�n y se entra de nuevo a la estructura organizativa, podr�amos
                            'recibir el nombre "Gestionar Centro de Coste" en vez de "Consultar". Lo modificamos aqu�
                            If gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.CentroCoste) Then
                                MDI.mnuPopUpEstrOrgNiv1(3).caption = m_sIdiConsultarCC 'Consultar Centro de Coste
                            End If
                            PopupMenu MDI.mnuPopUpEstrOrgNivel1
            Case "UON2":
                            MDI.mnuPopUpEstrOrgNiv2(1).caption = m_sIdiA�adir & " " & gParametrosGenerales.gsDEN_UON3
                            'Si se modifica la integraci�n y se entra de nuevo a la estructura organizativa, podr�amos
                            'recibir el nombre "Gestionar Centro de Coste" en vez de "Consultar". Lo modificamos aqu�
                            If gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.CentroCoste) Then
                                MDI.mnuPopUpEstrOrgNiv2(3).caption = m_sIdiConsultarCC 'Consultar Centro de Coste
                            End If
                            PopupMenu MDI.mnuPopUpEstrOrgNivel2
            Case "UON3":
                            If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
                                ConfigurarInterfazSeguridad nodx
                            End If
                            'Si se modifica la integraci�n y se entra de nuevo a la estructura organizativa, podr�amos
                            'recibir el nombre "Gestionar Centro de Coste" en vez de "Consultar". Lo modificamos aqu�
                            If gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.CentroCoste) Then
                                MDI.mnuPopUpEstrOrgNiv3(3).caption = m_sIdiConsultarCC 'Consultar Centro de Coste
                            End If
                            PopupMenu MDI.mnuPopUpEstrOrgNivel3
            Case "UON4":
                                                       
                            'Si se modifica la integraci�n y se entra de nuevo a la estructura organizativa, podr�amos
                            'recibir el nombre "Gestionar Centro de Coste" en vez de "Consultar". Lo modificamos aqu�
                            If gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.CentroCoste) Then
                                MDI.mnuPopUpEstrOrgNiv4(1).caption = m_sIdiConsultarCC 'Consultar Centro de Coste
                            End If
                            PopupMenu MDI.mnuPopUpEstrOrgNivel4

                            
            Case "DEP0", "DEP1", "DEP2", "DEP3":
            
                            PopupMenu MDI.mnuPopUpEstrOrgDep
            
            Case "PER0", "PER1", "PER2", "PER3":
            
                            PopupMenu MDI.mnuPopUpEstrOrgPer
        
        End Select
        
    End If

End If
   
End Sub

Public Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)
Dim i As Integer
Set g_SelNode = node

    ConfigurarInterfazSeguridad node
    
    Select Case Left(node.Tag, 4)

        Case "DEP0", "DEP1", "DEP2", "DEP3"
            sstabEstrorg.TabVisible(1) = False
        
        Case "PER0", "PER1", "PER2", "PER3"
            sstabEstrorg.TabVisible(1) = False
        
        Case "UON0", "UON1", "UON2", "UON3", "UON4"
                If SeguridadDestinos(node) = True Then
                    sstabEstrorg.TabVisible(1) = True
                Else
                    sstabEstrorg.TabVisible(1) = False
                End If
    End Select
        
                
        If m_bModifDestinos Then
            cmdModoEdicion.Visible = True
        Else
            cmdModoEdicion.Visible = False
        End If
        
        If m_bAsigDestinos Then
            sdbddDestinos.AddItem ""
            sdbgDestinos.Columns("COD").DropDownHwnd = sdbddDestinos.hWnd
            cmdModoEdicion.Visible = True
            If Not m_bModifDestinos Then
                For i = 0 To 12
                    sdbgDestinos.Columns(i).Locked = True
                Next
            End If
        Else
            sdbgDestinos.Columns("COD").DropDownHwnd = 0
        End If
    
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

Select Case Left(node.Tag, 4)

Case "DEP0", "DEP1", "DEP2", "DEP3"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "UON1", "UON2", "UON3", "UON4"
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "PER0", "PER1", "PER2", "PER3"
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
End Select

End Function

Public Function DevolverDen(ByVal sCodYDen) As Variant

Dim str As String

str = Mid(sCodYDen, InStr(sCodYDen, "-") + 2, Len(sCodYDen))
DevolverDen = str

End Function

Private Sub EliminarUODeEstructura()

Dim nod As node
Dim nodSiguiente As MSComctlLib.node

Set nod = tvwestrorg.selectedItem

    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If

   tvwestrorg.Nodes.Remove nod.key
   nodSiguiente.Selected = True
   ConfigurarInterfazSeguridad nodSiguiente
   Set nod = Nothing

End Sub
Private Sub EliminarDepDeEstructura()

Dim nod As MSComctlLib.node
Dim nodSiguiente As MSComctlLib.node

Set nod = tvwestrorg.selectedItem

    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If

    tvwestrorg.Nodes.Remove nod.key
        
    Set nod = Nothing

    nodSiguiente.Selected = True
    ConfigurarInterfazSeguridad nodSiguiente


End Sub

Private Sub EliminarPersonaDeEstructura()
Dim nodSiguiente As MSComctlLib.node
Dim nod As MSComctlLib.node

    Set nod = tvwestrorg.selectedItem

    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If
    
tvwestrorg.Nodes.Remove nod.Index
nodSiguiente.Selected = True
ConfigurarInterfazSeguridad nodSiguiente
End Sub

''' <summary>
''' Ordenar el arbol por denominacion � codigo
''' </summary>
''' <param name="bOrdPorDen">Ordebado por denominacion (true)/codigo (false)</param>
''' <remarks>Llamada desde: mdi/mnuPopUpEstrOrgNiv0_Click ; Tiempo m�ximo: 0,2</remarks>
Public Sub Ordenar(ByVal bOrdPorDen As Boolean)
    tvwestrorg.Nodes.clear
    sstabEstrorg.TabVisible(1) = False
    GenerarEstructuraOrg bOrdPorDen
End Sub

''' <summary>
''' Procedimiento que carga los textos que aparecer�n en el formulario en el idioma que corresponda
''' </summary>
''' <remarks>Llamada desde form_load. Tiempo m�ximo < 0,01 seg.</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim k As Integer
Dim oIdi As CIdioma
Dim IndexIdi As Integer 'Indice para recorrer las columnas de los idiomas

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRORG, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sstabEstrorg.TabCaption(0) = Ador(0).Value
        caption = Ador(0).Value
        m_asFrmTitulo(6) = Ador(0).Value
        Ador.MoveNext
        
        cmdA�adir.caption = Ador(0).Value
        cmdA�adirDest.caption = Ador(0).Value
        Ador.MoveNext
        cmdBuscar.caption = Ador(0).Value
        Ador.MoveNext
        cmdEli.caption = Ador(0).Value
        cmdEliminarDest.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        cmdListadoDest.caption = Ador(0).Value
        Ador.MoveNext
        cmdModif.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        cmdRestaurarDest.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiUO = Ador(0).Value
        Ador.MoveNext
        m_sIdiDept = Ador(0).Value
        Ador.MoveNext
        m_sIdiPers = Ador(0).Value
        Ador.MoveNext
        m_sIdiCod = Ador(0).Value
        sdbgDestinos.Columns(1).caption = Ador(0).Value
        cmdCodigo.caption = "&" & Ador(0).Value
        Ador.MoveNext
        m_sIdiA�adir = Ador(0).Value
        Ador.MoveNext
        m_sIdiDeptCod = Ador(0).Value
        Ador.MoveNext
        m_sIdiPersCod = Ador(0).Value
        Ador.MoveNext
        m_sIdiDet = Ador(0).Value
        Ador.MoveNext
        m_sIdiModif = Ador(0).Value
        Ador.MoveNext
        m_sIdiModifDept = Ador(0).Value
        Ador.MoveNext
        m_sIdiCodP = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        cmdFiltrar.caption = Ador(0).Value
        m_scodDest = cmdCodigo.caption & ":"
        Ador.MoveNext
        m_sEdicion = Ador(0).Value
        cmdModoEdicion.caption = m_sEdicion
        Ador.MoveNext
        m_asFrmTitulo(1) = Ador(0).Value
        Ador.MoveNext
        
        
        ' Columnas de los idiomas
        For IndexIdi = PosIniIdiomas To PosFinIdiomas
            sdbgDestinos.Columns(IndexIdi).caption = Ador(0).Value
        Next
        IndexIdi = IndexIdi + 1 'IndexIdi ya tiene el valor de la siguiente columna a poder rellenar
            
        sdbddProvincias.Columns(1).caption = Ador(0).Value
        m_asMensajes(1) = Ador(0).Value
        sdbddPaises.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgDestinos.Columns("DIR").caption = Ador(0).Value
        m_asMensajes(2) = Ador(0).Value
        Ador.MoveNext
        sdbgDestinos.Columns("POB").caption = Ador(0).Value
        Ador.MoveNext
        sdbgDestinos.Columns("CP").caption = Ador(0).Value  'Caption form modo consulta
        Ador.MoveNext
        sdbgDestinos.Columns("PAI").caption = Ador(0).Value
        Ador.MoveNext
        sdbgDestinos.Columns("PROVI").caption = Ador(0).Value
        Ador.MoveNext
        sdbddProvincias.Columns(0).caption = Ador(0).Value
        sdbddPaises.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        m_asFrmTitulo(2) = Ador(0).Value
        Ador.MoveNext
        m_sConsulta = Ador(0).Value
        Ador.MoveNext
        m_sOrdenando = Ador(0).Value
        Ador.MoveNext
        m_asFrmTitulo(4) = Ador(0).Value
        Ador.MoveNext
        m_asMensajes(3) = Ador(0).Value
        Ador.MoveNext
        m_asFrmTitulo(5) = Ador(0).Value
        Ador.MoveNext
        m_asMensajes(4) = Ador(0).Value
        Ador.MoveNext
        sdbgDestinos.Columns("U.O").caption = Ador(0).Value  'Caption form modo edicion
        Ador.MoveNext
        m_asFrmTitulo(3) = Ador(0).Value
        Ador.MoveNext
        caption = Ador(0).Value
        m_asFrmTitulo(6) = Ador(0).Value
        Ador.MoveNext
        sstabEstrorg.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        m_sIdiSincronizado = Ador(0).Value
        Ador.MoveNext
        m_sIdiNoSincronizado = Ador(0).Value
        
        Ador.MoveNext
        cmdBajaLog.caption = Ador(0).Value  'Baja l�gica
        m_sBajaLog = Ador(0).Value
        Ador.MoveNext
        chkBajaLog.caption = Ador(0).Value  'Ver bajas l�gicas
        Ador.MoveNext
        m_sDesBajaLog = Ador(0).Value  'Deshacer bajas l�gicas
        
        Ador.MoveNext
        cmdReubicar.caption = Ador(0).Value 'Reubicar
        
        Ador.MoveNext
        sdbgDestinos.Columns("ALMAC").caption = Ador(0).Value

        Ador.MoveNext
        m_sNoHayEmpresaAsociada = Ador(0).Value ' Para gestionar el centro de coste �ste debe tener una empresa asociada. 'BLP
        
        Ador.MoveNext
        m_sIdiConsultarCC = Ador(0).Value 'Consultar Centro de Coste
        
        Ador.MoveNext
        sdbgDestinos.Columns("TFNO").caption = Ador(0).Value  'Tfno. 51
        
        Ador.MoveNext
        sdbgDestinos.Columns("FAX").caption = Ador(0).Value 'FAX 52
        
        Ador.MoveNext
        sdbgDestinos.Columns("EMAIL").caption = Ador(0).Value 'E-Mail 53
        Ador.MoveNext
        sdbgDestinos.Columns("GEN").caption = Ador(0).Value
        Ador.MoveNext
        m_sValidarDestGenerico = Ador(0).Value
        Ador.Close

    
    End If
    
    
    'Se renombran las columnas de la denominacion en el combo de destinos.
    sdbddDestinos.Columns(1).Name = "DEN_" & gParametrosInstalacion.gIdioma
    k = 7
    For Each oIdi In m_oIdiomas
        If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
            sdbddDestinos.Columns(k).Name = "DEN_" & oIdi.Cod
            k = k + 1
        End If
    Next
    
    Set Ador = Nothing

End Sub
Private Sub cmdA�adirDest_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgDestinos.Scroll 0, sdbgDestinos.Rows - sdbgDestinos.Row
    
    If sdbgDestinos.VisibleRows > 0 Then
        
        If sdbgDestinos.VisibleRows > sdbgDestinos.Rows Then
            sdbgDestinos.Row = sdbgDestinos.Rows
        Else
            sdbgDestinos.Row = sdbgDestinos.Rows - (sdbgDestinos.Rows - sdbgDestinos.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgDestinos.SetFocus
    
End Sub

Private Sub cmdFiltrar_Click()
    Dim arFiltros As Variant
    
    arFiltros = MostrarFormDESTFiltrar(oGestorIdiomas, gParametrosInstalacion.gIdioma, gLongitudesDeCodigos.giLongCodDEST, oGestorParametros.DevolverIdiomas(False, False, True))
    
    If Not IsEmpty(arFiltros) Then
        Dim Dest As CDestino
        Dim sDest As String
        Dim oIdi As CIdioma
        
        Screen.MousePointer = vbHourglass
    
        Set oDestinos = oFSGSRaiz.Generar_CDestinos
    
        ponerCaption "", 1, False
    
        If arFiltros(0) And arFiltros(1) <> "" Then
            If Not arFiltros(2) Then
                oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(arFiltros(1)), , False, , , , , , True, gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                ponerCaption arFiltros(1), 3, True
            Else
                oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(arFiltros(1)), , True, , , , , , True, gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                ponerCaption arFiltros(1), 3, False
            End If
        Else
            If arFiltros(3) And arFiltros(4) <> "" Then
                If Not arFiltros(5) Then
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(arFiltros(4)), False, , , , , , True, gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                    ponerCaption arFiltros(4), 4, True
                Else
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(arFiltros(4)), True, , , , , , True, gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                    ponerCaption arFiltros(4), 4, False
                End If
            Else
                oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , , , , True, gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
            End If
        End If
    
        sdbgDestinos.RemoveAll
    
        ' Reordenaci�n para que se muestre ordenado seg�n vista GS
        For Each Dest In oDestinos
            sDest = Dest.EstadoIntegracion & Chr(m_lSeparador) & Dest.Cod & Chr(m_lSeparador) & Dest.Generico
            sDest = sDest & Chr(m_lSeparador) & Dest.dir & Chr(m_lSeparador) & Dest.Pob & Chr(m_lSeparador) & Dest.cP & Chr(m_lSeparador) & _
            Dest.Pais & Chr(m_lSeparador) & Dest.Provi & Chr(m_lSeparador) & _
            Dest.Tfno & Chr(m_lSeparador) & Dest.Fax & Chr(m_lSeparador) & Dest.Email & Chr(m_lSeparador) & _
            " " & Chr(m_lSeparador) & Dest.UONString & Chr(m_lSeparador) & _
            Dest.UON1 & Chr(m_lSeparador) & Dest.UON2 & Chr(m_lSeparador) & Dest.UON3 & Chr(m_lSeparador) & Dest.UON4
            sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            For Each oIdi In m_oIdiomas
                If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                    sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(oIdi.Cod).Den
                End If
            Next
            sdbgDestinos.AddItem sDest
        Next
        Set Dest = Nothing
            
        sdbgDestinos.MoveFirst
    
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub cmdCodigo_Click()
    Dim teserror As TipoErrorSummit
    
    ''' Resaltar el destino actual
    If sdbgDestinos.Rows = 0 Then Exit Sub
    
    If sdbgDestinos.SelBookmarks.Count > 1 Then Exit Sub
    
    sdbgDestinos.SelBookmarks.Add sdbgDestinos.Bookmark
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    Set m_oDestinoEnEdicion = oDestinos.Item(CStr(sdbgDestinos.Columns("COD").Text))
    Set oIBAseDatosEnEdicion = m_oDestinoEnEdicion
    
    teserror = oIBAseDatosEnEdicion.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgDestinos.SetFocus
        Exit Sub
    End If
        
    oIBAseDatosEnEdicion.CancelarEdicion
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
    frmMODCOD.caption = m_asFrmTitulo(5)
    frmMODCOD.Left = frmESTRORG.Left + 500
    frmMODCOD.Top = frmESTRORG.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodDEST
    frmMODCOD.txtCodAct.Text = m_oDestinoEnEdicion.Cod
    Set frmMODCOD.fOrigen = frmESTRORG
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido m_asMensajes(1)
        Set oIBAseDatosEnEdicion = Nothing
        Set m_oDestinoEnEdicion = Nothing
        Exit Sub
    End If

    If UCase(g_sCodigoNuevo) = UCase(m_oDestinoEnEdicion.Cod) Then
        oMensajes.NoValido m_asMensajes(1)
        Set oIBAseDatosEnEdicion = Nothing
        Set m_oDestinoEnEdicion = Nothing
        Exit Sub
    End If

    
    m_bCambioGeneral = False
    m_bCambioInstala = False
    If UCase(m_oDestinoEnEdicion.Cod) = UCase(gParametrosGenerales.gsDESTDEF) Then
        m_bCambioGeneral = True
    End If
    
    If UCase(m_oDestinoEnEdicion.Cod) = UCase(gParametrosInstalacion.gsDestino) Then
        m_bCambioInstala = True
    End If
            
    ''' Cambiar el codigo
    
    Screen.MousePointer = vbHourglass
    
    ''' Pasamos el usuario que ejucuta FSSG para el control de cambios
    m_oDestinoEnEdicion.Usuario = basOptimizacion.gvarCodUsuario
    teserror = oIBAseDatosEnEdicion.CambiarCodigo(g_sCodigoNuevo)
             
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    ''' Actualizar los datos
    If m_bCambioGeneral Then
          gParametrosGenerales.gsDESTDEF = g_sCodigoNuevo
    End If

    If m_bCambioInstala Then
          gParametrosInstalacion.gsDestino = g_sCodigoNuevo
    End If
    
    oDestinos.Item(sdbgDestinos.Columns("COD").Text).Cod = g_sCodigoNuevo
    
    sdbgDestinos.Columns("COD").Text = g_sCodigoNuevo
    
    Set oIBAseDatosEnEdicion = Nothing
    Set m_oDestinoEnEdicion = Nothing
    
    cmdRestaurarDest_Click
    
    Set oIBAseDatosEnEdicion = Nothing
    Set m_oDestinoEnEdicion = Nothing
    
End Sub
Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en la Destino actual
    
    sdbgDestinos.CancelUpdate
    sdbgDestinos.DataChanged = False
    
    If Not m_oDestinoEnEdicion Is Nothing Then
        ''oIBAseDatosEnEdicion.CancelarEdicion
        Set oIBAseDatosEnEdicion = Nothing
        Set m_oDestinoEnEdicion = Nothing
    End If
    
    cmdA�adirDest.Enabled = True
    cmdEliminarDest.Enabled = True
    cmdDeshacer.Enabled = False
    cmdCodigo.Enabled = True
        
    m_bCargarComboDesdePai = False
    m_bCargarComboDesdeProvi = False
    
    Accion = ACCDestCon
        
End Sub

Private Sub cmdEliminarDest_Click()
'*********************************************************************
'*** Descripci�n: Elimina el/los destino/s seleccionados           ***
'***              En caso de no poder informa de los destinos que  ***
'***              no pudieron ser eliminados                       ***
'*** Par�metros:         ----------                                ***
'*** Valor que devuelve: ----------                                ***
'*********************************************************************
Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim j As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit

    If sdbgDestinos.Rows = 0 Then Exit Sub
    
    If sdbgDestinos.SelBookmarks.Count = 0 Then sdbgDestinos.SelBookmarks.Add sdbgDestinos.Bookmark
       
    Select Case m_sUON
        Case "UON0"
            If sdbgDestinos.Columns("UON1").Text <> "" Then Exit Sub
        Case "UON1"
            If sdbgDestinos.Columns("UON2").Text <> "" Then Exit Sub
        Case "UON2"
            If sdbgDestinos.Columns("UON3").Text <> "" Then Exit Sub
        Case "UON3"
    End Select
    
    Screen.MousePointer = vbHourglass

    ReDim aIdentificadores(sdbgDestinos.SelBookmarks.Count)
    ReDim aBookmarks(sdbgDestinos.SelBookmarks.Count)
    
    i = 0
    j = 0
    ReDim m_arDestinosDeHijos(0)
    
    While i < sdbgDestinos.SelBookmarks.Count
        aIdentificadores(i + 1) = oDestinos.Item(sdbgDestinos.Columns("COD").Text).Cod
        aBookmarks(i + 1) = sdbgDestinos.SelBookmarks(i)
        sdbgDestinos.Bookmark = sdbgDestinos.SelBookmarks(i)
        'Comprobamos que el DESTINO pertenece a la UO y no a alguno de sus hijos.
        If Not oDestinos.Item(sdbgDestinos.Columns("COD").Text) Is Nothing Then
            Select Case m_sUON
                Case "UON0"
                    'El destino pertenece a un hijo
                    If oDestinos.Item(sdbgDestinos.Columns("COD").Text).UON1 <> "" And Not IsNull(oDestinos.Item(sdbgDestinos.Columns("COD").Text).UON1) Then
                        m_arDestinosDeHijos(j) = sdbgDestinos.Columns("COD").Text
                        ReDim Preserve m_arDestinosDeHijos(UBound(m_arDestinosDeHijos) + 1)
                        j = j + 1
                    End If
                Case "UON1"
                    'El destino pertenece a un hijo
                    If oDestinos.Item(sdbgDestinos.Columns("COD").Text).UON2 <> "" And Not IsNull(oDestinos.Item(sdbgDestinos.Columns("COD").Text).UON2) Then
                        m_arDestinosDeHijos(j) = sdbgDestinos.Columns("COD").Text
                        ReDim Preserve m_arDestinosDeHijos(UBound(m_arDestinosDeHijos) + 1)
                        j = j + 1
                    End If
                Case "UON2"
                    'El destino pertenece a un hijo
                    If oDestinos.Item(sdbgDestinos.Columns("COD").Text).UON3 <> "" And Not IsNull(oDestinos.Item(sdbgDestinos.Columns("COD").Text).UON3) Then
                        m_arDestinosDeHijos(j) = sdbgDestinos.Columns("COD").Text
                        ReDim Preserve m_arDestinosDeHijos(UBound(m_arDestinosDeHijos) + 1)
                        j = j + 1
                    End If
                Case "UON3"
                    'El destino pertenece a un hijo
                    If oDestinos.Item(sdbgDestinos.Columns("COD").Text).UON4 <> "" And Not IsNull(oDestinos.Item(sdbgDestinos.Columns("COD").Text).UON4) Then
                        m_arDestinosDeHijos(j) = sdbgDestinos.Columns("COD").Text
                        ReDim Preserve m_arDestinosDeHijos(UBound(m_arDestinosDeHijos) + 1)
                        j = j + 1
                    End If
            End Select
        End If
        i = i + 1
        
    Wend
    
    
    Select Case sdbgDestinos.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
            'irespuesta = oMensajes.PreguntaEliminar(m_asMensajes(3) & " " & sdbgDestinos.Columns(1).Value & " (" & sdbgDestinos.Columns(2).Value & ")")
            ' La (2) antes era la denominaci�n por defecto??
            ' Ahora la (2) es PosIniIdiomas
            irespuesta = oMensajes.PreguntaEliminar(m_asMensajes(3) & " " & sdbgDestinos.Columns(1).Value & " (" & sdbgDestinos.Columns(PosIniIdiomas).Value & ")")
            
        Case Is > 1
            irespuesta = oMensajes.PreguntaEliminarDestinos(m_asMensajes(4))
    End Select

    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        If UBound(m_arDestinosDeHijos) > 0 Then
            oMensajes.InformarDestinosNoPertenecientesALaUON (m_arDestinosDeHijos)
        End If
    End If
    
    ''' 10/06/2002 Nuevo par�metro para controlar que no se elimine el destino de la tabla DEST cuando no se tiene el permiso de modificar
    udtTeserror = oDestinos.EliminarDestinosDeBaseDatos(aIdentificadores, g_vUON1, g_vUON2, g_vUON3, basOptimizacion.gvarCodUsuario, m_bModifDestinos, g_vUON4)
    If udtTeserror.NumError <> TESnoerror Then
      If udtTeserror.NumError = TESImposibleEliminar Then
          iIndice = 1
          aAux = udtTeserror.Arg1
          inum = UBound(udtTeserror.Arg1, 2)

          For i = 0 To inum
              udtTeserror.Arg1(2, i) = oDestinos.Item(CStr(sdbgDestinos.SelBookmarks(aAux(2, i) - iIndice))).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
              sdbgDestinos.SelBookmarks.Remove (aAux(2, i) - iIndice)
              iIndice = iIndice + 1
          Next i
          'Mensaje de cuales no se pudieron eliminar y porque
          oMensajes.ImposibleEliminacionMultiple 297, udtTeserror.Arg1
          If sdbgDestinos.SelBookmarks.Count > 0 Then
                cmdRestaurarDest_Click
                DoEvents
                'sdbgDestinos.ReBind
          End If
      Else
          TratarError udtTeserror
          Screen.MousePointer = vbNormal
          Exit Sub
      End If
    Else
        cmdRestaurarDest_Click
        DoEvents
        'sdbgDestinos.ReBind
    End If
    sdbgDestinos.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdListadoDest_Click()

Dim nodx As node
    ''' * Objetivo: Obtener un listado del organigrama
    frmLstDEST.WindowState = vbNormal
    
    Set nodx = tvwestrorg.selectedItem
    NodoSeleccionado nodx
        If Mid(nodx.Tag, 4, 1) = "0" Then
            frmLstDEST.txtEstOrg.Text = ""
        Else
            Screen.MousePointer = vbHourglass
            Select Case Left(nodx.Tag, 4)
            Case "UON1"
                frmLstDEST.txtEstOrg = oUON1Seleccionada.Cod
                frmLstDEST.sUON1 = oUON1Seleccionada.Cod
                frmLstDEST.sUON2 = ""
                frmLstDEST.sUON3 = ""
            Case "UON2"
                frmLstDEST.txtEstOrg = oUON2Seleccionada.CodUnidadOrgNivel1 & " - " & oUON2Seleccionada.Cod
                frmLstDEST.sUON1 = oUON2Seleccionada.CodUnidadOrgNivel1
                frmLstDEST.sUON2 = oUON2Seleccionada.Cod
                frmLstDEST.sUON3 = ""
            Case "UON3"
                frmLstDEST.txtEstOrg = oUON3Seleccionada.CodUnidadOrgNivel1 & " - " & oUON3Seleccionada.CodUnidadOrgNivel2 & " - " & oUON3Seleccionada.Cod
                frmLstDEST.sUON1 = oUON3Seleccionada.CodUnidadOrgNivel1
                frmLstDEST.sUON2 = oUON3Seleccionada.CodUnidadOrgNivel2
                frmLstDEST.sUON3 = oUON3Seleccionada.Cod
            End Select
            Screen.MousePointer = vbNormal
        End If
   
    frmLstDEST.Show 1
    
End Sub
Private Sub cmdModoEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not m_bModoEdicion Then
                
        ' PASAR A MODO EDICION
        
        sdbgDestinos.AllowAddNew = True
        sdbgDestinos.AllowUpdate = True
        sdbgDestinos.AllowDelete = False
        
        'Set oIBAseDatosEnEdicion = m_oDestinoEnEdicion
                
        frmESTRORG.caption = m_asFrmTitulo(2)
        cmdModoEdicion.caption = m_sConsulta
        
        cmdFiltrar.Visible = False
        cmdRestaurarDest.Visible = False
        cmdListadoDest.Visible = False
        
        If m_bModifDestinos Then
            cmdA�adirDest.Visible = True
            cmdEliminarDest.Visible = True
            cmdEliminarDest.Enabled = True
            cmdA�adirDest.Enabled = True
        Else
             If m_bAsigDestinos Then
                cmdDeshacer.Left = cmdA�adirDest.Left
                cmdEliminarDest.Visible = True
                cmdEliminarDest.Enabled = True
            End If
        End If
        
        
        cmdDeshacer.Visible = True
        cmdCodigo.Visible = (m_bModifCodigoDest And Not basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Dest))
           
        m_bModoEdicion = True
        
        Accion = ACCDestCon
    
    Else
                
        If sdbgDestinos.DataChanged = True Then
        
            v = sdbgDestinos.ActiveCell.Value
            If Me.Visible Then sdbgDestinos.SetFocus
            If (v <> "") Then
                sdbgDestinos.ActiveCell.Value = v
            End If
            
            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        End If
        
        sdbgDestinos.AllowAddNew = False
        sdbgDestinos.AllowUpdate = False
        sdbgDestinos.AllowDelete = False
                
        cmdA�adirDest.Visible = False
        cmdEliminarDest.Visible = False
        cmdDeshacer.Visible = False
        cmdCodigo.Visible = False
        cmdFiltrar.Visible = True
        cmdRestaurarDest.Visible = True
        cmdListadoDest.Visible = True
        
        frmESTRORG.caption = m_asFrmTitulo(1)
        
        cmdModoEdicion.caption = m_sEdicion
        

        sdbgDestinos.Columns("PAI").DropDownHwnd = 0
        sdbgDestinos.Columns("PROVI").DropDownHwnd = 0
        
        m_bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgDestinos.SetFocus
    
End Sub
Private Sub cmdRestaurarDest_Click()

    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    Dim Dest As CDestino
    Dim oIdi As CIdioma
    Dim sDest As String
    
    sdbgDestinos.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    Me.caption = m_asFrmTitulo(1)
    
    Set oDestinos = Nothing
    
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
  
    
    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
    
    For Each Dest In oDestinos
        sDest = Dest.EstadoIntegracion & Chr(m_lSeparador) & Dest.Cod
        sDest = sDest & Chr(m_lSeparador) & Dest.dir & Chr(m_lSeparador) & Dest.Pob & Chr(m_lSeparador) & Dest.cP & Chr(m_lSeparador) & Dest.Pais & Chr(m_lSeparador) & Dest.Provi & _
        Chr(m_lSeparador) & Dest.Tfno & Chr(m_lSeparador) & Dest.Fax & Chr(m_lSeparador) & Dest.Email & _
        Chr(m_lSeparador) & Chr(m_lSeparador) & Dest.UONString & Chr(m_lSeparador) & Dest.UON1 & Chr(m_lSeparador) & Dest.UON2 & Chr(m_lSeparador) & Dest.UON3 & Chr(m_lSeparador) & Dest.UON4
        sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        For Each oIdi In m_oIdiomas
            If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(oIdi.Cod).Den
            End If
        Next
        sdbgDestinos.AddItem sDest

    Next
    
    
    sdbgDestinos.ReBind
    
    If Me.Visible Then sdbgDestinos.SetFocus
    
    Screen.MousePointer = vbNormal
    
End Sub
Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgDestinos.DataChanged = True Then
        m_bValError = False
        m_bAnyaError = False
        m_bModError = False
        If sdbgDestinos.Row = 0 Then
            sdbgDestinos.MoveNext
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgDestinos.MovePrevious
            End If
        Else
            sdbgDestinos.MovePrevious
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgDestinos.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function

Private Sub sdbgDestinos_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    RtnDispErrMsg = 0
    If (sdbgDestinos.Rows = 0) Then
        Exit Sub
    End If
    If Me.Visible Then sdbgDestinos.SetFocus
    sdbgDestinos.Bookmark = sdbgDestinos.RowBookmark(sdbgDestinos.Row)
End Sub
Private Sub sdbgDestinos_AfterInsert(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If m_bAnyaError = False Then
        cmdA�adirDest.Enabled = True
        cmdEliminarDest.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgDestinos_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False Then
        cmdA�adirDest.Enabled = True
        cmdEliminarDest.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgDestinos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    ''' * Objetivo: Confirmacion antes de eliminar
    DispPromptMsg = 0
End Sub
Private Sub sdbgDestinos_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    Dim teserror As TipoErrorSummit
    Dim i As Integer
    Dim oIdi As CIdioma
    Dim oDen As CMultiidiomas
    
    m_bValError = False
    Cancel = False
    
    If Not sdbgDestinos.IsAddRow Then
        If m_oDestinoEnEdicion Is Nothing Then Exit Sub
    End If
    
    If Trim(sdbgDestinos.Columns("COD").Value) = "" Then
        oMensajes.NoValido sdbgDestinos.Columns("COD").caption
        Cancel = True
        GoTo Salir
    End If
    
    'Antes 3 idiomas sdbDestinos.Columns(2),(3),(4) ahora eso es din�mico
    'For i = 2 To 4
    For i = 0 To sdbgDestinos.Cols - 1
        If Left(sdbgDestinos.Columns(i).Name, 4) = "DEN_" Then
            If Trim(sdbgDestinos.Columns(i).Value) = "" Then
                oMensajes.NoValida sdbgDestinos.Columns(i).caption
                Cancel = True
                GoTo Salir
            End If
        End If
    Next
    
    If Not sdbgDestinos.Columns("GEN").Value Then
        If Trim(sdbgDestinos.Columns("DIR").Value) = "" Then
            oMensajes.NoValido sdbgDestinos.Columns("DIR").caption
            Cancel = True
            GoTo Salir
        End If
        If Trim(sdbgDestinos.Columns("POB").Value) = "" Then
            oMensajes.NoValido sdbgDestinos.Columns("POB").caption
            Cancel = True
            GoTo Salir
        End If
        If Trim(sdbgDestinos.Columns("CP").Value) = "" Then
            oMensajes.NoValido sdbgDestinos.Columns("CP").caption
            Cancel = True
            GoTo Salir
        End If
        If Trim(sdbgDestinos.Columns("PAI").Value) = "" Then
            oMensajes.NoValido sdbgDestinos.Columns("PAI").caption
            Cancel = True
            GoTo Salir
        End If
    Else
        If Not oDestinos.ValidarDestGen�rico(sdbgDestinos.Columns("COD").Text) Then
            sdbgDestinos.Columns("GEN").Value = False
            oMensajes.mensajeGenericoOkOnly m_sValidarDestGenerico, Exclamation
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If sdbgDestinos.IsAddRow Then
        Set m_oDestinoEnEdicion = Nothing
        Set m_oDestinoEnEdicion = oFSGSRaiz.Generar_CDestino
        
        m_oDestinoEnEdicion.Cod = sdbgDestinos.Columns("COD").Value
        
        Set oDen = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdi In m_oIdiomas
            oDen.Add oIdi.Cod, ""
            oDen.Item(oIdi.Cod).Den = sdbgDestinos.Columns("DEN_" & oIdi.Cod).Value
        Next
        Set m_oDestinoEnEdicion.Denominaciones = oDen
        
        m_oDestinoEnEdicion.dir = StrToNull(sdbgDestinos.Columns("DIR").Value)
        m_oDestinoEnEdicion.Pob = StrToNull(sdbgDestinos.Columns("POB").Value)
        m_oDestinoEnEdicion.cP = StrToNull(sdbgDestinos.Columns("CP").Value)
        m_oDestinoEnEdicion.Pais = StrToNull(sdbgDestinos.Columns("PAI").Value)
        m_oDestinoEnEdicion.Provi = StrToNull(sdbgDestinos.Columns("PROVI").Value)
        m_oDestinoEnEdicion.Tfno = StrToNull(sdbgDestinos.Columns("TFNO").Value)
        m_oDestinoEnEdicion.Fax = StrToNull(sdbgDestinos.Columns("FAX").Value)
        m_oDestinoEnEdicion.Email = StrToNull(sdbgDestinos.Columns("EMAIL").Value)
        If sdbgDestinos.Columns("GEN").Value Then
            m_oDestinoEnEdicion.Generico = 1
        Else
            m_oDestinoEnEdicion.Generico = 0
        End If
        m_oDestinoEnEdicion.UON1 = g_vUON1
        m_oDestinoEnEdicion.UON2 = g_vUON2
        m_oDestinoEnEdicion.UON3 = g_vUON3
        m_oDestinoEnEdicion.UON4 = g_vUON4
        ''' Pasamos el usuario q ejecuta FSGS para el control de cambios
        m_oDestinoEnEdicion.Usuario = basOptimizacion.gvarCodUsuario

        Set oIBAseDatosEnEdicion = m_oDestinoEnEdicion
    
        teserror = oIBAseDatosEnEdicion.AnyadirABaseDatos
    
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgDestinos.SetFocus
            m_bAnyaError = True
            cmdDeshacer_Click
            Exit Sub
        Else
       ''' Registro de acciones
            If gParametrosGenerales.gbACTIVLOG Then
                oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, AccionesSummit.ACCDestAnya, "Cod:" & m_oDestinoEnEdicion.Cod
            End If
            Accion = ACCDestCon
        End If
        
        If Not oDestinos.Item(CStr(sdbgDestinos.Columns("COD").Text)) Is Nothing Then
            If oDestinos.Item(CStr(sdbgDestinos.Columns("COD").Text)).Cod <> sdbgDestinos.Columns("COD").Value Then
                oDestinos.Add sdbgDestinos.Columns("COD").Value, oDen, sdbgDestinos.Columns("DIR").Value, sdbgDestinos.Columns("POB").Value, sdbgDestinos.Columns("CP").Value, sdbgDestinos.Columns("PROVI").Value, sdbgDestinos.Columns("PAI").Value, , g_vUON1, g_vUON2, g_vUON3, g_vUON4, , m_oDestinoEnEdicion.FECACT, , , , m_oDestinoEnEdicion.Tfno, m_oDestinoEnEdicion.Fax, m_oDestinoEnEdicion.Email
            End If
        Else
            oDestinos.Add sdbgDestinos.Columns("COD").Value, oDen, sdbgDestinos.Columns("DIR").Value, sdbgDestinos.Columns("POB").Value, sdbgDestinos.Columns("CP").Value, sdbgDestinos.Columns("PROVI").Value, sdbgDestinos.Columns("PAI").Value, , g_vUON1, g_vUON2, g_vUON3, g_vUON4, , m_oDestinoEnEdicion.FECACT, , , , m_oDestinoEnEdicion.Tfno, m_oDestinoEnEdicion.Fax, m_oDestinoEnEdicion.Email
        End If
        
    Else   ''' Modificamos en la base de datos
        m_bModError = False
        Set oIBAseDatosEnEdicion = m_oDestinoEnEdicion
        
        m_oDestinoEnEdicion.Cod = sdbgDestinos.Columns("COD").Value
        
        Set oDen = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdi In m_oIdiomas
            oDen.Add oIdi.Cod, ""
        Next
        For Each oIdi In m_oIdiomas
            oDen.Item(oIdi.Cod).Den = sdbgDestinos.Columns("DEN_" & oIdi.Cod).Value
        Next
        Set m_oDestinoEnEdicion.Denominaciones = oDen
        m_oDestinoEnEdicion.dir = StrToNull(sdbgDestinos.Columns("DIR").Value)
        m_oDestinoEnEdicion.Pob = StrToNull(sdbgDestinos.Columns("POB").Value)
        m_oDestinoEnEdicion.cP = StrToNull(sdbgDestinos.Columns("CP").Value)
        m_oDestinoEnEdicion.Pais = StrToNull(sdbgDestinos.Columns("PAI").Value)
        m_oDestinoEnEdicion.Provi = StrToNull(sdbgDestinos.Columns("PROVI").Value)
        m_oDestinoEnEdicion.Tfno = StrToNull(sdbgDestinos.Columns("TFNO").Value)
        m_oDestinoEnEdicion.Fax = StrToNull(sdbgDestinos.Columns("FAX").Value)
        m_oDestinoEnEdicion.Email = StrToNull(sdbgDestinos.Columns("EMAIL").Value)
        If sdbgDestinos.Columns("GEN").Value Then
            m_oDestinoEnEdicion.Generico = 1
        Else
            m_oDestinoEnEdicion.Generico = 0
        End If
        ''' Pasamos el usuario q ejecuta FSGS para el control de cambios
        m_oDestinoEnEdicion.Usuario = basOptimizacion.gvarCodUsuario
        teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando

        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgDestinos.SetFocus
            m_bModError = True
        Else

        ''' Registro de acciones
            If gParametrosGenerales.gbACTIVLOG Then
                oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, AccionesSummit.ACCDestAnya, "Cod:" & m_oDestinoEnEdicion.Cod
            End If
            Accion = ACCDestCon
            ''' como no se refresca la informaci�n de base de datos, muevo el estado de integracion directamente a
            sdbgDestinos.Columns("INT").Value = CStr(EstadoIntegracion.PendienteDeTratar)
        End If
        
        Set oIBAseDatosEnEdicion = Nothing

    End If
        
    Set m_oDestinoEnEdicion = Nothing
    
Salir:
        
    m_bValError = Cancel
    If Me.Visible Then sdbgDestinos.SetFocus

End Sub
Private Sub sdbgDestinos_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    Dim oDen As CMultiidiomas
    Dim oIdi As CIdioma
    
    If sdbgDestinos.col = 1 Then
        If sdbgDestinos.Columns("COD").Locked = True Then
            sdbgDestinos.CancelUpdate
            sdbgDestinos.DataChanged = False
        End If
    End If
    
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adirDest.Enabled = False
        cmdEliminarDest.Enabled = False
        cmdDeshacer.Enabled = True
        cmdCodigo.Enabled = False
    
    End If

    If sdbgDestinos.Columns(sdbgDestinos.col).Name = "GEN" Then
        If sdbgDestinos.Columns("GEN").Value Then
            If Not oDestinos.ValidarDestGen�rico(sdbgDestinos.Columns("COD").Text) Then
                sdbgDestinos.Columns("GEN").Value = False
                oMensajes.mensajeGenericoOkOnly m_sValidarDestGenerico, Exclamation
                Exit Sub
            End If
        End If
    End If
    
    If Accion = ACCDestCon And Not sdbgDestinos.IsAddRow Then

        Set m_oDestinoEnEdicion = Nothing
        Set m_oDestinoEnEdicion = oDestinos.Item(CStr(sdbgDestinos.Columns("COD").Text))
        If m_oDestinoEnEdicion Is Nothing Then Exit Sub
        
        Set oIBAseDatosEnEdicion = m_oDestinoEnEdicion

        teserror = oIBAseDatosEnEdicion.IniciarEdicion

        If teserror.NumError = TESInfModificada Then

            TratarError teserror
            sdbgDestinos.DataChanged = False
            sdbgDestinos.Columns("COD").Value = m_oDestinoEnEdicion.Cod
            
            Set oDen = oFSGSRaiz.Generar_CMultiidiomas
            For Each oIdi In m_oIdiomas
                oDen.Add oIdi.Cod, ""
            Next
            For Each oIdi In m_oIdiomas
                oDen.Item(oIdi.Cod).Den = sdbgDestinos.Columns("DEN_" & oIdi.Cod).Value
            Next
            Set m_oDestinoEnEdicion.Denominaciones = oDen
            
            sdbgDestinos.Columns("DEN_" & gParametrosInstalacion.gIdioma).Text = m_oDestinoEnEdicion.Denominaciones.Item("DEN_" & gParametrosInstalacion.gIdioma).Den
            For Each oIdi In m_oIdiomas
                If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                    sdbgDestinos.Columns("DEN_" & oIdi.Cod).Text = m_oDestinoEnEdicion.Denominaciones.Item("DEN_" & oIdi.Cod).Den
                End If
            Next
            
            sdbgDestinos.Columns("DIR").Value = m_oDestinoEnEdicion.dir
            sdbgDestinos.Columns("POB").Value = m_oDestinoEnEdicion.Pob
            sdbgDestinos.Columns("CP").Value = m_oDestinoEnEdicion.cP
            sdbgDestinos.Columns("PAI").Value = m_oDestinoEnEdicion.Pais
            sdbgDestinos.Columns("PROVI").Value = m_oDestinoEnEdicion.Provi
            sdbgDestinos.Columns("TFNO").Value = m_oDestinoEnEdicion.Tfno
            sdbgDestinos.Columns("FAX").Value = m_oDestinoEnEdicion.Fax
            sdbgDestinos.Columns("EMAIL").Value = m_oDestinoEnEdicion.Email
            teserror.NumError = TESnoerror

        End If

        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgDestinos.SetFocus
        Else
            Accion = ACCDestMod
        End If


    End If

    If sdbgDestinos.col = 7 Then
        m_bCargarComboDesdePai = True
    End If

    If sdbgDestinos.col = 8 Then
        m_bCargarComboDesdeProvi = True
    End If
    
End Sub

''' * Objetivo: Ordenar el grid segun la columna
'''  Revisada EPB 14/09/2011
Private Sub sdbgDestinos_HeadClick(ByVal ColIndex As Integer)

    
    
    Dim sCodigo As String
    Dim lCodigo As Integer  ' multilenguaje
    Dim lDenominacion As Integer  ' multilenguaje
    Dim sDenominacion As String
    Dim sHeadCaption As String
    Dim Dest As CDestino
    Dim sDest As String
    Dim oIdi As CIdioma
    
    
    Dim columnaDEN As String
    Dim itemIdioma As Variant
    Dim arrayIdiomas() As String
    
    'arrayIdiomas = [DEN_SPA DEN_ENG DEN_GER DEN_FRA]'
    arrayIdiomas = Split(den_general, ",")
    
    For Each itemIdioma In arrayIdiomas
         If (UCase(itemIdioma) = UCase(sdbgDestinos.Columns(ColIndex).Name)) Then
                columnaDEN = itemIdioma 'columnaDEN=DEN_SPA o columnaDEN=DEN_ENG...
        End If
    Next
    
    
    If m_bModoEdicion Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgDestinos.Columns(ColIndex).caption
    sdbgDestinos.Columns(ColIndex).caption = m_sOrdenando
    
    ''' Volvemos a cargar las Destinos, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    
    Set oDestinos = Nothing
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
   
    
    If caption <> m_asFrmTitulo(1) Then
    
        If InStr(1, caption, m_asFrmTitulo(3), vbTextCompare) <> 0 Then
            ''' Datos filtrados por codigo
            
            If InStr(1, caption, "*", vbTextCompare) <> 0 Then
                
                lCodigo = Len(caption) - Len(m_asFrmTitulo(3))
                sCodigo = Mid(caption, Len(m_asFrmTitulo(3)) + 1, lCodigo - 1)
                
                Select Case UCase(sdbgDestinos.Columns(ColIndex).Name)
                
                Case "COD" 'Cod
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                ' Case "DEN_SPA", "DEN_ENG", "DEN_GER" ' Den
                Case columnaDEN 'columnaDEN=DEN_SPA o columnaDEN=DEN_ENG...
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, True, , Trim(sCodigo), , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , sdbgDestinos.Columns(ColIndex).Name, g_vUON4
                Case "DIR" ' Dir
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , , True, , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "POB" ' Pob
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , , , True, , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "CP" ' CP
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , , , , True, , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "PAI" ' Pai
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , , , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "PROVI" ' Prov
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , , , , , True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "INT"
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), True, , g_vUON4
                Case Else
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                End Select
                
            Else
                lCodigo = Len(caption) - Len(m_asFrmTitulo(3))
                sCodigo = Mid(caption, Len(m_asFrmTitulo(3)) + 1, lCodigo)
                
                Select Case UCase(sdbgDestinos.Columns(ColIndex).Name)
                
                Case "COD" 'Cod
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                ' Case "DEN_SPA", "DEN_ENG", "DEN_GER" ' Den
                Case columnaDEN
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, True, , , Trim(sCodigo), True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , sdbgDestinos.Columns(ColIndex).Name, g_vUON4
                Case "DIR" ' Dir
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , True, True, , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "POB" ' Pob
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , True, , True, , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "CP" ' CP
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , True, , , True, , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "PAI" ' Pai
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , True, , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "PROVI ' Prov"
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , True, , , , True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "INT"
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), True, , g_vUON4
                Case Else
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , Trim(sCodigo), , True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                End Select

                
           End If
           
        Else
            
            ''' Datos filtrados por denominacion
            
            lDenominacion = Len(caption) - Len(m_asFrmTitulo(4))
            sDenominacion = Mid(caption, Len(m_asFrmTitulo(4)) + 1, lDenominacion)
            If InStr(1, sDenominacion, "*", vbTextCompare) <> 0 Then
            
                 sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
                Select Case UCase(sdbgDestinos.Columns(ColIndex).Name)
                
                Case "COD" 'Cod
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                ' Case "DEN_SPA", "DEN_ENG", "DEN_GER" ' Den
                Case columnaDEN
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, True, , , Trim(sDenominacion), , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , sdbgDestinos.Columns(ColIndex).Name, g_vUON4
                Case "DIR" ' Dir
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), , True, , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "POB" ' Pob
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), , , True, , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "CP" ' CP
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), , , , True, , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "PAI" ' Pai
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), , , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "PROVI" ' Prov"
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), , , , , True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "INT"
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), True, , g_vUON4
                Case Else
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                End Select
                            
            Else
                
                sDenominacion = Left(sDenominacion, Len(sDenominacion))
             Select Case UCase(sdbgDestinos.Columns(ColIndex).Name)
                
                Case "COD" 'Cod
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                ' Case "DEN_SPA", "DEN_ENG", "DEN_GER" ' Den
                Case columnaDEN
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, True, , , Trim(sDenominacion), True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , sdbgDestinos.Columns(ColIndex).Name, g_vUON4
                Case "DIR" ' Dir
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), True, True, , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "POB" ' Pob
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), True, , True, , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "CP" ' CP
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), True, , , True, , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "PAI" ' Pai
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), True, , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "PROVI" ' Prov
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), True, , , , True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                Case "INT"
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), True, , g_vUON4
                Case Else
                    oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , Trim(sDenominacion), True, , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
                End Select

                    
            End If
                
        End If
                
    Else
    
        ''' Datos no filtrados
        Select Case UCase(sdbgDestinos.Columns(ColIndex).Name)
        Case "INT" 'Estado integracion
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), True, , g_vUON4
        Case "COD" 'Cod
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
        ' Case "DEN_SPA", "DEN_ENG", "DEN_GER" ' Den
        Case columnaDEN
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, True, , , , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , sdbgDestinos.Columns(ColIndex).Name, g_vUON4
        Case "DIR" ' Dir
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , True, , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
        Case "POB" ' Pob
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , True, , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
        Case "CP" ' CP
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , True, , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
        Case "PAI" ' Pai
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
        Case "PROVI" ' Prov
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , , True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
        Case Else
            oDestinos.CargarTodosLosDestinosUON g_vUON1, g_vUON2, g_vUON3, , , , , , , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Dest), , , g_vUON4
        
        End Select
    
    End If
    
    sdbgDestinos.RemoveAll
    
    ' Reordenaci�n para que se muestre ordenado seg�n vista GS
    For Each Dest In oDestinos
        sDest = Dest.EstadoIntegracion & Chr(m_lSeparador) & Dest.Cod & Chr(m_lSeparador) & Dest.Generico
        sDest = sDest & Chr(m_lSeparador) & Dest.dir & Chr(m_lSeparador) & Dest.Pob & Chr(m_lSeparador) & Dest.cP & Chr(m_lSeparador) & _
        Dest.Pais & Chr(m_lSeparador) & Dest.Provi & Chr(m_lSeparador) & _
        Dest.Tfno & Chr(m_lSeparador) & Dest.Fax & Chr(m_lSeparador) & Dest.Email & Chr(m_lSeparador) & _
        " " & Chr(m_lSeparador) & Dest.UONString & Chr(m_lSeparador) & _
        Dest.UON1 & Chr(m_lSeparador) & Dest.UON2 & Chr(m_lSeparador) & Dest.UON3 & Chr(m_lSeparador) & Dest.UON4
        sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        For Each oIdi In m_oIdiomas
            If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                sDest = sDest & Chr(m_lSeparador) & Dest.Denominaciones.Item(oIdi.Cod).Den
            End If
        Next
        sdbgDestinos.AddItem sDest
    Next
        
    Select Case ColIndex
    Case 1
        sOrdenListado = "COD"
    Case 2, 3, 4
        sOrdenListado = sdbgDestinos.Columns(ColIndex).Name
    End Select
    
    'sdbgDestinos.ReBind
    sdbgDestinos.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgDestinos_InitColumnProps()
    
    sdbgDestinos.Columns("COD").FieldLen = basParametros.gLongitudesDeCodigos.giLongCodDEST
    sdbgDestinos.BalloonHelp = True
    
End Sub

Private Sub sdbgDestinos_KeyDown(KeyCode As Integer, Shift As Integer)
'*********************************************************************
'*** Descripci�n: Captura la tecla Supr (Delete) y cuando          ***
'***              �sta es presionada se lanza el evento            ***
'***              Click asociado al bot�n eliminar para            ***
'***              borrar de la BD y de la grid las lineas          ***
'***              seleccionadas.                                   ***
'*** Par�metros:  KeyCode ::> Contiene el c�digo interno           ***
'***                          de la tecla pulsada.                 ***
'***              Shift   ::> Es la m�scara, sin uso en esta       ***
'***                          subrutina.                           ***
'*** Valor que devuelve: ----------                                ***
'*********************************************************************
    If KeyCode = vbKeyDelete Then
        If m_bModoEdicion Then
            If cmdEliminarDest.Enabled = True And sdbgDestinos.SelBookmarks.Count > 0 Then
                cmdEliminarDest_Click
            End If
             
        End If
    End If
End Sub

Private Sub sdbgDestinos_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgDestinos.DataChanged = False Then
            
            sdbgDestinos.CancelUpdate
            sdbgDestinos.DataChanged = False
            
            If Not m_oDestinoEnEdicion Is Nothing Then
                oIBAseDatosEnEdicion.CancelarEdicion
                Set oIBAseDatosEnEdicion = Nothing
                Set m_oDestinoEnEdicion = Nothing
            End If
           
            m_bCargarComboDesdePai = False
            m_bCargarComboDesdeProvi = False
            
            If Not sdbgDestinos.IsAddRow Then
                cmdA�adirDest.Enabled = True
                cmdEliminarDest.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
            Else
                cmdA�adirDest.Enabled = False
                cmdEliminarDest.Enabled = False
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = False
            End If
            
            Accion = ACCDestCon
            
        Else
        
            If sdbgDestinos.IsAddRow Then
           
                cmdA�adirDest.Enabled = True
                cmdEliminarDest.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
           
                Accion = ACCDestCon
            
            End If
            
        End If
        
    End If
    
End Sub

Private Sub sdbgDestinos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If m_bModoEdicion = False Then
        sdbgDestinos.Columns("PAI").DropDownHwnd = 0
        sdbgDestinos.Columns("PROVI").DropDownHwnd = 0
    ElseIf m_bModifDestinos Then
        sdbgDestinos.Columns("PAI").DropDownHwnd = sdbddPaises.hWnd
        sdbgDestinos.Columns("PROVI").DropDownHwnd = sdbddProvincias.hWnd
    End If
    
    If Not sdbgDestinos.IsAddRow Then
        sdbgDestinos.Columns("COD").Locked = True
        sdbddDestinos.Enabled = False
        sdbgDestinos.Columns("COD").DropDownHwnd = 0
        If sdbgDestinos.DataChanged = False Then
            cmdA�adirDest.Enabled = True
            cmdEliminarDest.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True
        End If
    Else
        If sdbgDestinos.DataChanged = True Then cmdDeshacer.Enabled = True
        sdbgDestinos.Columns("COD").Locked = False
        sdbddDestinos.Enabled = True
        sdbgDestinos.Columns("COD").DropDownHwnd = sdbddDestinos.hWnd
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgDestinos.Row) Then
                sdbgDestinos.col = 1
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adirDest.Enabled = False
        cmdEliminarDest.Enabled = False
        cmdCodigo.Enabled = False
        
    End If
        
    If sdbgDestinos.col = 7 Then
        m_bCargarComboDesdePai = True
    End If
    If sdbgDestinos.col = 8 Then
        m_bCargarComboDesdeProvi = True
    End If
    
End Sub
Private Sub sdbddPaises_CloseUp()
    If sdbddPaises.Columns(0).Value <> "" Then
        If sdbddPaises.Columns(0).Value = m_sPais Then
            Exit Sub
        Else
            sdbgDestinos.Columns("PROVI").Value = ""
        End If
    End If
    m_sPais = ""
End Sub


Private Sub sdbddPaises_DropDown()

    ''' * Objetivo: Abrir el combo de Paises de la forma adecuada
    Screen.MousePointer = vbHourglass
    
    If m_bCargarComboDesdePai Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgDestinos.ActiveCell.Value), , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If
        
    CargarGridConPaises
    
    sdbgDestinos.ActiveCell.SelStart = 0
    sdbgDestinos.ActiveCell.SelLength = Len(sdbgDestinos.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
    m_sPais = sdbgDestinos.Columns("PAI").Value
End Sub

Private Sub sdbddPaises_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddPaises.DataFieldList = "Column 0"
    sdbddPaises.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddPaises_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddPaises.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddPaises.Rows - 1
            bm = sdbddPaises.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddPaises.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddPaises.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddPaises_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    If Text = "..." Then RtnPassed = False
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
        
        ''' Comprobar la existencia en la lista
        
        If UCase(Text) = UCase(sdbddPaises.Columns(0).Value) Then
            RtnPassed = True
            Exit Sub
        End If
        
        m_oPaises.CargarTodosLosPaises Text, , True, , , False
        If Not m_oPaises.Item(Text) Is Nothing Then
            RtnPassed = True
            sdbgDestinos.Columns("PROVI").Value = ""
        Else
            sdbgDestinos.Columns("PAI").Value = ""
            sdbgDestinos.Columns("PROVI").Value = ""
        End If
    
    End If
    
End Sub
Private Sub CargarGridConPaises()

    ''' * Objetivo: Cargar combo con la coleccion de Paises
    
    Dim oPai As CPais
    
    sdbddPaises.RemoveAll
    
    For Each oPai In m_oPaises
        sdbddPaises.AddItem oPai.Cod & Chr(m_lSeparador) & oPai.Den
    Next
    
    If sdbddPaises.Rows = 0 Then
        sdbddPaises.AddItem ""
    Else
        If m_bCargarComboDesdePai And Not m_oPaises.EOF Then
            sdbddPaises.AddItem "..."
        End If
    End If
End Sub


Private Sub sdbddProvincias_DropDown()

Dim oPais As CPais

    ''' * Objetivo: Abrir el combo de Paises de la forma adecuada
    
    If sdbgDestinos.Columns("PAI").Value = "" Then
        sdbddProvincias.RemoveAll
        sdbddProvincias.AddItem ""
        sdbddProvincias.Refresh
        
        sdbgDestinos.ActiveCell.SelStart = 0
        sdbgDestinos.ActiveCell.SelLength = Len(sdbgDestinos.ActiveCell.Text)
        Screen.MousePointer = vbNormal
    
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    
    If m_oPaises.Item(sdbgDestinos.Columns("PAI").Value) Is Nothing Then
        If m_bCargarComboDesdePai Then
            m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgDestinos.ActiveCell.Value), , , False
        Else
            m_oPaises.CargarTodosLosPaises , , , , , False
        End If
    End If
    
    Set oPais = m_oPaises.Item(sdbgDestinos.Columns("PAI").Value)
      
    If m_bCargarComboDesdeProvi And sdbgDestinos.ActiveCell.Value <> "" Then
        oPais.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgDestinos.ActiveCell.Value), , , False
    Else
        oPais.CargarTodasLasProvincias
    End If
    
    Set m_oProvincias = oPais.Provincias
    
    
    Set oPais = Nothing
    
    CargarGridConProvincias
    
    sdbgDestinos.ActiveCell.SelStart = 0
    sdbgDestinos.ActiveCell.SelLength = Len(sdbgDestinos.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProvincias_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProvincias.DataFieldList = "Column 0"
    sdbddProvincias.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddProvincias_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProvincias.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProvincias.Rows - 1
            bm = sdbddProvincias.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProvincias.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProvincias.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddProvincias_ValidateList(Text As String, RtnPassed As Integer)
Dim oPais As CPais

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
    Else
        
        ''' Comprobar la existencia en la lista
                
        If Text = sdbddProvincias.Columns(0).Value Then
            RtnPassed = True
            Exit Sub
        End If
        
        Set oPais = m_oPaises.Item(sdbgDestinos.Columns("PAI").Value)
        
        If oPais Is Nothing Then
            sdbgDestinos.Columns("PROVI").Value = ""
            Exit Sub
        End If
        
        If oPais.Provincias Is Nothing Then
            oPais.CargarTodasLasProvincias
            If oPais.Provincias Is Nothing Then
                sdbgDestinos.Columns("PROVI").Value = ""
                Exit Sub
            End If
        End If
        
        If Not oPais.Provincias.Item(sdbgDestinos.ActiveCell.Value) Is Nothing Then
            RtnPassed = True
        Else
            sdbgDestinos.Columns("PROVI").Value = ""
        End If
    
    End If
    
End Sub
Private Sub CargarGridConProvincias()

    ''' * Objetivo: Cargar combo con la coleccion de Paises
    
    Dim oProvi As CProvincia
    
    sdbddProvincias.RemoveAll
    
    For Each oProvi In m_oProvincias
        sdbddProvincias.AddItem oProvi.Cod & Chr(m_lSeparador) & oProvi.Den
    Next
    
    If sdbddProvincias.Rows = 0 Then
        sdbddProvincias.AddItem ""
    Else
        If m_bCargarComboDesdeProvi And Not m_oProvincias.EOF Then
            sdbddProvincias.AddItem "..."
        End If
    End If
    
End Sub


Private Sub sdbddDestinos_CloseUp()
Dim oIdi As CIdioma

    sdbgDestinos.Columns("COD").Text = sdbddDestinos.Columns("COD").Text
    sdbgDestinos.Columns("DEN_" & gParametrosInstalacion.gIdioma).Text = sdbddDestinos.Columns("DEN_" & gParametrosInstalacion.gIdioma).Text
    For Each oIdi In m_oIdiomas
        If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
            sdbgDestinos.Columns("DEN_" & oIdi.Cod).Text = sdbddDestinos.Columns("DEN_" & oIdi.Cod).Text
        End If
    Next
    sdbgDestinos.Columns("DIR").Text = sdbddDestinos.Columns("DIR").Text
    sdbgDestinos.Columns("POB").Text = sdbddDestinos.Columns("POB").Text
    sdbgDestinos.Columns("CP").Text = sdbddDestinos.Columns("CP").Text
    sdbgDestinos.Columns("PAI").Text = sdbddDestinos.Columns("PAI").Text
    sdbgDestinos.Columns("PROVI").Text = sdbddDestinos.Columns("PROVI").Text
    
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adirDest.Enabled = False
        cmdEliminarDest.Enabled = False
        cmdDeshacer.Enabled = True
        cmdCodigo.Enabled = False
    
    End If
End Sub

Private Sub sdbddDestinos_InitColumnProps()
    
    sdbddDestinos.DataFieldList = "Column 0"
    sdbddDestinos.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbddDestinos_DropDown()

Dim ADORs As Ador.Recordset
Dim sDest As String
Dim oIdi As CIdioma

    
    Set ADORs = oDestinos.DevolverTodosLosDestinos(sdbgDestinos.Columns("COD").Text, sdbgDestinos.Columns("DEN_" & gParametrosInstalacion.gIdioma).Text, , , g_vUON1, g_vUON2, g_vUON3, True)
    
    If ADORs Is Nothing Then
        sdbddDestinos.RemoveAll
        Screen.MousePointer = vbNormal
        sdbddDestinos.AddItem ""
        sdbgDestinos.ActiveCell.SelStart = 0
        sdbgDestinos.ActiveCell.SelLength = Len(sdbgDestinos.ActiveCell.Text)
        Exit Sub
    End If
    
    sdbddDestinos.RemoveAll
    
    While Not ADORs.EOF
        sDest = ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        For Each oIdi In m_oIdiomas
            If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                sDest = sDest & Chr(m_lSeparador) & ADORs("DEN_" & oIdi.Cod).Value
            End If
        Next
        
        sdbddDestinos.AddItem sDest
        ADORs.MoveNext
    Wend
    
    If sdbddDestinos.Rows = 0 Then
        sdbddDestinos.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing
    
    sdbgDestinos.ActiveCell.SelStart = 0
    sdbgDestinos.ActiveCell.SelLength = Len(sdbgDestinos.ActiveCell.Text)
        
End Sub
Private Sub sdbddDestinos_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddDestinos.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddDestinos.Rows - 1
            bm = sdbddDestinos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddDestinos.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddDestinos.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddDestinos_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
Dim Ador As Ador.Recordset
Dim oDests As CDestinos
Dim oIdi As CIdioma

If Text = "..." Then RtnPassed = False

    ''' Si es nulo, correcto

If Text = "" Then
    RtnPassed = True
Else

   ''' Comprobar la existencia en la lista

    If UCase(Text) = UCase(sdbddDestinos.Columns(0).Value) Then
        RtnPassed = True
        Exit Sub
    End If

    Set oDests = oFSGSRaiz.Generar_CDestinos
    ''' A este punto solo se llega cuando se tiene, por lo menos, permiso de Asignaci�n
    ''' A�ado el coincidencia total, para el caso de que solo tenga permiso para Asignar, pero no modificar
    If Not m_bModifDestinos Then
        Set Ador = oDests.DevolverTodosLosDestinos(Text, , True)
    Else
        Set Ador = oDests.DevolverTodosLosDestinos(Text)
    End If
    
    If Not Ador.EOF Then
        If UCase(Ador("DESTINOCOD").Value) = UCase(Text) And Ador("SINTRANS").Value <> 1 Then
            sdbgDestinos.Columns("DEN_" & gParametrosInstalacion.gIdioma).Text = Ador("DEN_" & gParametrosInstalacion.gIdioma).Value
            For Each oIdi In m_oIdiomas
                If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                    sdbgDestinos.Columns("DEN_" & oIdi.Cod).Text = Ador("DEN_" & oIdi.Cod).Value
                End If
            Next
            sdbgDestinos.Columns("DIR").Value = NullToStr(Ador("DESTINODIR").Value)
            sdbgDestinos.Columns("POB").Value = NullToStr(Ador("DESTINOPOB").Value)
            sdbgDestinos.Columns("CP").Value = NullToStr(Ador("DESTINOCP").Value)
            sdbgDestinos.Columns("PROVI").Value = NullToStr(Ador("DESTINOPROVI").Value)
            sdbgDestinos.Columns("PAI").Value = NullToStr(Ador("DESTINOPAI").Value)
            sdbgDestinos.Columns("TFNO").Value = NullToStr(Ador("TFNO").Value)
            sdbgDestinos.Columns("FAX").Value = NullToStr(Ador("FAX").Value)
            sdbgDestinos.Columns("EMAIL").Value = NullToStr(Ador("EMAIL").Value)
        Else 'Al a�adir un nuevo destino se estaban cogiendo los valores del primer destino recuperado en el recordset
            For Each oIdi In m_oIdiomas
                sdbgDestinos.Columns("DEN_" & oIdi.Cod).Text = ""
            Next
            sdbgDestinos.Columns("DIR").Value = ""
            sdbgDestinos.Columns("POB").Value = ""
            sdbgDestinos.Columns("CP").Value = ""
            sdbgDestinos.Columns("PROVI").Value = ""
            sdbgDestinos.Columns("PAI").Value = ""
            sdbgDestinos.Columns("TFNO").Value = ""
            sdbgDestinos.Columns("FAX").Value = ""
            sdbgDestinos.Columns("EMAIL").Value = ""
        End If
        If (UCase(Ador("DESTINOCOD").Value) = UCase(Text) And Ador("SINTRANS").Value = 1) Then
            oMensajes.NoValido 2
            RtnPassed = False
        Else
            RtnPassed = True
        End If
    End If
    Ador.Close
    Set Ador = Nothing
    Set oDests = Nothing
End If
End Sub

Public Sub ponerCaption(Texto, indice, comodin)
    
    caption = m_asFrmTitulo(indice) & Texto & IIf(comodin, "*", "")

End Sub


Private Function SeguridadDestinos(ByVal node As MSComctlLib.node) As Boolean
'***************************************************************************
'***                                                                      '***
'*** Descripci�n: Segun los permisos hacer visible la pesta�a de destinos '***
'*** Parametros: El node del tvwestrorg                                   '***
'*** Valor que devuelve: Un Booleano, si es true la pesta�a es visible    '***
'***                                                                      '***
'****************************************************************************
    
    If basOptimizacion.gTipoDeUsuario = Persona Or basOptimizacion.gTipoDeUsuario = comprador Then
        If m_bConDestinos Then
            If bRUO Then
                
                If Left(node.Tag, 4) = "UON0" Then
                    If oUsuarioSummit.Persona.UON1 = "" Then
                        SeguridadDestinos = True
                    Else
                        SeguridadDestinos = False
                    End If
                    Exit Function
                End If
                
                If Left(node.Tag, 4) = "UON1" Then
                    If oUsuarioSummit.Persona.UON2 = "" Then
                        SeguridadDestinos = True
                    Else
                        SeguridadDestinos = False
                    End If
                    Exit Function
                End If
                
                If Left(node.Tag, 4) = "UON2" Then
                    If oUsuarioSummit.Persona.UON3 = "" Then
                        SeguridadDestinos = True
                    Else
                        SeguridadDestinos = False
                    End If
                    Exit Function
                End If
                
                If (Left(node.Tag, 4) = "UON3") Or (Left(node.Tag, 4) = "UON4") Then
                    SeguridadDestinos = True
                    Exit Function
                End If

            Else
                SeguridadDestinos = True
            End If
        Else
            SeguridadDestinos = False
        End If
    Else
         SeguridadDestinos = True
    End If
End Function


Public Sub DeshacerBajaLog()
Dim nodx As node
Dim teserror As TipoErrorSummit


    
    teserror.NumError = TESnoerror
    
    Set nodx = tvwestrorg.selectedItem

    NodoSeleccionado nodx
    
    If nodx Is Nothing Then Exit Sub
    
    If Not oPersonaSeleccionada Is Nothing Then
    
        Accion = ACCPerBajaLog


        
        If teserror.NumError = TESnoerror Then
    
            teserror = oPersonaSeleccionada.DeshacerBajaLogica
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
            Else
                
                'Marca la persona como de no baja l�gica.
                If Not IsNull(oPersonaSeleccionada.codEqp) Then
                    nodx.Image = "PerCesta"
                Else
                    nodx.Image = "Persona"
                End If
                cmdBajaLog.Enabled = True
                cmdModif.Enabled = True
                
                If InStr(1, UCase(nodx.Parent.Image), "BAJA") > 0 Then
                    nodx.Parent.Image = "Departamento"
                    oPersonaSeleccionada.DeshacerBajaLogicaDep
                End If
                
            End If
            
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            oIBaseDatos.CancelarEdicion
            Set oIBaseDatos = Nothing
        End If
        
    End If
    
    If Not oUON1Seleccionada Is Nothing Then
    
        If teserror.NumError = TESnoerror Then
    
            teserror = oUON1Seleccionada.DeshacerBajaLogica
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
            Else
                Select Case Left(nodx.Image, 4)
                Case "UON1"
                    nodx.Image = "UON1"
                Case Else
                    nodx.Image = "SELLO"
                End Select
                
                cmdBajaLog.Enabled = True
                cmdModif.Enabled = True

                DescheckearArbol nodx
                
            End If
            
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            oIBaseDatos.CancelarEdicion
            Set oIBaseDatos = Nothing
        End If
    
    End If
    
    If Not oUON2Seleccionada Is Nothing Then
    
        If teserror.NumError = TESnoerror Then
            
            teserror = oUON2Seleccionada.DeshacerBajaLogica
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
            Else
                'Marca la persona como de no baja l�gica.

                Select Case Left(nodx.Image, 4)
                Case "UON2"
                    nodx.Image = "UON2"
                Case Else
                    nodx.Image = "SELLO"
                End Select
                
                cmdBajaLog.Enabled = True
                cmdModif.Enabled = True

                DescheckearArbol nodx
                
            End If
            
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            oIBaseDatos.CancelarEdicion
            Set oIBaseDatos = Nothing
        End If
    
    End If
    
    If Not oUON3Seleccionada Is Nothing Then
        'Accion = ACCPerBajaLog
        
        If teserror.NumError = TESnoerror Then
    
            teserror = oUON3Seleccionada.DeshacerBajaLogica
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
            Else
                
                'Marca la persona como de no baja l�gica.
                Select Case Left(nodx.Image, 4)
                Case "UON3"
                    nodx.Image = "UON3"
                Case Else
                    nodx.Image = "SELLO"
                End Select
                
                cmdBajaLog.Enabled = True
                cmdModif.Enabled = True

                DescheckearArbol nodx
                
            End If
            
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            oIBaseDatos.CancelarEdicion
            Set oIBaseDatos = Nothing
        End If
    End If
    
    If Not oUON4Seleccionada Is Nothing Then
        
        If teserror.NumError = TESnoerror Then
        
            teserror = oUON4Seleccionada.DeshacerBajaLogica
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
            Else
            
                Select Case Left(nodx.Image, 4)
                Case "UON4"
                    nodx.Image = "UON4"
                Case Else
                    nodx.Image = "SELLO"
                End Select
                
                cmdBajaLog.Enabled = True
                cmdModif.Enabled = True

                DescheckearArbol nodx
                
            End If
            
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            oIBaseDatos.CancelarEdicion
            Set oIBaseDatos = Nothing
        End If
    End If
    
    
End Sub


Private Sub EliminarUONDeEstructura()
    Dim nodSiguiente As MSComctlLib.node
    Dim nod As MSComctlLib.node

    Set nod = tvwestrorg.selectedItem

    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If
    
    tvwestrorg.Nodes.Remove nod.Index
    nodSiguiente.Selected = True
    ConfigurarInterfazSeguridad nodSiguiente
End Sub

Private Sub CheckearArbol(ByRef nodo As MSComctlLib.node)

Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim l As Integer
Dim nodetmp As MSComctlLib.node
Dim nodetmp2 As MSComctlLib.node
Dim nodetmp3 As MSComctlLib.node
Dim nodetmp4 As MSComctlLib.node



For i = 1 To nodo.Children
    If i = 1 Then
        Set nodetmp = Me.tvwestrorg.Nodes(nodo.Child.Index)
    Else
        Set nodetmp = Me.tvwestrorg.Nodes(nodetmp.Next.Index)
    End If
    Select Case Left(nodetmp.Tag, 4)
        Case "UON4"
            If nodetmp.Image = "UON4" Then
                   nodetmp.Image = "UON4BAJA"
            End If
        Case "UON3"
            If nodetmp.Image = "UON3" Then
                nodetmp.Image = "UON3BAJA"
            Else
                If nodetmp.Image = "SELLO" Then
                    nodetmp.Image = "SELLOBAJA"
                End If
            End If
        Case "UON2"
            If nodetmp.Image = "UON2" Then
                nodetmp.Image = "UON2BAJA"
            Else
                If nodetmp.Image = "SELLO" Then
                    nodetmp.Image = "SELLOBAJA"
                End If
            End If
            
        Case "UON1"
            If nodetmp.Image = "UON1" Then
                nodetmp.Image = "UON1BAJA"
            Else
                If nodetmp.Image = "SELLO" Then
                    nodetmp.Image = "SELLOBAJA"
                End If
            End If
        Case "DEP0", "DEP1", "DEP2", "DEP3"
            nodetmp.Image = "DepartamentoBaja"
    End Select

        For j = 1 To nodetmp.Children
            If j = 1 Then
                Set nodetmp2 = Me.tvwestrorg.Nodes(nodetmp.Child.Index)
            Else
                Set nodetmp2 = Me.tvwestrorg.Nodes(nodetmp2.Next.Index)
            End If
            Select Case Left(nodetmp2.Tag, 4)
                Case "UON4"
                    If nodetmp2.Image = "UON4" Then
                           nodetmp2.Image = "UON4BAJA"
                    End If
                Case "UON3"
                    If nodetmp2.Image = "UON3" Then
                        nodetmp2.Image = "UON3BAJA"
                    Else
                        If nodetmp2.Image = "SELLO" Then
                            nodetmp2.Image = "SELLOBAJA"
                        End If
                    End If
                Case "UON2"
                    If nodetmp2.Image = "UON2" Then
                        nodetmp2.Image = "UON2BAJA"
                    Else
                        If nodetmp2.Image = "SELLO" Then
                            nodetmp2.Image = "SELLOBAJA"
                        End If
                    End If
                    
                Case "UON1"
                    If nodetmp2.Image = "UON1" Then
                        nodetmp2.Image = "UON1BAJA"
                    Else
                        If nodetmp2.Image = "SELLO" Then
                            nodetmp2.Image = "SELLOBAJA"
                        End If
                    End If
                Case "DEP0", "DEP1", "DEP2", "DEP3"
                    nodetmp2.Image = "DepartamentoBaja"
            
            End Select

            For k = 1 To nodetmp2.Children
                If k = 1 Then
                    Set nodetmp3 = Me.tvwestrorg.Nodes(nodetmp2.Child.Index)
                Else
                    Set nodetmp3 = Me.tvwestrorg.Nodes(nodetmp3.Next.Index)
                End If
                Select Case Left(nodetmp3.Tag, 4)
                    Case "UON4"
                        If nodetmp3.Image = "UON4" Then
                               nodetmp3.Image = "UON4BAJA"
                        End If
                    Case "UON3"
                        If nodetmp3.Image = "UON3" Then
                            nodetmp3.Image = "UON3BAJA"
                        Else
                            If nodetmp3.Image = "SELLO" Then
                                nodetmp3.Image = "SELLOBAJA"
                            End If
                        End If
                    Case "UON2"
                        If nodetmp3.Image = "UON2" Then
                            nodetmp3.Image = "UON2BAJA"
                        Else
                            If nodetmp3.Image = "SELLO" Then
                                nodetmp3.Image = "SELLOBAJA"
                            End If
                        End If
                        
                    Case "UON1"
                        If nodetmp3.Image = "UON1" Then
                            nodetmp3.Image = "UON1BAJA"
                        Else
                            If nodetmp3.Image = "SELLO" Then
                                nodetmp3.Image = "SELLOBAJA"
                            End If
                        End If
                    Case "DEP0", "DEP1", "DEP2", "DEP3"
                        nodetmp3.Image = "DepartamentoBaja"
                
                End Select

                For l = 1 To nodetmp3.Children
                    If l = 1 Then
                        Set nodetmp4 = Me.tvwestrorg.Nodes(nodetmp3.Child.Index)
                    Else
                        Set nodetmp4 = Me.tvwestrorg.Nodes(nodetmp4.Next.Index)
                    End If
                    Select Case Left(nodetmp4.Tag, 4)
                        Case "UON4"
                            If nodetmp4.Image = "UON4" Then
                                   nodetmp4.Image = "UON4BAJA"
                            End If
                        Case "UON3"
                            If nodetmp4.Image = "UON3" Then
                                nodetmp4.Image = "UON3BAJA"
                            Else
                                If nodetmp4.Image = "SELLO" Then
                                    nodetmp4.Image = "SELLOBAJA"
                                End If
                            End If
                        Case "UON2"
                            If nodetmp4.Image = "UON2" Then
                                nodetmp4.Image = "UON2BAJA"
                            Else
                                nodetmp4.Image = "SELLOBAJA"
                            End If
                            
                        Case "UON1"
                            If nodetmp4.Image = "UON1" Then
                                nodetmp4.Image = "UON1BAJA"
                            Else
                                If nodetmp4.Image = "SELLO" Then
                                    nodetmp4.Image = "SELLOBAJA"
                                End If
                            End If
                        Case "DEP0", "DEP1", "DEP2", "DEP3"
                            nodetmp4.Image = "DepartamentoBaja"
                    End Select

                Next l

            Next k

        Next j
Next i

End Sub

Private Sub DescheckearArbol(nodo As MSComctlLib.node)
    Dim i As Integer
    Dim nodetmp  As MSComctlLib.node
    
    For i = 1 To nodo.Children
        If i = 1 Then
            Set nodetmp = Me.tvwestrorg.Nodes(nodo.Child.Index)
        Else
            Set nodetmp = Me.tvwestrorg.Nodes(nodetmp.Next.Index)
        End If
        Select Case Left(nodetmp.Tag, 4)
            Case "DEP0", "DEP1", "DEP2", "DEP3"
                nodetmp.Image = "Departamento"
        End Select
    Next i
End Sub

Public Sub mnuPopUpAnyadirCC()
    Accion = ACCUON4Anya
    Set frmESTRORGDetalle.m_oUnidadesOrgN3 = oUON3Seleccionada
    frmESTRORGDetalle.mCodUON3 = oUON3Seleccionada.Cod
    frmESTRORGDetalle.caption = m_sIdiA�adir & " " & gParametrosGenerales.gsDEN_UON4
    frmESTRORGDetalle.Show 1
End Sub


''' <summary>
''' Procedimiento que carga el formulario de gesti�n del centro de coste (de los nodos presupuestarios vinculados a una Unidad Organizativa concreta) frmESTRORGCCoste
''' Previamente comprueba que la Unidad Organizativa seleccionada o alguna de sus unidades padre poseen una Empresa asociada. Caso de no poseer alguno empresa, no se permite la gesti�n del CC
''' </summary>
''' <remarks>Llamada desde MDI.mnuPopUpEstrOrgNiv1_Click, MDI.mnuPopUpEstrOrgNiv2_Click, MDI.mnuPopUpEstrOrgNiv3_Click, MDI.mnuPopUpEstrOrgNiv4_Click; Tiempo m�ximo < 0,01 seg.</remarks>
Public Sub GestionarCCoste()

    Dim nodx As node
    Dim oEmpresaSeleccionadaUON1 As CEmpresa
    Dim oEmpresaSeleccionadaUON2 As CEmpresa
    Dim oEmpresaSeleccionadaUON3 As CEmpresa
    Dim oEmpresaSeleccionadaUON4 As CEmpresa
     
    'Inicializamos
    Set oEmpresaSeleccionadaUON1 = Nothing
    Set oEmpresaSeleccionadaUON2 = Nothing
    Set oEmpresaSeleccionadaUON3 = Nothing
    Set oEmpresaSeleccionadaUON4 = Nothing
    
    Set nodx = tvwestrorg.selectedItem
    
    Select Case Left(nodx.Tag, 4)
        
        Case "UON1"
            If oUON1Seleccionada Is Nothing Then
                Set oUON1Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel1
                oUON1Seleccionada.Cod = DevolverCod(nodx)
                oUON1Seleccionada.Den = DevolverDen(nodx)
            End If
                
            Set oEmpresaSeleccionadaUON1 = frmESTRORG.oUON1Seleccionada.CargarEmpresa
                
        Case "UON2"
                
            If oUON2Seleccionada Is Nothing Then
                Set oUON2Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel2
                oUON2Seleccionada.Cod = DevolverCod(nodx)
                oUON2Seleccionada.Den = DevolverDen(nodx)
            End If
            If oUON1Seleccionada Is Nothing Then
                Set oUON1Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel1
                oUON1Seleccionada.Cod = DevolverCod(nodx.Parent)
                oUON1Seleccionada.Den = DevolverDen(nodx.Parent)
            End If
            oUON2Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent)
        
            Set oEmpresaSeleccionadaUON2 = frmESTRORG.oUON2Seleccionada.CargarEmpresa
            Set oEmpresaSeleccionadaUON1 = frmESTRORG.oUON1Seleccionada.CargarEmpresa
                
        Case "UON3"
                
            If oUON3Seleccionada Is Nothing Then
                Set oUON3Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel3
                oUON3Seleccionada.Cod = DevolverCod(nodx)
                oUON3Seleccionada.Den = DevolverDen(nodx)
            End If
            oUON3Seleccionada.CodUnidadOrgNivel2 = DevolverCod(nodx.Parent)
            oUON3Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent.Parent)
            If oUON2Seleccionada Is Nothing Then
                Set oUON2Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel2
                oUON2Seleccionada.Cod = DevolverCod(nodx.Parent)
                oUON2Seleccionada.Den = DevolverDen(nodx.Parent)
            End If
            oUON2Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent.Parent)
            If oUON1Seleccionada Is Nothing Then
                Set oUON1Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel1
                oUON1Seleccionada.Cod = DevolverCod(nodx.Parent.Parent)
                oUON1Seleccionada.Den = DevolverDen(nodx.Parent.Parent)
            End If
        
            Set oEmpresaSeleccionadaUON3 = frmESTRORG.oUON3Seleccionada.CargarEmpresa
            Set oEmpresaSeleccionadaUON2 = frmESTRORG.oUON2Seleccionada.CargarEmpresa
            Set oEmpresaSeleccionadaUON1 = frmESTRORG.oUON1Seleccionada.CargarEmpresa
        
        Case "UON4"
                
            If oUON4Seleccionada Is Nothing Then
                Set oUON4Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel4
                oUON4Seleccionada.Cod = DevolverCod(nodx)
                oUON4Seleccionada.Den = DevolverDen(nodx)
            End If
            oUON4Seleccionada.CodUnidadOrgNivel3 = DevolverCod(nodx.Parent)
            oUON4Seleccionada.CodUnidadOrgNivel2 = DevolverCod(nodx.Parent.Parent)
            oUON4Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent.Parent.Parent)
            If oUON3Seleccionada Is Nothing Then
                Set oUON3Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel3
                oUON3Seleccionada.Cod = DevolverCod(nodx.Parent)
                oUON3Seleccionada.Den = DevolverDen(nodx.Parent)
            End If
            oUON3Seleccionada.CodUnidadOrgNivel2 = DevolverCod(nodx.Parent.Parent)
            oUON3Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent.Parent.Parent)
            If oUON2Seleccionada Is Nothing Then
                Set oUON2Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel2
                oUON2Seleccionada.Cod = DevolverCod(nodx.Parent.Parent)
                oUON2Seleccionada.Den = DevolverDen(nodx.Parent.Parent)
            End If
            oUON2Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent.Parent.Parent)
            If oUON1Seleccionada Is Nothing Then
                Set oUON1Seleccionada = oFSGSRaiz.generar_CUnidadOrgNivel1
                oUON1Seleccionada.Cod = DevolverCod(nodx.Parent.Parent.Parent)
                oUON1Seleccionada.Den = DevolverDen(nodx.Parent.Parent.Parent)
            End If
        
            Set oEmpresaSeleccionadaUON4 = frmESTRORG.oUON4Seleccionada.CargarEmpresa
            Set oEmpresaSeleccionadaUON3 = frmESTRORG.oUON3Seleccionada.CargarEmpresa
            Set oEmpresaSeleccionadaUON2 = frmESTRORG.oUON2Seleccionada.CargarEmpresa
            Set oEmpresaSeleccionadaUON1 = frmESTRORG.oUON1Seleccionada.CargarEmpresa
                
    End Select
    
    'Si la UO seleccionada, o alguna de la unidades de las que cuelgue,
    'tienen empresa asociada, entonces mostramos el formulario de nodos
    If Not oEmpresaSeleccionadaUON1 Is Nothing Or Not oEmpresaSeleccionadaUON2 Is Nothing Or Not oEmpresaSeleccionadaUON3 Is Nothing Or Not oEmpresaSeleccionadaUON4 Is Nothing Then
        
        frmESTRORGCCoste.Show 1
    
    Else
        
        MsgBox (m_sNoHayEmpresaAsociada)
    End If
End Sub



