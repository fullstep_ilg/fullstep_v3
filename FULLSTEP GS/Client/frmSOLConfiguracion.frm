VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Object = "{14ACBB92-9C4A-4C45-AFD2-7AE60E71E5B3}#4.0#0"; "IGSplitter40.ocx"
Begin VB.Form frmSOLConfiguracion 
   Caption         =   "16"
   ClientHeight    =   5805
   ClientLeft      =   495
   ClientTop       =   3855
   ClientWidth     =   16350
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLConfiguracion.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5805
   ScaleWidth      =   16350
   Begin VB.PictureBox picCmdsCarpetas 
      BorderStyle     =   0  'None
      Height          =   435
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   5895
      TabIndex        =   11
      Top             =   5400
      Width           =   5895
      Begin VB.CommandButton cmdModificarCarpeta 
         Caption         =   "Modificar"
         Height          =   345
         Left            =   1200
         TabIndex        =   18
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "Listado"
         Height          =   345
         Left            =   4695
         TabIndex        =   15
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdActualizar 
         Caption         =   "Actualizar"
         Height          =   345
         Left            =   3530
         TabIndex        =   14
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdEliminarCarpeta 
         Caption         =   "Eliminar"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2365
         TabIndex        =   13
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdAnyadirCarpeta 
         Caption         =   "A�adir"
         Height          =   345
         Left            =   45
         TabIndex        =   12
         Top             =   0
         Width           =   1095
      End
   End
   Begin SSSplitter.SSSplitter SSSplitter1 
      Height          =   5295
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   9340
      _Version        =   262144
      PaneTree        =   "frmSOLConfiguracion.frx":0CB2
      Begin VB.PictureBox picSolicitudes 
         BorderStyle     =   0  'None
         Height          =   5235
         Left            =   4320
         ScaleHeight     =   5235
         ScaleWidth      =   10740
         TabIndex        =   1
         Top             =   30
         Width           =   10740
         Begin VB.PictureBox picEdicion 
            BorderStyle     =   0  'None
            Height          =   410
            Left            =   120
            ScaleHeight     =   405
            ScaleWidth      =   4995
            TabIndex        =   8
            Top             =   4320
            Visible         =   0   'False
            Width           =   5000
            Begin VB.CommandButton cmdDeshacer 
               Caption         =   "&Deshacer"
               Height          =   345
               Left            =   1200
               TabIndex        =   10
               Top             =   60
               Width           =   1005
            End
            Begin VB.CommandButton cmdAceptar 
               Caption         =   "&Aceptar"
               Height          =   345
               Left            =   90
               TabIndex        =   9
               Top             =   60
               Width           =   1005
            End
         End
         Begin VB.PictureBox picNavigate 
            BorderStyle     =   0  'None
            Height          =   435
            Left            =   0
            ScaleHeight     =   435
            ScaleWidth      =   10785
            TabIndex        =   3
            Top             =   4800
            Width           =   10785
            Begin VB.CommandButton cmdCopiar 
               Caption         =   "Copiar"
               Height          =   345
               Left            =   8520
               TabIndex        =   20
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdMover 
               Caption         =   "Mover"
               Height          =   345
               Left            =   7440
               TabIndex        =   17
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdRestaurar 
               Caption         =   "Restaurar"
               Height          =   345
               Left            =   6360
               TabIndex        =   19
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdEliminar 
               Caption         =   "Eliminar"
               Enabled         =   0   'False
               Height          =   345
               Left            =   5280
               TabIndex        =   7
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdAnyadirPet 
               Caption         =   "A�adir peticionario"
               Height          =   345
               Left            =   1965
               TabIndex        =   5
               Top             =   0
               Width           =   1600
            End
            Begin VB.CommandButton cmdAnyadirNotificado 
               Caption         =   "A�adir notificado"
               Height          =   345
               Left            =   3635
               TabIndex        =   6
               Top             =   0
               Width           =   1600
            End
            Begin VB.CommandButton cmdAnyadirSol 
               Caption         =   "A�adir solicitud"
               Height          =   345
               Left            =   45
               TabIndex        =   4
               Top             =   0
               Width           =   1850
            End
         End
         Begin UltraGrid.SSUltraGrid ssSOLConfig 
            Height          =   4755
            Left            =   0
            TabIndex        =   2
            Top             =   0
            Width           =   8565
            _ExtentX        =   15108
            _ExtentY        =   8387
            _Version        =   131072
            GridFlags       =   17040384
            Images          =   "frmSOLConfiguracion.frx":0D04
            LayoutFlags     =   67108864
            Caption         =   "ssSOLConfig"
         End
      End
      Begin MSComctlLib.TreeView tvwCarpetasSolicit 
         Height          =   5235
         Left            =   30
         TabIndex        =   16
         Top             =   30
         Width           =   4200
         _ExtentX        =   7408
         _ExtentY        =   9234
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6240
      Top             =   5280
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLConfiguracion.frx":0DAC
            Key             =   "CSN4"
            Object.Tag             =   "CSN4"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLConfiguracion.frx":0E5C
            Key             =   "CSN0"
            Object.Tag             =   "CSN0"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLConfiguracion.frx":12AE
            Key             =   "CSN1"
            Object.Tag             =   "CSN1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLConfiguracion.frx":135E
            Key             =   "CSN2"
            Object.Tag             =   "CSN2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLConfiguracion.frx":140E
            Key             =   "CSN3"
            Object.Tag             =   "CSN3"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSOLConfiguracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_oSolicitudSeleccionada As CSolicitud
Public g_bModifSolic As Boolean
Public g_bCancel As Boolean
Public g_udtAccion As AccionesSummit

'Variables privadas
Private m_oIBaseDatos As IBaseDatos
Private m_oSolicitudes As CSolicitudes
Private m_oIdiomas As CIdiomas

Private m_bModifPetic As Boolean
Private m_bRAltaPetUON As Boolean
Private m_bRAltaPetDep As Boolean
Private m_bModifFlujo As Boolean

Public m_bRespetar As Boolean

Private m_oCarpetasSolicitN1 As CCarpetasSolicitN1
Private m_sRaizCarpetas As String
Public m_TipoSolicitudNoEsFactura As Integer 'Incidencia Pruebas 31900.7 / 2013 / 79 permite cambiar una solicitud de un tipo distinto a una solicitud de factura
Public m_TipoSolicitudNoEsPedido As Integer
Public m_ContadorTipoSolicitudNoEsFactura As Integer 'Incidencia Pruebas 31900.7 / 2013 / 79 permite cambiar una solicitud de un tipo distinto a una solicitud de factura
Public m_ContadorTipoSolicitudNoEsPedido As Integer

'*********************************************************************
'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
'*********************************************************************
Public m_TipoContactoSolicitado As Integer '1 - Peticionario , 2 - Notificado ATENCI�N: SE UTILIZA PARA DIRECCIONAR EL BRANDS DE ssSOLConfig
'*********************************************************************

'IDIOMAS
Private m_sITipoSolCom As String
Private m_sITipoSolOtras As String
Private m_sICodigo As String
Private m_sIConfiguracion As String
Private m_sISolicitudes As String
Private m_sITipo As String
Private m_sIDen As String
Private m_sIGestor As String
Private m_sIDescr As String
Private m_sIArchivos As String
Private m_sIForm As String
Private m_sIWork As String
Private m_sIWorkModificacion As String
Private m_sIWorkBaja As String
Private m_sIPub As String
Private m_sIPedDir As String
Private m_sIPeticionarios As String
Private m_sINotificados As String
Private m_sIUON As String
Private m_sIDep As String
Private m_sIPer As String
Private m_sISolicitud As String
Private m_sIPeticionario As String
Private m_sINotificado As String
Private m_sIEstados As String
Private m_slaCarpeta As String
Private m_sIContratos As String
Private m_sIEmpresas As String
Private m_sIConfiguracionCertificado As String
Private m_sFormAsociadoNovalido As String
Private m_sDebeExistirMoneda As String

Public Sub cmdAceptar_Click()
    Dim vCancel As SSReturnBoolean

    If g_udtAccion = ACCSolConfAnyadir Or g_udtAccion = AccionesSummit.ACCSolConfAnyaPet Or g_udtAccion = AccionesSummit.ACCSolConfAnyaNotif Then
        ssSOLConfig.PerformAction ssKeyActionDeactivateCell
        ssSOLConfig_BeforeRowDeactivate vCancel
    Else
        ssSOLConfig.Update
    End If
End Sub

Private Sub cmdActualizar_Click()
    GenerarEstructuraCarpetas Me.tvwCarpetasSolicit, False, False
    'Carga la grid con las solicitudes:
    CargarGridSolicitudes
End Sub


Private Sub cmdAnyadirCarpeta_Click()
    Dim nodx As MSComctlLib.node
    Dim teserror As TipoErrorSummit
    Dim oCarpeta As Object
    Dim oNewCarpeta As Object
    Dim oIBaseDatos As IBaseDatos
    Dim sDen As String
    
    
    Set nodx = tvwCarpetasSolicit.selectedItem

    If Not nodx Is Nothing Then
        Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
        Dim iNivelCarpeta As Integer
        If Not oCarpeta Is Nothing Then
            iNivelCarpeta = oCarpeta.Nivel
        Else
            iNivelCarpeta = 0 'Est� seleccionada la Raiz del Arbol
        End If
        If iNivelCarpeta < 4 Then
            sDen = MostrarFormCarpetaSolicitDetalle(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, Me.Left + (Me.Width / 2), Me.Top + (Me.Height / 2), "")
            
            If sDen <> "" Then
                Select Case iNivelCarpeta
                    Case 0 'Hijo de la Raiz
                        Set oNewCarpeta = oFSGSRaiz.Generar_CCarpetaSolicitN1
                        oNewCarpeta.Den = sDen
                    Case 1
                        Set oNewCarpeta = oFSGSRaiz.Generar_CCarpetaSolicitN2
                        oNewCarpeta.Den = sDen
                        oNewCarpeta.CSN1 = oCarpeta.Id
                    Case 2
                        Set oNewCarpeta = oFSGSRaiz.Generar_CCarpetaSolicitN3
                        oNewCarpeta.Den = sDen
                        oNewCarpeta.CSN1 = oCarpeta.CSN1
                        oNewCarpeta.CSN2 = oCarpeta.Id
                    Case 3
                        Set oNewCarpeta = oFSGSRaiz.Generar_CCarpetaSolicitN4
                        oNewCarpeta.Den = sDen
                        oNewCarpeta.CSN1 = oCarpeta.CSN1
                        oNewCarpeta.CSN2 = oCarpeta.CSN2
                        oNewCarpeta.CSN3 = oCarpeta.Id
                End Select
                
                Screen.MousePointer = vbHourglass
                Set oIBaseDatos = oNewCarpeta
                teserror = oIBaseDatos.AnyadirABaseDatos
         
                
                If teserror.NumError = TESnoerror Then
                    Screen.MousePointer = vbHourglass
                    LockWindowUpdate Me.hWnd

                    GenerarEstructuraCarpetas Me.tvwCarpetasSolicit, False, False
                    Set ssSOLConfig.DataSource = Nothing
                    CargarGridSolicitudes
                    ssSOLConfig.CollapseAll

                    CrearValueList
                    LockWindowUpdate 0&
                    Screen.MousePointer = vbNormal
                    
                    Set nodx = tvwCarpetasSolicit.Nodes(nodx.key)
                    nodx.Selected = True
                    nodx.Expanded = True
            
                    tvwCarpetasSolicit_NodeClick nodx
                Else
                    TratarError teserror
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                Screen.MousePointer = vbNormal
            End If
        Else
            oMensajes.Solo4NivelesCarpetasSolicit
        End If
    End If
End Sub

Private Sub cmdCopiar_Click()
    'se copia la fila seleccionada
    Dim irespuesta As Long
    Dim idNuevoFormulario As Long
    Dim rsSolicitud As New ADODB.Recordset
    Dim rsPeticionarios As New ADODB.Recordset
    Dim tsError As TipoErrorSummit
    Dim blnCopiarFormulario As Boolean
    Dim m_oForm As CFormulario
    Dim nodx As node
    Dim oCarpeta As Object
    
    If Not ssSOLConfig.ActiveRow Is Nothing Then
        Set nodx = tvwCarpetasSolicit.selectedItem
        If ((ssSOLConfig.ActiveRow.Band.key <> "PET") And (ssSOLConfig.ActiveRow.Band.key <> "NOTIF")) Then
            If Not nodx Is Nothing Then
                g_udtAccion = ACCSolConfCopiar
                SolicitudSeleccionada
                
                irespuesta = oMensajes.PreguntaCopiarFormulario()
                
                If irespuesta = vbYes Then
                    blnCopiarFormulario = True
                    
                    Set m_oForm = oFSGSRaiz.Generar_CFormulario
                    m_oForm.Id = g_oSolicitudSeleccionada.Formulario.Id
                    m_oForm.CargarTodosLosGrupos
                    tsError = m_oForm.CopiarFormulario
                    
                    If tsError.NumError <> TESnoerror Then
                        TratarError tsError
                        Exit Sub
                    End If
                
                    idNuevoFormulario = m_oForm.ObtenerIDFormulario()
                Else
                    idNuevoFormulario = 0
                End If
                
                Dim idNuevaSol As Long
                
                tsError = g_oSolicitudSeleccionada.CopiaSolicitud(ssSOLConfig.ActiveRow.Cells("ID_SOL").Value, idNuevaSol, ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value, idNuevoFormulario, oFSGSRaiz)
                
                If tsError.NumError <> TESnoerror Then
                    TratarError tsError
                Else
                    Dim expandedNodes As Collection
                    Set expandedNodes = New Collection
                    Dim aRow As SSRow
                                       
                    Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
                    If oCarpeta Is Nothing Then
                        Set ssSOLConfig.DataSource = m_oSolicitudes.DevolverSolicitudes
                    Else
                        Set ssSOLConfig.DataSource = m_oSolicitudes.DevolverSolicitudes(oCarpeta.Nivel, oCarpeta.Id)
                    End If
                    
                    Set aRow = ssSOLConfig.GetRow(ssChildRowFirst)
                    If aRow.Expanded Then
                           expandedNodes.Add (aRow.Cells("ID").Value)
                    End If
                    Do
                        Set aRow = aRow.GetSibling(ssSiblingRowNext)
                        If aRow.Expanded Then
                            expandedNodes.Add (aRow.Cells("ID").Value)
                        End If
                    Loop Until Not aRow.HasNextSibling
                    Dim Index As Integer
                    Set aRow = ssSOLConfig.GetRow(ssChildRowFirst)
                    Do
                        If expandedNodes.Count > 0 Then
                            For Index = 1 To expandedNodes.Count
                                If expandedNodes.Item(Index) = aRow.Cells("ID").Value Then
                                    aRow.ExpandAll
                                End If
                            Next
                        End If
                        If aRow.Cells("ID").Value = idNuevaSol Then
                            ssSOLConfig.Selected.Rows.Add aRow
                            ssSOLConfig.ActiveRow = aRow
                            ssSOLConfig.ActiveRow.ExpandAll
                        End If
                        Set aRow = aRow.GetSibling(ssSiblingRowNext)
                    Loop Until Not aRow.HasNextSibling

                    If expandedNodes.Count > 0 Then
                        For Index = 1 To expandedNodes.Count
                            If expandedNodes.Item(Index) = aRow.Cells("ID").Value Then
                                aRow.ExpandAll
                            End If
                        Next
                    End If
                    If aRow.Cells("ID").Value = idNuevaSol Then
                        ssSOLConfig.Selected.Rows.Add aRow
                        ssSOLConfig.ActiveRow = aRow
                        ssSOLConfig.ActiveRow.ExpandAll
                    End If
                    
                    Set rsPeticionarios = Nothing
                    Set rsSolicitud = Nothing
                                       
                    g_udtAccion = ACCSolConfConsultar
                    
                    ssSOLConfig.PerformAction UltraGrid.ssKeyActionEnterEditMode
                    ssSOLConfig.ActiveRow.Refresh ssRefreshDisplay
                    
                    picNavigate.Visible = True
                    picEdicion.Visible = False
                    Screen.MousePointer = vbNormal
                    
                    SolicitudSeleccionada
                End If
            End If
        End If
    End If
End Sub
Private Sub cmdEliminarCarpeta_Click()
    Dim nodx As MSComctlLib.node
    Dim nodPadre As MSComctlLib.node
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim oCarpeta As Object
    Dim oIBaseDatos As IBaseDatos
    
    Set nodx = tvwCarpetasSolicit.selectedItem

    If Not nodx Is Nothing Then
         Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
         If Not oCarpeta Is Nothing Then
            irespuesta = oMensajes.PreguntaEliminar(" " & m_slaCarpeta & " " & oCarpeta.Den)
            If irespuesta = vbNo Then Exit Sub
             
            Screen.MousePointer = vbHourglass
            Set oIBaseDatos = oCarpeta
            teserror = oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            Set oCarpeta = Nothing
            Set oIBaseDatos = Nothing
            
            Set nodPadre = tvwCarpetasSolicit.Nodes(nodx.key).Parent
            
            tvwCarpetasSolicit.Nodes.Remove (nodx.key)
            Screen.MousePointer = vbNormal
            
            nodPadre.Selected = True
            
            tvwCarpetasSolicit_NodeClick nodPadre
                        
         Else
            oMensajes.SeleccionaCarpetaSolicit
        End If
    End If
End Sub

Private Sub cmdModificarCarpeta_Click()
    Dim nodx As MSComctlLib.node
    Dim teserror As TipoErrorSummit
    Dim oCarpeta As Object
    Dim oIBaseDatos As IBaseDatos
    Dim sDen As String
        
    Set nodx = tvwCarpetasSolicit.selectedItem

    If Not nodx Is Nothing Then
         Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
         If Not oCarpeta Is Nothing Then
            sDen = MostrarFormCarpetaSolicitDetalle(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, Me.Left + (Me.Width / 2), Me.Top + (Me.Height / 2), oCarpeta.Den)
            
            If sDen <> "" Then
                oCarpeta.Den = sDen
                
                Screen.MousePointer = vbHourglass
                Set oIBaseDatos = oCarpeta
                teserror = oIBaseDatos.FinalizarEdicionModificando
                         
                If teserror.NumError = TESnoerror Then
                    Screen.MousePointer = vbHourglass
                    LockWindowUpdate Me.hWnd

                    GenerarEstructuraCarpetas Me.tvwCarpetasSolicit, False, False
                    Set ssSOLConfig.DataSource = Nothing
                    CargarGridSolicitudes
                    ssSOLConfig.CollapseAll

                    CrearValueList
                    LockWindowUpdate 0&
                    Screen.MousePointer = vbNormal
                    
                    Set nodx = tvwCarpetasSolicit.Nodes(nodx.key)
                    nodx.Selected = True
            
                    tvwCarpetasSolicit_NodeClick nodx
                Else
                    TratarError teserror
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                Screen.MousePointer = vbNormal
            End If
        Else
            oMensajes.SeleccionaCarpetaSolicit
        End If
        
     End If
End Sub

Private Sub cmdMover_Click()
    Dim oNodoSeleccionado As MSComctlLib.node
    Dim oCarpetaSeleccionada As Object
    Dim teserror As TipoErrorSummit
        
    If ssSOLConfig.ActiveRow Is Nothing Then Exit Sub
    If m_bRespetar Then Exit Sub
    
    
    Select Case ssSOLConfig.ActiveRow.Band.Index

        Case 0
            frmCarpetaSolicitArbol.Show vbModal
            Set oNodoSeleccionado = frmCarpetaSolicitArbol.oNodoSeleccionado
            Set frmCarpetaSolicitArbol.oNodoSeleccionado = Nothing
            Set oCarpetaSeleccionada = frmCarpetaSolicitArbol.oCarpetaSeleccionada
            Set frmCarpetaSolicitArbol.oCarpetaSeleccionada = Nothing
            Unload frmCarpetaSolicitArbol
                            
            If oNodoSeleccionado Is Nothing Then Exit Sub
                                
            Screen.MousePointer = vbHourglass
                    
            Set g_oSolicitudSeleccionada = Nothing
            Set g_oSolicitudSeleccionada = oFSGSRaiz.Generar_CSolicitud
            g_oSolicitudSeleccionada.Id = ssSOLConfig.ActiveRow.Cells("ID_SOL").Value
            g_oSolicitudSeleccionada.FECACT = CDate(ssSOLConfig.ActiveRow.Cells("FECACT").Value)
            g_oSolicitudSeleccionada.Publicado = ssSOLConfig.ActiveRow.Cells("PUB").Value
            
            Set m_oIBaseDatos = g_oSolicitudSeleccionada
            teserror = m_oIBaseDatos.IniciarEdicion
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set m_oIBaseDatos = Nothing
                Set g_oSolicitudSeleccionada = Nothing
                Exit Sub
            Else
                If Not oCarpetaSeleccionada Is Nothing Then
                    Select Case oCarpetaSeleccionada.Nivel
                            Case 1
                                g_oSolicitudSeleccionada.CSN1 = oCarpetaSeleccionada.Id
                                g_oSolicitudSeleccionada.CSN2 = 0
                                g_oSolicitudSeleccionada.CSN3 = 0
                                g_oSolicitudSeleccionada.CSN4 = 0
                            Case 2
                                g_oSolicitudSeleccionada.CSN1 = 0
                                g_oSolicitudSeleccionada.CSN2 = oCarpetaSeleccionada.Id
                                g_oSolicitudSeleccionada.CSN3 = 0
                                g_oSolicitudSeleccionada.CSN4 = 0
                            Case 3
                                g_oSolicitudSeleccionada.CSN1 = 0
                                g_oSolicitudSeleccionada.CSN2 = 0
                                g_oSolicitudSeleccionada.CSN3 = oCarpetaSeleccionada.Id
                                g_oSolicitudSeleccionada.CSN4 = 0
                            Case 4
                                g_oSolicitudSeleccionada.CSN1 = 0
                                g_oSolicitudSeleccionada.CSN2 = 0
                                g_oSolicitudSeleccionada.CSN3 = 0
                                g_oSolicitudSeleccionada.CSN4 = oCarpetaSeleccionada.Id
                        End Select
                Else
                    g_oSolicitudSeleccionada.CSN1 = 0
                    g_oSolicitudSeleccionada.CSN2 = 0
                    g_oSolicitudSeleccionada.CSN3 = 0
                    g_oSolicitudSeleccionada.CSN4 = 0
                End If
                                                           
                teserror = m_oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    'RegistrarAccion g_udtAccion, "ID:" & g_oSolicitudSeleccionada.Id
                    m_bRespetar = True
                    ssSOLConfig.ActiveRow.Cells("FECACT").Value = g_oSolicitudSeleccionada.FECACT
                    m_bRespetar = False
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Set m_oIBaseDatos = Nothing
                    Set g_oSolicitudSeleccionada = Nothing
                    Exit Sub
                End If
            End If

        Case 1 'Peticionario
        Case 2 'Notificado
    
    End Select

    Screen.MousePointer = vbNormal
    CargarGridSolicitudes
End Sub


Private Sub cmdAnyadirSol_Click()
    Dim nodx As node
'    Dim oCarpeta As Object
    
    Set g_oSolicitudSeleccionada = Nothing
    
    If Not ssSOLConfig.ActiveRow Is Nothing Then
        ssSOLConfig.ActiveRow.Update
    End If
    
    Set nodx = tvwCarpetasSolicit.selectedItem

    If Not nodx Is Nothing Then
        
        g_udtAccion = AccionesSummit.ACCSolConfAnyadir
        
        'a�ade una nueva fila a la grid:
        ssSOLConfig.Bands(0).AddNew
    
        'Pone el foco en el campo TIPO de la nueva fila:
        If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
            ssSOLConfig.ActiveRow.Cells("COD").Column.Activation = UltraGrid.ssActivationAllowEdit
            ssSOLConfig.Selected.Cells.clear
            Set ssSOLConfig.ActiveCell = ssSOLConfig.ActiveRow.Cells("COD")
        Else
            ssSOLConfig.ActiveRow.Cells("TIPO").Column.Activation = UltraGrid.ssActivationAllowEdit
            ssSOLConfig.Selected.Cells.clear
            Set ssSOLConfig.ActiveCell = ssSOLConfig.ActiveRow.Cells("TIPO")
        End If
        
        If Me.Visible Then ssSOLConfig.SetFocus
        ssSOLConfig.PerformAction UltraGrid.ssKeyActionEnterEditMode
    
        picNavigate.Visible = False
        picEdicion.Visible = True
    End If

End Sub

Private Sub cmdAnyadirPet_Click()

    If Me.ssSOLConfig.ActiveRow Is Nothing Then Exit Sub
    Me.ssSOLConfig.ActiveRow.Update
    m_TipoContactoSolicitado = 1
    
    frmSOLSelPersona.g_sOrigen = "frmSOLConfiguracion"
    frmSOLSelPersona.bRDep = m_bRAltaPetDep
    frmSOLSelPersona.bRUO = m_bRAltaPetUON
    frmSOLSelPersona.Show vbModal
End Sub
'*********************************************************************
'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
'*********************************************************************
Private Sub cmdAnyadirNotificado_Click()
    If Me.ssSOLConfig.ActiveRow Is Nothing Then Exit Sub
    Me.ssSOLConfig.ActiveRow.Update
    m_TipoContactoSolicitado = 2
    
    frmSOLSelPersona.g_sOrigen = "frmSOLConfiguracion"
    frmSOLSelPersona.bRDep = m_bRAltaPetDep
    frmSOLSelPersona.bRUO = m_bRAltaPetUON
    frmSOLSelPersona.Show vbModal
End Sub
'*********************************************************************

Private Sub cmdDeshacer_Click()
    'SendKeys "{ESC}", True
    
    If g_udtAccion = AccionesSummit.ACCSolConfAnyadir Or g_udtAccion = AccionesSummit.ACCSolConfAnyaPet Or g_udtAccion = AccionesSummit.ACCSolConfAnyaNotif Or g_udtAccion = AccionesSummit.ACCSolConfCopiar Then
        ssSOLConfig.ActiveRow.Delete
    End If
    
    g_udtAccion = ACCSolConfConsultar
    
    picNavigate.Visible = True
    picEdicion.Visible = False
End Sub

''' <summary>
''' Elimina una solicitud o un peticionario
''' </summary>
''' <remarks>Llamada desde=Click del boton Eliminar; Tiempo m�ximo</remarks>
Private Sub cmdEliminar_Click()
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim oPeticionario As CPeticionario
    Dim bFavoritos As Boolean
    Dim TextoError As String

    If Not ssSOLConfig.ActiveRow Is Nothing Then
                
        Select Case ssSOLConfig.ActiveRow.Band.Index
            
            Case 0  'Solicitud
                 
                g_udtAccion = ACCSolConfEliminar
                
                Set g_oSolicitudSeleccionada = Nothing
                Set g_oSolicitudSeleccionada = oFSGSRaiz.Generar_CSolicitud
                g_oSolicitudSeleccionada.Id = ssSOLConfig.ActiveRow.Cells("ID_SOL").Value
                g_oSolicitudSeleccionada.FECACT = CDate(ssSOLConfig.ActiveRow.Cells("FECACT").Value)
                Set m_oIBaseDatos = g_oSolicitudSeleccionada
                
                Screen.MousePointer = vbHourglass
                teserror = m_oIBaseDatos.IniciarEdicion
                Screen.MousePointer = vbNormal
                
                If teserror.NumError = TESnoerror Then
                    If ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
                        TextoError = g_oSolicitudSeleccionada.ComprobarVariablesCalidad(g_oSolicitudSeleccionada.Id, gParametrosInstalacion.gIdioma, True)
                        If Not (TextoError = "") Then
                            oMensajes.NoEliminarCertificadoPorVC TextoError
                            Exit Sub
                        End If
                    End If
                
                    bFavoritos = g_oSolicitudSeleccionada.SolicitudesConFavoritos
                    If bFavoritos Then
                        irespuesta = oMensajes.PreguntaEliminarConFavoritas(m_sISolicitud & " " & CStr(g_oSolicitudSeleccionada.Codigo) & " - " & NullToStr(g_oSolicitudSeleccionada.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & "")
                        If irespuesta = vbYes Then
                            'Si tiene solicitudes Favoritas eliminarlas
                            irespuesta = oMensajes.PreguntaEliminarSolicitudesFavoritas
                        
                            If irespuesta = vbYes Then
                                Screen.MousePointer = vbHourglass
                                teserror = g_oSolicitudSeleccionada.EliminarSolicitudesFavoritas
                                Screen.MousePointer = vbNormal
                                
                                If teserror.NumError <> TESnoerror Then
                                    TratarError teserror
                                    Exit Sub
                                End If
                            End If
                        End If
                    
                    Else
                        irespuesta = oMensajes.PreguntaEliminar(m_sISolicitud & " " & CStr(g_oSolicitudSeleccionada.Codigo) & " - " & NullToStr(g_oSolicitudSeleccionada.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & "")
                    End If
                    
                    If irespuesta = vbYes Then
                                                                           
                        Dim sVariablesCal As String
                        ''COMPROBACION DE VARIABLES DE CALIDAD PARA CERTIFICADOS Y NO CONFORMIDADES
                        Dim bEsNoConformidadoCertificado As Boolean
                        
                        bEsNoConformidadoCertificado = g_oSolicitudSeleccionada.EsNoconformidadocertificado(ssSOLConfig.ActiveRow.Cells("TIPO").Value)
                        
                        If bEsNoConformidadoCertificado Then
                            
                            sVariablesCal = g_oSolicitudSeleccionada.ComprobarVariablesCalidad(g_oSolicitudSeleccionada.Id, basPublic.gParametrosInstalacion.gIdioma)
                            
                            If sVariablesCal <> "" Then
                            
                                oMensajes.VariablesdeCalidadAsociadas (sVariablesCal)
                            
                                Exit Sub
                            End If
                        End If
                        
                        teserror = m_oIBaseDatos.EliminarDeBaseDatos
                        Screen.MousePointer = vbNormal
                        
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Exit Sub
                        Else
                             'Si tiene workflow eliminarlo
                            If ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value <> TipoSolicitud.NoConformidades And ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value <> TipoSolicitud.Certificados Then
                                If ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value <> 0 Or ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value <> Null Then
                                    teserror = g_oSolicitudSeleccionada.EliminarWorkflow(ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value)
                                End If
                                'Si es de tipo contrato, y tiene workflows de modificaci�n y de baja tambi�n habr� que eliminarlos
                                If ssSOLConfig.ActiveRow.Cells("ID_WORKF_MODIF").Value <> 0 Or ssSOLConfig.ActiveRow.Cells("ID_WORKF_MODIF").Value <> Null Then
                                    teserror = g_oSolicitudSeleccionada.EliminarWorkflow(ssSOLConfig.ActiveRow.Cells("ID_WORKF_MODIF").Value)
                                End If
                                If ssSOLConfig.ActiveRow.Cells("ID_WORKF_BAJA").Value <> 0 Or ssSOLConfig.ActiveRow.Cells("ID_WORKF_BAJA").Value <> Null Then
                                    teserror = g_oSolicitudSeleccionada.EliminarWorkflow(ssSOLConfig.ActiveRow.Cells("ID_WORKF_BAJA").Value)
                                End If
                            End If
                            
                            EliminarDeGrid
                            RegistrarAccion ACCSolConfEliminar, "ID:" & CStr(g_oSolicitudSeleccionada.Id)
                            Set m_oIBaseDatos = Nothing
                        End If
                                                                        
                    Else
                        Set m_oIBaseDatos = Nothing
                    End If
                                                           
                Else
                    'Ha habido un error al iniciar la edicion
                    TratarError teserror
                    Set m_oIBaseDatos = Nothing
                    Set g_oSolicitudSeleccionada = Nothing
                    Exit Sub
                
                End If
        
            Case 1 'Peticionario
                 
                g_udtAccion = ACCSolConfElimPet

                Set oPeticionario = Nothing
                Set oPeticionario = oFSGSRaiz.Generar_CPeticionario
                oPeticionario.Id = ssSOLConfig.ActiveRow.Cells("ID_PET").Value
                Set m_oIBaseDatos = oPeticionario
                
                If Trim(ssSOLConfig.ActiveRow.Cells("PER_DEN").Value) <> "" And Not IsNull(ssSOLConfig.ActiveRow.Cells("PER_DEN").Value) Then
                    irespuesta = oMensajes.PreguntaEliminar(m_sIPeticionario & ssSOLConfig.ActiveRow.Cells("PER_DEN").Value)
                ElseIf Trim(ssSOLConfig.ActiveRow.Cells("DEPDEN").Value) <> "" And Not IsNull(ssSOLConfig.ActiveRow.Cells("DEPDEN").Value) Then
                    irespuesta = oMensajes.PreguntaEliminar(m_sIPeticionario & ssSOLConfig.ActiveRow.Cells("DEPDEN").Value)
                Else
                    irespuesta = oMensajes.PreguntaEliminar(m_sIPeticionario & ssSOLConfig.ActiveRow.Cells("UON_DEN").Value)
                End If
                If irespuesta = vbYes Then
                    Screen.MousePointer = vbHourglass
                    teserror = m_oIBaseDatos.EliminarDeBaseDatos
                    Screen.MousePointer = vbNormal

                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Exit Sub
                    Else
                        EliminarDeGrid
                        RegistrarAccion ACCSolConfElimPet, "ID:" & CStr(oPeticionario.Id)
                        Set m_oIBaseDatos = Nothing
                    End If
                Else
                    Set m_oIBaseDatos = Nothing
                End If
                Set oPeticionario = Nothing
                
            Case 2 'Notificado
                 
                g_udtAccion = ACCSolConfElimNotif

                Set oPeticionario = Nothing
                Set oPeticionario = oFSGSRaiz.Generar_CPeticionario
                oPeticionario.Id = ssSOLConfig.ActiveRow.Cells("ID_PET").Value
                Set m_oIBaseDatos = oPeticionario
                
                If Trim(ssSOLConfig.ActiveRow.Cells("PER_DEN").Value) <> "" And Not IsNull(ssSOLConfig.ActiveRow.Cells("PER_DEN").Value) Then
                    irespuesta = oMensajes.PreguntaEliminar(m_sINotificado & ssSOLConfig.ActiveRow.Cells("PER_DEN").Value)
                ElseIf Trim(ssSOLConfig.ActiveRow.Cells("DEPDEN").Value) <> "" And Not IsNull(ssSOLConfig.ActiveRow.Cells("DEPDEN").Value) Then
                    irespuesta = oMensajes.PreguntaEliminar(m_sINotificado & ssSOLConfig.ActiveRow.Cells("DEPDEN").Value)
                Else
                    irespuesta = oMensajes.PreguntaEliminar(m_sINotificado & ssSOLConfig.ActiveRow.Cells("UON_DEN").Value)
                End If
                If irespuesta = vbYes Then
                    Screen.MousePointer = vbHourglass
                    'teserror = m_oIBaseDatos.EliminarDeBaseDatos
                    teserror = oPeticionario.EliminarDeBaseDatosComoNotificado
                    Screen.MousePointer = vbNormal

                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Exit Sub
                    Else
                        EliminarDeGrid
                        RegistrarAccion ACCSolConfElimNotif, "ID:" & CStr(oPeticionario.Id)
                        Set m_oIBaseDatos = Nothing
                    End If
                Else
                    Set m_oIBaseDatos = Nothing
                End If
                Set oPeticionario = Nothing
        End Select
    
    End If
    
    g_udtAccion = ACCSolConfConsultar

End Sub

Private Sub cmdlistado_Click()
    Dim oNodoSeleccionado As MSComctlLib.node
    Dim oCarpetaSeleccionada As Object
    
    Set oNodoSeleccionado = tvwCarpetasSolicit.selectedItem
    
    If Not oNodoSeleccionado Is Nothing Then
        Set oCarpetaSeleccionada = DevolverCarpeta(Me.tvwCarpetasSolicit)
        If Not oCarpetaSeleccionada Is Nothing Then
            frmLstSolConfiguracion.m_lIDCarpeta = oCarpetaSeleccionada.Id
            frmLstSolConfiguracion.m_iNivelCarpeta = oCarpetaSeleccionada.Nivel
        Else
            frmLstSolConfiguracion.m_lIDCarpeta = 0
            frmLstSolConfiguracion.m_iNivelCarpeta = 0
        End If
        frmLstSolConfiguracion.txtCarpeta.Text = oNodoSeleccionado.Text
    Else
        frmLstSolConfiguracion.m_lIDCarpeta = Null
        frmLstSolConfiguracion.m_iNivelCarpeta = Null
        frmLstSolConfiguracion.txtCarpeta.Text = ""
    End If
    frmLstSolConfiguracion.Show vbModal
End Sub


Private Sub cmdRestaurar_Click()
    CargarGridSolicitudes
End Sub


Private Sub Form_Load()
    Me.Height = 6225
    Me.Width = 11055
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
        
    ConfigurarSeguridad

    GenerarEstructuraCarpetas Me.tvwCarpetasSolicit, False, False

    'Carga la grid con las solicitudes:
    CargarGridSolicitudes
    
    InicializarGrid
    
    ssSOLConfig.CollapseAll
       
    CrearValueList
    
    g_udtAccion = ACCSolConfConsultar
    
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    Screen.MousePointer = vbNormal
End Sub


Friend Sub GenerarEstructuraCarpetas(ByRef tvw As TreeView, ByVal bOrdenadoPorDen As Boolean, ByVal bExpandir As Boolean)

Dim oCSN1 As CCarpetaSolicitN1
Dim oCSN2 As CCarpetaSolicitN2
Dim oCSN3 As CCarpetaSolicitN3
Dim oCSN4 As CCarpetaSolicitN4

' Otras
Dim nodx As node
    Set m_oCarpetasSolicitN1 = Nothing
    Set m_oCarpetasSolicitN1 = oFSGSRaiz.Generar_CCarpetasSolicitN1.DevolverEstructuraCarpetas(bOrdenadoPorDen)
    
        
   '************************************************************
    'Generamos la estructura arborea
    tvw.Nodes.clear
    
    Set nodx = tvw.Nodes.Add(, , "CSN0", m_sRaizCarpetas, "CSN0")
    nodx.Tag = "CSN0"
    nodx.Expanded = True
    nodx.Selected = True
        
    For Each oCSN1 In m_oCarpetasSolicitN1
        
        Set nodx = tvw.Nodes.Add("CSN0", tvwChild, "CSN1" & CStr(oCSN1.Id), oCSN1.Den, "CSN1")
        nodx.Tag = "CSN1" & CStr(oCSN1.Id)
        nodx.Expanded = bExpandir
        
        For Each oCSN2 In oCSN1.CarpetasSolicitN2
            Set nodx = tvw.Nodes.Add("CSN1" & CStr(oCSN1.Id), tvwChild, "CSN2" & CStr(oCSN2.Id), oCSN2.Den, "CSN2")
            nodx.Tag = "CSN2" & CStr(oCSN2.Id)
            nodx.Expanded = bExpandir
            
            For Each oCSN3 In oCSN2.CarpetasSolicitN3
                Set nodx = tvw.Nodes.Add("CSN2" & CStr(oCSN2.Id), tvwChild, "CSN3" & CStr(oCSN3.Id), oCSN3.Den, "CSN3")
                nodx.Tag = "CSN3" & CStr(oCSN3.Id)
                nodx.Expanded = bExpandir
                
                For Each oCSN4 In oCSN3.CarpetasSolicitN4
                    Set nodx = tvw.Nodes.Add("CSN3" & CStr(oCSN3.Id), tvwChild, "CSN4" & CStr(oCSN4.Id), oCSN4.Den, "CSN4")
                    nodx.Tag = "CSN4" & CStr(oCSN4.Id)
                    nodx.Expanded = bExpandir
                Next
            Next
        Next
    Next
    
    
    Set nodx = Nothing
    
    Set oCSN1 = Nothing
    Set oCSN2 = Nothing
    Set oCSN3 = Nothing
    Set oCSN4 = Nothing
    
End Sub



Private Sub CargarGridSolicitudes()
    
    Dim nodx As MSComctlLib.node
    Dim oCarpeta As Object
    
    Screen.MousePointer = vbHourglass
    
    Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
    
    Set m_oSolicitudes = Nothing
    Set m_oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    
    Set nodx = tvwCarpetasSolicit.selectedItem
    
 

    If oCarpeta Is Nothing Then
        Set ssSOLConfig.DataSource = m_oSolicitudes.DevolverSolicitudes
    Else
        Set ssSOLConfig.DataSource = m_oSolicitudes.DevolverSolicitudes(oCarpeta.Nivel, oCarpeta.Id)
    End If
    ssSOLConfig.CollapseAll
    
    Screen.MousePointer = vbNormal

End Sub

Friend Function DevolverCarpeta(ByRef tvw As TreeView) As Object
Dim oCarpeta As Object
Dim sID1 As String
Dim sID2 As String
Dim sID3 As String
Dim sID4 As String

Dim nodx As MSComctlLib.node
    
        'iNivel = 0
        Set nodx = tvw.selectedItem
        If nodx Is Nothing Then
            Set DevolverCarpeta = Nothing
            Exit Function
        End If
        
        Select Case Left(nodx.Tag, 4)
            Case "CSN1"
                    'iNivel = 1
                    sID1 = DevolverId(nodx)
                    Set oCarpeta = m_oCarpetasSolicitN1.Item(sID1)
            Case "CSN2"
                    'iNivel = 2
                    sID1 = DevolverId(nodx.Parent)
                    sID2 = DevolverId(nodx)
                    Set oCarpeta = m_oCarpetasSolicitN1.Item(sID1).CarpetasSolicitN2.Item(sID2)
            Case "CSN3"
                    'iNivel = 3
                    sID1 = DevolverId(nodx.Parent.Parent)
                    sID2 = DevolverId(nodx.Parent)
                    sID3 = DevolverId(nodx)
                    Set oCarpeta = m_oCarpetasSolicitN1.Item(sID1).CarpetasSolicitN2.Item(sID2).CarpetasSolicitN3.Item(sID3)
            Case "CSN4"
                    'iNivel = 4
                    sID1 = DevolverId(nodx.Parent.Parent.Parent)
                    sID2 = DevolverId(nodx.Parent.Parent)
                    sID3 = DevolverId(nodx.Parent)
                    sID4 = DevolverId(nodx)
                    Set oCarpeta = m_oCarpetasSolicitN1.Item(sID1).CarpetasSolicitN2.Item(sID2).CarpetasSolicitN3.Item(sID3).CarpetasSolicitN4.Item(sID4)
        End Select

        Set DevolverCarpeta = oCarpeta
        Set oCarpeta = Nothing
        Set nodx = Nothing
End Function

Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant

    If node Is Nothing Then Exit Function

    DevolverId = Mid(node.Tag, 5, Len(node.Tag) - 4)

End Function


Private Sub ConfigurarSeguridad()
    
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
    
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFModificar)) Is Nothing) Then
            g_bModifSolic = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFModificarPetic)) Is Nothing) Then
            m_bModifPetic = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFRestUOPetic)) Is Nothing) Then
            m_bRAltaPetUON = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFRestDepPetic)) Is Nothing) Then
            m_bRAltaPetDep = True
        End If

        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFFlujoTrabajo)) Is Nothing) Then
            m_bModifFlujo = True
        End If
                
    Else
        g_bModifSolic = True
        m_bModifPetic = True
        m_bRAltaPetUON = False
        m_bRAltaPetDep = False
        m_bModifFlujo = True
    End If
    
    If Not g_bModifSolic Then
        cmdAnyadirSol.Visible = False
        cmdAnyadirPet.Left = cmdAnyadirSol.Left
                cmdAnyadirNotificado.Left = cmdAnyadirPet.Left + cmdAnyadirPet.Width + 75
        cmdEliminar.Left = cmdAnyadirNotificado.Left + cmdAnyadirNotificado.Width + 75
        cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 75
        'cmdListado.Left = cmdRestaurar.Left + cmdRestaurar.Width + 75
        cmdMover.Visible = False
        cmdCopiar.Visible = False
        cmdAnyadirCarpeta.Visible = False
        cmdModificarCarpeta.Visible = False
        cmdEliminarCarpeta.Visible = False
        cmdActualizar.Left = cmdAnyadirCarpeta.Left
        cmdListado.Left = cmdActualizar.Left + cmdActualizar.Width + 75
    End If
    
    If Not m_bModifPetic Then
        cmdAnyadirPet.Visible = False
                cmdAnyadirNotificado.Visible = False
        cmdEliminar.Left = cmdAnyadirPet.Left
        cmdRestaurar.Left = cmdEliminar.Left + cmdEliminar.Width + 75
        cmdMover.Left = cmdRestaurar.Left + cmdRestaurar.Width + 75
    End If
    
    If Not m_bModifPetic And Not g_bModifSolic Then
        cmdEliminar.Visible = False
        cmdRestaurar.Left = cmdEliminar.Left
        cmdMover.Left = cmdRestaurar.Left + cmdRestaurar.Width + 75
    End If
         
End Sub

Private Sub Form_Resize()
Dim dLong As Long

    If Me.Height < 2000 Then Exit Sub
    If Me.Width < 2000 Then Exit Sub

    SSSplitter1.Width = Me.Width - 195
    SSSplitter1.Height = Me.Height - picCmdsCarpetas.Height - 460
        
    SSSplitter1.Panes(0).Width = SSSplitter1.Width * 0.25
    SSSplitter1.Panes(1).Width = SSSplitter1.Width * 0.75
    
    ssSOLConfig.Width = SSSplitter1.Panes(1).Width '- 100
    ssSOLConfig.Height = SSSplitter1.Panes(1).Height - picNavigate.Height '- 200
    
    picNavigate.Top = ssSOLConfig.Top + ssSOLConfig.Height + 20
    
    picEdicion.Top = ssSOLConfig.Top + ssSOLConfig.Height + 20
    picEdicion.Left = SSSplitter1.Panes(1).Width / 2.5
    
    picCmdsCarpetas.Top = SSSplitter1.Top + SSSplitter1.Height + 20
    
    If ssSOLConfig.Bands.Count = 0 Then Exit Sub
    
    With ssSOLConfig.Bands(0)
        .Columns("TIPO").Width = ssSOLConfig.Width * 0.118
        .Columns("DEN").Width = ssSOLConfig.Width * 0.177
        .Columns("DESCR").Width = ssSOLConfig.Width * 0.045
        .Columns("ADJUN").Width = ssSOLConfig.Width * 0.0601
        
        If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
            .Columns("COD").Width = ssSOLConfig.Width * 0.085
            .Columns("GESTOR_DEN").Width = ssSOLConfig.Width * 0.12
            .Columns("FORM").Width = ssSOLConfig.Width * 0.12
            .Columns("WORKFL").Width = ssSOLConfig.Width * 0.12
            .Columns("WORKFL_MODIF").Width = ssSOLConfig.Width * 0.12
            .Columns("WORKFL_BAJA").Width = ssSOLConfig.Width * 0.12
        Else
            .Columns("COD").Width = ssSOLConfig.Width * 0.058
            .Columns("GESTOR_DEN").Width = ssSOLConfig.Width * 0.107
            .Columns("FORM").Width = ssSOLConfig.Width * 0.095
            .Columns("WORKFL").Width = ssSOLConfig.Width * 0.095
            .Columns("WORKFL_MODIF").Width = ssSOLConfig.Width * 0.095
            .Columns("WORKFL_BAJA").Width = ssSOLConfig.Width * 0.095
        End If
        
        .Columns("CONF").Width = ssSOLConfig.Width * 0.067
        .Columns("PUB").Width = ssSOLConfig.Width * 0.058
        .Columns("PEDIDO_DIRECTO").Width = ssSOLConfig.Width * 0.084
        If .Columns.Exists("ESTADOS") Then
            .Columns("ESTADOS").Width = ssSOLConfig.Width * 0.084
        End If
        
        dLong = .Columns("PEDIDO_DIRECTO").Width + .Columns("PUB").Width + .Columns("CONF").Width + .Columns("WORKFL_MODIF").Width + .Columns("WORKFL_BAJA").Width + .Columns("FORM").Width + .Columns("ADJUN").Width + .Columns("DESCR").Width + .Columns("GESTOR_DEN").Width + .Columns("DEN").Width + .Columns("COD").Width + .Columns("TIPO").Width
    End With
    
    With ssSOLConfig.Bands(1)
        If .Groups.Count = 0 Then Exit Sub
        .Groups(0).Width = dLong - 300
        .Columns("UON_DEN").Width = ssSOLConfig.Width * 0.31
        .Columns("DEPDEN").Width = ssSOLConfig.Width * 0.31
        .Columns("PER_DEN").Width = ssSOLConfig.Width * 0.31
    End With
    
    '*********************************************************************
    'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
    '*********************************************************************
    With ssSOLConfig.Bands(2)
        If .Groups.Count = 0 Then Exit Sub
        .Groups(0).Width = dLong - 300
        .Columns("UON_DEN").Width = ssSOLConfig.Width * 0.31
        .Columns("DEPDEN").Width = ssSOLConfig.Width * 0.31
        .Columns("PER_DEN").Width = ssSOLConfig.Width * 0.31
    End With
    '*********************************************************************
End Sub

Private Sub ssSOLConfig_AfterCellCancelUpdate(ByVal Cell As UltraGrid.SSCell)
    Select Case Cell.Column.key
        Case "TIPO"
            Cell.Value = Cell.Row.Cells("ID_TIPO").Value
    End Select
End Sub

Private Sub SSSplitter1_Resize(ByVal BorderPanes As SSSplitter.Panes)
    ssSOLConfig.Width = SSSplitter1.Panes(1).Width '- 100
    ssSOLConfig.Height = SSSplitter1.Panes(1).Height - picNavigate.Height '- 200
End Sub

Private Sub Form_Unload(Cancel As Integer)

''' * Objetivo: Descargar el formulario si no hay cambios pendientes
       
    Set ssSOLConfig.DataSource = Nothing
    
    Set m_oSolicitudes = Nothing
    Set m_oIBaseDatos = Nothing
    Set g_oSolicitudSeleccionada = Nothing
    
    g_udtAccion = ACCSolConfConsultar
    
    Me.Visible = False
    
End Sub

''' <summary>
''' Carga los idiomas
''' </summary>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,1seg.</remarks>

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLCONFIGURACION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        m_sIConfiguracion = Ador(0).Value
        Ador.MoveNext
        m_sISolicitudes = Ador(0).Value
        Ador.MoveNext
        m_sITipo = Ador(0).Value
        Ador.MoveNext
        m_sITipoSolCom = Ador(0).Value
        Ador.MoveNext
        m_sITipoSolOtras = Ador(0).Value '5
        Ador.MoveNext
        m_sICodigo = Ador(0).Value
        Ador.MoveNext
        m_sIDen = Ador(0).Value
        Ador.MoveNext
        m_sIGestor = Ador(0).Value
        Ador.MoveNext
        m_sIDescr = Ador(0).Value
        Ador.MoveNext
        m_sIArchivos = Ador(0).Value '10
        Ador.MoveNext
        m_sIForm = Ador(0).Value
        Ador.MoveNext
        m_sIWork = Ador(0).Value
        Ador.MoveNext
        m_sIPub = Ador(0).Value
        Ador.MoveNext
        m_sIPedDir = Ador(0).Value
        Ador.MoveNext
        m_sIPeticionarios = Ador(0).Value '15
        Ador.MoveNext
        m_sIUON = Ador(0).Value
        Ador.MoveNext
        m_sIDep = Ador(0).Value
        Ador.MoveNext
        m_sIPer = Ador(0).Value
        Ador.MoveNext
        cmdAnyadirSol.caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyadirPet.caption = Ador(0).Value '20
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        
        Ador.MoveNext
        m_sISolicitud = Ador(0).Value
        Ador.MoveNext
        m_sIPeticionario = Ador(0).Value '25
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        m_sIEstados = Ador(0).Value

        Ador.MoveNext
        cmdAnyadirCarpeta.caption = Ador(0).Value
        Ador.MoveNext
        cmdModificarCarpeta.caption = Ador(0).Value '30
        Ador.MoveNext
        cmdEliminarCarpeta.caption = Ador(0).Value
        Ador.MoveNext
        cmdActualizar.caption = Ador(0).Value
        Ador.MoveNext
        cmdMover.caption = Ador(0).Value
        Ador.MoveNext
        m_sRaizCarpetas = Ador(0).Value
        Ador.MoveNext
        m_slaCarpeta = Ador(0).Value
        
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        cmdCopiar.caption = Ador(0).Value

        Dim i As Integer
        For i = 0 To 1
            Ador.MoveNext
        Next
        
        For i = 0 To 16
            Ador.MoveNext
        Next

        Ador.MoveNext
        
        Ador.MoveNext
        
        Ador.MoveNext
        m_sIContratos = Ador(0).Value '60 Contratos
        Ador.MoveNext
        m_sIWork = Ador(0).Value '61 Workflow alta
        Ador.MoveNext
        m_sIWorkModificacion = Ador(0).Value '62 WorkFlow Modificaci�n
        Ador.MoveNext
        m_sIWorkBaja = Ador(0).Value '63 Workflow Baja
        Ador.MoveNext
        m_sIEmpresas = Ador(0).Value '64 Empresas
        Ador.MoveNext
        m_sIConfiguracionCertificado = Ador(0).Value '65 Configuraci�n
        
        '*********************************************************************
        'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
        '*********************************************************************
        Ador.MoveNext
        m_sINotificados = Ador(0).Value  '66 Notificados
        Ador.MoveNext
        cmdAnyadirNotificado.caption = Ador(0).Value  '67 A�adir &Notificado
        Ador.MoveNext
        m_sINotificado = Ador(0).Value '68 Notificado
        Ador.MoveNext
        m_sFormAsociadoNovalido = Ador(0).Value '69 El formulario asociado no es v�lido
        Ador.MoveNext
        m_sDebeExistirMoneda = Ador(0).Value '70 Debe existir un campo de sistema moneda
        '*********************************************************************
        Ador.Close
    End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ssSOLConfig_AfterCellListCloseUp
' Author    : ltg
' Date      : 06/10/2016
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Sub ssSOLConfig_AfterCellListCloseUp(ByVal Cell As UltraGrid.SSCell)
Dim oWorkflows As CWorkflows
Dim oForms As CFormularios
Dim oGestor As CPersona
Dim i As Long
Dim oDeptos As CDepartamentos

   On Error GoTo ssSOLConfig_AfterCellListCloseUp_Error

    Select Case Cell.Column.key
    Case "GESTOR_DEN"
        If Cell.GetText(ssMaskModeRaw) = "" Then
            oMensajes.NoValido Cell.Column.Header.caption
            Exit Sub
        Else
            If Not Cell.Column.ValueList.Find(Cell.GetText(ssMaskModeRaw)) Is Nothing Then
                m_bRespetar = True
                Cell.Row.Cells("GESTOR").Value = Cell.Column.ValueList.Find(Cell.GetText(ssMaskModeRaw)).DataValue
                m_bRespetar = False
                Set oGestor = oFSGSRaiz.Generar_CPersona
                oGestor.Cod = Cell.Row.Cells("GESTOR").Value
                oGestor.CargarTodosLosDatos True
                m_bRespetar = True
                Cell.Row.Cells("NOM").Value = oGestor.nombre
                Cell.Row.Cells("APE").Value = oGestor.Apellidos
                m_bRespetar = False
            End If
        End If
    
    Case "FORM"
        If Cell.GetText = "" Then
            ssSOLConfig.ActiveRow.Cells("ID_FORM").Value = ""
        Else
            Set oForms = oFSGSRaiz.Generar_CFormularios
            oForms.CargarTodosFormularios Cell.GetText, , True
            
            
            If oForms.Count > 0 Then
                m_bRespetar = True
                Cell.Row.Cells("ID_FORM").Value = oForms.Item(1).Id
                Cell.Row.Cells("MULT").Value = BooleanToSQLBinary(oForms.Item(1).Multiidioma)
                m_bRespetar = False
            End If
            Set oForms = Nothing
        End If
    
    Case "WORKFL"
        If Cell.SelText = "" Then
           Cell.Row.Cells("ID_WORKF").Value = Null
        Else
            Set oWorkflows = oFSGSRaiz.generar_CWorkflows
            oWorkflows.CargarTodosWorkflows Cell.SelText, , True
            If oWorkflows.Item(0) Is Nothing Then
                m_bRespetar = True
                Cell.Row.Cells("ID_WORKF").Value = oWorkflows.Item(1).Id
                m_bRespetar = False
            End If
            Set oWorkflows = Nothing
        End If
    
    Case "WORKFL_MODIF"
        If Cell.SelText = "" Then
           Cell.Row.Cells("ID_WORKF_MODIF").Value = Null
        Else
            Set oWorkflows = oFSGSRaiz.generar_CWorkflows
            oWorkflows.CargarTodosWorkflows Cell.SelText, , True
            If oWorkflows.Item(0) Is Nothing Then
                m_bRespetar = True
                Cell.Row.Cells("ID_WORKF_MODIF").Value = oWorkflows.Item(1).Id
                m_bRespetar = False
            End If
            Set oWorkflows = Nothing
        End If
        
    Case "WORKFL_BAJA"
        If Cell.SelText = "" Then
           Cell.Row.Cells("ID_WORKF_BAJA").Value = Null
        Else
            Set oWorkflows = oFSGSRaiz.generar_CWorkflows
            oWorkflows.CargarTodosWorkflows Cell.SelText, , True
            If oWorkflows.Item(0) Is Nothing Then
                m_bRespetar = True
                Cell.Row.Cells("ID_WORKF_BAJA").Value = oWorkflows.Item(1).Id
                m_bRespetar = False
            End If
            Set oWorkflows = Nothing
        End If
    
    Case "TIPO"
        If Cell.SelText = "" Then
            Cell.Row.Cells("ID_TIPO").Value = ""
        Else
            If Not Cell.Column.ValueList.Find(Cell.GetText(ssMaskModeRaw)) Is Nothing Then
                Dim bSeguir As Boolean
                bSeguir = True
                If g_udtAccion <> ACCSolConfAnyadir Then bSeguir = ComprobarCambioTipoSolicitud(Cell, Cell.Column.ValueList.Find(Cell.GetText(ssMaskModeRaw)).TagVariant)
                
                If bSeguir Then
                    m_bRespetar = True
                    Cell.Row.Cells("ID_TIPO").Value = Cell.Column.ValueList.Find(Cell.GetText(ssMaskModeRaw)).DataValue
                    Cell.Row.Cells("TIPO_SOLICIT").Value = Cell.Column.ValueList.Find(Cell.GetText(ssMaskModeRaw)).TagVariant
                    If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
                        Cell.Row.Cells("WORKFL").Value = ""
                        Cell.Row.Cells("ID_WORKF").Value = Null
                        Cell.Row.Cells("WORKFL").Column.Style = UltraGrid.ssStyleEdit
                        Cell.Row.Cells("WORKFL").Activation = ssActivationDisabled
                        Cell.Row.Cells("WORKFL").Appearance.Backcolor = vbButtonFace
                    Else
                        Cell.Row.Cells("WORKFL").Column.Style = UltraGrid.ssStyleEditButton
                        Cell.Row.Cells("WORKFL").Column.ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
                        Cell.Row.Cells("WORKFL").Activation = ssActivationAllowEdit
                        Cell.Row.Cells("WORKFL").Appearance.Backcolor = vbWhite
                    End If
                    
                    If Cell.Row.Cells("TIPO_SOLICIT").Value <> TipoSolicitud.Contrato Then
                        Cell.Row.Cells("WORKFL_MODIF").Value = ""
                        Cell.Row.Cells("ID_WORKF_MODIF").Value = Null
                        Cell.Row.Cells("WORKFL_MODIF").Column.Style = UltraGrid.ssStyleEdit
                        Cell.Row.Cells("WORKFL_MODIF").Activation = ssActivationDisabled
                        Cell.Row.Cells("WORKFL_MODIF").Appearance.Backcolor = vbButtonFace
                        
                        Cell.Row.Cells("WORKFL_BAJA").Value = ""
                        Cell.Row.Cells("ID_WORKF_BAJA").Value = Null
                        Cell.Row.Cells("WORKFL_BAJA").Column.Style = UltraGrid.ssStyleEdit
                        Cell.Row.Cells("WORKFL_BAJA").Activation = ssActivationDisabled
                        Cell.Row.Cells("WORKFL_BAJA").Appearance.Backcolor = vbButtonFace
                    Else
                        Cell.Row.Cells("WORKFL_MODIF").Column.Style = UltraGrid.ssStyleEditButton
                        Cell.Row.Cells("WORKFL_MODIF").Column.ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
                        Cell.Row.Cells("WORKFL_MODIF").Activation = ssActivationAllowEdit
                        Cell.Row.Cells("WORKFL_MODIF").Appearance.Backcolor = vbWhite
                        
                        Cell.Row.Cells("WORKFL_BAJA").Column.Style = UltraGrid.ssStyleEditButton
                        Cell.Row.Cells("WORKFL_BAJA").Column.ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
                        Cell.Row.Cells("WORKFL_BAJA").Activation = ssActivationAllowEdit
                        Cell.Row.Cells("WORKFL_BAJA").Appearance.Backcolor = vbWhite
                    End If
                    
                    If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
                        Cell.Row.Cells("Publicar").Value = Cell.Row.Cells("PUB").Value
                    Else
                        Cell.Row.Cells("Publicar").Value = Cell.Row.Cells("PUB").Value
                    End If
                    m_bRespetar = False
                Else
                    Cell.CancelUpdate
                End If
            End If
        End If
        
    Case "DEPDEN"
        If Cell.SelText = "" Then
            Cell.Row.Cells("DEPCOD").Value = ""
        Else
            Set oDeptos = oFSGSRaiz.Generar_CDepartamentos
            oDeptos.CargarTodosLosDepartamentos ssSOLConfig.ValueLists.Item("Departamentos").ValueListItems.Item(i).DataValue
            If oDeptos.Item(0) Is Nothing Then
                m_bRespetar = True
                Cell.Row.Cells("DEPCOD").Value = oDeptos.Item(1).Cod
                m_bRespetar = False
            End If
            Set oDeptos = Nothing
        End If
        
    Case "PER_DEN"
        If Cell.SelText = "" Then
            Cell.Row.Cells("PER").Value = ""
        Else
            For i = 0 To ssSOLConfig.ValueLists.Item("Personas").ValueListItems.Count - 1
                If ssSOLConfig.ValueLists.Item("Personas").ValueListItems.Item(i).DisplayText = Cell.SelText Then
                    m_bRespetar = True
                    Cell.Row.Cells("PER").Value = ssSOLConfig.ValueLists.Item("Personas").ValueListItems.Item(i).DataValue
                    m_bRespetar = False
                    Set oGestor = oFSGSRaiz.Generar_CPersona
                    oGestor.Cod = ssSOLConfig.ActiveRow.Cells("PER").Value
                    oGestor.CargarTodosLosDatos True
                    m_bRespetar = True
                    Cell.Row.Cells("NOM").Value = oGestor.nombre
                    Cell.Row.Cells("APE").Value = oGestor.Apellidos
                    m_bRespetar = False
                    Exit For
                End If
            Next i
                
        End If
        
    End Select

   On Error GoTo 0
   Exit Sub

ssSOLConfig_AfterCellListCloseUp_Error:

    MsgBox "Error " & err.Number & " (" & err.Description & ") in procedure ssSOLConfig_AfterCellListCloseUp of Form frmSOLConfiguracion"
    
End Sub


Private Sub ssSOLConfig_AfterCellUpdate(ByVal Cell As UltraGrid.SSCell)
Dim teserror As TipoErrorSummit
Dim oIdi As CIdioma

    If ssSOLConfig.ActiveRow Is Nothing Then Exit Sub
    If Cell.DataChanged = False Then Exit Sub
    If m_bRespetar Then Exit Sub
    
    If g_udtAccion = AccionesSummit.ACCSolConfAnyadir Or g_udtAccion = ACCSolConfAnyaPet Or g_udtAccion = ACCSolConfAnyaNotif Or g_udtAccion = ACCSolConfCopiar Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Select Case Cell.Column.Band.Index

        Case 0
            g_udtAccion = ACCSolConfModificar

            Set g_oSolicitudSeleccionada = Nothing
            Set g_oSolicitudSeleccionada = oFSGSRaiz.Generar_CSolicitud
            g_oSolicitudSeleccionada.Id = Cell.Row.Cells("ID_SOL").Value
            g_oSolicitudSeleccionada.FECACT = CDate(Cell.Row.Cells("FECACT").Value)
            
            Set m_oIBaseDatos = g_oSolicitudSeleccionada
            teserror = m_oIBaseDatos.IniciarEdicion
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set m_oIBaseDatos = Nothing
                Set g_oSolicitudSeleccionada = Nothing
                Exit Sub
            Else
                g_oSolicitudSeleccionada.Codigo = Cell.Row.Cells("COD").Value
                If ssSOLConfig.ActiveRow.Cells("MULT").Value = "1" Then
                    g_oSolicitudSeleccionada.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den = Cell.Row.Cells("DEN").Value
                Else
                    For Each oIdi In m_oIdiomas
                        g_oSolicitudSeleccionada.Denominaciones.Item(oIdi.Cod).Den = Cell.Row.Cells("DEN").Value
                    Next
                End If
                
                If g_oSolicitudSeleccionada.Gestor.Cod <> Cell.Row.Cells("GESTOR").Value Then
                    Set g_oSolicitudSeleccionada.Gestor = Nothing
                    Set g_oSolicitudSeleccionada.Gestor = oFSGSRaiz.Generar_CPersona
                    g_oSolicitudSeleccionada.Gestor.Cod = Cell.Row.Cells("GESTOR").Value
                    g_oSolicitudSeleccionada.Gestor.CargarTodosLosDatos True
                End If
                
                'Form
                Set g_oSolicitudSeleccionada.Formulario = Nothing
                Set g_oSolicitudSeleccionada.Formulario = oFSGSRaiz.Generar_CFormulario
                If Cell.Row.Cells("FORM").Column.ValueList.ValueListItems.Count > 0 Then
                    If Not Cell.Row.Cells("FORM").Column.ValueList.Find(Cell.Row.Cells("FORM").GetText(ssMaskModeRaw)) Is Nothing Then
                        g_oSolicitudSeleccionada.Formulario.Id = Cell.Row.Cells("FORM").Column.ValueList.Find(Cell.Row.Cells("FORM").GetText(ssMaskModeRaw)).DataValue
                    Else
                        g_oSolicitudSeleccionada.Formulario.Id = Cell.Row.Cells("ID_FORM").Value
                    End If
                Else
                    g_oSolicitudSeleccionada.Formulario.Id = Cell.Row.Cells("ID_FORM").Value
                End If
                
                'Work alta
                Set g_oSolicitudSeleccionada.Workflow = Nothing
                If Cell.Row.Cells("WORKFL").Value <> "" Then
                    Set g_oSolicitudSeleccionada.Workflow = oFSGSRaiz.Generar_CWorkflow
                    If Cell.Row.Cells("WORKFL").Column.ValueList.ValueListItems.Count > 0 Then
                        If Not Cell.Row.Cells("WORKFL").Column.ValueList.Find(Cell.Row.Cells("WORKFL").GetText(ssMaskModeRaw)) Is Nothing Then
                            g_oSolicitudSeleccionada.Workflow.Id = Cell.Row.Cells("WORKFL").Column.ValueList.Find(Cell.Row.Cells("WORKFL").GetText(ssMaskModeRaw)).DataValue
                        Else
                            g_oSolicitudSeleccionada.Workflow.Id = Cell.Row.Cells("ID_WORKF").Value
                        End If
                    Else
                        g_oSolicitudSeleccionada.Workflow.Id = Cell.Row.Cells("ID_WORKF").Value
                        cmdAnyadirPet.Enabled = False
                    End If
                Else
                    cmdAnyadirPet.Enabled = True
                End If
                
                'Work modificaci�n
                Set g_oSolicitudSeleccionada.WorkflowModificacion = Nothing
                If Cell.Row.Cells("WORKFL_MODIF").Value <> "" Then
                    Set g_oSolicitudSeleccionada.WorkflowModificacion = oFSGSRaiz.Generar_CWorkflow
                    If Cell.Row.Cells("WORKFL_MODIF").Column.ValueList.ValueListItems.Count > 0 Then
                        If Not Cell.Row.Cells("WORKFL_MODIF").Column.ValueList.Find(Cell.Row.Cells("WORKFL_MODIF").GetText(ssMaskModeRaw)) Is Nothing Then
                            g_oSolicitudSeleccionada.WorkflowModificacion.Id = Cell.Row.Cells("WORKFL_MODIF").Column.ValueList.Find(Cell.Row.Cells("WORKFL_MODIF").GetText(ssMaskModeRaw)).DataValue
                        Else
                            g_oSolicitudSeleccionada.WorkflowModificacion.Id = Cell.Row.Cells("ID_WORKF_MODIF").Value
                        End If
                    Else
                        g_oSolicitudSeleccionada.WorkflowModificacion.Id = Cell.Row.Cells("ID_WORKF_MODIF").Value
                    End If
                End If
                
                'Work baja
                Set g_oSolicitudSeleccionada.WorkflowBaja = Nothing
                If Cell.Row.Cells("WORKFL_BAJA").Value <> "" Then
                    Set g_oSolicitudSeleccionada.WorkflowBaja = oFSGSRaiz.Generar_CWorkflow
                    If Cell.Row.Cells("WORKFL_BAJA").Column.ValueList.ValueListItems.Count > 0 Then
                        If Not Cell.Row.Cells("WORKFL_BAJA").Column.ValueList.Find(Cell.Row.Cells("WORKFL_BAJA").GetText(ssMaskModeRaw)) Is Nothing Then
                            g_oSolicitudSeleccionada.WorkflowBaja.Id = Cell.Row.Cells("WORKFL_BAJA").Column.ValueList.Find(Cell.Row.Cells("WORKFL_BAJA").GetText(ssMaskModeRaw)).DataValue
                        Else
                            g_oSolicitudSeleccionada.WorkflowBaja.Id = Cell.Row.Cells("ID_WORKF_BAJA").Value
                        End If
                    Else
                        g_oSolicitudSeleccionada.WorkflowBaja.Id = NullToDbl0(Cell.Row.Cells("ID_WORKF_BAJA").Value)
                    End If
                End If
                
                g_oSolicitudSeleccionada.Publicado = GridCheckToBoolean(Cell.Row.Cells("PUBLICAR").Value)
                                
                If Cell.Row.Cells("TIPO_SOLICIT") = TipoSolicitud.Certificados Then
                    
                    m_bRespetar = True
                    If g_oSolicitudSeleccionada.Publicado Then
                        Cell.Row.Cells("PUB") = 1
                    Else
                        Cell.Row.Cells("PUB") = 0
                    End If
                    m_bRespetar = False
                
                Else
                    If GridCheckToBoolean(Cell.Row.Cells("PUBLICAR").Value) Then
                        g_oSolicitudSeleccionada.Publicacion = 1
                    Else
                        g_oSolicitudSeleccionada.Publicacion = 0
                    End If
                End If
                
                
                If IsNull(Cell.Row.Cells("PEDIDO_DIRECTO").Value) Then
                    g_oSolicitudSeleccionada.PedidoDirecto = Null
                Else
                    g_oSolicitudSeleccionada.PedidoDirecto = StrToDbl0(Cell.Row.Cells("PEDIDO_DIRECTO").Value)
                End If
                
                g_oSolicitudSeleccionada.Tipo = Cell.Row.Cells("ID_TIPO").Value
                
                teserror = m_oIBaseDatos.FinalizarEdicionModificando
                
                If Cell.Row.Cells("COD_EMPRESAS").Value <> "" Then
                    g_oSolicitudSeleccionada.CadenaEmpresas = Cell.Row.Cells("COD_EMPRESAS").Value
                    g_oSolicitudSeleccionada.ActualizarEmpresas
                End If
                
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion g_udtAccion, "ID:" & g_oSolicitudSeleccionada.Id
                    m_bRespetar = True
                    Cell.Row.Cells("FECACT").Value = g_oSolicitudSeleccionada.FECACT
                    m_bRespetar = False
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Set m_oIBaseDatos = Nothing
                    Set g_oSolicitudSeleccionada = Nothing
                    Exit Sub
                End If
            End If

        Case 1
            g_udtAccion = ACCSolConfModifPet
        Case 2
            g_udtAccion = ACCSolConfModifNotif
    End Select

    g_udtAccion = ACCSolConfConsultar
    picNavigate.Visible = True
    picEdicion.Visible = False
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub ssSOLConfig_AfterRowActivate()

    If ssSOLConfig.ActiveRow Is Nothing Then Exit Sub
    If g_udtAccion = AccionesSummit.ACCSolConfAnyadir Or g_udtAccion = AccionesSummit.ACCSolConfCopiar Then Exit Sub
    
    ConfigurarInterfazSeguridad ssSOLConfig.ActiveRow.Band.Index
    
    '*********************************************************************
    'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
    '*********************************************************************
    If ((ssSOLConfig.ActiveRow.Band.key <> "PET") And (ssSOLConfig.ActiveRow.Band.key <> "NOTIF")) Then
    
        If ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
            cmdAnyadirPet.Enabled = True
            ssSOLConfig.ActiveRow.Cells("WORKFL").Column.Style = UltraGrid.ssStyleEdit
            ssSOLConfig.ActiveRow.Cells("WORKFL").Activation = ssActivationDisabled
            ssSOLConfig.ActiveRow.Cells("WORKFL").Appearance.Backcolor = vbButtonFace
        Else
            If ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value = TipoSolicitud.NoConformidades And (IsNull(ssSOLConfig.ActiveRow.Cells("WORKFL").Value) Or ssSOLConfig.ActiveRow.Cells("WORKFL").Value = "") Then
                cmdAnyadirPet.Enabled = True
            Else
                cmdAnyadirPet.Enabled = False
            End If
            ssSOLConfig.ActiveRow.Cells("WORKFL").Column.Style = UltraGrid.ssStyleEditButton
            ssSOLConfig.ActiveRow.Cells("WORKFL").Column.Activation = ssActivationActivateOnly
            ssSOLConfig.ActiveRow.Cells("WORKFL").Column.ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
            ssSOLConfig.ActiveRow.Cells("WORKFL").Activation = ssActivationAllowEdit
            ssSOLConfig.ActiveRow.Cells("WORKFL").Appearance.Backcolor = vbWhite
        End If
        If ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
            cmdAnyadirNotificado.Enabled = True
        Else
            cmdAnyadirNotificado.Enabled = False
        End If
    End If
            
    If ssSOLConfig.ActiveRow.Band.Index = 0 Then
        If ssSOLConfig.ActiveRow.Cells("MULT").Value = 1 Then
            ssSOLConfig.ActiveRow.Cells("DEN").Column.Style = UltraGrid.ssStyleEditButton
            ssSOLConfig.ActiveRow.Cells("DEN").Column.ButtonDisplayStyle = ssButtonDisplayStyleOnCellActivate
        Else
            ssSOLConfig.ActiveRow.Cells("DEN").Column.Style = UltraGrid.ssStyleEdit
        End If
    End If
End Sub

Private Sub ssSOLConfig_AfterRowCollapsed(ByVal Row As UltraGrid.SSRow)
    
    If ssSOLConfig.ActiveRow Is Nothing Then Exit Sub
    
    If Row.Band.Index <> ssSOLConfig.ActiveRow.Band.Index Then Exit Sub
    
    ConfigurarInterfazSeguridad Row.Band.Index
    
End Sub


Private Sub ssSOLConfig_AfterRowInsert(ByVal Row As UltraGrid.SSRow)
     
    'La checbox de publicaci�n que salga deshabilitada
    If Row.Band.Index = 0 Then
        Row.Cells("PUB").Value = 0
        Row.Cells("MULT").Value = 0
        
        'Si tenemos acceso exclusivo al m�dulo de solicitudes de compras rellenamos la columna del tipo
        'con el id=1 de compras.
        If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
            Row.Cells("ID_TIPO").Value = TipoAccesoFSWS.AccesoFSWSSolicCompra
        End If
    End If
     
End Sub

Private Sub ssSOLConfig_BeforeCellActivate(ByVal Cell As UltraGrid.SSCell, ByVal Cancel As UltraGrid.SSReturnBoolean)
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then Exit Sub
    
    If Cell.Column.key = "PEDIDO_DIRECTO" Then
        If Cell.Row.Cells("ID_TIPO").GetText(ssMaskModeIncludeBoth) <> "" Then
        Select Case Cell.Row.Cells("ID_TIPO").Value
            Case TipoSolicitud.SolicitudCompras
                Cell.Activation = UltraGrid.ssActivationAllowEdit
            Case Else
                Cell.Activation = UltraGrid.ssActivationActivateNoEdit
        End Select
        End If
    End If

End Sub

''' Revisado por: blp. Fecha: 29/10/2012
''' <summary>
''' Da contenido a las columnas de tipo dropdown del control ssSOLConfig en el evento anterior a que se desplegue el combo
''' </summary>
''' Cell: celda del grid pulsada
''' Cancel: par�metro para determinar si se cancela el evento
''' Llamada desde el evento. M�x. 0,1 seg
Private Sub ssSOLConfig_BeforeCellListDropDown(ByVal Cell As UltraGrid.SSCell, ByVal Cancel As UltraGrid.SSReturnBoolean)
    Dim oGestores As CPersonas
    Dim oGestor As CPersona
    Dim sGestor As String
    Dim oForms As CFormularios
    Dim oForm As CFormulario
    Dim oWorkflows As CWorkflows
    Dim oWork As cworkflow
    Dim i As Long
    Dim oDeptos As CDepartamentos
    Dim oDepto As CDepartamento
    Dim oTiposPredef As CTiposSolicit
    Dim ADORs As Ador.Recordset
    
    Select Case Cell.Column.key
    Case "GESTOR_DEN"
        Set oGestores = oFSGSRaiz.Generar_CPersonas
        oGestores.CargarGestores
        i = 0
        ssSOLConfig.ValueLists.Item("Gestores").ValueListItems.clear
        
        For Each oGestor In oGestores
            If IsNull(oGestor.nombre) Then
                sGestor = oGestor.Apellidos
            Else
                sGestor = oGestor.nombre & " " & oGestor.Apellidos
            End If
            ssSOLConfig.ValueLists.Item("Gestores").ValueListItems.Add oGestor.Cod, sGestor, i
            i = i + 1
        Next
        Set oGestores = Nothing
        
    Case "FORM"
        Set oForms = oFSGSRaiz.Generar_CFormularios
        oForms.CargarTodosFormularios
        i = 0
        ssSOLConfig.ValueLists.Item("Formularios").ValueListItems.clear
        
        For Each oForm In oForms
            ssSOLConfig.ValueLists.Item("Formularios").ValueListItems.Add oForm.Id, oForm.Den, i
            i = i + 1
        Next
        Set oForms = Nothing
    
    Case "WORKFL"
        Set oWorkflows = oFSGSRaiz.generar_CWorkflows
        oWorkflows.CargarTodosWorkflows
        i = 0
        ssSOLConfig.ValueLists.Item("Workflows").ValueListItems.clear
        
        For Each oWork In oWorkflows
            ssSOLConfig.ValueLists.Item("Workflows").ValueListItems.Add oWork.Id, oWork.Den, i
            i = i + 1
        Next
        Set oWorkflows = Nothing
    
    Case "DEPDEN"
        Set oDeptos = oFSGSRaiz.Generar_CDepartamentos
        oDeptos.CargarTodosLosDepartamentos
        i = 0
        ssSOLConfig.ValueLists.Item("Departamentos").ValueListItems.clear
        
        For Each oDepto In oDeptos
            ssSOLConfig.ValueLists.Item("Departamentos").ValueListItems.Add oDepto.Cod, oDepto.Den, i
            i = i + 1
        Next
        Set oDeptos = Nothing
        
    Case "PER_DEN"
        Set oGestores = oFSGSRaiz.Generar_CPersonas
        oGestores.CargarTodasLasPersonas2
        i = 0
        ssSOLConfig.ValueLists.Item("Personas").ValueListItems.clear
        
        For Each oGestor In oGestores
            If IsNull(oGestor.nombre) Then
                sGestor = oGestor.Apellidos
            Else
                sGestor = oGestor.nombre & " " & oGestor.Apellidos
            End If
            ssSOLConfig.ValueLists.Item("Personas").ValueListItems.Add oGestor.Cod, sGestor, i
            i = i + 1
        Next
        Set oGestores = Nothing
        
    Case "TIPO"
        Set oTiposPredef = oFSGSRaiz.Generar_CTiposSolicit
        Set ADORs = oTiposPredef.DevolverTiposSolicitudes
        i = 0
        ssSOLConfig.ValueLists.Item("Tipos").ValueListItems.clear
        
        If Not ADORs Is Nothing Then
            While Not ADORs.EOF
                Dim bOcultarSolicitudesContrato  As Boolean
                Dim bOcultarSolicitudesFactura As Boolean
                Dim bPermisoContratosUsuario As Boolean
                Dim bPermisoFacturasUsuario As Boolean
                If (oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador) Then
                    bPermisoContratosUsuario = True
                    bPermisoFacturasUsuario = True
                Else
                    bPermisoContratosUsuario = oUsuarioSummit.AccesoFSContratos
                    bPermisoFacturasUsuario = oUsuarioSummit.AccesoFSIM
                End If
                bOcultarSolicitudesContrato = (gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.SinAcceso) Or Not bPermisoContratosUsuario
                bOcultarSolicitudesFactura = (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso) Or Not bPermisoFacturasUsuario
                'Los tipos de solicitud que devuelve la funci�n DevolverTiposSolicitudes llevan una columna TIPO.
                'Las solicitudes de contrato son aquellas con valor 5, las de factura son el 7 y 13
                'Si no hay acceso a contratos y el tipo es 5, nos la saltamos. Lo mismo si no hay acceso a facturas y el tipo es 7
                'S�lo a�adimos al men� los tipos de solicitud cuando se cumpla que hay acceso a Contratos o cuando su tipo sea distinto de 5
                If Not ((bOcultarSolicitudesContrato And ADORs("TIPO").Value = TipoSolicitud.Contrato) _
                Or (bOcultarSolicitudesFactura And (ADORs("TIPO").Value = TipoSolicitud.AUTOFACTURA Or ADORs("TIPO").Value = TipoSolicitud.Factura))) Then
                    ssSOLConfig.ValueLists.Item("Tipos").ValueListItems.Add ADORs("ID").Value, ADORs("DESCR_" & gParametrosInstalacion.gIdioma).Value, i
                    ssSOLConfig.ValueLists.Item("Tipos").ValueListItems.Item(i).TagVariant = ADORs("TIPO").Value
                    i = i + 1
                End If
                ADORs.MoveNext
            Wend
        End If
        
        ADORs.Close
        Set ADORs = Nothing
        Set oTiposPredef = Nothing
    
    End Select
    
End Sub

''' <summary>Aqu� se haran las comprobaciones antes de guardar a BD</summary>
''' <param name="Cell">Celda q cambia</param>
''' <param name="NewValue">Nuevo valor de la celda</param>
''' <param name="Cancel">Para poder cancelar los cambios segun indiquen las comprobaciones</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
Private Sub ssSOLConfig_BeforeCellUpdate(ByVal Cell As UltraGrid.SSCell, NewValue As Variant, ByVal Cancel As UltraGrid.SSReturnBoolean)
    Dim irespuesta As Integer
    Dim oFormulario As CFormulario

    If Not m_TipoSolicitudNoEsFactura = 1 Then If Not Cell.DataChanged Then Exit Sub
    m_TipoSolicitudNoEsFactura = 0 'Incidencia Pruebas 31900.7 / 2013 / 79 permite cambiar una solicitud de un tipo distinto a una solicitud de factura
    
    If Not m_TipoSolicitudNoEsPedido = 1 Then If Not Cell.DataChanged Then Exit Sub
    m_TipoSolicitudNoEsPedido = 0
    
    If m_bRespetar Then Exit Sub
    
    If Me.ActiveControl.Name = "cmdDeshacer" Then
        If Not (g_udtAccion = ACCSolConfAnyadir Or g_udtAccion = ACCSolConfAnyaPet Or g_udtAccion = ACCSolConfAnyaNotif) Then
            Cancel = True
            picNavigate.Visible = True
            picEdicion.Visible = False
            g_udtAccion = ACCSolConfConsultar
        End If
        Exit Sub
    End If
    
    If Not (g_udtAccion = ACCSolConfAnyadir Or g_udtAccion = ACCSolConfAnyaPet Or g_udtAccion = ACCSolConfAnyaNotif) Then
        If IsNull(NewValue) Or NewValue = "" Or NewValue = "-1" Then
            Select Case Cell.Column.key
                Case "ID_TIPO"
                    oMensajes.NoValido m_sITipo
                Case "COD"
                    oMensajes.NoValido m_sICodigo
                Case "DEN"
                    oMensajes.NoValido m_sIDen
                Case "FORM"
                    oMensajes.NoValido m_sIForm
                Case "GESTOR"
                    oMensajes.NoValido m_sIGestor
            End Select
            
            Cancel = True
            If Me.Visible Then ssSOLConfig.SetFocus
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    'Eliminar la validaci�n del formulario asociado a las solicitudes de tipo certificado
    'si se ha seleccionado un tipo de certificado o de no conformidad se hacen las comprobaciones necesarias:
    If Cell.Column.key = "FORM" Then
        If Cell.Column.ValueList.ValueListItems.Count > 0 Then
            If CStr(Cell.Row.Cells("ID_FORM").GetText(ssMaskModeIncludeBoth)) <> "" And CStr(Cell.Row.Cells("TIPO_SOLICIT").GetText(ssMaskModeIncludeBoth)) <> "" Then
                If Not ComprobarCambioTipoSolicitud(Cell, Cell.Row.Cells("TIPO_SOLICIT").GetText(ssMaskModeIncludeBoth)) Then
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If
    End If
    
    If Cell.Column.key = "FORM" Then
        'Para solicitudes de tipo Pedido Expr�s o negociado no se permiten campos proveedor duplicados o en el desglose y es obligatorio que haya proveedor en la cabecera
        If Cell.Row.Cells("TIPO_SOLICIT").GetText(ssMaskModeIncludeBoth) = TipoSolicitud.Express Or Cell.Row.Cells("TIPO_SOLICIT").GetText(ssMaskModeIncludeBoth) = TipoSolicitud.pedido Then
            Set oFormulario = oFSGSRaiz.Generar_CFormulario
            oFormulario.Id = Cell.Row.Cells("ID_FORM").GetText(ssMaskModeIncludeBoth)
            Dim ComprobarProveedor As erroressummit
            ComprobarProveedor = oFormulario.ComprobarProveedorUnico
            If ComprobarProveedor <> TESnoerror Then
                oMensajes.NoPermitidoProveedor (ComprobarProveedor)
                Cancel = True
                Exit Sub
            End If
            Set oFormulario = Nothing
        End If
        
        If Cell.Column.ValueList.ValueListItems.Count > 0 Then
            If Not Cell.Column.ValueList.Find(Cell.GetText(ssMaskModeRaw)) Is Nothing Then
                If CStr(Cell.Column.ValueList.Find(Cell.GetText(ssMaskModeRaw)).DataValue) <> CStr(NullToStr(NewValue)) Then
                    'Si se ha elegido un tipo de Solicitud "Solicitud de Compra", "pedido negociado" o "express" y el Formulario no tiene campo ES_IMPORTE_TOTAL --> Cancelamos
                    If (NullToStr(ssSOLConfig.ActiveRow.Cells("ID_FORM").Value) <> "" And CStr(NullToStr(NewValue)) <> "" And NullToStr(ssSOLConfig.ActiveRow.Cells("ID_TIPO").Value) = CStr(TipoSolicitud.SolicitudCompras)) _
                       Or (NullToStr(ssSOLConfig.ActiveRow.Cells("ID_FORM").Value) <> "" And CStr(NullToStr(NewValue)) <> "" And (NullToStr(ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value) = CStr(TipoSolicitud.pedido) Or NullToStr(ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value) = CStr(TipoSolicitud.Express))) Then
                        Dim bImporte As Boolean
                        bImporte = ComprobarCampoImporteWorkflow(CLng(ssSOLConfig.ActiveRow.Cells("ID_FORM").Value))
                        If Not bImporte Then
                            oMensajes.FaltaCampoImporteWorkflow
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                                                        
                    'Avisamos de que deber� volver a configurar el workflow. Preguntamos si desea contnuar
                    If NullToStr(ssSOLConfig.ActiveRow.Cells("ID_FORM").Value) <> "" And CStr(NullToStr(NewValue)) <> "" And NullToStr(ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value) <> "" Then
                        irespuesta = oMensajes.PreguntaEliminarConfWorkflow()
                        If irespuesta = vbNo Then
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                Else
                    Cancel = True
                End If
            End If
        End If
        
        'Tarea 2918 -
        'Para todos los tipos de solicitud con flujo (todos menos certificados y no conformidades) si
        'si existe un campo importe en el formulario... debe existir tambi�n un campo moneda
        Dim tipoSol As String
        tipoSol = NullToStr(ssSOLConfig.ActiveRow.Cells("ID_TIPO").Value)
        If (NullToStr(ssSOLConfig.ActiveRow.Cells("ID_FORM").Value) <> "" And CStr(NullToStr(NewValue)) <> "" _
                And (tipoSol <> CStr(TipoSolicitud.Certificados) Or tipoSol <> CStr(TipoSolicitud.NoConformidades) Or tipoSol <> CStr(TipoSolicitud.Contrato))) _
           Or (NullToStr(ssSOLConfig.ActiveRow.Cells("ID_FORM").Value) <> "" And CStr(NullToStr(NewValue)) <> "" And (NullToStr(ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value) = CStr(TipoSolicitud.pedido) Or NullToStr(ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value) = CStr(TipoSolicitud.Express))) Then
                                        
            Dim bMonedaImporte As Boolean
            Set oFormulario = oFSGSRaiz.Generar_CFormulario
            oFormulario.Id = Cell.Row.Cells("ID_FORM").GetText(ssMaskModeRaw)
            oFormulario.CargarTodosLosGrupos
            
            If oFormulario.ExisteCampoImporte Then
                bMonedaImporte = oFormulario.ExisteCampoGSFormulario(TipoCampoGS.Moneda)
                Set oFormulario = Nothing
                If Not bMonedaImporte Then
                    oMensajes.mensajeGenericoOkOnly m_sFormAsociadoNovalido & vbCrLf & m_sDebeExistirMoneda, Exclamation
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If
    End If
    
    'Realiza todas las comprobaciones del pedido directo:
    Dim bComprobar As Boolean
    Dim iResult As Integer
    Select Case Cell.Column.key
        Case "PUB"
            If Cell.Row.Cells("PEDIDO_DIRECTO").Value <> "" And Not IsNull(Cell.Row.Cells("PEDIDO_DIRECTO").Value) Then
                If NewValue = 1 Then bComprobar = True
            End If
        Case "FORM"
            If Cell.Row.Cells("PEDIDO_DIRECTO").Value <> "" And Not IsNull(Cell.Row.Cells("PEDIDO_DIRECTO").Value) Then bComprobar = True
        Case "PEDIDO_DIRECTO"
            If Not IsNull(NewValue) And (Cell.Value = "" Or IsNull(Cell.Value)) Then bComprobar = True
    End Select
    If bComprobar Then
        If Not IsNull(Cell.Row.Cells("ID_FORM").Value) And Cell.Row.Cells("ID_FORM").Value <> "" Then
            If g_oSolicitudSeleccionada Is Nothing Then
                Set g_oSolicitudSeleccionada = oFSGSRaiz.Generar_CSolicitud
            End If
            Set g_oSolicitudSeleccionada.Formulario = oFSGSRaiz.Generar_CFormulario
            g_oSolicitudSeleccionada.Formulario.Id = Cell.Row.Cells("ID_FORM").Value
            iResult = g_oSolicitudSeleccionada.ComprobarCompatiblePedDirect
            If iResult <> 0 Then
                oMensajes.FormularioNoPedidoDirecto iResult
                Cancel = True
                Exit Sub
            End If
        End If
    End If
End Sub

''' <summary>Comprueba el cambio de tipo de solicitud</summary>
''' <param name="Cell">celda del tipo de solicitud</param>
''' <param name="TipoSolicit">Tipo de solicitud</param>
''' <remarks>Llamada desde: ssSOLConfig_AfterCellListCloseUp, ssSOLConfig_BeforeCellUpdate</remarks>

Private Function ComprobarCambioTipoSolicitud(ByVal Cell As UltraGrid.SSCell, ByVal TipoSolicit As Integer) As Boolean
    Dim oFormulario As CFormulario
    Dim vCampos() As Integer
    
    ComprobarCambioTipoSolicitud = True
    
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = Cell.Row.Cells("ID_FORM").GetText(ssMaskModeIncludeBoth)
    
    Select Case TipoSolicit
        Case TipoSolicitud.Encuesta
            'Comprobar que hay un campo proveedor fuera de desglose y es �nico
            If Not oFormulario.ComprobarCompatibleEncuesta Then
                oMensajes.FormularioNoCompatibleEncuesta
                ComprobarCambioTipoSolicitud = False
            End If
    
        Case TipoSolicitud.Contrato
            'Comprueba que en el formulario existan los campos obligatorios (descripci�n breve, descripci�n detallada y archivo contrato).
            vCampos = oFormulario.ComprobarCompatibleContrato
            
            If UBound(vCampos) > 0 Then
                oMensajes.FormularioNoValido (vCampos)
                ComprobarCambioTipoSolicitud = False
                
                If Cell.Column.key = "TIPO" Then
                    Cell.Row.Cells("WORKFL_MODIF").Column.Style = UltraGrid.ssStyleEdit
                    Cell.Row.Cells("WORKFL_MODIF").Activation = ssActivationDisabled
                    Cell.Row.Cells("WORKFL_MODIF").Appearance.Backcolor = vbButtonFace
                    
                    Cell.Row.Cells("WORKFL_BAJA").Column.Style = UltraGrid.ssStyleEdit
                    Cell.Row.Cells("WORKFL_BAJA").Activation = ssActivationDisabled
                    Cell.Row.Cells("WORKFL_BAJA").Appearance.Backcolor = vbButtonFace
                End If
            End If
            
        Case TipoSolicitud.AUTOFACTURA
            'Comprueba que en el formulario existan el campo "Desglose factura".
            If Not oFormulario.ComprobarCompatibleFactura Then
                oMensajes.RequisitoSolicitudTipoFactura (1)
                m_TipoSolicitudNoEsFactura = 1
                ComprobarCambioTipoSolicitud = False
            End If
                                    
        Case TipoSolicitud.SolicitudDePedidoCatalogo, TipoSolicitud.SolicitudDePedidoContraPedidoAbierto
            'Comprobamos si el tipo de solicitud es de pedido, que el formulario tenga el campo Desglose de pedido
            If Not oFormulario.ComprobarCompatibleSolicitudDePedido Then
                oMensajes.RequisitoSolicitudTipoSolicitudDePedido (2)
                m_TipoSolicitudNoEsPedido = 1
                ComprobarCambioTipoSolicitud = False
                ssSOLConfig.CancelUpdate
            End If
                                    
        Case Else
            'Comprueba que en el formulario existan el campo "Desglose factura".
            If oFormulario.ComprobarCompatibleFactura Then
                'Incidencia Pruebas 31900.7 / 2013 / 79 permite cambiar una solicitud de un tipo distinto a una solicitud de factura
                m_TipoSolicitudNoEsFactura = 1
                If m_ContadorTipoSolicitudNoEsFactura = 0 Then
                    oMensajes.RequisitoSolicitudTipoFactura (2)
                    m_ContadorTipoSolicitudNoEsFactura = 1
                Else
                    If m_ContadorTipoSolicitudNoEsFactura = 3 Then
                        m_ContadorTipoSolicitudNoEsFactura = 0
                    Else
                        m_ContadorTipoSolicitudNoEsFactura = m_ContadorTipoSolicitudNoEsFactura + 1
                    End If
                End If
                ComprobarCambioTipoSolicitud = False
            End If
            
            'Comprueba si en el formulario existe el campo "Desglose de pedido"
            If oFormulario.ComprobarCompatibleSolicitudDePedido Then
                m_TipoSolicitudNoEsPedido = 1
                oMensajes.RequisitoSolicitudTipoSolicitudDePedido (1)
                If m_ContadorTipoSolicitudNoEsPedido = 0 Then
                    m_ContadorTipoSolicitudNoEsPedido = 1
                Else
                    If m_ContadorTipoSolicitudNoEsPedido = 3 Then
                        m_ContadorTipoSolicitudNoEsPedido = 0
                    Else
                        m_ContadorTipoSolicitudNoEsPedido = m_ContadorTipoSolicitudNoEsPedido + 1
                    End If
            
                End If
                ComprobarCambioTipoSolicitud = False
            End If
    End Select
    
    Set oFormulario = Nothing
End Function

Private Sub ssSOLConfig_BeforeRowDeactivate(ByVal Cancel As UltraGrid.SSReturnBoolean)
    'Antes de que se mueva a otra fila hago las comprobaciones
    Dim oCelda As SSCell
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    Dim oSolicitud As CSolicitud
    Dim oIdi As CIdioma
    Dim oPeticionario As CPeticionario
    Dim oIdioma As CIdioma
    Dim nodx As node
    Dim oCarpeta As Object
        
    If ssSOLConfig.ActiveRow.DataChanged = False Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If g_udtAccion = ACCSolConfAnyadir Then
        Set nodx = tvwCarpetasSolicit.selectedItem

        If Not nodx Is Nothing Then
            For Each oCelda In ssSOLConfig.ActiveRow.Cells
                If oCelda.GetText(ssMaskModeIncludeBoth) = "" Then
                   Select Case oCelda.Column.key
                       Case "ID_TIPO"
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido m_sITipo
                            If Not Cancel Is Nothing Then
                                Cancel = True
                            End If
                            If Me.Visible Then ssSOLConfig.SetFocus
                            Exit Sub
                                                  
                       Case "COD"
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido m_sICodigo
                            If Not Cancel Is Nothing Then
                                Cancel = True
                            End If
                            If Me.Visible Then ssSOLConfig.SetFocus
                            Exit Sub
                           
                       Case "DEN"
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido m_sIDen
                            If Not Cancel Is Nothing Then
                                Cancel = True
                            End If
                            If Me.Visible Then ssSOLConfig.SetFocus
                            Exit Sub
        
                       Case "FORM", "ID_FORM"
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido m_sIForm
                            If Not Cancel Is Nothing Then
                                Cancel = True
                            End If
                            If Me.Visible Then ssSOLConfig.SetFocus
                            Exit Sub
                           
                           
                        Case "GESTOR_DEN", "GESTOR"
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido m_sIGestor
                            If Not Cancel Is Nothing Then
                                Cancel = True
                            End If
                            If Me.Visible Then ssSOLConfig.SetFocus
                            Exit Sub

                        Case "ID_WORKF", "WORKFL"
                            If oCelda.Row.Cells("TIPO_SOLICIT").Value <> TipoSolicitud.Certificados And oCelda.Row.Cells("TIPO_SOLICIT").Value <> TipoSolicitud.NoConformidades Then
                                Screen.MousePointer = vbNormal
                                oMensajes.NoValido m_sIWork
                                If Not Cancel Is Nothing Then
                                    Cancel = True
                                End If
                                If Me.Visible Then ssSOLConfig.SetFocus
                                Exit Sub
                            End If
                   End Select
                End If
            Next
            
            'A�ade un nuevo tipo de solicitud:
            Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
            oSolicitud.Codigo = ssSOLConfig.ActiveRow.Cells("COD").Value
            oSolicitud.Tipo = ssSOLConfig.ActiveRow.Cells("ID_TIPO").Value
            
            Set oSolicitud.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
            For Each oIdioma In m_oIdiomas
                oSolicitud.Denominaciones.Add oIdioma.Cod, ssSOLConfig.ActiveRow.Cells("DEN")
            Next
            
            If ssSOLConfig.ActiveRow.Cells("PEDIDO_DIRECTO").Value <> "" Then
                oSolicitud.PedidoDirecto = StrToDbl0(ssSOLConfig.ActiveRow.Cells("PEDIDO_DIRECTO").Value)
            Else
                oSolicitud.PedidoDirecto = Null
            End If
            oSolicitud.Publicado = GridCheckToBoolean(ssSOLConfig.ActiveRow.Cells("PUB").Value)
                    
            Set oSolicitud.Formulario = Nothing
            Set oSolicitud.Formulario = oFSGSRaiz.Generar_CFormulario
            oSolicitud.Formulario.Id = ssSOLConfig.ActiveRow.Cells("ID_FORM").Value
            
            Set oSolicitud.Gestor = Nothing
            Set oSolicitud.Gestor = oFSGSRaiz.Generar_CPersona
            oSolicitud.Gestor.Cod = ssSOLConfig.ActiveRow.Cells("GESTOR").Value
            oSolicitud.Gestor.CargarTodosLosDatos True
    
            Set oSolicitud.Workflow = Nothing
            If ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value <> "" Then
                Set oSolicitud.Workflow = oFSGSRaiz.Generar_CWorkflow
                oSolicitud.Workflow.Id = ssSOLConfig.ActiveRow.Cells("ID_WORKF").Value
            Else
                If oSolicitud.Tipo = TipoSolicitud.NoConformidades Then cmdAnyadirPet.Enabled = True
            End If

            Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
            If Not oCarpeta Is Nothing Then
                Select Case oCarpeta.Nivel
                    Case 1
                        oSolicitud.CSN1 = oCarpeta.Id
                        oSolicitud.CSN2 = 0
                        oSolicitud.CSN3 = 0
                        oSolicitud.CSN4 = 0
                    Case 2
                        oSolicitud.CSN1 = 0
                        oSolicitud.CSN2 = oCarpeta.Id
                        oSolicitud.CSN3 = 0
                        oSolicitud.CSN4 = 0
                    Case 3
                        oSolicitud.CSN1 = 0
                        oSolicitud.CSN2 = 0
                        oSolicitud.CSN3 = oCarpeta.Id
                        oSolicitud.CSN4 = 0
                    Case 4
                        oSolicitud.CSN1 = 0
                        oSolicitud.CSN2 = 0
                        oSolicitud.CSN3 = 0
                        oSolicitud.CSN4 = oCarpeta.Id
                End Select
            Else
                oSolicitud.CSN1 = 0
                oSolicitud.CSN2 = 0
                oSolicitud.CSN3 = 0
                oSolicitud.CSN4 = 0
            End If
           
            Set oIBaseDatos = oSolicitud
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos = True Then  'comprueba que no exista una solicitud con ese c�digo
                Screen.MousePointer = vbNormal
                oMensajes.ExisteTipoSolicitud
                Set oSolicitud = Nothing
                Set oIBaseDatos = Nothing
                If Me.Visible Then ssSOLConfig.SetFocus
                Exit Sub
            End If
            
            teserror = oIBaseDatos.AnyadirABaseDatos
           
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                If Not Cancel Is Nothing Then
                    Cancel = True
                End If
                Set m_oIBaseDatos = Nothing
                Set oSolicitud = Nothing
                If Me.Visible Then ssSOLConfig.SetFocus
                Exit Sub
                
            Else
                ssSOLConfig.ActiveRow.Cells("FECACT").Value = oSolicitud.FECACT
                ssSOLConfig.ActiveRow.Cells("ID_SOL").Value = oSolicitud.Id
                
                RegistrarAccion g_udtAccion, "Id:" & oSolicitud.Id
                
                'si el formulario es multiidioma mostramos la pantalla para introducir las denominaciones en todos los idiomas:
                If ssSOLConfig.ActiveRow.Cells("MULT").Value = "1" Then
                    Screen.MousePointer = vbNormal
                    Set g_oSolicitudSeleccionada = oSolicitud
                    For Each oIdi In m_oIdiomas
                        If g_oSolicitudSeleccionada.Denominaciones.Item(CStr(oIdi.Cod)) Is Nothing Then
                            g_oSolicitudSeleccionada.Denominaciones.Add oIdi.Cod, ""
                        End If
                    Next
                    ssSOLConfig.ActiveCell = ssSOLConfig.ActiveRow.Cells("DEN")
                    ssSOLConfig_ClickCellButton ssSOLConfig.ActiveRow.Cells("DEN")
                End If
                
                If basParametros.gParametrosGenerales.gbAccesoQANoConformidad = True Then
                    If Not oSolicitud.Estados Is Nothing Then
                        ssSOLConfig.ActiveRow.Cells("NUM_ESTADOS").Value = oSolicitud.Estados.Count
                        If oSolicitud.Estados.Count > 0 Then
                            ssSOLConfig.ActiveRow.Cells("ESTADOS").Value = "..."
                        Else
                            ssSOLConfig.ActiveRow.Cells("ESTADOS").Value = ""
                        End If
                    End If
                End If
                
                ssSOLConfig.ActiveRow.Cells("NUM_PLANTILLAS").Value = oSolicitud.NumeroDePlantillas
                If ssSOLConfig.ActiveRow.Cells("NUM_PLANTILLAS").Value > 0 Then
                    ssSOLConfig.ActiveRow.Cells("CONTRATOS").Value = "..."
                Else
                    ssSOLConfig.ActiveRow.Cells("CONTRATOS").Value = ""
                End If
                
                ssSOLConfig.ActiveRow.Cells("NUM_EMPRESAS").Value = oSolicitud.NumeroDeEmpresas
                If ssSOLConfig.ActiveRow.Cells("NUM_EMPRESAS").Value > 0 Then
                    ssSOLConfig.ActiveRow.Cells("EMPRESAS").Value = "..."
                Else
                    ssSOLConfig.ActiveRow.Cells("EMPRESAS").Value = ""
                End If
            End If
            
            Set oSolicitud = Nothing
            Set oIBaseDatos = Nothing
    
            picNavigate.Visible = True
            picEdicion.Visible = False
                
            g_udtAccion = ACCSolConfConsultar
        
            'Si el formulario es multiidioma se muestra la pantalla para rellenar la denominaci�n en todos los idiomas:
            If ssSOLConfig.ActiveRow.Cells("MULT").Value = 1 Then
                ssSOLConfig_ClickCellButton ssSOLConfig.ActiveRow.Cells("DEN")
            End If
    
            SolicitudSeleccionada
        Else
            ssSOLConfig.ActiveRow.Delete
            g_udtAccion = ACCSolConfConsultar
        End If
        
    ElseIf g_udtAccion = AccionesSummit.ACCSolConfAnyaPet Then
        'Est� a�adiento un peticionario:
        Set oPeticionario = oFSGSRaiz.Generar_CPeticionario
        oPeticionario.Solicitud = ssSOLConfig.ActiveRow.GetParent.Cells("ID_SOL").Value
        oPeticionario.UON1 = ssSOLConfig.ActiveRow.Cells("UON1").Value
        oPeticionario.UON2 = ssSOLConfig.ActiveRow.Cells("UON2").Value
        oPeticionario.UON3 = ssSOLConfig.ActiveRow.Cells("UON3").Value
        oPeticionario.CodDep = ssSOLConfig.ActiveRow.Cells("DEPCOD").Value
        oPeticionario.Cod = ssSOLConfig.ActiveRow.Cells("PER").Value
                
        Set oIBaseDatos = oPeticionario
    
        teserror = oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Set m_oIBaseDatos = Nothing
            Set oPeticionario = Nothing
            If Me.Visible Then ssSOLConfig.SetFocus
            Exit Sub
            
        Else
        RegistrarAccion g_udtAccion, "Id:" & oPeticionario.Id
            ssSOLConfig.ActiveRow.Cells("ID_PET").Value = oPeticionario.Id
        End If
        
        Set oPeticionario = Nothing
        
        Set oIBaseDatos = Nothing

        picNavigate.Visible = True
        picEdicion.Visible = False
                
        g_udtAccion = ACCSolConfConsultar
        
    ElseIf g_udtAccion = AccionesSummit.ACCSolConfAnyaNotif Then
        Set oPeticionario = oFSGSRaiz.Generar_CPeticionario
        oPeticionario.Solicitud = ssSOLConfig.ActiveRow.GetParent.Cells("ID_SOL").Value
        oPeticionario.UON1 = ssSOLConfig.ActiveRow.Cells("UON1").Value
        oPeticionario.UON2 = ssSOLConfig.ActiveRow.Cells("UON2").Value
        oPeticionario.UON3 = ssSOLConfig.ActiveRow.Cells("UON3").Value
        oPeticionario.CodDep = ssSOLConfig.ActiveRow.Cells("DEPCOD").Value
        oPeticionario.Cod = ssSOLConfig.ActiveRow.Cells("PER").Value
                
        teserror = oPeticionario.AnyadirABaseDatosComoNotificado
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Set m_oIBaseDatos = Nothing
            Set oPeticionario = Nothing
            If Me.Visible Then ssSOLConfig.SetFocus
            Exit Sub
            
        Else
            RegistrarAccion g_udtAccion, "Notificado - Id:" & oPeticionario.Id
            ssSOLConfig.ActiveRow.Cells("ID_PET").Value = oPeticionario.Id
        End If

        
        Set oPeticionario = Nothing
        
        Set oIBaseDatos = Nothing

        picNavigate.Visible = True
        picEdicion.Visible = False
                
        g_udtAccion = ACCSolConfConsultar
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub ssSOLConfig_BeforeRowUpdate(ByVal Row As UltraGrid.SSRow, ByVal Cancel As UltraGrid.SSReturnBoolean)
Dim oIdioma As CIdioma

    If Row.DataChanged = False Then Exit Sub
    If m_bRespetar Then Exit Sub
    
    If Me.ActiveControl.Name = "cmdDeshacer" Then
        If g_udtAccion = ACCSolConfAnyadir Or g_udtAccion = ACCSolConfAnyaPet Or g_udtAccion = ACCSolConfAnyaNotif Then
        Else
            Cancel = True
            picNavigate.Visible = True
            picEdicion.Visible = False
            g_udtAccion = ACCSolConfConsultar
        End If
        Exit Sub
    End If
    
    If g_udtAccion = ACCSolConfAnyadir Then
        If IsNull(Row.Cells("COD").Value) Or Row.Cells("COD").Value = "" Then
            oMensajes.NoValido m_sICodigo
            Cancel = True
            If Me.Visible Then ssSOLConfig.SetFocus
            Exit Sub
        End If
        If IsNull(Row.Cells("DEN").Value) Or Row.Cells("DEN").Value = "" Then
            oMensajes.NoValido m_sIDen
            Cancel = True
            If Me.Visible Then ssSOLConfig.SetFocus
            Exit Sub
        End If
        If g_oSolicitudSeleccionada.Denominaciones Is Nothing Then
            oMensajes.NoValido m_sIDen
            Cancel = True
            If Me.Visible Then ssSOLConfig.SetFocus
            Exit Sub
        End If
        If g_oSolicitudSeleccionada.Descripciones Is Nothing Then
            oMensajes.NoValido m_sIDescr
            Cancel = True
            If Me.Visible Then ssSOLConfig.SetFocus
            Exit Sub
        End If
        If Row.Cells("FORM").Value = "" Then
            oMensajes.NoValido m_sIForm
            Cancel = True
            If Me.Visible Then ssSOLConfig.SetFocus
            Exit Sub
        End If
        If Row.Cells("MULT").Value = 1 Then
            For Each oIdioma In m_oIdiomas
                If g_oSolicitudSeleccionada.Denominaciones.Item(oIdioma.Cod) Is Nothing Then
                    oMensajes.NoValido m_sIDen
                    Cancel = True
                    If Me.Visible Then ssSOLConfig.SetFocus
                    Exit Sub
                ElseIf g_oSolicitudSeleccionada.Denominaciones.Item(oIdioma.Cod).Den = "" Then
                    oMensajes.NoValido m_sIDen
                    Cancel = True
                    If Me.Visible Then ssSOLConfig.SetFocus
                    Exit Sub
                End If
                If g_oSolicitudSeleccionada.Descripciones.Item(oIdioma.Cod) Is Nothing Then
                    oMensajes.NoValido m_sIDescr
                    Cancel = True
                    If Me.Visible Then ssSOLConfig.SetFocus
                    Exit Sub
                ElseIf g_oSolicitudSeleccionada.Descripciones.Item(oIdioma.Cod).Den = "" Then
                    oMensajes.NoValido m_sIDescr
                    Cancel = True
                    If Me.Visible Then ssSOLConfig.SetFocus
                    Exit Sub
                End If
                
            Next
        End If
        
    End If
End Sub

Private Sub ssSOLConfig_BeforeRowsDeleted(ByVal Rows As UltraGrid.SSSelectedRows, ByVal DisplayPromptMsg As UltraGrid.SSReturnBoolean, ByVal Cancel As UltraGrid.SSReturnBoolean)
    'para que no salte el mensaje de confirmaci�n de borrado de las filas
    DisplayPromptMsg = False
End Sub


Private Sub ssSOLConfig_CellChange(ByVal Cell As UltraGrid.SSCell)
    picNavigate.Visible = False
    picEdicion.Visible = True
End Sub

Private Sub ssSOLConfig_ClickCellButton(ByVal Cell As UltraGrid.SSCell)
Dim oCarpeta As Object
Dim teserror As TipoErrorSummit

    'Si se est� a�adiendo una solicitud,antes de introducir adjuntos,la configuraci�n,etc se guarda la solicitud en BD.

    'Excepto el workflow
    If g_udtAccion = AccionesSummit.ACCSolConfAnyadir And Cell.Column.key <> "WORKFL" Then Exit Sub
    
    Select Case Cell.Column.key
        Case "DEN", "DESCR"
            frmDescripAnya.g_sOrigen = "frmSOLConfiguracion"
            frmDescripAnya.g_bModif = g_bModifSolic
            g_bCancel = True
            frmDescripAnya.Show vbModal
            Screen.MousePointer = vbHourglass
            If Not g_bCancel And g_udtAccion <> ACCSolConfAnyadir Then
                teserror = g_oSolicitudSeleccionada.GuardarDenominaciones(Cell.Column.key)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                Else
                    RegistrarAccion AccionesSummit.ACCSolConfModificar, "ID:" & g_oSolicitudSeleccionada.Id
                    m_bRespetar = True
                    Cell.Row.Cells("FECACT").Value = g_oSolicitudSeleccionada.FECACT
                    m_bRespetar = False
                    If Cell.Column.key = "DEN" Then
                        m_bRespetar = True
                        Cell.Row.Cells("DEN").Value = NullToStr(g_oSolicitudSeleccionada.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                        ssSOLConfig.Update
                        m_bRespetar = False
                    End If
                    Screen.MousePointer = vbNormal
                End If
            End If
            
        Case "ADJUN"
            frmFormAdjuntos.g_sOrigen = "frmSOLConfiguracion"
            Set frmFormAdjuntos.g_oSolicitudSeleccionada = g_oSolicitudSeleccionada
            If Not ssSOLConfig.ActiveRow Is Nothing Then
                ssSOLConfig.ActiveRow.Update
            End If
            frmFormAdjuntos.g_bModif = g_bModifSolic
            frmFormAdjuntos.Show vbModal
        
        Case "CONF"

            If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Or Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.NoConformidades Then

                frmSOLCumplimentacion.g_bModificar = g_bModifSolic
                frmSOLCumplimentacion.g_lFormulario = Cell.Row.Cells("ID_FORM").Value
                frmSOLCumplimentacion.g_lWorkFlow = NullToDbl0(Cell.Row.Cells("ID_WORKF").Value)
                Set frmSOLCumplimentacion.g_oSolicitud = g_oSolicitudSeleccionada
                frmSOLCumplimentacion.Show vbModal
            ElseIf Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.SolicitudCompras Or Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Abono Then
                
                MostrarfrmSOLCumplimentacionGS Cell.Row.Cells("ID_FORM").Value, g_oSolicitudSeleccionada.Id, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oGestorIdiomas, oMensajes
                                
            End If
            
        Case "UON_DEN"
            If m_bModifPetic = False Then Exit Sub
            frmSELUO.sOrigen = "frmSOLConfiguracion"
            frmSELUO.bRUO = m_bRAltaPetUON
            frmSELUO.Show vbModal
            
        Case "ESTADOS"  'Solo para las no conformidades
            If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.NoConformidades Then
                Set frmSOLConfigEstados.g_oSolicitud = g_oSolicitudSeleccionada
                frmSOLConfigEstados.g_bModif = g_bModifSolic
                frmSOLConfigEstados.Show vbModal
            End If
        
        Case "CONTRATOS"
            If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Contrato Then
                Set frmParContr.g_oSolicitud = g_oSolicitudSeleccionada
                frmParContr.Show vbModal
            End If
        
        Case "WORKFL"
            If Not g_oSolicitudSeleccionada Is Nothing Then
                If Not ssSOLConfig.ActiveRow Is Nothing Then
                    ssSOLConfig.ActiveRow.Update
                End If
            End If
            If Cell.Row.HasChild Then
                oMensajes.HayPeticionarioSeleccionado
                Exit Sub
            End If
            If Not IsNull(Cell.Row.Cells("ID_FORM").Value) Then
                If Not IsNull(Cell.Row.Cells("DEN").Value) Then
                    If Cell.Row.Cells("TIPO").Value <> "" Then
                        If Cell.Row.Cells("TIPO_SOLICIT").Value <> TipoSolicitud.Certificados Then
                            If m_bModifFlujo Then
                                '1� Comprobamos que el Formulario tiene un campo Importe de Workflow (s�lo si es de solicitudes de compra)
                                If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.SolicitudCompras Or Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Express Or Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.pedido Then
                                    Dim bImporte As Boolean
                                    bImporte = ComprobarCampoImporteWorkflow(CLng(ssSOLConfig.ActiveRow.Cells("ID_FORM").Value))
                                    If Not bImporte Then
                                        oMensajes.FaltaCampoImporteWorkflow
                                        Exit Sub
                                    End If
                                End If
                                
                                '2� Hacemos todas las gestiones del Workflow
                                frmFlujosSelWorkflow.lIdFormulario = Cell.Row.Cells("ID_FORM").Value
                                frmFlujosSelWorkflow.sSolicitud = Cell.Row.Cells("DEN").Value
                                Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
                                If Not oCarpeta Is Nothing Then
                                    Select Case oCarpeta.Nivel
                                        Case 1
                                            frmFlujosSelWorkflow.CSN1 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 2
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 3
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 4
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = oCarpeta.Id
                                    End Select
                                Else
                                    frmFlujosSelWorkflow.CSN1 = 0
                                    frmFlujosSelWorkflow.CSN2 = 0
                                    frmFlujosSelWorkflow.CSN3 = 0
                                    frmFlujosSelWorkflow.CSN4 = 0
                                End If
                                If Not IsNull(Cell.Row.Cells("ID_WORKF").Value) Then
                                    frmFlujosSelWorkflow.lIdWorkflow_Act = Cell.Row.Cells("ID_WORKF").Value
                                End If
                                frmFlujosSelWorkflow.m_bModifFlujo = m_bModifFlujo
                                If Not g_oSolicitudSeleccionada Is Nothing Then
                                    frmFlujosSelWorkflow.g_lSolicitud = g_oSolicitudSeleccionada.Id
                                    frmFlujosSelWorkflow.g_lSolicitudTipo = g_oSolicitudSeleccionada.Tipo
                                End If
                                frmFlujosSelWorkflow.g_iSolicitudTipoTipo = Cell.Row.Cells("TIPO_SOLICIT").Value
                                frmFlujosSelWorkflow.sTipoWorkflow = "ALTA"
                                frmFlujosSelWorkflow.Show vbModal
                                Unload frmFlujosSelWorkflow
                            Else
                                frmFlujos.m_lIdFlujo = Cell.Row.Cells("ID_WORKF").Value
                                frmFlujos.m_sSolicitud = Cell.Row.Cells("FORM").Value
                                frmFlujos.lblDenFlujo.caption = Cell.Row.Cells("WORKFL").Value
                                frmFlujos.m_bModifFlujo = m_bModifFlujo
                                frmFlujos.m_lIdFormulario = Cell.Row.Cells("ID_FORM").Value
                                frmFlujos.g_iSolicitudTipoTipo = Cell.Row.Cells("TIPO_SOLICIT").Value
                                If Not g_oSolicitudSeleccionada Is Nothing Then
                                    frmFlujos.g_lSolicitud = g_oSolicitudSeleccionada.Id
                                    frmFlujos.g_lSolicitudTipo = g_oSolicitudSeleccionada.Tipo
                                    
                                End If
                                frmFlujos.Show vbModal
                                Unload frmFlujos
                            End If
                        End If
                    End If
                End If
            End If
        Case "WORKFL_MODIF"
            If Not g_oSolicitudSeleccionada Is Nothing Then
                If Not ssSOLConfig.ActiveRow Is Nothing Then
                    ssSOLConfig.ActiveRow.Update
                End If
            End If
            If Not IsNull(Cell.Row.Cells("ID_FORM").Value) Then
                If Not IsNull(Cell.Row.Cells("DEN").Value) Then
                    If Cell.Row.Cells("TIPO").Value <> "" Then
                        If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Contrato Then
                            If m_bModifFlujo Then
                                'Hacemos todas las gestiones del Workflow
                                frmFlujosSelWorkflow.lIdFormulario = Cell.Row.Cells("ID_FORM").Value
                                frmFlujosSelWorkflow.sSolicitud = Cell.Row.Cells("DEN").Value
                                Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
                                If Not oCarpeta Is Nothing Then
                                    Select Case oCarpeta.Nivel
                                        Case 1
                                            frmFlujosSelWorkflow.CSN1 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 2
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 3
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 4
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = oCarpeta.Id
                                    End Select
                                Else
                                    frmFlujosSelWorkflow.CSN1 = 0
                                    frmFlujosSelWorkflow.CSN2 = 0
                                    frmFlujosSelWorkflow.CSN3 = 0
                                    frmFlujosSelWorkflow.CSN4 = 0
                                End If
                                If Not IsNull(Cell.Row.Cells("ID_WORKF_MODIF").Value) Then
                                    frmFlujosSelWorkflow.lIdWorkflow_Act = Cell.Row.Cells("ID_WORKF_MODIF").Value
                                End If
                                frmFlujosSelWorkflow.m_bModifFlujo = m_bModifFlujo
                                If Not g_oSolicitudSeleccionada Is Nothing Then
                                    frmFlujosSelWorkflow.g_lSolicitud = g_oSolicitudSeleccionada.Id
                                    frmFlujosSelWorkflow.g_lSolicitudTipo = g_oSolicitudSeleccionada.Tipo
                                End If
                                frmFlujosSelWorkflow.g_iSolicitudTipoTipo = Cell.Row.Cells("TIPO_SOLICIT").Value
                                frmFlujosSelWorkflow.sTipoWorkflow = "MODIF"
                                frmFlujosSelWorkflow.Show vbModal
                                Unload frmFlujosSelWorkflow
                            Else
                                frmFlujos.m_lIdFlujo = Cell.Row.Cells("ID_WORKF_MODIF").Value
                                frmFlujos.m_sSolicitud = Cell.Row.Cells("FORM").Value
                                frmFlujos.lblDenFlujo.caption = Cell.Row.Cells("WORKFL_MODIF").Value
                                frmFlujos.m_bModifFlujo = m_bModifFlujo
                                frmFlujos.m_lIdFormulario = Cell.Row.Cells("ID_FORM").Value
                                frmFlujos.g_iSolicitudTipoTipo = Cell.Row.Cells("TIPO_SOLICIT").Value
                                If Not g_oSolicitudSeleccionada Is Nothing Then
                                    frmFlujos.g_lSolicitud = g_oSolicitudSeleccionada.Id
                                    frmFlujos.g_lSolicitudTipo = g_oSolicitudSeleccionada.Tipo
                                End If
                                frmFlujos.Show vbModal
                                Unload frmFlujos
                            End If
                        End If
                    End If
                End If
            End If
        Case "WORKFL_BAJA"
            If Not g_oSolicitudSeleccionada Is Nothing Then
                If Not ssSOLConfig.ActiveRow Is Nothing Then
                    ssSOLConfig.ActiveRow.Update
                End If
            End If
            If Not IsNull(Cell.Row.Cells("ID_FORM").Value) Then
                If Not IsNull(Cell.Row.Cells("DEN").Value) Then
                    If Cell.Row.Cells("TIPO").Value <> "" Then
                        If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Contrato Then
                            If m_bModifFlujo Then
                                'Hacemos todas las gestiones del Workflow
                                frmFlujosSelWorkflow.lIdFormulario = Cell.Row.Cells("ID_FORM").Value
                                frmFlujosSelWorkflow.sSolicitud = Cell.Row.Cells("DEN").Value
                                Set oCarpeta = DevolverCarpeta(Me.tvwCarpetasSolicit)
                                If Not oCarpeta Is Nothing Then
                                    Select Case oCarpeta.Nivel
                                        Case 1
                                            frmFlujosSelWorkflow.CSN1 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 2
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 3
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = oCarpeta.Id
                                            frmFlujosSelWorkflow.CSN4 = 0
                                        Case 4
                                            frmFlujosSelWorkflow.CSN1 = 0
                                            frmFlujosSelWorkflow.CSN2 = 0
                                            frmFlujosSelWorkflow.CSN3 = 0
                                            frmFlujosSelWorkflow.CSN4 = oCarpeta.Id
                                    End Select
                                Else
                                    frmFlujosSelWorkflow.CSN1 = 0
                                    frmFlujosSelWorkflow.CSN2 = 0
                                    frmFlujosSelWorkflow.CSN3 = 0
                                    frmFlujosSelWorkflow.CSN4 = 0
                                End If
                                If Not IsNull(Cell.Row.Cells("ID_WORKF_BAJA").Value) Then
                                    frmFlujosSelWorkflow.lIdWorkflow_Act = Cell.Row.Cells("ID_WORKF_BAJA").Value
                                End If
                                frmFlujosSelWorkflow.m_bModifFlujo = m_bModifFlujo
                                If Not g_oSolicitudSeleccionada Is Nothing Then
                                    frmFlujosSelWorkflow.g_lSolicitud = g_oSolicitudSeleccionada.Id
                                    frmFlujosSelWorkflow.g_lSolicitudTipo = g_oSolicitudSeleccionada.Tipo
                                End If
                                frmFlujosSelWorkflow.g_iSolicitudTipoTipo = Cell.Row.Cells("TIPO_SOLICIT").Value
                                frmFlujosSelWorkflow.sTipoWorkflow = "BAJA"
                                frmFlujosSelWorkflow.Show vbModal
                                Unload frmFlujosSelWorkflow
                            Else
                                frmFlujos.m_lIdFlujo = Cell.Row.Cells("ID_WORKF_BAJA").Value
                                frmFlujos.m_sSolicitud = Cell.Row.Cells("FORM").Value
                                frmFlujos.lblDenFlujo.caption = Cell.Row.Cells("WORKFL_BAJA").Value
                                frmFlujos.m_bModifFlujo = m_bModifFlujo
                                frmFlujos.m_lIdFormulario = Cell.Row.Cells("ID_FORM").Value
                                frmFlujos.g_iSolicitudTipoTipo = Cell.Row.Cells("TIPO_SOLICIT").Value
                                If Not g_oSolicitudSeleccionada Is Nothing Then
                                    frmFlujos.g_lSolicitud = g_oSolicitudSeleccionada.Id
                                    frmFlujos.g_lSolicitudTipo = g_oSolicitudSeleccionada.Tipo
                                End If
                                frmFlujos.Show vbModal
                                Unload frmFlujos
                            End If
                        End If
                    End If
                End If
            End If
            
        Case "EMPRESAS"
            Select Case Cell.Row.Cells("TIPO_SOLICIT").Value
                Case TipoSolicitud.Contrato, TipoSolicitud.AUTOFACTURA, TipoSolicitud.SolicitudDePedidoContraPedidoAbierto
                    Set frmSelEmpresas.g_oSolicitud = g_oSolicitudSeleccionada
                    frmSelEmpresas.g_sEmpresas = ssSOLConfig.ActiveRow.Cells("COD_EMPRESAS").Value
                    frmSelEmpresas.g_TipoSolicitud = Cell.Row.Cells("TIPO_SOLICIT").Value
                    frmSelEmpresas.Show vbModal
                    Dim sResultado As String
                    sResultado = g_oSolicitudSeleccionada.CadenaEmpresas
                    If sResultado <> "" Then
                        Dim sAux As String
                        sAux = Replace(sResultado, ",", "")
                        If sAux <> "" Then
                            ssSOLConfig.ActiveRow.Cells("EMPRESAS").Value = "..."
                        Else
                            ssSOLConfig.ActiveRow.Cells("EMPRESAS").Value = ""
                        End If
                        ssSOLConfig.ActiveRow.Cells("COD_EMPRESAS").Value = sResultado
                    End If
            End Select
            
    'Configuraci�n de las solicitudes de tipo Certificado
    Case "CONFIGURACION"
            If Cell.Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
                MostrarFormConfigCertificado oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, g_oSolicitudSeleccionada, oMensajes, gParametrosGenerales, oGestorSeguridad, oUsuarioSummit.Cod, _
                    basParametros.gLongitudesDeCodigos, gParametrosGenerales
                
                'Actualizar la FecAct de la solicitud si ha cambiado
                If g_oSolicitudSeleccionada.FECACT <> Cell.Row.Cells("FECACT").Value Then Cell.Row.Cells("FECACT").Value = g_oSolicitudSeleccionada.FECACT
            End If
    End Select
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub ssSOLConfig_DblClick()
    If ssSOLConfig.ActiveRow Is Nothing Then Exit Sub
    
    If ssSOLConfig.ActiveRow.Expanded = True Then
        ssSOLConfig.ActiveRow.CollapseAll
    Else
        ssSOLConfig.ActiveRow.Expanded = True
    End If
End Sub

Private Sub ssSOLConfig_Error(ByVal ErrorInfo As UltraGrid.SSError)
    'If ErrorInfo.Code = 3617 Then
        ErrorInfo.DisplayErrorDialog = False
    '    oMensajes.FaltaValor
    'End If
End Sub

Private Sub ssSOLConfig_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    InicializarGrid
End Sub

Private Sub InicializarGrid()
    'Inicializa el layout de la grid
    With ssSOLConfig.Bands(0)
        'En vez de Cargar el Layaut del archivo, ahora se crean las columnas por c�digo.

        If Not .Columns.Exists("TIPO") Then .Columns.Add "TIPO"
        If Not .Columns.Exists("GESTOR_DEN") Then .Columns.Add "GESTOR_DEN"
        If Not .Columns.Exists("DESCR") Then .Columns.Add "DESCR"
        If Not .Columns.Exists("ADJUN") Then .Columns.Add "ADJUN"
        If Not .Columns.Exists("CONF") Then .Columns.Add "CONF"
        
        If Not .Columns.Exists("PUBLICAR") Then .Columns.Add "PUBLICAR"
        
        If Not .Columns.Exists("WORKFL_MODIF") Then .Columns.Add "WORKFL_MODIF"
        If Not .Columns.Exists("WORKFL_BAJA") Then .Columns.Add "WORKFL_BAJA"
        If Not .Columns.Exists("ID_WORKF_MODIF") Then .Columns.Add "ID_WORKF_MODIF"
        If Not .Columns.Exists("ID_WORKF_BAJA") Then .Columns.Add "ID_WORKF_BAJA"
        
        If Not .Columns.Exists("NUM_PLANTILLAS") Then .Columns.Add "NUM_PLANTILLAS"
        If Not .Columns.Exists("NUM_EMPRESAS") Then .Columns.Add "NUM_EMPRESAS"
        
        .Columns("ID").Hidden = True
        .Columns("ID_TIPO").Hidden = True
        .Columns("NOM").Hidden = True
        .Columns("APE").Hidden = True
        .Columns("GESTOR").Hidden = True
        .Columns("MULT").Hidden = True
        .Columns("ID_FORM").Hidden = True
        .Columns("ID_WORKF").Hidden = True
        .Columns("ID_WORKF_MODIF").Hidden = True
        .Columns("ID_WORKF_BAJA").Hidden = True
        .Columns("FECACT").Hidden = True
        .Columns("NUM_ESTADOS").Hidden = True
        .Columns("NUM_PLANTILLAS").Hidden = True
        .Columns("NUM_EMPRESAS").Hidden = True
        .Columns("PUB").Hidden = True
        
        .Columns("TIPO").Header.VisiblePosition = 1
        .Columns("COD").Header.VisiblePosition = 2
        .Columns("DEN").Header.VisiblePosition = 3
        .Columns("GESTOR_DEN").Header.VisiblePosition = 4
        .Columns("DESCR").Header.VisiblePosition = 5
        .Columns("ADJUN").Header.VisiblePosition = 6
        .Columns("FORM").Header.VisiblePosition = 7
        .Columns("WORKFL").Header.VisiblePosition = 8
        .Columns("WORKFL_MODIF").Header.VisiblePosition = 9
        .Columns("WORKFL_BAJA").Header.VisiblePosition = 10
        .Columns("CONF").Header.VisiblePosition = 11
        
        .Columns("PUBLICAR").Header.VisiblePosition = 12
        
        'si tiene acceso exclusivo al m�dulo de solicitudes de compras no se muestra la columna del tipo de solicitud:
        If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
            .Columns("TIPO").Hidden = True
        Else
            .HeaderVisible = True
            .Header.caption = m_sISolicitudes
            
            .Columns("TIPO").Header.caption = m_sITipo
            .Columns("TIPO").Style = UltraGrid.ssStyleDropDown
            .Columns("TIPO").AutoEdit = False
            
        End If
        
        .Columns("TIPO_SOLICIT").Hidden = True
         
        .Columns("COD").Header.caption = m_sICodigo
        .Columns("DEN").Header.caption = m_sIDen
        
        .Columns("GESTOR_DEN").Header.caption = m_sIGestor
        .Columns("GESTOR_DEN").Style = UltraGrid.ssStyleDropDown
        .Columns("GESTOR_DEN").AutoEdit = False
        
        .Columns("DESCR").Header.caption = m_sIDescr
        .Columns("DESCR").Style = UltraGrid.ssStyleButton
        .Columns("DESCR").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
        
        .Columns("ADJUN").Header.caption = m_sIArchivos
        .Columns("ADJUN").Style = UltraGrid.ssStyleButton
        .Columns("ADJUN").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
        
        .Columns("FORM").Header.caption = m_sIForm
        .Columns("FORM").Style = UltraGrid.ssStyleDropDownList
        .Columns("FORM").AutoEdit = False
        
        .Columns("WORKFL").Header.caption = m_sIWork
        .Columns("WORKFL").Style = UltraGrid.ssStyleEditButton
        .Columns("WORKFL").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
        .Columns("WORKFL").AutoEdit = False
        
        .Columns("WORKFL_MODIF").Header.caption = m_sIWorkModificacion
        .Columns("WORKFL_MODIF").Style = UltraGrid.ssStyleEditButton
        .Columns("WORKFL_MODIF").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
        .Columns("WORKFL_MODIF").AutoEdit = False
        
        .Columns("WORKFL_BAJA").Header.caption = m_sIWorkBaja
        .Columns("WORKFL_BAJA").Style = UltraGrid.ssStyleEditButton
        .Columns("WORKFL_BAJA").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
        .Columns("WORKFL_BAJA").AutoEdit = False
                
        .Columns("CONF").Header.caption = m_sIConfiguracion
        .Columns("CONF").Style = UltraGrid.ssStyleButton
        .Columns("CONF").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
        
        .Columns("PUBLICAR").Header.caption = m_sIPub
        .Columns("PUBLICAR").Style = UltraGrid.ssStyleCheckBox
        
        .Columns("PUB").Header.caption = m_sIPub
        .Columns("PUB").Style = UltraGrid.ssStyleCheckBox
        
        .Columns("PEDIDO_DIRECTO").Hidden = True
        .Columns("PEDIDO_DIRECTO").Header.caption = m_sIPedDir
        .Columns("PEDIDO_DIRECTO").Format = "Standard"
        
        If Not .Columns.Exists("ID_SOL") Then .Columns.Add "ID_SOL"
        .Columns("ID_SOL").Hidden = True
        
        .Columns("NUM_ADJUNTOS").Hidden = True
        
        If basParametros.gParametrosGenerales.gbAccesoQANoConformidad = True Then
            If Not .Columns.Exists("ESTADOS") Then .Columns.Add "ESTADOS"
            .Columns("ESTADOS").Header.caption = m_sIEstados
            .Columns("ESTADOS").Style = UltraGrid.ssStyleButton
            .Columns("ESTADOS").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
            .Columns("ESTADOS").CellAppearance.TextAlign = UltraGrid.ssAlignCenter
            .Columns("ESTADOS").Width = ssSOLConfig.Width * 0.084
        End If
        
        If Not .Columns.Exists("CONTRATOS") Then .Columns.Add "CONTRATOS"
        .Columns("CONTRATOS").Header.caption = m_sIContratos
        .Columns("CONTRATOS").Style = UltraGrid.ssStyleButton
        .Columns("CONTRATOS").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
        .Columns("CONTRATOS").CellAppearance.TextAlign = UltraGrid.ssAlignCenter
        .Columns("CONTRATOS").Width = ssSOLConfig.Width * 0.084
        
        If Not .Columns.Exists("EMPRESAS") Then .Columns.Add "EMPRESAS"
        .Columns("EMPRESAS").Header.caption = m_sIEmpresas
        .Columns("EMPRESAS").Style = UltraGrid.ssStyleButton
        .Columns("EMPRESAS").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
        .Columns("EMPRESAS").CellAppearance.TextAlign = UltraGrid.ssAlignCenter
        .Columns("EMPRESAS").Width = ssSOLConfig.Width * 0.084
        
        If Not .Columns.Exists("COD_EMPRESAS") Then .Columns.Add "COD_EMPRESAS"
        .Columns("COD_EMPRESAS").Hidden = True
               
        If Not .Columns.Exists("CONFIGURACION") Then .Columns.Add "CONFIGURACION"
        .Columns("CONFIGURACION").Header.caption = m_sIConfiguracionCertificado
        .Columns("CONFIGURACION").Style = UltraGrid.ssStyleButton
        .Columns("CONFIGURACION").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
        .Columns("CONFIGURACION").CellAppearance.TextAlign = UltraGrid.ssAlignCenter
        .Columns("CONFIGURACION").Width = ssSOLConfig.Width * 0.084

        
        
        If g_bModifSolic = False Then
            .Columns("TIPO").Activation = ssActivationActivateNoEdit
            .Columns("COD").Activation = ssActivationActivateNoEdit
            .Columns("DEN").Activation = ssActivationActivateNoEdit
            .Columns("GESTOR_DEN").Activation = ssActivationActivateNoEdit
            .Columns("DESCR").Activation = ssActivationActivateNoEdit
            .Columns("ADJUN").Activation = ssActivationActivateNoEdit
            .Columns("FORM").Activation = ssActivationActivateNoEdit
            .Columns("WORKFL").Activation = ssActivationActivateNoEdit
            .Columns("WORKFL_MODIF").Activation = ssActivationActivateNoEdit
            .Columns("WORKFL_BAJA").Activation = ssActivationActivateNoEdit
            .Columns("CONF").Activation = ssActivationActivateNoEdit
            .Columns("PUB").Activation = ssActivationActivateNoEdit
            .Columns("PEDIDO_DIRECTO").Activation = ssActivationActivateNoEdit
        End If
        
    End With
    
    With ssSOLConfig.Bands(1)
        .HeaderVisible = False
        .Header.caption = m_sIPeticionarios
        
            .Columns("ID").Hidden = True
            .Columns("SOLICITUD").Hidden = True
            .Columns("UON1").Hidden = True
            .Columns("UON1DEN").Hidden = True
            .Columns("UON2").Hidden = True
            .Columns("UON2DEN").Hidden = True
            .Columns("UON3").Hidden = True
            .Columns("UON3DEN").Hidden = True
            .Columns("DEPCOD").Hidden = True
            .Columns("PER").Hidden = True
            .Columns("NOM").Hidden = True
            .Columns("APE").Hidden = True
    
            'A�ade las columnas:
            If Not .Columns.Exists("ID_PET") Then .Columns.Add "ID_PET"
            .Columns("ID_PET").Hidden = True
            
            If Not .Columns.Exists("UON_DEN") Then .Columns.Add "UON_DEN", m_sIUON
            .Columns("UON_DEN").Activation = UltraGrid.ssActivationActivateOnly
            
            If Not .Columns.Exists("PER_DEN") Then .Columns.Add "PER_DEN", m_sIPer
            .Columns("PER_DEN").Activation = UltraGrid.ssActivationActivateOnly
            
            .Columns("DEPDEN").Header.caption = m_sIDep
            .Columns("DEPDEN").Activation = UltraGrid.ssActivationActivateOnly
            
            If Not .Groups.Exists("PET") Then
                .Groups.Add "PET"
                .Groups(0).Header.caption = m_sIPeticionarios
                .Groups(0).Columns.Add "UON_DEN"
                .Groups(0).Columns.Add "DEPDEN"
                .Groups(0).Columns.Add "PER_DEN"
            End If
            
            If m_bModifPetic = False Then
                .Columns("UON_DEN").Activation = ssActivationActivateNoEdit
                .Columns("DEPDEN").Activation = ssActivationActivateNoEdit
                .Columns("PER_DEN").Activation = ssActivationActivateNoEdit
            End If
        
            .Groups(0).Columns("UON_DEN").Width = ssSOLConfig.Width * 0.31
    '''        .Groups(0).Columns("DEPDEN").Width = ssSOLConfig.Width * 0.31
            .Groups(0).Columns("PER_DEN").Width = ssSOLConfig.Width * 0.31
        
    End With
    
    '*********************************************************************
    'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
    '*********************************************************************
    With ssSOLConfig.Bands(2)
        .HeaderVisible = False
        .Header.caption = m_sINotificados
        
        .Columns("ID").Hidden = True
        .Columns("SOLICITUD").Hidden = True
        .Columns("UON1").Hidden = True
        .Columns("UON1DEN").Hidden = True
        .Columns("UON2").Hidden = True
        .Columns("UON2DEN").Hidden = True
        .Columns("UON3").Hidden = True
        .Columns("UON3DEN").Hidden = True
        .Columns("DEPCOD").Hidden = True
        .Columns("PER").Hidden = True
        .Columns("NOM").Hidden = True
        .Columns("APE").Hidden = True

        'A�ade las columnas:
        If Not .Columns.Exists("ID_PET") Then .Columns.Add "ID_PET"
        .Columns("ID_PET").Hidden = True
        
        If Not .Columns.Exists("UON_DEN") Then .Columns.Add "UON_DEN", m_sIUON
        .Columns("UON_DEN").Activation = UltraGrid.ssActivationActivateOnly
        
        If Not .Columns.Exists("PER_DEN") Then .Columns.Add "PER_DEN", m_sIPer
        .Columns("PER_DEN").Activation = UltraGrid.ssActivationActivateOnly
        
        .Columns("DEPDEN").Header.caption = m_sIDep
        .Columns("DEPDEN").Activation = UltraGrid.ssActivationActivateOnly
        
        If Not .Groups.Exists("PET") Then
            .Groups.Add "PET"
            .Groups(0).Header.caption = m_sINotificados
            .Groups(0).Columns.Add "UON_DEN"
            .Groups(0).Columns.Add "DEPDEN"
            .Groups(0).Columns.Add "PER_DEN"
        End If
        
        If m_bModifPetic = False Then
            .Columns("UON_DEN").Activation = ssActivationActivateNoEdit
            .Columns("DEPDEN").Activation = ssActivationActivateNoEdit
            .Columns("PER_DEN").Activation = ssActivationActivateNoEdit
        End If
    
        .Groups(0).Columns("UON_DEN").Width = ssSOLConfig.Width * 0.31
'''        .Groups(0).Columns("DEPDEN").Width = ssSOLConfig.Width * 0.31
        .Groups(0).Columns("PER_DEN").Width = ssSOLConfig.Width * 0.31
        
    End With
    '*********************************************************************

    ssSOLConfig.Override.SelectTypeCol = UltraGrid.ssSelectTypeNone
    ssSOLConfig.Bands(0).Override.SelectTypeRow = UltraGrid.ssSelectTypeSingle
    ssSOLConfig.Bands(1).Override.SelectTypeRow = UltraGrid.ssSelectTypeExtended
    '*********************************************************************
    'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
    '*********************************************************************
    ssSOLConfig.Bands(2).Override.SelectTypeRow = UltraGrid.ssSelectTypeExtended
    '*********************************************************************
    
    ssSOLConfig.Override.AllowColMoving = ssAllowColMovingNotAllowed
    ssSOLConfig.Override.AllowGroupMoving = ssAllowGroupMovingNotAllowed
    
    'Establecer los anchos de las columnas
    ssSOLConfig.Bands(0).Columns("TIPO").Width = 1680
    ssSOLConfig.Bands(0).Columns("COD").Width = 825
    ssSOLConfig.Bands(0).Columns("DEN").Width = 2535
    ssSOLConfig.Bands(0).Columns("GESTOR_DEN").Width = 1530
    ssSOLConfig.Bands(0).Columns("DESCR").Width = 645
    ssSOLConfig.Bands(0).Columns("ADJUN").Width = 855
    ssSOLConfig.Bands(0).Columns("FORM").Width = 1365
    ssSOLConfig.Bands(0).Columns("WORKFL").Width = 1365
    ssSOLConfig.Bands(0).Columns("WORKFL_MODIF").Width = 1365
    ssSOLConfig.Bands(0).Columns("WORKFL_BAJA").Width = 1365
    ssSOLConfig.Bands(0).Columns("CONF").Width = 960
    ssSOLConfig.Bands(0).Columns("PUB").Width = 825
    ssSOLConfig.Bands(0).Columns("PEDIDO_DIRECTO").Width = 1200
    If basParametros.gParametrosGenerales.gbAccesoQANoConformidad = True Then
        ssSOLConfig.Bands(0).Columns("ESTADOS").Width = 1200
    End If
    ssSOLConfig.Bands(0).Columns("CONTRATOS").Width = 1200
    ssSOLConfig.Bands(0).Columns("EMPRESAS").Width = 1200
    ssSOLConfig.Bands(0).Columns("CONFIGURACION").Width = 1200
    ssSOLConfig.Bands(1).Columns("UON_DEN").Width = 4425
    ssSOLConfig.Bands(1).Columns("DEPDEN").Width = 4425
    ssSOLConfig.Bands(1).Columns("PER_DEN").Width = 4425
    '*********************************************************************
    'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
    '*********************************************************************
    ssSOLConfig.Bands(2).Columns("UON_DEN").Width = 4425
    ssSOLConfig.Bands(2).Columns("DEPDEN").Width = 4425
    ssSOLConfig.Bands(2).Columns("PER_DEN").Width = 4425
    '*********************************************************************

End Sub

Public Sub ConfigurarInterfazSeguridad(ByVal indice As Integer)

    If ssSOLConfig.ActiveRow Is Nothing Then Exit Sub
    
    Select Case indice
        Case 0  'Solicitud
            If g_bModifSolic = True Then
                cmdEliminar.Enabled = True
            Else
                cmdEliminar.Enabled = False
            End If
            
            cmdCopiar.Enabled = True
            
            SolicitudSeleccionada
                    
        Case 1  'Peticionario
            If m_bModifPetic = True Then
                cmdEliminar.Enabled = True
            Else
                cmdEliminar.Enabled = False
            End If
            cmdCopiar.Enabled = False
        Case 2  'Notificado
            If m_bModifPetic = True Then
                cmdEliminar.Enabled = True
            Else
                cmdEliminar.Enabled = False
            End If
            cmdCopiar.Enabled = False
    End Select
    
End Sub

''' <summary>
''' Establecer las propiedades del objeto g_oSolicitudSeleccionada a lo q esta en pantalla.
''' </summary>
''' <remarks>Llamada desde: cmdCopiar_Click     ssSOLConfig_BeforeRowDeactivate    ConfigurarInterfazSeguridad; Tiempo m�ximo:0</remarks>
Private Sub SolicitudSeleccionada()
    Dim oIdioma As CIdioma
    Dim Row As UltraGrid.SSRow

    Set g_oSolicitudSeleccionada = oFSGSRaiz.Generar_CSolicitud
    
    If g_udtAccion <> ACCSolConfAnyadir Then
        If ssSOLConfig.ActiveRow.Band.Index = 1 Then
            Set Row = ssSOLConfig.ActiveRow.GetParent
        ElseIf ssSOLConfig.ActiveRow.Band.Index = 2 Then
            Set Row = ssSOLConfig.ActiveRow.GetParent
        Else
            Set Row = ssSOLConfig.ActiveRow
        End If
        
        g_oSolicitudSeleccionada.Id = Row.Cells("ID_SOL").Value
        g_oSolicitudSeleccionada.Tipo = Row.Cells("ID_TIPO").Value
        g_oSolicitudSeleccionada.Codigo = Row.Cells("COD").Value
        g_oSolicitudSeleccionada.CargarDenominaciones

        Set g_oSolicitudSeleccionada.Gestor = oFSGSRaiz.Generar_CPersona
        g_oSolicitudSeleccionada.Gestor.Cod = Row.Cells("GESTOR").Value
        g_oSolicitudSeleccionada.Gestor.CargarTodosLosDatos True

        g_oSolicitudSeleccionada.CargarAdjuntos True

        Set g_oSolicitudSeleccionada.Formulario = oFSGSRaiz.Generar_CFormulario
        g_oSolicitudSeleccionada.Formulario.Id = Row.Cells("ID_FORM").Value

        If Row.Cells("WORKFL").Value <> "" Then
            Set g_oSolicitudSeleccionada.Workflow = oFSGSRaiz.Generar_CWorkflow
            g_oSolicitudSeleccionada.Workflow.Id = Row.Cells("ID_WORKF").Value
        End If
        g_oSolicitudSeleccionada.Baja = False
        g_oSolicitudSeleccionada.Publicado = GridCheckToBoolean(Row.Cells("PUB").Value)
        g_oSolicitudSeleccionada.PedidoDirecto = StrToDblOrNull(Row.Cells("PEDIDO_DIRECTO").Value)
        If Not IsNull(Row.Cells("FECACT").Value) Then
            g_oSolicitudSeleccionada.FECACT = CDate(Row.Cells("FECACT").Value)
        End If
    Else
        Set g_oSolicitudSeleccionada.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
        Set g_oSolicitudSeleccionada.Descripciones = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdioma In m_oIdiomas
            g_oSolicitudSeleccionada.Denominaciones.Add oIdioma.Cod, ""
            g_oSolicitudSeleccionada.Descripciones.Add oIdioma.Cod, ""
        Next
    End If

End Sub

''' <summary>
''' Eliminar una solicitud � un certificado � una no conformidad � un peticionario de certificado � un peticionario
''' de  no conformidad del grid de configuraci�n
''' </summary>
''' <remarks>Llamada desde: cmdEliminar_Click ; Tiempo m�ximo: 0,1</remarks>
Private Function EliminarDeGrid()
Dim FilaSiguiente As UltraGrid.SSRow

    'elimina una fila de la grid y se posiciona en la correspondiente, configurando su seguridad
    
    If ssSOLConfig.ActiveRow.HasNextSibling = True Then
        'se situa en la siguiente del mismo nivel
        Set FilaSiguiente = ssSOLConfig.ActiveRow.GetSibling(UltraGrid.ssSiblingRowNext)
    ElseIf ssSOLConfig.ActiveRow.HasPrevSibling = True Then
        Set FilaSiguiente = ssSOLConfig.ActiveRow.GetSibling(UltraGrid.ssSiblingRowPrevious)
    ElseIf ssSOLConfig.ActiveRow.HasParent = True Then
        Set FilaSiguiente = ssSOLConfig.ActiveRow.GetParent
    ElseIf ssSOLConfig.ActiveRow.HasChild = True Then
        Set FilaSiguiente = ssSOLConfig.ActiveRow.GetChild(UltraGrid.ssChildRowFirst)
    End If
    
    If ssSOLConfig.ActiveRow.Band.Index = 0 Then
        If ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value = TipoSolicitud.NoConformidades Or ssSOLConfig.ActiveRow.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
            'Comprobar si tiene peticionarios para borrarlos tambien
            If ssSOLConfig.ActiveRow.HasChild Then
                ssSOLConfig.ActiveRow.GetChild(ssChildRowFirst, 1).Delete
            End If
        End If
    End If
    
    ssSOLConfig.ActiveRow.Delete
    
    If Not FilaSiguiente Is Nothing Then
        If Not ssSOLConfig.GetRow(ssChildRowFirst) Is Nothing Then
            ssSOLConfig.ActiveRow = FilaSiguiente
            ssSOLConfig_AfterRowActivate
        End If
    End If
    
    If Me.Visible Then ssSOLConfig.SetFocus


End Function

''' <summary>
''' Initialize una Fila del grid
''' </summary>
''' <param name="Context">Contexto del grid</param>
''' <param name="Row">Fila del grid</param>
''' <param name="ReInitialize">si ReInitialize al grid</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub ssSOLConfig_InitializeRow(ByVal Context As UltraGrid.Constants_Context, ByVal Row As UltraGrid.SSRow, ByVal ReInitialize As Boolean)
    'TIPO Y PEDIDO_DIRECTO
    On Error Resume Next
    If Row.Band.Index = 0 Then
        If g_udtAccion <> ACCSolConfAnyadir And g_udtAccion <> ACCSolConfModificar Then
            'tipo de solicitud
            If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.AccesoFSWSSolicCompra Then
                If Not ssSOLConfig.IsInEditMode Then
                    Row.Cells("TIPO").Value = Row.Cells("ID_TIPO").Value
                End If
            End If
            
            'DEN
            If Row.Cells("MULT").Value = 1 Then
                Row.Band.Columns("DEN").Style = UltraGrid.ssStyleEditButton
            Else
                Row.Band.Columns("DEN").Style = UltraGrid.ssStyleEdit
            End If
        
            'GESTOR
            If Not ssSOLConfig.IsInEditMode Then
                If Row.Cells("NOM").Value = "" Then
                    Row.Cells("GESTOR_DEN").Value = NullToStr(Row.Cells("APE").Value)
                Else
                    Row.Cells("GESTOR_DEN").Value = NullToStr(Row.Cells("NOM").Value) & " " & NullToStr(Row.Cells("APE").Value)
                End If
            End If
            
            If IsNull(Row.Cells("ID_SOL").Value) Or Row.Cells("ID_SOL").Value = "" Then
                Row.Cells("ID_SOL").Value = Row.Cells("ID").Value
            End If
            
            If NullToDbl0(Row.Cells("NUM_ADJUNTOS").Value) > 0 Then
                Row.Cells("ADJUN").Appearance.Picture = ssSOLConfig.Images.Item("Adjuntos").Picture
                Row.Cells("ADJUN").Appearance.PictureAlign = ssAlignRight
            Else
                Row.Cells("ADJUN").Appearance.Picture = ""
            End If
            
            If basParametros.gParametrosGenerales.gbAccesoQANoConformidad = True Then
                If NullToDbl0(Row.Cells("NUM_ESTADOS").Value) > 0 Then
                    Row.Cells("ESTADOS").Value = "..."
                Else
                    Row.Cells("ESTADOS").Value = ""
                End If
            End If
            
            If NullToDbl0(Row.Cells("NUM_PLANTILLAS").Value) > 0 Then
                Row.Cells("CONTRATOS").Value = "..."
            Else
                Row.Cells("CONTRATOS").Value = ""
            End If
            
            If NullToDbl0(Row.Cells("NUM_EMPRESAS").Value) > 0 Then
                Row.Cells("EMPRESAS").Value = "..."
            Else
                Row.Cells("EMPRESAS").Value = ""
            End If

            If Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
                Row.Cells("WORKFL").Column.Style = UltraGrid.ssStyleEdit
                Row.Cells("WORKFL").Activation = ssActivationDisabled
                Row.Cells("WORKFL").Appearance.Backcolor = vbButtonFace
            Else
                Row.Cells("WORKFL").Column.Style = UltraGrid.ssStyleEditButton
                Row.Cells("WORKFL").Column.ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
                Row.Cells("WORKFL").Activation = ssActivationAllowEdit
                Row.Cells("WORKFL").Appearance.Backcolor = vbWhite
            End If
            
            If Row.Cells("TIPO_SOLICIT").Value <> TipoSolicitud.Contrato Then
                Row.Cells("WORKFL_MODIF").Column.Style = UltraGrid.ssStyleEdit
                Row.Cells("WORKFL_MODIF").Activation = ssActivationDisabled
                Row.Cells("WORKFL_MODIF").Appearance.Backcolor = vbButtonFace
                Row.Cells("WORKFL_BAJA").Column.Style = UltraGrid.ssStyleEdit
                Row.Cells("WORKFL_BAJA").Activation = ssActivationDisabled
                Row.Cells("WORKFL_BAJA").Appearance.Backcolor = vbButtonFace
            Else
                Row.Cells("WORKFL_MODIF").Column.Style = UltraGrid.ssStyleEditButton
                Row.Cells("WORKFL_MODIF").Column.ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
                Row.Cells("WORKFL_MODIF").Activation = ssActivationAllowEdit
                Row.Cells("WORKFL_MODIF").Appearance.Backcolor = vbWhite
                Row.Cells("WORKFL_BAJA").Column.Style = UltraGrid.ssStyleEditButton
                Row.Cells("WORKFL_BAJA").Column.ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleOnMouseEnter
                Row.Cells("WORKFL_BAJA").Activation = ssActivationAllowEdit
                Row.Cells("WORKFL_BAJA").Appearance.Backcolor = vbWhite
            End If
            
            If Row.Cells("TIPO_SOLICIT").Value = TipoSolicitud.Certificados Then
                               
                If Not ssSOLConfig.IsInEditMode Then
                    Select Case Row.Cells("PUB")
                        Case 0
                            Row.Cells("Publicar").Value = 0
                        Case 1
                            Row.Cells("Publicar").Value = 1
                        Case 2
                            Row.Cells("Publicar").Value = 0
                        Case 3
                            Row.Cells("Publicar").Value = 1
                    End Select
                End If
            Else
                If Row.Cells("Publicar").Value = "" Then Row.Cells("Publicar").Value = Row.Cells("PUB").Value
                
               
            End If
            
            If NullToDbl0(Row.Cells("NUM_EMPRESAS").Value) > 0 And NullToDbl0(Row.Cells("NUM_EMPRESAS").Value) <> "" Then
                Row.Cells("EMPRESAS").Value = "..."
            Else
                Row.Cells("EMPRESAS").Value = ""
            End If
        End If
        
    Else
        If ((g_udtAccion <> ACCSolConfModifPet) And (g_udtAccion <> ACCSolConfModifNotif)) Then
            If Not ssSOLConfig.IsInEditMode Then
                'Unidad organizativa
                If Row.Cells("UON3").Value <> "" Then
                    Row.Cells("UON_DEN").Value = Row.Cells("UON1").Value & " - " & Row.Cells("UON2").Value & " - " & Row.Cells("UON3").Value & " - " & Row.Cells("UON3DEN").Value
                ElseIf Row.Cells("UON2").Value <> "" Then
                    Row.Cells("UON_DEN").Value = Row.Cells("UON1").Value & " - " & Row.Cells("UON2").Value & " - " & Row.Cells("UON2DEN").Value
                ElseIf Row.Cells("UON1").Value <> "" Then
                    Row.Cells("UON_DEN").Value = Row.Cells("UON1").Value & " - " & Row.Cells("UON1DEN").Value
                End If
                
                'Persona
                If IsNull(Row.Cells("APE").Value) Or (Row.Cells("APE").Value = "") Then
                    Row.Cells("PER_DEN").Value = NullToStr(Row.Cells("NOM").Value)
                Else
                    Row.Cells("PER_DEN").Value = NullToStr(Row.Cells("NOM").Value) & " " & NullToStr(Row.Cells("APE").Value)
                End If
                
                If ((g_udtAccion <> ACCSolConfAnyaPet) And (g_udtAccion <> ACCSolConfAnyaNotif)) Then
                    If IsNull(Row.Cells("ID_PET").Value) Or Row.Cells("ID_PET").Value = "" Then
                        Row.Cells("ID_PET").Value = Row.Cells("ID").Value
                    End If
                End If
            End If
        End If
    End If

End Sub

Private Sub ssSOLConfig_KeyDown(KeyCode As UltraGrid.SSReturnShort, Shift As Integer)
Dim bEdit As Boolean

    'Para que se pueda dejar vacio el Workflow
    If KeyCode = vbKeyBack Then
        If ssSOLConfig.ActiveCell Is Nothing Then Exit Sub
        If ssSOLConfig.ActiveCell.Column.Band.Index = 0 And g_bModifSolic = False Then Exit Sub
        If ssSOLConfig.ActiveCell.Column.Band.Index = 1 And m_bModifPetic = False Then Exit Sub
        If ssSOLConfig.ActiveCell.Column.key <> "WORKFL" Then Exit Sub
        If ssSOLConfig.ActiveCell.Value = "" Then Exit Sub
        
        If ssSOLConfig.ActiveCell.Column.Style = UltraGrid.ssStyleDropDown Then
            bEdit = ssSOLConfig.IsInEditMode
            If bEdit = True Then
                ssSOLConfig.PerformAction UltraGrid.ssKeyActionExitEditMode
            End If
            ssSOLConfig.ActiveCell.Value = Null
            If bEdit = True Then
                ssSOLConfig.PerformAction ssKeyActionEnterEditMode
            End If
        End If
    ElseIf KeyCode = vbKeyDelete Then
        If ssSOLConfig.ActiveRow Is Nothing Then
            Exit Sub
        Else
            If Me.ssSOLConfig.ActiveRow.DataChanged = False Then
                cmdEliminar_Click
            Else
                If g_udtAccion = AccionesSummit.ACCSolConfAnyadir Then
                    If Me.ssSOLConfig.ActiveCell Is Nothing Then
                        g_udtAccion = ACCSolConfConsultar
                        picNavigate.Visible = True
                        picEdicion.Visible = False
                    End If
                End If
            End If
        End If
    End If

End Sub

Public Sub MostrarUOSeleccionada()

    If frmSELUO.sUON3 <> "" Then
        ssSOLConfig.ActiveRow.Cells("UON1").Value = frmSELUO.sUON1
        ssSOLConfig.ActiveRow.Cells("UON2").Value = frmSELUO.sUON2
        ssSOLConfig.ActiveRow.Cells("UON3").Value = frmSELUO.sUON3
        ssSOLConfig.ActiveRow.Cells("UON3DEN").Value = frmSELUO.sDen
        Exit Sub
    End If
    
    If frmSELUO.sUON2 <> "" Then
        ssSOLConfig.ActiveRow.Cells("UON1").Value = frmSELUO.sUON1
        ssSOLConfig.ActiveRow.Cells("UON2").Value = frmSELUO.sUON2
        ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
        ssSOLConfig.ActiveRow.Cells("UON2DEN").Value = frmSELUO.sDen
        Exit Sub
    End If
    
    If frmSELUO.sUON1 <> "" Then
        ssSOLConfig.ActiveRow.Cells("UON1").Value = frmSELUO.sUON1
        ssSOLConfig.ActiveRow.Cells("UON2").Value = ""
        ssSOLConfig.ActiveRow.Cells("UON3").Value = ""
        ssSOLConfig.ActiveRow.Cells("UON1DEN").Value = frmSELUO.sDen
        Exit Sub
    End If

    ssSOLConfig.ActiveRow.Cells("DEPDEN").Value = ""
    ssSOLConfig.ActiveRow.Cells("PER_DEN").Value = ""
    
    ssSOLConfig.Update
End Sub

Private Sub CrearValueList()
'Recoge los valores de los combos que se muestran en pantalla y los almacena en ValueList
Dim i As Long
Dim oTiposPredef As CTiposSolicit
Dim ADORs As Ador.Recordset

    'Crea las listas de valores:
    ssSOLConfig.ValueLists.clear

    'Gestores:
    ssSOLConfig.ValueLists.Add "Gestores"
    ssSOLConfig.Bands(0).Columns("GESTOR_DEN").ValueList = "Gestores"
    
    'Formularios:
    ssSOLConfig.ValueLists.Add "Formularios"
    ssSOLConfig.Bands(0).Columns("FORM").ValueList = "Formularios"
    
    'Workflows:
    ssSOLConfig.ValueLists.Add "Workflows"
    ssSOLConfig.Bands(0).Columns("WORKFL").ValueList = "Workflows"
    
    ssSOLConfig.ValueLists.Add "WorkflowsModificacion"
    ssSOLConfig.Bands(0).Columns("WORKFL_MODIF").ValueList = "WorkflowsModificacion"
    
    ssSOLConfig.ValueLists.Add "WorkflowsBaja"
    ssSOLConfig.Bands(0).Columns("WORKFL_BAJA").ValueList = "WorkflowsBaja"
    
    'Tipos de solicitud:
    ssSOLConfig.ValueLists.Add "Tipos"

    Set oTiposPredef = oFSGSRaiz.Generar_CTiposSolicit
    Set ADORs = oTiposPredef.DevolverTiposSolicitudes
    i = 0
    ssSOLConfig.ValueLists.Item("Tipos").ValueListItems.clear
    
    If Not ADORs Is Nothing Then
        While Not ADORs.EOF
            ssSOLConfig.ValueLists.Item("Tipos").ValueListItems.Add ADORs("ID").Value, NullToStr(ADORs("DESCR_" & gParametrosInstalacion.gIdioma).Value), i
            ssSOLConfig.ValueLists.Item("Tipos").ValueListItems.Item(i).TagVariant = ADORs("TIPO").Value
            ADORs.MoveNext
            i = i + 1
        Wend
    End If
    
    ADORs.Close
    Set ADORs = Nothing
    Set oTiposPredef = Nothing
    
    ssSOLConfig.Bands(0).Columns("TIPO").ValueList = "Tipos"
    
End Sub


Private Sub tvwCarpetasSolicit_Collapse(ByVal node As MSComctlLib.node)
tvwCarpetasSolicit_NodeClick node
End Sub

Private Sub tvwCarpetasSolicit_NodeClick(ByVal node As MSComctlLib.node)
    'Carga la grid con las solicitudes:
    Screen.MousePointer = vbHourglass
    LockWindowUpdate Me.hWnd
    
    Set ssSOLConfig.DataSource = Nothing
    CargarGridSolicitudes
    ssSOLConfig.CollapseAll
       
    CrearValueList
    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal
    
    'Control de Botones
    Dim oCarpetaSeleccionada As Object
    If Not node Is Nothing Then
        Set oCarpetaSeleccionada = DevolverCarpeta(Me.tvwCarpetasSolicit)
        If Not oCarpetaSeleccionada Is Nothing Then
            cmdAnyadirCarpeta.Enabled = (oCarpetaSeleccionada.Nivel < 4)
            cmdModificarCarpeta.Enabled = True
            cmdEliminarCarpeta.Enabled = (node.Children = 0) And (Not ssSOLConfig.HasRows)
            cmdCopiar.Enabled = True
        Else
            cmdAnyadirCarpeta.Enabled = True
            cmdModificarCarpeta.Enabled = False
            cmdEliminarCarpeta.Enabled = False
        End If
    End If
End Sub


Private Function ComprobarCampoImporteWorkflow(ByVal lIdFormulario As Long) As Boolean
    Dim oFormulario As CFormulario
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = lIdFormulario
    
    ComprobarCampoImporteWorkflow = oFormulario.ComprobarExisteCampoImporte

    Set oFormulario = Nothing
    
End Function
