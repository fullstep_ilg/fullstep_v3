VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstAtributos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de atributos "
   ClientHeight    =   7740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9705
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstAtributos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7740
   ScaleWidth      =   9705
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      Height          =   345
      Left            =   8640
      TabIndex        =   20
      Top             =   7200
      Width           =   1020
   End
   Begin TabDlg.SSTab sstabCatalogo 
      Height          =   7065
      Left            =   15
      TabIndex        =   0
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   12462
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DSelecci�n"
      TabPicture(0)   =   "frmLstAtributos.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frmArticulo"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "frmMat"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraAtributo"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "frmTipo"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "frmIntro"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "frmAmbito"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "fraUons"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstAtributos.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraDescr"
      Tab(1).Control(1)=   "fraOrden"
      Tab(1).ControlCount=   2
      Begin VB.Frame fraUons 
         Height          =   1455
         Left            =   120
         TabIndex        =   43
         Top             =   1920
         Width           =   6495
         Begin VB.OptionButton chkUonCoincidente 
            Caption         =   "DMostrar s�lo atributos relacionados directamente con la unidad organizativa"
            Height          =   195
            Left            =   120
            TabIndex        =   49
            Top             =   600
            Width           =   6255
         End
         Begin VB.OptionButton chkUonContenida 
            Caption         =   "DMostrar s�lo atributos relacionados con la unidad organizativa o contenida"
            Height          =   195
            Left            =   120
            TabIndex        =   48
            Top             =   960
            Value           =   -1  'True
            Width           =   6255
         End
         Begin VB.CommandButton cmdLimpiarUon 
            Height          =   285
            Left            =   5640
            Picture         =   "frmLstAtributos.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   46
            Top             =   240
            Width           =   315
         End
         Begin VB.CommandButton cmdBuscarUon 
            Height          =   285
            Left            =   6000
            Picture         =   "frmLstAtributos.frx":122C
            Style           =   1  'Graphical
            TabIndex        =   45
            Top             =   240
            Width           =   315
         End
         Begin VB.TextBox txtUonSeleccionada 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1320
            MaxLength       =   100
            TabIndex        =   44
            Top             =   240
            Width           =   4215
         End
         Begin VB.Label lblUons 
            Caption         =   "dUn. org.:"
            Height          =   255
            Left            =   120
            TabIndex        =   47
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Frame frmAmbito 
         Caption         =   "D�mbito"
         Height          =   1335
         Left            =   6840
         TabIndex        =   39
         Top             =   5400
         Width           =   2625
         Begin VB.CheckBox chkAmbitoUon 
            Caption         =   "DUnidad Organizativa"
            Height          =   315
            Left            =   360
            TabIndex        =   42
            Top             =   240
            UseMaskColor    =   -1  'True
            Width           =   2565
         End
         Begin VB.CheckBox chkAmbitoMaterial 
            Caption         =   "DMaterial"
            Height          =   315
            Left            =   360
            TabIndex        =   41
            Top             =   600
            Width           =   1725
         End
         Begin VB.CheckBox chkAmbitoArticulo 
            Caption         =   "DArticulo"
            Height          =   315
            Left            =   360
            TabIndex        =   40
            Top             =   960
            Width           =   1725
         End
      End
      Begin VB.Frame fraDescr 
         Caption         =   "Descripci�n de atributos"
         Height          =   2625
         Left            =   -74880
         TabIndex        =   37
         Top             =   2445
         Width           =   9375
         Begin VB.CheckBox chkDescr 
            Caption         =   "Incluir descripci�n de atributos"
            Height          =   195
            Left            =   360
            TabIndex        =   38
            Top             =   720
            Width           =   4005
         End
      End
      Begin VB.Frame fraOrden 
         Caption         =   "Criterio de ordenaci�n del listado"
         Height          =   1770
         Left            =   -74880
         TabIndex        =   34
         Top             =   480
         Width           =   9375
         Begin VB.OptionButton optOrdDen 
            Caption         =   "DDenominacion"
            Height          =   255
            Left            =   360
            TabIndex        =   36
            Top             =   1080
            Width           =   2055
         End
         Begin VB.OptionButton optOrdCod 
            Caption         =   "DC�digo"
            Height          =   255
            Left            =   360
            TabIndex        =   35
            Top             =   480
            Value           =   -1  'True
            Width           =   2175
         End
      End
      Begin VB.Frame frmIntro 
         Caption         =   "DIntroducci�n"
         Height          =   1695
         Left            =   6795
         TabIndex        =   33
         Top             =   3480
         Width           =   2700
         Begin VB.CheckBox chkSeleccion 
            Caption         =   "DSelecci�n"
            Height          =   270
            Left            =   420
            TabIndex        =   19
            Top             =   1140
            Width           =   1845
         End
         Begin VB.CheckBox chkLibre 
            Caption         =   "DLibre"
            Height          =   270
            Left            =   420
            TabIndex        =   18
            Top             =   585
            Width           =   1845
         End
      End
      Begin VB.Frame frmTipo 
         Caption         =   "DTipo de datos"
         Height          =   2910
         Left            =   6795
         TabIndex        =   32
         Top             =   480
         Width           =   2700
         Begin VB.OptionButton optTipo 
            Caption         =   "DArchivo"
            Height          =   195
            Index           =   5
            Left            =   345
            TabIndex        =   50
            Top             =   2430
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DS�/No"
            Height          =   195
            Index           =   4
            Left            =   345
            TabIndex        =   17
            Top             =   2025
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DTexto"
            Height          =   195
            Index           =   3
            Left            =   345
            TabIndex        =   16
            Top             =   1608
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DFecha"
            Height          =   195
            Index           =   2
            Left            =   345
            TabIndex        =   15
            Top             =   1192
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DNum�rico"
            Height          =   195
            Index           =   1
            Left            =   345
            TabIndex        =   14
            Top             =   776
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DTodos"
            Height          =   195
            Index           =   0
            Left            =   345
            TabIndex        =   13
            Top             =   360
            Value           =   -1  'True
            Width           =   2055
         End
      End
      Begin VB.Frame fraAtributo 
         Caption         =   "DAtributo"
         Height          =   1305
         Left            =   120
         TabIndex        =   29
         Top             =   480
         Width           =   6570
         Begin VB.TextBox txtCod 
            Height          =   285
            Left            =   1365
            TabIndex        =   1
            Top             =   270
            Width           =   1335
         End
         Begin VB.TextBox txtDen 
            Height          =   285
            Left            =   1365
            TabIndex        =   2
            Top             =   765
            Width           =   4890
         End
         Begin VB.Label lblCod 
            Caption         =   "DC�digo:"
            Height          =   195
            Left            =   225
            TabIndex        =   31
            Top             =   300
            Width           =   840
         End
         Begin VB.Label lblDen 
            Caption         =   "DDenominaci�n:"
            Height          =   195
            Left            =   210
            TabIndex        =   30
            Top             =   825
            Width           =   1095
         End
      End
      Begin VB.Frame frmMat 
         Caption         =   "DMaterial"
         Height          =   2355
         Left            =   120
         TabIndex        =   23
         Top             =   3480
         Width           =   6570
         Begin VB.CommandButton cmdSelMat 
            Height          =   315
            Left            =   5955
            Picture         =   "frmLstAtributos.frx":15AE
            Style           =   1  'Graphical
            TabIndex        =   24
            Top             =   135
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   1365
            TabIndex        =   3
            Top             =   480
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
            Height          =   285
            Left            =   1365
            TabIndex        =   5
            Top             =   900
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
            Height          =   285
            Left            =   1365
            TabIndex        =   7
            Top             =   1320
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
            Height          =   285
            Left            =   1365
            TabIndex        =   9
            Top             =   1740
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
            Height          =   285
            Left            =   2475
            TabIndex        =   6
            Top             =   900
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
            Height          =   285
            Left            =   2475
            TabIndex        =   8
            Top             =   1320
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
            Height          =   285
            Left            =   2475
            TabIndex        =   10
            Top             =   1740
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
            Height          =   285
            Left            =   2475
            TabIndex        =   4
            Top             =   480
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "DSubfamilia:"
            Height          =   195
            Left            =   225
            TabIndex        =   28
            Top             =   1380
            Width           =   1560
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "DFamilia:"
            Height          =   195
            Left            =   225
            TabIndex        =   27
            Top             =   960
            Width           =   1560
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "DCommodity:"
            Height          =   195
            Left            =   225
            TabIndex        =   26
            Top             =   540
            Width           =   1560
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "DGrupo:"
            Height          =   195
            Left            =   225
            TabIndex        =   25
            Top             =   1800
            Width           =   1560
         End
      End
      Begin VB.Frame frmArticulo 
         Height          =   900
         Left            =   120
         TabIndex        =   21
         Top             =   5880
         Width           =   6570
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiCod 
            Height          =   285
            Left            =   1110
            TabIndex        =   11
            Top             =   345
            Width           =   1335
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2355
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiDen 
            Height          =   285
            Left            =   2475
            TabIndex        =   12
            Top             =   345
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblArtiCod 
            Caption         =   "DArt�culo:"
            Height          =   225
            Left            =   225
            TabIndex        =   22
            Top             =   360
            Width           =   825
         End
      End
   End
End
Attribute VB_Name = "frmLstAtributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String

'Variables para materiales
Private m_oGruposMN1 As CGruposMatNivel1
Private m_oGruposMN2 As CGruposMatNivel2
Private m_oGruposMN3 As CGruposMatNivel3
Private m_oGruposMN4 As CGruposMatNivel4

Public m_oGMN1Seleccionado As CGrupoMatNivel1
Public m_oGMN2Seleccionado As CGrupoMatNivel2
Public m_oGMN3Seleccionado As CGrupoMatNivel3
Public m_oGMN4Seleccionado As CGrupoMatNivel4

Private m_GMN1CargarComboDesde As Boolean
Private m_GMN2CargarComboDesde As Boolean
Private g_GMN2RespetarCombo As Boolean
Private m_GMN3CargarComboDesde As Boolean
Private g_GMN3RespetarCombo As Boolean
Private m_GMN4CargarComboDesde As Boolean
Private g_GMN4RespetarCombo As Boolean

Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean
Private g_GMN1RespetarCombo As Boolean

'Seguridad
Public m_bRComprador As Boolean
Private m_bBajasLog As Boolean

'Idiomas:
Private m_stxtTitulo As String
Private m_sIdiGenerando As String
Private m_sIdiSeleccionando As String
Private m_sIdiVisualizando As String
Private m_sPag As String
Private m_sDe As String
Private m_sLibre As String
Private m_sSeleccion As String
Private m_sCod As String
Private m_sDen As String
Private m_sNumerico As String
Private m_sFecha As String
Private m_stexto As String
Private m_sBoolean As String
Private m_sTipo As String
Private m_sIntro As String
Private m_sOpciones As String
Private m_sPonderacion As String
Private m_sValorBajo As String
Private m_sMinimo As String
Private m_sMaximo As String
Private m_sSi As String
Private m_sNo As String
Private m_sContinua As String
Private m_sDiscreta As String
Private m_sFormula As String
Private m_sManual As String
Private m_sSiNo As String
Private m_sDescripcion  As String

Public g_sGmn1 As String
Public g_sGmn2 As String
Public g_sGmn3 As String
Public g_sGmn4 As String
Public g_sArtCod As String

Private m_bRestrMantAtribUsu As Boolean
Private m_bRestrMantAtribPerf As Boolean

Private m_oUonsSeleccionadas As CUnidadesOrganizativas

Private Sub chkLibre_Click()
    If chkSeleccion.Value = vbChecked And chkLibre.Value = vbChecked Then
        chkSeleccion.Value = vbUnchecked
    End If
End Sub

Private Sub chkSeleccion_Click()
    If chkSeleccion.Value = vbChecked And chkLibre.Value = vbChecked Then
        chkLibre.Value = vbUnchecked
    End If
End Sub

Private Sub cmdBuscarUON_Click()
    '''Cargar unidades organizativas en funci�n de las restricciones
    Dim ouons1 As CUnidadesOrgNivel1
    Dim ouons2 As CUnidadesOrgNivel2
    Dim ouons3 As CUnidadesOrgNivel3
    Set ouons1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set ouons2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set ouons3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
    
    ouons1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, oUsuarioSummit.perfil.Id, , , False
    ouons2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, oUsuarioSummit.perfil.Id, , , False
    ouons3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, oUsuarioSummit.perfil.Id, , False
    
    Dim frm As frmSELUO
    Set frm = New frmSELUO
    
    frm.multiselect = True
    frm.GuardarUon0 = False
    frm.cargarArbol ouons1, ouons2, ouons3
    
    If m_oUonsSeleccionadas.Count > 0 Then
        Set frm.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    
    If chkUonCoincidente.Value = True Then
        frm.CheckChildren = False
    Else
        frm.CheckChildren = True
    End If
    frm.Show vbModal
    If frm.Aceptar Then
        txtUonSeleccionada.Text = frm.UonsSeleccionadas.titulo
        Set m_oUonsSeleccionadas = frm.UonsSeleccionadas
    End If
    Set ouons1 = Nothing
    Set ouons2 = Nothing
    Set ouons3 = Nothing
    Set frm = Nothing
End Sub

Private Sub cmdLimpiarUon_Click()
    Me.txtUonSeleccionada.Text = ""
    m_oUonsSeleccionadas.clear
    txtUonSeleccionada.Text = ""
End Sub

Private Sub cmdObtener_Click()
    ObtenerListado
End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = g_sOrigen & "frmLstAtributos"
    frmSELMAT.bRComprador = m_bRComprador
    frmSELMAT.Show 1
End Sub

Private Sub Form_Initialize()
    Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
End Sub

Private Sub Form_Load()
    
    Me.Width = 9795
    Me.Height = 8160
    
    CargarRecursos
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
    ConfigurarNombres
    If m_oUonsSeleccionadas.Count > 0 Then
        txtUonSeleccionada.Text = m_oUonsSeleccionadas.titulo
    End If
End Sub

Private Sub Form_Terminate()
    Set m_oUonsSeleccionadas = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oGruposMN1 = Nothing
    Set m_oGruposMN2 = Nothing
    Set m_oGruposMN3 = Nothing
    Set m_oGruposMN4 = Nothing
    
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGMN2Seleccionado = Nothing
    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN4Seleccionado = Nothing
End Sub


Private Sub sdbcArtiCod_Click()
    If Not sdbcArtiCod.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiCod_CloseUp()
 
    If sdbcArtiCod.Value = "..." Then
        sdbcArtiCod.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiCod.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcArtiDen.Text = sdbcArtiCod.Columns(1).Text
    sdbcArtiCod.Text = sdbcArtiCod.Columns(0).Text
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
End Sub

Private Sub sdbcArtiCod_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcArtiDen.Text = ""
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiCod_DropDown()
    Dim oArt As CArticulo
    
    sdbcArtiCod.RemoveAll

    Screen.MousePointer = vbHourglass
    GMN4Seleccionado
    If m_oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos
    
    For Each oArt In m_oGMN4Seleccionado.ARTICULOS
   
        sdbcArtiCod.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
   
    Next

    sdbcArtiCod.SelStart = 0
    sdbcArtiCod.SelLength = Len(sdbcArtiCod.Text)
    sdbcArtiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiCod_InitColumnProps()
    
    sdbcArtiCod.DataFieldList = "Column 0"
    sdbcArtiCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiCod_PositionList(ByVal Text As String)
PositionList sdbcArtiCod, Text
End Sub

Public Sub sdbcArtiCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    If sdbcArtiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, sdbcArtiCod, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (m_oGMN4Seleccionado.ARTICULOS.Count = 0)
            
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiCod.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcArtiDen.Text = m_oGMN4Seleccionado.ARTICULOS.Item(1).Den
        
        sdbcArtiCod.Columns(0).Value = sdbcArtiCod.Text
        sdbcArtiCod.Columns(1).Value = sdbcArtiDen.Text
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = False
    End If
    If Not m_oGMN4Seleccionado Is Nothing Then
        Set m_oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub

Private Sub sdbcArtiDen_Click()
    
    If Not sdbcArtiDen.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiDen_CloseUp()
    
    If sdbcArtiDen.Value = "..." Then
        sdbcArtiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiDen.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcArtiCod.Text = sdbcArtiDen.Columns(1).Text
    sdbcArtiDen.Text = sdbcArtiDen.Columns(0).Text
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
End Sub

Private Sub sdbcArtiDen_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcArtiCod.Text = ""
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiDen_DropDown()
Dim I As Integer
Dim oArt As CArticulo
    
    sdbcArtiDen.RemoveAll
   
    Screen.MousePointer = vbHourglass
    GMN4Seleccionado

    If m_oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If m_bCargarComboDesde Then
        m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
    Else
        m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , True
    End If
    
    
    For Each oArt In m_oGMN4Seleccionado.ARTICULOS
   
        sdbcArtiDen.AddItem oArt.Den & Chr(m_lSeparador) & oArt.Cod
   
    Next

    sdbcArtiDen.SelStart = 0
    sdbcArtiDen.SelLength = Len(sdbcArtiDen.Text)
    sdbcArtiDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiDen_InitColumnProps()
    
    sdbcArtiDen.DataFieldList = "Column 0"
    sdbcArtiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiDen_PositionList(ByVal Text As String)
PositionList sdbcArtiDen, Text
End Sub

Private Sub sdbcArtiDen_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    
    
    If sdbcArtiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (m_oGMN4Seleccionado.ARTICULOS.Count = 0)
            
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiDen.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcArtiCod.Text = m_oGMN4Seleccionado.ARTICULOS.Item(1).Cod
        
        sdbcArtiDen.Columns(0).Value = sdbcArtiDen.Text
        sdbcArtiDen.Columns(1).Value = sdbcArtiCod.Text
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = False
    End If
    If Not m_oGMN4Seleccionado Is Nothing Then
       Set m_oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    g_GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    g_GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    m_GMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim I As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    If m_bRComprador Then
        
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        If m_GMN1CargarComboDesde Then
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
          If m_GMN1CargarComboDesde Then
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            m_oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = m_oGruposMN1.DevolverLosCodigos
    
    For I = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem Codigos.Cod(I) & Chr(m_lSeparador) & Codigos.Den(I)
   
    Next

    If m_GMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If m_bRComprador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
    Else
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set m_oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        g_GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        g_GMN1RespetarCombo = False
        GMN1Seleccionado
        m_GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing

End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN1_4Den_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If m_bRComprador Then
         Set oIMAsig = oUsuarioSummit.comprador
         Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, , False)
    Else
         oGMN1s.CargarTodosLosGruposMat , sdbcGMN1_4Den.Text, True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set m_oGMN1Seleccionado = Nothing
        sdbcGMN1_4Den.Text = ""
    Else
        g_GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4Den.Columns(0).Value = sdbcGMN1_4Den.Text
        sdbcGMN1_4Den.Columns(1).Value = sdbcGMN1_4Cod.Text
        g_GMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing

End Sub


Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Public Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If m_bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
            m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = m_oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        g_GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        g_GMN2RespetarCombo = False
        GMN2Seleccionado
        m_GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If m_bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, , Trim(sdbcGMN2_4Den), True, , False)
        Else
            m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = m_oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
    Else
        g_GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2s.Item(1).Cod
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
        g_GMN2RespetarCombo = False
        GMN2Seleccionado
        m_GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing

End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Public Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If m_bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = m_oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
    Else
        g_GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        g_GMN3RespetarCombo = False
        GMN3Seleccionado
        m_GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not g_GMN3RespetarCombo Then

        g_GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        g_GMN3RespetarCombo = False
        Set m_oGMN3Seleccionado = Nothing
        m_GMN3CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Den.Value = "" Then Exit Sub
    
    g_GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    g_GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    m_GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim I As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If m_oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN3 = Nothing
        If m_GMN3CargarComboDesde Then
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False)
        End If
        
    Else
        
           If m_GMN3CargarComboDesde Then
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN3 = m_oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = m_oGruposMN3.DevolverLosCodigos
    
    For I = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(I) & Chr(m_lSeparador) & Codigos.Cod(I)
   
    Next

    If m_GMN3CargarComboDesde And Not m_oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If m_bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False)
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = m_oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
    Else
        g_GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3s.Item(1).Cod
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
        g_GMN3RespetarCombo = False
        GMN3Seleccionado
        m_GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Public Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If m_bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = m_oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        g_GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        g_GMN4RespetarCombo = False
        GMN4Seleccionado
        m_GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not g_GMN4RespetarCombo Then

        g_GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        g_GMN4RespetarCombo = False
        Set m_oGMN4Seleccionado = Nothing
        m_GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Value = "" Then Exit Sub
    
    g_GMN4RespetarCombo = True
    If sdbcGMN4_4Cod.Text <> sdbcGMN4_4Den.Columns(1).Text Then
        sdbcArtiCod.Text = ""
    End If
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    g_GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    m_GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim I As Integer

    sdbcGMN4_4Den.RemoveAll
    
    If m_oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If m_bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN4 = Nothing
        If m_GMN4CargarComboDesde Then
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False)
        End If
    Else
        
        If m_GMN4CargarComboDesde Then
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN4 = m_oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = m_oGruposMN4.DevolverLosCodigos
    
    For I = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(I) & Chr(m_lSeparador) & Codigos.Cod(I)
   
    Next

    If m_GMN4CargarComboDesde And Not m_oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub

Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN3Seleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        If m_bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = m_oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        g_GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
        g_GMN4RespetarCombo = False
        GMN4Seleccionado
        m_GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub
Private Sub sdbcGMN1_4Cod_Change()

    If Not g_GMN1RespetarCombo Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn1 = ""
        End If
        g_GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        g_GMN1RespetarCombo = False
        Set m_oGMN1Seleccionado = Nothing
        m_GMN1CargarComboDesde = True
        
    End If

End Sub
Private Sub sdbcGMN1_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim I As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll

    If m_bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        If m_GMN1CargarComboDesde Then
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         If m_GMN1CargarComboDesde Then
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            m_oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = m_oGruposMN1.DevolverLosCodigos
    
    For I = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem Codigos.Den(I) & Chr(m_lSeparador) & Codigos.Cod(I)
   
    Next

    If m_GMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not g_GMN2RespetarCombo Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn2 = ""
        End If
        g_GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        g_GMN2RespetarCombo = False
        Set m_oGMN2Seleccionado = Nothing
        
        m_GMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    g_GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    g_GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    m_GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim I As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN2_4Cod.RemoveAll

    If m_oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If m_bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN2 = Nothing
        If m_GMN2CargarComboDesde Then
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
         If m_GMN2CargarComboDesde Then
            m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            m_oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set m_oGruposMN2 = m_oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = m_oGruposMN2.DevolverLosCodigos
    
    For I = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(I) & Chr(m_lSeparador) & Codigos.Den(I)
   
    Next

    If m_GMN2CargarComboDesde And Not m_oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not g_GMN2RespetarCombo Then

        g_GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        g_GMN2RespetarCombo = False
        Set m_oGMN2Seleccionado = Nothing
        m_GMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Den.Value = "" Then Exit Sub
    
    g_GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    g_GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    m_GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim I As Integer
    
    sdbcGMN2_4Den.RemoveAll
    
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN2 = Nothing
        If m_GMN2CargarComboDesde Then
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , False)
        End If
    
    Else
        
        If m_GMN2CargarComboDesde Then
            m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            m_oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set m_oGruposMN2 = m_oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = m_oGruposMN2.DevolverLosCodigos
    
    For I = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(I) & Chr(m_lSeparador) & Codigos.Cod(I)
   
    Next

    If m_GMN2CargarComboDesde And Not m_oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Cod_Change()
    
    If Not g_GMN3RespetarCombo Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn3 = ""
        End If
        g_GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        g_GMN3RespetarCombo = False
        Set m_oGMN3Seleccionado = Nothing
        m_GMN3CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    g_GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    g_GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    m_GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim I As Integer

    sdbcGMN3_4Cod.RemoveAll

    If m_oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN3 = Nothing
        If m_GMN3CargarComboDesde Then
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
         If m_GMN3CargarComboDesde Then
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN3 = m_oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = m_oGruposMN3.DevolverLosCodigos
    
    For I = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(I) & Chr(m_lSeparador) & Codigos.Den(I)
   
    Next

    If m_GMN3CargarComboDesde And Not m_oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not g_GMN1RespetarCombo Then

        g_GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        g_GMN1RespetarCombo = False
        Set m_oGMN1Seleccionado = Nothing
        m_GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Den.Value = "" Then Exit Sub
    
    g_GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    g_GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    m_GMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
        
    If Not g_GMN4RespetarCombo Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn4 = ""
        End If
        g_GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        g_GMN4RespetarCombo = False
        Set m_oGMN4Seleccionado = Nothing
        m_GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Value = "" Then Exit Sub
    
    g_GMN4RespetarCombo = True
    If sdbcGMN4_4Den.Text <> sdbcGMN4_4Cod.Columns(1).Text Then
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
    End If
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    g_GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    m_GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim oIMAsig As IMaterialAsignado
    Dim I As Integer

     sdbcGMN4_4Cod.RemoveAll

    If m_oGMN3Seleccionado Is Nothing Then Exit Sub

    Screen.MousePointer = vbHourglass

    If m_bRComprador Then

        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN4 = Nothing
        If m_GMN4CargarComboDesde Then
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else

       If m_GMN4CargarComboDesde Then
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN4 = m_oGMN3Seleccionado.GruposMatNivel4

    End If

    Codigos = m_oGruposMN4.DevolverLosCodigos

    For I = 0 To UBound(Codigos.Cod) - 1

        sdbcGMN4_4Cod.AddItem Codigos.Cod(I) & Chr(m_lSeparador) & Codigos.Den(I)

    Next

    If m_GMN4CargarComboDesde And Not m_oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh

    Screen.MousePointer = vbNormal

End Sub


Public Sub PonerMatSeleccionadoEnCombos()

    Set m_oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado

    If Not m_oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = m_oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Value = ""

    End If

    Set m_oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado

    If Not m_oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = m_oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Value = ""

    End If

    Set m_oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado

    If Not m_oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = m_oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Value = ""

    End If

    Set m_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado

    If Not m_oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = m_oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Value = ""

    End If
    sdbcArtiCod.Text = ""

End Sub

Private Sub GMN1Seleccionado()
    
    sdbcGMN2_4Cod.Text = ""
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcGMN2_4Den.Text = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn2 = ""
        g_sGmn3 = ""
        g_sGmn4 = ""
        g_sArtCod = ""
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    Set m_oGMN2Seleccionado = Nothing
    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN4Seleccionado = Nothing
    
    m_oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn3 = ""
        g_sGmn4 = ""
        g_sArtCod = ""
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    Set m_oGMN2Seleccionado = Nothing
    Set m_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    m_oGMN2Seleccionado.Cod = sdbcGMN2_4Cod.Text
    m_oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
       
End Sub
Private Sub GMN3Seleccionado()

    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    Set m_oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn4 = ""
        g_sArtCod = ""
    End If
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    m_oGMN3Seleccionado.Cod = sdbcGMN3_4Cod.Text
    m_oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    m_oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod.Text
    
End Sub

Private Sub GMN4Seleccionado()
      
    g_sArtCod = ""
    
    Set m_oGMN4Seleccionado = Nothing
    Set m_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    m_oGMN4Seleccionado.Cod = sdbcGMN4_4Cod.Text
    m_oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    m_oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod.Text
    m_oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod.Text
        
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If g_sOrigen = "" Or g_sOrigen = "MDI" Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBRestMatComp)) Is Nothing) And basOptimizacion.gTipoDeUsuario = comprador Then
            m_bRComprador = True
        End If
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBBajasLog)) Is Nothing) Then
        m_bBajasLog = True
    End If
 
    If g_sOrigen = "frmPROCE" Then
        m_bBajasLog = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATAtribRestrMantAtribUonUsu)) Is Nothing) Then
        m_bRestrMantAtribUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATAtribRestrMantAtribUonPer)) Is Nothing) Then
        m_bRestrMantAtribPerf = True
    End If
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim I As Integer

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LST_ATRIB, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        fraAtributo.caption = Ador(0).Value  '1 Atributo
        Ador.MoveNext
        lblCod.caption = Ador(0).Value       '2 C�digo
        m_sCod = Ador(0).Value
        optOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value       '3 Denominaci�n
        m_sDen = Ador(0).Value
        optOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        frmMat.caption = Ador(0).Value       '4 Material
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        lblArtiCod.caption = Ador(0).Value   '9 Art�culo:
        Ador.MoveNext
        frmTipo.caption = Ador(0).Value      '10 Tipo de datos
        m_sTipo = Ador(0).Value
        Ador.MoveNext
        optTipo(0).caption = Ador(0).Value      '11 Todos
        Ador.MoveNext
        optTipo(1).caption = Ador(0).Value      '12  Num�rico
        m_sNumerico = Ador(0).Value
        Ador.MoveNext
        optTipo(2).caption = Ador(0).Value      '13 Fecha
        m_sFecha = Ador(0).Value
        Ador.MoveNext
        optTipo(3).caption = Ador(0).Value      '14 Texto
        m_stexto = Ador(0).Value
        Ador.MoveNext
        optTipo(4).caption = Ador(0).Value      '15 S�/No
        m_sBoolean = Ador(0).Value
        Ador.MoveNext
        frmIntro.caption = Ador(0).Value     '16 Introducci�n
        m_sIntro = Ador(0).Value
        Ador.MoveNext
        chkLibre.caption = Ador(0).Value     '17 Libre
        m_sLibre = Ador(0).Value
        Ador.MoveNext
        chkSeleccion.caption = Ador(0).Value  '18 Selecci�n
        m_sSeleccion = Ador(0).Value
        sstabCatalogo.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabCatalogo.TabCaption(1) = Ador(0).Value  '19 Orden
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value    '20  Obtener
        Ador.MoveNext
        m_stxtTitulo = Ador(0).Value  '21 Listado de atributos
        Ador.MoveNext
        m_sIdiGenerando = Ador(0).Value   '22  Generando listado de atributos
        Ador.MoveNext
        m_sIdiVisualizando = Ador(0).Value   '23 Visualizando registros...
        Ador.MoveNext
        m_sIdiSeleccionando = Ador(0).Value   '24 Seleccionando registros...
        Ador.MoveNext
        m_sPag = Ador(0).Value   '25 Pag
        Ador.MoveNext
        m_sDe = Ador(0).Value   '26 /
        Ador.MoveNext
        m_sOpciones = Ador(0).Value  '27 Opciones
        Ador.MoveNext
        m_sPonderacion = Ador(0).Value  '28 Ponderaci�n
        Ador.MoveNext
        m_sMinimo = Ador(0).Value    '29 m�nimo
        Ador.MoveNext
        m_sMaximo = Ador(0).Value  '30 M�ximo
        Ador.MoveNext
        m_sValorBajo = Ador(0).Value   '31 Valor bajo
        Ador.MoveNext
        m_sSi = Ador(0).Value '32 Si
        Ador.MoveNext
        m_sNo = Ador(0).Value '33 No
        Ador.MoveNext
        m_sContinua = Ador(0).Value '34 Continua
        Ador.MoveNext
        m_sDiscreta = Ador(0).Value '35 discreta
        Ador.MoveNext
        m_sFormula = Ador(0).Value '36 f�rmula
        Ador.MoveNext
        m_sManual = Ador(0).Value '37 manual
        Ador.MoveNext
        m_sSiNo = Ador(0).Value '38 SiNo
        Ador.MoveNext
        
        Me.fraOrden.caption = Ador(0).Value
        Ador.MoveNext
        Me.fraDescr.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkDescr.caption = Ador(0).Value
        Ador.MoveNext
        m_sDescripcion = Ador(0).Value
        'Tarea 3299 - Atributos uon
        Ador.MoveNext
        chkUonCoincidente.caption = Ador(0).Value
        Ador.MoveNext
        chkUonContenida.caption = Ador(0).Value
        Ador.MoveNext
        frmAmbito.caption = Ador(0).Value
        Ador.MoveNext
        chkAmbitoUon.caption = Ador(0).Value
        Ador.MoveNext
        chkAmbitoMaterial.caption = Ador(0).Value
        Ador.MoveNext
        chkAmbitoArticulo.caption = Ador(0).Value
        Ador.MoveNext
        lblUons.caption = Ador(0).Value
        Ador.MoveNext
        optTipo(5).caption = Ador(0).Value  'Archivo
        Ador.Close
    End If
    
    Me.caption = m_stxtTitulo & " (" & m_sOpciones & ")"
    
    Set Ador = Nothing
    
End Sub


Private Sub ObtenerListado()
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim oCRAtributos As CRAtributos
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim sSeleccion As String
    Dim vTipo As Variant
    Dim vIntroducc As Variant
    If crs_Connected = False Then
        Exit Sub
    End If
       
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Set oReport = Nothing
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
        
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAtributos.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    Set oCRAtributos = New CRAtributos
    
    sSeleccion = GenerarTextoSeleccion
    
    If optTipo(0).Value = True Then
        vTipo = Null
    ElseIf optTipo(1).Value = True Then
        vTipo = 2
    ElseIf optTipo(2).Value = True Then
        vTipo = 3
    ElseIf optTipo(3).Value = True Then
        vTipo = 1
    ElseIf optTipo(4).Value = True Then
        vTipo = 4
    ElseIf optTipo(5).Value = True Then
        vTipo = TiposDeAtributos.TipoArchivo
    End If
            
    
    If chkLibre.Value = vbChecked Then
        vIntroducc = 0
    Else
        If chkSeleccion.Value = vbChecked Then
            vIntroducc = 1
        Else
            vIntroducc = Null
        End If
    End If



   '*********** FORMULA FIELDS REPORT
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MOSTRARDESCR")).Text = IIf(Me.chkDescr.Value = 1, 1, 0)
    
    
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & m_sPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & m_sDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTITULO")).Text = """" & m_stxtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtBoolean")).Text = """" & m_sBoolean & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecha")).Text = """" & m_sFecha & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNumerico")).Text = """" & m_sNumerico & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTexto")).Text = """" & m_stexto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLibre")).Text = """" & m_sLibre & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSelec")).Text = """" & m_sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtArchivo")).Text = """" & optTipo(5).caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCODIGO")).Text = """" & m_sCod & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDENOM")).Text = """" & m_sDen & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipoLIT")).Text = """" & m_sTipo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtIntroLIT")).Text = """" & m_sIntro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & m_sSeleccion & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtValorBajo")).Text = """" & m_sValorBajo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSi")).Text = """" & m_sSi & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNo")).Text = """" & m_sNo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMinimo")).Text = """" & m_sMinimo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMaximo")).Text = """" & m_sMaximo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPonderacion")).Text = """" & m_sPonderacion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtContinua")).Text = """" & m_sContinua & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDiscreta")).Text = """" & m_sDiscreta & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFormula")).Text = """" & m_sFormula & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtManual")).Text = """" & m_sManual & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSino")).Text = """" & m_sSiNo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDescripcion")).Text = """" & m_sDescripcion & """"
    If m_bRComprador Then
        oCRAtributos.Listado oReport, g_sOrigen, txtCod.Text, txtDen.Text, vTipo, vIntroducc, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, _
            sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, sdbcArtiCod.Text, m_bBajasLog, optOrdCod.Value, g_sGmn1, g_sGmn2, g_sGmn3, _
            g_sGmn4, g_sArtCod, m_bRComprador, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod, Me.chkAmbitoMaterial.Value, Me.chkAmbitoArticulo.Value, _
            Me.chkAmbitoUon.Value, m_oUonsSeleccionadas, Me.chkUonCoincidente.Value
    Else
        oCRAtributos.Listado oReport, g_sOrigen, txtCod.Text, txtDen.Text, vTipo, vIntroducc, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, _
            sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, sdbcArtiCod.Text, m_bBajasLog, optOrdCod.Value, g_sGmn1, g_sGmn2, g_sGmn3, _
            g_sGmn4, g_sArtCod, m_bRComprador, , , Me.chkAmbitoMaterial.Value, Me.chkAmbitoArticulo.Value, _
            Me.chkAmbitoUon.Value, m_oUonsSeleccionadas, Me.chkUonCoincidente.Value
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set pv = New Preview
    Me.Hide
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.lblGeneral.caption = m_sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = m_sIdiSeleccionando
    frmESPERA.lblDetalle = m_sIdiVisualizando
    frmESPERA.Show

    Timer1.Enabled = True
    pv.caption = m_stxtTitulo
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False

    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oCRAtributos = Nothing
End Sub


Private Function GenerarTextoSeleccion() As String
    Dim sSeleccion As String
    Dim sUnion As String

    sSeleccion = ""
    sUnion = ""
    
    If txtCod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblCod.caption & " " & txtCod.Text
    End If
    
    If txtDen.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblDen.caption & " " & txtDen.Text
    End If
    
    If sdbcGMN1_4Cod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblGMN1_4.caption & " " & sdbcGMN1_4Cod.Text
    End If
    
    If sdbcGMN2_4Cod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblGMN2_4.caption & " " & sdbcGMN2_4Cod.Text
    End If
    
    If sdbcGMN3_4Cod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblGMN3_4.caption & " " & sdbcGMN3_4Cod.Text
    End If
    
    If sdbcGMN4_4Cod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblGMN4_4.caption & " " & sdbcGMN4_4Cod.Text
    End If
    
    If sdbcArtiCod.Text <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & lblArtiCod.caption & " " & sdbcArtiCod.Text
    End If
    
    If sSeleccion <> "" Then sUnion = "; "
    sSeleccion = sSeleccion & sUnion & frmTipo.caption & ": "
    If optTipo(0).Value Then
        sSeleccion = sSeleccion & optTipo(0).caption
    ElseIf optTipo(1).Value Then
        sSeleccion = sSeleccion & optTipo(1).caption
    ElseIf optTipo(2).Value Then
        sSeleccion = sSeleccion & optTipo(2).caption
    ElseIf optTipo(3).Value Then
        sSeleccion = sSeleccion & optTipo(3).caption
    ElseIf optTipo(4).Value Then
        sSeleccion = sSeleccion & optTipo(4).caption
    ElseIf optTipo(5).Value Then
        sSeleccion = sSeleccion & optTipo(5).caption
    End If
    
    If chkLibre.Value = vbChecked Or chkSeleccion.Value = vbChecked Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & frmIntro.caption & ":"
        If chkLibre.Value = vbChecked Then
            sSeleccion = sSeleccion & " " & chkLibre.caption
        ElseIf chkSeleccion.Value = vbChecked Then
            sSeleccion = sSeleccion & " " & chkSeleccion.caption
        End If
    End If
     
    GenerarTextoSeleccion = sSeleccion

End Function


Private Sub ConfigurarNombres()
    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"
End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If
End Sub

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas
    Set UonsSeleccionadas = m_oUonsSeleccionadas
End Property

Public Property Set UonsSeleccionadas(Value As CUnidadesOrganizativas)
    Set m_oUonsSeleccionadas = Value
    If m_oUonsSeleccionadas.Count > 0 Then
        txtUonSeleccionada.Text = m_oUonsSeleccionadas.titulo
    End If
End Property
