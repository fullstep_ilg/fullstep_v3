VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELPer 
   BackColor       =   &H00808000&
   Caption         =   "Selecci�n de personas relacionadas con el proceso"
   ClientHeight    =   5325
   ClientLeft      =   60
   ClientTop       =   2640
   ClientWidth     =   9045
   ForeColor       =   &H00000000&
   Icon            =   "frmSELPer.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5325
   ScaleWidth      =   9045
   Begin VB.PictureBox picCod 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   525
      Left            =   0
      ScaleHeight     =   525
      ScaleWidth      =   6615
      TabIndex        =   9
      Top             =   400
      Width           =   6615
      Begin VB.TextBox tbBuscarCodigo 
         Height          =   285
         Left            =   2040
         TabIndex        =   11
         Top             =   120
         Width           =   1980
      End
      Begin VB.CommandButton cmdBuscarCodigo 
         Height          =   295
         Left            =   4200
         Picture         =   "frmSELPer.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   120
         Width           =   335
      End
      Begin VB.Label lblPersona 
         BackColor       =   &H00808000&
         Caption         =   "C�digo/Nombre/Apellido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   120
         TabIndex        =   12
         Top             =   165
         Width           =   1800
      End
   End
   Begin VB.PictureBox picRol 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   400
      Left            =   0
      ScaleHeight     =   405
      ScaleWidth      =   6615
      TabIndex        =   5
      Top             =   0
      Width           =   6615
      Begin SSDataWidgets_B.SSDBCombo sdbcRolCod 
         Height          =   285
         Left            =   960
         TabIndex        =   6
         Top             =   120
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1244
         Columns(2).Caption=   "Conv."
         Columns(2).Name =   "CONV"
         Columns(2).CaptionAlignment=   2
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   2
         Columns(3).Width=   1296
         Columns(3).Caption=   "Invitado"
         Columns(3).Name =   "INVI"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483630
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcRolDen 
         Height          =   285
         Left            =   1920
         TabIndex        =   7
         Top             =   120
         Width           =   2595
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1111
         Columns(2).Caption=   "Conv."
         Columns(2).Name =   "CONV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   2
         Columns(3).Width=   1376
         Columns(3).Caption=   "Invitado"
         Columns(3).Name =   "INVI"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         _ExtentX        =   4577
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblRol 
         BackColor       =   &H00808000&
         Caption         =   "Rol:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   120
         TabIndex        =   8
         Top             =   165
         Width           =   705
      End
   End
   Begin MSComctlLib.ListView lstvwPer 
      Height          =   3795
      Left            =   4800
      TabIndex        =   4
      Top             =   940
      Width           =   4155
      _ExtentX        =   7329
      _ExtentY        =   6694
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Persona"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "ROL"
         Object.Width           =   1746
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   3795
      Left            =   60
      TabIndex        =   0
      Top             =   940
      Width           =   4635
      _ExtentX        =   8176
      _ExtentY        =   6694
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   9045
      TabIndex        =   1
      Top             =   4950
      Width           =   9045
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4560
         TabIndex        =   3
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3360
         TabIndex        =   2
         Top             =   30
         Width           =   1005
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   180
      Top             =   4080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPer.frx":0D03
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPer.frx":10CB
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPer.frx":141F
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPer.frx":1773
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPer.frx":1AC7
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPer.frx":1BD3
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSELPer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private iAnchura As Integer
Private iAltura As Integer
Private oRoles As CRoles
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Private sIdiRol As String

' Variables para interactuar con otros forms
Public sOrigen As String
Public oPerSeleccionada As CPersona
Public sCodPer As String
Public bRUO As Boolean
Public bRPerfUO As Boolean
Public bRDep As Boolean

' Variable de control de flujo
Public Accion As accionessummit

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private m_lNodo As Long
''' <summary>
''' Traslada la persona seleccionada a la pantalla llamante
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwestrorg.selectedItem
    
    If Not nodx Is Nothing Then
            
        Screen.MousePointer = vbHourglass
        Select Case sOrigen
        
            Case "frmPROCE"
                    
                    If Left(nodx.Tag, 3) = "PER" Then
                        sCodPer = DevolverCod(nodx)
                        frmPROCE.AnyadirPersonasSeleccionada
                    End If
            
            Case "frmREUPer"
                    
                    If Left(nodx.Tag, 3) = "PER" Then
                        sCodPer = DevolverCod(nodx)
                        frmREUPer.AnyadirPersonasSeleccionadaReu
                    End If
            
            Case "frmPARASIS"
                    
                    If Left(nodx.Tag, 3) = "PER" Then
                        sCodPer = DevolverCod(nodx)
                        frmPARAsis.AnyadirPersonasSeleccionadas
                    End If
                    
            Case Else
            
                Select Case Left(nodx.Tag, 4)
                
                    Case "PER0"
                        
                        Set oPerSeleccionada = oFSGSRaiz.Generar_CPersona
                        oPerSeleccionada.CodDep = DevolverCod(nodx.Parent)
                        oPerSeleccionada.Cod = DevolverCod(nodx)
                        
                        
                    Case "PER1"
                        
                        Set oPerSeleccionada = oFSGSRaiz.Generar_CPersona
                        oPerSeleccionada.CodDep = DevolverCod(nodx.Parent)
                        oPerSeleccionada.Cod = DevolverCod(nodx)
                        oPerSeleccionada.UON1 = DevolverCod(nodx.Parent.Parent)
                                        
                    Case "PER2"
                        
                        Set oPerSeleccionada = oFSGSRaiz.Generar_CPersona
                        oPerSeleccionada.CodDep = DevolverCod(nodx.Parent)
                        oPerSeleccionada.Cod = DevolverCod(nodx)
                        oPerSeleccionada.UON1 = DevolverCod(nodx.Parent.Parent.Parent)
                        oPerSeleccionada.UON2 = DevolverCod(nodx.Parent.Parent)
                        
                    Case "PER3"
                        
                        Set oPerSeleccionada = oFSGSRaiz.Generar_CPersona
                        oPerSeleccionada.CodDep = DevolverCod(nodx.Parent)
                        oPerSeleccionada.Cod = DevolverCod(nodx)
                        oPerSeleccionada.UON1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                        oPerSeleccionada.UON2 = DevolverCod(nodx.Parent.Parent.Parent)
                        oPerSeleccionada.UON3 = DevolverCod(nodx.Parent.Parent)
                    
                    Case Else
                        Screen.MousePointer = vbNormal
                        Exit Sub
                
                End Select
                    
            End Select
        
        Screen.MousePointer = vbNormal
   
    
    Unload Me
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBuscarCodigo_Click()

Dim nodx As MSComctlLib.node
Dim i As Integer
Dim bParar As Boolean

If tbBuscarCodigo.Text = "" Or Len(tbBuscarCodigo) <= 2 Then Exit Sub

If m_lNodo >= tvwestrorg.Nodes.Count Then m_lNodo = 1
bParar = False
For i = m_lNodo + 1 To tvwestrorg.Nodes.Count
        Set nodx = tvwestrorg.Nodes.Item(i)
        If InStr(1, nodx.key, "PERS", vbTextCompare) > 0 Then
            If InStr(1, nodx.Text, tbBuscarCodigo.Text, vbTextCompare) > 0 Then
                nodx.Selected = True
                Set tvwestrorg.selectedItem = nodx
                m_lNodo = nodx.Index
                Exit For
            End If
            If i = tvwestrorg.Nodes.Count And Not bParar Then
                i = 2
                m_lNodo = 1
                bParar = True
            End If
        End If
Next
End Sub

Private Sub cmdCancelar_Click()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPER, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        sIdiRol = Ador(0).Value
        lblRol.caption = sIdiRol & ":"
        lstvwPer.ColumnHeaders(2).Text = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmSELPer.caption = Ador(0).Value
        Ador.MoveNext
        sdbcRolCod.Columns(0).caption = Ador(0).Value
        sdbcRolDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcRolCod.Columns(1).caption = Ador(0).Value
        sdbcRolDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcRolCod.Columns(2).caption = Ador(0).Value
        sdbcRolDen.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        lstvwPer.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        sdbcRolCod.Columns("INVI").caption = Ador(0).Value ' pongo el caption al nuevo elemento del combo Kepa Mu�oz 20/01/2006
        sdbcRolDen.Columns("INVI").caption = Ador(0).Value
        Ador.MoveNext
        lblPersona.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    m_bActivado = False
    iAltura = 5235
    iAnchura = 8220
    Me.Width = 8220
    Me.Height = 8935
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Set oRoles = oFSGSRaiz.generar_CRoles
       
    GenerarEstructuraOrg False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Resize()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 3500 Then Me.Height = 3500
    If Me.Width < 5000 Then Me.Width = 5000

    picNavigate.Top = Me.ScaleHeight - picNavigate.Height
    
    If sOrigen = "frmPARASIS" Or sOrigen = "frmCATSeguridad" Then
        picCod.Top = 0
    Else
        picCod.Top = picRol.Height
    End If
    
    tvwestrorg.Top = picCod.Top + picCod.Height
    lstvwPer.Top = tvwestrorg.Top
    
    tvwestrorg.Height = picNavigate.Top - tvwestrorg.Top - 100
    lstvwPer.Height = tvwestrorg.Height
        
    tvwestrorg.Width = (Me.ScaleWidth * 0.5) - 270
    lstvwPer.Left = tvwestrorg.Width + tvwestrorg.Left + 300
    lstvwPer.Width = (Me.ScaleWidth * 0.5) - 270
        
    cmdAceptar.Left = (Me.ScaleWidth * 0.5) - cmdAceptar.Width - 100
    cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + 200

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "Form_Resize", err, Erl)
      Exit Sub
   End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload frmESTRORGAnyaDep
    Unload frmESTRORGBuscarPersona
    Unload frmESTRORGBuscarUO
    Unload frmESTRORGBuscarDep
    Unload frmESTRORGDetalle
    Unload frmESTRORGPersona
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub lstvwPer_ItemClick(ByVal Item As MSComctlLib.listItem)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lstvwPer.ListItems.Remove (Item.key)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "lstvwPer_ItemClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcRolCod_DropDown()

    Dim oRol As CRol
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcRolCod.RemoveAll
    
    If bCargarComboDesde Then
        oRoles.CargarTodosLosRolesDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcRolCod.Text, , , , , basPublic.gParametrosInstalacion.gIdioma
    Else
        oRoles.CargarTodosLosRoles , , , , , , basPublic.gParametrosInstalacion.gIdioma
    End If
    
    For Each oRol In oRoles
        
        sdbcRolCod.AddItem oRol.Cod & Chr(m_lSeparador) & oRol.Den & Chr(m_lSeparador) & SQLBinaryToBoolean(oRol.Conv) & Chr(m_lSeparador) & SQLBinaryToBoolean(oRol.Invi) 'a�ado el invitado al combo Kepa Mu�oz 20/01/2005
    
    Next
    
    If bCargarComboDesde And Not oRoles.EOF Then
        sdbcRolCod.AddItem "..."
    End If

    sdbcRolCod.SelStart = 0
    sdbcRolCod.SelLength = Len(sdbcRolCod.Text)
    sdbcRolCod.Refresh
        
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcRolDen_DropDown()

    Dim oRol As CRol
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcRolDen.RemoveAll
    
    If bCargarComboDesde Then
        oRoles.CargarTodosLosRolesDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcRolDen.Text, True, , , basPublic.gParametrosInstalacion.gIdioma
    Else
        oRoles.CargarTodosLosRoles , , , True, , , basPublic.gParametrosInstalacion.gIdioma
    End If
    
    
    For Each oRol In oRoles
        sdbcRolDen.AddItem oRol.Den & Chr(m_lSeparador) & oRol.Cod & Chr(m_lSeparador) & SQLBinaryToBoolean(oRol.Conv) & Chr(m_lSeparador) & SQLBinaryToBoolean(oRol.Invi) ' a�ado el invitado al combo Kepa Mu�oz 20/01/2006
    Next
    
    If bCargarComboDesde And Not oRoles.EOF Then
        sdbcRolDen.AddItem "..."
    End If

    sdbcRolDen.SelStart = 0
    sdbcRolDen.SelLength = Len(sdbcRolCod.Text)
    sdbcRolDen.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Public Sub Ordenar(ByVal bOrdPorDen As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwestrorg.Nodes.clear
    GenerarEstructuraOrg bOrdPorDen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "Ordenar", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Genera la estructura de la organizaci�n</summary>
''' <param name="bOrdenadoPorDen">Orden por denominaci�n</param>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    ' Departamentos asociados
    Dim oDepsAsocN0 As CDepAsociados
    Dim oDepsAsocN1 As CDepAsociados
    Dim oDepsAsocN2 As CDepAsociados
    Dim oDepsAsocN3 As CDepAsociados
    Dim oDepAsoc As CDepAsociado
    ' Unidades organizativas
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    ' Personas
    Dim oPersN0 As CPersonas
    Dim oPersN1 As CPersonas
    Dim oPersN2 As CPersonas
    Dim oPersN3 As CPersonas
    Dim oPer As CPersona
    ' Otras
    Dim nodx As node
    Dim varCodPersona As Variant
    Dim lIdPerfil As Long
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
        
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador Then
        If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
        varCodPersona = oUsuarioSummit.Persona.Cod
        
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
        
        ' Cargamos las personas  de esos departamentos.
        
        For Each oDepAsoc In oDepsAsocN0
            oDepAsoc.CargarTodasLasPersonas , , , , , , , , , False
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False, , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil, True
            Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil, True
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , , , , , , , , , , , , bRPerfUO, lIdPerfil, True
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil, True
            Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , , bRPerfUO, lIdPerfil, True
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , , , bRPerfUO, lIdPerfil, True
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil, True
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil, True
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil, True
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , , , , False
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                    oDepAsoc.CargarTodasLasPersonas , , , , , , , , , False
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , , , , False
                Next
        End Select
    Else
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        oPersN0.CargarTodasLasPersonas , , , , 0, , , , , False, , , , , , , True
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, , , , , True
                oPersN1.CargarTodasLasPersonas , , , , 1, , , , , False, , , , , , , True
            Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, , , , , True
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False, , , , , True
                oPersN2.CargarTodasLasPersonas , , , , 2, , , , , False, , , , , , , True
                oPersN1.CargarTodasLasPersonas , , , , 1, , , , , False, , , , , , , True
            Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False, , , , , True
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True, , , , , True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False, , , , , True
                oPersN1.CargarTodasLasPersonas , , , , 1, , , , , False, , , , , , , True
                oPersN2.CargarTodasLasPersonas , , , , 2, , , , , False, , , , , , , True
                oPersN3.CargarTodasLasPersonas , , , , 3, , , , , False, , , , , , , True
        End Select
    End If
    
        
        
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
    Next
    
    ' Departamentos
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                nodx.Tag = "PER0" & CStr(oPer.Cod)
            Next
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                nodx.Tag = "PER1" & CStr(oPer.Cod)
            Next
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                nodx.Tag = "PER2" & CStr(oPer.Cod)
            Next
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        Next
    Else
        'Departamentos
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
        Next
                
        'Personas
        For Each oPer In oPersN0
            scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            nodx.Tag = "PER0" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN1
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
        
            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            nodx.Tag = "PER1" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN2
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
        
            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            nodx.Tag = "PER2" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN3
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
            scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
        
            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            nodx.Tag = "PER3" & CStr(oPer.Cod)
        Next
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "GenerarEstructuraOrg", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub tbBuscarCodigo_Change()
Dim nodx As MSComctlLib.node
    If tbBuscarCodigo.Text = "" Or Len(tbBuscarCodigo) <= 2 Then Exit Sub
    For Each nodx In tvwestrorg.Nodes
        If InStr(1, nodx.key, "PERS", vbTextCompare) > 0 Then
            If InStr(1, nodx.Text, tbBuscarCodigo.Text, vbTextCompare) > 0 Then
                nodx.Selected = True
                Set tvwestrorg.selectedItem = nodx
                m_lNodo = nodx.Index
                Exit For
            End If
        End If
    Next
End Sub

Private Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next
    
    If sOrigen <> "frmPARASIS" And sOrigen <> "frmCATSeguridad" Then
        
        If Left(node.Tag, 3) = "PER" Then
            If sdbcRolCod.Value = "" Then
                oMensajes.NoValido (sIdiRol)
            Else
                                                
                lstvwPer.ListItems.Add , node.key & sdbcRolCod.Value, node.Text, , "Persona"
                lstvwPer.ListItems.Item(node.key & sdbcRolCod.Value).Tag = DevolverCod(node)
                lstvwPer.ListItems.Item(node.key & sdbcRolCod.Value).ListSubItems.Add , node.key & sdbcRolCod.Value, sdbcRolCod.Value, , node.Text
            End If
        End If
    
    Else
        If Left(node.Tag, 3) = "PER" Then
            lstvwPer.ListItems.Add , node.key, node.Text, , "Persona"
            lstvwPer.ListItems.Item(node.key).Tag = DevolverCod(node)
            lstvwPer.ListItems.Item(node.key).ListSubItems.Add , node.key, , node.Text
        End If
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "tvwestrorg_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcRolCod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcRolDen.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
    
    End If
    
End Sub

Private Sub sdbcRolCod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcRolCod.Value = "..." Then
        sdbcRolCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcRolDen.Text = sdbcRolCod.Columns(1).Text
    sdbcRolCod.Text = sdbcRolCod.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub

Private Sub sdbcRolCod_InitColumnProps()

    sdbcRolCod.DataFieldList = "Column 0"
    sdbcRolCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcRolCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcRolCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcRolCod.Rows - 1
            bm = sdbcRolCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcRolCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcRolCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolCod_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Public Sub sdbcRolCod_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcRolCod.Text = "" Then Exit Sub
    
    If sdbcRolCod.Text = sdbcRolCod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcRolDen.Text = sdbcRolCod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el Rol
    Screen.MousePointer = vbHourglass
    oRoles.CargarTodosLosRoles sdbcRolCod.Value, , True, , , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    If oRoles.Count = 0 Then
        sdbcRolCod.Text = ""
        
    Else
        bRespetarCombo = True
        sdbcRolDen.Text = oRoles.Item(1).Den
        sdbcRolCod.RemoveAll
        sdbcRolCod.AddItem oRoles.Item(1).Cod & Chr(m_lSeparador) & oRoles.Item(1).Den & Chr(m_lSeparador) & SQLBinaryToBoolean(oRoles.Item(1).Conv) & Chr(m_lSeparador) & SQLBinaryToBoolean(oRoles.Item(1).Invi)
        bRespetarCombo = False
        bCargarComboDesde = False
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcRolden_Change()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcRolCod.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolden_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcRolDen_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcRolDen.Value = "..." Then
        sdbcRolDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcRolDen.Text = sdbcRolDen.Columns(0).Text
    sdbcRolCod.Text = sdbcRolDen.Columns(1).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub

Private Sub sdbcRolDen_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcRolDen.DataFieldList = "Column 0"
    sdbcRolDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub sdbcRolDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcRolDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcRolDen.Rows - 1
            bm = sdbcRolDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcRolDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcRolDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolDen_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub sdbcRolDen_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcRolDen.Text = "" Then Exit Sub
          
    If sdbcRolDen.Text = sdbcRolDen.Columns(0).Text Then
        bRespetarCombo = True
        sdbcRolCod.Text = sdbcRolDen.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el Rolso
    Screen.MousePointer = vbHourglass
    oRoles.CargarTodosLosRoles , sdbcRolDen.Value, True, , , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    If oRoles.Count = 0 Then
        sdbcRolDen.Text = ""
    Else
        bRespetarCombo = True
        sdbcRolDen.Text = oRoles.Item(1).Den
        bRespetarCombo = False
        bCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPer", "sdbcRolDen_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


