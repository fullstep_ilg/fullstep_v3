VERSION 5.00
Begin VB.Form frmSeguimientoEsp 
   BackColor       =   &H00808000&
   Caption         =   "DEspecificaciones"
   ClientHeight    =   3090
   ClientLeft      =   6315
   ClientTop       =   4725
   ClientWidth     =   7575
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSeguimientoEsp.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   7575
   Begin FSGSClient.ctlAdjunPedidos ctlAdjunPedidos 
      Height          =   2955
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   5212
   End
End
Attribute VB_Name = "frmSeguimientoEsp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bActivado As Boolean

Private Sub Form_Activate()
On Error Resume Next
If Not m_bActivado Then
    ctlAdjunPedidos.Initialize Me.Backcolor
    
End If
m_bActivado = True

End Sub

Private Sub Form_Load()
Dim Ador As Ador.Recordset
Dim i As Integer
m_bActivado = False
Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEGUIMIENTO_ADJUN, basPublic.gParametrosInstalacion.gIdioma)
If Not Ador Is Nothing Then
    For i = 1 To 7
        Ador.MoveNext
    Next
    Me.caption = Ador(0).Value
End If
Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Resize()
    
ctlAdjunPedidos.Width = Me.Width - 20
ctlAdjunPedidos.Height = Me.Height - 350
    
End Sub




