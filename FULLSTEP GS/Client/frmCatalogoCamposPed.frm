VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCatalogoCamposPed 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCampos Pedido"
   ClientHeight    =   6135
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14685
   Icon            =   "frmCatalogoCamposPed.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6135
   ScaleWidth      =   14685
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab tabCamposPedidoCatalogo 
      Height          =   5865
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14505
      _ExtentX        =   25585
      _ExtentY        =   10345
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DCamposPedidoLinea"
      TabPicture(0)   =   "frmCatalogoCamposPed.frx":0562
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frmCamposPedidoLinea"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "frmA�adirCamposPedidoLinea(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "frmA�adirCamposPedidoLinea(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "DCamposPedidoCabecera"
      TabPicture(1)   =   "frmCatalogoCamposPed.frx":057E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picNavigateAdj"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "frmCamposPedidoCabecera"
      Tab(1).ControlCount=   2
      Begin VB.Frame frmA�adirCamposPedidoLinea 
         BackColor       =   &H00808000&
         Height          =   2505
         Index           =   0
         Left            =   7800
         TabIndex        =   10
         Top             =   3240
         Width           =   6615
         Begin VB.CommandButton cmdPasarTodosALinea 
            Caption         =   "ALL"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   450
            Index           =   0
            Left            =   120
            Picture         =   "frmCatalogoCamposPed.frx":059A
            Style           =   1  'Graphical
            TabIndex        =   12
            Top             =   1365
            Width           =   495
         End
         Begin VB.CommandButton cmdPasarALinea 
            Height          =   315
            Index           =   0
            Left            =   120
            Picture         =   "frmCatalogoCamposPed.frx":08F3
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   945
            Width           =   495
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgCamposPedidos 
            Height          =   1860
            Index           =   0
            Left            =   720
            TabIndex        =   13
            Top             =   480
            Width           =   5820
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   9
            stylesets.count =   2
            stylesets(0).Name=   "Normal"
            stylesets(0).ForeColor=   0
            stylesets(0).BackColor=   16777215
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCamposPed.frx":0C4C
            stylesets(1).Name=   "Selected"
            stylesets(1).ForeColor=   16777215
            stylesets(1).BackColor=   8388608
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCatalogoCamposPed.frx":0C68
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   -2147483630
            ForeColorOdd    =   -2147483630
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   9
            Columns(0).Width=   2090
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).BackColor=   16776960
            Columns(1).Width=   5159
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).BackColor=   16776960
            Columns(2).Width=   2381
            Columns(2).Caption=   "Valor"
            Columns(2).Name =   "VAL"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).BackColor=   16776960
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "TIPO_INTRODUCCION"
            Columns(3).Name =   "INTRO"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "TIPO_DATOS"
            Columns(4).Name =   "TIPO_DATO"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "MIN"
            Columns(5).Name =   "MIN"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "MAX"
            Columns(6).Name =   "MAX"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "ATRIBID"
            Columns(7).Name =   "ATRIB_ID"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "ID"
            Columns(8).Name =   "ID"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            _ExtentX        =   10266
            _ExtentY        =   3281
            _StockProps     =   79
            BackColor       =   -2147483643
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblCamposOferta 
            BackColor       =   &H00808000&
            Caption         =   "DCampos de la oferta adjudicada"
            ForeColor       =   &H8000000E&
            Height          =   255
            Left            =   120
            TabIndex        =   14
            Top             =   240
            Width           =   5400
         End
      End
      Begin VB.Frame frmA�adirCamposPedidoLinea 
         BackColor       =   &H00808000&
         Height          =   2505
         Index           =   1
         Left            =   7800
         TabIndex        =   5
         Top             =   600
         Width           =   6615
         Begin VB.CommandButton cmdPasarALinea 
            Height          =   315
            Index           =   1
            Left            =   120
            Picture         =   "frmCatalogoCamposPed.frx":0C84
            Style           =   1  'Graphical
            TabIndex        =   7
            Top             =   945
            Width           =   495
         End
         Begin VB.CommandButton cmdPasarTodosALinea 
            Caption         =   "ALL"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   450
            Index           =   1
            Left            =   120
            Picture         =   "frmCatalogoCamposPed.frx":0FDD
            Style           =   1  'Graphical
            TabIndex        =   6
            Top             =   1365
            Width           =   495
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgCamposPedidos 
            Height          =   1860
            Index           =   1
            Left            =   720
            TabIndex        =   8
            Top             =   480
            Width           =   5850
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   10
            stylesets.count =   2
            stylesets(0).Name=   "Normal"
            stylesets(0).ForeColor=   0
            stylesets(0).BackColor=   16777215
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCamposPed.frx":1336
            stylesets(1).Name=   "Selected"
            stylesets(1).ForeColor=   16777215
            stylesets(1).BackColor=   8388608
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCatalogoCamposPed.frx":1352
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   -2147483630
            ForeColorOdd    =   -2147483630
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   10
            Columns(0).Width=   2090
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).BackColor=   16776960
            Columns(1).Width=   5080
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).BackColor=   16776960
            Columns(2).Width=   2461
            Columns(2).Caption=   "Valor"
            Columns(2).Name =   "VALOR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).BackColor=   16776960
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "TIPO_INTRODUCCION"
            Columns(3).Name =   "INTRO"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "TIPO_DATOS"
            Columns(4).Name =   "TIPO_DATO"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "MIN"
            Columns(5).Name =   "MIN"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "MAX"
            Columns(6).Name =   "MAX"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "ATRIBID"
            Columns(7).Name =   "ATRIB_ID"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "ORIGEN"
            Columns(8).Name =   "ORIGEN"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "ID"
            Columns(9).Name =   "ID"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            _ExtentX        =   10319
            _ExtentY        =   3281
            _StockProps     =   79
            BackColor       =   -2147483643
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblCamposSolicitud 
            BackColor       =   &H00808000&
            Caption         =   "DCampos de la solicitud"
            ForeColor       =   &H8000000E&
            Height          =   255
            Left            =   120
            TabIndex        =   9
            Top             =   240
            Width           =   5400
         End
      End
      Begin VB.Frame frmCamposPedidoLinea 
         Height          =   5295
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   7575
         Begin VB.CommandButton cmdAnyadirCampo 
            Caption         =   "DA�adir Otros"
            Height          =   375
            Left            =   4920
            TabIndex        =   17
            Top             =   4800
            Width           =   1215
         End
         Begin VB.CommandButton cmdEliminarCampo 
            Caption         =   "DEliminar"
            Height          =   375
            Left            =   6360
            TabIndex        =   16
            Top             =   4800
            Width           =   1095
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgCamposPedidoLinea 
            Height          =   4170
            Left            =   0
            TabIndex        =   4
            Top             =   480
            Width           =   7470
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   15
            stylesets.count =   3
            stylesets(0).Name=   "Gris"
            stylesets(0).BackColor=   -2147483633
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCamposPed.frx":136E
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCatalogoCamposPed.frx":138A
            stylesets(2).Name=   "Selected"
            stylesets(2).ForeColor=   16777215
            stylesets(2).BackColor=   8388608
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmCatalogoCamposPed.frx":13A6
            AllowDelete     =   -1  'True
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   -2147483630
            ForeColorOdd    =   -2147483630
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   15
            Columns(0).Width=   1905
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(1).Width=   4630
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   2619
            Columns(2).Caption=   "DValor"
            Columns(2).Name =   "VALOR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   1164
            Columns(3).Caption=   "DOblig"
            Columns(3).Name =   "OBLIG"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Style=   2
            Columns(4).Width=   2461
            Columns(4).Caption=   "DOrigen"
            Columns(4).Name =   "ORIGEN"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(5).Width=   1455
            Columns(5).Caption=   "DInterno"
            Columns(5).Name =   "INTERNO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Style=   2
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "TIPO_DATO"
            Columns(6).Name =   "TIPO_DATO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "INTRO"
            Columns(7).Name =   "INTRO"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "MAX"
            Columns(8).Name =   "MAX"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MIN"
            Columns(9).Name =   "MIN"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "ATRIB_ID"
            Columns(10).Name=   "ATRIB_ID"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "ID"
            Columns(11).Name=   "ID"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "ORIGEN_VALOR"
            Columns(12).Name=   "ORIGEN_VALOR"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "LISTA_EXTERNA"
            Columns(13).Name=   "LISTA_EXTERNA"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   11
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Caption=   "DMostrar En Recepci�n"
            Columns(14).Name=   "VERENRECEP"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(14).Style=   2
            _ExtentX        =   13176
            _ExtentY        =   7355
            _StockProps     =   79
            BackColor       =   -2147483643
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValorLinea 
            Height          =   360
            Left            =   120
            TabIndex        =   15
            Top             =   4800
            Width           =   3015
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCamposPed.frx":13C2
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   635
            _StockProps     =   77
         End
         Begin VB.Label lblCamposLineaPedido 
            Caption         =   "dCampos de la linea de pedido:"
            Height          =   255
            Left            =   120
            TabIndex        =   23
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.Frame frmCamposPedidoCabecera 
         Height          =   5175
         Left            =   -74760
         TabIndex        =   2
         Top             =   480
         Width           =   13935
         Begin SSDataWidgets_B.SSDBGrid sdbgCamposPedidoCabecera 
            Height          =   2010
            Left            =   120
            TabIndex        =   18
            Top             =   480
            Width           =   13455
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   14
            stylesets.count =   3
            stylesets(0).Name=   "Gris"
            stylesets(0).BackColor=   -2147483633
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCamposPed.frx":13DE
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCatalogoCamposPed.frx":13FA
            stylesets(2).Name=   "Selected"
            stylesets(2).ForeColor=   16777215
            stylesets(2).BackColor=   8388608
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmCatalogoCamposPed.frx":1416
            AllowDelete     =   -1  'True
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            StyleSet        =   "Gris"
            ForeColorEven   =   -2147483630
            ForeColorOdd    =   -2147483630
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   14
            Columns(0).Width=   5027
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(1).Width=   9022
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   3545
            Columns(2).Caption=   "DValor"
            Columns(2).Name =   "VALOR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   1164
            Columns(3).Caption=   "DOblig"
            Columns(3).Name =   "OBLIG"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).Style=   2
            Columns(4).Width=   2461
            Columns(4).Caption=   "DOrigen"
            Columns(4).Name =   "ORIGEN"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(5).Width=   1455
            Columns(5).Caption=   "DInterno"
            Columns(5).Name =   "INTERNO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(5).Style=   2
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "TIPO_DATO"
            Columns(6).Name =   "TIPO_DATO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "INTRO"
            Columns(7).Name =   "INTRO"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Locked=   -1  'True
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "MAX"
            Columns(8).Name =   "MAX"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Locked=   -1  'True
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MIN"
            Columns(9).Name =   "MIN"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "ATRIB_ID"
            Columns(10).Name=   "ATRIB_ID"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(10).Locked=   -1  'True
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "ID"
            Columns(11).Name=   "ID"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Locked=   -1  'True
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "ORIGEN_VALOR"
            Columns(12).Name=   "ORIGEN_VALOR"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Locked=   -1  'True
            Columns(13).Width=   3200
            Columns(13).Caption=   "dMostrar En Recepci�n"
            Columns(13).Name=   "VERENRECEP"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(13).Locked=   -1  'True
            Columns(13).Style=   2
            _ExtentX        =   23733
            _ExtentY        =   3545
            _StockProps     =   79
            BackColor       =   -2147483643
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcTiposPedido 
            Height          =   285
            Left            =   2280
            TabIndex        =   21
            Top             =   2640
            Width           =   4695
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5450
            Columns(0).Caption=   "Denominacion"
            Columns(0).Name =   "Denominacion"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "Codigo"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Id"
            Columns(2).Name =   "Id"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   8281
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgCamposPedidoTipoPedido 
            Height          =   2010
            Left            =   120
            TabIndex        =   22
            Top             =   3000
            Width           =   13455
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   13
            stylesets.count =   3
            stylesets(0).Name=   "Gris"
            stylesets(0).BackColor=   -2147483633
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCamposPed.frx":1432
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCatalogoCamposPed.frx":144E
            stylesets(2).Name=   "Selected"
            stylesets(2).ForeColor=   16777215
            stylesets(2).BackColor=   8388608
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmCatalogoCamposPed.frx":146A
            AllowDelete     =   -1  'True
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            StyleSet        =   "Gris"
            ForeColorEven   =   -2147483630
            ForeColorOdd    =   -2147483630
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   13
            Columns(0).Width=   6059
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(1).Width=   11033
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   4683
            Columns(2).Caption=   "DValor"
            Columns(2).Name =   "VALOR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   1164
            Columns(3).Caption=   "DOblig"
            Columns(3).Name =   "OBLIG"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).Style=   2
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "DOrigen"
            Columns(4).Name =   "ORIGEN"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "DInterno"
            Columns(5).Name =   "INTERNO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(5).Style=   2
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "TIPO_DATO"
            Columns(6).Name =   "TIPO_DATO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "INTRO"
            Columns(7).Name =   "INTRO"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Locked=   -1  'True
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "MAX"
            Columns(8).Name =   "MAX"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Locked=   -1  'True
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MIN"
            Columns(9).Name =   "MIN"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(9).Locked=   -1  'True
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "ATRIB_ID"
            Columns(10).Name=   "ATRIB_ID"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(10).Locked=   -1  'True
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "ID"
            Columns(11).Name=   "ID"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Locked=   -1  'True
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "ORIGEN_VALOR"
            Columns(12).Name=   "ORIGEN_VALOR"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Locked=   -1  'True
            _ExtentX        =   23733
            _ExtentY        =   3545
            _StockProps     =   79
            BackColor       =   -2147483643
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblCamposTipoDePedidoCabecera 
            Caption         =   "dCampos del tipo de pedido:"
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   2640
            Width           =   2415
         End
         Begin VB.Label lblCamposDePedidoDeCategoriaCabecera 
            Caption         =   "dCampos de pedido de la categoria:"
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   240
            Width           =   5055
         End
      End
      Begin VB.PictureBox picNavigateAdj 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   -74880
         ScaleHeight     =   420
         ScaleWidth      =   11250
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   6945
         Width           =   11250
      End
   End
End
Attribute VB_Name = "frmCatalogoCamposPed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public g_oLinea         As CLineaCatalogo
Public g_oCamposPedidoLinea   As CCampos  'Campos de pedido de ambito linea
Public g_bRecep As Boolean
'Variables privadas
Private m_oAtributosLinea   As CAtributos 'Campos de pedido de ambito linea
Private m_oAtributosCabecera   As CAtributos 'Campos de pedido de ambito cabecera
Private m_oCamposPedidoCategoria   As CCampos 'Campos de pedido de la categoria del catalogo
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_oAtributoEnEdicion As CCampo  'Atributo que se copia a la linea de catalogo
Private m_bCamposPedidoCabeceraCargados As Boolean
Private m_bModificarValorCamposPedido As Boolean
Private m_sNoHayCampoSeleccionado As String
Private m_sEliminarCampoCategoria As String
Private m_sEliminaCampoPedido As String
Private m_sMaestro As String
Private m_sProceso As String
Private m_sSolicitud As String
Private m_sCategoria As String




''' <summary>Mostrara el maestro de atributos para a�adirlo como campo de pedido de la linea de catalogo</summary>
Private Sub cmdAnyadirCampo_Click()
    Dim ofrmATRIBMod As frmAtribMod
        
    Set ofrmATRIBMod = New frmAtribMod
    ofrmATRIBMod.g_sOrigen = "frmCatalogoCamposPed"
    Set g_oCamposPedidoLinea = oFSGSRaiz.Generar_CCampos
    ofrmATRIBMod.Show vbModal
    
    'GUARDAMOS LOS CAMPOS A�ADIDOS EN BDD
    Dim tsError As TipoErrorSummit
    tsError = g_oLinea.AnyadirCamposPedido(g_oCamposPedidoLinea, g_bRecep)
    'RECARGAMOS LAS L�NEAS
    Set m_oAtributosLinea = Nothing
    Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
    Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , CamposPedido, Linea)
    sdbgCamposPedidoLinea.Refresh
    
    'Ponemos el ID a cada l�nea del grid
    With sdbgCamposPedidoLinea
        If .Rows > 0 And m_oAtributosLinea.Count > 0 Then
            Dim vBmIni As Variant
            Dim vbm As Variant
            Dim oAtr As CAtributo
            
            vBmIni = .Bookmark
            For Each oAtr In m_oAtributosLinea
                Dim i As Integer
                For i = 0 To .Rows - 1
                    vbm = .AddItemBookmark(i)
                    If .Columns("COD").CellValue(vbm) = oAtr.Cod And .Columns("ATRIB_ID").CellValue(vbm) = CStr(oAtr.Atrib) Then
                        If (.Columns("ID").CellValue(vbm) = CStr(0) Or .Columns("ID").CellValue(vbm) = "") Then
                            .Bookmark = vbm
                            .Columns("ID").Value = oAtr.Id
                        End If
                        Exit For
                    End If
                Next
            Next
            .Bookmark = vBmIni
            
            Set oAtr = Nothing
        End If
    End With

    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

''' <summary>Elimina uno o varios campos de pedido de la linea de catalogo(CATALOG_LIN_ATRIB)</summary>
Private Sub cmdEliminarCampo_Click()
    Screen.MousePointer = vbHourglass
    Dim inum As Integer
    Dim v As Variant
    Dim oCamposPedido As CCampos
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    
    sdbgCamposPedidoLinea.Columns("VALOR").DropDownHwnd = 0
    With sdbgCamposPedidoLinea
        If .SelBookmarks.Count > 0 Then
            'Si el campo de pedido que se pretende eliminar es heredado de la categoria se indicara que no se puede eliminar
            If .Columns("ORIGEN_VALOR").Value = "CAT" Then
                MsgBox m_sEliminarCampoCategoria, vbInformation
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            i = oMensajes.PreguntaEliminar(m_sEliminaCampoPedido)
            If i = vbYes Then
                inum = 0
                Set oCamposPedido = oFSGSRaiz.Generar_CCampos
                While inum < .SelBookmarks.Count
                    .Bookmark = .SelBookmarks(inum)
                    oCamposPedido.Add .Columns("ID").Value, .Columns("COD").Text, .Columns("DEN").Text
                inum = inum + 1
                Wend
                '3. ELIMINAR DE BDD
                
                tsError = g_oLinea.EliminarCamposPedido(oCamposPedido)
                '2.ELIMINAR DEL GRID
                .DeleteSelected
                .Update
                .Refresh
                '4. RECARGAR LA VARIABLE DE LOS ATRIBUTOS
                Set m_oAtributosLinea = Nothing
                Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
                Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , CamposPedido, Linea)
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox m_sNoHayCampoSeleccionado, vbInformation
            Exit Sub
        End If
    End With
    'frmCatalogo.g_sOrigen = ""
    sdbgCamposPedidoLinea.MoveLast
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Pasa los campos de pedido(atributos) de solicitud o de proceso a campos de pedido de la linea de catalogo(CATALOG_LIN_ATRIB)</summary>
Private Sub cmdPasarALinea_Click(Index As Integer)
    Select Case Index
    Case 0 'Campos Pedido de la oferta adjudicada de ese proceso
        CopiarAtributosALineaCatalogo sdbgCamposPedidos(Index), AtributosLineaCatalogo.ProveArtYProceOfer, False
    Case 1 'Campos Pedido de solicitud
        CopiarAtributosALineaCatalogo sdbgCamposPedidos(Index), AtributosLineaCatalogo.ProceEsp, False
    End Select
End Sub

Private Sub cmdPasarTodosALinea_Click(Index As Integer)
    Select Case Index
    Case 0 'Campos Pedido de la oferta adjudicada de ese proceso
        CopiarAtributosALineaCatalogo sdbgCamposPedidos(Index), AtributosLineaCatalogo.ProveArtYProceOfer, True
    Case 1 'Campos Pedido de solicitud
        CopiarAtributosALineaCatalogo sdbgCamposPedidos(Index), AtributosLineaCatalogo.ProceEsp, True
    End Select
End Sub

Private Sub Form_Load()
    If g_oLinea Is Nothing Then Unload Me
    
    PonerFieldSeparator Me
    
    CargarRecursos
    
    'Cargamos en el objeto m_oCamposPedidoCategoria los campos de pedido de ambito linea de la categoria y los carga en la grid
    'Estos campos no se podrian modificar
    CargarCamposPedidoCategoria (Linea)
    'Cargamos en el objeto m_oAtributosLinea los campos de pedido de ambito linea y su grid
    CargarCamposPedido (ambitodelcampodepedido.Linea)
    'Comprobamos si se puede modificar el valor de los campos de pedido y bloqueamos o no la columna valor del grid
    ConfigurarSeguridadGridCamposPedidoLinea
    
    'Comprobamos si tenemos que mostrar los grids de los campos de pedido de solicitud y proceso
    If ConfigurarVisibilidadGridsCamposPedido Then
        
        CargarCamposPedidoDeSolicitud
        
        CargarCamposPedidoDeOfertaAdjudicada
    End If
End Sub

''' Copiar atributos de solicitud, o proceso a la l�nea de catalogo
''' grid: grid DESDE LA que copiamos los datos A LA l�nea
''' tipoAtributo: El tipo de atributo DESDE el que a�adimos los registros, de solicitud o de oferta de proceso
Private Function CopiarAtributosALineaCatalogo(ByRef Grid As SSDBGrid, ByVal tipoAtributo As AtributosLineaCatalogo, ByVal copiarTodos As Boolean) As TipoErrorSummit
    Dim inum As Integer
    Dim v As Variant
    Dim oCamposPedidoAGrabar As CCampos
    Dim oCamposPedido As CCampos
    Dim tsError As TipoErrorSummit
    Dim inum2 As Integer
    Dim vbm As Variant
    Dim i As Integer
    Dim valorText As Variant
    Dim valorNum As Variant
    Dim valorFec As Variant
    Dim valorBool As Variant
    
    Set oCamposPedidoAGrabar = oFSGSRaiz.Generar_CCampos
    
    If Not copiarTodos Then
        If Grid.SelBookmarks.Count = 0 Then
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox m_sNoHayCampoSeleccionado, vbInformation
            Exit Function
        End If
    Else
        Grid.SelBookmarks.RemoveAll
        For i = 0 To Grid.Rows - 1
            vbm = Grid.AddItemBookmark(i)
            Grid.SelBookmarks.Add vbm
        Next i
    End If
    'Si hay algun campo de pedido seleccionado
    If Grid.SelBookmarks.Count > 0 Then
        inum = 0
        With sdbgCamposPedidoLinea
            If copiarTodos Then
                Dim oCampoPedido As CCampo
                Dim oAtributo As CAtributo
                Select Case tipoAtributo
                    Case AtributosLineaCatalogo.ProveArtYProceOfer
                        'Campos pedido de la oferta adjudicada de un proceso
                        Set oCamposPedido = g_oLinea.CamposPedidoOferta
                    Case AtributosLineaCatalogo.ProceEsp
                        'Campos pedido de una solicitud
                        Set oCamposPedido = g_oLinea.CamposPedidoSolicitud
                End Select
                'Reviso si el campo de pedido que se quiere copiar, ya esta en los campos de pedido de la linea de catalogo
                Dim vValor As Variant
                Dim bCampoPedidoEncontrado As Boolean
                
                For Each oCampoPedido In oCamposPedido
                    bCampoPedidoEncontrado = False
                    For Each oAtributo In m_oAtributosLinea
                        If oCampoPedido.AtribID = oAtributo.Atrib Then
                            bCampoPedidoEncontrado = True
                            Exit For
                        End If
                    Next
                    If bCampoPedidoEncontrado = False Then
                        'Si el atributo no esta se a�adira a la coleccion de campos de pedido que se a�adiran a la linea de catalogo
                        Select Case oCampoPedido.Tipo
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio:
                                vValor = oCampoPedido.valorText
                                valorText = oCampoPedido.valorText
                            Case TiposDeAtributos.TipoNumerico
                                vValor = oCampoPedido.valorNum
                                 valorNum = oCampoPedido.valorNum
                            Case TiposDeAtributos.TipoFecha
                                vValor = oCampoPedido.valorFec
                                valorFec = oCampoPedido.valorFec
                            Case TiposDeAtributos.TipoBoolean
                                vValor = oCampoPedido.valorBool
                                valorBool = oCampoPedido.valorBool
                        End Select
                        oCamposPedidoAGrabar.Add 0, oCampoPedido.Cod, oCampoPedido.Den, oCampoPedido.Obligatorio, vValor, _
                                         , oCampoPedido.ambito, oCampoPedido.TipoIntroduccion, oCampoPedido.Minimo, oCampoPedido.Maximo, oCampoPedido.Tipo, oCampoPedido.AtribID, oCampoPedido.Origen, valorNum, valorText, valorFec, valorBool
                        
                    End If
                Next
                
                
            Else
                While inum < Grid.SelBookmarks.Count
                    'Selecciono el campo de pedido a copiar a la linea de catalogo
                    Select Case tipoAtributo
                        Case AtributosLineaCatalogo.ProceEsp:
                            Set m_oAtributoEnEdicion = g_oLinea.CamposPedidoSolicitud.Item(CInt(Grid.Columns("ID").Value))
                        Case AtributosLineaCatalogo.ProveArtYProceOfer:
                            Set m_oAtributoEnEdicion = g_oLinea.CamposPedidoOferta.Item(CInt(Grid.Columns("ID").Value))
                    End Select
                    Grid.Bookmark = Grid.SelBookmarks(inum)
                    If .Rows > 0 Then
                        For inum2 = 0 To .Rows - 1
                            vbm = .AddItemBookmark(inum2)
                            If CLng(m_oAtributoEnEdicion.AtribID) = CLng(.Columns("ATRIB_ID").CellValue(vbm)) And .Columns("ORIGEN_VALOR").CellValue(vbm) = tipoAtributo Then
                                GoTo Siguiente
                            End If
                        Next inum2
                    End If
                    '1. CREAR VARIABLE DE ATRIBUTOS PARA A�ADIR A BDD DE LINEA
                    
                    valorText = Null
                    valorNum = Null
                    valorFec = Null
                    valorBool = Null
                    Select Case Grid.Columns("TIPO_DATO").Value
                        Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio:
                            valorText = Grid.Columns("VALOR").Value
                        Case TiposDeAtributos.TipoNumerico
                            valorNum = Grid.Columns("VALOR").Value
                        Case TiposDeAtributos.TipoFecha
                            valorFec = Grid.Columns("VALOR").Value
                        Case TiposDeAtributos.TipoBoolean
                            valorBool = Grid.Columns("VALOR").Value
                    End Select
                    
                    oCamposPedidoAGrabar.Add 0, Grid.Columns("COD").Value, Grid.Columns("DEN").Value, , Grid.Columns("VALOR").Value _
                                        , , ambitodelcampodepedido.Linea, Grid.Columns("INTRO").Value, Grid.Columns("MIN").Value, Grid.Columns("MAX").Value, Grid.Columns("TIPO_DATO").Value, Grid.Columns("ATRIB_ID").Value, tipoAtributo, valorNum, valorText, valorFec, valorBool
Siguiente:
                    inum = inum + 1
                Wend
            End If
            '2. A�ADIR A BDD
            tsError = g_oLinea.AnyadirCamposPedido(oCamposPedidoAGrabar, g_bRecep)
            If tsError.NumError <> TESnoerror Then
                GoTo ERROR
            End If
            inum = 0
            Set vbm = Nothing
            While inum < Grid.SelBookmarks.Count
                .AddNew
                'Voy seleccionando del grid del que se copia, la fila a copiar
                Grid.Bookmark = Grid.SelBookmarks(inum)
                
                Select Case tipoAtributo
                    Case AtributosLineaCatalogo.ProceEsp:
                        Set m_oAtributoEnEdicion = g_oLinea.CamposPedidoSolicitud.Item(CLng(Grid.Columns("ID").Value))
                    Case AtributosLineaCatalogo.ProveArtYProceOfer
                        Set m_oAtributoEnEdicion = g_oLinea.CamposPedidoOferta.Item(CLng(Grid.Columns("ID").Value))
                End Select
                inum2 = 0
                If .Rows > 0 Then
                    For inum2 = 0 To .Rows - 1
                        vbm = .AddItemBookmark(inum2)
                        If Not IsEmpty(vbm) Then
                            If m_oAtributoEnEdicion.AtribID = CLng(.Columns("ATRIB_ID").CellValue(vbm)) And .Columns("ORIGEN_VALOR").CellValue(vbm) = tipoAtributo Then
                                .CancelUpdate
                                GoTo Siguiente2
                            End If
                        End If
                    Next inum2
                End If
                '3. A�ADIR AL GRID DE CAMPOS DE PEDIDO DE L�NEA DE CATALOGO
                .Columns("COD").Text = Grid.Columns("COD").Value
                .Columns("DEN").Value = Grid.Columns("DEN").Value
                .Columns("VALOR").Value = Grid.Columns("VALOR").Value
                .Columns("VALOR").Text = Grid.Columns("VALOR").Text
                Dim sOrigen As String
                sOrigen = ""
                Select Case tipoAtributo
                    Case AtributosLineaCatalogo.ProveArtYProceOfer:
                       sOrigen = m_sProceso
                    Case AtributosLineaCatalogo.ProceEsp:
                       sOrigen = m_sSolicitud
                End Select
                .Columns("ORIGEN").Text = sOrigen
                .Columns("ORIGEN_VALOR").Text = tipoAtributo
                .Columns("INTRO").Value = Grid.Columns("INTRO").Value
                .Columns("TIPO_DATO").Value = Grid.Columns("TIPO_DATO").Value
                .Columns("MIN").Value = Grid.Columns("MIN").Value
                .Columns("MAX").Value = Grid.Columns("MAX").Value
                .Columns("ATRIB_ID").Value = Grid.Columns("ATRIB_ID").Value
                .Update
                .Refresh
Siguiente2:
                inum = inum + 1
            Wend
        End With
        'Recargamos la variable de atributos(campos de pedido) de la linea de catalogo
        Set m_oAtributosLinea = Nothing
        Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
        Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , CamposPedido, ambitodelcampodepedido.Linea)
        'Ponemos el ID a cada l�nea del grid
        With sdbgCamposPedidoLinea
            .MoveFirst
            If .Rows > 0 Then
                Dim oAtr As CAtributo
                For Each oAtr In m_oAtributosLinea
                    .MoveFirst
                    For i = 0 To .Rows
                        If (.Columns("ID").Value = CStr(0) Or .Columns("ID").Value = "") And .Columns("COD").Value = oAtr.Cod And .Columns("ATRIB_ID").Value = CStr(oAtr.Atrib) Then
                            .Columns("ID").Value = oAtr.Id
                            Exit For
                        End If
                        .MoveNext
                    Next
                Next
            End If
        End With
    End If
    Grid.SelBookmarks.RemoveAll
    sdbgCamposPedidoLinea.MoveLast
ERROR:
    CopiarAtributosALineaCatalogo = tsError
End Function

''' <summary>En el caso de que la l�nea del cat�logo no proceda de una adjudicaci�n, la pantalla no mostrar� las grids para a�adir los campos de pedidos y devolvera false, true si muestra los grids</summary>
Private Function ConfigurarVisibilidadGridsCamposPedido() As Boolean
    If g_oLinea.Anyo <> "" And g_oLinea.GMN1Cod <> "" And g_oLinea.ProceCod <> "" Then
        frmA�adirCamposPedidoLinea(0).Visible = True
        frmA�adirCamposPedidoLinea(1).Visible = True
        tabCamposPedidoCatalogo.Width = frmCamposPedidoLinea.Width + frmA�adirCamposPedidoLinea(0).Width + 400
        frmCatalogoCamposPed.Width = tabCamposPedidoCatalogo.Width + 300
        ConfigurarVisibilidadGridsCamposPedido = True
        sdbgCamposPedidoCabecera.Width = 13455
        sdbgCamposPedidoTipoPedido.Width = 13455
    Else
        frmA�adirCamposPedidoLinea(0).Visible = False
        frmA�adirCamposPedidoLinea(1).Visible = False
        tabCamposPedidoCatalogo.Width = frmCamposPedidoLinea.Width + 400
        frmCatalogoCamposPed.Width = tabCamposPedidoCatalogo.Width + 300
        sdbgCamposPedidoCabecera.Width = Me.sdbgCamposPedidoLinea.Width
        sdbgCamposPedidoTipoPedido.Width = Me.sdbgCamposPedidoLinea.Width
        ConfigurarVisibilidadGridsCamposPedido = False
    End If
    
    Me.sdbgCamposPedidoCabecera.Columns("VERENRECEP").Visible = (Not g_bRecep)
    Me.sdbgCamposPedidoLinea.Columns("VERENRECEP").Visible = (Not g_bRecep)
End Function
''' <summary>Cargaremos en el objeto solicitud los campos de pedido de esa solicitud a traves de el proceso de esa linea de catalogo</summary>
Private Sub CargarCamposPedidoDeSolicitud()
    g_oLinea.CargarCamposPedidoSolicitud
    
    CargarGridCamposPedidoSolicitud
End Sub
''' <summary>Cargaremos en el objeto Oferta los campos de pedido de esa oferta a traves de el proceso de esa linea de catalogo</summary>
Private Sub CargarCamposPedidoDeOfertaAdjudicada()
    g_oLinea.CargarCamposPedidoOfertaAdjudicada
    
    CargarGridCamposPedidoOfertaAdjudicada
End Sub

''' <summary>Carga el objeto de m_oCamposPedidoCategoria con los campos de pedido del ambito linea de la categoria de catalogo</summary>
Private Sub CargarCamposPedidoCategoria(ambito As ambitodelcampodepedido)
    Dim oCampoPerCategoria As CCampo
    Dim Maximo As Variant
    Dim Minimo As Variant
    If g_oLinea.Cat1 <> "" And g_oLinea.Cat2 <> "" And g_oLinea.Cat3 <> "" And g_oLinea.Cat4 <> "" And g_oLinea.Cat5 <> "" Then
        Dim oCategoriaN5 As CCategoriaN5
        Set oCategoriaN5 = oFSGSRaiz.Generar_CCategoriaN5
        oCategoriaN5.Cat1 = g_oLinea.Cat1
        oCategoriaN5.Cat2 = g_oLinea.Cat2
        oCategoriaN5.Cat3 = g_oLinea.Cat3
        oCategoriaN5.Cat4 = g_oLinea.Cat4
        oCategoriaN5.Id = g_oLinea.Cat5
        oCategoriaN5.CargarCamposDePedido (ambito)
        
        Select Case ambito
        Case ambitodelcampodepedido.cabecera
            For Each oCampoPerCategoria In oCategoriaN5.CamposPersonalizados
                sdbgCamposPedidoCabecera.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        Case ambitodelcampodepedido.Linea
            For Each oCampoPerCategoria In oCategoriaN5.CamposPersonalizados
                sdbgCamposPedidoLinea.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        End Select
        
        
    ElseIf g_oLinea.Cat1 <> "" And g_oLinea.Cat2 <> "" And g_oLinea.Cat3 <> "" And g_oLinea.Cat4 <> "" Then
        Dim oCategoriaN4 As CCategoriaN4
        Set oCategoriaN4 = oFSGSRaiz.Generar_CCategoriaN4
        oCategoriaN4.Cat1 = g_oLinea.Cat1
        oCategoriaN4.Cat2 = g_oLinea.Cat2
        oCategoriaN4.Cat3 = g_oLinea.Cat3
        oCategoriaN4.Id = g_oLinea.Cat4
        oCategoriaN4.CargarCamposDePedido (ambito)
        
        Select Case ambito
        Case ambitodelcampodepedido.cabecera
            For Each oCampoPerCategoria In oCategoriaN4.CamposPersonalizados
                sdbgCamposPedidoCabecera.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        Case ambitodelcampodepedido.Linea
            For Each oCampoPerCategoria In oCategoriaN4.CamposPersonalizados
                sdbgCamposPedidoLinea.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        End Select
    ElseIf g_oLinea.Cat1 <> "" And g_oLinea.Cat2 <> "" And g_oLinea.Cat3 <> "" Then
        Dim oCategoriaN3 As CCategoriaN3
        Set oCategoriaN3 = oFSGSRaiz.Generar_CCategoriaN3
        oCategoriaN3.Cat1 = g_oLinea.Cat1
        oCategoriaN3.Cat2 = g_oLinea.Cat2
        oCategoriaN3.Id = g_oLinea.Cat3
        oCategoriaN3.CargarCamposDePedido (ambito)
        
        Select Case ambito
        Case ambitodelcampodepedido.cabecera
            For Each oCampoPerCategoria In oCategoriaN3.CamposPersonalizados
                sdbgCamposPedidoCabecera.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        Case ambitodelcampodepedido.Linea
            For Each oCampoPerCategoria In oCategoriaN3.CamposPersonalizados
                sdbgCamposPedidoLinea.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        End Select
    ElseIf g_oLinea.Cat1 <> "" And g_oLinea.Cat2 <> "" Then
        Dim oCategoriaN2 As CCategoriaN2
        Set oCategoriaN2 = oFSGSRaiz.Generar_CCategoriaN2
        oCategoriaN2.Cat1 = g_oLinea.Cat1
        oCategoriaN2.Id = g_oLinea.Cat2
        oCategoriaN2.CargarCamposDePedido (ambito)
        
        Select Case ambito
        Case ambitodelcampodepedido.cabecera
            For Each oCampoPerCategoria In oCategoriaN2.CamposPersonalizados
                sdbgCamposPedidoCabecera.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        Case ambitodelcampodepedido.Linea
            For Each oCampoPerCategoria In oCategoriaN2.CamposPersonalizados
                sdbgCamposPedidoLinea.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        End Select
    ElseIf g_oLinea.Cat1 <> "" Then
        Dim oCategoriaN1 As CCategoriaN1
        Set oCategoriaN1 = oFSGSRaiz.Generar_CCategoriaN1
        oCategoriaN1.Id = g_oLinea.Cat1
        oCategoriaN1.CargarCamposDePedido (ambito)
        
        Select Case ambito
        Case ambitodelcampodepedido.cabecera
            For Each oCampoPerCategoria In oCategoriaN1.CamposPersonalizados
                sdbgCamposPedidoCabecera.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        Case ambitodelcampodepedido.Linea
            For Each oCampoPerCategoria In oCategoriaN1.CamposPersonalizados
                sdbgCamposPedidoLinea.AddItem oCampoPerCategoria.Cod & Chr(m_lSeparador) & oCampoPerCategoria.Den & Chr(m_lSeparador) & oCampoPerCategoria.valor & Chr(m_lSeparador) & IIf(oCampoPerCategoria.Obligatorio = False, "0", "1") & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCampoPerCategoria.interno & Chr(m_lSeparador) & oCampoPerCategoria.Tipo & Chr(m_lSeparador) & oCampoPerCategoria.TipoIntroduccion & Chr(m_lSeparador) & oCampoPerCategoria.Maximo & Chr(m_lSeparador) & oCampoPerCategoria.Minimo & Chr(m_lSeparador) & oCampoPerCategoria.AtribID & Chr(m_lSeparador) & oCampoPerCategoria.Id & Chr(m_lSeparador) & "CAT" & Chr(m_lSeparador) & oCampoPerCategoria.Lista_Externa & Chr(m_lSeparador) & oCampoPerCategoria.MostrarEnRecep
            Next
        End Select
    End If
    
End Sub
''' <summary>Carga el combo de tipos de pedido en la pesta�a de campos de pedido de cabecera</summary>
Private Sub CargarComboTiposPedido()
    Dim oTiposPedido As CTiposPedido
    Dim oTipoPedido As CTipoPedido
    Set oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    
    
    oTiposPedido.CargarTodosLosTiposPedidos (gParametrosInstalacion.gIdioma)
    For Each oTipoPedido In oTiposPedido
        sdbcTiposPedido.AddItem oTipoPedido.Den & Chr(m_lSeparador) & oTipoPedido.Cod & Chr(m_lSeparador) & oTipoPedido.Id
    Next
End Sub
''' <summary>Carga los campos de pedido del tipo de pedido</summary>
''' <param name="idTipoPedido">Id del tipo de pedido</param>
Private Sub CargarCamposPedidoDeTipoPedido(ByVal idTipoPedido As Long)
    g_oLinea.CargarCamposPedidoTipoPedido (idTipoPedido)
    CargarGridCamposPedidoTipoPedido
End Sub

''' <summary>Carga Textos de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim sTitForm As String
    Dim sLitCamposPedRecep As String
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_CAMPOSPED, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.tabCamposPedidoCatalogo.TabCaption(0) = Ador(0).Value '1
        Ador.MoveNext
        Me.tabCamposPedidoCatalogo.TabCaption(1) = Ador(0).Value '2
        Ador.MoveNext
        lblCamposLineaPedido.caption = Ador(0).Value '3 Campos de la linea de pedido
        Ador.MoveNext
        Me.sdbgCamposPedidoLinea.Columns("COD").caption = Ador(0).Value '4 Codigo
        Me.sdbgCamposPedidoCabecera.Columns("COD").caption = Ador(0).Value '4 Codigo
        Me.sdbgCamposPedidos(0).Columns("COD").caption = Ador(0).Value '4 Codigo
        Me.sdbgCamposPedidos(1).Columns("COD").caption = Ador(0).Value '4 Codigo
        Me.sdbgCamposPedidoTipoPedido.Columns("COD").caption = Ador(0).Value '4 Codigo
        Ador.MoveNext
        Me.sdbgCamposPedidoLinea.Columns("DEN").caption = Ador(0).Value '5 Denominacion
        Me.sdbgCamposPedidoCabecera.Columns("DEN").caption = Ador(0).Value '5 Denominacion
        Me.sdbgCamposPedidos(0).Columns("DEN").caption = Ador(0).Value '5 Denominaciono
        Me.sdbgCamposPedidos(1).Columns("DEN").caption = Ador(0).Value '5 Denominacion
        Me.sdbgCamposPedidoTipoPedido.Columns("DEN").caption = Ador(0).Value '5 Denominacion
        Ador.MoveNext
        Me.sdbgCamposPedidoLinea.Columns("VALOR").caption = Ador(0).Value '6 Valor
        Me.sdbgCamposPedidoCabecera.Columns("VALOR").caption = Ador(0).Value '6 Valor
        Me.sdbgCamposPedidos(0).Columns("VALOR").caption = Ador(0).Value '6 Valor
        Me.sdbgCamposPedidos(1).Columns("VALOR").caption = Ador(0).Value '6 Valor
        Me.sdbgCamposPedidoTipoPedido.Columns("VALOR").caption = Ador(0).Value '6 Valor
        Ador.MoveNext
        Me.sdbgCamposPedidoLinea.Columns("OBLIG").caption = Ador(0).Value '7 Oblig
        Me.sdbgCamposPedidoCabecera.Columns("OBLIG").caption = Ador(0).Value '7 Oblig
        Me.sdbgCamposPedidoTipoPedido.Columns("OBLIG").caption = Ador(0).Value '7 Oblig
        Ador.MoveNext
        Me.sdbgCamposPedidoLinea.Columns("ORIGEN").caption = Ador(0).Value '8 Origen
        Me.sdbgCamposPedidoCabecera.Columns("ORIGEN").caption = Ador(0).Value '8 Origen
        Me.sdbgCamposPedidos(0).Columns("ORIGEN").caption = Ador(0).Value '8 Origen
        Me.sdbgCamposPedidos(1).Columns("ORIGEN").caption = Ador(0).Value '8 Origen
        Ador.MoveNext
        Me.sdbgCamposPedidoLinea.Columns("INTERNO").caption = Ador(0).Value '9 Interno
        Me.sdbgCamposPedidoCabecera.Columns("INTERNO").caption = Ador(0).Value '9 Interno
        Me.sdbgCamposPedidoTipoPedido.Columns("INTERNO").caption = Ador(0).Value '9 Interno
        Ador.MoveNext
        Me.cmdAnyadirCampo.caption = Ador(0).Value '10 A�adir campo
        Ador.MoveNext
        Me.cmdEliminarCampo.caption = Ador(0).Value '11 Eliminar campo
        Ador.MoveNext
        Me.lblCamposSolicitud.caption = Ador(0).Value '12
        Ador.MoveNext
        Me.lblCamposOferta.caption = Ador(0).Value  '13
        Ador.MoveNext
        Me.lblCamposDePedidoDeCategoriaCabecera.caption = Ador(0).Value  '14
        Ador.MoveNext
        Me.lblCamposTipoDePedidoCabecera.caption = Ador(0).Value '15
        Ador.MoveNext
        sLitCamposPedRecep = Ador(0).Value '16
        Ador.MoveNext
        sTitForm = " " & Ador(0).Value & ":" & g_oLinea.ProveCod & " /" '17
        Ador.MoveNext
        sTitForm = sTitForm & " " & Ador(0).Value & ":" & g_oLinea.ArtCod_Interno & "-" & g_oLinea.ArtDen '18
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value '19 Si
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value '20 No
        Ador.MoveNext
        m_sEliminarCampoCategoria = Ador(0).Value '21
        Ador.MoveNext
        m_sEliminaCampoPedido = Ador(0).Value '22
        Ador.MoveNext
        m_sNoHayCampoSeleccionado = Ador(0).Value '23
        Ador.MoveNext
        m_sMaestro = Ador(0).Value '24
        Ador.MoveNext
        m_sProceso = Ador(0).Value '25
        Ador.MoveNext
        m_sSolicitud = Ador(0).Value '26
        Ador.MoveNext
        m_sCategoria = Ador(0).Value '27
        Ador.MoveNext
        If g_bRecep Then
            sLitCamposPedRecep = Ador(0).Value '28
        End If
        Ador.MoveNext
        Me.sdbgCamposPedidoCabecera.Columns("VERENRECEP").caption = Ador(0).Value 'Mostrar en Recepci�n
        Me.sdbgCamposPedidoLinea.Columns("VERENRECEP").caption = Ador(0).Value 'Mostrar en Recepci�n
        Ador.Close
        Me.caption = sLitCamposPedRecep & sTitForm
    End If
End Sub



''' <summary>Carga el objeto de CAtributos(m_oAtributosLinea o m_oAtributosCabecera) con los campos de pedido del ambito que se indique(CATALOG_LIN_ATRIB) y carga su grid correspondiente</summary>
''' <param name="AmbitoCampoPedido">Ambito de los campos de pedido, o de linea o de cabecera</param>
Private Sub CargarCamposPedido(AmbitoCampoPedido As ambitodelcampodepedido)
    If g_oLinea Is Nothing Then Unload Me
    
    If AmbitoCampoPedido = Linea Then
        Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , IIf(g_bRecep, CamposRecepcion, CamposPedido), ambitodelcampodepedido.Linea)
        CargarGridCamposPedidoLinea
    Else
        Set m_oAtributosCabecera = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , IIf(g_bRecep, CamposRecepcion, CamposPedido), ambitodelcampodepedido.cabecera)
        CargarGridCamposPedidoCabecera
    End If
    
End Sub
''' <summary>Carga en la grid los campos de pedido del tipo de pedido seleccionado(tab de cabecera)</summary>
Private Sub CargarGridCamposPedidoTipoPedido()
    Dim oatrib As CCampo
    sdbgCamposPedidoTipoPedido.RemoveAll
    
    If Not g_oLinea.CamposPedidoTipoPedido Is Nothing Then
        For Each oatrib In g_oLinea.CamposPedidoTipoPedido
            sdbgCamposPedidoTipoPedido.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.Obligatorio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.interno & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.AtribID & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Origen
        Next
    End If
    
End Sub
''' <summary>Carga en la grid los campos de pedido de la solicitud si la linea proviene de una adjudicaci�n</summary>
Private Sub CargarGridCamposPedidoSolicitud()
    Dim oatrib As CCampo
    
    If Not g_oLinea.CamposPedidoSolicitud Is Nothing Then
        For Each oatrib In g_oLinea.CamposPedidoSolicitud
            sdbgCamposPedidos(1).AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.AtribID & Chr(m_lSeparador) & AtributosLineaCatalogo.ProceEsp & Chr(m_lSeparador) & oatrib.Id
        Next
    End If
    
End Sub
''' <summary>Carga en la grid los campos de pedido de la oferta adjudicada del proceso, si la linea viene de una adjudicacion</summary>
Private Sub CargarGridCamposPedidoOfertaAdjudicada()
    Dim oatrib As CCampo
    
    If Not g_oLinea.CamposPedidoOferta Is Nothing Then
        For Each oatrib In g_oLinea.CamposPedidoOferta
            sdbgCamposPedidos(0).AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.AtribID & Chr(m_lSeparador) & oatrib.Id
        Next
    End If
    
End Sub

''' <summary>Carga en la grid los campos de pedido de ambito linea(CATALOG_LIN_ATRIB)</summary>
Private Sub CargarGridCamposPedidoLinea()
    Dim oatrib As CAtributo
    If Not m_oAtributosLinea Is Nothing Then
        For Each oatrib In m_oAtributosLinea
            sdbgCamposPedidoLinea.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.Obligatorio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.interno & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Origen & Chr(m_lSeparador) & oatrib.ListaExterna & Chr(m_lSeparador) & oatrib.MostrarEnRecep
        Next
    End If
End Sub
''' <summary>Carga en la grid los campos de pedido de ambito Cabecera(CATALOG_LIN_ATRIB)</summary>
Private Sub CargarGridCamposPedidoCabecera()
    Dim oatrib As CAtributo
    If Not m_oAtributosCabecera Is Nothing Then
        For Each oatrib In m_oAtributosLinea
            sdbgCamposPedidoCabecera.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.Obligatorio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.interno & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Origen & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & oatrib.MostrarEnRecep
        Next
    End If
End Sub



'''<summary>Configura la grid,bloqueando o no la columna de valor depediendo de un permiso del usuario</summary>
Private Sub ConfigurarSeguridadGridCamposPedidoLinea()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGModificarValorCampoPedido)) Is Nothing) Then
        sdbgCamposPedidoLinea.Columns("VALOR").Locked = True
    Else
        sdbgCamposPedidoLinea.Columns("VALOR").Locked = False
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.sdbgCamposPedidoLinea.Rows > 0 Then
        frmCatalogo.sdbgAdjudicaciones.Columns(IIf(g_bRecep, "CAMRECEP", "CAMPED")).Value = "1"
    End If
    
    If sdbgCamposPedidoLinea.DataChanged Then
        sdbgCamposPedidoLinea.Update
    End If
    
    m_bCamposPedidoCabeceraCargados = False
End Sub

Private Sub sdbcTiposPedido_InitColumnProps()
    sdbcTiposPedido.DataFieldList = "Column 2"
    sdbcTiposPedido.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcTiposPedido_CloseUp()
    If sdbcTiposPedido.Value <> "" Then
        CargarCamposPedidoDeTipoPedido (sdbcTiposPedido.Value)
    End If
End Sub

'Inicializa el combo de la grid
Private Sub sdbddValorLinea_InitColumnProps()
    sdbddValorLinea.DataFieldList = "Column 0"
    sdbddValorLinea.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValorLinea_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    On Error Resume Next
    sdbddValorLinea.MoveFirst
    If Text <> "" Then
        For i = 0 To sdbddValorLinea.Rows - 1
            bm = sdbddValorLinea.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValorLinea.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbgCamposPedidoLinea.Columns("VALOR").Value = Mid(sdbddValorLinea.Columns(1).CellText(bm), 1, Len(Text))
                sdbddValorLinea.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

'Evento del combo de la grid de campos de pedido de linea cuando se despliega
Private Sub sdbddValorLinea_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgCamposPedidoLinea.Rows = 0 Then Exit Sub
   
    If sdbgCamposPedidoLinea.Columns("VALOR").Locked Then
        sdbddValorLinea.DroppedDown = False
        Exit Sub
    End If
    sdbddValorLinea.RemoveAll
    sdbddValorLinea.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    oAtributo.Id = sdbgCamposPedidoLinea.Columns("ATRIB_ID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgCamposPedidoLinea.Columns("INTRO").Value = "1" Then
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValorLinea.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        Else
            If sdbgCamposPedidoLinea.Columns("TIPO_DATO").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValorLinea.AddItem "1" & Chr(m_lSeparador) & m_sIdiTrue
                sdbddValorLinea.AddItem "0" & Chr(m_lSeparador) & m_sIdiFalse
            End If
        End If
    End If
    
    sdbddValorLinea.Width = sdbgCamposPedidoLinea.Columns("VALOR").Width
    sdbddValorLinea.Columns("DESC").Width = sdbddValorLinea.Width
End Sub
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE CAMPOS DE PEDIDO DE L�NEA
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Private Sub sdbgCamposPedidoLinea_AfterColUpdate(ByVal ColIndex As Integer)
    With sdbgCamposPedidoLinea
        'Si el campo de pedido es heredado de los campos de pedido de la categoria, no se podra modificar
        If .Columns("ORIGEN_VALOR").Value = "CAT" Then
            Exit Sub
        End If

        '1. ACTUALIZAR BD
        Dim tsError As TipoErrorSummit
        tsError = g_oLinea.ActualizarAtributo(.Columns("ID").Value, .Columns("VALOR").Value, .Columns("TIPO_DATO").Value, .Columns("INTERNO").Value, .Columns("OBLIG").Value, .Columns("VERENRECEP").Value)

        '2.ACTUALIZAR VARIABLE ATRIBUTOS L�NEA
        Dim i As Integer
        For i = 1 To m_oAtributosLinea.Count
            If m_oAtributosLinea.Item(i).Id = CLng(.Columns("ID").Value) Then
                m_oAtributosLinea.Item(i).valor = .Columns("VALOR").Value
                m_oAtributosLinea.Item(i).Obligatorio = (.Columns("OBLIG").Value = "-1")
                m_oAtributosLinea.Item(i).interno = (.Columns("INTERNO").Value = "-1")

                Exit For
            End If
        Next

        'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
        If tsError.NumError <> TESnoerror Then
            TratarError tsError
            .DataChanged = False
        End If
    End With
End Sub

Private Sub sdbgCamposPedidoLinea_Change()
    With sdbgCamposPedidoLinea
        If .col <> -1 Then
            Select Case .Columns(.col).Name
                Case "OBLIG"
                    'Si el atributo es un campo de pedido de una categoria del catalogo no se puede modificar
                    If .Columns("ORIGEN_VALOR").Value = "CAT" Then
                        If .Columns("OBLIG").Value = -1 Then
                            .Columns("OBLIG").Value = 0
                        Else
                            .Columns("OBLIG").Value = -1
                        End If
                        .Columns("OBLIG").Locked = True
                    End If
                Case "INTERNO"
                    'El campo interno no se puede modificar
                    If .Columns("ORIGEN_VALOR").Value = "CAT" Then
                        If .Columns("INTERNO").Value = -1 Then
                            .Columns("INTERNO").Value = 0
                        Else
                            .Columns("INTERNO").Value = -1
                        End If
                        .Columns("INTERNO").Locked = True
                    End If
            End Select
        End If
    End With
End Sub

'Evento de la grid que salta cuando cambiamos de columna o de fila
Private Sub sdbgCamposPedidoLinea_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim oElem As CValorPond
    Dim oLista As CValoresPond
    Dim oatrib As CAtributo
    Dim m_bModificarValorCampoPedido As Boolean
    
    m_bModificarValorCampoPedido = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGModificarValorCampoPedido)) Is Nothing)
    
    With sdbgCamposPedidoLinea
        If .col <> -1 Then
            If .Columns("TIPO_DATO").Value = TipoArchivo Then
                .Columns("VALOR").Locked = True
                .Columns("VALOR").DropDownHwnd = 0
            Else
                'Si el atributo es un campo de pedido de una categoria del catalogo no se puede modificar
                If .Columns("ORIGEN_VALOR").Value = "CAT" Then
                    .Columns("VALOR").Locked = True
                Else
                    If m_bModificarValorCampoPedido And (.Columns("ORIGEN_VALOR").Value = AtributosLineaCatalogo.ProceEsp Or .Columns("ORIGEN_VALOR").Value = AtributosLineaCatalogo.ProveArtYProceOfer) Then
                        'Puede modificar el valor del campo de pedido si el atributo es de oferta o de solicitud
                        .Columns("VALOR").Locked = False
                    ElseIf m_bModificarValorCampoPedido = False And (.Columns("ORIGEN_VALOR").Value = AtributosLineaCatalogo.ProceEsp Or .Columns("ORIGEN_VALOR").Value = AtributosLineaCatalogo.ProveArtYProceOfer) Then
                        'No puede modificar el valor del campo de pedido si el atributo es de oferta o de solicitud
                        .Columns("VALOR").Locked = True
                    Else
                        'Es un atributo del maestro y lo puede modificar
                        .Columns("VALOR").Locked = False
                    End If
                End If
            End If
            
            If .Columns("LISTA_EXTERNA").Value Then
                .Columns("VALOR").Locked = True
            Else
                If .Columns("INTRO").Value = 0 Then 'Introduccion Libre
                    If .Columns("TIPO_DATO").Value = TipoBoolean Then
                        sdbddValorLinea.RemoveAll
                        sdbddValorLinea.AddItem ""
                        .Columns("VALOR").DropDownHwnd = sdbddValorLinea.hWnd
                        sdbddValorLinea.Enabled = True
                    Else
                        .Columns("VALOR").DropDownHwnd = 0
                        sdbddValorLinea.Enabled = False
                        If .Columns("TIPO_DATO").Value = 1 Then
                           .Columns("VALOR").Style = ssStyleEditButton
                        End If
                    End If
                Else 'Lista
                    sdbddValorLinea.RemoveAll
                    sdbddValorLinea.AddItem ""
                    .Columns("VALOR").DropDownHwnd = sdbddValorLinea.hWnd
                    sdbddValorLinea.Enabled = True
                End If
            End If
        End If
    End With
End Sub

Private Sub sdbgCamposPedidoLinea_Scroll(Cancel As Integer)
    sdbgCamposPedidoLinea.Columns("VALOR").DropDownHwnd = 0
End Sub

''' <summary>
''' Evento que salta al hacer cambios en la grid
''' </summary>
''' <param name="Cancel">Cancelacion del cambio</param>
''' <returns></returns>
''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>
Private Sub sdbgCamposPedidoLinea_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim teserror As TipoErrorSummit
    
    Cancel = False
    
    With sdbgCamposPedidoLinea
        If .Columns(ColIndex).Name = "VALOR" Then
            If .Columns("VALOR").Text <> "" Then
                Select Case UCase(.Columns("TIPO_DATO").Text)
                    Case 2 'Numerico
                        If (Not IsNumeric(.Columns("VALOR").Text)) Then
                            oMensajes.AtributoValorNoValido ("TIPO2")
                            Cancel = True
                        Else
                            If .Columns("MIN").Text <> "" And .Columns("MAX").Text <> "" Then
                                If StrToDbl0(.Columns("MIN").Text) > StrToDbl0(.Columns("VALOR").Text) Or StrToDbl0(.Columns("MAX").Text) < StrToDbl0(.Columns("VALOR").Text) Then
                                    oMensajes.ValorEntreMaximoYMinimo .Columns("MIN").Text, .Columns("MAX").Text
                                    Cancel = True
                                End If
                            End If
                        End If
                    Case 3 'Fecha
                        If (Not IsDate(.Columns("VALOR").Text) And .Columns("VALOR").Text <> "") Then
                            oMensajes.AtributoValorNoValido ("TIPO3")
                            Cancel = True
                        Else
                            If .Columns("MIN").Text <> "" And .Columns("MAX").Text <> "" Then
                                If CDate(.Columns("MIN").Text) > CDate(.Columns("VALOR").Text) Or CDate(.Columns("MAX").Text) < CDate(.Columns("VALOR").Text) Then
                                    oMensajes.ValorEntreMaximoYMinimo .Columns("MIN").Text, .Columns("MAX").Text
                                    Cancel = True
                                End If
                            End If
                        End If
                End Select
            End If
        End If
    End With
End Sub

Private Sub sdbgCamposPedidoLinea_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If IsEmpty(sdbgCamposPedidoLinea.RowBookmark(sdbgCamposPedidoLinea.Row)) Then
        sdbgCamposPedidoLinea.Bookmark = sdbgCamposPedidoLinea.RowBookmark(sdbgCamposPedidoLinea.Row - 1)
    Else
        sdbgCamposPedidoLinea.Bookmark = sdbgCamposPedidoLinea.RowBookmark(sdbgCamposPedidoLinea.Row)
    End If
End Sub

Private Sub sdbgCamposPedidoLinea_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgCamposPedidoLinea_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        If sdbgCamposPedidoLinea.DataChanged = False Then
            sdbgCamposPedidoLinea.CancelUpdate
            sdbgCamposPedidoLinea.DataChanged = False
        End If
    End If
End Sub

Private Sub sdbgCamposPedidoLinea_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer

    'Indicamos el origen del campo de pedido, si es de Categoria de catalogo, de una solicitud....
    If sdbgCamposPedidoLinea.Columns("ORIGEN_VALOR").CellValue(Bookmark) = "CAT" Then
        For i = 0 To sdbgCamposPedidoLinea.Columns.Count - 1
            sdbgCamposPedidoLinea.Columns(i).CellStyleSet "Gris"
        Next
        sdbgCamposPedidoLinea.Columns("ORIGEN").Text = m_sCategoria
    ElseIf sdbgCamposPedidoLinea.Columns("ORIGEN_VALOR").CellValue(Bookmark) = AtributosLineaCatalogo.MaestroAtributos Then
        sdbgCamposPedidoLinea.Columns("ORIGEN").Text = m_sMaestro
    ElseIf sdbgCamposPedidoLinea.Columns("ORIGEN_VALOR").CellValue(Bookmark) = AtributosLineaCatalogo.ProveArtYProceOfer Then
        sdbgCamposPedidoLinea.Columns("ORIGEN").Text = m_sProceso
    Else
        sdbgCamposPedidoLinea.Columns("ORIGEN").Text = m_sSolicitud
    End If
    
    If sdbgCamposPedidoLinea.Columns("TIPO_DATO").CellValue(Bookmark) = TiposDeAtributos.TipoBoolean Then
        If sdbgCamposPedidoLinea.Columns("VALOR").CellValue(Bookmark) = 1 Then
            sdbgCamposPedidoLinea.Columns("VALOR").Text = m_sIdiTrue
        ElseIf sdbgCamposPedidoLinea.Columns("VALOR").CellValue(Bookmark) = 0 Then
            sdbgCamposPedidoLinea.Columns("VALOR").Text = m_sIdiFalse
        End If
    End If
    If sdbgCamposPedidoLinea.Columns("TIPO_DATO").CellValue(Bookmark) = TiposDeAtributos.TipoArchivo Then
        sdbgCamposPedidoLinea.Columns("VALOR").CellStyleSet "Gris"
    End If
End Sub
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE CAMPOS DE PEDIDO DE CABECERA
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Private Sub sdbgCamposPedidoCabecera_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    'Indicamos el origen del campo de pedido, si es de Categoria de catalogo
    If sdbgCamposPedidoCabecera.Columns("ORIGEN_VALOR").CellValue(Bookmark) = "CAT" Then
        For i = 0 To sdbgCamposPedidoCabecera.Columns.Count - 1
            sdbgCamposPedidoCabecera.Columns(i).CellStyleSet "Gris"
        Next
        sdbgCamposPedidoCabecera.Columns("ORIGEN").Text = m_sCategoria
    End If
End Sub

Private Sub tabCamposPedidoCatalogo_Click(PreviousTab As Integer)
    Select Case tabCamposPedidoCatalogo.Tab
    Case 1
        'Cuando se clickea en el tab, en la pesta�a de cabecera se mira si los campos de pedido ya han sido cargados previamente
        If Not m_bCamposPedidoCabeceraCargados Then
            CargarCamposPedidoCategoria (cabecera)
            CargarComboTiposPedido
        End If
        m_bCamposPedidoCabeceraCargados = True
    End Select
End Sub

