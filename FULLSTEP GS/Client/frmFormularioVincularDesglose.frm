VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFormularioVincularDesglose 
   BackColor       =   &H00808000&
   Caption         =   "DVincular a otras solicitudes"
   ClientHeight    =   3390
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9045
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormularioVincularDesglose.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3390
   ScaleWidth      =   9045
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAnya 
      Height          =   300
      Left            =   8160
      Picture         =   "frmFormularioVincularDesglose.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   120
      Width           =   350
   End
   Begin VB.CommandButton cmdElim 
      Height          =   300
      Left            =   8640
      Picture         =   "frmFormularioVincularDesglose.frx":0D34
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   120
      Width           =   350
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "DAceptar"
      Height          =   315
      Left            =   2880
      TabIndex        =   3
      Top             =   3000
      Width           =   1500
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "DCancelar"
      Height          =   315
      Left            =   4680
      TabIndex        =   2
      Top             =   3000
      Width           =   1500
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddDesglose 
      Height          =   975
      Left            =   6480
      TabIndex        =   0
      Top             =   2280
      Width           =   2295
      DataFieldList   =   "Column 1"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3757
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1720
      _StockProps     =   77
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTipo 
      Height          =   975
      Left            =   240
      TabIndex        =   1
      Top             =   2400
      Width           =   2295
      DataFieldList   =   "Column 1"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3757
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1720
      _StockProps     =   77
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgVincular 
      Height          =   2415
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   8865
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   6
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Caption=   "TIPO"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   3
      Columns(1).Width=   3200
      Columns(1).Caption=   "DESGLOSE"
      Columns(1).Name =   "DESGLOSE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   3
      Columns(2).Width=   3200
      Columns(2).Caption=   "CAMPO"
      Columns(2).Name =   "CAMPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   1
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "IDTIPO"
      Columns(4).Name =   "IDTIPO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "IDDESGLOSE"
      Columns(5).Name =   "IDDESGLOSE"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   15637
      _ExtentY        =   4260
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblSelecc 
      BackColor       =   &H00808000&
      Caption         =   "DSeleccione los tipo de solicitudes que desea vincular, y el campo Desglose de cada una de ellas"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   7575
   End
End
Attribute VB_Name = "frmFormularioVincularDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oCampoDesglose As CFormItem
Private m_oVinculados As CVinculaciones
Public g_bModificar As Boolean
''' <summary>
''' Grabar los cambios realizados en la vinculacion
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim i As Integer

    Dim aIdentificadoresSol() As String
    Dim aIdentificadoresOri() As String
    Dim oListaCtrlVinculados As CVinculaciones
    
    Set oListaCtrlVinculados = Nothing
    Set oListaCtrlVinculados = oFSGSRaiz.Generar_CVinculados
       
    Set m_oVinculados = Nothing
    Set m_oVinculados = oFSGSRaiz.Generar_CVinculados

    'Tiene que introducir tipo y desglose en todas las lineas
    LockWindowUpdate Me.hWnd
    Screen.MousePointer = vbHourglass
    
    ReDim aIdentificadoresSol(sdbgVincular.Rows)
    ReDim aIdentificadoresOri(sdbgVincular.Rows)
    
    Me.sdbgVincular.MoveFirst
    For i = 0 To sdbgVincular.Rows - 1

        If sdbgVincular.Columns("TIPO").Value = "" Then
            oMensajes.NoValido sdbgVincular.Columns("TIPO").caption
            LockWindowUpdate 0&
            Screen.MousePointer = vbNormal
            Exit Sub
        End If

        If sdbgVincular.Columns("DESGLOSE").Value = "" Then
            sdbgVincular.Columns("IDDESGLOSE").Value = ""
        End If

        If oListaCtrlVinculados.Item(CStr(g_oCampoDesglose.Id) & "#" & CStr(sdbgVincular.Columns("IDTIPO").Value) & "#") Is Nothing Then
        Else
            oMensajes.ImposibleGrabarVinculaciones
            LockWindowUpdate 0&
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        'Clase donde van lo q se va a grabar
        m_oVinculados.Add sdbgVincular.Columns("ID").Value, sdbgVincular.Columns("IDTIPO").Value, g_oCampoDesglose.Id, sdbgVincular.Columns("IDDESGLOSE").Value
        
        'Clase usada para ctrol de repetidos
        '   no puede haber 2 vinculaciones "origen" a la misma solicitud. Pq entonces �en Pm q se sacar�a como LINEAS? uno u otro (como saber cual?)
        '   Nota: debe ser uno u otro pq las columnas del desglose origen pueden diferir.
        oListaCtrlVinculados.Add "", sdbgVincular.Columns("IDTIPO").Value, g_oCampoDesglose.Id, ""
        
        aIdentificadoresSol(i) = sdbgVincular.Columns("IDTIPO").Value
        aIdentificadoresOri(i) = sdbgVincular.Columns("IDDESGLOSE").Value
        
        Me.sdbgVincular.MoveNext
    Next i
    
    LockWindowUpdate 0&
    
    udtTeserror = m_oVinculados.GrabarVinculaciones
    If udtTeserror.NumError = TESnoerror Then
        For i = 1 To UBound(aIdentificadoresSol)
            RegistrarAccion ACCVincularDesglose, "Desgl Vinc:" & CStr(g_oCampoDesglose.Id) _
            & " Solic:" & aIdentificadoresSol(i) & " Desgl Origen:" & aIdentificadoresOri(i)
        Next
        Unload Me
    Else
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
    End If

    Screen.MousePointer = vbNormal
End Sub
''' <summary>
''' A�adir una nueva vinculacion
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAnya_Click()
    If sdbgVincular.DataChanged = True Then
        sdbgVincular.Update
    End If
    
    If sdbgVincular.Rows > 0 Then
        If sdbgVincular.Columns("TIPO").Value = "" Then
            Exit Sub
        End If
    End If
        
    'A�ade una fila a la grid:
    sdbgVincular.AddNew
    
    Me.cmdElim.Enabled = True
    
    sdbgVincular.Scroll 0, sdbgVincular.Rows - sdbgVincular.Row

    If sdbgVincular.VisibleRows > 0 Then
        If sdbgVincular.VisibleRows >= sdbgVincular.Rows Then
            sdbgVincular.Row = sdbgVincular.Rows
        Else
            sdbgVincular.Row = sdbgVincular.Rows - (sdbgVincular.Rows - sdbgVincular.VisibleRows) - 1
        End If
    End If
            
    If Me.Visible Then sdbgVincular.SetFocus
End Sub
''' <summary>
''' Cerrar pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdCancelar_Click()
    Unload Me
End Sub
''' <summary>
''' Eliminar las vinculaciones seleccionadas
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdElim_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim i As Integer
    Dim aIdentificadores As Variant
    Dim aBookmarks As Variant
        
    On Error GoTo Cancelar:
    
    If sdbgVincular.DataChanged = True Then
        sdbgVincular.Update
    End If
    
    If sdbgVincular.Rows = 0 Then
        Me.cmdElim.Enabled = False
        Exit Sub
    End If
    If sdbgVincular.SelBookmarks.Count = 0 Then sdbgVincular.SelBookmarks.Add sdbgVincular.Bookmark

    If sdbgVincular.Columns("ID").Value = "" Then
        sdbgVincular.CancelUpdate
        sdbgVincular.DataChanged = False
                
        sdbgVincular.RemoveItem (sdbgVincular.AddItemRowIndex(sdbgVincular.SelBookmarks(0)))
        sdbgVincular.MovePrevious
    Else
   
        Screen.MousePointer = vbHourglass
        
        'Elimina de base de datos:
        ReDim aIdentificadores(sdbgVincular.SelBookmarks.Count)
        ReDim aBookmarks(sdbgVincular.SelBookmarks.Count)
    
        i = 0
        While i < sdbgVincular.SelBookmarks.Count
            sdbgVincular.Bookmark = sdbgVincular.SelBookmarks(i)
            aIdentificadores(i + 1) = sdbgVincular.Columns("ID").Value
            aBookmarks(i + 1) = sdbgVincular.SelBookmarks(i)
            i = i + 1
        Wend
        
        udtTeserror = m_oVinculados.EliminarVinculaciones(aIdentificadores)
    
        If udtTeserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError udtTeserror
        Else
            'Elimina los campos de la grid:
            For i = 1 To UBound(aBookmarks)
                sdbgVincular.RemoveItem (sdbgVincular.AddItemRowIndex(aBookmarks(i)))
            Next i
            
            'Se posiciona en la fila correspondiente:
            If sdbgVincular.Rows > 0 Then
                If IsEmpty(sdbgVincular.RowBookmark(sdbgVincular.Row)) Then
                    sdbgVincular.Bookmark = sdbgVincular.RowBookmark(sdbgVincular.Row - 1)
                Else
                    sdbgVincular.Bookmark = sdbgVincular.RowBookmark(sdbgVincular.Row)
                End If
            End If
        End If
    End If
    
    Me.cmdElim.Enabled = (sdbgVincular.Rows > 0)
    
    sdbgVincular.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgVincular.SetFocus

    Screen.MousePointer = vbNormal

    Exit Sub
    
Cancelar:
    'Metes una linea no haces nada mas, das a eliminar -> el cancelupdate elimina por si solo la linea, el resto del codigo en
    'este caso no hace falta y provoca, de hecho, este error
    Me.cmdElim.Enabled = (sdbgVincular.Rows > 0)
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Carga de la pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
  
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Set m_oVinculados = Nothing
    Set m_oVinculados = oFSGSRaiz.Generar_CVinculados
        
    CargaGridVinculaci�n
    If g_bModificar Then
        Carga_sdbddTipo
        sdbgVincular.Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
        
        sdbddDesglose.AddItem ""
        sdbgVincular.Columns("DESGLOSE").DropDownHwnd = sdbddDesglose.hWnd
    Else
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        cmdAnya.Visible = False
        cmdElim.Visible = False
        sdbgVincular.Columns("TIPO").Locked = True
        sdbgVincular.Columns("TIPO").Style = ssStyleEdit
        sdbgVincular.Columns("DESGLOSE").Locked = True
        sdbgVincular.Columns("DESGLOSE").Style = ssStyleEdit
        sdbgVincular.Columns("CAMPO").Locked = True
    End If
    Me.cmdElim.Enabled = (Me.sdbgVincular.Rows > 0)

End Sub
''' <summary>
''' Carga de los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_VINCULARDESGLOSE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblSelecc.caption = Ador(0).Value
        Ador.MoveNext
        sdbgVincular.Columns("TIPO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgVincular.Columns("DESGLOSE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgVincular.Columns("CAMPO").caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

''' <summary>
''' Establecer los tama�os de los campos de pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Resize()
    Me.sdbgVincular.Width = Me.Width - 285
    Me.sdbgVincular.Height = Me.Height - 1500
    
    Me.sdbgVincular.Columns("TIPO").Width = ((Me.sdbgVincular.Width / 5) * 2) - 400
    Me.sdbgVincular.Columns("DESGLOSE").Width = Me.sdbgVincular.Columns("TIPO").Width
    Me.sdbgVincular.Columns("CAMPO").Width = Me.sdbgVincular.Width - (2 * Me.sdbgVincular.Columns("TIPO").Width) - 600
    
    Me.sdbddTipo.Columns("DEN").Width = Me.sdbgVincular.Columns("TIPO").Width - 380
    Me.sdbddDesglose.Columns("DEN").Width = Me.sdbgVincular.Columns("DESGLOSE").Width - 180
    
    Me.cmdAceptar.Top = Me.Height - 915
    Me.cmdCancelar.Top = Me.cmdAceptar.Top
    
    Me.cmdAceptar.Left = (Me.Width / 2) - Me.cmdAceptar.Width - 195
    Me.cmdCancelar.Left = Me.cmdAceptar.Left + Me.cmdAceptar.Width + 300
    
    Me.cmdElim.Left = Me.Width - 510
    Me.cmdAnya.Left = Me.cmdElim.Left - Me.cmdAnya.Width - 130
    
    Me.lblSelecc.Width = Me.sdbgVincular.Width - 1290
End Sub
''' <summary>
''' Cargar las vinculaciones de un desglose
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo:0,2</remarks>
Private Sub CargaGridVinculaci�n()
    Dim sCadena As String
    Dim i As Integer
    
    sdbgVincular.RemoveAll

    m_oVinculados.CargarVinculados g_oCampoDesglose.Id

    For i = 1 To m_oVinculados.Count
        sCadena = m_oVinculados.Item(i).DescrSolicitud & Chr(m_lSeparador) & m_oVinculados.Item(i).DescrDesgloseOrigen & Chr(m_lSeparador) & m_oVinculados.Item(i).Campos
        sCadena = sCadena & Chr(m_lSeparador) & m_oVinculados.Item(i).Id & Chr(m_lSeparador) & m_oVinculados.Item(i).Solicitud
        sCadena = sCadena & Chr(m_lSeparador) & m_oVinculados.Item(i).DesgloseOrigen

        sdbgVincular.AddItem sCadena
    Next
End Sub
''' <summary>
''' Seleccionar un desglose para la columna desglose origen
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddDesglose_CloseUp()
    If sdbgVincular.Columns("IDDESGLOSE").Value = sdbddDesglose.Columns("ID").Value Then Exit Sub
    
    sdbgVincular.Columns("IDDESGLOSE").Value = sdbddDesglose.Columns("ID").Value
    sdbgVincular.Columns("DESGLOSE").Value = sdbddDesglose.Columns("DEN").Value
    sdbgVincular.Columns("CAMPO").Value = ""
End Sub
''' <summary>
''' Cargar el combo para la columna desglose origen
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddDesglose_DropDown()
    Dim oDesgloses As CFormItems
    Dim Ador As ADODB.Recordset
    Dim sCadena As String
    
    sdbddDesglose.RemoveAll
    
    Set oDesgloses = oFSGSRaiz.Generar_CFormCampos
    Set Ador = oDesgloses.DevolverDesgloses(Me.sdbgVincular.Columns("IDTIPO").Value)
    
    While Not Ador.EOF
        sCadena = Ador.Fields("ID").Value
        sCadena = sCadena & Chr(m_lSeparador) & "(" & Ador.Fields("GRUPODEN").Value
        sCadena = sCadena & ") " & Ador.Fields("DESGLDEN").Value
        
        sdbddDesglose.AddItem sCadena
        Ador.MoveNext
    Wend
    
    Ador.Close
    Set Ador = Nothing
    Set oDesgloses = Nothing
    
    If sdbddDesglose.Rows = 0 Then
        sdbddDesglose.AddItem ""
    End If
    
    sdbgVincular.ActiveCell.SelStart = 0
    sdbgVincular.ActiveCell.SelLength = Len(sdbgVincular.ActiveCell.Value)
End Sub
''' <summary>
''' Inicializar el combo para la columna desglose origen
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddDesglose_InitColumnProps()
    sdbddDesglose.DataFieldList = "Column 1"
    sdbddDesglose.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>
''' Posicionar el combo para la columna desglose origen
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddDesglose_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddDesglose.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddDesglose.Rows - 1
            bm = sdbddDesglose.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddDesglose.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbddDesglose.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
''' <summary>
''' Seleccionar una solicitud para la columna de solicitudes
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddTipo_CloseUp()
    If sdbgVincular.Columns("IDTIPO").Value = sdbddTipo.Columns("ID").Value Then Exit Sub
    
    sdbgVincular.Columns("IDTIPO").Value = sdbddTipo.Columns("ID").Value
    sdbgVincular.Columns("TIPO").Value = sdbddTipo.Columns("DEN").Value
    sdbgVincular.Columns("DESGLOSE").Value = ""
    sdbgVincular.Columns("CAMPO").Value = ""
End Sub
''' <summary>
''' Cargar del combo para la columna solicitudes
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub Carga_sdbddTipo()
    Dim oSolicitudes As CSolicitudes
    Dim oSolicitud As CSolicitud
    
    sdbddTipo.RemoveAll
    
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    oSolicitudes.CargarTodasSolicitudesSalvoQA g_oCampoDesglose.Grupo.Formulario.Id
        
    For Each oSolicitud In oSolicitudes
        sdbddTipo.AddItem oSolicitud.Id & Chr(m_lSeparador) & oSolicitud.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next
    
    Set oSolicitud = Nothing
    Set oSolicitudes = Nothing
    
    If sdbddTipo.Rows = 0 Then
        sdbddTipo.AddItem ""
    End If
    
    sdbgVincular.ActiveCell.SelStart = 0
    sdbgVincular.ActiveCell.SelLength = Len(sdbgVincular.ActiveCell.Value)
End Sub
''' <summary>
''' Inicializar el combo para la columna solicitudes
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddTipo_InitColumnProps()
    sdbddTipo.DataFieldList = "Column 1"
    sdbddTipo.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>
''' Posicionar el combo para la columna solicitudes
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
Private Sub sdbddTipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTipo.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddTipo.Rows - 1
            bm = sdbddTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTipo.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbddTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
''' <summary>
''' Abrir la pantalla para estbalcer los campos vinculados
''' </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbgVincular_BtnClick()
    If sdbgVincular.Columns("TIPO").Value = "" Or sdbgVincular.Columns("DESGLOSE").Value = "" Then
        Exit Sub
    End If

    frmFormularioVincularCampos.g_lCampoDesgloseVinculado = g_oCampoDesglose.Id
    frmFormularioVincularCampos.g_vDesgloseVinculadoId = Me.sdbgVincular.Columns("ID").Value
    frmFormularioVincularCampos.g_lCampoDesgloseOrigen = Me.sdbgVincular.Columns("IDDESGLOSE").Value
    frmFormularioVincularCampos.g_lSolicitudOrigen = Me.sdbgVincular.Columns("IDTIPO").Value
    frmFormularioVincularCampos.g_bModificar = g_bModificar
    frmFormularioVincularCampos.Show vbModal
End Sub



