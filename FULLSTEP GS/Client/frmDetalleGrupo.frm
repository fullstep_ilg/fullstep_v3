VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmDetalleGrupo 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DDetalle de grupo"
   ClientHeight    =   4425
   ClientLeft      =   5700
   ClientTop       =   2265
   ClientWidth     =   7485
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetalleGrupo.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4425
   ScaleWidth      =   7485
   Begin VB.CommandButton cmdEsp 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   360
      Picture         =   "frmDetalleGrupo.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Pulse para ver las especificaciones del  proceso"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdAtributos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   720
      Picture         =   "frmDetalleGrupo.frx":1013
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Pulse para ver los atributos del proceso"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdDatGen 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   0
      Picture         =   "frmDetalleGrupo.frx":1184
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Pulse para ver las personas asignadas al proceso"
      Top             =   0
      Width           =   375
   End
   Begin VB.PictureBox picProce 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3990
      Left            =   0
      ScaleHeight     =   3990
      ScaleWidth      =   7515
      TabIndex        =   3
      Top             =   360
      Width           =   7515
      Begin VB.TextBox txtDescr 
         Height          =   2055
         Left            =   1680
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   26
         Top             =   120
         Width           =   5430
      End
      Begin VB.TextBox txtFin 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   2385
         Width           =   1200
      End
      Begin VB.TextBox txtIni 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   2385
         Width           =   1200
      End
      Begin VB.Label lblProveedor 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   14
         Top             =   3600
         Width           =   5430
      End
      Begin VB.Label lblFormaPago 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   13
         Top             =   3180
         Width           =   5430
      End
      Begin VB.Label lblDestino 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   12
         Top             =   2760
         Width           =   5430
      End
      Begin VB.Label lblDescripcion 
         BackColor       =   &H00808000&
         Caption         =   "DDescripci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   90
         TabIndex        =   11
         Top             =   180
         Width           =   1485
      End
      Begin VB.Label lblDest 
         BackStyle       =   0  'Transparent
         Caption         =   "DDestino"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   90
         TabIndex        =   10
         Top             =   2790
         Width           =   1395
      End
      Begin VB.Label lblPag 
         BackStyle       =   0  'Transparent
         Caption         =   "DForma de pago"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   90
         TabIndex        =   9
         Top             =   3195
         Width           =   1455
      End
      Begin VB.Label lblProve 
         BackStyle       =   0  'Transparent
         Caption         =   "DProveedor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   90
         TabIndex        =   8
         Top             =   3645
         Width           =   1530
      End
      Begin VB.Label lblFecFin 
         BackStyle       =   0  'Transparent
         Caption         =   "DFin suministro"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   3105
         TabIndex        =   7
         Top             =   2430
         Width           =   1455
      End
      Begin VB.Label lblFecIni 
         BackStyle       =   0  'Transparent
         Caption         =   "DInicio suministro"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   90
         TabIndex        =   6
         Top             =   2400
         Width           =   1455
      End
   End
   Begin VB.PictureBox picEsp 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4035
      Left            =   0
      ScaleHeight     =   4035
      ScaleWidth      =   7395
      TabIndex        =   18
      Top             =   360
      Width           =   7395
      Begin VB.TextBox txtProceEsp 
         BackColor       =   &H00FFFFFF&
         Height          =   1635
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   21
         Top             =   240
         Width           =   7155
      End
      Begin VB.CommandButton cmdAbrirEspGen 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   6870
         Picture         =   "frmDetalleGrupo.frx":170E
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   3720
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdSalvarEspGen 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   6390
         Picture         =   "frmDetalleGrupo.frx":1A50
         Style           =   1  'Graphical
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   3720
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin MSComctlLib.ListView lstvwEsp 
         Height          =   1395
         Left            =   135
         TabIndex        =   22
         Top             =   2220
         Width           =   7155
         _ExtentX        =   12621
         _ExtentY        =   2461
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Comentario"
            Object.Width           =   6588
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2867
         EndProperty
      End
      Begin MSComDlg.CommonDialog cmmdEsp 
         Left            =   2880
         Top             =   1935
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label lblEspecificaciones 
         BackColor       =   &H00808000&
         Caption         =   "DEspecificaciones"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   24
         Top             =   0
         Width           =   2235
      End
      Begin VB.Label lblArchivosAdj 
         BackColor       =   &H00808000&
         Caption         =   "DArchivos adjuntos"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   23
         Top             =   1980
         Width           =   2235
      End
   End
   Begin VB.PictureBox picAtributos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4035
      Left            =   0
      ScaleHeight     =   4035
      ScaleWidth      =   7395
      TabIndex        =   15
      Top             =   360
      Width           =   7395
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
         Height          =   3555
         Left            =   135
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   300
         Width           =   7215
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         stylesets.count =   2
         stylesets(0).Name=   "Head"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmDetalleGrupo.frx":1D92
         stylesets(1).Name=   "Verde"
         stylesets(1).BackColor=   32896
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmDetalleGrupo.frx":1DAE
         UseGroups       =   -1  'True
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         SelectByCell    =   -1  'True
         CellNavigation  =   1
         MaxSelectedRows =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   26
         SplitterVisible =   -1  'True
         Groups(0).Width =   5106
         Groups(0).Caption=   "DESCR"
         Groups(0).HeadStyleSet=   "Head"
         Groups(0).Columns.Count=   3
         Groups(0).Columns(0).Width=   1349
         Groups(0).Columns(0).Caption=   "COD"
         Groups(0).Columns(0).Name=   "COD"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Locked=   -1  'True
         Groups(0).Columns(0).StyleSet=   "Verde"
         Groups(0).Columns(1).Width=   3757
         Groups(0).Columns(1).Caption=   "DEN"
         Groups(0).Columns(1).Name=   "DEN"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(1).Locked=   -1  'True
         Groups(0).Columns(1).Style=   1
         Groups(0).Columns(1).ButtonsAlways=   -1  'True
         Groups(0).Columns(1).StyleSet=   "Verde"
         Groups(0).Columns(2).Width=   1561
         Groups(0).Columns(2).Visible=   0   'False
         Groups(0).Columns(2).Caption=   "ID"
         Groups(0).Columns(2).Name=   "ID"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         _ExtentX        =   12726
         _ExtentY        =   6271
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAtributos 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DAtributos"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   135
         TabIndex        =   17
         Top             =   0
         Width           =   765
      End
   End
   Begin VB.Label lblGrupo 
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "                                              "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1080
      TabIndex        =   25
      Top             =   0
      Width           =   6385
   End
End
Attribute VB_Name = "frmDetalleGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public m_oProcesoSeleccionado As CProceso
Public g_oProveedores As CProveedores
Public g_oGrupoSeleccionado As CGrupo
Public g_sFormatoNumber As String

'Variables privadas
Private m_aSayFileNames() As String
Private m_sIdiElArchivo As String
Private m_sIdiTipoOrig As String
Private m_sIdiGuardar As String
Private m_sProceso As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_skb As String



Private Sub CargarRecursos()
    Dim Adores As Ador.Recordset

    On Error Resume Next

    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_DETALLE_GRUPO, basPublic.gParametrosInstalacion.gIdioma)

    If Not Adores Is Nothing Then
        Me.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblFecIni.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblFecFin.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblDest.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblPag.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblProve.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblAtributos.caption = Adores.Fields.Item(0).Value & ":"
        sdbgAtributos.Groups.Item(0).caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblEspecificaciones.caption = Adores.Fields.Item(0).Value & ":"
        Adores.MoveNext
        lblArchivosAdj.caption = Adores.Fields.Item(0).Value & ":"
        Adores.MoveNext
        m_sIdiElArchivo = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sIdiTipoOrig = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sIdiGuardar = Adores.Fields.Item(0).Value
        Adores.MoveNext
        Adores.MoveNext
        m_sProceso = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lblDescripcion.caption = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(1).Text = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(3).Text = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(4).Text = Adores.Fields.Item(0).Value
        
        Adores.MoveNext
        m_sIdiTrue = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_sIdiFalse = Adores.Fields.Item(0).Value
        
        Adores.MoveNext
        cmdAtributos.ToolTipText = Adores.Fields.Item(0).Value
        Adores.MoveNext
        cmdEsp.ToolTipText = Adores.Fields.Item(0).Value
        Adores.MoveNext
        cmdDatGen.ToolTipText = Adores.Fields.Item(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(2).Text = Adores.Fields.Item(0).Value
        Adores.MoveNext
        m_skb = Adores.Fields.Item(0).Value
        
        Set Adores = Nothing
        Adores.Close
    End If
    
End Sub

Private Sub cmdAbrirEspGen_Click()
    Dim Item As MSComctlLib.listItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
            
    On Error GoTo Cancelar:
    
    Set Item = lstvwEsp.selectedItem
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    Else
        Set oEsp = g_oGrupoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & oEsp.nombre
        sFileTitle = Item.Text
    End If
    
    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing
    
    oEsp.ComenzarLecturaData (TipoEspecificacion.EspGrupo)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oEsp = Nothing
        Exit Sub
    End If
            
    oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspGrupo, oEsp.DataSize, sFileName
    
    'Lanzamos la aplicacion
    ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Set oEsp = Nothing
End Sub

Private Sub cmdAtributos_Click()
    Dim oAtributo As CAtributo
    Dim oOferta As COferta
    Dim sLinea As String
    Dim i As Integer
    Dim oProv As CProveedor
    Dim scod1 As String
    
    'A�ade los grupos de proveedores a la grid
    If sdbgAtributos.Groups.Count = 1 Then
         i = 1
         For Each oProv In g_oProveedores
             sdbgAtributos.Groups.Add i
             sdbgAtributos.Groups(i).caption = oProv.Cod & " - " & oProv.Den
             sdbgAtributos.Groups(i).Columns.Add 0
             sdbgAtributos.Groups(i).Columns(0).Name = oProv.Cod
             sdbgAtributos.Groups(i).Columns(0).Locked = True
             sdbgAtributos.Groups(i).HeadStyleSet = "Head"
             i = i + 1
        Next
    End If
    
    'Ahora rellena la grid
    sdbgAtributos.RemoveAll
    
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
    For Each oAtributo In m_oProcesoSeleccionado.AtributosGrupo
        If IsNull(oAtributo.codgrupo) Or oAtributo.codgrupo = g_oGrupoSeleccionado.Codigo Then
            sLinea = oAtributo.Cod & Chr(m_lSeparador) & oAtributo.Den & Chr(m_lSeparador) & oAtributo.idAtribProce
            For Each oProv In g_oProveedores
                Set oOferta = m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod)
                If Not oOferta Is Nothing Then
                    If Not oOferta.AtribGrOfertados Is Nothing Then
                        scod1 = g_oGrupoSeleccionado.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(g_oGrupoSeleccionado.Codigo))
                        scod1 = scod1 & CStr(oAtributo.idAtribProce)
                        If Not oOferta.AtribGrOfertados.Item(scod1) Is Nothing Then
                            Select Case oAtributo.Tipo
                                Case TiposDeAtributos.TipoBoolean
                                    If oOferta.AtribGrOfertados.Item(scod1).valorBool = 1 Then
                                        sLinea = sLinea & Chr(m_lSeparador) & m_sIdiTrue
                                    Else
                                        sLinea = sLinea & Chr(m_lSeparador) & m_sIdiFalse
                                    End If
                                
                                Case TiposDeAtributos.TipoFecha
                                    sLinea = sLinea & Chr(m_lSeparador) & oOferta.AtribGrOfertados.Item(scod1).valorFec
                                Case TiposDeAtributos.TipoNumerico
                                    sLinea = sLinea & Chr(m_lSeparador) & Format(oOferta.AtribGrOfertados.Item(scod1).valorNum, g_sFormatoNumber)
                                Case TiposDeAtributos.TipoString
                                    sLinea = sLinea & Chr(m_lSeparador) & EliminarTab(NullToStr(oOferta.AtribGrOfertados.Item(scod1).valorText))
                            End Select
                        Else
                            sLinea = sLinea & Chr(m_lSeparador) & ""
                        End If
                    Else
                        sLinea = sLinea & Chr(m_lSeparador) & ""
                    End If
                Else
                    sLinea = sLinea & Chr(m_lSeparador) & ""
                End If
                Set oOferta = Nothing
            Next
            'A�ade la l�nea a la grid
            sdbgAtributos.AddItem sLinea
        End If
    Next
    End If
    'Muestra el splitter
    sdbgAtributos.SplitterVisible = True
    sdbgAtributos.SplitterPos = 1
    
    'Muestra el picture de atributos y oculta los dem�s
    picAtributos.Visible = True
    picEsp.Visible = False
    picProce.Visible = False
End Sub

Private Sub cmdDatGen_Click()

    picEsp.Visible = False
    picAtributos.Visible = False
    picProce.Visible = True
    
End Sub

Private Sub cmdEsp_Click()
    Dim oEsp As CEspecificacion
    
    If g_oGrupoSeleccionado.especificaciones Is Nothing Then
        g_oGrupoSeleccionado.CargarTodasLasEspecificaciones , True
    ElseIf g_oGrupoSeleccionado.especificaciones.Count = 0 Then
        g_oGrupoSeleccionado.CargarTodasLasEspecificaciones , True
    End If
    
    'Especificaci�n del proceso:
    txtProceEsp.Text = NullToStr(g_oGrupoSeleccionado.esp)
    
    'Limpia la lista
    lstvwEsp.ListItems.clear
    'A�ade las especificaciones a la lista
    For Each oEsp In g_oGrupoSeleccionado.especificaciones
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.indice), oEsp.nombre
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).Tag = CStr(oEsp.indice)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).ListSubItems.Add , "ESP" & CStr(oEsp.indice), NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).ListSubItems.Add , , oEsp.Fecha
    Next
    
    'Muestra el picture de especificaciones y oculta los dem�s
    picEsp.Visible = True
    picAtributos.Visible = False
    picProce.Visible = False
End Sub

Private Sub cmdSalvarEspGen_Click()
    Dim Item As MSComctlLib.listItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
        
    On Error GoTo Cancelar:
    

    Set Item = lstvwEsp.selectedItem

    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        Set oEsp = g_oGrupoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        cmmdEsp.DialogTitle = m_sIdiGuardar
        cmmdEsp.CancelError = True
        cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
        cmmdEsp.filename = oEsp.nombre
        cmmdEsp.ShowSave

        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdiElArchivo
            Exit Sub
        End If
    
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspGrupo)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Exit Sub
        End If
       
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspGrupo, oEsp.DataSize, sFileName
    End If

Cancelar:
    On Error Resume Next

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
    End If
End Sub


Private Sub Form_Load()
    Dim bSinAtributos As Boolean
    Dim oatrib As CAtributo
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ReDim m_aSayFileNames(0)
    
    'si no se han definido especificaciones a nivel de grupo se ocultan y se dimesionan
    'el resto de los botones
    If g_oGrupoSeleccionado.DefEspecificaciones = False Then
        cmdEsp.Visible = False
        lblGrupo.Left = cmdAtributos.Left
        lblGrupo.Width = lblGrupo.Width + cmdEsp.Width
        cmdAtributos.Left = cmdEsp.Left
    End If
    
    'si no se han definido atributos a nivel de grupo se ocultan y se dimesionan
    'el resto de los botones
    bSinAtributos = False
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        For Each oatrib In m_oProcesoSeleccionado.AtributosGrupo
            If IsNull(oatrib.codgrupo) Or oatrib.codgrupo = g_oGrupoSeleccionado.Codigo Then
                bSinAtributos = True
                Exit For
            End If
        Next
    End If
    If bSinAtributos = False Then
        cmdAtributos.Visible = False
        lblGrupo.Left = cmdAtributos.Left
        lblGrupo.Width = lblGrupo.Width + cmdAtributos.Width
    End If
    
    
    'Rellena el picture de los datos generales y lo muestra
    RellenarDatosGenerales
    cmdDatGen_Click
    
End Sub

Private Sub RellenarDatosGenerales()
'**************************************************************************************
'*** Descripci�n: Este procedimiento rellena los datos generales del grupo          ***
'***              seleccionado en la pantalla de comparativa y adjudicaciones. Se   ***
'***              rellenan los campos correspondientes a la pantalla de datos       ***
'***              generales del grupo  , que es la que se ve cuando se carga el     ***
'***              form.                                                             ***
'***                                                                                ***
'*** Par�metros : ------                                                            ***
'***                                                                                ***
'*** Valor que devuelve: ------                                                     ***
'**************************************************************************************
Const OFFSET = 1200

lblGrupo.caption = Space(4) & g_oGrupoSeleccionado.Codigo & " - " & g_oGrupoSeleccionado.Den

txtDescr = NullToStr(g_oGrupoSeleccionado.Descripcion)

g_oGrupoSeleccionado.CargarTodosLosDatos

If g_oGrupoSeleccionado.DefEspecificaciones = False Then
    Me.Height = txtDescr.Height + txtDescr.Top + 1000
    
    
    'Los siguientes datos se visualizar�n o no dependiendo de si est�n definidos o no a nivel de grupo:
    If g_oGrupoSeleccionado.DefFechasSum = True Then
        txtFin.Text = Format(g_oGrupoSeleccionado.FechaFinSuministro, "Short date")
        txtIni.Text = Format(g_oGrupoSeleccionado.FechaInicioSuministro, "Short date")
        Me.Height = txtIni.Top + OFFSET
    Else
        txtFin.Visible = False
        txtIni.Visible = False
        lblFecFin.Visible = False
        lblFecIni.Visible = False
        'Redimensiona el resto de controles
        lblProveedor.Top = lblFormaPago.Top
        lblProve.Top = lblPag.Top
        lblFormaPago.Top = LblDestino.Top
        lblPag.Top = lblDest.Top
        LblDestino.Top = txtFin.Top
        lblDest.Top = lblFecFin.Top
    End If
    
    If g_oGrupoSeleccionado.DefDestino = True Then
        LblDestino.caption = NullToStr(g_oGrupoSeleccionado.DestCod)
        If g_oGrupoSeleccionado.DestDen <> "" And Not IsNull(g_oGrupoSeleccionado.DestDen) Then
            LblDestino.caption = LblDestino.caption & " - " & g_oGrupoSeleccionado.DestDen
            Me.Height = LblDestino.Top + OFFSET
        End If
    Else
        LblDestino.Visible = False
        lblDest.Visible = False
        'Redimensiona el resto de controles
        lblProveedor.Top = lblFormaPago.Top
        lblProve.Top = lblPag.Top
        lblFormaPago.Top = LblDestino.Top
        lblPag.Top = lblDest.Top
    End If
    
    If g_oGrupoSeleccionado.DefFormaPago = True Then
        lblFormaPago.caption = NullToStr(g_oGrupoSeleccionado.PagCod) & " - " & NullToStr(g_oGrupoSeleccionado.PagDen)
        Me.Height = lblFormaPago.Top + OFFSET
    Else
        lblFormaPago.Visible = False
        lblPag.Visible = False
        'Redimensiona el resto de controles
        lblProveedor.Top = lblFormaPago.Top
        lblProve.Top = lblPag.Top
    End If
    
    If g_oGrupoSeleccionado.DefProveActual = True Then
        lblProveedor.caption = NullToStr(g_oGrupoSeleccionado.ProveActual) & " - " & g_oGrupoSeleccionado.ProveActDen
        Me.Height = lblProveedor.Top + OFFSET
    Else
        lblProveedor.Visible = False
        lblProve.Visible = False
    End If

Else 'Si hay especificaciones el tama�o de la ventana es el mismo
    If g_oGrupoSeleccionado.DefProveActual = True Then
        lblProveedor.caption = NullToStr(g_oGrupoSeleccionado.ProveActual) & " - " & g_oGrupoSeleccionado.ProveActDen
    Else
        lblProveedor.Visible = False
        lblProve.Visible = False
        lblFecIni.Top = lblDest.Top
        lblFecFin.Top = lblDest.Top
        txtIni.Top = LblDestino.Top
        txtFin.Top = LblDestino.Top
        lblDest.Top = lblPag.Top
        LblDestino.Top = lblFormaPago.Top
        lblPag.Top = lblProve.Top
        lblFormaPago.Top = lblProveedor.Top
        txtDescr.Height = txtIni.Top - OFFSET
    End If
    If g_oGrupoSeleccionado.DefFormaPago = True Then
        lblFormaPago.caption = NullToStr(g_oGrupoSeleccionado.PagCod) & " - " & NullToStr(g_oGrupoSeleccionado.PagDen)
    Else
        lblFormaPago.Visible = False
        lblPag.Visible = False
        'Redimensiona el resto de controles
        lblFecIni.Top = lblDest.Top
        lblFecFin.Top = lblDest.Top
        txtIni.Top = LblDestino.Top
        txtFin.Top = LblDestino.Top
        lblDest.Top = lblPag.Top
        LblDestino.Top = lblFormaPago.Top
        txtDescr.Height = txtIni.Top - OFFSET
    End If
    If g_oGrupoSeleccionado.DefDestino = True Then
        LblDestino.caption = NullToStr(g_oGrupoSeleccionado.DestCod)
        If g_oGrupoSeleccionado.DestDen <> "" And Not IsNull(g_oGrupoSeleccionado.DestDen) Then
            LblDestino.caption = LblDestino.caption & " - " & g_oGrupoSeleccionado.DestDen
        End If
    Else
        LblDestino.Visible = False
        lblDest.Visible = False
        'Redimensiona el resto de controles
        lblFecIni.Top = lblDest.Top
        lblFecFin.Top = lblDest.Top
        txtIni.Top = LblDestino.Top
        txtFin.Top = LblDestino.Top
        txtDescr.Height = txtIni.Top - OFFSET
    End If
    If g_oGrupoSeleccionado.DefFechasSum = True Then
        txtFin.Text = Format(g_oGrupoSeleccionado.FechaFinSuministro, "Short date")
        txtIni.Text = Format(g_oGrupoSeleccionado.FechaInicioSuministro, "Short date")
    Else
        txtFin.Visible = False
        txtIni.Visible = False
        lblFecFin.Visible = False
        lblFecIni.Visible = False
        txtDescr.Height = txtDescr.Height + OFFSET
    End If

End If
End Sub

Private Sub Form_Resize()

On Error Resume Next
    
    If Me.Height <= 1500 Then Exit Sub
    If Me.Width <= 3000 Then Exit Sub
    
    'Picture para el proceso:
    picProce.Width = Me.Width - 60
    lblGrupo.Width = Me.Width - lblGrupo.Left
    txtDescr.Width = picProce.Width - 2100
    'Picture de los adjuntos:
    picEsp.Width = Me.Width - 180
    picEsp.Height = Me.Height - 765
    
    cmdAbrirEspGen.Left = Me.Width - 705
    cmdAbrirEspGen.Top = Me.Height - 1080
    cmdSalvarEspGen.Left = Me.Width - 1185
    cmdSalvarEspGen.Top = Me.Height - 1080
    
    txtProceEsp.Width = Me.Width - 420
    txtProceEsp.Height = Me.Height / 3 + 300
    lblArchivosAdj.Top = txtProceEsp.Height + 250
    lstvwEsp.Top = lblArchivosAdj.Top + 245
    lstvwEsp.Width = Me.Width - 420
    If (cmdAbrirEspGen.Top - lstvwEsp.Top - 90) < 0 Then
        lstvwEsp.Height = (Me.Height / 3) + 120
    Else
        lstvwEsp.Height = cmdAbrirEspGen.Top - lstvwEsp.Top - 90
    End If
    
    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.3
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.15
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.34
    lstvwEsp.ColumnHeaders(4).Width = lstvwEsp.Width * 0.2
    
    'Picture de los atributos:
    picAtributos.Height = Me.Height - 765
    picAtributos.Width = Me.Width - 180
    sdbgAtributos.Height = Me.Height - 1245
    sdbgAtributos.Width = Me.Width - 360
    

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oProcesoSeleccionado = Nothing
    Set g_oGrupoSeleccionado = Nothing
    Set g_oProveedores = Nothing
End Sub



Private Sub sdbgAtributos_BtnClick()
    'Mostrar el detalle del atributo
    
    Dim sIdAtrib As String
    
    sIdAtrib = sdbgAtributos.Columns("ID").Value
    
    Set frmDetAtribProce.g_oProceso = m_oProcesoSeleccionado
    Set frmDetAtribProce.g_oProveedores = g_oProveedores
    
    'Comprueba si es un atributo de grupo
    If Not m_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        If Not m_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib) Is Nothing Then
            Set frmDetAtribProce.g_oAtributo = m_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib)
            
            If Not IsNull(m_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib).codgrupo) Then
                'Comprueba si est� definido a nivel de grupo o de proceso
                Set frmDetAtribProce.g_oGrupoSeleccionado = g_oGrupoSeleccionado
                frmDetAtribProce.sdbcGrupos.Value = g_oGrupoSeleccionado.Codigo
                frmDetAtribProce.sdbcGrupos.Text = g_oGrupoSeleccionado.Codigo & " - " & g_oGrupoSeleccionado.Den
            Else
                frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            End If
            
        End If
    End If
    frmDetAtribProce.g_sOrigen = "frmDetalleProceso"
    frmDetAtribProce.sstabGeneral.Tab = 0
    frmDetAtribProce.Show vbModal
End Sub


