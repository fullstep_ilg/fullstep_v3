VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmNuevoImpuesto 
   BackColor       =   &H00808000&
   Caption         =   "dA�adir tipo de impuesto"
   ClientHeight    =   2835
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   6570
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmNuevoImpuesto.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2835
   ScaleWidth      =   6570
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtGrupoCompatibilidad 
      Height          =   285
      Left            =   1650
      TabIndex        =   7
      Top             =   600
      Width           =   3135
   End
   Begin VB.TextBox txtRetenido 
      Height          =   285
      Left            =   1650
      TabIndex        =   5
      Top             =   210
      Width           =   3135
   End
   Begin VB.CheckBox chkRetenido 
      BackColor       =   &H00808000&
      Caption         =   "dRetenido"
      ForeColor       =   &H8000000B&
      Height          =   255
      Left            =   5010
      TabIndex        =   3
      Top             =   210
      Width           =   1155
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3330
      TabIndex        =   2
      Top             =   2460
      Width           =   975
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2130
      TabIndex        =   1
      Top             =   2460
      Width           =   975
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgImpuesto 
      Height          =   615
      Left            =   90
      TabIndex        =   0
      Top             =   1710
      Width           =   6360
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   3
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2514
      Columns(0).Caption=   "IDI"
      Columns(0).Name =   "IDI"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   14671839
      Columns(1).Width=   8123
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_IDI"
      Columns(2).Name =   "COD_IDI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   11218
      _ExtentY        =   1085
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcConcepto 
      Height          =   285
      Left            =   1650
      TabIndex        =   10
      Top             =   980
      Width           =   2205
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6482
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3889
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblAplicableA 
      BackColor       =   &H00808000&
      Caption         =   "DAplicable a:"
      ForeColor       =   &H8000000B&
      Height          =   285
      Left            =   120
      TabIndex        =   9
      Top             =   980
      Width           =   1485
   End
   Begin VB.Label lblGrupoCompatibilidad 
      BackColor       =   &H00808000&
      Caption         =   "DGrupo Compatibilidad"
      ForeColor       =   &H8000000B&
      Height          =   405
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Width           =   1485
   End
   Begin VB.Label lblDescrip 
      BackColor       =   &H00808000&
      Caption         =   "DDescripcion"
      ForeColor       =   &H8000000B&
      Height          =   285
      Left            =   120
      TabIndex        =   6
      Top             =   1410
      Width           =   1485
   End
   Begin VB.Label lblTipoImpuesto 
      BackColor       =   &H00808000&
      Caption         =   "DTipo de impuesto"
      ForeColor       =   &H8000000B&
      Height          =   285
      Left            =   120
      TabIndex        =   4
      Top             =   210
      Width           =   1485
   End
End
Attribute VB_Name = "frmNuevoImpuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public m_sCodImpuesto As String
Public g_lGrupoCompatibilidad As Long
Public g_ConceptoLineaCatalogo As Variant

Public FormOrigen As String

Private g_oIdiomas As CIdiomas
Private m_sGasto As String
Private m_sInversion As String
Private m_sAmbos As String
''' <summary>
''' Procedimiento que carga los textos que aparecer�n en el formulario en el idioma que corresponda
''' </summary>
''' <remarks>Llamada desde: form_load ; Tiempo m�ximo: 0</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_NUEVO_IMPUESTO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblTipoImpuesto.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkRetenido.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblDescrip.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblGrupoCompatibilidad.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblAplicableA.caption = Ador(0).Value
        Ador.MoveNext
        m_sGasto = Ador(0).Value
        Ador.MoveNext
        m_sInversion = Ador(0).Value
        Ador.MoveNext
        m_sAmbos = Ador(0).Value
        Ador.MoveNext
        Me.sdbgImpuesto.Columns("IDI").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgImpuesto.Columns("DEN").caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
''' <summary>
''' Graba el  nuevo impuesto y se lo pasa al frmImpuestos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdAceptar_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim i As Integer
    Dim vbm As Variant
    Dim oMultiidi As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim oImpuesto As CImpuesto
    Dim Crear As Boolean
    
    Crear = False
    
    'Tiene que introducir la denominaci�n en todos los idiomas:
    sdbgImpuesto.MoveFirst
    For i = 0 To sdbgImpuesto.Rows - 1
        vbm = sdbgImpuesto.AddItemBookmark(i)
            If sdbgImpuesto.Columns("DEN").CellValue(vbm) = "" Then
                oMensajes.NoValido sdbgImpuesto.Columns("IDI").CellValue(vbm)
                Exit Sub
            End If
    Next i
    
    If Me.txtGrupoCompatibilidad.Text <> "" Then
       If Not IsNumeric(Me.txtGrupoCompatibilidad.Text) Then
            oMensajes.NoValido Replace(Me.lblGrupoCompatibilidad.caption, ":", "")
            Exit Sub
       ElseIf g_lGrupoCompatibilidad <> 0 And g_lGrupoCompatibilidad <> Me.txtGrupoCompatibilidad.Text Then
            oMensajes.NoValido Replace(Me.lblGrupoCompatibilidad.caption, ":", "")
            Exit Sub
       End If
    Else
        oMensajes.NoValido Replace(Me.lblGrupoCompatibilidad.caption, ":", "")
        Exit Sub
    End If
    
    
    If Me.sdbcConcepto.Value = "" Then
        oMensajes.NoValido Replace(Me.lblAplicableA.caption, ":", "")
        Exit Sub
    ElseIf CInt(Me.sdbcConcepto.Value) <> g_ConceptoLineaCatalogo And Not IsNull(g_ConceptoLineaCatalogo) And Not IsEmpty(g_ConceptoLineaCatalogo) Then
        oMensajes.NoValido Replace(Me.lblAplicableA.caption, ":", "")
        Exit Sub
    End If
    
    
    Screen.MousePointer = vbHourglass
    
    Set oImpuesto = oFSGSRaiz.Generar_CImpuesto
    oImpuesto.CodImpuesto = Me.txtRetenido.Text
    oImpuesto.Retenido = (Me.chkRetenido.Value = vbChecked)
    oImpuesto.Concepto = sdbcConcepto.Value
    oImpuesto.GrupoCompatibilidad = txtGrupoCompatibilidad.Text
         
    Set oMultiidi = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In g_oIdiomas
        For i = 0 To sdbgImpuesto.Rows - 1
            vbm = sdbgImpuesto.AddItemBookmark(i)
            If sdbgImpuesto.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                oMultiidi.Add oIdioma.Cod, Trim(sdbgImpuesto.Columns("DEN").CellValue(vbm))
                
                If oIdioma.Cod = gParametrosInstalacion.gIdioma Then oImpuesto.Descripcion = Trim(sdbgImpuesto.Columns("DEN").CellValue(vbm))
                
                Exit For
            End If
        Next i
    Next
    

    If FormOrigen = "frmImpuestoPedido.ctlImpuestos_NuevoImpuesto" Then
      Crear = frmImpuestoPedido.ctlImpuestos.ComprobarImpuestoAInsertar(oImpuesto.Concepto, oImpuesto.GrupoCompatibilidad)
      'If cumple los requisitos, se crea el nuevo impuesto a�adiendose en BD
      If Crear Then
        Set oImpuesto.Denominaciones = oMultiidi
        udtTeserror = oImpuesto.CrearNuevoImpuesto
      Else
        Screen.MousePointer = vbNormal
        Set oImpuesto = Nothing
        Exit Sub
      End If
    Else
      Set oImpuesto.Denominaciones = oMultiidi
      udtTeserror = oImpuesto.CrearNuevoImpuesto
   End If
    
    
    If udtTeserror.NumError = TESnoerror Then
        Select Case FormOrigen
            Case "frmImpuestoPedido.ctlImpuestos_NuevoImpuesto"
                If Crear Then
                    frmImpuestoPedido.ctlImpuestos.CargaIdImpuesto oImpuesto.IDImpuesto, oImpuesto.CodImpuesto, oImpuesto.Descripcion, oImpuesto.Retenido, oImpuesto.Concepto, oImpuesto.GrupoCompatibilidad, FormOrigen
                End If
            Case Else
                frmImpuestos.CargaIdImpuesto oImpuesto.IDImpuesto, oImpuesto.CodImpuesto, oImpuesto.Descripcion, oImpuesto.Retenido, oImpuesto.Concepto, oImpuesto.GrupoCompatibilidad
        End Select

    Else
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        Exit Sub
    End If
    
    Screen.MousePointer = vbNormal
    Set oImpuesto = Nothing
    
    Unload Me
End Sub
''' <summary>
''' Cierra la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdCancelar_Click()
    Unload Me
End Sub
''' <summary>
''' Cargar la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Me.txtRetenido.Text = m_sCodImpuesto
    Me.txtGrupoCompatibilidad.Text = 1
    
    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
    Set g_oIdiomas = oGestorParametros.DevolverIdiomas(, , False)
    
    CargarGridIdiomas
    
    CargarComboConceptos
    
    'Ajusta la pantalla para que no haya que hacer scroll
    sdbgImpuesto.Height = (sdbgImpuesto.Rows + 1.2) * sdbgImpuesto.RowHeight
    cmdAceptar.Top = sdbgImpuesto.Top + sdbgImpuesto.Height + 85
    cmdCancelar.Top = cmdAceptar.Top
    Me.Height = cmdAceptar.Top + cmdAceptar.Height + 550
End Sub
''' <summary>
''' Cargar el combo de los conceptos(gastp, inversion, ambos...)
''' </summary>
Private Sub CargarComboConceptos()
    
    Me.sdbcConcepto.AddItem TipoConcepto.Gasto & Chr(m_lSeparador) & m_sGasto
    Me.sdbcConcepto.AddItem TipoConcepto.Inversion & Chr(m_lSeparador) & m_sInversion
    Me.sdbcConcepto.AddItem TipoConcepto.Ambos & Chr(m_lSeparador) & m_sAmbos

End Sub

Private Sub sdbcConcepto_InitColumnProps()
    sdbcConcepto.DataFieldList = "Column 0"
    sdbcConcepto.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Cargar grid con los idiomas
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridIdiomas()
    Dim oIdioma As CIdioma

    'Carga las denominaciones del grupo de datos generales en todos los idiomas:
    For Each oIdioma In g_oIdiomas
        sdbgImpuesto.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
    Next
    sdbgImpuesto.MoveFirst

End Sub
''' <summary>
''' Descargar las variables de la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Set g_oIdiomas = Nothing
End Sub


