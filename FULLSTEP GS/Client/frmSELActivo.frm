VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSelActivo 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "B�squeda de Activos"
   ClientHeight    =   4320
   ClientLeft      =   1440
   ClientTop       =   1635
   ClientWidth     =   11835
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELActivo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4320
   ScaleWidth      =   11835
   Begin VB.PictureBox picDatArticulo 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1050
      Left            =   15
      ScaleHeight     =   1050
      ScaleWidth      =   11760
      TabIndex        =   9
      Top             =   30
      Width           =   11760
      Begin VB.CommandButton cmdCargar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11340
         Picture         =   "frmSELActivo.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Cargar"
         Top             =   60
         Width           =   315
      End
      Begin VB.CommandButton cmdAyuda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   11430
         Picture         =   "frmSELActivo.frx":0D3E
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   450
         Width           =   225
      End
      Begin VB.CommandButton cmdSelCentro 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10380
         Picture         =   "frmSELActivo.frx":0F6F
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   660
         Width           =   345
      End
      Begin VB.TextBox txtDen 
         Height          =   315
         Left            =   6465
         TabIndex        =   1
         Top             =   90
         Width           =   3870
      End
      Begin VB.TextBox txtNumERP 
         Height          =   315
         Left            =   1155
         TabIndex        =   2
         Top             =   645
         Width           =   3870
      End
      Begin VB.TextBox txtCod 
         Height          =   315
         Left            =   1185
         TabIndex        =   0
         Top             =   105
         Width           =   3870
      End
      Begin VB.TextBox txtCentro 
         Height          =   315
         Left            =   6465
         TabIndex        =   3
         Top             =   645
         Width           =   3870
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n:"
         ForeColor       =   &H80000014&
         Height          =   255
         Left            =   5325
         TabIndex        =   13
         Top             =   165
         Width           =   1440
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         ForeColor       =   &H80000014&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   165
         Width           =   885
      End
      Begin VB.Label lblNumERP 
         BackColor       =   &H00808000&
         Caption         =   "N�mero ERP:"
         ForeColor       =   &H80000014&
         Height          =   255
         Left            =   135
         TabIndex        =   11
         Top             =   675
         Width           =   1425
      End
      Begin VB.Label lblCentro 
         BackColor       =   &H00808000&
         Caption         =   "Centro:"
         ForeColor       =   &H80000014&
         Height          =   255
         Left            =   5295
         TabIndex        =   10
         Top             =   675
         Width           =   1440
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgActivos 
      Height          =   2835
      Left            =   0
      TabIndex        =   6
      Top             =   1080
      Width           =   11790
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   6
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   16777215
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSELActivo.frx":12F1
      stylesets(1).Name=   "ActiveRowBlue"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8388608
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSELActivo.frx":130D
      stylesets(1).AlignmentPicture=   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   0
      StyleSet        =   "Normal"
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Columns.Count   =   6
      Columns(0).Width=   3889
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "CODIGO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(1).Width=   5900
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DENOM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasForeColor=   -1  'True
      Columns(2).Width=   4048
      Columns(2).Caption=   "N�mero ERP"
      Columns(2).Name =   "NUMERP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   5900
      Columns(3).Caption=   "Centro de Coste "
      Columns(3).Name =   "CENTRO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasForeColor=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "GENERICO"
      Columns(4).Name =   "ID"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "UNI"
      Columns(5).Name =   "UNI"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   20796
      _ExtentY        =   5001
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCerrar 
      Cancel          =   -1  'True
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   6120
      TabIndex        =   8
      Top             =   3960
      Width           =   1125
   End
   Begin VB.CommandButton cmdSeleccionar 
      Caption         =   "&Seleccionar"
      Height          =   315
      Left            =   4875
      TabIndex        =   7
      Top             =   3960
      Width           =   1125
   End
End
Attribute VB_Name = "frmSelActivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_sOrigen As String
Public m_oActivos As CActivos
Public g_lEmpresa As Long
Public g_sCodCentroSM As String
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String
Public g_sUON4 As String
Private m_oCentrosCoste As CCentrosCoste
Public g_oCCSeleccionado As CCentroCoste
Public m_bRespetar As String

Public g_oOrigen As Form 'Variable para frmpedidos

Private Sub cmdAyuda_Click()
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub cmdCargar_Click()
    CargarActivos
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub cmdSelCentro_Click()
    frmSelCenCoste.g_sOrigen = "SelActivo"
    frmSelCenCoste.g_bCentrosSM = True
    frmSelCenCoste.g_lIdEmpresa = g_lEmpresa
    frmSelCenCoste.Show 1
    m_bRespetar = True
    If Not g_oCCSeleccionado Is Nothing Then
        txtCentro.Text = g_oCCSeleccionado.CodConcat
    End If
End Sub

''' <summary>
''' Pasar al form origen el activo
''' </summary>
''' <remarks>Tiempo m�ximo:0seg.</remarks>
Private Sub cmdSeleccionar_Click()

    If m_oActivos.Count = 0 Then Exit Sub
    
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Activos) Then
        If m_oActivos.Item(sdbgActivos.Columns("CODIGO").Value).CodERP = "" Then
            oMensajes.ActivoNoVinculadoSAP (True)
            Exit Sub
        End If
    End If
    
    Select Case g_sOrigen
    
        Case "frmRecepcion"
            Set frmRecepcion.g_oActivo = m_oActivos.Item(sdbgActivos.Columns("CODIGO").Value)
        Case "frmSeguimiento"
            Set frmSeguimiento.g_oActivo = m_oActivos.Item(sdbgActivos.Columns("CODIGO").Value)
        Case "MisPedidos"
            Set g_oOrigen.g_oLineaSeleccionada.Activo = m_oActivos.Item(sdbgActivos.Columns("CODIGO").Value)
    End Select
    
    Unload Me
End Sub

Private Sub Form_Load()
    
    Me.Height = 4725
    Me.Width = 11955
            
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
    PonerFieldSeparator Me
    
    Set m_oActivos = oFSGSRaiz.Generar_CActivos
    Set m_oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
    
    If Not basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Activos) Then
        sdbgActivos.Columns("NUMERP").Visible = False
    End If
    
    ValidarCentroCoste
   
End Sub
''' <summary>
''' Procedimiento que valida el centro de coste que se le pasa al form en la llamada
''' </summary>
''' <remarks>Llamada desde Form_Load. Tiempo m�ximo 0,03 seg.</remarks>
Private Sub ValidarCentroCoste()

    If (g_sCodCentroSM <> "") Then
        m_oCentrosCoste.CargarCentrosDeCoste g_sCodCentroSM, , , , , , IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), True, g_lEmpresa
    ElseIf g_sUON1 <> "" Then
        m_oCentrosCoste.CargarCentrosDeCoste , g_sUON1, g_sUON2, g_sUON3, g_sUON4, , IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), True, g_lEmpresa
    End If
    
    If m_oCentrosCoste.Count > 0 Then
        m_bRespetar = True
        Set g_oCCSeleccionado = m_oCentrosCoste.Item(1)
        txtCentro.Text = g_oCCSeleccionado.CodConcat
        m_bRespetar = False
        CargarActivos
    End If
    
End Sub

Private Sub Form_Resize()

If sdbgActivos.Columns("NUMERP").Visible = False Then
    sdbgActivos.Columns("CODIGO").Width = sdbgActivos.Width * 21 / 100
    sdbgActivos.Columns("DENOM").Width = sdbgActivos.Width * 40 / 100
    sdbgActivos.Columns("CENTRO").Width = sdbgActivos.Width * 33 / 100

Else
    sdbgActivos.Columns("CODIGO").Width = sdbgActivos.Width * 17 / 100
    sdbgActivos.Columns("DENOM").Width = sdbgActivos.Width * 30 / 100
    sdbgActivos.Columns("NUMERP").Width = sdbgActivos.Width * 16 / 100
    sdbgActivos.Columns("CENTRO").Width = sdbgActivos.Width * 30 / 100
End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oActivos = Nothing
    Set m_oCentrosCoste = Nothing
    g_lEmpresa = 0
    g_sCodCentroSM = ""
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
    g_sUON4 = ""
    g_sOrigen = ""

    Set g_oCCSeleccionado = Nothing
    m_bRespetar = False
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELACTIVO, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        lblCod.caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value
        Ador.MoveNext
        lblNumERP.caption = Ador(0).Value
        Ador.MoveNext
        lblCentro.caption = Ador(0).Value
        Ador.MoveNext
'        cmdBuscar.caption = Ador(0).Value
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value
        Ador.MoveNext
        sdbgActivos.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgActivos.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgActivos.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgActivos.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Private Sub sdbgActivos_DblClick()
    cmdSeleccionar_Click
End Sub

Private Sub sdbgActivos_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then cmdSeleccionar_Click
End Sub

Private Sub txtCentro_Change()
    sdbgActivos.RemoveAll
End Sub

Private Sub txtCod_Change()
    sdbgActivos.RemoveAll
End Sub

Private Sub txtCentro_Validate(Cancel As Boolean)
Dim bExiste As Boolean
'El usuario ha podido introducir un CC SM o un c�digo de UO o una den
    If m_bRespetar Then Exit Sub
    
    If txtCentro.Text = "" Then Exit Sub
    
    bExiste = True
    'Busca por codigo SM
    m_oCentrosCoste.CargarCentrosDeCoste txtCentro.Text, , , , , , IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), True, g_lEmpresa
    If m_oCentrosCoste.Count = 0 Then
        'Busca por UON1
        m_oCentrosCoste.CargarCentrosDeCoste , txtCentro.Text, , , , , IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), True, g_lEmpresa
        If m_oCentrosCoste.Count = 0 Then
            'Busca por UON2
            m_oCentrosCoste.CargarCentrosDeCoste , , txtCentro.Text, , , , IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), True, g_lEmpresa
            If m_oCentrosCoste.Count = 0 Then
                'Busca por UON3
                m_oCentrosCoste.CargarCentrosDeCoste , , , txtCentro.Text, , , IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), True, g_lEmpresa
                If m_oCentrosCoste.Count = 0 Then
                    'Busca por UON4
                    m_oCentrosCoste.CargarCentrosDeCoste , , , , txtCentro.Text, , IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), True, g_lEmpresa
                    If m_oCentrosCoste.Count = 0 Then
                        'Busca por den
                        m_oCentrosCoste.CargarCentrosDeCoste , , , , , txtCentro.Text, IIf(basOptimizacion.gTipoDeUsuario = Administrador, "", basOptimizacion.gvarCodUsuario), True, g_lEmpresa
                        If m_oCentrosCoste.Count = 0 Then
                            bExiste = False
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    If bExiste Then
        m_bRespetar = True
        txtCentro.Text = m_oCentrosCoste.Item(1).CodConcat
        Set g_oCCSeleccionado = m_oCentrosCoste.Item(1)
        m_bRespetar = False
    Else
        oMensajes.NoValido Left(lblCentro.caption, Len(lblCentro.caption) - 1)
        txtCentro.Text = ""
        Set g_oCCSeleccionado = Nothing
        g_sCodCentroSM = ""
    End If
End Sub

Private Sub txtDen_Change()
    sdbgActivos.RemoveAll
End Sub

''' <summary>
''' Procedimiento que carga los activos segun el filtro en el grid
''' </summary>
''' <remarks>Llamada desde cmdCargar_Click, ValidarCentroCoste. Tiempo m�ximo 0,03 seg.</remarks>
Private Sub CargarActivos()
    Dim oActivo As CActivo
    Dim sCod As String
    Dim sDen As String
    Dim sERP As String
    Dim sLike As String

    If txtCod.Text = "" And txtDen.Text = "" And txtNumERP.Text = "" And txtCentro.Text = "" Then
        'No ha seleccionado ning�n criterio de b�squeda
        oMensajes.MensajeOKOnly oMensajes.CargarTexto(FRM_SELACTIVO, 13), TipoIconoMensaje.Information
        Exit Sub
    Else
        sLike = "000"
        If txtCod.Text <> "" Then
            If InStr(txtCod.Text, "*") > 0 Then
                sLike = "100"
                sCod = txtCod.Text
                sCod = Replace(sCod, "*", "%", , , vbTextCompare)
            Else
                sCod = txtCod.Text
            End If
        End If
        If txtDen.Text <> "" Then
            If InStr(txtDen.Text, "*") > 0 Then
                sLike = Left(sLike, 1) & "1" & Right(sLike, 1)
                sDen = txtDen.Text
                sDen = Replace(sDen, "*", "%", , , vbTextCompare)
            Else
                sDen = txtDen.Text
            End If
        End If
        If txtNumERP.Text <> "" Then
            If InStr(txtNumERP.Text, "*") > 0 Then
                sLike = Left(sLike, 2) & "1"
                sERP = txtNumERP.Text
                sERP = Replace(sERP, "*", "%", , , vbTextCompare)
            Else
                sERP = txtNumERP.Text
            End If
        End If
        If Not g_oCCSeleccionado Is Nothing Then
            g_sCodCentroSM = g_oCCSeleccionado.COD_SM
        Else
            g_sCodCentroSM = ""
        End If
        m_oActivos.CargarTodosLosActivos g_lEmpresa, basOptimizacion.gTipoDeUsuario, CStr(basOptimizacion.gvarCodUsuario), g_sCodCentroSM, sCod, sDen, sERP, sLike
        
        sdbgActivos.RemoveAll
        For Each oActivo In m_oActivos
            sdbgActivos.AddItem oActivo.Cod & Chr(m_lSeparador) & oActivo.Den & Chr(m_lSeparador) & oActivo.CodERP & Chr(m_lSeparador) & oActivo.CentroSM & " - " & oActivo.CentroSMDen & Chr(m_lSeparador)
        Next
    End If
End Sub

Private Sub txtNumERP_Change()
    sdbgActivos.RemoveAll
End Sub
