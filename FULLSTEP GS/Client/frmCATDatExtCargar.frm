VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCATDatExtCargar 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCarga de datos externos"
   ClientHeight    =   1395
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7845
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATDatExtCargar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1395
   ScaleWidth      =   7845
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdFuente 
      Height          =   285
      Left            =   7440
      Picture         =   "frmCATDatExtCargar.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Examinar"
      Top             =   360
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   3000
      TabIndex        =   3
      Top             =   960
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   4125
      TabIndex        =   4
      Top             =   960
      Width           =   1005
   End
   Begin VB.TextBox txtFuente 
      Height          =   285
      Left            =   2520
      TabIndex        =   1
      Top             =   360
      Width           =   4815
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblFuente 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "DFuente de datos externos:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   400
      Width           =   1965
   End
End
Attribute VB_Name = "frmCATDatExtCargar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private oFos As Scripting.FileSystemObject
Private sFuente As String
Private sDocument As String
Private sGenerarExcel As String
Private sCargandoDatos As String
Private sGuardandoBD As String
Private sTrue As String
Private sFalse As String
Private m_sFileName As String

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_CARGAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblFuente.caption = Ador(0).Value & ":"
        sFuente = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sDocument = Ador(0).Value
        Ador.MoveNext
        sGenerarExcel = Ador(0).Value
        Ador.MoveNext
        sCargandoDatos = Ador(0).Value
        Ador.MoveNext
        sGuardandoBD = Ador(0).Value
        
        Ador.MoveNext
        sTrue = Ador(0).Value
        Ador.MoveNext
        sFalse = Ador(0).Value
        Ador.Close
            
    End If
    
    Set Ador = Nothing
End Sub

Private Sub cmdAceptar_Click()
Dim oDrive As Drive
Dim bBorrar As Boolean

    'Realiza la carga de los datos del fichero especificado
    If txtFuente.Text = "" Then
        oMensajes.FaltaFuente
        Exit Sub
    Else
        If Right(txtFuente.Text, 3) <> "xls" Then
            oMensajes.NoValido sFuente
            If Me.Visible Then cmdFuente.SetFocus
            Exit Sub
        End If
    End If
     
    If Not oFos.FileExists(txtFuente.Text) Then
        oMensajes.PlantillaNoEncontrada txtFuente.Text
        If Me.Visible Then cmdFuente.SetFocus
        Exit Sub
    End If
    bBorrar = False
    Set oDrive = oFos.GetDrive(oFos.GetDriveName(txtFuente.Text))
    If oDrive.DriveType <> 2 Then
        m_sFileName = FSGSLibrary.DevolverPathFichTemp
        oFos.CopyFile txtFuente.Text, m_sFileName
        m_sFileName = m_sFileName & oFos.GetFileName(txtFuente.Text)
        bBorrar = True
    Else
        m_sFileName = txtFuente.Text
    End If
    Set oDrive = Nothing
    CargarDatos
    If bBorrar Then
        oFos.DeleteFile m_sFileName, True
    End If
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdFuente_Click()
    On Error GoTo Error
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sDocument & " (*.xls)|*.xls"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtFuente.Text = cmmdDot.filename
    
    Exit Sub
    
Error:
    
    Exit Sub
End Sub

Private Sub Form_Load()
    CargarRecursos
    m_sFileName = ""
    Set oFos = New Scripting.FileSystemObject
End Sub

Private Sub CargarDatos()
    Dim appexcel As Object
    Dim xlBook As Object
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim sGmn1 As Variant
    Dim sGMN2 As Variant
    Dim sGMN3 As Variant
    Dim sGMN4 As Variant
    Dim scodProve As String
    Dim sDenProve As String
    Dim oArticulos As CArticulos
    Dim oAtribs As CAtributos
    Dim oProves  As CProveedor
    Dim teserror As TipoErrorSummit
    Dim pi As PajantImage
    Dim imgProve As Variant
    Dim bExiste As Boolean
    Dim sConnect As String
    Dim oExcelAdoConn As ADODB.Connection
    Dim oExcelAdoRS As ADODB.Recordset
    Dim xls()
    Dim iNumArts As Integer
    Dim iNumAtribs As Integer
    Dim iLineaArticulo As Integer
    Dim iLineaAtributo As Integer
    Dim iTipoDato As Integer
    Dim iTipoIntroduccion As Integer
    Dim dblMax As Double
    Dim dblMin As Double
    Dim dtMax As Date
    Dim dtMin As Date
    Dim bExcelOpened  As Boolean

On Error GoTo Error

    Screen.MousePointer = vbHourglass

    Me.Hide
    
    Set oExcelAdoConn = New ADODB.Connection
    sConnect = "Provider=MSDASQL.1;" _
             & "Extended Properties=""DBQ=" & m_sFileName & ";" _
             & "Driver={Microsoft Excel Driver (*.xls)};" _
             & "FIL=excel 8.0;" _
             & "ReadOnly=0;" _
             & "UID=admin;"""
             
    oExcelAdoConn.Open sConnect
    
    
    Set oExcelAdoRS = New ADODB.Recordset
    
    oExcelAdoRS.Open "SELECT * from [HojaDeCargaEnDB$]", oExcelAdoConn
    
    
    xls = oExcelAdoRS.GetRows()
        
    
    oExcelAdoRS.Close
    Set oExcelAdoRS = Nothing
    
    oExcelAdoConn.Close
    
    Set oExcelAdoConn = Nothing
    
        
    frmESPERA.lblGeneral.caption = sCargandoDatos

    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.ProgressBar2.Value = 1

    frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 1
    
    frmESPERA.lblDetalle = sGenerarExcel
    
    
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    
    frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    frmESPERA.lblDetalle = sCargandoDatos
    scodProve = CStr(xls(0, 0))
    sDenProve = xls(0, 1)
    iNumArts = CInt(xls(0, 2))
    sGmn1 = CStr(NullToStr(xls(0, 3)))
    sGMN2 = CStr(NullToStr(xls(0, 4)))
    sGMN3 = CStr(NullToStr(xls(0, 5)))
    sGMN4 = CStr(NullToStr(xls(0, 6)))
    
    For i = 1 To iNumArts
        iLineaArticulo = DevolverLineaArticulo(xls, i)
        
        ' **************  Trata la imagen **************************************************
        Set appexcel = CreateObject("Excel.Application")
        bExcelOpened = True
        appexcel.DisplayAlerts = False
        Set xlBook = appexcel.Workbooks.Open(m_sFileName)
        
        Set pi = New PajantImage
        pi.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"
        
        bExiste = False
        For k = 1 To xlBook.Sheets(3).Pictures.Count
            If xlBook.Sheets(3).Pictures(k).Name = "Picture" & CStr(xls(5, iLineaArticulo)) Then
                bExiste = True
                Exit For
            End If
        Next k

        If bExiste = True Then
            xlBook.Sheets(3).Shapes("Picture" & CStr(xls(5, iLineaArticulo))).Select
            '1=xlScreen, 2=xlBitmap
            xlBook.Sheets(3).Shapes("Picture" & CStr(xls(5, iLineaArticulo))).CopyPicture 1, 2

            pi.ImportFrom PJT_CLIPBOARD, 0, 0, imgProve, PJT_TONEWIMAGE

            'Guarda la imagen en imgProve para a�adirla despu�s a la base de datos
            pi.ExportTo PJT_MEMPNG, 0, 0, imgProve, PJT_FRAME

            Set pi = Nothing
        Else
            imgProve = Null
        End If
        'Cierra excel
        xlBook.Close False
        appexcel.Quit
        bExcelOpened = False
        Set appexcel = Nothing
        Set xlBook = Nothing
        '******************************************************************************
        oArticulos.Add CStr(xls(1, iLineaArticulo)), CStr(xls(2, iLineaArticulo)), CStr(xls(3, iLineaArticulo)), CStr(xls(4, iLineaArticulo)), CStr(xls(5, iLineaArticulo)), CStr(xls(6, iLineaArticulo)), , , , , , , IIf((CStr(xls(14, iLineaArticulo)) = "-"), Null, CStr(NullToStr(xls(7, iLineaArticulo)))), imgProve
        Set oAtribs = oFSGSRaiz.Generar_CAtributos
        iNumAtribs = CInt(xls(0, iLineaArticulo + 1))
        iLineaAtributo = iLineaArticulo + 2
        For j = 1 To iNumAtribs
            iTipoDato = CInt(xls(4, iLineaAtributo))
            iTipoIntroduccion = CInt(xls(11, iLineaAtributo))
            Select Case iTipoDato
                Case 1  'String
                    If (xls(14, iLineaAtributo) = "-") Then
                        oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                    Else
                        oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CStr(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                    End If
                Case 2  'Num�rico
                    If (xls(14, iLineaAtributo) = "-") Then
                        If iTipoIntroduccion = 1 Then 'Selecci�n
                            oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                        Else                          'Libre
                            If Not IsNull(StrToNull(xls(12, iLineaAtributo))) And Not IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dblMin = CDbl(StrToNull(xls(12, iLineaAtributo)))
                                dblMax = CDbl(StrToNull(xls(13, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), dblMin, dblMax
                            ElseIf IsNull(StrToNull(xls(12, iLineaAtributo))) And Not IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dblMax = CDbl(StrToNull(xls(13, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), Null, dblMax
                            ElseIf Not IsNull(StrToNull(xls(12, iLineaAtributo))) And IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dblMin = CDbl(StrToNull(xls(12, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), dblMin, Null
                            ElseIf IsNull(StrToNull(xls(12, iLineaAtributo))) And IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                            End If
                        End If
                    Else
                        If iTipoIntroduccion = 1 Then 'Selecci�n
                            oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDbl(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                        Else                          'Libre
                            If Not IsNull(StrToNull(xls(12, iLineaAtributo))) And Not IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dblMin = CDbl(StrToNull(xls(12, iLineaAtributo)))
                                dblMax = CDbl(StrToNull(xls(13, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDbl(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), dblMin, dblMax
                            ElseIf IsNull(StrToNull(xls(12, iLineaAtributo))) And Not IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dblMax = CDbl(StrToNull(xls(13, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDbl(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), Null, dblMax
                            ElseIf Not IsNull(StrToNull(xls(12, iLineaAtributo))) And IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dblMin = CDbl(StrToNull(xls(12, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDbl(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), dblMin, Null
                            ElseIf IsNull(StrToNull(xls(12, iLineaAtributo))) And IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDbl(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                            End If
                        End If
                    End If
                Case 3  'Fecha
                    If (xls(14, iLineaAtributo) = "-") Then
                        If iTipoIntroduccion = 1 Then 'Selecci�n
                            oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                        Else                          'Libre
                            If Not IsNull(StrToNull(xls(12, iLineaAtributo))) And Not IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dtMin = CDate(StrToNull(xls(12, iLineaAtributo)))
                                dtMax = CDate(StrToNull(xls(13, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), dtMin, dtMax
                            ElseIf IsNull(StrToNull(xls(12, iLineaAtributo))) And Not IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dtMax = CDate(StrToNull(xls(13, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), Null, dtMax
                            ElseIf Not IsNull(StrToNull(xls(12, iLineaAtributo))) And IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dtMin = CDate(StrToNull(xls(12, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), dtMin, Null
                            ElseIf IsNull(StrToNull(xls(12, iLineaAtributo))) And IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                            End If
                        End If
                    Else
                        If iTipoIntroduccion = 1 Then 'Selecci�n
                            oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDate(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                        Else                          'Libre
                            If Not IsNull(StrToNull(xls(12, iLineaAtributo))) And Not IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dtMin = CDate(StrToNull(xls(12, iLineaAtributo)))
                                dtMax = CDate(StrToNull(xls(13, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDate(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), dtMin, dtMax
                            ElseIf IsNull(StrToNull(xls(12, iLineaAtributo))) And Not IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dtMax = CDate(StrToNull(xls(13, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDate(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), Null, dtMax
                            ElseIf Not IsNull(StrToNull(xls(12, iLineaAtributo))) And IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                dtMin = CDate(StrToNull(xls(12, iLineaAtributo)))
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDate(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), dtMin, Null
                            ElseIf IsNull(StrToNull(xls(12, iLineaAtributo))) And IsNull(StrToNull(xls(13, iLineaAtributo))) Then
                                oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , CDate(xls(10, iLineaAtributo)), , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                            End If
                        End If
                    End If
                Case 4  'Boolean
                    If (xls(14, iLineaAtributo) = "-") Then
                        oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , Null, , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                    Else
                        If CStr(xls(10, iLineaAtributo)) = sTrue Then
                            oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , True, , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                        Else
                            oAtribs.Add CLng(xls(1, iLineaAtributo)), CStr(xls(2, iLineaAtributo)), CStr(xls(3, iLineaAtributo)), CInt(xls(4, iLineaAtributo)), , CStr(xls(5, iLineaAtributo)), CStr(xls(6, iLineaAtributo)), CStr(xls(7, iLineaAtributo)), CStr(xls(8, iLineaAtributo)), , CStr(xls(9, iLineaAtributo)), , False, , , , , , , CInt(xls(11, iLineaAtributo)), Null, Null
                        End If
                    End If
            End Select
            iLineaAtributo = iLineaAtributo + 1
        Next j
        Set oArticulos.Item(i).ATRIBUTOS = oAtribs
        Set oAtribs = Nothing
    Next i
    
    frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    frmESPERA.lblDetalle = sGuardandoBD
    
    
    Set oProves = oFSGSRaiz.generar_CProveedor
    oProves.Cod = scodProve
    oProves.Den = sDenProve

    'Guarda en BD
    teserror = oProves.GuardarDatosExternosArticulos(oArticulos)

    If teserror.NumError <> TESnoerror Then
        Unload frmESPERA
        basErrores.TratarError teserror
        Set oArticulos = Nothing
        Set oAtribs = Nothing
        Set oProves = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    'Actualiza el hist�rico
    If oUsuarioSummit.Persona Is Nothing Then
        oProves.AnyadirSolicitudOActualizacion 1
    Else
        oProves.AnyadirSolicitudOActualizacion 1, oUsuarioSummit.Persona.Cod
    End If
    
    basSeguridad.RegistrarAccion accionessummit.ACCArticDatExtActualizar, "Proveedor:" & oProves.Cod

    'Desconecta todos los objetos
    Set oArticulos = Nothing
    Set oAtribs = Nothing
    
    Screen.MousePointer = vbNormal

    'Cierra el formulario de carga
    frmCatalogoDatosExt.CargarNuevosAtributosProv oProves, sGmn1, sGMN2, sGMN3, sGMN4
    Set oProves = Nothing
    
    Unload frmESPERA
    
    'Unload Me

    Exit Sub

Error:

    If err.Number = 1004 Then
        Resume Next
    Else
        MsgBox err.Description
''        If Err.Number = 7 Then
''            Unload frmESPERA
''            Screen.MousePointer = vbNormal
''            appexcel.quit
''            Exit Sub
''        End If
    End If
    Unload frmESPERA
    Screen.MousePointer = vbNormal
    If bExcelOpened = True Then
        appexcel.Quit
        Set appexcel = Nothing
    End If
End Sub

Private Function DevolverLineaArticulo(xls As Variant, ByVal iNumArt As Integer) As Integer
'*********************************************************************************************
'*** Descripci�n: Dado el array que contiene la informaci�n para cargar los datos desde la ***
'***              hoja excel y un n�mero de art�culo, la funci�n devuelve el n�mero de     ***
'***              de fila en el array d�nde se encuentran los datos asociados a dicho      ***
'***              art�culo.                                                                ***
'***                                                                                       ***
'*** Par�metros : xls ::> de tipo variant, ser� el array que contiene todos los datos      ***
'***                      necesarios para cargar los datos externos desde una hoja excel.  ***
'***              iNumArt ::> de tipo entero, contiene el n�mero de art�culo del cu�l      ***
'***                          quiere conocerse el n�mero de l�nea en el array d�nde se     ***
'***                          encuentran sus datos asociados.                              ***
'***                                                                                       ***
'*** Valor que devuelve: un entero que ser� el n�mero de fila en el array d�nde se         ***
'***                     encuentran los datos relacionados con el art�culo a buscar.       ***
'*********************************************************************************************
    Dim i As Integer
    
    For i = 0 To UBound(xls, 2)
        If xls(0, i) = "<ART-" & iNumArt & ">" Then
            DevolverLineaArticulo = i
            Exit Function
        End If
    Next i
    DevolverLineaArticulo = -1
End Function

Private Sub Form_Unload(Cancel As Integer)
Set oFos = Nothing
End Sub
