VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstMAT 
   Caption         =   "Listado de materiales (Opciones)"
   ClientHeight    =   5985
   ClientLeft      =   2400
   ClientTop       =   1410
   ClientWidth     =   8340
   Icon            =   "frmLstMAT.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5985
   ScaleWidth      =   8340
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   8340
      TabIndex        =   9
      Top             =   5610
      Width           =   8340
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6840
         TabIndex        =   6
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5445
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   8325
      _ExtentX        =   14684
      _ExtentY        =   9604
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstMAT.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Timer1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstMAT.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTipoPer"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Opciones"
      TabPicture(2)   =   "frmLstMAT.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraTipoOrg"
      Tab(2).ControlCount=   1
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   4680
         Top             =   120
      End
      Begin VB.Frame fraTipoPer 
         Height          =   1185
         Left            =   -74850
         TabIndex        =   12
         Top             =   480
         Width           =   5250
         Begin VB.OptionButton optOrdDen 
            Caption         =   "Denominaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   240
            TabIndex        =   3
            Top             =   735
            Width           =   4740
         End
         Begin VB.OptionButton optOrdCod 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   240
            TabIndex        =   2
            Top             =   360
            Value           =   -1  'True
            Width           =   4215
         End
      End
      Begin VB.Frame Frame2 
         Height          =   4965
         Left            =   165
         TabIndex        =   10
         Top             =   330
         Width           =   8010
         Begin VB.CheckBox chkSoloCentrales 
            Caption         =   "dS�lo art�culos Centrales"
            Height          =   375
            Left            =   2760
            TabIndex        =   41
            Top             =   1200
            Width           =   2295
         End
         Begin VB.CommandButton cmdLimpiarUon 
            Height          =   285
            Left            =   7170
            Picture         =   "frmLstMAT.frx":0D06
            Style           =   1  'Graphical
            TabIndex        =   39
            Top             =   735
            Width           =   315
         End
         Begin VB.CommandButton cmdBuscarUon 
            Height          =   285
            Left            =   7500
            Picture         =   "frmLstMAT.frx":0DAB
            Style           =   1  'Graphical
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   735
            Width           =   315
         End
         Begin VB.TextBox txtUonsSeleccionadas 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   4200
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   720
            Width           =   2895
         End
         Begin VB.CommandButton cmdBuscaAtrib 
            Height          =   285
            Left            =   7515
            Picture         =   "frmLstMAT.frx":0E17
            Style           =   1  'Graphical
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   1560
            Width           =   315
         End
         Begin VB.CheckBox chkArtGen 
            Caption         =   "S�lo art�culos gen�ricos"
            Height          =   375
            Left            =   120
            TabIndex        =   28
            Top             =   1200
            Width           =   4455
         End
         Begin VB.Frame fraRecepcion 
            BorderStyle     =   0  'None
            Caption         =   "Recepci�n"
            ForeColor       =   &H00000000&
            Height          =   1215
            Left            =   5340
            TabIndex        =   24
            Top             =   3600
            Width           =   2115
            Begin VB.CheckBox chkRecep 
               Caption         =   "Obligatoria"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   27
               Top             =   390
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               Caption         =   "No recepcionar"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   26
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               Caption         =   "Opcional"
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   2
               Left            =   120
               TabIndex        =   25
               Top             =   990
               Width           =   1935
            End
            Begin VB.Label lblRecepcion 
               Caption         =   "DRecepci�n"
               Height          =   255
               Left            =   120
               TabIndex        =   36
               Top             =   0
               Width           =   1215
            End
         End
         Begin VB.Frame fraAlmacen 
            BorderStyle     =   0  'None
            Caption         =   "Almacenamiento"
            ForeColor       =   &H00000000&
            Height          =   1215
            Left            =   2700
            TabIndex        =   20
            Top             =   3600
            Width           =   2115
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "Obligatorio"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   23
               Top             =   390
               Width           =   1485
            End
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "No almacenar"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   22
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "Opcional"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   21
               Top             =   990
               Width           =   1875
            End
            Begin VB.Label lblAlmacenamiento 
               Caption         =   "DAlmacenamiento"
               Height          =   375
               Left            =   120
               TabIndex        =   35
               Top             =   0
               Width           =   1575
            End
         End
         Begin VB.Frame fraConcepto 
            BorderStyle     =   0  'None
            Caption         =   "Concepto"
            ForeColor       =   &H00000000&
            Height          =   1215
            Left            =   60
            TabIndex        =   16
            Top             =   3600
            Width           =   2115
            Begin VB.CheckBox chkConcepto 
               Caption         =   "Gasto"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   19
               Top             =   390
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               Caption         =   "Inversi�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   18
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               Caption         =   "Gasto/Inversi�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   17
               Top             =   990
               Width           =   1935
            End
            Begin VB.Label lblConcepto 
               Caption         =   "DConcepto"
               Height          =   255
               Left            =   120
               TabIndex        =   34
               Top             =   0
               Width           =   975
            End
         End
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   7155
            Picture         =   "frmLstMAT.frx":0EA4
            Style           =   1  'Graphical
            TabIndex        =   14
            Top             =   270
            Width           =   315
         End
         Begin VB.CommandButton cmdSelMat 
            Height          =   315
            Left            =   7485
            Picture         =   "frmLstMAT.frx":0F49
            Style           =   1  'Graphical
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   270
            Width           =   345
         End
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4200
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   285
            Width           =   2895
         End
         Begin VB.OptionButton opTodos 
            Caption         =   "Listado completo"
            Height          =   195
            Left            =   120
            TabIndex        =   0
            Top             =   360
            Value           =   -1  'True
            Width           =   2190
         End
         Begin VB.OptionButton opPais 
            Caption         =   "Listar Rama:"
            Height          =   195
            Left            =   2400
            TabIndex        =   1
            Top             =   360
            Width           =   1590
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOper 
            Height          =   915
            Left            =   1560
            TabIndex        =   30
            Top             =   2040
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstMAT.frx":0FB5
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
            Height          =   915
            Left            =   3720
            TabIndex        =   31
            Top             =   2040
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstMAT.frx":0FD1
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
            Height          =   1455
            Left            =   120
            TabIndex        =   32
            Top             =   1920
            Width           =   7710
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   11
            stylesets.count =   3
            stylesets(0).Name=   "Yellow"
            stylesets(0).BackColor=   11862015
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstMAT.frx":0FED
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmLstMAT.frx":1009
            stylesets(2).Name=   "Header"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmLstMAT.frx":1025
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            HeadStyleSet    =   "Header"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   11
            Columns(0).Width=   979
            Columns(0).Name =   "USAR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(1).Width=   3201
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   4419
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   1058
            Columns(3).Caption=   "OPER"
            Columns(3).Name =   "OPER"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3889
            Columns(4).Caption=   "VALOR"
            Columns(4).Name =   "VALOR"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "INTRO"
            Columns(5).Name =   "INTRO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "IDTIPO"
            Columns(6).Name =   "IDTIPO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "ID_ATRIB"
            Columns(7).Name =   "ID_ATRIB"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "MAXIMO"
            Columns(8).Name =   "MAXIMO"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MINIMO"
            Columns(9).Name =   "MINIMO"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "VALOR_ATRIB"
            Columns(10).Name=   "VALOR_ATRIB"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            _ExtentX        =   13600
            _ExtentY        =   2566
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblUons 
            Caption         =   "DUnidades Org."
            Height          =   255
            Left            =   2400
            TabIndex        =   40
            Top             =   720
            Width           =   1695
         End
         Begin VB.Label lblBusqAtrib 
            Caption         =   "B�squeda por atributos"
            Height          =   255
            Left            =   120
            TabIndex        =   33
            Top             =   1680
            Width           =   1935
         End
      End
      Begin VB.Frame fraTipoOrg 
         Height          =   1740
         Left            =   -74850
         TabIndex        =   8
         Top             =   375
         Width           =   5250
         Begin VB.CheckBox chkIncluirArtPlanta 
            Caption         =   "dIncluir art�culos de planta"
            Height          =   255
            Left            =   360
            TabIndex        =   42
            Top             =   1320
            Width           =   2295
         End
         Begin VB.CheckBox chkIncluirEspecificaciones 
            Caption         =   "Incluir especificaciones de art�culos"
            Height          =   195
            Left            =   360
            TabIndex        =   15
            Top             =   1005
            Width           =   4095
         End
         Begin VB.CheckBox chkIncluirDetalleItems 
            Caption         =   "Incluir detalles art�culos"
            Height          =   255
            Left            =   360
            TabIndex        =   5
            Top             =   630
            Width           =   4485
         End
         Begin VB.CheckBox chkIncluirItems 
            Caption         =   "Incluir art�culos"
            Height          =   195
            Left            =   360
            TabIndex        =   4
            Top             =   300
            Width           =   4485
         End
      End
   End
End
Attribute VB_Name = "frmLstMAT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnEspacio As Long = 120

Private bRMat As Boolean ' Aplicar restricci�n de material a comprador
' variables para seleccion de una rama
Private sGMN1Cod As String
Private sGMN2Cod As String
Private sGMN3Cod As String
Private sGMN4Cod As String
'Variables de idioma
Private sIdiSel1 As String
Private sIdiSel2 As String
Private sEspera(1 To 3) As String
Private txtTitulo As String
Private txtPag As String
Private txtDe As String
Private txtSeleccion As String
Private txtArt As String
Private txtUni As String
Private txtCant As String
Private txtGenerico As String
Private txtPorImporte As String
Private txtPorCantidad As String
Private txtCantModificable As String
Private txtCantNoModificable As String
Private sIdiEspecific As String
Private txtGasto As String
Private txtInversion As String
Private txtGastoInversion As String
Private txtNoAlmacenable As String
Private txtAlmacenamientoObligatorio As String
Private txtAlmacenamientoOpcional As String
Private txtNoRecepcionable As String
Private txtRecepcionObligatoria As String
Private txtRecepcionOpcional As String
Private txtTipoRecepcion As String


Public g_oADORes As Ador.Recordset
Private m_sConcep As String
Private m_sAlmac As String
Private m_sRecep As String
Private m_sGenerico As String
Private arOper As Variant
Private msTrue As String
Private msFalse As String
Private msMsgMinMax As String
Private m_oUonsSeleccionadas As CUnidadesOrganizativas
Private m_bRestrMantUsu As Boolean
Private m_bRestrMantPerf As Boolean
Private m_oAtributos As CAtributos


Public Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonUsu)) Is Nothing) Then
        m_bRestrMantUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonPerf)) Is Nothing) Then
        m_bRestrMantPerf = True
    End If
End Sub

''' <summary>Muestra en pantalla el material seleccionado</summary>
''' <param name="sOrigen">Formulario origen</param>
''' <remarks>Llamada desde:Evento; Tiempo m�ximo > 2seg </remarks>

Public Sub PonerMatSeleccionado(sOrigen As String)
    sGMN1Cod = vbNullString
    sGMN2Cod = vbNullString
    sGMN3Cod = vbNullString
    sGMN4Cod = vbNullString
    
    If sOrigen = "frmESTRMAT" Then
        If Not frmESTRMAT.g_oGMN1Seleccionado Is Nothing Then sGMN1Cod = frmESTRMAT.g_oGMN1Seleccionado.Cod
        If Not frmESTRMAT.g_oGMN2Seleccionado Is Nothing Then sGMN2Cod = frmESTRMAT.g_oGMN2Seleccionado.Cod
        If Not frmESTRMAT.g_oGMN3Seleccionado Is Nothing Then sGMN3Cod = frmESTRMAT.g_oGMN3Seleccionado.Cod
        If Not frmESTRMAT.g_oGMN4Seleccionado Is Nothing Then sGMN4Cod = frmESTRMAT.g_oGMN4Seleccionado.Cod
    Else
        If Not frmSELMAT.oGMN1Seleccionado Is Nothing Then sGMN1Cod = frmSELMAT.oGMN1Seleccionado.Cod
        If Not frmSELMAT.oGMN2Seleccionado Is Nothing Then sGMN2Cod = frmSELMAT.oGMN2Seleccionado.Cod
        If Not frmSELMAT.oGMN3Seleccionado Is Nothing Then sGMN3Cod = frmSELMAT.oGMN3Seleccionado.Cod
        If Not frmSELMAT.oGMN4Seleccionado Is Nothing Then sGMN4Cod = frmSELMAT.oGMN4Seleccionado.Cod
    End If
    
    txtEstMat.Text = ""
    
    If sGMN1Cod <> vbNullString Then
         txtEstMat.Text = sGMN1Cod
    End If
        
    If sGMN2Cod <> vbNullString Then
        txtEstMat.Text = txtEstMat.Text & " - " & sGMN2Cod
    End If
        
    If sGMN3Cod <> vbNullString Then
        txtEstMat.Text = txtEstMat.Text & " - " & sGMN3Cod
    End If
        
    If sGMN4Cod <> vbNullString Then
        txtEstMat.Text = txtEstMat.Text & " - " & sGMN4Cod
    End If
End Sub

Private Sub chkIncluirItems_Click()
    If chkIncluirItems.Value = vbChecked Then
        chkIncluirDetalleItems.Enabled = True
        chkIncluirEspecificaciones.Enabled = True
        Me.chkIncluirArtPlanta.Enabled = True
    Else
        chkIncluirDetalleItems.Value = vbUnchecked
        chkIncluirDetalleItems.Enabled = False
        chkIncluirEspecificaciones = vbUnchecked
        chkIncluirEspecificaciones.Enabled = False
        Me.chkIncluirArtPlanta = vbUnchecked
        Me.chkIncluirArtPlanta.Enabled = False
    End If
End Sub

Private Sub cmdBorrar_Click()
    sGMN1Cod = vbNullString
    sGMN2Cod = vbNullString
    sGMN3Cod = vbNullString
    sGMN4Cod = vbNullString
    
    txtEstMat.Text = ""
End Sub

Private Sub cmdBuscaAtrib_Click()
    Dim ofrmATRIB As frmAtribMod

    Set ofrmATRIB = New frmAtribMod

    ofrmATRIB.g_sOrigen = "frmLstMAT"

    ofrmATRIB.g_sGmn1 = sGMN1Cod
    ofrmATRIB.g_sGmn2 = sGMN2Cod
    ofrmATRIB.g_sGmn3 = sGMN3Cod
    ofrmATRIB.g_sGmn4 = sGMN4Cod

    ofrmATRIB.sstabGeneral.Tab = 0
    ofrmATRIB.g_GMN1RespetarCombo = True

    ofrmATRIB.sdbcGMN1_4Cod.Text = sGMN1Cod

    If sGMN1Cod & sGMN2Cod & sGMN3Cod & sGMN4Cod = vbNullString Then
        ofrmATRIB.sdbcGMN1_4Cod_Validate False
        ofrmATRIB.sdbcGMN2_4Cod_Validate False
        ofrmATRIB.sdbcGMN3_4Cod_Validate False
        ofrmATRIB.sdbcGMN4_4Cod_Validate False
    Else
        If sGMN1Cod <> vbNullString Then
            ofrmATRIB.sdbcGMN1_4Cod.Text = sGMN1Cod
            ofrmATRIB.sdbcGMN1_4Cod_Validate False
        End If
        ofrmATRIB.g_GMN1RespetarCombo = False
                
        If sGMN2Cod <> vbNullString Then
            ofrmATRIB.g_GMN2RespetarCombo = True
            ofrmATRIB.sdbcGMN2_4Cod.Text = sGMN2Cod
            ofrmATRIB.sdbcGMN2_4Cod_Validate False
        End If
        ofrmATRIB.g_GMN2RespetarCombo = False
                
        If sGMN3Cod <> vbNullString Then
            ofrmATRIB.g_GMN3RespetarCombo = True
            ofrmATRIB.sdbcGMN3_4Cod.Text = sGMN3Cod
            ofrmATRIB.sdbcGMN3_4Cod_Validate False
        End If
        ofrmATRIB.g_GMN3RespetarCombo = False
                
        If sGMN4Cod <> vbNullString Then
            ofrmATRIB.g_GMN4RespetarCombo = True
            ofrmATRIB.sdbcGMN4_4Cod.Text = sGMN4Cod
            ofrmATRIB.sdbcGMN4_4Cod_Validate False
        End If
        ofrmATRIB.g_GMN4RespetarCombo = False
    End If

    ofrmATRIB.cmdSeleccionar.Visible = True

    ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
    ofrmATRIB.g_bSoloSeleccion = True

    ofrmATRIB.Show vbModal
End Sub

Private Sub cmdBuscarUon_Click()
    '''Cargar unidades organizativas en funci�n de las restricciones
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    

    Dim frm As frmSELUO
    Set frm = New frmSELUO
    frm.multiselect = True
    frm.CheckChildren = False
    
    oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.perfil.Id, , , False
    oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.perfil.Id, , , False
    oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.perfil.Id, , False

    frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
    
    If Not m_oUonsSeleccionadas Is Nothing Then
        Set frm.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    
    frm.Show vbModal
    If frm.Aceptar Then
        Me.txtUonsSeleccionadas.Text = frm.UonsSeleccionadas.titulo
        Set m_oUonsSeleccionadas = frm.UonsSeleccionadas
    End If
    
    Set frm = Nothing
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    CargarAtributos
End Sub

Private Sub cmdLimpiarUon_Click()
    Me.txtUonsSeleccionadas.Text = ""
    m_oUonsSeleccionadas.clear
End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmLstMAT"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Show 1
End Sub

'''<summary>Carga los idiomas del formulario</summary>
'''<remarks>Tiempo m�ximo: >2 seg</remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTMAT, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value     '2
        Ador.MoveNext
        SSTab1.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        opTodos.caption = Ador(0).Value '5
        Ador.MoveNext
        opPais.caption = Ador(0).Value
        Ador.MoveNext
        frmLstMAT.caption = Ador(0).Value
        Ador.MoveNext
        optOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        optOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        chkIncluirItems.caption = Ador(0).Value '10
        Ador.MoveNext
        chkIncluirDetalleItems.caption = Ador(0).Value
        Ador.MoveNext
        sIdiSel1 = Ador(0).Value
        Ador.MoveNext
        sIdiSel2 = Ador(0).Value
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        'Idiomas del RPT
        Ador.MoveNext
        txtTitulo = Ador(0).Value '200
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtSeleccion = Ador(0).Value
        Ador.MoveNext
        txtArt = Ador(0).Value
        Ador.MoveNext
        txtUni = Ador(0).Value
        Ador.MoveNext
        txtCant = Ador(0).Value '& " " & Year(Date)
        Ador.MoveNext
        chkIncluirEspecificaciones.caption = Ador(0).Value
        Ador.MoveNext
        sIdiEspecific = Ador(0).Value
        Ador.MoveNext
        txtGenerico = Ador(0).Value
        Ador.MoveNext
        fraConcepto.caption = Ador(0).Value
        lblConcepto.caption = Ador(0).Value
        Ador.MoveNext
        fraAlmacen.caption = Ador(0).Value
        lblAlmacenamiento.caption = Ador(0).Value
        Ador.MoveNext
        fraRecepcion.caption = Ador(0).Value
        lblRecepcion.caption = Ador(0).Value
        Ador.MoveNext
        chkConcepto(0).caption = Ador(0).Value
        Ador.MoveNext
        chkConcepto(1).caption = Ador(0).Value
        Ador.MoveNext
        chkConcepto(2).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(1).caption = Ador(0).Value
        chkRecep(1).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(0).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(2).caption = Ador(0).Value
        chkRecep(2).caption = Ador(0).Value
        Ador.MoveNext
        chkRecep(0).caption = Ador(0).Value
        Ador.MoveNext
        'Fecha Fin
        Ador.MoveNext
        txtGasto = Ador(0).Value
        Ador.MoveNext
        txtInversion = Ador(0).Value
        Ador.MoveNext
        txtGastoInversion = Ador(0).Value
        Ador.MoveNext
        txtNoAlmacenable = Ador(0).Value
        Ador.MoveNext
        txtAlmacenamientoObligatorio = Ador(0).Value
        Ador.MoveNext
        txtAlmacenamientoOpcional = Ador(0).Value
        Ador.MoveNext
        txtNoRecepcionable = Ador(0).Value
        Ador.MoveNext
        txtRecepcionObligatoria = Ador(0).Value
        Ador.MoveNext
        txtRecepcionOpcional = Ador(0).Value
        Ador.MoveNext
        lblBusqAtrib.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgAtributos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("VALOR").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("OPER").caption = Ador(0).Value
        Ador.MoveNext
        msTrue = Ador(0).Value
        Ador.MoveNext
        msFalse = Ador(0).Value
        Ador.MoveNext
        msMsgMinMax = Ador(0).Value
        Ador.MoveNext
        txtTipoRecepcion = Ador(0).Value
        Ador.MoveNext
        Me.lblUons.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkSoloCentrales.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkIncluirArtPlanta.caption = Ador(0).Value
        Ador.MoveNext
        txtPorImporte = Ador(0).Value
        Ador.MoveNext
        txtPorCantidad = Ador(0).Value
        Ador.MoveNext
        txtCantModificable = Ador(0).Value
        Ador.MoveNext
        txtCantNoModificable = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
Private Sub Form_Load()

    'Me.Width = 5700
    'Me.Height = 2790
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    PonerFieldSeparator Me
    CargarComboOperandos
    ConfigurarSeguridad
    
    txtEstMat.Enabled = False
    
    If chkIncluirItems.Value = vbUnchecked Then
        chkIncluirDetalleItems.Enabled = False
        chkIncluirEspecificaciones.Enabled = False
        Me.chkIncluirArtPlanta.Enabled = False
    End If
    If opTodos.Value = True Then
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    End If
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATRestMatComp)) Is Nothing) Then
            bRMat = True
        End If
    End If
    
    chkArtGen.Visible = gParametrosGenerales.gbArticulosGenericos
    Me.chkSoloCentrales.Visible = gParametrosGenerales.gbArticulosCentrales
    Me.chkIncluirArtPlanta.Visible = gParametrosGenerales.gbArticulosCentrales
    Set Me.UonsSeleccionadas = m_oUonsSeleccionadas
    
End Sub

''' <summary>Carga el combo de operandos</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarComboOperandos()
    Dim i As Integer
    
    arOper = Array("=", ">", ">=", "<", "<=")
    
    sdbddOper.RemoveAll
    
    sdbddOper.AddItem ""
    For i = 0 To UBound(arOper)
        sdbddOper.AddItem arOper(i) & Chr(m_lSeparador) & arOper(i)
    Next
End Sub

''' <summary>Carga el grid de atributos con los atributos de material</summary>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Sub CargarAtributos()
    Dim oatrib As CAtributo
    Dim oGMN As Object
    Dim bAddAtrib As Boolean
    
    Set m_oAtributos = Nothing
    
    sdbgAtributos.RemoveAll
    
    If sGMN4Cod <> vbNullString Then
        Set oGMN = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGMN.GMN1Cod = sGMN1Cod
        oGMN.GMN2Cod = sGMN2Cod
        oGMN.GMN3Cod = sGMN3Cod
        oGMN.Cod = sGMN4Cod
    ElseIf sGMN3Cod <> vbNullString Then
        Set oGMN = oFSGSRaiz.generar_CGrupoMatNivel3
        oGMN.GMN1Cod = sGMN1Cod
        oGMN.GMN2Cod = sGMN2Cod
        oGMN.Cod = sGMN3Cod
    ElseIf sGMN2Cod <> vbNullString Then
        Set oGMN = oFSGSRaiz.generar_CGrupoMatNivel2
        oGMN.GMN1Cod = sGMN1Cod
        oGMN.Cod = sGMN2Cod
    ElseIf sGMN1Cod <> vbNullString Then
        Set oGMN = oFSGSRaiz.generar_CGrupoMatNivel1
        oGMN.Cod = sGMN1Cod
    Else
        Set oGMN = oFSGSRaiz.Generar_CGrupoMatNivel4
    End If
    Set m_oAtributos = oGMN.DevolverAtribMatYArtMat(oUons:=m_oUonsSeleccionadas)
    Set oGMN = Nothing
        
    If Not m_oAtributos Is Nothing Then
        If m_oAtributos.Count > 0 Then
            With sdbgAtributos
                For Each oatrib In m_oAtributos
                    .AddItem "0" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo
                Next
            End With
        End If
    End If
    
    Set oatrib = Nothing
End Sub

''' <summary>Obtiene el listado de materiales
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:Evento; Tiempo m�ximo > 2seg </remarks>
''' <remarks>Revisado por: LTG  Fecha: 16/08/2011</remarks>

Private Sub cmdObtener_Click()
    Dim RepPath As String
    Dim oReport As CRAXDRT.Report
    Dim oFos As FileSystemObject
    Dim SubListado As CRAXDRT.Report
    ' ARRAY SelectionText, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula
    Dim SelectionText(1 To 7, 1 To 8) As String
    Dim oCRMateriales As New CRMateriales
    Dim pv As Preview
    Dim sCodComp As String
    Dim sCodEqp As String
    Dim i As Integer
    Dim oAtributos As CAtributos
    Dim iNumAtrib As Integer
    Dim vBmk As Variant

    Dim iPerfil As Integer
    If Not oUsuarioSummit.perfil Is Nothing Then
        iPerfil = oUsuarioSummit.perfil.Id
    Else
        iPerfil = 0
    End If
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptMAT.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    ConectarReport oReport, crs_Server, crs_Database, crs_User, crs_Password
    ' FORMULA FIELDS A NIVEL DE REPORT
    SelectionText(2, 1) = "SEL"
    SelectionText(2, 2) = "OcultarItem" 'Ocultar Items
    SelectionText(2, 3) = "OcultarCampos" 'Ocultar detalles Items
    SelectionText(2, 8) = "OcultarEspecificaciones" 'Ocultar especificaciones art�culos
    
    If chkIncluirItems = vbChecked Then
        SelectionText(1, 2) = "N"
        If chkIncluirDetalleItems = vbChecked Then
            SelectionText(1, 3) = "N"
        Else
            SelectionText(1, 3) = "S"
        End If
        If chkIncluirEspecificaciones = vbChecked Then
            SelectionText(1, 8) = "N"
        Else
            SelectionText(1, 8) = "S"
        End If
    Else
        SelectionText(1, 2) = "S"
        SelectionText(1, 3) = "S"
    End If
        
    m_sConcep = ""
    For i = 0 To 2
        If chkConcepto(i).Value = vbChecked Then
            m_sConcep = m_sConcep & "1"
        Else
            m_sConcep = m_sConcep & "0"
        End If
    Next
        
    m_sAlmac = ""
    For i = 0 To 2
        If chkAlmacen(i).Value = vbChecked Then
            m_sAlmac = m_sAlmac & "1"
        Else
            m_sAlmac = m_sAlmac & "0"
        End If
    Next
    
    m_sRecep = ""
    For i = 0 To 2
        If chkRecep(i).Value = vbChecked Then
            m_sRecep = m_sRecep & "1"
        Else
            m_sRecep = m_sRecep & "0"
        End If
    Next
    
    'Atributos
    If sdbgAtributos.Rows > 0 Then
        Dim oAtributo As CAtributo
        
        Set oAtributos = oFSGSRaiz.Generar_CAtributos
        
        With sdbgAtributos
            .Update
            
            iNumAtrib = 0
            Do While iNumAtrib < .Rows
                vBmk = .GetBookmark(iNumAtrib)
                If .Columns("USAR").CellValue(vBmk) Then
                    If .Columns("VALOR").CellValue(vBmk) <> "" Then
                        Select Case .Columns("IDTIPO").CellValue(vBmk)
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                                    Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk), valor_text:=Replace(.Columns("VALOR").CellValue(vBmk), "*", "%"))
                                End If
                            Case TiposDeAtributos.TipoNumerico
                                If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                                    Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk), valor_num:=.Columns("VALOR").CellValue(vBmk), Formula:=.Columns("OPER").CellValue(vBmk))
                                End If
                            Case TiposDeAtributos.TipoFecha
                                If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                                    Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk), valor_fec:=.Columns("VALOR").CellValue(vBmk))
                                End If
                            Case TiposDeAtributos.TipoBoolean
                                If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                                    Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk), valor_bool:=.Columns("VALOR_ATRIB").CellValue(vBmk))
                                End If
                        End Select
                    Else
                        If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                            Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk))
                        End If
                    End If
                    oAtributo.AmbitoAtributo = m_oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)).AmbitoAtributo
                End If
                iNumAtrib = iNumAtrib + 1
            Loop
        End With
        Set oAtributo = Nothing
    End If
        
    'NOMBRES ESTRUCTURA MATERIALES
    SelectionText(2, 4) = "GMN1NOM"
    SelectionText(1, 4) = gParametrosGenerales.gsDEN_GMN1 & ":"
    SelectionText(2, 5) = "GMN2NOM"
    SelectionText(1, 5) = gParametrosGenerales.gsDEN_GMN2 & ":"
    SelectionText(2, 6) = "GMN3NOM"
    SelectionText(1, 6) = gParametrosGenerales.gsDEN_GMN3 & ":"
    SelectionText(2, 7) = "GMN4NOM"
    SelectionText(1, 7) = " " & gParametrosGenerales.gsDEN_GMN4 & ":"
    
    If opTodos = True Then
        SelectionText(1, 1) = sIdiSel1
    Else
        SelectionText(1, 1) = sIdiSel2 & " " & txtEstMat
    End If
    ' Formula Fields
    
    For i = 1 To UBound(SelectionText, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionText(2, i))).Text = """" & SelectionText(1, i) & """"
    Next i
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & txtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & txtSeleccion & """"
    
    ' PARAMETROS REPORT, llamada STORED PROCEDURE
    sCodComp = ""
    sCodEqp = ""
    If bRMat Then
        sCodComp = oUsuarioSummit.comprador.Cod
        sCodEqp = oUsuarioSummit.comprador.codEqp
    End If
    
    If opTodos Then
        sGMN4Cod = ""
        sGMN3Cod = ""
        sGMN2Cod = ""
        sGMN1Cod = ""
    End If

    'Sub report articulos de planta
    Set SubListado = oReport.OpenSubreport("rptArtArt")
    ConectarReport SubListado, crs_Server, crs_Database, crs_User, crs_Password
    
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArtPlanta")).Text = """" & gParametrosGenerales.gsArticulosPlanta & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArticulo")).Text = """" & txtArt & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnidad")).Text = """" & txtUni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGEN")).Text = """" & txtGenerico & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTipoRecepcion")).Text = """" & txtTipoRecepcion & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtArt")).Text = """" & txtArt & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUni")).Text = """" & txtUni & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCant")).Text = """" & txtCant & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "mostrarArtPlanta")).Text = """" & Me.chkIncluirArtPlanta.Value & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipoRecepcion")).Text = """" & txtTipoRecepcion & """"
        
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEspecific")).Text = """" & sIdiEspecific & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGasto")).Text = """" & txtGasto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtInversion")).Text = """" & txtInversion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGastoInversion")).Text = """" & txtGastoInversion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNoAlmacenable")).Text = """" & txtNoAlmacenable & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAlmacenamientoObligatorio")).Text = """" & txtAlmacenamientoObligatorio & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAlmacenamientoOpcional")).Text = """" & txtAlmacenamientoOpcional & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNoRecepcionable")).Text = """" & txtNoRecepcionable & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRecepcionObligatoria")).Text = """" & txtRecepcionObligatoria & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRecepcionOpcional")).Text = """" & txtRecepcionOpcional & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "sConcep")).Text = """" & m_sConcep & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "sAlmac")).Text = """" & m_sAlmac & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "sRecep")).Text = """" & m_sRecep & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGenerico")).Text = """" & txtGenerico & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPorImporte")).Text = """" & txtPorImporte & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPorCantidad")).Text = """" & txtPorCantidad & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCantModificable")).Text = """" & txtCantModificable & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCantNoModificable")).Text = """" & txtCantNoModificable & """"
    
    If gParametrosGenerales.gbArticulosGenericos = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGenerico")).Text = """" & txtGenerico & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGenerico")).Text = """" & "" & """"
    End If
    
    
    Set SubListado = Nothing
    
    '********** REPORT ***************
    Set g_oADORes = oGestorInformes.ListadoMATERIALES(sCodEqp, sCodComp, optOrdDen, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, oAtributos, Me.chkIncluirItems.Value, Me.chkIncluirArtPlanta, Me.chkSoloCentrales)
    
    Dim bListado As Boolean
    If Not g_oADORes Is Nothing Then
        If Not g_oADORes.EOF Then
            oReport.Database.SetDataSource g_oADORes
            bListado = True
        Else
            bListado = False
        End If
    Else
        bListado = False
    End If
    
    If Not bListado Then
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        'oCRMateriales.Listado oGestorInformes, oReport, sCodComp, sCodEqp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, chkIncluirItems, optOrdDen, chkArtGen, oAtributos, IIf(Me.chkSoloCentrales.Value = 1, True, False), m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_oUonsSeleccionadas, False, False
        
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    DoEvents
    Me.Hide
    frmESPERA.Show
    frmESPERA.lblGeneral.caption = sEspera(1)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    
    Set pv = New Preview
    pv.Hide
    pv.caption = txtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.g_sOrigen = "frmLstMAT"
    
    Timer1.Enabled = True
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Set oReport = Nothing
    Unload Me
    DoEvents
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>Coloca los controles en el formulario en funci�n del tama�o de este</summary>
''' <remarks>Llamada desde: Form_Resize</remarks>

Private Sub Arrange()
    If Me.WindowState <> vbMinimized Then
        If Me.Height < 5325 Then Me.Height = 5325
        If Me.Width < 8460 Then Me.Width = 8460
        
        SSTab1.Width = Me.ScaleWidth - (cnEspacio / 2)
        SSTab1.Height = Me.ScaleHeight - Picture1.Height - (cnEspacio / 2)
        
        Picture1.Top = SSTab1.Top + SSTab1.Height + cnEspacio
        'Picture1.Width = Me.Width
        cmdObtener.Left = Picture1.Width - cmdObtener.Width - cnEspacio
        
        Frame2.Height = SSTab1.Height - (4 * cnEspacio)
        Frame2.Width = SSTab1.Width - (2 * cnEspacio)
        
        If chkArtGen.Visible Then
            lblBusqAtrib.Top = chkArtGen.Top + chkArtGen.Height + cnEspacio
        Else
            lblBusqAtrib.Top = opTodos.Top + opTodos.Height + (2 * cnEspacio)
        End If
        cmdBuscaAtrib.Top = lblBusqAtrib.Top - (cmdBuscaAtrib.Height - lblBusqAtrib.Height) - 40
        sdbgAtributos.Top = lblBusqAtrib.Top + lblBusqAtrib.Height
        
        sdbgAtributos.Width = Frame2.Width - (2 * cnEspacio)
        sdbgAtributos.Height = Frame2.Height - sdbgAtributos.Top - fraConcepto.Height - (2 * cnEspacio)
        fraConcepto.Top = Frame2.Height - fraConcepto.Height - cnEspacio
        fraAlmacen.Top = fraConcepto.Top
        fraRecepcion.Top = fraConcepto.Top

        sdbgAtributos.Columns("USAR").Width = 600
        sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width - 1200) * 0.22
        sdbgAtributos.Columns("DEN").Width = (sdbgAtributos.Width - 1200) * 0.42
        sdbgAtributos.Columns("OPER").Width = 600
        sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width - 1200) * 0.33
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
 
 sGMN1Cod = ""
 sGMN2Cod = ""
 sGMN3Cod = ""
 sGMN4Cod = ""

End Sub

Private Sub opPais_Click()

    If opPais.Value = True Then
        cmdSelMat.Enabled = True
        cmdBorrar.Enabled = True
    Else
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    End If
    
End Sub

Private Sub opTodos_Click()

    If opTodos.Value = True Then
        txtEstMat = ""
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    Else
        cmdSelMat.Enabled = True
        cmdBorrar.Enabled = True
    End If

End Sub

Private Sub sdbddOper_InitColumnProps()
    sdbddOper.DataFieldList = "Column 0"
    sdbddOper.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddOper_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddOper.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOper.Rows - 1
            bm = sdbddOper.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOper.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddOper_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    Dim i As Integer

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        
        ''' Comprobar la existencia en la lista
        For i = 0 To UBound(arOper)
            If arOper(i) = sdbgAtributos.Columns(sdbgAtributos.col).Text Then
                bExiste = True
                Exit For
            End If
        Next
        
        If Not bExiste Then
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.col).Text
            sdbgAtributos.Columns(sdbgAtributos.col).Text = ""
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    Dim oLista As CValoresPond
    Dim oElem As CValorPond

    sdbddValor.RemoveAll
    
    sdbddValor.AddItem ""

    iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

    Set oatrib = oFSGSRaiz.Generar_CAtributo
    Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)

    If Not oatrib Is Nothing Then
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            oatrib.CargarListaDeValores
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                sdbddValor.AddItem oElem.ValorLista & Chr(m_lSeparador) & oElem.ValorLista
            Next
            Set oLista = Nothing
        Else
            'Campo de tipo boolean:
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem msTrue & Chr(m_lSeparador) & msTrue
                sdbddValor.AddItem msFalse & Chr(m_lSeparador) & msFalse
            End If
        End If
    
        Set oatrib = Nothing
    End If
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

            Set oatrib = oFSGSRaiz.Generar_CAtributo
            Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
            
            If oatrib Is Nothing Then Exit Sub
            
            oatrib.CargarListaDeValores
            
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                If UCase(msTrue) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "1"
                    bExiste = True
                End If
                If UCase(msFalse) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "0"
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns(sdbgAtributos.col).Text = ""
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.col).caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbgatributos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    With sdbgAtributos
        Select Case .Columns(ColIndex).Name
            Case "VALOR"
                'Comprobar que el valor introducido se corresponde con el tipo de atributo
                If .Columns("VALOR").Text <> "" Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoString
                        Case TiposDeAtributos.TipoNumerico
                            If (Not IsNumeric(.Columns("VALOR").Value)) Then
                                MsgBox "El valor del campo Valor debe ser num�rico", vbInformation, "FULLSTEP"
                                Cancel = True
                            Else
                                If .Columns("OPER").Text <> "" And .Columns("OPER").Text = "=" Then
                                    If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                        Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                                    End If
                                End If
                            End If
                            
                            If .Columns("OPER").Text = "" Then
                                .Columns("OPER").Text = "="
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                                End If
                            End If
                        Case TiposDeAtributos.TipoFecha
                            If (Not IsDate(.Columns("VALOR").Text) And .Columns("VALOR").Value <> "") Then
                                MsgBox "El valor del campo Valor debe ser una fecha.", vbInformation, "FULLSTEP"
                                Cancel = True
                            Else
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    If CDate(.Columns("MINIMO").Value) > CDate(.Columns("VALOR").Value) Or CDate(.Columns("MAXIMO").Value) < CDate(.Columns("VALOR").Value) Then
                                        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", .Columns("MINIMO").Value), "@Valor2", .Columns("MAXIMO").Value), vbInformation, "FULLSTEP"
                                        Cancel = True
                                    End If
                                End If
                            End If
                        Case TiposDeAtributos.TipoBoolean
                    End Select
                End If
            Case "OPER"
                'Comprobar si se est� intentando dejar en blanco la col. del operando teniendo un valor introducido
                If .Columns("OPER").Text = vbNullString And .Columns("VALOR").Text <> vbNullString Then
                    Cancel = True
                ElseIf .Columns("OPER").Text = "=" And .Columns("VALOR").Text <> vbNullString Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoNumerico
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                            End If
                    End Select
                End If
        End Select
    End With
End Sub

''' <summary>Comprueba que un valor est� entre un m�nimo y un m�ximo</summary>
''' <remarks>Llamada desde: sdbgatributos_BeforeColUpdate</remarks>
''' <param name="dblValor">Valor</param>
''' <param name="dblMin">M�nimo</param>
''' <param name="dblMax">M�ximo</param>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Function ComprobarValoresMaxMin(ByVal dblValor As Double, ByVal dblMin As Double, ByVal dblMax As Double) As Boolean
    ComprobarValoresMaxMin = True
    
    If dblMin > dblValor Or dblMax < dblValor Then
        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", dblMin), "@Valor2", dblMax), vbInformation, "FULLSTEP"
        ComprobarValoresMaxMin = False
    End If
End Function

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bCargarCombo As Boolean
    
    'Combo de operandos
    If sdbgAtributos.Columns("IDTIPO").Value = TipoNumerico Then
        sdbgAtributos.Columns("OPER").DropDownHwnd = sdbddOper.hWnd
        sdbgAtributos.Columns("OPER").Locked = False
    Else
        sdbgAtributos.Columns("OPER").DropDownHwnd = 0
        sdbgAtributos.Columns("OPER").Locked = True
    End If
    
    'Combo de valores
    bCargarCombo = False
    If sdbgAtributos.col = sdbgAtributos.Columns("VALOR").Position Then
        If sdbgAtributos.Columns("INTRO").Value Then
            bCargarCombo = True
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TipoBoolean Then
                bCargarCombo = True
            End If
        End If
    End If
    
    If bCargarCombo Then
        sdbddValor.RemoveAll
        sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor_DropDown
    Else
        sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
    End If
End Sub

Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas

    Set UonsSeleccionadas = m_oUonsSeleccionadas

End Property

Public Property Set UonsSeleccionadas(oUonsSeleccionadas As CUnidadesOrganizativas)
    If Not oUonsSeleccionadas Is Nothing Then
        Set m_oUonsSeleccionadas = oUonsSeleccionadas
        Me.txtUonsSeleccionadas = m_oUonsSeleccionadas.titulo
    End If
End Property

Public Function addAtributo(ByRef oAtributo As CAtributo) As CAtributo
    'If Not oAtribs.Existe(oAtributo.Id) Then
    '    Set addAtributo = oAtribs.addAtributo(oAtributo)
    'End If
End Function
