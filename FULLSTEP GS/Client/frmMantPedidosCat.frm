VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMantPedidosCat 
   BackColor       =   &H00808000&
   Caption         =   "DSelecci�n de categor�as"
   ClientHeight    =   5100
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8025
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMantPedidosCat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5100
   ScaleWidth      =   8025
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   3765
      TabIndex        =   2
      Top             =   4560
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2580
      TabIndex        =   1
      Top             =   4560
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrCat 
      Height          =   4335
      Left            =   150
      TabIndex        =   0
      Top             =   120
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   7646
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidosCat.frx":0CB2
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidosCat.frx":0DC4
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidosCat.frx":0ED6
            Key             =   "RAIZ"
            Object.Tag             =   "RAIZ"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidosCat.frx":19A0
            Key             =   "CANDADO"
            Object.Tag             =   "CANDADO"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidosCat.frx":1DF2
            Key             =   "Baja"
            Object.Tag             =   "Baja"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidosCat.frx":2195
            Key             =   "Marcado"
            Object.Tag             =   "Marcado"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMantPedidosCat.frx":2532
            Key             =   "CANMarca"
            Object.Tag             =   "CANMarca"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmMantPedidosCat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oActsN1 As CCategoriaSN1
Private sIdioma(1) As String
Private sIdiCategoria As String
Private sIdiCerrar As String

Public g_lTipopedidoId As Long

'3328
Public g_bModoEdicion As Boolean
Private oTipoPedidoCats As CTipoPedidoCats
Private m_oIBaseDatos As IBaseDatos


Private Sub cmdAceptar_Click()

    GuardarCategoriasSeleccionadas
    
    If frmMantPedidos.sdbgTiposPedido.Rows > 0 And g_bModoEdicion Then
        frmMantPedidos.g_oTiposPedido.Item(CStr(frmMantPedidos.sdbgTiposPedido.Bookmark)).DevolverTieneCategoriasElTipoPedido
        frmMantPedidos.sdbgTiposPedido.Refresh
    End If
    
    Unload Me
        
End Sub



'3328 Guarda las categorias checkeadas en el arbol
Private Sub GuardarCategoriasSeleccionadas()

    oTipoPedidoCats.EliminarCategoriasTipoPedido g_lTipopedidoId
    RecorrerNodos tvwEstrCat.Nodes(1)
    
End Sub


'3328 Recorre todos los nodos del arbol
Public Sub RecorrerNodos(objNode As node)

    Dim oNodo As node
    Dim vCATN As Variant
    Dim vNivel As Variant
    Dim oTipoPedidoCat As CtipoPedidoCat

    Set oNodo = objNode

   'Recorre los nodos en forma recursiva hasta que no haya mas nodos
   Do
        'Si el nodo no esta checkeado...
        If Not oNodo.Checked Then
            If Not oNodo.Child Is Nothing Then
                'Nodos que cuelgan del actual
                Call RecorrerNodos(oNodo.Child)
            End If
        Else
            If oNodo.Tag = "Raiz" Then
            
                If Not oNodo.Child Is Nothing Then
                    Call RecorrerNodos(oNodo.Child)
                End If
                
            Else
            
                vCATN = DevolverId(oNodo)
                
                Select Case Left(oNodo.Tag, 4)
                    Case "CAT1"
                        vNivel = 1
                    Case "CAT2"
                        vNivel = 2
                    Case "CAT3"
                        vNivel = 3
                    Case "CAT4"
                        vNivel = 4
                    Case "CAT5"
                        vNivel = 5
                End Select
                
                Set oTipoPedidoCat = Nothing
                Set oTipoPedidoCat = oFSGSRaiz.Generar_CTipoPedidoCat
                
                oTipoPedidoCat.TipoPedidoId = g_lTipopedidoId
                oTipoPedidoCat.CATN = vCATN
                oTipoPedidoCat.Nivel = vNivel
                
                Set m_oIBaseDatos = oTipoPedidoCat
                m_oIBaseDatos.AnyadirABaseDatos

            End If
            
        End If

        'Siguiente nodo
        Set oNodo = oNodo.Next

   Loop While Not oNodo Is Nothing

End Sub




Private Sub cmdCancelar_Click()
    If frmMantPedidos.sdbgTiposPedido.Rows > 0 And g_bModoEdicion Then
        frmMantPedidos.g_oTiposPedido.Item(CStr(frmMantPedidos.sdbgTiposPedido.Bookmark)).DevolverTieneCategoriasElTipoPedido
        frmMantPedidos.sdbgTiposPedido.Refresh
    End If
    Unload Me

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_MANTPEDIDOSCAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sIdioma(1) = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value '8
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiCategoria = Ador(0).Value
        Ador.MoveNext
        sIdiCerrar = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub



Private Sub CargarCategoriasTipoPedido()

    Set oTipoPedidoCats = Nothing
    Set oTipoPedidoCats = oFSGSRaiz.Generar_CTipoPedidoCats
    
    oTipoPedidoCats.CargarTodasLasCategoriasTiposPedidos g_lTipopedidoId
    
End Sub




Private Sub Form_Activate()

    Dim nodx As MSComctlLib.node

    For Each nodx In tvwEstrCat.Nodes
        If Not oTipoPedidoCats.Item(nodx.key) Is Nothing Then
            If g_bModoEdicion Then
                nodx.Checked = True
            Else
                nodx.Image = "ACTASIG"
            End If
            MarcarTodosLosHijos nodx, False
            'If tvwEstrCat.Nodes(nodx.key).Visible Then
            nodx.EnsureVisible
            'End If
            DoEvents
        End If
    Next

End Sub

Private Sub Form_Load()

    Dim sCaption As String
    
    CargarRecursos
    
    Me.Height = 5485
    Me.Width = 7355
    
    sCaption = Me.caption
        
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    DoEvents
    
    If g_bModoEdicion = False Then
        tvwEstrCat.Checkboxes = False
        cmdAceptar.Visible = False
        cmdCancelar.Left = (Me.Width / 2) - (cmdCancelar.Width / 2)
        cmdCancelar.caption = sIdiCerrar
    Else
        tvwEstrCat.Checkboxes = True
    End If
    
    '3328
    CargarCategoriasTipoPedido
    
    GenerarEstructuraCat False
         
    Me.caption = sCaption

End Sub




Private Sub Form_Resize()
        
    If Me.Height < 1200 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    tvwEstrCat.Height = Me.Height - 1080
    tvwEstrCat.Width = Me.Width - 420
    cmdAceptar.Top = Me.Height - 900
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (tvwEstrCat.Width / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (tvwEstrCat.Width / 2) + 300
    
    If g_bModoEdicion = False Then
        tvwEstrCat.Checkboxes = False
        cmdAceptar.Visible = False
        cmdCancelar.Left = (Me.Width / 2) - (cmdCancelar.Width / 2)
        cmdCancelar.caption = sIdiCerrar
    Else
        tvwEstrCat.Checkboxes = True
    End If
    
End Sub




Private Sub Form_Unload(Cancel As Integer)

    Set oActsN1 = Nothing

End Sub



Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

    If node Is Nothing Then Exit Function

    'posici�n del primer -%$
    iPosicion = InStr(6, node.Tag, "-%$")
    DevolverCod = Mid(node.Tag, 6, iPosicion - 6)

End Function




Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion1 As Integer
Dim iPosicion2 As Integer

    If node Is Nothing Then Exit Function
    
    'posici�n del primer -%$,justo �ntes del ID
    iPosicion1 = InStr(6, node.Tag, "-%$")
    iPosicion1 = iPosicion1 + 3
    'posici�n del segundo -%$
    iPosicion2 = InStr(iPosicion1, node.Tag, "-%$")
    If iPosicion2 = 0 Then
        DevolverId = val(Right(node.Tag, Len(node.Tag) - (iPosicion1 - 1)))
    Else
        DevolverId = val(Mid(node.Tag, iPosicion1, iPosicion2 - iPosicion1))
    End If

End Function


Public Sub GenerarEstructuraCat(ByVal bOrdenadoPorDen As Boolean)
Dim oACN1 As CCategoriaN1
Dim oACN2 As CCategoriaN2
Dim oACN3 As CCategoriaN3
Dim oACN4 As CCategoriaN4
Dim oACN5 As CCategoriaN5

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Dim nodx As MSComctlLib.node


    
On Error GoTo Error

    tvwEstrCat.Nodes.clear
    
    Set nodx = tvwEstrCat.Nodes.Add(, , "Raiz", sIdioma(1), "RAIZ")
    nodx.Tag = "Raiz"
    nodx.Expanded = True

    Set oActsN1 = oFSGSRaiz.Generar_CCategoriasN1

    '3328
    oActsN1.GenerarEstructuraCategorias False, bOrdenadoPorDen
    
    If Not oActsN1 Is Nothing Then
           
        For Each oACN1 In oActsN1

            scod1 = CStr(oACN1.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
            Set nodx = tvwEstrCat.Nodes.Add("Raiz", tvwChild, CStr(g_lTipopedidoId) & "-" & CStr(oACN1.Id) & "-Nivel-1", oACN1.Cod & " - " & oACN1.Den, "ACT")
            nodx.Tag = "CAT1-" & oACN1.Cod & "-%$" & oACN1.Id
            
            nodx.Expanded = False
                  
            If Not oACN1.CategoriasN2 Is Nothing Then
            
                For Each oACN2 In oACN1.CategoriasN2

                    scod2 = CStr(oACN2.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
                    Set nodx = tvwEstrCat.Nodes.Add(CStr(g_lTipopedidoId) & "-" & CStr(oACN1.Id) & "-Nivel-1", tvwChild, CStr(g_lTipopedidoId) & "-" & CStr(oACN2.Id) & "-Nivel-2", oACN2.Cod & " - " & oACN2.Den, "ACT")
                    nodx.Tag = "CAT2-" & oACN2.Cod & "-%$" & oACN2.Id
                    
                    nodx.Expanded = False
                    
                    If Not oACN2.CategoriasN3 Is Nothing Then
                        For Each oACN3 In oACN2.CategoriasN3

                            scod3 = CStr(oACN3.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
                            Set nodx = tvwEstrCat.Nodes.Add(CStr(g_lTipopedidoId) & "-" & CStr(oACN2.Id) & "-Nivel-2", tvwChild, CStr(g_lTipopedidoId) & "-" & CStr(oACN3.Id) & "-Nivel-3", oACN3.Cod & " - " & oACN3.Den, "ACT")
                            nodx.Tag = "CAT3-" & oACN3.Cod & "-%$" & oACN3.Id
                            
                            nodx.Expanded = False
                            
                            If Not oACN3.CategoriasN4 Is Nothing Then
                            
                                For Each oACN4 In oACN3.CategoriasN4

                                    scod4 = CStr(oACN4.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
                                    Set nodx = tvwEstrCat.Nodes.Add(CStr(g_lTipopedidoId) & "-" & CStr(oACN3.Id) & "-Nivel-3", tvwChild, CStr(g_lTipopedidoId) & "-" & CStr(oACN4.Id) & "-Nivel-4", oACN4.Cod & " - " & oACN4.Den, "ACT")
                                    nodx.Tag = "CAT4-" & oACN4.Cod & "-%$" & oACN4.Id
                                    
                                    nodx.Expanded = False
                                    
                                    If Not oACN4.CategoriasN5 Is Nothing Then
                                    
                                        For Each oACN5 In oACN4.CategoriasN5

                                            scod5 = CStr(oACN5.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
                                            Set nodx = tvwEstrCat.Nodes.Add(CStr(g_lTipopedidoId) & "-" & CStr(oACN4.Id) & "-Nivel-4", tvwChild, CStr(g_lTipopedidoId) & "-" & CStr(oACN5.Id) & "-Nivel-5", oACN5.Cod & " - " & oACN5.Den, "ACT")
                                            nodx.Tag = "CAT5-" & oACN5.Cod & "-%$" & oACN5.Id
                                            
                                            nodx.Expanded = False

                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
   
    End If
    
    For Each nodx In tvwEstrCat.Nodes
        If Not oTipoPedidoCats.Item(nodx.key) Is Nothing Then
            If g_bModoEdicion Then
                nodx.Checked = True
            Else
                nodx.Image = "ACTASIG"
            End If
            MarcarTodosLosHijos nodx, False
            'If tvwEstrCat.Nodes(nodx.key).Visible Then
            nodx.EnsureVisible
            'End If
            DoEvents
        End If
    Next
    
Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
    
End Sub




Public Function DevolverDen(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

    If node Is Nothing Then Exit Function

    'posici�n del gui�n
    iPosicion = InStr(1, node.Text, "-")
    DevolverDen = Mid(node.Text, iPosicion + 2, Len(node.Text) - iPosicion)

End Function





'3328
Private Sub tvwEstrCat_NodeCheck(ByVal node As MSComctlLib.node)
    
    If node.Children > 0 Or Not node.Parent Is Nothing Then
        
        If node.Checked Then
            MarcarTodosLosHijos node, True
        Else
            QuitarMarcaTodosSusPadres node
            QuitarMarcaTodosLosHijos node
        End If
    End If
    
End Sub



'3328
Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.node, ByVal bMostrarHijos As Boolean)
    
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
    Dim nod4 As MSComctlLib.node


    Set nod1 = nodx.Child
    
    If Not nod1 Is Nothing Then
    
        While Not nod1 Is Nothing
             If g_bModoEdicion Then
                nod1.Checked = True
             Else
                nod1.Image = "ACTASIG"
             End If
             If bMostrarHijos Then nod1.EnsureVisible
             nod1.Expanded = False
             Set nod2 = nod1.Child
             If Not nod2 Is Nothing Then
             
                While Not nod2 Is Nothing
                    If g_bModoEdicion Then
                        nod2.Checked = True
                    Else
                        nod2.Image = "ACTASIG"
                    End If
                    If bMostrarHijos Then nod2.EnsureVisible
                    nod2.Expanded = False
                    Set nod3 = nod2.Child
                    If Not nod3 Is Nothing Then
                    
                        While Not nod3 Is Nothing
                            If g_bModoEdicion Then
                                nod3.Checked = True
                            Else
                                nod3.Image = "ACTASIG"
                            End If
                            If bMostrarHijos Then nod3.EnsureVisible
                            nod3.Expanded = False
                            Set nod4 = nod3.Child
                            If Not nod4 Is Nothing Then
                            
                                While Not nod4 Is Nothing
                                    If g_bModoEdicion Then
                                        nod4.Checked = True
                                    Else
                                        nod4.Image = "ACTASIG"
                                    End If
                                    If bMostrarHijos Then nod4.EnsureVisible
                                    nod4.Expanded = False
                                    Set nod4 = nod4.Next
                                Wend

                            End If
                            Set nod3 = nod3.Next
                        Wend
                    End If

                    Set nod2 = nod2.Next
                Wend
             End If

             Set nod1 = nod1.Next
        Wend
    DoEvents

    End If

End Sub

'3328
Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.node)
    
    If nodx.Index = 1 Then
        'Es el nodo raiz
        nodx.Checked = False
    Else
        Set nodx = nodx.Parent
        While Not nodx Is Nothing 'And nodx.Index <> 1
            nodx.Checked = False
            Set nodx = nodx.Parent
        Wend
    End If
    DoEvents
    
End Sub



'3328
Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.node)
    
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

    Set nod1 = nodx.Child

    While Not nod1 Is Nothing
         
         nod1.Checked = False
         Set nod2 = nod1.Child
         
         While Not nod2 Is Nothing
             nod2.Checked = False
             Set nod3 = nod2.Child
             
             While Not nod3 Is Nothing
                 nod3.Checked = False
                 Set nod4 = nod3.Child
                 
                 While Not nod4 Is Nothing
                     nod4.Checked = False
                     Set nod4 = nod4.Next
                 Wend
                 Set nod3 = nod3.Next
             Wend
             Set nod2 = nod2.Next
         Wend
         Set nod1 = nod1.Next
    Wend
            
    DoEvents

End Sub




