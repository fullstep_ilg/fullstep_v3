VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmLista 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DValores de la lista"
   ClientHeight    =   5295
   ClientLeft      =   1395
   ClientTop       =   1995
   ClientWidth     =   6210
   Icon            =   "frmLista.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5295
   ScaleWidth      =   6210
   Begin VB.PictureBox picOptions 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   240
      ScaleHeight     =   300
      ScaleWidth      =   5175
      TabIndex        =   11
      Top             =   100
      Width           =   5175
      Begin VB.OptionButton optListaExterna 
         BackColor       =   &H00808000&
         Caption         =   "DLista externa"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3000
         TabIndex        =   13
         Top             =   0
         Width           =   2535
      End
      Begin VB.OptionButton optValoresLista 
         BackColor       =   &H00808000&
         Caption         =   "DEstablecer valores de la lista"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   12
         Top             =   0
         Value           =   -1  'True
         Width           =   2535
      End
   End
   Begin VB.Frame fraOptions 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   6255
   End
   Begin VB.CheckBox chkGuardarDef 
      BackColor       =   &H00808000&
      Caption         =   "DGuardar en la definici�n del atributo "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   840
      TabIndex        =   9
      Top             =   60
      Visible         =   0   'False
      Width           =   3825
   End
   Begin VB.Frame fraLista 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2895
      Left            =   75
      TabIndex        =   2
      Top             =   400
      Width           =   4890
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "D&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   8
         Top             =   2400
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.CommandButton cmdAnyadir 
         Caption         =   "DA&�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   225
         TabIndex        =   7
         Top             =   2400
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "D&Deshacer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2175
         TabIndex        =   6
         Top             =   2400
         Visible         =   0   'False
         Width           =   990
      End
      Begin VB.CommandButton cmdSortDesc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4425
         Picture         =   "frmLista.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   645
         Width           =   345
      End
      Begin VB.CommandButton cmdSortAsc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4425
         Picture         =   "frmLista.frx":0FF4
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   300
         Width           =   345
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgValores 
         Height          =   2085
         Left            =   225
         TabIndex        =   3
         Top             =   240
         Width           =   4125
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   3
         Col.Count       =   15
         Row(0).Col(0)   =   "Entre 100 y 500"
         Row(1).Col(0)   =   "Entre 500 y 1.000"
         Row(2).Col(0)   =   "Entre 1.000 y 2.000"
         stylesets.count =   2
         stylesets(0).Name=   "Yellow"
         stylesets(0).BackColor=   8454143
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmLista.frx":1336
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmLista.frx":1352
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   5
         Columns(0).Width=   6324
         Columns(0).Caption=   "DValor"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   800
         Columns(0).HasForeColor=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "HER"
         Columns(1).Name =   "ORD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   2
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "IdAtributo"
         Columns(2).Name =   "IdATRIB"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "ValorPond"
         Columns(3).Name =   "VALORPOND"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FECACT"
         Columns(4).Name =   "FECACT"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   7276
         _ExtentY        =   3678
         _StockProps     =   79
         BackColor       =   -2147483648
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2250
      TabIndex        =   1
      Top             =   3510
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1200
      TabIndex        =   0
      Top             =   3510
      Visible         =   0   'False
      Width           =   915
   End
End
Attribute VB_Name = "frmLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable de control de flujo de proceso
Public Accion As AccionesSummit

Public g_oAtributo As CAtributo
Public g_oPondIntermedia As CValoresPond
Public g_iIdAtrib As Long
Public g_iTipo As Integer
Public g_bEdicion As Boolean
Public g_iIndiCole As Integer
Public g_sOrigen As String
Public g_bModDefAtrib As Boolean

Private m_bError As Boolean
Private m_bBorrarPond As Boolean
Private m_bRespetarLista As Boolean
Private m_sMensaje(0 To 4) As String
Private m_oListaEnEdicion As CValorPond
Public g_bAtribConOfeNoModif As Boolean
Private m_oIBAseDatosEnEdicion As IBaseDatos

Public m_bRespuesta As Boolean
Private mbHayCambios As Boolean

Private Sub CargarRecursos()
   Dim Ador As Ador.Recordset
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LISTA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        frmLista.caption = Ador(0).Value
        Ador.MoveNext
        sdbgValores.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(0) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(1) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(2) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(3) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(4) = Ador(0).Value
        Ador.MoveNext
        cmdAnyadir.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        chkGuardarDef.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        optValoresLista.caption = Ador(0).Value
        Ador.MoveNext
        optListaExterna.caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

''' <summary>
''' Acepta los cambios hechos sobre la lista del atributo
''' </summary>
''' <returns>Trure</returns>
''' <remarks>Llamada desde: frmAtrib,frmproce,frmPlantillasProce,frmformularios,frmPARTipoSolicit,frmLista,frmESTRMATAtrib  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub cmdAceptar_Click()
    Dim iIndice As Integer
    Dim teserror As TipoErrorSummit
    Dim teserror2 As TipoErrorSummit
    Dim iIDAtributo As Long
    Dim bValorDefectoErroneo As Boolean
    Dim SProcesos As String
    Dim SFormularios As String
    Dim SPedidos As String
    Dim sCadena As String
    Dim iRespuestaRoles As Integer
    
    If sdbgValores.DataChanged Then
        sdbgValores.Update
        If m_bError Then
            Exit Sub
        End If
    End If
    
    If Not mbHayCambios Then
        Unload Me
        Exit Sub
    End If
    
    Set g_oAtributo.ListaPonderacion = Nothing
    Set g_oAtributo.ListaPonderacion = oFSGSRaiz.Generar_CValoresPond
    
    sdbgValores.MoveFirst
    
    iIDAtributo = StrToDbl0(sdbgValores.Columns("IdATRIB").Value)
     g_oAtributo.ListaExterna = IIf(optListaExterna.Value, 1, 0)
    If Not g_oAtributo.ListaExterna = 1 Then
        For iIndice = 1 To sdbgValores.Rows
            If sdbgValores.Columns("COD").Value <> "" Then
                If g_oAtributo.Tipo = TiposDeAtributos.TipoFecha Then
                    g_oAtributo.ListaPonderacion.Add g_oAtributo.Id, , , , , iIndice, CDate(sdbgValores.Columns("COD").Value), StrToNull(sdbgValores.Columns("VALORPOND").Value), , , , iIndice
                Else
                    If g_oAtributo.Tipo = TipoNumerico Then
                        g_oAtributo.ListaPonderacion.Add g_oAtributo.Id, , , , , iIndice, StrToDbl0(sdbgValores.Columns("COD").Value), StrToNull(sdbgValores.Columns("VALORPOND").Value), , , , iIndice
                    Else
                        g_oAtributo.ListaPonderacion.Add g_oAtributo.Id, , , , , iIndice, StrToNull(sdbgValores.Columns("COD").Value), StrToNull(sdbgValores.Columns("VALORPOND").Value), , , , iIndice
                    End If
                End If
            End If
            sdbgValores.MoveNext
            Select Case g_sOrigen
                Case "MDIfrmATRIB"
                    frmAtrib.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "PLANTILLAS"
                    frmPlantillasProce.sdbgAtribPlant.Columns("BOT").Value = "..."
                Case "frmPROCE"
                    frmPROCE.sdbgAtributos.Columns("BOT").Value = "..."
                Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
                    frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "frmPROCEfrmATRIB"
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "frmFormulariosfrmATRIB"
                    frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "frmDesglosefrmATRIB"
                    frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "frmPARTipoSolicitfrmATRIB"
                    frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "frmPARTipoSolicitDesglosefrmATRIB"
                    frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "ITEM_ATRIBESPfrmATRIB"
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "ATRIB_ESP_GRUPOfrmATRIB"
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "ATRIB_ESPfrmATRIB"
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case "frmCONFIntegracion"
                    frmCONFIntegracion.sdbgAtribPlant.Columns("LISTAVALORES").Value = "..."
                Case "CONFINTEGRACIONfrmATRIB"
                    frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
                Case Else
                    frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = "..."
            End Select
        Next iIndice
    End If
    
    Dim bComprobacionesDatos As Boolean
    bComprobacionesDatos = False
    
    Dim sVdefecto As String
    Select Case g_sOrigen
        Case "MDIfrmATRIB"
            g_oAtributo.FecAct = frmAtrib.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
            g_oAtributo.FecAct = frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "frmPROCEfrmATRIB"
            g_oAtributo.FecAct = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "PLANTILLAS"
        Case "frmPROCE"
        Case "frmFormulariosfrmATRIB"
            g_oAtributo.FecAct = frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "frmDesglosefrmATRIB"
            g_oAtributo.FecAct = frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "frmPARTipoSolicitfrmATRIB"
            g_oAtributo.FecAct = frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "frmPARTipoSolicitDesglosefrmATRIB"
            g_oAtributo.FecAct = frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "ITEM_ATRIBESPfrmATRIB"
            g_oAtributo.FecAct = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "ATRIB_ESP_GRUPOfrmATRIB"
            g_oAtributo.FecAct = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "ATRIB_ESPfrmATRIB"
            g_oAtributo.FecAct = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case "frmCONFIntegracion"
            g_oAtributo.FecAct = frmCONFIntegracion.sdbgAtribPlant.Columns("FECACT").Value
            sVdefecto = frmCONFIntegracion.sdbgAtribPlant.Columns("VALORPORDEFECTO").Text
            bComprobacionesDatos = True
        Case "CONFINTEGRACIONfrmATRIB"
            g_oAtributo.FecAct = frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            sVdefecto = frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
        Case Else
            g_oAtributo.FecAct = frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value
            sVdefecto = frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
            bComprobacionesDatos = True
    End Select
    
    If bComprobacionesDatos Then
        bValorDefectoErroneo = True
        
        If g_oAtributo.ListaExterna = 1 Then
            sVdefecto = ""
            bValorDefectoErroneo = False
            frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
            frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Text = ""
        Else
            If sVdefecto <> "" Then
                sdbgValores.MoveFirst
                For iIndice = 1 To sdbgValores.Rows
                    If sdbgValores.Columns("COD").Value = sVdefecto Then
                        bValorDefectoErroneo = False
                        Exit For
                    End If
                    sdbgValores.MoveNext
                Next
            Else
                bValorDefectoErroneo = False
            End If
        End If
        
        If bValorDefectoErroneo Then
            oMensajes.MensajeOKOnly 926, TipoIconoMensaje.Information
              
            Select Case g_sOrigen
                Case "MDIfrmATRIB", "MDI"
                    frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Text = ""
                    
                Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
                    frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    
                Case "frmPROCEfrmATRIB"
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    
                Case "PLANTILLAS"
                Case "frmPROCE"
                Case "frmFormulariosfrmATRIB"
                    frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    
                Case "frmDesglosefrmATRIB"
                    frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                                
                Case "frmPARTipoSolicitfrmATRIB"
                    frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    
                Case "frmPARTipoSolicitDesglosefrmATRIB"
                    frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                                
        
                Case "ITEM_ATRIBESPfrmATRIB"
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                                
                Case "ATRIB_ESP_GRUPOfrmATRIB"
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    
                Case "ATRIB_ESPfrmATRIB"
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    
               Case "frmCONFIntegracion"
                    frmCONFIntegracion.sdbgAtribPlant.Columns("VALORPORDEFECTO").Value = ""
                    
               Case "CONFINTEGRACIONfrmATRIB"
                    frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = ""
                    
                Case Else
                    frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                    frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
            End Select
        End If
        
        If sdbgValores.Rows > 0 And Not g_oAtributo.ListaExterna = 1 Then
            If g_oAtributo.ComprobarAtributoEnFormsPM(SFormularios) Then
                frmAtribMsg.g_sOrigen = "FRMLISTA"
                frmAtribMsg.m_itipoMensaje = 1
                frmAtribMsg.txtMsg.Text = "Formulario" & vbCrLf & SFormularios
                frmAtribMsg.Show 1
                
                If m_bRespuesta = False Then
                    Unload Me
                    Exit Sub
                End If
            End If
        
            If g_oAtributo.ComprobarAtributoEnProcesosYPedidos(SProcesos, SPedidos) Then
                m_bRespuesta = False
                frmAtribMsg.m_itipoMensaje = 2
                If SProcesos <> "" Then
                    sCadena = "Procesos" & vbCrLf
                    sCadena = sCadena & SProcesos
                End If
            
                frmAtribMsg.g_sOrigen = "FRMLISTA"
                frmAtribMsg.txtMsg.Text = sCadena
                frmAtribMsg.Show 1
            
                If m_bRespuesta = False Then
                    Unload Me
                    Exit Sub
                End If
            End If
        End If
    End If
        
    Dim bRespuestaRoles As Boolean
    bRespuestaRoles = True
    If g_oAtributo.ValoresNoEncontrados Then
        If g_oAtributo.ComprobarAtributoEnWorkFlowsPM Then
            iRespuestaRoles = oMensajes.PreguntaAplicarValorARoles
            
            If iRespuestaRoles = vbNo Then
                bRespuestaRoles = False
            End If
        End If
    End If
       
    teserror2.NumError = TESnoerror
    teserror = g_oAtributo.ModificarListaDeValores(chkGuardarDef.Value, bRespuestaRoles)
        
    Dim sTexto
    If sVdefecto <> "" And Not bValorDefectoErroneo And Not g_oAtributo.ListaExterna = 1 Then
        teserror2 = g_oAtributo.InsertarValorNum(iIndice)
        
        Select Case g_sOrigen
            Case "MDIfrmATRIB", "MDI"
                sTexto = frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmAtrib.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                        
            Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
                sTexto = frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                        
            Case "frmPROCEfrmATRIB"
                sTexto = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                        
            Case "PLANTILLAS"
             
            Case "frmPROCE"
               
            Case "frmFormulariosfrmATRIB"
                sTexto = frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                        
            Case "frmDesglosefrmATRIB"
                sTexto = frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                                    
            Case "frmPARTipoSolicitfrmATRIB"
                sTexto = frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                            
            Case "frmPARTipoSolicitDesglosefrmATRIB"
                sTexto = frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                                    
                                                
            Case "ITEM_ATRIBESPfrmATRIB"
                sTexto = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                        
            Case "ATRIB_ESP_GRUPOfrmATRIB"
                sTexto = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                                                
            Case "ATRIB_ESPfrmATRIB"
                sTexto = frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
                                    
            Case Else
                sTexto = frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text
                frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = iIndice
                frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Text = sTexto
            
                frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
                frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("VDEFECTO").Value = ""
        End Select
    Else
        If sVdefecto <> "" And bValorDefectoErroneo Then
            teserror2 = g_oAtributo.EliminarValorDefecto
        End If
    End If
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Unload frmESPERA
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Select Case g_sOrigen
        Case "MDIfrmATRIB"
            frmAtrib.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
            frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "frmPROCEfrmATRIB"
            frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "PLANTILLAS"
        Case "frmPROCE"
            If frmPROCE.sdbcGrupoA.Visible And frmPROCE.sdbcGrupoA.Columns(0).Value <> "************" Then
                frmPROCE.g_oGrupoSeleccionado.ATRIBUTOS.Item(CStr(g_oAtributo.idAtribProce)).FecAct = g_oAtributo.FecAct
            Else
                frmPROCE.g_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(g_oAtributo.idAtribProce)).FecAct = g_oAtributo.FecAct
            End If
        Case "frmFormulariosfrmATRIB"
            frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "frmDesglosefrmATRIB"
            frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "frmPARTipoSolicitfrmATRIB"
            frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "frmPARTipoSolicitDesglosefrmATRIB"
            frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "ITEM_ATRIBESPfrmATRIB"
            frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "ATRIB_ESP_GRUPOfrmATRIB"
            frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "ATRIB_ESPfrmATRIB"
            frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "frmCONFIntegracion"
            frmCONFIntegracion.sdbgAtribPlant.Columns("FECACT").Value = g_oAtributo.FecAct
        Case "CONFINTEGRACIONfrmATRIB"
            frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
        Case Else
            frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FecAct
    End Select
    
    Unload Me
End Sub

Private Sub cmdAnyadir_Click()
    cmdEliminar.Enabled = False
    cmdAnyadir.Enabled = False
    cmdDeshacer.Enabled = False

    sdbgValores.Scroll 0, sdbgValores.Rows - sdbgValores.Row
    
    If sdbgValores.VisibleRows > 0 Then
        
        If sdbgValores.VisibleRows > sdbgValores.Rows Then
            sdbgValores.Row = sdbgValores.Rows
        Else
            sdbgValores.Row = sdbgValores.Rows - (sdbgValores.Rows - sdbgValores.VisibleRows) - 1
        End If
        
    End If

    If Me.Visible Then sdbgValores.SetFocus
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdDeshacer_Click()
    ''' * Objetivo: Deshacer la edicion en el valor actual
    sdbgValores.CancelUpdate
    sdbgValores.DataChanged = False
    
    If g_bEdicion Then
        sdbgValores.Columns("COD").Locked = False
    End If
    
    cmdAnyadir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    
    If sdbgValores.Rows = 1 Then
        sdbgValores.MoveFirst
        sdbgValores.Refresh
        sdbgValores.Bookmark = sdbgValores.SelBookmarks(0)
        sdbgValores.SelBookmarks.RemoveAll
    End If
    m_bError = False
End Sub

Private Sub cmdEliminar_Click()
    If sdbgValores.Rows > 0 And sdbgValores.Columns("COD").Value <> "" Then
        Screen.MousePointer = vbHourglass
        sdbgValores.SelBookmarks.Add sdbgValores.Bookmark
        EliminarPonderacionSeleccionada
        Screen.MousePointer = vbNormal
    End If
End Sub

''' <summary>
''' Cambia el orden de los valores de la lista
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdSortAsc_Click()
    Dim sText As String
    Dim sText2 As String
    Dim sPond1 As String
    Dim sPond2 As String
    Dim sOrd1 As String
    Dim sOrd2 As String

    If sdbgValores.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgValores.AddItemRowIndex(sdbgValores.Bookmark) = 0 Then Exit Sub
    sText = sdbgValores.Columns("COD").Value
    sPond1 = sdbgValores.Columns("VALORPOND").Value
    sOrd1 = sdbgValores.Columns("ORD").Value
    sdbgValores.MovePrevious
    sText2 = sdbgValores.Columns("COD").Value
    sPond2 = sdbgValores.Columns("VALORPOND").Value
    sOrd2 = sdbgValores.Columns("ORD").Value
    sdbgValores.Columns("COD").Value = sText
    sdbgValores.Columns("VALORPOND").Value = sPond1
    sdbgValores.Columns("ORD").Value = sOrd1
    m_bRespetarLista = True
    sdbgValores.MoveNext
    sdbgValores.Columns("COD").Value = sText2
    sdbgValores.Columns("VALORPOND").Value = sPond2
    sdbgValores.Columns("ORD").Value = sOrd2
    
    sdbgValores.SelBookmarks.RemoveAll
    sdbgValores.MovePrevious
    m_bRespetarLista = False
    sdbgValores.SelBookmarks.Add sdbgValores.Bookmark

    mbHayCambios = True
End Sub

''' <summary>
''' Cambia el orden de los valores de la lista
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdSortDesc_Click()
    Dim sText As String
    Dim sText2 As String
    Dim sPond1 As String
    Dim sPond2 As String
    Dim sOrd1 As String
    Dim sOrd2 As String

    If sdbgValores.SelBookmarks.Count = 0 Then Exit Sub
    'If val(sdbgValores.SelBookmarks.Item(0)) = sdbgValores.Rows - 1 Then Exit Sub
    If sdbgValores.AddItemRowIndex(sdbgValores.Bookmark) = sdbgValores.Rows - 1 Then Exit Sub
    sText = sdbgValores.Columns("COD").Value
    sPond1 = sdbgValores.Columns("VALORPOND").Value
    sOrd1 = sdbgValores.Columns("ORD").Value
    sdbgValores.MoveNext
    sText2 = sdbgValores.Columns("COD").Value
    sPond2 = sdbgValores.Columns("VALORPOND").Value
    sOrd2 = sdbgValores.Columns("ORD").Value
    sdbgValores.Columns("COD").Value = sText
    sdbgValores.Columns("VALORPOND").Value = sPond1
    sdbgValores.Columns("ORD").Value = sOrd1
    m_bRespetarLista = True
    sdbgValores.MovePrevious
    sdbgValores.Columns("COD").Value = sText2
    sdbgValores.Columns("VALORPOND").Value = sPond2
    sdbgValores.Columns("ORD").Value = sOrd2
    sdbgValores.SelBookmarks.RemoveAll
    sdbgValores.MoveNext
    m_bRespetarLista = False
    sdbgValores.SelBookmarks.Add sdbgValores.Bookmark

    mbHayCambios = True
End Sub

''' <summary>
''' Carga inicial de pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    CargarRecursos
    
    PonerFieldSeparator Me

    sdbgValores.RemoveAll
    
    FraLista.Height = 2500
    
    Resize
End Sub
Private Sub Resize()
    If Not g_oAtributo Is Nothing Then
        If g_oAtributo.Tipo = TipoString And g_bEdicion Then
            'Tama�o mas grande pq pueden meter textos de 800 caracteres
            frmLista.Height = 3600
            frmLista.Width = 9000
                
            frmLista.Top = MDI.ScaleHeight / 2 - frmLista.Height
            frmLista.Left = 5730
            FraLista.Width = 8700
            chkGuardarDef.Visible = False
            
            sdbgValores.Width = 8000
            sdbgValores.Columns(0).Width = 7405
            cmdSortAsc.Left = sdbgValores.Width + sdbgValores.Left + 75
            cmdSortDesc.Left = sdbgValores.Width + sdbgValores.Left + 75
            
            cmdAceptar.Left = (Me.Width / 2) - cmdAceptar.Width - 100
            cmdCancelar.Left = (Me.Width / 2) + 100
            
            fraOptions.Visible = True
            picOptions.Enabled = True
        Else
            frmLista.Width = 5190
            
            frmLista.Top = MDI.ScaleHeight / 2 - frmLista.Height
            frmLista.Left = 5730
            
            cmdAceptar.Left = 1200
            cmdCancelar.Left = 2250
            If g_oAtributo.Tipo = TipoString Then
                picOptions.Enabled = False
                frmLista.Height = 3600
            Else
                picOptions.Visible = False
                frmLista.Height = 3600
            End If
        End If
    Else
        frmLista.Height = 3600
        frmLista.Width = 5190
        frmLista.Top = MDI.ScaleHeight / 2 - frmLista.Height
        frmLista.Left = 5730
        
        cmdAceptar.Left = 1200
        cmdCancelar.Left = 2250
        
        fraOptions.Visible = False
        picOptions.Visible = False
    End If
    If frmLista.Top < 0 Then
        frmLista.Top = 0
    End If
    chkGuardarDef.Visible = False
            
    If g_bEdicion And Not g_bAtribConOfeNoModif Then
        sdbgValores.AllowAddNew = True
        sdbgValores.AllowDelete = True
        sdbgValores.AllowUpdate = True
        cmdSortDesc.Enabled = True
        cmdSortAsc.Enabled = True
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        cmdAnyadir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
        cmdDeshacer.Enabled = False
        frmLista.Height = 4200
        FraLista.Height = 2900 ' Modifico el alto del frame ya que apareceran los botones de eliminar, deshacer y a�adir
        cmdAceptar.Top = 3400
        cmdCancelar.Top = 3400
    Else
        sdbgValores.AllowAddNew = False
        sdbgValores.AllowDelete = False
        sdbgValores.AllowUpdate = False
        cmdSortDesc.Enabled = False
        cmdSortAsc.Enabled = False
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        cmdAnyadir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
    End If
    m_bBorrarPond = False
    
    mbHayCambios = False
End Sub
Private Sub Form_Unload(Cancel As Integer)
    If g_oAtributo.ListaPonderacion.Count = 0 And Not optListaExterna.Value Then
        Select Case g_sOrigen
            Case "MDIfrmATRIB"
                frmAtrib.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmAtrib.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmAtrib.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmAtrib.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmAtrib.g_oAtributoEnEdicion = g_oAtributo
                
            Case "PLANTILLAS"
                If frmPlantillasProce.m_bModoEdicionAtr Then
                    frmPlantillasProce.sdbgAtribPlant.Columns("BOT").Value = " "
                    frmPlantillasProce.sdbgAtribPlant.Columns("LIBRE").Value = -1
                    frmPlantillasProce.sdbgAtribPlant.Columns("SELECCION").Value = 0
                    frmPlantillasProce.m_bModificarDefAtrib = False
                    If m_bBorrarPond Then
                       frmPlantillasProce.sdbgAtribPlant.Columns("TipoPond").Value = 0
                       g_oAtributo.TipoPonderacion = SinPonderacion
                    End If
                End If
                
                
            Case "PLANTILLASfrmATRIB", "PLANTILLASESPfrmATRIB"
                frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmPlantillasProce.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
                
            Case "frmPROCEfrmATRIB"
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmPROCE.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
            
            Case "frmPROCE"
                frmPROCE.sdbgAtributos.Columns("SELECCION").Value = 0
                frmPROCE.sdbgAtributos.Columns("LIBRE").Value = -1
                frmPROCE.sdbgAtributos.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmPROCE.sdbgAtributos.Columns("TIPOPONDERACION").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmPROCE.g_oAtributoEnEdicion = g_oAtributo
            
            Case "frmFormulariosfrmATRIB"
                frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmFormularios.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
            
            Case "frmDesglosefrmATRIB"
                frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmFormularios.g_ofrmDesglose.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
                
            Case "frmPARTipoSolicitfrmATRIB"
                frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmPARTipoSolicit.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
                
            Case "frmPARTipoSolicitDesglosefrmATRIB"
                frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
                
            Case "ITEM_ATRIBESPfrmATRIB"
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmPROCE.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
                
            Case "ATRIB_ESP_GRUPOfrmATRIB"
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmPROCE.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
            
            Case "ATRIB_ESPfrmATRIB"
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmPROCE.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
                
            Case "frmCONFIntegracion"
                frmCONFIntegracion.sdbgAtribPlant.Columns("SELECCION").Value = 0
                frmCONFIntegracion.sdbgAtribPlant.Columns("LISTAVALORES").Value = " "
    
                Set frmCONFIntegracion.m_oAtributoEnEdicion = g_oAtributo
                
            Case "CONFINTEGRACIONfrmATRIB"
                frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("SELECCION").Value = 0
                frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("LIBRE").Value = -1
                frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmCONFIntegracion.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmCONFIntegracion.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
        
            Case Else
                frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("BOT").Value = " "
                If m_bBorrarPond Then
                   frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = 0
                   g_oAtributo.TipoPonderacion = SinPonderacion
                End If
                Set frmESTRMATAtrib.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
        End Select
    End If
    If Not m_oIBAseDatosEnEdicion Is Nothing Then
        m_oIBAseDatosEnEdicion.CancelarEdicion
        Set m_oIBAseDatosEnEdicion = Nothing
    End If
    Set m_oListaEnEdicion = Nothing
    g_bAtribConOfeNoModif = False
    Set g_oAtributo = Nothing
    Set g_oPondIntermedia = Nothing
    g_iIdAtrib = 0
    g_iTipo = 0
    g_bEdicion = False
    g_iIndiCole = 0
    g_sOrigen = ""
    m_bError = False
    m_bBorrarPond = False
    m_bRespetarLista = False
    chkGuardarDef.Value = vbUnchecked
End Sub

Private Sub optListaExterna_Click()
    If g_bEdicion Then
        frmLista.Height = 1500
    Else
        frmLista.Height = 1000
    End If
    FraLista.Visible = False
    cmdAceptar.Top = 500
    cmdCancelar.Top = 500
    If Not g_oAtributo.ListaExterna Then
        mbHayCambios = True
    End If
End Sub

Private Sub optValoresLista_Click()
    FraLista.Visible = True
    Resize
    If g_oAtributo.ListaExterna Then
        mbHayCambios = True
    End If
End Sub

Private Sub sdbgValores_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' Comprobaciones antes de updatar
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgValores_BeforeUpdate(Cancel As Integer)
    Dim vIndice As Variant
    Dim oListaInter As CValorPond
    Dim vValor As Variant
    
    m_bError = False
    If Not m_bRespetarLista Then
        If sdbgValores.Columns("COD").Text = "" Then
            If sdbgValores.Columns("ORD").Value <> "" Then
               g_oPondIntermedia.Remove (sdbgValores.Columns("ORD").Value)
            Else
                sdbgValores.CancelUpdate
            End If
        Else
            If g_oAtributo.Tipo = TiposDeAtributos.TipoFecha And Not IsDate(sdbgValores.Columns("COD").Value) Then
                oMensajes.NoValido sdbgValores.Columns("COD").caption & " " & m_sMensaje(3)
                If Me.Visible Then sdbgValores.SetFocus
                Cancel = True
                m_bError = True
            Else
                If g_oAtributo.Tipo = TiposDeAtributos.TipoNumerico And Not IsNumeric(sdbgValores.Columns("COD").Value) Then
                    oMensajes.NoValido sdbgValores.Columns("COD").caption & " " & m_sMensaje(2)
                    If Me.Visible Then sdbgValores.SetFocus
                    Cancel = True
                    m_bError = True
                End If
            End If
            If Not m_bError Then
                If Not sdbgValores.IsAddRow Then
                    If sdbgValores.AddItemRowIndex(sdbgValores.Bookmark) + 1 <= g_oPondIntermedia.Count Then
                        g_oPondIntermedia.Item(sdbgValores.Columns("ORD").Value).ValorLista = sdbgValores.Columns("COD").Value
                    End If
                End If
                If g_oAtributo.Tipo = TiposDeAtributos.TipoNumerico Then
                    vValor = StrToDbl0(sdbgValores.Columns("COD").Value)
                Else
                    If g_oAtributo.Tipo = TiposDeAtributos.TipoFecha Then
                        vValor = CDate(sdbgValores.Columns("COD").Value)
                    Else
                        vValor = sdbgValores.Columns("COD").Value
                    End If
                End If
                For Each oListaInter In g_oPondIntermedia
                    Dim bCheck As Boolean
                    If sdbgValores.IsAddRow Then
                        bCheck = True
                    Else
                        bCheck = (sdbgValores.AddItemRowIndex(sdbgValores.Bookmark) + 1 <> oListaInter.indice)
                    End If
                    If bCheck Then
                        If vValor = oListaInter.ValorLista Then
                            'el valor definido ya existe
                            oMensajes.NoValido m_sMensaje(4)
                            If Me.Visible Then sdbgValores.SetFocus
                            Cancel = True
                            m_bError = True
                            Exit Sub
                        End If
                    End If
                Next
                If sdbgValores.IsAddRow Then
                    sdbgValores.Columns("ORD").Value = g_iIndiCole
                    g_oPondIntermedia.Add g_oAtributo.Id, , , , , , vValor, , , , Date, g_iIndiCole
                    sdbgValores.Columns("FECACT").Value = Date
                    g_iIndiCole = g_iIndiCole + 1
                End If
                
                mbHayCambios = True
                
            End If
        End If
    End If
End Sub

Private Sub sdbgValores_Change()
    If cmdDeshacer.Enabled = False Then
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    End If
End Sub

Private Sub sdbgValores_KeyDown(KeyCode As Integer, Shift As Integer)
    ''' * Objetivo: Capturar la tecla Supr para
    ''' * Objetivo: poder no dejar eliminar desde el teclado
    If KeyCode = vbKeyDelete Then
        Exit Sub
    End If
End Sub

Private Sub sdbgValores_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
        If Not sdbgValores.IsAddRow And sdbgValores.Columns(2).Value <> "" Then
            sdbgValores.Columns(0).Locked = True
        Else
            sdbgValores.Columns(0).Locked = False
        End If
            
        If sdbgValores.DataChanged = True Then
            sdbgValores.Update
        End If
        If sdbgValores.IsAddRow Then
            cmdEliminar.Enabled = False
        Else
            cmdEliminar.Enabled = True
        End If
        cmdAnyadir.Enabled = True
        cmdDeshacer.Enabled = False
End Sub

Private Sub sdbgValores_RowLoaded(ByVal Bookmark As Variant)
    If sdbgValores.IsAddRow Then
        cmdEliminar.Enabled = False
    Else
        cmdEliminar.Enabled = True
    End If
End Sub

Private Sub sdbgValores_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
    If sdbgValores.SelBookmarks.Count > 0 Then
        If g_bEdicion Then
            cmdSortDesc.Enabled = True
            cmdSortAsc.Enabled = True
        End If
    Else
        cmdSortDesc.Enabled = False
        cmdSortAsc.Enabled = False
    End If
End Sub

''' <summary>
''' Pregunta antes de eliminar y elimina si respondes q s�
''' </summary>
''' <remarks>Llamada desde: cmdEliminar_Click ; Tiempo m�ximo: 0,2</remarks>
Public Sub EliminarPonderacionSeleccionada()
    Dim irespuesta As Integer
    Dim iIndice As Integer

    If g_oAtributo.TipoPonderacion = EscalaDiscreta Then
        For iIndice = 1 To g_oAtributo.ListaPonderacion.Count
            If g_oAtributo.ListaPonderacion.Item(iIndice).ValorLista = g_oPondIntermedia.Item(CStr(sdbgValores.Columns("ORD").Value)).ValorLista Then
                'el atributo es de ponderac�n discreta y se va a eliminar una fila que puede tener valores asignados,luego, si continua borra su ponderaci�n
                irespuesta = oMensajes.PreguntaCambiarPonderacion
                If irespuesta = vbNo Then
                    Exit Sub
                Else
                    m_bBorrarPond = True
                    Set m_oListaEnEdicion = g_oPondIntermedia.Item(CStr(sdbgValores.Columns("ORD").Value))
                    m_oListaEnEdicion.IdOrden = sdbgValores.Columns("ORD").Value
                    Set m_oIBAseDatosEnEdicion = m_oListaEnEdicion
                    GoTo Continuar
                End If
            End If
        Next
    End If
    Set m_oListaEnEdicion = g_oPondIntermedia.Item(CStr(sdbgValores.Columns("ORD").Value))
    m_oListaEnEdicion.IdOrden = sdbgValores.Columns("ORD").Value
    Set m_oIBAseDatosEnEdicion = m_oListaEnEdicion
    
    irespuesta = oMensajes.PreguntaEliminar(sdbgValores.Columns("COD").caption & " " & sdbgValores.Columns("COD").Value)
Continuar:
    If irespuesta = vbYes Then
                            
        sdbgValores.SelBookmarks.Add sdbgValores.Bookmark
        g_oPondIntermedia.Remove (CStr(sdbgValores.Columns("ORD").Value))

        If sdbgValores.AddItemRowIndex(sdbgValores.Bookmark) > -1 Then
            sdbgValores.RemoveItem (sdbgValores.AddItemRowIndex(sdbgValores.Bookmark))
        Else
            sdbgValores.RemoveItem (0)
        End If
        
        mbHayCambios = True
    End If
    
    If sdbgValores.Rows > 0 Then
        If IsEmpty(sdbgValores.RowBookmark(sdbgValores.Row)) Then
            sdbgValores.Bookmark = sdbgValores.RowBookmark(sdbgValores.Row - 1)
        Else
            sdbgValores.Bookmark = sdbgValores.RowBookmark(sdbgValores.Row)
        End If
    End If
    sdbgValores.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgValores.SetFocus
End Sub
