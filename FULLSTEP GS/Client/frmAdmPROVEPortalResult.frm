VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAdmPROVEPortalResult 
   Caption         =   "DProveedores del portal"
   ClientHeight    =   5160
   ClientLeft      =   585
   ClientTop       =   4140
   ClientWidth     =   9405
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAdmPROVEPortalResult.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5160
   ScaleWidth      =   9405
   Begin VB.CommandButton cmdModoEdicion 
      Caption         =   "D&Edici�n"
      Height          =   345
      Left            =   8400
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   4740
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Height          =   345
      Left            =   3825
      TabIndex        =   2
      Top             =   4740
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   5025
      TabIndex        =   1
      Top             =   4740
      Visible         =   0   'False
      Width           =   1125
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgProve 
      Height          =   4680
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9435
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   12
      stylesets.count =   2
      stylesets(0).Name=   "Premium"
      stylesets(0).BackColor=   10079487
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmAdmPROVEPortalResult.frx":0CB2
      stylesets(0).AlignmentText=   0
      stylesets(0).AlignmentPicture=   3
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmAdmPROVEPortalResult.frx":1027
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   12
      Columns(0).Width=   2725
      Columns(0).Caption=   "C�digo portal"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   10079487
      Columns(0).StyleSet=   "Normal"
      Columns(1).Width=   2858
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "Denominaci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   10079487
      Columns(2).Width=   1667
      Columns(2).Caption=   "Actividad"
      Columns(2).Name =   "Actividad"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).Style=   4
      Columns(2).ButtonsAlways=   -1  'True
      Columns(3).Width=   2514
      Columns(3).Caption=   "Lugar"
      Columns(3).Name =   "Lugar"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   10079487
      Columns(4).Width=   847
      Columns(4).Caption=   "Prem."
      Columns(4).Name =   "Premium"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   10079487
      Columns(5).Width=   2778
      Columns(5).Caption=   "C�digo FSGS"
      Columns(5).Name =   "C�digo FSGS"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   16777215
      Columns(6).Width=   1138
      Columns(6).Caption=   "Aut."
      Columns(6).Name =   "Aut."
      Columns(6).Alignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   2
      Columns(6).HasBackColor=   -1  'True
      Columns(6).BackColor=   16777215
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "ID"
      Columns(7).Name =   "ID"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "PREM"
      Columns(8).Name =   "PREM"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   11
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Anterior"
      Columns(9).Name =   "Anterior"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   1138
      Columns(10).Caption=   "Detalle"
      Columns(10).Name=   "Detalle"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).Style=   4
      Columns(10).ButtonsAlways=   -1  'True
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "NIF"
      Columns(11).Name=   "NIF"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      _ExtentX        =   16642
      _ExtentY        =   8255
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAdmPROVEPortalResult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_vACN1Seleccionada As Variant
Public g_vACN2Seleccionada As Variant
Public g_vACN3Seleccionada As Variant
Public g_vACN4Seleccionada As Variant
Public g_vACN5Seleccionada As Variant
Public g_oGrupoMatNivel4 As CGrupoMatNivel4
Public g_sOrigen As String
Public g_sOrigenCarga As String
Public g_bHacerUnload As Boolean
Public g_sNombre As String
Public g_sPais As String
Public g_sProvi As String
Public g_bSoloFS As Boolean
Public g_bSoloAut As Boolean
Public g_bPremium As Boolean
Public g_bNoFS As Boolean

Private m_oProveedores As CProveedores

Private m_bError As Boolean
Private m_bEnlazarProveedores As Boolean
Private m_bA�adirProveedores As Boolean
Private m_bAutorizar As Boolean
Private m_bModoEdicion As Boolean
Private m_bConsultarProveMat As Boolean
Private bRespetarUpdate As Boolean
Private m_sQuitar As String
'Textos de la traducci�n
Private m_sCap(1 To 3) As String
Private msIdiProve As String


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADMPROVPOR_RES, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        m_sCap(1) = Ador(0).Value      '1 Edici�n
        cmdModoEdicion.caption = Ador(0).Value
        Ador.MoveNext
        m_sCap(2) = Ador(0).Value      '2 Consulta
        Ador.MoveNext
        Ador.MoveNext
        sdbgProve.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProve.Columns(0).caption = Ador(0).Value '5
        Ador.MoveNext
        sdbgProve.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProve.Columns(5).caption = Ador(0).Value '7
        Ador.MoveNext
        sdbgProve.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProve.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sdbgProve.Columns(10).caption = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        sdbgProve.Columns(2).caption = Ador(0).Value '14 Actividad
        Ador.MoveNext
        msIdiProve = Ador(0).Value
        
        Ador.MoveNext
        If gParametrosGenerales.gbSincronizacionMat = True Then
            sdbgProve.Columns(2).caption = Ador(0).Value '16 Material
        End If
        
        Ador.Close
        
    End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Public Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPortalIdentificar)) Is Nothing) Then
        m_bEnlazarProveedores = True
    Else
        m_bEnlazarProveedores = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPortalIncorporar)) Is Nothing) Then
        m_bA�adirProveedores = True
    Else
        m_bA�adirProveedores = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPortalActivarDesactivar)) Is Nothing) Then
        m_bAutorizar = True
    Else
        m_bAutorizar = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVEConsultar)) Is Nothing) Then
        m_bConsultarProveMat = True
    Else
        m_bConsultarProveMat = False
    End If
    
    If Not m_bAutorizar And Not m_bEnlazarProveedores And Not m_bA�adirProveedores Then
        cmdModoEdicion.Visible = False
    End If
    
    If Not m_bEnlazarProveedores Then
        sdbgProve.Columns("C�digo FSGS").Style = ssStyleEdit
    End If
    
    If Not m_bA�adirProveedores Then
        sdbgProve.Columns("C�digo FSGS").Locked = True
    End If
    
    If Not m_bAutorizar Then
        sdbgProve.Columns("Aut.").Locked = True
    End If
End Sub
''' <summary>
''' Comprueba q hay seleccionado un proveedor, q este enlazado y si lo esta, lo carga en la pantalla de a�adir proveedores a proceso.
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
Dim irespuesta As Integer
Dim oProve As CProveedor

    If sdbgProve.Rows = 0 Then Exit Sub

    If sdbgProve.Columns("C�digo FSGS").Value = "" Then
        oMensajes.EnlazarProveedor
        Exit Sub
    End If

    'Tanto si hemos contestado que No, como si venimos de frmACT_GMN habiendo contestado S�.
    Set oProve = oFSGSRaiz.generar_CProveedor
    oProve.Cod = sdbgProve.Columns("C�digo FSGS").Value
    
    Me.Hide
    
    frmSELPROVEAnya.CargarProveedorPortal oProve
    
    If g_sOrigen = "frmSELPROVEAnya" Then
        Unload frmAdmProvePortal
        Unload frmAdmPROVEPortalBuscar
    End If
    
    Unload Me
    
    Set g_oGrupoMatNivel4 = Nothing
    
    DoEvents
    
    frmSELPROVEAnya.Show
    frmSELPROVEAnya.SetFocus
    
End Sub

Private Sub cmdCancelar_Click()
    
     If g_sOrigen = "frmSELPROVEAnya" Then
        Unload frmAdmProvePortal
        Unload frmAdmPROVEPortalBuscar
     End If
    
    Unload Me
    
End Sub

Private Sub cmdModoEdicion_Click()

    If m_bModoEdicion Then
        If sdbgProve.DataChanged Then
            sdbgProve.Update
        End If
        sdbgProve.Columns("C�digo FSGS").Style = ssStyleEdit
        cmdModoEdicion.caption = m_sCap(1)
        m_bModoEdicion = False
        sdbgProve.Columns("Aut.").Locked = True
        sdbgProve.Columns("C�digo FSGS").Locked = True
        sdbgProve.Columns("C�digo").Locked = True

    Else
        sdbgProve.Columns("C�digo FSGS").Style = ssStyleEditButton

        If m_bAutorizar Then
            sdbgProve.Columns("Aut.").Locked = False
        End If

        If m_bEnlazarProveedores Then
            sdbgProve.Columns("C�digo FSGS").Locked = False
        End If

        If m_bA�adirProveedores Then
            sdbgProve.Columns("C�digo FSGS").Locked = False
        End If

       
        cmdModoEdicion.caption = m_sCap(2)
        m_bModoEdicion = True

    End If

End Sub
Private Sub Form_Activate()
    If g_sOrigen = "frmSELPROVEAnya" Then
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
    End If
End Sub

Private Sub Form_Load()
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    If Not gParametrosGenerales.gbPremium Or FSEPConf Then
        sdbgProve.Columns(4).Visible = False
        Me.Width = 8820
    Else
        sdbgProve.Columns(4).Visible = True
        Me.Width = 9525
    End If
    Me.Height = 5430
    Set m_oProveedores = oFSGSRaiz.generar_CProveedores
    
    If g_sOrigen = "frmSELPROVEAnya" Then
        
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        
    End If
    
    CargarRecursos
    ConfigurarSeguridad
    
    g_bHacerUnload = True
    m_bModoEdicion = False
    
    PonerFieldSeparator Me
End Sub

Private Sub Form_Resize()
    If Height < 1500 Then Exit Sub
    If Width < 3000 Then Exit Sub
    If Not gParametrosGenerales.gbPremium Or FSEPConf Then
        
        If Me.Height >= 900 Then
            sdbgProve.Height = Me.Height - 900
        End If
        If Me.Width >= 300 Then
            sdbgProve.Width = Me.Width - 300
        End If
        
        sdbgProve.Width = Me.Width - 100
        sdbgProve.Height = Me.Height - 830
        
        sdbgProve.Columns("C�digo").Width = 0.14 * sdbgProve.Width
        sdbgProve.Columns("Denominaci�n").Width = 0.28 * sdbgProve.Width
        sdbgProve.Columns("Actividad").Width = 0.095 * sdbgProve.Width
        sdbgProve.Columns("Lugar").Width = 0.14 * sdbgProve.Width
        sdbgProve.Columns("C�digo FSGS").Width = 0.14 * sdbgProve.Width
        sdbgProve.Columns("Aut.").Width = 0.08 * sdbgProve.Width - 75
        sdbgProve.Columns("Detalle").Width = 0.08 * sdbgProve.Width - 75
        
        cmdModoEdicion.Top = sdbgProve.Height + 75
        cmdAceptar.Top = cmdModoEdicion.Top
        cmdCancelar.Top = cmdModoEdicion.Top
        cmdModoEdicion.Left = Me.Width - cmdModoEdicion.Width - 105
        cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 50
        cmdCancelar.Left = Me.Width / 2 + 50

    Else

        sdbgProve.Width = Me.Width - 100
        sdbgProve.Height = Me.Height - 830
        
        sdbgProve.Columns("C�digo").Width = 0.13 * sdbgProve.Width
        sdbgProve.Columns("Denominaci�n").Width = 0.25 * sdbgProve.Width
        sdbgProve.Columns("Actividad").Width = 0.1 * sdbgProve.Width
        sdbgProve.Columns("Lugar").Width = 0.13 * sdbgProve.Width
        sdbgProve.Columns("Premium").Width = 0.06 * sdbgProve.Width
        sdbgProve.Columns("C�digo FSGS").Width = 0.13 * sdbgProve.Width
        sdbgProve.Columns("Aut.").Width = 0.08 * sdbgProve.Width - 100
        sdbgProve.Columns("Detalle").Width = 0.08 * sdbgProve.Width - 120
        
        cmdModoEdicion.Top = sdbgProve.Height + 75
        cmdAceptar.Top = cmdModoEdicion.Top
        cmdCancelar.Top = cmdModoEdicion.Top
        cmdModoEdicion.Left = Me.Width - cmdModoEdicion.Width - 105
        cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 50
        cmdCancelar.Left = Me.Width / 2 + 50
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_vACN1Seleccionada = Null
    g_vACN2Seleccionada = Null
    g_vACN3Seleccionada = Null
    g_vACN4Seleccionada = Null
    g_vACN5Seleccionada = Null
    Set g_oGrupoMatNivel4 = Nothing
    g_sNombre = ""
    g_sPais = ""
    g_sProvi = ""
    
    Set m_oProveedores = Nothing

    m_bModoEdicion = False
    
    If g_sOrigen = "frmSELPROVEAnya" Then
        frmSELPROVEAnya.bNoDescargar = True
    End If
    g_sOrigen = ""
    Me.Visible = False
End Sub

Private Sub sdbgProve_AfterUpdate(RtnDispErrMsg As Integer)
Dim vbm As Variant
Dim vbmAnt As Variant
Dim j As Integer
    
    If m_sQuitar <> "" Then
        For j = 0 To sdbgProve.Rows - 1
            
            vbm = sdbgProve.AddItemBookmark(j)
            
            If UCase(sdbgProve.Columns("C�digo").CellValue(vbm)) = UCase(m_sQuitar) Then
                If sdbgProve.Columns("C�digo FSGS").CellValue(vbm) <> "" Then
                    bRespetarUpdate = True
                    vbmAnt = sdbgProve.Bookmark
                    sdbgProve.Bookmark = vbm
                    sdbgProve.Columns("C�digo FSGS").Value = ""
                    sdbgProve.Columns("Anterior").Value = ""
                    sdbgProve.Columns("Aut.").Value = "0"
                    m_sQuitar = ""
                    sdbgProve.Update
                    sdbgProve.Bookmark = vbmAnt
                    bRespetarUpdate = False
                    Exit For
                End If
            End If
        Next
        m_sQuitar = ""
    End If
    

End Sub

Private Sub sdbgProve_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim oProveERPs As CProveERPs
Dim oProvesVinculados As CProveedores
Dim sProvesVinc As String
Dim oProve As CProveedor
Dim iRet As Integer


    If bRespetarUpdate Then Exit Sub
    If ColIndex <> 5 Then Exit Sub
    m_bError = False

    'Si es el mismo proveedor
    If sdbgProve.Columns("C�digo FSGS").Value = OldValue Then
        OldValue = sdbgProve.Columns("Anterior").Value
        If sdbgProve.Columns("C�digo FSGS").Value = OldValue Then Exit Sub
    End If

    If OldValue <> "" And sdbgProve.Columns("Anterior").Value = "" Then
        OldValue = ""
    End If

    If OldValue <> "" And sdbgProve.Columns("PREM").Value Then
        oMensajes.ProveedorEsPremium 1
        sdbgProve.Columns("C�digo FSGS").Value = OldValue
        sdbgProve.Columns("C�digo FSGS").Text = OldValue
        m_bError = True
        Exit Sub
    End If

    'Primero comprobamos si existe ese proveedor en fullstep
    If sdbgProve.Columns("C�digo FSGS").Value = "" Then

        Screen.MousePointer = vbHourglass

        If Not m_bEnlazarProveedores Then
            oMensajes.ImposibleEnlazarProvePortal
            'NOTA: Si se utiliza Cancel y DataCahged se bloquea la grid y no se disparan los eventos
            sdbgProve.Columns("C�digo FSGS").Value = OldValue
            sdbgProve.Columns("C�digo FSGS").Text = OldValue
            m_bError = True
'            Cancel = 1
'            sdbgProve.DataChanged = False
            Exit Sub
        End If

        frmEsperaPortal.Show
        DoEvents
        ' LOG cambios e Integracion. Pasamos el usuario que realiza la modificaci�n
        m_oProveedores.Usuario = basOptimizacion.gvarCodUsuario
        teserror = m_oProveedores.desenlazarproveedordelportal(sdbgProve.Columns("ID").Value, sdbgProve.Columns("C�digo").Value)
        Unload frmEsperaPortal

        Screen.MousePointer = vbNormal

        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            sdbgProve.Columns("C�digo FSGS").Value = OldValue
            sdbgProve.Columns("C�digo FSGS").Text = OldValue
            m_bError = True
 '           Cancel = 1
 '           sdbgProve.DataChanged = False
            Exit Sub
        Else
            sdbgProve.Columns(6).Value = 0
            sdbgProve.Columns("Anterior").Value = ""
            Exit Sub
        End If
    End If

    Screen.MousePointer = vbHourglass

    m_oProveedores.CargarTodosLosProveedoresDesde3 1, sdbgProve.Columns("C�digo FSGS").Value, , True

    If m_oProveedores.Count = 1 Then

        If Not m_bEnlazarProveedores Then
            oMensajes.ImposibleEnlazarProvePortal
            sdbgProve.Columns("C�digo FSGS").Value = OldValue
            sdbgProve.Columns("C�digo FSGS").Text = OldValue
            m_bError = True
 '          Cancel = 1
 '           sdbgProve.DataChanged = False
            Screen.MousePointer = vbNormal
            Exit Sub
        End If

        If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgProve.Columns("PREM").Value Then
            irespuesta = oMensajes.PreguntaEnlazarProveedor(sdbgProve.Columns("C�digo FSGS").Value, sdbgProve.Columns("C�digo").Value)
            If irespuesta = vbYes Then
                irespuesta = oMensajes.PreguntaEnlazarProveedorPremium(False)
            End If
        Else
            irespuesta = oMensajes.PreguntaEnlazarProveedor(sdbgProve.Columns("C�digo FSGS").Value, sdbgProve.Columns("C�digo").Value)
        End If
        If irespuesta = vbNo Then
            Screen.MousePointer = vbNormal
            sdbgProve.Columns("C�digo FSGS").Value = OldValue
            sdbgProve.Columns("C�digo FSGS").Text = OldValue
            m_bError = True
            'sdbgProve.DataChanged = False
            'Cancel = 1
            Exit Sub
        Else

            frmEsperaPortal.Show
            DoEvents
            ' LOG cambios e integraci�n, pasamos el usuario que realiza la modificaci�n
            m_oProveedores.Usuario = basOptimizacion.gvarCodUsuario
            m_oProveedores.CargarDatosProveedor sdbgProve.Columns("C�digo FSGS").Value
            If NullToStr(m_oProveedores.Item(sdbgProve.Columns("C�digo FSGS").Value).CodPortal) <> "" Then
                'Si esta cargado este proveedor tengo que quitarle el enlace
                m_sQuitar = m_oProveedores.Item(sdbgProve.Columns("C�digo FSGS").Value).CodPortal
            Else
                m_sQuitar = ""
            End If
            
            teserror = m_oProveedores.enlazarproveedordelportal(sdbgProve.Columns("C�digo FSGS").Value, sdbgProve.Columns("C�digo").Value, sdbgProve.Columns("ID").Value, OldValue)

            If teserror.NumError <> TESnoerror Then
                Unload frmEsperaPortal
                basErrores.TratarError teserror
                sdbgProve.Columns("C�digo FSGS").Value = OldValue
                sdbgProve.Columns("C�digo FSGS").Text = OldValue
                m_bError = True
  '              Cancel = 1
  '              sdbgProve.DataChanged = False
                Screen.MousePointer = vbNormal
                Exit Sub
            Else

                Unload frmEsperaPortal
                basSeguridad.RegistrarAccion accionessummit.ACCProvePortalIdentificar, "Prove:" & sdbgProve.Columns("C�digo FSGS").Value & "ProvePortal:" & sdbgProve.Columns("C�digo").Value
                sdbgProve.Columns("Aut.").Value = "1"
                Screen.MousePointer = vbNormal
                'si tiene permiso de consulta sobre material por proveedor
                If m_bConsultarProveMat And Not gParametrosGenerales.gbSincronizacionMat Then
                    'llamada a Asignaci�n de material a proveedor
                    frmAdmProveMat.sOrigen = "frmAdmProvePortalresult"
                    Set frmAdmProveMat.SelNode = frmAdmProveMat.tvwEstrMatMod.Nodes.Item(1)
                    frmAdmProveMat.Show
                End If
                sdbgProve.Columns("Anterior").Value = sdbgProve.Columns("C�digo FSGS").Value
                    
            End If
        End If
    Else

        If Not m_bA�adirProveedores Then
            oMensajes.PermisoDenegadoAnyadirProvePortal
            sdbgProve.Columns("C�digo FSGS").Value = OldValue
            sdbgProve.Columns("C�digo FSGS").Text = OldValue
            m_bError = True
            Screen.MousePointer = vbNormal
            Exit Sub
        End If

        If gParametrosGenerales.giINSTWEB = ConPortal And gParametrosGenerales.gbPremium And sdbgProve.Columns("PREM").Value Then
            irespuesta = oMensajes.PreguntaA�adirProveedorDelPortal(sdbgProve.Columns("C�digo FSGS").Value)
            If irespuesta = vbYes Then
                irespuesta = oMensajes.PreguntaEnlazarProveedorPremium(True)
            End If
        Else
            irespuesta = oMensajes.PreguntaA�adirProveedorDelPortal(sdbgProve.Columns("C�digo FSGS").Value)
        End If
        If irespuesta = vbYes Then
            frmEsperaPortal.Show
            DoEvents
            
            If gParametrosGenerales.gbActivarCodProveErp Then
                If Trim(Me.sdbgProve.Columns("NIF").Value) <> "" Then
                
                    Set oProveERPs = oFSGSRaiz.Generar_CProveERPs
                    Set oProvesVinculados = oProveERPs.ProveedoresVinculados(Me.sdbgProve.Columns("NIF").Value)
                    If oProvesVinculados.Count > 0 Then
                        sProvesVinc = ""
                        For Each oProve In oProvesVinculados
                            sProvesVinc = msIdiProve & ": " & oProve.Cod & " - " & oProve.Den & vbCrLf
                        Next
                        iRet = oMensajes.ExistenProveedoresVinculados(sProvesVinc)
                        If iRet = vbNo Then
                            sdbgProve.Columns("C�digo FSGS").Value = OldValue
                            sdbgProve.Columns("C�digo FSGS").Text = OldValue
                            m_bError = True
                            Screen.MousePointer = vbNormal
                            Set oProveERPs = Nothing
                            Set oProvesVinculados = Nothing
                            Exit Sub
                        End If
                    End If
                    Set oProveERPs = Nothing
                    Set oProvesVinculados = Nothing
                End If
            End If
            
            
            ' LOG de cambios e integraci�n. Pasamos el usuario que realiza la modificaci�n
           
            m_oProveedores.Usuario = basOptimizacion.gvarCodUsuario
            teserror = m_oProveedores.A�adirProveedorDelPortal(sdbgProve.Columns("ID").Value, sdbgProve.Columns("C�digo FSGS").Value, sdbgProve.Columns("C�digo").Value, OldValue)
            Unload frmEsperaPortal
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                sdbgProve.Columns("C�digo FSGS").Value = OldValue
                sdbgProve.Columns("C�digo FSGS").Text = OldValue
                m_bError = True
'                Cancel = 1
'                sdbgProve.DataChanged = False
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                basSeguridad.RegistrarAccion accionessummit.ACCProvePortalA�adir, "Prove:" & "ProvePortal:"
                sdbgProve.Columns("Aut.").Value = "1"
                'sdbgProve.Update

                'si tiene permiso de consulta sobre material por proveedor
                If m_bConsultarProveMat And Not gParametrosGenerales.gbSincronizacionMat Then
                    'llamada a Asignaci�n de material a proveedor
                    Screen.MousePointer = vbNormal
                    frmAdmProveMat.sOrigen = "frmAdmProvePortalresult"
                    Set frmAdmProveMat.SelNode = frmAdmProveMat.tvwEstrMatMod.Nodes.Item(1)
                    frmAdmProveMat.Show
                End If

                sdbgProve.Columns("Anterior").Value = sdbgProve.Columns("C�digo FSGS").Value
            End If

        Else

            sdbgProve.Columns("C�digo FSGS").Value = OldValue
            sdbgProve.Columns("C�digo FSGS").Text = OldValue
            m_bError = True
'            Cancel = 1
'            sdbgProve.DataChanged = False

        End If

    End If

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgProve_BtnClick()

    DoEvents
    If sdbgProve.Col <> 2 And sdbgProve.Col <> 5 And sdbgProve.Col <> 10 Then Exit Sub
    
    If sdbgProve.DataChanged = True Then
        sdbgProve.Update
        DoEvents
    End If
    
    If sdbgProve.Col = 2 Then
        MostrarActividadesDelProveedor sdbgProve.Columns("ID").Value
        sdbgProve.Col = 3
    Else
    
        If sdbgProve.Col = 5 Then
            frmPROVEBuscar.sOrigen = "frmAdmProvePortalresult"
            frmPROVEBuscar.Show 1
            
            sdbgProve.Col = 5
        Else
        
            frmPROVEPortalDetalle.g_sProveCod = sdbgProve.Columns(0).Value
            frmPROVEPortalDetalle.g_sProveDen = sdbgProve.Columns(1).Value
            frmPROVEPortalDetalle.ProveedorSeleccionado
            frmPROVEPortalDetalle.Hide
            frmPROVEPortalDetalle.Show
    
        End If
    End If

End Sub

Private Sub sdbgProve_Change()
Dim teserror As TipoErrorSummit

    If sdbgProve.Col = 6 Then
        If sdbgProve.Columns("C�digo FSGS").Value <> "" And sdbgProve.Columns("PREM").Value Then 'And sdbgProve.Columns(6).Value = 0 Then
            'oMensajes.ProveedorEsPremium 1
            sdbgProve.CancelUpdate
            sdbgProve.DataChanged = False
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If

    If sdbgProve.Col = 6 Then

        Screen.MousePointer = vbHourglass

        If sdbgProve.Columns("C�digo FSGS").Value = "" Then
            sdbgProve.CancelUpdate
            sdbgProve.DataChanged = False
            Screen.MousePointer = vbNormal
            Exit Sub
        End If

        If Abs(val(sdbgProve.Columns(6).Value)) = 1 Then
            frmEsperaPortal.Show
            DoEvents
            teserror = m_oProveedores.AutorizarProveedorDelPortal(sdbgProve.Columns("ID").Value)
            If teserror.NumError <> TESnoerror Then
                Unload frmEsperaPortal
                basErrores.TratarError teserror
                sdbgProve.DataChanged = False
                sdbgProve.CancelUpdate
            Else
                Unload frmEsperaPortal
                sdbgProve.Update

                basSeguridad.RegistrarAccion accionessummit.ACCProvePortalActivar, "ProvePortal:" & sdbgProve.Columns("C�digo").Value
                Screen.MousePointer = vbNormal
            End If

        Else
            frmEsperaPortal.Show
            DoEvents
            teserror = m_oProveedores.DesAutorizarProveedorDelPortal(sdbgProve.Columns("ID").Value, sdbgProve.Columns("C�digo").Value)

            If teserror.NumError <> TESnoerror Then
                Unload frmEsperaPortal
                basErrores.TratarError teserror
                sdbgProve.DataChanged = False
                sdbgProve.CancelUpdate
            Else
                Unload frmEsperaPortal
                sdbgProve.Update

                basSeguridad.RegistrarAccion accionessummit.ACCProvePortalDesActivar, "ProvePortal:" & sdbgProve.Columns("C�digo").Value

            End If
        End If



    End If

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbgProve_HeadClick(ByVal ColIndex As Integer)
Dim oProveedores As CProveedores
Dim oProve As CProveedor
Dim aut As Integer
Dim iOrden As Integer
    
    Screen.MousePointer = vbHourglass
    
    If sdbgProve.Columns("Aut.").Locked = True Then
               
        Select Case ColIndex
        
            Case 0
                    iOrden = 1
            Case 1
                    iOrden = 2
            Case 3
                    iOrden = 3
            Case 5
                    iOrden = 4
            Case 6
                    iOrden = 5
            Case Else
                    Screen.MousePointer = vbNormal
                    Exit Sub
        End Select
    
        sdbgProve.RemoveAll
        frmEsperaPortal.Show
        DoEvents
        Set oProveedores = oFSGSRaiz.generar_CProveedores

        If g_sOrigenCarga = "BuscarAct" Then
            oProveedores.CargarProveedoresPortalConActividad g_vACN1Seleccionada, g_vACN2Seleccionada, g_vACN3Seleccionada, g_vACN4Seleccionada, g_vACN5Seleccionada, iOrden
        Else
            oProveedores.CargarTodosLosProveedoresDelPortal g_vACN1Seleccionada, g_vACN2Seleccionada, g_vACN3Seleccionada, g_vACN4Seleccionada, g_vACN5Seleccionada, g_sNombre, g_sPais, g_sProvi, g_bSoloFS, g_bSoloAut, iOrden, g_bPremium, , g_bNoFS
        End If
        
        Unload frmEsperaPortal
        
        
        For Each oProve In oProveedores
            With oProve
                
                If .EstadoEnPortal = fsgsserver.TESPCActivo Then
                    aut = 1
                Else
                    aut = 0
                End If
                
                sdbgProve.AddItem .CodPortal & Chr(m_lSeparador) & .Den & Chr(m_lSeparador) & Chr(m_lSeparador) & .CodPais & " " & .DenProvi & Chr(m_lSeparador) & Chr(m_lSeparador) & .Cod & Chr(m_lSeparador) & aut & Chr(m_lSeparador) & .IDPortal & Chr(m_lSeparador) & .EsPremium & Chr(m_lSeparador) & .Cod & Chr(m_lSeparador) & Chr(m_lSeparador) & .NIF
    
            
            End With
        Next
    End If
    
    Set oProveedores = Nothing
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbgProve_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    If sdbgProve.DataChanged Then
        sdbgProve.Update
    End If

End Sub

Private Sub sdbgProve_RowLoaded(ByVal Bookmark As Variant)

    If sdbgProve.Columns(8).Value Then
        sdbgProve.Columns(4).Text = " "
        sdbgProve.Columns(4).CellStyleSet "Premium"
    End If

End Sub


Private Sub MostrarActividadesDelProveedor(ByVal Cia As Long)

        
    frmSELMAT.sOrigen = "frmAdmProvePortal2"
    frmEsperaPortal.Show
    DoEvents
    
    frmSELMAT.GenerarEstructuraActividadesProveedor Cia
    Unload frmEsperaPortal
    frmSELMAT.Show 1

    Screen.MousePointer = vbNormal

End Sub


