�Aviso de la proximidad de despublicaci�n de proceso!
Le comunicamos que el plazo para ofertar en el proceso @ANYO_PROCE/@COD_GMN1/@COD_PROCE @DEN_PROCE finaliza el @FEC_CIERRE a las @HORA_CIERRE


	Datos de la compa��a proveedora
	------------------------------------------------------
  	C�digo:		@COD_CIA
  	Denominaci�n: 	@DEN_CIA

	Datos del proveedor 
	------------------------------------------------------
	Nombre:		@NOM_PROVE
  	Apellidos:		@APE_PROVE
  	Cargo:		@CAR_PROVE
  	Tel�fono:		@TFNO1_PROVE
	Tel�fono 2:		@TFNO2_PROVE
	Fax:			@FAX_PROVE
	E-mail:		@EMAIL_PROVE
	Departamento:	@DEP_PROVE

	Datos del proceso
	------------------------------------------------------
	A�o:				@ANYO_PROCE
	C�digo:			@COD_PROCE
	Denominaci�n:		@DEN_PROCE

	Fecha de despublicaci�n
	------------------------------------------------------
	Fecha de cierre:		@FEC_CIERRE
	Hora de cierre:		@HORA_CIERRE
	Fecha UTC de cierre:	@FEC_UTC_CIERRE
	Hora UTC de cierre:	@HORA_UTC_CIERRE

<!--INICIO COMPRADOR-->
	Datos del comprador responsable 
	------------------------------------------------------
	Nombre: 	@NOM_COM
	Apellidos: 	@APE_COM
	E-mail:  	@EMAIL_COM
	Tel�fono: 	@TFNO1_COM
	Tel�fono 2:	@TFNO2_COM
	Fax:		@FAX_COM
	Cargo:	@CARGO_COM
<!--FIN COMPRADOR-->

Un saludo