VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmDatoAmbitoProce 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccione"
   ClientHeight    =   3300
   ClientLeft      =   1560
   ClientTop       =   3810
   ClientWidth     =   7065
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDatoAmbitoProce.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   7065
   Begin VB.PictureBox PicPresEsc 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   2670
      Left            =   0
      ScaleHeight     =   2670
      ScaleWidth      =   7035
      TabIndex        =   72
      Top             =   0
      Visible         =   0   'False
      Width           =   7035
      Begin VB.TextBox txtPresUniItem 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   3075
         TabIndex        =   75
         Top             =   990
         Width           =   1830
      End
      Begin VB.CheckBox chkPresPlanifEsc 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el presupuesto unitario planificado si est� disponible"
         ForeColor       =   &H00FFFFFF&
         Height          =   370
         Left            =   315
         TabIndex        =   74
         Top             =   405
         Width           =   6015
      End
      Begin VB.CheckBox chkPresEsc 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el precio de la �ltima adjudicaci�n si est� disponible"
         ForeColor       =   &H80000005&
         Height          =   400
         Left            =   315
         TabIndex        =   73
         Top             =   0
         Width           =   6015
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGridEscalados 
         Height          =   3000
         Left            =   270
         TabIndex        =   77
         Top             =   1770
         Width           =   6510
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   5
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmDatoAmbitoProce.frx":0CB2
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6403
         Columns(1).Caption=   "Cantidades directas"
         Columns(1).Name =   "DIR"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).NumberFormat=   "standard"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Caption=   "Cantidad inicial"
         Columns(2).Name =   "INI"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "standard"
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "Cantidad final"
         Columns(3).Name =   "FIN"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "standard"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Caption=   "Presupuesto Unitario"
         Columns(4).Name =   "PRES"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "#,##0.00####################"
         Columns(4).FieldLen=   256
         _ExtentX        =   11483
         _ExtentY        =   5292
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblPresUniItem 
         BackColor       =   &H00808000&
         Caption         =   "Presupuesto unitario para el item:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   315
         TabIndex        =   78
         Top             =   990
         Width           =   2535
      End
      Begin VB.Label LabelPresUni 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "Presupuestos unitarios escalados:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   315
         TabIndex        =   76
         Top             =   1440
         Width           =   2445
      End
   End
   Begin VB.PictureBox picPres 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   920
      Left            =   0
      ScaleHeight     =   915
      ScaleWidth      =   6615
      TabIndex        =   26
      Top             =   100
      Visible         =   0   'False
      Width           =   6615
      Begin VB.CheckBox chkPresPlanif 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el presupuesto unitario planificado si est� disponible"
         ForeColor       =   &H00FFFFFF&
         Height          =   370
         Left            =   2950
         TabIndex        =   71
         Top             =   520
         Width           =   3495
      End
      Begin VB.TextBox txtPres 
         Height          =   285
         Left            =   1200
         TabIndex        =   27
         Top             =   260
         Width           =   1600
      End
      Begin VB.CheckBox chkPres 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el precio de la �ltima adjudicaci�n si est� disponible"
         ForeColor       =   &H80000005&
         Height          =   400
         Left            =   2950
         TabIndex        =   28
         Top             =   30
         Width           =   3495
      End
      Begin VB.Label lblPres 
         BackStyle       =   0  'Transparent
         Caption         =   "Presupuesto:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   300
         Width           =   1000
      End
   End
   Begin VB.PictureBox picCant 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   500
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   6615
      TabIndex        =   30
      Top             =   100
      Visible         =   0   'False
      Width           =   6615
      Begin VB.CheckBox chkCant 
         BackColor       =   &H00808000&
         Caption         =   "Intoducir automaticamente la cantidad estimada para el articulo"
         ForeColor       =   &H80000005&
         Height          =   400
         Left            =   2880
         TabIndex        =   32
         Top             =   30
         Width           =   3495
      End
      Begin VB.TextBox txtCant 
         Height          =   285
         Left            =   1000
         TabIndex        =   31
         Top             =   60
         Width           =   1695
      End
      Begin VB.Label lblCant 
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   100
         Width           =   855
      End
   End
   Begin VB.PictureBox picBajMinPuja 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   550
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   5895
      TabIndex        =   66
      Top             =   100
      Visible         =   0   'False
      Width           =   5895
      Begin VB.TextBox txtBajMinPuja 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   1920
         TabIndex        =   68
         Top             =   60
         Width           =   1500
      End
      Begin VB.Label lblBajMinPuja 
         BackColor       =   &H00808000&
         Caption         =   "DBajada m�nima de puja:"
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   67
         Top             =   100
         Width           =   2295
      End
   End
   Begin VB.PictureBox picFechas 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   6675
      TabIndex        =   11
      Top             =   30
      Visible         =   0   'False
      Width           =   6680
      Begin VB.CommandButton cmdCalFecFin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6160
         Picture         =   "frmDatoAmbitoProce.frx":0CCE
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   130
         Width           =   315
      End
      Begin VB.TextBox txtFecFin 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4960
         TabIndex        =   16
         Top             =   115
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecIni 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2940
         Picture         =   "frmDatoAmbitoProce.frx":1258
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   130
         Width           =   315
      End
      Begin VB.TextBox txtFecIni 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1740
         TabIndex        =   13
         Top             =   115
         Width           =   1110
      End
      Begin VB.Label lblFecFin 
         BackStyle       =   0  'Transparent
         Caption         =   "Fin suministro"
         ForeColor       =   &H80000005&
         Height          =   240
         Left            =   3760
         TabIndex        =   15
         Top             =   130
         Width           =   1095
      End
      Begin VB.Label lblFecIni 
         BackStyle       =   0  'Transparent
         Caption         =   "Inicio suministro"
         ForeColor       =   &H80000005&
         Height          =   240
         Left            =   500
         TabIndex        =   12
         Top             =   130
         Width           =   1275
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   2130
      ScaleHeight     =   450
      ScaleWidth      =   2745
      TabIndex        =   0
      Top             =   2790
      Width           =   2745
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   2
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   1
         Top             =   60
         Width           =   1050
      End
   End
   Begin VB.PictureBox picGrupos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   6675
      TabIndex        =   18
      Top             =   100
      Visible         =   0   'False
      Width           =   6680
      Begin SSDataWidgets_B.SSDBCombo sdbcGrupoDen 
         Height          =   285
         Left            =   2640
         TabIndex        =   20
         Top             =   60
         Width           =   3795
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   4974
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2143
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6703
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGrupoCod 
         Height          =   285
         Left            =   1300
         TabIndex        =   19
         Top             =   60
         Width           =   1275
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1905
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4710
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   2240
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblGrupos 
         BackColor       =   &H00808000&
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   100
         Width           =   1200
      End
   End
   Begin VB.PictureBox picSubasta 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   800
      Left            =   120
      ScaleHeight     =   795
      ScaleWidth      =   6975
      TabIndex        =   40
      Top             =   30
      Visible         =   0   'False
      Width           =   6975
      Begin VB.TextBox txtHoraFinSub 
         Height          =   285
         Left            =   4100
         TabIndex        =   46
         Top             =   450
         Width           =   1125
      End
      Begin VB.CommandButton cmdCalFecFinSub 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2410
         Picture         =   "frmDatoAmbitoProce.frx":17E2
         Style           =   1  'Graphical
         TabIndex        =   45
         TabStop         =   0   'False
         Top             =   460
         Width           =   315
      End
      Begin VB.TextBox txtFecFinSub 
         Height          =   285
         Left            =   1240
         TabIndex        =   44
         Top             =   450
         Width           =   1110
      End
      Begin VB.TextBox txtHoraIniSub 
         Height          =   285
         Left            =   4100
         TabIndex        =   43
         Top             =   50
         Width           =   1125
      End
      Begin VB.CommandButton cmdCalFecInisub 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2410
         Picture         =   "frmDatoAmbitoProce.frx":1D6C
         Style           =   1  'Graphical
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   60
         Width           =   315
      End
      Begin VB.TextBox txtFecIniSub 
         Height          =   285
         Left            =   1240
         TabIndex        =   41
         Top             =   50
         Width           =   1110
      End
      Begin VB.Label lblHoraFinSub 
         BackStyle       =   0  'Transparent
         Caption         =   "Hora:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   3450
         TabIndex        =   50
         Top             =   500
         Width           =   1000
      End
      Begin VB.Label lblFecFinSub 
         BackStyle       =   0  'Transparent
         Caption         =   "Cierre Subasta:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   0
         TabIndex        =   49
         Top             =   500
         Width           =   1320
      End
      Begin VB.Label lblHoraIniSub 
         BackStyle       =   0  'Transparent
         Caption         =   "Hora:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   3450
         TabIndex        =   48
         Top             =   100
         Width           =   1000
      End
      Begin VB.Label lblFecIniSub 
         BackStyle       =   0  'Transparent
         Caption         =   "Inicio Subasta:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   0
         TabIndex        =   47
         Top             =   90
         Width           =   1320
      End
   End
   Begin VB.PictureBox picFechaLim 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   90
      ScaleHeight     =   495
      ScaleWidth      =   6945
      TabIndex        =   62
      Top             =   30
      Visible         =   0   'False
      Width           =   6945
      Begin VB.TextBox txtHoraLim 
         Height          =   285
         Left            =   5700
         TabIndex        =   69
         Top             =   120
         Width           =   1035
      End
      Begin VB.TextBox txtFecLim 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   3510
         TabIndex        =   63
         Top             =   120
         Width           =   1170
      End
      Begin VB.CommandButton cmdCalFecLim 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4740
         Picture         =   "frmDatoAmbitoProce.frx":22F6
         Style           =   1  'Graphical
         TabIndex        =   64
         TabStop         =   0   'False
         Top             =   130
         Width           =   315
      End
      Begin VB.Label lblFecLim 
         BackStyle       =   0  'Transparent
         Caption         =   "Hora:"
         ForeColor       =   &H80000005&
         Height          =   255
         Index           =   1
         Left            =   5190
         TabIndex        =   70
         Top             =   128
         Width           =   465
      End
      Begin VB.Label lblFecLim 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha l�mite de ofertas:"
         ForeColor       =   &H80000005&
         Height          =   240
         Index           =   0
         Left            =   75
         TabIndex        =   65
         Top             =   135
         Width           =   3045
      End
   End
   Begin VB.PictureBox picDest 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   6675
      TabIndex        =   3
      Top             =   30
      Visible         =   0   'False
      Width           =   6680
      Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
         Height          =   285
         Left            =   2640
         TabIndex        =   6
         Top             =   60
         Width           =   3795
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   7541
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2143
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2487
         Columns(2).Caption=   "Direcci�n"
         Columns(2).Name =   "DIR"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1958
         Columns(3).Caption=   "Poblaci�n"
         Columns(3).Name =   "POB"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1191
         Columns(4).Caption=   "CP"
         Columns(4).Name =   "CP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1270
         Columns(5).Caption=   "Pais"
         Columns(5).Name =   "PAIS"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1402
         Columns(6).Caption=   "Provincia"
         Columns(6).Name =   "PROVI"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   6703
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
         Height          =   285
         Left            =   1350
         TabIndex        =   5
         Top             =   60
         Width           =   1275
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   1905
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7011
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Direcci�n"
         Columns(2).Name =   "DIR"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2011
         Columns(3).Caption=   "Poblaci�n"
         Columns(3).Name =   "POB"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   979
         Columns(4).Caption=   "CP"
         Columns(4).Name =   "CP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1191
         Columns(5).Caption=   "Pais"
         Columns(5).Name =   "PAIS"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1455
         Columns(6).Caption=   "Provincia"
         Columns(6).Name =   "PROVI"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   2240
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblDest 
         BackColor       =   &H00808000&
         BackStyle       =   0  'Transparent
         Caption         =   "Destino:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   100
         Width           =   1200
      End
   End
   Begin VB.PictureBox picPago 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   6675
      TabIndex        =   7
      Top             =   480
      Visible         =   0   'False
      Width           =   6680
      Begin SSDataWidgets_B.SSDBCombo sdbcPagoCod 
         Height          =   285
         Left            =   1320
         TabIndex        =   9
         Top             =   60
         Width           =   1275
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1826
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   6033
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   2240
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483642
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPagoDen 
         Height          =   285
         Left            =   2640
         TabIndex        =   10
         Top             =   60
         Width           =   3795
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4815
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2302
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6703
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblPago 
         BackStyle       =   0  'Transparent
         Caption         =   "Forma de pago"
         ForeColor       =   &H80000005&
         Height          =   555
         Left            =   120
         TabIndex        =   8
         Top             =   100
         Width           =   1260
      End
   End
   Begin VB.PictureBox picProve 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   700
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   7005
      TabIndex        =   35
      Top             =   100
      Visible         =   0   'False
      Width           =   7000
      Begin VB.CheckBox chkProve 
         BackColor       =   &H00808000&
         Caption         =   "Introducir proveedor de la �ltima adjudicaci�n"
         ForeColor       =   &H80000005&
         Height          =   300
         Left            =   100
         TabIndex        =   51
         Top             =   400
         Width           =   5500
      End
      Begin VB.CommandButton cmdBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6600
         Picture         =   "frmDatoAmbitoProce.frx":2880
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   60
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
         Height          =   285
         Left            =   1440
         TabIndex        =   37
         Top             =   60
         Width           =   1275
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2725
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   6535
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   2240
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483642
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
         Height          =   285
         Left            =   2760
         TabIndex        =   38
         Top             =   60
         Width           =   3795
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6271
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2752
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6703
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblProveAct 
         BackStyle       =   0  'Transparent
         Caption         =   "Proveedor actual"
         ForeColor       =   &H80000005&
         Height          =   755
         Left            =   40
         TabIndex        =   39
         Top             =   40
         Width           =   1300
      End
   End
   Begin VB.PictureBox picUnidad 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   650
      Left            =   0
      ScaleHeight     =   645
      ScaleWidth      =   6675
      TabIndex        =   22
      Top             =   100
      Visible         =   0   'False
      Width           =   6680
      Begin VB.CheckBox chkUnidad 
         BackColor       =   &H00808000&
         Caption         =   "Introducir la unidad por defecto"
         ForeColor       =   &H80000005&
         Height          =   200
         Left            =   100
         TabIndex        =   34
         Top             =   450
         Width           =   6000
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcUnidadCod 
         Height          =   285
         Left            =   1320
         TabIndex        =   23
         Top             =   60
         Width           =   1275
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2143
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5900
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2249
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcUnidadDen 
         Height          =   285
         Left            =   2640
         TabIndex        =   25
         Top             =   60
         Width           =   3795
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5662
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2143
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6694
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblUnidad 
         BackColor       =   &H00808000&
         BackStyle       =   0  'Transparent
         Caption         =   "Unidad:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   100
         Width           =   1200
      End
   End
   Begin VB.PictureBox picSobre 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1545
      Left            =   0
      ScaleHeight     =   1545
      ScaleWidth      =   7035
      TabIndex        =   52
      Top             =   30
      Visible         =   0   'False
      Width           =   7035
      Begin VB.TextBox txtFechaSobre 
         Height          =   285
         Left            =   3570
         TabIndex        =   54
         Top             =   450
         Width           =   1215
      End
      Begin VB.CommandButton cmdSobre 
         Height          =   285
         Index           =   1
         Left            =   6465
         Picture         =   "frmDatoAmbitoProce.frx":2BC2
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   1080
         Width           =   315
      End
      Begin VB.CommandButton cmdSobre 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   4830
         Picture         =   "frmDatoAmbitoProce.frx":2C4F
         Style           =   1  'Graphical
         TabIndex        =   55
         TabStop         =   0   'False
         Top             =   450
         Width           =   315
      End
      Begin VB.TextBox txtSobre 
         Height          =   285
         Left            =   5775
         TabIndex        =   56
         Top             =   435
         Width           =   1035
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcUsuSobre 
         Height          =   285
         Left            =   300
         TabIndex        =   57
         Top             =   1080
         Width           =   6135
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   11642
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblSobre 
         BackColor       =   &H00808000&
         Caption         =   "Datos de Sobre 1:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   285
         Index           =   3
         Left            =   150
         TabIndex        =   61
         Top             =   90
         Width           =   5865
      End
      Begin VB.Label lblSobre 
         BackStyle       =   0  'Transparent
         Caption         =   "Usuario encargado de la apertura:"
         ForeColor       =   &H80000005&
         Height          =   240
         Index           =   2
         Left            =   150
         TabIndex        =   60
         Top             =   840
         Width           =   4635
      End
      Begin VB.Label lblSobre 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha prevista de apertura de sobre:"
         ForeColor       =   &H80000005&
         Height          =   240
         Index           =   0
         Left            =   150
         TabIndex        =   59
         Top             =   480
         Width           =   3015
      End
      Begin VB.Label lblSobre 
         BackStyle       =   0  'Transparent
         Caption         =   "Hora:"
         ForeColor       =   &H80000005&
         Height          =   255
         Index           =   1
         Left            =   5265
         TabIndex        =   53
         Top             =   495
         Width           =   465
      End
   End
End
Attribute VB_Name = "frmDatoAmbitoProce"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sIdiomas() As String

Public g_sOrigen As String
Public g_iCol As Integer
Public g_iLstGrupoItem As Integer

Private m_oDestinos As CDestinos
Private m_oPagos As CPagos
Private m_oProves As CProveedores
Private m_oUnidades As CUnidades
Private m_oUsuarios As CUsuarios
Private m_sIdiDestino As String
Private m_sIdiPago As String
Private m_sIdiProve As String
Private m_sIdiFecIni As String
Private m_sIdiFecFin As String
Private m_sIdiGrupo As String
Private m_sIdiUnidad As String
Private m_sIdiCant As String
Private m_sIdiPres As String
Private m_sIdiIniSubasta As String
Private m_sIdiFinSubasta As String
Private m_sIdiHoraIniSubasta As String
Private m_sIdiHoraFinSubasta As String
Private m_sIdiBajMinPuja As String
Private m_sMenCambiarGrupo As String


'Combos
Private m_bDestRespetarCombo As Boolean
Private m_bDestCargarComboDesde As Boolean
Private m_bPagoRespetarCombo As Boolean
Private m_bPagoCargarComboDesde As Boolean
Private m_bRespetarComboProve As Boolean
Private m_bGrupoRespetarCombo As Boolean
Private m_bUniRespetarCombo As Boolean
Private m_bUniCargarComboDesde As Boolean
Private m_bCancelar As Boolean
Private m_lGrupoID As Long

Public g_oPresupuestos1Nivel4 As CPresProyectosNivel4
Public g_oPresupuestos2Nivel4 As CPresContablesNivel4
Public g_oPresupuestos3Nivel4 As CPresConceptos3Nivel4
Public g_oPresupuestos4Nivel4 As CPresConceptos4Nivel4

Public g_oPresupuestos1Nivel4Nuevos As CPresProyectosNivel4
Public g_oPresupuestos2Nivel4Nuevos As CPresContablesNivel4
Public g_oPresupuestos3Nivel4Nuevos As CPresConceptos3Nivel4
Public g_oPresupuestos4Nivel4Nuevos As CPresConceptos4Nivel4

Public bRestProvMatComp As Boolean

Private m_sProceso As String
Private m_sGrupo As String
Private m_sItem As String
Private m_sPresUniItem As String
Public g_ogrupo As CGrupo

Private m_bRecalcularOfertas As Boolean

Private m_lIdPerfil As Long
Private m_bRDestPerf As Boolean
Private m_bRDest As Boolean
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
''' <summary>
''' Carga de los recursos de la pagina
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;Load de la pagina Tiempo m�ximo</remarks>
''' <revision>LTG 06/02/2012</revision>

Private Sub CargarRecursos()
    Dim Adores As Ador.Recordset
    Dim i As Integer


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_DATOAMBITOPROCE, basPublic.gParametrosInstalacion.gIdioma)

    If Not Adores Is Nothing Then
        ReDim m_sIdiomas(1 To 17)
        For i = 1 To 4
            m_sIdiomas(i) = Adores(0).Value
            Adores.MoveNext
        Next
        lblDest.caption = Adores(0).Value & ":"
        m_sIdiDestino = Adores(0).Value
        Adores.MoveNext
        lblPago.caption = Adores(0).Value & ":"
        m_sIdiPago = Adores(0).Value
        Adores.MoveNext
        lblProveAct.caption = Adores(0).Value & ":"
        m_sIdiProve = Adores(0).Value
        Adores.MoveNext
        lblFecIni.caption = Adores(0).Value & ":"
        m_sIdiFecIni = Adores(0).Value
        Adores.MoveNext
        lblFecFin.caption = Adores(0).Value & ":"
        m_sIdiFecFin = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns(0).caption = Adores(0).Value
        sdbcDestDen.Columns(1).caption = Adores(0).Value
        sdbcPagoCod.Columns(0).caption = Adores(0).Value
        sdbcPagoDen.Columns(1).caption = Adores(0).Value
        sdbcProveCod.Columns(0).caption = Adores(0).Value
        sdbcProveDen.Columns(1).caption = Adores(0).Value
        sdbcGrupoCod.Columns(0).caption = Adores(0).Value
        sdbcGrupoDen.Columns(1).caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns(1).caption = Adores(0).Value
        sdbcDestDen.Columns(0).caption = Adores(0).Value
        sdbcPagoCod.Columns(1).caption = Adores(0).Value
        sdbcPagoDen.Columns(0).caption = Adores(0).Value
        sdbcProveCod.Columns(1).caption = Adores(0).Value
        sdbcProveDen.Columns(0).caption = Adores(0).Value
        sdbcGrupoCod.Columns(1).caption = Adores(0).Value
        sdbcGrupoDen.Columns(0).caption = Adores(0).Value
        Adores.MoveNext
        cmdAceptar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(5) = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(6) = Adores(0).Value
        Adores.MoveNext
        lblGrupos.caption = Adores(0).Value & ":"
        m_sIdiGrupo = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(7) = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(8) = Adores(0).Value
        Adores.MoveNext
        'Valor
        Adores.MoveNext
        chkCant.caption = Adores(0).Value
        Adores.MoveNext
        chkPres.caption = Adores(0).Value
        chkPresEsc.caption = Adores(0).Value
        Adores.MoveNext
        lblCant.caption = Adores(0).Value & ":"
        m_sIdiCant = Adores(0).Value
        Adores.MoveNext
        lblPres.caption = Adores(0).Value & ":"
        m_sIdiPres = Adores(0).Value
        Adores.MoveNext
        chkUnidad.caption = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(9) = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(10) = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns(2).caption = Adores(0).Value
        sdbcDestDen.Columns(2).caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns(3).caption = Adores(0).Value
        sdbcDestDen.Columns(3).caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns(4).caption = Adores(0).Value
        sdbcDestDen.Columns(4).caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns(5).caption = Adores(0).Value
        sdbcDestDen.Columns(5).caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns(6).caption = Adores(0).Value
        sdbcDestDen.Columns(6).caption = Adores(0).Value
        Adores.MoveNext
        lblFecIniSub.caption = Adores(0).Value & ":"
        m_sIdiIniSubasta = Adores(0).Value
        Adores.MoveNext
        m_sIdiHoraIniSubasta = Adores(0).Value
        Adores.MoveNext
        lblFecFinSub.caption = Adores(0).Value & ":"
        m_sIdiFinSubasta = Adores(0).Value
        Adores.MoveNext
        m_sIdiHoraFinSubasta = Adores(0).Value
        Adores.MoveNext
        lblHoraIniSub.caption = Adores(0).Value
        lblHoraFinSub.caption = Adores(0).Value
        lblFecLim(1).caption = Adores(0).Value
        lblSobre(1).caption = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(11) = Adores(0).Value
        Adores.MoveNext
        chkProve.caption = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(12) = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(13) = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(14) = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(15) = Adores(0).Value
        Adores.MoveNext
        m_sIdiUnidad = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(16) = Adores(0).Value 'Titulo para sobre
        Adores.MoveNext
        lblSobre(0).caption = Adores(0).Value
        Adores.MoveNext
        lblSobre(2).caption = Adores(0).Value
        Adores.MoveNext
        lblSobre(3).caption = Adores(0).Value
        Adores.MoveNext
        lblFecLim(0).caption = Adores(0).Value
        Adores.MoveNext
        lblBajMinPuja.caption = Adores(0).Value & ":"  'Bajada m�nima de puja
        m_sIdiBajMinPuja = Adores(0).Value
        Adores.MoveNext
        m_sIdiomas(17) = Adores(0).Value  'Seleccione bajada m�nima de puja
        
        Adores.MoveNext
        chkPresPlanif.caption = Adores(0).Value
        chkPresPlanifEsc.caption = Adores(0).Value
        
        Adores.MoveNext
        m_sProceso = Adores(0).Value
        Adores.MoveNext
        m_sGrupo = Adores(0).Value
        Adores.MoveNext
        m_sItem = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("INI").caption = Adores(0).Value  ' Cantidad inicial
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("FIN").caption = Adores(0).Value  ' Cantidad final
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("ESC").caption = Adores(0).Value ' Candidades directas
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("PRES").caption = Adores(0).Value ' Presupuesto unitario
        Adores.MoveNext
        m_sPresUniItem = Adores(0).Value
        lblPresUniItem.caption = Adores(0).Value & ":"
        Adores.MoveNext
        LabelPresUni.caption = Adores(0).Value & ":"
        Adores.MoveNext
        m_sMenCambiarGrupo = Adores(0).Value
              
        Adores.Close
    End If
    
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>
''' Modifica la cantidad en los items seleccionados
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;cmdAceptar_Click() Tiempo m�ximo</remarks>
Private Sub ModifCantItems()
Dim oItem As CItem
Dim oItems As CItems
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim bErrores As Boolean
Dim vErrores As Variant
Dim irespuesta As Integer
Dim vPresActual As Variant
Dim dPresAbierto As Double
Dim dPresAbiertoMod As Double
Dim iPresAnu1 As Integer
Dim iPresAnu2 As Integer
Dim iPres1 As Integer
Dim iPres2 As Integer



If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set oItems = oFSGSRaiz.Generar_CItems
        Set oItem = oFSGSRaiz.Generar_CItem
        
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            
            With oItem
            
                .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                    Case EnItem
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                                .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                            Else
                                .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
                End Select
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                    Case EnItem
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                                .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                            Else
                                .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
                End Select
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                    Case EnItem
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                                .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                                .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                            Else
                                .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                                .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                            .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
                End Select
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                    Case EnItem
                            .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                    Case EnGrupo
                            .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                    Case EnProceso
                            .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
                End Select
            
                If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .ArticuloCod = Null
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
            
                If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .Descr = Null
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
            
                .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Cantidad = txtCant.Text
                .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .grupoCod = frmPROCE.sdbgItems.Columns("GRUPO").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .GrupoID = frmPROCE.sdbgItems.Columns("GRUPOID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .GMN1Cod = frmPROCE.sdbgItems.Columns("GMN1").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .GMN2Cod = frmPROCE.sdbgItems.Columns("GMN2").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .GMN3Cod = frmPROCE.sdbgItems.Columns("GMN3").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .GMN4Cod = frmPROCE.sdbgItems.Columns("GMN4").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End With
            
            oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , NullToStr(oItem.grupoCod), oItem.FECACT, sGMN1:=oItem.GMN1Cod, sGMN2:=oItem.GMN2Cod, sGMN3:=oItem.GMN3Cod, sGMN4:=oItem.GMN4Cod, lGrupoID:=oItem.GrupoID
        
        Next

        If gParametrosGenerales.gbPresupuestosAut Then
            Screen.MousePointer = vbHourglass
            teserror = oItems.ModificarValores(False, chkCant.Value, , , , , , , , , , , , basOptimizacion.gvarCodUsuario, m_bRecalcularOfertas)
        Else
            'Se comprueba si alguno de los presupuestos est� definido a nivel de proceso o del grupo de los items, si tienen alg�n presupuesto distribuido
            If gParametrosGenerales.gbUsarPres1 Then
                If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 Then
                    iPresAnu1 = 1
                Else
                    If frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1 And frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 Then
                        iPresAnu1 = 2
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres2 Then
                If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                    iPresAnu2 = 1
                Else
                    If frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2 And frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                        iPresAnu2 = 2
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres3 Then
                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                    iPres1 = 1
                Else
                    If frmPROCE.g_oGrupoSeleccionado.DefPresTipo1 And frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                        iPres1 = 2
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres4 Then
                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                    iPres2 = 1
                Else
                    If frmPROCE.g_oGrupoSeleccionado.DefPresTipo2 And frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                        iPres2 = 2
                    End If
                End If
            End If
            
            If iPresAnu1 > 0 Or iPresAnu2 > 0 Or iPres1 > 0 Or iPres2 > 0 Then
                irespuesta = oMensajes.PreguntaReasignarPresupuestos
                If irespuesta = vbNo Then
                    Screen.MousePointer = vbHourglass
                    teserror = oItems.ModificarValores(False, chkCant.Value, , , , , , , , , , , , basOptimizacion.gvarCodUsuario, m_bRecalcularOfertas)
                Else
                    'pantallas de presupuestos
                    'Los presupuestos que no est�n distribuidos no hace falta redistribuirlos
                    vPresActual = oItems.DevolverPresupuestoActual(IIf(txtCant.Text <> "", True, False), chkCant.Value, False, False)
                    '*****PRESANU1
                    If iPresAnu1 > 0 Then
                        If iPresAnu1 = 1 Then
                            dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                        Else
                            If iPresAnu1 = 2 Then
                                dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                            End If
                        End If
                        'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                        dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                        frmPRESAsig.g_iAmbitoPresup = iPresAnu1
                        frmPRESAsig.g_iTipoPres = 1
                        frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo1
                        frmPRESAsig.g_sOrigen = "CANT_ITEMS"
                        frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                        frmPRESAsig.g_dblAsignado = dPresAbierto
                        frmPRESAsig.g_bHayPres = True
                        frmPRESAsig.g_bHayPresBajaLog = False
                        frmPRESAsig.g_bModif = True
                        frmPRESAsig.Show 1
                    End If
                    '*****PRESANU2
                    If iPresAnu2 > 0 Then
                        If iPresAnu2 = 1 Then
                            dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                        Else
                            If iPresAnu2 = 2 Then
                                dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                            End If
                        End If
                        'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                        dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                        frmPRESAsig.g_iTipoPres = 2
                        frmPRESAsig.g_iAmbitoPresup = iPresAnu2
                        frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo2
                        frmPRESAsig.g_sOrigen = "CANT_ITEMS"
                        frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                        frmPRESAsig.g_dblAsignado = dPresAbierto
                        frmPRESAsig.g_bHayPres = True
                        frmPRESAsig.g_bHayPresBajaLog = False
                        frmPRESAsig.g_bModif = True
                        frmPRESAsig.Show 1
                    End If
                    '*****PRES1
                    If iPres1 > 0 Then
                        If iPres1 = 1 Then
                            dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                        Else
                            If iPres1 = 2 Then
                                dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                            End If
                        End If
                        'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                        dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                        frmPRESAsig.g_iTipoPres = 3
                        frmPRESAsig.g_iAmbitoPresup = iPres1
                        frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo1
                        frmPRESAsig.g_sOrigen = "CANT_ITEMS"
                        frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                        frmPRESAsig.g_dblAsignado = dPresAbierto
                        frmPRESAsig.g_bHayPres = True
                        frmPRESAsig.g_bHayPresBajaLog = False
                        frmPRESAsig.g_bModif = True
                        frmPRESAsig.Show 1
                    End If
                    '*****PRES2
                    If iPres2 > 0 Then
                        If iPres2 = 1 Then
                            dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                        Else
                            If iPres2 = 2 Then
                                dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                            End If
                        End If
                        'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                        dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                        frmPRESAsig.g_iTipoPres = 4
                        frmPRESAsig.g_iAmbitoPresup = iPres2
                        frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo2
                        frmPRESAsig.g_sOrigen = "CANT_ITEMS"
                        frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                        frmPRESAsig.g_dblAsignado = dPresAbierto
                        frmPRESAsig.g_bHayPres = True
                        frmPRESAsig.g_bHayPresBajaLog = False
                        frmPRESAsig.g_bModif = True
                        frmPRESAsig.Show 1
                    End If
                    Screen.MousePointer = vbHourglass
                    teserror = oItems.ModificarValores(False, chkCant.Value, , , g_oPresupuestos1Nivel4, g_oPresupuestos2Nivel4, g_oPresupuestos3Nivel4, g_oPresupuestos4Nivel4, True, , , , , basOptimizacion.gvarCodUsuario, m_bRecalcularOfertas)
                End If
            Else
                Screen.MousePointer = vbHourglass
                teserror = oItems.ModificarValores(False, chkCant.Value, , , , , , , , , , , , basOptimizacion.gvarCodUsuario, m_bRecalcularOfertas)
            End If
        End If
        
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESVolumenAdjDirSuperado Then
            basErrores.TratarError teserror
            
            Set oItems = Nothing
            Set oItem = Nothing
            Set g_oPresupuestos1Nivel4 = Nothing
            Set g_oPresupuestos2Nivel4 = Nothing
            Set g_oPresupuestos3Nivel4 = Nothing
            Set g_oPresupuestos4Nivel4 = Nothing
            Unload Me
            frmPROCE.sdbgItems.SelBookmarks.RemoveAll
            Exit Sub
        ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
            basErrores.TratarError teserror
        End If
    Else
        bErrores = teserror.Arg2
        vErrores = teserror.Arg1
        If bErrores Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
            Screen.MousePointer = vbHourglass
        End If
    End If
    
    
    LockWindowUpdate frmPROCE.hWnd 'Para bloquear
    Dim bmAct As Variant
    bmAct = frmPROCE.sdbgItems.Bookmark
        
    If teserror.NumError = TESVolumenAdjDirSuperado Then
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            frmPROCE.sdbgItems.Columns("CANT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Cantidad
            frmPROCE.sdbgItems.Columns("PRES").Value = CDbl(frmPROCE.sdbgItems.Columns("CANT").Value) * CDbl(frmPROCE.sdbgItems.Columns("PREC").Value)
            frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            'Recalcular los presupuestos
            frmPROCE.g_dPresGrupo = (frmPROCE.g_dPresGrupo - frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) + frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.g_oProcesoSeleccionado.PresAbierto = (frmPROCE.g_oProcesoSeleccionado.PresAbierto - frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) + frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.sdbgItems.Columns("PRESACTUAL").Value = frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.sdbgItems.Columns("CANTOLD").Value = frmPROCE.sdbgItems.Columns("CANT").Value
            frmPROCE.sdbgItems.Columns("PRECOLD").Value = frmPROCE.sdbgItems.Columns("PREC").Value
        Next
    Else
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            If vErrores(1, i) = 0 Then 'Ning�n error
                'Registro la acci�n (cambio en la cantidad del item)
                If frmPROCE.sdbgItems.Columns("CANT").Value <> oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Cantidad Then
                    basSeguridad.RegistrarAccion ACCCondOfeCantidad, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Cod:" & frmPROCE.sdbcProceCod.Value & " Id:" & frmPROCE.sdbgItems.Columns("ID").Value
                End If
                frmPROCE.sdbgItems.Columns("CANT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Cantidad
                'frmPROCE.sdbgItems.Columns("PRES").Value = CDbl(frmPROCE.sdbgItems.Columns("CANT").Value) * CDbl(frmPROCE.sdbgItems.Columns("PREC").Value)
                frmPROCE.sdbgItems.Columns("PREC").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Precio
                frmPROCE.sdbgItems.Columns("PRES").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Presupuesto
                
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
                'Recalcular los presupuestos
                frmPROCE.g_dPresGrupo = (frmPROCE.g_dPresGrupo - val(frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))) + val(frmPROCE.sdbgItems.Columns("PRES").Value)
                frmPROCE.g_oProcesoSeleccionado.PresAbierto = (frmPROCE.g_oProcesoSeleccionado.PresAbierto - val(frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))) + val(frmPROCE.sdbgItems.Columns("PRES").Value)
                frmPROCE.sdbgItems.Columns("PRESACTUAL").Value = frmPROCE.sdbgItems.Columns("PRES").Value
                frmPROCE.sdbgItems.Columns("CANTOLD").Value = frmPROCE.sdbgItems.Columns("CANT").Value
                frmPROCE.sdbgItems.Columns("PRECOLD").Value = frmPROCE.sdbgItems.Columns("PREC").Value
                
                basSeguridad.RegistrarAccion accionessummit.ACCProceModifMultItems, "CODIGO:" & frmPROCE.sdbgItems.Columns("COD").Value & " Cant:" & frmPROCE.sdbgItems.Columns("CANT").Value & " Cant:" & frmPROCE.sdbgItems.Columns("CANT").Value
                
            End If
        Next
        'Registramos llamada a recalcular
        If m_bRecalcularOfertas Then
             basSeguridad.RegistrarAccion accionessummit.ACCCondOfeRecalculo, "Llamada desde:Apertura Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Cod:" & frmPROCE.sdbcProceCod.Value
        End If
        
    End If
    
    frmPROCE.sdbgItems.Bookmark = bmAct
    frmPROCE.sdbgItems.Update
    frmPROCE.g_bUpdate = True

    LockWindowUpdate 0& ' Para desbloquear

    
    
    frmPROCE.lblPresGrupo = Format(frmPROCE.g_dPresGrupo, "#,##0.00####################")
    frmPROCE.lblPresTotalProce = Format(frmPROCE.g_oProcesoSeleccionado.PresAbierto, "#,##0.00####################")
    'frmPROCE.sdbgItems.SelBookmarks.RemoveAll
    frmPROCE.sdbgItems.Update
    'Reestablecemos la marca de control de cambios del grid
    frmPROCE.g_bUpdate = True
    
    Screen.MousePointer = vbNormal

    Set oItem = Nothing
    Set oItems = Nothing
    Set g_oPresupuestos1Nivel4 = Nothing
    Set g_oPresupuestos2Nivel4 = Nothing
    Set g_oPresupuestos3Nivel4 = Nothing
    Set g_oPresupuestos4Nivel4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifCantItems", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Modifica el precio en los items seleccionados
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;cmdAceptar_Click() Tiempo m�ximo</remarks>

Private Sub ModifPrecItems()
    Dim oItem As CItem
    Dim oItems As CItems
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim bErrores As Boolean
    Dim vErrores As Variant
    Dim irespuesta As Integer
    Dim vPresActual As Variant
    Dim dPresDif As Double
    Dim dPresAbierto As Double
    Dim dPresAbiertoMod As Double
    Dim iPresAnu1 As Integer
    Dim iPresAnu2 As Integer
    Dim iPres1 As Integer
    Dim iPres2 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oItems = oFSGSRaiz.Generar_CItems
    Set oItem = oFSGSRaiz.Generar_CItem
    
    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
        
        With oItem
        
            .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                Case EnItem
                        .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                            .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                        Else
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                Case EnItem
                        .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                            .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                        Else
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                Case EnItem
                        .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                            .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                            .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                        Else
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                        .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                Case EnItem
                        .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                Case EnGrupo
                        .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                Case EnProceso
                        .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
            End Select
            
            If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                .ArticuloCod = Null
                .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                .Descr = Null
                .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            
            .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .Precio = txtPres.Text
            .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .grupoCod = frmPROCE.sdbgItems.Columns("GRUPO").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .GrupoID = frmPROCE.sdbgItems.Columns("GRUPOID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        
        End With
        
        oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , oItem.grupoCod, oItem.FECACT, lGrupoID:=oItem.GrupoID
    
    Next
    
    If gParametrosGenerales.gbPresupuestosAut Then
        Screen.MousePointer = vbHourglass
        teserror = oItems.ModificarValores(False, False, chkPres.Value, , , , , , , , , , chkPresPlanif.Value, basOptimizacion.gvarCodUsuario)
    Else
        'Se comprueba si alguno de los presupuestos est� definido a nivel de proceso o del grupo de los items, si tienen alg�n presupuesto distribuido
        If gParametrosGenerales.gbUsarPres1 Then
            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 Then
                iPresAnu1 = 1
            Else
                If frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1 And frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 Then
                    iPresAnu1 = 2
                End If
            End If
        End If
        If gParametrosGenerales.gbUsarPres2 Then
            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                iPresAnu2 = 1
            Else
                If frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2 And frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                    iPresAnu2 = 2
                End If
            End If
        End If
        If gParametrosGenerales.gbUsarPres3 Then
            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                iPres1 = 1
            Else
                If frmPROCE.g_oGrupoSeleccionado.DefPresTipo1 And frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                    iPres1 = 2
                End If
            End If
        End If
        If gParametrosGenerales.gbUsarPres4 Then
            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                iPres2 = 1
            Else
                If frmPROCE.g_oGrupoSeleccionado.DefPresTipo2 And frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                    iPres2 = 2
                End If
            End If
        End If
        
        If iPresAnu1 > 0 Or iPresAnu2 > 0 Or iPres1 > 0 Or iPres2 > 0 Then
            irespuesta = oMensajes.PreguntaReasignarPresupuestos
            If irespuesta = vbNo Then
                Screen.MousePointer = vbHourglass
                teserror = oItems.ModificarValores(False, False, chkPres.Value, , , , , , , , , , chkPresPlanif.Value, basOptimizacion.gvarCodUsuario)
            Else
                'pantallas de presupuestos
                'Los presupuestos que no est�n distribuidos no hace falta redistribuirlos
                vPresActual = oItems.DevolverPresupuestoActual(False, False, IIf(txtPres.Text <> "", True, False), chkPres.Value, , , , , chkPresPlanif.Value)
                '*****PRESANU1
                If iPresAnu1 > 0 Then
                    If iPresAnu1 = 1 Then
                        dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                    Else
                        If iPresAnu1 = 2 Then
                            dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                        End If
                    End If
                    'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                    dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                    'Pendiente=AbiertoDespuesMod - Abierto
                    dPresDif = dPresAbiertoMod - dPresAbierto
                    frmPRESAsig.g_iAmbitoPresup = iPresAnu1
                    frmPRESAsig.g_iTipoPres = 1
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo1
                    frmPRESAsig.g_sOrigen = "PRES_ITEMS"
                    frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                    frmPRESAsig.g_dblAsignado = dPresAbierto
                    frmPRESAsig.g_bHayPres = True
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_bModif = True
                    frmPRESAsig.Show 1
                End If
                '*****PRESANU2
                If iPresAnu2 > 0 Then
                    If iPresAnu2 = 1 Then
                        dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                    Else
                        If iPresAnu2 = 2 Then
                            dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                        End If
                    End If
                    'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                    dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                    'Pendiente=AbiertoDespuesMod - Abierto
                    dPresDif = dPresAbiertoMod - dPresAbierto
                    frmPRESAsig.g_iTipoPres = 2
                    frmPRESAsig.g_iAmbitoPresup = iPresAnu2
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo2
                    frmPRESAsig.g_sOrigen = "PRES_ITEMS"
                    frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                    frmPRESAsig.g_dblAsignado = dPresAbierto
                    frmPRESAsig.g_bHayPres = True
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_bModif = True
                    frmPRESAsig.Show 1
                End If
                '*****PRES1
                If iPres1 > 0 Then
                    If iPres1 = 1 Then
                        dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                    Else
                        If iPres1 = 2 Then
                            dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                        End If
                    End If
                    'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                    dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                    'Pendiente=AbiertoDespuesMod - Abierto
                    dPresDif = dPresAbiertoMod - dPresAbierto
                    frmPRESAsig.g_iTipoPres = 3
                    frmPRESAsig.g_iAmbitoPresup = iPres1
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo1
                    frmPRESAsig.g_sOrigen = "PRES_ITEMS"
                    frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                    frmPRESAsig.g_dblAsignado = dPresAbierto
                    frmPRESAsig.g_bHayPres = True
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_bModif = True
                    frmPRESAsig.Show 1
                End If
                '*****PRES2
                If iPres2 > 0 Then
                    If iPres2 = 1 Then
                        dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                    Else
                        If iPres2 = 2 Then
                            dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                        End If
                    End If
                    'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                    dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                    'Pendiente=AbiertoDespuesMod - Abierto
                    dPresDif = dPresAbiertoMod - dPresAbierto
                    frmPRESAsig.g_iTipoPres = 4
                    frmPRESAsig.g_iAmbitoPresup = iPres2
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo2
                    frmPRESAsig.g_sOrigen = "PRES_ITEMS"
                    frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                    frmPRESAsig.g_dblAsignado = dPresAbierto
                    frmPRESAsig.g_bHayPres = True
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_bModif = True
                    frmPRESAsig.Show 1
                End If
                Screen.MousePointer = vbHourglass
                teserror = oItems.ModificarValores(False, False, chkPres.Value, , g_oPresupuestos1Nivel4, g_oPresupuestos2Nivel4, g_oPresupuestos3Nivel4, g_oPresupuestos4Nivel4, False, True, , , chkPresPlanif.Value, basOptimizacion.gvarCodUsuario)
            End If
        Else
            Screen.MousePointer = vbHourglass
            teserror = oItems.ModificarValores(False, False, chkPres.Value, , , , , , , , , , chkPresPlanif.Value, basOptimizacion.gvarCodUsuario)
        End If
    End If
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESVolumenAdjDirSuperado Then
            basErrores.TratarError teserror
            Set oItems = Nothing
            Set oItem = Nothing
            Set g_oPresupuestos1Nivel4 = Nothing
            Set g_oPresupuestos2Nivel4 = Nothing
            Set g_oPresupuestos3Nivel4 = Nothing
            Set g_oPresupuestos4Nivel4 = Nothing
            Unload Me
            frmPROCE.sdbgItems.SelBookmarks.RemoveAll
            Exit Sub
        ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
            basErrores.TratarError teserror
        End If
    Else
        bErrores = teserror.Arg2
        vErrores = teserror.Arg1
        If bErrores Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
            Screen.MousePointer = vbHourglass
        End If
    End If
        
    If teserror.NumError = TESVolumenAdjDirSuperado Then
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            frmPROCE.sdbgItems.Columns("PREC").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Precio
            frmPROCE.sdbgItems.Columns("PRES").Value = CDbl(frmPROCE.sdbgItems.Columns("CANT").Value) * CDbl(frmPROCE.sdbgItems.Columns("PREC").Value)
            frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            'Recalcular los presupuestos
            frmPROCE.g_dPresGrupo = (frmPROCE.g_dPresGrupo - frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) + frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.g_oProcesoSeleccionado.PresAbierto = (frmPROCE.g_oProcesoSeleccionado.PresAbierto - frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) + frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.sdbgItems.Columns("PRESACTUAL").Value = frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.sdbgItems.Columns("CANTOLD").Value = frmPROCE.sdbgItems.Columns("CANT").Value
            frmPROCE.sdbgItems.Columns("PRECOLD").Value = frmPROCE.sdbgItems.Columns("PREC").Value
        Next
    Else
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            If vErrores(1, i) = 0 Then 'Ning�n error
                frmPROCE.sdbgItems.Columns("PREC").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Precio
                frmPROCE.sdbgItems.Columns("PRES").Value = CDbl(frmPROCE.sdbgItems.Columns("CANT").Value) * CDbl(frmPROCE.sdbgItems.Columns("PREC").Value)
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
                'Recalcular los presupuestos
                frmPROCE.g_dPresGrupo = (frmPROCE.g_dPresGrupo - frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) + frmPROCE.sdbgItems.Columns("PRES").Value
                frmPROCE.g_oProcesoSeleccionado.PresAbierto = (frmPROCE.g_oProcesoSeleccionado.PresAbierto - frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) + frmPROCE.sdbgItems.Columns("PRES").Value
                frmPROCE.sdbgItems.Columns("PRESACTUAL").Value = frmPROCE.sdbgItems.Columns("PRES").Value
                frmPROCE.sdbgItems.Columns("CANTOLD").Value = frmPROCE.sdbgItems.Columns("CANT").Value
                frmPROCE.sdbgItems.Columns("PRECOLD").Value = frmPROCE.sdbgItems.Columns("PREC").Value
            End If
        Next
    End If
    
    frmPROCE.lblPresGrupo = Format(frmPROCE.g_dPresGrupo, "#,##0.00####################")
    frmPROCE.lblPresTotalProce = Format(frmPROCE.g_oProcesoSeleccionado.PresAbierto, "#,##0.00####################")
    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
    frmPROCE.sdbgItems.Update
    frmPROCE.g_bUpdate = True
    
    Screen.MousePointer = vbNormal

    Set oItem = Nothing
    Set oItems = Nothing
    Set g_oPresupuestos1Nivel4 = Nothing
    Set g_oPresupuestos2Nivel4 = Nothing
    Set g_oPresupuestos3Nivel4 = Nothing
    Set g_oPresupuestos4Nivel4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifPrecItems", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Modifica la fecha fin suministro en los items seleccionados
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo: 0,2</remarks>
Private Sub ModifFecFinItems()
Dim oItem As CItem
Dim oItems As CItems
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim bErrores As Boolean
Dim vErrores As Variant
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set oItems = oFSGSRaiz.Generar_CItems
        Set oItem = oFSGSRaiz.Generar_CItem
        
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            
            With oItem
            
                'Comprueba si la nueva fecha de fin es menor que alguna de las fechas
                'de inicio de suministro de los �tems seleccionados.
                If CDate(txtFecFin.Text) < CDate(frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) Then
                    oMensajes.FechaDesdeMayorFechaHasta 2
                    Set oItems = Nothing
                    Set oItem = Nothing
                    Unload Me
                    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                    Exit Sub
                End If
            
                .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FechaFinSuministro = txtFecFin.Text
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                    Case EnItem
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                                .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                            Else
                                .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
                End Select
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                    Case EnItem
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                                .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                            Else
                                .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
                End Select
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                    Case EnItem
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                                .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                            Else
                                .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                End Select
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                    Case EnItem
                            .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                    Case EnGrupo
                            .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                    Case EnProceso
                            .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
                End Select
            
                If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .ArticuloCod = Null
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .Descr = Null
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                
                .grupoCod = frmPROCE.g_oGrupoSeleccionado.Codigo
            End With
            
            oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , oItem.grupoCod, oItem.FECACT
        
        Next

        Screen.MousePointer = vbHourglass
        teserror = oItems.ModificarValores(, , , , , , , , , , , , , basOptimizacion.gvarCodUsuario)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            
            If teserror.NumError <> TESVolumenAdjDirSuperado Then
                basErrores.TratarError teserror
                Set oItems = Nothing
                Set oItem = Nothing
                Unload Me
                frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                Exit Sub
            ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
                basErrores.TratarError teserror
            End If
        Else
            bErrores = teserror.Arg2
            vErrores = teserror.Arg1
            If bErrores Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
                Screen.MousePointer = vbHourglass
            End If
        End If
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                frmPROCE.sdbgItems.Columns("FIN").Value = txtFecFin.Text
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            Next
        Else
            bErrores = teserror.Arg2
            vErrores = teserror.Arg1
            If bErrores Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
                Screen.MousePointer = vbHourglass
            End If
            
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                If vErrores(1, i) = 0 Then 'Ning�n error
                    frmPROCE.sdbgItems.Columns("FIN").Value = txtFecFin.Text
                    frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
                End If
            Next
        End If
        
        frmPROCE.sdbgItems.SelBookmarks.RemoveAll
        frmPROCE.sdbgItems.Update
        frmPROCE.g_bUpdate = True
        Screen.MousePointer = vbNormal
        
        Set oItem = Nothing
        Set oItems = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifFecFinItems", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Modifica la fecha inicio suministro en los items seleccionados
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo: 0,2</remarks>
Private Sub ModifFecIniItems()
Dim oItem As CItem
Dim oItems As CItems
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim bErrores As Boolean
Dim vErrores As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set oItems = oFSGSRaiz.Generar_CItems
        Set oItem = oFSGSRaiz.Generar_CItem
        
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            
            With oItem
            
                'Comprueba si la nueva fecha de inicio es mayor que alguna de las fechas
                'de fin de suministro de los �tems seleccionados.
                If CDate(txtFecIni.Text) > CDate(frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) Then
                    oMensajes.FechaDesdeMayorFechaHasta 2
                    Set oItems = Nothing
                    Set oItem = Nothing
                    Unload Me
                    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                    Exit Sub
                End If
                
                .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FechaInicioSuministro = txtFecIni.Text
                Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                    Case EnItem
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                                .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                            Else
                                .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
                End Select
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                    Case EnItem
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                                .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                            Else
                                .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
                End Select
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                    Case EnItem
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                                .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                            Else
                                .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
                End Select
                Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                    Case EnItem
                            .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                    Case EnGrupo
                            .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                    Case EnProceso
                            .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
                End Select
                If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .ArticuloCod = Null
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .Descr = Null
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                oItem.Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
                .grupoCod = frmPROCE.g_oGrupoSeleccionado.Codigo
            End With
            
            oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , oItem.grupoCod, oItem.FECACT
        
        Next

        Screen.MousePointer = vbHourglass
        teserror = oItems.ModificarValores(, , , , , , , , , , , , , basOptimizacion.gvarCodUsuario)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            
            If teserror.NumError <> TESVolumenAdjDirSuperado Then
                basErrores.TratarError teserror
                Set oItems = Nothing
                Set oItem = Nothing
                Unload Me
                frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                Exit Sub
            ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
                basErrores.TratarError teserror
            End If
        Else
            bErrores = teserror.Arg2
            vErrores = teserror.Arg1
            If bErrores Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
                Screen.MousePointer = vbHourglass
            End If
        End If
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                frmPROCE.sdbgItems.Columns("INI").Value = txtFecIni.Text
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            Next
        Else
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                If vErrores(1, i) = 0 Then 'Ning�n error
                    frmPROCE.sdbgItems.Columns("INI").Value = txtFecIni.Text
                    frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
                End If
            Next
        End If
        
        frmPROCE.sdbgItems.SelBookmarks.RemoveAll
        frmPROCE.sdbgItems.Update
        frmPROCE.g_bUpdate = True
        Screen.MousePointer = vbNormal
        
        Set oItem = Nothing
        Set oItems = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifFecIniItems", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Modifica la forma pago en los items seleccionados
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo: 0,2</remarks>
Private Sub ModifPagoItems()
Dim oItem As CItem
Dim oItems As CItems
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim bErrores As Boolean
Dim vErrores As Variant
            
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set oItems = oFSGSRaiz.Generar_CItems
        Set oItem = oFSGSRaiz.Generar_CItem
        
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            
            With oItem
            
                .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .PagCod = sdbcPagoCod.Value
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                    Case EnItem
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                                .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                            Else
                                .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
                End Select
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                    Case EnItem
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                                .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                                .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                            Else
                                .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                                .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                            .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
                End Select
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                    Case EnItem
                            .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                    Case EnGrupo
                            .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                    Case EnProceso
                            .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
                End Select
                
                If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .ArticuloCod = Null
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                
                If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .Descr = Null
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                
                .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
                .grupoCod = frmPROCE.g_oGrupoSeleccionado.Codigo
            End With
            
            oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , oItem.grupoCod, oItem.FECACT
        
        Next

        Screen.MousePointer = vbHourglass
        teserror = oItems.ModificarValores(, , , , , , , , , , , , , basOptimizacion.gvarCodUsuario)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            
            If teserror.NumError <> TESVolumenAdjDirSuperado Then
                basErrores.TratarError teserror
                Set oItems = Nothing
                Set oItem = Nothing
                Unload Me
                frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                Exit Sub
            ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
                basErrores.TratarError teserror
            End If
        Else
            bErrores = teserror.Arg2
            vErrores = teserror.Arg1
            If bErrores Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
                Screen.MousePointer = vbHourglass
            End If
        End If
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                frmPROCE.sdbgItems.Columns("PAG").Value = sdbcPagoCod.Value
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            Next
        Else
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                If vErrores(1, i) = 0 Then 'Ning�n error
                    frmPROCE.sdbgItems.Columns("PAG").Value = sdbcPagoCod.Value
                    frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
                End If
            Next
        End If
        
        frmPROCE.sdbgItems.SelBookmarks.RemoveAll
        frmPROCE.sdbgItems.Update
        frmPROCE.g_bUpdate = True
        Screen.MousePointer = vbNormal
        
        Set oItem = Nothing
        Set oItems = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifPagoItems", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

'''<summary>Modifica el proveedor de los items</summary>
'''<remarks>Llamada desde: cmdAceptar_Click</remarks>
'''<revision>LTG 27/04/2012</revision>

Private Sub ModifProveItems()
    Dim oItem As CItem
    Dim oItems As CItems
    Dim teserror As TipoErrorSummit
    Dim i As Integer
    Dim bErrores As Boolean
    Dim vErrores As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set oItems = oFSGSRaiz.Generar_CItems
        Set oItem = oFSGSRaiz.Generar_CItem
        
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            
            With oItem
            
                .ProveAct = StrToNull(sdbcProveCod.Value)
                .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                    Case EnItem
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                                .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                            Else
                                .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
                End Select
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                    Case EnItem
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                                .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                            Else
                                .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
                End Select
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                    Case EnItem
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                                .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                                .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                            Else
                                .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                                .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                            .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
                End Select
                
                If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .ArticuloCod = Null
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                
                If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .Descr = Null
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                
                .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .grupoCod = frmPROCE.g_oGrupoSeleccionado.Codigo
            End With
            
            oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , oItem.grupoCod, oItem.FECACT
        
        Next

        Screen.MousePointer = vbHourglass
        teserror = oItems.ModificarValores(, , , , , , , , , , chkProve.Value, , , basOptimizacion.gvarCodUsuario)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            
            If teserror.NumError <> TESVolumenAdjDirSuperado Then
                basErrores.TratarError teserror
                Set oItems = Nothing
                Set oItem = Nothing
                Unload Me
                frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                Exit Sub
            ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
                basErrores.TratarError teserror
            End If
        Else
            bErrores = teserror.Arg2
            vErrores = teserror.Arg1
            If bErrores Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
                Screen.MousePointer = vbHourglass
            End If
        End If
        
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                frmPROCE.sdbgItems.Columns("PROVE").Value = oItems.Item(i + 1).ProveAct
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            Next
        Else
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                If vErrores(1, i) = 0 Then 'Ning�n error
                    frmPROCE.sdbgItems.Columns("PROVE").Value = oItems.Item(i + 1).ProveAct
                    frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
                End If
            Next
        End If
        
        frmPROCE.sdbgItems.SelBookmarks.RemoveAll
        frmPROCE.sdbgItems.Update
        frmPROCE.g_bUpdate = True
        Screen.MousePointer = vbNormal
        
        Set oItem = Nothing
        Set oItems = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifProveItems", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Modifica la unidad en los items seleccionados
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo: 0,2</remarks>
Private Sub ModifUniItems()
Dim oItem As CItem
Dim oItems As CItems
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim bErrores As Boolean
Dim vErrores As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oItems = oFSGSRaiz.Generar_CItems
    Set oItem = oFSGSRaiz.Generar_CItem
    
    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
        
        With oItem
        
            .UniCod = sdbcUnidadCod.Value
            .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                Case EnItem
                        .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                            .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                        Else
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                Case EnItem
                        .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                            .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                        Else
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                Case EnItem
                        .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                            .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                            .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                        Else
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                        .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                Case EnItem
                        .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                Case EnGrupo
                        .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                Case EnProceso
                        .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
            End Select
            
            If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                .ArticuloCod = Null
                .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            
            If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                .Descr = Null
                .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            
            .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        
        
            .GMN1Cod = frmPROCE.sdbgItems.Columns("GMN1").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .GMN2Cod = frmPROCE.sdbgItems.Columns("GMN2").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .GMN3Cod = frmPROCE.sdbgItems.Columns("GMN3").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .GMN4Cod = frmPROCE.sdbgItems.Columns("GMN4").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
            .grupoCod = frmPROCE.g_oGrupoSeleccionado.Codigo
        End With
        
        oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , oItem.grupoCod, oItem.FECACT, , , , , oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod
    
    Next

    Screen.MousePointer = vbHourglass
    teserror = oItems.ModificarValores(chkUnidad.Value, , , , , , , , , , , , , basOptimizacion.gvarCodUsuario)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESVolumenAdjDirSuperado Then
            basErrores.TratarError teserror
            Set oItems = Nothing
            Set oItem = Nothing
            Unload Me
            frmPROCE.sdbgItems.SelBookmarks.RemoveAll
            Exit Sub
        ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
            basErrores.TratarError teserror
        End If
    Else
        bErrores = teserror.Arg2
        vErrores = teserror.Arg1
        If bErrores Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
            Screen.MousePointer = vbHourglass
        End If
    End If
    
    If teserror.NumError = TESVolumenAdjDirSuperado Then
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            frmPROCE.sdbgItems.Columns("UNI").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).UniCod
            frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
        Next
    Else
        bErrores = teserror.Arg2
        vErrores = teserror.Arg1
        If bErrores Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
            Screen.MousePointer = vbHourglass
        End If
        
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            If vErrores(1, i) = 0 Then 'Ning�n error
                frmPROCE.sdbgItems.Columns("UNI").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).UniCod
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            End If
        Next
    End If
    
    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
    frmPROCE.sdbgItems.Update
    frmPROCE.g_bUpdate = True
    Screen.MousePointer = vbNormal

    Set oItems = Nothing
    Set oItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifUniItems", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub chkPres_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPres.Value = 1 Then
        chkPresPlanif.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "chkPres_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkPresEsc_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPresEsc.Value = 1 Then
        chkPresPlanifEsc.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "chkPresEsc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub chkPresPlanif_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPresPlanif.Value = 1 Then
        chkPres.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "chkPresPlanif_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkPresPlanifEsc_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPresPlanifEsc.Value = 1 Then
        chkPresEsc.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "chkPresPlanifEsc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Acepta los cambios
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema (bt cmdAceptar); Tiempo m�ximo: 0</remarks>
''' <revision>LTG 06/02/2012</revision>

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim oGrupo As CGrupo
    Dim irespuesta As Integer
    Dim iDef As Integer
    Dim oItems As CItems
    Dim i As Integer
    Dim iBookmark As Integer
    Dim oProveedores As CProveedores
    Dim oTESError As TipoErrorSummit
    Dim oIPub As IPublicaciones
    Dim oAtribsEspec As CAtributosEspecificacion
    Dim oAtribEsp As CAtributoEspecificacion
    Dim sCadena As String
    Dim sCadenaTmp As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_sOrigen
    
        Case "DEST"
        
                If sdbcDestCod.Value = "" Then
                    oMensajes.NoValido m_sIdiDestino
                    Exit Sub
                End If
        
                If g_iCol = 2 Then 'Pasa a PROCESO
                    frmPROCE.g_oProcesoSeleccionado.DefDestino = EnProceso
                    frmPROCE.g_oProcesoSeleccionado.DestCod = sdbcDestCod.Value
                    frmPROCE.g_oProcesoSeleccionado.DestDen = sdbcDestDen.Value
                    Screen.MousePointer = vbHourglass
                    teserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(Destino)
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        'Restauramos los valores anteriores
                        frmPROCE.MostrarConfiguracion
                        m_bCancelar = True
                        Unload Me
                        Exit Sub
                    End If
                    frmPROCE.sdbcGrupo.Columns(0).Value = ""
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefDestino Then
                            oGrupo.DefDestino = False
                            '�Cambiar en BD la config de los GRUPOS si antes estaba en GRUPO?
                        End If
                    Next
                    Screen.MousePointer = vbNormal
                End If
                If g_iCol = 3 Then 'Pasa a GRUPO
                    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag))
                    If oGrupo Is Nothing Then Exit Sub
                    frmPROCE.g_oProcesoSeleccionado.DefDestino = EnGrupo
                    oGrupo.DefDestino = True
                    oGrupo.DestCod = sdbcDestCod.Value
                    oGrupo.DestDen = sdbcDestDen.Value
                End If
            
    
        Case "PAGO"
        
                If sdbcPagoCod.Value = "" Then
                    oMensajes.NoValido m_sIdiPago
                    Exit Sub
                End If
        
                If g_iCol = 2 Then 'Pasa a PROCESO
                    frmPROCE.g_oProcesoSeleccionado.DefFormaPago = EnProceso
                    frmPROCE.g_oProcesoSeleccionado.PagCod = sdbcPagoCod.Value
                    frmPROCE.g_oProcesoSeleccionado.PagDen = sdbcPagoDen.Value
                    Screen.MousePointer = vbHourglass
                    teserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(Pago)
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        'Restauramos los valores anteriores
                        frmPROCE.MostrarConfiguracion
                        m_bCancelar = True
                        Unload Me
                        Exit Sub
                    End If
                    frmPROCE.sdbcGrupo.Columns(0).Value = ""
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefFormaPago Then
                            oGrupo.DefFormaPago = False
                            '�Cambiar en BD la config de los GRUPOS si antes estaba en GRUPO?
                        End If
                    Next
                    Screen.MousePointer = vbNormal
                End If
                If g_iCol = 3 Then 'Pasa a GRUPOS
                    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag))
                    If oGrupo Is Nothing Then Exit Sub
                    frmPROCE.g_oProcesoSeleccionado.DefFormaPago = EnGrupo
                    oGrupo.DefFormaPago = True
                    oGrupo.PagCod = sdbcPagoCod.Value
                    oGrupo.PagDen = sdbcPagoDen.Value
                End If
            
    
        Case "FECHAS"
        
                If Not IsDate(txtFecIni.Text) Then
                    oMensajes.NoValido m_sIdiFecIni
                    Exit Sub
                End If
        
                If Not IsDate(txtFecFin.Text) Then
                    oMensajes.NoValido m_sIdiFecFin
                    Exit Sub
                End If
                
                If CDate(txtFecIni.Text) > CDate(txtFecFin.Text) Then
                    oMensajes.FechaDesdeMayorFechaHasta 2
                    Exit Sub
                End If
                
                If g_iCol = 2 Then 'Pasa a PROCESO
                    frmPROCE.g_oProcesoSeleccionado.DefFechasSum = EnProceso
                    frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro = txtFecIni.Text
                    frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro = txtFecFin.Text
                    Screen.MousePointer = vbHourglass
                    teserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(FechasSum)
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        frmPROCE.MostrarConfiguracion
                        m_bCancelar = True
                        Unload Me
                        Exit Sub
                    End If
                    frmPROCE.sdbcGrupo.Columns(0).Value = ""
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefFechasSum Then
                            oGrupo.DefFechasSum = False
                            '�Cambiar en BD la config de los GRUPOS si antes estaba en GRUPO?
                        End If
                    Next
                    Screen.MousePointer = vbNormal
                End If
                If g_iCol = 3 Then 'Pasa a GRUPO
                    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag))
                    If oGrupo Is Nothing Then Exit Sub
                    oGrupo.DefFechasSum = True
                    frmPROCE.g_oProcesoSeleccionado.DefFechasSum = EnGrupo
                    oGrupo.FechaInicioSuministro = txtFecIni.Text
                    oGrupo.FechaFinSuministro = txtFecFin.Text
                End If
            
            
        Case "PROVE"
        
                If g_iCol = 2 Then 'Pasa a PROCESO
                    frmPROCE.g_oProcesoSeleccionado.DefProveActual = EnProceso
                    frmPROCE.g_oProcesoSeleccionado.ProveActual = StrToNull(sdbcProveCod.Value)
                    frmPROCE.g_oProcesoSeleccionado.ProveActDen = StrToNull(sdbcProveDen.Value)
                    Screen.MousePointer = vbHourglass
                    teserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(ProveedorAct)
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        frmPROCE.MostrarConfiguracion
                        m_bCancelar = True
                        Unload Me
                        Exit Sub
                    End If
                    frmPROCE.sdbcGrupo.Columns(0).Value = ""
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefProveActual Then
                            oGrupo.DefProveActual = False
                            '�Cambiar en BD la config de los GRUPOS si antes estaba en GRUPO?
                        End If
                    Next
                    Screen.MousePointer = vbNormal
                End If
                If g_iCol = 3 Then 'Pasa a GRUPO
                    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag))
                    If oGrupo Is Nothing Then Exit Sub
                    frmPROCE.g_oProcesoSeleccionado.DefProveActual = EnGrupo
                    oGrupo.DefProveActual = True
                    oGrupo.ProveActual = StrToNull(sdbcProveCod.Value)
                    oGrupo.ProveActDen = StrToNull(sdbcProveDen.Value)
                End If
            
        Case "GRUPOS"
            If sdbcGrupoCod.Value = "" Then
                oMensajes.NoValido m_sIdiGrupo
                Exit Sub
            End If
            
            'Recalculo de ofertas
            Dim iOfertas As Integer
            iOfertas = frmPROCE.g_oProcesoSeleccionado.tieneofertas
            If iOfertas > 1 Then
                
                m_sMenCambiarGrupo = m_sMenCambiarGrupo & vbCrLf & frmPROCE.m_sMensajeAfecta
                m_sMenCambiarGrupo = m_sMenCambiarGrupo & vbCrLf & frmPROCE.m_sMensajeSiContinua
                m_sMenCambiarGrupo = m_sMenCambiarGrupo & vbCrLf
                m_sMenCambiarGrupo = m_sMenCambiarGrupo & vbCrLf & frmPROCE.m_sMensajeContinuar
                
                irespuesta = oMensajes.AvisoConConfirmacion(m_sMenCambiarGrupo)
            Else
                irespuesta = oMensajes.CambiarItemsDeGrupo
            End If
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            If iOfertas > 0 Then
                m_bRecalcularOfertas = True
            End If
            
            If Not gParametrosGenerales.gbPresupuestosAut Then
                Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(sdbcGrupoCod.Value)
                If oGrupo.HayPresAnualTipo1 Or oGrupo.HayPresAnualTipo2 Or oGrupo.HayPresTipo1 Or oGrupo.HayPresTipo2 Then
                    irespuesta = oMensajes.PreguntaReasignarPresupuestos
                    If irespuesta = vbYes Then
                        '**PRESANU1
                        If oGrupo.HayPresAnualTipo1 Then
                            ReasignarPresupNuevoGrupo 1, oGrupo
                        End If
                        '**PRESANU2
                        If oGrupo.HayPresAnualTipo2 Then
                            ReasignarPresupNuevoGrupo 2, oGrupo
                        End If
                        '**PRES1
                        If oGrupo.HayPresTipo1 Then
                            ReasignarPresupNuevoGrupo 3, oGrupo
                        End If
                        '**PRES2
                        If oGrupo.HayPresTipo2 Then
                            ReasignarPresupNuevoGrupo 4, oGrupo
                        End If
                    End If
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            Set oItems = oFSGSRaiz.Generar_CItems
            
            
            
            'Escalados...
            Dim aEscOri() As Variant
            Dim aEscDes() As Variant
            Dim oEscalado As CEscalado
            Dim ind As Long
            ReDim aEscOri(0)
            ReDim aEscDes(0)
            Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(sdbcGrupoCod.Value)
            If oGrupo.UsarEscalados Then
                For Each oEscalado In frmPROCE.g_oGrupoSeleccionado.Escalados
                    ind = UBound(aEscOri) + 1
                    ReDim Preserve aEscOri(ind)
                    aEscOri(ind) = oEscalado.Id
                Next
                If frmPROCE.g_oProcesoSeleccionado.Grupos.Item(m_lGrupoID).Escalados Is Nothing Then
                    frmPROCE.g_oProcesoSeleccionado.Grupos.Item(m_lGrupoID).CargarEscalados
                End If
                For Each oEscalado In oGrupo.Escalados
                    ind = UBound(aEscDes) + 1
                    ReDim Preserve aEscDes(ind)
                    aEscDes(ind) = oEscalado.Id
                Next
            End If
            
            
            'Para tener el proceso
            oItems.Add frmPROCE.g_oProcesoSeleccionado, 1, "A", "U", "P", 1, Date, Date

            teserror = oItems.CambiarDeGrupo(frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, frmPROCE.g_arItems, frmPROCE.g_oGrupoSeleccionado.Id, m_lGrupoID, g_oPresupuestos1Nivel4, g_oPresupuestos2Nivel4, g_oPresupuestos3Nivel4, g_oPresupuestos4Nivel4, g_oPresupuestos1Nivel4Nuevos, g_oPresupuestos2Nivel4Nuevos, g_oPresupuestos3Nivel4Nuevos, g_oPresupuestos4Nivel4Nuevos, oGrupo.UsarEscalados, aEscOri, aEscDes, m_bRecalcularOfertas)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oItems = Nothing
                Set oGrupo = Nothing
                Exit Sub
            End If
            frmPROCE.g_oProcesoSeleccionado.FECACT = teserror.Arg1
            
            i = frmPROCE.sdbgItems.SelBookmarks.Count - 1
            For iBookmark = 0 To i
                RegistrarAccion accionessummit.ACCProceItemsGrupoCamb, "Proceso:" & frmPROCE.g_oProcesoSeleccionado.Anyo & "/" & frmPROCE.g_oProcesoSeleccionado.GMN1Cod & "/" & frmPROCE.g_oProcesoSeleccionado.Cod & " Item:" & frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(iBookmark)) & " GrupoNuevo:" & sdbcGrupoCod.Value & " GrupoAnterior:" & frmPROCE.sdbgItems.Columns("GRUPO").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(iBookmark))
            Next
            
            frmPROCE.g_bRespetarDeleteItems = True
            frmPROCE.sdbgItems.DeleteSelected
            frmPROCE.g_bRespetarDeleteItems = False
            frmPROCE.sdbgItems.MoveFirst
            If m_bRecalcularOfertas Then
                basSeguridad.RegistrarAccion accionessummit.ACCCondOfeRecalculo, "Llamada desde:Cambio items de grupo:" & Trim(frmPROCE.g_oProcesoSeleccionado.Anyo) & " GMN1:" & Trim(frmPROCE.g_oProcesoSeleccionado.GMN1Cod) & " Proce:" & frmPROCE.g_oProcesoSeleccionado.Cod
            End If

            frmPROCE.g_oGrupoSeleccionado.NumItems = frmPROCE.g_oGrupoSeleccionado.NumItems - UBound(frmPROCE.g_arItems)
            frmPROCE.g_oProcesoSeleccionado.Grupos.Item(sdbcGrupoCod.Value).NumItems = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(sdbcGrupoCod.Value).NumItems + UBound(frmPROCE.g_arItems)
                        
            
            '@@DPD -> He detectado que tras el cambio de grupo de uno o varios items, no se actualiza el label de importe de grupo.
            'independientemente de si hay o no escalados. Queda pendiente de revisar.
            
        Case "DEST_ITEMS"
            If sdbcDestCod.Value = "" Then
                oMensajes.NoValido m_sIdiDestino
                Exit Sub
            End If

            ModifDestItems
    
        Case "UNI_ITEMS"
            If sdbcUnidadCod.Value = "" Then
                oMensajes.NoValido m_sIdiUnidad
                Exit Sub
            End If
            
             ModifUniItems
            
        Case "PROVE_ITEMS"

            ModifProveItems
            
        Case "CANT_ITEMS"
            If frmPROCE.g_oGrupoSeleccionado.UsarEscalados = 0 Then
                If txtCant.Text = "" Then
                    oMensajes.NoValido m_sIdiCant
                    Exit Sub
                End If
                
                If Not IsNumeric(txtCant.Text) Then
                    oMensajes.NoValido m_sIdiCant
                    Exit Sub
                End If
            End If
            
            m_bRecalcularOfertas = False
            If Not RecalcularOfertas Then
                Exit Sub
            End If
            ModifCantItems
            
        Case "PRES_ITEMS"
            If txtPres.Text = "" Then
                oMensajes.NoValido m_sIdiPres
                Exit Sub
            End If
            If Not IsNumeric(txtPres.Text) Then
                oMensajes.NoValido m_sIdiPres
                Exit Sub
            End If
            

            ModifPrecItems
            
        Case "PRES_ITEMS_ESC"
            If ComprobarFormulario Then
                If Not ModifPrecItemsEsc Then Exit Sub
            Else
                Exit Sub
            End If
            
        Case "FECINI_ITEMS"
            If Not IsDate(txtFecIni.Text) Then
                oMensajes.NoValido m_sIdiFecIni
                Exit Sub
            End If
            If frmPROCE.txtFecNec.Text <> "" Then
                If ((frmPROCE.g_oProcesoSeleccionado.FechaNecModificadaPorUsu = False) And (CDate(txtFecIni.Text) < CDate(frmPROCE.txtFecNec.Text))) Then
                    frmPROCE.txtFecNec.Text = txtFecIni.Text
                ElseIf ((frmPROCE.g_oProcesoSeleccionado.FechaNecModificadaPorUsu = True) And (CDate(txtFecIni.Text) < CDate(frmPROCE.txtFecNec.Text))) Then
                    oMensajes.FechaMenorQueNecesidad frmPROCE.m_sIdiFecNec, frmPROCE.txtFecNec.Text
                    Exit Sub
                End If
            End If
            
            ModifFecIniItems
            
        Case "FECFIN_ITEMS"
            If Not IsDate(txtFecFin.Text) Then
                oMensajes.NoValido m_sIdiFecFin
                Exit Sub
            End If
            
            ModifFecFinItems
            
        Case "PAGO_ITEMS"
            If sdbcPagoCod.Value = "" Then
                oMensajes.NoValido m_sIdiPago
                Exit Sub
            End If

            ModifPagoItems
                       
        Case "SOBRE"
            If picDest.Visible Then
                If sdbcDestCod.Value = "" Then
                    oMensajes.NoValido m_sIdiDestino
                    Exit Sub
                End If
                frmPROCE.g_oProcesoSeleccionado.DestCod = sdbcDestCod.Value
                frmPROCE.g_oProcesoSeleccionado.DestDen = sdbcDestDen.Value
            End If
            If picPago.Visible Then
                If sdbcPagoCod.Value = "" Then
                    oMensajes.NoValido m_sIdiPago
                    Exit Sub
                End If
                frmPROCE.g_oProcesoSeleccionado.PagCod = sdbcPagoCod.Value
                frmPROCE.g_oProcesoSeleccionado.PagDen = sdbcPagoDen.Value
            End If
            If txtFechaSobre.Text = "" Then
                oMensajes.NoValido 129
                Exit Sub
            End If
            If Not IsDate(txtFechaSobre.Text) Then
                oMensajes.NoValido 129
                Exit Sub
            End If
            If txtSobre.Text = "" Then txtSobre.Text = TimeValue("00:00")
            If Not IsTime(txtSobre.Text) Then
                oMensajes.NoValido Left(lblSobre(1).caption, Len(lblSobre(1).caption) - 1)
                Exit Sub
            End If
            If CDate(txtFechaSobre.Text & " " & txtSobre.Text) < Now Then
                oMensajes.NoValido 134 'fecha caducada
                Exit Sub
            End If
            If picFechaLim.Visible Then
                If Trim(txtFecLim.Text) = "" Then
                    oMensajes.NoValido 35
                    Exit Sub
                End If
                If Not IsDate(Trim(txtFecLim.Text)) Then
                    oMensajes.NoValido 35
                    Exit Sub
                End If
                txtFecLim.Text = Trim(txtFecLim.Text)
                If Trim(txtHoraLim.Text) = "" Then
                    oMensajes.NoValido 137 'hora lim ofertas
                    Exit Sub
                End If
                If Not IsTime(Trim(txtHoraLim.Text)) Then
                    oMensajes.NoValido 137
                    Exit Sub
                End If
                txtHoraLim.Text = Trim(txtHoraLim.Text)
                If CDate(txtFechaSobre.Text & " " & txtSobre.Text) < CDate(txtFecLim.Text & " " & txtHoraLim.Text) Then
                    oMensajes.FechaSobreMenorQueLimOfer txtFecLim.Text & " " & txtHoraLim.Text, 35
                    Exit Sub
                End If
            Else
                If CDate(txtFechaSobre.Text & " " & txtSobre.Text) < frmPROCE.g_oProcesoSeleccionado.FechaMinimoLimOfertas Then
                    oMensajes.FechaSobreMenorQueLimOfer frmPROCE.g_oProcesoSeleccionado.FechaMinimoLimOfertas, 35
                    Exit Sub
                End If
            End If
            If sdbcUsuSobre.Value = "" Then
                oMensajes.NoValido 130
                Exit Sub
            End If
            Set frmPROCE.g_oProcesoSeleccionado.Sobres = oFSGSRaiz.Generar_CSobres
            If picFechaLim.Visible Then frmPROCE.g_oProcesoSeleccionado.FechaMinimoLimOfertas = txtFecLim.Text & " " & txtHoraLim.Text
            frmPROCE.g_oProcesoSeleccionado.Sobres.Add frmPROCE.g_oProcesoSeleccionado, 1, , , sdbcUsuSobre.Columns(1).Value, , , , , CDate(txtFechaSobre.Text & " " & txtSobre.Text), 0
            frmPROCE.g_bCancelar = False
            
        Case "BAJMINPUJA_ITEMS"
            If Trim(txtBajMinPuja.Text) = "" Then
                oMensajes.NoValido m_sIdiBajMinPuja
                Exit Sub
            End If

            If Not IsNumeric(Trim(txtBajMinPuja.Text)) Then
                oMensajes.NoValido m_sIdiBajMinPuja
                Exit Sub
            End If
            
            ModifBajMinPuja
                
    End Select

    m_bCancelar = True
    
    Screen.MousePointer = vbNormal
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub ReasignarPresupNuevoGrupo(ByVal iTipo As Integer, ByVal oGrupo As CGrupo)
Dim dblAbierto As Double
Dim dblPresItems As Double
Dim dblAbiertoMod As Double
Dim oItems As CItems
Dim vPres As Variant
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oItems = oFSGSRaiz.Generar_CItems
            
    dblAbierto = oGrupo.DevolverPresupuestoAbierto
    vPres = oItems.DevolverPresupuestoActual(, , , , frmPROCE.g_arItems, frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod)
    dblPresItems = vPres(0)
    'AbiertoDespuesModificar=Abierto + PresupItems
    dblAbiertoMod = dblAbierto + dblPresItems
            
    frmPRESAsig.g_iAmbitoPresup = 2
    frmPRESAsig.g_iTipoPres = iTipo
    frmPRESAsig.g_bModif = True
    Select Case iTipo
            Case 1
                frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo1
            Case 2
                frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo2
            Case 3
                frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo1
            Case 4
                frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo2
    End Select
            
    frmPRESAsig.g_sOrigen = "GRUPO_NUEVO"
    frmPRESAsig.g_sGrupoNuevo = sdbcGrupoCod.Value
    frmPRESAsig.g_dblAbierto = dblAbiertoMod
    frmPRESAsig.g_dblAsignado = dblAbierto
    frmPRESAsig.g_bHayPres = True
    frmPRESAsig.g_bHayPresBajaLog = False
    frmPRESAsig.Show 1
        
    Set oItems = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ReasignarPresupNuevoGrupo", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub

''' <summary>
''' Modifica el destino en los items seleccionados
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo: 0,2</remarks>
Private Sub ModifDestItems()
Dim oItem As CItem
Dim oItems As CItems
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim bErrores As Boolean
Dim vErrores As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set oItems = oFSGSRaiz.Generar_CItems
        Set oItem = oFSGSRaiz.Generar_CItem
        
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
        
            With oItem
            
                .DestCod = sdbcDestCod.Value
                .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                    Case EnItem
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                                .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                            Else
                                .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
                End Select
            
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                    Case EnItem
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                                .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                                .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                            Else
                                .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                                .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                            .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
                End Select
                
                Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                    Case EnItem
                            .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                    Case EnGrupo
                            .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                    Case EnProceso
                            .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
                End Select
            
                If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .ArticuloCod = Null
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
                If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                    .Descr = Null
                    .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Else
                    .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                End If
            
                .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                
                .GMN1Cod = frmPROCE.sdbgItems.Columns("GMN1").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .GMN2Cod = frmPROCE.sdbgItems.Columns("GMN2").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .GMN3Cod = frmPROCE.sdbgItems.Columns("GMN3").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                .GMN4Cod = frmPROCE.sdbgItems.Columns("GMN4").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
                .grupoCod = frmPROCE.g_oGrupoSeleccionado.Codigo
            End With
                
            oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , oItem.grupoCod, oItem.FECACT, , , , , oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN4Cod, oItem.GMN4Cod
        
        Next

        Screen.MousePointer = vbHourglass
        teserror = oItems.ModificarValores(, , , , , , , , , , , , , basOptimizacion.gvarCodUsuario)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            
            If teserror.NumError <> TESVolumenAdjDirSuperado Then
                basErrores.TratarError teserror
                Set oItems = Nothing
                Set oItem = Nothing
                Unload Me
                frmPROCE.sdbgItems.SelBookmarks.RemoveAll
                Exit Sub
            ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
                basErrores.TratarError teserror
            End If
        Else
            bErrores = teserror.Arg2
            vErrores = teserror.Arg1
            If bErrores Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
                Screen.MousePointer = vbHourglass
            End If
        End If
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                frmPROCE.sdbgItems.Columns("DEST").Value = sdbcDestCod.Value
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            Next
        Else
            bErrores = teserror.Arg2
            vErrores = teserror.Arg1
            If bErrores Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
                Screen.MousePointer = vbHourglass
            End If
            
            frmPROCE.g_bUpdate = False
            For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                If vErrores(1, i) = 0 Then 'Ning�n error
                    frmPROCE.sdbgItems.Columns("DEST").Value = sdbcDestCod.Value
                    frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
                End If
            Next
        End If
        
        frmPROCE.sdbgItems.SelBookmarks.RemoveAll
        frmPROCE.sdbgItems.Update
        frmPROCE.g_bUpdate = True
        Screen.MousePointer = vbNormal
        
        Set oItem = Nothing
        Set oItems = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifDestItems", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub cmdBuscar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmPROVEBuscar.sOrigen = "frmDatoAmbitoProce"
    frmPROVEBuscar.sdbcGMN1_4Cod.Value = frmPROCE.g_oProcesoSeleccionado.GMN1Cod
    frmPROVEBuscar.sdbcGMN1_4Cod_Validate False
    frmPROVEBuscar.bRMat = bRestProvMatComp

'    If bREqp Then
'        frmPROVEBuscar.codEqp = basOptimizacion.gCodEqpUsuario
'    End If
    
    frmPROVEBuscar.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "cmdBuscar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Public Sub CargarProveedorConBusqueda()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    m_bRespetarComboProve = True
    sdbcProveCod.Value = m_oProves.Item(1).Cod
    sdbcProveDen.Value = m_oProves.Item(1).Den
    m_bRespetarComboProve = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "CargarProveedorConBusqueda", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecFin_Click()
    AbrirFormCalendar Me, txtFecFin
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecFinSub_Click()
    AbrirFormCalendar Me, txtFecFinSub
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecIni_Click()
    AbrirFormCalendar Me, txtFecIni
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecInisub_Click()
    AbrirFormCalendar Me, txtFecIniSub
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecLim_Click()
    AbrirFormCalendar Me, txtFecLim
End Sub

Public Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_sOrigen = "SOBRE" Then
        frmPROCE.g_bCancelar = True
    End If
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' Realiza la llamada al buscador usuarios para luego almacenar el valor.
''' </summary>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub cmdSobre_Click(Index As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Index = 0 Then
        AbrirFormCalendar Me, txtFechaSobre, "SOBRE"
    Else
        frmUSUBuscar.g_sOrigen = "frmDatoAmbito"
        frmUSUBuscar.Show 1
    End If
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "cmdSobre_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
Select Case g_sOrigen
    Case "DEST", "DEST_ITEMS"
            sdbcDestCod.Value = gParametrosInstalacion.gsDestino
            sdbcDestCod_Validate False
            
    Case "PAGO", "PAGO_ITEMS"
            sdbcPagoCod.Value = gParametrosInstalacion.gsFormaPago
            sdbcPagoCod_Validate False
    
    Case "UNI_ITEMS"
                sdbcUnidadCod.Value = gParametrosGenerales.gsUNIDEF
                sdbcUnidadCod_Validate False

    Case "SOBRE"

                With frmPROCE.g_oProcesoSeleccionado
                    If .DefDestino = EnProceso And .DefFormaPago = EnProceso And IsDate(.FechaMinimoLimOfertas) Then
                        Me.Height = 2400
                        picEdit.Top = 1545
                        picSobre.Top = 30
                        picSobre.Visible = True
                    Else
                        If .DefDestino <> EnProceso Then
                            picDest.Visible = True
                            picSobre.Top = 480
                            picSobre.Visible = True
                            picEdit.Top = 2400
                            Me.Height = 3225
                        End If
                        If .DefFormaPago <> EnProceso Then
                            If picDest.Visible Then
                                picPago.Top = 480
                                picSobre.Top = 870
                                picSobre.Visible = True
                                picEdit.Top = 2400
                                Me.Height = 3225
                            Else
                                picPago.Top = 30
                                picSobre.Top = 480
                                picSobre.Visible = True
                                picEdit.Top = 2010
                                Me.Height = 2865
                            End If
                            picPago.Visible = True
                        End If
                        If Not IsDate(.FechaMinimoLimOfertas) Then
                            If picDest.Visible Then
                                If picPago.Visible Then
                                    picFechaLim.Top = 870
                                    picSobre.Top = 1290
                                    picSobre.Visible = True
                                    picEdit.Top = 2820
                                    Me.Height = 3675
                                Else
                                    picFechaLim.Top = 480
                                    picSobre.Top = 870
                                    picSobre.Visible = True
                                    picEdit.Top = 2400
                                    Me.Height = 3225
                                End If
                            Else
                                If picPago.Visible Then
                                    picFechaLim.Top = 480
                                    picSobre.Top = 870
                                    picSobre.Visible = True
                                    picEdit.Top = 2400
                                    Me.Height = 3225
                                Else
                                    picFechaLim.Top = 30
                                    picSobre.Top = 480
                                    picSobre.Visible = True
                                    picEdit.Top = 2010
                                    Me.Height = 2865
                                End If
                            End If
                            picFechaLim.Visible = True
                        End If
                    End If
                End With
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cargar la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, True
m_bActivado = False
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarControles
    
    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    Set m_oPagos = oFSGSRaiz.generar_CPagos
    Set m_oProves = oFSGSRaiz.generar_CProveedores
    Set m_oUnidades = oFSGSRaiz.Generar_CUnidades
    m_bCancelar = False
    
    m_bRDest = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDest)) Is Nothing)
    
    If Not oUsuarioSummit.Perfil Is Nothing Then m_lIdPerfil = oUsuarioSummit.Perfil.Id
        
    m_bRDestPerf = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDestPerf)) Is Nothing)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Configura los controles del formulario en funci�n del cambio que se quiera hacer</summary>
''' <remarks>Llamada desde: Form_Load ;Tiempo m�ximo</remarks>

Private Sub ConfigurarControles()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picDest.Top = 30
    picPago.Top = 30
    picFechaLim.Top = 30
    picSobre.Top = 30

    Select Case g_sOrigen
        Case "DEST", "DEST_ITEMS"
                If g_iCol = 3 Then
                    Me.caption = m_sIdiomas(12) & " " & frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag)).Codigo
                Else
                    Me.caption = m_sIdiomas(1)
                End If
                Me.Height = 1450
                picEdit.Top = 600
                picDest.Visible = True
        Case "PAGO", "PAGO_ITEMS"
                If g_iCol = 3 Then
                    Me.caption = m_sIdiomas(13) & " " & frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag)).Codigo
                Else
                    Me.caption = m_sIdiomas(2)
                End If
                Me.Height = 1450
                picEdit.Top = 600
                picPago.Visible = True
        Case "PROVE", "PROVE_ITEMS"
                If g_iCol = 3 Then
                    Me.caption = m_sIdiomas(14) & " " & frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag)).Codigo
                Else
                    Me.caption = m_sIdiomas(3)
                End If
                picProve.Visible = True
                If g_sOrigen = "PROVE" Then
                    picProve.Height = 400
                    Me.Height = 1450
                    picEdit.Top = 600
                    chkProve.Visible = False
                Else
                    Me.Height = 1600
                    picEdit.Top = 800
                End If
        Case "FECHAS"
                If g_iCol = 3 Then
                    Me.caption = m_sIdiomas(15) & " " & frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag)).Codigo
                Else
                    Me.caption = m_sIdiomas(4)
                End If
                Me.Height = 1450
                picEdit.Top = 600
                picFechas.Visible = True
        Case "FECINI_ITEMS"
                Me.caption = m_sIdiomas(9)
                Me.Height = 1450
                picEdit.Top = 600
                lblFecIni.Left = lblFecIni.Left + 900
                txtFecIni.Left = txtFecIni.Left + 1300
                cmdCalFecIni.Left = cmdCalFecIni.Left + 1300
                lblFecFin.Visible = False
                txtFecFin.Visible = False
                cmdCalFecFin.Visible = False
                picFechas.Visible = True
        Case "FECFIN_ITEMS"
                Me.caption = m_sIdiomas(10)
                Me.Height = 1450
                picEdit.Top = 600
                lblFecFin.Left = lblFecFin.Left - 2200
                txtFecFin.Left = txtFecFin.Left - 1800
                cmdCalFecFin.Left = cmdCalFecFin.Left - 1800
                lblFecIni.Visible = False
                txtFecIni.Visible = False
                cmdCalFecIni.Visible = False
                picFechas.Visible = True
        Case "GRUPOS"
                Me.caption = m_sIdiomas(5)
                Me.Height = 1450
                picEdit.Top = 600
                picGrupos.Visible = True
        Case "UNI_ITEMS"
                Me.caption = m_sIdiomas(6)
                picUnidad.Visible = True
                Me.Height = 1650
                picEdit.Top = 800
        Case "CANT_ITEMS"
                Me.caption = m_sIdiomas(7)
                Me.Height = 1450
                picEdit.Top = 600
                picCant.Visible = True
        Case "PRES_ITEMS"
                Me.caption = m_sIdiomas(8)
                picPres.Visible = True
                Me.Height = 2000
                picEdit.Top = 1050
                If gParametrosInstalacion.gbPresupuestoPlanificado = True Then
                    chkPresPlanif.Value = 1
                Else
                    chkPres.Value = 1
                End If
                If Not frmPROCE.g_oProcesoSeleccionado.NoCalPresUniEnBasePrecioOfer And frmPROCE.g_oProcesoSeleccionado.tieneofertas > 0 Then
                    'picPres.Visible = False
                    chkPresPlanif.Enabled = False
                    chkPres.Enabled = False
                    txtPres.Enabled = False
                    cmdAceptar.Enabled = False
                End If
        Case "PRES_ITEMS_ESC"
                Me.caption = m_sIdiomas(8)
                PicPresEsc.Visible = True
                If gParametrosInstalacion.gbPresupuestoPlanificado = True Then
                    chkPresPlanifEsc.Value = 1
                Else
                    chkPresEsc.Value = 1
                End If
                
                
                CargarEscalados
                
                'Calculamos las  dimensiones del form
                Dim separador As Long: separador = 180
                Dim SeparadorV As Long: SeparadorV = 80

                'Me.Width = 5670
                Dim nFilas As Long
                nFilas = Me.SSDBGridEscalados.Rows
                If nFilas > 10 Then nFilas = 10
                Me.SSDBGridEscalados.Height = 280 + (nFilas * Me.SSDBGridEscalados.RowHeight)
                Me.PicPresEsc.Height = Me.SSDBGridEscalados.Top + Me.SSDBGridEscalados.Height + SeparadorV
                

                'Ajustar las columnas del grid
                Me.SSDBGridEscalados.Width = Me.Width - (2 * separador)
                Me.SSDBGridEscalados.Left = separador
                Dim Scroll As Long
                    Scroll = 200
                Dim Marcadores As Long
                    Marcadores = 380
                
                 If g_ogrupo.TipoEscalados = ModoRangos Then
                     Me.SSDBGridEscalados.Columns("INI").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.3
                     Me.SSDBGridEscalados.Columns("FIN").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.3
                     Me.SSDBGridEscalados.Columns("PRES").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.4
                 Else
                     Me.SSDBGridEscalados.Columns("DIR").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
                     Me.SSDBGridEscalados.Columns("PRES").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
                 End If
                
                
                picEdit.Top = Me.PicPresEsc.Height
                Me.Height = picEdit.Top + picEdit.Height + (3 * separador)
                
        Case "SOLICIT"
                If g_iCol = 3 Then
                    Me.caption = m_sIdiomas(12) & " " & frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems.Item(g_iLstGrupoItem).Tag)).Codigo
                Else
                    Me.caption = m_sIdiomas(1)
                End If
                Me.Height = 1450
                picEdit.Top = 600
                picDest.Visible = True
        Case "SOBRE"
                Me.caption = m_sIdiomas(16)
                frmPROCE.g_bCancelar = True
                Set m_oUsuarios = oGestorSeguridad.DevolverTodosLosUsuarios(CargarNombres:=True)
        
        Case "BAJMINPUJA_ITEMS"
                Me.caption = m_sIdiomas(17)
                picBajMinPuja.Visible = True
                Me.Height = 1450
                picEdit.Top = 600
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ConfigurarControles", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcUsuSobre_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUsuSobre.Value = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUsuSobre_Click", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbcUsuSobre_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUsuSobre.Value = "..." Then
        sdbcUsuSobre.Value = ""
        Exit Sub
    End If
    sdbcUsuSobre.Value = sdbcUsuSobre.Columns(0).Value
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUsuSobre_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcUsuSobre_DropDown()
Dim arUsuarios() As TipoDatosUsuPer
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    sdbcUsuSobre.RemoveAll
    
    arUsuarios = oGestorSeguridad.BuscarTodosLosUsuariosDeTipoPersona(, , , , TipoOrdenacionPersonas.OrdPorCod)
    If UBound(arUsuarios) < 0 Then
        sdbcUsuSobre.Text = ""
    Else
        For i = 0 To UBound(arUsuarios) - 1
            sdbcUsuSobre.AddItem arUsuarios(i).USU & ", " & arUsuarios(i).Ape & ", " & arUsuarios(i).Nom & ", " & arUsuarios(i).Dep & Chr(m_lSeparador) & arUsuarios(i).USU
        Next
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUsuSobre_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcUsuSobre_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUsuSobre.DataFieldList = "Column 0"
    sdbcUsuSobre.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUsuSobre_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUsuSobre_PositionList(ByVal Text As String)
PositionList sdbcUsuSobre, Text
End Sub

Public Sub sdbcUsuSobre_Validate(Cancel As Boolean)
Dim arUsuarios() As TipoDatosUsuPer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUsuSobre.Value = "" Then Exit Sub
    
    If sdbcUsuSobre.Value = sdbcUsuSobre.Columns(0).Value Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    arUsuarios = oGestorSeguridad.BuscarTodosLosUsuariosDeTipoPersona(CodUsu:=sdbcUsuSobre.Text, Orden:=TipoOrdenacionPersonas.OrdPorCod, coincidenciatotal:=True)

    If UBound(arUsuarios) < 0 Then
        sdbcUsuSobre.Value = ""
    Else
        sdbcUsuSobre.Columns(1).Value = arUsuarios(0).USU
        sdbcUsuSobre.Value = arUsuarios(0).USU & ", " & arUsuarios(0).Ape & ", " & arUsuarios(0).Nom & ", " & arUsuarios(0).Dep
        sdbcUsuSobre.Columns(0).Value = sdbcUsuSobre.Value
    End If
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUsuSobre_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Liberar los objetos
''' </summary>
''' <param name="Cancel">Cancelar el cierre</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
picDest.Top = 30
picPago.Top = 30
picFechaLim.Top = 30
picSobre.Top = 30

Screen.MousePointer = vbNormal

m_bCancelar = False

g_sOrigen = ""
g_iCol = 0
Set m_oDestinos = Nothing
Set m_oPagos = Nothing
Set m_oProves = Nothing
Set m_oUnidades = Nothing
Set m_oUsuarios = Nothing
Set g_oPresupuestos1Nivel4 = Nothing
Set g_oPresupuestos2Nivel4 = Nothing
Set g_oPresupuestos3Nivel4 = Nothing
Set g_oPresupuestos4Nivel4 = Nothing
Set g_oPresupuestos1Nivel4Nuevos = Nothing
Set g_oPresupuestos2Nivel4Nuevos = Nothing
Set g_oPresupuestos3Nivel4Nuevos = Nothing
Set g_oPresupuestos4Nivel4Nuevos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcDestCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Value = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestCod.DroppedDown Then
        sdbcDestCod.Value = ""
        sdbcDestDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCod.Value = "..." Then
        sdbcDestCod.Value = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestDen.Value = sdbcDestCod.Columns(1).Value
    sdbcDestCod.Value = sdbcDestCod.Columns(0).Value
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cargar el combo Destinos a nivel de proceso/grupo tras un cambio en el ambito
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestCod_DropDown()
    Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    sdbcDestCod.RemoveAll
            
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(sdbcDestCod.Value, , , m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(sdbcDestCod.Value, , , m_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCod.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestCod.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestCod.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestCod.Rows = 0 Then
        sdbcDestCod.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcDestCod_PositionList(ByVal Text As String)
PositionList sdbcDestCod, Text
End Sub

''' <summary>
''' Validar lo q se ha puesto en el combo
''' </summary>
''' <param name="Cancel">Cancela pq no es valido</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestCod_Validate(Cancel As Boolean)
    Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCod.Value = "" Then Exit Sub

    If sdbcDestCod.Value = sdbcDestCod.Columns(0).Value Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Value = sdbcDestCod.Columns(1).Value
        m_bDestRespetarCombo = False
        Exit Sub
    End If

    If sdbcDestCod.Value = sdbcDestDen.Columns(1).Value Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Value = sdbcDestDen.Columns(0).Value
        m_bDestRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el destino
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , True, m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , True, m_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCod.Value = ""
        Screen.MousePointer = vbNormal
    ElseIf ADORs.EOF Then
        sdbcDestCod.Value = ""
        Screen.MousePointer = vbNormal
        ADORs.Close
        Set ADORs = Nothing
    Else
        m_bDestRespetarCombo = True
        sdbcDestDen.Value = ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value

        sdbcDestCod.Columns(0).Value = sdbcDestCod.Value
        sdbcDestCod.Columns(1).Value = sdbcDestDen.Value

        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = False

        ADORs.Close
        Set ADORs = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcDestDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestCod.Value = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestDen.DroppedDown Then
        sdbcDestDen.Value = ""
        sdbcDestCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestDen.Value = "..." Then
        sdbcDestDen.Value = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestCod.Value = sdbcDestDen.Columns(1).Value
    sdbcDestDen.Value = sdbcDestDen.Columns(0).Value
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cargar el combo Denom Destinos a nivel de proceso/grupo tras un cambio en el ambito
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestDen_DropDown()
    Dim ADORs As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestDen.RemoveAll
    
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, CStr(sdbcDestDen.Value), , m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, CStr(sdbcDestDen.Value), , m_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbcDestDen.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestDen.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestDen.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend

    If sdbcDestDen.Rows = 0 Then
        sdbcDestDen.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDestDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcDestDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcDestDen_PositionList(ByVal Text As String)
PositionList sdbcDestDen, Text
End Sub


Private Sub sdbcGrupoCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bGrupoRespetarCombo Then
        m_bGrupoRespetarCombo = True
        sdbcGrupoDen.Value = ""
        m_bGrupoRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupoCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcGrupoCod.DroppedDown Then
        sdbcGrupoCod.Value = ""
        sdbcGrupoDen.Value = ""
        m_lGrupoID = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupoCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGrupoCod.Value = "" Then Exit Sub
    
    If sdbcGrupoCod.Value = "..." Then
        sdbcGrupoCod.Value = ""
        Exit Sub
    End If
    
    m_bGrupoRespetarCombo = True
    sdbcGrupoDen.Value = sdbcGrupoCod.Columns(1).Value
    sdbcGrupoCod.Value = sdbcGrupoCod.Columns(0).Value
    m_lGrupoID = sdbcGrupoCod.Columns(2).Value
    m_bGrupoRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcGrupoCod_DropDown()
Dim oGrupo As CGrupo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGrupoCod.RemoveAll

    Screen.MousePointer = vbHourglass
    
    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
        If frmPROCE.g_oGrupoSeleccionado.Codigo <> oGrupo.Codigo And (oGrupo.Cerrado = 0 Or IsNull(oGrupo.Cerrado)) Then
            sdbcGrupoCod.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den & Chr(m_lSeparador) & oGrupo.Id
        End If
    Next

    If sdbcGrupoCod.Rows = 0 Then
        sdbcGrupoCod.AddItem ""
    End If
    
    Set oGrupo = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGrupoCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGrupoCod.DataFieldList = "Column 0"
    sdbcGrupoCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupoCod_PositionList(ByVal Text As String)
PositionList sdbcGrupoCod, Text
End Sub

Private Sub sdbcGrupoCod_Validate(Cancel As Boolean)
Dim oGrupo As CGrupo
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGrupoCod.Value = "" Then Exit Sub

    If sdbcGrupoCod.Value = sdbcGrupoCod.Columns(0).Value Then
        m_bGrupoRespetarCombo = True
        sdbcGrupoDen.Value = sdbcGrupoCod.Columns(1).Value
        m_lGrupoID = sdbcGrupoCod.Columns(2).Value
        m_bGrupoRespetarCombo = False
        Exit Sub
    End If

    If sdbcGrupoCod.Value = sdbcGrupoDen.Columns(1).Value Then
        m_bGrupoRespetarCombo = True
        sdbcGrupoDen.Value = sdbcGrupoDen.Columns(0).Value
        m_lGrupoID = sdbcGrupoCod.Columns(2).Value
        m_bGrupoRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el destino
    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
        If UCase(oGrupo.Codigo) = UCase(sdbcGrupoCod.Value) Then Exit For
        i = i + 1
    Next
    
    If i = frmPROCE.g_oProcesoSeleccionado.Grupos.Count Then
        sdbcGrupoCod.Value = ""
        Screen.MousePointer = vbNormal
    Else
        m_bGrupoRespetarCombo = True
        sdbcGrupoDen.Value = oGrupo.Den

        sdbcGrupoCod.Columns(0).Value = sdbcGrupoCod.Value
        sdbcGrupoCod.Columns(1).Value = sdbcGrupoDen.Value

        m_bGrupoRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGrupoDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bGrupoRespetarCombo Then
        m_bGrupoRespetarCombo = True
        sdbcGrupoCod.Value = ""
        m_bGrupoRespetarCombo = False
        m_lGrupoID = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupoDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcGrupoDen.DroppedDown Then
        sdbcGrupoDen.Value = ""
        sdbcGrupoCod.Value = ""
        m_lGrupoID = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupoDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGrupoDen.Value = "" Then Exit Sub

    If sdbcGrupoDen.Value = "..." Then
        sdbcGrupoDen.Value = ""
        Exit Sub
    End If
    
    m_bGrupoRespetarCombo = True
    sdbcGrupoCod.Value = sdbcGrupoDen.Columns(1).Value
    sdbcGrupoDen.Value = sdbcGrupoDen.Columns(0).Value
    m_lGrupoID = sdbcGrupoDen.Columns(2).Value
    m_bGrupoRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcGrupoDen_DropDown()
Dim oGrupo As CGrupo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGrupoDen.RemoveAll

    Screen.MousePointer = vbHourglass
    
    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
        If frmPROCE.g_oGrupoSeleccionado.Codigo <> oGrupo.Codigo And (oGrupo.Cerrado = 0 Or IsNull(oGrupo.Cerrado)) Then
            sdbcGrupoDen.AddItem oGrupo.Den & Chr(m_lSeparador) & oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Id
        End If
    Next

    If sdbcGrupoDen.Rows = 0 Then
        sdbcGrupoDen.AddItem ""
    End If
    
    Set oGrupo = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcGrupoDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGrupoDen.DataFieldList = "Column 0"
    sdbcGrupoDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcGrupoDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGrupoDen_PositionList(ByVal Text As String)
PositionList sdbcGrupoDen, Text
End Sub

Private Sub sdbcPagoCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Value = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPagoCod.DroppedDown Then
        sdbcPagoCod.Value = ""
        sdbcPagoDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPagoCod.Value = "..." Then
        sdbcPagoCod.Value = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoDen.Value = sdbcPagoCod.Columns(1).Value
    sdbcPagoCod.Value = sdbcPagoCod.Columns(0).Value
    m_bPagoRespetarCombo = False
    
    m_bPagoCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcPagoCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcPagoCod.RemoveAll
    
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbcPagoCod.Value), , , False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    End If
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoCod.AddItem "..."
    End If

    sdbcPagoCod.SelStart = 0
    sdbcPagoCod.SelLength = Len(sdbcPagoCod.Value)
    sdbcPagoCod.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcPagoCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPagoCod.DataFieldList = "Column 0"
    sdbcPagoCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcPagoCod_PositionList(ByVal Text As String)
PositionList sdbcPagoCod, Text
End Sub


Private Sub sdbcPagoCod_Validate(Cancel As Boolean)
Dim oPagos As CPagos
Dim bExiste As Boolean
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPagos = oFSGSRaiz.generar_CPagos
    If sdbcPagoCod.Value = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oPagos.CargarTodosLosPagos sdbcPagoCod.Value, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPagos.Count = 0)
    
    If Not bExiste Then
        sdbcPagoCod.Value = ""
    Else
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Value = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcPagoCod.Columns(0).Value = sdbcPagoCod.Value
        sdbcPagoCod.Columns(1).Value = sdbcPagoDen.Value
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = False
    End If
    
    Set oPagos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcPagoDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoCod.Value = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPagoDen.DroppedDown Then
        sdbcPagoDen.Value = ""
        sdbcPagoCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPagoDen.Value = "..." Then
        sdbcPagoDen.Value = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoCod.Value = sdbcPagoDen.Columns(1).Value
    sdbcPagoDen.Value = sdbcPagoDen.Columns(0).Value
    m_bPagoRespetarCombo = False
    
    m_bPagoCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcPagoDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.generar_CPagos
     
    sdbcPagoDen.RemoveAll
    
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , CStr(sdbcPagoDen.Value), True, False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
    End If
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoDen.AddItem "..."
    End If

    sdbcPagoDen.SelStart = 0
    sdbcPagoDen.SelLength = Len(sdbcPagoDen.Value)
    sdbcPagoDen.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcPagoDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPagoDen.DataFieldList = "Column 0"
    sdbcPagoDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcPagoDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcPagoDen_PositionList(ByVal Text As String)
PositionList sdbcPagoDen, Text
End Sub

Private Sub sdbcProveCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarComboProve Then
        m_bRespetarComboProve = True
        sdbcProveDen.Value = ""
        m_bRespetarComboProve = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod.Value = ""
        sdbcProveDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Value = ""
        Exit Sub
    End If
      
    If sdbcProveCod.Value = "" Then Exit Sub
    
    m_bRespetarComboProve = True
    sdbcProveDen.Value = sdbcProveCod.Columns(1).Value
    sdbcProveCod.Value = sdbcProveCod.Columns(0).Value
    m_bRespetarComboProve = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcProveCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll

    If bRestProvMatComp Then
        m_oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(sdbcProveCod.Text)
    Else
        m_oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveCod.Value), , , , False
    End If

    Codigos = m_oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Value)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcProveCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub


Private Sub sdbcProveCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Value = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor
    If bRestProvMatComp Then
        m_oProves.BuscarProveedoresConMatDelCompDesde 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(sdbcProveCod.Text)
    Else
        m_oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Value), , True, , False
    End If
    

    bExiste = Not (m_oProves.Count = 0)
    If bExiste Then bExiste = (UCase(m_oProves.Item(1).Cod) = UCase(sdbcProveCod.Value))
    
    If Not bExiste Then
        sdbcProveCod.Value = ""
    Else
        m_bRespetarComboProve = True
        sdbcProveDen.Value = m_oProves.Item(1).Den
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Value
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Value
        m_bRespetarComboProve = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcProveDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarComboProve Then
        m_bRespetarComboProve = True
        sdbcProveCod.Value = ""
        m_bRespetarComboProve = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod.Value = ""
        sdbcProveDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Value = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    m_bRespetarComboProve = True
    sdbcProveCod.Value = sdbcProveDen.Columns(1).Value
    sdbcProveDen.Value = sdbcProveDen.Columns(0).Value
    m_bRespetarComboProve = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcProveDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll

    If bRestProvMatComp Then
        m_oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen.Value)
    Else
        m_oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveDen.Value, , True, False
    End If
    
    Codigos = m_oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Value)
    sdbcProveCod.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcProveDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcProveDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub


Private Sub sdbcUnidadCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bUniRespetarCombo Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Value = ""
        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcUnidadCod.DroppedDown Then
        sdbcUnidadCod.Value = ""
        sdbcUnidadDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcUnidadCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Value = "..." Then
        sdbcUnidadCod.Value = ""
        Exit Sub
    End If
    
    m_bUniRespetarCombo = True
    sdbcUnidadDen.Value = sdbcUnidadCod.Columns(1).Value
    sdbcUnidadCod.Value = sdbcUnidadCod.Columns(0).Value
    m_bUniRespetarCombo = False
    
    m_bUniCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcUnidadCod_DropDown()
Dim oUni As CUnidad

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.RemoveAll

    Screen.MousePointer = vbHourglass
    If m_bUniCargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidades CStr(sdbcDestCod.Value)
    Else
        m_oUnidades.CargarTodasLasUnidades
    End If

    For Each oUni In m_oUnidades
        sdbcUnidadCod.AddItem oUni.Cod & Chr(m_lSeparador) & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next

    If sdbcUnidadCod.Rows = 0 Then
        sdbcUnidadCod.AddItem ""
    End If

    Set oUni = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcUnidadCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.DataFieldList = "Column 0"
    sdbcUnidadCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcUnidadCod_PositionList(ByVal Text As String)
PositionList sdbcUnidadCod, Text
End Sub


Private Sub sdbcUnidadCod_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Value = "" Then Exit Sub

    If sdbcUnidadCod.Value = sdbcUnidadCod.Columns(0).Value Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Value = sdbcUnidadCod.Columns(1).Value
        m_bUniRespetarCombo = False
        Exit Sub
    End If

    If sdbcUnidadCod.Value = sdbcUnidadDen.Columns(1).Value Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Value = sdbcUnidadDen.Columns(0).Value
        m_bUniRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el destino
    m_oUnidades.CargarTodasLasUnidades CStr(sdbcUnidadCod.Value), , True
    If m_oUnidades.Count = 0 Then
        sdbcUnidadCod.Value = ""
        Screen.MousePointer = vbNormal
    Else
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Value = m_oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den

        sdbcUnidadCod.Columns(0).Value = sdbcUnidadCod.Value
        sdbcUnidadCod.Columns(1).Value = sdbcUnidadDen.Value

        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcUnidadDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bUniRespetarCombo Then
        m_bUniRespetarCombo = True
        sdbcUnidadCod.Value = ""
        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcUnidadDen.DroppedDown Then
        sdbcUnidadDen.Value = ""
        sdbcUnidadCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadDen.Value = "..." Then
        sdbcUnidadDen.Value = ""
        Exit Sub
    End If
    
    m_bUniRespetarCombo = True
    sdbcUnidadCod.Value = sdbcUnidadDen.Columns(1).Value
    sdbcUnidadDen.Value = sdbcUnidadDen.Columns(0).Value
    m_bUniRespetarCombo = False
    
    m_bUniCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbcUnidadDen_DropDown()
Dim oUni As CUnidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If m_bUniCargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidades , CStr(sdbcUnidadDen.Value), , True
    Else
        m_oUnidades.CargarTodasLasUnidades , , , True
    End If

    For Each oUni In m_oUnidades
        sdbcUnidadDen.AddItem oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oUni.Cod
    Next

    If sdbcUnidadDen.Rows = 0 Then
        sdbcUnidadDen.AddItem ""
    End If

    Set oUni = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcUnidadDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadDen.DataFieldList = "Column 0"
    sdbcUnidadDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "sdbcUnidadDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcUnidadDen_PositionList(ByVal Text As String)
PositionList sdbcUnidadDen, Text
End Sub

''' <summary>Cambio de la cantidad m�nima de bajada para una puja</summary>
''' <remarks>Llamada desde: cmdAceptar_Click;ModifCantItems Tiempo m�ximo</remarks>
''' <revision>LTG 29/03/2011</revision>

Private Sub ModifBajMinPuja()
    Dim oItem As CItem
    Dim oItems As CItems
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim bErrores As Boolean
    Dim vErrores As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oItems = oFSGSRaiz.Generar_CItems
    Set oItem = oFSGSRaiz.Generar_CItem
    
    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
        
        With oItem
            .MinPujGanador = Trim(txtBajMinPuja.Text)
            .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                Case EnItem
                        .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                            .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                        Else
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                Case EnItem
                        .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                            .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                        Else
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                Case EnItem
                        .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                            .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                            .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                        Else
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        End If
                Case EnProceso
                        .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                        .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
            End Select
            
            Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                Case EnItem
                        .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                Case EnGrupo
                        .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                Case EnProceso
                        .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
            End Select
            
            If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                .ArticuloCod = Null
                .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            
            If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                .Descr = Null
                .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            
            .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        
        End With
        
        oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , , oItem.FECACT, , oItem.MinPujGanador
    
    Next

    Screen.MousePointer = vbHourglass
    teserror = oItems.ModificarValores(, , , , , , , , , , , , , basOptimizacion.gvarCodUsuario)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESVolumenAdjDirSuperado Then
            basErrores.TratarError teserror
            Set oItems = Nothing
            Set oItem = Nothing
            Unload Me
            frmPROCE.sdbgItems.SelBookmarks.RemoveAll
            Exit Sub
        ElseIf frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then
            basErrores.TratarError teserror
        End If
    Else
        bErrores = teserror.Arg2
        vErrores = teserror.Arg1
        If bErrores Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
            Screen.MousePointer = vbHourglass
        End If
    End If
    
    If teserror.NumError = TESVolumenAdjDirSuperado Then
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            frmPROCE.sdbgItems.Columns("BAJ_PUJA").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).MinPujGanador
            frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
        Next
    Else
        bErrores = teserror.Arg2
        vErrores = teserror.Arg1
        If bErrores Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
            Screen.MousePointer = vbHourglass
        End If
        
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            If vErrores(1, i) = 0 Then 'Ning�n error
                frmPROCE.sdbgItems.Columns("BAJ_PUJA").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).MinPujGanador
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).FECACT
            End If
        Next
    End If
    
    frmPROCE.sdbgItems.SelBookmarks.RemoveAll
    frmPROCE.sdbgItems.Update
    frmPROCE.g_bUpdate = True
    Screen.MousePointer = vbNormal

    Set oItems = Nothing
    Set oItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifBajMinPuja", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub txtFecFinSub_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If IsDate(txtFecFinSub.Text) Then
    If txtHoraFinSub.Text = "" Then txtHoraFinSub.Text = TimeValue("23:59")
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "txtFecFinSub_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtFecIniSub_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If IsDate(txtFecIniSub.Text) Then
    If txtHoraIniSub.Text = "" Then txtHoraIniSub.Text = TimeValue("00:00")
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "txtFecIniSub_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtFecLim_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If IsDate(txtFecLim.Text) Then
    If txtHoraLim.Text = "" Then txtHoraLim.Text = TimeValue("23:59")
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "txtFecLim_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Mira si hay que recalcular las ofertas por cambiar la cantidad de �tems
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;ModifCantItems Tiempo m�ximo</remarks>
Private Function RecalcularOfertas() As Boolean
Dim iOfertas As Integer
Dim bCambioCantidad As Boolean
Dim i As Integer
Dim irespuesta As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bCambioCantidad = False
    'Si hay ofertas y cambia la cantidad hay que recalcular
    iOfertas = frmPROCE.g_oProcesoSeleccionado.tieneofertas
    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
        If txtCant.Text <> frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) Then
                bCambioCantidad = True
                'Si se modifica la cantidad de un �tem hay que recalcular las ofertas guardadas.
                If iOfertas > 0 Then
                    'Como tiene ofertas guardadas hay que realizar el recalculo.
                    m_bRecalcularOfertas = True
                    Exit For
                End If
        End If
    Next
    RecalcularOfertas = True
    If bCambioCantidad And iOfertas > 1 Then
        irespuesta = oMensajes.AvisoConConfirmacion(frmPROCE.m_sMensajeModifCant)
        If irespuesta = vbNo Then
            RecalcularOfertas = False
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "RecalcularOfertas", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function


'--<summary>
'--Carga los escalados en el grid
'--</summary>
'--<remarks>Llamada desde Form-Load</remarks>
'--<revision>DPD 09/08/2011</revision>
Sub CargarEscalados()

Dim cEsc As CEscalado


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set g_ogrupo = frmPROCE.g_oGrupoSeleccionado
g_ogrupo.CargarEscalados


If Not g_ogrupo.Escalados Is Nothing Then

    Dim i As Long
    Dim Linea As String
    For Each cEsc In g_ogrupo.Escalados
    
        Linea = cEsc.Id & Chr(m_lSeparador)
    
        'Set cEsc = Me.g_oGrupoSeleccionado.Escalados.Item(i)
        Linea = Linea & cEsc.Inicial & Chr(m_lSeparador) & cEsc.Inicial & Chr(m_lSeparador)
        
        If g_ogrupo.TipoEscalados = ModoRangos Then
            Linea = Linea & cEsc.final & Chr(m_lSeparador)
        Else
            Linea = Linea & "" & Chr(m_lSeparador)
        End If
        
        Linea = Linea & cEsc.Presupuesto
        
        Me.SSDBGridEscalados.AddItem Linea

    Next
End If

If g_ogrupo.TipoEscalados = ModoRangos Then
    ModoARangos
Else
    ModoADirecto
End If
Set cEsc = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "CargarEscalados", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

'Establece el modo de escalado en Rangos
Sub ModoARangos()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados.Columns("DIR").Visible = False
    SSDBGridEscalados.Columns("INI").Visible = True
    SSDBGridEscalados.Columns("FIN").Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModoARangos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub
'Establece el modo de escalado en Cantidades directas
Sub ModoADirecto()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados.Columns("DIR").Visible = True
    SSDBGridEscalados.Columns("INI").Visible = False
    SSDBGridEscalados.Columns("FIN").Visible = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModoADirecto", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


'--<summary>
'--Comprueba que se han introducido correctamente los presupuestos unitarios necesarios
'--En caso de encontrar un valor no  v�lido muestra un mensaje, y se posiciona en la celda en modo edici�n
'--</summary>
'--<remarks>Click Bot�n Aceptar</remarks>
'--<revision>DPD 08/08/2011</revision>

Function ComprobarFormulario() As Boolean
    Dim vPres As Variant
    Dim i As Long
    Dim bm As Variant ' Bookmark
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtPresUniItem.Text <> "" Then
        If Not IsNumeric(txtPresUniItem.Text) Then
            oMensajes.NoValido m_sPresUniItem
            ComprobarFormulario = False
            Exit Function
        End If
    End If

    If SSDBGridEscalados.DataChanged Then SSDBGridEscalados.Update
        
    For i = 0 To Me.SSDBGridEscalados.Rows
        'Comprobamos los presupuestos
        bm = SSDBGridEscalados.AddItemBookmark(i)
        vPres = SSDBGridEscalados.Columns("PRES").CellValue(bm)
        If (Not IsNumeric(vPres)) And CStr(vPres) <> "" Then
            oMensajes.valorCampoNoValido
            SSDBGridEscalados.Bookmark = bm
            SSDBGridEscalados.col = SSDBGridEscalados.Columns("PRES").Position
            SSDBGridEscalados.Columns("PRES").Value = " "
            SSDBGridEscalados.Columns("PRES").Value = ""
            ComprobarFormulario = False
            Exit Function
        End If
    Next i

    ComprobarFormulario = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ComprobarFormulario", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Modifica los presupuestos unitarios escalados en los items seleccionados
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;cmdAceptar_Click() Tiempo m�ximo</remarks>
''' <revision>LTG 06/02/2012</revision>

Private Function ModifPrecItemsEsc() As Boolean
    Dim oItems As CItems
    Dim oItem As CItem
    Dim oEscalados As CEscalados
    Dim oEscalado As CEscalado
    Dim teserror As TipoErrorSummit
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ModifPrecItemsEsc = True
    
    Set oItems = oFSGSRaiz.Generar_CItems
    Dim bm As Variant
    Dim i As Long
    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
        Set oItem = oFSGSRaiz.Generar_CItem
        oItem.Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        oItem.grupoCod = frmPROCE.sdbgItems.Columns("GRUPO").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        oItem.GrupoID = frmPROCE.sdbgItems.Columns("GRUPOID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        Set oItem.proceso = frmPROCE.g_oProcesoSeleccionado
        oItem.FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        If frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) <> "" Then
            oItem.Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        End If
        oItem.ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        oItem.UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        If txtPresUniItem.Text <> "" Then oItem.Precio = txtPresUniItem.Text
        oItems.Add frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , oItem.grupoCod, oItem.FECACT, lGrupoID:=oItem.GrupoID
    Next
    Dim Id As Long
    
    Set oEscalados = oFSGSRaiz.Generar_CEscalados
    oEscalados.modo = frmPROCE.g_oGrupoSeleccionado.TipoEscalados
    
    If SSDBGridEscalados.DataChanged Then SSDBGridEscalados.Update
    For i = 0 To SSDBGridEscalados.Rows - 1
        bm = SSDBGridEscalados.AddItemBookmark(i)
        If CStr(SSDBGridEscalados.Columns("ID").CellValue(bm)) = "" Then
            Id = 0
        Else
            Id = SSDBGridEscalados.Columns("ID").CellValue(bm)
        End If
            
        If frmPROCE.g_oGrupoSeleccionado.TipoEscalados = ModoRangos Then
            Set oEscalado = oEscalados.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(bm), SSDBGridEscalados.Columns("FIN").CellValue(bm), SSDBGridEscalados.Columns("PRES").CellValue(bm))
        Else
            Set oEscalado = oEscalados.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(bm), Null, SSDBGridEscalados.Columns("PRES").CellValue(bm))
        End If
        
    Next i
    
    teserror = oEscalados.GuardarPresupuestosItems(oItems, Me.chkPresEsc.Value, Me.chkPresPlanifEsc.Value)
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        ModifPrecItemsEsc = False
        Exit Function
    End If
    'Marca par que no se actualice la base de datos al actualizar el grid
    frmPROCE.g_bUpdate = False
    
    LockWindowUpdate frmPROCE.hWnd 'Para bloquear

    'Actualizamos el grid
    Dim aux
    Dim bmAct As Variant
    bmAct = frmPROCE.sdbgItems.Bookmark
    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
        'bm = frmPROCE.sdbgItems.AddItemBookmark(i)
        bm = frmPROCE.sdbgItems.SelBookmarks.Item(i)
        frmPROCE.sdbgItems.Bookmark = bm
        frmPROCE.sdbgItems.Columns("PREC").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Precio  'oEscalados.PresupuestoCantidad(frmPROCE.sdbgItems.Columns("CANT").Value)
        frmPROCE.sdbgItems.Columns("PRES").Value = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))).Presupuesto
        
        'Recalcular presupuestos
        frmPROCE.g_dPresGrupo = frmPROCE.g_dPresGrupo - val(frmPROCE.sdbgItems.Columns("PRESACTUAL").Value) + val(frmPROCE.sdbgItems.Columns("PRES").Value)
        frmPROCE.g_oProcesoSeleccionado.PresAbierto = (frmPROCE.g_oProcesoSeleccionado.PresAbierto - val(frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))) + val(frmPROCE.sdbgItems.Columns("PRES").Value)
        
        frmPROCE.sdbgItems.Columns("PRESACTUAL").Value = frmPROCE.sdbgItems.Columns("PRES").Value
        frmPROCE.sdbgItems.Columns("FECACT").Value = oItems.Item(frmPROCE.sdbgItems.Columns("ID").Value).FECACT
        'CDbl(frmPROCE.sdbgItems.Columns("CANT").Value) * CDbl(frmPROCE.sdbgItems.Columns("PREC").Value)
    Next i
    If frmPROCE.g_dPresGrupo = 0 Then
        frmPROCE.lblPresGrupo.caption = ""
    Else
        frmPROCE.lblPresGrupo.caption = Format(frmPROCE.g_dPresGrupo, "#,##0.00####################")
        frmPROCE.lblPresTotalProce = Format(frmPROCE.g_oProcesoSeleccionado.PresAbierto, "#,##0.00####################")
    End If
    
    frmPROCE.sdbgItems.Bookmark = bmAct
    frmPROCE.sdbgItems.Update
    'Reestablecemos la marca de control de cambios del grid
    frmPROCE.g_bUpdate = True

    LockWindowUpdate 0& ' Para desbloquear
    Set oItem = Nothing
    Set oItems = Nothing
    Set oEscalado = Nothing
    Set oEscalados = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmDatoAmbitoProce", "ModifPrecItemsEsc", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
